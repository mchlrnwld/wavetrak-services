import HDCams from '../../components/Icons/HDCams';
import PremiumAnalysis from '../../components/Icons/PremiumAnalysis';
import EnhancedTools from '../../components/Icons/EnhancedTools';
import Calendar from '../../components/Icons/Calendar';

const translations = {
  premiumCTA: {
    ForecastVisualizerCTA: {
      header: {
        icon: Calendar,
        title: 'Want the Complete, Long-Range Forecast?',
      },
      linkText: 'Start Free Trial',
      mainText: '',
      learnMoreText: 'Learn more about Surfline Premium',
      features: {
        forecasts: {
          icon: PremiumAnalysis,
          title: 'Trusted Forecast Team',
          description: 'Plan your next trip or session with expert guidance.',
        },
        expert: {
          icon: EnhancedTools,
          title: 'Exclusive Forecast Tools',
          description: 'Access curated condition reports and proprietary surf, wind and buoy data.',
        },
        cams: {
          icon: HDCams,
          title: 'Ad-Free and Premium Cams',
          description:
            'Watch live conditions ad-free -- and access exclusive, high-resolution cams.',
        },
      },
    },
    ChartRangeSelectorCTA: {
      mainText: 'Want the Complete, Long-Range Forecast?',
      linkText: 'Start Free Trial',
    },
    SpotReportCamHeaderCTA: {
      text: 'Stream Live Conditions ad free -',
      linkText: 'Start Free Trial',
    },
    SubHeader: {
      textBefore: 'Unlock premium cam',
      text: ' stream',
      textAfter: 's',
      linkText: 'START FREE TRIAL',
    },
    MobileForecastCTA: {
      text: 'Plan ahead with 16 day forecasts. ',
      linkText: 'Start Free Trial',
    },
    RegionForecastUpdateCTA: {
      defaultText: "Looking for today's Best Bet, along with expert forecast analysis?",
      spotPageText: 'Looking for expert forecast analysis on this spot?',
      linkText: 'Start Free Trial',
    },
  },
  spotListHeader: {
    loading: {
      text: 'Finding spots',
      optionalText: 'please wait or move map',
    },
    topSpots: {
      text: 'Displaying top spots in your map view',
      optionalText: 'zoom in to see more',
    },
    noSpots: {
      text: 'No spots available',
      optionalText: 'please reload the page or move map',
    },
  },
  SpotForecast: {
    buoyError: {
      message: 'We apologize, we are working to restore buoys for this spot ASAP.',
    },
  },
  SpotPage: {
    header: {
      titleSuffix: 'Surf Report & Forecast',
    },
    conditionsNoneText: 'conditions unobservable',
    conditionsExpiredText: 'Report Coming Soon',
    pageLevelLinkText: {
      today: {
        text: 'Today',
        title: (spotName) => `${spotName} Surf Report & Forecast`,
      },
      spotForecast: {
        text: 'Spot Forecast',
        title: (spotName) => `${spotName} Surf Forecast`,
      },
    },
    externalLinkText: {
      region: 'Regional Forecast',
      premiumAnalysis: 'Premium Analysis',
      buoys: 'Buoys',
      charts: 'Charts',
      travel: 'Travel',
      camRewind: 'Cam Rewind',
    },
  },
  RegionalForecast: {
    header: {
      titleSuffix: 'Regional Surf Forecast',
    },
    pageLevelLinkText: {
      forecast: {
        text: 'Regional Forecast',
        title: (subregionName) => `${subregionName} Regional Surf Forecast`,
        path: (subregionName, subregionId) => `/surf-forecasts/${subregionName}/${subregionId}`,
      },
      premiumAnalysis: {
        text: 'Premium Analysis',
        title: (subregionName) => `${subregionName} Premium Analysis`,
        path: (subregionName, subregionId) =>
          `/surf-forecasts/${subregionName}/${subregionId}/premium-analysis`,
      },
      charts: {
        text: 'Charts',
        title: (subregionName) => `${subregionName} Charts`,
        path: (subregionName, subregionId) =>
          `/surf-forecasts/${subregionName}/${subregionId}/charts`,
      },
    },
    externalLinkText: {
      buoys: 'Buoys',
      charts: 'Charts',
      travel: 'Travel',
    },
  },
  ForecastHeader: {
    menuHeader: 'More',
    camsReportsTitle: 'Cams & Reports',
    spotForecastsTitle: 'Spot Forecasts',
    nearbySpotsTitle: 'Nearby Spots',
    nearbySubregionsTitle: 'Nearby Regions',
  },
  SpotCamera: {
    CamList: {
      nearbyError: {
        title: 'Loading Error',
        message: 'We apologize, we are working to restore service ASAP. ',
        link: {
          beforeText: 'Check out ',
          linkText: 'Cams & Forecasts',
          afterText: ' instead.',
        },
      },
    },
  },
  ChartPlayer: {
    description: 'Access long-range charts',
    link: 'Start Free Trial',
  },
  swellTrendSlider: {
    tooltipPaywall: 'Premium members get the detailed 16-day forecast.',
    hourlyPremiumTooltip: 'The 1-day hourly forecast is only available up to 5 days.',
    hourlyFreeTooltip: 'Premium Members get five days of hourly wind.',
  },
};

export default translations;
