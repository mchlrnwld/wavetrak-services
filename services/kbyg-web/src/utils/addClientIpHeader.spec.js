import { expect } from 'chai';
import addClientIpHeader from './addClientIpHeader';

describe('utils / addClientIpHeader', () => {
  it('returns the formatted client ip in a header', () => {
    expect(addClientIpHeader('8.8.8.8')).to.deep.equal({ 'x-client-ip': '8.8.8.8' });
  });

  it('returns null when clientIp is falsey', () => {
    expect(addClientIpHeader(null)).to.equal(null);
    expect(addClientIpHeader(undefined)).to.equal(null);
    expect(addClientIpHeader(0)).to.equal(null);
  });
});
