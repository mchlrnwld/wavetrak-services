import { canUseDOM, getWindow } from '@surfline/web-common';

const win = getWindow();

export const getMapPageHeight = () => {
  if (canUseDOM) {
    return { height: `${win.innerHeight - 96}px` };
  }

  return {
    height: '0px',
  };
};

export const getMapPageWidth = () => {
  if (canUseDOM) {
    return win.innerWidth;
  }

  return 0;
};
