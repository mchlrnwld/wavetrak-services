import { expect } from 'chai';
import midnightOfDate from './midnightOfDate';

describe('midnightOfDate', () => {
  it('returns midnight of timestamp local to UTC offset', () => {
    expect(midnightOfDate(-8, 1519369200)) // 7am UTC, 11pm the day before in UTC-8
      .to.deep.equal(1519286400); // 8am UTC, Midnight the day before in UTC-8
    expect(midnightOfDate(+8, 1519405200)) // 5pm UTC, 1am the day after in UTC+8
      .to.deep.equal(1519401600); // 4pm UTC, Midnight the day after in UTC+8
    expect(midnightOfDate(+2, 1519383600)) // 11am UTC, 1pm the same day in UTC+2
      .to.deep.equal(1519336800); // 10pm the day before, Midnight the same day in UTC+2
  });
});
