import { expect } from 'chai';
import { nextUpdateDate } from './formatReportDate';

describe('utils / nextUpdateDate', () => {
  it('should properly format the next forecast date', () => {
    const nextUpdate = 1632258000;
    const utcOffset = -7;
    const abbrTimezone = 'PDT';
    const nextForecastDate = nextUpdateDate(nextUpdate, utcOffset, abbrTimezone);
    expect(nextForecastDate).to.equal('Next update Tue by 2pm PDT');
  });

  it('should properly format the next forecast date across DST change', () => {
    const nextUpdate = 1637532000;
    const utcOffset = -8;
    const abbrTimezone = 'PST';
    const nextForecastDate = nextUpdateDate(nextUpdate, utcOffset, abbrTimezone);
    expect(nextForecastDate).to.equal('Next update Sun by 2pm PST');
  });
});
