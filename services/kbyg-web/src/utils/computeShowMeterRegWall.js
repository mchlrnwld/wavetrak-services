/**
 * @param {string} urlPath - current url path
 * @description This util function is used to check if the urlPath is rate limited or not.
 * @returns Boolean showMeterRegWall true|false
 */

const computeShowMeterRegWall = (urlPath) => {
  const rateLimitedPaths = [
    '/surf-report/',
    '/surf-reports-forecasts-cams/',
    '/surf-reports-forecasts-cams-map',
    '/surf-forecasts/',
    '/surf-charts/',
    '/surf-cams/',
    '/buoy-report',
  ];
  const showMeterRegWall = rateLimitedPaths.some((item) => urlPath.includes(item));
  return showMeterRegWall;
};

export default computeShowMeterRegWall;
