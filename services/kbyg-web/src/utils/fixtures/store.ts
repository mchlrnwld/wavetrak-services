import { FEET } from '@surfline/web-common';
import { DateFormat, Height, Speed, SurfHeight, Temperature } from '../../types/units';
import { MDY } from '../../common/constants';

export const getBackplaneSettingsFixture = ({
  dateFormat = MDY,
  surfHeight = FEET,
  swellHeight = FEET,
  tideHeight = FEET,
  windSpeed = 'MPH',
  temperature = 'F',
}: {
  dateFormat?: DateFormat;
  surfHeight?: SurfHeight;
  swellHeight?: SurfHeight;
  tideHeight?: Height;
  windSpeed?: Speed;
  temperature?: Temperature;
} = {}) => ({
  units: {
    surfHeight,
    swellHeight,
    tideHeight,
    windSpeed,
    temperature,
  },
  feed: {
    localized: 'LOCALIZED',
  },
  date: {
    format: dateFormat,
  },
  navigation: {
    spotsTaxonomyLocation: '58f7f00ddadb30820bb69bbc',
    forecastsTaxonomyLocation: '58f7ef37dadb30820bb5a7e8',
  },
  recentlyVisited: {
    subregions: [
      {
        _id: '58581a836630e24c44878fd6',
        name: 'North Orange County',
      },
    ],
    spots: [
      {
        _id: '5842041f4e65fad6a7708819',
        name: 'Venice Beach',
      },
    ],
  },
  feedFilters: {
    subregions: '',
  },
  model: 'LOTUS',
  onboarded: ['FAVORITE_SPOTS'],
  boardTypes: [],
  travelDistance: null,
  bwWindWaveModel: 'LOTUS',
  preferredWaveChart: 'SURF',
  homeSpot: null,
  homeForecast: null,
  isSwellChartCollapsed: true,
  receivePromotions: true,
  _id: '5afb450817841780df30ffee',
  user: '5afb4508951a7900112f9934',
  __v: 0,
  createdAt: '2018-05-15T20:37:28.708Z',
  updatedAt: '2021-06-02T18:00:15.588Z',
});

export const getLocationViewFixture = () => ({
  taxonomy: {
    _id: '58f7ed54dadb30820bb38b79',
    geonameId: 5393052,
    type: 'geoname',
    liesIn: ['58f7ed54dadb30820bb38b6a'],
    geonames: {
      fcode: 'PPLA2',
      lat: '36.97412',
      adminName1: 'California',
      fcodeName: 'seat of a second-order administrative division',
      countryName: 'United States',
      fclName: 'city, village,...',
      name: 'Santa Cruz',
      countryCode: 'US',
      population: 64220,
      fcl: 'P',
      countryId: '6252001',
      toponymName: 'Santa Cruz',
      geonameId: 5393052,
      lng: '-122.0308',
      adminCode1: 'CA',
    },
    location: {
      coordinates: [-122.0308, 36.97412],
      type: 'Point',
    },
    enumeratedPath: ',Earth,North America,United States,California,Santa Cruz County,Santa Cruz',
    name: 'Santa Cruz',
    category: 'geonames',
    hasSpots: true,
    associated: {
      links: [
        {
          key: 'taxonomy',
          href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5393052&type=geoname',
        },
        {
          key: 'www',
          href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/santa-cruz-county/santa-cruz/5393052',
        },
        {
          key: 'travel',
          href: 'https://sandbox.surfline.com/travel/united-states/california/santa-cruz-county/santa-cruz-surfing-and-beaches/5393052',
        },
      ],
    },
    in: [
      {
        _id: '58f7ed54dadb30820bb38b6a',
        geonameId: 5393068,
        type: 'geoname',
        liesIn: ['58f7ed51dadb30820bb387a6'],
        geonames: {
          fcode: 'ADM2',
          lat: '37.02161',
          adminName1: 'California',
          fcodeName: 'second-order administrative division',
          countryName: 'United States',
          fclName: 'country, state, region,...',
          name: 'Santa Cruz County',
          countryCode: 'US',
          population: 262382,
          fcl: 'A',
          countryId: '6252001',
          toponymName: 'Santa Cruz County',
          geonameId: 5393068,
          lng: '-122.00979',
          adminCode1: 'CA',
        },
        location: {
          coordinates: [-122.00979, 37.02161],
          type: 'Point',
        },
        enumeratedPath: ',Earth,North America,United States,California,Santa Cruz County',
        name: 'Santa Cruz County',
        category: 'geonames',
        hasSpots: true,
        depth: 1,
        associated: {
          links: [
            {
              key: 'taxonomy',
              href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5393068&type=geoname',
            },
            {
              key: 'www',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/santa-cruz-county/5393068',
            },
            {
              key: 'travel',
              href: 'https://sandbox.surfline.com/travel/united-states/california/santa-cruz-county-surfing-and-beaches/5393068',
            },
          ],
        },
      },
    ],
    contains: [
      {
        _id: '58f80ab7dadb30820bd14cbc',
        spot: '584204214e65fad6a7709d20',
        type: 'spot',
        liesIn: ['58f7ed54dadb30820bb38b79', '58f7ed54dadb30820bb38b94'],
        location: {
          coordinates: [-122.02321529388428, 36.960814372533086],
          type: 'Point',
        },
        name: 'Cowells Overview',
        category: 'surfline',
        hasSpots: false,
        depth: 0,
        associated: {
          links: [
            {
              key: 'taxonomy',
              href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=584204214e65fad6a7709d20&type=spot',
            },
            {
              key: 'api',
              href: 'http://spots-api.sandbox.surfline.com/admin/spots/584204214e65fad6a7709d20',
            },
            {
              key: 'www',
              href: 'https://sandbox.surfline.com/surf-report/cowells-overview/584204214e65fad6a7709d20',
            },
          ],
        },
      },
    ],
  },
  breadCrumbs: [
    {
      name: 'United States',
      url: '/united-states/6252001',
      geonameId: 6252001,
      id: '58f7ed51dadb30820bb3879c',
    },
    {
      name: 'California',
      url: '/united-states/california/5332921',
      geonameId: 5332921,
      id: '58f7ed51dadb30820bb387a6',
    },
    {
      name: 'Santa Cruz County',
      url: '/united-states/california/santa-cruz-county/5393068',
      geonameId: 5393068,
      id: '58f7ed54dadb30820bb38b6a',
    },
    {
      name: 'Santa Cruz',
      url: '/united-states/california/santa-cruz-county/santa-cruz/5393052',
      geonameId: 5393052,
      id: '58f7ed54dadb30820bb38b79',
    },
  ],
  boundingBox: {
    north: 36.96632852130351,
    south: 36.94785295955728,
    east: -121.9628,
    west: -122.1264825495626,
  },
  travelContent: null,
  siblings: [
    {
      _id: '58f7edd1dadb30820bb4144b',
      geonameId: 5330222,
      type: 'geoname',
      liesIn: ['58f7ed54dadb30820bb38b6a'],
      geonames: {
        fcode: 'PPL',
        lat: '37.12606',
        adminName1: 'California',
        fcodeName: 'populated place',
        countryName: 'United States',
        fclName: 'city, village,...',
        name: 'Boulder Creek',
        countryCode: 'US',
        population: 4923,
        fcl: 'P',
        countryId: '6252001',
        toponymName: 'Boulder Creek',
        geonameId: 5330222,
        lng: '-122.12219',
        adminCode1: 'CA',
      },
      location: {
        coordinates: [-122.12219, 37.12606],
        type: 'Point',
      },
      enumeratedPath:
        ',Earth,North America,United States,California,Santa Cruz County,Boulder Creek',
      name: 'Boulder Creek',
      category: 'geonames',
      hasSpots: true,
      depth: 0,
      associated: {
        links: [
          {
            key: 'taxonomy',
            href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5330222&type=geoname',
          },
          {
            key: 'www',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/santa-cruz-county/boulder-creek/5330222',
          },
          {
            key: 'travel',
            href: 'https://sandbox.surfline.com/travel/united-states/california/santa-cruz-county/boulder-creek-surfing-and-beaches/5330222',
          },
        ],
      },
      distance: 18.739940240955768,
    },
  ],
  url: '/united-states/california/santa-cruz-county/santa-cruz/5393052',
});
