export const upperCaseFirstLetter = (string) => {
  const allLower = string.toLowerCase();
  return `${allLower.charAt(0).toUpperCase()}${allLower.slice(1)}`;
};
