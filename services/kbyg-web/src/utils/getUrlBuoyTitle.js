import { slugify } from '@surfline/web-common';

const getUrlBuoyTitle = (title) => {
  const titleArray = title.split('(');
  const urlTitle = slugify(titleArray[0].trim());
  return urlTitle;
};

export default getUrlBuoyTitle;
