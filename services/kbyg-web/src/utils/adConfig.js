import getUserType from './getUserType';

const adConfigMap = {
  forecastFeedMobile: {
    adUnit: '/1024858/Forecast_Feed_Mobile',
    adId: 'Forecast_Feed_Mobile',
    adSizes: [300, 250],
    adSizeDesktop: [],
    adSizeMobile: [300, 250],
    adViewType: 'SUBREGION',
  },
  forecastFeedMobileBottom: {
    adUnit: '/1024858/Forecast_Feed_Mobile_Bottom',
    adId: 'Forecast_Feed_Mobile_Bottom',
    adSizes: [300, 250],
    adSizeDesktop: [],
    adSizeMobile: [300, 250],
    adViewType: 'SUBREGION',
  },
  camHostAd: {
    adUnit: '/1024858/Text_Link',
    adId: 'Text_Link',
    adSizes: ['fluid'],
    adSizeMappings: [],
    adViewType: 'SPOT',
  },
  spotMiddle: {
    adUnit: '/1024858/SL_Cams_Middle',
    adId: 'SL_Cams_Middle',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeDesktop: [
      [970, 90],
      [970, 250],
      [728, 90],
    ],
    adSizeMobile: [300, 250],
    adViewType: 'SPOT',
  },
  sessionClipBottom: {
    adUnit: '/1024858/SL_Sessions_Bottom',
    adId: 'SL_Sessions_Bottom',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeDesktop: [
      [970, 90],
      [970, 250],
      [728, 90],
    ],
    adSizeMobile: [300, 250],
    adViewType: 'SESSIONS_CLIPS',
  },
};

const loadAdConfig = (
  adIdentifier,
  adTargets = [],
  entitlements = [],
  isUser = false,
  adViewType = null,
) => {
  const userType = getUserType(entitlements, isUser);
  const adConfigResult = {
    ...adConfigMap[adIdentifier],
    adTargets: [...adTargets, ['usertype', userType]],
    adViewType: adViewType || adConfigMap[adIdentifier].adViewType,
  };
  return adConfigResult;
};

export const subregionForecastAdBaseConfig = (subregionId, userEntitlements, isUser) => ({
  adTargets: [['subregionid', subregionId]],
  userEntitlements,
  isUser,
});

export default loadAdConfig;
