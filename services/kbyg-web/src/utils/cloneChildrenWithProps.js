import React from 'react';

/**
 * @template P
 * @param {React.ReactChildren} children
 * @param {P} props
 */
const cloneChildrenWithProps = (children, props) => {
  const childrenWithProps = React.Children.map(children, (child) => {
    // checking isValidElement is the safe way and avoids a typescript error too
    /* istanbul ignore else */
    if (React.isValidElement(child)) {
      return React.cloneElement(child, props);
    }
    return child;
  });

  return childrenWithProps;
};

export default cloneChildrenWithProps;
