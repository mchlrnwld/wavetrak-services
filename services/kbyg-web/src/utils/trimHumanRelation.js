const trimHumanRelation = (humanRelation) =>
  humanRelation && humanRelation.indexOf(' – ') !== -1
    ? humanRelation.split(' – ')[1]
    : humanRelation;

export default trimHumanRelation;
