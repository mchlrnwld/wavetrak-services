const occassionalUnits = (occasionalHeight, units) =>
  occasionalHeight ? ` occ. ${occasionalHeight} ${units.toLowerCase()}` : null;
export default occassionalUnits;
