export const findIsLotusFromArguments = (args) =>
  args.find((elem) => elem && typeof elem === 'object' && 'isLotus' in elem)?.isLotus;
