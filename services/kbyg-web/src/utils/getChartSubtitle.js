const getChartSubtitle = (type, region, subregion) => {
  const chartSubtitleMap = {
    nearshore: 'Nearshore Models',
    region,
    subregion,
    global: 'Global',
  };
  return chartSubtitleMap[type] || type;
};

export default getChartSubtitle;
