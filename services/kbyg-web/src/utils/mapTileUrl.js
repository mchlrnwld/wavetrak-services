import config from '../config';

// https://tileserver.readthedocs.io/en/latest/endpoints.html
const mapTileUrl = (width, height, lat, lon, zoom = 13) =>
  `${config.mapTileUrl}/static/${lon},${lat},${zoom}/${width}x${height}.png?key=${config.mapTileKey}`;

export default mapTileUrl;
