const getCamStillImage = (camera) => (camera ? camera.stillUrl : null);

export default getCamStillImage;
