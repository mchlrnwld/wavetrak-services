const getSubscriptionEntitlement = (isPremium, userDetails) => {
  if (isPremium) return 'isPremium';
  if (userDetails) return 'isBasic';
  return 'anonymous';
};

export default getSubscriptionEntitlement;
