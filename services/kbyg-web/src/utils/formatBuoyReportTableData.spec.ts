import { expect } from 'chai';
import { buoyReportDataFixture } from '../common/api/fixtures/buoys';
import { formatBuoyReportTableData } from './formatBuoyReportTableData';
import { BuoyReportResponse } from '../types/buoys';

const resultFixture = {
  units: {
    temperature: 'F',
    tideHeight: 'FT',
    swellHeight: 'FT',
    waveHeight: 'FT',
    surfHeight: 'FT',
    windSpeed: 'KTS',
  },
  days: [
    [
      {
        timestamp: 1627422600,
        utcOffset: -7,
        period: 13,
        height: 4.3,
        direction: 179,
        metrics: {
          wind: {
            direction: 290,
            gust: 8,
            speed: 6,
          },
          waterTemperature: 32,
          airTemperature: 58,
          pressure: 1017.13,
          dewPoint: 56,
        },
        swells: [
          {
            height: 3.2,
            period: 8,
            direction: 320,
            index: 0,
          },
          {
            height: 0,
            period: 13,
            direction: 175,
            index: 1,
          },
          {
            height: 1.5,
            period: null,
            direction: 280,
            index: 2,
          },
          {
            height: 3.2,
            period: 8,
            direction: null,
            index: 3,
          },
        ],
      },
    ],
  ],
  utcOffset: -7,
  showMinutes: true,
};

describe('utils / formatBuoyReportTableData', () => {
  it('should properly format the buoy data', () => {
    const { days, units } = formatBuoyReportTableData(buoyReportDataFixture as BuoyReportResponse);
    expect(units).to.deep.equal(resultFixture.units);
    expect(days).to.deep.include(resultFixture.days[0]);
  });

  it('should short circuit if the latest data is missing', () => {
    const { days, units } = formatBuoyReportTableData({
      ...buoyReportDataFixture,
      data: [],
    } as BuoyReportResponse);
    expect(units).to.deep.equal(resultFixture.units);
    expect(days).to.deep.equal([]);
  });

  it('should add placeholder data points to any missing days', () => {
    const today = +new Date();
    const { days } = formatBuoyReportTableData({
      associated: {
        abbrTimezone: 'PST',
        units: {
          temperature: 'F',
          tideHeight: 'FT',
          swellHeight: 'FT',
          waveHeight: 'FT',
          surfHeight: 'FT',
          windSpeed: 'KTS',
        },
      },
      data: [
        {
          timestamp: today / 1000,
          swells: [],
          utcOffset: -7,
          height: 1,
          period: 2,
          direction: 1,
        },
      ],
    });
    // formatBuoyReportTableData should add 2 more days to list
    expect(days).to.have.length(3);
  });
});
