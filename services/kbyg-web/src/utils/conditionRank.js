const conditions = [
  'FLAT',
  'VERY_POOR',
  'POOR',
  'POOR_TO_FAIR',
  'FAIR',
  'FAIR_TO_GOOD',
  'GOOD',
  'VERY_GOOD',
  'GOOD_TO_EPIC',
  'EPIC',
];

export const conditionToRank = (condition) => conditions.indexOf(condition);

/**
 * @param {number} rank
 */
export const rankToCondition = (rank) => conditions[rank];
