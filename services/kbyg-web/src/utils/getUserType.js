const getUserType = (entitlements, isUser) => {
  if (entitlements.includes('sl_vip_advertiser')) return 'VIP_ADVERTISER';
  if (entitlements.includes('sl_vip')) return 'VIP';
  if (entitlements.includes('sl_premium')) return 'PREMIUM';
  if (isUser) return 'REGISTERED';
  return 'ANONYMOUS';
};

export default getUserType;
