import { expect } from 'chai';
import computeShowMeterRegWall from './computeShowMeterRegWall';

describe('utils / computeShowMeterRegWall', () => {
  it('computes showMeterRegWall for url paths', () => {
    expect(
      computeShowMeterRegWall('/surf-report/hb-pier-northside/5842041f4e65fad6a7708827'),
    ).to.equal(true);

    expect(
      computeShowMeterRegWall(
        '/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis',
      ),
    ).to.equal(true);

    expect(
      computeShowMeterRegWall('/surf-reports-forecasts-cams/united-states/california/5332921'),
    ).to.equal(true);

    expect(
      computeShowMeterRegWall('/surf-charts/wave-height/panhandle/58581a836630e24c44878ff2'),
    ).to.equal(true);

    expect(
      computeShowMeterRegWall(
        '/buoy-report/san-pedro-ca-092--buoy-46222/b613dbf0-ca13-11eb-ba78-0242015eddd9',
      ),
    ).to.equal(true);

    expect(computeShowMeterRegWall('/not-a-rate-limited-route')).to.equal(false);
  });
});
