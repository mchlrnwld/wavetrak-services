import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import closestBinarySearch from './closestBinarySearch';

describe('utils / closestBinarySearch', () => {
  const sortedHaystack = deepFreeze([{ rank: 1 }, { rank: 2 }, { rank: 3 }, { rank: 5 }]);
  const propertyGetter = ({ rank }) => rank;

  it('returns index of needle in haystack if found', () => {
    expect(closestBinarySearch(sortedHaystack, { rank: 1 }, propertyGetter)).to.equal(0);
    expect(closestBinarySearch(sortedHaystack, { rank: 2 }, propertyGetter)).to.equal(1);
    expect(closestBinarySearch(sortedHaystack, { rank: 3 }, propertyGetter)).to.equal(2);
    expect(closestBinarySearch(sortedHaystack, { rank: 5 }, propertyGetter)).to.equal(3);
  });

  it('returns 0 if insertion position of needle is before the first item of haystack', () => {
    expect(closestBinarySearch(sortedHaystack, { rank: 0 }, propertyGetter)).to.equal(0);
  });

  it('returns last index if insertion position of needle is after the last item of haystack', () => {
    expect(closestBinarySearch(sortedHaystack, { rank: 6 }, propertyGetter)).to.equal(3);
  });

  it('returns index to item in haystack nearest to needle', () => {
    expect(closestBinarySearch(sortedHaystack, { rank: 4 }, propertyGetter)).to.equal(2);
    expect(closestBinarySearch(sortedHaystack, { rank: 1.1 }, propertyGetter)).to.equal(0);
    expect(closestBinarySearch(sortedHaystack, { rank: 1.7 }, propertyGetter)).to.equal(1);
  });
});
