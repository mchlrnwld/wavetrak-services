import _groupBy from 'lodash/groupBy';

/**
 * @param {string} name - The name of the basin chart (Ex: North Pacific Dominant Wave Period)
 * @description This util function is used to parse the name of a basin from a basin chart
 * is not given to us split from the name of the chart. The basin chart names come in a form similar
 * to: "North Pacific Dominant Wave Period".
 * This helper function just helps to parse the name of the basin for use in the menu and header.
 */
export const getBasinChartName = (name) => name.split(/\s(swell|wind|wave|dominant)/i)[0];

/**
 * @description All of the basin charts for a subregion are loaded into the same object. We want
 * them to be inside seperate dropdown menu's, so we need to filter these chart types into their
 * own top level objects like so:
 * Before: {
 *   ...
 *   global: []
 *   basins: []
 * }
 * After: {
 *   ...
 *   global: []
 *   "North Pacific": [],
 *   "South Pacific": []
 * }
 * @param {Object} types - Object containing chart types
 * @param {Array<Object>} [types.basins] - The array of basin charts
 */
export const formatChartTypes = (types) => {
  const { basins, global, nearshore, region, subregion } = types;

  const globalWithLegends = global.map((chart) => ({
    ...chart,
    hasLegend: true,
  }));

  if (!basins) {
    return {
      subregion,
      nearshore,
      region,
      global: globalWithLegends,
    };
  }
  const namedBasins = basins.map((basin) => {
    const basinName = getBasinChartName(basin.name);
    return {
      ...basin,
      name: basin.name.replace(`${basinName} `, ''),
      basinName,
      hasLegend: true,
    };
  });

  const formattedBasins = _groupBy(namedBasins, 'basinName');

  return {
    subregion,
    nearshore,
    region,
    ...formattedBasins,
    global: globalWithLegends,
  };
};
