import round from 'lodash/round';

// eslint-disable-next-line import/prefer-default-export
export const roundSurfHeight = (height, units) => {
  switch (units) {
    case 'FT':
      return round(height, 0);
    case 'M':
      return round(height, 1);
    default:
      return height;
  }
};
