/**
 * @param {string} condition
 */
const conditionMapTo7Ratings = (condition) => {
  switch (condition) {
    case 'FLAT':
      return 'VERY_POOR';
    case 'VERY_GOOD':
      return 'GOOD';
    case 'GOOD_TO_EPIC':
      return 'GOOD';
    default:
      return condition;
  }
};

export default conditionMapTo7Ratings;
