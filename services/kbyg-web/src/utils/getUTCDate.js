const getUTCDate = (date = new Date()) =>
  new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

export default getUTCDate;
