import { Cookies } from 'react-cookie';

export const readStickyCamCookie = () => {
  const cookies = new Cookies();
  const stickyCamCookie = cookies.get('sl_spot_cam_visible');
  return typeof stickyCamCookie !== 'undefined' ? stickyCamCookie === 'true' : true;
};

export const setStickyCamCookie = (isOpen) => {
  const cookies = new Cookies();
  cookies.set('sl_spot_cam_visible', isOpen, { path: '/' });
};
