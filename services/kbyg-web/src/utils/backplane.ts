import type { NextRouter } from 'next/router';
import {
  canUseDOM,
  getWindow,
  createSplitFilter,
  setWavetrakSplitFilters,
  getWavetrakSplitFilters,
  setWavetrakFeatureFrameworkCB,
  BackplaneState,
} from '@surfline/web-common';
import { once } from 'lodash';

import { WavetrakStore } from '../stores';
import * as treatments from '../common/treatments';
import setupLogRocket from './setupLogRocket';

export const instantiateClientSideBackplane = (
  store: WavetrakStore,
  router: NextRouter,
  setSplitReady: () => void,
  setAnalyticsReady: () => void,
) => {
  if (canUseDOM) {
    const win = getWindow();
    win.wavetrakStore = store;

    // Get the list on enabled splits and create the list of treatment names to load
    const splitFilters = createSplitFilter(treatments);

    // Set the treatment names on the window so that backplane can access them
    setWavetrakSplitFilters([...splitFilters, ...getWavetrakSplitFilters()]);

    setWavetrakFeatureFrameworkCB(
      once(() => {
        setupLogRocket(store, router.query);
        setSplitReady();
        setAnalyticsReady();
      }),
    );

    win.wavetrakBackplaneMount?.();
  }
};

export const getBackplaneRenderOpts = (pathname: string, query: { native?: string }) => {
  const isMobile = query.native === 'true';
  let defaultRenderOpts: { [key: string]: string | boolean } = {
    app: 'kbyg',
    'renderOptions[showAdBlock]': 'true',
    'renderOptions[bannerAd]': false,
  };

  if (pathname.indexOf('/sessions/clips') > -1) {
    defaultRenderOpts = { ...defaultRenderOpts, 'renderOptions[bannerAd]': 'sessions_clips' };
  }

  if (isMobile) defaultRenderOpts = { ...defaultRenderOpts, 'renderOptions[hideNav]': true };

  return defaultRenderOpts;
};

export interface Backplane {
  associated: {
    vendor: {
      js: string;
    };
  };
  data: {
    js: string;
    css: string;
    components: {
      header: string;
      regwall: string;
      footer: string;
    };
    redux: BackplaneState;
    api: {
      geo: {
        ip: string;
        countryCode: string;
        location: {
          latitude: number;
          longitude: number;
        };
      };
      renderOptions: Record<string, string | boolean>;
    };
  };
}
