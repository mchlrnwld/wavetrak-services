/* eslint-disable import/no-extraneous-dependencies */
import { RouterContext } from 'next/dist/shared/lib/router-context';
import { NextRouter } from 'next/router';
import sinon from 'sinon';
import React, { JSXElementConstructor } from 'react';
import { mount, shallow } from 'enzyme';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { ErrorBoundary } from '@surfline/quiver-react';

import defaultState from '../selectors/fixtures/state';
import { AppState } from '../stores';

export const createMockRouter = (router: Partial<NextRouter> = {}): NextRouter => ({
  basePath: '',
  pathname: '/',
  route: '/',
  query: {},
  asPath: '/',
  back: sinon.stub(),
  beforePopState: sinon.stub(),
  prefetch: sinon.stub(),
  push: sinon.stub(),
  reload: sinon.stub(),
  replace: sinon.stub(),
  events: {
    on: sinon.stub(),
    off: sinon.stub(),
    emit: sinon.stub(),
  },
  isFallback: false,
  isLocaleDomain: false,
  isReady: true,
  defaultLocale: 'en',
  domainLocales: [],
  isPreview: false,
  ...router,
});

interface Options {
  initialState?: AppState;
  store?: MockStoreEnhanced;
  router?: Partial<NextRouter>;
}

const Wrapper: React.FC<{ store: MockStoreEnhanced; router: NextRouter }> = ({
  children,
  store,
  router,
}) => (
  <RouterContext.Provider value={router}>
    <Provider store={store}>
      <ErrorBoundary>{children}</ErrorBoundary>
    </Provider>
  </RouterContext.Provider>
);

export const mountWithReduxAndRouter = <
  T extends keyof JSX.IntrinsicElements | JSXElementConstructor<any>,
>(
  Component: T,
  props: React.ComponentProps<T> = {} as any,
  {
    initialState = defaultState,
    store = configureStore<AppState>([thunk])(initialState),
    router = {},
  }: Options = {},
) => {
  const mockRouter = createMockRouter(router);
  const wrapper = mount(<Component {...props} />, {
    wrappingComponent: Wrapper,
    wrappingComponentProps: {
      store,
      router: mockRouter,
    },
  });
  return {
    wrapper,
    store,
    router: mockRouter,
  };
};

export const shallowWithReduxAndRouter = <
  T extends keyof JSX.IntrinsicElements | JSXElementConstructor<any>,
>(
  Component: T,
  props: React.ComponentProps<T> = {} as any,
  {
    initialState = defaultState,
    store = configureStore<AppState>([thunk])(initialState),
    router = {},
  }: Options = {},
) => {
  const mockRouter = createMockRouter(router);
  const wrapper = shallow(<Component {...props} />, {
    wrappingComponent: Wrapper,
    wrappingComponentProps: {
      store,
      router: mockRouter,
    },
  });
  return {
    wrapper,
    store,
    router: mockRouter,
  };
};

export const wait = (ms: number) =>
  new Promise((res) => {
    setTimeout(res, ms);
  });
