const addClientIpHeader = (clientIp) =>
  clientIp
    ? {
        'x-client-ip': clientIp,
      }
    : null;

export default addClientIpHeader;
