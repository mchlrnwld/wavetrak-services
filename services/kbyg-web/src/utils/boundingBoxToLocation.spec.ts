import { expect } from 'chai';
import { Map } from 'leaflet';
import boundingBoxToLocation from './boundingBoxToLocation';

describe('utils / boundingBoxToLocation', () => {
  const map = { getContainer: () => ({ clientWidth: 500, clientHeight: 500 }) };
  const boundingBox = {
    north: 36.96632852130351,
    south: 36.94785295955728,
    east: -121.9628,
    west: -122.1264825495626,
  };

  it('should return null if the bounding box is missing', () => {
    expect(boundingBoxToLocation({} as Map)).to.be.null();
  });

  it('should return null if the map is missing', () => {
    expect(boundingBoxToLocation(undefined, boundingBox)).to.be.null();
  });

  it('should return a location', () => {
    expect(boundingBoxToLocation(map as Map, boundingBox)).to.be.deep.equal({
      center: { lat: 36.957091300729985, lon: -122.04464127478214 },
      zoom: 11,
      boundingBox,
    });
  });
});
