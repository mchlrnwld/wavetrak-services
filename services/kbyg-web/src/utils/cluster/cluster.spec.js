import { expect } from 'chai';
import Supercluster from 'supercluster';
import cluster from './cluster';
import points from './testpoints.json';

const sum = (array) => array.reduce((a, b) => a + b, 0);

describe('utils / cluster', () => {
  it("clusters map points using the same algorithm as MapBox's supercluster", () => {
    const radius = 100;
    const zoom = 11;

    const index = new Supercluster({ radius, minZoom: zoom, maxZoom: zoom, extent: 256 });
    index.load(points);
    const mapBoxClusters = index.getClusters([-180, -90, 180, 90], zoom);

    const properties = (p) => ({
      cluster: true,
      point_count: p.length,
      point_count_abbreviated: p.length,
    });

    const options = { radius, properties };
    const clusters = cluster(points, zoom, options);

    expect(clusters.length).to.equal(mapBoxClusters.length);

    for (let i = 0; i < cluster.length; i += 1) {
      expect(clusters[i].properties.point_count).to.equal(mapBoxClusters[i].properties.point_count);
      expect(clusters[i].geometry).to.deep.equal(mapBoxClusters[i].geometry);
    }
  });

  it('appends properties to cluster', () => {
    const properties = (p) => ({
      points: p,
      count: p.length,
      sumOfRandomNumber: sum(p.map((p1) => p1.properties.randomNumber)),
    });
    const clusters = cluster(points, 11, { properties });

    clusters.forEach((c) => {
      expect(c.properties.count).to.equal(c.properties.points.length);
      expect(c.properties.sumOfRandomNumber).to.be.a('number');
      expect(c.properties.sumOfRandomNumber).to.equal(
        sum(c.properties.points.map((p) => p.properties.randomNumber)),
      );
    });
  });
});
