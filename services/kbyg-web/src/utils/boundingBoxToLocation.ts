import { WebMercatorViewport } from '@math.gl/web-mercator';
import { Map } from 'leaflet';
import { round } from 'lodash';

import { BoundingBox, Location } from '../types/map';

const boundingBoxToLocation = (map?: Map, boundingBox?: BoundingBox): Location | null => {
  if (!map || !boundingBox) {
    return null;
  }

  const mapContainer = map.getContainer();
  const height = mapContainer.clientHeight;
  const width = mapContainer.clientWidth;

  const { north: lat0, west: lon0, south: lat1, east: lon1 } = boundingBox;

  const viewport = new WebMercatorViewport({ width, height }).fitBounds([
    [lon0, lat0],
    [lon1, lat1],
  ]);

  return {
    center: { lat: viewport.latitude, lon: viewport.longitude },
    // Sometime we get weird float values here even though the map doesn't support floats for zoom.
    // This causes an extra re-render. We can just pre-empt that and round the value to the nearest
    // whole number
    zoom: round(viewport.zoom),
    boundingBox,
  };
};

export default boundingBoxToLocation;
