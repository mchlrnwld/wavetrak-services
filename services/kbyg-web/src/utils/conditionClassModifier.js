import conditionMapTo7Ratings from './conditionMapTo7Ratings';

/**
 * @param {string} condition
 */
const conditionClassModifier = (condition, sevenRatings = false) => {
  if (!condition) {
    return 'lotus';
  }

  return sevenRatings
    ? conditionMapTo7Ratings(condition).toLowerCase().replace(/_/g, '-')
    : condition.toLowerCase().replace(/_/g, '-');
};

export default conditionClassModifier;
