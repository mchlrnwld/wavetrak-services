import bs from 'binary-search';

const closestBinarySearch = (sortedHaystack, needle, propertyGetter) => {
  const foundIndex = bs(sortedHaystack, needle, (a, b) => propertyGetter(a) - propertyGetter(b));

  if (foundIndex > -1) return foundIndex;
  if (foundIndex === -1) return 0;
  if (-foundIndex > sortedHaystack.length) return sortedHaystack.length - 1;

  const beforeIndex = -foundIndex - 2;
  const afterIndex = beforeIndex + 1;

  const before = propertyGetter(sortedHaystack[beforeIndex]);
  const after = propertyGetter(sortedHaystack[afterIndex]);

  const needleProperty = propertyGetter(needle);
  return needleProperty - before <= after - needleProperty ? beforeIndex : afterIndex;
};

export default closestBinarySearch;
