import _round from 'lodash/round';

const binMap = {
  KTS: [10, 20, 30],
  MPH: [11.51, 23.02, 34.52],
  KPH: [18.52, 37.04, 55.56],
};

const hourlyBinMap = {
  KTS: [10, 20, 25, 30],
  MPH: [11.51, 23.02, 28, 34.52],
  KPH: [18.52, 37.04, 45, 55.56],
};

const getMaxWindSpeed = (maxHeight, units, isHourly = false) => {
  const bins = isHourly ? hourlyBinMap[units] || [] : binMap[units] || [];
  const digits = 0;
  const maxWind = bins.find((bin) => maxHeight <= bin) || _round(maxHeight, digits);
  if (isHourly && maxWind <= bins[0]) {
    return bins[1] - bins[0] / 2;
  }
  return maxWind;
};

export default getMaxWindSpeed;
