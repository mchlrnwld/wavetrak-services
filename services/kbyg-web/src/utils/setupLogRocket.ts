import LogRocket from 'logrocket';
import setupLogRocketReact from 'logrocket-react';
import { getUserDetails, getWavetrakIdentity } from '@surfline/web-common';

import { NextRouter } from 'next/router';
import config from '../config';
import { WavetrakStore } from '../stores';

const setupLogRocket = (store: WavetrakStore, query: NextRouter['query']) => {
  const { anonymousId } = getWavetrakIdentity() || {};

  const logRocketEnabled = query?.logrocket === 'on';
  const shouldEnableForAnonymousId = ['undefined', 'Undefined', 'null'].includes(anonymousId || '');

  if (logRocketEnabled || shouldEnableForAnonymousId) {
    LogRocket.init(config.appKeys.logrocket, {
      network: {
        requestSanitizer: (req) => {
          req.headers['X-Auth-AccessToken'] = 'redacted';
          req.url = req.url.replace(/access(_)?token=([^&]*)/, 'accesstoken=**redacted**');

          if (req.body) {
            req.body = req.body.replace(/password=([^&]*)/, 'password=**redacted**');
            req.body = req.body.replace(/access(_)?token=([^&]*)/, 'accesstoken=**redacted**');
          }

          return req;
        },
      },
    });

    setupLogRocketReact(LogRocket);

    const storeState = store.getState();
    const userData = getUserDetails(storeState);

    if (userData) {
      LogRocket.identify(userData?._id || anonymousId || 'Unidentified', {
        name: `${userData.firstName} ${userData.lastName}`,
        email: userData.email || '',
      });
    }
  }
};

export default setupLogRocket;
