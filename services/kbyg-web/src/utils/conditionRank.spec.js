import { expect } from 'chai';
import { conditionToRank, rankToCondition } from './conditionRank';

describe('utils / conditionRank', () => {
  describe('conditionToRank', () => {
    it('converts conditions to rank', () => {
      expect(conditionToRank('FLAT')).to.equal(0);
      expect(conditionToRank('VERY_POOR')).to.equal(1);
      expect(conditionToRank('POOR')).to.equal(2);
      expect(conditionToRank('POOR_TO_FAIR')).to.equal(3);
      expect(conditionToRank('FAIR')).to.equal(4);
      expect(conditionToRank('FAIR_TO_GOOD')).to.equal(5);
      expect(conditionToRank('GOOD')).to.equal(6);
      expect(conditionToRank('VERY_GOOD')).to.equal(7);
      expect(conditionToRank('GOOD_TO_EPIC')).to.equal(8);
      expect(conditionToRank('EPIC')).to.equal(9);
      expect(conditionToRank(null)).to.equal(-1);
    });
  });

  describe('rankToCondition', () => {
    it('converts rank to condition', () => {
      expect(rankToCondition(0)).to.equal('FLAT');
      expect(rankToCondition(1)).to.equal('VERY_POOR');
      expect(rankToCondition(2)).to.equal('POOR');
      expect(rankToCondition(3)).to.equal('POOR_TO_FAIR');
      expect(rankToCondition(4)).to.equal('FAIR');
      expect(rankToCondition(5)).to.equal('FAIR_TO_GOOD');
      expect(rankToCondition(6)).to.equal('GOOD');
      expect(rankToCondition(7)).to.equal('VERY_GOOD');
      expect(rankToCondition(8)).to.equal('GOOD_TO_EPIC');
      expect(rankToCondition(9)).to.equal('EPIC');
      expect(rankToCondition(10)).to.equal(undefined);
    });
  });
});
