import * as segment from '@segment/snippet';
import config from '../config';

const renderSnippet = () => {
  const opts = {
    apiKey: config.appKeys.segment,
    page: false,
    load: false,
  };

  if (process.env.NODE_ENV === 'development') {
    return segment.max(opts);
  }

  return segment.min(opts);
};

export default renderSnippet;
