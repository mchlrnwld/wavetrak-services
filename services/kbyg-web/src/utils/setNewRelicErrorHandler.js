/* istanbul ignore file */
import { canUseDOM, getWindow } from '@surfline/web-common';
import UAParser from 'ua-parser-js';

/**
 * @param {Error} error
 */
const shouldIgnoreError = (error) => {
  const win = getWindow();
  const { message, stack } = error;

  const userAgent = win?.navigator?.userAgent;
  const parser = new UAParser();
  const browser = parser.setUA(userAgent).getBrowser().name;

  // Only ignore script errors for Safari
  // These errors are related to third party script cross-origin errors
  // so we can safely ignore them.
  if (browser === 'Safari' && message?.includes('Script error')) {
    return true;
  }

  // If it's some unknown error originating from google SDK, then ignore it
  if (stack?.includes('imasdk.googleapis.com')) {
    return true;
  }

  return false;
};

const setNewRelicErrorHandler = () => {
  if (canUseDOM) {
    const win = getWindow();

    if (win?.newrelic) {
      win.newrelic.setErrorHandler(shouldIgnoreError);
    }
  }
};

export default setNewRelicErrorHandler;
