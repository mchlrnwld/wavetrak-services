import React from 'react';
import ReactDOM from 'react-dom';
import EmbedContainer from 'react-oembed-container';
import { fetchOembed, getWindow, trackClickedSubscribeCTA } from '@surfline/web-common';
import { CamPlayer, CamPlayerV2, PremiumCamCTA } from '@surfline/quiver-react';
import { fetchReport } from '../common/api/spot';
import { fetchCamEmbedDetails } from '../common/api/cameras';
import config from '../config';

const win = getWindow();

export const renderIgElements = () => {
  const igElements = win?.document.querySelectorAll('[data-sl-ig]');
  if (igElements.length) {
    [...igElements].map(async (ig) => {
      const {
        oembedObj: { html },
      } = await fetchOembed(ig.dataset.slIg, '/travel/oembed');
      ReactDOM.render(
        <EmbedContainer markup={html}>
          {/* eslint-disable-next-line react/no-danger */}
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </EmbedContainer>,
        ig,
      );
    });
  }
};

export const renderJWElements = () => {
  const jwElements = win?.document.querySelectorAll("[id*='jw']");
  if (jwElements.length) {
    [...jwElements].map(async (jw) => {
      // eslint-disable-next-line
      const jwId = jw.id.split('_')[1];
      if (win?.jwplayer && jwId) {
        win
          .jwplayer(jw.id)
          .setup({ playlist: `https://content.jwplatform.com/feeds/${jwId}.json`, ph: 2 });
      }
    });
  }
};

export const renderTwitterElements = () => {
  const twitterElements = win?.document.getElementsByClassName('twitter-tweet');
  if (twitterElements.length) {
    [...twitterElements].map(async (tweetElement) => {
      const tweetUrl = tweetElement.getElementsByTagName('a')[1].href;
      const {
        oembedObj: { html },
      } = await fetchOembed(tweetUrl, '/travel/oembed');
      ReactDOM.render(
        <EmbedContainer markup={html}>
          {/* eslint-disable-next-line react/no-danger */}
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </EmbedContainer>,
        tweetElement,
      );
    });
  }
};

const createCameraObject = (cameraDetails) => ({
  streamUrl: cameraDetails.cam.streamUrl,
  stillUrl: cameraDetails.cam.stillUrl,
  status: {
    isDown: !!cameraDetails.cam.isDown.status,
    message: cameraDetails.cam.isDown.message,
    subMessage: cameraDetails.cam.isDown.subMessage,
  },
  isPrerecorded: false,
  isPremium: cameraDetails.cam.isPremium,
  title: 'title',
});

/**
 * @param {boolean} isPremium
 * @param {boolean} [isMetered]
 */
export const renderCameraEmbeds = (isPremium, isMetered) => {
  const cameraEmbeds = win?.document.getElementsByClassName('wp-cam-embed');
  if (cameraEmbeds.length) {
    [...cameraEmbeds].map(async (embedElement) => {
      const cameraId = embedElement.getAttribute('data-sl-cam-id');
      const detailsJson = await fetchCamEmbedDetails(cameraId);
      const spotReport = await fetchReport(detailsJson.spot.id);
      const camera = createCameraObject(detailsJson);

      const camStillUrl = camera.stillUrl;
      const isPremiumCam = camera.isPremium;
      const showPremiumCamPaywall = isPremiumCam && !isPremium;
      const segmentProperties = {
        location: 'premium only cam - cam embed',
        title: 'Premium Analysis',
        category: 'forecast',
        spotId: detailsJson.spot.id,
        spotName: detailsJson.spot.name,
      };
      if (showPremiumCamPaywall) {
        ReactDOM.render(
          <div className="video-wrap-container">
            <div className="video-wrap">
              <PremiumCamCTA
                camStillUrl={camStillUrl}
                href={`${config.surflineHost}${config.funnelUrl}`}
                onClickPaywall={() => trackClickedSubscribeCTA(segmentProperties)}
              />
            </div>
          </div>,
          embedElement,
        );
      } else {
        ReactDOM.render(
          <div className="video-wrap-container">
            <div className="video-wrap">
              {isMetered ? (
                <CamPlayerV2
                  aspectRatio="16:9"
                  playerId={cameraId || 'sl-editorial-embed'}
                  camera={camera}
                  spotName={detailsJson.spot.name}
                  spotId={detailsJson.spot.id}
                  isPremium={isPremium}
                  isMultiCam={false}
                  location="Editorial Embed"
                />
              ) : (
                <CamPlayer
                  aspectRatio="16:9"
                  adTag={{ iu: '/1024858/SL_Editorial_Embed_PreRoll' }}
                  advertisingIds={spotReport.associated.advertising}
                  playerId={cameraId || 'sl-editorial-embed'}
                  camera={camera}
                  spotName={detailsJson.spot.name}
                  spotId={detailsJson.spot.id}
                  isPremium={isPremium}
                  isMultiCam={false}
                  location="Editorial Embed"
                />
              )}
            </div>
          </div>,
          embedElement,
        );
      }
    });
  }
};
