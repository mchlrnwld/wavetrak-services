/** @typedef {import('../../propTypes/map/location').Location} Location */

export const mapPaths = {
  base: `/surf-reports-forecasts-cams-map`,
  buoys: `/surf-reports-forecasts-cams-map/buoys`,
  geonameBase: `/surf-reports-forecasts-cams`,
  geonameBuoysBase: `/surf-reports-forecasts-cams/buoys`,
};

/** @param {Location} location */
export const locationObjectToString = (location) => {
  if (!location) {
    return null;
  }

  const {
    center: { lat, lon },
    zoom,
  } = location;

  return `${lat},${lon},${zoom}z`;
};

export const getSpotMapPath = () => mapPaths.base;
export const getBuoyMapPath = () => mapPaths.buoys;

/** @param {string} location */
export const getSpotMapPathWithLocation = (location) => {
  if (location.indexOf('@') !== -1) {
    return `/surf-reports-forecasts-cams-map/${location}`;
  }
  return `/surf-reports-forecasts-cams-map/@${location}`;
};

/** @param {string} location */
export const getBuoyMapPathWithLocation = (location) => {
  if (location.indexOf('@') !== -1) {
    return `/surf-reports-forecasts-cams-map/buoys/${location}`;
  }
  return `/surf-reports-forecasts-cams-map/buoys/@${location}`;
};

/** @param {Location} location */
export const getSpotMapPathWithLocationObject = (location) =>
  `/surf-reports-forecasts-cams-map/@${locationObjectToString(location)}`;

/** @param {Location} location */
export const getBuoyMapPathWithLocationObject = (location) =>
  `/surf-reports-forecasts-cams-map/buoys/@${locationObjectToString(location)}`;

/**
 * @param {object} params
 * @param {string} params.breadcrumbs
 * @param {string} params.geonameId
 */
export const getBuoyGeonameMapPath = ({ breadcrumbs, geonameId }) =>
  decodeURIComponent(`/surf-reports-forecasts-cams/buoys/${breadcrumbs}/${geonameId}`);

/**
 * @param {object} params
 * @param {string} params.breadcrumbs
 * @param {string} params.geonameId
 */
export const getGeonameMapPath = ({ breadcrumbs, geonameId }) =>
  decodeURIComponent(`/surf-reports-forecasts-cams/${breadcrumbs}/${geonameId}`);

/** @param {string} location */
export const transformLocationParam = (location) => {
  const splitLocation = location.split(',');
  return {
    center: {
      lat: Number(splitLocation[0]),
      lon: Number(splitLocation[1]),
    },
    zoom: Number(splitLocation[2].replace('z', '')),
  };
};
