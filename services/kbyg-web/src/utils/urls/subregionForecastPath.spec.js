import { expect } from 'chai';
import subregionForecastPath from './subregionForecastPath';

describe('utils / subregionForecastPath', () => {
  it('creates a path to the subregion forecast page from subregion ID', () => {
    expect(subregionForecastPath('58581a836630e24c44878fd6', 'north-orange-county')).to.equal(
      '/surf-forecasts/north-orange-county/58581a836630e24c44878fd6',
    );
  });
});
