import { expect } from 'chai';
import resolveBreadcrumbs from './breadcrumbResolver';

describe('utils / breadcrumbResolver', () => {
  it('removes protocol from href', () => {
    const breadcrumbs = [
      {
        name: 'Pacific Grove',
        href: 'https://staging.surfline.com/travel/united-states/california/monterey-county/pacific-grove-surfing-and-beaches/5380437',
      },
      {
        name: 'Monterey County',
        href: 'http://staging.surfline.com/travel/united-states/california/monterey-county-surfing-and-beaches/5374376',
      },
      {
        name: 'California',
        href: 'https://staging.surfline.com/travel/united-states/california-surfing-and-beaches/5332921',
      },
      {
        name: 'United States',
        href: 'http://staging.surfline.com/travel/united-states-surfing-and-beaches/6252001',
      },
    ];
    const newBreadcrumbs = resolveBreadcrumbs(breadcrumbs);
    const expectedBreadcrumbs = [
      {
        name: 'Pacific Grove',
        href: '/travel/united-states/california/monterey-county/pacific-grove-surfing-and-beaches/5380437',
      },
      {
        name: 'Monterey County',
        href: '/travel/united-states/california/monterey-county-surfing-and-beaches/5374376',
      },
      {
        name: 'California',
        href: '/travel/united-states/california-surfing-and-beaches/5332921',
      },
      {
        name: 'United States',
        href: '/travel/united-states-surfing-and-beaches/6252001',
      },
    ];
    expect(newBreadcrumbs).to.deep.equal(expectedBreadcrumbs);
  });
});
