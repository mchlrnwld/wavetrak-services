import { expect } from 'chai';
import { mapPath } from './camsAndForecastsPath';

describe('utils / camsAndForecastsPath', () => {
  it('creates a path to the map view from center and zoom', () => {
    const center = {
      lat: 33.654213041213,
      lon: -118.0032588192,
    };
    const zoom = 12;
    const path = `/surf-reports-forecasts-cams-map/@${center.lat},${center.lon},${zoom}z`;
    expect(mapPath(center, zoom)).to.equal(path);
  });
});
