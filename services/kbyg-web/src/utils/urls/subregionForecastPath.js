import { slugify } from '@surfline/web-common';

export const subregionForecastPaths = {
  base: '/surf-forecasts',
  premiumAnalysis: 'premium-analysis',
  realTime: 'real-time-forecast',
  shortTerm: 'short-term-forecast',
  longTerm: 'long-term-forecast',
  seasonal: 'seasonal-forecast',
};

const subregionForecastPath = (subregionId, subregionName) =>
  `${subregionForecastPaths.base}/${slugify(subregionName)}/${subregionId}`;

export const premiumAnalysisPath = (subregionId, subregionName, subroute) => {
  let path = `${subregionForecastPath(subregionId, subregionName)}/${
    subregionForecastPaths.premiumAnalysis
  }`;
  if (subroute) path += `/${subregionForecastPaths[subroute]}`;
  return path;
};

export const premiumAnalysisLinkMap = {
  all: {
    text: 'All',
    path: (id, name) => premiumAnalysisPath(id, name),
  },
  realtimeforecast: {
    text: 'Real Time Forecast',
    path: (id, name) => premiumAnalysisPath(id, name, 'realTime'),
  },
  shorttermforecast: {
    text: 'Short Term Forecast',
    path: (id, name) => premiumAnalysisPath(id, name, 'shortTerm'),
  },
  longtermforecast: {
    text: 'Long Term Forecast',
    path: (id, name) => premiumAnalysisPath(id, name, 'longTerm'),
  },
  seasonalforecast: {
    text: 'Seasonal Forecast',
    path: (id, name) => premiumAnalysisPath(id, name, 'seasonal'),
  },
};

export default subregionForecastPath;
