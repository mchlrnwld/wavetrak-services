import * as camsAndForecastsPath from './camsAndForecastsPath';

export const { mapPath } = camsAndForecastsPath;
export const { geonameMapPath } = camsAndForecastsPath;
export { getBuoyReportPath } from './buoyReportPath';
export { default as spotForecastPath, spotReportPath } from './spotForecastPath';
export { default as subregionForecastPath, premiumAnalysisLinkMap } from './subregionForecastPath';
