import { slugify } from '@surfline/web-common';

export const spotForecastPaths = {
  base: '/surf-report',
  report: '/surf-report/:spotSlug/:spotId',
  forecast: '/surf-report/:spotSlug/:spotId/forecast',
  region: 'region',
};

export const spotReportPath = ({ _id, name }) =>
  `${spotForecastPaths.base}/${slugify(name)}/${_id}`;

const spotForecastPath = ({ _id, name }) =>
  `${spotForecastPaths.base}/${slugify(name)}/${_id}/forecast`;

export default spotForecastPath;
