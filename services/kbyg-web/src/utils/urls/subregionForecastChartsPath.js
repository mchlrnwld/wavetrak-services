import { slugify } from '@surfline/web-common';

export const subregionForecastChartsPaths = {
  base: '/surf-charts',
};

const subregionForecastChartsPath = (subregionId, chartsSlug, locationName) =>
  `${subregionForecastChartsPaths.base}/${chartsSlug}/${slugify(locationName)}/${subregionId}`;

export default subregionForecastChartsPath;
