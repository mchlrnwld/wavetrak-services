const conditionEnumToText = (condition, newSubtr) =>
  condition && condition.replace(/_/g, newSubtr || ' ');

export default conditionEnumToText;
