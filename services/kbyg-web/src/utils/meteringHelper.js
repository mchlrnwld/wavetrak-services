/*
This function adds the metering headers to the request header object
*/
export const addMeteringHeaders = ({ anonymousId, treatmentState }) => {
  if (!anonymousId || !treatmentState) return null;
  // If treatment is OFF then no need to send the AnonymousId header
  if (treatmentState === 'off') {
    return null;
  }
  return {
    'X-Auth-AnonymousId': anonymousId,
  };
};

/*
This function accepts a meter object and filters the meterRemaining property from it.
Also it computes the meterHeaders that should be used in the api call.
*/
export const getMeterParamsForRequest = (meter) => {
  const meterHeaders = meter ? addMeteringHeaders(meter) : null;
  const meterRemaining = meter?.meterRemaining;
  return { meterHeaders, meterRemaining };
};

export default addMeteringHeaders;
