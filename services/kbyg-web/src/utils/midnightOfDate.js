const secondsPerHour = 60 * 60;

export const secondsPerDay = secondsPerHour * 24;

/** Creates a timestamp representing the most recent midnight in the desired timezone
 * @param  {Number} utcOffset the UTC offset for the timezone you want to know midnight for
 * @param  {Number} timestamp a timestamp representing now
 *
 * @returns {Number} an timestamp for the time when midnight occurs in the desired timezone
 */
const midnightOfDate = (utcOffset, timestamp = Math.floor(new Date() / 1000)) => {
  const localDate = timestamp + utcOffset * secondsPerHour;
  const localMidnight = localDate - (localDate % secondsPerDay);
  const midnight = localMidnight - utcOffset * secondsPerHour;
  return midnight;
};

export default midnightOfDate;
