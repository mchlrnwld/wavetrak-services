import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { ActionCreator } from 'redux';

const useSyncValueToStore = <A>(action: ActionCreator<A>, value: unknown) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(action(value));
  }, [value, dispatch, action]);
};

export default useSyncValueToStore;
