import React from 'react';
import { act } from 'react-dom/test-utils';
import { mount } from 'enzyme';
import { expect } from 'chai';
import useActiveBuoy from './useActiveBuoy';

describe('hooks / buoys / useActiveBuoy', () => {
  let childBuoy;
  let compBuoy;
  let doSetActiveBuoy;

  afterEach(() => {
    childBuoy = undefined;
    compBuoy = undefined;
    doSetActiveBuoy = undefined;
  });

  const Child = () => {
    const [activeBuoyId] = useActiveBuoy();

    childBuoy = activeBuoyId;
    return <div />;
  };
  const Comp = () => {
    const [activeBuoyId, setBuoy] = useActiveBuoy();

    compBuoy = activeBuoyId;
    doSetActiveBuoy = setBuoy;
    return (
      <div>
        <Child />
      </div>
    );
  };

  it('should manage the global active buoy state', () => {
    const wrapper = mount(<Comp />);

    expect(childBuoy).to.be.undefined();
    expect(compBuoy).to.be.undefined();

    const buoyId = '123';
    act(() => {
      doSetActiveBuoy(buoyId);

      wrapper.update();
    });

    expect(childBuoy).to.be.equal(buoyId);
    expect(compBuoy).to.be.equal(buoyId);
  });
});
