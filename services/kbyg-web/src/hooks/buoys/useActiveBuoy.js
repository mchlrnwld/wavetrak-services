import { createGlobalState } from 'react-use';

const useGlobalActiveBuoyState = createGlobalState();

/**
 * @returns {[string, (state?: string) => void]}
 */
const useActiveBuoy = () => useGlobalActiveBuoyState();

export default useActiveBuoy;
