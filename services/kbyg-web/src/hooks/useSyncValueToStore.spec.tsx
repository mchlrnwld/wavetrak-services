import React from 'react';
import { expect } from 'chai';

import sinon from 'sinon';

import { mountWithReduxAndRouter } from '../utils/test-utils';
import useSyncValueToStore from './useSyncValueToStore';

describe('hooks / useSyncValueToStore', () => {
  const action = { type: 'DUMMY_ACTION' };
  const dummyActionCreator = sinon.stub().returns(action);

  afterEach(() => {
    dummyActionCreator.resetHistory();
  });
  const Comp = ({ value }: { value: boolean }) => {
    useSyncValueToStore(dummyActionCreator, value);

    return <div />;
  };

  it('should fire an action with the value and fire again if it changes', () => {
    const { wrapper, store } = mountWithReduxAndRouter(Comp, { value: true });

    expect(dummyActionCreator).to.have.been.calledOnceWithExactly(true);

    expect(store.getActions()).to.deep.equal([action]);

    dummyActionCreator.resetHistory();
    wrapper.setProps({ value: false });

    expect(dummyActionCreator).to.have.been.calledOnceWithExactly(false);
    expect(store.getActions()).to.deep.equal([action, action]);
  });
});
