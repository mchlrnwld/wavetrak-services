import { useMemo } from 'react';

import useMap from './useMap';
import boundingBoxToLocation from '../../utils/boundingBoxToLocation';
import { useLocationViewBoundingBox } from '../../selectors/mapV2';

/**
 * @description
 * This hook takes the location view bounding box that exists in the redux store
 * and transforms it to a location object and returns it
 */
const useMapLocationView = () => {
  const [map] = useMap();
  const boundingBox = useLocationViewBoundingBox();

  const location = useMemo(() => {
    if (boundingBox && map) {
      return boundingBoxToLocation(map, boundingBox);
    }
    return null;
  }, [boundingBox, map]);

  return location;
};

export default useMapLocationView;
