import { Map } from 'leaflet';
import { createGlobalState } from 'react-use';

const useGlobalMapState = createGlobalState<Map | undefined>();

const useMap = () => useGlobalMapState();

export default useMap;
