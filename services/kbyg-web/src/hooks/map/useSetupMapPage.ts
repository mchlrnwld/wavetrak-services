import { getWindow, trackNavigatedToPage } from '@surfline/web-common';
import { useMount, useUnmount } from 'react-use';
import { getMeteringEnabled } from '../../selectors/meter';
import { useAppSelector } from '../../stores/hooks';

const useSetupMapPage = () => {
  const win = getWindow();
  const meteringEnabled = useAppSelector(getMeteringEnabled);
  useMount(() => {
    trackNavigatedToPage('Map', {
      meteringEnabled,
      category: 'cams & forecasts',
      channel: 'cams & forecasts',
    });

    if (win) win.document.body.classList.add('sl-hide-ze-widget', 'sl-map-page', 'sl-map-v2');
  });

  useUnmount(() => {
    if (win) win.document.body.classList.remove('sl-hide-ze-widget', 'sl-map-page', 'sl-map-v2');
  });
};

export default useSetupMapPage;
