import React from 'react';
import { expect } from 'chai';

import sinon, { SinonStub } from 'sinon';

import * as useMapLocation from './useMapLocation';
import useSyncMapLocationToUrl from './useSyncMapLocationToUrl';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

import { initialState } from '../../reducers/mapV2';

describe('hooks / map / useSyncMapLocationToUrl', () => {
  let useMapLocationStub: SinonStub;

  beforeEach(() => {
    useMapLocationStub = sinon.stub(useMapLocation, 'default').returns({
      location: initialState.location,
      boundingBox: null,
      locationLoaded: initialState.locationLoaded,
      doSetMapLocation: () => {},
    });
  });

  afterEach(() => {
    useMapLocationStub.restore();
  });

  // eslint-disable-next-line react/prop-types
  const Comp = ({ getter }: { getter: SinonStub }) => {
    useSyncMapLocationToUrl(getter);

    return <div />;
  };

  it('should not do anything if the map location does not exist', () => {
    const getter = sinon.stub();
    mountWithReduxAndRouter(Comp, { getter });

    expect(getter).to.not.have.been.called();
  });

  it('should not do anything if the map location is not loaded', () => {
    const getter = sinon.stub();
    const location = { center: { lat: 1, lon: 1 }, zoom: 12 };
    useMapLocationStub.returns({ location, locationLoaded: false });
    mountWithReduxAndRouter(Comp, { getter });

    expect(getter).to.not.have.been.called();
  });

  it('should not do anything if the map location has not changed', () => {
    const getter = sinon.stub();
    const location = { center: { lat: 1, lon: 1 }, zoom: 12 };
    useMapLocationStub.returns({ location, locationLoaded: false });
    mountWithReduxAndRouter(
      Comp,
      { getter },
      {
        router: {
          pathname: '/:location',
          asPath: '/1,1,12z',
          query: { location: '1,1,12z' },
        },
      },
    );

    expect(getter).to.not.have.been.called();
    // expect(replaceSpy).to.not.have.been.called();
  });

  it('should sync the location when the location changes', () => {
    const location = { center: { lat: 1, lon: 1 }, zoom: 12 };
    useMapLocationStub.returns({ location, locationLoaded: true });
    const getter = sinon.stub().returns('/path');

    const { wrapper } = mountWithReduxAndRouter(Comp, { getter });

    expect(getter).to.have.been.calledOnceWithExactly(location);
    // expect(replaceSpy).to.have.been.calledOnceWithExactly('/path');

    const locationTwo = { center: { lat: 2, lon: 2 }, zoom: 10 };
    useMapLocationStub.returns({ location: locationTwo, locationLoaded: true });

    getter.resetHistory();
    // replaceSpy.resetHistory();

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(getter).to.have.been.calledOnceWithExactly(locationTwo);
    // expect(replaceSpy).to.have.been.calledOnceWithExactly('/path');
  });

  it('should sync the location when the getter changes', () => {
    const location = { center: { lat: 1, lon: 1 }, zoom: 12 };

    useMapLocationStub.returns({ location, locationLoaded: true });

    const getter = sinon.stub().returns('/path');

    const { wrapper } = mountWithReduxAndRouter(Comp, { getter });

    expect(getter).to.have.been.calledOnceWithExactly(location);
    // expect(replaceSpy).to.have.been.calledOnceWithExactly('/path');

    const getterTwo = sinon.stub().returns('/path2');
    // replaceSpy.resetHistory();

    wrapper.setProps({ getter: getterTwo });
    wrapper.update();

    expect(getter).to.have.been.calledOnceWithExactly(location);
    // expect(replaceSpy).to.have.been.calledOnceWithExactly('/path2');
  });
});
