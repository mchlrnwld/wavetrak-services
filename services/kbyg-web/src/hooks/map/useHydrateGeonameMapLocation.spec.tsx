import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import sinon, { SinonStub } from 'sinon';

import * as useMapLocation from './useMapLocation';
import * as useMapLocationView from './useMapLocationView';

import useHydrateGeonameMapLocation from './useHydrateGeonameMapLocation';

describe('hooks / map / useHydrateGeonameMapLocation', () => {
  let useMapLocationStub: SinonStub;

  let useMapLocationViewStub: SinonStub;

  const doSetMapLocation = sinon.stub();
  const location = { center: { lat: 2, lon: 2 }, zoom: 1 };

  beforeEach(() => {
    useMapLocationStub = sinon.stub(useMapLocation, 'default').returns({
      doSetMapLocation,
      location: {
        center: { lat: 0, lon: 0 },
        zoom: 12,
      },
      locationLoaded: false,
      boundingBox: null,
    });
    useMapLocationViewStub = sinon.stub(useMapLocationView, 'default');
  });

  afterEach(() => {
    useMapLocationStub.restore();
    useMapLocationViewStub.restore();
  });

  const Comp = () => {
    useHydrateGeonameMapLocation();

    return <div />;
  };

  it('should not call doSetMapLocation if no location exists', () => {
    mount(<Comp />);

    expect(doSetMapLocation).to.not.have.been.called();
  });

  it('should call doSetMapLocation if  location exists', () => {
    useMapLocationViewStub.returns(location);
    mount(<Comp />);

    expect(doSetMapLocation).to.have.been.calledOnceWithExactly(location);
  });
});
