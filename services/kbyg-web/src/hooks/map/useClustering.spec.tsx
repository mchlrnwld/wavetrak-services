import React from 'react';
import { expect } from 'chai';

import sinon, { SinonSpy, SinonStub } from 'sinon';

import useClustering, { Data, properties } from './useClustering';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import * as cluster from '../../utils/cluster/cluster';
import * as mapSelectors from '../../selectors/mapV2';

describe('hooks / map / useClustering', () => {
  let getMapZoomStub: SinonStub;

  let clusterSpy: SinonSpy;

  let clusters: any[] | null;

  beforeEach(() => {
    getMapZoomStub = sinon.stub(mapSelectors, 'getMapZoom');
    clusterSpy = sinon.spy(cluster, 'default');
  });

  afterEach(() => {
    getMapZoomStub.restore();
    clusterSpy.restore();
    clusters = null;
  });

  // eslint-disable-next-line react/prop-types
  const Comp = ({ data }: { data: Data[] | null }) => {
    clusters = useClustering(data);

    return <div />;
  };

  it('should not try to cluster if no data exists', () => {
    getMapZoomStub.returns(1);
    mountWithReduxAndRouter(Comp, { data: null });

    expect(clusterSpy).to.not.have.been.called();
    expect(clusters).to.not.exist();
  });

  it('should not try to cluster if the map is more zoomed in then the maxClusterZoom', () => {
    getMapZoomStub.returns(10);
    mountWithReduxAndRouter(Comp, { data: null });

    expect(clusterSpy).to.not.have.been.called();
    expect(clusters).to.not.exist();
  });

  it('should cluster using the data points with lat/lon', () => {
    const data = [{ lat: 1, lon: 2 }];
    getMapZoomStub.returns(6);
    mountWithReduxAndRouter(Comp, { data });

    expect(clusterSpy).to.have.been.calledOnceWithExactly(
      [{ type: 'Feature', geometry: { type: 'Point', coordinates: [2, 1] } }],
      6,
      { radius: 100, properties },
    );

    expect(clusters).to.deep.equal([
      {
        geometry: { type: 'Point', coordinates: [1.999999999999993, 1.0000000000000142] },
        type: 'Feature',
        properties: { count: 1 },
      },
    ]);
  });

  it('should cluster using the data points with latitude/longitude', () => {
    const data = [{ latitude: 1, longitude: 2 }];
    getMapZoomStub.returns(2);
    mountWithReduxAndRouter(Comp, { data });

    expect(clusterSpy).to.have.been.calledOnceWithExactly(
      [{ type: 'Feature', geometry: { type: 'Point', coordinates: [2, 1] } }],
      2,
      { radius: 150, properties },
    );

    expect(clusters).to.deep.equal([
      {
        geometry: { type: 'Point', coordinates: [1.999999999999993, 1.0000000000000142] },
        type: 'Feature',
        properties: { count: 1 },
      },
    ]);
  });
});
