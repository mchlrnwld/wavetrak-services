import { useGetBuoysInBoundingBoxQuery } from '../../actions/buoys';

import useMapLocation from './useMapLocation';
import useSyncValueToStore from '../useSyncValueToStore';

import { setMapError, setMapLoading, setMapMessage } from '../../actions/mapV2';

const useBuoysBoundingBoxQuery = () => {
  const { boundingBox } = useMapLocation();

  const { data, error, isFetching, isUninitialized } = useGetBuoysInBoundingBoxQuery(boundingBox!, {
    skip: !boundingBox,
  });

  const loading = isFetching || isUninitialized;

  const buoysData = data?.data;
  const hasBuoys = buoysData && buoysData?.length > 0;

  const noBuoysFound = !hasBuoys && !error && !loading;

  const message = noBuoysFound ? 'No buoys found! Try zooming out or moving map' : '';

  useSyncValueToStore(setMapError, error);
  useSyncValueToStore(setMapLoading, loading);
  useSyncValueToStore(setMapMessage, message);

  return { buoys: buoysData, loading, error, noBuoysFound, hasBuoys };
};

export default useBuoysBoundingBoxQuery;
