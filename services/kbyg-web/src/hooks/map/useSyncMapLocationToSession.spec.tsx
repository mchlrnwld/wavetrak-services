import React from 'react';
import { expect } from 'chai';

import sinon, { SinonStub } from 'sinon';

import * as useMapLocation from './useMapLocation';
import * as useMapSessionLocation from './useMapSessionLocation';

import useSyncMapLocationToSession from './useSyncMapLocationToSession';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { initialState } from '../../reducers/mapV2';

describe('hooks / map / useSyncMapLocationToSession', () => {
  let useMapLocationStub: SinonStub;
  let useMapSessionLocationStub: SinonStub;

  const setMapSessionLocationStub = sinon.stub();

  beforeEach(() => {
    useMapLocationStub = sinon.stub(useMapLocation, 'default').returns({
      boundingBox: null,
      location: initialState.location,
      doSetMapLocation: () => {},
      locationLoaded: false,
    });
    useMapSessionLocationStub = sinon
      .stub(useMapSessionLocation, 'default')
      .returns({ mapSessionLocation: null, setMapSessionLocation: setMapSessionLocationStub });
  });

  afterEach(() => {
    useMapLocationStub.restore();
    useMapSessionLocationStub.restore();
    setMapSessionLocationStub.reset();
  });

  const Comp = () => {
    useSyncMapLocationToSession();

    return <div />;
  };

  it('should not do anything if the map location does not exist', () => {
    mountWithReduxAndRouter(Comp, {});

    expect(setMapSessionLocationStub).to.not.have.been.called();
  });

  it('should sync the location when the location changes', () => {
    const location = { center: { lat: 1, lon: 1 }, zoom: 12 };
    useMapLocationStub.returns({
      location,
      locationLoaded: true,
    });

    const { wrapper } = mountWithReduxAndRouter(Comp, {});

    expect(setMapSessionLocationStub).to.have.been.calledOnceWithExactly(location);

    const locationTwo = { center: { lat: 2, lon: 2 }, zoom: 10 };

    useMapLocationStub.returns({
      location: locationTwo,
      locationLoaded: true,
    });

    setMapSessionLocationStub.resetHistory();

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(setMapSessionLocationStub).to.have.been.calledOnceWithExactly(locationTwo);
  });
});
