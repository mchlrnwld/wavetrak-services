import React from 'react';

import sinon, { SinonSpy, SinonStub } from 'sinon';
import { expect } from 'chai';
import * as mapSelectors from '../../selectors/mapV2';
import * as mapActions from '../../actions/mapV2';
import useMapLocation from './useMapLocation';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { BoundingBox, Location } from '../../types/map';

describe('hooks / map / useMapLocation', () => {
  let getMapLocationStub: SinonStub;
  let getMapBoundingBoxStub: SinonStub;
  let getMapLocationLoadedStub: SinonStub;
  let setMapLocationSpy: SinonSpy;
  let setMapLocationLoadedSpy: SinonSpy;

  const location = {
    center: { lat: 0, lon: 0 },
    zoom: 0,
  };

  const boundingBox = {
    north: 1,
    south: -1,
    east: 1,
    west: -1,
  };

  let locationResult: Location | undefined | null;
  let boundingBoxResult: BoundingBox | undefined | null;
  let locationLoadedResult: boolean | undefined | null;
  let doSetMapLocationResult: Function | undefined;

  beforeEach(() => {
    getMapLocationStub = sinon.stub(mapSelectors, 'getMapLocation').returns(location);
    getMapBoundingBoxStub = sinon.stub(mapSelectors, 'getMapBoundingBox').returns(boundingBox);
    getMapLocationLoadedStub = sinon.stub(mapSelectors, 'getMapLocationLoaded').returns(true);
    setMapLocationSpy = sinon.spy(mapActions, 'setMapLocation');
    setMapLocationLoadedSpy = sinon.spy(mapActions, 'setMapLocationLoaded');
  });

  afterEach(() => {
    getMapLocationStub.restore();
    getMapBoundingBoxStub.restore();
    getMapLocationLoadedStub.restore();
    setMapLocationSpy.restore();
    setMapLocationLoadedSpy.restore();

    locationResult = undefined;
    boundingBoxResult = undefined;
    locationLoadedResult = undefined;
    doSetMapLocationResult = undefined;
  });

  const Comp = () => {
    const { location: loc, boundingBox: bbox, locationLoaded, doSetMapLocation } = useMapLocation();

    locationResult = loc;
    boundingBoxResult = bbox;
    locationLoadedResult = locationLoaded;
    doSetMapLocationResult = doSetMapLocation;

    return <div />;
  };

  it('should return the location of the map', () => {
    mountWithReduxAndRouter(Comp);

    expect(locationResult).to.deep.equal(location);
  });

  it('should return the bounding box of the map', () => {
    mountWithReduxAndRouter(Comp);

    expect(boundingBoxResult).to.deep.equal(boundingBox);
  });

  it('should return the location loaded property of the map state', () => {
    mountWithReduxAndRouter(Comp);

    expect(locationLoadedResult).to.be.true();
  });

  it('should return a map location setter', () => {
    const locationTwo = { center: { lat: 2, lon: 2 }, zoom: 1 };
    mountWithReduxAndRouter(Comp);

    expect(doSetMapLocationResult).to.exist();
    doSetMapLocationResult!(locationTwo);

    expect(setMapLocationSpy).to.have.been.calledOnceWithExactly(locationTwo);
  });
});
