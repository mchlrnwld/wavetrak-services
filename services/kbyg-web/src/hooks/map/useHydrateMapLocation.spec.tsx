import React from 'react';
import { expect } from 'chai';

import sinon, { SinonStub } from 'sinon';

import * as useMapSessionLocation from './useMapSessionLocation';
import * as useMapLocation from './useMapLocation';
import * as mapActions from '../../actions/map';
import { mountWithReduxAndRouter, wait } from '../../utils/test-utils';
// import { getBuoyMapPathWithLocationObject, getBuoyMapPath } from '../../utils/urls/mapPaths';

import useHydrateMapLocation from './useHydrateMapLocation';

describe('hooks / map / useHydrateMapLocation', () => {
  let useMapLocationStub: SinonStub;
  let useMapSessionLocationStub: SinonStub;

  let getUserLocationStub: SinonStub;

  const doSetMapLocation = sinon.stub();
  const sessionLocation = { center: { lat: 1, lon: 1 }, zoom: 12 };
  const location = { center: { lat: 2, lon: 2 }, zoom: 1 };

  beforeEach(() => {
    useMapLocationStub = sinon
      .stub(useMapLocation, 'default')
      .returns({ doSetMapLocation, location, boundingBox: null, locationLoaded: false });
    useMapSessionLocationStub = sinon
      .stub(useMapSessionLocation, 'default')
      .returns({ mapSessionLocation: sessionLocation, setMapSessionLocation: () => {} });
    getUserLocationStub = sinon.stub(mapActions, 'getUserLocation');
  });

  afterEach(() => {
    doSetMapLocation.reset();
    useMapLocationStub.restore();
    useMapSessionLocationStub.restore();
    getUserLocationStub.restore();
  });

  const Comp = () => {
    useHydrateMapLocation();

    return <div />;
  };

  it('should call doSetMapLocation with the user location if no location exists', async () => {
    const loc = { center: { lat: 1, lon: 1 }, zoom: 10 };
    useMapSessionLocationStub.returns({});
    getUserLocationStub.returns(loc);
    const { wrapper } = mountWithReduxAndRouter(Comp);

    await wait(1);

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(doSetMapLocation).to.have.been.calledOnceWithExactly(loc);
  });

  it('should use a location from the buoy map path', () => {
    useMapSessionLocationStub.returns({});
    const { wrapper } = mountWithReduxAndRouter(
      Comp,
      {},
      { router: { query: { location: '1,1,12z' } } },
    );

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(doSetMapLocation).to.have.been.calledOnceWithExactly({
      center: { lat: 1, lon: 1 },
      zoom: 12,
    });
  });

  it('should prioritize a location from the URL', () => {
    mountWithReduxAndRouter(Comp, {}, { router: { query: { location: '1,1,12z' } } });

    expect(doSetMapLocation).to.have.been.calledOnceWithExactly({
      center: { lat: 1, lon: 1 },
      zoom: 12,
    });
  });

  it('should fallback to a location from the sessionStorage', () => {
    mountWithReduxAndRouter(Comp);

    expect(doSetMapLocation).to.have.been.calledOnceWithExactly(sessionLocation);
  });
});
