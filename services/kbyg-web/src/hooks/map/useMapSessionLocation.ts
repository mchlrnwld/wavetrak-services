import { useSessionStorageValue } from '@react-hookz/web';
import { Location } from '../../types/map';

export const MAP_LOCATION_KEY = 'slMapLocation';

/**
 * @description
 * This hook is responsible for retrieving the saved map location from the session storage and providing
 * a function to update it.
 */
const useMapSessionLocation = () => {
  const [mapSessionLocation, setMapSessionLocation] =
    useSessionStorageValue<Location>(MAP_LOCATION_KEY);

  return { mapSessionLocation, setMapSessionLocation };
};

export default useMapSessionLocation;
