import React from 'react';
import { expect } from 'chai';

import { getWindow } from '@surfline/web-common';
import useSetupMapPage from './useSetupMapPage';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

import state from '../../selectors/fixtures/state';

describe('hooks / map / useSetupMapPage', () => {
  const win = getWindow();
  const Comp = () => {
    useSetupMapPage();

    return <div />;
  };

  it('should perform mount and unmount operations', () => {
    const { wrapper } = mountWithReduxAndRouter(
      Comp,
      {},
      {
        initialState: state,
      },
    );

    expect(win.document.body.classList.contains('sl-hide-ze-widget')).to.be.true();
    expect(win.document.body.classList.contains('sl-map-page')).to.be.true();
    expect(win.document.body.classList.contains('sl-map-v2')).to.be.true();

    wrapper.unmount();

    expect(win.document.body.classList.contains('sl-hide-ze-widget')).to.be.false();
    expect(win.document.body.classList.contains('sl-map-page')).to.be.false();
    expect(win.document.body.classList.contains('sl-map-v2')).to.be.false();
  });
});
