import { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { getMapZoom } from '../../selectors/mapV2';
import cluster from '../../utils/cluster';

export const properties = (points: Array<object>) => {
  const count = points.length;

  return {
    count,
  };
};

export interface Data {
  lon?: number;
  longitude?: number;
  lat?: number;
  latitude?: number;
}

interface Options {
  maxClusterZoom?: number;
  minRadius?: number;
  maxRadius?: number;
  radiusIncreaseThreshold?: number;
}

const useClustering = (
  data: Data[] | null,
  {
    maxClusterZoom = 7,
    minRadius = 100,
    maxRadius = 150,
    radiusIncreaseThreshold = 5,
  }: Options = {},
) => {
  const zoom = useSelector(getMapZoom);
  const shouldCluster = zoom <= maxClusterZoom;

  const clusters = useMemo(
    () => {
      if (shouldCluster && data) {
        const points = data.map((d) => ({
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [d.lon || d.longitude, d.lat || d.latitude],
          },
        }));

        const radius = zoom < radiusIncreaseThreshold ? maxRadius : minRadius;
        const options = { radius, properties };

        return cluster(points, zoom, options);
      }
      return null;
    },
    // we don't want to recompute clustering every time the zoom changes, since we'll have to recompute
    // after the data changes as a result of the new zoom
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data, shouldCluster, maxRadius, minRadius, radiusIncreaseThreshold],
  );

  return clusters;
};

export default useClustering;
