import { useEffect } from 'react';
import useMapLocation from './useMapLocation';
import useMapSessionLocation from './useMapSessionLocation';

/**
 * @description
 * This hook is responsible for updating session storage whenever the map
 * location in the redux store changes.
 */
const useSyncMapLocationToSession = () => {
  const { location, locationLoaded } = useMapLocation();
  const { setMapSessionLocation } = useMapSessionLocation();

  useEffect(() => {
    if (location && locationLoaded) {
      setMapSessionLocation(location);
    }
  }, [location, locationLoaded, setMapSessionLocation]);
};

export default useSyncMapLocationToSession;
