import { useEffect } from 'react';

import useMapLocation from './useMapLocation';
import useMapLocationView from './useMapLocationView';

/**
 * @description
 * When using the Geoname map the geoname location view needs to exist. We use this
 * and the map instance to calculate the location to center the map on
 */
const useHydrateGeonameMapLocation = () => {
  const { doSetMapLocation } = useMapLocation();
  const location = useMapLocationView();

  useEffect(() => {
    if (location) {
      doSetMapLocation(location);
    }
  }, [location, doSetMapLocation]);
};

export default useHydrateGeonameMapLocation;
