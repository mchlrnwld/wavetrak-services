import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon, { SinonStub } from 'sinon';

import useMapLocationView from './useMapLocationView';

import * as useMap from './useMap';
import * as mapSelectors from '../../selectors/mapV2';
import * as boundingBoxToLocation from '../../utils/boundingBoxToLocation';
import { Location } from '../../types/map';

describe('hooks / map / useMapLocationView', () => {
  let useLocationViewBoundingBoxStub: SinonStub;
  let useMapStub: SinonStub;
  let boundingBoxToLocationStub: SinonStub;

  let locationResult: Location | undefined | null;

  const location = {
    center: {
      lat: 1,
      lon: 1,
    },
    zoom: 12,
  };

  beforeEach(() => {
    useLocationViewBoundingBoxStub = sinon.stub(mapSelectors, 'useLocationViewBoundingBox');
    useMapStub = sinon.stub(useMap, 'default');
    boundingBoxToLocationStub = sinon.stub(boundingBoxToLocation, 'default');
  });

  afterEach(() => {
    useLocationViewBoundingBoxStub.restore();
    useMapStub.restore();
    boundingBoxToLocationStub.restore();

    locationResult = undefined;
  });

  const Comp = () => {
    const loc = useMapLocationView();

    locationResult = loc;
    return <div />;
  };

  it('should return null if no bounding box exists', () => {
    useMapStub.returns([undefined]);
    useLocationViewBoundingBoxStub.returns(null);

    mount(<Comp />);

    expect(locationResult).to.be.null();
  });

  it('should return null if the map is not defined', () => {
    const boundingBox = { north: 1, south: 1, east: 1, west: 1 };
    useMapStub.returns([undefined]);
    useLocationViewBoundingBoxStub.returns(boundingBox);

    mount(<Comp />);

    expect(locationResult).to.be.null();
    expect(boundingBoxToLocationStub).to.not.have.been.called();
  });

  it('should return the location if the bounding box and map exist', () => {
    const boundingBox = { north: 1, south: 1, east: 1, west: 1 };
    const map = {};
    useMapStub.returns([map]);
    useLocationViewBoundingBoxStub.returns(boundingBox);
    boundingBoxToLocationStub.returns(location);

    mount(<Comp />);

    expect(boundingBoxToLocationStub).to.have.been.calledOnceWithExactly(map, boundingBox);
    expect(locationResult).to.deep.equal(location);
  });
});
