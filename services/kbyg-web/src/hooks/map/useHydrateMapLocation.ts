import { useMount } from 'react-use';
import { useRouter } from 'next/router';

import { getUserLocation } from '../../actions/map';
import useMapSessionLocation from './useMapSessionLocation';
import useMapLocation from './useMapLocation';
import { transformLocationParam } from '../../utils/urls/mapPaths';

/**
 * @description
 * We want to hydrate the map location from either the URL or the
 * session storage (in that order of priority)
 */
const useHydrateMapLocation = () => {
  const { doSetMapLocation } = useMapLocation();
  const router = useRouter();
  const { location } = router.query as { location: string };
  const { mapSessionLocation } = useMapSessionLocation();

  useMount(async () => {
    // Path location param is the second highest precendence but only available
    // on the location map pages
    if (location) {
      return doSetMapLocation(transformLocationParam(location));
    }

    // Session location is the last resort for hydrating pre-existing location
    if (mapSessionLocation) {
      return doSetMapLocation(mapSessionLocation);
    }

    const userLocation = await getUserLocation();

    // Fallback to the user location or surfline HQ
    return doSetMapLocation(userLocation);
  });
};

export default useHydrateMapLocation;
