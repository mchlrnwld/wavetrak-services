import React from 'react';

import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon, { SinonStub } from 'sinon';

import * as buoysActions from '../../actions/buoys';
import * as useMapLocation from './useMapLocation';
import * as useSyncValueToStore from '../useSyncValueToStore';
import { setMapError, setMapLoading, setMapMessage } from '../../actions/mapV2';
import { initialState } from '../../reducers/mapV2';

import useBuoysBoundingBox from './useBuoysBoundingBoxQuery';

describe('hooks / map / useBuoysBoundingBoxQuery', () => {
  let useMapLocationStub: SinonStub;
  let useGetBuoysInBoundingBoxQueryStub: SinonStub;
  let useSyncValueToStoreStub: SinonStub;

  const boundingBox = { north: 1, south: 1, east: 1, west: 1 };

  let buoysVal: any;
  let loadingVal: any;
  let errorVal: any;
  let noBuoysFoundVal: any;

  beforeEach(() => {
    useMapLocationStub = sinon.stub(useMapLocation, 'default').returns({
      location: initialState.location,
      boundingBox,
      locationLoaded: initialState.locationLoaded,
      doSetMapLocation: () => {},
    });
    useGetBuoysInBoundingBoxQueryStub = sinon
      .stub(buoysActions, 'useGetBuoysInBoundingBoxQuery')
      .returns({ refetch: () => {} });

    useSyncValueToStoreStub = sinon.stub(useSyncValueToStore, 'default');
  });

  afterEach(() => {
    useMapLocationStub.restore();
    useGetBuoysInBoundingBoxQueryStub.restore();
    useSyncValueToStoreStub.restore();

    buoysVal = undefined;
    loadingVal = undefined;
    errorVal = undefined;
    noBuoysFoundVal = undefined;
  });

  const Comp = () => {
    const { buoys, loading, error, noBuoysFound } = useBuoysBoundingBox();

    buoysVal = buoys;
    loadingVal = loading;
    errorVal = error;
    noBuoysFoundVal = noBuoysFound;

    return <div />;
  };

  it('should skip the query if the bounding box is not set', () => {
    useMapLocationStub.returns({});
    mount(<Comp />);

    expect(useGetBuoysInBoundingBoxQueryStub).to.have.been.calledOnceWithExactly(undefined, {
      skip: true,
    });
  });

  it('should perform the query with the bounding box', () => {
    mount(<Comp />);

    expect(useGetBuoysInBoundingBoxQueryStub).to.have.been.calledOnceWithExactly(boundingBox, {
      skip: false,
    });
  });

  it('should handle uninitialized state', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: undefined,
      error: undefined,
      isFetching: false,
      isUninitialized: true,
    });

    mount(<Comp />);

    expect(buoysVal).to.deep.equal(undefined);
    expect(loadingVal).to.equal(true);
    expect(errorVal).to.equal(undefined);
    expect(noBuoysFoundVal).to.equal(false);
  });

  it('should handle loading state', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: undefined,
      error: undefined,
      isFetching: true,
      isUninitialized: false,
    });

    mount(<Comp />);

    expect(buoysVal).to.deep.equal(undefined);
    expect(loadingVal).to.equal(true);
    expect(errorVal).to.equal(undefined);
    expect(noBuoysFoundVal).to.equal(false);
  });

  it('should sync the loading state to the store', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: undefined,
      error: undefined,
      isFetching: true,
      isUninitialized: false,
    });

    mount(<Comp />);

    expect(useSyncValueToStoreStub).to.have.been.calledWithExactly(setMapLoading, true);
  });

  it('should handle no buoys in response', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: { data: [], associated: { units: {} } },
      error: undefined,
      isFetching: false,
      isUninitialized: false,
    });

    mount(<Comp />);

    expect(buoysVal).to.deep.equal([]);
    expect(loadingVal).to.equal(false);
    expect(errorVal).to.equal(undefined);
    expect(noBuoysFoundVal).to.equal(true);
  });

  it('should sync a message to the store if there are no buoys', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: { data: [], associated: { units: {} } },
      error: undefined,
      isFetching: false,
      isUninitialized: false,
    });

    const wrapper = mount(<Comp />);

    expect(useSyncValueToStoreStub).to.have.been.calledWithExactly(
      setMapMessage,
      'No buoys found! Try zooming out or moving map',
    );

    useGetBuoysInBoundingBoxQueryStub.resetBehavior();
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: { data: [{ buoy: true }], associated: { units: {} } },
      error: undefined,
      isFetching: false,
      isUninitialized: false,
    });

    useGetBuoysInBoundingBoxQueryStub.resetHistory();

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(useSyncValueToStoreStub).to.have.been.calledWithExactly(setMapMessage, '');
  });

  it('should return the error', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: undefined,
      error: { message: 'message' },
      isFetching: false,
      isUninitialized: false,
    });

    mount(<Comp />);

    expect(buoysVal).to.deep.equal(undefined);
    expect(loadingVal).to.equal(false);
    expect(errorVal).to.deep.equal({ message: 'message' });
    expect(noBuoysFoundVal).to.equal(false);
  });

  it('should sync the error to the store', () => {
    useGetBuoysInBoundingBoxQueryStub.returns({
      data: undefined,
      error: { message: 'message' },
      isFetching: false,
      isUninitialized: false,
    });

    mount(<Comp />);

    expect(useSyncValueToStoreStub).to.have.been.calledWithExactly(setMapError, {
      message: 'message',
    });
  });
});
