import React from 'react';

import { mount } from 'enzyme';
import { expect } from 'chai';
import { Map } from 'leaflet';
import { act } from 'react-dom/test-utils';
import useMap from './useMap';

describe('hooks / map / useMap', () => {
  let childMap: Map | undefined;
  let compMap: Map | undefined;
  let doSetMap: ReturnType<typeof useMap>[1] | undefined;

  afterEach(() => {
    childMap = undefined;
    compMap = undefined;
    doSetMap = undefined;
  });

  const Child = () => {
    const [map] = useMap();

    childMap = map;
    return <div />;
  };
  const Comp = () => {
    const [map, setMap] = useMap();

    compMap = map;
    doSetMap = setMap;
    return (
      <div>
        <Child />
      </div>
    );
  };

  it('should manage the global map state', () => {
    const wrapper = mount(<Comp />);

    expect(childMap).to.be.undefined();
    expect(compMap).to.be.undefined();

    const map = { getContainer: () => null } as any as Map;
    act(() => {
      doSetMap!(map);
      wrapper.update();
    });

    expect(childMap).to.be.equal(map);
    expect(compMap).to.be.equal(map);
  });
});
