import { useEffect } from 'react';
import type { UrlObject } from 'url';
import { useRouter } from 'next/router';

import useMapLocation from './useMapLocation';
import { locationObjectToString } from '../../utils/urls/mapPaths';
import { Location } from '../../types/map';

/**
 * @description
 * We want to sync the map location to the URL so that users can share/bookmark/reload the page
 * and keep their position on the map
 */
const useSyncMapLocationToUrl = (pathGetter: (location: Location) => UrlObject | string) => {
  const { location, locationLoaded } = useMapLocation();
  const router = useRouter();
  const { location: currentLocation } = router.query;

  useEffect(() => {
    const pathHasChanged = location && currentLocation !== locationObjectToString(location);

    if (location && locationLoaded && pathHasChanged) {
      router.replace(pathGetter(location), undefined, { shallow: true });
    }
  }, [location, locationLoaded, currentLocation, pathGetter, router]);
};

export default useSyncMapLocationToUrl;
