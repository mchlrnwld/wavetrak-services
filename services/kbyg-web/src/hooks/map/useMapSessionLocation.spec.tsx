import React from 'react';
import { mount, shallow } from 'enzyme';
import { getWindow } from '@surfline/web-common';
import { expect } from 'chai';

import useMapSessionLocation, { MAP_LOCATION_KEY } from './useMapSessionLocation';

describe('hooks / map / useMapSessionLocation', () => {
  const win = getWindow();
  const Comp = () => {
    const { mapSessionLocation } = useMapSessionLocation();

    return <div>{JSON.stringify(mapSessionLocation)}</div>;
  };

  beforeEach(() => {
    win.sessionStorage.setItem(MAP_LOCATION_KEY, JSON.stringify({ test: true }));
  });

  afterEach(() => {
    win.sessionStorage.clear();
  });

  it('should grab the existing value', () => {
    const wrapper = shallow(<Comp />);
    const div = wrapper.find('div');

    expect(div.text()).to.equal('{"test":true}');
  });

  it('should return null if no value exists', () => {
    win.sessionStorage.clear();
    const wrapper = mount(<Comp />);
    const div = wrapper.find('div');

    expect(div.text()).to.equal('null');
  });
});
