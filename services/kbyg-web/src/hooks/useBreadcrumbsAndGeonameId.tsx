import { useRouter } from 'next/router';

const useBreadcrumbsAndGeonameId = (): { breadcrumbs: string[]; geonameId: string } => {
  const router = useRouter();
  const { geonames } = router.query as { geonames: string[] };

  const breadcrumbs = geonames.slice(0, -1);
  const geonameId = geonames[geonames.length - 1];

  return {
    breadcrumbs,
    geonameId,
  };
};

export default useBreadcrumbsAndGeonameId;
