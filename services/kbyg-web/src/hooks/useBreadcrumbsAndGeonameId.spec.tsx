import React from 'react';

import { expect } from 'chai';

import useBreadcrumbsAndGeonameId from './useBreadcrumbsAndGeonameId';
import { mountWithReduxAndRouter } from '../utils/test-utils';

describe('hooks / map / useBreadcrumbsAndGeonameId', () => {
  let breadcrumbs: string[] | undefined;
  let geonameId: string | undefined;

  afterEach(() => {
    breadcrumbs = undefined;
    geonameId = undefined;
  });

  const Comp = () => {
    const result = useBreadcrumbsAndGeonameId();
    breadcrumbs = result.breadcrumbs;
    geonameId = result.geonameId;

    return <div />;
  };

  it('should get the expected breadcrumbs and geoname ID', () => {
    mountWithReduxAndRouter(
      Comp,
      {},
      { router: { query: { geonames: ['united-states', 'new-york', '123'] } } },
    );

    expect(breadcrumbs).to.deep.equal(['united-states', 'new-york']);
    expect(geonameId).to.be.equal('123');
  });
});
