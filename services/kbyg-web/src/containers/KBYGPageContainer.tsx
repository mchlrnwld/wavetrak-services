import React, { useCallback, useEffect, useRef } from 'react';
import classNames from 'classnames';
import {
  canUseDOM,
  getUserPremiumStatus,
  getWavetrakIdentity,
  getWindow,
} from '@surfline/web-common';
import { ErrorBoundary } from '@surfline/quiver-react';

import ErrorPage from './ErrorPage';
import { getStatusCode } from '../selectors/status';
import { useAppSelector } from '../stores/hooks';

const getClassName = (isPremium: boolean) =>
  classNames({
    'sl-kbyg-page-container': true,
    'sl-kbyg-page-container--is-premium': isPremium,
  });

const useIdentifyUser = () => {
  const analyticsReady = useAppSelector((state) => state.analytics.ready);
  // We don't need to re-render the whole tree for this value, so let's use a ref
  const hasIdentified = useRef(false);

  useEffect(() => {
    if (analyticsReady && !hasIdentified.current) {
      const win = getWindow();
      const identity = getWavetrakIdentity();
      if (canUseDOM && win?.analytics && win?.analytics.user && identity) {
        if (identity.type === 'logged_in' && identity.userId) {
          win.analytics.identify(identity.userId, { sourceOrigin: 'KBYG' });
          hasIdentified.current = true;
        }
      }
    }
  }, [analyticsReady]);
};

const KBYGPageContainer: React.FC = ({ children }) => {
  const status = useAppSelector(getStatusCode);
  const isPremium = useAppSelector(getUserPremiumStatus);
  useIdentifyUser();

  const pageContent = useCallback(() => {
    if (status !== 200) {
      return <ErrorPage statusCode={status} />;
    }
    return children;
  }, [status, children]);

  return (
    <>
      <div className={getClassName(isPremium)}>
        <ErrorBoundary error={status === 500}>{pageContent()}</ErrorBoundary>
      </div>
      <div id="modal-root" />
    </>
  );
};

export default KBYGPageContainer;
