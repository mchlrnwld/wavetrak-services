import { connect } from 'react-redux';
import { withDevice } from '@surfline/quiver-react';
import { withRouter } from 'next/router';

import { getSpotMultiCam, getSpotReportData } from '../../selectors/spot';
import { setPrimaryCam, setCamView } from '../../actions/multiCam';
import SpotReportCams from './SpotReportCams';
import { getIsSurfCheckEligible } from '../../selectors/meter';

export default withDevice(
  connect(
    (state) => ({
      multiCam: getSpotMultiCam(state),
      reportData: getSpotReportData(state),
      isSurfCheckEligible: getIsSurfCheckEligible(state),
    }),
    (dispatch) => ({
      doSetPrimaryCam: (camera) => dispatch(setPrimaryCam(camera)),
      doSetCamView: (activeView) => dispatch(setCamView(activeView)),
    }),
  )(withRouter(SpotReportCams)),
);
