import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Spinner, CTAProvider, CamTimeout, CamCTA } from '@surfline/quiver-react';
import classNames from 'classnames';

import { canUseDOM, getWindow } from '@surfline/web-common';
import { withRouter } from 'next/router';
import config from '../../config';

import SpotReportCam from '../../components/SpotReportCam';
import NearbyCams from '../../components/NearbyCams';
import SpotReportCamHeaderCTA from '../../components/SpotReportCamHeaderCTA';

import devicePropType from '../../propTypes/device';
import spotReportDataPropType from '../../propTypes/spot';
import userDetailsPropTypes from '../../propTypes/userDetails';
import multiCamPropType from '../../propTypes/multiCam';

import CamSelectorMenu from '../../components/CamSelectorMenu';
import MultiCamPaywall from '../../components/MultiCamPaywall';
import getCamStillImage from '../../utils/getCamStillImage';

import meterPropTypes from '../../propTypes/meter';
import { ALT, MSG, SUB } from '../../common/constants';

const spotReportCamerasClassName = (isMultiCam, numCamsDisplayed, isPremium, multiCamTimeout) =>
  classNames({
    'sl-spot-report-page__cameras': true,
    'sl-spot-report-page__cameras--multi-cam': isMultiCam,
    'sl-spot-report-page__cameras--single-cam': isMultiCam && isPremium && numCamsDisplayed === 1,
    'sl-spot-report-page__cameras--quad-cam': isMultiCam && numCamsDisplayed === 4,
    'sl-spot-report-page__cameras--multi-timeout': multiCamTimeout,
  });

const camContainerClassName = (isPrimary, camNum, showPaywall) => {
  const secondaryCamNumClass = `sl-spot-report-page-multi-cam__secondary--${camNum + 1}`;
  return classNames({
    'sl-spot-report-page-multi-cam__secondary': !isPrimary,
    'sl-spot-report-page-multi-cam__primary': isPrimary,
    [secondaryCamNumClass]: !isPrimary,
    'sl-spot-report-page-multi-cam__paywall': showPaywall,
  });
};

const multiCamLoadingClassName = (numCams) =>
  classNames({
    'sl-multi-cam-loading': true,
    'sl-multi-cam-loading--double': numCams === 2,
    'sl-multi-cam-loading--triple': numCams === 3,
    'sl-multi-cam-loading--quad': numCams === 4,
  });

const multiCamContainerClassName = (numCamsDisplayed) =>
  classNames({
    'sl-spot-report-page-multi-cam': true,
    'sl-spot-report-page-multi-cam--single': numCamsDisplayed === 1,
    'sl-spot-report-page-multi-cam--double': numCamsDisplayed === 2,
    'sl-spot-report-page-multi-cam--triple': numCamsDisplayed === 3,
    'sl-spot-report-page-multi-cam--quad': numCamsDisplayed === 4,
  });

/**
 * @typedef {object} Props
 * @prop {import('next/router').NextRouter} router
 *
 * @extends {Component<Props>}
 */
class SpotReportCams extends Component {
  constructor(props) {
    super(props);

    const { isTheatreMode, multiCam, urlSetCamId, isMultiCam, isPremium } = this.props;

    this.state = {
      numCamsDisplayed:
        isTheatreMode && multiCam.activeView === 'MULTI' && !urlSetCamId ? multiCam.numCams : 1,
      multiCamTimeout: false,
      multiCamIsLoading: !!isMultiCam && isPremium,
    };
  }

  componentDidMount() {
    const { shouldRecomputeMultiCamView, setShouldRecomputeMultiCamView } = this.props;
    if (shouldRecomputeMultiCamView) {
      this.setupMultiCamView();
      setShouldRecomputeMultiCamView(false);
    }
  }

  completeMultiCamLoading = () => this.setState({ multiCamIsLoading: false });

  setupMultiCamView = () => {
    const { multiCam, isPremium, urlSetCamId } = this.props;
    const win = getWindow();

    if (canUseDOM) {
      const isDesktopOrTablet = win.innerWidth >= 512;
      const defaultToMulti = multiCam.isMultiCam && isDesktopOrTablet && isPremium;
      const defaultToSingle = !!urlSetCamId;

      if (defaultToMulti && !defaultToSingle) {
        this.doChangeCamView('MULTI', multiCam.primaryCam);
      } else {
        this.doChangeCamView('SINGLE', multiCam.primaryCam);
      }
      this.completeMultiCamLoading();
    }
  };

  timeoutMultiCams = () => {
    this.setState({
      multiCamTimeout: true,
    });
  };

  resumeMultiCams = () =>
    this.setState({
      multiCamTimeout: false,
    });

  doChangeCamView = (activeView, camera) => {
    const { multiCam, doSetCamView, doSetPrimaryCam } = this.props;
    const numCamsDisplayed = multiCam.numCams;

    const isPrimaryCam = camera?._id === multiCam?.primaryCam?._id;
    const isMultiView = activeView === 'MULTI';

    this.doUpdateCamQueryParam(camera, isMultiView);
    doSetCamView(activeView);

    if (!isPrimaryCam) {
      doSetPrimaryCam(camera);
    }

    return this.setState({
      numCamsDisplayed: isMultiView ? numCamsDisplayed : 1,
    });
  };

  /**
   * @param {Object} cam
   * @param {boolean} [isMultiView=false]
   * @memberof SpotReportCams
   */
  doUpdateCamQueryParam = (cam, isMultiView = false) => {
    const { router, isMultiCam } = this.props;

    const hasCamParam = !!router.query.camId;

    if (!isMultiView || (isMultiView && hasCamParam)) {
      const isMulti = isMultiView || !isMultiCam;

      const query = {
        ...router.query,
        camId: cam._id,
      };

      // When we go to multi-cam, we strip the camId param
      if (isMulti) {
        delete query.camId;
      }

      router.replace(
        {
          pathname: router.pathname,
          query,
        },
        undefined,
        { shallow: true },
      );
    }
  };

  render() {
    const {
      isMultiCam,
      multiCam,
      spotId,
      legacySpotId,
      spot,
      isTheatreMode,
      reportData,
      userDetails,
      isPremium,
      doSetPrimaryCam,
      headerCTASegmentProperties,
      device,
      urlSetCamId,
      meter,
      isSurfCheckEligible,
    } = this.props;
    const { multiCamIsLoading } = this.state;

    const subregionId = spot && spot.subregion._id;

    const spotReportCamProps = {
      spotId,
      legacySpotId,
      spot,
      reportData,
      userDetails,
      isPremium,
    };

    const multiCamPaywallSegmentProperties = {
      location: 'multi cam view',
      category: 'cams & reports',
      pageName: 'Spot Report',
      subregionId,
      spotId,
      spotName: spot.name,
    };

    const singlePlayerCam = spot.cameras.filter((cam) => cam._id === urlSetCamId);
    const isSingleCamAssignedToSpot = singlePlayerCam.length > 0;
    const primaryCamMatches = multiCam.primaryCam._id === urlSetCamId;
    if (!primaryCamMatches && isSingleCamAssignedToSpot) {
      doSetPrimaryCam(singlePlayerCam[0]);
    }

    const { numCamsDisplayed, multiCamTimeout } = this.state;
    const showPaywall = isMultiCam && multiCam.activeView === 'MULTI' && !isPremium;
    const displayedCams =
      multiCam.activeView === 'MULTI'
        ? multiCam.initialCameras.slice(0, numCamsDisplayed)
        : [multiCam.primaryCam];
    const numberOfCams = displayedCams.length;

    const isMobile = device.width <= config.mobileWidth;
    const isTablet = device.width <= config.tabletLargeWidth && !isMobile;
    let messageType = isMobile || isTablet ? MSG : SUB;
    const isMetered = meter?.meteringEnabled;
    const outOfSurfChecks = isMetered && !isSurfCheckEligible;
    const isAnonymous = !userDetails;
    const showRLMultiCamPaywall =
      isMetered &&
      isMultiCam &&
      multiCam.activeView === 'MULTI' &&
      (isAnonymous || (userDetails && outOfSurfChecks));
    return (
      <div
        className={spotReportCamerasClassName(
          isMultiCam,
          numCamsDisplayed,
          isPremium,
          multiCamTimeout,
        )}
      >
        {!isMetered && !isPremium && !isTheatreMode ? (
          <div className="sl-spot-report-cam__cta">
            <SpotReportCamHeaderCTA eventProperties={headerCTASegmentProperties} />
          </div>
        ) : null}
        {isMultiCam && multiCamIsLoading && (
          <div className={multiCamLoadingClassName(multiCam.cameras.length)}>
            <Spinner />
          </div>
        )}
        {isMultiCam && !multiCamIsLoading && (
          <>
            <CamSelectorMenu
              cameras={multiCam.cameras}
              isPremium={isPremium}
              primaryCam={multiCam.primaryCam}
              numCams={multiCam.numCams}
              activeView={multiCam.activeView}
              initialCameras={multiCam.initialCameras}
              doSetCamView={this.doChangeCamView}
              spotId={spotId}
              subregionId={spot && spot.subregion._id}
              userDetails={userDetails}
            />
            <div className={multiCamContainerClassName(numCamsDisplayed)}>
              {multiCamTimeout && <CamTimeout isMultiCam onClick={this.resumeMultiCams} />}
              {displayedCams.map((cam) => {
                const isPrimaryCam = cam._id === multiCam.primaryCam._id || numCamsDisplayed === 1;
                const camNum = multiCam.cameras.findIndex(
                  (orderedCam) => orderedCam._id === cam._id,
                );
                const camStillUrl = getCamStillImage(cam);

                // specific messageType for multi cam configs
                if (numberOfCams % 2 === 0 && !isTablet) {
                  messageType = isMobile ? ALT : SUB;
                }

                if (numberOfCams === 3) {
                  if (!isPrimaryCam) {
                    messageType = isMobile || isTablet ? ALT : MSG;
                  } else if (isMobile) {
                    messageType = ALT;
                  }
                }

                return (
                  <div
                    key={cam._id}
                    className={camContainerClassName(isPrimaryCam, camNum, showPaywall)}
                  >
                    {showPaywall || multiCamTimeout || showRLMultiCamPaywall ? (
                      <div
                        className="sl-spot-report-page-multi-cam__camStill"
                        style={{ backgroundImage: `url(${camStillUrl})` }}
                      />
                    ) : (
                      <SpotReportCam
                        urlSetCamId={urlSetCamId}
                        isMultiCam
                        key={cam._id}
                        playerId={`sl-spot-report-cam-${cam._id}`}
                        camera={cam}
                        numCamsDisplayed={numCamsDisplayed}
                        isPersistentCam={isPrimaryCam}
                        multiCamTimeout={multiCamTimeout}
                        isPrimaryCam={isPrimaryCam}
                        messageType={messageType}
                        timeoutMultiCams={this.timeoutMultiCams}
                        resumeMultiCams={this.resumeMultiCams}
                        {...spotReportCamProps}
                      />
                    )}
                  </div>
                );
              })}
              {showRLMultiCamPaywall ? (
                <CamCTA
                  isAnonymous={isAnonymous}
                  isMulti
                  segmentProperties={multiCamPaywallSegmentProperties}
                />
              ) : (
                showPaywall && (
                  <CTAProvider segmentProperties={multiCamPaywallSegmentProperties}>
                    <MultiCamPaywall />
                  </CTAProvider>
                )
              )}
            </div>
          </>
        )}
        {!isMultiCam && (
          <SpotReportCam
            urlSetCamId={urlSetCamId}
            isPersistentCam
            camera={isSingleCamAssignedToSpot ? singlePlayerCam[0] : spot.cameras[0]}
            messageType={messageType}
            {...spotReportCamProps}
          />
        )}
        {!isTheatreMode && <NearbyCams spotId={spotId} />}
      </div>
    );
  }
}

SpotReportCams.propTypes = {
  shouldRecomputeMultiCamView: PropTypes.bool,
  setShouldRecomputeMultiCamView: PropTypes.func.isRequired,
  doSetCamView: PropTypes.func.isRequired,
  multiCam: multiCamPropType.isRequired,
  isPremium: PropTypes.bool,
  isTheatreMode: PropTypes.bool,
  spot: PropTypes.shape({
    name: PropTypes.string,
    subregion: PropTypes.shape({
      _id: PropTypes.string,
      forecastStatus: PropTypes.string,
    }),
    cameras: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  spotId: PropTypes.string.isRequired,
  legacySpotId: PropTypes.string.isRequired,
  userDetails: userDetailsPropTypes,
  reportData: spotReportDataPropType,
  doSetPrimaryCam: PropTypes.func.isRequired,
  device: devicePropType.isRequired,
  headerCTASegmentProperties: PropTypes.shape({}).isRequired,
  isMultiCam: PropTypes.bool,
  urlSetCamId: PropTypes.string,
  meter: meterPropTypes,
  isSurfCheckEligible: PropTypes.bool.isRequired,
};

SpotReportCams.defaultProps = {
  shouldRecomputeMultiCamView: false,
  meter: null,
  spot: null,
  isPremium: false,
  userDetails: null,
  reportData: null,
  isTheatreMode: false,
  isMultiCam: false,
  urlSetCamId: null,
};

export default withRouter(SpotReportCams);
