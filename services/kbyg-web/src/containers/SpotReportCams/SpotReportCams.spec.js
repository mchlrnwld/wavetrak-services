import React from 'react';
import { act } from 'react-dom/test-utils';
import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { mount } from 'enzyme';
import { CamPlayer, CamTimeout } from '@surfline/quiver-react';
import ConnectedSpotReportCams from '.';
import SpotReportCams from './SpotReportCams';
import CamList from '../../components/CamList';
import CamListError from '../../components/CamListError';
import CamSelectorMenu from '../../components/CamSelectorMenu';
import SpotReportCamHeaderCTA from '../../components/SpotReportCamHeaderCTA';
import MultiCamPaywall from '../../components/MultiCamPaywall';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

const cameras = [
  {
    _id: '58a377daea714bf7668c000f',
    title: 'SOCAL - HB Pier Southside Overview',
    streamUrl: 'https://cams.cdn-surfline.com/cdn-wc/wc-hbpierssov/playlist.m3u8',
    stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpierssov/latest_small.jpg',
    rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/wc-hbpierssov/wc-hbpierssov',
    isPremium: false,
    status: {
      isDown: false,
      message: '',
    },
    control: 'https://camstills.cdn-surfline.com/wc-hbpierssov/latest_small.jpg',
    nighttime: false,
    rewindClip:
      'https://camrewinds.cdn-surfline.com/wc-hbpierssov/wc-hbpierssov.1300.2018-12-02.mp4',
  },
  {
    _id: '5978cb2518fd6daf0da062ae',
    title: 'SOCAL - HB Pier Southside Closeup',
    streamUrl: 'https://cams.cdn-surfline.com/cdn-wc/wc-hbpiersclose/playlist.m3u8',
    stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small.jpg',
    rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/wc-hbpiersclose/wc-hbpiersclose',
    isPremium: false,
    status: {
      isDown: false,
      message: '',
    },
    control: 'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small.jpg',
    nighttime: false,
    rewindClip:
      'https://camrewinds.cdn-surfline.com/wc-hbpiersclose/wc-hbpiersclose.1300.2018-12-02.mp4',
  },
];

describe('containers / SpotReportCams', () => {
  let mockStore;
  const defaultState = {
    animations: {
      scrollPositions: {
        camList: {
          top: 0,
          left: 0,
        },
      },
    },
    backplane: {
      user: {
        entitlements: [],
        settings: {
          units: {
            surfHeight: 'M',
          },
        },
      },
    },
    spot: {
      multiCam: {
        primaryCam: cameras[0],
        cameras,
        initialCameras: cameras,
        numCams: 2,
        isMultiCam: true,
        activeView: 'SINGLE',
      },
      report: {
        associated: {
          advertising: {
            spotId: '148545',
            subregionId: '2143',
            sst: '61-65',
          },
        },
      },
      nearby: {
        data: {
          spots: [],
        },
      },
    },
  };
  const defaultProps = {
    spot: {
      name: 'HB Pier - Southside',
      cameras,
      subregion: {
        _id: '58581a836630e24c44878fd6',
        forecastStatus: 'active',
      },
    },
    device: {
      width: 400,
    },
    spotId: '123',
    legacySpotId: '11233',
    headerCTASegmentProperties: {},
    isMultiCam: true,
  };

  beforeAll(() => {
    mockStore = configureStore([ReduxThunk]);
  });

  it('renders the CamSelectorMenu if multiCam is enabled', () => {
    const store = mockStore(defaultState);
    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...defaultProps} />
      </Provider>,
    );
    const CamSelectorMenuInstance = wrapper.find(CamSelectorMenu);
    expect(CamSelectorMenuInstance).to.have.length(1);
  });

  it('Does not render the CamSelectorMenu if multiCam is false', () => {
    const store = mockStore({
      ...defaultState,
      spot: {
        ...defaultState.spot,
        multiCam: {
          ...defaultState.spot.multiCam,
          isMultiCam: false,
        },
      },
    });
    const altDefaultProps = {
      ...defaultProps,
      isMultiCam: false,
    };
    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...altDefaultProps} />
      </Provider>,
    );
    const CamSelectorMenuInstance = wrapper.find(CamSelectorMenu);
    expect(CamSelectorMenuInstance).to.have.length(0);
  });

  it('renders the NearbyCams list for a Spot with nearby spots', () => {
    const store = mockStore(defaultState);
    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...defaultProps} />
      </Provider>,
    );
    const CamListInstance = wrapper.find(CamList);
    expect(CamListInstance).to.have.length(1);
  });

  it('Does not render the NearbyCams if there is a nearby cam error', () => {
    const state = {
      ...defaultState,
      spot: {
        ...defaultState.spot,
        nearby: {
          error: true,
        },
      },
    };
    const store = mockStore(state);
    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...defaultProps} />
      </Provider>,
    );
    const CamListInstance = wrapper.find(CamList);
    expect(CamListInstance).to.have.length(0);
    const CamListErrorInstance = wrapper.find(CamListError);
    expect(CamListErrorInstance).to.have.length(0);
  });

  it('renders the SpotReportCamHeaderCTA for non-premium users', () => {
    const store = mockStore(defaultState);
    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...defaultProps} />
      </Provider>,
    );
    const CTA = wrapper.find(SpotReportCamHeaderCTA);
    expect(CTA).to.have.length(1);
  });

  it('renders the Multi Cam paywall for non-premium users', () => {
    const store = mockStore({
      ...defaultState,
      spot: {
        ...defaultState.spot,
        multiCam: {
          ...defaultState.spot.multiCam,
          activeView: 'MULTI',
        },
      },
    });

    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...defaultProps} isTheatreMode />
      </Provider>,
    );

    const MultiCamCTA = wrapper.find(MultiCamPaywall);
    expect(MultiCamCTA).to.have.length(1);
    const CamStills = wrapper.find('.sl-spot-report-page-multi-cam__camStill');
    expect(CamStills).to.have.length(cameras.length);
  });

  it('does not render the SpotReportCamHeaderCTA for premium users', () => {
    const state = {
      ...defaultState,
      backplane: {
        ...defaultState.backplane,
        user: {
          ...defaultState.backplane.user,
          entitlements: ['sl_premium'],
        },
      },
    };
    const store = mockStore(state);
    const wrapper = mount(
      <Provider store={store}>
        <ConnectedSpotReportCams {...defaultProps} isPremium />
      </Provider>,
    );
    const CTA = wrapper.find(SpotReportCamHeaderCTA);
    expect(CTA).to.have.length(0);
  });

  it('does render the cam stills when the multi cam times out', () => {
    const props = {
      ...defaultProps,
      doSetCamView: () => true,
      multiCam: {
        ...defaultState.spot.multiCam,
        activeView: 'MULTI',
      },
      isPremium: true,
      isTheatreMode: true,
      userDetails: {},
      reportData: defaultState.spot.report,
      doSetPrimaryCam: () => true,
    };

    const store = mockStore(defaultState);
    const { wrapper } = mountWithReduxAndRouter(SpotReportCams, props, {
      initialState: defaultState,
      store,
    });

    // Find the actual component not the wrapper `withRouter`
    const SpotReportCamsInstance = wrapper.find('SpotReportCams');

    act(() => {
      SpotReportCamsInstance.setState({ multiCamIsLoading: false });
      SpotReportCamsInstance.instance().timeoutMultiCams();
      wrapper.update();
    });

    const CamTimeoutInstance = wrapper.find(CamTimeout);
    const CamStillsInstance = wrapper.find('.sl-spot-report-page-multi-cam__camStill');
    expect(CamStillsInstance).to.have.length(2);
    expect(CamTimeoutInstance).to.have.length(1);
    expect(SpotReportCamsInstance.state('multiCamTimeout')).to.be.true();
  });

  it('restarts the cam if the user clicks continue watching', () => {
    const props = {
      ...defaultProps,
      doSetCamView: () => true,
      multiCam: {
        ...defaultState.spot.multiCam,
        activeView: 'MULTI',
      },
      isPremium: true,
      isTheatreMode: true,
      userDetails: {},
      reportData: defaultState.spot.report,
      doSetPrimaryCam: () => true,
    };

    const store = mockStore(defaultState);
    const wrapper = mount(
      <Provider store={store}>
        <SpotReportCams {...props} />
      </Provider>,
    );

    // Find the actual component not the wrapper `withRouter`
    const SpotReportCamsInstance = wrapper.find('SpotReportCams');

    act(() => {
      SpotReportCamsInstance.setState({ multiCamIsLoading: false });
      SpotReportCamsInstance.instance().timeoutMultiCams();
      wrapper.update();
    });

    const CamTimeoutInstance = wrapper.find(CamTimeout);
    const CamStillsInstance = wrapper.find('.sl-spot-report-page-multi-cam__camStill');
    expect(CamStillsInstance).to.have.length(2);
    expect(CamTimeoutInstance).to.have.length(1);
    expect(SpotReportCamsInstance.state('multiCamTimeout')).to.be.true();

    SpotReportCamsInstance.instance().resumeMultiCams();
    wrapper.update();
    const CamTimeoutInstance2 = wrapper.find(CamTimeout);
    const CamStillsInstance2 = wrapper.find('.sl-spot-report-page-multi-cam__camStill');
    expect(CamStillsInstance2).to.have.length(0);
    expect(CamTimeoutInstance2).to.have.length(0);
    expect(SpotReportCamsInstance.state('multiCamTimeout')).to.be.false();
    const CamPlayerInstance = wrapper.find(CamPlayer);
    expect(CamPlayerInstance).to.have.length(2);
  });
});
