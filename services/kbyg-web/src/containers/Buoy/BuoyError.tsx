import React from 'react';
import { ErrorBoundary, ErrorBox } from '@surfline/quiver-react';

import styles from './BuoyError.module.scss';

const BuoysError: React.FC = ({ children }) => (
  <ErrorBoundary
    render={() => (
      <ErrorBox classes={{ root: styles.errorBox }} message="Oh buoy! Something went wrong." />
    )}
  >
    {children}
  </ErrorBoundary>
);

export default BuoysError;
