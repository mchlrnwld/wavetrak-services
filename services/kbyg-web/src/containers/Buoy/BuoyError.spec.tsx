import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import BuoyError from './BuoyError';

const Child = () => <div className="child" />;

const Error = () => <div>this will error</div>;

describe('containers / Buoy / BuoyError', () => {
  it('should render the children if there is no error', () => {
    const wrapper = mount(
      <BuoyError>
        <Child />
      </BuoyError>,
    );

    const childWrapper = wrapper.find('Child');
    expect(childWrapper).to.have.length(1);
  });

  it('should render the error if a component throws', () => {
    const wrapper = mount(
      <BuoyError>
        <Error />
      </BuoyError>,
    );

    const errorWrapper = wrapper.find(Error);
    errorWrapper.simulateError('some error');

    wrapper.update();

    expect(wrapper.find('ErrorBox')).to.have.length(1);
  });
});
