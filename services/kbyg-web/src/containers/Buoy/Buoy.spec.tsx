import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon, { SinonStub } from 'sinon';

import * as buoyActions from '../../actions/buoys';
import BuoyPage from './Buoy';
import BuoyPageError from './BuoyError';
import BuoyReport from '../BuoyReport';

describe('containers / routes / BuoyPage', () => {
  let serverSideFetchBuoyDetailsStub: SinonStub;

  beforeEach(() => {
    serverSideFetchBuoyDetailsStub = sinon.stub(buoyActions, 'serverSideFetchBuoyDetails');
  });

  afterEach(() => {
    serverSideFetchBuoyDetailsStub.restore();
  });

  it('should render the BuoyReport as a child of the error wrapper', () => {
    const wrapper = shallow(<BuoyPage />);

    const errorWrapper = wrapper.find(BuoyPageError);
    expect(errorWrapper).to.have.length(1);
    expect(errorWrapper.find(BuoyReport)).to.have.length(1);
  });
});
