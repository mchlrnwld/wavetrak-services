import React from 'react';

import BuoyReport from '../BuoyReport';
import BuoyError from './BuoyError';

import styles from './Buoy.module.scss';

const BuoyPage = () => (
  <div className={styles.buoyReport}>
    <main>
      <BuoyError>
        <BuoyReport />
      </BuoyError>
    </main>
  </div>
);

export default BuoyPage;
