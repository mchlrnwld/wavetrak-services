import { ContentContainer, SwellEventCardContainer } from '@surfline/quiver-react';
import { getUserDetails, getUserSettings, slugify, trackEvent } from '@surfline/web-common';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useMemo } from 'react';
import { useMount, useUnmount } from 'react-use';
import { clearOverviewData, setSubregionLoading } from '../../actions/subregion';
import { doSetRecentlyVisited as setRecentlyVisited } from '../../actions/user';
import ForecastHeader from '../../components/ForecastHeader';
import PageLoading from '../../components/PageLoading';
import en from '../../intl/translations/en';
import {
  getNearbySubregions,
  getSubregionAssociated,
  getSubregionFeed,
  getSubregionLoading,
  getSubregionOverviewData,
} from '../../selectors/subregion';
import getSwellEvents from '../../selectors/swellEvents';
import { useAppDispatch, useAppSelector } from '../../stores/hooks';
import { NearbySubregions, SubregionFeed } from '../../types/subregion';
import { SwellEvents } from '../../types/swellEvents';

interface Props {
  isChartsPage?: boolean;
}

interface PageLevelLink {
  text: string;
  title?: string;
  path: string;
  isIndex?: boolean;
  newWindow?: boolean;
}

const SubregionForecastContainer: React.FC<Props> = ({ children, isChartsPage }) => {
  const router = useRouter();
  const { subregionId } = router.query as {
    subregionId: string;
    subregionSlug: string;
  };

  const dispatch = useAppDispatch();

  const subregionAssociated = useAppSelector(getSubregionAssociated);
  const userDetails = useAppSelector(getUserDetails);
  const overviewData = useAppSelector(getSubregionOverviewData);
  const userSettings = useAppSelector(getUserSettings);
  const nearbySubregions = useAppSelector(getNearbySubregions) as NearbySubregions;
  const feed = useAppSelector(getSubregionFeed) as SubregionFeed;
  const swellEvents = useAppSelector(getSwellEvents) as SwellEvents;

  useEffect(
    () => {
      const handleLeavingOrChangingSubregions = (url: string) => {
        if (url.indexOf(`/${subregionId}`) !== -1) {
          // We don't want to set a loading state since it's staying in the same subregion
          return null;
        }

        return dispatch(setSubregionLoading(true));
      };

      router.events.on('routeChangeStart', handleLeavingOrChangingSubregions);
      return () => {
        router.events.off('routeChangeStart', handleLeavingOrChangingSubregions);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useMount(() => {
    if (userDetails) {
      dispatch(setRecentlyVisited('subregions', subregionId, overviewData.name));
    }
  });

  useUnmount(() => {
    dispatch(clearOverviewData());
  });

  const pageLevelLinks = useMemo(() => {
    const subregionName = overviewData.name || '';
    const chartsUrl = subregionAssociated ? subregionAssociated.chartsUrl : null;
    const pageLinks: PageLevelLink[] = [
      {
        text: en.RegionalForecast.pageLevelLinkText.forecast.text,
        title: en.RegionalForecast.pageLevelLinkText.forecast.title(subregionName),
        path: en.RegionalForecast.pageLevelLinkText.forecast.path(
          slugify(subregionName),
          subregionId,
        ),
        isIndex: true,
      },
    ];
    if (feed.articles.length > 0) {
      pageLinks.push({
        text: en.RegionalForecast.pageLevelLinkText.premiumAnalysis.text,
        title: en.RegionalForecast.pageLevelLinkText.premiumAnalysis.title(subregionName),
        path: en.RegionalForecast.pageLevelLinkText.premiumAnalysis.path(
          slugify(subregionName),
          subregionId,
        ),
        isIndex: false,
      });
    }
    if (chartsUrl) {
      pageLinks.push({
        text: en.RegionalForecast.externalLinkText.charts,
        path: chartsUrl,
        newWindow: false,
      });
    }

    return pageLinks;
  }, [overviewData.name, subregionAssociated, feed.articles.length, subregionId]);

  const externalLinks = useMemo(() => {
    const buoysLink = overviewData.breadcrumb?.length
      ? overviewData.breadcrumb[overviewData.breadcrumb.length - 1].href.replace(
          'surf-reports-forecasts-cams/',
          'surf-reports-forecasts-cams/buoys/',
        )
      : null;
    const links = [];

    if (buoysLink) {
      links.push({
        text: en.RegionalForecast.externalLinkText.buoys,
        path: buoysLink,
        newWindow: false,
      });
    }

    return links;
  }, [overviewData]);

  const onClickCard = useCallback(
    (swellEvent) => {
      trackEvent('Clicked Swell Alert Card', {
        title: swellEvent.name,
        contentType: 'Swell Alert',
        basins: swellEvent.basins.join(','),
        locationCategory: 'Region Forecast Page - Top',
        destinationUrl: swellEvent.permalink,
        subregionId,
        subregionName: overviewData.name,
      });
    },
    [overviewData, subregionId],
  );

  return (
    <div className="sl-subregion-forecast-page">
      <div>
        <ContentContainer>
          {swellEvents.length ? (
            <SwellEventCardContainer
              onClickCard={(swellEvent) => onClickCard(swellEvent)}
              events={swellEvents}
            />
          ) : null}
          <ForecastHeader
            nearbySubregions={nearbySubregions}
            pathName={router.asPath}
            isMultiPage={isChartsPage}
            breadcrumb={overviewData.breadcrumb}
            pageTitle={`${overviewData.name} ${en.RegionalForecast.header.titleSuffix}`}
            nearbySpots={overviewData.spots}
            externalLinks1={externalLinks}
            pageLevelLinks={pageLevelLinks}
            userSettings={userSettings}
          />
        </ContentContainer>
        {children}
      </div>
    </div>
  );
};

const SubregionForecastContainerWithLoading: React.FC<Props> = ({ children, isChartsPage }) => {
  const router = useRouter();
  const { subregionId } = router.query as { subregionId: string };

  const loading = useAppSelector(getSubregionLoading);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setSubregionLoading(false));
  }, [subregionId, dispatch]);

  if (loading) {
    return <PageLoading />;
  }

  return (
    <SubregionForecastContainer isChartsPage={isChartsPage}>{children}</SubregionForecastContainer>
  );
};

export default SubregionForecastContainerWithLoading;
