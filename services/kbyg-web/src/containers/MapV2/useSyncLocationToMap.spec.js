import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';

import useSyncLocationToMap from './useSyncLocationToMap';
import * as useMapLocation from '../../hooks/map/useMapLocation';
import * as useMap from '../../hooks/map/useMap';

describe('hooks / map / useSyncLocationToMap', () => {
  /** @type {sinon.SinonStub} */
  let useMapLocationStub;
  /** @type {sinon.SinonStub} */
  let useMapStub;

  const setViewStub = sinon.stub();
  const mapStub = { setView: setViewStub };
  const location = { center: { lat: 1, lon: 1 }, zoom: 12 };

  beforeEach(() => {
    useMapLocationStub = sinon.stub(useMapLocation, 'default');
    useMapStub = sinon.stub(useMap, 'default').returns([mapStub]);
  });

  afterEach(() => {
    useMapLocationStub.restore();
    useMapStub.restore();
    setViewStub.reset();
  });

  const Comp = () => {
    useSyncLocationToMap();

    return <div />;
  };

  it('should not do anything if the map does not exist', () => {
    useMapLocationStub.returns({ location, locationLoaded: true });
    useMapStub.returns([]);

    mount(<Comp />);

    expect(setViewStub).to.not.have.been.called();
  });

  it('should not do anything if the map location does not exist', () => {
    useMapLocationStub.returns({ location: null, locationLoaded: true });
    mount(<Comp />);

    expect(setViewStub).to.not.have.been.called();
  });

  it('should not do anything if the map location is not loaded', () => {
    useMapLocationStub.returns({ location, locationLoaded: false });
    mount(<Comp />);

    expect(setViewStub).to.not.have.been.called();
  });

  it('should sync the location when the location changes', () => {
    useMapLocationStub.returns({ location, locationLoaded: true });

    const wrapper = mount(<Comp />);

    expect(setViewStub).to.have.been.calledOnceWithExactly(location.center, location.zoom);

    const locationTwo = { center: { lat: 2, lon: 2 }, zoom: 10 };
    useMapLocationStub.returns({ location: locationTwo, locationLoaded: true });

    setViewStub.resetHistory();

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(setViewStub).to.have.been.calledOnceWithExactly(locationTwo.center, locationTwo.zoom);
  });
});
