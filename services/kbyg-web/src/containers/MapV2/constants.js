import config from '../../config';

export const ATTRIBUTION =
  '<a href="https://www.maptiler.com/copyright" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap</a> contributors';

export const MAP_TILE_URL = `${config.mapTileUrl}/256/{z}/{x}/{y}@2x.png?key=${config.mapTileKey}`;
export const SATELLITE_MAP_TILE_URL = config.mapTileSatUrl;
