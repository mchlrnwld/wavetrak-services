import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import * as mapActions from '../../actions/mapV2';
import * as mapUtils from './map';
import * as useMap from '../../hooks/map/useMap';
import * as useMapLocation from '../../hooks/map/useMapLocation';
import useSyncMapLocationToStore from './useSyncMapLocationToStore';
import { mountWithReduxAndRouter, wait } from '../../utils/test-utils';

describe('containers / routes / MapV2 / useSyncMapLocation', () => {
  /** @type {sinon.SinonStub} */
  let setMapLocationStub;

  /** @type {sinon.SinonStub} */
  let useMapStub;

  /** @type {sinon.SinonStub} */
  let useMapLocationStub;

  /** @type {sinon.SinonStub} */
  let getLocationFromMapStub;

  const currentLocation = {
    center: { lat: 1, lon: 1 },
    zoom: 1,
  };

  const newLocation = {
    center: { lat: 0, lon: 0 },
    zoom: 0,
  };

  let registeredHandlers = {};
  const map = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  beforeEach(() => {
    setMapLocationStub = sinon.stub(mapActions, 'setMapLocation').returns({ type: 'ACTION' });
    getLocationFromMapStub = sinon.stub(mapUtils, 'getLocationFromMap').returns(newLocation);
    useMapStub = sinon.stub(useMap, 'default').returns([map]);
    useMapLocationStub = sinon
      .stub(useMapLocation, 'default')
      .returns({ location: currentLocation });
  });

  afterEach(() => {
    setMapLocationStub.restore();
    getLocationFromMapStub.restore();
    useMapStub.restore();
    useMapLocationStub.restore();
    registeredHandlers = {};
  });

  const Comp = () => {
    useSyncMapLocationToStore();

    return <div />;
  };

  it('should not add event listeners if the map does not exist', () => {
    useMapStub.returns([]);
    mountWithReduxAndRouter(Comp);

    expect(registeredHandlers).to.not.have.ownProperty('moveend');
  });

  it('should sync the location of the map with a debounce', async () => {
    mountWithReduxAndRouter(Comp);

    expect(registeredHandlers.moveend).to.have.length(1);

    registeredHandlers.moveend[0]();
    registeredHandlers.moveend[0]();
    registeredHandlers.moveend[0]();

    await wait(770);

    expect(setMapLocationStub).to.have.been.calledOnceWithExactly(newLocation);
  });

  it('should not sync the location if it has not changed', async () => {
    useMapLocationStub.returns({ location: newLocation });
    mountWithReduxAndRouter(Comp);

    expect(registeredHandlers.moveend).to.have.length(1);

    registeredHandlers.moveend[0]();

    await wait(770);

    expect(setMapLocationStub).to.have.been.not.been.called();
  });
});
