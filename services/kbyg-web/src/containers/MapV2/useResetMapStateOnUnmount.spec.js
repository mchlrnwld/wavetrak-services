import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import useResetMapStateOnUnmount from './useResetMapStateOnUnmount';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import * as mapActions from '../../actions/mapV2';

describe('containers / routes / MapV2 / useResetMapStateOnUnmount', () => {
  /** @type {sinon.SinonStub} */
  let resetMapStateStub;

  const Comp = () => {
    useResetMapStateOnUnmount();

    return <div />;
  };

  beforeEach(() => {
    resetMapStateStub = sinon.stub(mapActions, 'resetMapState').returns({ type: 'TEST_ACTION' });
  });

  afterEach(() => {
    resetMapStateStub.restore();
  });

  it('should reset the map state on unmount', () => {
    const { wrapper } = mountWithReduxAndRouter(Comp);

    wrapper.unmount();

    expect(resetMapStateStub).to.have.been.calledOnce();
  });
});
