import { useDispatch } from 'react-redux';
import { useUnmountEffect } from '@react-hookz/web';
import { resetMapState } from '../../actions/mapV2';

const useResetMapStateOnUnmount = () => {
  const dispatch = useDispatch();

  useUnmountEffect(() => {
    dispatch(resetMapState());
  });
};

export default useResetMapStateOnUnmount;
