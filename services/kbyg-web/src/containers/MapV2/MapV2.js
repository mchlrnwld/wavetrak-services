import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Map, PageRail, TileLayer } from '@surfline/quiver-react';

import MapPlaceholder from '../../components/MapPlaceholder';
import MapNotification from '../../components/MapNotification';
import mapOptions from '../../utils/mapOptions';
import cloneChildrenWithProps from '../../utils/cloneChildrenWithProps';
import useMap from '../../hooks/map/useMap';
import useMapLocation from '../../hooks/map/useMapLocation';
import useSetupMapPage from '../../hooks/map/useSetupMapPage';
import useSyncMapLocationToStore from './useSyncMapLocationToStore';
import useSyncMapBoundingBoxToStore from './useSyncMapBoundingBoxToStore';
import useResetMapStateOnUnmount from './useResetMapStateOnUnmount';

import { useMapError, useMapLoading } from '../../selectors/mapV2';
import { ATTRIBUTION, MAP_TILE_URL } from './constants';

import useSyncLocationToMap from './useSyncLocationToMap';
import MapPageMeta from '../../components/MapPageMeta/MapPageMeta';

import styles from './MapV2.module.scss';

/**
 * @param {boolean} loading
 * @param {boolean} error
 */
const mapClassName = (loading, error) =>
  // TODO: Make V1 base class once we have migrated all map pages
  classNames('sl-map-page', 'sl-map-page-v2', styles.slMapPageV2, {
    [styles.slMapPageV2Loading]: loading,
    [styles.slMapPageV2Error]: error,
  });

const getLeftRailClass = () => classNames('sl-left-rail', styles.slMapV2LeftRail);

const MapV2 = ({ children }) => {
  const [map, setMap] = useMap();
  const loading = useMapLoading();
  const error = useMapError();

  useSetupMapPage();
  useResetMapStateOnUnmount();

  // Order matters here, we want to make sure we sync the location to the map first since the other hooks depend
  // the location of the map
  useSyncLocationToMap();
  useSyncMapBoundingBoxToStore();
  useSyncMapLocationToStore();
  const { location } = useMapLocation();

  const childrenWithMap = cloneChildrenWithProps(children, { map });

  return (
    <div className={mapClassName(loading, error)}>
      {location && <MapPageMeta buoyMap location={location} />}
      <PageRail side="right" className={styles.slMapV2RightRail}>
        <MapPlaceholder show={!map} />
        <Map
          center={location.center}
          zoom={location.zoom}
          mapProps={{
            ...mapOptions,
            placeholder: <MapPlaceholder show />,
            whenCreated: (m) => {
              m.zoomControl.setPosition('topright');
              setMap(m);
            },
            preferCanvas: false,
          }}
        >
          <TileLayer attribution={ATTRIBUTION} url={MAP_TILE_URL} />
        </Map>
        <MapNotification hide={!map} />
      </PageRail>
      <PageRail side="left" className={getLeftRailClass()}>
        {childrenWithMap}
      </PageRail>
    </div>
  );
};

MapV2.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MapV2;
