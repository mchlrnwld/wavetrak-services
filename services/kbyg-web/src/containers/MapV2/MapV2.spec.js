import React from 'react';
import { act } from 'react-dom/test-utils';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import MapV2 from './MapV2';
import MapPlaceholder from '../../components/MapPlaceholder';
import MapNotification from '../../components/MapNotification';
import mapOptions from '../../utils/mapOptions';
import { mountWithReduxAndRouter, wait } from '../../utils/test-utils';
import { ATTRIBUTION, MAP_TILE_URL } from './constants';
import * as mapSelectors from '../../selectors/mapV2';
import * as useMap from '../../hooks/map/useMap';
import * as useMapLocation from '../../hooks/map/useMapLocation';
import * as useSyncMapLocationToStore from './useSyncMapLocationToStore';
import * as useSetupMapPage from '../../hooks/map/useSetupMapPage';
import * as useSyncMapBoundingBoxToStore from './useSyncMapBoundingBoxToStore';
import * as useSyncLocationToMap from './useSyncLocationToMap';
import * as useResetMapStateOnUnmount from './useResetMapStateOnUnmount';

describe('containers / routes / MapV2', () => {
  /** @type {sinon.SinonStub} */
  let useMapLocationStub;

  /** @type {sinon.SinonStub} */
  let useSyncMapLocationToStoreStub;

  /** @type {sinon.SinonStub} */
  let useSetupMapPageStub;

  /** @type {sinon.SinonStub} */
  let useMapStub;

  /** @type {sinon.SinonStub} */
  let useSyncMapBoundingBoxToStoreStub;

  /** @type {sinon.SinonStub} */
  let useMapLoadingStub;

  /** @type {sinon.SinonStub} */
  let useMapErrorStub;

  /** @type {sinon.SinonStub} */
  let useSyncLocationToMapStub;

  /** @type {sinon.SinonStub} */
  let useResetMapStateOnUnmountStub;

  const location = {
    center: { lat: 1, lon: 1 },
    zoom: 9,
  };

  const setMapStub = sinon.stub();

  beforeEach(() => {
    useMapLocationStub = sinon.stub(useMapLocation, 'default').returns({ location });
    useSyncMapLocationToStoreStub = sinon.stub(useSyncMapLocationToStore, 'default');
    useSyncLocationToMapStub = sinon.stub(useSyncLocationToMap, 'default');
    useSetupMapPageStub = sinon.stub(useSetupMapPage, 'default');
    useSyncMapBoundingBoxToStoreStub = sinon.stub(useSyncMapBoundingBoxToStore, 'default');
    useMapStub = sinon.stub(useMap, 'default').returns([undefined, setMapStub]);
    useMapLoadingStub = sinon.stub(mapSelectors, 'useMapLoading');
    useMapErrorStub = sinon.stub(mapSelectors, 'useMapError');
    useResetMapStateOnUnmountStub = sinon.stub(useResetMapStateOnUnmount, 'default');
  });

  afterEach(() => {
    useMapLocationStub.restore();
    useSyncMapLocationToStoreStub.restore();
    useSetupMapPageStub.restore();
    useMapStub.restore();
    useSyncMapBoundingBoxToStoreStub.restore();
    useSyncLocationToMapStub.restore();
    setMapStub.reset();
    useMapErrorStub.restore();
    useMapLoadingStub.restore();
    useResetMapStateOnUnmountStub.restore();
  });

  it('should render the MapPlaceholder if the map is not set', () => {
    const wrapper = shallow(
      <MapV2>
        <div className="child" />
      </MapV2>,
    );

    const placeholder = wrapper.find(MapPlaceholder);

    expect(placeholder).to.have.length(1);
    expect(placeholder.prop('show')).to.be.true();
  });

  it('should call the hooks', () => {
    shallow(
      <MapV2>
        <div className="child" />
      </MapV2>,
    );

    expect(useSetupMapPageStub).to.have.been.calledOnce();
    expect(useResetMapStateOnUnmountStub).to.have.been.calledOnce();

    // These hooks should be called in order
    expect(useSyncLocationToMapStub).to.have.been.calledBefore(useSyncMapBoundingBoxToStoreStub);
    expect(useSyncMapBoundingBoxToStoreStub).to.have.been.calledBefore(
      useSyncMapLocationToStoreStub,
    );
    expect(useSyncMapLocationToStoreStub).to.have.been.calledBefore(useMapLocationStub);
  });

  it('should render the children with a map prop', () => {
    const map = {};
    useMapStub.returns([map, setMapStub]);

    const wrapper = shallow(
      <MapV2>
        <div className="child" />
      </MapV2>,
    );

    const child = wrapper.find('.child');

    expect(child).to.have.length(1);
    expect(child.prop('map')).to.be.equal(map);
  });

  it('should render the MapNotification hidden if the map is not set', () => {
    const wrapper = shallow(
      <MapV2>
        <div className="child" />
      </MapV2>,
    );

    const notification = wrapper.find(MapNotification);

    expect(notification).to.have.length(1);
    expect(notification.prop('hide')).to.be.true();
  });

  it('should set the error className', () => {
    useMapErrorStub.returns({ message: 'message', status: 500 });
    const wrapper = shallow(
      <MapV2>
        <div className="child" />
      </MapV2>,
    );

    const error = wrapper.find('.slMapPageV2Error');

    expect(error).to.have.length(1);
  });

  it('should set the loading className', () => {
    useMapLoadingStub.returns(true);
    const wrapper = shallow(
      <MapV2>
        <div className="child" />
      </MapV2>,
    );

    const loading = wrapper.find('.slMapPageV2Loading');

    expect(loading).to.have.length(1);
  });

  it('should render the Map and TileLayer', async () => {
    const map = {};
    useMapStub.returns([map, setMapStub]);

    const { wrapper } = mountWithReduxAndRouter(MapV2);

    await act(async () => {
      await wait(150);
      wrapper.setProps({ update: 1 });
      wrapper.update();
    });

    const Map = wrapper.find('Map');
    const TileLayer = wrapper.find('TileLayer');
    const placeholder = wrapper.find(MapPlaceholder);
    const notification = wrapper.find(MapNotification);

    expect(Map).to.have.length(1);
    expect(Map.prop('center')).to.deep.equal({ ...location.center });
    expect(Map.prop('zoom')).to.equal(location.zoom);

    expect(Map.prop('mapProps').placeholder).to.be.an('object');
    expect(Map.prop('mapProps').minZoom).to.equal(mapOptions.minZoom);
    expect(Map.prop('mapProps').maxZoom).to.equal(mapOptions.maxZoom);
    expect(Map.prop('mapProps').preferCanvas).to.equal(false);
    expect(Map.prop('mapProps').worldCopyJump).to.equal(mapOptions.worldCopyJump);

    expect(TileLayer).to.have.length(1);
    expect(TileLayer.props()).to.deep.equal({
      attribution: ATTRIBUTION,
      url: MAP_TILE_URL,
    });

    expect(placeholder).to.have.length(1);
    expect(placeholder.prop('show')).to.be.false();
    expect(notification).to.have.length(1);
    expect(notification.prop('hide')).to.be.false();

    expect(setMapStub).to.have.been.called();
  });
});
