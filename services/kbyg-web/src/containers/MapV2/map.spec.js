import { expect } from 'chai';
import { getBoundingBoxFromMap, getLocationFromMap } from './map';

describe('containers / routes / MapV2 / map', () => {
  describe('getLocationFromMap', () => {
    const center = { lat: 1, lon: 1 };
    const zoom = 10;

    const map = { getCenter: () => ({ lat: 1, lng: 1 }), getZoom: () => 10 };
    it('should get the location from the map', () => {
      expect(getLocationFromMap(map)).to.deep.equal({ center, zoom });
    });
  });

  describe('getBoundingBoxFromMap', () => {
    const boundingBox = { north: 2, south: -2, east: 3, west: -3 };

    const map = {
      getBounds: () => ({
        getNorth: () => 2,
        getSouth: () => -2,
        getEast: () => 3,
        getWest: () => -3,
      }),
    };
    it('should get the location from the map', () => {
      expect(getBoundingBoxFromMap(map)).to.deep.equal(boundingBox);
    });
  });
});
