import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';

import useSyncMapBoundingBoxToStore from './useSyncMapBoundingBoxToStore';
import * as mapActions from '../../actions/mapV2';
import * as mapUtils from './map';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import * as useMapLocation from '../../hooks/map/useMapLocation';
import * as useMap from '../../hooks/map/useMap';

describe('containers / routes / MapV2 / useSyncMapBoundingBoxToStore', () => {
  /** @type {sinon.SinonStub} */
  let getBoundingBoxFromMapStub;
  /** @type {sinon.SinonStub} */
  let useMapLocationStub;
  /** @type {sinon.SinonStub} */
  let useMapStub;
  /** @type {sinon.SinonSpy} */
  let setMapBoundingBoxStub;

  const map = {};

  beforeEach(() => {
    setMapBoundingBoxStub = sinon.spy(mapActions, 'setMapBoundingBox');
    getBoundingBoxFromMapStub = sinon.stub(mapUtils, 'getBoundingBoxFromMap');
    useMapLocationStub = sinon.stub(useMapLocation, 'default');
    useMapStub = sinon.stub(useMap, 'default').returns([map]);
  });
  afterEach(() => {
    setMapBoundingBoxStub.restore();
    getBoundingBoxFromMapStub.restore();
    useMapLocationStub.restore();
    useMapStub.restore();
  });

  const Comp = () => {
    useSyncMapBoundingBoxToStore();

    return <div />;
  };

  it('should not sync the bounding box until the location is loaded', () => {
    useMapLocationStub.returns({
      location: { center: { lat: 1, lon: 1 }, zoom: 12 },
      locationLoaded: false,
    });

    mountWithReduxAndRouter(Comp, {});

    expect(getBoundingBoxFromMapStub).to.not.have.been.called();
    expect(setMapBoundingBoxStub).to.not.have.been.called();
  });

  it('should not sync bounding box if the location is loaded', () => {
    const boundingBox = { north: 1, south: -1, east: 1, west: -1 };
    useMapLocationStub.returns({
      location: { center: { lat: 1, lon: 1 }, zoom: 12 },
      locationLoaded: true,
    });
    getBoundingBoxFromMapStub.returns(boundingBox);

    mountWithReduxAndRouter(Comp, {});

    expect(getBoundingBoxFromMapStub).to.have.been.calledOnceWithExactly(map);
    expect(setMapBoundingBoxStub).to.have.been.calledOnceWithExactly(boundingBox);
  });
});
