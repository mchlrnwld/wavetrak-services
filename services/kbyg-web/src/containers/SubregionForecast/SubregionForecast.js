import React from 'react';
import { useDeviceSize } from '@surfline/quiver-react';
import {
  getUserPremiumStatus,
  getUserSettings,
  trackNavigatedToPage,
  getUserDetails,
} from '@surfline/web-common';
import { useSelector } from 'react-redux';
import { useMount } from 'react-use';
import useMetering from '../../common/hooks/useMetering';
import RegionalForecast from '../../components/RegionalForecast';
import RegionalForecastMeta from '../../components/RegionalForecastMeta';
import { getChartDaySummary } from '../../selectors/graphs';
import {
  getPrimarySpotId,
  getSubregionAssociated,
  getSubregionOverviewData,
} from '../../selectors/subregion';
import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { useSplitTreatment } from '../../selectors/split';

const SubregionForecast = () => {
  const overviewData = useSelector(getSubregionOverviewData);
  const primarySpotId = useSelector(getPrimarySpotId);
  const userSettings = useSelector(getUserSettings);
  const userDetails = useSelector(getUserDetails);
  const summary = useSelector(getChartDaySummary);
  const isPremium = useSelector(getUserPremiumStatus);
  const subregionAssociated = useSelector(getSubregionAssociated);
  const conditionsTreatment = useSplitTreatment(SL_WEB_CONDITION_VARIATION);

  const meter = useMetering();
  const device = useDeviceSize();

  useMount(() => {
    const { name, _id, forecastSummary } = overviewData;
    trackNavigatedToPage('Regional Forecast', {
      category: 'forecast',
      channel: 'forecast',
      subregionId: _id,
      subregionName: name,
      human:
        forecastSummary?.forecastStatus?.status === 'active' &&
        (forecastSummary?.hasHighlights || forecastSummary?.hasBets),
      viewedLocation: 'regional',
      meteringEnabled: meter.meteringEnabled,
    });
  });

  return (
    <div>
      {overviewData ? (
        <div>
          <RegionalForecastMeta subregionId={overviewData._id} subregionName={overviewData.name} />
          <RegionalForecast
            userSettings={userSettings}
            device={device}
            overviewData={overviewData}
            primarySpotId={primarySpotId}
            isPremium={isPremium}
            summary={summary}
            utcOffset={subregionAssociated.utcOffset}
            abbrTimezone={subregionAssociated.abbrTimezone}
            userDetails={userDetails}
            conditionsTreatment={conditionsTreatment}
          />
        </div>
      ) : null}
    </div>
  );
};

export default SubregionForecast;
