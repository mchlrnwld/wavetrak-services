import { expect } from 'chai';

import sinon from 'sinon';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import BuoyGeonameMap from './BuoyGeonameMap';
import GeonameMapLinks from '../../components/GeonameMapLinks';
import MapBuoys from '../../components/MapBuoys';
import GeonameMapHeader from '../../components/GeonameMapHeader';

import * as useSyncMapLocationToSession from '../../hooks/map/useSyncMapLocationToSession';
import * as useHydrateGeonameMapLocation from '../../hooks/map/useHydrateGeonameMapLocation';
import * as useFetchLocationViewOnChange from './useFetchLocationViewOnChange';
import * as mapActions from '../../actions/mapV2';
// import { getBuoyGeonameMapPath } from '../../utils/urls/mapPaths';

describe('containers / BuoyGeonameMap', () => {
  let fetchLocationViewStub: sinon.SinonStub;

  let useSyncMapLocationToSessionStub: sinon.SinonStub;

  let useHydrateGeonameMapLocationStub: sinon.SinonStub;

  let useFetchLocationViewOnChangeStub: sinon.SinonStub;

  const router = {
    pathname: '[...geonames]',
    asPath: '/united-states/california/santa-cruz/1234',
    query: { geonames: ['united-states', 'california', 'santa-cruz', '1234'] },
  };

  beforeEach(() => {
    fetchLocationViewStub = sinon.stub(mapActions, 'fetchLocationView');
    useSyncMapLocationToSessionStub = sinon.stub(useSyncMapLocationToSession, 'default');
    useHydrateGeonameMapLocationStub = sinon.stub(useHydrateGeonameMapLocation, 'default');
    useFetchLocationViewOnChangeStub = sinon.stub(useFetchLocationViewOnChange, 'default');
  });

  afterEach(() => {
    fetchLocationViewStub.restore();
    useSyncMapLocationToSessionStub.restore();
    useHydrateGeonameMapLocationStub.restore();
    useFetchLocationViewOnChangeStub.restore();
  });

  it('should sync the map location to the session', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyGeonameMap, {}, { router });
    try {
      expect(useSyncMapLocationToSessionStub).to.have.been.calledOnce();
    } finally {
      wrapper.unmount();
    }
  });

  it('should call fetch location view on change hook', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyGeonameMap, {}, { router });

    try {
      expect(useFetchLocationViewOnChangeStub).to.have.been.calledOnce();
    } finally {
      wrapper.unmount();
    }
  });

  it('should hydrate using the geoname map location', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyGeonameMap, {}, { router });

    try {
      expect(useHydrateGeonameMapLocationStub).to.have.been.calledOnce();
    } finally {
      wrapper.unmount();
    }
  });

  it('should render the GeonameMapLinks', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyGeonameMap, {}, { router });

    try {
      expect(wrapper.find(GeonameMapLinks)).to.have.length(1);
    } finally {
      wrapper.unmount();
    }
  });

  it('should render the MapBuoyCards', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyGeonameMap, {}, { router });

    try {
      expect(wrapper.find(MapBuoys)).to.have.length(1);
    } finally {
      wrapper.unmount();
    }
  });

  it('should render the GeonameMapHeader', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyGeonameMap, {}, { router });

    try {
      expect(wrapper.find(GeonameMapHeader)).to.have.length(1);
    } finally {
      wrapper.unmount();
    }
  });
});
