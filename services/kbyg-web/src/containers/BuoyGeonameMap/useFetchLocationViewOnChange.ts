import { useEffect } from 'react';

import { fetchLocationView } from '../../actions/mapV2';
import useBreadcrumbsAndGeonameId from '../../hooks/useBreadcrumbsAndGeonameId';
import { getLocationViewGeonameId } from '../../selectors/mapV2';
import { useAppDispatch, useAppSelector } from '../../stores/hooks';

const useFetchLocationViewOnChange = () => {
  const dispatch = useAppDispatch();
  const { geonameId } = useBreadcrumbsAndGeonameId();
  const currentGeonameId = useAppSelector(getLocationViewGeonameId);

  useEffect(() => {
    if (geonameId && currentGeonameId?.toString() !== geonameId) {
      dispatch(fetchLocationView(geonameId));
    }
  }, [geonameId, currentGeonameId, dispatch]);
};

export default useFetchLocationViewOnChange;
