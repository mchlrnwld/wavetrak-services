import React from 'react';

import { expect } from 'chai';
import sinon, { SinonStub } from 'sinon';

import useFetchLocationViewOnChange from './useFetchLocationViewOnChange';
import * as mapSelectors from '../../selectors/mapV2';
import * as mapActions from '../../actions/mapV2';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('containers / BuoyGeonameMap / useFetchLocationViewOnChange', () => {
  let getLocationViewGeonameIdStub: SinonStub;
  let fetchLocationViewStub: SinonStub;

  beforeEach(() => {
    getLocationViewGeonameIdStub = sinon.stub(mapSelectors, 'getLocationViewGeonameId');
    fetchLocationViewStub = sinon.stub(mapActions, 'fetchLocationView');
  });

  afterEach(() => {
    getLocationViewGeonameIdStub.restore();
    fetchLocationViewStub.restore();
  });

  const Comp = () => {
    useFetchLocationViewOnChange();

    return <div>Content</div>;
  };

  it('should fetch the location view if the geonameId has changed', () => {
    getLocationViewGeonameIdStub.returns(1234);
    fetchLocationViewStub.returns(() => ({ type: 'DUMMY_ACTION' }));

    const { wrapper } = mountWithReduxAndRouter(
      Comp,
      {},
      { router: { query: { geonames: ['test', '1234'] } } },
    );

    expect(fetchLocationViewStub).to.not.have.been.called();

    getLocationViewGeonameIdStub.returns(3456);
    fetchLocationViewStub.resetHistory();

    wrapper.setProps({ update: 1 });
    wrapper.update();

    expect(fetchLocationViewStub).to.not.have.been.calledOnceWithExactly(3456);
  });
});
