import React from 'react';

import GeonameMapLinks from '../../components/GeonameMapLinks';
import MapBuoys from '../../components/MapBuoys';
import GeonameMapHeader from '../../components/GeonameMapHeader';

import useSyncMapLocationToSession from '../../hooks/map/useSyncMapLocationToSession';
import useHydrateGeonameMapLocation from '../../hooks/map/useHydrateGeonameMapLocation';
import useFetchLocationViewOnChange from './useFetchLocationViewOnChange';
import { useLocationView } from '../../selectors/mapV2';

import styles from './BuoyGeonameMap.module.scss';

const BuoyGeonameMap: React.FC = () => {
  useFetchLocationViewOnChange();
  useHydrateGeonameMapLocation();
  useSyncMapLocationToSession();

  // TODO: Move this into `GeonameMapHeader` once the old map pages are migrated over
  const locationView = useLocationView();

  return (
    <div className={styles.buoyGeonameMap}>
      <GeonameMapLinks />
      <GeonameMapHeader
        locationView={locationView}
        isBuoys
        classes={{ root: styles.mapHeader, content: styles.mapHeaderContent }}
      />
      <MapBuoys />
    </div>
  );
};

BuoyGeonameMap.propTypes = {};

export default BuoyGeonameMap;
