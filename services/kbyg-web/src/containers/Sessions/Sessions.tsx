import React, { useEffect, useState } from 'react';
import { useIsomorphicLayoutEffect } from 'react-use';
import { useRouter } from 'next/router';
import useSWR from 'swr';
import { PageRail } from '@surfline/quiver-react';

import { getLocalDate, getWindow } from '@surfline/web-common';
import { fetchSession } from '../../common/api/sessions';
import PageLoading from '../../components/PageLoading';
import SessionClipPlayer from '../../components/SessionClipPlayer';
import SessionDetails from '../../components/SessionDetails';
import SessionDetailsBanner from '../../components/SessionDetailsBanner';
import SessionWaves from '../../components/SessionWaves';
import { SessionWave } from '../../types/sessions';

import styles from './Sessions.module.scss';

const Sessions = () => {
  useIsomorphicLayoutEffect(() => {
    const win = getWindow();
    win.document.documentElement.style.overflow = 'hidden';
    return () => {
      // Remove the overflow style when leaving the page via client side navigation
      win.document.documentElement.style.overflow = '';
    };
  }, []);

  const router = useRouter();
  const { sessionId } = router.query;
  const { data: session, error } = useSWR(sessionId, fetchSession, {
    revalidateOnFocus: false,
    shouldRetryOnError: false,
  });

  const [activeWave, setActiveWave] = useState<SessionWave>();
  const [activeClipIndex, setActiveClipIndex] = useState(0);

  useEffect(() => {
    if (session) {
      setActiveWave(session.waves[0]);
    }
  }, [session]);

  if (error) {
    // Error will be caught by the error boundary
    throw error;
  }

  if (!session) {
    return <PageLoading />;
  }

  const clip = activeWave?.clips?.[activeClipIndex];
  const camera = clip ? session.cams.find((cam) => cam.id === clip.cameraId) : null;
  const {
    associated: { timezone },
  } = session;
  const timezoneId = session?.timezone?.id;
  const currentTimezone = timezone.filter((tz) => tz.id === timezoneId);
  const utcOffset = currentTimezone[0]?.offset;
  const timezoneAbbr = currentTimezone[0]?.abbr;

  return (
    <div className={styles.slSessionsPage}>
      <PageRail side="left" className={styles.quiverPageRailLeft}>
        <SessionClipPlayer
          key={clip?.clipId || 'sl-clip-player'}
          session={session}
          clip={clip}
          camera={camera}
        />
        <SessionDetails
          session={session}
          setActiveClipIndex={setActiveClipIndex}
          activeWave={activeWave}
          activeClipIndex={activeClipIndex}
          utcOffset={utcOffset}
          timezoneAbbr={timezoneAbbr}
        />
      </PageRail>
      <PageRail side="right">
        <div className={styles.slSessionsPageRightRail}>
          <div className={styles.slSessionsPageUser}>
            <h1 className={styles.slSessionsPageUserName}>
              {session.user?.firstName} {session.user?.lastName || ''}
            </h1>
          </div>
          <SessionDetailsBanner
            sessionName={session.name}
            sessionRating={session.rating}
            sessionStartTime={getLocalDate(session.startTimestamp / 1000, utcOffset)}
            sessionSpot={session.spot}
            sessionConditions={session.conditions}
            timezoneAbbr={timezoneAbbr}
          />
          <SessionWaves
            setActiveClipIndex={setActiveClipIndex}
            waves={session.waves}
            units={session.associated.units}
            setActiveWave={setActiveWave}
            activeWave={activeWave}
            utcOffset={utcOffset}
          />
        </div>
      </PageRail>
    </div>
  );
};

export default Sessions;
