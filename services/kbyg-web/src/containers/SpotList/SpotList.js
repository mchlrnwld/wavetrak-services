import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { getUserPremiumStatus, getEntitlements } from '@surfline/web-common';
import {
  setActiveSpot,
  clearActiveSpot,
  setSpotListScrollPosition,
} from '../../actions/animations';
import mapPropType from '../../propTypes/map';
import unitsPropType from '../../propTypes/units';
import animationsPropType from '../../propTypes/animations';
import scrollPositionPropType from '../../propTypes/animations/scrollPosition';
import SpotList from '../../components/SpotList';
import MotionScrollWrapper from '../../components/MotionScrollWrapper';
import SubHeader from '../../components/SubHeader';
import { getSpotListScrollPosition } from '../../selectors/animations';
import en from '../../intl/translations/en';
import locationViewPropShape from '../../propTypes/locationView';
import withMetering from '../../common/withMetering';

const segmentProperties = {
  location: 'bottom',
  pageName: 'map',
};

const SpotListContainer = ({
  map,
  units,
  animations,
  spotListScrollPosition,
  doSetActiveSpot,
  doClearActiveSpot,
  doSetScrollPosition,
  isPremium,
  entitlements,
  locationView,
}) => {
  let message;
  if (map.status.loading && !map.spots.length) {
    message = en.spotListHeader.loading;
  } else if (map.clusters.length > 0) {
    message = en.spotListHeader.topSpots;
  } else if (map.spots.length === 0) {
    message = en.spotListHeader.noSpots;
  }

  return (
    <div>
      <MotionScrollWrapper scrollPosition={spotListScrollPosition}>
        <SpotList
          locationView={locationView}
          spots={map.spots}
          subregions={map.subregions}
          units={units}
          animations={animations}
          doSetActiveSpot={doSetActiveSpot}
          doSetScrollPosition={doSetScrollPosition}
          doClearActiveSpot={doClearActiveSpot}
          message={message}
          entitlements={entitlements}
          isPremium={isPremium}
        />
      </MotionScrollWrapper>
      {!isPremium ? <SubHeader eventProperties={segmentProperties} /> : null}
    </div>
  );
};

SpotListContainer.propTypes = {
  map: mapPropType.isRequired,
  units: unitsPropType.isRequired,
  animations: animationsPropType.isRequired,
  spotListScrollPosition: scrollPositionPropType.isRequired,
  doSetActiveSpot: PropTypes.func.isRequired,
  doClearActiveSpot: PropTypes.func.isRequired,
  doSetScrollPosition: PropTypes.func.isRequired,
  isPremium: PropTypes.bool,
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  locationView: locationViewPropShape,
};

SpotListContainer.defaultProps = {
  locationView: null,
  isPremium: false,
};

export default connect(
  (state) => ({
    map: state.map,
    units: state.units,
    animations: state.animations,
    isPremium: getUserPremiumStatus(state),
    spotListScrollPosition: getSpotListScrollPosition(state),
    entitlements: getEntitlements(state),
  }),
  (dispatch) => ({
    doSetActiveSpot: (spotId) => dispatch(setActiveSpot(spotId)),
    doClearActiveSpot: () => dispatch(clearActiveSpot()),
    doSetScrollPosition: (scrollPosition) => dispatch(setSpotListScrollPosition(scrollPosition)),
  }),
)(withMetering(SpotListContainer));
