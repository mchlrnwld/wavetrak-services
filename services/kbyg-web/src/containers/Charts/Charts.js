import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import _throttle from 'lodash/throttle';
import _get from 'lodash/get';
import {
  trackNavigatedToPage,
  getUserPremiumStatus,
  getUserSettings,
  getWindow,
} from '@surfline/web-common';
import { ContentContainer, PageRail, Spinner } from '@surfline/quiver-react';
import { withRouter } from 'next/router';
import { fetchChartImages, setActiveChart } from '../../actions/charts';
import ChartPlayer from '../../components/ChartPlayer';
import ChartMenu from '../../components/ChartMenu';
import ChartsPageMeta from '../../components/ChartsPageMeta';
import withMetering from '../../common/withMetering';
import { chartsPropTypes } from '../../propTypes/charts';
import userSettingsPropType from '../../propTypes/userSettings';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';
import meterPropType from '../../propTypes/meter';
import { getCharts } from '../../selectors/charts';
import { getSubregionOverviewData, getSubregionAssociated } from '../../selectors/subregion';
import subregionForecastChartsPath from '../../utils/urls/subregionForecastChartsPath';

class Charts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileMenuOpen: false,
      chartMenuHeight: 0,
      mobileMenuVisible: false,
    };
    this.chartSegmentProps = {
      pageName: 'Sub-Region Charts',
      category: 'charts',
    };
  }

  componentDidMount() {
    const {
      charts,
      doFetchChartImages,
      overviewData: { name, _id },
      meter,
    } = this.props;
    const win = getWindow();
    doFetchChartImages(charts.active.type, charts.active.name);
    win.addEventListener('resize', this.resizeListener);
    const { category, pageName } = this.chartSegmentProps;

    trackNavigatedToPage(pageName, {
      category,
      channel: 'forecast',
      subregionId: _id,
      subregionName: name,
      viewedLocation: 'regional',
      chartType: this.chartType(charts),
      meteringEnabled: meter.meteringEnabled,
    });
    this.adjustChartMenuHeight();
    this.isMobileMenuVisible();
  }

  componentDidUpdate(prevProps) {
    const { charts, doFetchChartImages, overviewData, meter } = this.props;

    if (charts.active?.type !== prevProps.charts.active?.type) {
      const { category, pageName } = this.chartSegmentProps;
      trackNavigatedToPage(pageName, {
        category,
        channel: 'forecast',
        subregionId: overviewData._id,
        subregionName: overviewData.name,
        viewedLocation: 'regional',
        chartType: this.chartType(charts),
        meteringEnabled: meter.meteringEnabled,
      });

      doFetchChartImages(charts.active.type, charts.active.name);
    }
  }

  componentWillUnmount() {
    const win = getWindow();
    win.removeEventListener('resize', this.resizeListener);
    win.document.body.style.position = 'relative';
  }

  onMenuClick = (slug, subregionId) => {
    const { doSetActiveChart } = this.props;
    doSetActiveChart(slug, subregionId);
    this.toggleMobileMenu();
  };

  getBasinChartType = (type) => {
    let chartType = type;

    if (chartType.indexOf('band') > -1) {
      const typeSplit = chartType.split('band');
      chartType = `basinswell${typeSplit[1]}`;
    } else if (chartType.indexOf('wave') > -1) {
      return 'basinwave';
    } else if (chartType.indexOf('wind') > -1) {
      return 'basinwind';
    } else if (chartType.indexOf('period') > -1) {
      return 'basinperiod';
    }

    return chartType;
  };

  chartType = (chart) =>
    Number.isNaN(parseFloat(chart.active.type)) && chart.active.slug.indexOf('hi-res') === -1
      ? chart.active.type
      : 'hireswind';

  toggleMobileMenu = () => {
    const win = getWindow();
    const { mobileMenuOpen } = this.state;
    if (!mobileMenuOpen && win.innerWidth < 976) {
      win.document.body.style.position = 'fixed';
      this.setState({ mobileMenuOpen: true });
    } else {
      win.document.body.style.position = 'relative';
      this.setState({ mobileMenuOpen: false });
    }
  };

  adjustChartMenuHeight = () => {
    this.setState({ chartMenuHeight: this.chartMenuRef.chartMenuRef.offsetHeight });
  };

  isMobileMenuVisible = () => {
    const win = getWindow();
    this.setState({ mobileMenuVisible: win.innerWidth < 976 });
  };

  handleResize = () => {
    const win = getWindow();
    const { mobileMenuOpen } = this.state;
    if (win.innerWidth >= 976 && mobileMenuOpen) this.toggleMobileMenu();
    this.adjustChartMenuHeight();
    this.isMobileMenuVisible();
  };

  // eslint-disable-next-line react/sort-comp
  resizeListener = _throttle(this.handleResize, 250);

  render() {
    const { charts, overviewData, subregionAssociated, isPremium, router, userSettings } =
      this.props;
    const { subregionId } = router.query;
    const { mobileMenuOpen, mobileMenuVisible, chartMenuHeight } = this.state;
    const chartImages = charts.images && charts.images.images;
    let chartType = Number.isNaN(parseFloat(charts.active.type))
      ? charts.active.type
      : 'localhireswind';
    let metaName;
    if (charts.active.category === 'subregion') {
      metaName = overviewData.name;
    } else if (charts.active.category === 'nearshore') {
      metaName = charts.active.displayName ? charts.active.displayName : charts.active.name;
    } else if (charts.active.category === 'global') {
      metaName = charts.active.name;
    } else if (charts.active.basinName) {
      metaName = charts.active.name.split(/\s(swell|wind|wave|dominant)/i)[0];
      chartType = this.getBasinChartType(charts.active.type);
    } else {
      metaName = charts.region;
    }

    return (
      <div className="sl-charts-page">
        {metaName && chartType ? (
          <ChartsPageMeta
            type={chartType}
            name={metaName}
            urlPath={subregionForecastChartsPath(
              subregionId,
              charts.active.slug,
              overviewData.name,
            )}
          />
        ) : null}
        <ContentContainer>
          <PageRail side="left">
            <ChartMenu
              charts={charts}
              mobileMenuOpen={mobileMenuOpen}
              mobileMenuVisible={mobileMenuVisible}
              onMenuClick={this.onMenuClick}
              toggleMobileMenu={this.toggleMobileMenu}
              chartMenuHeight={chartMenuHeight}
              subregionName={overviewData.name}
              subregionId={subregionId}
              legacyId={subregionAssociated.legacyId}
              chartSegmentProps={this.chartSegmentProps}
              ref={(el) => {
                this.chartMenuRef = el;
              }}
            />
          </PageRail>
          <PageRail side="right">
            {!charts.images.loading && chartImages.length > 0 ? (
              <ChartPlayer
                charts={charts}
                toggleMobileMenu={this.toggleMobileMenu}
                subregionName={overviewData.name}
                utcOffset={_get(subregionAssociated, 'utcOffset', null)}
                overviewData={overviewData}
                chartSegmentProps={this.chartSegmentProps}
                isPremium={isPremium}
                userSettings={userSettings}
              />
            ) : (
              <div className="sl-chart-player">
                <Spinner />
              </div>
            )}
          </PageRail>
        </ContentContainer>
      </div>
    );
  }
}

Charts.defaultProps = {
  meter: null,
  userSettings: null,
};

Charts.propTypes = {
  userSettings: userSettingsPropType,
  charts: chartsPropTypes.isRequired,
  doFetchChartImages: PropTypes.func.isRequired,
  doSetActiveChart: PropTypes.func.isRequired,
  overviewData: overviewDataPropTypes.isRequired,
  subregionAssociated: PropTypes.shape({
    legacyId: PropTypes.string,
  }).isRequired,
  isPremium: PropTypes.bool.isRequired,
  meter: meterPropType,
  router: PropTypes.shape({
    query: PropTypes.shape({
      subregionId: PropTypes.string,
    }).isRequired,
  }).isRequired,
};

export default connect(
  (state) => ({
    charts: getCharts(state),
    overviewData: getSubregionOverviewData(state),
    subregionAssociated: getSubregionAssociated(state),
    isPremium: getUserPremiumStatus(state),
    userSettings: getUserSettings(state),
  }),
  (dispatch) => ({
    doFetchChartImages: (type, nearshoreModelName) =>
      dispatch(fetchChartImages(type, nearshoreModelName)),
    doSetActiveChart: (slug, subregionId) => dispatch(setActiveChart(slug, subregionId)),
  }),
)(withMetering(withRouter(Charts)));
