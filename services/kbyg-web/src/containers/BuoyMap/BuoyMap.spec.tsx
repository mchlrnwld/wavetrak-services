import React from 'react';
import { expect } from 'chai';

import sinon, { SinonStub } from 'sinon';
import { shallow } from 'enzyme';

import BuoyMap from './BuoyMap';
import MapLinks from '../../components/MapLinks';
import MapBuoys from '../../components/MapBuoys';

import * as useSyncMapLocationToUrl from '../../hooks/map/useSyncMapLocationToUrl';
import * as useSyncMapLocationToSession from '../../hooks/map/useSyncMapLocationToSession';
import * as useHydrateMapLocation from '../../hooks/map/useHydrateMapLocation';

import { getBuoyMapPathWithLocationObject } from '../../utils/urls/mapPaths';

describe('containers / BuoyMap', () => {
  let useSyncMapLocationToSessionStub: SinonStub;

  let useSyncMapLocationToUrlStub: SinonStub;

  let useHydrateMapLocationStub: SinonStub;

  beforeEach(() => {
    useSyncMapLocationToSessionStub = sinon.stub(useSyncMapLocationToSession, 'default');
    useSyncMapLocationToUrlStub = sinon.stub(useSyncMapLocationToUrl, 'default');
    useHydrateMapLocationStub = sinon.stub(useHydrateMapLocation, 'default');
  });

  afterEach(() => {
    useSyncMapLocationToSessionStub.restore();
    useSyncMapLocationToUrlStub.restore();
    useHydrateMapLocationStub.restore();
  });

  it('should render the map links', () => {
    const wrapper = shallow(<BuoyMap />);

    const mapLinks = wrapper.find(MapLinks);
    expect(mapLinks).to.have.length(1);
  });

  it('should render the map buoys', () => {
    const wrapper = shallow(<BuoyMap />);

    const mapLinks = wrapper.find(MapBuoys);
    expect(mapLinks).to.have.length(1);
  });

  it('should sync the map location to the session', () => {
    shallow(<BuoyMap />);

    expect(useSyncMapLocationToSessionStub).to.have.been.calledOnce();
  });

  it('should sync the map location to the url', () => {
    shallow(<BuoyMap />);

    expect(useSyncMapLocationToUrlStub).to.have.been.calledOnceWithExactly(
      getBuoyMapPathWithLocationObject,
    );
  });

  it('should hydrate the map location', () => {
    shallow(<BuoyMap />);

    expect(useHydrateMapLocationStub).to.have.been.calledOnce();
  });

  it('should call the hooks in order', () => {
    shallow(<BuoyMap />);

    expect(useHydrateMapLocationStub).to.have.been.calledBefore(useSyncMapLocationToSessionStub);
    expect(useHydrateMapLocationStub).to.have.been.calledBefore(useSyncMapLocationToUrlStub);
    expect(useSyncMapLocationToSessionStub).to.have.been.calledBefore(useSyncMapLocationToUrlStub);
  });
});
