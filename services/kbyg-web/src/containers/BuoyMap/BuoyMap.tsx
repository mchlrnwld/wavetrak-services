import React from 'react';

import MapLinks from '../../components/MapLinks';
import MapBuoys from '../../components/MapBuoys';
import MapHeader from '../../components/MapHeader';

import { getBuoyMapPathWithLocationObject } from '../../utils/urls/mapPaths';
import useSyncMapLocationToUrl from '../../hooks/map/useSyncMapLocationToUrl';
import useSyncMapLocationToSession from '../../hooks/map/useSyncMapLocationToSession';
import useHydrateMapLocation from '../../hooks/map/useHydrateMapLocation';

import styles from './BuoyMap.module.scss';

const BuoyMap: React.FC = () => {
  // Be careful changing the order of these hooks, as other hooks may depend on the location
  // set into the store via `useHydrateMapLocation`
  useHydrateMapLocation();
  useSyncMapLocationToSession();
  useSyncMapLocationToUrl(getBuoyMapPathWithLocationObject);

  return (
    <div className={styles.slBuoyMap}>
      <MapHeader isBuoys />
      <MapLinks />
      <MapBuoys />
    </div>
  );
};

BuoyMap.propTypes = {};

export default BuoyMap;
