import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { WorldTaxonomy } from '@surfline/quiver-react';
import MetaTags from '../../components/MetaTags';
import { camsAndForecastsPaths, geonameMapPath } from '../../utils/urls/camsAndForecastsPath';
import { AppState } from '../../stores';
import type { WorldTaxonomy as Taxonomy } from '../../types/locationView';

import styles from './CamsAndForecasts.module.scss';

const SUFFIX = 'Surf Reports & Cams';

const CamsAndForecasts = () => {
  const worldTaxonomy = useSelector<AppState>((state) => state.worldTaxonomy);

  const CountryComponent = useCallback((country: Taxonomy['countries'][0]): JSX.Element => {
    const { name } = country;
    return (
      <a href={geonameMapPath(country)}>
        {name} {SUFFIX}
      </a>
    );
  }, []);

  return (
    <div className={styles.camsAndForecasts}>
      <MetaTags
        title="Surfline Maps of Every Surf Spot in the World"
        description="Know Before You Go. Access to the world's best surf forecast team at Surfline and get the best travel and weather info along with live HD surf cams."
        urlPath={camsAndForecastsPaths.base}
      />
      <WorldTaxonomy
        taxonomy={worldTaxonomy}
        countryComponent={CountryComponent}
        headerSuffix={SUFFIX}
      />
    </div>
  );
};

export default CamsAndForecasts;
