import React from 'react';
import { mount } from 'enzyme';
import { NotFound, FiveHundred } from '@surfline/quiver-react';
import ErrorPage from './ErrorPage';

describe('components / ErrorPage', () => {
  it('should render the 404 page', () => {
    const wrapper = mount(<ErrorPage statusCode={404} />);

    expect(wrapper.find(NotFound)).toHaveLength(1);
  });

  it('should render the 500 page', () => {
    const wrapper = mount(<ErrorPage statusCode={500} />);

    expect(wrapper.find(FiveHundred)).toHaveLength(1);
  });
});
