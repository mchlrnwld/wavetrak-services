import sinon, { SinonStub } from 'sinon';
import { expect } from 'chai';

import { act } from 'react-dom/test-utils';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import * as buoysActions from '../../actions/buoys';
import { getBuoyDetailsFixture, getBuoyReportFixture } from '../../common/api/fixtures/buoys';

import BuoyReport from './BuoyReport';
import BuoysHeader from '../../components/BuoysHeader';
import BuoyCurrentReadings from '../../components/BuoyCurrentReadings';
import BuoyReadingsTable from '../../components/BuoyReadingsTable';
import storeState from '../../selectors/fixtures/state';
import { AppState } from '../../stores';

const meter = {
  showMeterRegwall: false,
  meterRemaining: undefined,
  isMeteredPremium: false,
  meteringEnabled: false,
};

const storeStateWithMeter = {
  ...storeState,
  backplane: {
    ...storeState.backplane,
    meter: {
      anonymousId: '9b7fa07b-6d33-4484-afa3-ec3ea281d100',
      treatmentName: 'sl_rate_limiting_v3',
      treatmentState: 'on',
      meterLimit: 0,
      meterRemaining: -1,
    },
    user: {
      ...storeState.backplane.user,
      entitlements: ['sl_metered'],
    },
    state: {
      meterLoaded: true,
      shouldRecomputeMeterModal: false,
      meteringEnabled: true,
    },
  },
} as AppState;

describe('components / BuoyReport', () => {
  let useGetBuoyDetailsQueryStub: SinonStub;
  let useGetBuoyReportQueryStub: SinonStub;
  let useGetNearbyBuoysQueryStub: SinonStub;

  beforeEach(() => {
    useGetBuoyDetailsQueryStub = sinon
      .stub(buoysActions, 'useGetBuoyDetailsQuery')
      .returns({ isUninitialized: true, refetch: () => {} });
    useGetBuoyReportQueryStub = sinon
      .stub(buoysActions, 'useGetBuoyReportQuery')
      .returns({ isUninitialized: true, refetch: () => {} });
    useGetNearbyBuoysQueryStub = sinon
      .stub(buoysActions, 'useGetNearbyBuoysQuery')
      .returns({ isUninitialized: true, refetch: () => {} });
  });

  afterEach(() => {
    useGetBuoyDetailsQueryStub.restore();
    useGetBuoyReportQueryStub.restore();
    useGetNearbyBuoysQueryStub.restore();
  });

  it('should render the buoys header', () => {
    useGetBuoyDetailsQueryStub.returns({ data: getBuoyDetailsFixture });

    const { wrapper } = mountWithReduxAndRouter(
      BuoyReport,
      {},
      { router: { pathname: '/[buoyId]', asPath: '/123', query: { buoyId: '123' } } },
    );

    expect(useGetBuoyReportQueryStub).to.have.been.calledOnceWithExactly({
      id: '123',
      meter,
    });
    expect(wrapper.find(BuoysHeader)).to.have.length(1);
  });

  it('should render the buoy current conditions', () => {
    useGetBuoyDetailsQueryStub.returns({ data: getBuoyDetailsFixture });
    useGetBuoyReportQueryStub.returns({ data: getBuoyReportFixture });

    const { wrapper } = mountWithReduxAndRouter(
      BuoyReport,
      {},
      { router: { pathname: '/[buoyId]', asPath: '/123', query: { buoyId: '123' } } },
    );

    expect(useGetBuoyReportQueryStub).to.have.been.calledOnceWithExactly({
      id: '123',
      meter,
    });
    expect(wrapper.find(BuoyCurrentReadings)).to.have.length(1);
  });

  it('should render the buoy current conditions even without buoy details', () => {
    useGetBuoyReportQueryStub.returns({ data: getBuoyReportFixture });

    const { wrapper } = mountWithReduxAndRouter(
      BuoyReport,
      {},
      { router: { pathname: '/[buoyId]', asPath: '/123', query: { buoyId: '123' } } },
    );

    expect(useGetBuoyReportQueryStub).to.have.been.calledOnceWithExactly({
      id: '123',
      meter,
    });
    expect(wrapper.find(BuoyCurrentReadings)).to.have.length(1);
  });

  it('should render the buoy table', () => {
    useGetBuoyReportQueryStub.returns({ data: getBuoyReportFixture });

    const { wrapper } = mountWithReduxAndRouter(
      BuoyReport,
      {},
      { router: { pathname: '/[buoyId]', asPath: '/123', query: { buoyId: '123' } } },
    );

    expect(wrapper.find(BuoyReadingsTable)).to.have.length(1);
  });

  it('should not render the buoy table for a metered user and zero out current readings swells', () => {
    useGetBuoyReportQueryStub.returns({ data: getBuoyReportFixture });

    const { wrapper } = mountWithReduxAndRouter(
      BuoyReport,
      {},
      {
        router: { pathname: '/[buoyId]', asPath: '/123', query: { buoyId: '123' } },
        initialState: storeStateWithMeter,
      },
    );

    expect(wrapper.find(BuoyReadingsTable)).to.have.length(0);
    expect(wrapper.find(BuoyCurrentReadings).prop('swells')).to.deep.equal([]);
  });

  it('should collapse the buoy table on error', () => {
    useGetBuoyReportQueryStub.returns({ data: getBuoyReportFixture });

    const { wrapper } = mountWithReduxAndRouter(
      BuoyReport,
      {},
      { router: { pathname: '/[buoyId]', asPath: '/123', query: { buoyId: '123' } } },
    );

    act(() => {
      wrapper.find(BuoyReadingsTable).simulateError(new Error('something went wrong'));
      wrapper.update();
    });

    expect(wrapper.isEmptyRender()).to.be.false();
  });
});
