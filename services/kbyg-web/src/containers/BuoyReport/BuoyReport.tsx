import React, { useMemo, useEffect } from 'react';
import { ErrorBoundary, ContentContainer } from '@surfline/quiver-react';
import { trackNavigatedToPage, getUserDetails, getWindow } from '@surfline/web-common';
import { shallowEqual } from 'react-redux';
import { useRouter } from 'next/router';

import { useAppSelector } from '../../stores/hooks';
import BuoysHeader from '../../components/BuoysHeader';
import BuoyCurrentReadings from '../../components/BuoyCurrentReadings';
import BuoyReadingsTable from '../../components/BuoyReadingsTable';
import BuoysPageMeta from '../../components/BuoysPageMeta';
import NearbyBuoys from '../../components/NearbyBuoys';

import { useGetBuoyReportQuery, useGetBuoyDetailsQuery } from '../../actions/buoys';
import config from '../../config';
import useMetering from '../../common/hooks/useMetering';

import styles from './BuoyReport.module.scss';

const BuoyReport: React.FC = () => {
  const meter = useMetering('BuoyReport');
  const router = useRouter();
  const { query } = router;
  const buoyId = query.buoyId as string;
  const {
    data: buoyDetails,
    error: detailsError,
    isFetching: detailsFetching,
    isUninitialized: detailsUninitialized,
  } = useGetBuoyDetailsQuery(buoyId);

  const {
    data: buoyReport,
    error: buoyReportError,
    isFetching: reportFetching,
    isUninitialized: reportUninitialized,
  } = useGetBuoyReportQuery({ id: buoyId, meter });

  const details = buoyDetails?.data;
  const associated = buoyDetails?.associated;
  const detailsLoading = detailsFetching || detailsUninitialized;

  const report = buoyReport?.data;
  const reportLoading = reportFetching || reportUninitialized;

  const latestBuoyData = report?.days?.[0]?.[0];

  const { showMeterRegwall } = meter;
  const native = (query.native as string)?.toLowerCase() === 'true';

  const location = useMemo(
    () => (details ? { latitude: details.latitude, longitude: details.longitude } : undefined),
    [details],
  );

  useEffect(() => {
    if (details?.sourceId) {
      trackNavigatedToPage('Buoy Station Report', {
        meteringEnabled: showMeterRegwall,
        category: 'buoys',
        buoyId: details.sourceId,
        buoyName: details.name,
        native,
      });
    }
  }, [details?.sourceId, details?.name, showMeterRegwall, native]);

  const reportMissing = !reportLoading && !report;
  const emptyReport = !reportLoading && !report?.days?.length;
  const isOffline = !reportLoading && details?.status === 'OFFLINE';
  const collapseReadingsTable = reportMissing || isOffline || emptyReport;

  const userDetails = useAppSelector(getUserDetails, shallowEqual);
  const redirectUrl = getWindow()?.location.pathname;

  const changeAccountSettingsHref = userDetails
    ? `${config.accountSettingsUrl}?redirectUrl=${redirectUrl}`
    : `${config.createAccountUrl}?redirectUrl=${encodeURIComponent(
        `${config.surflineHost}${config.accountSettingsUrl}?redirectUrl=${config.surflineHost}${redirectUrl}`,
      )}`;

  return (
    <>
      {details && (
        <BuoysPageMeta
          name={details?.name}
          id={buoyId}
          sourceID={details?.sourceId}
          latitude={location?.latitude}
          longitude={location?.longitude}
          latestTimestamp={details?.latestTimestamp}
          utcOffset={details?.utcOffset}
        />
      )}
      <BuoysHeader
        name={details?.name}
        id={details?.sourceId}
        loading={detailsLoading}
        error={detailsError as Error}
      />
      <ContentContainer classes={{ root: styles.contentContainer }}>
        <BuoyCurrentReadings
          utcOffset={latestBuoyData?.utcOffset}
          swells={showMeterRegwall ? [] : latestBuoyData?.swells}
          wind={latestBuoyData?.metrics.wind}
          airTemperature={latestBuoyData?.metrics.airTemperature}
          waterTemperature={latestBuoyData?.metrics.waterTemperature}
          height={latestBuoyData?.height}
          period={latestBuoyData?.period}
          timestamp={latestBuoyData?.timestamp}
          pressure={latestBuoyData?.metrics.pressure}
          dewPoint={latestBuoyData?.metrics.dewPoint}
          loading={reportLoading}
          error={buoyReportError as Error}
          lat={location?.latitude}
          lon={location?.longitude}
          abbrTimezone={associated?.abbrTimezone}
          status={details?.status}
        />
        {!showMeterRegwall && !collapseReadingsTable && (
          <ErrorBoundary render={() => null}>
            <BuoyReadingsTable
              buoyReportData={buoyReport?.data}
              changeUnitsHref={changeAccountSettingsHref}
              loading={reportLoading}
              error={buoyReportError}
            />
          </ErrorBoundary>
        )}
        <NearbyBuoys
          buoyId={buoyId}
          loading={detailsLoading}
          error={detailsError as Error}
          location={location}
          trackLocation="Buoy Station Report"
        />
      </ContentContainer>
    </>
  );
};

export default BuoyReport;
