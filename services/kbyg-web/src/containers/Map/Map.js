import { kbygPaths, slugify } from '@surfline/web-common';
import { useRouter } from 'next/router';
import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import classNames from 'classnames';

import { scrollToSpot } from '../../actions/animations';
import MapPageMeta from '../../components/MapPageMeta';
import { ALWAYS_SYNC } from '../../propTypes/map/mapUrlSync';
import { getMapPageWidth } from '../../utils/dimensions';
import mapOptions from '../../utils/mapOptions';
import SpotList from '../SpotList';
import SpotMap from '../SpotMap';

import styles from './Map.module.scss';

const { spotReportPath } = kbygPaths;

const getRightRailClass = () => classNames(styles.slRightRail, 'sl-right-rail');
const getLeftRailClass = () => classNames(styles.slLeftRail, 'sl-left-rail');

/** @type {React.FC} */
const Map = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const doScrollToSpot = useCallback(
    (spot) => {
      dispatch(scrollToSpot(spot._id));
    },
    [dispatch],
  );

  const goToSpot = useCallback(
    (spot) => router.push(spotReportPath(slugify(spot.name), spot._id)),
    [router],
  );

  return (
    <div className={styles.slMapPage} style={{ height: 'calc(100vh - 97px)' }}>
      <MapPageMeta />
      <div className={getRightRailClass()}>
        <SpotMap
          showBottomControls
          mapOptions={mapOptions}
          clusterMaxZoom={11}
          onClickSpotMarker={getMapPageWidth() < 992 ? doScrollToSpot : goToSpot}
          syncUrl={ALWAYS_SYNC}
          storeLocationInSession
        />
      </div>
      <div className={getLeftRailClass()}>
        <SpotList />
      </div>
    </div>
  );
};

export default Map;
