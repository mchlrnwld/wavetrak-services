import { expect } from 'chai';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import Map from './Map';
import MapPageMeta from '../../components/MapPageMeta';
import SpotMap from '../SpotMap';
import SpotList from '../../components/SpotList';

describe('containers / Map', () => {
  it('should render the spot map and spot list', () => {
    const { wrapper } = mountWithReduxAndRouter(Map);

    expect(wrapper.find(MapPageMeta)).to.have.length(1);
    expect(wrapper.find(SpotMap)).to.have.length(1);
    expect(wrapper.find(SpotList)).to.have.length(1);
  });
});
