## How to Copy Clips to lower tiers when testing

Ensure you've whitelisted your IP on both surfline-dev and surfline-prod in Atlas

If you have poor internet connection use a temp EC2 instance ([Confluence: Create a Temporary EC2 Instance](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1107951999/Create+a+Temporary+EC2+Instance)) as these commands download and upload the whole collection over your connection and takes a while even with good internet. If the pipe breaks then you have to start again

You can use this to sync other Collections but don't copy `Spots` as that requires an aditional task to assign the correct POI Ids since they differ across environments

### staging
```
mongodump "mongodb+srv://[USER]:[PASSWORD]@prod-common-mongodb-atlas-aj1dk.mongodb.net/KBYG?ssl=true&authSource=admin&readPreference=secondaryPreferred" -c CameraRecording --archive --ssl | mongorestore "mongodb+srv://[USER]:[PASSWORD]@dev-common-mongodb-atla.baspr.mongodb.net/KBYG?retryWrites=true&w=majority" --archive --ssl --drop
```

### sandbox
```
mongodump "mongodb+srv://[USER]:[PASSWORD]@prod-common-mongodb-atlas-aj1dk.mongodb.net/KBYG?ssl=true&authSource=admin&readPreference=secondaryPreferred" -c CameraRecording --archive --ssl | mongorestore "mongodb+srv://[USER]:[PASSWORD]@dev-common-mongodb-atla.baspr.mongodb.net/KBYG_sandbox?retryWrites=true&w=majority" --archive --ssl --drop --nsFrom KBYG.CameraRecording --nsTo KBYG_sandbox.CameraRecording
```
