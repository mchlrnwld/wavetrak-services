const camRewind = {
  spots: [
    {
      _id: '5842041f4e65fad6a7708890',
      name: 'Pipeline',
      cams: ['58349eed3421b20545c4b56c', '58349ef6e411dc743a5d52cc'],
      timezone: 'Pacific/Honolulu',
      hasThumbnail: true,
      timezoneOffset: -10,
      cameras: [
        {
          _id: '58349eed3421b20545c4b56c',
          title: 'HI - Pipeline',
          alias: 'hi-pipeline',
          stillUrl: 'https://camstills.cdn-surfline.com/hi-pipeline/latest_small.jpg',
        },
        {
          _id: '58349ef6e411dc743a5d52cc',
          title: 'HI - Pipeline Overview',
          alias: 'hi-pipelinefixed',
          stillUrl: 'https://camstills.cdn-surfline.com/hi-pipelinefixed/latest_small.jpg',
        },
      ],
    },
    {
      _id: '584204214e65fad6a7709cdf',
      name: 'Pipeline Overview',
      cams: ['58349ef6e411dc743a5d52cc'],
      timezone: 'Pacific/Honolulu',
      hasThumbnail: true,
      timezoneOffset: -10,
      cameras: [
        {
          _id: '58349ef6e411dc743a5d52cc',
          title: 'HI - Pipeline Overview',
          alias: 'hi-pipelinefixed',
          stillUrl: 'https://camstills.cdn-surfline.com/hi-pipelinefixed/latest_small.jpg',
        },
      ],
    },
  ],
  clips: [
    {
      startDate: '2020-11-01T09:43:43.000Z',
      endDate: '2020-11-01T09:53:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T094343176_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T094343176.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T094343176_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T09:33:41.000Z',
      endDate: '2020-11-01T09:43:42.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T093341135_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T093341135.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T093341135_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T09:23:39.000Z',
      endDate: '2020-11-01T09:33:40.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T092339307_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T092339307.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T092339307_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T09:13:37.000Z',
      endDate: '2020-11-01T09:23:38.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T091337271_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T091337271.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T091337271_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T09:03:38.000Z',
      endDate: '2020-11-01T09:13:39.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T090338172_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T090338172.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T090338172_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T08:53:38.000Z',
      endDate: '2020-11-01T09:03:38.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T085338078_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T085338078.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T085338078_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T08:43:36.000Z',
      endDate: '2020-11-01T08:53:37.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T084336362_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T084336362.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T084336362_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T08:33:33.000Z',
      endDate: '2020-11-01T08:43:33.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T083333844_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T083333844.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T083333844_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T08:23:32.000Z',
      endDate: '2020-11-01T08:33:34.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T082332006_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T082332006.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T082332006_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T08:13:29.000Z',
      endDate: '2020-11-01T08:23:30.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T081329948_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T081329948.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T081329948_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T08:03:29.000Z',
      endDate: '2020-11-01T08:13:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T080329497_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T080329497.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T080329497_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T07:53:29.000Z',
      endDate: '2020-11-01T08:03:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T075329000_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T075329000.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T075329000_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T07:43:27.000Z',
      endDate: '2020-11-01T07:53:28.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T074327200_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T074327200.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T074327200_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T07:33:27.000Z',
      endDate: '2020-11-01T07:43:27.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T073327123_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T073327123.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T073327123_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T07:23:25.000Z',
      endDate: '2020-11-01T07:33:26.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T072325189_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T072325189.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T072325189_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T07:13:23.000Z',
      endDate: '2020-11-01T07:23:24.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T071323302_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T071323302.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T071323302_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T07:03:21.000Z',
      endDate: '2020-11-01T07:13:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T070321157_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T070321157.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T070321157_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T06:53:21.000Z',
      endDate: '2020-11-01T07:03:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T065321937_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T065321937.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T065321937_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T06:43:22.000Z',
      endDate: '2020-11-01T06:53:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T064322098_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T064322098.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T064322098_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T06:33:20.000Z',
      endDate: '2020-11-01T06:43:21.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T063320014_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T063320014.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T063320014_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T06:23:18.000Z',
      endDate: '2020-11-01T06:33:19.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T062318139_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T062318139.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T062318139_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T06:13:15.000Z',
      endDate: '2020-11-01T06:23:15.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T061315862_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T061315862.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T061315862_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T06:03:13.000Z',
      endDate: '2020-11-01T06:13:14.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T060313883_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T060313883.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T060313883_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T05:53:12.000Z',
      endDate: '2020-11-01T06:03:13.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T055312976_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T055312976.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T055312976_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T05:43:10.000Z',
      endDate: '2020-11-01T05:53:11.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T054310978_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T054310978.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T054310978_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T05:33:08.000Z',
      endDate: '2020-11-01T05:43:09.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T053308970_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T053308970.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T053308970_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T05:23:07.000Z',
      endDate: '2020-11-01T05:33:08.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T052307110_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T052307110.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T052307110_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T05:13:07.000Z',
      endDate: '2020-11-01T05:23:07.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T051307020_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T051307020.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T051307020_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T05:03:05.000Z',
      endDate: '2020-11-01T05:13:06.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T050305222_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T050305222.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T050305222_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T04:53:05.000Z',
      endDate: '2020-11-01T05:03:05.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T045305260_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T045305260.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T045305260_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T04:43:04.000Z',
      endDate: '2020-11-01T04:53:06.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T044304936_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T044304936.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T044304936_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T04:33:03.000Z',
      endDate: '2020-11-01T04:43:04.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T043303043_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T043303043.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T043303043_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T04:23:00.000Z',
      endDate: '2020-11-01T04:33:01.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T042300559_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T042300559.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T042300559_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T04:13:01.000Z',
      endDate: '2020-11-01T04:23:01.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T041301528_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T041301528.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T041301528_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T04:02:59.000Z',
      endDate: '2020-11-01T04:13:00.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T040259577_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T040259577.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T040259577_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T03:52:57.000Z',
      endDate: '2020-11-01T04:02:57.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T035257808_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T035257808.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T035257808_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T03:42:57.000Z',
      endDate: '2020-11-01T03:52:57.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T034257707_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T034257707.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T034257707_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T03:32:58.000Z',
      endDate: '2020-11-01T03:42:59.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T033258518_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T033258518.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T033258518_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T03:22:56.000Z',
      endDate: '2020-11-01T03:32:56.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T032256790_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T032256790.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T032256790_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T03:12:55.000Z',
      endDate: '2020-11-01T03:22:55.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T031255860_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T031255860.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T031255860_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T03:02:55.000Z',
      endDate: '2020-11-01T03:12:55.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T030255746_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T030255746.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T030255746_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T02:52:53.000Z',
      endDate: '2020-11-01T03:02:54.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T025253816_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T025253816.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T025253816_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T02:42:53.000Z',
      endDate: '2020-11-01T02:52:53.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T024253820_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T024253820.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T024253820_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T02:32:51.000Z',
      endDate: '2020-11-01T02:42:52.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T023251809_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T023251809.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T023251809_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T02:22:52.000Z',
      endDate: '2020-11-01T02:32:53.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T022252444_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T022252444.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T022252444_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T02:12:50.000Z',
      endDate: '2020-11-01T02:22:51.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T021250600_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T021250600.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T021250600_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T02:02:50.000Z',
      endDate: '2020-11-01T02:12:50.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T020250549_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T020250549.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T020250549_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T01:52:50.000Z',
      endDate: '2020-11-01T02:02:50.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T015250629_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T015250629.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T015250629_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T01:42:48.000Z',
      endDate: '2020-11-01T01:52:49.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T014248553_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T014248553.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T014248553_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T01:32:46.000Z',
      endDate: '2020-11-01T01:42:46.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T013246928_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T013246928.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T013246928_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T01:22:46.000Z',
      endDate: '2020-11-01T01:32:46.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T012246910_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T012246910.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T012246910_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T01:12:47.000Z',
      endDate: '2020-11-01T01:22:47.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T011247870_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T011247870.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T011247870_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T01:02:46.000Z',
      endDate: '2020-11-01T01:12:47.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T010246544_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T010246544.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T010246544_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T00:52:44.000Z',
      endDate: '2020-11-01T01:02:44.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T005244806_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T005244806.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T005244806_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T00:42:43.000Z',
      endDate: '2020-11-01T00:52:44.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T004243997_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T004243997.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T004243997_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T00:32:43.000Z',
      endDate: '2020-11-01T00:42:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T003243814_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T003243814.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T003243814_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T00:22:41.000Z',
      endDate: '2020-11-01T00:32:42.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T002241894_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T002241894.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T002241894_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T00:12:43.000Z',
      endDate: '2020-11-01T00:22:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T001243760_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T001243760.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T001243760_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-11-01T00:02:43.000Z',
      endDate: '2020-11-01T00:12:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T000243624_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201101T000243624.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/11/01/58349ef6e411dc743a5d52cc.20201101T000243624_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T23:52:43.000Z',
      endDate: '2020-11-01T00:02:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T235243599_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T235243599.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T235243599_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T23:42:43.000Z',
      endDate: '2020-10-31T23:52:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T234243644_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T234243644.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T234243644_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T23:32:42.000Z',
      endDate: '2020-10-31T23:42:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T233242550_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T233242550.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T233242550_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T23:22:41.000Z',
      endDate: '2020-10-31T23:32:42.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T232241731_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T232241731.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T232241731_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T23:12:39.000Z',
      endDate: '2020-10-31T23:22:39.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T231239905_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T231239905.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T231239905_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T23:02:39.000Z',
      endDate: '2020-10-31T23:12:39.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T230239923_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T230239923.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T230239923_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T22:52:37.000Z',
      endDate: '2020-10-31T23:02:38.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T225237903_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T225237903.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T225237903_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T22:42:37.000Z',
      endDate: '2020-10-31T22:52:37.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T224237865_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T224237865.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T224237865_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T22:32:37.000Z',
      endDate: '2020-10-31T22:42:37.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T223237752_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T223237752.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T223237752_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T22:22:35.000Z',
      endDate: '2020-10-31T22:32:36.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T222235869_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T222235869.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T222235869_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T22:12:33.000Z',
      endDate: '2020-10-31T22:22:34.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T221233809_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T221233809.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T221233809_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T22:02:35.000Z',
      endDate: '2020-10-31T22:12:35.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T220235669_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T220235669.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T220235669_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T21:52:35.000Z',
      endDate: '2020-10-31T22:02:35.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T215235587_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T215235587.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T215235587_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T21:42:35.000Z',
      endDate: '2020-10-31T21:52:35.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T214235541_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T214235541.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T214235541_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T21:32:33.000Z',
      endDate: '2020-10-31T21:42:34.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T213233599_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T213233599.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T213233599_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T21:22:33.000Z',
      endDate: '2020-10-31T21:32:33.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T212233642_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T212233642.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T212233642_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T21:12:31.000Z',
      endDate: '2020-10-31T21:22:31.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T211231779_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T211231779.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T211231779_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T21:02:31.000Z',
      endDate: '2020-10-31T21:12:31.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T210231817_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T210231817.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T210231817_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T20:52:31.000Z',
      endDate: '2020-10-31T21:02:31.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T205231821_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T205231821.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T205231821_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T20:42:29.000Z',
      endDate: '2020-10-31T20:52:30.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T204229820_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T204229820.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T204229820_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T20:32:29.000Z',
      endDate: '2020-10-31T20:42:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T203229820_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T203229820.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T203229820_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T20:22:29.000Z',
      endDate: '2020-10-31T20:32:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T202229809_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T202229809.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T202229809_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T20:12:29.000Z',
      endDate: '2020-10-31T20:22:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T201229789_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T201229789.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T201229789_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T20:02:27.000Z',
      endDate: '2020-10-31T20:12:28.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T200227780_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T200227780.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T200227780_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T19:52:28.000Z',
      endDate: '2020-10-31T20:02:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T195228703_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T195228703.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T195228703_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T19:42:26.000Z',
      endDate: '2020-10-31T19:52:27.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T194226532_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T194226532.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T194226532_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T19:32:26.000Z',
      endDate: '2020-10-31T19:42:26.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T193226575_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T193226575.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T193226575_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T19:22:26.000Z',
      endDate: '2020-10-31T19:32:26.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T192226579_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T192226579.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T192226579_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T19:12:26.000Z',
      endDate: '2020-10-31T19:22:26.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T191226564_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T191226564.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T191226564_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T19:02:24.000Z',
      endDate: '2020-10-31T19:12:25.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T190224468_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T190224468.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T190224468_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T18:52:22.000Z',
      endDate: '2020-10-31T19:02:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T185222753_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T185222753.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T185222753_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T18:42:22.000Z',
      endDate: '2020-10-31T18:52:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T184222767_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T184222767.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T184222767_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T18:32:22.000Z',
      endDate: '2020-10-31T18:42:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T183222765_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T183222765.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T183222765_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T18:22:22.000Z',
      endDate: '2020-10-31T18:32:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T182222819_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T182222819.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T182222819_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T18:12:21.000Z',
      endDate: '2020-10-31T18:22:22.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T181221910_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T181221910.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T181221910_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T18:02:19.000Z',
      endDate: '2020-10-31T18:12:20.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T180219810_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T180219810.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T180219810_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T17:52:17.000Z',
      endDate: '2020-10-31T18:02:18.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T175217829_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T175217829.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T175217829_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T17:42:19.000Z',
      endDate: '2020-10-31T17:52:19.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T174219549_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T174219549.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T174219549_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T17:32:17.000Z',
      endDate: '2020-10-31T17:42:18.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T173217661_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T173217661.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T173217661_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T17:22:17.000Z',
      endDate: '2020-10-31T17:32:17.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T172217568_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T172217568.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T172217568_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T17:12:17.000Z',
      endDate: '2020-10-31T17:22:17.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T171217889_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T171217889.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T171217889_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T17:02:15.000Z',
      endDate: '2020-10-31T17:12:16.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T170215597_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T170215597.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T170215597_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T16:52:15.000Z',
      endDate: '2020-10-31T17:02:15.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T165215795_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T165215795.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T165215795_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T16:42:13.000Z',
      endDate: '2020-10-31T16:52:13.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T164213859_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T164213859.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T164213859_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T16:32:11.000Z',
      endDate: '2020-10-31T16:42:12.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T163211892_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T163211892.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T163211892_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T16:22:11.000Z',
      endDate: '2020-10-31T16:32:11.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T162211821_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T162211821.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T162211821_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T16:12:11.000Z',
      endDate: '2020-10-31T16:22:11.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T161211770_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T161211770.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T161211770_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T16:02:10.000Z',
      endDate: '2020-10-31T16:12:11.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T160210051_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T160210051.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T160210051_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T15:52:08.000Z',
      endDate: '2020-10-31T16:02:09.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T155208080_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T155208080.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T155208080_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T15:42:06.000Z',
      endDate: '2020-10-31T15:52:07.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T154206169_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T154206169.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T154206169_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T15:32:07.000Z',
      endDate: '2020-10-31T15:42:07.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T153207852_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T153207852.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T153207852_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T15:22:07.000Z',
      endDate: '2020-10-31T15:32:07.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T152207890_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T152207890.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T152207890_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T15:12:06.000Z',
      endDate: '2020-10-31T15:22:07.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T151206006_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T151206006.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T151206006_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T15:02:04.000Z',
      endDate: '2020-10-31T15:12:06.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T150204055_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T150204055.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T150204055_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T14:52:02.000Z',
      endDate: '2020-10-31T15:02:03.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T145202235_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T145202235.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T145202235_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T14:42:02.000Z',
      endDate: '2020-10-31T14:52:02.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T144202070_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T144202070.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T144202070_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T14:32:00.000Z',
      endDate: '2020-10-31T14:42:01.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T143200117_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T143200117.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T143200117_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T14:21:56.000Z',
      endDate: '2020-10-31T14:31:57.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T142156858_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T142156858.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T142156858_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T14:11:54.000Z',
      endDate: '2020-10-31T14:21:55.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T141154920_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T141154920.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T141154920_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T14:01:52.000Z',
      endDate: '2020-10-31T14:11:53.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T140152888_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T140152888.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T140152888_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T13:51:53.000Z',
      endDate: '2020-10-31T14:01:53.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T135153010_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T135153010.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T135153010_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T13:41:51.000Z',
      endDate: '2020-10-31T13:51:52.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T134151972_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T134151972.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T134151972_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T13:31:49.000Z',
      endDate: '2020-10-31T13:41:50.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T133149989_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T133149989.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T133149989_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T13:21:49.000Z',
      endDate: '2020-10-31T13:31:51.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T132149790_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T132149790.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T132149790_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T13:11:47.000Z',
      endDate: '2020-10-31T13:21:49.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T131147794_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T131147794.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T131147794_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T13:01:45.000Z',
      endDate: '2020-10-31T13:11:47.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T130145897_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T130145897.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T130145897_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T12:51:43.000Z',
      endDate: '2020-10-31T13:01:44.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T125143996_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T125143996.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T125143996_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T12:41:43.000Z',
      endDate: '2020-10-31T12:51:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T124143895_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T124143895.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T124143895_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T12:31:42.000Z',
      endDate: '2020-10-31T12:41:43.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T123142005_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T123142005.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T123142005_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T12:21:42.000Z',
      endDate: '2020-10-31T12:31:42.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T122142256_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T122142256.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T122142256_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T12:11:40.000Z',
      endDate: '2020-10-31T12:21:41.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T121140379_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T121140379.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T121140379_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T12:01:38.000Z',
      endDate: '2020-10-31T12:11:38.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T120138016_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T120138016.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T120138016_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T11:51:35.000Z',
      endDate: '2020-10-31T12:01:36.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T115135978_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T115135978.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T115135978_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T11:41:33.000Z',
      endDate: '2020-10-31T11:51:35.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T114133999_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T114133999.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T114133999_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T11:31:31.000Z',
      endDate: '2020-10-31T11:41:32.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T113131954_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T113131954.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T113131954_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T11:21:32.000Z',
      endDate: '2020-10-31T11:31:32.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T112132209_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T112132209.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T112132209_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T11:11:31.000Z',
      endDate: '2020-10-31T11:21:32.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T111131852_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T111131852.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T111131852_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T11:01:32.000Z',
      endDate: '2020-10-31T11:11:32.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T110132062_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T110132062.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T110132062_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T10:51:29.000Z',
      endDate: '2020-10-31T11:01:30.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T105129845_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T105129845.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T105129845_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T10:41:29.000Z',
      endDate: '2020-10-31T10:51:29.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T104129908_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T104129908.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T104129908_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T10:31:27.000Z',
      endDate: '2020-10-31T10:41:28.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T103127940_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T103127940.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T103127940_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T10:21:28.000Z',
      endDate: '2020-10-31T10:31:28.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T102128093_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T102128093.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T102128093_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T10:11:26.000Z',
      endDate: '2020-10-31T10:21:27.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T101126142_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T101126142.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T101126142_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
    {
      startDate: '2020-10-31T10:01:24.000Z',
      endDate: '2020-10-31T10:11:26.000Z',
      thumbLargeUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T100124074_full.jpg',
      alias: 'hi-pipelinefixed',
      recordingUrl:
        'https://camrewinds.cdn-surfline.com/live/hi-pipelinefixed.stream.20201031T100124074.mp4',
      thumbSmallUrl:
        'https://camstills.cdn-surfline.com/58349ef6e411dc743a5d52cc/2020/10/31/58349ef6e411dc743a5d52cc.20201031T100124074_small.jpg',
      cameraId: '58349ef6e411dc743a5d52cc',
    },
  ],
};

export default camRewind;
