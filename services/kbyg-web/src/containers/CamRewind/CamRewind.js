import React, { useEffect, useMemo, useRef, useState } from 'react';
import useSWR from 'swr';
import classnames from 'classnames';
import {
  CamPlayerV2 as CamPlayer,
  Chevron,
  createAccessibleOnClick,
  CTAProvider,
  PageRail,
  PremiumCamCTA,
  Select,
  CamRewindIcon,
} from '@surfline/quiver-react';
import { getLocalDate, getUserPremiumStatus, slugify, trackEvent } from '@surfline/web-common';
import { useRouter } from 'next/router';
import { useStore, useDispatch } from 'react-redux';

import { NOT_FOUND } from '../../actions/status';
import { fetchCamRewind, fetchSpotsForCam } from '../../common/api/cameras';
import { usePageCall } from '../../common/hooks/usePageCall';
import CameraClips from '../../components/CameraClips';
import PageLoading from '../../components/PageLoading';
import MetaTags from '../../components/MetaTags';
import midnightOfDate, { secondsPerDay } from '../../utils/midnightOfDate';
import CamAngleSelector from '../../components/CamAngleSelector';
import { DownloadIcon } from './DownloadIcon';
import config from '../../config';
import { useSpotIsFavorite } from '../../hooks/useSpotIsFavorite';
import { getActiveMeter } from '../../selectors/meter';
import useMetering from '../../common/hooks/useMetering';

import styles from './CamRewind.module.scss';

const namedDays = ['Today', 'Yesterday'];

const CamAngleSelectorClasses = (focused) =>
  classnames({
    [styles.camRewindHeadingLinksRight]: true,
    [styles.camRewindHeadingLinksFocused]: focused,
  });

const CamRewind = () => {
  const router = useRouter();

  const [dateOptions, setDateOptions] = useState([]);
  const [currentDate, setCurrentDate] = useState(0);
  const [daysAgo, setDaysAgo] = useState(0);
  const [showCamAngleSelector, setShowCamAngleSelector] = useState(false);
  const [currentCam, setCurrentCam] = useState(null);
  const [activeClip, setActiveClip] = useState();
  const [spot, setSpot] = useState();
  const dispatch = useDispatch();

  const { camId, spotName } = router.query;

  const segmentPageParams = useMemo(
    () => ({
      category: 'cams & reports',
      channel: 'cams & reports',
      name: 'Cam Rewind',
      camId,
      title: `${spot?.name} Surf Cam Rewinds From Today and up to Five Days Ago`,
      spotName: spot?.name,
      camName: currentCam?.title,
    }),
    [camId, spot?.name, currentCam],
  );

  const isFavorite = useSpotIsFavorite(spot?._id);

  usePageCall('Cam Rewind', segmentPageParams, (camId && spot?.name && currentCam) || false);

  const { data: spotData, error: spotError } = useSWR([camId], fetchSpotsForCam, {
    revalidateOnFocus: false,
    shouldRetryOnError: false,
  });

  const store = useStore();
  const state = store.getState();
  const meter = useMetering();
  const { isMeteredPremium } = meter;
  const isPremium = getUserPremiumStatus(state) || isMeteredPremium;
  const activeMeter = getActiveMeter(state);

  const { data: clips, error: clipError } = useSWR(
    [currentCam?.alias, currentDate, isPremium, activeMeter],
    fetchCamRewind,
    {
      revalidateOnFocus: false,
      shouldRetryOnError: false,
    },
  );

  useEffect(() => {
    const matchSpotName = ({ name }) => slugify(name) === spotName;

    setSpot(spotData?.spots.find(matchSpotName) || spotData?.spots[0]);
  }, [spotName, spotData]);

  useEffect(() => {
    if (!spot) return;
    setShowCamAngleSelector(false);
    setCurrentCam(spot.cameras?.find(({ _id }) => _id === camId));
    let today = midnightOfDate(spot.timezoneOffset);

    setCurrentDate(today);
    setDateOptions(
      Array(5)
        .fill(0)
        .map((val, ind) => {
          const option = {
            value: today,
            text: namedDays[ind] || getLocalDate(today, spot.timezoneOffset).toDateString(),
          };

          today -= secondsPerDay;

          return option;
        }),
    );
  }, [camId, spot]);

  useEffect(() => {
    setDaysAgo(dateOptions?.findIndex((option) => option.value === +currentDate));
  }, [currentDate, dateOptions]);

  const daytimeClipRef = useRef();
  useEffect(() => {
    if (daysAgo > 0) {
      daytimeClipRef.current?.scrollIntoView({
        behavior: 'smooth',
        block: 'nearest',
      });
    }
  }, [daysAgo, clips]);

  const segmentPropertiesCTA = useMemo(
    () => ({
      pageName: 'Cam Rewind',
      spotId: spot?._id,
      spotName: spot?.name,
      category: 'cams & reports',
      camId,
      premiumCam: currentCam?.isPremium,
      platform: 'web',
    }),
    [spot?._id, spot?.name, camId, currentCam?.isPremium],
  );

  const segmentProperties = useMemo(
    () => ({
      ...segmentPropertiesCTA,
      daysAgo,
      camName: currentCam?.title,
    }),
    [daysAgo, currentCam?.title, segmentPropertiesCTA],
  );
  if (clipError) throw clipError;
  if (spotError) {
    if (spotError.statusCode === 400 || spotError.statusCode === 404) {
      dispatch({ type: NOT_FOUND, message: 'Spot not found' });
    } else {
      throw spotError;
    }
  }

  if (!spot || !currentCam) {
    return <PageLoading classes={{ root: styles.camRewindLoading }} />;
  }

  const camName = currentCam?.title.split(' - ')[1] || currentCam?.title;
  const camTitle = spot?.cameras && spot.cameras.length > 1 ? ` - ${camName}` : '';
  const urlPath = `/surf-cams/${slugify(spot.name)}/${camId}`;

  return (
    <div className={styles.camRewind}>
      <MetaTags
        title={`${spot.name}${camTitle} Surf Cam Rewinds From Today and up to Five Days Ago`}
        description={`Relive Your Sessions. Paddle out in front of any Surfline camera at ${spot.name}${camTitle}, surf until your arms feel like noodles, then watch video clips of all your rides via Cam Rewind or Surfline Sessions. Share your favorite rides with your friends on social media.`}
        urlPath={urlPath}
        image={currentCam.stillUrl}
      >
        <script type="application/ld+json">
          {JSON.stringify(
            {
              '@context': 'http://schema.org/',
              '@type': 'VideoObject',
              thumbnailUrl: currentCam.stillUrl,
              '@id': camId,
              name: `${spot.name}${camTitle} Cam Rewind`,
              description: `Relive Your Sessions. Paddle out in front of any Surfline camera at ${spot.name}${camTitle}, surf until your arms feel like noodles, then watch video clips of all your rides via Cam Rewind or Surfline Sessions. Share your favorite rides with your friends on social media.`,
              contentUrl: `${config.surflineHost}${urlPath}`,
              uploadDate: new Date(),
              author: {
                '@type': 'Person',
                name: 'Surfline',
              },
            },
            null,
            2,
          )}
        </script>
      </MetaTags>
      {!clips && isPremium ? (
        <PageLoading classes={{ root: styles.camRewindLoading }} />
      ) : (
        <>
          <PageRail side="left" className={styles.pageRailLeft}>
            {!isPremium ? (
              <CTAProvider segmentProperties={{ ...segmentPropertiesCTA, location: 'Cam Rewind' }}>
                <PremiumCamCTA
                  spotName={spot.name}
                  camStillUrl={currentCam.stillUrl}
                  copy={{
                    indicatorText: 'cam rewind',
                    headerText: 'Relive your session',
                    subheaderText:
                      "Download your rides – or the waves that got away – then share 'em with your friends",
                  }}
                  Icon={CamRewindIcon}
                />
              </CTAProvider>
            ) : null}
            {activeClip?.recordingUrl && isPremium ? (
              <div>
                <CamPlayer
                  playerId="sl-rewind-player"
                  key="sl-rewind-player"
                  camera={{
                    ...currentCam,
                    streamUrl: activeClip.recordingUrl,
                    stillUrl: activeClip.thumbSmallUrl,
                    status: {
                      isDown: false,
                      message: 'Cam Rewind',
                    },
                  }}
                  spotName={spot.name}
                  spotId={spot._id}
                  playbackRateControls
                  location="Cam Rewind"
                  isFavorite={isFavorite}
                  isPremium={isPremium}
                />
              </div>
            ) : null}
            {activeClip?.recordingUrl && isPremium ? (
              <div className={styles.camRewindDownloadBar}>
                <a
                  href={activeClip.recordingUrl}
                  download={`${activeClip.alias}_${activeClip.startDate}`}
                  target="_blank"
                  rel="noreferrer"
                  {...createAccessibleOnClick(() => {
                    trackEvent('Saved Clip', segmentProperties);
                    return null;
                  }, 'button')}
                >
                  <DownloadIcon />
                  <div>Right Click and Save to Download Clip</div>
                </a>
              </div>
            ) : null}
            {!activeClip?.recordingUrl && isPremium ? (
              <div className={styles.camRewindPlayerPlaceholder} />
            ) : null}
          </PageRail>
          <PageRail side="right">
            {spot ? (
              <div className={styles.camRewindRightRail}>
                <div className={styles.camRewindHeading}>
                  {showCamAngleSelector ? (
                    <CamAngleSelector
                      angles={spot?.cameras || []}
                      setVisible={setShowCamAngleSelector}
                      spotName={spot.name}
                    />
                  ) : null}
                  <div className={styles.camRewindHeadingLinks}>
                    <a
                      href={`/surf-report/${slugify(spot.name)}/${spot._id}`}
                      className={styles.camRewindHeadingsLinksLeft}
                    >
                      <Chevron direction="left" />
                      Back to live cam
                    </a>
                    {spot.cameras.length > 1 ? (
                      <div
                        {...createAccessibleOnClick((event) => {
                          event.preventDefault();
                          setShowCamAngleSelector(!showCamAngleSelector);
                        }, 'button')}
                        className={CamAngleSelectorClasses(showCamAngleSelector)}
                      >
                        Change Angle
                      </div>
                    ) : null}
                  </div>
                  <h1 className={styles.camRewindHeadingTitle}>{`${camName} Cam Rewind`}</h1>
                  <div className={styles.camRewindHeadingSelect}>
                    <Select
                      id="sl-cam-rewind-date"
                      select={{
                        disabled: false,
                        onChange: (event) => {
                          setCurrentDate(event.target.value);
                        },
                      }}
                      defaultValue={currentDate}
                      options={dateOptions}
                    />
                  </div>
                </div>
                <CameraClips
                  clips={clips || []}
                  setActiveClip={setActiveClip}
                  activeClip={activeClip}
                  utcOffset={spot.timezoneOffset}
                  isPremium={isPremium}
                  segmentProperties={segmentProperties}
                  daysAgo={daysAgo}
                />
              </div>
            ) : null}
          </PageRail>
        </>
      )}
    </div>
  );
};

export default CamRewind;
