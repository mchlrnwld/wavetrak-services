import articles from './articles';
import longTermArticle from './longTermArticle';
import seasonalArticle from './seasonalArticle';
import shortTermArticle from './shortTermArticle';

const fixtures = {
  articles,
  longTermArticle,
  seasonalArticle,
  shortTermArticle,
};

export default fixtures;
