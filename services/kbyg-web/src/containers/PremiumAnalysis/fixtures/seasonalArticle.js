const seasonalArticle = [
  {
    id: '4Hm6Ggb9i8CAmqUqmYIoE8',
    contentType: 'FORECAST',
    createdAt: 1511387848,
    updatedAt: 1511388196,
    premium: true,
    promoted: ['REGIONAL'],
    externalSource: null,
    externalLink: null,
    newWindow: false,
    content: {
      title: 'Seasonal Article Title',
      subtitle: null,
      body: 'Mock body content',
    },
    permalink: null,
    media: null,
    author: {
      name: 'Surfline Forecast Team',
      iconUrl: 'https://www.gravatar.com/avatar/39a28a9b56c17aa6005dc1bc71bcf2dd?d=mm',
    },
    tags: [],
    menuOrder: null,
    nextPageStart: '1511387848288,4Hm6Ggb9i8CAmqUqmYIoE8',
  },
];

export default seasonalArticle;
