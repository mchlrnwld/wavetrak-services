import _find from 'lodash/find';
import { premiumAnalysisLinkMap } from '../../utils/urls/subregionForecastPath';

export const availableAnalysisConfig = {
  all: {
    limit: 12,
  },
  realtimeforecast: {
    limit: 12,
  },
  shorttermforecast: {
    limit: 1,
  },
  longtermforecast: {
    limit: 1,
  },
  seasonalforecast: {
    limit: 1,
  },
};

const availableAnalysisFilters = [
  'all',
  'realtimeforecast',
  'shorttermforecast',
  'longtermforecast',
  'seasonalforecast',
];

export const tabPath = (path) => {
  const pathEnums = {
    'real-time-forecast': 'realtimeforecast',
    'short-term-forecast': 'shorttermforecast',
    'long-term-forecast': 'longtermforecast',
    'seasonal-forecast': 'seasonalforecast',
  };
  return pathEnums[path] || 'all';
};

export const getActiveNavItem = (navItems, pathname) =>
  _find(navItems, (navItem) => navItem.path === pathname);

export const buildNavItems = (params, keywords = availableAnalysisFilters) =>
  availableAnalysisFilters
    .filter((filter) => keywords.includes(filter))
    .map((tab) => ({
      name: tab === 'all' ? '' : tab,
      path: `/${premiumAnalysisLinkMap[tab].path(params.subregionId, params.subregionName)}`,
      text: premiumAnalysisLinkMap[tab].text,
      limit: availableAnalysisConfig[tab].limit,
    }));
