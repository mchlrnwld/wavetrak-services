import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ContentContainer, Alert, Spinner } from '@surfline/quiver-react';
import {
  trackNavigatedToPage,
  getUserPremiumStatus,
  getUserDetails,
  getEntitlements,
} from '@surfline/web-common';
import { withRouter } from 'next/router';
import PremiumAnalysisMeta from '../../components/PremiumAnalysisMeta';
import FeedList from '../../components/FeedList';
import withMetering from '../../common/withMetering';
import { getSubregionOverviewData } from '../../selectors/subregion';
import { subregionForecastAdBaseConfig } from '../../utils/adConfig';
import userDetailsPropType from '../../propTypes/userDetails';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';
import articlePropTypes from '../../propTypes/article';
import meterPropType from '../../propTypes/meter';
import {
  renderJWElements,
  renderTwitterElements,
  renderIgElements,
  renderCameraEmbeds,
} from '../../utils/renderCustomComponents';

class PremiumAnalysis extends Component {
  componentDidMount() {
    const {
      isPremium,
      overviewData: { name, _id },
      meter,
    } = this.props;

    const isMetered = meter?.meteringEnabled;

    trackNavigatedToPage('Premium Analysis', {
      category: 'forecast',
      channel: 'forecast',
      subregionId: _id,
      subregionName: name,
      viewedLocation: 'regional',
      meteringEnabled: meter.meteringEnabled,
    });
    renderTwitterElements();
    renderIgElements();
    renderCameraEmbeds(isPremium, isMetered);
    renderJWElements();
  }

  orderedArticles = (articles) => {
    const shorttermforecast = articles.find(
      (article) => article.keywords[0] === 'shorttermforecast',
    );
    const longtermforecast = articles.find((article) => article.keywords[0] === 'longtermforecast');
    const seasonalforecast = articles.find((article) => article.keywords[0] === 'seasonalforecast');
    return [shorttermforecast, longtermforecast, seasonalforecast].filter((article) => article);
  };

  PremiumAnalysisContent = ({
    feed,
    articles,
    overviewData,
    isPremium,
    subregionId,
    showMeterRegwall,
    subregionAdConfig,
  }) => (
    <div className="sl-premium-analysis__content">
      {feed.loading && <Spinner />}
      {feed.error && <Spinner error />}
      {feed.articles.length > 0 && !showMeterRegwall && (
        <FeedList
          articles={articles}
          subregionAdConfig={subregionAdConfig}
          isPremium={isPremium}
          subregionName={overviewData.name}
          subregionId={subregionId}
          showFullArticle
        />
      )}
      {feed.articles.length === 0 && !feed.loading && !feed.error && (
        <div className="sl-premium-analysis__message">
          <Alert>
            Premium Analysis currently unavailable for this region.
            <a href="mailto:forecast_feedback@surfline.com">Feedback?</a>
          </Alert>
        </div>
      )}
    </div>
  );

  render() {
    const { feed, isPremium, userDetails, entitlements, overviewData, meter, router } = this.props;
    const { subregionId } = router.query;
    const subregionAdConfig = subregionForecastAdBaseConfig(
      subregionId,
      entitlements,
      !!userDetails,
    );
    const { showMeterRegwall } = meter;
    const articles = feed.articles.length ? this.orderedArticles(feed.articles) : [];
    const [firstArticle] = articles;
    const headline = firstArticle?.content?.title;
    const author = firstArticle?.author?.name;

    return (
      <div className="sl-premium-analysis">
        <PremiumAnalysisMeta
          subregionName={overviewData.name}
          subregionId={subregionId}
          timestamp={overviewData.timestamp}
          headline={headline}
          author={author}
        />
        <ContentContainer>
          {this.PremiumAnalysisContent({
            feed,
            articles,
            overviewData,
            subregionId,
            isPremium,
            showMeterRegwall,
            subregionAdConfig,
          })}
        </ContentContainer>
      </div>
    );
  }
}

PremiumAnalysis.defaultProps = {
  overviewData: null,
  isPremium: null,
  userDetails: null,
  entitlements: [],
  meter: null,
};

PremiumAnalysis.propTypes = {
  isPremium: PropTypes.bool,
  userDetails: userDetailsPropType,
  entitlements: PropTypes.arrayOf(PropTypes.string),
  router: PropTypes.shape({
    query: PropTypes.shape({
      subregionId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  overviewData: overviewDataPropTypes,
  feed: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.string,
    articles: PropTypes.arrayOf(articlePropTypes),
  }).isRequired,
  meter: meterPropType,
};

export default connect((state) => ({
  overviewData: getSubregionOverviewData(state),
  isPremium: getUserPremiumStatus(state),
  userDetails: getUserDetails(state),
  entitlements: getEntitlements(state),
  feed: state.subregion.feed,
}))(withMetering(withRouter(PremiumAnalysis)));
