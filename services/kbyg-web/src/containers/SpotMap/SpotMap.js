import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';
import { getUserPremiumStatus, getWindow, canUseDOM } from '@surfline/web-common';
import { withRouter } from 'next/router';
import SpotMarker from '../../components/SpotMarker';
import SpotMarkerTooltip from '../../components/SpotMarkerTooltip';
import SpotClusterMarker from '../../components/SpotClusterMarker';
import MapBuoysButton from '../../components/MapBuoysButton';
import MapChartsButton from '../../components/MapChartsButton';
import MapLocationButton from '../../components/MapLocationButton';
import SubregionHeader from '../../components/SubregionHeader';
import { goToUsersLocation, setMapLocation, zoomIntoLocation } from '../../actions/map';
import { fetchSpotMapData } from '../../actions/spotMapData';
import { clearActiveSpot, setActiveSpot } from '../../actions/animations';
import mapPropType from '../../propTypes/map';
import animationsPropType from '../../propTypes/animations';
import {
  mapUrlSyncPropType,
  NO_SYNC,
  ALWAYS_SYNC,
  SYNC_ONLY_ON_MAP_INTERACTIONS,
} from '../../propTypes/map/mapUrlSync';
import config from '../../config';
import { showPremiumCamCTA } from '../../utils/premiumCam';

import styles from './SpotMap.module.scss';
import WavetrakLink from '../../components/WavetrakLink';
import { getBuoyMapPath, getBuoyMapPathWithLocation } from '../../utils/urls/mapPaths';

class SpotMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDragging: false,
    };
  }

  componentDidMount() {
    const { doClearActiveSpot } = this.props;
    const win = getWindow();

    if (canUseDOM && win.document.getElementById('backplane-footer')) {
      win.document.getElementById('backplane-footer').style.display = 'none';
    }

    doClearActiveSpot();

    if (canUseDOM && !/HeadlessChrome/.test(win.navigator.userAgent)) {
      // eslint-disable-next-line global-require
      this.L = require('leaflet'); // Leaflet doesn't belong server side
      setTimeout(this.configureRasterMap, 200); // sizing of frame isn't ready yet
    }
  }

  componentDidUpdate(prevProps) {
    const { doClearActiveSpot, animations } = this.props;
    const { isDragging } = this.state;

    if (
      prevProps.animations.activeSpotId &&
      prevProps.animations.activeSpotId === animations.activeSpotId
    ) {
      doClearActiveSpot();
    }

    if (this.leafletMap && !isDragging) {
      const {
        map: {
          location: { center: newCenter, zoom: newZoom },
        },
      } = this.props;
      const { lat, lng } = this.leafletMap.getCenter();
      if (
        newCenter.lat &&
        (newCenter.lat !== lat || newCenter.lon !== lng || newZoom !== this.leafletMap.getZoom())
      ) {
        this.isExternalChange = true;
        this.leafletMap.setView(this.L.latLng(newCenter.lat, newCenter.lon), newZoom);
      }
    }
  }

  componentWillUnmount() {
    if (!this.leafletMap) return;
    this.leafletMap.off('movestart', this.onMoveStart);
    this.leafletMap.off('moveend', this.onMoveEnd);
    this.leafletMap.remove();
  }

  isExternalChange = false;

  updateSpots = debounce(
    () => {
      const { doFetchSpotMapData, clusterMaxZoom } = this.props;
      const bounds = this.leafletMap.getBounds();
      if (bounds) {
        doFetchSpotMapData(
          {
            north: bounds.getNorth(),
            south: bounds.getSouth(),
            west: bounds.getWest(),
            east: bounds.getEast(),
          },
          this.leafletMap.getZoom() <= clusterMaxZoom,
        );
      }
    },
    50,
    {
      maxWait: 250,
    },
  );

  onMoveEnd = () => {
    const { lat, lng } = this.leafletMap.getCenter();
    this.setMapLocation(lat, lng, this.leafletMap.getZoom());
    this.updateSpots();
    this.setState({ isDragging: false });
    this.isExternalChange = false;
  };

  onMoveStart = () => {
    this.setState({ isDragging: true });
  };

  setMapLocation = (latitude, longitude, zoom) => {
    const { doSetMapLocation, syncUrl, storeLocationInSession } = this.props;
    doSetMapLocation(
      {
        center: {
          lat: latitude,
          lon: longitude,
        },
        zoom,
      },
      syncUrl === ALWAYS_SYNC ||
        (syncUrl === SYNC_ONLY_ON_MAP_INTERACTIONS && !this.isExternalChange),
      storeLocationInSession,
    );
  };

  configureRasterMap = () => {
    const {
      map: { location },
      mapOptions: { maxZoom, minZoom },
    } = this.props;
    this.leafletMap = this.L.map('rasterMap', {
      center: [location.center.lat || 0, location.center.lon || 0],
      zoom: location.zoom,
      maxZoom,
      minZoom,
      preferCanvas: true,
      worldCopyJump: true,
      renderer: this.L.canvas(),
      zoomAnimation: this.L.Browser.webkit, // You could enable this for other browsers
      fadeAnimation: this.L.Browser.webkit,
      markerZoomAnimation: this.L.Browser.webkit,
    });
    this.leafletMap.on('moveend', this.onMoveEnd);
    this.leafletMap.on('movestart', this.onMoveStart);
    this.L.tileLayer(`${config.mapTileUrl}/256/{z}/{x}/{y}.png?key=${config.mapTileKey}`, {
      detectRetina: true,
      maxZoom,
      minZoom,
      tileSize: 256,
    }).addTo(this.leafletMap);
    this.L.control
      .attribution({ prefix: false })
      .addAttribution(
        '<a href="https://www.maptiler.com/copyright" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap</a> contributors',
      )
      .addTo(this.leafletMap);
    this.updateSpots();
  };

  findSubregionById = (id) => {
    const { map } = this.props;
    const subregionMap = new Map(
      map.subregions.subregions.map((subregion) => [subregion._id, subregion.subregion]),
    );
    return subregionMap.get(id) || null;
  };

  drawSpotOverlay = () => {
    const {
      map: { spots, clusters },
      units,
      animations,
      doZoomIntoLocation,
      doSetActiveSpot,
      doClearActiveSpot,
      onClickSpotMarker,
      syncUrl,
      currentSpotId,
      isPremium,
    } = this.props;

    const getMarkerStyle = (lat, lon, spotId) => {
      const style = {};
      if (this.leafletMap) {
        const point = this.leafletMap.latLngToContainerPoint(
          this.L.latLng(lat, lon),
          this.leafletMap.getZoom(),
        );
        style.left = point.x || 0;
        style.top = point.y || 0;
        if (spotId && animations.activeSpotId === spotId) {
          style.zIndex = 99;
        }
        if (spotId && currentSpotId === spotId) {
          style.zIndex = 98;
        }
      }
      if (style.left <= 0 || !style.left || style.top <= 0 || !style.top) {
        // Why doesn't overflow: hidden work for the pan events?
        style.display = 'none';
      }
      return style;
    };

    const getTooltipPosition = ({ N, S, E, W }, { lat, lon }) => {
      const vertical = lat <= S + (3 / 5) * (N - S) ? 'top' : 'bottom';
      let horizontal;
      if (lon < (E - W) / 5 + W) {
        horizontal = 'right';
      } else if (lon < (E - W) * (4 / 5) + W) {
        horizontal = 'center';
      } else {
        horizontal = 'left';
      }
      return { vertical, horizontal };
    };

    return (
      <div>
        {clusters.length
          ? clusters.map((cluster) => {
              const [lon, lat] = cluster.geometry.coordinates;
              const { spotCount, worstCondition, bestCondition } = cluster.properties;
              return (
                <div
                  className={styles.spotMapOverlayMarker}
                  style={getMarkerStyle(lat, lon)}
                  key={`${lat},${lon}`}
                >
                  <SpotClusterMarker
                    lat={lat}
                    lng={lon}
                    spotCount={spotCount}
                    worstCondition={worstCondition}
                    bestCondition={bestCondition}
                    onClick={() => doZoomIntoLocation({ lat, lon }, syncUrl !== NO_SYNC)}
                  />
                </div>
              );
            })
          : spots.map((spot) => {
              const { lat, lon } = spot;
              const bounds = this.leafletMap.getBounds();
              const tooltipPosition = getTooltipPosition(
                {
                  N: bounds.getNorth(),
                  E: bounds.getEast(),
                  S: bounds.getSouth(),
                  W: bounds.getWest(),
                },
                spot,
              );
              return (
                <div
                  className={styles.spotMapOverlayMarker}
                  style={getMarkerStyle(lat, lon, spot._id)}
                  key={`${lat},${lon}`}
                >
                  <SpotMarker
                    key={spot._id}
                    lat={spot.lat}
                    lng={spot.lon}
                    surf={{
                      min: spot.waveHeight.min,
                      max: spot.waveHeight.max,
                      units: units.waveHeight,
                    }}
                    conditions={spot.conditions.value || 'LOTUS'}
                    hasCamera={spot.cameras.length > 0}
                    spotId={spot._id}
                    activeSpotId={animations.activeSpotId}
                    highlight={animations.activeSpotId === spot._id || currentSpotId === spot._id}
                    onMouseEnter={() => doSetActiveSpot(spot._id)}
                    onMouseLeave={() => doClearActiveSpot()}
                    onClick={() => onClickSpotMarker(spot)}
                    current={currentSpotId === spot._id}
                  />
                  {animations.activeSpotId === spot._id ? (
                    <SpotMarkerTooltip
                      spot={spot}
                      units={units}
                      position={tooltipPosition}
                      showPremiumCamCTA={showPremiumCamCTA({
                        spot,
                        isPremium,
                      })}
                    />
                  ) : null}
                </div>
              );
            })}
      </div>
    );
  };

  getClass = () =>
    classNames({
      [styles.spotMap]: true,
    });

  render() {
    const {
      map: {
        regionalForecast: { subregionId },
        location: { center },
      },
      showBottomControls,
      doGoToUsersLocation,
      router,
    } = this.props;
    const { location } = router.query;
    const { isDragging } = this.state;
    const subregion = this.findSubregionById(subregionId);
    return (
      <div className={this.getClass()}>
        <div id="rasterMap" />
        {this.leafletMap && !isDragging ? this.drawSpotOverlay() : <div />}
        {subregion && <SubregionHeader subregion={subregion} map />}
        {showBottomControls ? (
          <div>
            <div className={styles.spotMapBottomLeft}>
              <WavetrakLink
                href={location ? getBuoyMapPathWithLocation(location) : getBuoyMapPath()}
              >
                <MapBuoysButton />
              </WavetrakLink>
              <a href={`/surf-charts/?lat=${center.lat}&lon=${center.lon}`}>
                <MapChartsButton />
              </a>
            </div>
            <div className={styles.spotMapBottomRight}>
              <MapLocationButton onClick={doGoToUsersLocation} />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

SpotMap.propTypes = {
  map: mapPropType.isRequired,
  units: PropTypes.shape({
    waveHeight: PropTypes.string,
  }).isRequired,
  animations: animationsPropType.isRequired,
  mapOptions: PropTypes.shape({
    maxZoom: PropTypes.number,
    minZoom: PropTypes.number,
  }),
  showBottomControls: PropTypes.bool,
  clusterMaxZoom: PropTypes.number,
  doSetMapLocation: PropTypes.func.isRequired,
  doFetchSpotMapData: PropTypes.func.isRequired,
  doGoToUsersLocation: PropTypes.func.isRequired,
  doZoomIntoLocation: PropTypes.func.isRequired,
  doSetActiveSpot: PropTypes.func.isRequired,
  doClearActiveSpot: PropTypes.func.isRequired,
  onClickSpotMarker: PropTypes.func.isRequired,
  syncUrl: mapUrlSyncPropType,
  storeLocationInSession: PropTypes.bool,
  currentSpotId: PropTypes.string,
  isPremium: PropTypes.bool,
  router: PropTypes.shape({
    push: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
    query: PropTypes.shape(),
  }).isRequired,
};

SpotMap.defaultProps = {
  clusterMaxZoom: 0,
  mapOptions: {
    maxZoom: 18,
    minZoom: 4,
  },
  showBottomControls: false,
  syncUrl: SYNC_ONLY_ON_MAP_INTERACTIONS,
  storeLocationInSession: false,
  currentSpotId: null,
  isPremium: false,
};

export default withRouter(
  connect(
    (state) => ({
      map: state.map,
      units: state.units,
      animations: state.animations,
      isPremium: getUserPremiumStatus(state),
    }),
    (dispatch, props) => ({
      doSetMapLocation: (location, syncUrl, storeLocationInSession) =>
        dispatch(setMapLocation(location, props.router, syncUrl, storeLocationInSession)),
      doFetchSpotMapData: (bounds, doCluster) => dispatch(fetchSpotMapData(bounds, doCluster)),
      doGoToUsersLocation: () =>
        dispatch(goToUsersLocation(props.router, props.syncUrl !== NO_SYNC)),
      doZoomIntoLocation: (center, syncUrl) =>
        dispatch(zoomIntoLocation(center, props.router, syncUrl)),
      doSetActiveSpot: (spotId) => dispatch(setActiveSpot(spotId)),
      doClearActiveSpot: () => dispatch(clearActiveSpot()),
    }),
    null,
    { forwardRef: true },
  )(SpotMap),
);
