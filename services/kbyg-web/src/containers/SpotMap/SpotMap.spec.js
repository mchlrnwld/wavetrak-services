import { expect } from 'chai';
import { act } from 'react-dom/test-utils';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import spotsFixture from './fixtures/spots.json';
import clustersFixture from './fixtures/clusters.json';
import MapButton from '../../components/MapButton';
import MapLocationButton from '../../components/MapLocationButton';
import MapBuoysButton from '../../components/MapBuoysButton';
import MapChartsButton from '../../components/MapChartsButton';
import SpotClusterMarker from '../../components/SpotClusterMarker';
import SpotMarker from '../../components/SpotMarker';
import * as animationActions from '../../actions/animations';
import * as mapActions from '../../actions/map';
import * as spotMapDataActions from '../../actions/spotMapData';
import * as splitSelectors from '../../selectors/split';
import SpotMap from './SpotMap';
import { NO_SYNC, ALWAYS_SYNC } from '../../propTypes/map/mapUrlSync';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('containers / SpotMap', () => {
  let mockStore;
  const defaultState = {
    units: {},
    map: {
      location: {
        center: {},
      },
      clusters: [],
      spots: [],
      subregions: {
        subregions: [],
      },
      regionalForecast: {},
    },
    animations: {},
    backplane: {
      user: {
        entitlements: [],
      },
      meter: {},
    },
  };

  const leafletMap = {
    latLngToContainerPoint: () => ({ x: 1, y: 1 }),
    getZoom: () => {},
    getBounds: () => ({
      getNorth: () => {},
      getSouth: () => {},
      getEast: () => {},
      getWest: () => {},
    }),
    getCenter: () => ({ lat: 1, lng: 1 }),
    on: () => {},
    addTo: () => {},
  };

  leafletMap.addAttribution = () => leafletMap;

  const L = {
    latLng: () => {},
    map: () => leafletMap,
    tileLayer: () => ({ addTo: () => {} }),
    canvas: () => {},
    Browser: { webkit: true },
    control: {
      attribution: () => leafletMap,
    },
  };

  beforeAll(() => {
    mockStore = configureStore();
  });

  beforeEach(() => {
    sinon.stub(animationActions, 'setActiveSpot').returns({ type: 'setActiveSpot was called' });
    sinon.stub(animationActions, 'clearActiveSpot').returns({ type: 'clearActiveSpot was called' });
    sinon.stub(animationActions, 'scrollToSpot').returns({ type: 'scrollToSpot' });
    sinon.stub(mapActions, 'setMapLocation').returns({ type: 'setMapLocation was called' });
    sinon.stub(mapActions, 'goToUsersLocation').returns({ type: 'goToUsersLocation was called' });
    sinon.stub(mapActions, 'zoomIntoLocation').returns({ type: 'zoomIntoLocation was called' });
    sinon.stub(L, 'map').returns(leafletMap);
    sinon.stub(spotMapDataActions, 'fetchSpotMapData').returns({
      type: 'fetchSpotMapData was called',
    });
    sinon.stub(splitSelectors, 'useSplitTreatment').returns({ loading: false, treatment: null });
  });

  afterEach(() => {
    animationActions.setActiveSpot.restore();
    animationActions.clearActiveSpot.restore();
    animationActions.scrollToSpot.restore();
    mapActions.setMapLocation.restore();
    mapActions.goToUsersLocation.restore();
    mapActions.zoomIntoLocation.restore();
    spotMapDataActions.fetchSpotMapData.restore();
    splitSelectors.useSplitTreatment.restore();
    L.map.restore();
  });

  it('dispatches events when movement ends', () => {
    const center = {
      lat: 33.654213041213,
      lon: -118.0032588192,
    };
    const zoom = 11;
    const bounds = {
      north: 33.707209069466316,
      south: 33.60118435784217,
      east: -117.86936294517659,
      west: -118.13715469322347,
    };
    const store = mockStore(defaultState);

    const props = {
      syncUrl: NO_SYNC,
      clusterMaxZoom: 12,
      store,
    };
    const { wrapper, router } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = wrapper.find('SpotMap').instance();

    spotMap.leafletMap = leafletMap;
    const zoomStub = sinon.stub(leafletMap, 'getZoom').returns(zoom);
    const boundsStub = sinon.stub(leafletMap, 'getBounds').returns({
      getNorth: () => bounds.north,
      getSouth: () => bounds.south,
      getEast: () => bounds.east,
      getWest: () => bounds.west,
    });
    const centerStub = sinon
      .stub(leafletMap, 'getCenter')
      .returns({ lat: center.lat, lng: center.lon });
    spotMap.onMoveEnd();
    spotMap.updateSpots.flush();

    expect(zoomStub).to.have.been.calledTwice();
    expect(boundsStub).to.have.been.calledOnce();
    expect(centerStub).to.have.been.calledTwice();
    expect(mapActions.setMapLocation).to.have.been.calledOnce();
    expect(mapActions.setMapLocation).to.have.been.calledWithExactly(
      { center, zoom },
      router,
      false,
      false,
    );
    expect(spotMapDataActions.fetchSpotMapData).to.have.been.calledOnce();
    expect(spotMapDataActions.fetchSpotMapData).to.have.been.calledWithExactly(bounds, true);

    zoomStub.restore();
    boundsStub.restore();
    centerStub.restore();
  });

  it('initializes map with location and zoom from props', () => {
    const center = deepFreeze({
      lat: 33.654213041213,
      lon: -118.0032588192,
    });
    const zoom = 12;
    const store = mockStore({
      ...defaultState,
      map: {
        ...defaultState.map,
        location: {
          ...defaultState.map.location,
          center,
          zoom,
        },
      },
    });
    const props = {
      syncUrl: ALWAYS_SYNC,
      store,
    };

    const { wrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = wrapper.find('SpotMap').instance();
    spotMap.L = L;
    spotMap.configureRasterMap();
    expect(L.map).to.have.been.calledWith('rasterMap', {
      center: [center.lat, center.lon],
      zoom,
      maxZoom: 18,
      minZoom: 4,
      preferCanvas: true,
      worldCopyJump: true,
      renderer: L.canvas(),
      zoomAnimation: L.Browser.webkit,
      fadeAnimation: L.Browser.webkit,
      markerZoomAnimation: L.Browser.webkit,
    });
    spotMap.updateSpots.flush();
  });

  it('handles clicking on spot markers', () => {
    const store = mockStore({
      ...defaultState,
      map: {
        ...defaultState.map,
        spots: spotsFixture,
      },
    });
    const onClickSpotMarker = sinon.spy();
    const props = {
      syncUrl: ALWAYS_SYNC,
      store,
      onClickSpotMarker,
    };
    const { wrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = wrapper.find('SpotMap').instance();
    spotMap.leafletMap = leafletMap;
    spotMap.L = L;
    const overlay = spotMap.drawSpotOverlay();
    const overlayMounted = mount(overlay);
    const spotMarkers = overlayMounted.find(SpotMarker);
    spotMarkers.at(1).prop('onClick')();
    expect(onClickSpotMarker).to.have.been.calledOnce();
    expect(onClickSpotMarker).to.have.been.calledWithExactly(spotsFixture[1]);
    spotMap.updateSpots.flush();
  });

  it('renders MapLocationButton, MapChartsButton, and MapBuoysButton if showBottomControls is true', () => {
    const store = mockStore(defaultState);
    let props = {
      store,
    };
    const { wrapper: defaultWrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = defaultWrapper.find('SpotMap').instance();
    expect(defaultWrapper.find(MapLocationButton)).to.have.length(0);
    expect(defaultWrapper.find(MapBuoysButton)).to.have.length(0);
    expect(defaultWrapper.find(MapChartsButton)).to.have.length(0);

    props = {
      ...props,
      showBottomControls: true,
    };
    const { wrapper: showBottomControlsWrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    expect(showBottomControlsWrapper.find(MapLocationButton)).to.have.length(1);
    expect(showBottomControlsWrapper.find(MapBuoysButton)).to.have.length(1);
    expect(showBottomControlsWrapper.find(MapChartsButton)).to.have.length(1);
    spotMap.updateSpots.flush();
  });

  it("goes to user's location when MapLocationButton is clicked", () => {
    const store = mockStore(defaultState);
    const props = {
      showBottomControls: true,
    };
    const { wrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = wrapper.find('SpotMap').instance();
    const mapLocationButton = wrapper.find(MapLocationButton);
    const mapButton = mapLocationButton.find(MapButton);
    mapButton.simulate('click');
    expect(mapActions.goToUsersLocation).to.have.been.calledOnce();
    spotMap.updateSpots.flush();
  });

  it('renders SpotMarkers from map props', () => {
    const store = mockStore({
      ...defaultState,
      map: {
        ...defaultState.map,
        spots: spotsFixture,
      },
    });
    const props = {
      syncUrl: ALWAYS_SYNC,
      store,
    };
    const { wrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = wrapper.find('SpotMap').instance();
    spotMap.leafletMap = leafletMap;
    spotMap.L = L;
    const overlay = spotMap.drawSpotOverlay();
    const overlayMounted = mount(overlay);
    expect(overlayMounted.find(SpotMarker)).to.have.length(3);
    spotMap.updateSpots.flush();
  });

  it('renders SpotClusterMarkers from map props', () => {
    const store = mockStore({
      ...defaultState,
      map: {
        ...defaultState.map,
        clusters: clustersFixture,
      },
    });
    const props = {
      syncUrl: ALWAYS_SYNC,
      store,
    };
    const { wrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });
    const spotMap = wrapper.find('SpotMap').instance();
    spotMap.leafletMap = leafletMap;
    spotMap.L = L;
    const overlay = spotMap.drawSpotOverlay();
    const overlayMounted = shallow(overlay);
    expect(overlayMounted.find(SpotClusterMarker)).to.have.length(3);
    spotMap.updateSpots.flush();
  });

  it('zooms into SpotClusterMarker location when SpotClusterMarker is clicked', () => {
    const store = mockStore({
      ...defaultState,
      map: {
        ...defaultState.map,
        clusters: clustersFixture,
      },
    });
    const props = {
      syncUrl: ALWAYS_SYNC,
      store,
    };
    const { wrapper, router } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });

    const spotMap = wrapper.find('SpotMap').instance();

    spotMap.leafletMap = leafletMap;
    spotMap.L = L;

    const overlay = spotMap.drawSpotOverlay();
    const overlayMounted = mount(overlay);
    const spotMarkers = overlayMounted.find(SpotClusterMarker);

    act(() => {
      spotMarkers.at(1).prop('onClick')();
    });

    expect(mapActions.zoomIntoLocation).to.have.been.calledOnce();
    expect(mapActions.zoomIntoLocation).to.have.been.calledWithExactly(
      {
        lat: clustersFixture[1].geometry.coordinates[1],
        lon: clustersFixture[1].geometry.coordinates[0],
      },
      router,
      true,
    );
    spotMap.updateSpots.flush();
  });

  it('sets active spot when hovering over a SpotMarker', () => {
    const store = mockStore({
      ...defaultState,
      map: {
        ...defaultState.map,
        spots: spotsFixture,
      },
    });
    const props = {
      syncUrl: ALWAYS_SYNC,
      store,
    };
    const { wrapper } = mountWithReduxAndRouter(SpotMap, props, {
      initialState: defaultState,
      store,
    });

    const spotMap = wrapper.find('SpotMap').instance();

    spotMap.leafletMap = leafletMap;
    spotMap.L = L;

    const overlay = spotMap.drawSpotOverlay();
    const overlayMounted = mount(overlay);
    const spotMarkers = overlayMounted.find(SpotMarker);
    const activeSpot = spotMarkers.at(1);

    expect(animationActions.clearActiveSpot).to.have.been.calledOnce();

    activeSpot.simulate('mouseenter');
    expect(animationActions.setActiveSpot).to.have.been.calledOnce();
    expect(animationActions.setActiveSpot).to.have.been.calledWithExactly(spotsFixture[1]._id);

    activeSpot.simulate('mouseleave');
    expect(animationActions.setActiveSpot).to.have.been.calledOnce();
    expect(animationActions.clearActiveSpot).to.have.been.calledTwice();
    spotMap.updateSpots.flush();
  });
});
