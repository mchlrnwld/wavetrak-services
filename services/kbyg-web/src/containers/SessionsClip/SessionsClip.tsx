import { ContentContainer, GoogleDFP } from '@surfline/quiver-react';
import { getEntitlements, trackNavigatedToPage } from '@surfline/web-common';
import { useRouter } from 'next/router';
import React from 'react';
import { useMount } from 'react-use';

import config from '../../config';
import { SessionClip } from '../../types/sessions';
import useMetering from '../../common/hooks/useMetering';
import ClipFooter from '../../components/ClipFooter';
import ClipHeader from '../../components/ClipHeader';
import MetaTags from '../../components/MetaTags';
import SessionClipPlayer from '../../components/SessionClipPlayer';
import { useAppSelector } from '../../stores/hooks';
import loadAdConfig from '../../utils/adConfig';

import styles from './SessionsClip.module.scss';

const { surflineHost } = config;

interface Props {
  sessionClip: SessionClip;
}

const SessionsClip: React.FC<Props> = ({ sessionClip }) => {
  const meter = useMetering();
  useMount(() => {
    document.documentElement.style.overflowX = 'hidden';
    document.documentElement.style.overflowY = 'auto';

    if (sessionClip) {
      const { spot } = sessionClip;
      trackNavigatedToPage('Sessions Clip Share', {
        category: 'sessions',
        spotName: spot.name,
        spotId: spot.id,
        meteringEnabled: meter.meteringEnabled,
      });
    }
  });

  const router = useRouter();
  const entitlements = useAppSelector(getEntitlements);
  const currentUrl = `${surflineHost}${router.asPath}`;

  const name = sessionClip.user?.firstName;
  const title = `${name || 'Surfline Sessions User'}'s wave from ${sessionClip.spot.name}`;
  const { thumbnail } = sessionClip.wave.clip;

  return (
    <div className={styles.clipContainer}>
      <MetaTags
        urlPath={`/sessions/clips/${router.query.clipId}`}
        title={title}
        description="Relive your surf sessions and analyze your performance. Pair your iOS device with an Apple Watch or other compatible GPS device to receive individual video clips of your rides from Surfline Cams, and detailed performance data such as top speed, number of waves ridden, longest ride and more."
        image={thumbnail?.url?.replace('{size}', thumbnail.sizes[2])}
      />
      <ContentContainer>
        <div className={styles.clipContent}>
          <ClipHeader
            firstName={sessionClip.user.firstName}
            spotName={sessionClip.spot.name}
            url={currentUrl}
          />
          <SessionClipPlayer session={sessionClip} clip={sessionClip.wave.clip} />
          <ClipFooter session={sessionClip} />
        </div>
      </ContentContainer>
      <div className={styles.footerAd}>
        <GoogleDFP
          unifiedAdMarketplace
          adConfig={loadAdConfig('sessionClipBottom', [['sessionClip']], entitlements)}
        />
      </div>
    </div>
  );
};

export default SessionsClip;
