import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { WebMercatorViewport } from 'viewport-mercator-project';
import { Spinner } from '@surfline/quiver-react';
import { slugify, kbygPaths } from '@surfline/web-common';
import { withRouter } from 'next/router';
import classNames from 'classnames';
import SpotMap from '../SpotMap';
import SpotList from '../SpotList';
import GeonameMapPageMeta from '../../components/GeonameMapPageMeta';
import { scrollToSpot } from '../../actions/animations';
import { setMapLocation } from '../../actions/map';

import locationViewPropShape from '../../propTypes/locationView';
import spotPropShape from '../../propTypes/spot';
import { SYNC_ONLY_ON_MAP_INTERACTIONS } from '../../propTypes/map/mapUrlSync';
import mapOptions from '../../utils/mapOptions';
import { camsAndForecastsPaths } from '../../utils/urls/camsAndForecastsPath';
import { getMapPageWidth } from '../../utils/dimensions';
import styles from './GeonameMap.module.scss';

const { spotReportPath } = kbygPaths;

const getRightRailClass = () => classNames(styles.slRightRail, 'sl-right-rail');
const getLeftRailClass = () => classNames(styles.slLeftRail, 'sl-left-rail');

class GeonameMap extends Component {
  componentDidMount() {
    const { locationView } = this.props;
    this.fitBoundsAndSetMapLocation(locationView.boundingBox);
  }

  componentDidUpdate(prevProps) {
    const { locationView } = this.props;

    /* set map location on bounding box change */
    if (locationView.boundingBox !== prevProps.locationView.boundingBox) {
      this.fitBoundsAndSetMapLocation(locationView.boundingBox);
    }
  }

  fitBoundsAndSetMapLocation = (boundingBox) => {
    const { doSetMapLocation } = this.props;

    const height = this.mapRef.clientHeight ? this.mapRef.clientHeight : 1000;
    const width = this.mapRef.clientWidth ? this.mapRef.clientWidth : 1000;
    const { north: lat0, west: lon0, south: lat1, east: lon1 } = boundingBox;
    const viewport = new WebMercatorViewport({ width, height }).fitBounds([
      [lon0, lat0],
      [lon1, lat1],
    ]);
    doSetMapLocation({
      center: { lat: viewport.latitude, lon: viewport.longitude },
      zoom: viewport.zoom,
    });
  };

  goToSpot = (spot) => {
    const { router } = this.props;
    return router.push(spotReportPath(slugify(spot.name), spot._id));
  };

  render() {
    const { locationView, spots, loadingSpots, doScrollToSpot } = this.props;
    const urlPath =
      locationView && locationView.url && `${camsAndForecastsPaths.base}${locationView.url}`;
    return (
      <div className={styles.slMapPage} style={{ height: 'calc(100vh - 96px)' }}>
        <GeonameMapPageMeta name={locationView.taxonomy.name || ''} urlPath={urlPath} />
        <div
          className={getRightRailClass()}
          ref={(ref) => {
            this.mapRef = ref;
          }}
        >
          <SpotMap
            mapOptions={mapOptions}
            showBottomControls
            clusterMaxZoom={11}
            syncUrl={SYNC_ONLY_ON_MAP_INTERACTIONS}
            storeLocationInSession
            onClickSpotMarker={getMapPageWidth() < 992 ? doScrollToSpot : this.goToSpot}
          />
        </div>
        <div className={getLeftRailClass()}>
          {!locationView.loading && !loadingSpots && spots.length > 0 ? (
            <div>
              <SpotList locationView={locationView} />
            </div>
          ) : (
            <div className={styles.spinner}>
              <Spinner />
            </div>
          )}
        </div>
      </div>
    );
  }
}

GeonameMap.propTypes = {
  doScrollToSpot: PropTypes.func.isRequired,
  doSetMapLocation: PropTypes.func.isRequired,
  locationView: locationViewPropShape,
  spots: PropTypes.arrayOf(spotPropShape),
  loadingSpots: PropTypes.bool,
};

GeonameMap.defaultProps = {
  locationView: {
    boundingBox: {},
    taxonomy: {
      name: null,
      location: {
        coordinates: [],
      },
    },
  },
  spots: [],
  loadingSpots: false,
};

export default withRouter(
  connect(
    (state) => ({
      locationView: state.map.locationView,
      spots: state.map.spots,
      loadingSpots: state.status.loading,
    }),
    (dispatch, props) => ({
      doSetMapLocation: (location) => dispatch(setMapLocation(location, props.router, false, true)),
      doScrollToSpot: (spot) => dispatch(scrollToSpot(spot._id)),
    }),
  )(GeonameMap),
);
