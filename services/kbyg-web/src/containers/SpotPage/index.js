import { withDevice } from '@surfline/quiver-react';
import SpotPage from './SpotPage';

export default withDevice(SpotPage);
