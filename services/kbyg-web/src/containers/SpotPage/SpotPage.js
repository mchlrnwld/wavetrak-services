import React, { Component } from 'react';
import { ContentContainer, SwellEventCardContainer } from '@surfline/quiver-react';
import {
  getUserDetails,
  getUserFavorites,
  getUserPremiumStatus,
  getUserSettings,
  getWindow,
  slugify,
  TABLET_LARGE_WIDTH,
  trackEvent,
  trackNavigatedToPage,
  canUseDOM,
  getTreatment,
} from '@surfline/web-common';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withRouter } from 'next/router';
import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { fetchOverviewData } from '../../actions/subregion';
import { fetchSwellEvents } from '../../actions/swellEvents';
import {
  doFavoriteSpot,
  doSetRecentlyVisited as setRecentlyVisited,
  doUnfavoriteSpot,
} from '../../actions/user';
import withMetering from '../../common/withMetering';
import FeaturedContent from '../../components/FeaturedContent';
import ForecastHeader from '../../components/ForecastHeader';
import IdealConditions from '../../components/IdealConditions';
import NearbyBuoys from '../../components/NearbyBuoys';
import PageLoading from '../../components/PageLoading';
import SpotPageMeta from '../../components/SpotPageMeta';
import SpotTravelInfo from '../../components/SpotTravelInfo';
import config from '../../config';
import en from '../../intl/translations/en';
import articlePropTypes from '../../propTypes/article';
import devicePropTypes from '../../propTypes/device';
import featuredContentPropType from '../../propTypes/featuredContent';
import meterPropType from '../../propTypes/meter';
import multiCamPropType from '../../propTypes/multiCam';
import spotReportDataPropTypes from '../../propTypes/spot';
import spotAssociatedPropTypes from '../../propTypes/spotAssociated';
import spotIdealConditionsPropShape from '../../propTypes/spotIdealConditions';
import swellEventsPropType from '../../propTypes/swellEvents';
import travelDetailsPropTypes from '../../propTypes/travelDetails';
import userDetailsPropType from '../../propTypes/userDetails';
import userFavoritesPropType from '../../propTypes/userFavorites';
import userSettingsPropTypes from '../../propTypes/userSettings';
import {
  getAdvertisingIds,
  getFeaturedContent,
  getNearbySpots,
  getSpotBuoys,
  getSpotDetails,
  getSpotIdealConditions,
  getSpotLoading,
  getSpotMultiCam,
  getSpotReport,
  getSpotReportAssociated,
  getSpotReportData,
  getTravelDetails,
} from '../../selectors/spot';
import {
  getSubregionAssociated,
  getSubregionOverviewFetched,
  getSubregionOverviewLoading,
} from '../../selectors/subregion';
import getSwellEvents from '../../selectors/swellEvents';
import ForecastDataVisualizer from '../ForecastDataVisualizer';
import SpotCurrentConditionsSection from '../SpotCurrentConditionsSection';

/** @type {React.Component<{spotId: string}>} */
class SpotPage extends Component {
  componentDidMount() {
    const {
      spotId,
      userDetails,
      spot,
      doSetRecentlyVisited,
      doFetchsubregionAssociated,
      doFetchSpotSwellEvents,
      reportData,
      isPremium,
      meter,
      subregionOverviewFetched,
      conditionsTreatment,
    } = this.props;

    if (spot && reportData) {
      this.timer = setTimeout(() => {
        const { device } = this.props;
        this.handleNavigatedToPage(spotId, spot, reportData, device, isPremium, meter);
      }, 0);
    }

    const sevenRatings = conditionsTreatment === 'on';
    if (userDetails) {
      doSetRecentlyVisited(spotId, spot.name);
    }
    if (spot && !subregionOverviewFetched) {
      doFetchsubregionAssociated(spot.subregion._id, sevenRatings);
    }

    doFetchSpotSwellEvents(spotId);
  }

  componentDidUpdate(prevProps) {
    const {
      spotId,
      meter,
      userDetails,
      spot,
      doSetRecentlyVisited,
      doFetchsubregionAssociated,
      reportData,
      device,
      isPremium,
      conditionsTreatment,
    } = this.props;
    const sevenRatings = conditionsTreatment === 'on';

    if (userDetails && spot && spot !== prevProps.spot) {
      doSetRecentlyVisited(spotId, spot.name);
    }

    if (spot && !prevProps.spot) {
      doFetchsubregionAssociated(spot.subregion._id, sevenRatings);
    }

    if (spotId !== prevProps.spotId) {
      // Have to use setTimeout here so that the title property is accurate
      // Without this, the event fires before react-helmet updates the title tag
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(() => {
        this.handleNavigatedToPage(spotId, spot, reportData, device, isPremium, meter);
      }, 0);
    }
  }

  handleNavigatedToPage = (spotId, spot, reportData, device, isPremium, meter) => {
    const getCamProps = () => {
      const hasCam = !!spot.cameras.length;
      if (!hasCam) return { hasCam };
      const isSingleCam = !(isPremium && !device.mobile && spot.cameras.length > 1);
      return {
        hasCam,
        isSingleCam,
        premiumCam: spot.cameras[0].isPremium,
        // return first camId if single cam view or list of all spot camIds if multi cam
        camId: isSingleCam ? spot.cameras[0]._id : spot.cameras.map((cam) => cam._id).join(),
        camNamePrimary: spot.cameras[0].title,
      };
    };

    trackNavigatedToPage('Spot Report', {
      category: 'cams & reports',
      spotId,
      spotName: spot.name,
      subregionId: spot.subregion._id,
      condition: reportData.conditions.value,
      human: reportData.conditions.human,
      waveHeightMin: reportData.waveHeight.min,
      waveHeightMax: reportData.waveHeight.max,
      abilityLevels:
        spot.abilityLevels && spot.abilityLevels.length ? spot.abilityLevels.join() : null,
      boardTypes: spot.boardTypes && spot.boardTypes.length ? spot.boardTypes.join() : null,
      isMobileView: device.width <= TABLET_LARGE_WIDTH,
      ...getCamProps(),
      meteringEnabled: meter.meteringEnabled,
    });
  };

  spotFavorited = (favorites, spotId) => !!favorites.find((favorite) => favorite._id === spotId);

  handleFavoriteClick = (favorited, spotId) => {
    const { savingFavorite, userDetails, doToggleFavorite } = this.props;
    if (!savingFavorite && !!userDetails) {
      doToggleFavorite(!favorited, spotId);
    } else if (canUseDOM) {
      const win = getWindow();
      win.location.href = `${
        config.createAccountUrl
      }?alert=noAuthFav&redirectUrl=${encodeURIComponent(win.location)}`;
    }
  };

  renderCombinedPageLevelLinks = () => {
    const { spotAssociated, feed } = this.props;
    const pageLinks = [
      {
        text: en.SpotPage.externalLinkText.region,
        path: spotAssociated.subregionUrl,
        newWindow: false,
      },
    ];
    if (feed.articles.length > 0) {
      pageLinks.push({
        text: en.SpotPage.externalLinkText.premiumAnalysis,
        path: `${spotAssociated.subregionUrl}/premium-analysis`,
        newWindow: false,
      });
    }
    if (spotAssociated.chartsUrl) {
      pageLinks.push({
        text: en.SpotPage.externalLinkText.charts,
        path: spotAssociated.chartsUrl,
        newWindow: false,
      });
    }
    return pageLinks;
  };

  /**
   * @param {string} spotId
   */
  renderExternalLinks = (spotId) => {
    const { spotAssociated, spot } = this.props;
    const win = getWindow();
    const buoysLink = spot.breadcrumb?.length
      ? spot.breadcrumb[spot.breadcrumb.length - 1].href.replace(
          'surf-reports-forecasts-cams/',
          'surf-reports-forecasts-cams/buoys/',
        )
      : null;

    const externalLinks = [];

    if (buoysLink) {
      externalLinks.push({
        text: en.SpotPage.externalLinkText.buoys,
        path: buoysLink,
        newWindow: false,
      });
    }

    if (spotAssociated.beachesUrl) {
      externalLinks.push({
        text: en.SpotPage.externalLinkText.travel,
        path: spotAssociated.beachesUrl,
        newWindow: false,
      });
    }

    if (spot.cameras.length > 0) {
      externalLinks.push({
        text: en.SpotPage.externalLinkText.camRewind,
        path: `/surf-cams/${slugify(spot.name)}/${spot.cameras[0]._id}`,
        newWindow: false,
        eventName: 'Clicked Cam Rewind',
        segmentProperties: {
          spotId,
          spotName: spot.name,
          location: 'spot page sub navigation',
          camId: spot.cameras[0]._id,
          camName: spot.cameras[0].title,
          premiumCam: spot.cameras[0].isPremium,
          isMobileView: canUseDOM ? win?.innerWidth < TABLET_LARGE_WIDTH : false,
          platform: 'web',
        },
      });
    }
    return externalLinks;
  };

  onClickCard = (swellEvent) => {
    const { spot } = this.props;
    trackEvent('Clicked Swell Alert Card', {
      title: swellEvent.name,
      contentType: 'Swell Alert',
      basins: swellEvent.basins.join(','),
      locationCategory: 'Spot Page - Top',
      destinationUrl: swellEvent.permalink,
      subregionId: spot && spot.subregion ? spot.subregion._id : null,
    });
  };

  render() {
    const {
      spot,
      spotId,
      loading,
      nearbySpots,
      report,
      multiCam,
      device,
      userSettings,
      favorites,
      idealSpotConditions,
      spotAssociated,
      featuredContent,
      swellEvents,
      meter,
      travelDetails,
      router,
      conditionsTreatment,
    } = this.props;
    const adTargets = [['spotid', spotId]];
    const { camId } = router.query;
    const urlSetCam = spot && spot.cameras.filter((cam) => cam._id === camId);
    const urlSetCamId = urlSetCam && urlSetCam.length > 0 ? urlSetCam[0]._id : null;
    const { showMeterRegwall } = meter;

    let dateFormat = 'MDY';
    if (userSettings && userSettings.date && userSettings.date.format)
      dateFormat = userSettings.date.format;

    return (
      <div className="sl-spot-page">
        {(() => {
          if (loading) {
            return <PageLoading />;
          }
          if (spot) {
            return (
              <div>
                <SpotPageMeta
                  spotName={spot.name}
                  report={report}
                  multiCam={multiCam.isMultiCam}
                  spotId={spotId}
                  cameras={spot.cameras}
                  metaDescription={spot.metaDescription}
                  titleTag={spot.titleTag}
                />
                <ContentContainer>
                  {swellEvents.length ? (
                    <SwellEventCardContainer
                      onClickCard={(swellEvent) => this.onClickCard(swellEvent)}
                      events={swellEvents}
                    />
                  ) : null}
                  <ForecastHeader
                    breadcrumb={spot.breadcrumb}
                    spotName={spot.name}
                    pageTitle={`${spot.name} ${en.SpotPage.header.titleSuffix}`}
                    nearbySpots={nearbySpots}
                    showFavorite
                    spotPage
                    pageLevelLinks={this.renderCombinedPageLevelLinks()}
                    externalLinks1={this.renderExternalLinks(spotId)}
                    userSettings={userSettings}
                    handleFavoriteClick={(favorited) => this.handleFavoriteClick(favorited, spotId)}
                    favorited={this.spotFavorited(favorites, spotId)}
                  />
                </ContentContainer>
                <SpotCurrentConditionsSection
                  spotId={spotId}
                  adTargets={adTargets}
                  featuredContent={featuredContent}
                  urlSetCamId={urlSetCamId}
                  dateFormat={dateFormat}
                  device={device}
                />
                {showMeterRegwall ? null : (
                  <div className="sl-spot-page__below-the-fold">
                    <ContentContainer>
                      <div className="sl-spot-report-page__charts">
                        <ForecastDataVisualizer
                          type="SPOT"
                          category="cams & reports"
                          pageName="Spot Forecast"
                          spotId={spotId}
                          spotName={spot.name}
                          utcOffset={spotAssociated.utcOffset}
                          subregionId={
                            spot.subregion && spot.subregion._id ? spot.subregion._id : null
                          }
                          extended
                          canToggleDataView
                          canToggleTimeView
                          spotPage
                          device={device}
                          dateFormat={dateFormat}
                          sevenRatings={conditionsTreatment === 'on'}
                        />
                      </div>
                      <div className="sl-nearby-buoys-wrapper">
                        <NearbyBuoys
                          location={{
                            latitude: spot.lat,
                            longitude: spot.lon,
                          }}
                          trackLocation="Spot Report"
                          spotId={spotId}
                          spotName={spot.name}
                        />
                      </div>
                      <FeaturedContent
                        featuredContent={featuredContent}
                        spotName={spot.name}
                        spotId={spotId}
                        feedLocale={userSettings?.feed?.localized}
                        device={device}
                      />
                      {idealSpotConditions && (
                        <IdealConditions name={spot.name} idealConditions={idealSpotConditions} />
                      )}
                      {travelDetails && (
                        <SpotTravelInfo spotName={spot.name} travelDetails={travelDetails} />
                      )}
                    </ContentContainer>
                  </div>
                )}
              </div>
            );
          }
          return null;
        })()}
      </div>
    );
  }
}

SpotPage.defaultProps = {
  userDetails: null,
  spot: null,
  spotAssociated: null,
  loading: false,
  nearbySpots: null,
  userSettings: null,
  report: null,
  multiCam: null,
  favorites: [],
  savingFavorite: false,
  featuredContent: null,
  reportData: null,
  swellEvents: [],
  meter: null,
  travelDetails: {},
  subregionOverviewFetched: false,
  idealSpotConditions: null,
  conditionsTreatment: null,
};

SpotPage.propTypes = {
  userDetails: userDetailsPropType,
  reportData: spotReportDataPropTypes,
  spot: PropTypes.shape({
    name: PropTypes.string,
    metaDescription: PropTypes.string,
    titleTag: PropTypes.string,
    subregion: PropTypes.shape({
      _id: PropTypes.string,
      forecastStatus: PropTypes.string,
    }),
    breadcrumb: PropTypes.arrayOf(
      PropTypes.shape({ href: PropTypes.string, name: PropTypes.string }),
    ),
    cameras: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string.isRequired,
        isPremium: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ),
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  spotAssociated: spotAssociatedPropTypes,
  spotId: PropTypes.string.isRequired,
  report: PropTypes.shape(),
  multiCam: multiCamPropType,
  loading: PropTypes.bool,
  doSetRecentlyVisited: PropTypes.func.isRequired,
  doFetchSpotSwellEvents: PropTypes.func.isRequired,
  nearbySpots: PropTypes.arrayOf(PropTypes.shape()),
  userSettings: userSettingsPropTypes,
  doToggleFavorite: PropTypes.func.isRequired,
  favorites: userFavoritesPropType,
  feed: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    articles: PropTypes.arrayOf(articlePropTypes),
  }).isRequired,
  savingFavorite: PropTypes.bool,
  idealSpotConditions: spotIdealConditionsPropShape,
  isPremium: PropTypes.bool.isRequired,
  doFetchsubregionAssociated: PropTypes.func.isRequired,
  featuredContent: featuredContentPropType,
  nearbyBuoys: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.bool,
    data: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  device: devicePropTypes.isRequired,
  swellEvents: swellEventsPropType,
  meter: meterPropType,
  travelDetails: travelDetailsPropTypes,
  subregionOverviewFetched: PropTypes.bool,
  conditionsTreatment: PropTypes.string,
};

export default withRouter(
  connect(
    (state, props) => ({
      spotId: props.router.query.spotId,
      spot: getSpotDetails(state),
      loading: getSpotLoading(state) || getSubregionOverviewLoading(state),
      userDetails: getUserDetails(state),
      userSettings: getUserSettings(state),
      multiCam: getSpotMultiCam(state),
      report: getSpotReport(state),
      nearbySpots: getNearbySpots(state),
      nearbyBuoys: getSpotBuoys(state),
      reportData: getSpotReportData(state),
      spotAssociated: getSpotReportAssociated(state),
      favorites: getUserFavorites(state),
      savingFavorite: state.backplane.user.favoriteSpotLoading,
      idealSpotConditions: getSpotIdealConditions(state),
      isPremium: getUserPremiumStatus(state),
      advertisingIds: getAdvertisingIds(state),
      subregionAssociated: getSubregionAssociated(state),
      featuredContent: getFeaturedContent(state),
      feed: state.subregion.feed,
      swellEvents: getSwellEvents(state),
      travelDetails: getTravelDetails(state),
      subregionOverviewFetched: getSubregionOverviewFetched(state),
      conditionsTreatment: getTreatment(SL_WEB_CONDITION_VARIATION),
    }),
    (dispatch) => ({
      doSetRecentlyVisited: (spotId, name) => dispatch(setRecentlyVisited('spots', spotId, name)),
      doFetchSpotSwellEvents: (spotId) => dispatch(fetchSwellEvents(spotId, 'spot')),
      doToggleFavorite: (favorited, spotId) =>
        dispatch(favorited ? doFavoriteSpot(spotId) : doUnfavoriteSpot(spotId)),
      doFetchsubregionAssociated: (subregionId, sevenRatings = false) =>
        dispatch(fetchOverviewData(subregionId, sevenRatings)),
    }),
  )(withMetering(SpotPage)),
);
