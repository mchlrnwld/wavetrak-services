/* eslint-disable jsx-a11y/anchor-is-valid */
import { getWavetrakIdentity } from '@surfline/web-common';
import Link from 'next/link';
import React, { useEffect } from 'react';
import useMetering from '../../common/hooks/useMetering';
import { useAppSelector } from '../../stores/hooks';

const RateLimitTestPage = () => {
  const meter = useMetering();
  const analyticsReady = useAppSelector((state) => state.analytics.ready);
  const [identity, setIdentity] = React.useState(null);

  useEffect(() => {
    if (analyticsReady) {
      setIdentity(getWavetrakIdentity());
    }
  }, [analyticsReady]);

  const containerStyle = {
    backgroundColor: '#1f4662',
    color: '#fff',
    fontSize: '12px',
  };

  const headerStyle = {
    backgroundColor: '#193549',
    padding: '5px 10px',
    fontFamily: 'monospace',
    color: '#ffc600',
  };

  const preStyle = {
    display: 'block',
    padding: '10px 30px',
    margin: '0',
    overflow: 'scroll',
  };

  const h4Style = {
    color: '#ffc600',
    fontSize: '18px',
    lineHeight: '22px',
  };

  return (
    <div style={containerStyle}>
      <div style={headerStyle}>
        <strong>Debug Rate Limit Meter</strong>
      </div>
      <Link href="/">
        <a>Back to home</a>
      </Link>
      <h4 style={h4Style}>METER</h4>
      <pre style={preStyle}>{JSON.stringify(meter, null, 2)}</pre>
      <h4 style={h4Style}>IDENTITY</h4>
      <pre style={preStyle}>{JSON.stringify(identity, null, 2)}</pre>
    </div>
  );
};
export default RateLimitTestPage;
