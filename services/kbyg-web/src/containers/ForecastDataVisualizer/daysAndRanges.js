import config from '../../config';

const maxNumberOfDays = (isPremium, extended, isHourly) => {
  if (!extended || isHourly) return 1;
  if (isPremium) return 16;
  return 6;
};

const getDaysPerRange = (extended, deviceWidth, isHourly) => {
  if (!extended || deviceWidth < config.rangeSelectorWidths.oneDay || isHourly) return 1;
  // Should we add the following?
  // if (deviceWidth < 1024) return 3;
  if (deviceWidth < config.rangeSelectorWidths.fourDay) return 4;
  if (deviceWidth < config.rangeSelectorWidths.fiveDay) return 5;
  return 6;
};

const buildRanges = (extended, daysPerRange, isHourly) =>
  extended || isHourly
    ? [...Array(Math.ceil(16 / daysPerRange))].map((_, i) => ({
        start: daysPerRange * i,
        end: Math.min(daysPerRange * i + (daysPerRange - 1), 15),
      }))
    : [{ start: 0, end: 0 }];

const buildDays = (start, length, activeStartDay, isTabbing, isSlider, isHover) =>
  [...Array(length)].map((_, i) => {
    if (!isHover) return start + i;
    const countTo = activeStartDay + length;
    const willCountTo = start + length;
    const isNegative = start - length < 0;
    if (countTo <= willCountTo) {
      return isNegative ? start + i : start - i;
    }
    return willCountTo > 16 && isSlider ? start - i : start + i;
  });

const daysAndRanges = (
  isPremium,
  extended,
  deviceWidth,
  startDay,
  isTabbing,
  // eslint-disable-next-line default-param-last
  isSlider = false,
  // eslint-disable-next-line default-param-last
  isHover = false,
  activeStartDay,
  isHourly,
) => {
  const daysPerRange = getDaysPerRange(extended, deviceWidth, isHourly);

  const ranges = buildRanges(extended, daysPerRange, isHourly);

  const selectedRange = ranges.find(({ end }) => startDay <= end) || ranges[0];

  const numberOfDays = Math.min(
    maxNumberOfDays(isPremium, extended, isHourly),
    selectedRange.end - selectedRange.start + 1,
  );

  const days =
    isTabbing || isSlider
      ? buildDays(startDay, daysPerRange, activeStartDay, isTabbing, isSlider, isHover)
          .filter((day) => day < 16)
          .sort((a, b) => a - b)
      : buildDays(selectedRange.start, numberOfDays);

  const showFiller = days.length < daysPerRange;

  return { days, ranges, showFiller, daysPerRange };
};

export default daysAndRanges;
