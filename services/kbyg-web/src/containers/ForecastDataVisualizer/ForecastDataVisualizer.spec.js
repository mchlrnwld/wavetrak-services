import { expect } from 'chai';
import daysAndRanges from './daysAndRanges';

describe('containers / ForecastDataVisualizer', () => {
  describe('daysAndRanges', () => {
    it('returns days and ranges according to device width and premium status', () => {
      expect(daysAndRanges(true, true, 1712, 7)).to.deep.equal({
        days: [6, 7, 8, 9, 10, 11],
        ranges: [
          { start: 0, end: 5 },
          { start: 6, end: 11 },
          { start: 12, end: 15 },
        ],
        showFiller: false,
        daysPerRange: 6,
      });
      expect(daysAndRanges(true, true, 1256, 2)).to.deep.equal({
        days: [0, 1, 2, 3, 4],
        ranges: [
          { start: 0, end: 4 },
          { start: 5, end: 9 },
          { start: 10, end: 14 },
          { start: 15, end: 15 },
        ],
        showFiller: false,
        daysPerRange: 5,
      });
      expect(daysAndRanges(true, true, 975, 3)).to.deep.equal({
        days: [3],
        ranges: [...Array(16)].map((_, index) => ({ start: index, end: index })),
        showFiller: false,
        daysPerRange: 1,
      });
      expect(daysAndRanges(false, true, 1712, 1)).to.deep.equal({
        days: [0, 1, 2, 3, 4, 5],
        ranges: [
          { start: 0, end: 5 },
          { start: 6, end: 11 },
          { start: 12, end: 15 },
        ],
        showFiller: false,
        daysPerRange: 6,
      });
    });

    it('returns 1 day for not extended', () => {
      expect(daysAndRanges(false, false, null, 0)).to.deep.equal({
        days: [0],
        ranges: [{ start: 0, end: 0 }],
        showFiller: false,
        daysPerRange: 1,
      });
      expect(daysAndRanges(true, false, null, 0)).to.deep.equal({
        days: [0],
        ranges: [{ start: 0, end: 0 }],
        showFiller: false,
        daysPerRange: 1,
      });
    });

    it('Allows manually tabbing days for premium users', () => {
      expect(daysAndRanges(true, true, 1712, 3, true)).to.deep.equal({
        days: [3, 4, 5, 6, 7, 8],
        ranges: [
          { start: 0, end: 5 },
          { start: 6, end: 11 },
          { start: 12, end: 15 },
        ],
        showFiller: false,
        daysPerRange: 6,
      });
      expect(daysAndRanges(true, true, 1256, 13, true)).to.deep.equal({
        days: [13, 14, 15],
        ranges: [
          { start: 0, end: 4 },
          { start: 5, end: 9 },
          { start: 10, end: 14 },
          { start: 15, end: 15 },
        ],
        showFiller: true,
        daysPerRange: 5,
      });
    });
  });
});
