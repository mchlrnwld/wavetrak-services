import { ForecastGraphs, withDevice } from '@surfline/quiver-react';
import {
  getTreatment,
  getUserDetails,
  getUserPremiumStatus,
  trackEvent,
} from '@surfline/web-common';
import classNames from 'classnames';
import { addDays } from 'date-fns';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import {
  fetchGraphAutomatedRatingForecast,
  fetchGraphConditionForecast,
  fetchGraphTideForecast,
  fetchGraphWaveForecast,
  fetchGraphWaveForecastMaxHeights,
  fetchGraphWeatherForecast,
  fetchGraphWindForecast,
} from '../../actions/graphs';
import { LOTUS } from '../../common/constants';
import { SL_IOS_WEB_AUTOMATED_CONDITION_RATINGS } from '../../common/treatments';
import withConditionVariation from '../../common/withConditionVariation';
import withMetering from '../../common/withMetering';
import ChangeUnits from '../../components/ChangeUnits';
import ChartRangeSelector from '../../components/ChartRangeSelector';
import ForecastDataViewToggle from '../../components/ForecastDataViewToggle';
import ForecastGraphsCTA from '../../components/ForecastGraphsCTA';
import ForecastTable from '../../components/ForecastTable';
import ForecastTimeViewDropdown from '../../components/ForecastTimeViewDropdown';
import ForecastTimeViewToggle from '../../components/ForecastTimeViewToggle';
import SwellTrendSlider from '../../components/SwellTrendSlider';
import WavetrakLink from '../../components/WavetrakLink';
import config from '../../config';
import { automatedRatingsPropType } from '../../propTypes/automatedRatings';
import devicePropType from '../../propTypes/device';
import overviewDataPropShape from '../../propTypes/subregionOverviewData';
import summaryPropType from '../../propTypes/summary';
import sunlightTimesPropType from '../../propTypes/sunlightTimes';
import tideChartPropType from '../../propTypes/tideChart';
import userDetailsPropType from '../../propTypes/userDetails';
import waveChartPropType from '../../propTypes/waveChart';
import waveMaxHeightsPropType from '../../propTypes/waveMaxHeights';
import weatherChartPropType from '../../propTypes/weatherChart';
import windChartPropType from '../../propTypes/windChart';
import {
  getAutomatedRatings,
  getChartDaySummary,
  getChartSunlightTimes,
  getChartTide,
  getChartWave,
  getChartWeather,
  getChartWind,
  getWaveMaxHeights,
} from '../../selectors/graphs';
import { getSubregionOverviewData } from '../../selectors/subregion';
import midnightOfDate from '../../utils/midnightOfDate';
import daysAndRanges from './daysAndRanges';

const ForecastGraphsWithConditionVariation = withConditionVariation(ForecastGraphs);

const SIXTEEN_DAY = '16day';
const HOURLY = 'hourly';
const GRAPH = 'GRAPH';
const TABLE = 'TABLE';
const REGIONAL = 'REGIONAL';
const SPOT = 'SPOT';
const OPTIMAL = 'OPTIMAL';
const SWELL_HEIGHT = 'SWELL_HEIGHT';
const ONE_DAY = '1-day';

/**
 * @typedef {object} Props
 * @prop {import('next/router').NextRouter} router
 *
 * @extends {PureComponent<Props>}
 */
class ForecastDataVisualizer extends PureComponent {
  constructor(props) {
    super(props);

    const { isPremium, extended } = this.props;
    const tableDaysAllowed = this.daysAllowed(isPremium, extended);
    const tableDaysEnabled = this.daysEnabled(tableDaysAllowed, isPremium);

    this.state = {
      mounted: false,
      timeView: SIXTEEN_DAY,
      dataView: GRAPH,
      graphs: {
        now: new Date() / 1000,
        selectedStartDay: 0,
        previousStartDay: 0,
        sliderRangeStart: 0,
        activeSwell: null,
        isTabbing: false,
        isSlider: false,
      },
      table: {
        days: [...Array(Math.min(tableDaysAllowed, tableDaysEnabled + 1))].map((_, day) => ({
          blurred: day >= tableDaysEnabled,
          expanded: day >= tableDaysEnabled,
        })),
        filterTop3Swells: true,
      },
    };
  }

  async componentDidMount() {
    this.setMounted();
    return this.fetchAllData();
  }

  async componentDidUpdate(prevProps) {
    const {
      device: { width },
      canToggleTimeView,
      splitReady,
    } = this.props;

    const { tabletLargeWidth } = config;
    const { timeView, dataView } = this.state;
    const wasDesktop = prevProps.device.width > tabletLargeWidth;
    const isMobile = width < tabletLargeWidth;
    const passedBreakpoint = wasDesktop && isMobile;
    const isGraph = dataView === GRAPH;
    const isHourly = timeView === HOURLY;
    const resizeShouldTriggerToggle = passedBreakpoint && isGraph && isHourly && canToggleTimeView;

    if (splitReady && !this.automatedRatingsTreatment) {
      this.automatedRatingsTreatment = getTreatment(SL_IOS_WEB_AUTOMATED_CONDITION_RATINGS);
      if (this.automatedRatingsTreatment === 'on') {
        this.fetchAutomatedRatings();
      }
    }

    if (resizeShouldTriggerToggle) {
      this.toggleTimeView(SIXTEEN_DAY, '16 Day');
    }
  }

  setActiveSwell = (activeSwellIndex) => {
    const { graphs } = this.state;
    this.setState({
      graphs: {
        ...graphs,
        activeSwell: activeSwellIndex,
      },
    });
  };

  getCurrentRange = () => {
    const { graphs, timeView } = this.state;
    const {
      device: { width },
      extended,
      isPremium,
      canToggleTimeView,
    } = this.props;

    const isExtended = canToggleTimeView ? timeView === SIXTEEN_DAY : extended;
    const { days } = daysAndRanges(
      isPremium,
      isExtended,
      width,
      graphs.selectedStartDay,
      graphs.isTabbing,
    );
    return { start: days[0], end: days[days.length - 1] };
  };

  fetchAutomatedRatings = () => {
    const { timeView } = this.state;
    const { isPremium, extended, doFetchGraphAutomatedRatingForecast } = this.props;
    const isExtended = timeView ? timeView === SIXTEEN_DAY : extended;
    const days = this.daysAllowed(isPremium, isExtended);
    const waveInterval = 1;
    const desired16DayWaveInterval = 3;
    doFetchGraphAutomatedRatingForecast({
      days,
      intervalHours: waveInterval,
      desired16DayInterval: desired16DayWaveInterval,
    });
  };

  fetchAllData = () => {
    const {
      isPremium,
      extended,
      type,
      doFetchGraphConditionForecast,
      doFetchGraphTideForecast,
      doFetchGraphWaveForecast,
      doFetchGraphWaveForecastMaxHeights,
      doFetchGraphWeatherForecast,
      doFetchGraphWindForecast,
      sevenRatings,
    } = this.props;
    const { timeView } = this.state;
    const isExtended = timeView ? timeView === SIXTEEN_DAY : extended;
    const days = this.daysAllowed(isPremium, isExtended);
    const intervalHours = isExtended ? 3 : 1;
    const waveInterval = 1;
    const desired16DayWaveInterval = 3;
    const desired16DayInterval = 3;
    const windInterval = 1;
    const weatherInterval = 1;
    if (isExtended && type === REGIONAL) {
      doFetchGraphConditionForecast(days, sevenRatings);
    }
    doFetchGraphWaveForecast({
      days,
      intervalHours: waveInterval,
      desired16DayInterval: desired16DayWaveInterval,
    });

    doFetchGraphWindForecast({ days, intervalHours: windInterval, desired16DayInterval });
    doFetchGraphTideForecast({ days, intervalHours });
    doFetchGraphWeatherForecast({
      days,
      intervalHours: weatherInterval,
      desired16DayInterval,
    });
    doFetchGraphWaveForecastMaxHeights({
      days: 16,
      intervalHours: waveInterval,
      desired16DayInterval: desired16DayWaveInterval,
    });
  };

  daysAllowed = (isPremium, extended) => {
    const extendedDays = isPremium ? 16 : 6;
    return extended ? extendedDays : 1;
  };

  daysEnabled = (daysAllowed, isPremium, isHourly) => {
    if (isHourly) return isPremium ? 5 : 2;
    if (daysAllowed !== 6) return 16;
    if (!isPremium) return 2;
    return 3;
  };

  // eslint-disable-next-line default-param-last
  handleSelectRange = (selectedRange, isSlider = false, sliderRangeStart, clicked = false) => {
    let action;
    if (isSlider && clicked) {
      action = 'Clicked Day';
    } else if (isSlider) {
      action = 'Slide Range';
    } else {
      action = 'Change Range';
    }
    const { graphs } = this.state;
    this.trackSelectRange(selectedRange, action);
    this.setState({
      graphs: {
        ...graphs,
        isTabbing: false,
        selectedStartDay: selectedRange.start,
        previousStartDay: graphs.selectedStartDay,
        sliderRangeStart,
        isSlider,
      },
    });
  };

  trackSelectRange = (range, buttonAction) => {
    const { device, type } = this.props;
    const mobileView = device.width < config.tabletLargeWidth;
    const forecastPeriod = mobileView
      ? `${range.start + 1}`
      : `${range.start + 1}-${range.end + 1}`;

    const category = type === SPOT ? 'spot forecast' : 'regional forecast';

    trackEvent('Clicked Forecast Range', {
      forecastPeriod,
      category,
      isMobileView: mobileView,
      buttonAction,
    });
  };

  trackChartToggle = (view) => {
    const { pageName, spotId, subregionId, type } = this.props;
    trackEvent('Toggled Forecast View', {
      category: type === SPOT ? 'spot forecast' : 'regional forecast',
      subregionId,
      spotId,
      pageName,
      view,
    });
  };

  lotusHeaderClassName = () => {
    const { timeView } = this.state;
    return classNames({
      'sl-forecast-data-visualizer__header__lotus': true,
      'sl-forecast-data-visualizer__header__lotus--hourly': timeView === HOURLY,
    });
  };

  className = (extended) => {
    const { isPremium, spotPage } = this.props;
    return classNames({
      'sl-forecast-data-visualizer': true,
      'sl-forecast-data-visualizer--extended': !!extended,
      'sl-forecast-data-visualizer--premium': isPremium,
      'sl-forecast-data-visualizer--spot-page': spotPage,
    });
  };

  headerClassName = (extended) => {
    const { spotPage } = this.props;
    const { dataView } = this.state;
    return classNames({
      'sl-forecast-data-visualizer__header': true,
      'sl-forecast-data-visualizer__header--extended': !!extended,
      'sl-forecast-data-visualizer__header--spot-page': spotPage,
      'sl-forecast-data-visualizer__header--regional-page': !spotPage,
      'sl-forecast-data-visualizer__header--table-view': dataView === TABLE,
    });
  };

  setMounted = () => {
    this.setState({ mounted: true });
  };

  toggleDataView = (view) => {
    this.trackChartToggle(view);
    this.setState({ dataView: view });
  };

  /**
   * @description Toggles between the time view shown on the graph page
   * and triggers relevant segment calls
   * @param {string} timeView - The desired time view to toggle to ('16day' | 'hourly')
   * @param {string} [btnLocation=false] - the button location to pass to the segment event
   * @param {number} [dayNum=false] - the day to toggle to
   * @memberof ForecastDataVisualizer
   */
  toggleTimeView = (timeView, btnLocation = false, dayNum = false) => {
    const { graphs } = this.state;

    // Have to reset to the first day when switching to hourly
    this.setState({
      timeView,
      graphs: {
        ...graphs,
        selectedStartDay: dayNum || 0,
      },
    });
    const { pageName, spotId, spotName, subregionId, subregionData, type } = this.props;
    trackEvent('Toggled Spot Forecast', {
      category: type === SPOT ? 'spot forecast' : 'regional forecast',
      subregionId,
      subregionName: subregionData?.name || null,
      spotId,
      spotName,
      pageName,
      buttonAction: btnLocation || ONE_DAY,
      view: timeView === HOURLY ? 'Hourly Forecast' : '16-Day Spot Forecast',
    });
  };

  toggleDayExpanded = (day) => {
    const { table } = this.state;
    const days = [...table.days];
    days[day] = {
      ...days[day],
      expanded: !days[day].expanded,
    };
    this.setState({
      table: {
        ...table,
        days,
      },
    });
  };

  expandAllDays = (expanded) => {
    const { table } = this.state;
    this.setState({
      table: {
        ...table,
        days: table.days.map((day) => ({
          ...day,
          expanded: day.blurred || expanded,
        })),
      },
    });
  };

  tabBackwards = () => {
    const { graphs } = this.state;
    const newState = {
      graphs: {
        ...graphs,
        isTabbing: true,
        selectedStartDay:
          (graphs.sliderRangeStart ? graphs.sliderRangeStart : graphs.selectedStartDay) - 1,
        sliderRangeStart: graphs.sliderRangeStart && graphs.sliderRangeStart - 1,
      },
    };

    if (graphs.selectedStartDay > 0) {
      this.setState(newState, () => this.trackSelectRange(this.getCurrentRange(), 'Scroll Back'));
    }
  };

  tabForwards = () => {
    const { graphs } = this.state;
    const newState = {
      graphs: {
        ...graphs,
        isTabbing: true,
        selectedStartDay:
          (graphs.sliderRangeStart ? graphs.sliderRangeStart : graphs.selectedStartDay) + 1,
        sliderRangeStart: graphs.sliderRangeStart && graphs.sliderRangeStart + 1,
      },
    };

    if (graphs.selectedStartDay < 16) {
      this.setState(newState, () =>
        this.trackSelectRange(this.getCurrentRange(), 'Scroll Forward'),
      );
    }
  };

  GraphsCTA = () => {
    const { category, pageName, spotId, subregionId } = this.props;
    return (
      <ForecastGraphsCTA
        category={category}
        pageName={pageName}
        spotId={spotId}
        subregionId={subregionId}
      />
    );
  };

  render() {
    const { dataView, graphs, table, timeView, mounted } = this.state;
    const {
      device: { mobile, width },
      device,
      type,
      category,
      pageName,
      extended,
      utcOffset,
      isPremium,
      userDetails,
      summary,
      sunlightTimes,
      wave,
      waveMaxHeights,
      tide,
      weather,
      wind,
      spotId,
      spotName,
      subregionId,
      canToggleDataView,
      canToggleTimeView,
      spotPage,
      subregionData,
      lastUpdate,
      dateFormat,
      automatedRatings,
      router,
      showForecastGraphsDaySummariesCTA,
      isAnonymous,
      sevenRatings,
      splitReady,
    } = this.props;
    const isHourly = timeView === HOURLY;
    const isExtended = canToggleTimeView ? timeView === SIXTEEN_DAY : extended;
    const pathname = router.asPath;
    const { days, ranges, daysPerRange } = daysAndRanges(
      isPremium,
      isExtended,
      width,
      graphs.selectedStartDay,
      graphs.isTabbing,
      graphs.isSlider,
      false,
      graphs.previousStartDay,
      isHourly,
    );
    const changeAccountSettingsHref = userDetails
      ? `${config.accountSettingsUrl}?redirectUrl=${pathname}`
      : `${config.createAccountUrl}?redirectUrl=${encodeURIComponent(
          `${config.surflineHost}${config.accountSettingsUrl}?redirectUrl=${config.surflineHost}${pathname}`,
        )}`;

    const daysAllowed = this.daysAllowed(isPremium, isExtended);
    const daysEnabled = this.daysEnabled(daysAllowed, isPremium, isHourly);

    const isTablet = width < config.tabletLargeWidth;
    const isGraphView = dataView === GRAPH;
    const showLotusHeader =
      isTablet || isHourly || (!isGraphView && !canToggleTimeView) || !canToggleTimeView;
    const showTimeViewToggle = canToggleTimeView && isGraphView && !isTablet;
    const showTimeViewDropdown = showTimeViewToggle && width < config.rangeSelectorWidths.fourDay;
    const showChartRangeSelector = isGraphView && width && (isExtended || isHourly) && ranges;
    const showSwellTrendSlider = showChartRangeSelector && width > config.tabletLargeWidth;
    const show24HourLink = !canToggleTimeView && !isExtended;

    const localDate = new Date(midnightOfDate(tide.utcOffset) * 1000);
    const startDate = addDays(localDate, days[0]);
    const endDate = addDays(localDate, days[0] + days.length);

    return (
      <div className="sl-forecast-data-visualizer-container">
        <div className={this.className(isExtended)}>
          <div className={this.headerClassName(isExtended, spotPage)}>
            {showLotusHeader && type === SPOT ? (
              <div className={this.lotusHeaderClassName()}>
                <h6>Model Forecast</h6>
                <p>Generated by {LOTUS}</p>
              </div>
            ) : null}
            {!showTimeViewToggle &&
              canToggleDataView &&
              !(canToggleTimeView && isHourly && !mobile) && (
                <ForecastDataViewToggle dataView={dataView} onClick={this.toggleDataView} />
              )}
            {showTimeViewToggle && !showTimeViewDropdown && (
              <ForecastTimeViewToggle timeView={timeView} onClick={this.toggleTimeView} />
            )}
            {showTimeViewDropdown && (
              <ForecastTimeViewDropdown timeView={timeView} onClick={this.toggleTimeView} />
            )}
            {showChartRangeSelector && mounted && !showSwellTrendSlider ? (
              <ChartRangeSelector
                mobile={width < config.tabletLargeWidth}
                ranges={ranges}
                waveDays={wave.days}
                waveMaxHeightsDays={waveMaxHeights.days}
                summaryDays={summary.days}
                showConditions={type === REGIONAL}
                units={wave.units}
                utcOffset={utcOffset}
                selectedStartDay={graphs.selectedStartDay}
                daysEnabled={daysEnabled}
                onSelectRange={this.handleSelectRange}
                isPremium={isPremium}
                type={type}
                spotId={spotId}
                isTabbing={graphs.isTabbing}
                isHourly={isHourly}
                dateFormat={dateFormat}
                sevenRatings={sevenRatings}
                splitReady={splitReady}
              />
            ) : null}
            {showSwellTrendSlider && mounted && (
              <SwellTrendSlider
                daysPerRange={daysPerRange}
                days={days}
                deviceWidth={width}
                ranges={ranges}
                waveMaxHeightsDays={waveMaxHeights.days}
                utcOffset={utcOffset}
                selectedStartDay={graphs.selectedStartDay}
                daysEnabled={daysEnabled}
                onSelectRange={this.handleSelectRange}
                isPremium={isPremium}
                type={type}
                spotId={spotId}
                isTabbing={graphs.isTabbing}
                isSlider={graphs.isSlider}
                daysAndRanges={daysAndRanges}
                isHourly={isHourly}
                dateFormat={dateFormat}
              />
            )}
            {showTimeViewToggle &&
              canToggleDataView &&
              !(canToggleTimeView && isHourly && !mobile) && (
                <ForecastDataViewToggle dataView={dataView} onClick={this.toggleDataView} />
              )}
            {show24HourLink && (
              <div className="sl-forecast-data-visualizer__24-hour-header">
                24 Hour Forecast
                <WavetrakLink
                  className="sl-forecast-data-visualizer__16-day-link"
                  href={`${pathname}/forecast`}
                  onClick={() =>
                    trackEvent('Clicked Link', {
                      linkUrl: `${pathname}/forecast`,
                      linkName: '16-day forcast',
                      linkLocation: '24-hour forecast',
                      category: 'cams & reports',
                    })
                  }
                >
                  View 16-day forecast
                </WavetrakLink>
              </div>
            )}
          </div>
          {isGraphView ? (
            <ForecastGraphsWithConditionVariation
              useNextLink
              isHourly={isHourly}
              extended={isExtended}
              days={days}
              daysEnabled={daysEnabled}
              type={type}
              lastUpdate={lastUpdate}
              device={device}
              mobile={mobile}
              category={category}
              pageName={pageName}
              spotName={spotName}
              spotId={spotId}
              subregionId={subregionId}
              subregionName={subregionData?.name}
              utcOffset={utcOffset}
              summary={summary}
              sunlightTimes={sunlightTimes}
              wave={wave}
              tide={tide}
              automatedRatings={
                this.automatedRatingsTreatment === 'on' ? automatedRatings : undefined
              }
              weather={weather}
              wind={wind}
              now={graphs.now}
              showCTA={isExtended && !isPremium}
              activeSwell={graphs.activeSwell}
              setActiveSwell={this.setActiveSwell}
              tabBackwards={this.tabBackwards}
              tabForwards={this.tabForwards}
              isPremium={isPremium}
              spotPage={spotPage}
              changeToHourlyView={(btnLocation, dayNum) =>
                this.toggleTimeView(HOURLY, btnLocation, dayNum)
              }
              CTAComponent={this.GraphsCTA}
              dateFormat={dateFormat}
              startDate={startDate}
              endDate={endDate}
              showForecastGraphsDaySummariesCTA={showForecastGraphsDaySummariesCTA}
              isAnonymous={isAnonymous}
            />
          ) : (
            <ForecastTable
              dateFormat={dateFormat}
              days={table.days}
              wave={wave}
              wind={wind}
              utcOffset={utcOffset}
              category={category}
              pageName={pageName}
              spotId={spotId}
              subregionId={subregionId}
              showCTA={isExtended && !isPremium}
              toggleDayExpanded={this.toggleDayExpanded}
              expandAllDays={this.expandAllDays}
              hideSurfAndWind={type === REGIONAL}
              highlightType={type === SPOT ? OPTIMAL : SWELL_HEIGHT}
              deviceWidth={width}
            />
          )}
          <ChangeUnits href={changeAccountSettingsHref} />
        </div>
      </div>
    );
  }
}

ForecastDataVisualizer.propTypes = {
  device: devicePropType.isRequired,
  type: PropTypes.oneOf([SPOT, REGIONAL]).isRequired,
  category: PropTypes.string.isRequired,
  pageName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  spotName: PropTypes.string,
  subregionId: PropTypes.string,
  subregionData: overviewDataPropShape,
  extended: PropTypes.bool,
  utcOffset: PropTypes.number.isRequired,
  isPremium: PropTypes.bool.isRequired,
  userDetails: userDetailsPropType,
  doFetchGraphConditionForecast: PropTypes.func.isRequired,
  doFetchGraphWaveForecast: PropTypes.func.isRequired,
  doFetchGraphAutomatedRatingForecast: PropTypes.func.isRequired,
  doFetchGraphWaveForecastMaxHeights: PropTypes.func.isRequired,
  doFetchGraphTideForecast: PropTypes.func.isRequired,
  doFetchGraphWeatherForecast: PropTypes.func.isRequired,
  doFetchGraphWindForecast: PropTypes.func.isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  wave: waveChartPropType.isRequired,
  automatedRatings: automatedRatingsPropType.isRequired,
  waveMaxHeights: waveMaxHeightsPropType.isRequired,
  tide: tideChartPropType.isRequired,
  weather: weatherChartPropType.isRequired,
  wind: windChartPropType.isRequired,
  summary: summaryPropType.isRequired,
  canToggleDataView: PropTypes.bool,
  canToggleTimeView: PropTypes.bool,
  lastUpdate: PropTypes.string,
  spotPage: PropTypes.bool,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
  showForecastGraphsDaySummariesCTA: PropTypes.bool,
  isAnonymous: PropTypes.bool,
  sevenRatings: PropTypes.bool,
};

ForecastDataVisualizer.defaultProps = {
  spotName: null,
  spotPage: false,
  extended: false,
  subregionId: null,
  lastUpdate: null,
  canToggleDataView: false,
  canToggleTimeView: false,
  userDetails: null,
  subregionData: null,
  dateFormat: 'MDY',
  showForecastGraphsDaySummariesCTA: false,
  isAnonymous: null,
  sevenRatings: false,
};

export default withRouter(
  connect(
    (state, props) => ({
      userDetails: getUserDetails(state),
      isPremium: getUserPremiumStatus(state),
      sunlightTimes: getChartSunlightTimes(state),
      summary: getChartDaySummary(state),
      wave: getChartWave(state, props.device?.mobile, props.extended),
      automatedRatings: getAutomatedRatings(state),
      waveMaxHeights: getWaveMaxHeights(state, props.device?.mobile, props.extended),
      tide: getChartTide(state, props),
      weather: getChartWeather(state, props),
      wind: getChartWind(state, props),
      subregionData: getSubregionOverviewData(state),
      splitReady: state.split.ready,
      componentName: 'ForecastDataVisualizer',
    }),
    (dispatch, { type, spotId, subregionId, router }) => ({
      doFetchGraphConditionForecast: ({ days, sevenRatings = false }) =>
        dispatch(
          fetchGraphConditionForecast({
            id: subregionId,
            days,
            query: router.query,
            sevenRatings,
          }),
        ),
      doFetchGraphWaveForecast: ({ days, intervalHours, desired16DayInterval }) =>
        dispatch(
          fetchGraphWaveForecast(type)({
            id: type === REGIONAL ? subregionId : spotId,
            days,
            intervalHours,
            maxHeights: false,
            desired16DayInterval,
            isGraphUpdates: true,
            query: router.query,
          }),
        ),
      doFetchGraphWaveForecastMaxHeights: ({ days, intervalHours, desired16DayInterval }) =>
        dispatch(
          fetchGraphWaveForecastMaxHeights(type)({
            id: type === REGIONAL ? subregionId : spotId,
            days,
            intervalHours,
            maxHeights: true,
            desired16DayInterval,
            isGraphUpdates: false,
            query: router.query,
          }),
        ),
      doFetchGraphTideForecast: ({ days, intervalHours }) =>
        dispatch(
          fetchGraphTideForecast({
            id: spotId,
            days,
            intervalHours,
            maxHeights: false,
            isGraphUpdates: false,
            query: router.query,
          }),
        ),
      doFetchGraphWeatherForecast: ({ days, intervalHours, desired16DayInterval }) =>
        dispatch(
          fetchGraphWeatherForecast({
            id: spotId,
            days,
            intervalHours,
            maxHeights: false,
            desired16DayInterval,
            isGraphUpdates: false,
            query: router.query,
          }),
        ),
      doFetchGraphWindForecast: ({ days, intervalHours, desired16DayInterval }) =>
        dispatch(
          fetchGraphWindForecast({
            id: spotId,
            days,
            intervalHours,
            maxHeights: false,
            desired16DayInterval,
            isGraphUpdates: false,
            query: router.query,
          }),
        ),
      doFetchGraphAutomatedRatingForecast: ({ days, intervalHours, desired16DayInterval }) =>
        dispatch(
          fetchGraphAutomatedRatingForecast({
            id: spotId,
            days,
            intervalHours,
            desired16DayInterval,
            query: router.query,
          }),
        ),
    }),
  )(withMetering(withDevice(ForecastDataVisualizer))),
);
