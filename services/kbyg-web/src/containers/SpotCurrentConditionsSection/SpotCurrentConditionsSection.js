import PropTypes from 'prop-types';
import React, { useMemo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  ContentCard,
  ContentContainer,
  PageRail,
  SpotReport,
  GoogleDFP,
  DailyForecastReportCTA,
} from '@surfline/quiver-react';
import {
  getUserPremiumStatus,
  getUserDetails,
  getUserSettings,
  getEntitlements,
  postedDate,
} from '@surfline/web-common';
import { BRAZE_CONTENT_CARDS } from '../../common/constants';

import loadAdConfig from '../../utils/adConfig';
import config from '../../config';
import SpotReportCams from '../SpotReportCams';
import userDetailsPropType from '../../propTypes/userDetails';
import userSettingsPropType from '../../propTypes/userSettings';
import {
  getSpotDetails,
  getSpotReport,
  getSpotReportData,
  getSpotReportAssociated,
  getAdvertisingIds,
  getLegacySpotId,
  getSpotMultiCam,
} from '../../selectors/spot';
import { getSubregionAssociated } from '../../selectors/subregion';
import SpotReportCamHeaderCTA from '../../components/SpotReportCamHeaderCTA';
import NearbyCams from '../../components/NearbyCams';
import FeaturedContent from '../../components/FeaturedContent';
import featuredContentPropType from '../../propTypes/featuredContent';
import multiCamPropType from '../../propTypes/multiCam';
import spotReportDataPropType from '../../propTypes/spot';
import devicePropType from '../../propTypes/device';
import meterPropTypes from '../../propTypes/meter';
import withMetering from '../../common/withMetering';
import withConditionVariation from '../../common/withConditionVariation';

const SpotReportWithConditionVariation = withConditionVariation(SpotReport);

const spotCurrentConditionsSectionClass = (
  hasCam,
  hasReport,
  isPremium,
  isMultiCam,
  isTheatreMode,
) =>
  classNames({
    'sl-spot-current-conditions-section': true,
    'sl-spot-current-conditions-section--with-cam': hasCam,
    'sl-spot-current-conditions-section--no-cam': !hasCam,
    'sl-spot-current-conditions-section--with-report': hasReport,
    'sl-spot-current-conditions-section--no-report': !hasReport,
    'sl-spot-current-conditions-section--premium': isPremium,
    'sl-spot-current-conditions-section--free': !isPremium,
    'sl-spot-current-conditions-section--multi-cam': isMultiCam,
    'sl-spot-current-conditions-section--single-cam': hasCam && !isMultiCam,
    'sl-spot-current-conditions-section--theatre-mode': isTheatreMode,
  });

const spotModuleClass = (hasCam, isMultiCam) =>
  classNames({
    'sl-spot-module': true,
    'sl-spot-module--with-cam': hasCam,
    'sl-spot-module--no-cam': !hasCam,
    'sl-spot-module--multi-cam': isMultiCam,
  });

/**
 * @param {string} spotId
 */
const useMultiCamRecomputeView = (spotId) => {
  const [shouldRecomputeMultiCamView, setShouldRecomputeMultiCamView] = useState(true);

  // When we change spots, we want to set the recompute flag again
  useEffect(() => {
    setShouldRecomputeMultiCamView(true);
  }, [spotId]);

  return [shouldRecomputeMultiCamView, setShouldRecomputeMultiCamView];
};

const SpotCurrentConditionsSection = ({
  isPremium,
  spot,
  spotId,
  report,
  reportData,
  reportAssociated,
  subregionAssociated,
  userDetails,
  entitlements,
  adTargets,
  featuredContent,
  multiCam,
  legacySpotId,
  device,
  urlSetCamId,
  dateFormat,
  userSettings,
  meter,
}) => {
  const [shouldRecomputeMultiCamView, setShouldRecomputeMultiCamView] =
    useMultiCamRecomputeView(spotId);

  const hasCam = spot.cameras.length > 0;
  const hasReport = !!report;
  const note = reportData && reportData.note ? reportData.note : null;
  const isDesktop = device.width >= config.smallDesktopWidth;
  // Logic for (multiCam.isMultiCam || isPremium): Show if multiCam spot
  // or if user is premium giving them access to theatre mode
  const { isMultiCam } = multiCam;
  const multiCamActiveView = multiCam.activeView;
  const isTheatreMode = (isPremium && isMultiCam) || (!isPremium && multiCamActiveView === 'MULTI');
  const headerCTASegmentProperties = useMemo(
    () => ({
      location: 'cam player top',
      category: 'cams & reports',
      pageName: 'Spot Report',
      spotId,
      spotName: spot.name,
    }),
    [spot.name, spotId],
  );

  const isMetered = meter?.meteringEnabled;

  const spotMiddleAdTarget = urlSetCamId ? [['camId', urlSetCamId]] : adTargets;
  const isAnonymous = !userDetails;
  const showPaywall =
    meter?.meteringEnabled &&
    (isAnonymous || (userDetails && !(meter?.meterRemaining > -1))) &&
    hasReport;
  const lastUpdate =
    report?.timestamp &&
    reportAssociated?.utcOffset &&
    subregionAssociated?.abbrTimezone &&
    dateFormat
      ? postedDate(
          report.timestamp,
          reportAssociated.utcOffset,
          subregionAssociated.abbrTimezone,
          true,
          dateFormat,
        )
      : null;
  const showReport = !showPaywall && ((hasCam && !isTheatreMode) || (!isDesktop && isMultiCam));
  const showSpotReport = !showPaywall && ((hasCam && isTheatreMode && isDesktop) || !hasCam);
  const segmentProperties = useMemo(
    () => ({
      location: 'Reports',
      pageName: 'Spot Report',
      spotId,
      spotName: spot.name,
    }),
    [spot.name, spotId],
  );
  return (
    <div
      className={spotCurrentConditionsSectionClass(
        hasCam,
        hasReport,
        isPremium,
        isMultiCam,
        isTheatreMode,
      )}
    >
      {hasCam && isTheatreMode && (
        <>
          {!isPremium && !isMetered && (
            <div className="sl-spot-report-cam__cta">
              <SpotReportCamHeaderCTA eventProperties={headerCTASegmentProperties} />
            </div>
          )}
          <div className="sl-spot-banner-container--theatre-mode">
            <ContentContainer>
              <ContentCard card={{ name: BRAZE_CONTENT_CARDS.SPOT_PAGE, extras: null }} />
            </ContentContainer>
          </div>
          <div className="sl-spot-current-conditions-section__theatre-cam">
            <ContentContainer>
              <SpotReportCams
                shouldRecomputeMultiCamView={shouldRecomputeMultiCamView}
                setShouldRecomputeMultiCamView={setShouldRecomputeMultiCamView}
                urlSetCamId={urlSetCamId}
                spotId={spotId}
                legacySpotId={legacySpotId}
                spot={spot}
                reportData={reportData}
                userDetails={userDetails}
                isPremium={isPremium}
                isTheatreMode={isTheatreMode}
                headerCTASegmentProperties={headerCTASegmentProperties}
                isMultiCam={isMultiCam}
                meter={meter}
              />
            </ContentContainer>
          </div>
          <ContentContainer>
            <NearbyCams spotId={spotId} />
          </ContentContainer>
        </>
      )}
      {(!hasCam || !isTheatreMode) && (
        <div className="sl-spot-banner-container">
          <ContentContainer>
            <ContentCard card={{ name: BRAZE_CONTENT_CARDS.SPOT_PAGE, extras: null }} />
          </ContentContainer>
        </div>
      )}
      <ContentContainer>
        <PageRail side="left">
          {hasCam && !isTheatreMode && (
            <SpotReportCams
              shouldRecomputeMultiCamView={shouldRecomputeMultiCamView}
              setShouldRecomputeMultiCamView={setShouldRecomputeMultiCamView}
              urlSetCamId={urlSetCamId}
              spotId={spotId}
              legacySpotId={legacySpotId}
              spot={spot}
              reportData={reportData}
              userDetails={userDetails}
              isPremium={isPremium}
              isTheatreMode={isTheatreMode}
              headerCTASegmentProperties={headerCTASegmentProperties}
              isMultiCam={isMultiCam}
              meter={meter}
            />
          )}
          {showSpotReport && (
            <div className={spotModuleClass(hasCam, isMultiCam)}>
              <SpotReportWithConditionVariation
                spot={spot}
                note={note}
                humanReport={hasReport ? report : null}
                readings={reportData}
                associated={reportAssociated}
                subregionAssociated={subregionAssociated}
                adTargets={adTargets}
                userDetails={userDetails}
                entitlements={entitlements}
                isMultiCam={isMultiCam}
                baseUrl={config.homepageUrl}
                dateFormat={dateFormat}
              />
            </div>
          )}
        </PageRail>
        <PageRail side="right">
          <FeaturedContent
            featuredContent={featuredContent}
            spotName={spot.name}
            spotId={spotId}
            feedLocale={userSettings?.feed?.localized}
            device={device}
          />
        </PageRail>
      </ContentContainer>
      <ContentContainer>
        {showReport && (
          <SpotReportWithConditionVariation
            spot={spot}
            note={note}
            humanReport={hasReport ? report : null}
            readings={reportData}
            entitlements={entitlements}
            associated={reportAssociated}
            subregionAssociated={subregionAssociated}
            adTargets={adTargets}
            isMultiCam={isMultiCam}
            dateFormat={dateFormat}
          />
        )}
        {showPaywall && hasReport && (
          <DailyForecastReportCTA
            avatarUrl={report.forecaster.iconUrl}
            isAnonymous
            forecasterName={report.forecaster.name}
            forecasterTitle={report.forecaster.title}
            lastUpdate={lastUpdate}
            segmentProperties={segmentProperties}
          />
        )}
        <div className="sl-spot-current-conditions-section__middle-ad">
          <GoogleDFP
            adConfig={loadAdConfig('spotMiddle', spotMiddleAdTarget, entitlements)}
            isHtl
            isTesting={config.htl.isTesting}
          />
        </div>
      </ContentContainer>
    </div>
  );
};

SpotCurrentConditionsSection.propTypes = {
  isPremium: PropTypes.bool.isRequired,
  report: PropTypes.shape(),
  reportData: spotReportDataPropType,
  spot: PropTypes.shape({
    name: PropTypes.string,
    subregion: PropTypes.shape({
      _id: PropTypes.string,
      forecastStatus: PropTypes.string,
    }),
    cameras: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  spotId: PropTypes.string,
  entitlements: PropTypes.arrayOf(PropTypes.string),
  reportAssociated: PropTypes.shape({
    utcOffset: PropTypes.number,
  }).isRequired,
  subregionAssociated: PropTypes.shape({
    abbrTimezone: PropTypes.string,
  }).isRequired,
  advertisingIds: PropTypes.shape({
    spotId: PropTypes.string,
    subregionId: PropTypes.string,
  }).isRequired,
  userDetails: userDetailsPropType,
  userSettings: userSettingsPropType,
  multiCam: multiCamPropType.isRequired,
  legacySpotId: PropTypes.string,
  adTargets: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  featuredContent: featuredContentPropType,
  device: devicePropType.isRequired,
  urlSetCamId: PropTypes.string,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
  meter: meterPropTypes,
};

SpotCurrentConditionsSection.defaultProps = {
  userDetails: null,
  userSettings: null,
  reportData: null,
  spot: null,
  spotId: null,
  report: null,
  entitlements: [],
  adTargets: [],
  legacySpotId: null,
  featuredContent: null,
  urlSetCamId: null,
  dateFormat: 'MDY',
  meter: null,
};

export default connect((state) => ({
  userDetails: getUserDetails(state),
  userSettings: getUserSettings(state),
  entitlements: getEntitlements(state),
  isPremium: getUserPremiumStatus(state),
  spot: getSpotDetails(state),
  multiCam: getSpotMultiCam(state),
  legacySpotId: getLegacySpotId(state),
  report: getSpotReport(state),
  advertisingIds: getAdvertisingIds(state),
  reportData: getSpotReportData(state),
  reportAssociated: getSpotReportAssociated(state),
  subregionAssociated: getSubregionAssociated(state),
}))(withMetering(SpotCurrentConditionsSection));
