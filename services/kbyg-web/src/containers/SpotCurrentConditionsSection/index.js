import { withDevice } from '@surfline/quiver-react';
import SpotCurrentConditionsSection from './SpotCurrentConditionsSection';

export default withDevice(SpotCurrentConditionsSection);
