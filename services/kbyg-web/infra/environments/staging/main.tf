provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/staging/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/staging/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/kbyg/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-3-staging"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/e10b8d509912ed71"
}

locals {
  dns_name      = "kbyg.staging.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-kbyg.staging.surfline.com"
    },
  ]
}

module "kbyg" {
  source = "../../"

  company          = "sl"
  application      = "kbyg"
  environment      = "staging"

  default_vpc   = "vpc-981887fd"
  dns_name      = local.dns_name
  dns_zone_id   = "Z3JHKQ8ELQG5UE"
  ecs_cluster   = "sl-core-svc-staging"

  service_lb_rules      = local.service_lb_rules
  service_td_count      = 1

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}

resource "aws_route53_record" "kbyg_internal" {
  zone_id = "Z3JHKQ8ELQG5UE"
  name    = "internal-kbyg.staging.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
