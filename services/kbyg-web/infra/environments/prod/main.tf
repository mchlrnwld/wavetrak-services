provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/kbyg/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-3-prod"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/4796722fa18acc2f"
}

locals {
  dns_name      = "kbyg.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-kbyg.surfline.com"
    },
  ]
}

module "kbyg" {
  source = "../../"

  company          = "sl"
  application      = "kbyg"
  environment      = "prod"

  default_vpc   = "vpc-116fdb74"
  dns_name      = local.dns_name
  dns_zone_id   = "Z3LLOZIY0ZZQDE"
  ecs_cluster   = "sl-core-svc-prod"

  service_lb_rules      = local.service_lb_rules
  service_td_count      = 12

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  load_balancer_arn = data.aws_alb.main_internal.arn
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 200
  auto_scaling_min_size     = 4
  auto_scaling_max_size     = 5000
}

resource "aws_route53_record" "kbyg_internal" {
  zone_id = "ZY7MYOQ65TY5X"
  name    = "internal-kbyg.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
