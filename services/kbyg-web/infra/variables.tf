variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "alb_listener_arn" {
}

variable "default_vpc" {
}

variable "ecs_cluster" {
}

output "target_group" {
  value = module.kbyg.target_group
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_response_latency"
}

variable "auto_scaling_min_size" {
  default = 12
}

variable "auto_scaling_max_size" {
  default = 30
}

variable "auto_scaling_up_metric" {
  default = 0.6
}

variable "auto_scaling_down_metric" {
  default = 0.3
}

variable "auto_scaling_up_count" {
  default = 2
}

variable "auto_scaling_down_count" {
  default = -2
}

variable "auto_scaling_target_value" {
  default = ""
}
