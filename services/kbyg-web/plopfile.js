/**
 * @param {import('plop').NodePlopAPI} plop
 */
module.exports = (plop) => {
  // controller generator
  plop.setGenerator('component', {
    description: 'Create a new react component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Component name:',
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/components/{{properCase name}}/index.ts',
        templateFile: 'generators/component/index.ts.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{properCase name}}/{{properCase name}}.tsx',
        templateFile: 'generators/component/Component.tsx.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{properCase name}}/{{properCase name}}.spec.tsx',
        templateFile: 'generators/component/Component.spec.tsx.hbs',
      },
    ],
  });
};
