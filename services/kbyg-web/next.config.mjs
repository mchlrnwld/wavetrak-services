// eslint-disable-next-line import/extensions
import withImages from 'next-images';

const cdnUrls = {
  development: '/',
  sandbox: 'https://product-cdn.sandbox.surfline.com/kbyg/',
  staging: 'https://product-cdn.staging.surfline.com/kbyg/',
  production: 'https://wa.cdn-surfline.com/kbyg/',
};

const nextConfig = () =>
  /**
   * @type {import('next').NextConfig}
   */
  ({
    webpack: (config, { isServer }) => {
      if (isServer) {
        return config;
      }

      // eslint-disable-next-line no-param-reassign
      config.externals = {
        ...config.externals,
        react: 'vendor.React',
        'react-dom': 'vendor.ReactDOM',
      };

      return config;
    },
    async rewrites() {
      return [
        {
          source: '/surf-reports-forecasts-cams-map/@:location',
          destination: '/surf-reports-forecasts-cams-map/:location',
        },
        {
          source: '/surf-reports-forecasts-cams-map/buoys/@:location',
          destination: '/surf-reports-forecasts-cams-map/buoys/:location',
        },
        {
          source: '/health',
          destination: '/api/health',
        },
      ];
    },
    images: {
      domains: [
        'slcharts01.cdn-surfline.com',
        'slcharts02.cdn-surfline.com',
        'wa.cdn-surfline.com',
        'product-cdn.staging.surfline.com',
        'product-cdn.sandbox.surfline.com',
        'd14fqx6aetz9ka.cloudfront.net',
        'd2pn5sjh0l9yc4.cloudfront.net',
      ],
    },
    assetPrefix: cdnUrls[process.env.APP_ENV],
    productionBrowserSourceMaps: true,
    swcMinify: true,
    publicRuntimeConfig: {
      APP_ENV: process.env.APP_ENV,
    },
    env: {
      APP_ENV: process.env.APP_ENV,
    },
    crossOrigin: 'anonymous',
    eslint: {
      ignoreDuringBuilds: true,
    },
  });

const config = (phase) => withImages(nextConfig(phase));

export default config;
