variable "company" {
  default = "sl"
}

variable "application" {
  default = "admin-reports-service"
}

variable "environment" {
}

variable "alb_listener_arn" {
}

variable "iam_role_arn" {
}

variable "load_balancer_arn" {
}

variable "target_group_name" {
  default = ""
}

variable "default_vpc" {
}

variable "dns_zone" {
}

variable "container_name" {
  default = "reports"
}

variable "service_lb_healthcheck_path" {
  default = "/reports/health"
}

variable "service_lb_priority" {
  default = 200
}

variable "service_port" {
  default = 8080
}

variable "service_td_count" {
  default = 1
}

variable "task_deregistration_delay" {
  default = 60
}
