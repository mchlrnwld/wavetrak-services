# WaveTrak Notifications Service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Development](#development)
  - [notifications-api Directory](#notifications-api-directory)
    - [/common](#common)
    - [/error](#error)
    - [/model](#model)
    - [/server](#server)
- [API](#api) - [Table of Contents](#table-of-contents)
  - [Notifications](#notifications)
    - [Authentication](#authentication)
      - [Headers](#headers)
    - [API Errors](#api-errors)
  - [Notification Service API Endpoints](#notification-service-api-endpoints)
    - [Root URLs](#root-urls)
    - [Endpoints](#endpoints)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Development

### notifications-api Directory

```
 - /src
   | - /common
   | - /error
   | - /external
   | - /model
   | - /server
```

#### /common

Files included in the common directory should be generic and reusable throughout our suite of microservices.

#### /error

All errors thrown by the notifications service are defined within individual files named after their constructor type, and exported from `index.js`. **NOTE:** Try to make these errors as genaric as possible. We may want to resuse this logic within other services.

#### /model

Models included most of the business logic for this service. At the root, we define the parent Notifications schema from which all other `kind`s inherit from.

#### /server

The notifications service API is exposed under `/notifications`.

- TBD - document `/src` directory, linting, and test runners.

## Architecture

Archtecture documentation and diagram can be [found here](../../architecture/systems/notifications/README.md).

# API

#### Table of Contents

- [Notifications](##notifications)

## Notifications

This service is organized around REST, and provides a flexible yet predictable API that supports multiple type of notificationsd items.

### Authentication

##### Headers

Each request made to the notifications service is authenticated and requires a valid `X-Auth-AccessToken` or `X-Auth-UserId` to exist in the **request header**. Unauthorized requests will respond with the following response.

```
// HTTP GET `/notifications`
// Response statusCode: 401 Unauthorized
{
  "message": "You are unauthorized to view this resource"
}
```

### API Errors

| StatusCode | Message                                      | Triggered by                                                          |
| ---------- | -------------------------------------------- | --------------------------------------------------------------------- |
| 401        | "You are unauthorized to view this resource" | Invalid request headers                                               |
| 400        | "Invalid Parameters"                         | Ommitting `spotId` or other invalid, missing, or malformed parmeters. |
| 500        | "Internal Server Error"                      | We messed up...                                                       |

## Notification Service API Endpoints

### Root URLs

- Service URL: http://notifications-service.prod.surfline.com
  - internal services require x-auth-userid header (provided via public proxy via access tokens)
- Public URL: https://services.surfline.com/notifications
  - public services use services proxy to take url `accesstoken` or `x-auth-accesstoken` header to pass through `x-auth-userid` to underlying services (and provide top level authentication)

_Note_
Notification service need access to an `x-auth-userid` header (mongo user id).
The documentation below is specific to the Internal URL.

The Public URL requires passing an `accesstoken` which the proxy will translate into the necessary `x-auth-userid` header and pass along to the underlying service.

### Endpoints

- [Outlets](docs/outlets.md)
- [Preferences](docs/preferences.md)

# Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `notification-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4T1RZeU1USXdOVEUifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE9UWXlNVEl3TlRFIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [example requests](https://github.com/Surfline/wavetrak-services/blob/master/services/notifications-service/docs/outlets.md#example-request) section.

**Note:** An `X-Auth-UserId` must be passed in the header to perform the following `curl` requests. This value corresponds to the `_id` value in the Mongo `userInfo` collection.

**Ex:**

Test `GET` requests against the `/anonymous` endpoint:

```
curl -X GET http://notifications-service.sandbox.surfline.com/preferences/sessions \
	-H 'Content-Type: text/json; charset=utf-8' \
	-H 'x-auth-userid: 5cb8c278a9680b000ffc68be'
```

**Expected response:** If the user doesn't have any sessions notification preferences, you'll get:

```
{"message":"No session notification preference found for user: 5cb8c278a9680b000ffc68be"}%
```

If the user _does_ have session notification preferences, the response should look like the following. (See [documentation](file:///Users/pauloginni/tmp/replica/wavetrak-services/services/notifications-service/docs/preferences.md#get-specific-session-notification-preferences) for more information.

```
{
  "_id": "509e191e810c19729de660ea",
  "active": true
}
```
