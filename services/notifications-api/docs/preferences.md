<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Sessions Notifications](#sessions-notifications)
  - [Add Session Notification preferences](#add-session-notification-preferences)
  - [Update session notification preference](#update-session-notification-preference)
  - [Get specific session notification preferences](#get-specific-session-notification-preferences)
  - [Get all session notification preferences](#get-all-session-notification-preferences)
  - [Delete sessions notification preferences](#delete-sessions-notification-preferences)
- [KBYG Spots Notifications](#kbyg-spots-notifications)
  - [Save spot notification preferences](#save-spot-notification-preferences)
  - [Update spot notification preferences](#update-spot-notification-preferences)
  - [Update specific spot notification preferences](#update-specific-spot-notification-preferences)
  - [Get spot notification preferences by notificationId](#get-spot-notification-preferences-by-notificationid)
  - [Get all spot notification preferences](#get-all-spot-notification-preferences)
  - [Delete a spot notification preference](#delete-a-spot-notification-preference)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Sessions Notifications

## Add Session Notification preferences

Path: `POST /preferences/sessions`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

### Request Object
| Property         | Type      | Required | Description                                        |
|------------------|-----------|----------|----------------------------------------------------|
| `active`         | Boolean   |    No    | True/False     (default true)                      |

* active will be set to true by default, not a required request parameter.

#### Example Request
```json
curl -X POST http://notifications-service.prod.surfline.com/preferences/sessions \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001' \
-d @- << EOF
{
  "active": true
}
EOF
```

### Response Object Attributes
| Property              | Type             | Nullable | Description                          |
|-----------------------|------------------|----------|--------------------------------------|
| `_id`                 | String           | No       | Mongo ID of the notification         |
| `message`             | String           | No       | Success or failure message           |

#### Example Response Object (200 Success)
```json
{
  "_id": "509e191e810c19729de660ea",
  "message": "Session notification preferences saved"
}
```

#### Example Response Object (400 Error)
```json
{
  "message": "Session notification preference not saved"
}
```

## Update session notification preference

Path: `PUT /preferences/sessions/:notificationId`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

### Request Path Parameters
| Property          | Type      | Required | Description                                          |
|-------------------|-----------|----------|------------------------------------------------------|
| `:notificationId` | String    |    Yes   | device id defined on device and stored in mongo      |

### Request Object
| Property       | Type      | Required | Description                                             |
|----------------|-----------|----------|---------------------------------------------------------|
| active         | Boolean    |    Yes   | true/false                                             |

#### Example Request
```json
curl -X PUT http://notifications-service.prod.surfline.com/preferences/sessions/509e191e810c19729de660ea \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001' \
-d @- << EOF
{
  "active": true
}
EOF
```

### Response Object Attributes
| Property              | Type             | Nullable | Description                          |
|-----------------------|------------------|----------|--------------------------------------|
| `_id`                 | String           | No       | Mongo ID of the notification         |
| `message`             | String           | No       | Success or failure message           |

#### Example Response Object (Success)
```json
{
  "_id": "509e191e810c19729de660ea",
  "message": "Session notification preferences saved"
}
```

#### Example Response Object (Error)
```json
{
  "message": "Session notification information not saved"
}
```

## Get specific session notification preferences

Path: `GET /preferences/sessions`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

#### Example Request
```json
curl -X GET http://notifications-service.prod.surfline.com/preferences/sessions \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001'
```

### Response Object Attributes
| Property            | Type    | Nullable | Description                     |
|---------------------|---------|----------|---------------------------------|
| `_id`               | String  | No       | Mongo ID of the notification    |
| `active`            | Boolean | No       | true/false                      |

#### Example Response Object (200 Success)
```json
{
  "_id": "509e191e810c19729de660ea",
  "active": true
}
```

#### Example Response Object (404 Error)
```json
{
  "message": "Session notification preference not found"
}
```

## Get all session notification preferences

Path: `GET /preferences/sessions`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

#### Example Request
```json
curl -X GET http://notifications-service.prod.surfline.com/preferences/sessions \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001'
```

### Response Object Attributes
| Property            | Type    | Nullable | Description                     |
|---------------------|---------|----------|---------------------------------|
| `_id`               | String  | No       | Mongo ID of the notification    |
| `active`            | Boolean | No       | true/false                      |

* Returns an array of objects matching above fields.
* Will be a single array element as it acts as a toggle

#### Example Response Object (200 Success)
```json
[
{
  "_id": "509e191e810c19729de660ea",
  "active": true
},
]
```

#### Example Response Object (404 Error)
```json
{
  "message": "Session notification preference not found"
}
```

## Delete sessions notification preferences

Path: `DELETE /preferences/sessions/:notificationId`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

### Request Path Parameters
| Property          | Type      | Required | Description                                          |
|-------------------|-----------|----------|------------------------------------------------------|
| `:notificationId` | String    |    Yes   | device id defined on device and stored in mongo      |

#### Example Request
```json
curl -X DELETE http://notifications-service.prod.surfline.com/preferences/sessions/537f191e810c19729de860ea \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001'
```

### Response Object Attributes
| Property            | Type             | Nullable | Description                       |
|---------------------|------------------|----------|-----------------------------------|
| `message`           | String           | No       | Success or failure message        |

#### Example Response Object (200 Success)
```json
{
  "message": "Session notification preference deleted successfully"
}
```

#### Example Response Object (400 Error)
```json
{
  "message": "Session notification preference not deleted"
}
```

# KBYG Spots Notifications

## Save spot notification preferences

Path: `POST /preferences/spots`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

### Request Object
| Property               | Type      | Required | Description                                    |
|------------------------|------------------|----------|-----------------------------------------|
| `spot`                 | String           |    Yes   | MongoID of associated spot.             |
| `am`                   | Boolean          |    No    | Receive am spot notifications?          |
| `pm`                   | Boolean          |    No    | Receive pm spot notifications?          |
| `minWaveHeight`        | Float            |    No    | Minimum waveHeight match preference     |
| `maxWaveHeight`        | Float            |    No    | Maximum waveHeight match preference     |
| `condition`            | String (enum)    |    No    | Minimum condition to match on           |

* active by default, not required field for POST

#### Example Request
```json
curl -X POST http://notifications-service.prod.surfline.com/preferences/spots \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001' \
-d @- << EOF
{
  "spot": "609e191e810c19729de660ea",
  "am": true,
  "pm": false,
  "minWaveHeight": 1,
  "maxWaveHeight": 4,
  "condition": "GOOD",
  "active": true
}
EOF
```

### Response Object Attributes
| Property              | Type             | Nullable | Description                          |
|-----------------------|------------------|----------|--------------------------------------|
| `_id`                 | String           | No       | Mongo ID of the notification         |
| `message`             | String           | No       | Success or failure message           |

#### Example Response Object (200 Success)
```json
{
  "_id": "509e191e810c19729de660ea",
  "message": "Spot notification preferences saved successfully"
}
```

#### Example Response Object (400 Error)
```json
{
  "message": "Spot notification preferences not saved"
}
```

## Update spot notification preferences

Path: `PUT /preferences/spots/:notificationId`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |


### Request Path Parameters
| Property          | Type      | Required | Description                                          |
|-------------------|-----------|----------|------------------------------------------------------|
| `:notificationId` | String    |    Yes   | device id defined on device and stored in mongo      |

### Request Object
| Property               | Type          | Required | Description                                    |
|------------------------|---------------|----------|------------------------------------------------|
| `spot`                 | String        |    Yes   | MongoID of associated spot.                    |
| `am`                   | Boolean       |    Yes   | Receive am spot notifications?                 |
| `pm`                   | Boolean       |    Yes   | Receive pm spot notifications?                 |
| `minWaveHeight`        | Float         |    No    | Minimum waveHeight match preference            |
| `maxWaveHeight`        | Float         |    No    | Maximum waveHeight match preference            |
| `condition`            | String (enum) |    No    | Minimum condition to match on                  |
| `active`               | Boolean       |    Yes   | true/false (defaults to true)                  |

#### Example Request
```json
curl -X PUT http://notifications-service.prod.surfline.com/preferences/spots/509e191e810c19729de660ea \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001' \
-d @- << EOF
{
  "spot": "609e191e810c19729de660ea",
  "am": true,
  "pm": false,
  "minWaveHeight": 1,
  "maxWaveHeight": 4,
  "condition": "GOOD",
  "active": true
}
EOF
```

### Response Object Attributes
| Property              | Type             | Nullable | Description                          |
|-----------------------|------------------|----------|--------------------------------------|
| `_id`                 | String           | No       | Mongo ID of the notification         |
| `message`             | String           | No       | Success or failure message           |

#### Example Response Object (Success)
```json
{
  "_id": "509e191e810c19729de660ea",
  "message": "Spot notification information saved successfully"
}
```

#### Example Response Object (Error)
```json
{
  "message": "Spot notification preferences saved"
}
```

## Update specific spot notification preferences

Path: `PATCH /preferences/spots/:notificationId`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |


### Request Path Parameters
| Property          | Type      | Required | Description                                          |
|-------------------|-----------|----------|------------------------------------------------------|
| `:notificationId` | String    |    Yes   | device id defined on device and stored in mongo      |

### Request Object
| Property               | Type          | Required | Description                                    |
|------------------------|---------------|----------|------------------------------------------------|
| `spot`                 | String        |    Yes   | MongoID of associated spot.                    |
| `am`                   | Boolean       |    No    | Receive am spot notifications?                 |
| `pm`                   | Boolean       |    No    | Receive pm spot notifications?                 |
| `minWaveHeight`        | Float         |    No    | Minimum waveHeight match preference            |
| `maxWaveHeight`        | Float         |    No    | Maximum waveHeight match preference            |
| `condition`            | String (enum) |    No    | Minimum condition to match on                  |
| `active`               | Boolean       |    Yes   | true/false (defaults to true)                  |

* Can send any or all of the above items as part of the request to update specific value.

#### Example Request
```json
curl -X PATCH http://notifications-service.prod.surfline.com/preferences/spots/509e191e810c19729de660ea \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001' \
-d @- << EOF
{
  "spot": "609e191e810c19729de660ea",
  "am": true,
}
EOF
```

### Response Object Attributes
| Property              | Type             | Nullable | Description                          |
|-----------------------|------------------|----------|--------------------------------------|
| `_id`                 | String           | No       | Mongo ID of the notification         |
| `message`             | String           | No       | Success or failure message           |

#### Example Response Object (Success)
```json
{
  "_id": "509e191e810c19729de660ea",
  "message": "Spot notification information saved successfully"
}
```

#### Example Response Object (Error)
```json
{
  "message": "Spot notification preferences saved"
}
```

## Get spot notification preferences by notificationId

Path: `GET /preferences/spots/:notificationId`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

### Request Path Parameters
| Property          | Type      | Required | Description                                          |
|-------------------|-----------|----------|------------------------------------------------------|
| `:notificationId` | String    |    Yes   | device id defined on device and stored in mongo      |

#### Example Request
```json
curl -X GET http://notifications-service.prod.surfline.com/preferences/spots/529e191e810c19729de660ea \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001'
```

### Response Object Attributes
| Property               | Type             | Nullable | Description                                      |
|------------------------|------------------|----------|--------------------------------------------------|
| `_id`                  | String           |    No    | mongo id of notification on success              |
| `spot`                 | String           |    No    | MongoID of associated spot.                      |
| `am`                   | Boolean          |    Yes   | Receive am spot notifications?                   |
| `pm`                   | Boolean          |    Yes   | Receive pm spot notifications?                   |
| `minWaveHeight`        | Float            |    No    | Minimum waveHeight match preference              |
| `maxWaveHeight`        | Float            |    No    | Maximum waveHeight match preference              |
| `condition`            | String (enum)    |    No    | Minimum condition to match on                    |
| `active`               | Boolean          |    No    | true/false (defaults to true)                    |


#### Example Response Object (200 Success)
```json
{
  "_id": "529e191e810c19729de660ea",
  "spot": "509e191e810c19729de660ea",
  "am": true,
  "pm": false,
  "minWaveHeight": 1,
  "maxWaveHeight": 1,
  "condition": "FAIR",
}
```

#### Example Response Object (404 Error)
```json
{
  "message": "Spot notification preference not found"
}
```

## Get all spot notification preferences

Path: `GET /preferences/spots`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

#### Example Request
```json
curl -X GET http://notifications-service.prod.surfline.com/preferences/spots \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001'
```

### Response Object Attributes
Array of the following object properties, matching single spot notification GET (see example)

| Property.              | Type             | Nullable | Description                                      |
|------------------------|------------------|----------|--------------------------------------------------|
| `_id`                  | String           |    No    | mongo id of notification on success              |
| `spot`                 | String           |    No    | MongoID of associated spot.                      |
| `am`                   | Boolean          |    Yes   | Receive am spot notifications?                   |
| `pm`                   | Boolean          |    Yes   | Receive pm spot notifications?                   |
| `minWaveHeight`        | Float            |    No    | Minimum waveHeight match preference              |
| `maxWaveHeight`        | Float            |    No    | Maximum waveHeight match preference              |
| `condition`            | String (enum)    |    No    | Minimum condition to match on                    |
| `active`               | Boolean          |    No    | true/false (defaults to true)                    |

#### Example Response Object (200 Success)
```json
[
{
  "_id": "509e191e810c19729de660ea",
  "spot": "509e191e810c19729de660ea",
  "am": true,
  "pm": false,
  "minWaveHeight": 1,
  "maxWaveHeight": 1,
  "condition": "POOR"
},
{
  "_id": "529e191e810c19729de660ea",
  "spot": "508f191e810c19729de660ea",
  "am": false,
  "pm": true,
  "minWaveHeight": 1,
  "maxWaveHeight": 1,
  "condition": "FAIR"
}
]
```

#### Example Response Object (400 Error)
```json
{
  "message": "Spot notification preference not found"
}
```

## Delete a spot notification preference

Path: `DELETE /preferences/spots/:notificationId`

### Request Headers
| Property             | Type     | Required | Description                                        |
|----------------------|----------|----------|----------------------------------------------------|
| `x-auth-userid`      | String   |  Yes     | mongo id of logged in user (passed by proxy)       |

### Request Path Parameters
| Property          | Type      | Required | Description                                          |
|-------------------|-----------|----------|------------------------------------------------------|
| `:notificationId` | String    |    Yes   | device id defined on device and stored in mongo      |

#### Example Request
```json
curl -X DELETE http://notifications-service.prod.surfline.com/preferences/spots/537f191e810c19729de860ea \
-H 'Content-Type: text/json; charset=utf-8' \
-H 'x-auth-userid: 50001'
```

### Response Object Attributes
| Property            | Type             | Nullable | Description                         |
|---------------------|------------------|----------|-------------------------------------|
| `message`             | String           | No       | Success or failure message        |

#### Example Response Object (200 Success)
```json
{
  "message": "Spot notification preferences deleted successfully"
}
```

#### Example Response Object (400 Error)
```json
{
  "message": "Spot notification preferences not deleted"
}
```
