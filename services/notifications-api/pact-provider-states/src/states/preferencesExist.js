import { ObjectID } from 'mongodb';

/* eslint-disable arrow-parens */
const preferencesExist = db => db.collection('NotificationPreferences').insertOne(
  {
    _id: new ObjectID('5842041f4e65fad6a77088ed'),
    spot: new ObjectID('609e191e810c19729de660ea'),
    am: true,
    pm: false,
    minWaveHeight: 1,
    maxWaveHeight: 4,
    condition: 'GOOD',
    active: true,
  },
);

export default preferencesExist;
