import { ObjectID } from 'mongodb';

const outletsExist = db => db.collection('Outlets').insertOne(
  {
    _id: new ObjectID('5842041f4e65fad6a77088ed'),
    appName: 'SURFLINE',
    deviceToken: 'SOMETOKEN',
    channel: 'APNS',
    user: new ObjectID('582e0255d7e44b1074114ba9'),
  },
);

export default outletsExist;
