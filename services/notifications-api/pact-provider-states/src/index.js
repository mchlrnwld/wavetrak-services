import connectMongo from './helpers/connectMongo';
import server from './server';

connectMongo().then(server);
