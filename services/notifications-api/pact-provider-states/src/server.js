import express from 'express';
import bodyParser from 'body-parser';
import cleanMongo from './helpers/cleanMongo';
import outletsExist from './states/outletsExist';
import preferencesExist from './states/preferencesExist';

const states = {
  'outlets exist': outletsExist,
  'preferences exist': preferencesExist,
};

const server = (db) => {
  const app = express();

  app.use(bodyParser.json());

  app.post('/setup', async (req, res) => {
    try {
      await cleanMongo(db);

      const stateSetup = states[req.body.state];
      if (stateSetup) await stateSetup(db);

      return res.end();
    } catch (err) {
      return res.status(500).send({
        message: err.message,
      });
    }
  });

  app.listen(80, () => {
    console.log('Pact provider states listening on port 80');
  });
};

export default server;
