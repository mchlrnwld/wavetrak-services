import { MongoClient } from 'mongodb';

const connectMongo = () => MongoClient.connect('mongodb://mongo:27017/UserDB');

export default connectMongo;
