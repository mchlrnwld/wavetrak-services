import outlets from './outlets';
import preferences from './preferences';
import webhooks from './webhooks';

const routes = () => {
  describe('/', () => {
    outlets();
    preferences();
    webhooks();
  });
};

export default routes;
