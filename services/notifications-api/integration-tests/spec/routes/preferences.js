import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import config from '../config';
import cleanMongo from '../helpers/cleanMongo';

const preferences = () => {
  describe('/preferences/spots', () => {
    let db;
    const preferenceId = new ObjectID('5908c45e6a2e4300134fbe92');
    const userId = new ObjectID('5908c45e6a2e4300134fbe93');

    before(async () => {
      db = await connectMongo();
    });

    after(() => {
      db.close();
    });

    afterEach(async () => {
      await cleanMongo(db);
    });

    describe('POST', () => {
      it('creates a preference', async () => {
        const model = {
          _id: preferenceId,
          spot: '609e191e810c19729de660ea',
          am: true,
          pm: false,
          minWaveHeight: 1,
          maxWaveHeight: 4,
          condition: 'GOOD',
          active: true,
          kind: 'SPOT',
        };
        const response = await fetch(`${config.NOTIFICATIONS_SERVICE}/preferences/spots`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-auth-userid': userId,
          },
          body: JSON.stringify(model),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.be.ok();

        const preference = await db
          .collection('NotificationPreferences')
          .findOne({ _id: new ObjectID(body._id) });
        expect(preference).to.be.ok();
      });
    });

    describe('DELETE', () => {
      const deletePreferenceId = new ObjectID();

      before(async () => {
        await db.collection('NotificationPreferences').insert({
          _id: deletePreferenceId,
          spot: '609e191e810c19729de660ea',
          am: true,
          pm: false,
          minWaveHeight: 1,
          maxWaveHeight: 4,
          condition: 'GOOD',
          active: true,
          user: userId,
          kind: 'SPOT',
        });
      });
      it('deletes a preference', async () => {
        const response = await fetch(
          `${config.NOTIFICATIONS_SERVICE}/preferences/spots/${deletePreferenceId}`,
          {
            headers: {
              'x-auth-userid': userId,
            },
            method: 'DELETE',
          },
        );
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body.message).to.equal('Spot notification preferences deleted successfully');

        const preference = await db
          .collection('NotificationPreferences')
          .findOne({ _id: deletePreferenceId });
        expect(preference).to.be.null();
      });
    });

    describe('PUT', () => {
      before(async () => {
        await Promise.all([
          db.collection('NotificationPreferences').insert({
            _id: preferenceId,
            spot: '609e191e810c19729de660ea',
            am: true,
            kind: 'SPOT',
            pm: false,
            minWaveHeight: 1,
            maxWaveHeight: 4,
            condition: 'GOOD',
            active: true,
            user: userId,
          }),
        ]);
      });

      it('updates a preference', async () => {
        const response = await fetch(
          `${config.NOTIFICATIONS_SERVICE}/preferences/spots/${preferenceId}`,
          {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'x-auth-userid': userId,
            },
            body: JSON.stringify({
              _id: preferenceId,
              spot: '609e191e810c19729de660ea',
              am: true,
              pm: false,
              minWaveHeight: 1,
              maxWaveHeight: 4,
              condition: 'EPIC',
              active: true,
            }),
          },
        );
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const preference = await db
          .collection('NotificationPreferences')
          .findOne({ _id: preferenceId });
        expect(preference).to.be.ok();
        expect(preference.condition).to.equal('EPIC');
      });
    });
  });
};

export default preferences;
