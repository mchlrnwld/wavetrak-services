import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';

const outlets = () => {
  describe('/outlets', () => {
    let db;
    const endpointArn = 'arn:aws:sns:us-west-1:123456789100:test-endpoint-arn';
    const outletId = new ObjectID('5908c45e6a2e4300134fbe92');
    const userId = new ObjectID('5908c45e6a2e4300134fbe93');
    const deviceToken = 'SOMETOKEN';

    before(async () => {
      db = await connectMongo();
    });

    beforeEach(async () => {
      await Promise.all([
        db.collection('Outlets').insert({
          _id: outletId,
          deviceToken,
          endpointArn,
          appName: 'SURFLINE',
          channel: 'APNS',
          user: userId,
        }),
      ]);
    });

    afterEach(async () => {
      await cleanMongo(db);
    });

    after(() => {
      db.close();
    });

    it('POST /outlets creates an outlet', async () => {
      const model = {
        appName: 'SURFLINE',
        deviceToken: 'POSTTOKEN',
        channel: 'APNS',
        user: userId,
      };

      const response = await fetch(`${config.NOTIFICATIONS_SERVICE}/outlets`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'x-auth-userid': userId,
        },
        body: JSON.stringify(model),
      });
      expect(response.status).to.equal(200);
      const body = await response.json();
      expect(body).to.be.ok();
      expect(body._id).to.be.ok();

      const id = body._id;
      const outlet = await db.collection('Outlets').findOne({ _id: new ObjectID(id) });

      expect(outlet.deviceToken).to.equal('POSTTOKEN');
    });

    it('PUT /outlets updates an outlet', async () => {
      const response = await fetch(`${config.NOTIFICATIONS_SERVICE}/outlets/${outletId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'x-auth-userid': userId,
        },
        body: JSON.stringify({
          _id: outletId,
          appName: 'BUOYWEATHER',
          deviceToken: 'SOMETOKEN1',
          channel: 'APNS',
          user: '582e0255d7e44b1074114ba9',
        }),
      });

      expect(response.status).to.equal(200);
      const body = await response.json();
      expect(body).to.be.ok();

      const outlet = await db.collection('Outlets').findOne({ _id: outletId });

      expect(outlet).to.be.ok();
      expect(outlet.deviceToken).to.equal('SOMETOKEN1');
      expect(outlet._id).to.deep.equal(outletId);
    });


    it('DELETE /outlets deletes an outlet', async () => {
      const delOutletId = new ObjectID();

      db.collection('Outlets').insert({
        _id: delOutletId,
        deviceToken: 'SOMETOKEN_TO_DELETE',
        endpointArn,
        appName: 'SURFLINE',
        channel: 'APNS',
        user: userId,
      });

      const response = await fetch(`${config.NOTIFICATIONS_SERVICE}/outlets/${delOutletId}`, {
        headers: {
          'x-auth-userid': userId,
        },
        method: 'DELETE',
      });
      expect(response.status).to.equal(200);
      const body = await response.json();
      expect(body).to.be.ok();
      expect(body.message).to.equal('Outlet deleted successfully');

      const outlet = await db.collection('Outlets').findOne({ _id: delOutletId });
      expect(outlet).to.be.null();
    });
  });
};

export default outlets;
