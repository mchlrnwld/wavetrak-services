import chai from 'chai';
import dirtyChai from 'dirty-chai';
import pact from './pact';
import routes from './routes';

chai.use(dirtyChai);

describe('Notifications Service', () => {
  pact();
  routes();
});
