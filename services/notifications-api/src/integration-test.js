/* eslint-disable import/no-extraneous-dependencies */
import awsMock from 'aws-sdk-mock';
import AWS from 'aws-sdk';

awsMock.setSDKInstance(AWS);

awsMock.mock('SNS', 'publish', 'test message please ignore');
awsMock.mock(
  'SNS',
  'createPlatformEndpoint',
  { EndpointArn: 'arn:aws:sns:us-west-1:123456789100:test-endpoint-arn' },
);
awsMock.mock('SNS', 'setEndpointAttributes', 'test response please ignore');
awsMock.mock('SNS', 'deleteEndpoint', 'test response please ignore');
awsMock.mock('S3', 'putObject', 'test response please ignore');

require('./index');
