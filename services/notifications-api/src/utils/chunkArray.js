const chunkArray = (array, size) => {
  const chunkedArray = [];
  const copied = [...array];
  const numOfChild = Math.ceil(copied.length / size);
  for (let i = 0; i < numOfChild; i += 1) {
    chunkedArray.push(copied.splice(0, size));
  }
  return chunkedArray;
};

export default chunkArray;
