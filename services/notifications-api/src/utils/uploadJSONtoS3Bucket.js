import AWS from 'aws-sdk';
import config from '../config';

const uploadJSONtoS3Bucket = (json, fileName, path = config.NOTIFICATION_BATCHES_S3_BUCKET) => {
  const s3 = new AWS.S3();

  return new Promise((resolve, reject) => {
    const data = {
      Key: fileName,
      Body: JSON.stringify(json),
      Bucket: path,
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    s3.putObject(data, (err) => {
      if (err) {
        return reject(err);
      }
      return resolve({
        name: fileName,
        path,
      });
    });
  });
}

export default uploadJSONtoS3Bucket;
