import { redisPromise } from '../model/dbContext';

const redisKeyPrefix = 'notifications-api';

export const setKey = async (key, cacheTimeout) => {
  const fullKey = `${redisKeyPrefix}:${key}`;
  const keySet = await redisPromise().setex(fullKey, cacheTimeout, JSON.stringify({ set: true }));
  if (keySet && keySet.indexOf('OK') > -1) {
    return true;
  }
  throw new Error(`Unable to set key: ${key}`);
};

export const setNxKey = async (key, cacheTimeout) => {
  const fullKey = `${redisKeyPrefix}:${key}`;
  const keySet = await redisPromise().set(fullKey, 1, 'EX', cacheTimeout, 'NX');
  if (keySet && keySet.indexOf('OK') > -1) {
    return true;
  }
  throw new Error(`Unable to set key: ${key}`);
};

export const getKey = async key => {
  const fullKey = `${redisKeyPrefix}:${key}`;
  const data = await redisPromise().get(fullKey);
  return data && data.length;
};
