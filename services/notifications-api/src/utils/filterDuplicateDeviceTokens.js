import _uniqBy from 'lodash/uniqBy';

export default outlets => _uniqBy(outlets, 'deviceToken');
