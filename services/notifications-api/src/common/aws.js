import AWS from 'aws-sdk';
import config from '../config';

const { SNS_API_VERSION, APNS_PLATFORM_APPLICATION_ARN } = config;

export const createPlatformEndpoint = deviceToken => {
  const SNS = new AWS.SNS({
    apiVersion: SNS_API_VERSION,
    region: 'us-west-1',
  });
  return SNS.createPlatformEndpoint({
    PlatformApplicationArn: APNS_PLATFORM_APPLICATION_ARN,
    Token: deviceToken,
  }).promise();
}

export const updatePlatformEndpoint = async (endpointArn, deviceToken) => {
  const SNS = new AWS.SNS({
    apiVersion: SNS_API_VERSION,
    region: 'us-west-1',
  });

  const params = {
    EndpointArn: endpointArn,
    Attributes: {
      Enabled: 'true',
      Token: deviceToken,
    },
  };

  return SNS.setEndpointAttributes(params).promise();
};

export const deletePlatformEndpoint = async endpointArn => {
  const SNS = new AWS.SNS({
    apiVersion: SNS_API_VERSION,
    region: 'us-west-1',
  });

  return SNS.deleteEndpoint({
    EndpointArn: endpointArn,
  }).promise();
}

/**
 * @description Subscription confirmation function for AWS SNS Subscriptions
 *
 * @param {string} token
 * @param {string} topicArn
 * @param {string} [authenticateOnUnsubscribe='false']
 */
export const confirmSubscription = async (token, topicArn, authenticateOnUnsubscribe = 'false') => {
  const SNS = new AWS.SNS({
    apiVersion: SNS_API_VERSION,
    region: 'us-west-1',
  });

  return SNS.confirmSubscription({
    Token: token,
    TopicArn: topicArn,
    AuthenticateOnUnsubscribe: authenticateOnUnsubscribe,
  }).promise();
}

/**
 * @description This function is used to publish a message to
 * an SNS topic.
 * Before being sent, the `message` is passed into `JSON.stringify()`.
 * @param {string} topicArn
 * @param {Object} message
 */
export const publishMessage = async (topicArn, message) => {
  const SNS = new AWS.SNS({
    apiVersion: SNS_API_VERSION,
    region: 'us-west-1',
  });
  
  return SNS.publish({
    TopicArn: topicArn,
    Message: JSON.stringify(message),
  }).promise();
}

/**
 * @description Invokes an AWS Lambda Function Asynchronously.
 * @param {*} payload 
 */
export const invokeLambda = (payload, functionName) => {
  const lambda = new AWS.Lambda({
    apiVersion: '2015-03-31',
    region: 'us-west-1',
  });

  return lambda.invoke({
    FunctionName: functionName,
    InvocationType: 'Event',
    LogType: 'None',
    Payload: JSON.stringify(payload),
  }).promise();
}
