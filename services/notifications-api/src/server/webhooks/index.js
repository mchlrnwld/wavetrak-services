import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  spotDetailsPostHandler,
  sessionDetailsPostHandler,
} from './handlers';

export const trackRequests = log => (req, _, next) => {
  log.trace({
    action: '/notifications/webhooks',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default log => {
  const api = Router();
  api.use(
    '*',
    (req, _, next) => {
      if (req.headers['x-amz-sns-message-type']) {
        req.headers['content-type'] = 'application/json';
      }
      next();
    },
    json(),
    trackRequests(log),
  );
  api.post('/spotDetails', wrapErrors(spotDetailsPostHandler));
  api.post('/sessionDetails', wrapErrors(sessionDetailsPostHandler));

  return api;
};
