import chai, { expect } from 'chai';
import sinon from 'sinon';
import { format } from 'date-fns';
import express from 'express';
import { Types } from 'mongoose';
import webhooks from '.';
import Outlets, * as outletModel from '../../model/Outlets/schema';
import SessionPreferences, * as sessionPreferencesModel from '../../model/Preferences/Sessions/schema';
import SpotPreferences, * as spotPreferencesModel from '../../model/Preferences/Spots/schema';
import * as aws from '../../common/aws';
import * as uploadJSONtoS3Bucket from '../../utils/uploadJSONtoS3Bucket';

describe('/webhooks', () => {
  let log;
  let request;
  const user = Types.ObjectId().toString();
  const sessionId = Types.ObjectId().toString();
  let clock;
  let dateTimeString;

  const start = 1546329600000; // 01/01/2019@00:00PDT

  before(() => {
    clock = sinon.useFakeTimers(start);
    log = { trace: sinon.spy() };
    const app = express();
    app.use(webhooks(log));
    request = chai.request(app).keepOpen();
    dateTimeString = format(new Date(), 'YYYY-MM-DD-H-m');
  });

  beforeEach(() => {
    sinon.stub(Outlets, 'findOne');
    sinon.stub(Outlets, 'findOneAndUpdate');
    sinon.stub(outletModel, 'createOutlet');
    sinon.stub(outletModel, 'getSurflineOutlet');
    sinon.stub(outletModel, 'getSurflineOutlets');

    sinon.stub(aws, 'createPlatformEndpoint');
    sinon.stub(aws, 'updatePlatformEndpoint');
    sinon.stub(aws, 'invokeLambda');
    sinon.stub(uploadJSONtoS3Bucket, 'default');

    sinon.stub(sessionPreferencesModel, 'getSessionNotificationPreference');
    sinon.stub(sessionPreferencesModel, 'createSessionNotificationPreference');

    sinon.stub(spotPreferencesModel, 'createSpotNotification');

    sinon.stub(SpotPreferences, 'find');
    sinon.stub(SpotPreferences, 'findOne');

    sinon.stub(SessionPreferences, 'find');
    sinon.stub(SessionPreferences, 'findOne');
  });

  afterEach(() => {
    Outlets.findOne.restore();
    Outlets.findOneAndUpdate.restore();
    outletModel.createOutlet.restore();
    outletModel.getSurflineOutlet.restore();
    outletModel.getSurflineOutlets.restore();

    aws.createPlatformEndpoint.restore();
    aws.updatePlatformEndpoint.restore();
    aws.invokeLambda.restore();
    uploadJSONtoS3Bucket.default.restore();

    sessionPreferencesModel.getSessionNotificationPreference.restore();
    sessionPreferencesModel.createSessionNotificationPreference.restore();

    spotPreferencesModel.createSpotNotification.restore();

    SpotPreferences.find.restore();
    SpotPreferences.findOne.restore();

    SessionPreferences.find.restore();
    SessionPreferences.findOne.restore();
  });

  after(() => {
    clock.restore();
  });

  it('should POST invoke the notification processor for an active notification user', async () => {
    const outletStub = {
      _id: Types.ObjectId().toString(),
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
      endpointArn: 'test-endpoint-arn',
      user,
    };
    const preferenceStub = {
      active: true,
      kind: 'SESSION',
      user,
    };
    const messagePayload = {
      sessionId,
      user,
      waveCount: 1,
      spot: {},
    };

    outletModel.getSurflineOutlets.resolves([outletStub]);
    sessionPreferencesModel.getSessionNotificationPreference.resolves(preferenceStub);
    uploadJSONtoS3Bucket.default.resolves();

    const res = await request
      .post('/sessionDetails')
      .set('x-amz-sns-message-type', 'Notification')
      .set('Content-Type', 'text/plain')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send(
        JSON.stringify({
          Message: JSON.stringify(messagePayload),
        }),
      );

    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      message: 'Session batch uploaded successfully',
    });

    expect(
      sessionPreferencesModel.getSessionNotificationPreference,
    ).to.have.been.calledOnceWithExactly(user);
    expect(outletModel.getSurflineOutlets).to.have.been.calledOnceWithExactly(user);
    expect(uploadJSONtoS3Bucket.default).to.have.been.calledOnce();
    expect(uploadJSONtoS3Bucket.default).to.have.been.calledWithExactly(
      {
        type: 'SESSION',
        session: {
          ...messagePayload,
        },
        outlets: [
          {
            appName: outletStub.appName,
            channel: outletStub.channel,
            endpointArn: outletStub.endpointArn,
          },
        ],
      },
      `session-${sessionId}-${dateTimeString}.json`,
    );
  });
});
