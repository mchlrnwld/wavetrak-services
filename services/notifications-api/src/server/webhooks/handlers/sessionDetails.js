import newrelic from 'newrelic';
import { format } from 'date-fns';
import { confirmSubscription } from '../../../common/aws';
import { getSessionNotificationPreference } from '../../../model/Preferences/Sessions/schema';
import { getSurflineOutlets } from '../../../model/Outlets/schema';
import uploadJSONtoS3Bucket from '../../../utils/uploadJSONtoS3Bucket';
import filterDuplicateDeviceTokens from '../../../utils/filterDuplicateDeviceTokens';

const SESSION = 'SESSION';

export const sessionDetailsGetHandler = async (_, res) => res.send({});

export const sessionDetailsPostHandler = async (req, res) => {
  const messageType = req.get('x-amz-sns-message-type');
  if (messageType === 'SubscriptionConfirmation') {
    const { Token: token } = req.body;
    const topicArn = req.get('x-amz-sns-topic-arn');
    await confirmSubscription(token, topicArn);

    return res.send({
      message: 'Subscription confirmed successfully',
    });
  }

  if (messageType === 'Notification') {
    const message = JSON.parse(req.body.Message);
    const { sessionId, user, waveCount, spot } = message;

    const preference = await getSessionNotificationPreference(user);

    if (preference?.active) {
      const outlets = await getSurflineOutlets(user);

      if (outlets) {
        const outletsToSend = filterDuplicateDeviceTokens(outlets).map(
          ({ channel, endpointArn, appName }) => ({
            channel,
            endpointArn,
            appName,
          }),
        );

        const sessionNotificationJson = {
          type: SESSION,
          session: {
            sessionId,
            user,
            waveCount,
            spot,
          },
          outlets: outletsToSend,
        };

        const dateTime = format(new Date(), 'YYYY-MM-DD-H-m');
        try {
          await uploadJSONtoS3Bucket(
            sessionNotificationJson,
            `session-${sessionId}-${dateTime}.json`,
          );
          return res.send({
            message: 'Session batch uploaded successfully',
          });
        } catch (error) {
          console.log(error);
          newrelic.noticeError(error);
          throw new Error('Session notification not sent');
        }
      }
    }

    return res.send({ message: 'Session notifications not active.' });
  }

  return res.status(400).send('Invalid message type.');
};
