import moment from 'moment-timezone';
import newrelic from 'newrelic';
import { APIError } from '@surfline/services-common';
import { format } from 'date-fns';
import { getSpotReport, getSpotReportView } from '../../../external/reports';
import { getSpot } from '../../../external/spots';
import getSubscribe from '../../../external/sns';
import SpotsNotificationModel from '../../../model/Preferences/Spots/schema';
import OutletsModel from '../../../model/Outlets/schema';
import { getKey, setKey } from '../../../utils/redisKeyUtils';
import uploadJSONtoS3Bucket from '../../../utils/uploadJSONtoS3Bucket';
import chunkArray from '../../../utils/chunkArray';
import config from '../../../config';
import filterDuplicateDeviceTokens from '../../../utils/filterDuplicateDeviceTokens';

const SPOT = 'SPOT';
const conditionTypes = [
  'FLAT',
  'VERY_POOR',
  'POOR',
  'POOR_TO_FAIR',
  'FAIR',
  'FAIR_TO_GOOD',
  'GOOD',
  'VERY_GOOD',
  'GOOD_TO_EPIC',
  'EPIC',
];
const notificationHandler = async message => {
  try {
    const { spotId } = JSON.parse(message);
    if (!spotId) {
      throw new APIError(`Valid spotId required, instead received: ${spotId}`, { statusCode: 400 });
    }
    newrelic.addCustomAttribute('spotId', spotId);
    const spot = await getSpot(spotId);
    if (!spot || spot?.status !== "PUBLISHED") {
      throw new APIError(`Valid spotId required, instead received: ${spotId}`, { statusCode: 400 });
    }
    const spotReportsArray = await getSpotReport(spotId);
    const spotReport = spotReportsArray.spotReports[0];
    const { rating, maxHeight, minHeight, occasionalHeight } = spotReport;
    const createdAt = moment(spotReport.createdAt);
    const localTime = moment.tz(createdAt, spot.timezone);
    const isAm = localTime.format('A') === 'AM';
    const redisKeyPrefix = 'notification-handler';
    const conditionsMatchKey = `${spotId}-${minHeight}-${maxHeight}-${rating}-${isAm}`;
    const redisKey = `${redisKeyPrefix}:${conditionsMatchKey}`;
    const keyExists = await getKey(redisKey);
    newrelic.addCustomAttributes({
      rating,
      maxHeight,
      minHeight,
      occasionalHeight,
      conditionsMatchKey,
      redisKey,
      keyExists,
    });
    if (keyExists) return { message: 'ok', cached: true };
    const spotReportView = await getSpotReportView(spotId);
    const {
      spot: { name, cameras },
      forecast: { wind, tide },
    } = spotReportView;
    // We want to get an array of all conditions types lower or equal to incoming rating
    // since when we query we want to find prefences that have a lower or equal rating than
    // current one. Since the condition entries mean "CONDITION or better" we want to grab any
    // which are lower than the current conditions. For example if my desired condition is
    // "FAIR OR BETTER" which is stored as "FAIR" in the DB, and the spot report is "GOOD".
    // I should be notified because the condition is better than "FAIR"
    const conditionLoc = rating ? conditionTypes.indexOf(rating) : -1;
    const conditionsMatch = conditionTypes.filter((_, index) => index <= conditionLoc);
    const spotNotifications = await SpotsNotificationModel.find({
      spot: spotId,
      minWaveHeight: { $lte: minHeight },
      maxWaveHeight: { $gte: maxHeight },
      condition: { $in: conditionsMatch },
      [`${isAm ? 'am' : 'pm'}`]: true,
    });
    const users = spotNotifications.map(preference => preference.user);
    const userOutlets = await OutletsModel.find({ user: { $in: users } });
    const outlets = filterDuplicateDeviceTokens(userOutlets).map(outlet => {
      const { endpointArn, channel, user, appName } = outlet;
      return {
        endpointArn,
        channel,
        appName,
        user,
      };
    });
    if (outlets.length === 0) return { message: 'no outlets', cached: false };
    const jsonBatch = {
      type: SPOT,
      spot: {
        spotId,
        spotName: name,
        clipUrl: cameras && cameras.length > 0 ? cameras[0].rewindClip : null,
        waveHeightMin: minHeight,
        waveHeightMax: maxHeight,
        waveHeightOccasional: occasionalHeight,
        condition: rating,
        windSpeed: wind.speed,
        windDirection: wind.direction,
        currentTide: tide.current,
      },
      outlets,
    };
    const dateTime = format(new Date(), 'YYYY-MM-DD-H-m');
    const maxPerBatch = config.MAX_OUTLETS_PER_BATCH;
    if (outlets.length > maxPerBatch) {
      const loopCount = Math.ceil(outlets.length / maxPerBatch);
      const chunkedOutlets = chunkArray(outlets, loopCount);
      await Promise.all(
        chunkedOutlets.map((outletArray, index) => {
          const jsonChunkedBatch = {
            ...jsonBatch,
            outlets: outletArray,
          };
          return uploadJSONtoS3Bucket(jsonChunkedBatch, `spot-${spotId}-${dateTime}-${index}.json`);
        }),
      );
    } else if (outlets.length > 0) {
      await uploadJSONtoS3Bucket(jsonBatch, `spot-${spotId}-${dateTime}-1.json`);
    }
    const cacheTimeout = 43200; // 12 hours
    setKey(redisKey, cacheTimeout);
    return { message: 'ok', cached: false };
  } catch (error) {
    newrelic.noticeError(error);
    throw error;
  }
};
const subscriptionConfirmationHandler = async reqBody => {
  const { SubscribeURL } = reqBody;
  await getSubscribe(SubscribeURL);
  return { ...reqBody };
};
/* eslint-disable import/prefer-default-export */
export const spotDetailsPostHandler = async (req, res) => {
  try {
    const { Message } = req.body;
    const type = req.headers['x-amz-sns-message-type'];
    let response;
    if (type === 'SubscriptionConfirmation') {
      response = await subscriptionConfirmationHandler(req.body);
    } else if (type === 'Notification') {
      response = await notificationHandler(Message);
    } else {
      throw new APIError('No matching handler to run for provided message type', { statusCode: 400 });
    }
    return res.send(response);
  } catch (error) {
    newrelic.noticeError(error);
    throw new APIError(error?.message || 'Something went wrong processing the message', {
      statusCode: error?.statusCode || 500,
      data: {
        requestBody: req?.body,
        ...error,
      },
    });
  }
};
