import chai, { expect } from 'chai';
import authenticateRequest from '@surfline/services-common/dist/middlewares/authenticateRequest';
import sinon from 'sinon';
import { json } from 'body-parser';
import express from 'express';
import { Types } from 'mongoose';
import notifications from '.';
import Spots, * as spotModel from '../../model/Preferences/Spots/schema';
import Sessions, * as sessionModel from '../../model/Preferences/Sessions/schema';
import * as spotsAPI from '../../external/spots';

describe('/sessions', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(authenticateRequest({ userIdRequired: true }), json(), notifications(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(Sessions, 'findOne');
    sinon.stub(Sessions, 'find');
    sinon.stub(sessionModel, 'createSessionNotificationPreference');
  });

  afterEach(() => {
    Sessions.findOne.restore();
    Sessions.find.restore();
    sessionModel.createSessionNotificationPreference.restore();
  });

  it('should fail with no auth', async () => {
    const res = await request.get('/sessions').send();

    expect(res).to.have.status(401);
  });

  it('should GET session by notification ID', async () => {
    const model = {
      _id: new Types.ObjectId(),
      active: true,
    };
    const userId = '582e0255d7e44b1074114ba9';
    const _id = `${model._id}`;
    Sessions.findOne.resolves({
      ...model,
      toJSON: sinon.stub().returns(model),
    });
    const res = await request
      .get(`/sessions`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      _id,
      active: true,
    });
    expect(Sessions.findOne).to.have.been.calledOnce();
    expect(Sessions.findOne).to.have.been.calledWithExactly({ user: userId });
  });

  it('should POST', async () => {
    const model = {
      active: true,
    };
    const user = `${new Types.ObjectId()}`;
    const resultModel = {
      message: 'Session notification preference created',
    };
    sessionModel.createSessionNotificationPreference.resolves(resultModel);
    const res = await request
      .post('/sessions')
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', user)
      .send(model);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(resultModel);
    expect(sessionModel.createSessionNotificationPreference).to.have.been.calledOnce();
  });

  it('should PUT by notificationId', async () => {
    const model = {
      _id: new Types.ObjectId(),
      active: true,
    };
    const existingModel = {
      _id: new Types.ObjectId(),
      save: sinon.stub().resolves(),
    };
    const resultModel = {
      _id: existingModel._id,
      message: 'Session notification preference saved',
    };
    const _id = `${existingModel._id}`;
    const user = `${new Types.ObjectId()}`;
    Sessions.findOne.resolves(existingModel);
    const res = await request
      .put(`/sessions/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', user)
      .send(model);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      ...resultModel,
      _id,
    });
    expect(Sessions.findOne).to.have.been.calledOnce();
    expect(Sessions.findOne.firstCall.args).to.deep.equal([{ _id, user }]);
    expect(existingModel.save).to.have.been.calledOnce();
  });
});

describe('/spots', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(authenticateRequest({ userIdRequired: true }), json(), notifications(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(Spots, 'findOne');
    sinon.stub(Spots, 'find');
    sinon.stub(spotModel, 'createSpotNotification');
    sinon.stub(spotsAPI, 'getSpot');
  });

  afterEach(() => {
    Spots.findOne.restore();
    Spots.find.restore();
    spotModel.createSpotNotification.restore();
    spotsAPI.getSpot.restore();
  });

  it('should fail with no auth', async () => {
    request
      .get('/spots')
      .send()
      .end((err, res) => {
        expect(res).to.have.status(401);
      });
  });

  it('should GET spot by notificationId', async () => {
    const model = {
      _id: new Types.ObjectId(),
      spot: '609e191e810c19729de660ea',
      am: true,
      pm: false,
      minWaveHeight: 1,
      maxWaveHeight: 4,
      condition: 'GOOD',
      active: true,
    };
    const _id = `${model._id}`;
    Spots.findOne.resolves({
      ...model,
      toJSON: sinon.stub().returns(model),
    });
    const spot = { name: 'Pipeline' };
    spotsAPI.getSpot.resolves(spot);
    const res = await request
      .get(`/spots/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      _id,
      spot: '609e191e810c19729de660ea',
      spotName: spot.name,
      am: true,
      pm: false,
      minWaveHeight: 1,
      maxWaveHeight: 4,
      condition: 'GOOD',
      active: true,
    });
    expect(Spots.findOne).to.have.been.calledOnce();
    expect(Spots.findOne).to.have.been.calledWithExactly({ _id });
  });

  it('should POST to spot', async () => {
    const model = {
      spot: '609e191e810c19729de660ea',
      am: true,
      pm: false,
      minWaveHeight: 1,
      maxWaveHeight: 4,
      condition: 'GOOD',
      kind: 'SPOT',
      active: true,
    };

    const user = `${new Types.ObjectId()}`;

    const resultModel = {
      _id: '1',
      message: 'Spot notification preferences saved',
    };

    Spots.findOne.resolves();
    spotModel.createSpotNotification.resolves(resultModel);

    const res = await request
      .post('/spots')
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', user)
      .send(model);

    expect(res).to.be.json();
    expect(res.body).to.deep.equal(resultModel);
    expect(Spots.findOne).to.have.been.calledOnceWithExactly({ spot: model.spot, user });
    expect(spotModel.createSpotNotification).to.have.been.calledOnce();
  });

  it('should PUT by notificationId', async () => {
    const model = {
      _id: new Types.ObjectId(),
      spot: '609e191e810c19729de660ea',
      am: true,
      pm: false,
      minWaveHeight: 1,
      maxWaveHeight: 4,
      condition: 'GOOD',
      active: true,
    };
    const existingModel = {
      _id: new Types.ObjectId(),
      spot: '609e191e810c19729de660ea',
      save: sinon.stub().resolves(),
    };
    const resultModel = {
      _id: existingModel._id,
      message: 'Spot notification preferences saved',
    };
    const _id = `${existingModel._id}`;
    const user = `${new Types.ObjectId()}`;
    Spots.findOne.resolves(existingModel);
    const res = await request
      .put(`/spots/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', user)
      .send(model);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      ...resultModel,
      _id,
    });
    expect(Spots.findOne).to.have.been.calledOnce();
    expect(Spots.findOne.firstCall.args).to.deep.equal([{ _id, user }]);
    expect(existingModel.save).to.have.been.calledOnce();
  });

  it('should PATCH by notificationId', async () => {
    const model = {
      _id: new Types.ObjectId(),
      spot: '609e191e810c19729de660ea',
      am: true,
      pm: false,
      minWaveHeight: 1,
      maxWaveHeight: 4,
      condition: 'GOOD',
      active: true,
    };
    const existingModel = {
      _id: new Types.ObjectId(),
      spot: '609e191e810c19729de660ea',
      save: sinon.stub().resolves(),
    };
    const resultModel = {
      _id: existingModel._id,
      message: 'Spot notification preferences saved',
    };
    const _id = `${existingModel._id}`;
    const user = `${new Types.ObjectId()}`;
    Spots.findOne.resolves(existingModel);
    const res = await request
      .patch(`/spots/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', user)
      .send(model);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      ...resultModel,
      _id,
    });

    expect(Spots.findOne).to.have.been.calledOnce();
    expect(Spots.findOne.firstCall.args).to.deep.equal([{ _id, user }]);
    expect(existingModel.save).to.have.been.calledOnce();
  });
});
