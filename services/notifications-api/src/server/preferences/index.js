import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { sessions, spots } from './handlers';

export const trackRequests = log => (req, _, next) => {
  log.trace({
    action: '/preferences',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const preferences = log => {
  const api = Router();
  api.use('*', trackRequests(log));

  // SESSIONS
  api.get('/sessions', wrapErrors(sessions.getHandler));
  api.post('/sessions', wrapErrors(sessions.postHandler));
  api.put(
    '/sessions/:notificationId',
    sessions.checkNotificationId,
    wrapErrors(sessions.putHandler),
  );
  api.delete(
    '/sessions/:notificationId',
    sessions.checkNotificationId,
    wrapErrors(sessions.deleteHandler),
  );

  // SPOTS
  api.get('/spots', wrapErrors(spots.getAllHandler));
  api.get('/spots/:notificationId', spots.checkNotificationId, wrapErrors(spots.getHandler));
  api.post('/spots', wrapErrors(spots.postHandler));
  api.put('/spots/:notificationId', spots.checkNotificationId, wrapErrors(spots.putHandler));
  api.patch('/spots/:notificationId', spots.checkNotificationId, wrapErrors(spots.putHandler));
  api.delete('/spots/:notificationId', spots.checkNotificationId, wrapErrors(spots.deleteHandler));

  return api;
};

export default preferences;
