import { Types } from 'mongoose';
import { APIError } from '@surfline/services-common';
import SpotsNotificationModel, {
  createSpotNotification,
} from '../../../model/Preferences/Spots/schema';
import { getSpot } from '../../../external/spots';

const NOT_FOUND = { message: 'Spot notification preference not found' };

export const checkNotificationId = (req, res, next) => {
  const { notificationId } = req.params;
  if (!Types.ObjectId.isValid(notificationId)) return res.status(404).send(NOT_FOUND);
  return next();
};

export const getAllHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot find spot with empty userId');
  const spotNotifications = await SpotsNotificationModel.find({ user: userId });
  let spotNotificationResponse;

  try {
    spotNotificationResponse = await Promise.all(
      spotNotifications.map(async spotNotification => {
        const {
          _id,
          spot,
          am,
          pm,
          minWaveHeight,
          maxWaveHeight,
          condition,
          active,
        } = spotNotification;
        const spotResponse = await getSpot(spot);
        const spotName = spotResponse.name;
        return {
          _id,
          spot,
          spotName,
          am,
          pm,
          minWaveHeight,
          maxWaveHeight,
          condition,
          active,
        };
      }),
    );
  } catch (err) {
    throw new APIError(err.message);
  }

  return res.send(spotNotificationResponse);
};

export const getHandler = async (req, res) => {
  const {
    params: { notificationId },
  } = req;
  if (!notificationId)
    throw new APIError('Cannot find spot notification preference with empty notificationId');
  const spotNotification = await SpotsNotificationModel.findOne({ _id: notificationId });
  const { _id, spot, am, pm, minWaveHeight, maxWaveHeight, condition, active } = spotNotification;
  let spotName;

  try {
    const spotResponse = await getSpot(spot);
    spotName = spotResponse.name;
  } catch (err) {
    throw new APIError(err.message);
  }

  const spotNotificationResponse = {
    _id,
    spot,
    spotName,
    am,
    pm,
    minWaveHeight,
    maxWaveHeight,
    condition,
    active,
  };

  return res.send(spotNotificationResponse);
};

export const postHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  const duplicateEntryCheck = await SpotsNotificationModel.findOne({
    spot: req.body.spot,
    user: userId,
  });
  if (duplicateEntryCheck)
    throw new APIError('Cannot create duplicate spot notification preference', { statusCode: 400 });
  let spotNotification;

  try {
    const { minWaveHeight, maxWaveHeight } = req.body;
    spotNotification = await createSpotNotification({
      ...req.body,
      minWaveHeight: Math.floor(minWaveHeight),
      maxWaveHeight: Math.floor(maxWaveHeight),
      user: userId,
    });
  } catch (err) {
    throw new APIError(err.message);
  }

  const { _id } = spotNotification;
  const spotNotificationResponse = {
    _id,
    message: 'Spot notification preferences saved',
  };

  return res.send(spotNotificationResponse);
};

export const putHandler = async (req, res) => {
  const {
    params: { notificationId },
  } = req;
  if (!notificationId)
    throw new APIError('Cannot update spot notification with empty notificationId');
  const { am, pm, minWaveHeight, maxWaveHeight, condition, active } = req.body;
  const userId = req.authenticatedUserId;
  const spotNotification = await SpotsNotificationModel.findOne({
    _id: notificationId,
    user: userId,
  });
  if (!spotNotification) return res.status(404).send(NOT_FOUND);
  spotNotification.am = am;
  spotNotification.pm = pm;
  spotNotification.minWaveHeight = Math.floor(minWaveHeight);
  spotNotification.maxWaveHeight = Math.floor(maxWaveHeight);
  spotNotification.condition = condition;
  spotNotification.active = active;
  try {
    await spotNotification.save();
  } catch (err) {
    throw new APIError(err.message);
  }

  const { _id } = spotNotification;
  const spotNotificationResponse = {
    _id,
    message: 'Spot notification preferences saved',
  };

  return res.send(spotNotificationResponse);
};

export const patchHandler = async (req, res) => {
  const {
    params: { notificationId },
  } = req;
  if (!notificationId)
    throw new APIError('Cannot update spot notification with empty notificationId');
  const { am, pm, minWaveHeight, maxWaveHeight, condition, active } = req.body;
  const userId = req.authenticatedUserId;
  const spotNotification = await SpotsNotificationModel.findOne({
    _id: notificationId,
    user: userId,
  });
  if (!spotNotification) return res.status(404).send(NOT_FOUND);
  spotNotification.am = am || spotNotification.am;
  spotNotification.pm = pm || spotNotification.pm;
  spotNotification.minWaveHeight = minWaveHeight
    ? Math.floor(minWaveHeight)
    : spotNotification.minWaveHeight;
  spotNotification.maxWaveHeight = maxWaveHeight
    ? Math.floor(maxWaveHeight)
    : spotNotification.maxWaveHeight;
  spotNotification.condition = condition || spotNotification.condition;
  spotNotification.active = active || spotNotification.active;
  try {
    await spotNotification.save();
  } catch (err) {
    throw new APIError(err.message);
  }

  const { _id } = spotNotification;
  const spotNotificationResponse = {
    _id,
    message: 'Spot notification preferences saved',
  };

  return res.send(spotNotificationResponse);
};

export const deleteHandler = async (req, res) => {
  const {
    params: { notificationId },
  } = req;
  if (!notificationId)
    throw new APIError('Cannot delete notification preferences with empty notificationId');
  const userId = req.authenticatedUserId;
  await SpotsNotificationModel.deleteOne({
    user: userId,
    _id: notificationId,
  });

  const spotNotificationResponse = {
    message: 'Spot notification preferences deleted successfully',
  };

  return res.send(spotNotificationResponse);
};
