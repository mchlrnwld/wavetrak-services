import { Types } from 'mongoose';
import { APIError, NotFound } from '@surfline/services-common';
import SessionNotificationModel, {
  createSessionNotificationPreference,
} from '../../../model/Preferences/Sessions/schema';

const NOT_FOUND = { message: 'Session notification preference not found' };

export const checkNotificationId = (req, res, next) => {
  const { notificationId } = req.params;
  if (!Types.ObjectId.isValid(notificationId)) {
    return res.status(404).send(NOT_FOUND);
  }
  return next();
};

export const getHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot get a notification preference without a user ID');

  const sessionNotification = await SessionNotificationModel.findOne({ user: userId });

  if (!sessionNotification) {
    throw new NotFound(`No session notification preference found for user: ${userId}`)
  }
  const { _id, active } = sessionNotification;
  res.send({ _id, active });
};

export const postHandler = async (req, res) => {
  const { active } = req.body;
  const userId = req.authenticatedUserId;
  const duplicateEntryCheck = await SessionNotificationModel.findOne({
    user: userId,
  });
  if (duplicateEntryCheck)
    throw new APIError('Cannot create duplicate session notification preference', { statusCode: 400 });
  let sessionNotification;
  try {
    sessionNotification = await createSessionNotificationPreference({
      active,
      user: userId,
    });
  } catch (err) {
    throw new APIError(err.message);
  }
  const { _id } = sessionNotification;
  return res.send({
    _id,
    message: 'Session notification preference created',
  });
};

export const putHandler = async (req, res) => {
  const {
    params: { notificationId },
  } = req;
  if (!notificationId)
    throw new APIError(
      'Cannot update a session notification preference with an empty notification ID',
    );
  const { active } = req.body;
  const userId = req.authenticatedUserId;
  const sessionNotification = await SessionNotificationModel.findOne({
    _id: notificationId,
    user: userId,
  });
  if (!sessionNotification) return res.status(404).send(NOT_FOUND);
  sessionNotification.active = active;
  try {
    await sessionNotification.save();
  } catch (err) {
    throw new APIError(err.message);
  }
  const { _id } = sessionNotification;
  return res.send({
    _id,
    message: 'Session notification preference saved',
  });
};

export const deleteHandler = async (req, res) => {
  const {
    params: { notificationId },
  } = req;
  if (!notificationId)
    throw new APIError('Cannot delete notification preference with an empty notificationId');
  const userId = req.authenticatedUserId;
  await SessionNotificationModel.deleteOne({
    user: userId,
    _id: notificationId,
  });
  return res.send({
    message: 'Session notification preference deleted successfully',
  });
};
