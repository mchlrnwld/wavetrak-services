import chai, { expect } from 'chai';
import authenticateRequest from '@surfline/services-common/dist/middlewares/authenticateRequest';
import sinon from 'sinon';
import { json } from 'body-parser';
import express from 'express';
import { Types } from 'mongoose';
import notifications from '.';
import Outlets, * as outletModel from '../../model/Outlets/schema';
import * as aws from '../../common/aws';

describe('/outlets', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(authenticateRequest({ userIdRequired: true }), json(), notifications(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(Outlets, 'findOne');
    sinon.stub(Outlets, 'findOneAndUpdate');
    sinon.stub(outletModel, 'createOutlet');
    sinon.stub(aws, 'createPlatformEndpoint');
    sinon.stub(aws, 'updatePlatformEndpoint');
  });

  afterEach(() => {
    Outlets.findOne.restore();
    Outlets.findOneAndUpdate.restore();
    outletModel.createOutlet.restore();
    aws.createPlatformEndpoint.restore();
    aws.updatePlatformEndpoint.restore();
  });

  it('should fail with no auth', async () => {
    request
      .get('/outlets')
      .send()
      .end((_, res) => {
        expect(res).to.have.status(401);
      });
  });

  it('should GET outlet by outletId', async () => {
    const model = {
      _id: Types.ObjectId().toString(),
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
    };
    const _id = `${model._id}`;
    Outlets.findOne.returns({
      ...model,
      select: sinon.stub().resolves(model),
      toJSON: sinon.stub().returns(model),
    });
    const res = await request
      .get(`/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      _id,
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
    });
    expect(Outlets.findOne).to.have.been.calledOnce();
    expect(Outlets.findOne).to.have.been.calledWithExactly({ _id: { $eq: _id } });
  });

  it('should POST to outlet', async () => {
    const outletId = Types.ObjectId().toString();
    const model = {
      _id: outletId,
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
      user: '582e0255d7e44b1074114ba9',
    };

    const resultModel = {
      _id: outletId,
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
      endpointArn: 'some-endpoint-arn',
      user: '582e0255d7e44b1074114ba9',
    };

    outletModel.createOutlet.resolves(resultModel);

    aws.createPlatformEndpoint.resolves({
      EndpointArn: 'some-endpoint-arn',
    });

    const res = await request
      .post('/')
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send(model);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      message: 'Notification outlet information saved successfully',
      _id: outletId,
    });
    expect(outletModel.createOutlet).to.have.been.calledOnce();
    expect(aws.createPlatformEndpoint).to.have.been.calledOnce();
    expect(outletModel.createOutlet.firstCall.args).to.deep.equal([resultModel]);
  });

  it('should PUT by outletId and create a platform endpoint if one does not exist', async () => {
    const model = {
      _id: Types.ObjectId().toString(),
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
      user: '582e0255d7e44b1074114ba9',
    };
    const existingModel = {
      _id: Types.ObjectId().toString(),
      save: sinon.stub().resolves(),
    };
    const expectedResponse = {
      _id: existingModel._id,
      message: 'Notification outlet information saved successfully',
    };
    const _id = `${existingModel._id}`;
    Outlets.findOne.withArgs({ _id }).resolves(existingModel);
    Outlets.findOne.resolves();

    aws.createPlatformEndpoint.resolves({
      EndpointArn: 'some-endpoint-arn',
    });

    const res = await request
      .put(`/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send(model);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(expectedResponse);
    expect(existingModel).to.deep.equal({
      ...existingModel,
      endpointArn: 'some-endpoint-arn',
    });
    expect(Outlets.findOne).to.have.been.calledTwice();
    expect(Outlets.findOne.firstCall.args).to.deep.equal([{ _id }]);
    expect(aws.createPlatformEndpoint).to.have.been.calledOnceWithExactly('SOMETOKEN');
    expect(existingModel.save).to.have.been.calledOnce();
  });

  it('should PUT by outletId and update the platform endpoint if the device token changed', async () => {
    const putBody = {
      _id: Types.ObjectId().toString(),
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN2',
      channel: 'APNS',
      user: '582e0255d7e44b1074114ba9',
      endpointArn: 'some-endpoint-arn',
    };
    const existingModel = {
      _id: Types.ObjectId().toString(),
      deviceToken: 'SOMETOKEN1',
      save: sinon.stub().resolves(),
      endpointArn: 'some-endpoint-arn',
    };
    const expectedResult = {
      _id: existingModel._id,
      message: 'Notification outlet information saved successfully',
    };

    const _id = `${existingModel._id}`;
    Outlets.findOne.withArgs({ _id }).resolves(existingModel);
    Outlets.findOne.resolves();

    aws.updatePlatformEndpoint.resolves();

    const res = await request
      .put(`/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send(putBody);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      ...expectedResult,
      _id,
    });
    expect(existingModel).to.deep.equal({
      ...existingModel,
      deviceToken: 'SOMETOKEN2',
    });
    expect(Outlets.findOne).to.have.been.calledTwice();
    expect(Outlets.findOne.firstCall.args).to.deep.equal([{ _id }]);
    expect(aws.updatePlatformEndpoint).to.have.been.calledOnceWithExactly(
      'some-endpoint-arn',
      'SOMETOKEN2',
    );
    expect(existingModel.save).to.have.been.calledOnce();
  });

  it('should PATCH by outletId and update the platform endpoint if the device token changed', async () => {
    const patchBody = {
      _id: Types.ObjectId().toString(),
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN1',
      channel: 'APNS',
    };
    const existingModel = {
      _id: Types.ObjectId().toString(),
      deviceToken: 'SOMETOKEN',
      endpointArn: 'some-endpoint-arn',
      save: sinon.stub().resolves(),
    };
    const expectedResult = {
      _id: existingModel._id,
      message: 'Notification outlet information saved successfully',
    };
    const _id = `${existingModel._id}`;
    Outlets.findOne.withArgs({ _id }).resolves(existingModel);
    Outlets.findOne.resolves();

    aws.updatePlatformEndpoint.resolves();

    const res = await request
      .patch(`/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send(patchBody);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(expectedResult);
    expect(existingModel).to.deep.equal({
      ...existingModel,
      deviceToken: 'SOMETOKEN1',
    });
    expect(Outlets.findOne).to.have.been.calledTwice();
    expect(Outlets.findOne.firstCall.args).to.deep.equal([{ _id }]);
    expect(aws.updatePlatformEndpoint).to.have.been.calledOnceWithExactly(
      'some-endpoint-arn',
      'SOMETOKEN1',
    );
    expect(existingModel.save).to.have.been.calledOnce();
  });

  it('should PATCH by outletId and create a platform endpoint if there is no endpointArn on the document', async () => {
    const patchBody = {
      _id: Types.ObjectId().toString(),
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
    };
    const existingModel = {
      _id: Types.ObjectId().toString(),
      save: sinon.stub().resolves(),
    };
    const expectedResult = {
      _id: existingModel._id,
      message: 'Notification outlet information saved successfully',
    };
    const _id = `${existingModel._id}`;
    Outlets.findOne.resolves(existingModel);
    Outlets.findOne.withArgs({ _id }).resolves(existingModel);
    Outlets.findOne.resolves();

    aws.createPlatformEndpoint.resolves({
      EndpointArn: 'some-endpoint-arn',
    });

    const res = await request
      .patch(`/${_id}`)
      .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
      .set('Content-Type', 'application/json')
      .set('x-auth-userid', '582e0255d7e44b1074114ba9')
      .send(patchBody);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(expectedResult);
    expect(existingModel).to.deep.equal({
      ...existingModel,
      endpointArn: 'some-endpoint-arn',
    });
    expect(Outlets.findOne).to.have.been.calledTwice();
    expect(Outlets.findOne.firstCall.args).to.deep.equal([{ _id }]);
    expect(aws.createPlatformEndpoint).to.have.been.calledOnceWithExactly('SOMETOKEN');
    expect(existingModel.save).to.have.been.calledOnce();
  });
});
