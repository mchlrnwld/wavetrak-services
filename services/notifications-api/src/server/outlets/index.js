import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as outlet from './handlers';

export const trackRequests = log => (req, _, next) => {
  log.trace({
    action: '/notifications/outlets',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const outlets = log => {
  const api = Router();
  api.use('*', trackRequests(log));

  api.get('/', wrapErrors(outlet.getAllHandler));
  api.get('/:outletId', outlet.checkOutletId, wrapErrors(outlet.getHandler));
  api.post('/', wrapErrors(outlet.postHandler));
  api.put('/:outletId', outlet.checkOutletId, wrapErrors(outlet.putHandler));
  api.patch('/:outletId', outlet.checkOutletId, wrapErrors(outlet.patchHandler));
  api.delete('/:outletId', outlet.checkOutletId, wrapErrors(outlet.deleteHandler));

  return api;
};

export default outlets;
