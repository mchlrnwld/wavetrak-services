import newrelic from 'newrelic';
import { Types } from 'mongoose';
import { APIError } from '@surfline/services-common';
import Outlets, { createOutlet } from '../../../model/Outlets/schema';
import * as aws from '../../../common/aws';

const NOT_FOUND = { message: 'Outlet not found' };

export const checkOutletId = (req, res, next) => {
  const { outletId } = req.params;
  if (!Types.ObjectId.isValid(outletId)) return res.status(404).send(NOT_FOUND);
  return next();
};

export const getAllHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot get outlets with empty userId');

  const outlets = await Outlets.find({ user: userId }).select({
    _id: 1,
    deviceToken: 1,
    channel: 1,
    appName: 1,
    appVersion: 1,
    deviceOS: 1,
  });
  if (!outlets) return res.status(404).send(NOT_FOUND);
  return res.send(outlets);
};

export const getHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot get outlet with empty userId');

  const { outletId } = req.params;
  if (!outletId) throw new APIError('Outlet Id is Required.');

  const outlet = await Outlets.findOne({ _id: { $eq: outletId } }).select({
    _id: 1,
    deviceToken: 1,
    channel: 1,
    appName: 1,
    appVersion: 1,
    deviceOS: 1,
  });

  if (!outlet) return res.status(404).send(NOT_FOUND);

  const { _id, deviceToken, channel, appName, appVersion, deviceOS } = outlet;
  return res.send({
    _id,
    deviceToken,
    channel,
    appName,
    appVersion,
    deviceOS,
  });
};

export const postHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot post outlet with empty userId');

  const { deviceToken } = req.body;
  if (!deviceToken) {
    throw new APIError('Device Token is Required.', { statusCode: 400 });
  }

  async function checkForDuplicate() {
    const duplicateEntryCheck = await Outlets.findOne({
      deviceToken: { $eq: deviceToken },
      user: { $eq: userId },
    });

    if (duplicateEntryCheck) {
      throw new APIError('Cannot create duplicate device token', {
        statusCode: 400,
        data: { existingOutletId: duplicateEntryCheck._id },
      });
    }
  }

  await checkForDuplicate();

  try {
    const createEndpointArn = await aws.createPlatformEndpoint(deviceToken);
    const endpointArn = createEndpointArn.EndpointArn;

    const outlet = await createOutlet({
      ...req.body,
      endpointArn,
      user: userId,
    });
    return res.send({
      message: 'Notification outlet information saved successfully',
      _id: outlet._id,
    });
  } catch (error) {
    console.log(error);
    newrelic.noticeError(error)
    // check for - MongoError: E11000 duplicate key error collection
    if(error?.code === 11000) {
      // duplicate error occured on creation attempt
      // attempt to check for duplicate outlet ID a second time
      await checkForDuplicate();
    }
    throw new APIError('Notification outlet information not saved');
  }
};

export const putHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot put outlet with empty userId');

  const { outletId } = req.params;
  if (!outletId) throw new APIError('Outlet Id is Required.');

  const { appName, deviceToken, channel, appVersion, deviceOS } = req.body;
  if (!deviceToken) throw new APIError('Device Token is Required.');

  const outlet = await Outlets.findOne({ _id: outletId });
  if (!outlet) return res.status(404).send(NOT_FOUND);

  const duplicateEntryCheck = await Outlets.findOne({
    deviceToken: { $eq: deviceToken },
  });
  if (duplicateEntryCheck) throw new APIError('Cannot create duplicate device token', { statusCode: 400 });

  try {
    if (!outlet.endpointArn) {
      const createEndpointArn = await aws.createPlatformEndpoint(deviceToken);
      const endpointArn = createEndpointArn.EndpointArn;
      outlet.endpointArn = endpointArn;
    }

    if (outlet.deviceToken && outlet.deviceToken !== deviceToken) {
      await aws.updatePlatformEndpoint(outlet.endpointArn, deviceToken);
    }

    outlet.appName = appName;
    outlet.deviceToken = deviceToken;
    outlet.channel = channel;
    outlet.appVersion = appVersion;
    outlet.deviceOS = deviceOS;
    outlet.user = userId;

    await outlet.save();

    return res.send({
      message: 'Notification outlet information saved successfully',
      _id: outlet._id,
    });
  } catch (error) {
    console.log(error);
    throw new APIError('Notification outlet information not saved');
  }
};

export const patchHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot patch outlet with empty userId');

  const { outletId } = req.params;
  if (!outletId) throw new APIError('Outlet Id is Required.');

  const { appName, deviceToken, channel, appVersion, deviceOS } = req.body;

  const outlet = await Outlets.findOne({ _id: outletId });
  if (!outlet) return res.status(404).send(NOT_FOUND);

  // ensure that the given deviceToken does not already exist for the user
  if (deviceToken) {
    const duplicateEntryCheck = await Outlets.findOne({
      deviceToken: { $eq: deviceToken },
    });
    if (duplicateEntryCheck) throw new APIError('Cannot create duplicate device token', { statusCode: 400 });
  }

  try {
    if (!outlet.endpointArn && deviceToken) {
      const createEndpointArn = await aws.createPlatformEndpoint(deviceToken);
      const endpointArn = createEndpointArn.EndpointArn;
      outlet.endpointArn = endpointArn;
    }

    if (deviceToken && outlet.deviceToken !== deviceToken) {
      await aws.updatePlatformEndpoint(outlet.endpointArn, deviceToken);
    }

    outlet.appName = appName || outlet.appName;
    outlet.deviceToken = deviceToken || outlet.deviceToken;
    outlet.channel = channel || outlet.channel;
    outlet.appVersion = appVersion || outlet.appVersion;
    outlet.deviceOS = deviceOS || outlet.deviceOS;

    await outlet.save();
    return res.send({
      message: 'Notification outlet information saved successfully',
      _id: outlet._id,
    });
  } catch (error) {
    console.log(error);
    throw new APIError('Notification outlet information not saved');
  }
};

export const deleteHandler = async (req, res) => {
  const userId = req.authenticatedUserId;
  if (!userId) throw new APIError('Cannot delete outlet with empty userId');

  const { outletId } = req.params;
  if (!outletId) throw new APIError('Outlet Id is Required.');

  const outlet = await Outlets.findOne({ _id: { $eq: outletId } });
  if (!outlet) return res.status(404).send(NOT_FOUND);

  try {
    const { endpointArn } = outlet;
    await aws.deletePlatformEndpoint(endpointArn);

    await Outlets.deleteOne({ _id: outletId, user: userId });

    return res.send({
      message: 'Outlet deleted successfully',
    });
  } catch (error) {
    console.log(error);
    throw new APIError('Outlet not deleted');
  }
};
