import { setupExpress } from '@surfline/services-common';
import authenticateRequest from '@surfline/services-common/dist/middlewares/authenticateRequest';
import preferences from './preferences';
import outlets from './outlets';
import webhooks from './webhooks';
import logger from '../common/logger';
import config from '../config';

const log = logger('notifications-api');

const startApp = () =>
  setupExpress({
    port: config.EXPRESS_PORT || 8081,
    name: 'notifications-api',
    log,
    handlers: [
      ['/preferences', authenticateRequest({ userIdRequired: true }), preferences(log)],
      ['/outlets', authenticateRequest({ userIdRequired: true }), outlets(log)],
      ['/webhooks', webhooks(log)],
    ],
  });

export default startApp;
