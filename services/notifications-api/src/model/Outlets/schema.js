import mongoose from 'mongoose';

const appNames = ['SURFLINE', 'BUOYWEATHER', 'FISHTRACK', 'MAGICSEAWEED'];

const outletSchema = new mongoose.Schema(
  {
    appName: {
      type: String,
      enum: appNames,
      required: [true, 'App name is required'],
    },
    deviceToken: { type: String, required: [true, 'Device token is required'] },
    channel: { type: String, required: [true, 'Channel is required'] },
    endpointArn: { type: String },
    appVersion: { type: String },
    deviceOS: { type: String },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'User ID is required'],
    },
  },
  {
    timestamps: true,
    collection: 'Outlets',
  },
);

outletSchema.index({ user: 1, appName: 1 });
outletSchema.index({ deviceToken: 1 });
outletSchema.index({ user: 1, deviceToken: 1 }, { unique: true });

const OutletsModel = mongoose.model('Outlets', outletSchema);

export const createOutlet = async model => {
  const outlet = new OutletsModel(model);
  await outlet.save();
  return outlet;
};

export const getSurflineOutlet = user => OutletsModel.findOne({ user, appName: 'SURFLINE' }).lean();
export const getSurflineOutlets = user => OutletsModel.find({ user, appName: 'SURFLINE' }).lean();

export default OutletsModel;
