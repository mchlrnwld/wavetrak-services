import { expect } from 'chai';
import Outlets from './schema';

describe('Outlets Model', () => {
  it('should require appName', (done) => {
    const outlet = new Outlets({
      kind: 'SPOT',
      deviceToken: 'SOMETOKEN',
      channel: 'APNS',
    });
    outlet.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.appName).to.be.ok();
      expect(err.errors.appName.message).to.equal('App name is required');
      return done();
    });
  });

  it('should require deviceToken', (done) => {
    const outlet = new Outlets({
      kind: 'SPOT',
      appName: 'SURFLINE',
      channel: 'APNS',
    });
    outlet.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.deviceToken).to.be.ok();
      expect(err.errors.deviceToken.message).to.equal('Device token is required');
      return done();
    });
  });

  it('should require appName', (done) => {
    const outlet = new Outlets({
      kind: 'SPOT',
      appName: 'SURFLINE',
      deviceToken: 'SOMETOKEN',
    });
    outlet.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.channel).to.be.ok();
      expect(err.errors.channel.message).to.equal('Channel is required');
      return done();
    });
  });
});
