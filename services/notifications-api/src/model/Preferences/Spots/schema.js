import mongoose from 'mongoose';
import NotificationPreferences from '../schema';

const conditionTypes = [
  'FLAT',
  'VERY_POOR',
  'POOR',
  'POOR_TO_FAIR',
  'FAIR',
  'FAIR_TO_GOOD',
  'GOOD',
  'VERY_GOOD',
  'GOOD_TO_EPIC',
  'EPIC',
];

const options = { discriminatorKey: 'kind', timestamps: true };

const notificationSpotSchema = new mongoose.Schema(
  {
    spot: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'Spot ID is required'],
    },
    am: { type: Boolean, default: true },
    pm: { type: Boolean, default: true },
    maxWaveHeight: { type: Number, default: null },
    minWaveHeight: { type: Number, default: null },
    condition: {
      type: String,
      enum: conditionTypes,
    },
    active: { type: Boolean, default: true },
  },
  options,
);

notificationSpotSchema.index({ user: 1, spot: 1 }, { unique: true, sparse: true });
notificationSpotSchema.index({
  spot: 1,
  condition: 1,
  kind: 1,
  pm: 1,
  maxWaveHeight: 1,
  minWaveHeight: 1,
});
notificationSpotSchema.index({
  spot: 1,
  condition: 1,
  kind: 1,
  am: 1,
  maxWaveHeight: 1,
  minWaveHeight: 1,
});

const SpotsNotificationModel = NotificationPreferences.discriminator(
  'SPOT',
  notificationSpotSchema,
);

export const createSpotNotification = async (model) => {
  const spotNotification = new SpotsNotificationModel(model);
  await spotNotification.save();
  return spotNotification;
};

export default SpotsNotificationModel;
