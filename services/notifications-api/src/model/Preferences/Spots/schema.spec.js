import { expect } from 'chai';
import SpotModel from './schema';

describe('SpotModel schema', () => {
  it('should be invalid if spot is empty', (done) => {
    const spotNotification = new SpotModel();
    spotNotification.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.spot).to.be.ok();
      expect(err.errors.spot.message).to.equal('Spot ID is required');
      return done();
    });
  });

  it('should should have kind of "SPOT"', () => {
    const newSpotNotification = new SpotModel();
    expect(newSpotNotification.kind).to.equal('SPOT');
  });
});
