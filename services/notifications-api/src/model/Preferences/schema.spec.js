import { expect } from 'chai';
import Notification from './schema';

describe('Notification schema', () => {
  it('should be invalid if user is empty', done => {
    const notification = new Notification();
    notification.save(err => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.user).to.be.ok();
      expect(err.errors.user.message).to.equal('User is required');
      return done();
    });
  });

  it('should be invalid if user ID is invalid', done => {
    const notification = new Notification({ user: '1234' });
    notification.save(err => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.user).to.be.ok();
      expect(err.errors.user.message).to.equal(
        'Cast to ObjectId failed for value "1234" at path "user"',
      );
      return done();
    });
  });
});
