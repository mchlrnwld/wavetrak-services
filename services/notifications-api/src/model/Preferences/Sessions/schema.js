import mongoose from 'mongoose';
import NotificationPreferences from '../schema';

const options = { discriminatorKey: 'kind', timestamps: true };

const notificationSessionSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
  },
  options,
);

notificationSessionSchema.index({ user: 1, active: 1 }, { unique: true });

const SessionNotificationModel = NotificationPreferences.discriminator(
  'SESSION',
  notificationSessionSchema,
);

export const createSessionNotificationPreference = async model => {
  const sessionNotification = new SessionNotificationModel(model);
  await sessionNotification.save();
  return sessionNotification;
};

/**
 * @description Given a userId, finds the related sessions notification preference
 * @param {*} user
 */
export const getSessionNotificationPreference = user =>
  SessionNotificationModel.findOne({ user }).lean();

export default SessionNotificationModel;
