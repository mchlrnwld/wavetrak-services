import { expect } from 'chai';
import SessionModel from './schema';

describe('SessionsModel schema', () => {
  it('should be invalid if user is empty', done => {
    const sessionNotification = new SessionModel();
    sessionNotification.save(err => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.user).to.be.ok();
      expect(err.errors.user.message).to.equal('User is required');
      return done();
    });
  });

  it('should should have kind of "SESSION"', () => {
    const newSessionNotification = new SessionModel();
    expect(newSessionNotification.kind).to.equal('SESSION');
    expect(newSessionNotification.active).to.equal(true);
  });

  it('should should set active to false', () => {
    const newSessionNotification = new SessionModel({ active: false });
    expect(newSessionNotification.kind).to.equal('SESSION');
    expect(newSessionNotification.active).to.equal(false);
  });
});
