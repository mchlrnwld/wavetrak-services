import mongoose from 'mongoose';

const options = {
  collection: 'NotificationPreferences',
  timestamps: true,
  discriminatorKey: 'kind',
};

const notificationPreferenceSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserInfo',
    required: [true, 'User is required'],
  },
}, options);

export default mongoose.model('NotificationPreferences', notificationPreferenceSchema);
