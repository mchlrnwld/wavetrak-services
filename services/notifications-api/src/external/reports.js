import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';

export const getSpotReport = async spotId => {
  const params = { spotId, limit: 1 };
  const url = `${config.REPORTS_API}/admin/reports/spot?${createParamString(params)}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export const getSpotReportView = async spotId => {
  const url = `${config.KBYG_API}/kbyg/spots/reports?spotId=${spotId}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
