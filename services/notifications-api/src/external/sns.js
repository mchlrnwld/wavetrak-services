import fetch from 'node-fetch';

const getSubscribe = async (SubscribeURL) => {
  const response = await fetch(SubscribeURL);
  const body = await response.text();
  if (response.status === 200) return body;
  if (response.status === 404) return null;
  throw body;
};

export default getSubscribe;
