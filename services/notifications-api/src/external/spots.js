import fetch from 'node-fetch';
import config from '../config';

// eslint-disable-next-line import/prefer-default-export
export const getSpot = async spotId => {
  const url = `${config.SPOTS_API_HOST}/admin/spots/${spotId}`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status === 200) return body;
  if (response.status === 404) return null;
  console.log(body)
  throw body;
};
