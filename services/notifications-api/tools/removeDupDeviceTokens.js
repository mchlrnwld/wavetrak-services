/* eslint-disable no-await-in-loop */
import { initMongoDB } from '../src/model/dbContext';
import Outlets from '../src/model/Outlets/schema';

const wait = async ms => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

const removeDuplicates = async errors => {
  const duplicates = await Outlets.aggregate([
    {
      $group: {
        _id: { deviceToken: '$deviceToken', user: '$user' }, // can be grouped on multiple properties
        dups: { $addToSet: '$deviceToken' },
        count: { $sum: 1 },
      },
    },
    {
      $match: {
        count: { $gt: 1 }, // Duplicates considered as count greater than one
      },
    },
  ]).allowDiskUse(true);

  // eslint-disable-next-line no-restricted-syntax
  for (const duplicate of duplicates) {
    try {
      const {
        _id: { deviceToken, user },
        count,
      } = duplicate;

      const outletsToRemove = await Outlets.find({ deviceToken, user })
        .limit(count - 1)
        .select({ _id: 1 });

      console.log(outletsToRemove);
      await Outlets.deleteMany({
        _id: { $in: outletsToRemove.map(({ _id }) => _id.toString()) },
      });
    } catch (err) {
      errors.push(err);
    }
    await wait(5000);
  }
};

const runScript = async () => {
  try {
    await initMongoDB();
    const errors = [];

    await removeDuplicates(errors);

    console.log(errors);
    console.log('DONE');
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runScript();
