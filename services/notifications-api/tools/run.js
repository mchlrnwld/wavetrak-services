const run = (fn, options) => {
  const start = new Date();
  return fn(options).then(() =>
    console.log(`\nFinished after ${new Date().getTime() - start.getTime()} ms`));
};

if (process.mainModule.children.length === 0 && process.argv.length > 2) {
  delete require.cache[__filename];
  const module = require(`./${process.argv[2]}.js`).default;
  run(module)
    .catch(err => console.error(err.stack))
    .then(result => process.exit(0));
}

export default run;
