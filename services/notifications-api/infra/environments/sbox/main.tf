provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "notifications-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "notifications_service" {
  source = "../../"

  company     = "wt"
  application = "notifications-service"
  environment = "sandbox"

  apns_platform_type = "APNS_SANDBOX"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "notifications-service.sandbox.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"

  dns_name    = "notifications-service.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled = false

  notifications_service_sns_http_endpoint      = "http://services.sandbox.surfline.com/notifications/webhooks/spotDetails"
  session_updated_sns_http_endpoint            = "http://services.sandbox.surfline.com/notifications/webhooks/sessionDetails"
  session_updated_topic_arn                    = "arn:aws:sns:us-west-1:665294954271:sl-sessions-api-sessions-updated-sns_sandbox"
}
