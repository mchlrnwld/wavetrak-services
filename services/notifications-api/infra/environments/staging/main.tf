provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "notifications-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "notifications_service" {
  source = "../../"

  company     = "wt"
  application = "notifications-service"
  environment = "staging"

  apns_platform_type = "APNS_SANDBOX"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "notifications-service.staging.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"

  dns_name    = "notifications-service.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled = false

  notifications_service_sns_http_endpoint      = "http://services.staging.surfline.com/notifications/webhooks/spotDetails"
  session_updated_sns_http_endpoint            = "http://services.staging.surfline.com/notifications/webhooks/sessionDetails"
  session_updated_topic_arn                    = "arn:aws:sns:us-west-1:665294954271:sl-sessions-api-sessions-updated-sns_staging"
}
