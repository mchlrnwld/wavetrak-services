provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "notifications-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "notifications_service" {
  source = "../../"

  company     = "wt"
  application = "notifications-service"
  environment = "prod"

  apns_platform_type = "APNS"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "notifications-service.prod.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"

  dns_name    = "notifications-service.prod.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 800
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000

  notifications_service_sns_http_endpoint      = "http://services.surfline.com/notifications/webhooks/spotDetails"
  session_updated_sns_http_endpoint            = "http://services.surfline.com/notifications/webhooks/sessionDetails"
  session_updated_topic_arn                    = "arn:aws:sns:us-west-1:833713747344:sl-sessions-api-sessions-updated-sns_prod"
}
