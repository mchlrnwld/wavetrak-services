variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "default_vpc" {
}

variable "apns_platform_type" {
}

variable "ecs_cluster" {
}

variable "alb_listener_arn" {
}

variable "service_td_count" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_request_count"
}

variable "auto_scaling_min_size" {
  default = ""
}

variable "auto_scaling_max_size" {
  default = ""
}

variable "auto_scaling_target_value" {
  default = ""
}

variable "notifications_service_sns_http_endpoint" {
}

variable "session_updated_sns_http_endpoint" {
}

variable "session_updated_topic_arn" {
}
