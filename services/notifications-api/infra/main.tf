module "notifications_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-${var.application}"
  service_td_count            = var.service_td_count
  service_td_container_name   = var.application
  service_port                = 8080
  service_alb_priority        = 412

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  dns_name          = var.dns_name
  dns_zone_id       = var.dns_zone_id
  load_balancer_arn = var.load_balancer_arn
  default_vpc       = var.default_vpc
  ecs_cluster       = var.ecs_cluster

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
}

resource "aws_iam_policy" "sns_read_write_policy" {
  name        = "sns_policy_apns_platform_push_notifications_${var.environment}"
  description = "policy to publish/subscribe to sns platform application"
  policy = templatefile("${path.module}/resources/sns-platform-application-policy.json", {
    apns_application_arn = aws_sns_platform_application.apns_application.arn
  })
}

resource "aws_iam_role_policy_attachment" "notifications_service_sns_read_write_attachment" {
  role       = "notifications_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sns_read_write_policy.arn
}

resource "aws_iam_policy" "session_updated_sns_subscription_management_policy" {
  name        = "sns_policy_subscription_management_session_updated_${var.environment}"
  description = "policy to manage subscriptions to the session updated topic"
  policy = templatefile("${path.module}/resources/sns-session-update-subscription-policy.json", {
    topic_arn = var.session_updated_topic_arn
  })
}

resource "aws_iam_role_policy_attachment" "session_updated_sns_read_write_attachment" {
  role       = "notifications_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.session_updated_sns_subscription_management_policy.arn
}

resource "aws_iam_role" "sns_platform_application_delivery_logging_role" {
  name               = "sns_platform_application_delivery_logging_task_role_${var.environment}"
  assume_role_policy = file("${path.module}/resources/sns-delivery-logging-task-role.json")
}

resource "aws_iam_policy" "sns_platform_application_delivery_logging_policy" {
  name        = "${var.company}-${var.application}-platform-app-logging-${var.environment}"
  description = "policy to log delivery status of platform application notifications"
  policy      = file("${path.module}/resources/platform-app-logging-policy.json")
}

resource "aws_iam_role_policy_attachment" "sns_platform_application_delivery_logging_arn" {
  role       = aws_iam_role.sns_platform_application_delivery_logging_role.name
  policy_arn = aws_iam_policy.sns_platform_application_delivery_logging_policy.arn
}

data "aws_ssm_parameter" "apns_private_key" {
  name = "/${var.environment}/notifications-service/apns_private_key"
}

data "aws_ssm_parameter" "apns_certificate" {
  name = "/${var.environment}/notifications-service/apns_certificate"
}

resource "aws_sns_platform_application" "apns_application" {
  name                         = "apns_push_notifications_${var.environment}"
  platform                     = var.apns_platform_type
  platform_credential          = data.aws_ssm_parameter.apns_private_key.value
  platform_principal           = data.aws_ssm_parameter.apns_certificate.value
  success_feedback_sample_rate = 10
  success_feedback_role_arn    = aws_iam_role.sns_platform_application_delivery_logging_role.arn
  failure_feedback_role_arn    = aws_iam_role.sns_platform_application_delivery_logging_role.arn
}

data "aws_s3_bucket" "sl_notification_batches" {
  bucket = "sl-notification-batches-${var.environment}"
}

resource "aws_iam_policy" "s3_notification_batches_management_policy" {
  name        = "${var.company}-${var.application}-s3-notification-batches-management-policy-${var.environment}"
  description = "policy to create notification batch object in the s3 bucket"
  policy = templatefile("${path.module}/resources/s3-notification-policy.json", {
    bucket_arn = data.aws_s3_bucket.sl_notification_batches.arn
  })
}

resource "aws_iam_role_policy_attachment" "s3_notification_batches_management_policy_attachment" {
  role       = "notifications_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.s3_notification_batches_management_policy.arn
}

data "aws_sns_topic" "spot_report_updated_topic" {
  name = "spot_report_updated_${var.environment}"
}

resource "aws_sns_topic_subscription" "spot_report_updated_target" {
  topic_arn              = data.aws_sns_topic.spot_report_updated_topic.arn
  protocol               = "http"
  endpoint               = var.notifications_service_sns_http_endpoint
  endpoint_auto_confirms = true
  delivery_policy        = file("${path.module}/resources/default-delivery-policy.json")
}

resource "aws_sns_topic_subscription" "session_updated_sns_subscription" {
  topic_arn              = var.session_updated_topic_arn
  protocol               = "http"
  endpoint               = var.session_updated_sns_http_endpoint
  endpoint_auto_confirms = true
  delivery_policy        = file("${path.module}/resources/sns-subscription-delivery-policy.json")
}
