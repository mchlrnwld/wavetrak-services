module "spot_recommender_svc" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = "spot-recommender-svc"

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/status.jspx"
  service_td_name             = "sl-core-svc-${var.environment}-spot-recommender-svc"
  service_td_container_name   = "spot-recommender-svc"
  service_td_count            = var.service_td_count
  service_port                = 8091
  service_alb_priority        = 456

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  dns_name          = var.dns_name
  dns_zone_id       = var.dns_zone_id
  load_balancer_arn = var.load_balancer_arn
  default_vpc       = var.default_vpc
  ecs_cluster       = var.ecs_cluster
}
