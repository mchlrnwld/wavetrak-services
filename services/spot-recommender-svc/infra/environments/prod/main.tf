provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/spot-recommender-svc/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "spot_recommender_svc" {
  source = "../../"

  company     = "sl"
  application = "spot-recommender-svc"
  environment = "prod"

  ecs_cluster      = "sl-core-svc-prod"
  default_vpc      = "vpc-116fdb74"
  dns_name         = "spot-recommender-svc.prod.surfline.com"
  dns_zone_id      = "Z3LLOZIY0ZZQDE"
  service_td_count = 6
  service_lb_rules = [
    {
      field = "host-header"
      value = "spot-recommender-svc.prod.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"
}
