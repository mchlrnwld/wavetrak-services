provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/spot-recommender-svc/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "spot_recommender_svc" {
  source = "../../"

  company     = "sl"
  application = "spot-recommender-svc"
  environment = "sandbox"

  ecs_cluster      = "sl-core-svc-sandbox"
  default_vpc      = "vpc-981887fd"
  dns_name         = "spot-recommender-svc.sandbox.surfline.com"
  dns_zone_id      = "Z3DM6R3JR1RYXV"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "spot-recommender-svc.sandbox.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"
}
