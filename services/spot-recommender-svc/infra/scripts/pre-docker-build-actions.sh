#!/bin/bash

# This script pulls down model data from S3 before building docker image

set -x

trap catch_errors ERR;
function catch_errors() {
  echo "script aborted, because of errors";
  exit 1;
}

# Read in env specific parameters
PROFILE=$1
WORKSPACE=$2
CONTAINER_PATH=$3

assume-role ${PROFILE} aws s3 cp s3://sl-artifacts-${PROFILE}/ml/spot-recommender/model.tgz \
  "${WORKSPACE}/${CONTAINER_PATH}/model/model.tgz"

