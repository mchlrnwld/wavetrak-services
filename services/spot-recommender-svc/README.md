# Spot recommender service

## Background
Surfline spot recommender service utilizes [Oryx Machine Learning library v1](https://github.com/Surfline/Oryx) to analyze user-spot engagement and provides recommended spots.

## Run locally  
#### Prerequisite
Install [Docker](https://www.docker.com/)

#### Steps to run recommender (with a pre-built model) locally
Git clone this repo and switch to `services/spot-recommender-svc`

Run the following to build and run docker container

```
$ docker image build -t oryx-docker .
$ docker run -it -p 8091:8091 oryx-docker
```

Open your browser and navigate to [Oryx serving URL](http://localhost:8091/) for a set of supported API

## Main recommender API end points (WIP) ##

Oryx supports various APIs and the following are main APIs used for Surfline frontend. Note that Oryx is slightly problematic supporting request and response headers. The suggestion is using pure `text/plain` in the request header.


### `GET /recommend/<user-id>` ###

| Parameter | Required | Default Value | Description |
| --------- | -------- | ----------- | ----------- |
| `user-id` | Yes | | User ID to predict recommendation (path parameter)|
| `howMany` | No | 10 | How many items to return in the recommendation list  |
| `considerKnownItems` | No | False | Whether to consider returning known (visited) items in  the recommendation list |

Sample query:
```
curl -i -H "Accept: text/plain" http://<Server>:8091/recommend/56fae71219b74318a73a7f34
```

Sample response (the response is item-ID followed by a recommendation score):

```
HTTP/1.1 200 OK
Content-Type: text/plain;charset=UTF-8
Content-Length: 352
Date: Fri, 17 Nov 2017 18:58:03 GMT
Server: Oryx

584204204e65fad6a7709981,0.81340706
5842041f4e65fad6a7708863,0.8046647
5842041f4e65fad6a7708a44,0.78044516
5842041f4e65fad6a7708a42,0.6761961
5842041f4e65fad6a77088ee,0.6558729
584204214e65fad6a7709cba,0.62196326
5842041f4e65fad6a770882e,0.6187538
5842041f4e65fad6a7708a43,0.6163416
584204204e65fad6a7709464,0.603545
584204204e65fad6a77091aa,0.5906389

```

### `GET /mostPopularItems/` ###
(WIP)
