provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "admin-tools/admin-forecasts-service.tfstate"
    region = "us-west-1"
  }
}

module "service" {
  source = "../../"

  environment = "prod"
  default_vpc = "vpc-116fdb74"
  dns_zone    = "prod.surfline.com"

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/4796722fa18acc2f"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b"
  target_group_name = "admin-forecasts-svc"

  service_td_count = 2
}
