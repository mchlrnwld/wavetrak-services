import { slugify } from '@surfline/web-common';
import baseFetchSignedAWSESRequest from './baseFetchSignedAWSESRequest';
import config from './config';

const fetchForecastsWithScroll = async (scrollId = null) => {
  const scroll = '5m';
  const body = scrollId ? {
    scroll,
    scroll_id: scrollId,
  } : {
    query: {
      match_all: {},
    },
    sort: ['_doc'],
  };
  const query = scrollId ? '' : `scroll=${scroll}`;
  const path = scrollId ? '/_search/scroll' : '/feed/forecast/_search';
  return baseFetchSignedAWSESRequest(
    `${config.ELASTICSEARCH_URL}${path}?${query}`,
    JSON.stringify(body),
  );
};

const permalink = (title, id) => `${config.SURFLINE_HOST}/surf-news/local/${slugify(title)}/${id}`;

const main = async () => {
  const initialScrollSearch = await fetchForecastsWithScroll();
  const scrollId = initialScrollSearch._scroll_id; // eslint-disable-line no-underscore-dangle

  let documents = initialScrollSearch.hits.hits;
  let documentsFound = 0;

  while (documents.length > 0) {
    if (config.DEBUG === 'false') {
      await Promise.all(documents.map(({ _id, _source }) =>
        baseFetchSignedAWSESRequest(
          `${config.ELASTICSEARCH_URL}/feed/forecast/${_id}/_update`,
          JSON.stringify({
            doc: {
              permalink: permalink(_source.content.title, _id),
            },
          }),
        )));
    } else {
      console.log(documents.map(({ _id, _source }) => permalink(_source.content.title, _id)));
    }
    documentsFound += documents.length;
    console.log(documentsFound);
    const scrollSearch = await fetchForecastsWithScroll(scrollId);
    documents = scrollSearch.hits.hits;
  }

  console.log(`${documentsFound} articles ${config.DEBUG === 'false' ? 'updated' : 'found'}!`);
};

main().then(() => {
  console.log('Done!');
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(-1);
});
