# Run this to reset (delete!) your locations index and mappings. You'll need to reimport your data.

import json
from pprint import pprint
import codecs
import requests

location_mapping = json.load(codecs.open('../mapping/locations/location.json', 'r', 'utf-8-sig'))

url = 'http://localhost:9200/locations/'
r = requests.delete(url)
print(r.text)
if r.status_code > 300:
    pprint('Not 200!?')
else:
    url = 'http://localhost:9200/locations/'
    r = requests.put(url)
    print(r.text)
    if r.status_code > 300:
        pprint('Not 200!?')
    else:
        url = 'http://localhost:9200/locations/_mapping/location'
        r = requests.put(url, json=location_mapping)
        print(r.text)
        if r.status_code > 300:
            pprint('Not 200!?')