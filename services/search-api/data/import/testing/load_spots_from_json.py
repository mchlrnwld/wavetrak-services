import json
from pprint import pprint
import codecs
import requests

spots = json.load(codecs.open('spots_alternate_published.json', 'r', 'utf-8-sig'))

for spot in spots:
    url = 'http://localhost:9200/spots/spot'
    spotCopy = spot
    if spotCopy['location']:
        spotCopy['location'] = dict(
            lon = spot['location']['coordinates'][0],
            lat = spot['location']['coordinates'][1]
        )
    r = requests.post(url, json=spotCopy)
    print(r.text)
    if r.status_code > 300:
        pprint('Not 200!?')
