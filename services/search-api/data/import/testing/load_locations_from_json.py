import json
from pprint import pprint
import codecs
import requests

spots = json.load(codecs.open('spots_alternate_published.json', 'r', 'utf-8-sig'))

# {
#     "name":
#     "location"
#     "admin_info"
#         "geo_name_id":  { "type": "integer" },
#         "name": {  "type": "text" },
#         "admin_level": {  "type": "integer" }
#     "admin_level_1"
#         "geo_name_id":  { "type": "integer" },
#         "name": {  "type": "text" },
#     "admin_level_2"
#     "admin_level_3"
#     "admin_level_4"
#     "admin_level_5"
# }
# }
world = dict()
for spot in spots:
    if 'politicalLocation' in spot:
        politicalLocation = spot['politicalLocation']
        if 'hierarchy' in politicalLocation:
            level = 0
            levels = []
            for location in politicalLocation['hierarchy']:
                levels.append(location['geonameId'])
                if level == 1:
                    # continent
                    temp = world
                    parent = levels[1]
                    if parent not in world:
                        temp[parent] = dict(
                            name = location['name'],
                            location = dict(
                                lon = location['lng'],
                                lat = location['lat']
                            ),
                            children = dict()
                        )
                elif level == 2:
                    # country
                    temp = world[levels[1]]['children']
                    parent = levels[2]
                    if parent not in temp:
                        temp[parent] = dict(
                            name = location['name'],
                            location = dict(
                                lon = location['lng'],
                                lat = location['lat']
                            ),
                            children = dict()
                        )
                elif level == 3:
                    # state
                    temp = world[levels[1]]['children'][levels[2]]['children']
                    parent = levels[3]
                    if parent not in temp:
                        temp[parent] = dict(
                            name = location['name'],
                            location = dict(
                                lon = location['lng'],
                                lat = location['lat']
                            ),
                            children = dict()
                        )
                    else:
                        print(world)
                elif level == 4:
                    # county
                    temp = world[levels[1]]['children'][levels[2]]['children'][levels[3]]['children']
                    parent = levels[4]
                    if parent not in temp:
                        temp[parent] = dict(
                            name = location['name'],
                            location = dict(
                                lon = location['lng'],
                                lat = location['lat']
                            ),
                            children = dict()
                        )
                elif level == 5:
                    # city
                    temp = world[levels[1]]['children'][levels[2]]['children'][levels[3]]['children'][levels[4]]['children']
                    parent = levels[5]
                    if parent not in temp:
                        temp[parent] = dict(
                            name = location['name'],
                            location = dict(
                                lon = location['lng'],
                                lat = location['lat']
                            ),
                            children = dict()
                        )
                level += 1
print(world)

url = 'http://localhost:9200/locations/location'

for continent in world:
    r = requests.post(url, json=dict(
        name = world[continent]['name'],
        location = world[continent]['location'],
        admin_info = dict(
            geo_name_id = continent,
            name = world[continent]['name'],
            admin_level = 1
        )
    ))
    print(r.text)
    if r.status_code > 300:
        pprint('Not 200!?')
    for country in world[continent]['children']:
        _country = world[continent]['children'][country]
        r = requests.post(url, json=dict(
            name = _country['name'],
            location = _country['location'],
            admin_info = dict(
                geo_name_id = country,
                name = _country['name'],
                admin_level = 2
            ),
            admin_level_1 = dict(
                geo_name_id = continent,
                name = world[continent]['name']
            )
        ))
        print(r.text)
        if r.status_code > 300:
            pprint('Not 200!?')
        for state in _country['children']:
            _state = _country['children'][state]
            r = requests.post(url, json=dict(
                name = _state['name'],
                location = _state['location'],
                admin_info = dict(
                    geo_name_id = state,
                    name = _state['name'],
                    admin_level = 3
                ),
                admin_level_1 = dict(
                    geo_name_id = continent,
                    name = world[continent]['name']
                ),
                admin_level_2 = dict(
                    geo_name_id = country,
                    name = _country['name']
                )
            ))
            print(r.text)
            if r.status_code > 300:
                pprint('Not 200!?')
            for county in _state['children']:
                _county = _state['children'][county]
                r = requests.post(url, json=dict(
                    name = _county['name'],
                    location = _county['location'],
                    admin_info = dict(
                        geo_name_id = county,
                        name = _county['name'],
                        admin_level = 3
                    ),
                    admin_level_1 = dict(
                        geo_name_id = continent,
                        name = world[continent]['name']
                    ),
                    admin_level_2 = dict(
                        geo_name_id = country,
                        name = _country['name']
                    ),
                    admin_level_3 = dict(
                        geo_name_id = state,
                        name = _state['name']
                    )
                ))
                print(r.text)
                if r.status_code > 300:
                    pprint('Not 200!?')
                for city in _county['children']:
                    _city = _county['children'][city]
                    r = requests.post(url, json=dict(
                        name = _city['name'],
                        location = _city['location'],
                        admin_info = dict(
                            geo_name_id = city,
                            name = _city['name'],
                            admin_level = 3
                        ),
                        admin_level_1 = dict(
                            geo_name_id = continent,
                            name = world[continent]['name']
                        ),
                        admin_level_2 = dict(
                            geo_name_id = country,
                            name = _country['name']
                        ),
                        admin_level_3 = dict(
                            geo_name_id = state,
                            name = _state['name']
                        ),
                        admin_level_4 = dict(
                            geo_name_id = county,
                            name = _county['name']
                        )
                    ))
                    print(r.text)
                    if r.status_code > 300:
                        pprint('Not 200!?')
