# Data import for local testing

I'm using elasticsearch & kibana 5.2, locally installed with brew. Run elasticsearch (port 9200) and kibana (port 5601) independently.

Data import scripts depend on a mongo export of `Spots` collection. We will need to refactor with taxonomy data from taxonomy service.

## Prepare elasticsearch
 * Create index `PUT localhost:9200/spots/`
 * Create mapping `PUT localhost:9200/spots/_mapping/spot` with body from mapping dir
 
## Import spots
 * `queries/mongo_spotexport.txt`: I'm using MongoChef to export and save as mongoexport format
 * The spots are saved in `import/spots_partial.json`
 * Using python 3, `import/load_spots_from_json.py` loads the spots into elasticsearch
 
# Query elasticsearch
`queries/SpotSearch.postman_collection.json` has some sample queries

## Concepts

* Completion suggester https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-completion.html
* Context suggester https://www.elastic.co/guide/en/elasticsearch/reference/current/suggester-context.html

## Mapping modifications
Other than where documented, existing type and field mappings cannot be updated. Changing the mapping would mean invalidating already indexed documents. Instead, you should create a new index with the correct mappings and reindex your data into that index. [[source]](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html)

## Debugging
Use GeoNames's website: `http://www.geonames.org/${geoname_id}/` like: [4155751](http://www.geonames.org/4155751/). We could store the URL alongside geoname entries so we can debug in Kibana.