# Initializing Elasticsearch on AWS

* `initialize_aws_elasticsearch.js`: initializer for aws-credentialed ES domain; calls `load_mappings`
* `initialize_local_elasticsearch.js`: initializer for locally managed ES cluster; calls `load_mappings`
* `load_mappings.js`: creates spots index + spot mapping and locations index + location mapping
* `initialize_*_taxonomy.js`: initializer for ES domains; calls `load_spot_and_taxonomy`
* `load_spot_and_taxonomy.js`: loads spot and taxonomy data into indexes


If running locally, you can use environment variables for AWS config:
```
AWS_DEFAULT_REGION
AWS_SECRET_KEY
AWS_ACCESS_KEY
ELASTIC_SEARCH_CONNECTION_STRING
```
