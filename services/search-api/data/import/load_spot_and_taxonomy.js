import fetch from 'node-fetch';

const USE_DEMO_DATA = process.env.USE_DEMO_DATA || false;

const getAllSpots = async () => {
  if (USE_DEMO_DATA) {
    return require('./spots.json');
  }

  const url = `${SPOTS_API}/admin/spots/`;
  console.log(url);
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

const getTaxonomyForSpotId = async (spotId) => {
  const url = `${SPOTS_API}/taxonomy?id=${spotId}&type=spot`;
  console.log(url);
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

const getWwwLink = (associated) => {
  const www = (associated.links.find(link => (link && link.key === 'www')));
  return www ? www.href : '';
};
const getEndGeonamesNode = hierarchy => hierarchy.find(node => (node && node.category === 'geonames' && node.depth === 1));
const getBreadcrumbsFromPath = (enumeratedPath) => {
  const enumeratedPathArray = enumeratedPath.split(',');
  return enumeratedPathArray.slice(3, enumeratedPathArray.length - 1);
};
const getGeoPointFromCoordinates = coordinates => ({
  lat: coordinates[1],
  lon: coordinates[0],
});

const parseSpotInformationFromTaxonomy = (taxonomy) => {
  const { location: loc, associated, name, 'in': hierarchy } = taxonomy;
  const location = loc && loc.coordinates ? getGeoPointFromCoordinates(loc.coordinates) : null;
  const href = getWwwLink(associated);
  const { enumeratedPath } = getEndGeonamesNode(hierarchy);
  const breadCrumbs = getBreadcrumbsFromPath(enumeratedPath);
  return { location, href, name, breadCrumbs };
};

const parseLocationsFromTaxonomy = (taxonomy) => {
  const { 'in': [,, ...hierarchy] } = taxonomy; // Skipping earth and continent, straight to country
  return hierarchy.map((level) => {
    const { associated, location: loc, name, enumeratedPath, _id, type } = level;
    const location = loc && loc.coordinates ? getGeoPointFromCoordinates(loc.coordinates) : null;
    const href = getWwwLink(associated);
    const breadCrumbs = enumeratedPath ? getBreadcrumbsFromPath(enumeratedPath) : null;

    return { href, breadCrumbs, name, location, _id, type };
  });
};

module.exports = (client) => {
  async function indexSpot(taxonomy, cams, _id) {
    const spotInformation = parseSpotInformationFromTaxonomy(taxonomy);

    // Index spot
    await client.index({
      index: 'spots',
      type: 'spot',
      body: {
        ...spotInformation,
        name_standard: spotInformation.name,
        cams,
      },
      id: _id,
    });
    return spotInformation;
  }

  async function indexTaxonomy(taxonomy) {
    const locations = parseLocationsFromTaxonomy(taxonomy);

    for (const { href, breadCrumbs, name, location, _id, type } of locations) {
      // Index each taxonomy level, mapped to subregion or geoname type
      await client.index({
        index: 'taxonomy',
        type,
        body: { href, breadCrumbs, name, location, name_standard: name },
        id: _id,
      });
    }

    return locations;
  }

  async function loadSpotData(publishedSpots) {
    for (const { _id, name, cams } of publishedSpots) {
      try {
        const taxonomy = await getTaxonomyForSpotId(_id);
        await indexSpot(taxonomy, cams, _id);
        await indexTaxonomy(taxonomy);
      } catch (e) {
        console.error(e);
        console.error(_id, name);
      }
    }
  }

  async function run() {
    try {
      const spots = await getAllSpots();
      const publishedSpots = spots.filter(spot => spot.status === 'PUBLISHED');
      await loadSpotData(publishedSpots);
      process.exit(0);
    } catch (e) {
      console.error(e);
      process.exit(1);
    }
  }

  run();
};
