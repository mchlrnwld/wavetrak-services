import spotMapping from '../mapping/spots/spot.json';
import geonameMapping from '../mapping/taxonomy/geoname.json';
import subregionMapping from '../mapping/taxonomy/subregion.json';
import regionMapping from '../mapping/taxonomy/region.json';


module.exports = function (client) {
  // Deletes Indices
  async function deleteIndices() {
    const deleteResult1 = await client.indices
      .delete({ index: 'spots' });
    console.log('Deleted spots index', deleteResult1);
    const deleteResult2 = await client.indices
      .delete({ index: 'taxonomy' });
    console.log('Deleted taxonomy index', deleteResult2);
  }

  // Creates spots index -> mapping -> deletes index.
  async function initializeSpotsIndexAndMapping() {
    const indexResult = await client.indices
      .create({ index: 'spots' });
    console.log('Created spots index', indexResult);
    const mappingResult = await client.indices
      .putMapping({ index: 'spots', type: 'spot', body: spotMapping });
    console.log('Created spots index with spot mapping', mappingResult);
  }

  // Creates locations index -> mapping -> deletes index.
  async function initializeLocationsIndexAndMapping() {
    const indexResult = await client.indices
      .create({ index: 'taxonomy' });
    console.log('Created taxonomy index', indexResult);
    const geoMappingResult = await client.indices
      .putMapping({ index: 'taxonomy', type: 'geoname', body: geonameMapping });
    console.log('Created taxonomy index with geoname mapping', geoMappingResult);
    const subMappingResult = await client.indices
      .putMapping({ index: 'taxonomy', type: 'subregion', body: subregionMapping });
    console.log('Created taxonomy index with subregion mapping', subMappingResult);
    const regionResult = await client.indices
      .putMapping({ index: 'taxonomy', type: 'region', body: regionMapping });
    console.log('Created taxonomy index with region mapping', regionResult);
  }

  async function run() {
    try {
      await deleteIndices();
      await initializeSpotsIndexAndMapping();
      await initializeLocationsIndexAndMapping();
      process.exit(0);
    } catch (e) {
      console.error(e);
      process.exit(1);
    }
  }

  run();
};
