require('babel-register');
const es = require('elasticsearch');

const esConnectionString = process.env.ELASTIC_SEARCH_CONNECTION_STRING || 'http://localhost:9200';

const client = new es.Client({
  host: esConnectionString,
  log: 'trace',
});

require('./load_mappings')(client);
