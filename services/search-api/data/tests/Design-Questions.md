# Design discussions

## Requirements

* If I type "North Orange" I should get North Orange County which should take me to the forecast view.
* Working on the assumption that "Orange County" is a different hit from "HB Pier Southside" or "North Orange County" (but all three results should return for the query "Orange County")

## Feed
### Questions
* What query params does the feed store (ES) need to return personalized news articles for MVP (Can a favorites document drive this query?)
    * Input sources: Favorites, "Followed" entities
    * Authenticated call to get favorites -> use as _search attributes on feed/article/_search
    * Easy parameters: favorited SpotId, Spot admin levels, geo-location
    * Easy boosting: date, has cam, recently added, "promoted"-attribute
    * More difficult filtering: popularity (need to ingest visits per spot or per user), recommendations (need to ingest user activity)
    * Unauthenticated users uses geo-location?
    * Date sort or Date decay scoring: https://www.elastic.co/guide/en/elasticsearch/reference/5.2/query-dsl-function-score-query.html#function-script-score
* Where should those query params live? how often do they change?
    * The query params could live on the favorites object in Mongo. Call ES after a mongo call to get the array of favorites (and other possible feed filters).
* How will WordPress export data into ElasticSearch?
    * On create/delete/update trigger
    * Shards clear query cache on data updates, so watch for performance hits
* Feed search?
    * Full-text search on indexed articles
    * Store article text, tags, author, ...
    
## Taxonomy
### Questions
* What data does ES need to source from spots and related geographic regions/points to enable search for MVP launch
    * Geoname-id and/or lat-lon is the bare-minimum. Extra information is useful for plain-text searching and debugging. 
    * Cleaning up taxonomies would be helpful, but the existing "politicalLocation" actually works fine for querying
    * Since joins / relationships aren't Elasticsearch's strength, denormalizing taxonomoies through spots / feeds / etc... is recommended
* Prototypes and queries against ES to show what it's capabilities are
    * Added example to postman export
* What does the taxonomy look like in ElasticSearch
    * This query filters and ranks for [Florida](http://www.geonames.org/4155751/) > [Saint-John's County](http://www.geonames.org/4171153/saint-johns-county.html). 
    The example returns spots, but the same logic would work for the feed. 
```$xslt
{
  "_source": {
    "includes": [
      "timezone",
      "name",
      "_score",
      "*.geonameId"
    ]
  },
  "query": {
    "bool": {
      "filter": {
        "match": {
          "status": "PUBLISHED"
        }
      },
      "should": [
        {
          "query_string": {
            "fields": [
              "*.geonameId"
            ],
            "query": 4155751,
            "use_dis_max": true
          }
        },
        {
          "query_string": {
            "fields": [
              "*.geonameId"
            ],
            "query": 4171153,
            "use_dis_max": true
          }
        }
      ],
      "minimum_should_match": 1
    }
  }
}
```

## Elasticsearch 

* When spot details are updated will you invalidate the whole index, or update the index based on what changed. Same goes for regions and subregions.
	* Elasticsearch writes are more expensive than reads, but it's still near real-time. Index updates invalidate the shard cache. We still need to evaluate the impact of shard cache changes. 
* If we need to represent spots, subregions, geonames and regions as separate documents are these in the same index represented as different types, or separate indexes?
	* We should store these in a separate index because of shard caching, mapping consistency, and lack of efficient join support. We can build efficient queries that span indexes.
* When pulling these params from our data store would we need to identify each query param by type. 
	* `{
		  spots: [spotIds...]
		  geoNames: [geoNameIds...],
		  subregions: [...],
		  regions: [...]
		}`
	* We should retain geoName admin level organization so we can order results by admin level.
* Per @MWers's suggestion, would it be more efficient to store these in an ElasticSearch UserFavorites index?
	* I'm not clear on what we would gain by storing favorites in an ES index. 
* What would you need a lat/lon for?
	* We can use this for search result targetting. It's inexpensive to add, but can help organize/query spots.
* Working on the assumption that "Orange County" is a different hit from "HB Pier Southside" or "North Orange County" (but all three results should return for the query "Orange County")
	* We would need to evaluate a different suggester to support efficient in-string searching for auto-complete
* How do you determine that "Orange County" goes to the map page, but "HB Pier Southside" goes to a spot page?
	* The front-end search results choose URLs based on hit attributes.
* Should "Orange County" be it's own document?
	* If we want Orange County to show up as a search result, yes. Any taxonomy or geo-names objects that we want searchable as independent entities should be documents in an index.
	* We could index the names of taxonomy entries on spots and directly search those, but storing the taxonomies separately would enable us to increase query cache performance and give us more flexible query options.

## URL generation
* How would you generate a URL for "Orange County" if we're only sourcing Spot data? A geoname id is probably enough to send as a key to our API, but we need the search result to return a url similar to /surf-reports-forecasts-cams/us/ca/orange-county-[geoNameId]. I think our taxonomy API could do this, but I want to make sure we identify the requirement.
	* We can aggregate admin levels at the county level from spots to get a list of counties where we have spots
	* Rough template: `/surf-reports-forecasts-cams/{grouping.adm1.shortcode}/{grouping.adm2.shortcode}/{grouping.adm3.name}-{grouping.adm3.geoNameId}`
* Does the existing political hierarchy object give you enough information to buildup that URL?
	* If we only need shortcode, name, geoName, it should.
* Should we build up that URL somewhere else, eg taxonomy api, or frontend?
	* Are these URLs dynamically generated or do they include curated content?
	* Where do the links to these admin-level pages surface on the app/site?

## Data sourcing

* What would a cleaned up politicalLocation object look like? Does the existing implementation work well for querying anything other than spots?
	* We should prune the political location object to just include the data we want to query (admin level, name, code, shortcode, ?)
* What data are you missing to enable the rest of the search initiative.
	* More detailed Subregion and Region information. That data isn't currently located on the Spot model or API.
* Where does the source of truth come from? An API, or Mongo?
	* I'm treating Elasticsearch as a view/search layer. The source of truth for spots should be in Mongo. Elasticsearch would be the "source of truth" for search results. 
* What kind of hooks do you need? Eg, SNS
	* Taxonomy information feeds into spots and into their own index for searching. SNS hooks are a logical choice to track updates.  
	* What are the sources for taxonomy updates? Admin services that the UI drives?
