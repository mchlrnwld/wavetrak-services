import escapeStringRexexp from 'escape-string-regexp';
import scrollSearch from './scrollSearch';
import updateDoc from './updateDoc';
import config from './config';

const replaceHostName = url => url && url.replace(
  new RegExp(escapeStringRexexp(config.PREVIOUS_HOST_NAME), 'g'),
  config.NEXT_HOST_NAME,
);

const baseSearchAndUpdateHostName = async (path, buildUpdateDoc) => {
  const body = {
    query: {
      bool: {
        must: {
          match_all: {},
        },
        filter: {
          query_string: {
            query: config.PREVIOUS_HOST_NAME,
          },
        },
      },
    },
  };
  return scrollSearch(
    path,
    body,
    documents => Promise.all(documents.map(({ _index, _type, _id, _source }) =>
      updateDoc(_index, _type, _id, buildUpdateDoc(_source)))),
  );
};

export const searchAndUpdateFeedHostName = () => baseSearchAndUpdateHostName(
  '/feed',
  ({ permalink, tags, media, content }) => ({
    permalink: replaceHostName(permalink),
    media: media && {
      ...media,
      promobox1x: replaceHostName(media.promobox1x),
      promobox2x: replaceHostName(media.promobox2x),
      feed1x: replaceHostName(media.feed1x),
      feed2x: replaceHostName(media.feed2x),
    },
    tags: tags && tags.map(tag => ({
      ...tag,
      url: replaceHostName(tag.url),
    })),
    content: content && {
      ...content,
      body: replaceHostName(content.body),
    },
  }),
);

export const searchAndUpdateSpotsHostName = () => baseSearchAndUpdateHostName(
  '/spots',
  ({ href }) => ({ href: replaceHostName(href) }),
);

export const searchAndUpdateTaxonomyHostName = () => baseSearchAndUpdateHostName(
  '/taxonomy',
  ({ href, travel_href }) => ({
    href: replaceHostName(href),
    travel_href: replaceHostName(travel_href),
  }),
);
