import fetch from 'node-fetch';
import AWS from 'aws-sdk';
import config from './config';

const awsConfig = new AWS.Config({
  accessKeyId: config.AWS_ACCESS_KEY_ID,
  secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
  region: config.AWS_DEFAULT_REGION,
});

const getCredentials = () => new Promise((resolve, reject) =>
  awsConfig.getCredentials((err, credentials) => {
    if (err) return reject(err);
    return resolve(credentials);
  }));

const baseFetchSignedAWSESRequest = async (url, body) => {
  const endpoint = new AWS.Endpoint(url);
  const request = new AWS.HttpRequest(endpoint);
  request.region = awsConfig.region;
  request.headers = {
    Host: endpoint.host,
    'Content-Type': 'application/json',
  };
  request.body = body;
  const signer = new AWS.Signers.V4(request, 'es');
  signer.addAuthorization(await getCredentials(), new Date());
  const response = await fetch(request.endpoint.href, {
    method: request.method,
    headers: request.headers,
    body: request.body,
  });
  const responseBody = await response.json();
  // eslint-disable-next-line yoda
  if (200 <= response.status && response.status < 300) return responseBody;
  throw responseBody;
};

export default baseFetchSignedAWSESRequest;
