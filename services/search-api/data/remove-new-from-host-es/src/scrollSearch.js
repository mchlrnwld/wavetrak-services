import baseFetchSignedAWSESRequest from './baseFetchSignedAWSESRequest';
import config from './config';

const scrollSearch = async (basePath, baseBody, batchCallback) => {
  const fetchWithScroll = async (scrollId = null) => {
    const scroll = '5m';
    const body = JSON.stringify(scrollId ? {
      scroll,
      scroll_id: scrollId,
    } : {
      ...baseBody,
      sort: ['_doc'],
    });
    const query = scrollId ? '' : `scroll=${scroll}`;
    const path = scrollId ? '/_search/scroll' : `${basePath}/_search`;
    return baseFetchSignedAWSESRequest(
      `${config.ELASTICSEARCH_URL}${path}?${query}`,
      body,
    );
  };

  const initialScrollSearch = await fetchWithScroll();
  const scrollId = initialScrollSearch._scroll_id; // eslint-disable-line no-underscore-dangle

  let documents = initialScrollSearch.hits.hits;
  let documentsFound = 0;

  while (documents.length > 0) {
    await batchCallback(documents);
    documentsFound += documents.length;
    console.log(basePath, documentsFound);
    const searchResults = await fetchWithScroll(scrollId);
    documents = searchResults.hits.hits;
  }

  return { basePath, documentsFound };
};

export default scrollSearch;
