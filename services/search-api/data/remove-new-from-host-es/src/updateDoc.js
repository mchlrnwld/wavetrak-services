import baseFetchSignedAWSESRequest from './baseFetchSignedAWSESRequest';
import config from './config';

const updateDocNoSave = async (_index, _type, _id, doc) => {
  console.log(`${process.env.ELASTICSEARCH_URL}/${_index}/${_type}/${_id}/_update`, JSON.stringify({ doc }));
};

const updateDocSave = async (_index, _type, _id, doc) => baseFetchSignedAWSESRequest(
  `${process.env.ELASTICSEARCH_URL}/${_index}/${_type}/${_id}/_update`,
  JSON.stringify({ doc }),
);

export default config.DEBUG === 'false' ? updateDocSave : updateDocNoSave;
