import dotenv from 'dotenv';

dotenv.config();

export default {
  AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
  AWS_DEFAULT_REGION: process.env.AWS_DEFAULT_REGION,
  ELASTICSEARCH_URL: process.env.ELASTICSEARCH_URL,
  PREVIOUS_HOST_NAME: process.env.PREVIOUS_HOST_NAME,
  NEXT_HOST_NAME: process.env.NEXT_HOST_NAME,
  DEBUG: process.env.DEBUG,
};
