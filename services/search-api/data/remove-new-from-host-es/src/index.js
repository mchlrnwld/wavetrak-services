/*
Indexes, Documents, Properties to Replace

feed
└-- editorial
   ├-- permalink
   └-- tags[i]
       └-- url

spots
└-- spot
    └-- href

taxonomy
├-- subregion
|   └-- href
├-- travel
|   └-- href
└-- geoname
    └-- href
 */

import {
  searchAndUpdateFeedHostName,
  searchAndUpdateSpotsHostName,
  searchAndUpdateTaxonomyHostName,
} from './searchAndUpdateHostName';

const main = async () => {
  const updated = await Promise.all([
    searchAndUpdateFeedHostName(),
    searchAndUpdateSpotsHostName(),
    searchAndUpdateTaxonomyHostName(),
  ]);

  updated.map(({ basePath, documentsFound }) => console.log(basePath, documentsFound));
};

const start = new Date();
main().then(() => {
  const end = new Date();
  console.log(`Finished in ${end - start}ms!`);
  process.exit(0);
}).catch((err) => {
  const end = new Date();
  console.error(err);
  console.log(`Errored in ${end - start}ms!`);
  process.exit(-1);
});
