# Remove New From Host Name in Elasticsearch

This script does a scroll search through Elasticsearch indices and replaces the host name of properties in the documents found.

## Quickstart

Setup `.env` using `.env.sample` as a sample.

Setting `DEBUG=false` logs out potential updates for debugging purposes. Set `DEBUG=true` to confirm and save the update to ES.

```sh
yarn install
yarn start
```
