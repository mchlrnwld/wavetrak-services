import { setupExpress } from '@surfline/services-common';
import search from './search';
import logger from '../common/logger';

const log = logger('search-service');
const setupApp = () => {
  const { app } = setupExpress({
    allowedMethods: ['GET, OPTIONS'],
    name: 'search-service',
    port: process.env.EXPRESS_PORT || 8081,
    log,
    handlers: [['/search', search(log)]],
  });
  return app;
};

export default setupApp;
