import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import shipAnalytics from '../../handlers/analytics';
import getSite from '../../handlers/site';
import getSpot from '../../handlers/spot';
import getTaxonomy from '../../handlers/taxonomy';
import getMobileSpots from '../../handlers/mobile';

export const trackRequests = log => (req, _, next) => {
  log.trace({
    action: '/search',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const search = (log) => {
  const api = Router();

  api.use('*', trackRequests(log));
  api.post('/analytics', wrapErrors(shipAnalytics));
  api.use('/site', wrapErrors((req, res) => getSite(req, res)));
  api.get('/spots', wrapErrors((req, res) => getSpot(req, res)));
  api.get('/mobile/spots', wrapErrors(getMobileSpots));
  api.get('/taxonomy', wrapErrors((req, res) => getTaxonomy(req, res)));
  return api;
};

export default search;
