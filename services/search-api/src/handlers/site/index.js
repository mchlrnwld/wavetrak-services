import elasticsearch from '../../model/elasticsearch';
import spotsLocationsNewsAutoCompleteSearch from './search';

/**
 * GET http://${service_host}:${service_port}/search/site/?q=hunt
 *
 * Query results are in `hits`
 * Fuzzy suggestions are in `suggest`
 *
 * @param req
 *      query parameters:
 *          "q":  prefix to search for
 *                will use elasticsearch's completion suggester with default 'fuzzy' setting
 *          "suggestionSize":   defaults to 5, otherwise configurable
 *          "querySize":   defaults to 5, otherwise configurable
 * @param res
 *      Returns an array of responses in 'suggest' format. Take a swing at the API to get format.
 */
const getSite = async (req, res) => {
  const { query: { q, suggestionSize = 5, querySize = 5 } } = req;
  const coordinates = req.geoLatitude && req.geoLongitude ? {
    lat: req.geoLatitude,
    lon: req.geoLongitude,
  } : undefined;
  const client = await elasticsearch.getClient();
  const result = await client.msearch(
    spotsLocationsNewsAutoCompleteSearch(
      q,
      suggestionSize,
      querySize,
      coordinates,
    ));
  return res.send(result.responses);
};

export default getSite;
