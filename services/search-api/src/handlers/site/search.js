import {
  matchPhrasePrefixAndScoreLocation,
  matchPhrasePrefixAndScoreFeed,
} from '../../common/queries';

const DEFAULT_LOCATION = { lat: 33.6, lon: -117.9 }; // Huntington Beach

const spotsLocationsNewsAutoCompleteSearch = (
  namePrefix,
  suggestionSize = 5,
  querySize = 5,
  coordinates = DEFAULT_LOCATION) => ({
    body: [
    { index: 'spots' },
      {
        _source: {
          includes: [
            'breadCrumbs',
            'name',
            'location',
            'cams',
            'text',
            'href',
          ],
        },
        size: querySize,
        query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates),
        suggest: {
          'spot-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'name',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  { context: 'spot' },
                ],
                location: [
                  { context: coordinates, boost: 2 },
                ],
              },
            },
          },
        },
      },
    { index: 'taxonomy', types: 'subregion' },
      {
        _source: {
          includes: [
            'breadCrumbs',
            'name',
            'location',
            'text',
            'href',
            'type',
          ],
        },
        size: querySize,
        query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates),
        suggest: {
          'subregion-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'name',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  { context: 'subregion' },
                ],
              },
            },
          },
        },
      },
      { index: 'taxonomy', types: 'geoname' },
      {
        _source: {
          includes: [
            'breadCrumbs',
            'name',
            'location',
            'text',
            'href',
            'type',
          ],
        },
        size: querySize,
        query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates),
        suggest: {
          'geoname-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'name',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  { context: 'geoname' },
                ],
              },
            },
          },
        },
      },
      { index: 'feed' },
      {
        _source: {
          includes: [
            'content.title',
            'content.subtitle',
            'content.displayTitle',
            'createdAt',
            'permalink',
            'tags',
            'suggestTags',
          ],
        },
        size: querySize,
        query: matchPhrasePrefixAndScoreFeed(namePrefix),
        suggest: {
          'feed-tag-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'suggestTags',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  { context: 'editorial' },
                ],
              },
            },
          },
        },
      },
      { index: 'taxonomy', types: 'travel' },
      {
        _source: {
          includes: [
            'breadCrumbs',
            'name',
            'location',
            'text',
            'href',
            'type',
          ],
        },
        size: querySize,
        query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates),
        suggest: {
          'travel-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'name',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  { context: 'travel' },
                ],
              },
            },
          },
        },
      },
    ],
  });

export default spotsLocationsNewsAutoCompleteSearch;
