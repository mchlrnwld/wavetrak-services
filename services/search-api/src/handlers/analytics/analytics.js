import { analyticsLogger } from '../../common/logger';

const shipAnalytics = async (req, res) => {
  analyticsLogger(req.body.log);
  return res.send({ message: 'OK' });
};

export default shipAnalytics;
