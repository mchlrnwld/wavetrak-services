import elasticsearch from '../../model/elasticsearch';
import { matchPhrasePrefixAndScoreLocation } from '../../common/queries';

const DEFAULT_LOCATION = { lat: 33.6, lon: -117.9 }; // Huntington Beach

const spotsAndLocationsAutoCompleteSearch = (
  namePrefix,
  includeTaxonomyTypes,
  camsOnly,
  suggestionSize = 5,
  querySize = 5,
  offset = 0,
  coordinates = DEFAULT_LOCATION) => {
  const taxonomyTypeContexts =
    includeTaxonomyTypes ?
      includeTaxonomyTypes.split(',').map(type => ({ context: type })) :
      [];
  return {
    body: [
    { index: 'spots' },
      {
        _source: {
          includes: [
            'breadCrumbs',
            'name',
            'location',
            'cams',
            'text',
            'href',
            'thumbnail',
            'thumbnail_300',
            'thumbnail_640',
            'thumbnail_1500',
            'thumbnail_3000',
          ],
        },
        from: offset,
        size: querySize,
        query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates, camsOnly),
        suggest: {
          'spot-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'name',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  { context: 'spot' },
                ],
                location: [
                  { context: coordinates, boost: 2 },
                ],
              },
            },
          },
        },
      },
    { index: 'taxonomy', types: `subregion,${includeTaxonomyTypes}` },
      {
        _source: {
          includes: [
            'breadCrumbs',
            'name',
            'location',
            'text',
            'href',
            'type',
          ],
        },
        size: querySize,
        query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates),
        suggest: {
          'location-suggest': {
            prefix: namePrefix,
            completion: {
              field: 'name',
              size: suggestionSize,
              fuzzy: true,
              contexts: {
                type: [
                  ...taxonomyTypeContexts,
                  { context: 'subregion' },
                ],
              },
            },
          },
        },
      },
    ],
  };
};

/**
 * GET http://${service_host}:${service_port}/search/spots/?q=hunt&includeTaxonomyTypes=region,geoname
 *
 * Query results are in `hits`
 * Fuzzy suggestions are in `suggest`
 *
 * @param req
 *      query parameters:
 *          "q":  prefix to search for
 *                will use elasticsearch's completion suggester with default 'fuzzy' setting
 *          "includeTaxonomyTypes":  additional taxonomy types to include.
 *                Only subregion is included by default.
 *                Comma separated: 'includeTaxonomyTypes=region,geoname'
 *                Options: region, geoname, subregion (included by default)
 *          "suggestionSize":   defaults to 5, otherwise configurable
 *          "querySize":   defaults to 5, otherwise configurable
 * @param res
 *      Returns an array of responses in 'suggest' format. Take a swing at the API to get format.
 */
const getSpot = async (req, res) => {
  const {
    query: {
      q,
      includeTaxonomyTypes,
      camsOnly,
      suggestionSize = 5,
      querySize = 5,
      offset = 0,
    },
  } = req;
  const coordinates = req.geoLatitude && req.geoLongitude ? {
    lat: req.geoLatitude,
    lon: req.geoLongitude,
  } : undefined;
  const client = await elasticsearch.getClient();
  const result = await client.msearch(
    spotsAndLocationsAutoCompleteSearch(
      q,
      includeTaxonomyTypes,
      camsOnly && camsOnly.toUpperCase() === 'TRUE',
      suggestionSize,
      querySize,
      offset,
      coordinates,
    ));
  return res.send(result.responses);
};
export default getSpot;
