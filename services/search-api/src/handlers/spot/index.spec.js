import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import elasticsearch from '../../model/elasticsearch';
import getSpot from './index';
import sampleResponse from './sample_response.json';

const expect = chai.expect;
chai.should();
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('spot search handler', () => {
  let getClientStub;
  let getClientPromise;

  beforeEach(() => {
    elasticsearch.getClient = () => ({ msearch: () => {} });
    getClientStub = sinon.stub(elasticsearch, 'getClient');
    getClientPromise = getClientStub.returnsPromise();
  });

  afterEach(() => {
    getClientStub.restore();
  });

  it('should return the expected response for spot search', (done) => {
    getSpot(
      { query: { q: 'hun' } },
      { send: () => sampleResponse },
    )
      .then((res) => {
        expect(res).to.equal(sampleResponse);
        done();
      });

    getClientPromise.resolves({ msearch: () => sampleResponse });
  });
});
