import elasticsearch from '../../model/elasticsearch';

const taxonomyByIdSearch = (taxonomyIds = []) => ({
  index: 'taxonomy',
  body: {
    query: {
      bool: {
        filter: {
          terms: {
            _id: taxonomyIds,
          },
        },
      },
    },
  },
});

/**
 * GET http://${service_host}:${service_port}/search/taxonomy?ids=58f7f288dadb30820bba27e7,58f834c4dadb30820bf1b8c9
 *
 * @param req
 *      query parameters:
 *          "ids":  taxonomy ids to return.
 *                Comma separated: 'ids=58f7f288dadb30820bba27e7,58f834c4dadb30820bf1b8c9'
 * @param res
 *      Returns response in 'search' format. Take a swing at the API to get format.
 */
const getTaxonomy = async (req, res) => {
  const { query: { ids } } = req;
  const taxonomyIds = ids ? ids.split(',') : [];
  const client = await elasticsearch.getClient();
  const result = await client.search(taxonomyByIdSearch(taxonomyIds));
  return res.send(result);
};

export default getTaxonomy;
