import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import elasticsearch from '../../model/elasticsearch';
import sampleResponse from './sample_response.json';
import expectedResponse from './expected_response.json';
import app from '../../server/index';

const expect = chai.expect;
chai.should();
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('mobile spots search handler', () => {
  beforeEach(() => {
    elasticsearch.getClient = () => ({ search: () => {} });
    sinon.stub(elasticsearch, 'getClient');
    elasticsearch.getClient.resolves({ search: () => sampleResponse });
  });

  afterEach(() => {
    elasticsearch.getClient.restore();
  });

  it('should return the expected response for mobile spot search', (done) => {
    chai
      .request(app())
      .get(
        '/search/mobile/spots/?q=Gold%20Beach%20South%20Jetty&includeTaxonomyTypes=geoname,subregion&querySize=2&suggestionSize=2&offset=1'
      )
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal(expectedResponse);
        done();
      });
  });
});
