import { flatten, unionBy, pick } from 'lodash';
import elasticsearch from '../../model/elasticsearch';
import { matchPhrasePrefixAndScoreLocation } from '../../common/queries';

const DEFAULT_LOCATION = { lat: 33.6, lon: -117.9 }; // Huntington Beach

const spotsSearchForMobile = (
  namePrefix,
  includeTaxonomyTypes,
  suggestionSize = 5,
  querySize = 5,
  offset = 0,
  coordinates = DEFAULT_LOCATION) => ({
    index: 'spots',
    body:
    {
      _source: {
        includes: [
          'name',
          'location',
          'cams',
          'breadCrumbs',
        ],
      },
      from: offset,
      size: querySize,
      query: matchPhrasePrefixAndScoreLocation(namePrefix, coordinates),
      suggest: {
        'spot-suggest': {
          prefix: namePrefix,
          completion: {
            field: 'name',
            size: suggestionSize,
            fuzzy: true,
            contexts: {
              type: [
                { context: 'spot' },
              ],
            },
          },
        },
      },
    },
  });

/**
 * GET http://${service_host}:${service_port}/search/mobile/spots/?q=hunt&includeTaxonomyTypes=region,geoname
 *
 * Query results are in `hits`
 * Fuzzy suggestions are in `suggest`
 *
 * @param req
 *      query parameters:
 *          "q":  prefix to search for
 *                will use elasticsearch's completion suggester with default 'fuzzy' setting
 *          "includeTaxonomyTypes":  additional taxonomy types to include.
 *                Only subregion is included by default.
 *                Comma separated: 'includeTaxonomyTypes=region,geoname'
 *                Options: region, geoname, subregion (included by default)
 *          "suggestionSize":   defaults to 5, otherwise configurable
 *          "querySize":   defaults to 5, otherwise configurable
 * @param res
 *      Returns an array of responses in 'suggest' format. Take a swing at the API to get format.
 */

const getMobileSpots = async (req, res) => {
  /* eslint no-underscore-dangle: 0 */
  const { query: { q, includeTaxonomyTypes, suggestionSize = 5, querySize = 5, offset = 0 } } = req;
  const coordinates = req.geoLatitude && req.geoLongitude ? {
    lat: req.geoLatitude,
    lon: req.geoLongitude,
  } : undefined;
  const client = await elasticsearch.getClient();
  const result = await client.search(
    spotsSearchForMobile(
      q,
      includeTaxonomyTypes,
      suggestionSize,
      querySize,
      offset,
      coordinates,
    ));
  const spots = result.hits.hits;
  const suggestions = flatten(result.suggest['spot-suggest'].map(obj => obj.options));
  const combinedresult = unionBy(spots, suggestions, '_id');
  const fields = ['_type', '_id', '_source'];
  const filteredresult = flatten(combinedresult.map(obj => pick(obj, fields)));
  return res.send(filteredresult);
};

export default getMobileSpots;
