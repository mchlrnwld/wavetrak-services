import chai from 'chai';
import 'sinon-as-promised';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';

chai.use(chaiHttp);
chai.use(dirtyChai);
chai.use(sinonChai);
