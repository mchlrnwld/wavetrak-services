import bunyan from 'bunyan';
import Logsene from '@surfline/bunyan-logsene';
import PrettyStream from 'bunyan-prettystream';
import AWS from 'aws-sdk';

const kinesis = new AWS.Firehose({ region: 'us-west-2' });

const applicationLogseneStream = new Logsene({
  token: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63', // default to sandbox
});

const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);

const createLogger = ({ consoleLogLevel, logseneLevel, logseneStream }, name = 'default-js-logger') => {
  const streams = [
    {
      level: consoleLogLevel || 'debug',
      type: 'raw',
      stream: prettyStdOut,
    },
  ];

  if (logseneStream) {
    streams.push({
      level: logseneLevel || 'debug',
      stream: logseneStream,
      type: 'raw',
      reemitErrorEvents: true,
    });
  }

  const log = bunyan.createLogger({
    name,
    serializers: bunyan.stdSerializers,
    streams,
  });
  log.on('error', (err, stream) => {
    console.error('Problem communicating with logging server...');
    return console.error(stream);
  });
  log.trace(`${name} logging started.`);
  return log;
};

export const analyticsLogger = record =>
  kinesis.putRecord({
    DeliveryStreamName: process.env.SEARCH_ANALYTICS_STREAM,
    Record: {
      Data: `${JSON.stringify(record)}\n`,
    },
  }).promise();

export default (name = 'search-service') => createLogger({
  consoleLogLevel: process.env.CONSOLE_LOG_LEVEL || 'debug',
  logseneLevel: process.env.LOGSENE_LEVEL || 'debug',
  logseneStream: applicationLogseneStream,
}, name);
