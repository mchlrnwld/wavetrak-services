/* eslint-disable import/prefer-default-export */
export const matchPhrasePrefixAndScoreLocation = (phrase, origin, camsOnly) => {
  const query = {
    function_score: {
      query: {
        bool: {
          must: [
            {
              match_phrase_prefix: {
                name_standard: phrase,
              },
            },
          ],
        },
      },
      functions: [
        {
          filter: {
            bool: {
              filter: {
                exists: {
                  field: 'cams',
                },
              },
            },
          },
          weight: 2,
        },
        {
          exp: {
            location: {
              origin,
              offset: '20km',
              scale: '35km',
            },
          },
          weight: 4,
        },
      ],
      score_mode: 'sum',
    },
  };

  if (camsOnly) {
    query.function_score.query.bool.must.push({
      exists: {
        field: 'cams',
      },
    });
  }

  return query;
};

export const matchPhrasePrefixAndScoreFeed = (phrase) => {
  const query = {
    function_score: {
      query: {
        bool: {
          must: [
            {
              match_phrase_prefix: {
                'content.title': phrase,
              },
            },
          ],
          must_not: [
            { bool: {
              must: [{ term: { _type: 'forecast' } },
                {
                  range: {
                    createdAt: {
                      lte: 'now-7d/d',
                    },
                  },
                },
              ],
            },
            },
          ],
        },
      },
      functions: [
        {
          gauss: {
            createdAt: {
              origin: 'now',
              scale: '7d',
              decay: '0.9',
            },
          },
          weight: 100,
        },
        {
          filter: {
            match: { _type: 'editorial' },
          },
          weight: 4,
        },
        {
          filter: {
            match: { _type: 'forecast' },
          },
          weight: 1,
        },
        {
          filter: {
            match: { 'content.title': phrase },
          },
          weight: 4,
        },
        {
          filter: {
            match: { 'content.subtitle': phrase },
          },
          weight: 2,
        },
      ],
      score_mode: 'multiply',
    },
  };

  return query;
};
