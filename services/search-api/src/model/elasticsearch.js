import es from 'elasticsearch';
import AWS from 'aws-sdk';
import httpAwsEs from 'http-aws-es';
import logger from '../common/logger';

const log = logger('search-service:model:elasticsearch');

/*
Note: AWS_CONTAINER_CREDENTIALS_RELATIVE_URI is set automatically by the
ECS agent and used by the service to identify when running locally vs on ECS
*/
const isECS = !!process.env.AWS_CONTAINER_CREDENTIALS_RELATIVE_URI;

let savedCredentials = isECS ? new AWS.ECSCredentials() : new AWS.EnvironmentCredentials('AWS');
if (isECS) {
  savedCredentials.get();
}

const initializeClient = async (credentials) => {
  savedCredentials = await credentials.refreshPromise() || credentials;
  const extraConfiguration = {
    connectionClass: httpAwsEs,
    amazonES: {
      region: process.env.AWS_DEFAULT_REGION || 'us-west-1',
      credentials: savedCredentials,
    },
  };

  return new es.Client({
    host: process.env.ELASTIC_SEARCH_CONNECTION_STRING,
    log: 'info',
    ...extraConfiguration,
  });
};

let elasticsearchClient;
initializeClient(savedCredentials).then(
  (client) => {
    elasticsearchClient = client;
  },
);
const elasticsearch = {
  getClient: async () => {
    if (isECS && savedCredentials.needsRefresh()) {
      log.warn('Elasticsearch credential refresh');
      savedCredentials.get();
      elasticsearchClient = await initializeClient(savedCredentials);
    }
    return elasticsearchClient;
  },
};

export default elasticsearch;
