# Surfline Search Micro-service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [QuickStart](#quickstart)
- [Spot Search](#spot-search)
  - [Request Headers](#request-headers)
  - [Request Query Parameters](#request-query-parameters)
  - [Sample Request](#sample-request)
- [Site Search](#site-search)
  - [Request Query Parameters](#request-query-parameters-1)
  - [Sample Request](#sample-request-1)
- [Taxonomy Search](#taxonomy-search)
  - [Request Query Parameters](#request-query-parameters-2)
  - [Sample Request](#sample-request-2)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


### QuickStart
 1. `nvm use` (honors `.nvmrc`)
 2. `npm i`
 3. `cat .env.sample >> .env`. Fill out `AWS_SECRET_ACCESS_KEY` and `AWS_ACCESS_KEY_ID` for dev.
 4. `npm run startlocal`
 5. Check health [localhost:8082/health](http://localhost:8082/health)
 6. Or, try a sample query: `localhost:8082/search/site?q=hunt&querySize=10&suggestionSize=10`

### Spot Search

Searches spots by name. Spots are scored by distance to user's location with a boost for spots with cameras and returned sorted by score.

#### Request Headers

| Header          | Description                                                                          | Example   | Required |
| --------------- | ------------------------------------------------------------------------------------ | --------- | -------- |
| X-Geo-Latitude  | User's latitude. Results are scored based on distance from user. Defaults to 33.6.   | 19.386673 | No       |
| X-Geo-Longitude | User's longitude. Results are scored based on distance from user. Defaults to -117.9 | 79.090637 | No       |

#### Request Query Parameters

| Parameters           | Type   | Description                                                                                                                               | Example        | Required |
| -------------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------- | -------------- | -------- |
| q                    | string | Prefix to search for, will use Elasticsearch's completion suggester with default 'fuzzy setting'                                          | hunt           | Yes      |
| includeTaxonomyTypes | string | Additional taxonomy types to include. Only `subregion` is included by default. Comma separated. Options: `region`, `geoname`, `subregion` | region,geoname | No       |
| suggestionSize       | int    | Number of suggestions to return. Defaults to 5.                                                                                           | 10             | No       |
| querySize            | int    | Number of query results to return. Defaults to 5.                                                                                         | 10             | No       |
| offset               | int    | Number of query results to skip. Defaults to 0.                                                                                           | 1              | No       |

#### Sample Request

```
HTTP/1.1 GET /search/spots/?q=hunt&includeTaxonomyTypes=geoname,subregion&querySize=2&suggestionSize=2&offset=1
X-Geo-Latitude 19.386673
X-Geo-Longitude 79.090637
```

### Site Search

Searches Surfline pages by name. Includes spot, subregion, and taxonomy pages. Spots are scored by distance to user's location with a boost for spots with cameras and returned sorted by score.

| Header          | Description                                                                          | Example   | Required |
| --------------- | ------------------------------------------------------------------------------------ | --------- | -------- |
| X-Geo-Latitude  | User's latitude. Results are scored based on distance from user. Defaults to 33.6.   | 19.386673 | No       |
| X-Geo-Longitude | User's longitude. Results are scored based on distance from user. Defaults to -117.9 | 79.090637 | No       |

#### Request Query Parameters

| Parameters           | Type   | Description                                                                                                                               | Example        | Required |
| -------------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------- | -------------- | -------- |
| q                    | string | Prefix to search for, will use Elasticsearch's completion suggester with default 'fuzzy setting'                                          | hunt           | Yes      |
| includeTaxonomyTypes | string | Additional taxonomy types to include. Only `subregion` is included by default. Comma separated. Options: `region`, `geoname`, `subregion` | region,geoname | No       |
| suggestionSize       | int    | Number of suggestions to return. Defaults to 5.                                                                                           | 10             | No       |
| querySize            | int    | Number of query results to return. Defaults to 5.                                                                                         | 10             | No       |

#### Sample Request

```
HTTP/1.1 GET /search/site/?q=hunt&includeTaxonomyTypes=geoname,subregion&querySize=2&suggestionSize=2
X-Geo-Latitude 19.386673
X-Geo-Longitude 79.090637
```

### Taxonomy Search

#### Request Query Parameters

| Parameters | Type   | Description                   | Example                                           | Required |
| ---------- | ------ | ----------------------------- | ------------------------------------------------- | -------- |
| ids        | string | Comma separated taxonomy IDs. | 58f7f288dadb30820bba27e7,58f834c4dadb30820bf1b8c9 | Yes      |

#### Sample Request

```
HTTP/1.1 GET /search/taxonomy?ids=58f7f288dadb30820bba27e7,58f834c4dadb30820bf1b8c9
```

### Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `search-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTVRJNE5ERTRPUSJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME1USTROREU0T1EiLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Sample Requests](#sample-request) section. 

**Ex:**

Test `GET` requests to the `/search` endpoint:


```
curl \
    -X GET \
    -i \
    http://search-service.staging.surfline.com/search/site?q=walcott
```
**Expected response:** The response should look something like:
```
[
  {
    "took": 3,
    "timed_out": false,
    "_shards": {
      "total": 5,
      "successful": 5,
      "failed": 0
    },
    "hits": {
      "total": 0,
      "max_score": null,
      "hits": []
    },
    "suggest": { 
      "spot-suggest": [
        {
          "text": "walcott",
          "offset": 0,
          "length": 7,
          "options": []
        }
      ]
    },
    "status": 200
  },
  {
    "took": 1,
    "timed_out": false,
    "_shards": {
      "total": 5,
      "successful": 5,
      "failed": 0
    },
    "hits": {
      "total": 0,
      "max_score": null,
      "hits": []
    },
    "suggest": {
      "subregion-suggest": [
        {
          "text": "walcott",
          "offset": 0,
          "length": 7,
          "options": []
        }
      ]
    },
    "status": 200
  },
  {
    "took": 2,
    "timed_out": false,
    "_shards": {
      "total": 5,
      "successful": 5,
      "failed": 0
    },
    "hits": {
      "total": 0,
      "max_score": null,
      "hits": []
    },
    "suggest": {
      "geoname-suggest": [
        {
          "text": "walcott",
          "offset": 0,
          "length": 7,
          "options": [
            {
              "text": "Wolcott",
              "_index": "taxonomy",
              "_type": "geoname",
              "_id": "58f809dfdadb30820bd05927",
              "_score": 2,
              "_source": {
                "breadCrumbs": [
                  "United States",
                  "New York",
                  "Wayne County"
                ],
                "name": "Wolcott",
                "location": {
                  "lon": -76.81496,
                  "lat": 43.22062
                },
                "href": "https://staging.surfline.com/surf-reports-forecasts-cams/united-states/new-york/wayne-county/wolcott/5144861"
              },
              "contexts": {
                "type": [
                  "geoname"
                ]
              }
            }
          ]
        }
      ]
    },
    "status": 200
  },
  {
    "took": 37,
    "timed_out": false,
    "_shards": {
      "total": 5,
      "successful": 5,
      "failed": 0
    },
    "hits": {
      "total": 0,
      "max_score": null,
      "hits": []
    },
    "suggest": {
      "feed-tag-suggest": [
        {
          "text": "walcott",
          "offset": 0,
          "length": 7,
          "options": []
        }
      ]
    },
    "status": 200
  },
  {
    "took": 37,
    "timed_out": false,
    "_shards": {
      "total": 5,
      "successful": 5,
      "failed": 0
    },
    "hits": {
      "total": 0,
      "max_score": null,
      "hits": []
    },
    "suggest": {
      "travel-suggest": [
        {
          "text": "walcott",
          "offset": 0,
          "length": 7,
          "options": [
            {
              "text": "Wolcott",
              "_index": "taxonomy",
              "_type": "travel",
              "_id": "58f809dfdadb30820bd05927",
              "_score": 2,
              "_source": {
                "breadCrumbs": [
                  "United States",
                  "New York",
                  "Wayne County"
                ],
                "name": "Wolcott",
                "location": {
                  "lon": -76.81496,
                  "lat": 43.22062
                },
                "href": "https://staging.surfline.com/travel/united-states/new-york/wayne-county/wolcott-surfing-and-beaches/5144861"
              },
              "contexts": {
                "type": [
                  "travel"
                ]
              }
            }
          ]
        }
      ]
    },
    "status": 200
  }
]
```
