provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/search/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-staging"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
}

locals {
  dns_name          = "search-service.staging.surfline.com"
  elasticsearch_arn = "arn:aws:es:us-west-1:665294954271:domain/sl-es-staging"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/search*"
    },
    {
      field = "host-header"
      value = "internal-search.staging.surfline.com"
    },
  ]
}

module "search" {
  source = "../../"

  company     = "sl"
  application = "search-service"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  elasticsearch_arn = local.elasticsearch_arn
  iam_role_arn      = local.iam_role_arn
  load_balancer_arn = data.aws_alb.main_internal.arn

  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled = false
}
