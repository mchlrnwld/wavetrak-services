provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/search/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-prod"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
}

locals {
  dns_name          = "search-service.prod.surfline.com"
  elasticsearch_arn = "arn:aws:es:us-west-1:833713747344:domain/sl-es-prod"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/search*"
    },
    {
      field = "host-header"
      value = "internal-search.surfline.com"
    },
  ]
}

module "search" {
  source = "../../"

  company     = "sl"
  application = "search-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 10
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  elasticsearch_arn = local.elasticsearch_arn
  iam_role_arn      = local.iam_role_arn
  load_balancer_arn = data.aws_alb.main_internal.arn

  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 100
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}
