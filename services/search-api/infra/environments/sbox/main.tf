provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/search/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-sandbox"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
}

locals {
  dns_name          = "search-service.sandbox.surfline.com"
  elasticsearch_arn = "arn:aws:es:us-west-1:665294954271:domain/sl-es-sbox"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/search*"
    },
    {
      field = "host-header"
      value = "internal-search.sandbox.surfline.com"
    },
  ]
}

module "search" {
  source = "../../"

  company     = "sl"
  application = "search-service"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  elasticsearch_arn = local.elasticsearch_arn
  iam_role_arn      = local.iam_role_arn
  load_balancer_arn = data.aws_alb.main_internal.arn

  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled = false
}
