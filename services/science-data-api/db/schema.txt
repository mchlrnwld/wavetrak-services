// https://dbdiagram.io/d/5cbe094df7c5bb70c72fb6e6

table models {
  agency varchar(25) [pk]
  model varchar(25) [pk]
  created_at timestamp
  updated_at timestamp
}

table model_grids {
  agency varchar(25) [pk, ref: > models.agency]
  model varchar(25) [pk, ref: > models.model]
  grid varchar(25) [pk]
  created_at timestamp
  updated_at timestamp
}

table model_grid_points {
  agency varchar(25) [pk, ref: > model_grids.agency]
  model varchar(25) [pk, ref: > model_grids.model]
  grid varchar(25) [pk, ref: > model_grids.grid]
  latitude float [pk]
  longitude float [pk]
  geography_point geography(point)
  created_at timestamp
  updated_at timestamp
}

table model_runs {
  agency varchar(25) [pk, ref: > models.agency]
  model varchar(25) [pk, ref: > models.model]
  run timestamp [pk]
  status enum [note: "OFFLINE, PENDING, or ONLINE"]
  created_at timestamp
  updated_at timestamp
}

table points_of_interest {
  id UUID [pk]
  name text
  latitude float [unique, note: "Unique in combination with longitude"]
  longitude float [unique, note: "Unique in combination with latitude"]
  created_at timestamp
  updated_at timestamp
}

// Configurations specific to surf spot grid points.
table surf_spot_grid_point_configurations {
  point_of_interest_id UUID [pk, ref: > points_of_interest_grid_points.point_of_interest_id]
  agency varchar(25) [pk, ref: > points_of_interest_grid_points.agency]
  model varchar(25) [pk, ref: > points_of_interest_grid_points.model]
  grid varchar(25) [pk, ref: > points_of_interest_grid_points.grid]
  offshore_direction numeric
  optimal_swell_direction numeric
  breaking_wave_height_coefficient numeric
  breaking_wave_height_intercept numeric
  spectral_refraction_matrix text
  breaking_wave_height_algorithm enum
  created_at timestamp
  updated_at timestamp
}

// Mapping of points of interest to model grid points.
table points_of_interest_grid_points {
  point_of_interest_id UUID [pk, ref: > points_of_interest.id]
  agency varchar(25) [pk, ref: > model_grids.agency]
  model varchar(25) [pk, ref: > model_grids.model]
  grid varchar(25) [pk, ref: > model_grids.grid]
  longitude float
  latitude float
  created_at timestamp
  updated_at timestamp
}

// swells, weather, wind, and surf have latitude and longitude columns that store a snapshot of the grid point used for the point of interest at a point in history.
table swells {
  point_of_interest_id int [pk, ref: > points_of_interest.id]
  agency varchar(25) [pk, ref: > model_grid_points.agency]
  model varchar(25) [pk, ref: > model_grid_points.model]
  grid varchar(25) [pk, ref: > model_grid_points.grid]
  latitude float [pk, ref: > model_grid_points.latitude]
  longitude float [pk, ref: > model_grid_points.longitude]
  run timestamp [pk, ref: > model_runs.run]
  forecast_time timestamp [pk]
  height numeric
  period numeric
  direction numeric
  swell_wave_1_height numeric
  swell_wave_1_period numeric
  swell_wave_1_direction numeric
  swell_wave_1_spread numeric
  swell_wave_2_height numeric
  swell_wave_2_period numeric
  swell_wave_2_direction numeric
  swell_wave_2_spread numeric
  swell_wave_3_height numeric
  swell_wave_3_period numeric
  swell_wave_3_direction numeric
  swell_wave_3_spread numeric
  swell_wave_4_height numeric
  swell_wave_4_period numeric
  swell_wave_4_direction numeric
  swell_wave_4_spread numeric
  swell_wave_5_height numeric
  swell_wave_5_period numeric
  swell_wave_5_direction numeric
  swell_wave_5_spread numeric
  wind_wave_height numeric
  wind_wave_period numeric
  wind_wave_direction numeric
  wind_wave_spread numeric
  created_at timestamp
  updated_at timestamp
}

table weather {
  point_of_interest_id UUID [pk, ref: > points_of_interest.id]
  agency varchar(25) [pk, ref: > model_grids.agency]
  model varchar(25) [pk, ref: > model_grids.model]
  grid varchar(25) [pk, ref: > model_grids.grid]
  latitude float
  longitude float
  run timestamp [pk, ref: > model_runs.run]
  forecast_time timestamp [pk]
  wind_u numeric
  wind_v numeric
  wind_gust numeric
  temperature numeric
  dewpoint numeric
  visibility numeric
  humidity numeric
  pressure numeric
  precipitation_volume numeric
  precipitation_type enum [note: "See https://github.com/Surfline/wavetrak-services/blob/master/common/smdb/pkg/schemas/weather.go#L7-L11"]
  weather_conditions enum [note: "See https://github.com/Surfline/wavetrak-services/blob/master/common/smdb/pkg/schemas/weather.go#L35-L79"]
  created_at timestamp
  updated_at timestamp
}

table surf {
  point_of_interest_id int [pk, ref: > points_of_interest.id]
  agency varchar(25) [pk, ref: > model_grid_points.agency]
  model varchar(25) [pk, ref: > model_grid_points.model]
  grid varchar(25) [pk, ref: > model_grid_points.grid]
  latitude float [pk, ref: > model_grid_points.latitude]
  longitude float [pk, ref: > model_grid_points.longitude]
  run timestamp [pk, ref: > model_runs.run]
  forecast_time timestamp [pk]
  breaking_wave_height_algorithm enum
  breaking_wave_height_min float
  breaking_wave_height_max float
  breaking_wave_height_occasional float
  rating int
  wave_energy numeric_29
  wave_direction numeric_29
  created_at timestamp
  updated_at timestamp
}
