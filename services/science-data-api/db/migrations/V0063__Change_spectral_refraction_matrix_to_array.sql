ALTER TABLE surf_spot_grid_point_configurations
ALTER COLUMN spectral_refraction_matrix TYPE numeric[24][18] USING spectral_refraction_matrix::numeric[24][18];
