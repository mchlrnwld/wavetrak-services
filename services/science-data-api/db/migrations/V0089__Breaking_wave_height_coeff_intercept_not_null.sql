ALTER TABLE surf_spot_configurations ALTER COLUMN breaking_wave_height_coefficient SET DEFAULT 1.0;
ALTER TABLE surf_spot_configurations ALTER COLUMN breaking_wave_height_coefficient SET NOT NULL;
ALTER TABLE surf_spot_configurations ALTER COLUMN breaking_wave_height_intercept SET DEFAULT 0.0;
ALTER TABLE surf_spot_configurations ALTER COLUMN breaking_wave_height_intercept SET NOT NULL;
