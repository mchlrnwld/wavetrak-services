SET ROLE public_owner;

CREATE TABLE surf_spots (
    points_of_interest_id            UUID      NOT NULL PRIMARY KEY REFERENCES points_of_interest (id),
    offshore_direction               NUMERIC,
    breaking_wave_height_coefficient NUMERIC,
    created_at                       TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at                       TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER surf_spots_updated_at
BEFORE UPDATE ON surf_spots
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
