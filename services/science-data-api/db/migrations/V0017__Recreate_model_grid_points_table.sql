SET ROLE public_owner;

CREATE TABLE model_grid_points (
    agency          VARCHAR(25)      NOT NULL,
    model           VARCHAR(25)      NOT NULL,
    grid            VARCHAR(25)      NOT NULL,
    longitude       FLOAT            NOT NULL,
    latitude        FLOAT            NOT NULL,
    geography_point GEOGRAPHY(POINT) NOT NULL,
    PRIMARY KEY (agency, model, grid, longitude, latitude),
    FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid)
);

CREATE INDEX grid_idx
ON model_grid_points (agency, model, grid)
WITH (FILLFACTOR=99);

ALTER TABLE model_grid_points CLUSTER ON grid_idx;

CREATE INDEX geography_point_idx
ON model_grid_points
USING GIST (geography_point);

CREATE FUNCTION set_geography_point()
RETURNS TRIGGER AS $$
BEGIN
    NEW.geography_point := ST_POINT(NEW.longitude, NEW.latitude);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_geography_point
BEFORE INSERT OR UPDATE OF latitude, longitude ON model_grid_points
FOR EACH ROW
EXECUTE PROCEDURE set_geography_point();
