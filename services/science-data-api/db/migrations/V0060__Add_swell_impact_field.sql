ALTER TABLE swells
ADD COLUMN swell_wave_1_impact NUMERIC NULL,
ADD COLUMN swell_wave_2_impact NUMERIC NULL,
ADD COLUMN swell_wave_3_impact NUMERIC NULL,
ADD COLUMN swell_wave_4_impact NUMERIC NULL,
ADD COLUMN swell_wave_5_impact NUMERIC NULL,
ADD COLUMN wind_wave_impact NUMERIC NULL;
