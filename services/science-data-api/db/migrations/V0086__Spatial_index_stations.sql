CREATE INDEX stations_geography_point_idx
    ON stations
    USING GIST (geography_point);
