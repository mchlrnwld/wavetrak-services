SET ROLE public_owner;

-- surf table
CREATE TABLE surf (
    point_of_interest_id UUID        NOT NULL,
    agency               VARCHAR(25) NOT NULL,
    model                VARCHAR(25) NOT NULL,
    grid                 VARCHAR(25) NOT NULL,
    latitude             FLOAT       NOT NULL,
    longitude            FLOAT       NOT NULL,
    run                  TIMESTAMP   NOT NULL,
    forecast_time        TIMESTAMP   NOT NULL,
    wave_energy          NUMERIC[29] NOT NULL,
    wave_direction       NUMERIC[29] NOT NULL,
    created_at           TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at           TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER surf_updated_at
BEFORE UPDATE ON surf
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
