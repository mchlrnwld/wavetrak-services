SET ROLE public_owner;

CREATE TYPE product AS ENUM (
    'SL',
    'MSW'
);

ALTER TABLE points_of_interest ADD COLUMN product PRODUCT;

CREATE INDEX product_idx ON points_of_interest (product);
