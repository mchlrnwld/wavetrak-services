SET ROLE public_owner;

-- models table
CREATE TABLE models (
    agency     VARCHAR(25) NOT NULL,
    model      VARCHAR(25) NOT NULL,
    created_at TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (agency, model)
);

CREATE TRIGGER models_updated_at
BEFORE UPDATE ON models
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

-- model_grids table
CREATE TABLE model_grids (
    agency     VARCHAR(25) NOT NULL,
    model      VARCHAR(25) NOT NULL,
    grid       VARCHAR(25) NOT NULL,
    created_at TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (agency, model, grid),
    FOREIGN KEY (agency, model) REFERENCES models (agency, model)
);

CREATE TRIGGER model_grids_updated_at
BEFORE UPDATE ON model_grids
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

-- model_runs table
DROP TABLE IF EXISTS model_runs;
CREATE TABLE model_runs (
    agency     VARCHAR(25)      NOT NULL,
    model      VARCHAR(25)      NOT NULL,
    run        TIMESTAMP        NOT NULL,
    status     model_run_status NOT NULL DEFAULT 'PENDING',
    created_at TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (agency, model, run),
    FOREIGN KEY (agency, model) REFERENCES models (agency, model)
);

CREATE TRIGGER model_runs_updated_at
BEFORE UPDATE ON model_runs
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

CREATE INDEX status_idx ON model_runs (status);
