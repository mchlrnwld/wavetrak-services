ALTER TABLE surf DROP CONSTRAINT surf_pkey;
ALTER TABLE surf ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run);

ALTER TABLE swells DROP CONSTRAINT swells_pkey;
ALTER TABLE swells ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run);

ALTER TABLE weather DROP CONSTRAINT weather_pkey;
ALTER TABLE weather ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run);
