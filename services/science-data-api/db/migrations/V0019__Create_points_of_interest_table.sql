SET ROLE public_owner;

-- points of interest table
CREATE TABLE points_of_interest(
    id         INT         NOT NULL PRIMARY KEY,
    name       VARCHAR(25) NOT NULL,
    latitude   FLOAT       NOT NULL,
    longitude  FLOAT       NOT NULL,
    created_at TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (latitude, longitude)
);

CREATE TRIGGER points_of_interest_updated_at
BEFORE UPDATE ON points_of_interest
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
