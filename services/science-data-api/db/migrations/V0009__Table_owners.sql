CREATE ROLE public_owner;

GRANT public_owner to developer;
GRANT public_owner to admin WITH ADMIN OPTION;

ALTER TABLE model_runs OWNER to public_owner;
