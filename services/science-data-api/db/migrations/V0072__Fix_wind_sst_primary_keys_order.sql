ALTER TABLE sst DROP CONSTRAINT sst_pkey;
ALTER TABLE sst ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run);

ALTER TABLE wind DROP CONSTRAINT wind_pkey;
ALTER TABLE wind ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run);
