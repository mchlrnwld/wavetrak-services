SET ROLE public_owner;

CREATE TABLE model_runs_point_of_interest (
    agency     VARCHAR(25)                         NOT NULL,
    model      VARCHAR(25)                         NOT NULL,
    run        TIMESTAMP                           NOT NULL,
    status     model_run_status DEFAULT 'PENDING'  NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (agency, model, run),
    FOREIGN KEY (agency, model) REFERENCES models (agency, model)
);

CREATE INDEX model_runs_point_of_interest_status_idx ON model_runs_point_of_interest (status);

CREATE TRIGGER model_runs_point_of_interest_updated_at
BEFORE UPDATE ON model_runs_point_of_interest
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
