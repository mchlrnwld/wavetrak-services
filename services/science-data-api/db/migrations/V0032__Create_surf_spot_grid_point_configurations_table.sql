SET ROLE public_owner;

CREATE TYPE breaking_wave_height_algorithm AS ENUM ('POLYNOMIAL');

CREATE TABLE surf_spot_grid_point_configurations (
    point_of_interest_id             UUID                           NOT NULL,
    agency                           VARCHAR(25)                    NOT NULL,
    model                            VARCHAR(25)                    NOT NULL,
    grid                             VARCHAR(25)                    NOT NULL,
    longitude                        FLOAT                          NOT NULL,
    latitude                         FLOAT                          NOT NULL,
    offshore_direction               NUMERIC                        NOT NULL,
    optimal_swell_direction          NUMERIC,
    breaking_wave_height_coefficient NUMERIC,
    breaking_wave_height_intercept   NUMERIC,
    spectral_refraction_matrix       TEXT,
    breaking_wave_height_algorithm   BREAKING_WAVE_HEIGHT_ALGORITHM NOT NULL,
    created_at                       TIMESTAMP                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at                       TIMESTAMP                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, agency, model, grid, longitude, latitude),
    CONSTRAINT surf_spot_grid_point_configurations_point_of_interest_id_fkey FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id),
    CONSTRAINT surf_spot_grid_point_configurations_agency_fkey FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid)
);

CREATE TRIGGER surf_spot_grid_point_configurations_at
BEFORE UPDATE ON surf_spot_grid_point_configurations
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
