SET ROLE public_owner;

CREATE TYPE rating AS ENUM ('VERY_POOR', 'POOR', 'POOR_TO_FAIR', 'FAIR', 'GOOD');

CREATE TABLE reported_ratings (
    point_of_interest_id   UUID        NOT NULL,
    forecast_time          TIMESTAMP   NOT NULL,
    rating                 RATING      NOT NULL,
    created_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, forecast_time),
    FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id)
);

CREATE TRIGGER reported_ratings_updated_at
BEFORE UPDATE ON reported_ratings
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
