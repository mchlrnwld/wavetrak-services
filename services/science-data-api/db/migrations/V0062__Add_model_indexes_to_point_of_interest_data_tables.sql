CREATE INDEX IF NOT EXISTS sst_agency_model_run_idx ON sst (agency, model, run DESC);

CREATE INDEX IF NOT EXISTS surf_agency_model_run_idx ON surf (agency, model, run DESC);

CREATE INDEX IF NOT EXISTS swell_ensemble_members_agency_model_run_idx ON swell_ensemble_members (agency, model, run DESC);

CREATE INDEX IF NOT EXISTS swell_probabilities_agency_model_run_idx ON swell_probabilities (agency, model, run DESC);

CREATE INDEX IF NOT EXISTS swells_agency_model_run_idx ON swells (agency, model, run DESC);

CREATE INDEX IF NOT EXISTS weather_agency_model_run_idx ON weather (agency, model, run DESC);

CREATE INDEX IF NOT EXISTS wind_agency_model_run_idx ON wind (agency, model, run DESC);
