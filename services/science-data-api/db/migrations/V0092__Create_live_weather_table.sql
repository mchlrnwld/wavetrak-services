SET ROLE public_owner;

CREATE TABLE live_weather (
  sensor_id      TEXT        NOT NULL,
  reading_time   TIMESTAMP   NOT NULL,
  wind_speed     NUMERIC     NOT NULL,
  wind_direction NUMERIC     NOT NULL,
  gust_speed     NUMERIC     NOT NULL,
  created_at     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (sensor_id, reading_time)
);

CREATE TRIGGER live_weather_updated_at
BEFORE UPDATE ON live_weather
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
