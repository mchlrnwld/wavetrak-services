CREATE TYPE ratings_algorithm AS ENUM ('MATRIX', 'ML_ONLY_LOTUS');

ALTER TABLE surf_spot_configurations ADD COLUMN ratings_algorithm RATINGS_ALGORITHM DEFAULT 'MATRIX' NOT NULL;
