-- Drop unique constraint for the points_of_interest table

DO $$
DECLARE
   ukey RECORD;
BEGIN
    FOR ukey IN (
        SELECT table_name, constraint_name
        FROM information_schema.table_constraints
        WHERE 'UNIQUE'=constraint_type
            AND 'points_of_interest'=table_name
    ) LOOP
        EXECUTE format(
            'ALTER TABLE %I DROP CONSTRAINT %I;',
            ukey.table_name,
            ukey.constraint_name
        );
    END LOOP;
END; $$;
