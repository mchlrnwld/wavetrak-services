SET ROLE public_owner;

CREATE TABLE swell_probabilities (
    point_of_interest_id UUID        NOT NULL,
    agency               VARCHAR(25) NOT NULL,
    model                VARCHAR(25) NOT NULL,
    grid                 VARCHAR(25) NOT NULL,
    latitude             FLOAT       NOT NULL,
    longitude            FLOAT       NOT NULL,
    run                  TIMESTAMP   NOT NULL,
    forecast_time        TIMESTAMP   NOT NULL,
    probability          NUMERIC     NOT NULL,
    created_at           TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at           TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run),
    FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id),
    FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid),
    FOREIGN KEY (agency, model, run) REFERENCES model_runs (agency, model, run)
);

CREATE TRIGGER swell_probabilities_updated_at
BEFORE UPDATE ON swell_probabilities
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

CREATE TABLE swell_ensemble_members (
    point_of_interest_id   UUID        NOT NULL,
    agency                 VARCHAR(25) NOT NULL,
    model                  VARCHAR(25) NOT NULL,
    grid                   VARCHAR(25) NOT NULL,
    latitude               FLOAT       NOT NULL,
    longitude              FLOAT       NOT NULL,
    run                    TIMESTAMP   NOT NULL,
    forecast_time          TIMESTAMP   NOT NULL,
    member                 SMALLINT    NOT NULL,
    wind_u                 NUMERIC     NOT NULL,
    wind_v                 NUMERIC     NOT NULL,
    combined_wave_height   NUMERIC     NOT NULL,
    primary_wave_direction NUMERIC     NOT NULL,
    primary_wave_period    NUMERIC     NOT NULL,
    swell_wave_1_direction NUMERIC     NOT NULL,
    swell_wave_1_height    NUMERIC     NOT NULL,
    swell_wave_1_period    NUMERIC     NOT NULL,
    swell_wave_2_direction NUMERIC     NOT NULL,
    swell_wave_2_height    NUMERIC     NOT NULL,
    swell_wave_2_period    NUMERIC     NOT NULL,
    wind_wave_direction    NUMERIC     NOT NULL,
    wind_wave_height       NUMERIC     NOT NULL,
    wind_wave_period       NUMERIC     NOT NULL,
    created_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run),
    FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id),
    FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid),
    FOREIGN KEY (agency, model, run) REFERENCES model_runs (agency, model, run)
);

CREATE TRIGGER swell_ensemble_members_updated_at
BEFORE UPDATE ON swell_ensemble_members
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
