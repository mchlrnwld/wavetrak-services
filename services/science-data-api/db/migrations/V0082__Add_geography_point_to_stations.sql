ALTER TABLE stations
    ADD COLUMN geography_point geography(POINT, 4326);

UPDATE stations s
SET geography_point = ST_POINT(s.longitude, s.latitude)
WHERE geography_point IS NULL;

CREATE TRIGGER stations_update_geography_point
    BEFORE INSERT OR UPDATE OF latitude, longitude ON stations
    FOR EACH ROW
    EXECUTE PROCEDURE set_geography_point();
