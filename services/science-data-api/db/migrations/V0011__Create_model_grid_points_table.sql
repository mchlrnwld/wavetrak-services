CREATE TABLE model_grid_points (
    PRIMARY KEY (agency, model, grid, longitude, latitude),
    agency      VARCHAR(25)         NOT NULL,
    model       VARCHAR(25)         NOT NULL,
    grid        VARCHAR(25)         NOT NULL,
    longitude   FLOAT               NOT NULL,
    latitude    FLOAT               NOT NULL,
    grid_point  GEOGRAPHY(POINT)    NOT NULL
);

CREATE FUNCTION set_geography_grid_point()
RETURNS TRIGGER AS $$
BEGIN
    NEW.grid_point := ST_POINT(NEW.longitude, NEW.latitude);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_grid_point
    BEFORE INSERT OR UPDATE OF latitude, longitude ON model_grid_points
    FOR EACH ROW
    EXECUTE PROCEDURE set_geography_grid_point();
