SET ROLE public_owner;

ALTER TABLE points_of_interest_grid_points DROP CONSTRAINT points_of_interest_grid_points_model_run_grid_point_id_fkey;
ALTER TABLE points_of_interest_grid_points DROP CONSTRAINT points_of_interest_grid_points_pkey;

ALTER TABLE model_grid_points DROP CONSTRAINT model_grid_points_pkey;
ALTER TABLE model_grid_points DROP COLUMN IF EXISTS id;
ALTER TABLE model_grid_points ADD PRIMARY KEY (agency, model, grid, longitude, latitude);

ALTER TABLE points_of_interest_grid_points DROP COLUMN IF EXISTS model_run_grid_point_id;
ALTER TABLE points_of_interest_grid_points ADD COLUMN agency    VARCHAR(25) NOT NULL;
ALTER TABLE points_of_interest_grid_points ADD COLUMN model     VARCHAR(25) NOT NULL;
ALTER TABLE points_of_interest_grid_points ADD COLUMN grid      VARCHAR(25) NOT NULL;
ALTER TABLE points_of_interest_grid_points ADD COLUMN longitude FLOAT       NOT NULL;
ALTER TABLE points_of_interest_grid_points ADD COLUMN latitude  FLOAT       NOT NULL;

ALTER TABLE points_of_interest_grid_points ADD PRIMARY KEY (agency, model, grid, longitude, latitude, points_of_interest_id);
ALTER TABLE points_of_interest_grid_points ADD CONSTRAINT points_of_interest_grid_points_agency_fkey FOREIGN KEY (agency, model, grid, longitude, latitude) REFERENCES model_grid_points (agency, model, grid, longitude, latitude);

CREATE TYPE precipitation_type AS ENUM ('RAIN', 'ICE_PELLETS', 'FREEZING_RAIN', 'SNOW');

CREATE TYPE weather_conditions AS ENUM (
    'CLEAR',
    'MOSTLY CLEAR',
    'MOSTLY CLOUDY',
    'CLOUDY',
    'OVERCAST',
    'MIST',
    'FOG',
    'DRIZZLE',
    'BRIEF_SHOWERS_POSSIBLE',
    'BRIEF_SHOWERS',
    'LIGHT_SHOWERS_POSSIBLE',
    'LIGHT_SHOWERS',
    'SHOWERS',
    'HEAVY_SHOWERS_POSSIBLE',
    'HEAVY_SHOWERS',
    'LIGHT_RAIN',
    'LIGHT_RAIN_AND_FOG',
    'RAIN',
    'RAIN_AND_FOG',
    'HEAVY_RAIN',
    'TORRENTIAL_RAIN',
    'ICE_PELLETS',
    'BRIEF_ICE_PELLETS_SHOWERS',
    'ICE_PELLETS_SHOWERS',
    'FREEZING_RAIN',
    'BRIEF_FREEZING_RAIN_SHOWERS',
    'FREEZING_RAIN_SHOWERS',
    'LIGHT_SNOW',
    'SNOW',
    'HEAVY_SNOW',
    'BLIZZARD',
    'SEVERE_BLIZZARD',
    'BRIEF_SNOW_FLURRIES',
    'BRIEF_SNOW_SHOWERS',
    'LIGHT_SNOW_SHOWERS',
    'SNOW_SHOWERS',
    'BRIEF_HEAVY_SNOW_SHOWERS',
    'HEAVY_SNOW_SHOWERS',
    'SNOW_SQUALLS',
    'SEVERE_SNOW_SQUALLS',
    'THUNDER_SHOWERS',
    'HEAVY_THUNDER_SHOWERS',
    'THUNDER_STORMS',
    'HEAVY_THUNDER_STORMS',
    'SEVERE_THUNDER_STORMS'
);

-- weather table
CREATE TABLE weather (
    points_of_interest_id   UUID                NOT NULL,
    agency                  VARCHAR(25)         NOT NULL,
    model                   VARCHAR(25)         NOT NULL,
    grid                    VARCHAR(25)         NOT NULL,
    latitude                FLOAT               NOT NULL,
    longitude               FLOAT               NOT NULL,
    run                     TIMESTAMP           NOT NULL,
    forecast_time           TIMESTAMP           NOT NULL,
    wind_u                  NUMERIC             NOT NULL,
    wind_v                  NUMERIC             NOT NULL,
    wind_gust               NUMERIC             NOT NULL,
    temperature             NUMERIC             NOT NULL,
    dewpoint                NUMERIC             NOT NULL,
    visibility              NUMERIC             NOT NULL,
    humidity                NUMERIC             NOT NULL,
    pressure                NUMERIC             NOT NULL,
    precipitation_volume    NUMERIC             NOT NULL,
    precipitation_type      PRECIPITATION_TYPE,
    weather_conditions      WEATHER_CONDITIONS  NOT NULL,
    created_at              TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at              TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (points_of_interest_id, agency, model, grid, run, forecast_time),
    FOREIGN KEY (points_of_interest_id) REFERENCES points_of_interest (id),
    FOREIGN KEY (agency, model, run) REFERENCES model_runs (agency, model, run),
    FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid)
);

CREATE TRIGGER weather_updated_at
BEFORE UPDATE ON weather
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
