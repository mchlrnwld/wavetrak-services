SET ROLE public_owner;

CREATE TYPE forecast_type AS ENUM (
    'SWELLS',
    'WEATHER',
    'WIND'
);

CREATE TABLE points_of_interest_default_forecast_models (
    point_of_interest_id UUID          NOT NULL,
    forecast_type        forecast_type NOT NULL,
    agency               VARCHAR(25)   NOT NULL,
    model                VARCHAR(25)   NOT NULL,
    grid                 VARCHAR(25)   NOT NULL,
    created_at           TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at           TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, forecast_type),
    FOREIGN KEY (point_of_interest_id, agency, model, grid) REFERENCES points_of_interest_grid_points (point_of_interest_id, agency, model, grid)
);

CREATE TRIGGER points_of_interest_default_forecast_models_updated_at
BEFORE UPDATE ON points_of_interest_default_forecast_models
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
