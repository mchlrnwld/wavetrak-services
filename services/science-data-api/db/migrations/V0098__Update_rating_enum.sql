ALTER TYPE rating
RENAME TO old_rating;

CREATE TYPE rating AS ENUM (
    'VERY_POOR',
    'POOR',
    'POOR_TO_FAIR',
    'FAIR',
    'FAIR_TO_GOOD',
    'GOOD',
    'EPIC'
);

ALTER TABLE reported_ratings
ALTER COLUMN rating
TYPE rating
USING (
    CASE rating::TEXT
        WHEN 'GOOD' THEN 'GOOD'
        ELSE rating::TEXT
    END
)::rating;

DROP TYPE old_rating;
