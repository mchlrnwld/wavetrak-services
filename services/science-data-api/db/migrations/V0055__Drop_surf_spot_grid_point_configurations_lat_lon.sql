ALTER TABLE surf_spot_grid_point_configurations
    DROP COLUMN latitude,
    DROP COLUMN longitude;
