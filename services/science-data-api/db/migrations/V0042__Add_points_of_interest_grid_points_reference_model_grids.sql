ALTER TABLE points_of_interest_grid_points
ADD FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid);
