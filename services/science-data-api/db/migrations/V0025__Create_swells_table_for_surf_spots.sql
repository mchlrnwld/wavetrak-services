SET ROLE public_owner;

-- swells table
CREATE TABLE swells (
    point_of_interest_id   UUID        NOT NULL,
    agency                 VARCHAR(25) NOT NULL,
    model                  VARCHAR(25) NOT NULL,
    grid                   VARCHAR(25) NOT NULL,
    latitude               FLOAT       NOT NULL,
    longitude              FLOAT       NOT NULL,
    run                    TIMESTAMP   NOT NULL,
    forecast_time          TIMESTAMP   NOT NULL,
    height                 NUMERIC     NOT NULL,
    period                 NUMERIC     NOT NULL,
    direction              NUMERIC     NOT NULL,
    swell_wave_1_height    NUMERIC     NOT NULL,
    swell_wave_1_period    NUMERIC     NOT NULL,
    swell_wave_1_direction NUMERIC     NOT NULL,
    swell_wave_1_spread    NUMERIC     NOT NULL,
    swell_wave_2_height    NUMERIC     NOT NULL,
    swell_wave_2_period    NUMERIC     NOT NULL,
    swell_wave_2_direction NUMERIC     NOT NULL,
    swell_wave_2_spread    NUMERIC     NOT NULL,
    swell_wave_3_height    NUMERIC     NOT NULL,
    swell_wave_3_period    NUMERIC     NOT NULL,
    swell_wave_3_direction NUMERIC     NOT NULL,
    swell_wave_3_spread    NUMERIC     NOT NULL,
    swell_wave_4_height    NUMERIC     NOT NULL,
    swell_wave_4_period    NUMERIC     NOT NULL,
    swell_wave_4_direction NUMERIC     NOT NULL,
    swell_wave_4_spread    NUMERIC     NOT NULL,
    swell_wave_5_height    NUMERIC     NOT NULL,
    swell_wave_5_period    NUMERIC     NOT NULL,
    swell_wave_5_direction NUMERIC     NOT NULL,
    swell_wave_5_spread    NUMERIC     NOT NULL,
    wind_wave_height       NUMERIC     NOT NULL,
    wind_wave_period       NUMERIC     NOT NULL,
    wind_wave_direction    NUMERIC     NOT NULL,
    wind_wave_spread       NUMERIC     NOT NULL,
    created_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER swells_updated_at
BEFORE UPDATE ON swells
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

-- rename points_of_interest_id to point_of_interest_id
ALTER TABLE surf_spots
RENAME COLUMN points_of_interest_id TO point_of_interest_id;

ALTER TABLE points_of_interest_grid_points
RENAME COLUMN points_of_interest_id TO point_of_interest_id;

ALTER TABLE weather
RENAME COLUMN points_of_interest_id TO point_of_interest_id;
