ALTER TABLE surf ADD COLUMN breaking_wave_height_min NUMERIC NOT NULL;
ALTER TABLE surf ADD COLUMN breaking_wave_height_max NUMERIC NOT NULL;
ALTER TABLE surf ADD COLUMN breaking_wave_height_algorithm breaking_wave_height_algorithm NOT NULL;
