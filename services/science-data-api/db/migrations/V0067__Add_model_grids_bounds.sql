ALTER TABLE model_grids
ADD COLUMN start_latitude NUMERIC;

ALTER TABLE model_grids
ADD COLUMN end_latitude NUMERIC;

ALTER TABLE model_grids
ADD COLUMN start_longitude NUMERIC;

ALTER TABLE model_grids
ADD COLUMN end_longitude NUMERIC;
