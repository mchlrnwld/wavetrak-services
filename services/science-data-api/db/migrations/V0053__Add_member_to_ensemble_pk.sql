ALTER TABLE swell_ensemble_members
DROP CONSTRAINT swell_ensemble_members_pkey;

ALTER TABLE swell_ensemble_members
ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, forecast_time, run, member);
