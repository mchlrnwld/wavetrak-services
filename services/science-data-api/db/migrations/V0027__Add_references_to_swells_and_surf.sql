ALTER TABLE swells
ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, run, forecast_time),
ADD FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id),
ADD FOREIGN KEY (agency, model, run) REFERENCES model_runs (agency, model, run),
ADD FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid);

ALTER TABLE surf
ADD PRIMARY KEY (point_of_interest_id, agency, model, grid, run, forecast_time),
ADD FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id),
ADD FOREIGN KEY (agency, model, run) REFERENCES model_runs (agency, model, run),
ADD FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid);
