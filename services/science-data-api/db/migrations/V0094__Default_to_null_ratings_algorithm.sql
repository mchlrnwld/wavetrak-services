ALTER TABLE surf_spot_configurations ALTER COLUMN ratings_algorithm DROP NOT NULL;
ALTER TABLE surf_spot_configurations ALTER COLUMN ratings_algorithm SET DEFAULT NULL;
