ALTER TABLE swells
ADD COLUMN spectra1d_energy             NUMERIC[29],
ADD COLUMN spectra1d_direction          NUMERIC[29],
ADD COLUMN spectra1d_directional_spread NUMERIC[29];
