SET ROLE public_owner;

CREATE TABLE reported_surf_heights (
    point_of_interest_id   UUID        NOT NULL,
    forecast_time          TIMESTAMP   NOT NULL,
    min                    NUMERIC     NOT NULL,
    max                    NUMERIC     NOT NULL,
    created_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at             TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, forecast_time),
    FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id)
);

CREATE TRIGGER reported_surf_heights_updated_at
BEFORE UPDATE ON reported_surf_heights
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
