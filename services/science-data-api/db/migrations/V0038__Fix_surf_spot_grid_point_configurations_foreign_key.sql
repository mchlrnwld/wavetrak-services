ALTER TABLE points_of_interest_grid_points
DROP CONSTRAINT points_of_interest_grid_points_pkey;

ALTER TABLE points_of_interest_grid_points
ADD PRIMARY KEY (agency, model, grid, point_of_interest_id);

ALTER TABLE surf_spot_grid_point_configurations
DROP CONSTRAINT surf_spot_grid_point_configurations_pkey;

ALTER TABLE surf_spot_grid_point_configurations
ADD PRIMARY KEY (agency, model, grid, point_of_interest_id);

ALTER TABLE surf_spot_grid_point_configurations
DROP CONSTRAINT surf_spot_grid_point_configurations_agency_fkey;

ALTER TABLE surf_spot_grid_point_configurations
DROP CONSTRAINT surf_spot_grid_point_configurations_point_of_interest_id_fkey;

ALTER TABLE surf_spot_grid_point_configurations
ADD FOREIGN KEY (agency, model, grid, point_of_interest_id) REFERENCES points_of_interest_grid_points (agency, model, grid, point_of_interest_id);
