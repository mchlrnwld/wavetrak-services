SET ROLE public_owner;

CREATE TABLE station_sources (
    source         TEXT      NOT NULL,
    copyright_text TEXT,
    copyright_link TEXT,
    created_at     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (source)
);

CREATE TRIGGER station_sources_updated_at
BEFORE UPDATE ON station_sources
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

CREATE TYPE station_status as ENUM ('ONLINE', 'OFFLINE');

CREATE TABLE stations (
    id               UUID           NOT NULL DEFAULT (uuid_generate_v4()),
    source           TEXT           NOT NULL,
    source_id        TEXT           NOT NULL,
    status           station_status NOT NULL,
    reports_wave     BOOLEAN        NOT NULL,
    reports_wind     BOOLEAN        NOT NULL,
    name             TEXT,
    latitude         NUMERIC        NOT NULL,
    longitude        NUMERIC        NOT NULL,
    latest_timestamp TIMESTAMP      NOT NULL,
    created_at       TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at       TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (source) REFERENCES station_sources (source),
    UNIQUE (source, source_id)
);

CREATE TRIGGER stations_updated_at
BEFORE UPDATE ON stations
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();

CREATE TABLE station_data (
  station_id             UUID      NOT NULL,
  timestamp              TIMESTAMP NOT NULL,
  significant_height     NUMERIC,
  peak_period            NUMERIC,
  water_temperature      NUMERIC,
  air_temperature        NUMERIC,
  wind_speed             NUMERIC,
  wind_direction         NUMERIC,
  wind_gust              NUMERIC,
  pressure               NUMERIC,
  dew_point              NUMERIC,
  elevation              NUMERIC,
  swell_wave_1_height    NUMERIC,
  swell_wave_1_period    NUMERIC,
  swell_wave_1_direction NUMERIC,
  swell_wave_1_impact    NUMERIC,
  swell_wave_2_height    NUMERIC,
  swell_wave_2_period    NUMERIC,
  swell_wave_2_direction NUMERIC,
  swell_wave_2_impact    NUMERIC,
  swell_wave_3_height    NUMERIC,
  swell_wave_3_period    NUMERIC,
  swell_wave_3_direction NUMERIC,
  swell_wave_3_impact    NUMERIC,
  swell_wave_4_height    NUMERIC,
  swell_wave_4_period    NUMERIC,
  swell_wave_4_direction NUMERIC,
  swell_wave_4_impact    NUMERIC,
  swell_wave_5_height    NUMERIC,
  swell_wave_5_period    NUMERIC,
  swell_wave_5_direction NUMERIC,
  swell_wave_5_impact    NUMERIC,
  swell_wave_6_height    NUMERIC,
  swell_wave_6_period    NUMERIC,
  swell_wave_6_direction NUMERIC,
  swell_wave_6_impact    NUMERIC,
  created_at             TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at             TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (station_id, timestamp),
  FOREIGN KEY (station_id) REFERENCES stations (id)
);

CREATE INDEX IF NOT EXISTS station_data_timestamp_idx ON station_data (station_id, timestamp DESC);

CREATE TRIGGER station_data_updated_at
BEFORE UPDATE ON station_data
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
