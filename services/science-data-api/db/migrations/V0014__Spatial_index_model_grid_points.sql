CREATE INDEX grid_point_idx
    ON model_grid_points
    USING GIST (grid_point);
