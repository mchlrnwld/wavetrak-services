ALTER TABLE surf ALTER COLUMN breaking_wave_height_min DROP NOT NULL;
ALTER TABLE surf ALTER COLUMN breaking_wave_height_max DROP NOT NULL;
ALTER TABLE surf ALTER COLUMN breaking_wave_height_algorithm DROP NOT NULL;
