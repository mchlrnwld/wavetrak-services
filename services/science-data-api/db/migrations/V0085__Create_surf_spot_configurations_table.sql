SET ROLE public_owner;

CREATE TABLE surf_spot_configurations
(
    point_of_interest_id             UUID                           NOT NULL,
    offshore_direction               NUMERIC                        NOT NULL,
    optimal_swell_direction          NUMERIC,
    breaking_wave_height_coefficient NUMERIC,
    breaking_wave_height_intercept   NUMERIC,
    spectral_refraction_matrix       NUMERIC[24][18],
    breaking_wave_height_algorithm   BREAKING_WAVE_HEIGHT_ALGORITHM NOT NULL,
    created_at                       TIMESTAMP                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at                       TIMESTAMP                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id),
    CONSTRAINT surf_spot_configurations_point_of_interest_id_fkey FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id)
);

CREATE TRIGGER surf_spot_configurations_at
    BEFORE UPDATE
    ON surf_spot_configurations
    FOR EACH ROW
    EXECUTE PROCEDURE set_updated_at();
