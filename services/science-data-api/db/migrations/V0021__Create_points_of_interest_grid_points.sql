SET ROLE public_owner;

ALTER TABLE points_of_interest ALTER COLUMN id SET DATA TYPE UUID USING (uuid_generate_v4());
ALTER TABLE points_of_interest ALTER COLUMN id SET DEFAULT (uuid_generate_v4());
ALTER TABLE model_grid_points ADD COLUMN id SERIAL NOT NULL;
ALTER TABLE model_grid_points DROP CONSTRAINT model_grid_points_pkey;
ALTER TABLE model_grid_points ADD PRIMARY KEY (id);

-- points of interest grid points table
CREATE TABLE points_of_interest_grid_points(
    points_of_interest_id   UUID        NOT NULL REFERENCES points_of_interest (id),
    model_run_grid_point_id SERIAL      NOT NULL REFERENCES model_grid_points (id),
    created_at              TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at              TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (points_of_interest_id, model_run_grid_point_id)
);

CREATE TRIGGER points_of_interest_grid_points_updated_at
BEFORE UPDATE ON points_of_interest_grid_points
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
