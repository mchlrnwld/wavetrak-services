SET ROLE public_owner;

CREATE TABLE sst (
    point_of_interest_id UUID        NOT NULL,
    agency               VARCHAR(25) NOT NULL,
    model                VARCHAR(25) NOT NULL,
    grid                 VARCHAR(25) NOT NULL,
    latitude             FLOAT       NOT NULL,
    longitude            FLOAT       NOT NULL,
    run                  TIMESTAMP   NOT NULL,
    forecast_time        TIMESTAMP   NOT NULL,
    temperature          NUMERIC     NOT NULL,
    created_at           TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at           TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (point_of_interest_id, agency, model, grid, run, forecast_time),
    FOREIGN KEY (point_of_interest_id) REFERENCES points_of_interest (id),
    FOREIGN KEY (agency, model, run) REFERENCES model_runs (agency, model, run),
    FOREIGN KEY (agency, model, grid) REFERENCES model_grids (agency, model, grid)
);

CREATE TRIGGER sst_updated_at
BEFORE UPDATE ON sst
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
