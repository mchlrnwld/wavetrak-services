ALTER TABLE stations
    ADD COLUMN geometry_point geometry(POINT, 4326);

UPDATE stations s
SET geometry_point = ST_SETSRID(ST_POINT(s.longitude, s.latitude), 4326)
WHERE geometry_point IS NULL;

CREATE FUNCTION set_geometry_point()
RETURNS TRIGGER AS $$
BEGIN
    NEW.geometry_point := ST_SETSRID(ST_POINT(NEW.longitude, NEW.latitude), 4326);
RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER stations_update_geometry_point
    BEFORE INSERT OR UPDATE OF latitude, longitude ON stations
    FOR EACH ROW
    EXECUTE PROCEDURE set_geometry_point();

CREATE INDEX stations_geometry_point_idx
    ON stations
    USING GIST (geometry_point);
