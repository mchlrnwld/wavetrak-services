-- Drop foreign keys pointing to model_runs
DO $$
DECLARE
   fkey RECORD;
BEGIN
    FOR fkey IN (
        SELECT tc.table_name, tc.constraint_name
        FROM information_schema.table_constraints AS tc
        INNER JOIN information_schema.constraint_column_usage AS ccu
        ON tc.constraint_name=ccu.constraint_name
        WHERE 'FOREIGN KEY'=tc.constraint_type
            AND 'model_runs'=ccu.table_name
        GROUP BY tc.table_name, tc.constraint_name
    ) LOOP
        EXECUTE format(
            'ALTER TABLE %I DROP CONSTRAINT %I;',
            fkey.table_name,
            fkey.constraint_name
        );
    END LOOP;
END; $$;


-- Port data
INSERT INTO model_runs_point_of_interest (agency, model, run, status)
SELECT agency, model, run, status FROM model_runs
ON CONFLICT DO NOTHING;

-- Create new keys on tables
ALTER TABLE sst ADD CONSTRAINT sst_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE surf ADD CONSTRAINT surf_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE swell_ensemble_members ADD CONSTRAINT swell_ensemble_members_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE swell_probabilities ADD CONSTRAINT swell_probabilities_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE swells ADD CONSTRAINT swells_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE swells_ensemble ADD CONSTRAINT swells_ensemble_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE weather ADD CONSTRAINT weather_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

ALTER TABLE wind ADD CONSTRAINT wind_model_run_points_of_interest_fkey
    FOREIGN KEY (agency, model, run)
    REFERENCES model_runs_point_of_interest (agency, model, run);

