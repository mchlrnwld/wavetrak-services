CREATE INDEX agency_model_grid_idx
    ON model_grid_points (agency, model, grid)
    WITH (FILLFACTOR=99);

ALTER TABLE model_grid_points CLUSTER ON agency_model_grid_idx;
