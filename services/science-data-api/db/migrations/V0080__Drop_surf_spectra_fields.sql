ALTER TABLE surf
DROP COLUMN wave_energy,
DROP COLUMN wave_direction,
DROP COLUMN wave_directional_spread;
