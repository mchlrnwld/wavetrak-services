CREATE TYPE model_run_status AS ENUM ('ONLINE', 'PENDING', 'OFFLINE');

CREATE TABLE model_runs (
    PRIMARY KEY (agency, model, grid, run_time),
    agency   VARCHAR(25)                        NOT NULL,
    model    VARCHAR(25)                        NOT NULL,
    grid     VARCHAR(25)                        NOT NULL,
    run_time TIMESTAMP                          NOT NULL,
    status   model_run_status DEFAULT 'PENDING' NOT NULL
);

CREATE INDEX status_idx ON model_runs (status);
