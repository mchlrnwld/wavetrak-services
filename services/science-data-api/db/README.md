# Science Platform Database Management

## Development

### Setup requirements

Local development should be done through the available command line tools.

```
$ brew install postgresql
$ brew install flyway
$ brew install postgis
```

### Initializing DB
Use psql to spin up a local database and user; don't forget the semicolons.

```
$ pg_ctl -D /usr/local/var/postgres start
$ psql postgres
postgres=# create database localscienceplatform;
CREATE DATABASE
postgres=# create user localuser with superuser;
CREATE ROLE
```

Exit the CLI, change into the `db` directory, and use flyway to run SQL migrations in order to sync your local DB environment with what is deployed.
In `flyway.conf` on the root level point `flyway.url` to a JDBC driver. Format should be `jdbc:postgresql://<host>:<port>/<dbname>`.

```
$ cp flyway.conf.sample flyway.conf
$ flyway migrate
```

### Writing and Running migrations
Add SQL migration scripts under the `db/migrations` folder. Flyway will only apply migrations prefixed with `V<n>__<migration_name>` where `n` is a number greater than the most recently applied migration version number.

Changes can be verified using the command line tool `psql` or through the administrative tool `pgAdmin`.

Stylistically, SQL scripts should adhere to these guidelines: https://www.sqlstyle.guide/


### Running seed files

```
psql -U postgres -d <dbname> -a -f "absolute/path/to/sql/script"
```
