INSERT INTO models (agency, model, sst, surf, swell, weather, wind)
VALUES ('NOAA', 'GEFS-Wave', false, false, false, false, false);

INSERT INTO model_grids (
    agency,
    model,
    grid,
    resolution,
    description,
    start_latitude,
    end_latitude,
    start_longitude,
    end_longitude
)
VALUES (
    'NOAA',
    'GEFS-Wave',
    'global.0p25',
    0.25,
    'Global Ensemble Forecast System - Wave',
    -90,
    90,
    0,
    359.750
)
