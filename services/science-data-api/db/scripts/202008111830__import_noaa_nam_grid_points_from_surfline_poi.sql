BEGIN;

WITH pois AS
(
    -- Create ST_Points once to be reused in query for grid point.
    SELECT id,
           latitude,
           longitude,
           ST_Point(longitude, latitude) AS point
    FROM points_of_interest
    WHERE product = 'SL'
), pois_with_grid_points AS (
    -- Find all NOAA NAM model_grid_points within 2.1km of each POI. NAM grid points are 0.027 degrees apart,
    -- which translates to ~3km in distance. 2.1km then is sufficient for finding the closest grid point to a POI.
    -- The 2.1km distance accounts for a POI that is positioned in the middle of 2 NAM grid points, which is the farthest distance possible.
    SELECT pois.id,
           model_grid_points.agency,
           model_grid_points.model,
           model_grid_points.grid,
           model_grid_points.latitude,
           model_grid_points.longitude,
           ST_Distance(pois.point, model_grid_points.geography_point) AS distance
    FROM pois
    INNER JOIN model_grid_points
    -- ST_DWithin uses index for fast filtering of grid points before computing ST_Distance.
    ON ST_DWithin(pois.point, model_grid_points.geography_point, 2100)
    WHERE model_grid_points.agency = 'NOAA'
    AND model_grid_points.model = 'NAM'
    ORDER BY distance
), pois_grid_min_distances AS (
    -- Get distance to nearest grid point.
    SELECT id,
           MIN(distance) distance
    FROM pois_with_grid_points
    GROUP BY id
), non_unique_results AS (
    -- Filter for grid points nearest to each poi.
    SELECT pois_with_grid_points.*,
           -- Add row numbers by ID to identify pois with more than 1 nearest grid point. This can
           -- occur when a point is equidistant from 2 or 4 grid points.
           ROW_NUMBER() OVER (PARTITION BY pois_with_grid_points.id) AS result_number
    FROM pois_with_grid_points
    INNER JOIN pois_grid_min_distances
    ON pois_with_grid_points.id = pois_grid_min_distances.id
    AND pois_with_grid_points.distance = pois_grid_min_distances.distance
)
INSERT INTO points_of_interest_grid_points (
    point_of_interest_id,
    agency,
    model,
    grid,
    latitude,
    longitude
)
SELECT id,
       agency,
       model,
       grid,
       latitude,
       CASE WHEN longitude < 0 THEN longitude + 360 ELSE longitude END -- Convert longitudes in range: [-180, 0] to [180, 360]
FROM non_unique_results
-- Use first result for each spot to prevent duplicates.
WHERE result_number = 1;

ROLLBACK;
