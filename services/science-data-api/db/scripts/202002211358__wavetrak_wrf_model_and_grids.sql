BEGIN;
INSERT INTO models (agency, model, wind)
VALUES ('Wavetrak', 'WRF', true);

INSERT INTO model_grids (agency, model, grid)
VALUES ('Wavetrak', 'WRF', 'aust_nsw'),
       ('Wavetrak', 'WRF', 'aust_qland'),
       ('Wavetrak', 'WRF', 'aust_south'),
       ('Wavetrak', 'WRF', 'aust_vic_tas'),
       ('Wavetrak', 'WRF', 'aust_west'),
       ('Wavetrak', 'WRF', 'baja'),
       ('Wavetrak', 'WRF', 'bali'),
       ('Wavetrak', 'WRF', 'brazil_rio'),
       ('Wavetrak', 'WRF', 'fiji'),
       ('Wavetrak', 'WRF', 'hawaii_1km'),
       ('Wavetrak', 'WRF', 'iberia'),
       ('Wavetrak', 'WRF', 'med_east'),
       ('Wavetrak', 'WRF', 'mexico_central'),
       ('Wavetrak', 'WRF', 'new_zealand'),
       ('Wavetrak', 'WRF', 'puerto_rico'),
       ('Wavetrak', 'WRF', 'sumatra_new'),
       ('Wavetrak', 'WRF', 'tahiti_1km'),
       ('Wavetrak', 'WRF', 'uk');
COMMIT;
