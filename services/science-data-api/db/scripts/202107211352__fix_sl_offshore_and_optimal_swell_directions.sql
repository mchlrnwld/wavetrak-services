BEGIN;

SELECT *
FROM surf_spot_configurations
WHERE point_of_interest_id = '0ca5be9d-013b-4e62-991b-7c9f67196605';

UPDATE surf_spot_configurations
SET optimal_swell_direction = surf_spot_configurations.offshore_direction,
    offshore_direction = (surf_spot_configurations.offshore_direction + 180) % 360
WHERE point_of_interest_id IN (
  SELECT id
  FROM points_of_interest
  WHERE product = 'SL'
)
AND optimal_swell_direction IS NULL;

SELECT *
FROM surf_spot_configurations
WHERE point_of_interest_id = '0ca5be9d-013b-4e62-991b-7c9f67196605';

ROLLBACK;
