BEGIN;

UPDATE model_grids SET start_latitude=NULL, end_latitude=NULL, start_longitude=NULL, end_longitude=NULL WHERE model = 'NAM' and agency = 'NOAA';

UPDATE model_grids SET resolution=0.027 WHERE model = 'NAM' and agency = 'NOAA' and grid = 'conus';

SELECT * FROM model_grids WHERE model = 'NAM' and agency = 'NOAA';

ROLLBACK;
