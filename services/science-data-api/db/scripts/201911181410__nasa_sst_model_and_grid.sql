BEGIN;
INSERT INTO models (agency, model)
VALUES ('NASA', 'MUR-SST');

INSERT INTO model_grids (agency, model, grid)
VALUES ('NASA', 'MUR-SST', '0p01');

COMMIT;
