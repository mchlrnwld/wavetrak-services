BEGIN;
DO $$
BEGIN
  FOR i IN 0..1439 LOOP
     FOR j IN 0..720 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'GFS', '0p25', i / 4.0, 90 - j / 4.0);
    END LOOP;
  END LOOP;
END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
