BEGIN;
DO $$
BEGIN
  FOR i IN 0..1438 LOOP
     FOR j IN 0..720 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'GEFS-Wave', 'global.0p25', 0 + i * 0.25, -90.0 + j * 0.25);
    END LOOP;
  END LOOP;
END; $$;

CLUSTER model_grid_points USING grid_idx;

SELECT * FROM model_grid_points WHERE agency = 'NOAA' and model = 'GEFS-Wave';
ROLLBACK;
