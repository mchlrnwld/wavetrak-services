INSERT INTO points_of_interest_default_forecast_models (
    point_of_interest_id,
    forecast_type,
    agency,
    model,
    grid
)
SELECT poi.id, 'SWELLS', gp.agency, gp.model, gp.grid
FROM points_of_interest AS poi
INNER JOIN points_of_interest_grid_points AS gp
ON poi.id = gp.point_of_interest_id
WHERE poi.product = 'SL'
AND gp.agency = 'Wavetrak'
AND gp.model = 'Lotus-WW3'
AND gp.grid != 'GLOB_15m';
