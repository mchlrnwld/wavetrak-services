BEGIN;

UPDATE model_grids
SET start_latitude = -90,
    end_latitude = 90,
    start_longitude = 0,
    end_longitude = 359.75
WHERE agency = 'NOAA'
AND model = 'GFS'
AND grid = '0p25';

UPDATE model_grids
SET start_latitude = -36,
    end_latitude = -24,
    start_longitude = 150,
    end_longitude = 154.5
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'AUS_E_3m';

UPDATE model_grids
SET start_latitude = -36,
    end_latitude = -31,
    start_longitude = 114,
    end_longitude = 116
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'AUS_SW_3m';

UPDATE model_grids
SET start_latitude = -39.5,
    end_latitude = -37.5,
    start_longitude = 143,
    end_longitude = 147
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'AUS_VT_3m';

UPDATE model_grids
SET start_latitude = -9.5,
    end_latitude = -7.5,
    start_longitude = 113,
    end_longitude = 118
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'BALI_3m';

UPDATE model_grids
SET start_latitude = 31,
    end_latitude = 42,
    start_longitude = 234.5,
    end_longitude = 245
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'CAL_3m';

UPDATE model_grids
SET start_latitude = 43,
    end_latitude = 46,
    start_longitude = 350,
    end_longitude = 360
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'FR_3m';

UPDATE model_grids
SET start_latitude = -77.5,
    end_latitude = 77.5,
    start_longitude = 0,
    end_longitude = 359.5
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'GLOB_30m';

UPDATE model_grids
SET start_latitude = -77.5,
    end_latitude = 77.5,
    start_longitude = 0,
    end_longitude = 359.75
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'GLOB_15m';

UPDATE model_grids
SET start_latitude = 18,
    end_latitude = 23,
    start_longitude = 199,
    end_longitude = 206
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'HW_3m';

UPDATE model_grids
SET start_latitude = 36,
    end_latitude = 45,
    start_longitude = 350,
    end_longitude = 353
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'PT_3m';

UPDATE model_grids
SET start_latitude = 45,
    end_latitude = 52,
    start_longitude = 353,
    end_longitude = 360
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'UK_3m';

UPDATE model_grids
SET start_latitude = 30,
    end_latitude = 43,
    start_longitude = 278,
    end_longitude = 291
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid = 'US_E_3m';

UPDATE model_grids
SET start_latitude = 0,
    end_latitude = 55,
    start_longitude = 260,
    end_longitude = 310
WHERE agency = 'NOAA'
AND model = 'WW3'
AND grid = 'at_10m';

UPDATE model_grids
SET start_latitude = 15,
    end_latitude = 47,
    start_longitude = 261,
    end_longitude = 300
WHERE agency = 'NOAA'
AND model = 'WW3'
AND grid = 'at_4m';

UPDATE model_grids
SET start_latitude = -77.5,
    end_latitude = 77.5,
    start_longitude = 0,
    end_longitude = 359.5
WHERE agency = 'NOAA'
AND model = 'WW3'
AND grid = 'glo_30m';

UPDATE model_grids
SET start_latitude = 25,
    end_latitude = 50,
    start_longitude = 210,
    end_longitude = 250
WHERE agency = 'NOAA'
AND model = 'WW3'
AND grid = 'wc_10m';

UPDATE model_grids
SET start_latitude = 15,
    end_latitude = 50,
    start_longitude = 195,
    end_longitude = 244
WHERE agency = 'NOAA'
AND model = 'WW3'
AND grid = 'wc_4m';

UPDATE model_grids
SET start_latitude = -80,
    end_latitude = 80,
    start_longitude = 0,
    end_longitude = 359.5
WHERE agency = 'NOAA'
AND model = 'WW3-Ensemble'
AND grid = 'glo_30m';

UPDATE model_grids
SET start_latitude = -90,
    end_latitude = 90,
    start_longitude = -180,
    end_longitude = 180
WHERE agency = 'NASA'
AND model = 'MUR-SST'
AND grid = '0p01';

UPDATE model_grids
SET start_latitude = 18.073,
    end_latitude = 23.088,
    start_longitude = -161.525,
    end_longitude = -153.869
WHERE agency = 'NOAA'
AND model = 'NAM'
AND grid = 'hawaii';

UPDATE model_grids
SET start_latitude = -15,
    end_latitude = 22.005,
    start_longitude = -75.5,
    end_longitude = -62.509
WHERE agency = 'NOAA'
AND model = 'NAM'
AND grid = 'prico';

/*
Conus is rotated.
Do not include in initial seed.
UPDATE model_grids
SET start_latitude = 21.138,
    end_latitude = 47.84237791,
    start_longitude = -122.72,
    end_longitude = -60.91783901
WHERE agency = 'NOAA'
AND model = 'NAM'
AND grid = 'conus';
*/
SELECT * FROM model_grids;

ROLLBACK;
