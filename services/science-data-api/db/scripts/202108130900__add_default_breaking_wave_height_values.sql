BEGIN;

UPDATE surf_spot_configurations
SET breaking_wave_height_coefficient = 1.0
WHERE breaking_wave_height_coefficient IS NULL;

UPDATE surf_spot_configurations
SET breaking_wave_height_intercept = 0.0
WHERE breaking_wave_height_intercept IS NULL;

ROLLBACK;
