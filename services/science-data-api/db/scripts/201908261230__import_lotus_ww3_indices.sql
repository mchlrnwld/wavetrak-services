-- Lotus-WW3 Seed data is taken from here:
-- https://github.com/Surfline/lotus-dev/blob/master/model-source/base_files/ww3_grid.inp.AUS_E_3m
-- In the example file above, the following information is displayed:
--    91 	 241
--	  3.00 	  3.00 	 60.00
--	  150.0 	-36.0  1.00
-- (91, 421) represents the grid size
-- 3.00 represents grid frequency (3m)
-- 3.00/60.00 = 0.05 represents the spacing between grid points
-- (150.0, -36.0) are the starting grid points

-- template
-- FOR i IN 0..nx-1 LOOP
--    FOR j IN 0..ny-1  LOOP
--     INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
--     VALUES ('Wavetrak', 'Lotus-WW3', GRID_NAME, xi + i * delta_xy, yi + j * delta_xy);
--   END LOOP;
-- END LOOP;

BEGIN;
DO $$
BEGIN
  FOR i IN 0..90 LOOP
     FOR j IN 0..240 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'AUS_E_3m', 150.0 + i / 20.0, -36.0 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..40 LOOP
     FOR j IN 0..100 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'AUS_SW_3m', 114.0 + i / 20.0, -36.0 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..80 LOOP
     FOR j IN 0..40 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'AUS_VT_3m', 143 + i / 20.0, -39.5 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..100 LOOP
     FOR j IN 0..40 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'BALI_3m', 113.0 + i / 20.0, -9.5 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..210 LOOP
     FOR j IN 0..220 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'CAL_3m', 234.5 + i / 20.0, 31 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..200 LOOP
     FOR j IN 0..60 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'FR_3m', 350 + i / 20.0, 43 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..1439 LOOP
     FOR j IN 0..620 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'GLOB_15m', 0 + i / 4.0, -77.5 + j / 4.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..719 LOOP
     FOR j IN 0..310 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'GLOB_30m', 0 + i / 2.0, -77.5 + j / 2.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..140 LOOP
     FOR j IN 0..100 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'HW_3m', 199 + i / 20.0, 18 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..60 LOOP
     FOR j IN 0..180 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'PT_3m', 350.0 + i / 20.0, 36.0 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..140 LOOP
     FOR j IN 0..140 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'UK_3m', 353.0 + i / 20.0, 45 + j / 20.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..260 LOOP
     FOR j IN 0..260 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'US_E_3m', 278 + i / 20.0, 30 + j / 20.0);
    END LOOP;
  END LOOP;

END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
