BEGIN;

WITH pois AS
(
    -- Create ST_Points once to be reused in query for grid point.
    SELECT id,
           latitude,
           longitude,
           ST_Point(longitude, latitude) AS point
    FROM points_of_interest
    WHERE product = 'SL'
), pois_with_grid_points_1km AS (
    -- Find all WRF model_grid_points for 1km resolution grids.
    -- 0.7km then is sufficient for finding the closest grid point to a POI.
    -- The 0.7km distance accounts for a POI that is positioned in the middle of 2 WRF grid points,
    -- which is the furthest distance possible.
    SELECT pois.id,
           model_grid_points.agency,
           model_grid_points.model,
           model_grid_points.grid,
           model_grid_points.latitude,
           model_grid_points.longitude,
           ST_Distance(pois.point, model_grid_points.geography_point) AS distance
    FROM pois
    INNER JOIN model_grid_points
    -- ST_DWithin uses index for fast filtering of grid points before computing ST_Distance.
    ON ST_DWithin(pois.point, model_grid_points.geography_point, 700)
    INNER JOIN model_grids
    ON model_grids.grid = model_grid_points.grid
    AND model_grids.model = model_grid_points.model
    AND model_grids.agency = model_grid_points.agency
    WHERE model_grid_points.agency = 'Wavetrak'
    AND model_grid_points.model = 'WRF'
    AND model_grids.resolution = 0.009
), pois_with_grid_points_3km AS (
    -- Find all WRF model_grid_points for 3km resolution grids.
    -- 2.2km then is sufficient for finding the closest grid point to a POI.
    -- The 2.2km distance accounts for a POI that is positioned in the middle of 2 WRF grid points,
    -- which is the furthest distance possible.
    SELECT pois.id,
           model_grid_points.agency,
           model_grid_points.model,
           model_grid_points.grid,
           model_grid_points.latitude,
           model_grid_points.longitude,
           ST_Distance(pois.point, model_grid_points.geography_point) AS distance
    FROM pois
    INNER JOIN model_grid_points
    ON ST_DWithin(pois.point, model_grid_points.geography_point, 2200)
    INNER JOIN model_grids
    ON model_grids.grid = model_grid_points.grid
    AND model_grids.model = model_grid_points.model
    AND model_grids.agency = model_grid_points.agency
    WHERE model_grid_points.agency = 'Wavetrak'
    AND model_grid_points.model = 'WRF'
    AND model_grids.resolution = 0.027
), pois_with_grid_points_6km AS (
    -- Find all WRF model_grid_points for 6km resolution grids.
    -- 4.3km then is sufficient for finding the closest grid point to a POI.
    -- The 4.3km distance accounts for a POI that is positioned in the middle of 2 WRF grid points,
    -- which is the furthest distance possible.
    SELECT pois.id,
           model_grid_points.agency,
           model_grid_points.model,
           model_grid_points.grid,
           model_grid_points.latitude,
           model_grid_points.longitude,
           ST_Distance(pois.point, model_grid_points.geography_point) AS distance
    FROM pois
    INNER JOIN model_grid_points
    ON ST_DWithin(pois.point, model_grid_points.geography_point, 4300)
    INNER JOIN model_grids
    ON model_grids.grid = model_grid_points.grid
    AND model_grids.model = model_grid_points.model
    AND model_grids.agency = model_grid_points.agency
    WHERE model_grid_points.agency = 'Wavetrak'
    AND model_grid_points.model = 'WRF'
    AND model_grids.resolution = 0.054
), all_pois_with_grid_points AS (
    SELECT * FROM pois_with_grid_points_1km
    UNION SELECT * FROM pois_with_grid_points_3km
    UNION SELECT * FROM pois_with_grid_points_6km
), pois_grid_min_distances AS (
    -- Get distance to nearest grid point.
    SELECT id,
           MIN(distance) distance
    FROM all_pois_with_grid_points
    GROUP BY id
), non_unique_results AS (
    -- Filter for grid points nearest to each spot.
    SELECT all_pois_with_grid_points.*,
           -- Add row numbers by ID to identify pois with more than 1 nearest grid point. This can
           -- occur when a point is equidistant from 2 or 4 grid points.
           ROW_NUMBER() OVER (PARTITION BY all_pois_with_grid_points.id) AS result_number
    FROM all_pois_with_grid_points
    INNER JOIN pois_grid_min_distances
    ON all_pois_with_grid_points.id = pois_grid_min_distances.id
    AND all_pois_with_grid_points.distance = pois_grid_min_distances.distance
),unique_results AS (
    -- Filter POI grid points that overlap with NAM POI grid points.
    SELECT * FROM non_unique_results
    WHERE id NOT IN (
        SELECT point_of_interest_id
        FROM points_of_interest_grid_points
        WHERE model = 'NAM'
    )
    AND result_number = 1
)
INSERT INTO points_of_interest_grid_points (
    point_of_interest_id,
    agency,
    model,
    grid,
    latitude,
    longitude
)
SELECT id,
       agency,
       model,
       grid,
       latitude,
       CASE WHEN longitude < 0 THEN longitude + 360 ELSE longitude END -- Convert longitudes in range: [-180, 0] to [180, 360]
FROM unique_results;

ROLLBACK;
