/*
** Date:        2021-06-03
** Author:      Tom Portwood
** Description: 
**   Create missing default SURF and SWELLS forecast models for all 
**   points of interest that reference the GLOB_15m grid.
*/


BEGIN;

WITH new_default_forecast_models AS (
    -- all glob_15m point_of_interest_grid_points with no default SWELLS forecast model
    SELECT poigp.point_of_interest_id, 
           'SWELLS'::forecast_type, 
           poigp.agency,
           poigp.model,
           poigp.grid
    FROM points_of_interest_grid_points poigp
    LEFT JOIN points_of_interest_default_forecast_models poidfm
    ON poidfm.point_of_interest_id = poigp.point_of_interest_id 
    AND poidfm.forecast_type = 'SWELLS'
    WHERE poigp.grid = 'GLOB_15m'
    AND poigp.model = 'Lotus-WW3'
    AND poigp.agency = 'Wavetrak'
    AND poidfm.point_of_interest_id IS NULL
    UNION
    -- all glob15m point_of_interest_grid_points with no default SURF forecast model
    SELECT poigp.point_of_interest_id, 
           'SURF'::forecast_type, 
           poigp.agency,
           poigp.model,
           poigp.grid
    FROM points_of_interest_grid_points poigp
    LEFT JOIN points_of_interest_default_forecast_models poidfm
    ON poidfm.point_of_interest_id = poigp.point_of_interest_id 
    AND poidfm.forecast_type = 'SURF'
    WHERE poigp.grid = 'GLOB_15m'
    AND poigp.model = 'Lotus-WW3'
    AND poigp.agency = 'Wavetrak'
    AND poidfm.point_of_interest_id IS NULL
)
INSERT INTO points_of_interest_default_forecast_models (
    point_of_interest_id,
    forecast_type,
    agency,
    model,
    grid
)
SELECT *
FROM new_default_forecast_models;

ROLLBACK;
