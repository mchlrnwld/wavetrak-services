-- template
-- FOR i IN 0..nx-1 LOOP
--    FOR j IN 0..ny-1  LOOP
--     INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
--     VALUES ('Wavetrak', 'Proteus-WW3', GRID_NAME, xi + i * delta_xy, yi + j * delta_xy);
--   END LOOP;
-- END LOOP;

BEGIN;
DO $$
BEGIN
  FOR i IN 0..780 LOOP
     FOR j IN 0..360 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'AUS_10m', 90 + i / 6.0, -50 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..45 LOOP
     FOR j IN 0..30 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'BALI_4m', 114 + i / 15.0, -9.5 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..150 LOOP
     FOR j IN 0..120 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'BRA_4m', 310 + i / 15.0, -29 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..67 LOOP
     FOR j IN 0..180 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'EAUS_4m', 150 + i / 15.0, -36 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..360 LOOP
     FOR j IN 0..216 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'EU_10m', -15 + i / 6.0, 30 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..90 LOOP
     FOR j IN 0..75 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'FIJ_4m', 176 + i / 15.0, -20 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..60 LOOP
     FOR j IN 0..45 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'FPOL_4m', 208.5 + i / 15.0, -19 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..60 LOOP
     FOR j IN 0..37 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'FR_4m', -5 + i / 15.0, 43 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..719 LOOP
     FOR j IN 0..310 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'GLOB_30m', 0 + i / 2.0, -77.5 + j / 2.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..105 LOOP
     FOR j IN 0..75 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'HW_4m', 199 + i / 15.0, 18 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..45 LOOP
     FOR j IN 0..45 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'JBAY_4m', 24 + i / 15.0, -36 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..138 LOOP
     FOR j IN 0..96 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'MEST_10m', 47 + i / 6.0, 15 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..660 LOOP
     FOR j IN 0..270 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'NAM_10m', 195 + i / 6.0, 5 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..52 LOOP
     FOR j IN 0..60 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'NCAL_4m', 236 + i / 15.0, 35 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..30 LOOP
     FOR j IN 0..37 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'PT_4m', -10 + i / 15.0, 38 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..180 LOOP
     FOR j IN 0..120 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'SAF_10m', 10 + i / 6.0, -40 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..390 LOOP
     FOR j IN 0..330 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'SAM_10m', 265 + i / 6.0, -50 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..67 LOOP
     FOR j IN 0..52 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'SCAL_4m', 239 + i / 15.0, 31.5 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..30 LOOP
     FOR j IN 0..75 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'SWAUS_4m', 114 + i / 15.0, -36 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..105 LOOP
     FOR j IN 0..45 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'UK_4m', -7 + i / 15.0, 49 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..60 LOOP
     FOR j IN 0..30 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Proteus-WW3', 'VTAUS_4m', 143 + i / 15.0, -39.5 + j / 15.0);
    END LOOP;
  END LOOP;

END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
