BEGIN;

INSERT INTO models (agency, model, sst, surf, swell, weather, wind) VALUES
('NOAA', 'NAM', false, false, false, true, true);

INSERT INTO model_grids (agency, model, grid, resolution) VALUES
('NOAA', 'NAM', 'conus', 0.045),  -- ~5km
('NOAA', 'NAM', 'hawaii', 0.027), -- ~3km
('NOAA', 'NAM', 'prico', 0.027);  -- ~3km

COMMIT;
