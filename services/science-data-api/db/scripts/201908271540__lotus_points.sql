BEGIN;
DELETE FROM surf WHERE agency='Wavetrak' AND model='Lotus-WW3';
DELETE FROM swells WHERE agency='Wavetrak' AND model='Lotus-WW3';
DELETE FROM points_of_interest_grid_points WHERE agency='Wavetrak' AND model='Lotus-WW3';
\copy points_of_interest_grid_points (point_of_interest_id, agency, model, grid, longitude, latitude) FROM '201908271540__lotus_points_of_interest.csv' WITH CSV HEADER;
COMMIT;
