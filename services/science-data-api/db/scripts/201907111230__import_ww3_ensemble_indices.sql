BEGIN;
DO $$
BEGIN
  FOR i IN 0..719 LOOP
     FOR j IN 0..320 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'WW3-Ensemble', 'glo_30m', 0 + i * 0.5, -80.0 + j * 0.5);
    END LOOP;
  END LOOP;
END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
