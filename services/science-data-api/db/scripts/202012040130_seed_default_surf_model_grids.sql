INSERT INTO points_of_interest_default_forecast_models (
    point_of_interest_id,
    forecast_type,
    agency,
    model,
    grid
)
SELECT point_of_interest_id, 'SURF', agency, model, grid
FROM points_of_interest_default_forecast_models
WHERE forecast_type='SWELLS';
