-- Lotus-WW3 Seed data is taken from here:
-- https://github.com/Surfline/lotus-dev/blob/master/model-source/base_files/ww3_grid.inp.AUS_E_3m
-- In the example file above, the following information is displayed:
--    91 	 241
--	  3.00 	  3.00 	 60.00
--	  150.0 	-36.0  1.00
-- (91, 421) represents the grid size
-- 3.00 represents grid frequency (3m)
-- 3.00/60.00 = 0.05 represents the spacing between grid points
-- (150.0, -36.0) are the starting grid points

-- template
-- FOR i IN 0..nx-1 LOOP
--    FOR j IN 0..ny-1  LOOP
--     INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
--     VALUES ('Wavetrak', 'Lotus-WW3', GRID_NAME, xi + i * delta_xy, yi + j * delta_xy);
--   END LOOP;
-- END LOOP;

BEGIN;
DO $$
BEGIN
  FOR i IN 0..180 LOOP
     FOR j IN 0..80 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('Wavetrak', 'Lotus-WW3', 'GLAKES_6m', 267.0 + i / 10.0, 41.0 + j / 10.0);
    END LOOP;
  END LOOP;
END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
