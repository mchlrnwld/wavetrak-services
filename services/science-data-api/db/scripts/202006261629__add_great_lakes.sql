INSERT INTO model_grids (
    agency,
    model,
    grid,
    resolution,
    start_latitude,
    end_latitude,
    start_longitude,
    end_longitude,
    description
) VALUES (
    'Wavetrak',
    'Lotus-WW3',
    'GLAKES_6m',
    0.1,
    40,
    49,
    267,
    285,
    'Lotus 6m Great Lakes Wave Model'
);
