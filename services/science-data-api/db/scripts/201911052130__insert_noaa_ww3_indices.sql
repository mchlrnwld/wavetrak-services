BEGIN;
DO $$
BEGIN
  FOR i IN 0..719 LOOP
     FOR j IN 0..335 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'WW3', 'glo_30m', 0 + i * 0.5, -77.5 + j * 0.5);
    END LOOP;
  END LOOP;

  FOR i IN 0..735 LOOP
     FOR j IN 0..525 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'WW3', 'wc_4m', 195 + i / 15.0, 15 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..240 LOOP
     FOR j IN 0..150 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'WW3', 'wc_10m', 210 + i / 6.0, 25 + j / 6.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..585 LOOP
     FOR j IN 0..480 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'WW3', 'at_4m', 261 + i / 15.0, 15 + j / 15.0);
    END LOOP;
  END LOOP;

  FOR i IN 0..300 LOOP
     FOR j IN 0..330 LOOP
      INSERT INTO model_grid_points (agency, model, grid, longitude, latitude)
      VALUES ('NOAA', 'WW3', 'at_10m', 260 + i / 6.0, 0.0 + j / 6.0);
    END LOOP;
  END LOOP;
END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
