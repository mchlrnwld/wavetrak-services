BEGIN;

UPDATE model_grids SET resolution=0.5
WHERE grid LIKE '%_30m';

UPDATE model_grids SET resolution=0.25
WHERE grid LIKE '%_15m' OR grid = '0p25';

UPDATE model_grids SET resolution=1.0/6
WHERE grid LIKE '%_10m';

UPDATE model_grids SET resolution=1.0/15
WHERE grid LIKE '%_4m';

UPDATE model_grids SET resolution=0.05
WHERE grid LIKE '%_3m';

UPDATE model_grids SET resolution=0.01
WHERE grid = '0p01';

UPDATE model_grids SET resolution=1.0
WHERE model='LOLA-WW3' AND grid='glz';

UPDATE model_grids SET resolution=0.25
WHERE model='LOLA-WW3' AND grid IN ('wnz', 'enz');

UPDATE model_grids SET resolution=0.2
WHERE model='LOLA-WW3' AND grid IN ('mdz', 'nrz', 'rez');

UPDATE model_grids SET resolution=0.1
WHERE model='LOLA-WW3' AND grid IN ('btz', 'bkz');

UPDATE model_grids SET resolution=0.05
WHERE model='LOLA-WW3' AND grid IN ('scz', 'gkz');

-- WRF grids have resolution specified in km not degrees. Conversion from km to
-- deg using great-circle, with the Earth's circumference taken as 40,000km and
-- so 1km = 0.009 deg.
UPDATE model_grids SET resolution=0.009
WHERE model='WRF' AND grid IN ('hawaii_1km', 'tahiti_1km');

UPDATE model_grids SET resolution=0.027
WHERE model='WRF' AND grid IN (
    'aust_nsw',
    'aust_qland',
    'aust_south',
    'aust_vic_tas',
    'aust_west',
    'baja',
    'bali',
    'brazil_rio',
    'fiji',
    'med_east',
    'mexico_central',
    'new_zealand',
    'puerto_rico',
    'sumatra_new',
    'uk'
);

UPDATE model_grids SET resolution=0.054
WHERE model='WRF' AND grid IN ('iberia');

COMMIT;
