BEGIN;

DELETE FROM points_of_interest_default_forecast_models
WHERE agency = 'NOAA'
AND model = 'WW3';

DELETE FROM surf_spot_grid_point_configurations
WHERE agency = 'NOAA'
AND model = 'WW3';

DELETE FROM points_of_interest_grid_points
WHERE agency = 'NOAA'
AND model = 'WW3';

ROLLBACK;
