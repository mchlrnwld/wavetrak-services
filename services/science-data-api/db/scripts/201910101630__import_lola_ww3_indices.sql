BEGIN;
DO $$
BEGIN
    FOR i IN 0..180 LOOP
        FOR j IN 0..359 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'glz', -90.00 + i, j);
        END LOOP;
    END LOOP;

    FOR i IN 0..202 LOOP
        FOR j IN 0..274 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'wnz', -0.25 + i * 0.25, 261.75 + j * 0.25);
        END LOOP;
    END LOOP;

    FOR i IN 0..223 LOOP
        FOR j IN 0..372 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'enz', 4.75 + i * 0.25, 189.75 + j * 0.25);
        END LOOP;
    END LOOP;

    FOR i IN 0..60 LOOP
        FOR j IN 0..80 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'scz', 32.0 + i * 0.05, 239.0 + j * 0.05);
        END LOOP;
    END LOOP;

    FOR i IN 0..199 LOOP
        FOR j IN 0..399 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'gkz', 40.0 + i * 0.05, 265.0 + j * 0.05);
        END LOOP;
    END LOOP;

    FOR i IN 0..90 LOOP
        FOR j IN 0..220 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'mdz', 27.80 + i * 0.20, -6.20 + j * 0.20);
        END LOOP;
    END LOOP;

    FOR i IN 0..65 LOOP
        FOR j IN 0..140 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'nrz', 48.0 + i * 0.20, -15.0 + j * 0.20);
        END LOOP;
    END LOOP;

    FOR i IN 0..140 LOOP
        FOR j IN 0..210 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'btz', 53.0 + i * 0.10, 10.0 + j * 0.10);
        END LOOP;
    END LOOP;

    FOR i IN 0..80 LOOP
        FOR j IN 0..150 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'bkz', 40.0 + i * 0.10, 27.0 + j * 0.10);
        END LOOP;
    END LOOP;

    FOR i IN 0..185 LOOP
        FOR j IN 0..170 LOOP
            INSERT INTO model_grid_points (agency, model, grid, latitude, longitude)
            VALUES ('Wavetrak', 'LOLA-WW3', 'rez', -6.00 + i * 0.20, 31.0 + j * 0.20);
        END LOOP;
    END LOOP;
END; $$;

CLUSTER model_grid_points USING grid_idx;
COMMIT;
