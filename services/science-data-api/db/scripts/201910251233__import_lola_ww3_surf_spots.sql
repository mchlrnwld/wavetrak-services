BEGIN;
\copy points_of_interest (id, name, latitude, longitude) FROM '201910251233__import_lola_ww3_points_of_interest.csv' WITH CSV HEADER;
\copy points_of_interest_grid_points (point_of_interest_id, agency, model, grid, latitude, longitude) FROM '201910251233__import_lola_ww3_point_of_interest_grid_points.csv' WITH CSV HEADER;
\copy surf_spot_grid_point_configurations (point_of_interest_id, agency, model, grid, breaking_wave_height_algorithm) FROM '201910251233__import_lola_ww3_surf_spot_configurations.csv' WITH CSV HEADER;
COMMIT;
