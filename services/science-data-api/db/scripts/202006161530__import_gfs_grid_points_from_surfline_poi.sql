/*
** Date:        2020-06-16
** Author:      Stephen Link
** Description: Finds nearest NOAA GFS 0.25 model_grid_points to each Surfline POI and inserts
**              them into points_of_interest_grid_points.
*/

BEGIN;

WITH spots AS
(
    -- Create ST_Points once to be reused in query for grid point.
    SELECT id,
           latitude,
           longitude,
           ST_Point(longitude, latitude) AS point
    FROM points_of_interest
    WHERE product = 'SL'
), spots_with_grid_points AS (
    -- Find all NOAA GFS 0.25 model_grid_points within 20km of each spot. GFS grid points are 0.25 degrees apart,
    -- which translates to ~27km in distance. 20km then is sufficient for finding the closest grid point to a POI.
    SELECT spots.id,
           model_grid_points.agency,
           model_grid_points.model,
           model_grid_points.grid,
           model_grid_points.latitude,
           model_grid_points.longitude,
           ST_Distance(spots.point, model_grid_points.geography_point) AS distance
    FROM spots
    INNER JOIN model_grid_points
    -- ST_DWithin uses index for fast filtering of grid points before computing ST_Distance.
    ON ST_DWithin(spots.point, model_grid_points.geography_point, 20000)
    WHERE model_grid_points.agency = 'NOAA'
    AND model_grid_points.model = 'GFS'
    AND model_grid_points.grid = '0p25'
    ORDER BY distance
), spots_grid_min_distances AS (
    -- Get distance to nearest grid point.
    SELECT id,
           MIN(distance) distance
    FROM spots_with_grid_points
    GROUP BY id
), non_unique_results AS (
    -- Filter for grid points nearest to each spot.
    SELECT spots_with_grid_points.*,
           -- Add row numbers by ID to identify spots with more than 1 nearest grid point. This can
           -- occur when a point is equidistant from 2 or 4 grid points.
           ROW_NUMBER() OVER (PARTITION BY spots_with_grid_points.id) AS result_number
    FROM spots_with_grid_points
    INNER JOIN spots_grid_min_distances
    ON spots_with_grid_points.id = spots_grid_min_distances.id
    AND spots_with_grid_points.distance = spots_grid_min_distances.distance
)
INSERT INTO points_of_interest_grid_points (
    point_of_interest_id,
    agency,
    model,
    grid,
    latitude,
    longitude
)
SELECT id,
       agency,
       model,
       grid,
       latitude,
       longitude
FROM non_unique_results
-- Use first result for each spot to prevent duplicates.
WHERE result_number = 1;

ROLLBACK;
