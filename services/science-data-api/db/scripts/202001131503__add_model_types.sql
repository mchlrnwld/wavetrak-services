BEGIN;
UPDATE models
SET sst = True
WHERE agency = 'NASA' AND model = 'MUR-SST';

UPDATE models
SET weather = TRUE, wind = TRUE
WHERE agency = 'NOAA' AND model = 'GFS';

UPDATE models
SET surf = TRUE, swell = TRUE
WHERE agency = 'NOAA' AND model = 'WW3';

UPDATE models
SET surf = TRUE, swell = TRUE
WHERE agency = 'Wavetrak' AND model = 'LOLA-WW3';

UPDATE models
SET surf = TRUE, swell = TRUE
WHERE agency = 'Wavetrak' AND model = 'Lotus-WW3';

UPDATE models
SET surf = FALSE, swell = TRUE
WHERE agency = 'Wavetrak' AND model = 'Proteus-WW3';
COMMIT;
