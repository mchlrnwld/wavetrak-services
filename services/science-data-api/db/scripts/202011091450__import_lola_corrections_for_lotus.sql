UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.572, breaking_wave_height_intercept = 0.360914
WHERE point_of_interest_id = 'bb096e10-457a-4f54-bfe6-c68651adb26a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6579, breaking_wave_height_intercept = 0.312085
WHERE point_of_interest_id = '71639132-5e23-486e-a244-682ccc8c9b6e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6966, breaking_wave_height_intercept = 0.324917
WHERE point_of_interest_id = '3309f14c-0ad5-40c0-ba8e-d7cdd42703a9';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7759, breaking_wave_height_intercept = 0.272552
WHERE point_of_interest_id = 'b5464a7c-d386-4457-8c73-d5599b4d5866';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6392, breaking_wave_height_intercept = 0.494782
WHERE point_of_interest_id = '04c5ff0a-0059-4cb6-9c10-e0913f8a4447';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6651, breaking_wave_height_intercept = 0.289164
WHERE point_of_interest_id = 'c7c26711-0cd2-41d3-8ac1-b3fc00b9a715';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5009, breaking_wave_height_intercept = 0.420594
WHERE point_of_interest_id = 'af11c886-8eaf-45a5-836d-8860f0904325';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5691, breaking_wave_height_intercept = 0.371947
WHERE point_of_interest_id = '7a53a6b4-38b8-4046-a158-8a135fda10b7';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6596, breaking_wave_height_intercept = 0.289042
WHERE point_of_interest_id = 'f4bbdace-b796-444c-a8a7-35bec70028df';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6797, breaking_wave_height_intercept = 0.303489
WHERE point_of_interest_id = 'd5734df6-716c-439c-a51d-25abc7e9680b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6278, breaking_wave_height_intercept = 0.304434
WHERE point_of_interest_id = '1b548c25-e139-4413-9a96-318ee82c47ab';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.649, breaking_wave_height_intercept = 0.415503
WHERE point_of_interest_id = 'de457141-e17b-48b2-ade6-1994b5802318';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8201, breaking_wave_height_intercept = 0.453146
WHERE point_of_interest_id = 'b67137d5-f3d8-456d-9622-a44e1d2349f6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6726, breaking_wave_height_intercept = 0.395539
WHERE point_of_interest_id = '3aaf0792-c3b1-4239-9b02-4b7c2cd135bb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6049, breaking_wave_height_intercept = 0.3194
WHERE point_of_interest_id = '3161968f-76df-4fae-8517-9cf0bd711e3a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7992, breaking_wave_height_intercept = 0.245577
WHERE point_of_interest_id = 'f8cbda78-0097-4490-8614-0a1266fcb80b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6569, breaking_wave_height_intercept = 0.258989
WHERE point_of_interest_id = '21577ba8-d4f6-4a35-953f-a016dbbb5feb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6569, breaking_wave_height_intercept = 0.258989
WHERE point_of_interest_id = '675e1793-37e5-4472-a203-101de0a2b2ed';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.613, breaking_wave_height_intercept = 0.331226
WHERE point_of_interest_id = 'e31199f6-fb7e-48ff-9abe-076f8ceddd4c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5799, breaking_wave_height_intercept = 0.539953
WHERE point_of_interest_id = '34936aff-8b74-437e-a8a5-4fcd11aaa977';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7706, breaking_wave_height_intercept = 0.53532
WHERE point_of_interest_id = 'b6fd5595-b055-4423-b7f4-cf084adc69f7';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7219, breaking_wave_height_intercept = 0.466984
WHERE point_of_interest_id = '97334798-31c3-4a04-8756-730b01af909c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5204, breaking_wave_height_intercept = 0.393588
WHERE point_of_interest_id = '8e03eb2d-b68a-4aaa-8b68-20e87e019ffe';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6216, breaking_wave_height_intercept = 0.268651
WHERE point_of_interest_id = 'ac592bea-41ab-4f03-bea7-d34b756981f1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5923, breaking_wave_height_intercept = 0.342839
WHERE point_of_interest_id = '1a3d08be-f0d0-41d6-8cf6-eebad2a3ace9';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5559, breaking_wave_height_intercept = 0.329916
WHERE point_of_interest_id = 'eb9fbd41-43c4-4e47-84be-c026311e25ef';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7668, breaking_wave_height_intercept = 0.56897
WHERE point_of_interest_id = 'eb13cbae-ce55-48a1-b5cb-f72d1a71d118';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9188, breaking_wave_height_intercept = 0.635234
WHERE point_of_interest_id = '74a2e897-ab5b-47b1-8edb-5b20a88e4b86';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.411, breaking_wave_height_intercept = 0.439247
WHERE point_of_interest_id = '1ca1e5f0-a208-4456-8d15-cdc0e27b4312';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6598, breaking_wave_height_intercept = 0.609996
WHERE point_of_interest_id = 'ba618885-ab9f-44e2-b37d-d25381ecfc8b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.482, breaking_wave_height_intercept = 0.371978
WHERE point_of_interest_id = '7569e376-b136-4c7b-bf3f-8566657cb411';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4121, breaking_wave_height_intercept = 0.427787
WHERE point_of_interest_id = '47a1c606-6e1f-4def-817c-ee836e5df1ab';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6238, breaking_wave_height_intercept = 0.403311
WHERE point_of_interest_id = 'a783ecf5-ca65-4860-9a19-65936e3ff79a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7003, breaking_wave_height_intercept = 0.630509
WHERE point_of_interest_id = '18136fd0-6aee-4bfb-b411-6dd09d228836';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7421, breaking_wave_height_intercept = 0.447385
WHERE point_of_interest_id = '5a642e2b-5092-46c8-a777-618182876c1c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6312, breaking_wave_height_intercept = 0.462656
WHERE point_of_interest_id = 'fd62c4f6-f44c-4d10-91ed-5dee9914d29b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.1437, breaking_wave_height_intercept = 0.454061
WHERE point_of_interest_id = '874f9274-debe-4f50-a6da-e661715bb24e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5905, breaking_wave_height_intercept = 0.549951
WHERE point_of_interest_id = 'fbe0f1f9-cb38-4969-b791-3e2cf10052ca';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7021, breaking_wave_height_intercept = 0.596006
WHERE point_of_interest_id = '1bfae35f-e291-483e-9dfb-55a51d785ba6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7354, breaking_wave_height_intercept = 0.271028
WHERE point_of_interest_id = 'fb681066-f3c4-41c8-9f46-359b240d071d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6836, breaking_wave_height_intercept = 0.547908
WHERE point_of_interest_id = '097b3ab2-386f-48ad-b5e4-f2855a33b8ce';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6919, breaking_wave_height_intercept = 0.434188
WHERE point_of_interest_id = '1d0fc5e5-3602-4388-b493-5f456fe6daef';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6129, breaking_wave_height_intercept = 0.294864
WHERE point_of_interest_id = 'ac32539e-93b1-4a51-a5c1-77332fcfe2de';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5299, breaking_wave_height_intercept = 0.4864
WHERE point_of_interest_id = '81d8f69f-541f-4660-8e5d-97f6d018dc1a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5394, breaking_wave_height_intercept = 0.471465
WHERE point_of_interest_id = '8b3c745b-6864-44db-9458-030a9201eeb6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5156, breaking_wave_height_intercept = 0.495117
WHERE point_of_interest_id = '8c3615ba-7538-4e64-b697-4be94045d28b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3934, breaking_wave_height_intercept = 0.329733
WHERE point_of_interest_id = '20737401-a018-40e1-8cdc-005a45e0dac3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.575, breaking_wave_height_intercept = 0.323362
WHERE point_of_interest_id = '9195d9b8-0330-4b49-aed7-b8d7efcefea4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5244, breaking_wave_height_intercept = 0.34162
WHERE point_of_interest_id = '50e0a68a-2f69-4341-9a73-dc13b036ca92';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.614, breaking_wave_height_intercept = 0.414284
WHERE point_of_interest_id = 'fa322b80-e305-49f4-a74a-acfd79c2e06a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3647, breaking_wave_height_intercept = 0.334457
WHERE point_of_interest_id = '039606cd-355c-4ab2-805c-df5d4060ac42';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6426, breaking_wave_height_intercept = 0.430408
WHERE point_of_interest_id = '0342ae58-0e91-4dfa-80ef-c69b635c9cbb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5484, breaking_wave_height_intercept = 0.304587
WHERE point_of_interest_id = '8fbb9b83-34e8-4bac-9625-6383a7604ef7';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.691, breaking_wave_height_intercept = 0.365272
WHERE point_of_interest_id = 'dde62fc8-0bb9-4fce-91bb-e29d70169d70';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8162, breaking_wave_height_intercept = 0.216957
WHERE point_of_interest_id = '42e7eea6-1d0b-4ccc-bb68-672c2c724630';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7632, breaking_wave_height_intercept = 0.371277
WHERE point_of_interest_id = 'fb21dbb9-549e-4f1f-845e-802aa310d529';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7584, breaking_wave_height_intercept = 0.308549
WHERE point_of_interest_id = 'a40b91b4-d510-46f2-8ac1-f636320d1e2c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7331, breaking_wave_height_intercept = 0.4921
WHERE point_of_interest_id = '599f9642-de93-42cb-ac97-8e9a5196e509';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.616, breaking_wave_height_intercept = 0.546445
WHERE point_of_interest_id = '5fed5200-93df-44bc-a2b4-a6713417968f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9643, breaking_wave_height_intercept = 0.552115
WHERE point_of_interest_id = '80a6946e-0ad0-48c6-9197-7e30f5f1bf04';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7507, breaking_wave_height_intercept = 0.413553
WHERE point_of_interest_id = '3ed9d121-1c14-4386-9cdd-0842046f8bba';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7774, breaking_wave_height_intercept = 0.45973
WHERE point_of_interest_id = '9235cfea-a780-45f1-b4fd-3e4b68c41c08';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4644, breaking_wave_height_intercept = 0.72329
WHERE point_of_interest_id = '00d73943-44cf-4682-bdd4-a7fce21a5210';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7485, breaking_wave_height_intercept = 0.622219
WHERE point_of_interest_id = '823837ed-b879-413f-b459-9656e5ad86c5';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5465, breaking_wave_height_intercept = 0.557571
WHERE point_of_interest_id = '597a8193-ade2-4cbe-9af9-e8a3cb4b6efe';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.619, breaking_wave_height_intercept = 0.543428
WHERE point_of_interest_id = '9657ff2f-4969-458d-895a-7d4784af16d3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5145, breaking_wave_height_intercept = 0.730453
WHERE point_of_interest_id = 'ad1f95a7-0918-4c2b-91c9-29c31321c9db';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3965, breaking_wave_height_intercept = 0.422544
WHERE point_of_interest_id = 'f3948d70-930c-4e77-b29c-6bec354ecb83';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5796, breaking_wave_height_intercept = 0.603931
WHERE point_of_interest_id = '107aa167-ed75-4214-85d4-7479a4bc6c95';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5293, breaking_wave_height_intercept = 0.607771
WHERE point_of_interest_id = 'dcd72e40-ef04-4ec1-9217-d8c828ede0af';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7249, breaking_wave_height_intercept = 0.48387
WHERE point_of_interest_id = '72805a0b-cb82-4648-9820-e026b592b9d4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.882, breaking_wave_height_intercept = 0.242895
WHERE point_of_interest_id = '8da8bf6d-796f-49c0-a429-40a060b87487';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6039, breaking_wave_height_intercept = 0.462625
WHERE point_of_interest_id = 'ef912110-4c65-4019-844f-3038d961e5e2';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9098, breaking_wave_height_intercept = 0.395722
WHERE point_of_interest_id = '4922d7db-d689-418a-a953-e7ebeedc5bb9';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7538, breaking_wave_height_intercept = 0.21845
WHERE point_of_interest_id = '307f17a5-31a1-4ec1-af7d-7f194a695f8f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9777, breaking_wave_height_intercept = 0.435955
WHERE point_of_interest_id = 'fa2fd23d-82e3-4e42-baba-2bd9ce6c9e92';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9085, breaking_wave_height_intercept = 0.393192
WHERE point_of_interest_id = '0106a777-692c-4874-953a-0eacb14eb1b6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5366, breaking_wave_height_intercept = 0.145969
WHERE point_of_interest_id = 'e27c27b8-5f2b-47c9-bc7b-1d56beb16ef4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8233, breaking_wave_height_intercept = 0.054193
WHERE point_of_interest_id = 'e5de51e5-fe3d-41d4-a8de-c592422c50a7';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5755, breaking_wave_height_intercept = 0.433121
WHERE point_of_interest_id = '9986552e-10af-4990-bfad-34e4a2eb237d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6233, breaking_wave_height_intercept = 0.769407
WHERE point_of_interest_id = '4ffb7eba-3a2a-4136-9121-5f71d1a6e042';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6294, breaking_wave_height_intercept = 0.532364
WHERE point_of_interest_id = '33ad7496-06e7-4f0c-bc45-4f3ed2c71844';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.1119, breaking_wave_height_intercept = 0.423459
WHERE point_of_interest_id = '31ce9798-3732-4b54-a5ed-04ab27100a43';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9769, breaking_wave_height_intercept = 0.483078
WHERE point_of_interest_id = '1a111085-322d-42c0-bc7d-526c631b6956';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6832, breaking_wave_height_intercept = 0.520751
WHERE point_of_interest_id = '99980e82-78e1-4a20-aba4-910c63b24b0d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6092, breaking_wave_height_intercept = 0.393497
WHERE point_of_interest_id = 'dfdfefd7-0c49-4e13-bdbd-5427aede8363';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.744, breaking_wave_height_intercept = 0.44897
WHERE point_of_interest_id = 'c0943b6e-a7c4-43bd-8319-a475eadc9e2f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9292, breaking_wave_height_intercept = 0.100493
WHERE point_of_interest_id = '1b785a4a-e877-4149-8b6f-71da9baa8935';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.2073, breaking_wave_height_intercept = 0.419009
WHERE point_of_interest_id = 'f82dc061-7562-4f24-bd24-3aa6b4906d90';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7574, breaking_wave_height_intercept = 0.457657
WHERE point_of_interest_id = '07d729d3-1867-4290-a194-ecfbbcbb946b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6282, breaking_wave_height_intercept = 0.45278
WHERE point_of_interest_id = '1975e08e-4d61-4808-ab3d-94ad2e3928ff';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4401, breaking_wave_height_intercept = 0.388681
WHERE point_of_interest_id = '7152cb4c-a381-4540-9e15-7f6efce7d414';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6652, breaking_wave_height_intercept = 0.545165
WHERE point_of_interest_id = '758543cf-f0b1-4342-bcb2-71435ec45265';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6351, breaking_wave_height_intercept = 0.505084
WHERE point_of_interest_id = 'c6f36934-8c52-4fee-91f5-5bb4cf402e63';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5511, breaking_wave_height_intercept = 0.480487
WHERE point_of_interest_id = '0aa48b4e-1193-4252-a271-b198dc5430b0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5499, breaking_wave_height_intercept = 0.465369
WHERE point_of_interest_id = 'd4cfc3cf-8c9d-4b78-a67c-623b79905e1c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6016, breaking_wave_height_intercept = 0.308275
WHERE point_of_interest_id = '53858d9d-1d18-414b-8d75-e7ff8ccff7de';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5995, breaking_wave_height_intercept = 0.553761
WHERE point_of_interest_id = '979673c6-4d7b-4973-b479-f261c428c083';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6913, breaking_wave_height_intercept = 0.5715
WHERE point_of_interest_id = 'a38480bc-a695-47a7-b1a1-99372b9562cb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5532, breaking_wave_height_intercept = 0.477317
WHERE point_of_interest_id = '6f8ec669-ca76-490b-b1a7-a48506c122c3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7852, breaking_wave_height_intercept = 0.399379
WHERE point_of_interest_id = 'c8582533-7d03-4fab-a54d-d809ad804959';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7852, breaking_wave_height_intercept = 0.399379
WHERE point_of_interest_id = 'f0f372a2-37de-4bc6-9713-8f65b7314604';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8174, breaking_wave_height_intercept = 0.487528
WHERE point_of_interest_id = 'c692f308-3974-4a5f-915b-1a9ac6cd9d7e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3879, breaking_wave_height_intercept = 0.276911
WHERE point_of_interest_id = 'dbb98f0f-d6cf-4a20-84ab-11758b1457d1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8168, breaking_wave_height_intercept = 0.372831
WHERE point_of_interest_id = '92068ede-4751-4667-807a-c1dc34d511bb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.715, breaking_wave_height_intercept = 0.608259
WHERE point_of_interest_id = '8e6cb54a-5efd-4d52-b692-c52b733512bd';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8168, breaking_wave_height_intercept = 0.372831
WHERE point_of_interest_id = 'd3704d39-c2e0-44ac-8293-820f6cb04c31';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3867, breaking_wave_height_intercept = 0.27874
WHERE point_of_interest_id = '1efac385-1a72-4004-88be-164fb48765ca';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3911, breaking_wave_height_intercept = 0.387736
WHERE point_of_interest_id = 'c5fc30a6-cc34-49f3-96c9-820bf01951d4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7371, breaking_wave_height_intercept = 0.502371
WHERE point_of_interest_id = '67f2c4c8-a87f-44ef-b9a0-ea3fd9f5d28e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6876, breaking_wave_height_intercept = 0.532364
WHERE point_of_interest_id = '242cfb70-8a6d-4ea1-b726-78e47e8d7fb5';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.826, breaking_wave_height_intercept = 0.362133
WHERE point_of_interest_id = '389ad052-a572-4bcb-af72-c12a178abe6f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4399, breaking_wave_height_intercept = 0.105369
WHERE point_of_interest_id = 'fd26051e-ba79-4646-9eff-b4148a1470c2';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6418, breaking_wave_height_intercept = 0.337139
WHERE point_of_interest_id = '1fe297b7-322d-4470-ae7d-79f7cca281b5';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4053, breaking_wave_height_intercept = 0.486247
WHERE point_of_interest_id = '3b6176cb-7b11-4efd-88fd-1639961071a0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6947, breaking_wave_height_intercept = 0.248839
WHERE point_of_interest_id = '658d195a-2f47-412b-8385-a19c81ccaaa6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6451, breaking_wave_height_intercept = 0.330525
WHERE point_of_interest_id = 'ff6bae74-be21-4b3f-b42d-6092d3555877';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.931, breaking_wave_height_intercept = 0.294162
WHERE point_of_interest_id = '28d13bc8-2e62-4310-8d2d-1fa8fc0cc6d8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7602, breaking_wave_height_intercept = 0.333085
WHERE point_of_interest_id = 'e723697f-91c5-4c23-a374-10be33162a65';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7607, breaking_wave_height_intercept = 0.448879
WHERE point_of_interest_id = '01849e1c-64be-4460-9aad-4883edbe0029';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0363, breaking_wave_height_intercept = 0.369357
WHERE point_of_interest_id = '94d35249-30f8-4c97-af3d-4da0706c8127';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7043, breaking_wave_height_intercept = 0.537911
WHERE point_of_interest_id = 'f71486a4-df96-430d-bcce-5f44910fef2c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6421, breaking_wave_height_intercept = 0.399318
WHERE point_of_interest_id = '490c366e-e1dc-4511-9034-619a344b84e1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0476, breaking_wave_height_intercept = 0.348082
WHERE point_of_interest_id = '5c620be9-c04c-4b46-bef5-ab297efac474';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5607, breaking_wave_height_intercept = 0.131765
WHERE point_of_interest_id = '03d35317-1ba8-4203-b319-07748ea059e0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4233, breaking_wave_height_intercept = 0.374508
WHERE point_of_interest_id = '6e69a6dc-73f7-4e4d-99d2-63121faf5153';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7323, breaking_wave_height_intercept = 0.34101
WHERE point_of_interest_id = 'b9b292a1-b5a0-4454-b20b-e5d477997b4c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7441, breaking_wave_height_intercept = 0.340248
WHERE point_of_interest_id = '548a4ca3-a289-48f1-8279-2fb2281baac4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.639, breaking_wave_height_intercept = 0.531906
WHERE point_of_interest_id = '4bbef5e6-37e6-4b93-ac4b-0a4dd716b56b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4483, breaking_wave_height_intercept = 0.263682
WHERE point_of_interest_id = '86fe0589-2a90-4648-8ca3-5a911e688799';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6183, breaking_wave_height_intercept = 0.644103
WHERE point_of_interest_id = '5fee6076-8651-4b83-9b2e-8e288bd502b5';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8065, breaking_wave_height_intercept = 0.421508
WHERE point_of_interest_id = 'ae143d5f-c9e7-4ef5-8cd2-4cd58318b6ac';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6073, breaking_wave_height_intercept = 0.560741
WHERE point_of_interest_id = '54c8c34b-e692-4302-9832-3caf43772f09';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7242, breaking_wave_height_intercept = 0.469118
WHERE point_of_interest_id = 'd691f821-5248-4ad2-a266-866b4dfaae29';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7303, breaking_wave_height_intercept = 0.486034
WHERE point_of_interest_id = '79fb8b0f-4cad-4c78-8f3c-41e30f765708';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6649, breaking_wave_height_intercept = 0.351069
WHERE point_of_interest_id = '1f9a0512-e6d7-4e04-adf0-d3fab1922b78';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.677, breaking_wave_height_intercept = 0.328452
WHERE point_of_interest_id = '912b5ef0-e8ba-4994-8af2-1dd909cf91d1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7723, breaking_wave_height_intercept = 0.423337
WHERE point_of_interest_id = '6a9334ab-1731-4707-a524-6345a28a828d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6063, breaking_wave_height_intercept = 0.283677
WHERE point_of_interest_id = 'cdc44b4e-1d37-4525-a5f4-34b74dee0fe2';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5933, breaking_wave_height_intercept = 0.196322
WHERE point_of_interest_id = '32528259-886e-45be-813a-7a82bbf4f570';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6292, breaking_wave_height_intercept = 0.313487
WHERE point_of_interest_id = '807bb422-4317-437c-ac96-a163c1ff852f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.601, breaking_wave_height_intercept = 0.52514
WHERE point_of_interest_id = '9d78835b-d7b3-451f-a81e-0008c28535e0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7904, breaking_wave_height_intercept = 0.033193
WHERE point_of_interest_id = '15a820c6-d9e8-4cea-ae0d-b8ab1dea0076';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3872, breaking_wave_height_intercept = -0.030389
WHERE point_of_interest_id = '848f46b9-b49e-49a0-bbd5-d164e60a9526';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6607, breaking_wave_height_intercept = 0.63249
WHERE point_of_interest_id = '0a11c8eb-65d4-41a4-9b54-8b42b6a31df2';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7121, breaking_wave_height_intercept = 0.474939
WHERE point_of_interest_id = '304056a1-e6fc-4eab-bd63-1f59a4e54984';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7454, breaking_wave_height_intercept = 0.444947
WHERE point_of_interest_id = '63bb8676-57ad-405b-b5c3-53d73fee4e42';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.2105, breaking_wave_height_intercept = 0.381091
WHERE point_of_interest_id = 'b0be8466-3a08-4308-8966-8e98299e8e52';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6018, breaking_wave_height_intercept = 0.460065
WHERE point_of_interest_id = '0c0a6194-a16b-457e-8576-5b38eeff42e9';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4911, breaking_wave_height_intercept = 0.101041
WHERE point_of_interest_id = 'e4e61db9-237b-4542-bde2-40258c00668f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6438, breaking_wave_height_intercept = 0.290779
WHERE point_of_interest_id = '23c4f9e7-27ff-4464-aeb6-693bfc4bc7e8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6813, breaking_wave_height_intercept = 0.287274
WHERE point_of_interest_id = '7a445f68-80c9-4553-8a9d-aed718f04d4f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3653, breaking_wave_height_intercept = 0.741883
WHERE point_of_interest_id = 'f93a4697-3623-4d04-9c13-09269ccf19ed';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6265, breaking_wave_height_intercept = 0.310012
WHERE point_of_interest_id = '0abddd43-8447-4276-bf61-476dd60cfe4e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5477, breaking_wave_height_intercept = 0.363169
WHERE point_of_interest_id = '9c4be0cb-19ef-4725-adb9-d6d93c4166c9';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6906, breaking_wave_height_intercept = 0.388468
WHERE point_of_interest_id = '01bb998a-42d8-483d-a4c9-082fffb5e694';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6642, breaking_wave_height_intercept = 0.392948
WHERE point_of_interest_id = '096c8dd6-4be9-4617-aa38-2882e87dcf8d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6746, breaking_wave_height_intercept = 0.359359
WHERE point_of_interest_id = 'd2b3b726-48e1-4110-b2b4-5b37ce1bce5e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7236, breaking_wave_height_intercept = 0.312054
WHERE point_of_interest_id = '47f99647-dafd-43ae-bc89-152988df6aec';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6745, breaking_wave_height_intercept = 0.307787
WHERE point_of_interest_id = 'b5d33be5-8958-44cc-b30e-91b4995add6a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.731, breaking_wave_height_intercept = 0.296083
WHERE point_of_interest_id = '1b4f30d4-b101-4a5a-a86c-714240afc8e0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6542, breaking_wave_height_intercept = 0.30797
WHERE point_of_interest_id = '10b14334-e872-4b71-a8f2-0ad1f3d48ca2';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.751, breaking_wave_height_intercept = 0.317083
WHERE point_of_interest_id = 'a93470a0-7ef0-4f45-8cef-64244cf2e957';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7277, breaking_wave_height_intercept = 0.280812
WHERE point_of_interest_id = 'a9ee8883-1d93-4702-8f12-fafdefa1b443';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6389, breaking_wave_height_intercept = 0.083485
WHERE point_of_interest_id = 'fde33c81-ba2c-4e24-a025-887513b3ee0f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8323, breaking_wave_height_intercept = 0.484236
WHERE point_of_interest_id = '13befde9-cc49-479a-9a9e-80a1b7cde4f4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6919, breaking_wave_height_intercept = 0.571927
WHERE point_of_interest_id = '7da585be-6059-4d33-a1d0-ae8c2ee4e9bb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7577, breaking_wave_height_intercept = 0.504993
WHERE point_of_interest_id = 'de0697d2-80f7-48f2-b799-68852f5a4992';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7415, breaking_wave_height_intercept = 0.558241
WHERE point_of_interest_id = '48e7a2e1-7da6-4556-a77a-3655edb0aeae';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6856, breaking_wave_height_intercept = 0.346801
WHERE point_of_interest_id = '324e5354-9c13-4b65-b695-4522acfd4e50';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8264, breaking_wave_height_intercept = 0.24829
WHERE point_of_interest_id = 'a354b4f7-ea24-46b6-abf6-5f20fe958116';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8232, breaking_wave_height_intercept = 0.152979
WHERE point_of_interest_id = 'e3619925-4a4f-4230-8997-535c47f97163';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7581, breaking_wave_height_intercept = 0.279136
WHERE point_of_interest_id = '403911c8-ac8c-4fc8-b274-a1bde4bc5817';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5928, breaking_wave_height_intercept = 0.328148
WHERE point_of_interest_id = '0dbcdc93-fc65-4802-ac49-89acd05fb9d8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.2008, breaking_wave_height_intercept = 0.434523
WHERE point_of_interest_id = '5bcb4b08-24e2-4f5b-883f-18735aab0acb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3908, breaking_wave_height_intercept = 0.519318
WHERE point_of_interest_id = '067db985-bec9-4510-92f2-d01695d9046b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7393, breaking_wave_height_intercept = 0.453786
WHERE point_of_interest_id = 'df5827cb-8077-44ab-813b-7c091968d9d0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7387, breaking_wave_height_intercept = 0.453817
WHERE point_of_interest_id = 'cd718b1d-4314-4bc5-9a11-2f50dd7a3a2d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3743, breaking_wave_height_intercept = 0.388498
WHERE point_of_interest_id = 'd8f5524a-c7d7-4b5a-839b-ab10d6485449';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7053, breaking_wave_height_intercept = 0.66931
WHERE point_of_interest_id = '0078bfa0-3f72-45c1-8f1a-dbdef71191ad';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.408, breaking_wave_height_intercept = 0.50356
WHERE point_of_interest_id = 'b714fcf3-533b-4ea5-a34f-1d4ff6a42b68';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7008, breaking_wave_height_intercept = 0.552206
WHERE point_of_interest_id = 'da3260e0-b61c-40b1-95e9-283f6b6898f6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3816, breaking_wave_height_intercept = 0.332872
WHERE point_of_interest_id = '362a3d28-a6cf-4646-bf1d-e1c3f19b3053';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6948, breaking_wave_height_intercept = 0.488381
WHERE point_of_interest_id = 'f087cf78-325b-491f-a7ac-fa90f18e6b5d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7743, breaking_wave_height_intercept = 0.377678
WHERE point_of_interest_id = 'd19fdacf-3509-4826-a9ba-29b490eb0267';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5828, breaking_wave_height_intercept = 0.305592
WHERE point_of_interest_id = '03534b11-7b6e-4b6b-bb50-c80f0801e8eb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6324, breaking_wave_height_intercept = 0.294315
WHERE point_of_interest_id = '884562c4-a5b6-47f9-a864-585d6f1a3670';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5446, breaking_wave_height_intercept = 0.328605
WHERE point_of_interest_id = '614b5646-0f66-445f-a450-af3da76ebe04';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6399, breaking_wave_height_intercept = 0.430195
WHERE point_of_interest_id = 'a2d52f79-ceb5-45c1-8dda-020e0af266d9';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5805, breaking_wave_height_intercept = 0.456316
WHERE point_of_interest_id = 'fd7a4954-8baf-401a-af2e-32feba7a0c46';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9592, breaking_wave_height_intercept = 0.507157
WHERE point_of_interest_id = 'ee457975-fcc6-47ab-8dc9-f9b8f8f52d83';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5308, breaking_wave_height_intercept = 0.51944
WHERE point_of_interest_id = '3aa83022-596f-483f-b0a5-f18ed4c82e82';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5859, breaking_wave_height_intercept = 0.305958
WHERE point_of_interest_id = 'da6f5dac-4c97-4d15-9de4-ac3cb1e138b8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5082, breaking_wave_height_intercept = 0.523524
WHERE point_of_interest_id = '3cf0b264-0b06-4b53-9259-8b6c7e4c0e26';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5369, breaking_wave_height_intercept = 0.506364
WHERE point_of_interest_id = 'ed755515-1fe5-4afe-b7b7-53caed287f57';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9538, breaking_wave_height_intercept = 0.510022
WHERE point_of_interest_id = '16acd481-d19d-4759-92f7-c5dee5cc4161';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5714, breaking_wave_height_intercept = 0.09967
WHERE point_of_interest_id = '4a20bc47-17bf-48d7-ac07-b96042be0e6b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4976, breaking_wave_height_intercept = 0.327843
WHERE point_of_interest_id = '95dc78fe-62d4-42bd-826e-387e1489166b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5361, breaking_wave_height_intercept = 0.268072
WHERE point_of_interest_id = '5adc77eb-10a9-4a6b-bd5b-4fb875e97574';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.522, breaking_wave_height_intercept = 0.297302
WHERE point_of_interest_id = 'bf35a4f9-f930-4cc6-8793-3ffa5350b52f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.997, breaking_wave_height_intercept = 0.159807
WHERE point_of_interest_id = '0853fffd-f03f-411f-a0fa-7f045f02420e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6327, breaking_wave_height_intercept = 0.528401
WHERE point_of_interest_id = 'fcc49a49-f7b0-45ec-9e85-fe28a721d87a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0501, breaking_wave_height_intercept = 0.297546
WHERE point_of_interest_id = 'f94773f4-9317-4c0c-8d05-53aa55779da0';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7767, breaking_wave_height_intercept = 0.168402
WHERE point_of_interest_id = '6cf8acca-64d6-42e1-bb26-580c68303da3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7987, breaking_wave_height_intercept = 0.17334
WHERE point_of_interest_id = '1357f2b2-6ccd-43a4-b5be-607a9e1c3c3a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8251, breaking_wave_height_intercept = -0.038527
WHERE point_of_interest_id = '8bcc471a-48f2-42f6-ac41-1390aa92bf7a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6666, breaking_wave_height_intercept = 0.085588
WHERE point_of_interest_id = '34c222c3-1d3c-4335-8312-6deec96331e5';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8412, breaking_wave_height_intercept = -0.050658
WHERE point_of_interest_id = 'f3b26753-d3f2-4cab-a89d-078be5630140';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7948, breaking_wave_height_intercept = 0.433182
WHERE point_of_interest_id = 'aa0e9114-f466-4e53-8bfd-37e54b7c4ac8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0103, breaking_wave_height_intercept = 0.422392
WHERE point_of_interest_id = '0b1f7d00-fa64-4c98-9d3d-18cf63bd79f4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6745, breaking_wave_height_intercept = 0.360365
WHERE point_of_interest_id = 'd44eedf0-db8d-48d9-a53a-c678f79ef803';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8794, breaking_wave_height_intercept = 0.389626
WHERE point_of_interest_id = '1e470c8a-9053-43b3-b5cb-d042ef66f1b7';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.546, breaking_wave_height_intercept = 0.3543
WHERE point_of_interest_id = 'dca89647-ab7f-4078-a0eb-c2f7534158a4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9091, breaking_wave_height_intercept = 0.391058
WHERE point_of_interest_id = '71376d01-e57e-412e-a9e1-574be277b170';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4124, breaking_wave_height_intercept = 0.39182
WHERE point_of_interest_id = '5223d85f-7502-417a-96b9-475bc70658c8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5254, breaking_wave_height_intercept = 0.338298
WHERE point_of_interest_id = 'fa423624-65c9-4b7c-a644-c277483ebd22';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9299, breaking_wave_height_intercept = 0.520568
WHERE point_of_interest_id = '106d9e98-ba90-41b7-8b69-3b6f44117a96';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4093, breaking_wave_height_intercept = 0.617403
WHERE point_of_interest_id = '0fea1803-8245-4967-9029-b6a1858b3807';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5767, breaking_wave_height_intercept = 0.314767
WHERE point_of_interest_id = 'ff0dc3c2-80a5-4dd9-94b4-63a8c9ff94bb';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6941, breaking_wave_height_intercept = 0.658856
WHERE point_of_interest_id = 'de8723a1-20ce-48b8-a47c-057ab98257d1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8375, breaking_wave_height_intercept = 0.428884
WHERE point_of_interest_id = '128ebad5-37c2-4b73-a701-f6a18b86a709';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5116, breaking_wave_height_intercept = 0.491612
WHERE point_of_interest_id = 'f27e609b-f54e-4d53-93cb-9cf9cba01187';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9172, breaking_wave_height_intercept = 0.478506
WHERE point_of_interest_id = 'cfc69c52-553d-4b01-ba43-172c1df35c82';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.887, breaking_wave_height_intercept = 0.458358
WHERE point_of_interest_id = '3306e28c-6b2c-4bae-8ba6-4215195b9d7a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5514, breaking_wave_height_intercept = 0.573816
WHERE point_of_interest_id = '47a29264-ac96-4a09-ae61-235d984d75df';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5415, breaking_wave_height_intercept = 0.061813
WHERE point_of_interest_id = 'd1593e38-f6c6-45f2-bf33-9c68593ba194';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4955, breaking_wave_height_intercept = 0.225918
WHERE point_of_interest_id = 'c8cce03c-4271-4ab6-a29c-8dcd7099ad07';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.441, breaking_wave_height_intercept = 0.488777
WHERE point_of_interest_id = '30ea36d3-6dba-47c6-a8fd-d0eaca705f84';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4794, breaking_wave_height_intercept = 0.349667
WHERE point_of_interest_id = '752ceec0-c40c-4d67-a951-c19efd2409b3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4946, breaking_wave_height_intercept = 0.326715
WHERE point_of_interest_id = '5fcd26bd-e324-4251-acfe-66356052ad5f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7461, breaking_wave_height_intercept = 0.216042
WHERE point_of_interest_id = 'aea69e02-6873-4516-ae63-cdd619e988ab';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.971, breaking_wave_height_intercept = 0.430225
WHERE point_of_interest_id = 'dd3fbd8d-7125-43e7-a551-9506a92b6b15';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7094, breaking_wave_height_intercept = 0.226832
WHERE point_of_interest_id = '006b13e1-bf67-464d-ace7-8c9c75874863';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7346, breaking_wave_height_intercept = 0.241859
WHERE point_of_interest_id = 'e57016a8-009c-4fd9-9f33-f4b74d0d3ef4';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7081, breaking_wave_height_intercept = 0.240213
WHERE point_of_interest_id = '5718a9db-4f81-4b56-87cf-360cc57ac4b7';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7093, breaking_wave_height_intercept = 0.226741
WHERE point_of_interest_id = '49d7ad3a-3d9c-451c-8fcf-49ce2c90af9c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7719, breaking_wave_height_intercept = 0.232562
WHERE point_of_interest_id = 'a6d15592-ee30-4686-b1a8-236c3600b460';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5787, breaking_wave_height_intercept = 0.182667
WHERE point_of_interest_id = '496f8b8c-62c9-407a-98ec-91554b691418';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5781, breaking_wave_height_intercept = 0.208453
WHERE point_of_interest_id = '27f75391-530b-4da3-9f03-6ef64c5fb428';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.572, breaking_wave_height_intercept = 0.179314
WHERE point_of_interest_id = 'f9b85da4-abbe-4063-98b3-39c7f661de23';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0731, breaking_wave_height_intercept = 0.288066
WHERE point_of_interest_id = 'ebec19f0-9e86-4fd4-bade-41b89dba50c6';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6611, breaking_wave_height_intercept = 0.304922
WHERE point_of_interest_id = 'fd46dc1a-f1fa-4d13-a137-e07c654a9b50';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7718, breaking_wave_height_intercept = 0.235702
WHERE point_of_interest_id = '3efd9e1a-1b25-4182-a84b-c82fd4b81ee8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7946, breaking_wave_height_intercept = 0.05145
WHERE point_of_interest_id = '1547d8d4-8c8e-4afb-8549-84bd459ed647';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0981, breaking_wave_height_intercept = 0.305318
WHERE point_of_interest_id = '0be22071-c590-4fec-ad55-454118482a63';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.58, breaking_wave_height_intercept = 0.305684
WHERE point_of_interest_id = '963db2bc-caaa-4fa8-98cb-222c7ba69351';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6421, breaking_wave_height_intercept = 0.32324
WHERE point_of_interest_id = '5837c59b-17f5-482b-b3dc-fb22e73df7e8';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5314, breaking_wave_height_intercept = 0.407487
WHERE point_of_interest_id = '5ad6382d-4c75-41c9-8b07-b3a2d098b24e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6618, breaking_wave_height_intercept = 0.319766
WHERE point_of_interest_id = '9451d00d-0356-4d06-8e6e-8c43e39062e1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7009, breaking_wave_height_intercept = 0.275265
WHERE point_of_interest_id = 'ef5b06cd-adab-401d-9185-4d92ddaa5bff';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5846, breaking_wave_height_intercept = 0.197693
WHERE point_of_interest_id = 'd9d91865-1d05-40f0-b3fa-313677fef774';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.605, breaking_wave_height_intercept = 0.335097
WHERE point_of_interest_id = '44e4b156-695e-4a8f-ae87-935e429cae29';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5575, breaking_wave_height_intercept = 0.336042
WHERE point_of_interest_id = '24289eb0-d8a0-4b74-8fde-ebaef12b51c3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7256, breaking_wave_height_intercept = -0.299679
WHERE point_of_interest_id = 'b864132b-1f48-45bf-b5e8-336678270312';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6894, breaking_wave_height_intercept = -0.256489
WHERE point_of_interest_id = 'a1abfaab-2c3d-476e-ad24-f93247734844';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.713, breaking_wave_height_intercept = 0.445526
WHERE point_of_interest_id = '5ae78ba2-452a-4cab-aba9-34952780389d';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9135, breaking_wave_height_intercept = 0.214914
WHERE point_of_interest_id = '30e5523a-da4a-4fd8-8804-b230881d9a15';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.4757, breaking_wave_height_intercept = 0.381975
WHERE point_of_interest_id = 'a48e5e16-6d27-48bd-b84d-f4709e688a2c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0933, breaking_wave_height_intercept = 0.207142
WHERE point_of_interest_id = 'c0003455-8f23-4c1b-9b29-26ddf3ed4d34';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.0431, breaking_wave_height_intercept = 0.250149
WHERE point_of_interest_id = '2614d76b-aaef-48fc-851d-764e3fa5061a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5827, breaking_wave_height_intercept = 0.430408
WHERE point_of_interest_id = '0d5c7268-1e27-4340-b269-7f73071d940c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5455, breaking_wave_height_intercept = 0.355549
WHERE point_of_interest_id = 'e8b894e8-8519-45d7-b674-aa0c388a1923';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6452, breaking_wave_height_intercept = 0.583905
WHERE point_of_interest_id = 'a910d52a-1840-4149-9c46-12c911a805bf';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7244, breaking_wave_height_intercept = 0.31751
WHERE point_of_interest_id = '2b6d77bc-00dd-45be-aeb9-96b548c569b5';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5581, breaking_wave_height_intercept = 0.519379
WHERE point_of_interest_id = '0ccd031d-7db2-4f97-973a-c83b02d9c50a';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.3824, breaking_wave_height_intercept = 0.381823
WHERE point_of_interest_id = '7e0ddde3-f1cc-452a-9a2d-32f740a9c0e2';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6145, breaking_wave_height_intercept = 0.1638
WHERE point_of_interest_id = 'd2c3f04d-de8b-4482-9975-e2307ef4b807';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7553, breaking_wave_height_intercept = 0.281269
WHERE point_of_interest_id = '2b6eb7e7-23ee-46a2-8c00-b2883a6c902c';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7948, breaking_wave_height_intercept = 0.224699
WHERE point_of_interest_id = '40cbdb38-66c3-46bd-ac7e-4ad46a2a9ec3';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.8012, breaking_wave_height_intercept = 0.0794
WHERE point_of_interest_id = 'e87b7c61-1851-471e-b2c3-acbc0781930f';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7807, breaking_wave_height_intercept = 0.387218
WHERE point_of_interest_id = '6802b125-8653-42e7-b977-7471fa3ede62';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.8316, breaking_wave_height_intercept = 0.395082
WHERE point_of_interest_id = '52c1322d-5584-446c-98d7-2e5de3a8da7b';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.7861, breaking_wave_height_intercept = 0.190195
WHERE point_of_interest_id = '087668d8-0a2f-45dc-ad9f-f890ecb8a0a1';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6154, breaking_wave_height_intercept = 0.108326
WHERE point_of_interest_id = '59f6e159-62ef-412f-b2db-0e45b1efb4be';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.7626, breaking_wave_height_intercept = 0.265359
WHERE point_of_interest_id = '9f8c8c85-b1fa-4ae1-8933-b9cbd98b8447';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.9977, breaking_wave_height_intercept = 0.247437
WHERE point_of_interest_id = 'e3ea6d98-ffa3-4bef-b25e-01b8fdc1dd09';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.6326, breaking_wave_height_intercept = 0.579364
WHERE point_of_interest_id = '591b361a-f9ae-402f-9dbc-049680c2c27e';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 0.5431, breaking_wave_height_intercept = 0.566105
WHERE point_of_interest_id = '564d20d5-a2fb-4fe4-b3b9-c16602e19b82';
UPDATE surf_spot_grid_point_configurations
SET breaking_wave_height_coefficient = 1.4069, breaking_wave_height_intercept = -0.191201
WHERE point_of_interest_id = '9191ec7e-8cf6-4029-8058-7b156e9013a2';
