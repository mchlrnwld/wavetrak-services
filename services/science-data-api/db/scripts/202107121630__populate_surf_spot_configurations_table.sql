/*
 Date: 2021-07-12
 Author: Tom Portwood
 Description:
 Populate the new 'surf_spot_configurations' table from the existing 'surf_spot_grid_point_configurations' table.
 */

BEGIN;

INSERT INTO surf_spot_configurations
SELECT poi.id,
       ssgpc.offshore_direction,
       ssgpc.optimal_swell_direction,
       ssgpc.breaking_wave_height_coefficient,
       ssgpc.breaking_wave_height_intercept,
       ssgpc.spectral_refraction_matrix,
       ssgpc.breaking_wave_height_algorithm,
       ssgpc.created_at,
       ssgpc.updated_at
FROM points_of_interest poi
         INNER JOIN points_of_interest_default_forecast_models poidfm
                    ON poi.id = poidfm.point_of_interest_id
                        AND poidfm.forecast_type = 'SURF'
         INNER JOIN surf_spot_grid_point_configurations ssgpc
                    ON poidfm.agency = ssgpc.agency
                        AND poidfm.model = ssgpc.model
                        AND poidfm.grid = ssgpc.grid
                        AND poidfm.point_of_interest_id = ssgpc.point_of_interest_id;

ROLLBACK;
