BEGIN;

UPDATE surf_spot_configurations
SET ratings_algorithm = 'ML_SPECTRA_REF_MATRIX';

ROLLBACK;
