module "science_data_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = "wt"
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  # HACK: Hard coded Task Definition because deployment workflow deploys
  service_td_name           = "sl-core-svc-${var.environment}-science-data-api"
  service_td_count          = 1
  service_td_container_name = var.application

  service_alb_priority = 411
  service_port         = 8080

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id
  ecs_cluster = var.ecs_cluster

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = true
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = 4000
  auto_scaling_min_size       = 8
  auto_scaling_max_size       = 5000
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
}

data "aws_sns_topic" "spot_report_updated" {
  name = var.spot_report_updated_sns_topic
}

module "spot_report_updated_dead_letter_queue" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs"

  application = var.application
  environment = var.environment
  queue_name  = "${var.company}-${var.application}-dead-letter-queue"
  company     = var.company
}

locals {
  dead_letter_target_arn = jsonencode({
    deadLetterTargetArn = module.spot_report_updated_dead_letter_queue.queue_arn
  })
}

resource "aws_sns_topic_subscription" "spot_report_updated" {
  topic_arn              = data.aws_sns_topic.spot_report_updated.arn
  protocol               = "https"
  endpoint               = "https://${var.platform_host}/science-data-service/sns/spot-report-updated"
  endpoint_auto_confirms = true
  delivery_policy        = file("${path.module}/resources/sns-delivery-policy.json")
}

# NOTE: Temporary workaround since the aws_sns_topic_subscription
# resource does not support dead letter queues. The null resource below
# executes the AWS cli to attach a RedrivePolicy to the sns subscription resource.
#
# The RedrivePolicy tells the SNS subscription to send failed messages to the
# dead letter queue. There is an open PR that implements dead letter queue support for
# the sns subscription resource. Review is pending by the Hashicorp Team:
# https://github.com/terraform-providers/terraform-provider-aws/issues/10931
resource "null_resource" "spot_report_updated_redrive_policy" {
  provisioner "local-exec" {
    command = <<EOF
      aws sns set-subscription-attributes \
      --subscription-arn ${aws_sns_topic_subscription.spot_report_updated.arn} \
      --attribute-name RedrivePolicy \
      --attribute-value ${local.dead_letter_target_arn}
    EOF
  }

  triggers = {
    dlq_arn       = module.spot_report_updated_dead_letter_queue.queue_arn
    sns_topic_arn = aws_sns_topic_subscription.spot_report_updated.arn
  }

  depends_on = [
    aws_sns_topic_subscription.spot_report_updated,
    module.spot_report_updated_dead_letter_queue,
  ]
}

resource "aws_sqs_queue_policy" "sqs_read_policy" {
  queue_url = module.spot_report_updated_dead_letter_queue.queue_url
  policy = templatefile("${path.module}/resources/sqs-queue-policy.json", {
    dead_letter_queue   = module.spot_report_updated_dead_letter_queue.queue_arn,
    spot_report_updated = data.aws_sns_topic.spot_report_updated.arn
  })
}

resource "aws_iam_policy" "maptiler_bucket_access" {
  name   = "${var.company}-${var.application}-maptiler-bucket-access-${var.environment}"
  policy = templatefile("${path.module}/resources/bucket-list-and-read-access-policy.json", { bucket_name : "wt-maptiler-tiledb-prod" })
}

resource "aws_iam_role_policy_attachment" "maptiler_bucket_access" {
  role       = module.science_data_service.task_role_name
  policy_arn = aws_iam_policy.maptiler_bucket_access.arn
}
