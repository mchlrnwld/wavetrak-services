terraform {
  required_version = ">= 0.12"
  required_providers {
    aws      = "~> 3.26"
    null     = "~> 2.1"
    template = "~> 2.1"
  }
}
