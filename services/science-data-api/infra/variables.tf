variable "application" {
  type    = string
  default = "science-data-service"
}

variable "environment" {
  type        = string
  description = "Environment name (sandbox or prod)."
}

variable "company" {
  type        = string
  default     = "wt"
  description = "Company name (ex: wt)"
}

variable "default_vpc" {
  type = string
}

variable "ecs_cluster" {
  type        = string
  description = "ECS cluster to create application service."
}

variable "alb_listener_arn" {
  type        = string
  description = "Application load balancer listener ARN to load balance requests to service."
}

variable "service_lb_rules" {
  type        = list(map(string))
  description = "List of load balancer rules."
}

variable "iam_role_arn" {
  type        = string
  description = "ECS service IAM role ARN."
}

variable "dns_name" {
  type        = string
  description = "DNS name for service."
}

variable "dns_zone_id" {
  type        = string
  description = "DNS zone to create DNS record in."
}

variable "load_balancer_arn" {
  type        = string
  description = "Application load balancer ARN to load balance requests to service."
}

variable "spot_report_updated_sns_topic" {
  type        = string
  description = "Spot report updated SNS topic name. The service will subscribe to this topic via HTTPS."
}

variable "platform_host" {
  type        = string
  description = "Platform host name for SNS topic HTTPS subscription."
}
