variable "company" {
  default = "wt"
}

variable "application" {
  default = "science-platform-db"
}

variable "environment" {
}

variable "default_vpc" {
}

variable "instance_subnets" {
  type = list(string)
}

variable "db_instance_class" {
}

variable "db_instance_multi_az" {
  type        = bool
  description = "Enable multiple availability zone standby."
  default     = false
}

variable "db_instance_replica" {
  type        = bool
  description = "Enable database replica instance."
  default     = false
}

variable "vpn_sg" {
}

variable "airflow_sg" {
}

data "aws_ssm_parameter" "db_username" {
  name = "/${var.environment}/science-platform-db/db_user"
}

data "aws_ssm_parameter" "db_password" {
  name = "/${var.environment}/science-platform-db/db_pass"
}

locals {
  db_name = "SciencePlatform"
}
