data "aws_security_group" "core_services" {
  name = "sl-sg-ecs-core-svc-${var.environment}"
}

resource "aws_db_subnet_group" "postgres" {
  name       = "${var.company}-${var.application}-rds-${var.environment}"
  subnet_ids = var.instance_subnets
}

resource "aws_security_group" "postgres" {
  name   = "${var.company}-${var.application}-rds-${var.environment}"
  vpc_id = var.default_vpc
}

resource "aws_security_group_rule" "ingress_allow_core_services" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = data.aws_security_group.core_services.id
  security_group_id        = aws_security_group.postgres.id
}

resource "aws_security_group_rule" "ingress_allow_vpn" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = var.vpn_sg
  security_group_id        = aws_security_group.postgres.id
}

resource "aws_security_group_rule" "ingress_allow_airflow" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = var.airflow_sg
  security_group_id        = aws_security_group.postgres.id
}

resource "aws_security_group_rule" "egress_allow_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.postgres.id
}

resource "aws_db_parameter_group" "postgres12" {
  name   = "${var.company}-${var.application}-postgres12-${var.environment}"
  family = "postgres12"

  parameter {
    name  = "log_min_duration_statement"
    value = "500"
  }

  parameter {
    name         = "shared_preload_libraries"
    value        = "pg_stat_statements"
    apply_method = "pending-reboot"
  }
}

locals {
  db_engine                       = "postgres"
  db_engine_version               = "12.7"
  db_storage_type                 = "gp2"
  db_max_allocated_storage        = 2000
  db_performance_insights_enabled = true
  db_deletion_protection          = true
  db_maintenance_window           = "Sat:06:00-Sat:07:00"
  db_apply_immediately            = true
}

resource "aws_db_instance" "postgres" {
  identifier                   = "${var.company}-${var.application}-${var.environment}"
  engine                       = local.db_engine
  engine_version               = local.db_engine_version
  instance_class               = var.db_instance_class
  storage_type                 = local.db_storage_type
  max_allocated_storage        = local.db_max_allocated_storage
  parameter_group_name         = aws_db_parameter_group.postgres12.name
  multi_az                     = var.db_instance_multi_az
  performance_insights_enabled = local.db_performance_insights_enabled
  deletion_protection          = local.db_deletion_protection

  name     = local.db_name
  username = data.aws_ssm_parameter.db_username.value
  password = data.aws_ssm_parameter.db_password.value

  db_subnet_group_name   = aws_db_subnet_group.postgres.id
  vpc_security_group_ids = [aws_security_group.postgres.id]

  maintenance_window        = local.db_maintenance_window
  final_snapshot_identifier = "${var.company}-${var.application}-${var.environment}"
  apply_immediately         = local.db_apply_immediately
  backup_retention_period   = 7
  backup_window             = "03:00-04:00"

  depends_on = [
    aws_db_subnet_group.postgres,
    aws_security_group.postgres,
  ]
}

resource "aws_db_instance" "postgres_replica" {
  count                        = var.db_instance_replica ? 1 : 0
  identifier                   = "${var.company}-${var.application}-replica-${var.environment}"
  replicate_source_db          = aws_db_instance.postgres.id
  engine                       = local.db_engine
  engine_version               = local.db_engine_version
  instance_class               = var.db_instance_class
  storage_type                 = local.db_storage_type
  max_allocated_storage        = local.db_max_allocated_storage
  parameter_group_name         = aws_db_parameter_group.postgres12.name
  performance_insights_enabled = local.db_performance_insights_enabled
  deletion_protection          = local.db_deletion_protection
  skip_final_snapshot          = true

  vpc_security_group_ids = [aws_security_group.postgres.id]

  maintenance_window = local.db_maintenance_window
  apply_immediately  = local.db_apply_immediately

  depends_on = [
    aws_db_instance.postgres,
  ]
}
