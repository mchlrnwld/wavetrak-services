resource "aws_security_group" "redis" {
  name   = "${var.company}-${var.application}-redis-${var.environment}"
  vpc_id = var.vpc
}


resource "aws_security_group_rule" "ingress_allow_core_services" {
  type                     = "ingress"
  from_port                = 6379
  to_port                  = 6379
  protocol                 = "tcp"
  source_security_group_id = var.core_services_security_group
  security_group_id        = aws_security_group.redis.id
}

resource "aws_security_group_rule" "ingress_allow_vpn" {
  type                     = "ingress"
  from_port                = 6379
  to_port                  = 6379
  protocol                 = "tcp"
  source_security_group_id = var.vpn_security_group
  security_group_id        = aws_security_group.redis.id
}

resource "aws_security_group_rule" "egress_allow_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.redis.id
}

resource "aws_elasticache_subnet_group" "default" {
  name       = "${var.company}-${var.application}-redis-subnet-${var.environment}"
  subnet_ids = var.subnet_ids
}

resource "aws_elasticache_replication_group" "default" {
  replication_group_id          = "${var.company}-${var.application}-redis-${var.environment}"
  replication_group_description = "Redis cluster for Science Data Service"

  availability_zones = var.availability_zones
  multi_az_enabled   = var.multi_az_enabled

  node_type = var.node_type
  port      = var.port

  maintenance_window = var.maintenance_window

  snapshot_retention_limit = var.snapshot_retention_limit
  snapshot_window          = var.snapshot_window

  security_group_ids = [aws_security_group.redis.id]

  subnet_group_name          = aws_elasticache_subnet_group.default.name
  automatic_failover_enabled = var.automatic_failover_enabled

  number_cache_clusters = var.number_cache_clusters
}
