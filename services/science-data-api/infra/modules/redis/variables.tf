variable "vpn_security_group" {
  description = "Security Group for the engineering VPN."
  type        = string
}

variable "environment" {
  description = "Environment name (sandbox or prod)."
  type        = string
}

variable "company" {
  description = "Company name (ex: wt)."
  type        = string
  default     = "wt"
}

variable "vpc" {
  description = "VPC to place the Redis cluster in."
  type        = string
}

variable "application" {
  description = "The application name to associate with the Redis cluster."
  type        = string
  default     = "science-data-service"
}

variable "node_type" {
  description = "Specifies the node type for each Redis instance."
  type        = string
}

variable "core_services_security_group" {
  description = "Security group ID for wavetrak core services."
  type        = string
}

variable "maintenance_window" {
  description = "Specifies the weekly time range when maintence on the cluster is performed."
  type        = string
  default     = "sat:09:00-sat:10:00"
}

variable "snapshot_retention_limit" {
  description = "The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them."
  type        = number
  default     = 35
}

variable "snapshot_window" {
  description = "Daily time range (UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster."
  type        = string
  default     = "08:00-09:00"
}

variable "subnet_ids" {
  description = "Subnet IDs to place redis cluster in."
  type        = list(string)
}

variable "availability_zones" {
  description = "Availability zones to place redis cluster in."
  type        = list(string)
  default     = ["us-west-1c", "us-west-1b"]
}

variable "node_groups" {
  description = "Number of nodes groups to create in the cluster"
  type        = number
  default     = 1
}

variable "number_cache_clusters" {
  description = "The number of cache clusters the replication group will have."
  type        = number
  default     = 2
}

variable "automatic_failover_enabled" {
  description = "Specifies whether a read-only replica will be automatically promoted to a primary if the existing primary fails."
  type        = bool
  default     = true
}

variable "port" {
  description = "The port to use for each Redis node (the default is 6379)."
  type        = number
  default     = 6379
}

variable "multi_az_enabled" {
  description = "Specifies whether or not to enable multi availability zone support."
  type        = bool
  default     = true
}
