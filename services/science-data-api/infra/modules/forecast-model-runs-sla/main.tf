resource "newrelic_synthetics_monitor" "nasa_mur_sst_poi_sla" {
  name      = "NASA MUR-SST Point of Interest Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "nasa_mur_sst_poi_sla" {
  monitor_id = newrelic_synthetics_monitor.nasa_mur_sst_poi_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "NASA",
    model                 = "MUR-SST",
    type                  = "POINT_OF_INTEREST",
    hoursBetweenRuns      = 24,
    hoursBeforeRunIsReady = 28,
  })
}

resource "newrelic_synthetics_monitor" "noaa_gefs_wave_poi_sla" {
  name      = "NOAA GEFS-Wave Point of Interest Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "noaa_gefs_wave_poi_sla" {
  monitor_id = newrelic_synthetics_monitor.noaa_gefs_wave_poi_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "NOAA",
    model                 = "GEFS-Wave",
    type                  = "POINT_OF_INTEREST",
    hoursBetweenRuns      = 6,
    hoursBeforeRunIsReady = 8,
  })
}

resource "newrelic_synthetics_monitor" "noaa_gfs_poi_sla" {
  name      = "NOAA GFS Point of Interest Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "noaa_gfs_poi_sla" {
  monitor_id = newrelic_synthetics_monitor.noaa_gfs_poi_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "NOAA",
    model                 = "GFS",
    type                  = "POINT_OF_INTEREST",
    hoursBetweenRuns      = 6,
    hoursBeforeRunIsReady = 6,
  })
}

resource "newrelic_synthetics_monitor" "noaa_gfs_full_grid_sla" {
  name      = "NOAA GFS Full Grid Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "noaa_gfs_full_grid_sla" {
  monitor_id = newrelic_synthetics_monitor.noaa_gfs_full_grid_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "NOAA",
    model                 = "GFS",
    type                  = "FULL_GRID",
    hoursBetweenRuns      = 6,
    hoursBeforeRunIsReady = 9,
  })
}

resource "newrelic_synthetics_monitor" "noaa_nam_poi_sla" {
  name      = "NOAA NAM Point of Interest Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "noaa_nam_poi_sla" {
  monitor_id = newrelic_synthetics_monitor.noaa_nam_poi_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "NOAA",
    model                 = "NAM",
    type                  = "POINT_OF_INTEREST",
    hoursBetweenRuns      = 6,
    hoursBeforeRunIsReady = 4,
  })
}

resource "newrelic_synthetics_monitor" "wavetrak_lotus_ww3_poi_sla" {
  name      = "Wavetrak LOTUS-WW3 Point of Interest Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "wavetrak_lotus_ww3_poi_sla" {
  monitor_id = newrelic_synthetics_monitor.wavetrak_lotus_ww3_poi_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "Wavetrak",
    model                 = "Lotus-WW3",
    type                  = "POINT_OF_INTEREST",
    hoursBetweenRuns      = 6,
    hoursBeforeRunIsReady = 12,
  })
}

resource "newrelic_synthetics_monitor" "wavetrak_lotus_ww3_full_grid_sla" {
  name      = "Wavetrak LOTUS-WW3 Full Grid Model Runs SLA"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "wavetrak_lotus_ww3_full_grid_sla" {
  monitor_id = newrelic_synthetics_monitor.wavetrak_lotus_ww3_full_grid_sla.id
  text = templatefile("${path.module}/resources/script.js", {
    agency                = "Wavetrak",
    model                 = "Lotus-WW3",
    type                  = "FULL_GRID",
    hoursBetweenRuns      = 6,
    hoursBeforeRunIsReady = 16,
  })
}
