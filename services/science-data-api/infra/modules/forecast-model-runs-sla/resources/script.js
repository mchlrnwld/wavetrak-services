const assert = require('assert');

function runToDate(run) {
  const runString = `$${run}`;
  const year = Number(runString.slice(0, 4));
  const month = Number(runString.slice(4, 6));
  const day = Number(runString.slice(6, 8));
  const hour = Number(runString.slice(8, 10));

  return new Date(Date.UTC(year, month - 1, day, hour));
}

$http.post(
  'http://services.surfline.com/graphql?api_key=temporary_and_insecure_wavetrak_api_key',
  {
    json: {
      operationName: `FORECAST_MODEL_RUNS_SLA`,
      query: `
query FORECAST_MODEL_RUNS_SLA {
  models(agency: "${agency}", model: "${model}") {
    agency
    model
    runs(statuses:ONLINE, types:${type}) {
      run
    }
  }
}`,
    },
  },
  (err, response, body) => {
    if (response.statusCode !== 200) {
      return;
    }

    const now = new Date();

    console.log(`${agency} ${model}`);
    assert.ok(body.data.models, `Expected ${agency} ${model} to be found.`);

    const model = body.data.models[0];
    assert.ok(model.runs, `Expected ${agency} ${model} to have online runs.`);

    const latestRun = runToDate(model.runs.sort((a, b) => b - a)[0].run);
    const nextRun = new Date(latestRun.getTime() + ${hoursBetweenRuns} * 60 * 60 * 1000);
    const nextRunExpected = new Date(nextRun.getTime() + ${hoursBeforeRunIsReady} * 60 * 60 * 1000);
    console.log(`Latest run: $${latestRun}`);
    console.log(`Next run: $${nextRun}`);
    console.log(`Next run expected: $${nextRunExpected}`);

    const delay = (now - nextRunExpected) / 1000.0 / 60.0 / 60.0;

    assert.ok(delay < 0, `${agency} ${model} run $${nextRun} is $${delay} hours delayed.`);
  },
);
