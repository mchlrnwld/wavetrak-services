provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/sandbox/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/sandbox/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "science-data-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "science-data-service.sandbox.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "science_data_service" {
  source = "../../"

  environment      = "sandbox"
  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3DM6R3JR1RYXV"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac"

  spot_report_updated_sns_topic = "spot_report_updated_sandbox"
  platform_host                 = "platform.sandbox.surfline.com"
}

module "science_platform_db" {
  source = "../../modules/science-platform-db"

  environment = "sandbox"
  default_vpc = "vpc-981887fd"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  db_instance_class = "db.m5.xlarge"
  vpn_sg            = "sg-82aeaee7"
  airflow_sg        = "sg-2aafbf4e"
}

module "redis" {
  source = "../../modules/redis"

  environment                  = "sandbox"
  subnet_ids                   = ["subnet-0909466c", "subnet-f2d458ab"]
  availability_zones           = ["us-west-1c", "us-west-1b"]
  vpn_security_group           = "sg-82aeaee7"
  vpc                          = "vpc-981887fd"
  node_type                    = "cache.t2.small"
  core_services_security_group = "sg-0d90086a"
}
