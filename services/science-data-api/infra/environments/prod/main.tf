provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "science-data-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "science-data-service.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "science_data_service" {
  source = "../../"

  environment      = "prod"
  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3LLOZIY0ZZQDE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"

  spot_report_updated_sns_topic = "spot_report_updated_prod"
  platform_host                 = "platform.surfline.com"
}

module "science_platform_db" {
  source = "../../modules/science-platform-db"

  environment = "prod"
  default_vpc = "vpc-116fdb74"
  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0"
  ]
  db_instance_class    = "db.m5.xlarge"
  db_instance_multi_az = true
  db_instance_replica  = true
  vpn_sg               = "sg-cfa0e5aa"
  airflow_sg           = "sg-1094ad77"
}

module "forecast_model_runs_sla" {
  source = "../../modules/forecast-model-runs-sla"
}

module "redis" {
  source = "../../modules/redis"

  environment                  = "prod"
  subnet_ids                   = ["subnet-d1bb6988", "subnet-85ab36e0"]
  availability_zones           = ["us-west-1c", "us-west-1b"]
  vpn_security_group           = "sg-cfa0e5aa"
  vpc                          = "vpc-116fdb74"
  node_type                    = "cache.r6g.large"
  core_services_security_group = "sg-498ee82e"
}
