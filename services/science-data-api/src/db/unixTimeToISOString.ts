export default function(unixtime: number): string {
  return new Date(unixtime * 1000).toISOString();
}
