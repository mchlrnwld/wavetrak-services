import { expect } from 'chai';
import { runTimeToISOString } from './runTimeConversions';

describe('runTimeToISOString', () => {
  it('should convert model runtime to ISO 8601 date string', () => {
    const dateString = runTimeToISOString(2019082906);
    expect(dateString).to.equal('2019-08-29T06:00:00.000Z');
  });
});
