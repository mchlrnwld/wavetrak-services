import moment from 'moment-timezone';

export const runTimeToDate = run => moment.tz(`${run}`, 'YYYYMMDDHH', 'UTC').toDate();

export const runTimeToUnix = run => runTimeToDate(run).getTime() / 1000;

export const runTimeToISOString = (run: string | number): string =>
  runTimeToDate(run).toISOString();

export const formatRunTime = (unixTime: number): number =>
  parseInt(moment.utc(unixTime).format('YYYYMMDDHH'), 10);
