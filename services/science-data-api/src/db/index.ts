import knex from 'knex';
import knexPostgis from 'knex-postgis';
import knexStringcase from 'knex-stringcase';
import { builtins } from 'pg-types';
import { types } from 'pg';
import toNumber from 'lodash/toNumber';
import moment from 'moment';
import config from '../config';

// Parse PSQL NUMERIC type to number
types.setTypeParser(builtins.NUMERIC, toNumber);
types.setTypeParser(builtins.TIMESTAMP, v => moment.utc(v).toDate());

const db = {
  source: {
    client: null,
    st: null,
  },
  replica: {
    client: null,
    st: null,
  },
};

export const createKnexConfigWithHost = host =>
  knexStringcase({
    asyncStackTraces: process.env.NODE_ENV === 'development',
    debug: process.env.NODE_ENV === 'development',
    client: 'pg',
    connection: {
      host: host,
      database: config.SCIENCE_PLATFORM_DB_DATABASE,
      user: config.SCIENCE_PLATFORM_DB_USER,
      password: config.SCIENCE_PLATFORM_DB_PASSWORD,
    },
    pool: { min: 2, max: 10 },
  });

export const initializePostgresDB = async () => {
  db.source.client = knex(createKnexConfigWithHost(config.SCIENCE_PLATFORM_DB_HOST));
  db.replica.client = config.SCIENCE_PLATFORM_DB_REPLICA_HOST
    ? knex(createKnexConfigWithHost(config.SCIENCE_PLATFORM_DB_REPLICA_HOST))
    : db.source.client;
  await Promise.all([
    db.source.client.raw("SELECT 'Test connection'"),
    db.replica.client.raw("SELECT 'Test connection'"),
  ]);
  db.source.st = knexPostgis(db.source.client);
  db.replica.st = config.SCIENCE_PLATFORM_DB_REPLICA_HOST
    ? knexPostgis(db.replica.client)
    : db.source.st;
};

export default db;
