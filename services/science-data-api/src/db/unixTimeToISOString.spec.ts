import { expect } from 'chai';
import unixTimeToISOString from './unixTimeToISOString';

describe('unixTimeToISOString', () => {
  it('should convert unix timestamp to ISO 8601 date string', () => {
    const dateString = unixTimeToISOString(1567058400);
    expect(dateString).to.equal('2019-08-29T06:00:00.000Z');
  });
});
