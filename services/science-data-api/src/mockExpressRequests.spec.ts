import chai from 'chai';
import express from 'express';
import { json } from 'body-parser';

const mockExpressRequests = setupMiddleware => {
  const app = express();
  app.use(json());
  setupMiddleware(app);
  const request = chai.request(app).keepOpen();
  return request;
};

export default mockExpressRequests;
