import { queryPointsOfInterestGridPoints } from '../../models/pointOfInterestGridPoint';

// eslint-disable-next-line import/prefer-default-export
export async function getPointsOfInterestGridPointsHandler(req, res) {
  const { query } = req;
  const params = {
    agency: query.agency,
    model: query.model,
    grid: query.grid,
    offset: query.offset,
    limit: query.limit,
  };
  const pointsOfInterestGridPoints = await queryPointsOfInterestGridPoints(params);
  return res.send(pointsOfInterestGridPoints);
}
