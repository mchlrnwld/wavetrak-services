import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import validateOffsetLimit from './validateOffsetLimit';
import { getPointsOfInterestGridPointsHandler } from './handlers';

const pointsOfInterestGridPoints = Router();

pointsOfInterestGridPoints.get(
  '/',
  validateOffsetLimit,
  wrapErrors(getPointsOfInterestGridPointsHandler),
);

export default pointsOfInterestGridPoints;
