import joi from 'joi';
import { createValidationError } from '../../common/errors/ValidationError';

const offsetSchema = joi
  .number()
  .integer()
  .min(0)
  .optional();
const limitSchema = joi
  .number()
  .integer()
  .positive()
  .optional();

async function validateOffsetLimit(req, _, next) {
  const {
    query: { offset, limit },
  } = req;

  req.validatedQuery = req.validatedQuery || {};

  try {
    const validatedOffset = await joi.validate(offset, offsetSchema);
    req.validatedQuery.offset = validatedOffset;
  } catch (err) {
    return next(
      createValidationError(
        'Invalid "offset" requested',
        err.details.map(d => d.message)
      )
    );
  }

  try {
    const validatedLimit = await joi.validate(limit, limitSchema);
    req.validatedQuery.limit = validatedLimit;
    return next();
  } catch (err) {
    return next(
      createValidationError(
        'Invalid "limit" requested',
        err.details.map(d => d.message)
      )
    );
  }
}

export default validateOffsetLimit;
