import joi from 'joi';
import sortBy from 'lodash/fp/sortBy';
import { createValidationError } from '../../common/errors/ValidationError';
import { queryModelRuns } from '../../models/modelRun';
import { queryModelGrids } from '../../models/modelGrid';
import { getNearestModelGridPoint } from '../../models/modelGridPoint';
import * as modelRunSchemas from '../../models/modelRun/schema';
import models from '../../stores/models';
import getAlignedRun from '../graphql/forecasts/forecastModel/getAlignedRun';

const latLonSchema = joi.object().keys({
  lat: joi
    .number()
    .min(-90)
    .max(90)
    .required(),
  lon: joi.number().required(),
});

const modelSchema = joi.object().keys({
  agency: modelRunSchemas.agency.required(),
  model: modelRunSchemas.model.required(),
});

const validateOptions = {
  abortEarly: false,
};

const getModelMetaDataFactory = (validModels, useAlignedModelRunIfNotSpecified = false) =>
  async function getModelMetaData(req, _res, next) {
    const {
      query: { agency, model, run, grid, lat, lon },
    } = req;

    req.validatedQuery = req.validatedQuery || {};

    try {
      const validated = await joi.validate({ lat, lon }, latLonSchema, validateOptions);
      req.validatedQuery.lat = validated.lat;
      req.validatedQuery.lon = validated.lon;
    } catch (err) {
      return next(
        createValidationError(
          'Valid "lat" and "lon" required',
          err.details.map(d => d.message),
        ),
      );
    }

    try {
      const validated = await joi.validate(
        {
          agency,
          model,
        },
        modelSchema,
        validateOptions,
      );
      req.validatedQuery.agency = validated.agency;
      req.validatedQuery.model = validated.model;
    } catch (err) {
      return next(
        createValidationError(
          'Valid "agency", "model" required',
          err.details.map(d => d.message),
        ),
      );
    }

    const modelKey = `${req.validatedQuery.agency},${req.validatedQuery.model}`;

    if (
      !validModels.has(modelKey) ||
      !models[req.validatedQuery.agency] ||
      !models[req.validatedQuery.agency][req.validatedQuery.model]
    ) {
      const validModelsString = `${[...validModels].map(m => m.replace(/,/g, '/'))}`.replace(
        /,/g,
        ', ',
      );
      return next(
        createValidationError(
          'Valid "agency", "model" required',
          `"agency"/"model" must be one of [${validModelsString}]`,
        ),
      );
    }

    try {
      const validated = await joi.validate({ run }, { run: modelRunSchemas.run }, validateOptions);
      req.validatedQuery.run = validated.run;
    } catch (err) {
      return next(
        createValidationError(
          'Invalid "run" requested',
          err.details.map(d => d.message),
        ),
      );
    }

    if (grid) {
      const validModelGrids = await queryModelGrids({
        agency: req.validatedQuery.agency,
        model: req.validatedQuery.model,
      });

      const validGrids = validModelGrids.map(g => g.grid);
      if (!validGrids.includes(grid)) {
        return next(
          createValidationError(
            '"grid" invalid',
            `"grid" must be one of [${validGrids.join(', ')}]`,
          ),
        );
      }
    }

    const modelRuns =
      useAlignedModelRunIfNotSpecified && !req.validatedQuery.run
        ? [await getAlignedRun({ agency, model, types: ['FULL_GRID'] })]
        : await queryModelRuns({
            agency: req.validatedQuery.agency,
            model: req.validatedQuery.model,
            run: req.validatedQuery.run,
            statuses: ['ONLINE'],
            types: ['FULL_GRID'],
          });

    if (!modelRuns.length) {
      return next(createValidationError('No online model run available'));
    }

    const modelRun = sortBy(mr => -mr.run, modelRuns)[0];

    const gridPoint = await getNearestModelGridPoint({
      agency: modelRun.agency,
      model: modelRun.model,
      grid,
      lat: req.validatedQuery.lat,
      lon: req.validatedQuery.lon,
      within: 40000, // 40 km
    });

    if (!gridPoint) {
      return next(createValidationError('No grid point found within 40 km of requested point'));
    }

    req.modelMetaData = {
      agency: modelRun.agency,
      model: modelRun.model,
      run: modelRun.run,
      grid: {
        key: gridPoint.grid,
        lat: gridPoint.lat,
        lon: gridPoint.lon,
      },
    };

    return next();
  };

export default getModelMetaDataFactory;
