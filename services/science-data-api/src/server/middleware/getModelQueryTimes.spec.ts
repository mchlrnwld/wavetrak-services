import { expect } from 'chai';
import { matchForecastTime } from './getModelQueryTimes';

const getModelQueryTimesSpec = () => {
  describe('getModelQueryTimes', () => {
    describe('matchForecastTime', () => {
      const timeRanges = [
        {
          start: 0,
          end: 24,
          interval: 1,
        },
        {
          start: 48,
          end: 72,
          interval: 3,
        },
      ];

      it('returns null if time does not fall within given time ranges', () => {
        expect(matchForecastTime(timeRanges, -1)).to.be.null();
        expect(matchForecastTime(timeRanges, 36)).to.be.null();
        expect(matchForecastTime(timeRanges, 73)).to.be.null();
      });

      it('returns matching forecast time from a list of time ranges', () => {
        expect(matchForecastTime(timeRanges, 0)).to.equal(0);
        expect(matchForecastTime(timeRanges, 24)).to.equal(24);
        expect(matchForecastTime(timeRanges, 12)).to.equal(12);
        expect(matchForecastTime(timeRanges, 48)).to.equal(48);
        expect(matchForecastTime(timeRanges, 49)).to.equal(48);
        expect(matchForecastTime(timeRanges, 53)).to.equal(51);
        expect(matchForecastTime(timeRanges, 72)).to.equal(72);
      });
    });
  });
};

export default getModelQueryTimesSpec;
