import joi from 'joi';
import { createValidationError } from '../../common/errors/ValidationError';

const waveHeight = joi
  .string()
  .valid('m', 'ft')
  .default('m');
const windSpeed = joi
  .string()
  .valid('mi/h', 'knot', 'km/h')
  .default('km/h');

export const swellsUnitsSchema = joi.object().keys({
  waveHeight,
});

export const swellsEnsembleUnitsSchema = joi.object().keys({
  waveHeight,
  windSpeed,
});

export const weatherUnitsSchema = joi.object().keys({
  windSpeed,
  temperature: joi
    .string()
    .valid('C', 'F')
    .default('C'),
  pressure: joi
    .string()
    .valid('mb')
    .default('mb'),
  visibility: joi
    .string()
    .valid('m', 'ft')
    .default('m'),
  precipitation: joi
    .string()
    .valid('mm', 'in')
    .default('mm'),
});

const validateRequestedUnitsFactory = schema =>
  async function validateRequestedUnits(req, _, next) {
    const {
      query: { units },
    } = req;

    req.validatedQuery = req.validatedQuery || {};

    try {
      const validated = await joi.validate(units || {}, schema, { abortEarly: false });
      req.validatedQuery.units = validated;
      return next();
    } catch (err) {
      return next(
        createValidationError(
          'Error validating requested units',
          err.details.map(d => d.message)
        )
      );
    }
  };

export default validateRequestedUnitsFactory;
