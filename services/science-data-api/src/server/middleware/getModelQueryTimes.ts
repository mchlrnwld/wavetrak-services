import joi from 'joi';
import moment from 'moment';
import isFinite from 'lodash/isFinite';
import { createValidationError } from '../../common/errors/ValidationError';
import models from '../../stores/models';

export const matchForecastTime = (timeRanges, time) => {
  const range = timeRanges.find(r => r.start <= time && time <= r.end);
  if (!range) return null;

  const { start, interval } = range;
  const stepsFromStart = Math.floor((time - start) / interval);
  const forecastTime = start + stepsFromStart * interval;

  return forecastTime;
};

const rangeSchema = joi.object().keys({
  start: joi.number(),
  end: joi.number(),
  interval: joi
    .number()
    .valid(3600, 10800, 21600)
    .default(10800),
});

async function getModelQueryTimes(req, _res, next) {
  const {
    query: { start, end, interval },
    modelMetaData: { agency, model, run },
  } = req;

  req.validatedQuery = req.validatedQuery || {};

  try {
    const validated = await joi.validate({ start, end, interval }, rangeSchema, {
      abortEarly: false,
    });
    req.validatedQuery.start = validated.start;
    req.validatedQuery.end = validated.end;
    req.validatedQuery.interval = validated.interval;
  } catch (err) {
    return next(
      createValidationError(
        'Invalid "start", "end", or "interval" requested',
        err.details.map(d => d.message),
      ),
    );
  }

  req.modelQueryTimes = { start: null, end: null };

  if (!isFinite(req.validatedQuery.start) && !isFinite(req.validatedQuery.end)) {
    return next();
  }

  if (
    isFinite(req.validatedQuery.start) &&
    isFinite(req.validatedQuery.end) &&
    req.validatedQuery.end <= req.validatedQuery.start
  ) {
    return next(
      createValidationError('Invalid "start" or "end" requested', '"start" must be before "end"'),
    );
  }

  const runUnix = moment.utc(`${run}`, 'YYYYMMDDHH').unix();
  const timeRanges = models[agency][model].timeRanges.map(r => ({
    start: r.start + runUnix,
    end: r.end + runUnix,
    interval: r.interval,
  }));

  if (isFinite(req.validatedQuery.start)) {
    req.modelQueryTimes.start = matchForecastTime(timeRanges, req.validatedQuery.start);
    if (!req.modelQueryTimes.start) {
      return next(createValidationError(`Invalid "start" parameter for model run ${run}`));
    }
  }

  if (isFinite(req.validatedQuery.end)) {
    req.modelQueryTimes.end = matchForecastTime(timeRanges, req.validatedQuery.end);
    if (!req.modelQueryTimes.end) {
      return next(createValidationError(`Invalid "end" parameter for model run ${run}`));
    }
  }

  return next();
}

export default getModelQueryTimes;
