import { gql } from 'apollo-server-express';
import { buildFederatedSchema } from '@apollo/federation';
import { MODEL_TYPES } from '../../models/model/constants';
import { ForecastType } from '../../models/pointOfInterestDefaultForecastModel/index.d';
import { LatitudeType, LongitudeType } from './types';
import forecasts from './forecasts';
import models from './models';
import stations from './stations';
import pointOfInterest from './pointsOfInterest';
import charts from './charts';
import { instrumentSchema } from './newrelic';

// @cacheControl directive needs to be defined because of schema stitching or federation.
// https://stackoverflow.com/questions/52922080/how-to-implement-caching-on-apollo-server-hapi-graphql
const typeDefs = gql`
  scalar Longitude
  scalar Latitude

  enum BreakingWaveHeightAlgorithm {
    SPECTRAL_REFRACTION
    POLYNOMIAL
  }

  enum ForecastType {
    ${Object.values(ForecastType)}
  }

  enum ModelRunStatus {
    PENDING
    ONLINE
    OFFLINE
  }

  enum ModelRunType {
    FULL_GRID
    POINT_OF_INTEREST
  }

  enum ModelType {
    ${MODEL_TYPES.join('\n')}
  }

  enum CacheControlScope {
    PUBLIC
    PRIVATE
  }

  directive @cacheControl(
    maxAge: Int
    scope: CacheControlScope
  ) on FIELD_DEFINITION | OBJECT | INTERFACE

  type Query {
    _empty: String
  }

  type Mutation @cacheControl(maxAge: 0) {
    _empty: String
  }

  type Wind {
    speed: Float!
    direction: Float!
    gust: Float
    directionType: String!
  }

  type SwellComponent {
    height: Float
    direction: Float
    period: Float
    spread: Float
    impact: Float
    event: Int
  }

  interface MutationResponse {
    success: Boolean!
    message: String!
    errorJSON: String
  }
`;

const resolvers = {
  Longitude: LongitudeType,
  Latitude: LatitudeType,
};

const root = { typeDefs, resolvers };

const schema = instrumentSchema(
  buildFederatedSchema([root, charts, forecasts, models, pointOfInterest, stations]),
);

export default schema;
