import dataSourcesSpec from './dataSources/index.spec';
import forecastsSpec from './forecasts/index.spec';
import typesSpec from './types/index.spec';

describe('/graphql', () => {
  dataSourcesSpec();
  forecastsSpec();
  typesSpec();
});
