import chartTileBucketSpec from './chartTileBucket.spec';

const dataSourcesSpec = () => {
  describe('data sources', () => {
    chartTileBucketSpec();
  });
};

export default dataSourcesSpec;
