import config from '../../../config';
import db from '../../../db';
import SciencePlatformDB from './sciencePlatformDB';
import SurfRatingsAPI from './surfRatingsAPI';
import { ChartTileBucket } from './chartTileBucket';
import ScienceDataApiS3Client from '../../../external/s3';

export interface ScienceDataServiceDataSources {
  chartTileBucket: ChartTileBucket;
  sciencePlatformDB: SciencePlatformDB;
  surfRatingsAPI: SurfRatingsAPI;
}

let sciencePlatformDB = null;

const s3Client = new ScienceDataApiS3Client();

/**
 * Apollo recommends that the data source function "should create a new instance of each data source
 * for each operation"; however, for SQLDataSource we're following a pattern to return the same
 * instance on each invocation of the data source function per the SQLDataSource documentation
 * https://www.apollographql.com/docs/apollo-server/data/data-sources/#adding-data-sources-to-apollo-server
 * https://github.com/cvburgess/SQLDataSource#usage
 */
export default () => {
  if (sciencePlatformDB === null) sciencePlatformDB = new SciencePlatformDB(db.replica.client);
  return {
    chartTileBucket: new ChartTileBucket(s3Client, config.MAPTILER_TILEDB_BUCKET),
    sciencePlatformDB,
    surfRatingsAPI: new SurfRatingsAPI(),
  };
};
