import { SQLDataSource } from 'datasource-sql';
import { find as findReportedSurfHeights } from '../../../models/reportedSurfHeights/queries';
import { find as findReportedRatings } from '../../../models/reportedRatings/queries';
import { ReportedSurfHeightRecord } from '../../../models/reportedSurfHeights/index.d';
import { ReportedRatingRecord } from '../../../models/reportedRatings/index.d';
import { surfSpotSurf } from '../../../models/surf/queries';
import { SurfRecord } from '../../../models/surf/types';
import { surfSpotSwells } from '../../../models/swells/queries';
import { SwellsRecord } from '../../../models/swells/index.d';
import { surfSpotSST } from '../../../models/sst/queries';
import { SeaSurfaceTemperatureRecord } from '../../../models/sst/types';
import { surfSpotSwellEnsembleMembers } from '../../../models/swellEnsembleMembers/queries';
import { SwellEnsembleMembersRecord } from '../../../models/swellEnsembleMembers/types';
import { surfSpotSwellProbabilities } from '../../../models/swellProbabilities/queries';
import { SwellProbabilityRecord } from '../../../models/swellProbabilities/types';
import { surfSpotWeather } from '../../../models/weather/queries';
import { WeatherRecord } from '../../../models/weather/index.d';
import { surfSpotWind } from '../../../models/wind/queries';
import { WindRecord } from '../../../models/wind/types';
import { SurfSpotConfigurationRecord } from '../../../models/surfSpotConfigurations/types';
import DataLoader from 'dataloader';
import Knex from 'knex';
import { find } from '../../../models/surfSpotConfigurations/queries';

/**
 * Data sources are classes that Apollo Server can use to encapsulate fetching data from a
 * particular source, such as a database or a REST API. These classes help handle caching,
 * deduplication, and errors while resolving operations.
 *
 * - [Apollo Data Sources](https://www.apollographql.com/docs/apollo-server/data/data-sources/)
 * - [SQLDataSource](https://github.com/cvburgess/SQLDataSource)
 * - [GraphQL DataLoader](https://github.com/graphql/dataloader)
 */
export default class SciencePlatformDB extends SQLDataSource {
  private surfSpotConfigurationLoader: DataLoader<string, SurfSpotConfigurationRecord>;

  constructor(config: Knex.Config | Knex) {
    super(config);
    this.surfSpotConfigurationLoader = new DataLoader(
      async ids => {
        const surfSpotConfigurations = await find(ids, this.knex);
        const surfSpotConfigurationsMap = new Map<string, SurfSpotConfigurationRecord>(
          surfSpotConfigurations.map(c => [c.pointOfInterestId, c]),
        );
        return ids.map(id => surfSpotConfigurationsMap.get(id));
      },
      { cache: false },
    );
  }

  async getReportedSurfHeightRecords(args: {
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<ReportedSurfHeightRecord[]> {
    return (await findReportedSurfHeights(args, this.knex, true)).map(
      this._stringForecastTimeToDate,
    );
  }

  async getReportedRatingRecords(args: {
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<ReportedRatingRecord[]> {
    return (await findReportedRatings(args, this.knex)).map(
      this._stringForecastTimeToDate,
    );
  }

  async getSeaSurfaceTemperatureRecords(args: {
    agency: string;
    model: string;
    grid: string;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<SeaSurfaceTemperatureRecord[]> {
    return (await surfSpotSST(args, this.knex, true)).map(this._stringForecastTimeToDate);
  }

  async getSurfSpotConfigurationRecord(
    pointOfInterestId: string,
  ): Promise<SurfSpotConfigurationRecord> {
    return this.surfSpotConfigurationLoader.load(pointOfInterestId);
  }

  async getSurfRecords(args: {
    agency: string;
    model: string;
    grid: string;
    run: number;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<SurfRecord[]> {
    return (await surfSpotSurf(args, this.knex, true)).map(this._stringForecastTimeToDate);
  }

  async getSwellEnsembleMemberRecords(args: {
    agency: string;
    model: string;
    grid: string;
    run: number;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<SwellEnsembleMembersRecord[]> {
    return (await surfSpotSwellEnsembleMembers(args, this.knex, true)).map(
      this._stringForecastTimeToDate,
    );
  }

  async getSwellProbabilityRecords(args: {
    agency: string;
    model: string;
    grid: string;
    run: number;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<SwellProbabilityRecord[]> {
    return (await surfSpotSwellProbabilities(args, this.knex, true)).map(
      this._stringForecastTimeToDate,
    );
  }

  async getSwellRecords(args: {
    agency: string;
    model: string;
    grid: string;
    run: number;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<SwellsRecord[]> {
    return (await surfSpotSwells(args, this.knex, true)).map(this._stringForecastTimeToDate);
  }

  async getWeatherRecords(args: {
    agency: string;
    model: string;
    run: string;
    grid: string;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<WeatherRecord[]> {
    return (await surfSpotWeather(args, this.knex, true)).map(this._stringForecastTimeToDate);
  }

  async getWindRecords(args: {
    agency: string;
    model: string;
    run: string;
    grid: string;
    pointOfInterestId: string;
    start: number;
    end: number;
  }): Promise<WindRecord[]> {
    return (await surfSpotWind(args, this.knex, true)).map(this._stringForecastTimeToDate);
  }

  /**
   * This is a quick fix for string timestamps coming from redis. In the future, we'll use an argument
   * or reply transformer to do this conversion for us. https://www.npmjs.com/package/ioredis
   *
   * @param input
   */
  _stringForecastTimeToDate(input: any) {
    return {
      ...input,
      forecastTime:
        input.forecastTime && typeof input.forecastTime === 'string'
          ? new Date(input.forecastTime)
          : input.forecastTime,
    };
  }
}
