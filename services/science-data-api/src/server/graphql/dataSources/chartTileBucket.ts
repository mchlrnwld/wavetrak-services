import { DataSource } from 'apollo-datasource';
import moment, { Moment } from 'moment';
import * as path from 'path';
import ScienceDataApiS3Client from '../../../external/s3';

export interface ForecastChartTileRequest {
  end: number;
  start: number;
  type: string;
}

export interface ForecastChartTile {
  id: string;
  timestamp: number;
}

interface TileInfoFromKey {
  key: string;
  run: number;
  timestamp: number;
}

const forecastTypeToPrefixMap = {
  wave: 'wave',
  wind: 'wind',
  pressure: 'pressure',
  jet_stream: 'jet-stream',
  sea_ice: 'sea-ice',
  sst: 'sst',
};

const forecastTypeToIntervalHoursMap = {
  wave: 6,
  wind: 6,
  pressure: 6,
  jet_stream: 6,
  sea_ice: 6,
  sst: 24,
};

export class ChartTileBucket extends DataSource {
  readonly client: ScienceDataApiS3Client;
  readonly bucket: string;

  constructor(client: ScienceDataApiS3Client, bucket: string) {
    super();
    this.bucket = bucket;
    this.client = client;
  }

  async getForecastChartTiles(request: ForecastChartTileRequest): Promise<ForecastChartTile[]> {
    const tileInfo = await this._getTileInfo(request);
    const tiles = await Promise.all(tileInfo.map(t => this._readForecastTile(t)));
    return tiles.sort((a, b) => a.timestamp - b.timestamp);
  }

  async _getTileInfo(request: ForecastChartTileRequest): Promise<TileInfoFromKey[]> {
    let tileInfo = await this._getTileInfoForTilesOrganizedByRun(request);

    if (tileInfo.length === 0) tileInfo = await this._getTileInfoForUnorganizedTiles(request);

    const tilesBetweenStartAndEndTimes = tileInfo.filter(
      m => request.start <= m.timestamp && m.timestamp < request.end,
    );

    return this._getMostUpToDateTileInfoAtEachTimestamp(tilesBetweenStartAndEndTimes);
  }

  async _getTileInfoForUnorganizedTiles(
    request: ForecastChartTileRequest,
  ): Promise<TileInfoFromKey[]> {
    const objects = await this.client.listObjects(this.bucket, request.type.toLowerCase());
    return objects.map(o => this._translateKeyOfUnorganizedTileToTileInfo(o.Key));
  }

  _translateKeyOfUnorganizedTileToTileInfo(key: string): TileInfoFromKey {
    const splitKey = key.split('/');
    const fileName = splitKey[splitKey.length - 1];
    const run = parseInt(fileName.split('.')[0]);
    const timestamp = moment.utc(run, 'YYYYMMDDHH').unix();
    return { key, run, timestamp };
  }

  async _getTileInfoForTilesOrganizedByRun(
    request: ForecastChartTileRequest,
  ): Promise<TileInfoFromKey[]> {
    const runPrefixes = this._predictRunPrefixesThatWouldFulfillThisRequest(request);
    return (await Promise.all(runPrefixes.map(p => this._getTileInfoForAllWithPrefix(p)))).flat();
  }

  _predictRunPrefixesThatWouldFulfillThisRequest({
    type,
    start,
    end,
  }: ForecastChartTileRequest): string[] {
    const lowerCaseForecastType = type.toLowerCase();
    const forecastIntervalHours = forecastTypeToIntervalHoursMap[lowerCaseForecastType];
    const idealRunForStartTime = this._getIdealForecastRunForMoment(
      moment.unix(start),
      forecastIntervalHours,
    );
    const idealRunForEndTime = this._getIdealForecastRunForMoment(
      moment.unix(end),
      forecastIntervalHours,
    );
    const numberOfRunsInTimeRange =
      (idealRunForEndTime.unix() - idealRunForStartTime.unix()) / (forecastIntervalHours * 3600);
    const prefixes = [];
    for (let i = 0; i < numberOfRunsInTimeRange + 12; i++) {
      const runToLookFor = moment(idealRunForEndTime)
        .subtract(i * forecastIntervalHours, 'hours')
        .format('YYYYMMDDHH');
      prefixes.push(path.join(forecastTypeToPrefixMap[lowerCaseForecastType], runToLookFor));
    }
    return prefixes;
  }

  _getIdealForecastRunForMoment(m: Moment, forecastIntervalHours: number): Moment {
    const forecastRunHour =
      Math.floor(m.utc().hour() / forecastIntervalHours) * forecastIntervalHours;
    return moment(m)
      .subtract(m.utc().hour(), 'hours')
      .subtract(m.utc().minutes(), 'minutes')
      .subtract(m.utc().second(), 'seconds')
      .add(forecastRunHour, 'hours');
  }

  async _getTileInfoForAllWithPrefix(prefix: string): Promise<TileInfoFromKey[]> {
    const objects = await this.client.listObjects(this.bucket, prefix.toLowerCase());
    return objects ? objects.map(o => this._translateKeyOfTileOrganizedByRunToTileInfo(o.Key)) : [];
  }

  _translateKeyOfTileOrganizedByRunToTileInfo(key: string): TileInfoFromKey {
    const splitKey = key.split('/');
    const run = parseInt(splitKey[1]);
    const hour = parseInt(splitKey[2].split('.')[0]);
    const timestamp = moment
      .utc(run, 'YYYYMMDDHH')
      .add(hour, 'hours')
      .unix();
    return { key, run, timestamp };
  }

  _getMostUpToDateTileInfoAtEachTimestamp(info: TileInfoFromKey[]): TileInfoFromKey[] {
    const mapOfTimestampToInfo: { [timestamp: number]: TileInfoFromKey } = {};

    info.forEach(d => {
      if (!mapOfTimestampToInfo[d.timestamp] || mapOfTimestampToInfo[d.timestamp].run < d.run)
        mapOfTimestampToInfo[d.timestamp] = d;
    });

    return Object.values(mapOfTimestampToInfo);
  }

  async _readForecastTile(metadata: TileInfoFromKey): Promise<ForecastChartTile> {
    const object = await this.client.readObjectContentsAsJson(this.bucket, metadata.key);
    return { timestamp: metadata.timestamp, id: object.tileId };
  }
}
