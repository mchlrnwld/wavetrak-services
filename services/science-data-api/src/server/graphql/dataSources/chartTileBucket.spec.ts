import { expect } from 'chai';
import { ChartTileBucket } from './chartTileBucket';
import ScienceDataApiS3Client from '../../../external/s3';
import sinon from 'sinon';
import moment from 'moment';
import * as path from 'path';

const runTypesToCreateMockData = ['wave', 'wind', 'sea-ice'];
const runPrefixesToCreateMockData = ['2021121300', '2021121306', '2021121312'];
const objectNamesToCreateMockData = ['000.json', '003.json', '006.json', '009.json'];

const createChartTileBucketWithMockedS3Client = (): ChartTileBucket => {
  const mockS3Client = sinon.createStubInstance(ScienceDataApiS3Client);

  runTypesToCreateMockData.forEach(type => {
    runPrefixesToCreateMockData.forEach(runPrefix => {
      mockS3Client.listObjects.withArgs('test-bucket', path.join(type, runPrefix)).resolves(
        objectNamesToCreateMockData.map(objectName => ({
          Key: path.join(type, runPrefix, objectName),
        })),
      );

      objectNamesToCreateMockData.forEach(objectName => {
        mockS3Client.readObjectContentsAsJson
          .withArgs('test-bucket', path.join(type, runPrefix, objectName))
          .resolves({ tileId: `${type}-${runPrefix}-${objectName}` });
      });
    });
  });

  // TODO: remove this temporary mock when SST data is consistent with other types
  const sstMockObjectKeys = ['sst/2021121309.json', 'sst/2021121409.json', 'sst/2021121509.json'];
  mockS3Client.listObjects
    .withArgs('test-bucket', 'sst')
    .resolves(sstMockObjectKeys.map(k => ({ Key: k })));
  sstMockObjectKeys.forEach(k =>
    mockS3Client.readObjectContentsAsJson.withArgs('test-bucket', k).resolves({ tileId: '123' }),
  );

  return new ChartTileBucket(mockS3Client, 'test-bucket');
};

const chartTileBucket = () => {
  describe('chartTileBucket', () => {
    const chartTileBucket = createChartTileBucketWithMockedS3Client();

    it('should return the most up to date tile at each timestamp in the requested window', async () => {
      const startMoment = moment('2021-12-13T00:00Z');
      const endMoment = moment('2021-12-13T22:00Z');
      const tiles = await chartTileBucket.getForecastChartTiles({
        type: 'WAVE',
        start: startMoment.unix(),
        end: endMoment.unix(),
      });
      expect(tiles.length).to.equal(8);
      expect(tiles[0].id).to.be.equal('wave-2021121300-000.json');
      expect(tiles[1].id).to.be.equal('wave-2021121300-003.json');
      expect(tiles[2].id).to.be.equal('wave-2021121306-000.json');
      expect(tiles[3].id).to.be.equal('wave-2021121306-003.json');
      expect(tiles[4].id).to.be.equal('wave-2021121312-000.json');
      expect(tiles[5].id).to.be.equal('wave-2021121312-003.json');
      expect(tiles[6].id).to.be.equal('wave-2021121312-006.json');
      expect(tiles[7].id).to.be.equal('wave-2021121312-009.json');
    });

    it('should return forecast chart tiles of the right type', async () => {
      const startMoment = moment('2021-12-13T00:00Z');
      const endMoment = moment('2021-12-13T06:00Z');
      const tiles = await chartTileBucket.getForecastChartTiles({
        type: 'WAVE',
        start: startMoment.unix(),
        end: endMoment.unix(),
      });
      expect(tiles.length).to.equal(2);
      expect(tiles.every(t => t.id.toLowerCase().includes('wave'))).to.be.true();
    });

    it('should return forecast chart tiles between start (inclusive) and end (exclusive) times', async () => {
      const startMoment = moment('2021-12-13T00:00Z');
      const endMoment = moment('2021-12-13T06:00Z');
      const tiles = await chartTileBucket.getForecastChartTiles({
        type: 'WAVE',
        start: startMoment.unix(),
        end: endMoment.unix(),
      });
      expect(tiles.length).to.equal(2);
      expect(tiles[0].timestamp).to.equal(startMoment.unix());
      expect(tiles[1].timestamp).to.be.lessThan(endMoment.unix());
    });

    it(
      'should return forecast chart tiles when ' +
        'requested start time is in the past (predicted runs exist) and ' +
        'requested end time is in the future (predicted runs do not exist)',
      async () => {
        const startMoment = moment('2021-12-13T00:00Z');
        const endMoment = moment('2021-12-20T02:00Z');
        const tiles = await chartTileBucket.getForecastChartTiles({
          type: 'WAVE',
          start: startMoment.unix(),
          end: endMoment.unix(),
        });
        expect(tiles.length).to.equal(8);
        expect(tiles[0].timestamp).to.equal(startMoment.unix());
        expect(tiles[7].timestamp).to.equal(moment('2021-12-13T21:00Z').unix());
      },
    );

    it(
      'should return forecast chart tiles when ' +
        'requested start/end times are in the past (predicted runs exist)',
      async () => {
        const startMoment = moment('2021-12-13T00:00Z');
        const endMoment = moment('2021-12-13T19:00Z');
        const tiles = await chartTileBucket.getForecastChartTiles({
          type: 'WAVE',
          start: startMoment.unix(),
          end: endMoment.unix(),
        });
        expect(tiles.length).to.equal(7);
        expect(tiles[0].timestamp).to.equal(startMoment.unix());
        expect(tiles[6].timestamp).to.equal(moment('2021-12-13T18:00Z').unix());
      },
    );

    it(
      'should return forecast chart tiles when requested start/end times are in the future ' +
        '(ideal runs for requested start/end times do not exist)',
      async () => {
        const startMoment = moment('2021-12-13T18:00Z');
        const endMoment = moment('2021-12-15T00:00Z');
        const tiles = await chartTileBucket.getForecastChartTiles({
          type: 'WAVE',
          start: startMoment.unix(),
          end: endMoment.unix(),
        });
        expect(tiles.length).to.equal(2);
        expect(tiles[0].timestamp).to.equal(startMoment.unix());
        expect(tiles[1].timestamp).to.equal(moment('2021-12-13T21:00Z').unix());
      },
    );

    it('handles inconsistent SST data organization correctly', async () => {
      const startMoment = moment('2021-12-13T09:00Z');
      const endMoment = moment('2021-12-15T10:00Z');
      const tiles = await chartTileBucket.getForecastChartTiles({
        type: 'SST',
        start: startMoment.unix(),
        end: endMoment.unix(),
      });
      expect(tiles.length).to.equal(3);
      expect(tiles[0].timestamp).to.equal(startMoment.unix());
      expect(tiles[1].timestamp).to.equal(moment('2021-12-14T09:00Z').unix());
      expect(tiles[2].timestamp).to.equal(moment('2021-12-15T09:00Z').unix());
    });

    it('return tile data for data types that do not match the S3 prefix exactly', async () => {
      const startMoment = moment('2021-12-13T00:00Z');
      const endMoment = moment('2021-12-13T06:00Z');
      const tiles = await chartTileBucket.getForecastChartTiles({
        type: 'SEA_ICE',
        start: startMoment.unix(),
        end: endMoment.unix(),
      });
      expect(tiles.length).to.equal(2);
      expect(tiles[0].timestamp).to.equal(startMoment.unix());
      expect(tiles[1].timestamp).to.be.lessThan(endMoment.unix());
    });
  });
};

export default chartTileBucket;
