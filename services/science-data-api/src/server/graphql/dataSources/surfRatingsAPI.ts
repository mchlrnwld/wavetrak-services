import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../../config';

const ratingsAlgorithmToPathMap = {
  ML_RATINGS_MINIMAL: 'ratings-minimal',
  ML_SPECTRA_REF_MATRIX: 'ratings-spectra-ref-matrix',
};

/**
 * https://github.com/apollographql/apollo-server/tree/main/packages/apollo-datasource-rest
 */
export default class SurfRatingsAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = config.SURF_RATINGS_API;
  }

  async getSurfRatings(model: string, request: any) {
    const path = ratingsAlgorithmToPathMap[model] || model;
    const raw = await this.post(`predictions/${path}`, request);
    return JSON.parse(raw);
  }
}
