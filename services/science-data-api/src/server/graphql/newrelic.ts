import { mapSchema, MapperKind } from '@graphql-tools/utils';
import { GraphQLSchema } from 'graphql';
import nr from 'newrelic';

/**
 * ApolloServer plugin to add NewRelic transaction names custom attributes
 */
export const newrelicTransactionPlugin = {
  requestDidStart: requestContext => {
    if (requestContext.request.operationName) {
      nr.setTransactionName(requestContext.request.operationName);
    }
    const transaction = nr.getTransaction();

    return {
      executionDidStart: rc => {
        nr.addCustomAttribute('query', rc.request.query);
        if (rc.request.variables) {
          nr.addCustomAttribute('variables', JSON.stringify(rc.request.variables));
        }
      },
      didEncounterErrors: rc => {
        nr.noticeError(rc.errors[0].message);
      },
      willSendResponse: () => {
        transaction.end();
      },
    };
  },
};

/**
 * Maps a GraphQLSchema, adding NewRelic segments for each resolver. Original
 * schema is unchanged.
 *
 * @param schema      GraphQLSchema to map
 * @return            New GraphQLSchema
 */
export function instrumentSchema(schema: GraphQLSchema): GraphQLSchema {
  return mapSchema(schema, {
    // Note graphql-tool (which defines `mapSchema`) work with the types from the graphql module
    // (which is agnostic of implementation) and defines that fieldConfig is of type
    // GraphQLInputFieldConfig. Apollo adds the resolver function to these objects, and that is what
    // we want to access, but the GraphQLInputFieldConfig type has no property `resolve`. Specifying
    // `any` on the signiture here get around that.
    [MapperKind.FIELD]: (fieldConfig: any /* GraphQLInputFieldConfig */, fieldName: string) => {
      if (fieldConfig.resolve) {
        const originalResolver = fieldConfig.resolve;
        const type = fieldConfig.type.toString();
        const resolverName = originalResolver.name || 'anonymous';
        if (resolverName === 'anonymous') {
          console.warn(
            `Warning: ${type} has an anonymous resolver. Resolver needs to have a name for New Relic to track its metrics in a segment.`,
          );
        }
        fieldConfig.resolve = function(...rest) {
          return nr.startSegment(`resolver/${fieldName}:${type}/${resolverName}`, true, () =>
            originalResolver.apply(this, rest),
          );
        };
        return fieldConfig;
      }
      return undefined;
    },
  });
}
