import { gql } from 'apollo-server-express';
import { ScienceDataServiceDataSources } from '../dataSources';

const typeDefs = gql`
  enum ForecastChartType {
    WAVE
    WIND
    PRESSURE
    JET_STREAM
    SEA_ICE
    SST
  }

  type Tile {
    id: String!
    timestamp: Int!
  }

  type ForecastChart {
    tiles: [Tile]
    type: ForecastChartType
  }

  extend type Query {
    forecastCharts(type: ForecastChartType!, start: Int!, end: Int!): [ForecastChart]
  }
`;

const resolvers = {
  Query: {
    forecastCharts: async (
      _parent,
      { end, start, type },
      { dataSources: { chartTileBucket } }: { dataSources: ScienceDataServiceDataSources },
      _info,
    ) => {
      const tiles = await chartTileBucket.getForecastChartTiles({ end, start, type });
      return [{ tiles, type }];
    },
  },
};

const schema = { typeDefs, resolvers };

export default schema;
