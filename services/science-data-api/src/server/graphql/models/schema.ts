import { gql } from 'apollo-server-express';
import { MODEL_TYPES } from '../../../models/model/constants';
import { queryModels } from '../../../models/model';
import { queryModelGrids } from '../../../models/modelGrid';
import { queryModelRuns, updateModelRuns } from '../../../models/modelRun';
import { queryPointsOfInterestGridPoints } from '../../../models/pointOfInterestGridPoint';

const typeDefs = gql`
  extend type Query {
    models(agency: String, model: String, types: [ModelType!]): [Model] @cacheControl(maxAge: 0)
  }

  extend type Mutation {
    set_model_run(runs: [SetModelRunInput]): Boolean
  }

  input SetModelRunInput {
    agency: String!
    model: String!
    run: Int!
    status: ModelRunStatus!
    type: ModelRunType!
  }

  type Model {
    agency: String!
    model: String!
    types: [ModelType]!
    runs(statuses: [ModelRunStatus]!, types: [ModelRunType]!): [ModelRun]
    grids(grid: String): [ModelGrid]!
  }

  type ModelRun {
    run: Int!
    status: ModelRunStatus!
    type: ModelRunType!
  }

  type ModelGrid {
    agency: String!
    model: String!
    grid: String!
    description: String
    resolution: Float
    startLatitude: Latitude
    endLatitude: Latitude
    startLongitude: Longitude
    endLongitude: Longitude
    pointsOfInterest: [PointOfInterestGridPoint]!
  }
`;

const resolvers = {
  Query: {
    models: async (_obj, { agency, model, types }) => {
      const models = await queryModels({ agency, model, types });
      return models.map(m => ({
        agency: m.agency,
        model: m.model,
        types: MODEL_TYPES.map(t => (m[t.toLowerCase()] ? t : null)).filter(t => t),
      }));
    },
  },
  Mutation: {
    set_model_run: (_obj, { runs }) => !!updateModelRuns(runs),
  },
  Model: {
    runs: ({ agency, model }, { statuses, types }) =>
      queryModelRuns({ agency, model, statuses, types }),
    grids: ({ agency, model }, { grid }) => queryModelGrids({ agency, model, grid }),
  },
  ModelGrid: {
    pointsOfInterest: ({ agency, model, grid }) =>
      queryPointsOfInterestGridPoints({ agency, model, grid }),
  },
};

const schema = { typeDefs, resolvers };

export default schema;
