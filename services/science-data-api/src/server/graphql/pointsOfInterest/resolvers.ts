import {
  updatePointOfInterest as update,
  insertPointOfInterest as insert,
} from '../../../models/pointOfInterest';

const wrapMutationError = mutation => async data => {
  try {
    const { pointOfInterestId } = await mutation(data);
    return {
      success: true,
      message: 'Success',
      pointOfInterestId,
    };
  } catch (e) {
    return {
      success: false,
      message: e.message,
      errorJSON: e.body && JSON.stringify(e.body),
      pointOfInterestId: data.pointOfInterestId,
    };
  }
};

export const createPointOfInterest = wrapMutationError(insert);
export const updatePointOfInterest = wrapMutationError(update);
