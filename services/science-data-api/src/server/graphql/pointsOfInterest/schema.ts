import { gql } from 'apollo-server-express';
import { queryModelGrids } from '../../../models/modelGrid';
import { queryPointOfInterest } from '../../../models/pointOfInterest';
import { queryPointsOfInterestGridPoints } from '../../../models/pointOfInterestGridPoint';
import { queryPointsOfInterestDefaultForecastModels } from '../../../models/pointOfInterestDefaultForecastModel';
import { createPointOfInterest, updatePointOfInterest } from './resolvers';
import {
  upsertPointOfInterestGridPoints,
  removePointOfInterestGridPoints,
} from './pointOfInterestGridPoint';
import {
  upsertPointOfInterestDefaultForecastModels,
  removePointOfInterestDefaultForecastModels,
} from './pointOfInterestDefaultForecastModel';
import { upsertSurfSpotConfiguration } from './surfSpotConfiguration';
import { ScienceDataServiceDataSources } from '../dataSources';
import { GraphQLJSON } from 'graphql-type-json';

const typeDefs = gql`
  scalar JSON

  extend type Query {
    pointOfInterest(pointOfInterestId: String!): PointOfInterest @cacheControl(maxAge: 0)
  }

  extend type Mutation {
    createPointOfInterest(
      name: String!
      lat: Latitude!
      lon: Longitude!
    ): PointOfInterestMutationResponse
    updatePointOfInterest(
      pointOfInterestId: String!
      name: String!
      lat: Latitude!
      lon: Longitude!
    ): PointOfInterestMutationResponse
    upsertSurfSpotConfiguration(
      pointOfInterestId: String!
      surfSpotConfiguration: SurfSpotConfigurationInput!
    ): PointOfInterestMutationResponse
    upsertPointOfInterestGridPoints(
      pointOfInterestId: String!
      gridPoints: [PointOfInterestGridPointInput]!
    ): PointOfInterestMutationResponse
    removePointOfInterestGridPoints(
      pointOfInterestId: String!
      gridPoints: [RemovePointOfInterestGridPointInput]!
    ): PointOfInterestMutationResponse
    upsertPointOfInterestDefaultForecastModels(
      pointOfInterestId: String!
      defaultForecastModels: [PointOfInterestDefaultForecastModelInput]!
    ): PointOfInterestMutationResponse
    removePointOfInterestDefaultForecastModels(
      pointOfInterestId: String!
      forecastTypes: [ForecastType]!
    ): PointOfInterestMutationResponse
  }

  type PointOfInterestMutationResponse implements MutationResponse {
    success: Boolean!
    message: String!
    errorJSON: String
    pointOfInterest: PointOfInterest
  }

  input SurfSpotConfigurationInput {
    offshoreDirection: Float!
    optimalSwellDirection: Float
    breakingWaveHeightCoefficient: Float
    breakingWaveHeightIntercept: Float
    spectralRefractionMatrix: [[Float]]
    breakingWaveHeightAlgorithm: BreakingWaveHeightAlgorithm!
    ratingsAlgorithm: String
    ratingsMatrix: [[Float]]
    ratingsOptions: JSON
  }

  input PointOfInterestInput {
    name: String!
    lat: Latitude!
    lon: Longitude!
  }

  input RemovePointOfInterestGridPointInput {
    agency: String!
    model: String!
    grid: String!
  }

  input PointOfInterestGridPointInput {
    agency: String!
    model: String!
    grid: String!
    lat: Latitude!
    lon: Longitude!
  }

  input PointOfInterestDefaultForecastModelInput {
    forecastType: ForecastType!
    agency: String!
    model: String!
    grid: String!
  }

  type PointOfInterest @key(fields: "pointOfInterestId") {
    pointOfInterestId: String!
    name: String!
    lat: Latitude!
    lon: Longitude!
    defaultForecastModels: [PointOfInterestDefaultForecastModel]!
    gridPoints: [PointOfInterestGridPoint]!
    surfSpotConfiguration: SurfSpotConfiguration
  }

  type PointOfInterestGridPoint {
    id: String!
    lat: Latitude!
    lon: Longitude!
    grid: ModelGrid!
    surfSpotConfiguration: SurfSpotConfiguration
  }

  type PointOfInterestDefaultForecastModel {
    pointOfInterestId: String!
    forecastType: ForecastType!
    agency: String!
    model: String!
    grid: ModelGrid!
  }

  type SurfSpotConfiguration {
    breakingWaveHeightAlgorithm: BreakingWaveHeightAlgorithm!
    breakingWaveHeightCoefficient: Float
    breakingWaveHeightIntercept: Float
    offshoreDirection: Float!
    optimalSwellDirection: Float
    ratingsAlgorithm: String
    ratingsMatrix: [[Float]]
    ratingsOptions: JSON
    spectralRefractionMatrix: [[Float]]
  }
`;

const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    pointOfInterest: (_obj, { pointOfInterestId }) => queryPointOfInterest({ pointOfInterestId }),
  },
  PointOfInterest: {
    defaultForecastModels: queryPointsOfInterestDefaultForecastModels,
    gridPoints: ({ pointOfInterestId }, { agency, model, grid }) =>
      queryPointsOfInterestGridPoints({ pointOfInterestId, agency, model, grid }),
    surfSpotConfiguration: (
      { pointOfInterestId }: { pointOfInterestId: string },
      _,
      { dataSources }: { dataSources: ScienceDataServiceDataSources },
    ) => dataSources.sciencePlatformDB.getSurfSpotConfigurationRecord(pointOfInterestId),
    __resolveReference: queryPointOfInterest,
  },
  PointOfInterestGridPoint: {
    id: ({ pointOfInterestId }) => pointOfInterestId,
    grid: async ({ agency, model, grid }) => {
      return (await queryModelGrids({ agency, model, grid }))[0];
    },
    surfSpotConfiguration: (
      { pointOfInterestId }: { pointOfInterestId: string },
      _,
      { dataSources }: { dataSources: ScienceDataServiceDataSources },
    ) => dataSources.sciencePlatformDB.getSurfSpotConfigurationRecord(pointOfInterestId),
  },
  PointOfInterestDefaultForecastModel: {
    grid: async ({ agency, model, grid }) => {
      return (await queryModelGrids({ agency, model, grid }))[0];
    },
  },
  Mutation: {
    createPointOfInterest: (_obj, poiData) => createPointOfInterest(poiData),
    updatePointOfInterest: (_obj, poiData) => updatePointOfInterest(poiData),
    upsertSurfSpotConfiguration: (_obj, { pointOfInterestId, surfSpotConfiguration }) =>
      upsertSurfSpotConfiguration(pointOfInterestId, surfSpotConfiguration),
    upsertPointOfInterestGridPoints: (_obj, { pointOfInterestId, gridPoints }) =>
      upsertPointOfInterestGridPoints(pointOfInterestId, gridPoints),
    removePointOfInterestGridPoints: (_obj, { pointOfInterestId, gridPoints }) =>
      removePointOfInterestGridPoints(pointOfInterestId, gridPoints),
    upsertPointOfInterestDefaultForecastModels: (
      _obj,
      { pointOfInterestId, defaultForecastModels },
    ) => upsertPointOfInterestDefaultForecastModels(pointOfInterestId, defaultForecastModels),
    removePointOfInterestDefaultForecastModels: (_obj, { pointOfInterestId, forecastTypes }) =>
      removePointOfInterestDefaultForecastModels(pointOfInterestId, forecastTypes),
  },
  PointOfInterestMutationResponse: {
    pointOfInterest: ({ pointOfInterestId }) => queryPointOfInterest({ pointOfInterestId }),
  },
};

const schema = { typeDefs, resolvers };

export default schema;
