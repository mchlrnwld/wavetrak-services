import {
  // eslint-disable-next-line no-unused-vars
  POIGridPointData,
  removePointOfInterestGridPoints as remove,
  upsertPointsOfInterestGridPoints as upsert,
} from '../../../models/pointOfInterestGridPoint';

export const removePointOfInterestGridPoints = async (
  pointOfInterestId: string,
  gridPoints: Array<{ agency: string; model: string; grid: string }>,
) => {
  try {
    await remove(pointOfInterestId, gridPoints);
  } catch (e) {
    return {
      success: false,
      message: e.message,
      errorJSON: e.body && JSON.stringify(e.body),
      pointOfInterestId,
    };
  }

  return {
    success: true,
    message: 'Success',
    pointOfInterestId,
  };
};

export const upsertPointOfInterestGridPoints = async (
  pointOfInterestId: string,
  gridPoints: Array<POIGridPointData>,
) => {
  try {
    await upsert(pointOfInterestId, gridPoints);
  } catch (e) {
    return {
      success: false,
      message: e.message,
      errorJSON: e.body && JSON.stringify(e.body),
      pointOfInterestId,
    };
  }

  return {
    success: true,
    message: 'Success',
    pointOfInterestId,
  };
};
