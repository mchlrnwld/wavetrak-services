import {
  removePointOfInterestDefaultForecastModels as remove,
  upsertPointOfInterestDefaultForecastModels as upsert,
} from '../../../models/pointOfInterestDefaultForecastModel';
import {
  PointOfInterestDefaultForecastModel,
  ForecastType,
} from '../../../models/pointOfInterestDefaultForecastModel/index.d';

export const removePointOfInterestDefaultForecastModels = async (
  pointOfInterestId: string,
  forecastTypes: ForecastType[],
) => {
  try {
    await remove(pointOfInterestId, forecastTypes);
  } catch (e) {
    return {
      success: false,
      message: e.message,
      errorJSON: e.body && JSON.stringify(e.body),
      pointOfInterestId,
    };
  }

  return {
    success: true,
    message: 'Success',
    pointOfInterestId,
  };
};

export const upsertPointOfInterestDefaultForecastModels = async (
  pointOfInterestId: string,
  defaultForecastModels: PointOfInterestDefaultForecastModel[],
) => {
  try {
    await upsert(pointOfInterestId, defaultForecastModels);
  } catch (e) {
    return {
      success: false,
      message: e.message,
      errorJSON: e.body && JSON.stringify(e.body),
      pointOfInterestId,
    };
  }

  return {
    success: true,
    message: 'Success',
    pointOfInterestId,
  };
};
