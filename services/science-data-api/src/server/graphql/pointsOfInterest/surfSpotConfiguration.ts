import { SurfSpotConfigurationRecord } from '../../../models/surfSpotConfigurations/types';
import { upsertSurfSpotConfiguration as upsert } from '../../../models/surfSpotConfigurations';

export const upsertSurfSpotConfiguration = async (
  pointOfInterestId: string,
  configuration: SurfSpotConfigurationRecord,
) => {
  try {
    await upsert(pointOfInterestId, configuration);
  } catch (e) {
    return {
      success: false,
      message: e.message,
      errorJSON: e.body && JSON.stringify(e.body),
      pointOfInterestId,
    };
  }

  return {
    success: true,
    message: 'Success',
    pointOfInterestId,
  };
};
