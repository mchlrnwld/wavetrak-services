import { expect } from 'chai';
import blendData from './blendData';

const blendDataSpec = () => {
  describe('blendData', () => {
    it('blends wind data by choosing highest grid resolution for each forecast hour.', () => {
      const models = [
        {
          agency: 'NOAA',
          model: 'GFS',
          run: 2020042906,
          grid: { key: '0p25', lat: 43.75, lon: 7.75, resolution: 0.25 },
        },
        {
          agency: 'NOAA',
          model: 'NAM',
          run: 2020042906,
          grid: { key: 'hawaii', lat: 43.75, lon: 7.75, resolution: 0.027 },
        },
      ];
      const windData = [
        [
          {
            timestamp: 1587902400,
            run: 2020042906,
            wind: { direction: 30.18452, speed: 10.34788, gust: 22.92564 },
          },
          {
            timestamp: 1587913200,
            run: 2020042906,
            wind: { direction: 30.18452, speed: 10.34788, gust: 22.92564 },
          },
        ],
        [
          {
            timestamp: 1587913200,
            run: 2020042906,
            wind: { direction: 3.18452, speed: 31.34788, gust: 4.92564 },
          },
          {
            timestamp: 1587916800,
            run: 2020042906,
            wind: { direction: 28.18452, speed: 21.34788, gust: 8.92564 },
          },
          {
            timestamp: 1587920400,
            run: 2020042906,
            wind: { direction: 35.18452, speed: 11.066, gust: 20.92564 },
          },
        ],
      ];
      expect(blendData(windData, models)).to.deep.equal([
        {
          timestamp: 1587902400,
          run: 2020042906,
          wind: { direction: 30.18452, speed: 10.34788, gust: 22.92564 },
        },
        {
          timestamp: 1587913200,
          run: 2020042906,
          wind: { direction: 3.18452, speed: 31.34788, gust: 4.92564 },
        },
        {
          timestamp: 1587916800,
          run: 2020042906,
          wind: { direction: 28.18452, speed: 21.34788, gust: 8.92564 },
        },
        {
          timestamp: 1587920400,
          run: 2020042906,
          wind: { direction: 35.18452, speed: 11.066, gust: 20.92564 },
        },
      ]);
    });
    it('blends weather data by choosing highest grid resolution for each forecast hour.', () => {
      const models = [
        {
          agency: 'NOAA',
          model: 'GFS',
          run: 2020042906,
          grid: { key: '0p25', lat: 43.75, lon: 7.75, resolution: 0.25 },
        },
        {
          agency: 'NOAA',
          model: 'NAM',
          run: 2020042906,
          grid: { key: 'hawaii', lat: 43.75, lon: 7.75, resolution: 0.027 },
        },
      ];
      const weatherData = [
        [
          {
            timestamp: 1587913200,
            run: 2020042906,
            weather: {
              pressure: 1008.40125,
              temperature: 15.95001,
              visibility: 24135.107,
              dewpoint: 10.86917,
              humidity: 71.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1587916800,
            run: 2020042906,
            weather: {
              pressure: 2000.40125,
              temperature: 15.95001,
              visibility: 24135.107,
              dewpoint: 10.86917,
              humidity: 71.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1587920400,
            run: 2020042906,
            weather: {
              pressure: 900.40125,
              temperature: 15.95001,
              visibility: 20000.107,
              dewpoint: 15.86917,
              humidity: 71.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
        ],
        [
          {
            timestamp: 1587913200,
            run: 2020042906,
            weather: {
              pressure: 1008.40125,
              temperature: 15.95001,
              visibility: 24135.107,
              dewpoint: 10.86917,
              humidity: 50.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1587916800,
            run: 2020042906,
            weather: {
              pressure: 1000.40125,
              temperature: 10.95001,
              visibility: 22000.107,
              dewpoint: 10.05,
              humidity: 69.8,
              precipitation: {
                type: 'NONE',
                volume: 0.0,
              },
              conditions: 'CLEAR',
            },
          },
        ],
      ];
      expect(blendData(weatherData, models)).to.deep.equal([
        {
          timestamp: 1587913200,
          run: 2020042906,
          weather: {
            pressure: 1008.40125,
            temperature: 15.95001,
            visibility: 24135.107,
            dewpoint: 10.86917,
            humidity: 50.8,
            precipitation: {
              type: 'NONE',
              volume: 1.0,
            },
            conditions: 'CLEAR',
          },
        },
        {
          timestamp: 1587916800,
          run: 2020042906,
          weather: {
            pressure: 1000.40125,
            temperature: 10.95001,
            visibility: 22000.107,
            dewpoint: 10.05,
            humidity: 69.8,
            precipitation: {
              type: 'NONE',
              volume: 0.0,
            },
            conditions: 'CLEAR',
          },
        },
        {
          timestamp: 1587920400,
          run: 2020042906,
          weather: {
            pressure: 900.40125,
            temperature: 15.95001,
            visibility: 20000.107,
            dewpoint: 15.86917,
            humidity: 71.8,
            precipitation: {
              type: 'NONE',
              volume: 1.0,
            },
            conditions: 'CLEAR',
          },
        },
      ]);
    });
    it('does not blend data for a single model', () => {
      const models = [
        {
          agency: 'NOAA',
          model: 'GFS',
          run: 2020042906,
          grid: { key: '0p25', lat: 43.75, lon: 7.75, resolution: null },
        },
      ];
      const weatherData = [
        [
          {
            timestamp: 1587913200,
            run: 2020042906,
            weather: {
              pressure: 1008.40125,
              temperature: 15.95001,
              visibility: 24135.107,
              dewpoint: 10.86917,
              humidity: 71.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1587916800,
            run: 2020042906,
            weather: {
              pressure: 2000.40125,
              temperature: 15.95001,
              visibility: 24135.107,
              dewpoint: 10.86917,
              humidity: 71.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1587920400,
            run: 2020042906,
            weather: {
              pressure: 900.40125,
              temperature: 15.95001,
              visibility: 20000.107,
              dewpoint: 15.86917,
              humidity: 71.8,
              precipitation: {
                type: 'NONE',
                volume: 1.0,
              },
              conditions: 'CLEAR',
            },
          },
        ],
      ];
      expect(blendData(weatherData, models)).to.deep.equal([
        {
          timestamp: 1587913200,
          run: 2020042906,
          weather: {
            pressure: 1008.40125,
            temperature: 15.95001,
            visibility: 24135.107,
            dewpoint: 10.86917,
            humidity: 71.8,
            precipitation: {
              type: 'NONE',
              volume: 1.0,
            },
            conditions: 'CLEAR',
          },
        },
        {
          timestamp: 1587916800,
          run: 2020042906,
          weather: {
            pressure: 2000.40125,
            temperature: 15.95001,
            visibility: 24135.107,
            dewpoint: 10.86917,
            humidity: 71.8,
            precipitation: {
              type: 'NONE',
              volume: 1.0,
            },
            conditions: 'CLEAR',
          },
        },
        {
          timestamp: 1587920400,
          run: 2020042906,
          weather: {
            pressure: 900.40125,
            temperature: 15.95001,
            visibility: 20000.107,
            dewpoint: 15.86917,
            humidity: 71.8,
            precipitation: {
              type: 'NONE',
              volume: 1.0,
            },
            conditions: 'CLEAR',
          },
        },
      ]);
    });
  });
};

export default blendDataSpec;
