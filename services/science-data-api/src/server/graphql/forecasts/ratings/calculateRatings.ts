import log from '../../../../common/logger';
import defaultRatingsMatrix from './defaultRatingsMatrix';
import { zip } from 'lodash';

export interface RatingConfiguration {
  offshoreDirection: number;
  ratingsMatrix?: number[][];
}

export interface RatingInput {
  surf: {
    breakingWaveHeightMin: number;
    breakingWaveHeightMax: number;
  };
  timestamp: number;
  wind: {
    speed: number;
    direction: number;
  };
}

export interface RatingResult {
  timestamp: number;
  rating: number;
}

const normalizeWindDirection = (direction: number, offshoreDirection: number): number => {
  let deltaFromOffshoreDirection = direction - offshoreDirection;
  if (deltaFromOffshoreDirection >= 180)
    deltaFromOffshoreDirection = deltaFromOffshoreDirection - 360;
  if (deltaFromOffshoreDirection < -180)
    deltaFromOffshoreDirection = deltaFromOffshoreDirection + 360;
  return deltaFromOffshoreDirection;
};

// https://github.com/Surfline/surfline-labs/blob/deploy_ratingv3/forecast_api/src/lib/model_err.py#L108
const smoothRatingResults = (ratingInputs: RatingInput[], ratingResults: RatingResult[]): any[] => {
  const smoothed = [];
  let cachedDirection = 0;
  let cachedSpeed = 0;
  let cachedRating = 1;
  let directionFlag = false;
  let speedFlag = false;
  let breakingWaveHeightFlag = false;

  const windDirections = ratingInputs.map(r => r.wind.direction);
  const windSpeeds = ratingInputs.map(r => r.wind.speed);
  const predictions = ratingResults.map(r => r.rating);
  const minBreakingWaveHeights = ratingInputs.map(r => r.surf.breakingWaveHeightMin || 0);
  const maxBreakingWaveHeights = ratingInputs.map(r => r.surf.breakingWaveHeightMax || 0);

  zip(
    windDirections,
    windSpeeds,
    minBreakingWaveHeights,
    maxBreakingWaveHeights,
    predictions,
  ).forEach(
    (
      [windDirection, windSpeed, minBreakingWaveHeight, maxBreakingWaveHeight, prediction],
      index,
    ) => {
      if (index == 0) {
        smoothed.push(prediction);
        cachedRating = prediction;
        cachedDirection = windDirection;
        cachedSpeed = windSpeed;
      } else if (prediction == cachedRating) {
        smoothed.push(prediction);
        cachedRating = prediction;
        cachedDirection = windDirection;
        cachedSpeed = windSpeed;
      } else {
        // Macro: What kind of change?
        const levelup = cachedRating < prediction;
        const bigger =
          minBreakingWaveHeight > minBreakingWaveHeights[index - 1] ||
          maxBreakingWaveHeight > maxBreakingWaveHeights[index - 1];

        // Increase in surf height, increase in rating OK
        if (
          minBreakingWaveHeight != minBreakingWaveHeights[index - 1] ||
          maxBreakingWaveHeight != maxBreakingWaveHeights[index - 1]
        ) {
          if (bigger && levelup) {
            smoothed.push(prediction);
            cachedRating = prediction;
            cachedDirection = windDirection;
            cachedSpeed = windSpeed;
            return;
          }
        }

        // Check if changes in features were small!
        if (Math.abs(windDirection - cachedDirection) <= 5) directionFlag = true;

        if (Math.abs(windSpeed - cachedSpeed) <= 5) speedFlag = true;

        if (
          Math.abs(minBreakingWaveHeight - minBreakingWaveHeights[index - 1]) <= 1 ||
          Math.abs(maxBreakingWaveHeight - maxBreakingWaveHeights[index - 1]) <= 1
        )
          breakingWaveHeightFlag = true;

        // If it was just a small changes all around, hold at last rating
        if (directionFlag && speedFlag && breakingWaveHeightFlag) {
          smoothed.push(cachedRating);
          // Do not update cache yet
        } else {
          // Otherwise, allow change & update cached values
          smoothed.push(prediction);
          cachedRating = prediction;
          cachedDirection = windDirection;
          cachedSpeed = windSpeed;
        }

        directionFlag = false;
        speedFlag = false;
        breakingWaveHeightFlag = false;
      }
    },
  );
  return ratingResults.map((r, i) => ({ ...r, rating: smoothed[i] }));
};

export default (
  { offshoreDirection, ratingsMatrix }: RatingConfiguration,
  inputs: RatingInput[],
): RatingResult[] => {
  const inputsWithNormalizedWindDirection = inputs.map(i => ({
    ...i,
    wind: { ...i.wind, direction: normalizeWindDirection(i.wind.direction, offshoreDirection) },
  }));

  const results = inputsWithNormalizedWindDirection.map(i => {
    const { surf, timestamp, wind } = i;

    const rating = (ratingsMatrix || defaultRatingsMatrix).find(r => {
      const withinBreakingWaveHeightRange =
        r[0] <= surf.breakingWaveHeightMax && surf.breakingWaveHeightMax < r[1];
      const withinWindDirectionRange = r[2] <= wind.direction && wind.direction < r[3];
      const withinWindSpeedRange = r[4] <= wind.speed && wind.speed < r[5];
      return withinBreakingWaveHeightRange && withinWindDirectionRange && withinWindSpeedRange;
    });

    if (!rating) {
      log.error(`No rating found for input: ${JSON.stringify(i)}`);
      return { timestamp, rating: null };
    }

    return { timestamp, rating: rating[6] };
  });

  return smoothRatingResults(inputs, results);
};
