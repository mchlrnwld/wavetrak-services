import { expect } from 'chai';
import calculateRatings from './calculateRatings';

export default () => {
  describe('calculateRatings', () => {
    it('should determine ratings based on breaking wave height', async () => {
      const configuration = {
        offshoreDirection: 180,
        ratingsMatrix: [
          [0, 1, 0, 10, 0, 10, 1],
          [1, 2, 0, 10, 0, 10, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 5,
            direction: 180,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 1,
          },
          timestamp: 1609462800,
          wind: {
            speed: 5,
            direction: 180,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(2);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(2);
    });

    it('should determine ratings based on wind direction', async () => {
      const configuration = {
        offshoreDirection: 180,
        ratingsMatrix: [
          [0, 1, 0, 25, 0, 10, 1],
          [0, 1, 25, 50, 0, 10, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 5,
            direction: 180,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609462800,
          wind: {
            speed: 7,
            direction: 225,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(2);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(2);
    });

    it('should determine ratings based on wind speed', async () => {
      const configuration = {
        offshoreDirection: 180,
        ratingsMatrix: [
          [0, 1, 0, 10, 0, 10, 1],
          [0, 1, 0, 10, 10, 20, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 5,
            direction: 180,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609462800,
          wind: {
            speed: 15,
            direction: 180,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(2);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(2);
    });

    it('should determine ratings for a two week hourly forecast using the default matrix in under 200ms', async () => {
      const configuration = {
        offshoreDirection: 180,
      };

      const inputs = [];

      for (let i = 0; i < 14 * 24; i++) {
        inputs.push({
          surf: {
            breakingWaveHeightMax: Math.random() * 99,
          },
          timestamp: 1609459200 + i * 3600,
          wind: {
            speed: Math.random() * 100,
            direction: Math.random() * 360,
          },
        });
      }

      const start = process.hrtime();
      const ratingForecast = calculateRatings(configuration, inputs);
      const elapsed = process.hrtime(start);

      const ms = elapsed[0] * 1000 + elapsed[1] / 1000000;
      console.info(`calculated ${14 * 24} ratings in: ${ms}ms`);
      expect(ratingForecast).to.have.length(14 * 24);
      expect(ms).to.be.lessThan(200);
    });

    it('should handle wind direction differences greater than 180 and less than -180', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 90, 0, 10, 1],
          [0, 1, 90, 180, 0, 10, 2],
          [0, 1, -90, 0, 0, 10, 3],
          [0, 1, -180, -90, 0, 10, 4],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 7,
            direction: 45,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609462800,
          wind: {
            speed: 7,
            direction: 135,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609466400,
          wind: {
            speed: 7,
            direction: 225,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609470000,
          wind: {
            speed: 7,
            direction: 315,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(4);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(2);
      expect(ratingForecast[2].rating).to.equal(4);
      expect(ratingForecast[3].rating).to.equal(3);
    });

    it('returns null for breaking wave height input not found in the ratings matrix', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 90, 0, 10, 1],
          [0, 1, 90, 180, 0, 10, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 7,
            direction: 45,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 2,
          },
          timestamp: 1609462800,
          wind: {
            speed: 7,
            direction: 135,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(2);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(null);
    });

    it('returns null for wind direction input not found in the ratings matrix', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 90, 0, 10, 1],
          [0, 1, 90, 180, 0, 10, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 7,
            direction: 45,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609462800,
          wind: {
            speed: 7,
            direction: -135,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(2);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(null);
    });

    it('returns null for wind speed input not found in the ratings matrix', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 90, 0, 10, 1],
          [0, 1, 90, 180, 0, 10, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609459200,
          wind: {
            speed: 7,
            direction: 45,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0,
          },
          timestamp: 1609462800,
          wind: {
            speed: 20,
            direction: 112,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(2);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(null);
    });

    it('smooths through rating changes due to a wind difference of less than 5 knots', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 90, 0, 5, 1],
          [0, 1, 0, 90, 5, 10, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0.5,
          },
          timestamp: 1609459200,
          wind: {
            speed: 4,
            direction: 10,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0.5,
          },
          timestamp: 1609462800,
          wind: {
            speed: 6,
            direction: 10,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0.5,
          },
          timestamp: 1609466400,
          wind: {
            speed: 4,
            direction: 10,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(3);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(1);
      expect(ratingForecast[2].rating).to.equal(1);
    });

    it('smooths through rating changes due to a wind direction difference of less than 5 degrees', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 10, 0, 5, 1],
          [0, 1, 10, 20, 0, 5, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMax: 0.5,
          },
          timestamp: 1609459200,
          wind: {
            speed: 4,
            direction: 8,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0.5,
          },
          timestamp: 1609462800,
          wind: {
            speed: 4,
            direction: 12,
          },
        },
        {
          surf: {
            breakingWaveHeightMax: 0.5,
          },
          timestamp: 1609466400,
          wind: {
            speed: 4,
            direction: 8,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(3);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(1);
      expect(ratingForecast[2].rating).to.equal(1);
    });

    it('does not smooth through rating changes due to a max breaking wave height different of less than 1 foot', async () => {
      const configuration = {
        offshoreDirection: 0,
        ratingsMatrix: [
          [0, 1, 0, 10, 0, 5, 1],
          [1, 2, 0, 10, 0, 5, 2],
        ],
      };

      const inputs = [
        {
          surf: {
            breakingWaveHeightMin: 0.5,
            breakingWaveHeightMax: 0.8,
          },
          timestamp: 1609459200,
          wind: {
            speed: 4,
            direction: 8,
          },
        },
        {
          surf: {
            breakingWaveHeightMin: 0.5,
            breakingWaveHeightMax: 1.2,
          },
          timestamp: 1609462800,
          wind: {
            speed: 4,
            direction: 8,
          },
        },
        {
          surf: {
            breakingWaveHeightMin: 0.5,
            breakingWaveHeightMax: 0.8,
          },
          timestamp: 1609466400,
          wind: {
            speed: 4,
            direction: 8,
          },
        },
      ];

      const ratingForecast = calculateRatings(configuration, inputs);

      expect(ratingForecast).to.have.length(3);
      expect(ratingForecast[0].rating).to.equal(1);
      expect(ratingForecast[1].rating).to.equal(2);
      expect(ratingForecast[2].rating).to.equal(2);
    });
  });
};
