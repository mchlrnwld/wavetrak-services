import blendDataSpec from './blendData.spec';
import handleStartEndMinutesSpec from './handleStartEndMinutes.spec';
import resolversSpec from './resolvers.spec';
import assimilationSpec from './assimilation.spec';
import getSurfRatingsSpec from './getSurfRatings.spec';
import calculateRatingSpec from './ratings/calculateRatings.spec';

const forecastsSpec = () => {
  describe('forecasts', () => {
    assimilationSpec();
    blendDataSpec();
    calculateRatingSpec();
    getSurfRatingsSpec();
    handleStartEndMinutesSpec();
    resolversSpec();
  });
};

export default forecastsSpec;
