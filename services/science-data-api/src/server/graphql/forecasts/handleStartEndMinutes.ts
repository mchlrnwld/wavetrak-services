import { ScienceDataServiceDataSources } from '../dataSources';

export interface ParentResolverReturnValue {
  start?: number;
  end?: number;
}

export interface HandleStartEndMinutesReturn {
  timestamp: number;
}

/**
 * Decorates a query function where start and end are required to be on the hour for UTC. If start
 * and end are not on the hour, the query function is called for extra data and data points'
 * timestamps remapped to fit the requested start and end.
 *
 * @param resolverFunc - Query function wrapped. Takes as an argument an object with start and end
 *                    properties.
 * @return Array of timestamp adjusted data from the result of the query function.
 */
const handleStartEndMinutes = (
  resolverFunc: (
    parent: ParentResolverReturnValue,
    args: any,
    context: { dataSources: ScienceDataServiceDataSources },
  ) => Promise<HandleStartEndMinutesReturn[]>,
) => {
  const decoratedQueryFunc = async (parent, args, context) => {
    if (parent.start) {
      const startDate = new Date(parent.start * 1000);
      const startMinutes = startDate.getMinutes();
      const startSeconds = startDate.getSeconds();
      const start = parent.start - startMinutes * 60 - startSeconds;
      const end = parent.end ? parent.end - startMinutes * 60 - startSeconds : parent.end;
      const data = await resolverFunc({ ...parent, start, end }, args, context);
      return data
        ? data.map(point => ({
            ...point,
            timestamp: point.timestamp + startMinutes * 60 + startSeconds,
          }))
        : null;
    } else {
      return await resolverFunc(parent, args, context);
    }
  };

  // Preserve name for New Relic
  Object.defineProperty(decoratedQueryFunc, 'name', { value: resolverFunc.name, writable: false });

  return decoratedQueryFunc;
};

export default handleStartEndMinutes;
