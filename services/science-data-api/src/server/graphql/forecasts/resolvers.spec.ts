import { expect } from 'chai';
import sinon from 'sinon';
import {
  getRatingForecast,
  getSurfSpotSurfForecastData,
  getSurfSpotSwellsForecastData,
  correctQueryStartEnd,
} from './resolvers';
import { SurfRecord } from '../../../models/surf/types';
import { ReportedSurfHeightRecord } from '../../../models/reportedSurfHeights/index.d';
import { SwellsRecord } from '../../../models/swells/index.d';
import { ScienceDataServiceDataSources } from '../dataSources';
import SciencePlatformDB from '../dataSources/sciencePlatformDB';
import { WindRecord } from '../../../models/wind/types';
import { SurfSpotConfigurationRecord } from '../../../models/surfSpotConfigurations/types';
import SurfRatingsAPI from '../dataSources/surfRatingsAPI';

const createMockContext = () => {
  const reportedSurfHeightRecords: ReportedSurfHeightRecord[] = [
    {
      forecastTime: new Date('2020-08-25T05:00:00.000Z'),
      min: 2,
      max: 4,
      pointOfInterestId: '1234',
    },
    {
      forecastTime: new Date('2020-08-25T08:00:00.000Z'),
      min: 5,
      max: 8,
      pointOfInterestId: '1234',
    },
  ];
  const surfRecords: SurfRecord[] = [
    {
      forecastTime: new Date('2020-08-25T04:00:00.000Z'),
      run: 1598313600000,
      breakingWaveHeightMin: 0.3 * 0.3048,
      breakingWaveHeightMax: 0.9 * 0.3048,
    },
    // Matches with a reported surf height.
    {
      forecastTime: new Date('2020-08-25T05:00:00.000Z'),
      run: 1598313600000,
      breakingWaveHeightMin: 0.5 * 0.3048,
      breakingWaveHeightMax: 1.0 * 0.3048,
    },
    {
      forecastTime: new Date('2020-08-25T06:00:00.000Z'),
      run: 1598313600000,
      breakingWaveHeightMin: 1.02 * 0.3048,
      breakingWaveHeightMax: 1.22 * 0.3048,
    },
    {
      forecastTime: new Date('2020-08-25T07:00:00.000Z'),
      run: 1598313600000,
      breakingWaveHeightMin: 0.41 * 0.3048,
      breakingWaveHeightMax: 0.56 * 0.3048,
    },
    // Matches with a reported surf height.
    {
      forecastTime: new Date('2020-08-25T08:00:00.000Z'),
      run: 1598313600000,
      breakingWaveHeightMin: 0.11 * 0.3048,
      breakingWaveHeightMax: 0.41 * 0.3048,
    },
    {
      forecastTime: new Date('2020-08-25T09:00:00.000Z'),
      run: 1598313600000,
      breakingWaveHeightMin: 0.05 * 0.3048,
      breakingWaveHeightMax: 0.34 * 0.3048,
    },
  ];
  const swellRecords: SwellsRecord[] = [
    {
      run: 0,
      forecastTime: 0,
      height: 1.32,
      direction: 205.7,
      period: 11,
      swellWave_1Height: 1.05,
      swellWave_1Direction: 174.88,
      swellWave_1Period: 8,
      swellWave_1Spread: 20.11,
      swellWave_1Impact: 0.5,
      swellWave_2Height: 0.73,
      swellWave_2Direction: 205.55,
      swellWave_2Period: 12,
      swellWave_2Spread: 20.12,
      swellWave_2Impact: 0.2,
      swellWave_3Height: 0.27,
      swellWave_3Direction: 205.63,
      swellWave_3Period: 16,
      swellWave_3Spread: 20.13,
      swellWave_3Impact: 0.1,
      swellWave_4Height: 0.06,
      swellWave_4Direction: 207.87,
      swellWave_4Period: 21,
      swellWave_4Spread: 20.14,
      swellWave_4Impact: 0.0,
      swellWave_5Height: 0.06,
      swellWave_5Direction: 159.14,
      swellWave_5Period: 13,
      swellWave_5Spread: 20.15,
      swellWave_5Impact: 0.0,
      windWaveHeight: 0,
      windWaveDirection: 159.14,
      windWavePeriod: 13,
      windWaveSpread: 20.16,
      windWaveImpact: 0.0,
      spectra1dEnergy: [...Array(27)].map((_, i) => i * 0.01),
      spectra1dDirection: [...Array(27)].map((_, i) => i * 3),
      spectra1dDirectionalSpread: [...Array(27)].map(() => 10),
    },
  ];
  const windRecords: WindRecord[] = [
    { forecastTime: new Date('2020-08-25T04:00:00.000Z'), gust: 1, run: 1598313600000, u: 5, v: 5 },
    { forecastTime: new Date('2020-08-25T05:00:00.000Z'), gust: 1, run: 1598313600000, u: 5, v: 5 },
    { forecastTime: new Date('2020-08-25T06:00:00.000Z'), gust: 1, run: 1598313600000, u: 5, v: 5 },
    { forecastTime: new Date('2020-08-25T07:00:00.000Z'), gust: 1, run: 1598313600000, u: 5, v: 5 },
    { forecastTime: new Date('2020-08-25T08:00:00.000Z'), gust: 1, run: 1598313600000, u: 5, v: 5 },
    { forecastTime: new Date('2020-08-25T09:00:00.000Z'), gust: 1, run: 1598313600000, u: 5, v: 5 },
  ];
  const surfSpotConfiguration: SurfSpotConfigurationRecord = {
    offshoreDirection: 180,
    spectralRefractionMatrix: [],
    breakingWaveHeightIntercept: 1,
    breakingWaveHeightCoefficient: 0,
    breakingWaveHeightAlgorithm: 'POLYNOMIAL',
    optimalSwellDirection: 10,
    ratingsAlgorithm: null,
  };

  return {
    dataSources: {
      sciencePlatformDB: sinon.createStubInstance(SciencePlatformDB, {
        getReportedSurfHeightRecords: Promise.resolve(reportedSurfHeightRecords),
        getSurfRecords: Promise.resolve(surfRecords),
        getSurfSpotConfigurationRecord: Promise.resolve(surfSpotConfiguration),
        getSwellRecords: Promise.resolve(swellRecords),
        getWindRecords: Promise.resolve(windRecords),
      }),
      surfRatingsAPI: sinon.createStubInstance(SurfRatingsAPI, {
        getSurfRatings: Promise.resolve([1, 2, 3, 3, 2, 1]),
      }),
    },
  };
};

const resolversSpec = () => {
  describe('getSurfSpotSurfForecastData', () => {
    context('corrected query', () => {
      let mockContext: { dataSources: ScienceDataServiceDataSources };

      before(() => {
        mockContext = createMockContext();
      });

      it('returns the appropriate data when no start and no end given', async () => {
        const resultNoStart = await getSurfSpotSurfForecastData(
          {
            pointOfInterestId: '1234',
            start: null,
            end: null,
            interval: 3600,
            models: [
              { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020082506, grid: { key: 'CAL_3m' } },
            ],
            corrected: true,
            units: { waveHeight: 'ft' },
            interpolationRange: 2,
          },
          {},
          mockContext,
        );

        // Include all surf heights from the beginning of the run to the end.
        expect(resultNoStart.length).to.equal(4);
        expect(resultNoStart[0].timestamp).to.equal(
          new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
        );
        // Hour 6 is corrected by hour 5, even though hour 5 is not returned.
        expect(resultNoStart[0].surf.breakingWaveHeightMin).to.equal(1.77);
        expect(resultNoStart[0].surf.breakingWaveHeightMax).to.be.closeTo(2.71, 0.01);
      });

      it('returns the appropriate data when start and end given', async () => {
        const resultWithStart = await getSurfSpotSurfForecastData(
          {
            pointOfInterestId: '1234',
            start: new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
            end: new Date('2020-08-25T07:00:00.000Z').getTime() / 1000,
            interval: 3600,
            models: [
              { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020082506, grid: { key: 'CAL_3m' } },
            ],
            corrected: true,
            units: { waveHeight: 'ft' },
            interpolationRange: 2,
          },
          {},
          mockContext,
        );

        // Include only the surf heights from the start to end.
        expect(resultWithStart.length).to.equal(2);
        expect(resultWithStart[0].timestamp).to.equal(
          new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
        );
        // Hour 6 is corrected by hour 5, even though hour 5 is not returned.
        expect(resultWithStart[0].surf.breakingWaveHeightMin).to.equal(1.77);
        expect(resultWithStart[0].surf.breakingWaveHeightMax).to.be.closeTo(2.71, 0.01);
        expect(resultWithStart[1].timestamp).to.equal(
          new Date('2020-08-25T07:00:00.000Z').getTime() / 1000,
        );
        // Hour 6 is corrected by hour 8, even though hour 5 is not returned.
        expect(resultWithStart[1].surf.breakingWaveHeightMin).to.equal(2.855);
        expect(resultWithStart[1].surf.breakingWaveHeightMax).to.be.equal(4.355);
      });
    });
  });

  describe('getSurfSpotSwellsForecastData', () => {
    let mockContext: { dataSources: ScienceDataServiceDataSources };

    beforeEach(() => {
      mockContext = createMockContext();
    });

    it('returns the correct number of swell components', async () => {
      const swellsForecast = await getSurfSpotSwellsForecastData(
        {
          pointOfInterestId: '123',
          start: null,
          end: null,
          interval: 3600,
          models: [
            { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020082506, grid: { key: 'CAL_3m' } },
          ],
          units: { waveHeight: 'ft' },
        },
        {},
        mockContext,
      );

      expect(swellsForecast).to.have.length(1);
      expect(swellsForecast[0].swells.components).to.have.length(5);
    });
  });

  describe('getRatingForecast', () => {
    let mockContext: { dataSources: ScienceDataServiceDataSources };

    beforeEach(() => {
      mockContext = createMockContext();
    });

    describe('NULL algorithm', () => {
      it('returns null when no query param is passed and no ratings algorithm is configured', async () => {
        const ratingForecast = await getRatingForecast(
          {
            end: new Date('2020-08-25T09:00:00.000Z').getTime() / 1000,
            interval: 3600,
            models: [
              { agency: 'NOAA', forecastType: 'WIND', grid: { key: '0p25' }, model: 'GFS' },
              {
                agency: 'Wavetrak',
                forecastType: 'SURF',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
              {
                agency: 'Wavetrak',
                forecastType: 'SWELLS',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
            ],
            pointOfInterestId: '1234',
            start: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            ratingsAlgorithm: null,
          },
          {},
          mockContext,
        );

        expect(ratingForecast).to.be.null();
      });
    });

    describe('ML algorithms', () => {
      it('requests ratings from the surf ratings API data source ML_SPECTRA_REF_MATRIX algorithm', async () => {
        const ratingForecast = await getRatingForecast(
          {
            end: new Date('2020-08-25T09:00:00.000Z').getTime() / 1000,
            interval: 3600,
            models: [
              { agency: 'NOAA', forecastType: 'WIND', grid: { key: '0p25' }, model: 'GFS' },
              {
                agency: 'Wavetrak',
                forecastType: 'SURF',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
              {
                agency: 'Wavetrak',
                forecastType: 'SWELLS',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
            ],
            pointOfInterestId: '1234',
            start: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            ratingsAlgorithm: 'ML_SPECTRA_REF_MATRIX',
          },
          {},
          mockContext,
        );

        expect(ratingForecast).to.have.length(6);
      });
    });

    describe('MATRIX algorithm', () => {
      it('returns the correct number of ratings', async () => {
        const ratingForecast = await getRatingForecast(
          {
            end: new Date('2020-08-25T09:00:00.000Z').getTime() / 1000,
            interval: 3600,
            models: [
              { agency: 'NOAA', forecastType: 'WIND', grid: { key: '0p25' }, model: 'GFS' },
              {
                agency: 'Wavetrak',
                forecastType: 'SURF',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
            ],
            pointOfInterestId: '1234',
            start: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            ratingsAlgorithm: 'MATRIX',
          },
          {},
          mockContext,
        );

        expect(ratingForecast).to.have.length(6);
      });

      it('returns ratings between specified start and end', async () => {
        const ratingForecast = await getRatingForecast(
          {
            end: new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
            interval: 3600,
            models: [
              { agency: 'NOAA', forecastType: 'WIND', grid: { key: '0p25' }, model: 'GFS' },
              {
                agency: 'Wavetrak',
                forecastType: 'SURF',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
            ],
            pointOfInterestId: '1234',
            start: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            ratingsAlgorithm: 'MATRIX',
          },
          {},
          mockContext,
        );

        expect(ratingForecast).to.have.length(3);
      });

      it('returns ratings for blended model data (two models for one forecast type)', async () => {
        const ratingForecast = await getRatingForecast(
          {
            end: new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
            interval: 3600,
            models: [
              { agency: 'NOAA', forecastType: 'WIND', grid: { key: '0p25' }, model: 'GFS' },
              { agency: 'NOAA', forecastType: 'WIND', grid: { key: 'conus' }, model: 'NAM' },
              {
                agency: 'Wavetrak',
                forecastType: 'SURF',
                grid: { key: 'GLO_30M' },
                model: 'Lotus-WW3',
              },
            ],
            pointOfInterestId: '1234',
            start: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            ratingsAlgorithm: 'MATRIX',
          },
          {},
          mockContext,
        );

        expect(ratingForecast).to.have.length(3);
      });
    });
  });
  
  describe('correctQueryStartEnd', () => {
    const start = 1642636800;
    const end = 1642723136;
    const interpolationRange = 24;

    it('corrects the values of start and end when corrected is true', () => {
      const corrected = true;
      const correctedQueryStartEnd = correctQueryStartEnd(start, end, interpolationRange, corrected);

      expect(correctedQueryStartEnd).to.deep.equal({
        start: 1642550400,
        end: 1642809536
      });
    });

    it('keeps the values of start and end when corrected is false', () => {
      const corrected = false;
      const correctedQueryStartEnd = correctQueryStartEnd(start, end, interpolationRange, corrected);

      expect(correctedQueryStartEnd).to.deep.equal({
        start: 1642636800,
        end: 1642723136
      });
    });
  });
};

export default resolversSpec;
