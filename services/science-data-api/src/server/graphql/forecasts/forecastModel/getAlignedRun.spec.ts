import { expect } from 'chai';
import sinon from 'sinon';
import _ from 'lodash';
import getAlignedRun from './getAlignedRun';

import * as modelRun from '../../../../models/modelRun';
import * as findAlignedRun from './findAlignedRun';

const mockQueryModelRuns = runData => query => {
  const filterObject = _.pickBy(
    query,
    (value, key) => value && _.includes(['agency', 'model', 'run'], key),
  );
  return _.filter(runData, filterObject);
};

const getAlignedRunSpec = () => {
  describe('getAlignedRun', () => {
    let queryModelRunsStub;

    beforeEach(() => {
      queryModelRunsStub = sinon.stub(modelRun, 'queryModelRuns');
    });

    afterEach(() => {
      queryModelRunsStub.restore();
    });

    context(
      'there is no aligned run available. All models are more than 12 hours behind leader, GFS.',
      () => {
        beforeEach(() => {
          const runs = [
            { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010100 },
            { agency: 'NOAA', model: 'NAM', run: 2020010100 },
            { agency: 'NOAA', model: 'GFS', run: 2020010200 },
            { agency: 'NOAA', model: 'GFS', run: 2020010206 },
          ];
          queryModelRunsStub.callsFake(mockQueryModelRuns(runs));
        });

        afterEach(() => {
          queryModelRunsStub.restore();
        });

        it('gets the latest run for GFS', async () => {
          const result = await getAlignedRun({
            agency: 'NOAA',
            model: 'GFS',
            types: ['POINT_OF_INTEREST'],
          });
          expect(result.run).to.equal(2020010206);
        });
        it('gets the latest run for NAM', async () => {
          const result = await getAlignedRun({
            agency: 'NOAA',
            model: 'NAM',
            types: ['POINT_OF_INTEREST'],
          });
          expect(result.run).to.equal(2020010100);
        });
      },
    );

    context(
      'with NAM more than 12 hours behind leader. NAM should not block alignment of other models',
      () => {
        beforeEach(() => {
          const runs = [
            { agency: 'NOAA', model: 'NAM', run: 2020010112 },
            { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010200 },
            { agency: 'NOAA', model: 'GFS', run: 2020010200 },
            { agency: 'NOAA', model: 'GFS', run: 2020010206 },
          ];
          queryModelRunsStub.callsFake(mockQueryModelRuns(runs));
          sinon.stub(findAlignedRun, 'default').callsFake(_ => {
            return { run: 2020010200, delay: 0 };
          });
        });

        afterEach(() => {
          queryModelRunsStub.restore();
          findAlignedRun.default.restore();
        });

        it('should align GFS, NAM, and Lotus with same model run.', async () => {
          const result = await getAlignedRun({
            agency: 'NOAA',
            model: 'GFS',
            types: ['POINT_OF_INTEREST'],
          });
          expect(result.run).to.equal(2020010200);
        });
      },
    );

    context('with no runs available at all for the desired model', () => {
      beforeEach(() => {
        const runs = [
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010406 },
          { agency: 'NOAA', model: 'GFS', run: 2020010400 },
          { agency: 'NOAA', model: 'GFS', run: 2020010406 },
        ];
        queryModelRunsStub.callsFake(mockQueryModelRuns(runs));
      });

      afterEach(() => {
        queryModelRunsStub.restore();
      });

      it('returns null for NAM', async () => {
        const result = await getAlignedRun({
          agency: 'Wavetrak',
          model: 'NAM',
          types: ['POINT_OF_INTEREST'],
        });
        expect(result).to.equal(null);
      });
    });

    context('NAM model run ahead of GFS', () => {
      beforeEach(() => {
        const runs = [
          // NAM run: 2020010406 is greater than latest GFS run, ignore for alignment.
          { agency: 'NOAA', model: 'NAM', run: 2020010406 },
          { agency: 'NOAA', model: 'GFS', run: 2020010400 },
          { agency: 'NOAA', model: 'GFS', run: 2020010312 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010312 },
        ];
        queryModelRunsStub.callsFake(mockQueryModelRuns(runs));
      });

      afterEach(() => {
        queryModelRunsStub.restore();
      });

      it('returns the latest run for NAM', async () => {
        const result = await getAlignedRun({
          agency: 'NOAA',
          model: 'NAM',
          types: ['POINT_OF_INTEREST'],
        });
        expect(result.run).to.equal(2020010406);
        // A negative delay may exist, indicating that the model
        // run is greater than the latest GFS run.
        expect(result.delay).to.equal(-6);
      });

      it('returns aligned run for GFS', async () => {
        const result = await getAlignedRun({
          agency: 'NOAA',
          model: 'GFS',
          types: ['POINT_OF_INTEREST'],
        });
        expect(result.run).to.equal(2020010312);
        expect(result.delay).to.equal(0);
      });

      it('returns aligned run for Lotus', async () => {
        const result = await getAlignedRun({
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          types: ['POINT_OF_INTEREST'],
        });
        expect(result.run).to.equal(2020010312);
        expect(result.delay).to.equal(0);
      });
    });

    it('returns the latest run when no GFS available', async () => {
      const runs = [
        // GFS totally missing from model runs.
        { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010312 },
        { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010318 },
        { agency: 'NOAA', model: 'NAM', run: 2020010400 },
        { agency: 'NOAA', model: 'NAM', run: 2020010318 },
        { agency: 'NOAA', model: 'NAM', run: 2020010312 },
      ];
      queryModelRunsStub.callsFake(mockQueryModelRuns(runs));

      const gfsResult = await getAlignedRun({
        agency: 'NOAA',
        model: 'GFS',
        types: ['POINT_OF_INTEREST'],
      });
      expect(gfsResult).to.equal(null);

      const namResult = await getAlignedRun({
        agency: 'NOAA',
        model: 'NAM',
        types: ['POINT_OF_INTEREST'],
      });
      expect(namResult.run).to.equal(2020010400);
      expect(namResult.delay).to.equal(0);

      const lotusResult = await getAlignedRun({
        agency: 'Wavetrak',
        model: 'Lotus-WW3',
        types: ['POINT_OF_INTEREST'],
      });
      expect(lotusResult.run).to.equal(2020010318);
      expect(lotusResult.delay).to.equal(0);
    });

    context(
      'with no valid runs available for the desired model. Valid runs are those within 12 hours from the leading model run.',
      () => {
        beforeEach(() => {
          const runs = [
            { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010312 },
            { agency: 'NOAA', model: 'NAM', run: 2020010406 },
            { agency: 'NOAA', model: 'GFS', run: 2020010400 },
            { agency: 'NOAA', model: 'GFS', run: 2020010406 },
          ];
          queryModelRunsStub.callsFake(mockQueryModelRuns(runs));
        });

        afterEach(() => {
          queryModelRunsStub.restore();
        });

        it('returns null for Lotus-WW3, since Lotus-WW3 is more than 12 hours from the leading GFS run.', async () => {
          const result = await getAlignedRun({
            agency: 'Wavetrak',
            model: 'Lotus-WW3',
            types: ['POINT_OF_INTEREST'],
          });
          expect(result.run).to.equal(2020010312);
          expect(result.delay).to.equal(18);
        });
      },
    );
  });
};

export default getAlignedRunSpec;
