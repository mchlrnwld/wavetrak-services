import { runTimeToUnix } from '../../../../db/runTimeConversions';
import { queryModelRuns } from '../../../../models/modelRun';
import findAlignedRun from './findAlignedRun';
import { ALIGNED_MODELS } from './constants';

const sToHours = (seconds: number): number => seconds / 3600;

const maximumDelayForRuns = 12;

/*
 **
 * Determines the model run to use when a given agency, and model are requested.
 * For all models in ALIGNED_MODELS, it attempts to find the most recent common
 * run time such that all are in sync.
 *
 * Each model in ALIGNED_MODELS has an allowed run delay that specifies
 * how delayed a model can be from the most recent common run time.
 * For most models the allowed run delay is 0.
 * If there is no run time satisfying the conditions,
 * the latest available run for the requested model is returned.
 *
 * @param data - The requested model data
 * @param data.agency - Model agency
 * @param data.model - Model name
 * @param data.types - any combination of POINT_OF_INTEREST or FULL_GRID run types
 * @return Promise of the model run to use, or Null if there is no applicable run
 */
const getAlignedRun = async ({
  agency,
  model,
  types,
}: {
  agency: string;
  model: string;
  types: string[];
}): Promise<{ agency: string; model: string; run: number; delay: number } | null> => {
  const allRuns = (
    await Promise.all(
      ALIGNED_MODELS.map(({ agency: a, model: m }) =>
        queryModelRuns({
          agency: a,
          model: m,
          run: null,
          statuses: ['ONLINE'],
          types,
        }),
      ),
    )
  ).map((modelValues, i) => ({
    agency: ALIGNED_MODELS[i].agency,
    model: ALIGNED_MODELS[i].model,
    runs: modelValues.map(entry => entry.run),
    allowRunDelay: ALIGNED_MODELS[i].allowRunDelay,
  }));

  // Get model runs for the current model/agency combination.
  const ownRuns = allRuns
    .filter(modelValues => modelValues.model == model && modelValues.agency == agency)
    .map(modelValues => modelValues.runs)
    .flat();

  if (!ownRuns.length) {
    return null;
  }

  const latestOwnRun = Math.max(...ownRuns);

  // Get all GFS runs.
  const gfsRuns = allRuns
    .filter(modelValues => modelValues.model == 'GFS')
    .map(modelValues => modelValues.runs)
    .flat();

  // If no GFS runs exist, return latestOwnRun.
  if (!gfsRuns.length) {
    return {
      agency,
      model,
      run: latestOwnRun,
      delay: 0,
    };
  }

  // Calculate the latest GFS run available, only interested in
  // aligning on a run at most 12 hours behind this.
  const latestGFSRunDate = runTimeToUnix(Math.max(...gfsRuns));

  const validOwnRuns = ownRuns.filter(
    run =>
      sToHours(latestGFSRunDate - runTimeToUnix(run)) <= maximumDelayForRuns &&
      latestGFSRunDate - runTimeToUnix(run) >= 0,
  );

  if (!validOwnRuns.length) {
    return {
      agency,
      model,
      run: latestOwnRun,
      delay: sToHours(latestGFSRunDate - runTimeToUnix(latestOwnRun)),
    };
  }

  // Valid models are those who have runs up to 12 hours behind the latestGFSRunDate.
  const validModels = allRuns
    .map(modelValues => ({
      ...modelValues,
      runs: new Set(
        modelValues.runs.filter(
          run =>
            sToHours(latestGFSRunDate - runTimeToUnix(run)) <= maximumDelayForRuns &&
            latestGFSRunDate - runTimeToUnix(run) >= 0,
        ),
      ) as Set<number>,
    }))
    .filter(modelValues => modelValues.runs.size > 0);

  const validRuns = validModels.map(modelValues => [...modelValues.runs]).flat() as number[];

  const alignedRun = findAlignedRun({
    agency,
    model,
    allRuns: validRuns,
    allModels: validModels,
  });

  if (alignedRun) {
    return { agency, model, ...alignedRun };
  }

  return {
    agency,
    model,
    run: latestOwnRun,
    delay: sToHours(latestGFSRunDate - runTimeToUnix(latestOwnRun)),
  };
};

export default getAlignedRun;
