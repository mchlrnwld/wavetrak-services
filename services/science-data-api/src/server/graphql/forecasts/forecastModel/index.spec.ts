import findAlignedRunSpec from './findAlignedRun.spec';
import getAlignedRunSpec from './getAlignedRun.spec';
import getForecastModelSpec from './getForecastModel.spec';

describe('/forecastModel', () => {
  findAlignedRunSpec();
  getAlignedRunSpec();
  getForecastModelSpec();
});
