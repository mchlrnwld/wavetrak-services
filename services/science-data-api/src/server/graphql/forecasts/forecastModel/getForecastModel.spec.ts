import { expect } from 'chai';
import sinon from 'sinon';
import { filter, includes, pickBy } from 'lodash';
import getForecastModel from './getForecastModel';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'core-js';

import * as modelGrid from '../../../../models/modelGrid';
import * as modelRun from '../../../../models/modelRun';
import * as pointOfInterestDefaultForecastModel from '../../../../models/pointOfInterestDefaultForecastModel';
import { ForecastType } from '../../../../models/pointOfInterestDefaultForecastModel/index.d';
import * as pointOfInterestGridPoint from '../../../../models/pointOfInterestGridPoint';

const poiGridPoints = [
  {
    pointOfInterestId: 'aaa-bbb',
    agency: 'Wavetrak',
    model: 'Lotus-WW3',
    grid: 'GLOB_30m',
    lat: 54,
    lon: 356,
  },
  {
    pointOfInterestId: 'aaa-bbb',
    agency: 'NOAA',
    model: 'WW3',
    grid: 'glo_30m',
    lat: 54,
    lon: 356,
  },
  { pointOfInterestId: 'aaa-bbb', agency: 'NOAA', model: 'GFS', grid: '0p25', lat: 54, lon: 356 },
  { pointOfInterestId: 'aaa-bbb', agency: 'NOAA', model: 'NAM', grid: 'conus', lat: 54, lon: 356 },
];

const mockQueryModelRuns = runData => query => {
  const filterObject = pickBy(
    query,
    (value, key) => value && includes(['agency', 'model', 'run'], key),
  );
  return filter(runData, filterObject);
};

const mockQueryPointsOfInterestGridPoints = query => {
  const filterObject = pickBy(
    query,
    (value, key) => value && includes(['agency', 'model', 'grid', 'pointOfInterestId'], key),
  );
  return filter(poiGridPoints, filterObject);
};

const mockQueryModelGrids = modelGrids => query => {
  const filterObject = pickBy(
    query,
    (value, key) =>
      value && includes(['agency', 'model', 'grid', 'description', 'resolution'], key),
  );
  return filter(modelGrids, filterObject);
};

const getForecastModelSpec = () => {
  describe('getForecastModel', () => {
    let queryModelGridsStub;
    let queryModelRunsStub;
    let queryPointsOfInterestGridPointsStub;
    let queryPointsOfInterestDefaultForecastModelsStub;

    beforeEach(() => {
      queryModelGridsStub = sinon.stub(modelGrid, 'queryModelGrids');
      queryModelRunsStub = sinon.stub(modelRun, 'queryModelRuns');
      queryPointsOfInterestGridPointsStub = sinon.stub(
        pointOfInterestGridPoint,
        'queryPointsOfInterestGridPoints',
      );
      queryPointsOfInterestDefaultForecastModelsStub = sinon.stub(
        pointOfInterestDefaultForecastModel,
        'queryPointsOfInterestDefaultForecastModels',
      );

      queryPointsOfInterestGridPointsStub.callsFake(mockQueryPointsOfInterestGridPoints);
      queryPointsOfInterestDefaultForecastModelsStub.resolves([]);
    });

    afterEach(() => {
      queryModelGridsStub.restore();
      queryModelRunsStub.restore();
      queryPointsOfInterestGridPointsStub.restore();
      queryPointsOfInterestDefaultForecastModelsStub.restore();
    });

    it('returns properties for an online model run and point of interest grid point', async () => {
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010100 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010106 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010112 },
        ]),
      );
      const result = await getForecastModel(ForecastType.SWELLS)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          run: 2020010106,
          grid: 'GLOB_30m',
          start: 1600000000,
          end: 1600003600,
          interval: 3600,
        },
      );
      expect(result).to.deep.include({
        pointOfInterestId: 'aaa-bbb',
        start: 1600000000,
        end: 1600003600,
        interval: 3600,
        models: [
          {
            agency: 'Wavetrak',
            forecastType: 'SWELLS',
            model: 'Lotus-WW3',
            run: 2020010106,
            grid: {
              key: 'GLOB_30m',
              lat: 54,
              lon: 356,
              resolution: null,
            },
          },
        ],
      });
    });

    it('returns null if online run not found', async () => {
      queryModelRunsStub.callsFake(mockQueryModelRuns([]));
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          run: 2040010106,
          grid: 'GLOB_30m',
          start: 1600000000,
          end: 1600003600,
          interval: 3600,
        },
      );
      expect(result).to.equal(null);
    });

    it('returns null if grid point not found', async () => {
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([{ agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010106 }]),
      );

      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-ccc' },
        {
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          run: 2020010106,
          grid: 'GLOB_30m',
          start: 1600000000,
          end: 1600003600,
          interval: 3600,
        },
      );
      expect(result).to.equal(null);
    });

    it('gets the latest aligned run when run is not included in parameter', async () => {
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010106 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010112 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010118 },

          { agency: 'NOAA', model: 'NAM', run: 2020010106 },
          { agency: 'NOAA', model: 'NAM', run: 2020010112 },

          { agency: 'NOAA', model: 'GFS', run: 2020010106 },
          { agency: 'NOAA', model: 'GFS', run: 2020010112 },
          { agency: 'NOAA', model: 'GFS', run: 2020010118 },
        ]),
      );
      const result = await getForecastModel(ForecastType.SWELLS)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          grid: 'GLOB_30m',
        },
      );
      expect(result.models).to.have.length(1);
      expect(result.models[0].run).to.equal(2020010112);
    });

    it('supports a blended weather model for specified run', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'NOAA', model: 'NAM', run: 2020010112 },
          { agency: 'NOAA', model: 'GFS', run: 2020010112 },
        ]),
      );
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Blended',
          grid: 'Blended',
          run: 2020010112,
        },
      );
      expect(result.models).to.have.length(2);
      expect(result.models[0].agency).to.equal('NOAA');
      expect(result.models[0].model).to.equal('GFS');
      expect(result.models[0].grid.key).to.equal('0p25');
      expect(result.models[1].agency).to.equal('NOAA');
      expect(result.models[1].model).to.equal('NAM');
      expect(result.models[1].grid.key).to.equal('conus');
      result.models.forEach(({ run }) => {
        expect(run).to.equal(2020010112);
      });
    });

    it('gets the latest aligned run for blended model when run is not included in parameter', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010106 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010112 },
          { agency: 'Wavetrak', model: 'Lotus-WW3', run: 2020010118 },

          { agency: 'NOAA', model: 'NAM', run: 2020010106 },
          { agency: 'NOAA', model: 'NAM', run: 2020010112 },

          { agency: 'NOAA', model: 'GFS', run: 2020010106 },
          { agency: 'NOAA', model: 'GFS', run: 2020010112 },
          { agency: 'NOAA', model: 'GFS', run: 2020010118 },
        ]),
      );
      const result = await getForecastModel(ForecastType.SWELLS)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Blended',
          grid: 'Blended',
        },
      );
      expect(result.models).to.have.length(2);
      expect(result.models[0].agency).to.equal('NOAA');
      expect(result.models[0].model).to.equal('GFS');
      expect(result.models[0].grid.key).to.equal('0p25');
      expect(result.models[1].agency).to.equal('NOAA');
      expect(result.models[1].model).to.equal('NAM');
      expect(result.models[1].grid.key).to.equal('conus');
      result.models.forEach(({ run }) => {
        expect(run).to.equal(2020010112);
      });
    });

    it('treats specified run as offline if run is missing for any blended models', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'NOAA', model: 'NAM', run: 2020010112 },
          { agency: 'NOAA', model: 'GFS', run: 2020010112 },
          { agency: 'NOAA', model: 'GFS', run: 2020010118 },
        ]),
      );
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Blended',
          grid: 'Blended',
          run: 2020010118,
        },
      );
      expect(result).to.equal(null);
    });

    it('ignores missing run after 12 hours for blended model as long as GFS is available', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          // More than 12 hours behind the leading run from GFS: 2020010218.
          { agency: 'NOAA', model: 'NAM', run: 2020010118 },
          { agency: 'NOAA', model: 'GFS', run: 2020010212 },
          { agency: 'NOAA', model: 'GFS', run: 2020010218 },
        ]),
      );
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Blended',
          grid: 'Blended',
        },
      );
      // NAM is not returned.
      expect(result.models).to.have.length(1);
      expect(result.models[0].agency).to.equal('NOAA');
      expect(result.models[0].model).to.equal('GFS');
      expect(result.models[0].run).to.equal(2020010218);
    });

    it('returns GFS for blended model when there is no aligned run', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'NOAA', model: 'NAM', run: 2020010200 },
          { agency: 'NOAA', model: 'GFS', run: 2020010212 },
        ]),
      );
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Blended',
          grid: 'Blended',
        },
      );
      expect(result.models).to.have.length(1);
      expect(result.models[0].agency).to.equal('NOAA');
      expect(result.models[0].model).to.equal('GFS');
      expect(result.models[0].run).to.equal(2020010212);
    });

    it('returns null for blended model when GFS is not available', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          // GFS is missing.
          { agency: 'NOAA', model: 'NAM', run: 2020010212 },
          { agency: 'NOAA', model: 'NAM', run: 2020010218 },
        ]),
      );
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        {
          agency: 'Wavetrak',
          model: 'Blended',
          grid: 'Blended',
        },
      );
      expect(result).to.equal(null);
    });

    it('looks up default model for forecast type if not specified', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([{ agency: 'NOAA', model: 'GFS', run: 2020010112 }]),
      );
      queryPointsOfInterestDefaultForecastModelsStub.resolves([
        {
          agency: 'NOAA',
          model: 'GFS',
          grid: '0p25',
        },
      ]);
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        { run: 2020010112 },
      );
      expect(
        result.models.map(({ agency, model, run, grid: { key: grid } }) => ({
          agency,
          model,
          run,
          grid,
        })),
      ).to.deep.equal([{ agency: 'NOAA', model: 'GFS', run: 2020010112, grid: '0p25' }]);
      expect(queryPointsOfInterestDefaultForecastModelsStub).to.have.been.calledOnce();
    });

    it('uses hard coded default model for forecast type if not specified and default model not found', async () => {
      queryModelGridsStub.callsFake(
        mockQueryModelGrids([
          {
            agency: 'NOAA',
            model: 'GFS',
            grid: '0p25',
            description: 'Global grid',
            resolution: 0.25,
          },
        ]),
      );
      queryModelRunsStub.callsFake(
        mockQueryModelRuns([
          { agency: 'NOAA', model: 'NAM', run: 2020010112 },
          { agency: 'NOAA', model: 'GFS', run: 2020010112 },
        ]),
      );
      const result = await getForecastModel(ForecastType.WEATHER)(
        { pointOfInterestId: 'aaa-bbb' },
        { run: 2020010112 },
      );
      expect(
        result.models.map(({ agency, model, run, grid: { key: grid } }) => ({
          agency,
          model,
          run,
          grid,
        })),
      ).to.deep.equal([
        { agency: 'NOAA', model: 'GFS', run: 2020010112, grid: '0p25' },
        { agency: 'NOAA', model: 'NAM', run: 2020010112, grid: 'conus' },
      ]);
      expect(queryPointsOfInterestDefaultForecastModelsStub).to.have.been.calledOnce();
    });
  });
};

export default getForecastModelSpec;
