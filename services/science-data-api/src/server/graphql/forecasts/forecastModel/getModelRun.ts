import { queryModelRuns } from '../../../../models/modelRun';

/**
 * Gets the latest model run available for the agency, model, and run requested.
 * Returns null if the requested run is not available.
 *
 * @param data - The requested model data.
 * @param data.agency - Model agency.
 * @param data.model - Model name.
 * @param data.run - Model run.
 * @return Promise of the model run to use, or Null if there is no applicable run
 */
const getModelRun = async ({
  agency,
  model,
  run,
}: {
  agency: string;
  model: string;
  run?: number;
}): Promise<{ run: number; delay: 0 } | null> => {
  const modelRuns = await queryModelRuns({
    agency,
    model,
    run,
    statuses: ['ONLINE'],
    types: ['POINT_OF_INTEREST'],
  });
  if (!modelRuns.length) {
    return null;
  }
  return { run: Math.max(...modelRuns.map(modelValue => modelValue.run)), delay: 0 };
};

export default getModelRun;
