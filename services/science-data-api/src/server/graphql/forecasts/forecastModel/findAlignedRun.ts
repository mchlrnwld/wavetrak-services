import { runTimeToUnix } from '../../../../db/runTimeConversions';

const sToHours = (seconds: number): number => seconds / 3600;

/**
 * Attempts to find a common model run between all models.
 * Models that allow delays may return a model run that is up to the allowed delay.
 * Otherwise null is returned.
 *
 * @param data - The requested model data
 * @param data.agency - Model agency
 * @param data.model - Model name
 * @param data.allRuns - All the runs to compare.
 * @param data.allModels - All of the models to compare.
 * @return Promise of the model run to use, or Null if there is no applicable run
 */
const findAlignedRun = ({
  agency,
  model,
  allRuns,
  allModels,
}: {
  agency: string;
  model: string;
  allRuns: number[];
  allModels: { agency: string; model: string; allowRunDelay: number; runs: Set<number> }[];
}): { run: number; delay: number } | null => {
  const sortedRuns = new Set(allRuns.sort((previousRun, currentRun) => currentRun - previousRun));
  for (const run of sortedRuns) {
    const commonRuns = [];
    for (const modelValue of allModels) {
      const runsInRange = [...modelValue.runs]
        .filter(modelRun => modelRun <= run)
        .filter(
          modelRun =>
            sToHours(runTimeToUnix(run) - runTimeToUnix(modelRun)) <= modelValue.allowRunDelay,
        );
      if (runsInRange.length) {
        commonRuns.push(Math.max(...runsInRange));
      }
    }
    // If all models contain a common run (including delays),
    // return the run for the current model/agency.
    if (commonRuns.length == allModels.length) {
      const maxCommonRun = Math.max(...commonRuns);
      const index = allModels.findIndex(
        modelValue => modelValue.agency === agency && modelValue.model == model,
      );
      return {
        run: commonRuns[index],
        delay: sToHours(runTimeToUnix(maxCommonRun) - runTimeToUnix(commonRuns[index])),
      };
    }
  }
  return null;
};

export default findAlignedRun;
