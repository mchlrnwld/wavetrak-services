import { expect } from 'chai';
import findAlignedRun from './findAlignedRun';

const findAlignedRunSpec = () => {
  describe('findAlignedRun', () => {
    context('finds a common aligned run', () => {
      const models = [
        { agency: 'Wavetrak', model: 'Lotus-WW3', allowRunDelay: 0, runs: new Set([2020010118]) },
        { agency: 'NOAA', model: 'NAM', allowRunDelay: 0, runs: new Set([2020010118]) },
        { agency: 'NOAA', model: 'GFS', allowRunDelay: 0, runs: new Set([2020010118]) },
        { agency: 'Wavetrak', model: 'WRF', allowRunDelay: 6, runs: new Set([2020010112]) },
      ];
      const runs = [2020010118];
      it('returns aligned run for GFS', () => {
        const result = findAlignedRun({
          agency: 'NOAA',
          model: 'GFS',
          allRuns: runs,
          allModels: models,
        });
        expect(result.run).to.equal(2020010118);
        expect(result.delay).to.equal(0);
      });
      it('returns aligned run for NAM', () => {
        const result = findAlignedRun({
          agency: 'NOAA',
          model: 'GFS',
          allRuns: runs,
          allModels: models,
        });
        expect(result.run).to.equal(2020010118);
        expect(result.delay).to.equal(0);
      });
      it('returns delayed run for WRF, which is within the allowRunDelay: 6.', () => {
        const result = findAlignedRun({
          agency: 'Wavetrak',
          model: 'WRF',
          allRuns: runs,
          allModels: models,
        });
        expect(result.run).to.equal(2020010112);
        expect(result.delay).to.equal(6);
      });
    });

    it('cannot find a common run for all models. returns null', () => {
      const models = [
        { agency: 'Wavetrak', model: 'Lotus-WW3', allowRunDelay: 0, runs: new Set([2020010200]) },
        { agency: 'NOAA', model: 'NAM', allowRunDelay: 0, runs: new Set([2020010118]) },
        { agency: 'NOAA', model: 'GFS', allowRunDelay: 0, runs: new Set([2020010118]) },
        { agency: 'Wavetrak', model: 'WRF', allowRunDelay: 6, runs: new Set([2020010112]) },
      ];
      const runs = [2020010118];
      const result = findAlignedRun({
        agency: 'NOAA',
        model: 'GFS',
        allRuns: runs,
        allModels: models,
      });
      expect(result).to.equal(null);
    });

    context('with WRF the leader, but use delayed WRF run', () => {
      const allModels = [
        {
          agency: 'Wavetrak',
          model: 'WRF',
          allowRunDelay: 6,
          runs: new Set([2020010212, 2020010200]),
        },
        { agency: 'Wavetrak', model: 'Lotus-WW3', allowRunDelay: 0, runs: new Set([2020010206]) },
        { agency: 'NOAA', model: 'NAM', allowRunDelay: 0, runs: new Set([2020010206]) },
        { agency: 'NOAA', model: 'GFS', allowRunDelay: 0, runs: new Set([2020010200, 2020010206]) },
      ];
      const allRuns = [2020010212, 2020010206, 2020010200];

      it('returns aligned run for GFS', () => {
        const result = findAlignedRun({
          agency: 'NOAA',
          model: 'GFS',
          allRuns,
          allModels,
        });
        expect(result.run).to.equal(2020010206);
        expect(result.delay).to.equal(0);
      });

      it('returns aligned run for Lotus-WW3', async () => {
        const result = findAlignedRun({
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          allRuns,
          allModels,
        });
        expect(result.run).to.equal(2020010206);
        expect(result.delay).to.equal(0);
      });

      it('should return WRF run with delay of 6 hours', async () => {
        const result = findAlignedRun({
          agency: 'Wavetrak',
          model: 'WRF',
          allRuns,
          allModels,
        });
        expect(result.run).to.equal(2020010200);
        expect(result.delay).to.equal(6);
      });
    });

    context('with WRF ahead, but no common runs', () => {
      const allModels = [
        { agency: 'Wavetrak', model: 'WRF', allowRunDelay: 6, runs: new Set([2020010212]) },
        { agency: 'NOAA', model: 'NAM', allowRunDelay: 0, runs: new Set([2020010200]) },
        { agency: 'Wavetrak', model: 'Lotus-WW3', allowRunDelay: 0, runs: new Set([2020010200]) },
        { agency: 'NOAA', model: 'GFS', allowRunDelay: 0, runs: new Set([2020010200, 2020010206]) },
      ];
      const allRuns = [2020010212, 2020010206, 2020010200];

      it('should return null for WRF', () => {
        const result = findAlignedRun({
          agency: 'Wavetrak',
          model: 'WRF',
          allRuns,
          allModels,
        });
        expect(result).to.equal(null);
      });

      it('should return null for GFS', () => {
        const result = findAlignedRun({
          agency: 'NOAA',
          model: 'GFS',
          allRuns,
          allModels,
        });
        expect(result).to.equal(null);
      });

      it('should return null for NAM', () => {
        const result = findAlignedRun({
          agency: 'NOAA',
          model: 'NAM',
          allRuns,
          allModels,
        });
        expect(result).to.equal(null);
      });
    });

    context('NAM is missing entirely from models, find common run for models given', () => {
      const allModels = [
        { agency: 'Wavetrak', model: 'WRF', allowRunDelay: 6, runs: new Set([2020010200]) },
        { agency: 'Wavetrak', model: 'Lotus-WW3', allowRunDelay: 0, runs: new Set([2020010200]) },
        { agency: 'NOAA', model: 'GFS', allowRunDelay: 0, runs: new Set([2020010206, 2020010200]) },
      ];
      const allRuns = [2020010200, 2020010118, 2020010112];

      it('should still align GFS with other models', () => {
        const result = findAlignedRun({
          agency: 'NOAA',
          model: 'GFS',
          allRuns,
          allModels,
        });
        expect(result.run).to.equal(2020010200);
      });

      it('should still align Lotus with other models', () => {
        const result = findAlignedRun({
          agency: 'Wavetrak',
          model: 'Lotus-WW3',
          allRuns,
          allModels,
        });
        expect(result.run).to.equal(2020010200);
      });

      it('should still align WRF with other models', () => {
        const result = findAlignedRun({
          agency: 'Wavetrak',
          model: 'WRF',
          allRuns,
          allModels,
        });
        expect(result.run).to.equal(2020010200);
      });
    });
  });
};

export default findAlignedRunSpec;
