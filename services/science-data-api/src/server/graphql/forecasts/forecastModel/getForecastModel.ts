import { queryPointsOfInterestGridPoints } from '../../../../models/pointOfInterestGridPoint';
import { queryPointsOfInterestDefaultForecastModels } from '../../../../models/pointOfInterestDefaultForecastModel';
import { ForecastType } from '../../../../models/pointOfInterestDefaultForecastModel/index.d';
import { queryModelGrids } from '../../../../models/modelGrid';
import getAlignedRun from './getAlignedRun';
import getModelRun from './getModelRun';
import { ALIGNED_MODELS, BLENDED_MODELS, DEFAULT_FORECAST_MODELS } from './constants';
import { zip } from 'lodash';

/**
 * Queries default forecast model for a point of interest and forecast type. Returns hard coded
 * default if a default forecast model is not defined for the point of interest in the database.
 *
 * @param pointOfInterestId - Point of interest ID to query default forecast model for.
 * @param forecastType - Forecast type enum value to query default forecast model for.
 * @return - Promise that resolves a model grid object.
 */
const getDefaultForecastModel = async (
  pointOfInterestId: string,
  forecastType: ForecastType,
): Promise<{ agency: string; model: string; grid: string }> => {
  const defaultForecastModel = (
    await queryPointsOfInterestDefaultForecastModels({
      pointOfInterestId,
      forecastType,
    })
  )[0];
  return defaultForecastModel || DEFAULT_FORECAST_MODELS[forecastType];
};

/**
 * Find blended models with highest resolution available for a given point of interest.
 * Models must have the specified run available to be included.
 *
 * @param pointOfInterestId Point of interest ID to identify blended models.
 * @param run Specific model run that models must have available.
 */
const getWavetrakBlendedModelsWithRunAndGrid = async (
  pointOfInterestId: string,
  run: number,
): Promise<{
  agency: string;
  model: string;
  run: number;
  grid: { key: string; lat: number; lon: number; resolution: any };
}[]> => {
  const [modelRuns, modelGrids, modelPOIGridPoints] = await Promise.all<any[]>(
    [
      BLENDED_MODELS.map(modelValues =>
        run
          ? getModelRun({
              agency: modelValues.agency,
              model: modelValues.model,
              run,
            })
          : getAlignedRun({
              agency: modelValues.agency,
              model: modelValues.model,
              types: ['POINT_OF_INTEREST'],
            }),
      ),
      BLENDED_MODELS.map(modelValues =>
        queryModelGrids({
          agency: modelValues.agency,
          model: modelValues.model,
        }),
      ),
      BLENDED_MODELS.map(modelValues =>
        queryPointsOfInterestGridPoints({
          pointOfInterestId,
          agency: modelValues.agency,
          model: modelValues.model,
        }),
      ),
    ].map(Promise.all, Promise),
  );

  // When an explicit `run` is requested, make sure that all models have the
  // the requested `run` available. The `getModelRun` function returns null
  // if the requested `run` is not available.
  if (run && modelRuns.some(val => val === null)) {
    return null;
  }

  const modelGridResolutions = new Map();
  for (const modelGrid of modelGrids.flat()) {
    modelGridResolutions.set(
      `${modelGrid.agency},${modelGrid.model},${modelGrid.grid}`,
      modelGrid.resolution,
    );
  }

  const allModels = zip(BLENDED_MODELS, modelRuns, modelPOIGridPoints)
    .filter(
      ([_, modelRun, poiGridPoints]) =>
        // Remove models that are more than 12 hours from the latest GFS run,
        // and models with runs greater than the latest GFS run.
        modelRun && modelRun.delay <= 12 && modelRun.delay >= 0 && poiGridPoints.length,
    )
    .map(([modelValues, modelRun, poiGridPoints]) => {
      const highestResolutionGridPoint = poiGridPoints
        .map(gp => ({
          ...gp,
          resolution: modelGridResolutions.get(`${gp.agency},${gp.model},${gp.grid}`),
        }))
        .sort((a, b) => a.resolution - b.resolution)[0];

      return {
        agency: modelValues.agency,
        forecastType: modelValues.forecastType,
        model: modelValues.model,
        run: modelRun.run,
        actualDelay: modelRun.delay,
        allowedDelay: modelValues.allowRunDelay,
        grid: {
          key: highestResolutionGridPoint.grid,
          lat: highestResolutionGridPoint.lat,
          lon: highestResolutionGridPoint.lon,
          resolution: highestResolutionGridPoint.resolution,
        },
      };
    });

  const gfs = allModels.find(modelData => modelData.model == 'GFS');

  // GFS data must always be available, otherwise null.
  if (!gfs) {
    return null;
  }

  // Return only GFS if there are any models
  // with a delay greater than the expected delay.
  if (allModels.some(model => model.actualDelay > model.allowedDelay)) {
    const { actualDelay, allowedDelay, ...gfsData } = gfs;
    return [gfsData];
  }

  if (!allModels.length) {
    return null;
  }

  return allModels.map(({ actualDelay, allowedDelay, ...modelData }) => modelData);
};

export interface ForecastQueryParameters {
  agency?: string;
  model?: string;
  run?: number;
  grid?: string;
  start?: number;
  end?: number;
  interval?: number;
  corrected?: boolean;
  correctedSurf?: boolean;
  units?: object;
  ratingsAlgorithm?: string; // TODO: remove once this is configurable in POI CMS
}

// TODO: Capture unused arguments with ...args to be passed down to descendants.
// This will allow for each model type to return only the data that is needed.
const getForecastModel = (...forecastTypes: ForecastType[]) =>
  async function getForecastModel({ pointOfInterestId }, params: ForecastQueryParameters) {
    const {
      run,
      start,
      end,
      interval,
      corrected,
      correctedSurf,
      ratingsAlgorithm, // TODO: remove once this is configurable in POI CMS
      ...units
    } = params;

    const modelsWithRunAndGrid = (
      await Promise.all(
        forecastTypes.map(async forecastType => {
          let { agency, model, grid } = params;

          if (!agency || !model || !grid) {
            const defaultForecastModel = await getDefaultForecastModel(
              pointOfInterestId,
              forecastType,
            );
            agency = defaultForecastModel.agency;
            model = defaultForecastModel.model;
            grid = defaultForecastModel.grid;
          }

          if (agency == 'Wavetrak' && model == 'Blended')
            return await getWavetrakBlendedModelsWithRunAndGrid(pointOfInterestId, run);

          const shouldAlign = ALIGNED_MODELS.some(
            ({ agency: a, model: m }) => a === agency && m === model,
          );
          const [modelRun, gridPoints] = await Promise.all([
            !shouldAlign || run
              ? getModelRun({ agency, model, run })
              : getAlignedRun({ agency, model, types: ['POINT_OF_INTEREST'] }),
            queryPointsOfInterestGridPoints({ agency, model, grid, pointOfInterestId }),
          ]);

          if (!modelRun || !gridPoints.length) {
            return null;
          }

          const gridPoint = gridPoints[0];

          return {
            agency: agency,
            forecastType,
            model: model,
            run: modelRun.run,
            grid: {
              key: gridPoint.grid,
              lat: gridPoint.lat,
              lon: gridPoint.lon,
              resolution: null,
            },
          };
        }),
      )
    )
      .flat()
      .filter(m => m !== null);

    if (modelsWithRunAndGrid.length === 0) return null;

    return {
      pointOfInterestId,
      start,
      end,
      interval,
      models: modelsWithRunAndGrid,
      corrected,
      correctedSurf,
      units,
      ratingsAlgorithm, // TODO: remove once this is configurable in POI CMS
    };
  };

export default getForecastModel;
