import { ForecastType } from '../../../../models/pointOfInterestDefaultForecastModel/index.d';

export const ALIGNED_MODELS = [
  { agency: 'Wavetrak', model: 'Lotus-WW3', allowRunDelay: 0 },
  { agency: 'NOAA', model: 'GFS', allowRunDelay: 0 },
  { agency: 'NOAA', model: 'NAM', allowRunDelay: 0 },
];

export const BLENDED_MODELS = [
  { agency: 'NOAA', forecastType: ForecastType.WIND, model: 'GFS', allowRunDelay: 0 },
  { agency: 'NOAA', forecastType: ForecastType.WIND, model: 'NAM', allowRunDelay: 0 },
];

export const DEFAULT_FORECAST_MODELS = {
  [ForecastType.ENSEMBLE]: { agency: 'NOAA', model: 'GEFS-Wave', grid: 'global.0p25' },
  [ForecastType.SST]: { agency: 'NASA', model: 'MUR-SST', grid: '0p01' },
  [ForecastType.SURF]: { agency: 'Wavetrak', model: 'Lotus-WW3', grid: 'GLOB_15m' },
  [ForecastType.SWELLS]: { agency: 'Wavetrak', model: 'Lotus-WW3', grid: 'GLOB_15m' },
  [ForecastType.WEATHER]: { agency: 'Wavetrak', model: 'Blended', grid: 'Blended' },
  [ForecastType.WIND]: { agency: 'Wavetrak', model: 'Blended', grid: 'Blended' },
};
