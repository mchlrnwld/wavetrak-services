import { expect } from 'chai';
import sinon from 'sinon';
import SurfRatingsAPI from '../dataSources/surfRatingsAPI';
import getSurfRatings from './getSurfRatings';

const getSurfRatingsSpec = () => {
  describe('getSurfRatings', () => {
    const surfForecast = [
        {
          timestamp: 1642024800,
          run: 2022011218,
          surf: {
            breakingWaveHeightMin: 8.53018,
            breakingWaveHeightMax: 10.1706,
            breakingWaveHeightAlgorithm: 'SPECTRAL_REFRACTION'
          }
        },
        {
          timestamp: 1642035600,
          run: 2022011300,
          surf: {
            breakingWaveHeightMin: 8.33333,
            breakingWaveHeightMax: 9.97375,
            breakingWaveHeightAlgorithm: 'SPECTRAL_REFRACTION'
          }
        },
    ];
    
    const windForecast = [
        {
          timestamp: 1642024800,
          run: 2022011218,
          wind: {
            direction: 139.65849,
            speed: 3.25289,
            gust: 4.85961,
            directionType: 'Onshore'
          }
        },
        {
          timestamp: 1642035600,
          run: 2022011300,
          wind: {
            direction: 269.2031,
            speed: 3.36188,
            gust: 3.88769,
            directionType: 'Cross-shore'
          }
        },
    ];
    
    const swellsForecast = [
        {
          timestamp: 1642024800,
          run: 2022011218,
          swells: {
            combined: { height: 9.61352, direction: 284.48975, period: 14 },
            components: [
              {
                height: 9.54797,
                direction: 282.59796,
                period: 14,
                spread: 12.15882,
                impact: 0,
                event: 0
              },
              {
                height: 0.74449,
                direction: 197.07495,
                period: 12,
                spread: 25.81739,
                impact: 0.3053,
                event: 1
              },
              {
                height: 0.62365,
                direction: 211.63843,
                period: 15,
                spread: 11.57997,
                impact: 0.2655,
                event: 2
              },
              {
                height: 0.55925,
                direction: 169.69385,
                period: 15,
                spread: 10.01875,
                impact: 0.2893,
                event: 3
              }
            ],
            spectra1d: {
              has: false,
              period: [
                 2,  3,  4,  5,  6,  7,  8,  9,
                10, 11, 12, 13, 14, 15, 16, 17,
                18, 19, 20, 21, 22, 23, 24, 25,
                26, 27, 28
              ],
              energy: null,
              direction: null,
              directionalSpread: null
            }
          }
        },
        {
          timestamp: 1642035600,
          run: 2022011300,
          swells: {
            combined: { height: 9.39383, direction: 284.12274, period: 14 },
            components: [
              {
                height: 9.32546,
                direction: 282.56128,
                period: 14,
                spread: 12.0143,
                impact: 0,
                event: 0
              },
              {
                height: 0.65922,
                direction: 211.64795,
                period: 15,
                spread: 10.41089,
                impact: 0.2618,
                event: 2
              },
              {
                height: 0.63714,
                direction: 167.15216,
                period: 12,
                spread: 7.61851,
                impact: 0.273,
                event: 4
              },
              {
                height: 0.54905,
                direction: 218.04883,
                period: 12,
                spread: 9.59605,
                impact: 0.1949,
                event: 5
              },
              {
                height: 0.3707,
                direction: 171.99274,
                period: 15,
                spread: 9.70249,
                impact: 0.204,
                event: 3
              }
            ],
            spectra1d: {
              has: false,
              period: [
                 2,  3,  4,  5,  6,  7,  8,  9,
                10, 11, 12, 13, 14, 15, 16, 17,
                18, 19, 20, 21, 22, 23, 24, 25,
                26, 27, 28
              ],
              energy: null,
              direction: null,
              directionalSpread: null
            }
          }
        },
    ];      

    const configuration = {
        pointOfInterestId: 'afe83c75-9e7b-4156-a725-129602319740',
        offshoreDirection: 346,
        optimalSwellDirection: 166,
        breakingWaveHeightCoefficient: 1,
        breakingWaveHeightIntercept: 0,
        spectralRefractionMatrix: [
          [
            0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0
          ],
        ],
        breakingWaveHeightAlgorithm: 'SPECTRAL_REFRACTION',
        ratingsMatrix: null,
        ratingsAlgorithm: 'ML_SPECTRA_REF_MATRIX'
    };
    
    const modelRatingValues = [4.220000000000001, 4.75];

    const ratingsInput = {
      surfForecast: surfForecast,
      windForecast: windForecast,
      swellsForecast: swellsForecast,
    };

    const surfRatingsAPI = sinon.createStubInstance(SurfRatingsAPI, {
      getSurfRatings: Promise.resolve(modelRatingValues),
    });

    const context = {
      dataSources: {
        surfRatingsAPI: surfRatingsAPI,
      }
    };

    it('returns forecasted ratings', async () => {
      const finalAlgorithm = 'ML_SPECTRA_REF_MATRIX';
      const ratings = await getSurfRatings(ratingsInput, finalAlgorithm, context, configuration);
      expect(ratings).to.deep.equal([
        { timestamp: 1642024800, rating: 4.220000000000001 },
        { timestamp: 1642035600, rating: 4.75 },
      ]);
    });

    it('returns ratings when the final algorithm is MATRIX', async () => {
      const finalAlgorithm = 'MATRIX';
      const ratings = await getSurfRatings(ratingsInput, finalAlgorithm, context, configuration);
      expect(ratings).to.deep.equal([
        { timestamp: 1642024800, rating: 4 },
        { timestamp: 1642035600, rating: 4 },
      ]);
    });
  });
};

export default getSurfRatingsSpec;
