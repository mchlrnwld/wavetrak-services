import fillRequestedData from '../../forecasts/fillRequestedData';
import getSurfRatings from './getSurfRatings';
import blendData from './blendData';
import { WeatherForecast } from '../../../models/weather/index.d';
import { WindForecast } from '../../../models/wind/types';
import { formatRunTime, runTimeToUnix } from '../../../db/runTimeConversions';
import { assimilate } from './assimilation';
import { ScienceDataServiceDataSources } from '../dataSources';
import transformWind from '../../../models/wind/transformWind';
import modelsStore from '../../../stores/models';
import transformWeather from '../../../models/weather/transformWeather';
import isNumber from 'lodash/isNumber';
import { convertUnits } from '../../../common/units';
import transformEnsembleWind from '../../../models/swellEnsembleMembers/transformEnsembleWind';
import transformEnsembleSwells from '../../../models/swellEnsembleMembers/transformEnsembleSwells';
import trackSwellEvents from '../../../models/swells/trackSwellEvents';
import transformSwells from '../../../models/swells/transformSwells';
import convertSwellsUnits from '../../../models/swells/convertSwellsUnits';
import transformSurf from '../../../models/surf/transformSurf';
import { SurfForecast } from '../../../models/surf/types';
import transformReportedSurfHeights from '../../../models/reportedSurfHeights/transformReportSurfHeights';
import { valueFromRating, ratingFromValue } from '../../../models/reportedRatings/transformations';
import { ReportedSurfHeightUnits } from '../../../models/reportedSurfHeights/index.d';
import { SeaSurfaceTemperature } from '../../../models/sst/types';
import { ForecastType } from '../../../models/pointOfInterestDefaultForecastModel/index.d';
import { zip } from 'lodash';

const reportedSurfHeightUnits: ReportedSurfHeightUnits = { height: 'ft' };

export const correctQueryStartEnd = (
  start: number,
  end: number,
  interpolationRange: number,
  corrected: boolean,
): { start: number; end: number } => {
  if (corrected) {
    return {
      start: start - interpolationRange * 60 * 60,
      end: end ? end + interpolationRange * 60 * 60 : end,
    };
  }
  return {
    start,
    end,
  };
};

export const getSurfSpotSurfForecastData = async (
  { pointOfInterestId, start, end, interval, models, corrected, units, interpolationRange = 24 },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
) => {
  const {
    agency,
    model,
    run,
    grid: { key: grid },
  } = models[0];

  start = start || runTimeToUnix(run);
  const queryInterval = correctQueryStartEnd(start, end, interpolationRange, corrected);

  const [surfRecords, reportedSurfHeightRecords] = await Promise.all([
    dataSources.sciencePlatformDB.getSurfRecords({
      pointOfInterestId,
      start: queryInterval.start,
      end: queryInterval.end,
      agency,
      model,
      run,
      grid,
    }),
    corrected
      ? dataSources.sciencePlatformDB.getReportedSurfHeightRecords({
          pointOfInterestId,
          start: queryInterval.start,
          end: queryInterval.end,
        })
      : null,
  ]);

  const surfForecasts: SurfForecast[] = surfRecords.map(record => ({
    timestamp: record.forecastTime.getTime() / 1000,
    run: formatRunTime(record.run),
    surf: transformSurf(record, modelsStore[agency][model].storedUnits, units),
  }));

  if (!corrected) return fillRequestedData(surfForecasts, interval, start, end);

  const reportedSurfHeights = reportedSurfHeightRecords.map(record => ({
    timestamp: record.forecastTime.getTime() / 1000,
    ...transformReportedSurfHeights(record, reportedSurfHeightUnits, units),
  }));

  const reportedSurfHeightsMin = reportedSurfHeights.map(reportedSurfHeight => ({
    timestamp: reportedSurfHeight.timestamp,
    value: reportedSurfHeight.min,
  }));

  const reportedSurfHeightsMax = reportedSurfHeights.map(reportedSurfHeight => ({
    timestamp: reportedSurfHeight.timestamp,
    value: reportedSurfHeight.max,
  }));

  const surfForecastsMin = surfForecasts.map(surfForecast => ({
    timestamp: surfForecast.timestamp,
    value: surfForecast.surf.breakingWaveHeightMin,
  }));

  const surfForecastsMax = surfForecasts.map(surfForecast => ({
    timestamp: surfForecast.timestamp,
    value: surfForecast.surf.breakingWaveHeightMax,
  }));

  const correctedSurfHeightsMin = assimilate(
    interpolationRange,
    surfForecastsMin,
    reportedSurfHeightsMin,
  );

  const correctedSurfHeightsMax = assimilate(
    interpolationRange,
    surfForecastsMax,
    reportedSurfHeightsMax,
  );

  const correctedSurfHeights: SurfForecast[] = zip(
    correctedSurfHeightsMin,
    correctedSurfHeightsMax,
  ).map(([correctedSurfHeightMin, correctedSurfHeightMax]) => ({
    timestamp: correctedSurfHeightMin.timestamp,
    run: surfForecasts.find(
      surfForecast => surfForecast.timestamp === correctedSurfHeightMin.timestamp,
    ).run,
    surf: {
      breakingWaveHeightMin: correctedSurfHeightMin.value,
      breakingWaveHeightMax: correctedSurfHeightMax.value,
    },
  }));

  const filteredCorrectedSurfHeights = correctedSurfHeights.filter(
    surfHeight => surfHeight.timestamp >= start && (!end || surfHeight.timestamp <= end),
  );

  return fillRequestedData(filteredCorrectedSurfHeights, interval, start, end);
};

export const getSurfSpotSwellsForecastData = async (
  { pointOfInterestId, start, end, interval, models, units },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
) => {
  const {
    agency,
    model,
    run,
    grid: { key: grid },
  } = models[0];

  const records = await dataSources.sciencePlatformDB.getSwellRecords({
    agency,
    model,
    run,
    grid,
    pointOfInterestId,
    start,
    end,
  });

  const swellsForecasts = trackSwellEvents(
    records.map(record => ({
      timestamp: record.forecastTime / 1000,
      run: formatRunTime(record.run),
      swells: transformSwells(record),
    })),
  );

  const swells = swellsForecasts.map(({ timestamp, run, swells }) => ({
    timestamp,
    run,
    swells: convertSwellsUnits(swells, modelsStore[agency][model].storedUnits, units),
  }));

  return fillRequestedData(swells, interval, start, end);
};

export const getSurfSpotSwellEnsembleMembersForecastData = async (
  { pointOfInterestId, start, end, models, units },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
) => {
  const {
    agency,
    model,
    run,
    grid: { key: grid },
  } = models[0];

  const records = await dataSources.sciencePlatformDB.getSwellEnsembleMemberRecords({
    agency,
    model,
    run,
    grid,
    pointOfInterestId,
    start,
    end,
  });

  return records.map(record => ({
    timestamp: record.forecastTime / 1000,
    run: formatRunTime(record.run),
    member: record.member,
    wind: transformEnsembleWind(record, modelsStore[agency][model].storedUnits, units),
    swells: transformEnsembleSwells(record, modelsStore[agency][model].storedUnits, units),
  }));
};

export const getSurfSpotSwellProbabilitiesForecastData = async (
  { pointOfInterestId, start, end, interval, models },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
) => {
  const {
    agency,
    model,
    run,
    grid: { key: grid },
  } = models[0];

  const records = await dataSources.sciencePlatformDB.getSwellProbabilityRecords({
    agency,
    model,
    run,
    grid,
    pointOfInterestId,
    start,
    end,
  });

  const swellProbabilities = records.map(record => ({
    timestamp: record.forecastTime / 1000,
    run: formatRunTime(record.run),
    probability: record.probability,
  }));

  // TODO: pass requested start/end times to fillRequestedData
  //  when we have an SQL approach to get data necessary for filling
  return fillRequestedData(swellProbabilities, interval);
};

export const getSurfSpotWeatherForecastData = async (
  { pointOfInterestId, start, end, interval, models, units },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
): Promise<WeatherForecast[]> => {
  const maxRun = Math.max(...models.map(modelValue => modelValue.run));

  const weatherData = await Promise.all<WeatherForecast[]>(
    models.map(async modelValue => {
      const records = await dataSources.sciencePlatformDB.getWeatherRecords({
        pointOfInterestId,
        // If start timestamp is not given, use the corresponding timestamp
        // for the maximum run from all models. This is because some models may be
        // behind others (ex. WRF).
        start: start || runTimeToUnix(maxRun),
        end,
        agency: modelValue.agency,
        model: modelValue.model,
        run: modelValue.run,
        grid: modelValue.grid.key,
      });

      return records.map(record => ({
        timestamp: record.forecastTime / 1000,
        run: formatRunTime(record.run),
        weather: transformWeather(
          record,
          modelsStore[modelValue.agency][modelValue.model].storedUnits,
          units,
        ),
      }));
    }),
  );
  return fillRequestedData(blendData(weatherData, models), interval, start, end);
};

export const getSurfSpotSSTAnalysisData = async (
  { pointOfInterestId, start, end, interval, models, units },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
): Promise<SeaSurfaceTemperature[]> => {
  const {
    agency,
    model,
    grid: { key: grid },
  } = models[0];

  const records = await dataSources.sciencePlatformDB.getSeaSurfaceTemperatureRecords({
    pointOfInterestId,
    start,
    end,
    agency,
    model,
    grid,
  });

  const seaSurfaceTemperatures = records.map(record => ({
    timestamp: record.forecastTime / 1000,
    sst: {
      temperature: !isNumber(record.temperature)
        ? null
        : convertUnits('K', units.temperature)(record.temperature),
    },
  }));

  // TODO: pass requested start/end times to fillRequestedData
  //  when we have an SQL approach to get data necessary for filling
  return fillRequestedData(seaSurfaceTemperatures, interval);
};

export const getSurfSpotWindData = async (
  { pointOfInterestId, start, end, interval, models, units },
  _,
  { dataSources }: { dataSources: ScienceDataServiceDataSources },
): Promise<WindForecast[]> => {
  const maxRun = Math.max(...models.map(modelValue => modelValue.run));

  const sciencePlatformDB = dataSources.sciencePlatformDB;

  const offshoreDirection = (
    await sciencePlatformDB.getSurfSpotConfigurationRecord(pointOfInterestId)
  ).offshoreDirection;

  const windData = await Promise.all<WindForecast[]>(
    models.map(async modelValue => {
      const records = await sciencePlatformDB.getWindRecords({
        pointOfInterestId,
        // If start timestamp is not given, use the corresponding timestamp
        // for the maximum run from all models. This is because some models may be
        // behind others (ex. WRF).
        start: start || runTimeToUnix(maxRun),
        end,
        agency: modelValue.agency,
        model: modelValue.model,
        run: modelValue.run,
        grid: modelValue.grid.key,
      });

      return records.map(record => ({
        timestamp: record.forecastTime.getTime() / 1000,
        run: formatRunTime(record.run),
        wind: transformWind(
          record,
          modelsStore[modelValue.agency][modelValue.model].storedUnits,
          units,
          offshoreDirection,
        ),
      }));
    }),
  );
  return fillRequestedData(blendData(windData, models), interval, start, end);
};

export const getRatingForecast = async (
  {
    pointOfInterestId,
    start,
    end,
    interval,
    models,
    corrected,
    correctedSurf,
    ratingsAlgorithm: ratingsAlgorithmFromQueryParam,
    interpolationRange = 24,
  },
  args,
  context: { dataSources: ScienceDataServiceDataSources },
): Promise<{ timestamp: number; rating: { key: string; value: number } }[]> => {
  const { sciencePlatformDB } = context.dataSources;

  const surfModels = models.filter(m => m.forecastType === ForecastType.SURF);
  const windModels = models.filter(m => m.forecastType === ForecastType.WIND);
  const swellModels = models.filter(m => m.forecastType === ForecastType.SWELLS);

  const run = surfModels[0].run;

  start = start || runTimeToUnix(run);
  const queryInterval = correctQueryStartEnd(start, end, interpolationRange, corrected);

  const commonForecastInputs = { pointOfInterestId, start, end, interval };

  const configuration = await sciencePlatformDB.getSurfSpotConfigurationRecord(pointOfInterestId);

  const finalAlgorithm = ratingsAlgorithmFromQueryParam || configuration.ratingsAlgorithm;

  if (!finalAlgorithm) return null;

  const [surfForecast, windForecast, swellsForecast, reportedRatingRecords] = await Promise.all([
    getSurfSpotSurfForecastData(
      {
        ...commonForecastInputs,
        corrected: correctedSurf,
        models: surfModels,
        units: { waveHeight: 'ft' },
      },
      args,
      context,
    ),
    getSurfSpotWindData(
      {
        ...commonForecastInputs,
        models: windModels,
        units: { windSpeed: 'knot' },
      },
      args,
      context,
    ),
    finalAlgorithm === 'MATRIX'
      ? []
      : getSurfSpotSwellsForecastData(
          {
            ...commonForecastInputs,
            models: swellModels,
            units: { waveHeight: 'ft' },
          },
          args,
          context,
        ),
    corrected
      ? sciencePlatformDB.getReportedRatingRecords({
          pointOfInterestId,
          start: queryInterval.start,
          end: queryInterval.end,
        })
      : null,
  ]);

  const ratingsInput = {
    surfForecast: surfForecast,
    windForecast: windForecast,
    swellsForecast: swellsForecast,
  };

  const ratings = await getSurfRatings(ratingsInput, finalAlgorithm, context, configuration);

  if (corrected) {
    const reportedRatings = reportedRatingRecords.map(record => ({
      timestamp: record.forecastTime.getTime() / 1000,
      value: valueFromRating(record.rating),
    }));

    const ratingsTransformed = ratings.map(rating => ({
      timestamp: rating.timestamp,
      value: rating.rating,
    }));

    const correctedRatings = assimilate(interpolationRange, ratingsTransformed, reportedRatings);

    return correctedRatings.map(correctedRating => ({
      timestamp: correctedRating.timestamp,
      rating: {
        key: ratingFromValue(correctedRating.value),
        value: correctedRating.value,
      },
    }));
  } else {
    return ratings.map(rating => ({
      timestamp: rating.timestamp,
      rating: {
        key: ratingFromValue(rating.rating),
        value: rating.rating,
      },
    }));
  }
};
