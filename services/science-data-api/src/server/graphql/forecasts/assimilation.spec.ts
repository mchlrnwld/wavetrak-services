import { expect } from 'chai';
import { assimilate, linearInterpolation, isBetween } from './assimilation';

const assimilationSpec = () => {
  describe('assimilation', () => {
    describe('isBetween', () => {
      it('returns true when timestamp is in range. currentTimestamp is less than nextTimestamp.', () => {
        const currentTimestamp = new Date('2020-08-25T00:00:00.000Z').getTime() / 1000;
        const nextTimestamp = new Date('2020-08-25T04:00:00.000Z').getTime() / 1000;
        const targetTimestamp = new Date('2020-08-25T02:00:00.000Z').getTime() / 1000;
        expect(isBetween(currentTimestamp, nextTimestamp, targetTimestamp)).to.be.true();
      });

      it('returns false when timestamp is outside of range. currentTimestamp is less than nextTimestamp.', () => {
        const currentTimestamp = new Date('2020-08-25T00:00:00.000Z').getTime() / 1000;
        const nextTimestamp = new Date('2020-08-25T04:00:00.000Z').getTime() / 1000;
        const targetTimestamp = new Date('2020-08-25T05:00:00.000Z').getTime() / 1000;
        expect(isBetween(currentTimestamp, nextTimestamp, targetTimestamp)).to.be.false();
      });

      it('returns true when timestamp is in range. currentTimestamp is greater than nextTimestamp.', () => {
        const currentTimestamp = new Date('2020-08-25T05:00:00.000Z').getTime() / 1000;
        const nextTimestamp = new Date('2020-08-25T00:00:00.000Z').getTime() / 1000;
        const targetTimestamp = new Date('2020-08-25T02:00:00.000Z').getTime() / 1000;
        expect(isBetween(currentTimestamp, nextTimestamp, targetTimestamp)).to.be.true();
      });

      it('returns false when timestamp is outside range. currentTimestamp is greater than nextTimestamp.', () => {
        const currentTimestamp = new Date('2020-08-25T05:00:00.000Z').getTime() / 1000;
        const nextTimestamp = new Date('2020-08-25T00:00:00.000Z').getTime() / 1000;
        const targetTimestamp = new Date('2020-08-25T07:00:00.000Z').getTime() / 1000;
        expect(isBetween(currentTimestamp, nextTimestamp, targetTimestamp)).to.be.false();
      });
    });

    describe('linearInterpolation', () => {
      it('interpolates values within given range', () => {
        const currentTimestamp = new Date('2020-08-25T03:00:00.000Z').getTime() / 1000;
        const currentDifference = 10;
        const nextTimestamp = new Date('2020-08-25T09:00:00.000Z').getTime() / 1000;
        const nextDifference = 20;
        expect(
          linearInterpolation(
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          ),
        ).to.be.closeTo(11.67, 0.01);
        expect(
          linearInterpolation(
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          ),
        ).to.be.closeTo(13.33, 0.01);
        expect(
          linearInterpolation(
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T08:00:00.000Z').getTime() / 1000,
          ),
        ).to.be.closeTo(18.33, 0.01);
      });

      it('gets the difference at beginning of interval', () => {
        const currentTimestamp = new Date('2020-08-25T03:00:00.000Z').getTime() / 1000;
        const currentDifference = 10;
        const nextTimestamp = new Date('2020-08-25T09:00:00.000Z').getTime() / 1000;
        const nextDifference = 20;
        expect(
          linearInterpolation(
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          ),
        ).to.equal(10);
      });

      it('gets the difference at end of interval', () => {
        const currentTimestamp = new Date('2020-08-25T03:00:00.000Z').getTime() / 1000;
        const currentDifference = 10;
        const nextTimestamp = new Date('2020-08-25T09:00:00.000Z').getTime() / 1000;
        const nextDifference = 20;
        expect(
          linearInterpolation(
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T09:00:00.000Z').getTime() / 1000,
          ),
        ).to.equal(20);
      });

      it('errors if provided currentTimestamp and nextTimestamp are the same', () => {
        const currentTimestamp = new Date('2020-08-25T03:00:00.000Z').getTime() / 1000;
        const currentDifference = 10;
        const nextTimestamp = new Date('2020-08-25T03:00:00.000Z').getTime() / 1000;
        const nextDifference = 20;
        expect(
          linearInterpolation.bind(
            null,
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T09:00:00.000Z').getTime() / 1000,
          ),
        ).to.throw('currentTimestamp and nextTimestamp must differ for interpolation');
      });

      it('errors if targetTimestamp is outside the range of currentTimestamp and nextTimestamp', () => {
        const currentTimestamp = new Date('2020-08-25T03:00:00.000Z').getTime() / 1000;
        const currentDifference = 10;
        const nextTimestamp = new Date('2020-08-25T06:00:00.000Z').getTime() / 1000;
        const nextDifference = 20;
        expect(
          linearInterpolation.bind(
            null,
            currentTimestamp,
            currentDifference,
            nextTimestamp,
            nextDifference,
            new Date('2020-08-25T10:00:00.000Z').getTime() / 1000,
          ),
        ).to.throw('targetTimestamp must be in-between currentTimestamp and nextTimestamp');
      });
    });

    describe('assimilate', () => {
      it('returns no data when no values given', () => {
        const interpolationRange = 0;
        const values = [];
        const reportedValues = [
          { timestamp: new Date('2020-08-26T20:00:00.000Z').getTime() / 1000, value: 5 },
        ];
        expect(assimilate(interpolationRange, values, reportedValues)).to.be.empty();
        expect(assimilate(interpolationRange, values, reportedValues)).to.be.empty();
      });

      it('returns all values without corrections when no reported values given', () => {
        const interpolationRange = 0;
        const reportedValues = [];
        const values = [
          {
            timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
            value: 1.02,
          },
          {
            timestamp: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            value: 0.32,
          },
          {
            timestamp: new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
            value: 0.11,
          },
        ];

        const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
        expect(assimilatedValues.length).to.equal(3);
        expect(assimilatedValues[0].timestamp).to.equal(
          new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
        );
        expect(assimilatedValues[0].value).to.equal(1.02);
        expect(assimilatedValues[1].timestamp).to.equal(
          new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
        );
        expect(assimilatedValues[1].value).to.equal(0.32);
        expect(assimilatedValues[2].timestamp).to.equal(
          new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
        );
        expect(assimilatedValues[2].value).to.equal(0.11);
      });

      it('returns all values without corrections when there is no matching reported values', () => {
        const reportedValues = [
          { timestamp: new Date('2020-08-25T10:00:00.000Z').getTime() / 1000, value: 5 },
        ];
        const values = [
          {
            timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
            value: 1.02,
          },
          {
            timestamp: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            value: 0.32,
          },
          {
            timestamp: new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
            value: 0.11,
          },
        ];

        const interpolationRange = 2;
        const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
        expect(assimilatedValues.length).to.equal(3);
        expect(assimilatedValues[0].timestamp).to.equal(
          new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
        );
        expect(assimilatedValues[0].value).to.equal(1.02);
        expect(assimilatedValues[1].timestamp).to.equal(
          new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
        );
        expect(assimilatedValues[1].value).to.equal(0.32);
        expect(assimilatedValues[2].timestamp).to.equal(
          new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
        );
        expect(assimilatedValues[2].value).to.equal(0.11);
      });

      context('values before the first reported value', () => {
        const reportedValues = [
          { timestamp: new Date('2020-08-25T05:00:00.000Z').getTime() / 1000, value: 5 },
        ];
        const values = [
          {
            timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
            value: 1.02,
          },
          {
            timestamp: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            value: 0.41,
          },
          {
            timestamp: new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
            value: 0.11,
          },
        ];

        it('only corrects values that are within range, hours 3 & 4 are not corrected', () => {
          const interpolationRange = 0;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          expect(assimilatedValues[0].timestamp).to.equal(
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[0].value).to.equal(1.02);
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(0.41);
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.equal(5);
        });

        it('only corrects values that are within range, hour 3 is not corrected', () => {
          const interpolationRange = 1;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          expect(assimilatedValues[0].timestamp).to.equal(
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[0].value).to.equal(1.02);
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(0.41);
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.equal(5);
        });

        it('corrects all values', () => {
          const interpolationRange = 2;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          expect(assimilatedValues[0].timestamp).to.equal(
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[0].value).to.equal(1.02);
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(2.855);
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.equal(5);
        });
      });

      context('values in-between multiple reported values', () => {
        const reportedValues = [
          { timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000, value: 2 },
          { timestamp: new Date('2020-08-25T07:00:00.000Z').getTime() / 1000, value: 3 },
        ];
        const values = [
          // Matches with the first reported value.
          {
            timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
            value: 0.32,
          },
          {
            timestamp: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            value: 1.02,
          },
          {
            timestamp: new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
            value: 2.03,
          },
          {
            timestamp: new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
            value: 1.68,
          },
          // Matches with the last reported value.
          {
            timestamp: new Date('2020-08-25T07:00:00.000Z').getTime() / 1000,
            value: 0.56,
          },
        ];

        it('only calculates corrections for values within range', () => {
          const interpolationRange = 1;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          // Hour 4 correction is interpolated from Hour 3 with 0 difference.
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(1.02);
          // No correction calculated. Hour 5 not within range of reported hours 3 & 7.
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.equal(2.03);
          // Hour 6 correction is interpolated from Hour 3 with 0 difference.
          expect(assimilatedValues[3].timestamp).to.equal(
            new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[3].value).to.equal(1.68);
        });

        it('can combine corrections from multiple reported values when value within range', () => {
          const interpolationRange = 3;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          // The corrections for hours 4-6 are calculated as follows:
          // 1. Interpolation calculated from Hour 3 with 0 difference
          // 2. Interpolation calculated from Hour 7 with 0 difference.
          // 3. The corrections from each interpolation are then combined and applied.
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.be.closeTo(2.13, 0.01);
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.be.closeTo(3.4, 0.01);
          expect(assimilatedValues[3].timestamp).to.equal(
            new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[3].value).to.be.closeTo(3.3, 0.01);
        });

        it('calculates correction by interpolating between 2 reported values when reported values within range of each other', () => {
          const interpolationRange = 4;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          // Hour 4 correction is interpolated from Hour 3 & Hour 9.
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.be.closeTo(2.88, 0.01);
          // Hour 5 correction is interpolated from Hour 3 & Hour 9.
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.be.closeTo(4.09, 0.01);
          // Hour 5 correction is interpolated from Hour 3 & Hour 9.
          expect(assimilatedValues[3].timestamp).to.equal(
            new Date('2020-08-25T06:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[3].value).to.be.closeTo(3.92, 0.01);
        });
      });

      context('values after the last reported value', () => {
        const reportedValues = [
          { timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000, value: 5 },
        ];
        const values = [
          {
            timestamp: new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
            value: 1.02,
          },
          {
            timestamp: new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
            value: 0.41,
          },
          {
            timestamp: new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
            value: 0.11,
          },
        ];

        it('only corrects values that are within range, hours 4 & 5 are not corrected', () => {
          const interpolationRange = 0;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          expect(assimilatedValues[0].timestamp).to.equal(
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[0].value).to.equal(5);
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(0.41);
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.equal(0.11);
        });

        it('only corrects values that are within range, hour 5 is not corrected', () => {
          const interpolationRange = 1;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          expect(assimilatedValues[0].timestamp).to.equal(
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[0].value).to.equal(5);
          // Hour 4 interpolated from Hour 3.
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(0.41);
        });

        it('corrects all values', () => {
          const interpolationRange = 2;
          const assimilatedValues = assimilate(interpolationRange, values, reportedValues);
          expect(assimilatedValues[0].timestamp).to.equal(
            new Date('2020-08-25T03:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[0].value).to.equal(5);
          // Hour 4 interpolated from Hour 3.
          expect(assimilatedValues[1].timestamp).to.equal(
            new Date('2020-08-25T04:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[1].value).to.equal(2.4);
          // Hour 5 interpolated from Hour 3.
          expect(assimilatedValues[2].timestamp).to.equal(
            new Date('2020-08-25T05:00:00.000Z').getTime() / 1000,
          );
          expect(assimilatedValues[2].value).to.equal(0.11);
        });
      });
    });
  });
};

export default assimilationSpec;
