import { flatten, flow, groupBy, values, map, sortBy, head, zip } from 'lodash/fp';
import { ModelGrid } from '../../../models/modelGrid/index.d';
import nr from 'newrelic';

/**
 * Blends wind or weather data for each forecast hour.
 * The forecast hour with the highest grid resolution is chosen.
 *
 * @param data - The wind or weather data to blend.
 * @param models - The models associated with the wind or weather data.
 * @return A list of blended wind or weather data for each forecast hour.
 */
const blendData = (data: { timestamp: number }[][], models: ModelGrid[]) =>
  nr.startSegment(
    'blendData',
    true,
    () =>
      flow(
        sortBy([([_, model]) => model.grid.resolution]),
        map(([modelData, _]) => modelData),
        flatten,
        groupBy('timestamp'),
        values,
        map(head),
      )(zip(data, models)) as { timestamp: number }[],
  );

export default blendData;
