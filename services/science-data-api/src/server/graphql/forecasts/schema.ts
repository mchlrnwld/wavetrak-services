import { gql } from 'apollo-server-express';
import { ForecastType } from '../../../models/pointOfInterestDefaultForecastModel/index.d';
import getForecastModel from './forecastModel/getForecastModel';
import handleStartEndMinutes from './handleStartEndMinutes';
import {
  getSurfSpotSurfForecastData,
  getSurfSpotSwellsForecastData,
  getSurfSpotSwellEnsembleMembersForecastData,
  getSurfSpotSwellProbabilitiesForecastData,
  getSurfSpotWeatherForecastData,
  getSurfSpotSSTAnalysisData,
  getSurfSpotWindData,
  getRatingForecast,
} from './resolvers';

const typeDefs = gql`
  extend type Query {
    surfSpotForecasts(pointOfInterestId: String!): SurfSpotForecasts
  }

  type SurfSpotForecasts {
    pointOfInterestId: String!
    surf(
      agency: String
      model: String
      run: Int
      grid: String
      start: Int
      end: Int
      interval: Int = 10800
      corrected: Boolean = false
      waveHeight: String = "m"
    ): SurfForecast
    swells(
      agency: String
      model: String
      run: Int
      grid: String
      start: Int
      end: Int
      interval: Int = 10800
      waveHeight: String = "m"
    ): SwellsForecast
    swellEnsembleMembers(
      agency: String
      model: String
      run: Int
      grid: String
      start: Int
      end: Int
      waveHeight: String = "m"
      windSpeed: String = "m/s"
    ): SwellEnsembleMembersForecast
    swellProbabilities(
      agency: String
      model: String
      run: Int
      grid: String
      start: Int
      end: Int
      interval: Int = 10800
    ): SwellProbabilitiesForecast
    weather(
      agency: String
      model: String
      run: Int
      grid: String
      start: Int
      end: Int
      interval: Int = 10800
      temperature: String = "C"
      pressure: String = "mb"
      visibility: String = "m"
      precipitation: String = "mm"
    ): WeatherForecast
    sst(
      agency: String
      model: String
      grid: String
      start: Int
      end: Int
      temperature: String = "C"
      interval: Int = 86400
    ): SSTAnalysis
    wind(
      agency: String
      model: String
      run: Int
      grid: String
      start: Int
      end: Int
      interval: Int = 10800
      windSpeed: String = "m/s"
    ): WindForecast
    rating(
      run: Int
      start: Int
      end: Int
      interval: Int = 10800
      corrected: Boolean = false
      correctedSurf: Boolean = false
      ratingsAlgorithm: String
    ): RatingForecast
  }

  type ForecastModel {
    agency: String!
    model: String!
    run: Int!
    grid: ForecastGridPoint!
  }

  type ForecastGridPoint {
    key: String!
    lat: Latitude!
    lon: Longitude!
  }

  type SurfForecast {
    models: [ForecastModel]!
    units: SurfUnits!
    data: [SurfHour]!
  }

  type SurfUnits {
    waveHeight: String!
  }

  type SurfHour {
    timestamp: Int!
    run: Int!
    surf: Surf!
  }

  type Surf {
    breakingWaveHeightMin: Float
    breakingWaveHeightMax: Float
    breakingWaveHeightAlgorithm: BreakingWaveHeightAlgorithm
  }

  type SwellsForecast {
    models: [ForecastModel]!
    units: SwellsUnits!
    data: [SwellsHour]!
  }

  type SwellEnsembleMembersForecast {
    models: [ForecastModel]!
    units: SwellsUnits!
    data: [SwellEnsembleMembersHour]!
  }

  type SwellProbabilitiesForecast {
    models: [ForecastModel]!
    data: [SwellProbabilitiesHour]!
  }

  type SwellEnsembleMembersHour {
    timestamp: Int!
    run: Int!
    member: Int!
    wind: Wind!
    swells: Swells!
  }

  type SwellProbabilitiesHour {
    timestamp: Int!
    run: Int!
    probability: Float!
  }

  type SwellsUnits {
    waveHeight: String!
  }

  type SwellsHour {
    timestamp: Int!
    run: Int!
    swells: Swells!
  }

  type Swells {
    combined: Swell!
    components: [SwellComponent]!
    spectra1d: Spectra1d!
  }

  type Swell {
    height: Float
    direction: Float
    period: Float
  }

  type Spectra1d {
    has: Boolean!
    energy: [Float]
    period: [Float]
    direction: [Float]
    directionalSpread: [Float]
  }

  type WeatherForecast {
    models: [ForecastModel]!
    units: WeatherUnits!
    data: [WeatherHour]!
  }

  type WeatherUnits {
    temperature: String!
    pressure: String!
    visibility: String!
    precipitation: String!
  }

  type WeatherHour {
    timestamp: Int!
    run: Int!
    weather: Weather!
  }

  type Weather {
    pressure: Float!
    temperature: Float!
    visibility: Float!
    dewpoint: Float!
    humidity: Float!
    precipitation: Precipitation!
    conditions: String!
  }

  type WindForecast {
    models: [ForecastModel]!
    units: WindUnits!
    data: [WindHour]!
  }

  type WindHour {
    timestamp: Int!
    run: Int!
    wind: Wind!
  }

  type WindUnits {
    speed: String!
  }

  type Precipitation {
    type: String!
    volume: Float!
  }

  type SST {
    temperature: Float
  }

  type SSTAnalysis {
    models: [ForecastModel]!
    units: SSTUnits!
    data: [SSTHour]!
  }

  type SSTUnits {
    temperature: String!
  }

  type SSTHour {
    timestamp: Int!
    sst: SST!
  }

  type RatingForecast {
    models: [ForecastModel]!
    data: [RatingHour]
  }

  type RatingHour {
    timestamp: Int!
    rating: Rating!
  }

  type Rating {
    key: String!
    value: Float!
  }
`;

const resolvers = {
  Query: {
    surfSpotForecasts: (_obj, { pointOfInterestId }) => ({ pointOfInterestId }),
  },
  SurfSpotForecasts: {
    surf: getForecastModel(ForecastType.SURF),
    swells: getForecastModel(ForecastType.SWELLS),
    swellEnsembleMembers: getForecastModel(ForecastType.ENSEMBLE),
    swellProbabilities: getForecastModel(ForecastType.ENSEMBLE),
    weather: getForecastModel(ForecastType.WEATHER),
    sst: getForecastModel(ForecastType.SST),
    wind: getForecastModel(ForecastType.WIND),
    rating: getForecastModel(ForecastType.WIND, ForecastType.SURF, ForecastType.SWELLS),
  },
  SurfForecast: {
    data: handleStartEndMinutes(getSurfSpotSurfForecastData),
  },
  SwellsForecast: {
    data: handleStartEndMinutes(getSurfSpotSwellsForecastData),
  },
  SwellEnsembleMembersForecast: {
    data: handleStartEndMinutes(getSurfSpotSwellEnsembleMembersForecastData),
  },
  SwellProbabilitiesForecast: {
    data: handleStartEndMinutes(getSurfSpotSwellProbabilitiesForecastData),
  },
  WeatherForecast: {
    data: handleStartEndMinutes(getSurfSpotWeatherForecastData),
  },
  SSTAnalysis: {
    data: handleStartEndMinutes(getSurfSpotSSTAnalysisData),
  },
  WindForecast: {
    data: handleStartEndMinutes(getSurfSpotWindData),
  },
  RatingForecast: {
    data: handleStartEndMinutes(getRatingForecast),
  },
};

const schema = { typeDefs, resolvers };

export default schema;
