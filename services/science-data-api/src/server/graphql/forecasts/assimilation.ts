import { zip } from 'lodash';

export interface AssimilationValue {
  timestamp: number;
  value: number;
}

const timestampDifferenceInHours = (currentTimestamp: number, nextTimestamp: number): number => {
  return Math.abs(currentTimestamp - nextTimestamp) / 3600;
};

/**
 * Checks if the targetTimestamp is between the currentTimestamp and nextTimestamp.
 * @return true if targetTimestamp is in-between, false otherwise.
 */
export const isBetween = (
  currentTimestamp: number,
  nextTimestamp: number,
  targetTimestamp: number,
): boolean => (targetTimestamp - currentTimestamp) * (targetTimestamp - nextTimestamp) <= 0;

/**
 * Linearly interpolates 2 pairs of (timestamp, difference)
 * to find the target difference. Linear Interpolation is a
 * method of constructing new data points given two pairs of data.
 * Example:
 * currentTimestamp = 0, currentDifference = 5
 * nextTimestamp = 4, nextDifference = 10
 * targetTimestamp = 2
 * Returns: targetDifference = 2
 *
 * @param currentTimestamp - The current timestamp associated with the current difference.
 * @param currentDifference - The current difference associated with the current timestamp.
 * @param nextTimestamp - The next timestamp associated with the next difference.
 * @param nextDifference - The next difference associated with the next timestamp.
 * @param targetTimestamp - The target timestamp used to find the target difference.
 * @return The target difference interpolated from the 2 pairs of (timestamp, difference).
 */
export const linearInterpolation = (
  currentTimestamp: number,
  currentDifference: number,
  nextTimestamp: number,
  nextDifference: number,
  targetTimestamp: number,
): number => {
  if (targetTimestamp == currentTimestamp) {
    return currentDifference;
  }
  if (targetTimestamp == nextTimestamp) {
    return nextDifference;
  }
  if (nextTimestamp == currentTimestamp) {
    throw new Error('currentTimestamp and nextTimestamp must differ for interpolation');
  }
  if (!isBetween(currentTimestamp, nextTimestamp, targetTimestamp)) {
    throw new Error('targetTimestamp must be in-between currentTimestamp and nextTimestamp');
  }

  const targetDifference =
    currentDifference +
    (targetTimestamp - currentTimestamp) *
      ((nextDifference - currentDifference) / (nextTimestamp - currentTimestamp));

  return targetDifference;
};

/**
 * Corrects values based on reported values.
 * For values that do not have a matching reported value,
 * the value correction is linearly interpolated.
 * An interpolationRange value determines the interval of interpolation.
 *
 * @param interpolationRange - Determines the interval of interpolation.
 * @param modelRatings - Model data whose values to correct.
 * @param reportedRatings - Reported values to use for correction.
 * @return A list of corrected values based on the reported values given.
 */
export const assimilate = (
  interpolationRange: number,
  values: AssimilationValue[],
  reportedValues: AssimilationValue[],
): AssimilationValue[] => {
  let valueIndex = 0;
  let reportIndex = 0;
  const reportedCorrections = [];
  // Calculate reportedCorrections for all values and reported values that match.
  while (reportIndex < reportedValues.length && valueIndex < values.length) {
    if (values[valueIndex].timestamp == reportedValues[reportIndex].timestamp) {
      reportedCorrections.push({
        timestamp: reportedValues[reportIndex].timestamp,
        correction: reportedValues[reportIndex].value - values[valueIndex].value,
      });
      valueIndex++;
      reportIndex++;
    } else if (reportedValues[reportIndex].timestamp > values[valueIndex].timestamp) {
      valueIndex++;
    } else if (reportedValues[reportIndex].timestamp < values[valueIndex].timestamp) {
      reportIndex++;
    }
  }
  const corrections = [...Array(values.length)].map(_ => ({ value: 0 }));
  // Adjust each value that is within the
  // interpolationRange of a reported value.
  let reportedCorrectionsIndex = 0;
  for (const [index, value] of values.entries()) {
    // Value timestamp and reported value timestamp match.
    // Add the correction and move to next reported value and value.
    if (
      reportedCorrectionsIndex < reportedCorrections.length &&
      value.timestamp == reportedCorrections[reportedCorrectionsIndex].timestamp
    ) {
      corrections[index].value += reportedCorrections[reportedCorrectionsIndex].correction;
      reportedCorrectionsIndex++;
      continue;
    }
    // Calculate timestamp difference if the value
    // is between 2 reported value.
    const timestampDifference =
      reportedCorrectionsIndex && reportedCorrectionsIndex < reportedCorrections.length
        ? timestampDifferenceInHours(
            reportedCorrections[reportedCorrectionsIndex].timestamp,
            reportedCorrections[reportedCorrectionsIndex - 1].timestamp,
          ) <= interpolationRange
        : null;

    // Interpolate in-between 2 reported values and move to next value.
    if (timestampDifference) {
      corrections[index].value += linearInterpolation(
        reportedCorrections[reportedCorrectionsIndex - 1].timestamp,
        reportedCorrections[reportedCorrectionsIndex - 1].correction,
        reportedCorrections[reportedCorrectionsIndex].timestamp,
        reportedCorrections[reportedCorrectionsIndex].correction,
        value.timestamp,
      );
      continue;
    }
    // Interpolate from previous reported value to current value with 0 difference.
    // For values in-between 2 reported values and values after last reported value.
    if (
      reportedCorrectionsIndex &&
      timestampDifferenceInHours(
        value.timestamp,
        reportedCorrections[reportedCorrectionsIndex - 1].timestamp,
      ) <= interpolationRange
    ) {
      const nextTimestamp =
        reportedCorrections[reportedCorrectionsIndex - 1].timestamp + interpolationRange * 60 * 60;

      corrections[index].value += linearInterpolation(
        reportedCorrections[reportedCorrectionsIndex - 1].timestamp,
        reportedCorrections[reportedCorrectionsIndex - 1].correction,
        nextTimestamp,
        0,
        value.timestamp,
      );
    }
    // Interpolate from current value to next reported value with 0 difference.
    // For values in-between 2 reported values and values before first reported value.
    if (
      reportedCorrectionsIndex < reportedCorrections.length &&
      timestampDifferenceInHours(
        value.timestamp,
        reportedCorrections[reportedCorrectionsIndex].timestamp,
      ) <= interpolationRange
    ) {
      const nextTimestamp =
        reportedCorrections[reportedCorrectionsIndex].timestamp - interpolationRange * 60 * 60;

      corrections[index].value += linearInterpolation(
        reportedCorrections[reportedCorrectionsIndex].timestamp,
        reportedCorrections[reportedCorrectionsIndex].correction,
        nextTimestamp,
        0,
        value.timestamp,
      );
    }
  }
  return zip(values, corrections).map(([value, correction]) => ({
    ...value,
    value: value.value + correction.value,
  }));
};
