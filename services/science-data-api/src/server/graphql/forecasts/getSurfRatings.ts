import calculateRatings from './ratings/calculateRatings';
import { ScienceDataServiceDataSources } from '../dataSources';
import { SurfSpotConfigurationRecord } from '../../../models/surfSpotConfigurations/types';

const getSurfRatings = async (
  ratingsInput: { surfForecast, swellsForecast, windForecast },
  finalAlgorithm: string,
  context: { dataSources: ScienceDataServiceDataSources },
  configuration: SurfSpotConfigurationRecord
  ) => {
  const { surfRatingsAPI } = context.dataSources;

  if (finalAlgorithm !== 'MATRIX') {
    const modelRatingValues = await surfRatingsAPI.getSurfRatings(finalAlgorithm, {
      forecast: {
        surf: { data: ratingsInput.surfForecast },
        swells: { data: ratingsInput.swellsForecast },
        wind: { data: ratingsInput.windForecast },
      },
      offshore: configuration.offshoreDirection,
      refractionmatrix: configuration.spectralRefractionMatrix,
      ratingsOptions: configuration.ratingsOptions,
    });
  
    return modelRatingValues.map((rating, i) => ({
      timestamp: ratingsInput.surfForecast[i].timestamp,
      rating: rating,
    }));
  }
  const ratingsMatrixInputs = ratingsInput.surfForecast
    .map((s, i) => ({ ...s, ...ratingsInput.windForecast[i] }))
    .filter(i => i.surf && i.wind);

  return calculateRatings(configuration, ratingsMatrixInputs);
};
  
export default getSurfRatings;
