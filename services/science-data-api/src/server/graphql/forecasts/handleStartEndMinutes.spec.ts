import { expect } from 'chai';
import sinon from 'sinon';
import handleStartEndMinutes from './handleStartEndMinutes';

const handleStartEndMinutesSpec = () => {
  describe('handleStartEndMinutes', () => {
    // Mock a resolver that returns hourly data according to start and end.
    const queryFunc = sinon.spy(async ({ start, end, otherArgumentToPreserve }, _, __) => {
      const minutesToNextHour = (60 - new Date(start * 1000).getMinutes()) % 60;
      let timestamp = start + minutesToNextHour * 60;
      const data = [];
      while (timestamp <= end) {
        data.push({
          timestamp,
          otherArgumentToPreserve,
        });
        timestamp += 60 * 60;
      }
      return data;
    });

    beforeEach(() => {
      queryFunc.resetHistory();
    });

    it('returns data from query function adjusted to account for start and end with a non-integer hour UTC offset', async () => {
      const result = await handleStartEndMinutes(queryFunc)(
        {
          start: new Date('2020-10-05T11:15:00Z').getTime() / 1000,
          end: new Date('2020-10-05T14:00:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
        {},
        { dataSources: { someDataSource: {} } },
      );
      expect(queryFunc).to.have.been.calledOnce();
      expect(queryFunc.args[0]).to.deep.equal([
        {
          start: new Date('2020-10-05T11:00:00Z').getTime() / 1000,
          end: new Date('2020-10-05T13:45:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
        {},
        { dataSources: { someDataSource: {} } },
      ]);
      expect(result).to.deep.equal([
        {
          timestamp: new Date('2020-10-05T11:15:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
        {
          timestamp: new Date('2020-10-05T12:15:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
        {
          timestamp: new Date('2020-10-05T13:15:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
      ]);
    });

    it('returns data unadjusted if start and end are on a UTC hour', async () => {
      const args = {
        start: new Date('2020-10-05T11:00:00Z').getTime() / 1000,
        end: new Date('2020-10-05T13:00:00Z').getTime() / 1000,
        otherArgumentToPreserve: 0,
      };
      const result = await handleStartEndMinutes(queryFunc)(args, {}, {});
      expect(queryFunc).to.have.been.calledOnce();
      expect(queryFunc.args[0]).to.deep.equal([args, {}, {}]);
      expect(result).to.deep.equal([
        {
          timestamp: new Date('2020-10-05T11:00:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
        {
          timestamp: new Date('2020-10-05T12:00:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
        {
          timestamp: new Date('2020-10-05T13:00:00Z').getTime() / 1000,
          otherArgumentToPreserve: 0,
        },
      ]);
    });
  });
};

export default handleStartEndMinutesSpec;
