import { ApolloServer } from 'apollo-server-express';
import responseCachePlugin from 'apollo-server-plugin-response-cache';
import { RedisCache } from 'apollo-server-cache-redis';
import { newrelicTransactionPlugin } from './newrelic';
import schema from './schema';
import config from '../../config';
import dataSources from './dataSources';

const noCache = /no-cache/i;

export default new ApolloServer({
  cache:
    config.REDIS_IP && config.REDIS_PORT
      ? new RedisCache({
          host: config.REDIS_IP,
          port: config.REDIS_PORT,
          keyPrefix: 'science-data-service:',
        })
      : null,
  cacheControl: {
    defaultMaxAge: 5 * 60,
  },
  plugins: [
    newrelicTransactionPlugin,
    responseCachePlugin({
      shouldReadFromCache: ({
        request: {
          http: { headers },
        },
      }) => !headers.has('cache-control') || !noCache.test(headers.get('cache-control')),
    }),
  ],
  schema,
  dataSources,
});
