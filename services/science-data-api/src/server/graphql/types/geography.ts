import { GraphQLFloat, GraphQLScalarType } from 'graphql';

export const normalizeLongitudeOutgoing: (longitude: number) => number = longitude => {
  const lon = longitude % 360;
  if (lon < 180) return lon;
  return lon - 360;
};

export const normalizeLongitudeIncoming: (longitude: number) => number = longitude => {
  const lon = longitude % 360;
  if (lon < 0) return lon + 360;
  return lon;
};

export const LongitudeType = new GraphQLScalarType({
  name: 'Longitude',
  description:
    'The `Longitude` scalar type represents signed double-precision fractional longitude values.',
  serialize(longitude) {
    return normalizeLongitudeOutgoing(GraphQLFloat.serialize(longitude));
  },
  parseValue(longitude) {
    return normalizeLongitudeIncoming(GraphQLFloat.parseValue(longitude));
  },
  parseLiteral(ast) {
    return normalizeLongitudeIncoming(GraphQLFloat.parseLiteral(ast, null));
  },
});

function validateLatitude(latitude: number) {
  if (latitude < -90 || latitude > 90) {
    throw TypeError(`Latitude value ${latitude} not in range [-90, +90].`);
  }
  return latitude;
}

export const LatitudeType = new GraphQLScalarType({
  name: 'Latitude',
  description:
    'The `Latitude` scalar type represents signed double-precision fractional latitude values.',
  serialize(latitude) {
    return validateLatitude(GraphQLFloat.serialize(latitude));
  },
  parseValue(latitude) {
    return validateLatitude(GraphQLFloat.parseValue(latitude));
  },
  parseLiteral(ast) {
    return validateLatitude(GraphQLFloat.parseLiteral(ast, null));
  },
});
