import geographySpec from './geography.spec';

const typesSpec = () => {
  describe('types', () => {
    geographySpec();
  });
};

export default typesSpec;
