import { expect } from 'chai';
import { normalizeLongitudeIncoming, normalizeLongitudeOutgoing } from './geography';

const geographySpec = () => {
  describe('geography', () => {
    describe('normalizeLongitudeIncoming', () => {
      it('fits incoming longitude values into [0, 360)', () => {
        expect(normalizeLongitudeIncoming(-150)).to.equal(210);
        expect(normalizeLongitudeIncoming(-390)).to.equal(330);
        expect(normalizeLongitudeIncoming(360)).to.equal(0);
        expect(normalizeLongitudeIncoming(540)).to.equal(180);
      });
    });

    describe('normalizeLongitudeOutgoing', () => {
      it('fits outgoing longitude values into [-180, 180)', () => {
        expect(normalizeLongitudeOutgoing(270)).to.equal(-90);
        expect(normalizeLongitudeOutgoing(45)).to.equal(45);
        expect(normalizeLongitudeOutgoing(-540)).to.equal(-180);
        expect(normalizeLongitudeOutgoing(540)).to.equal(-180);
      });
    });
  });
};

export default geographySpec;
