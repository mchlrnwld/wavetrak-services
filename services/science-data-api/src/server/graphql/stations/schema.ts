import { gql } from 'apollo-server-express';
import {
  getSingleStationData,
  getStationsInBoundingBox,
  getStationInfo,
  getStationsWithinDistance,
} from '../../../models/stations';
import { StationInfoRecord } from '../../../models/stations/index.d';

const typeDefs = gql`
  extend type Query {
    station(id: String!): HistoricalStationData

    "Returns stations within specified distance (kilometers) of a geographic point"
    stationsRadius(
      point: PointInput!
      distance: Float!
      status: StationStatus
      limit: Int
    ): [LatestStationData]

    "Returns stations inside a geographic bounding box"
    stationsBoundingBox(boundingBox: BoundingBoxInput!, status: StationStatus): [LatestStationData]
  }
  input BoundingBoxInput {
    south: Latitude!
    west: Longitude!
    east: Longitude!
    north: Latitude!
  }
  input PointInput {
    latitude: Latitude!
    longitude: Longitude!
  }
  type Station {
    id: String!
    source: String!
    sourceId: String!
    name: String
    status: StationStatus!
    latitude: Latitude!
    longitude: Longitude!
    latestTimestamp: Int!
  }
  type HistoricalStationData {
    station: Station
    data(
      start: Int
      end: Int
      waveHeight: String = "m"
      windSpeed: String = "m/s"
      waterTemperature: String = "C"
      airTemperature: String = "C"
      dewPoint: String = "C"
      pressure: String = "mb"
    ): [StationData]
  }
  type LatestStationData {
    station: Station
    latestData(
      waveHeight: String = "m"
      windSpeed: String = "m/s"
      waterTemperature: String = "C"
      airTemperature: String = "C"
      dewPoint: String = "C"
      pressure: String = "mb"
    ): StationData
  }
  type StationData {
    timestamp: Int!
    height: Float!
    period: Float!
    direction: Float
    waterTemperature: Float
    airTemperature: Float
    dewPoint: Float
    pressure: Float
    wind: Wind
    swells: [SwellComponent]
  }
  enum StationStatus {
    ONLINE
    OFFLINE
  }
`;

const resolvers = {
  Query: {
    station: (_obj, { id }) => ({ id }),
    stationsRadius: async (
      _obj,
      { point, distance, status, limit },
    ): Promise<{ station: StationInfoRecord }[]> => {
      const stations = await getStationsWithinDistance(point, distance, status, limit);
      return stations.map(station => ({ station }));
    },
    stationsBoundingBox: async (
      _obj,
      { boundingBox, status },
    ): Promise<{ station: StationInfoRecord }[]> => {
      const stations = await getStationsInBoundingBox(boundingBox, status);
      return stations.map(station => ({ station }));
    },
  },
  HistoricalStationData: {
    station: ({ id }) => getStationInfo(id),
    data: async ({ id }, params) => {
      const { start, end, ...units } = params;
      let limit;
      if (!start && !end) {
        limit = 1;
      }
      return await getSingleStationData({ id, start, end, limit, units });
    },
  },
  LatestStationData: {
    latestData: async ({ station: { id } }, units) => {
      const data = await getSingleStationData({ id, limit: 1, units });
      return data.length > 0 ? data[0] : null;
    },
  },
};

const schema = { typeDefs, resolvers };

export default schema;
