import { expect } from 'chai';
import sinon from 'sinon';
import * as tzLookup from 'tz-lookup';
import getTimezones from './getTimezones';

const getTimezonesSpec = () => {
  describe('getTimezones', () => {
    beforeEach(() => {
      sinon.spy(tzLookup, 'default');
    });

    afterEach(() => {
      tzLookup.default.restore();
    });

    it('gets timezone per timestamp and builds timezone refIds', () => {
      const timestamps = [1551916800, 1552132800, 1552348800, 1552564800, 1552780800];
      const { timezones, timezoneRefIds } = getTimezones(33.75, -118, timestamps);

      expect(timezones).to.have.length(2);
      expect(timezones[0]).to.deep.equal({
        refId: 0,
        name: 'America/Los_Angeles',
        abbr: 'PST',
        offset: -8 * 60 * 60,
      });
      expect(timezones[1]).to.deep.equal({
        refId: 1,
        name: 'America/Los_Angeles',
        abbr: 'PDT',
        offset: -7 * 60 * 60,
      });

      expect(timezoneRefIds).to.have.length(timestamps.length);
      expect(timezoneRefIds[0]).to.equal(0);
      expect(timezoneRefIds[1]).to.equal(0);
      expect(timezoneRefIds[2]).to.equal(1);
      expect(timezoneRefIds[3]).to.equal(1);
      expect(timezoneRefIds[4]).to.equal(1);
    });

    it('looks up timezone based on latitude and wrapped longitude', () => {
      const timestamps = [1551916800];

      // lon > 180
      expect(getTimezones(33.75, 242, timestamps).timezones[0].name).to.equal(
        'America/Los_Angeles'
      );

      // lon < -180
      expect(getTimezones(33.75, -478, timestamps).timezones[0].name).to.equal(
        'America/Los_Angeles'
      );
    });
  });
};

export default getTimezonesSpec;
