const fillRequestedData = (data, interval, start = null, end = null) => {
  if (!data.length) {
    return [];
  }

  const range = {
    start: start === null ? data[0].timestamp : start,
    end: end === null ? data[data.length - 1].timestamp : end,
  };

  if (range.start < data[0].timestamp) {
    throw new Error('Attempted to fill with data after start timestamp');
  }

  /**
   * TODO:
   * We can't impose this limit currently because the 16 day time window is a hard requirement for
   * KBYG, even if the time window runs past the end of forecast run chosen by this service. Once
   * the data for KBYG is allowed to NOT include the entire time window, this limit should be
   * reinstated to avoid confusion from padding the end of a forecast.
   */
  // const dataInterval =
  //   data.length > 1 ? data[data.length - 1].timestamp - data[data.length - 2].timestamp : interval;
  //
  // if (range.end >= data[data.length - 1].timestamp + dataInterval) {
  //   throw new Error('Attempted to fill data to end beyond what is available');
  // }

  const timestamps = data.map(d => d.timestamp);
  const values = data.map(({ timestamp, ...rest }) => rest);

  let dataIndex = 0;
  const length = Math.floor(1 + (range.end - range.start) / interval);
  const result = [...Array(length)].map((_, i) => {
    const timestamp = range.start + i * interval;
    while (dataIndex < timestamps.length && timestamps[dataIndex] < timestamp) {
      dataIndex += 1;
    }
    if (dataIndex >= timestamps.length || timestamps[dataIndex] > timestamp) {
      dataIndex -= 1;
    }
    return {
      timestamp,
      ...values[dataIndex],
    };
  });

  return result;
};

export default fillRequestedData;
