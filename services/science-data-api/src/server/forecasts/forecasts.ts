import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import getModelMetaDataFactory from '../middleware/getModelMetaData';
import getModelQueryTimes from '../middleware/getModelQueryTimes';
import validateRequestedUnitsFactory, {
  swellsUnitsSchema,
  swellsEnsembleUnitsSchema,
  weatherUnitsSchema,
} from '../middleware/validateRequestedUnits';
import { swellsHandler, swellsEnsembleHandler, weatherHandler } from './handlers';

const validSwellsModels = new Set(['Wavetrak,Lotus-WW3']);
const validSwellsEnsembleModels = new Set(['NOAA,WW3-Ensemble']);
const validWeatherModels = new Set(['NOAA,GFS']);

const forecasts = Router();

forecasts.get(
  '/swells',
  wrapErrors(getModelMetaDataFactory(validSwellsModels, true)),
  wrapErrors(getModelQueryTimes),
  wrapErrors(validateRequestedUnitsFactory(swellsUnitsSchema)),
  wrapErrors(swellsHandler),
);

forecasts.get(
  '/swells-ensemble',
  wrapErrors(getModelMetaDataFactory(validSwellsEnsembleModels)),
  wrapErrors(getModelQueryTimes),
  wrapErrors(validateRequestedUnitsFactory(swellsEnsembleUnitsSchema)),
  wrapErrors(swellsEnsembleHandler),
);

forecasts.get(
  '/weather',
  wrapErrors(getModelMetaDataFactory(validWeatherModels, true)),
  wrapErrors(getModelQueryTimes),
  wrapErrors(validateRequestedUnitsFactory(weatherUnitsSchema)),
  wrapErrors(weatherHandler),
);

export default forecasts;
