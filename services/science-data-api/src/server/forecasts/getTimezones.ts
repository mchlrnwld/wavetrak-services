import moment from 'moment-timezone';
import tzLookup from 'tz-lookup';

const getTimezones = (lat, lon, timestamps) => {
  // Wrap longitude to [-180, 180]
  let wrappedLon = lon;
  while (wrappedLon < -180) {
    wrappedLon += 360;
  }

  while (wrappedLon > 180) {
    wrappedLon -= 360;
  }

  const zone = moment.tz.zone(tzLookup(lat, wrappedLon));
  const timezones = new Map();

  const timezoneRefIds = timestamps.map(timestamp => {
    const msFromEpoch = timestamp * 1000;
    const abbr = zone.abbr(msFromEpoch);

    if (!timezones.has(abbr)) {
      timezones.set(abbr, {
        refId: timezones.size,
        name: zone.name,
        abbr,
        offset: -zone.parse(msFromEpoch) * 60,
      });
    }

    return timezones.get(abbr).refId;
  });

  return {
    timezones: [...timezones.values()],
    timezoneRefIds,
  };
};

export default getTimezones;
