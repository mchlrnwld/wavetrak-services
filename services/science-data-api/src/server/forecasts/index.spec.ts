import fillRequestedDataSpec from './fillRequestedData.spec';
import getTimezonesSpec from './getTimezones.spec';

describe('/forecasts', () => {
  fillRequestedDataSpec();
  getTimezonesSpec();
});
