import { expect } from 'chai';
import fillRequestedData from './fillRequestedData';

const fillRequestedDataSpec = () => {
  describe('fillRequestedData', () => {
    const data = [
      { timestamp: 9, value: 0 },
      { timestamp: 12, value: 2 },
      { timestamp: 20, value: 3 },
      { timestamp: 30, value: 4 },
    ];

    it('returns data at the requested range and interval with data values carried forward', () => {
      expect(fillRequestedData(data, 5, 10, 30)).to.deep.equal([
        { timestamp: 10, value: 0 },
        { timestamp: 15, value: 2 },
        { timestamp: 20, value: 3 },
        { timestamp: 25, value: 3 },
        { timestamp: 30, value: 4 },
      ]);
    });

    it('returns data inclusive of the end points (if end - start = 0 modulo interval)', () => {
      expect(fillRequestedData(data, 7, null, null)).to.deep.equal([
        { timestamp: 9, value: 0 },
        { timestamp: 16, value: 2 },
        { timestamp: 23, value: 3 },
        { timestamp: 30, value: 4 },
      ]);
    });

    it('defaults to start and end from data', () => {
      expect(fillRequestedData(data, 5, null, null)).to.deep.equal([
        { timestamp: 9, value: 0 },
        { timestamp: 14, value: 2 },
        { timestamp: 19, value: 2 },
        { timestamp: 24, value: 3 },
        { timestamp: 29, value: 3 },
      ]);
    });

    it('errors if provided start is before first data point', () => {
      expect(fillRequestedData.bind(null, data, 5, 8, 30)).to.throw('start');
    });

    // it('errors if provided end is too far beyond last data point', () => {
    //   expect(fillRequestedData.bind(null, data, 5, 10, 40)).to.throw('end');
    // });

    it('returns empty if data is empty', () => {
      expect(fillRequestedData([], 5, 0, 35)).to.deep.equal([]);
    });

    it('extends the last forecast data point until the end of the interval defined by the last two data points', () => {
      fillRequestedData(data, 2, 9, 39).forEach(d => {
        if (d.timestamp < 12) expect(d.value).to.equal(0);
        else if (d.timestamp < 20) expect(d.value).to.equal(2);
        else if (d.timestamp < 30) expect(d.value).to.equal(3);
        else if (d.timestamp <= 39) expect(d.value).to.equal(4);
        else throw new Error('filled data contains timestamps outside of requested range');
      });
    });
  });
};

export default fillRequestedDataSpec;
