import { zip, chain } from 'lodash';
import { getSwells } from '../../models/swells';
import { getSwellsEnsemble } from '../../models/swellsEnsemble';
import { getWeather } from '../../models/weather';
import fillRequestedData from './fillRequestedData';
import getTimezones from './getTimezones';

export async function weatherHandler(req, res) {
  const {
    modelMetaData,
    modelQueryTimes,
    validatedQuery: { start, end, interval, units },
  } = req;
  const { grid } = modelMetaData;
  const weather = await getWeather(
    modelMetaData,
    modelQueryTimes.start,
    modelQueryTimes.end,
    units
  );
  const filledWeather = fillRequestedData(weather, interval, start, end);
  const { timezones, timezoneRefIds } = getTimezones(
    grid.lat,
    grid.lon,
    filledWeather.map(({ timestamp }) => timestamp)
  );
  const data = zip(timezoneRefIds, filledWeather).map(
    ([timezoneRefId, { timestamp, ...fields }]) => ({
      timestamp,
      timezoneRefId,
      ...fields,
    })
  );
  return res.send({
    associated: {
      model: modelMetaData,
      timezones,
      units,
    },
    data,
  });
}

export async function swellsHandler(req, res) {
  const {
    modelMetaData,
    modelQueryTimes,
    validatedQuery: { start, end, interval, units },
  } = req;
  const { grid } = modelMetaData;
  const swells = await getSwells(modelMetaData, modelQueryTimes.start, modelQueryTimes.end, units);
  const filledSwells = fillRequestedData(swells, interval, start, end);
  const { timezones, timezoneRefIds } = getTimezones(
    grid.lat,
    grid.lon,
    filledSwells.map(({ timestamp }) => timestamp)
  );
  const data = zip(timezoneRefIds, filledSwells).map(
    ([timezoneRefId, { timestamp, ...fields }]) => ({
      timestamp,
      timezoneRefId,
      ...fields,
    })
  );
  return res.send({
    associated: {
      model: modelMetaData,
      timezones,
      units,
    },
    data,
  });
}

export async function swellsEnsembleHandler(req, res) {
  const {
    modelMetaData,
    modelQueryTimes,
    validatedQuery: { start, end, interval, units },
  } = req;
  const { grid } = modelMetaData;
  const swellsEnsemble = await getSwellsEnsemble(
    modelMetaData,
    modelQueryTimes.start,
    modelQueryTimes.end,
    units
  );

  const values = chain(swellsEnsemble)
    .groupBy(se => se.timestamp)
    .map((value, key) => ({
      timestamp: parseInt(key, 10),
      members: chain(value)
        .sortBy(memberData => memberData.member)
        .map(memberData => chain(memberData).pick(['wind', 'swells'])),
    }))
    .value();
  const filledSwells = fillRequestedData(values, interval, start, end);
  const { timezones, timezoneRefIds } = getTimezones(
    grid.lat,
    grid.lon,
    filledSwells.map(({ timestamp }) => timestamp)
  );
  const data = zip(timezoneRefIds, filledSwells).map(
    ([timezoneRefId, { timestamp, ...fields }]) => ({
      timestamp,
      timezoneRefId,
      ...fields,
    })
  );
  return res.send({
    associated: {
      model: modelMetaData,
      timezones,
      units,
    },
    data,
  });
}
