import { expect } from 'chai';
import sinon from 'sinon';
import axios from 'axios';
import mockExpressRequests from '../../mockExpressRequests.spec';
import autoConfirmSubscription from './autoConfirmSubscription';

const autoConfirmSubscriptionSpec = () => {
  describe('autoConfirmSubscription', () => {
    let request;
    const SNS_TOPIC_ARN = 'arn:aws:sns:us-west-1:123456789012:MyTopic';
    const SUBSCRIBE_URL = `https://sns.us-west-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=${SNS_TOPIC_ARN}`;

    before(() => {
      request = mockExpressRequests(app => {
        app.use('/sns', autoConfirmSubscription(SNS_TOPIC_ARN), (_req, res) =>
          res.send({ message: 'Other message type' }),
        );
      });

      sinon.stub(axios, 'get');
    });

    afterEach(() => {
      axios.get.resetHistory();
    });

    after(() => {
      axios.get.restore();
    });

    it('does nothing if the message type is not SubscriptionConfirmation', async () => {
      const res = await request.post('/sns').set('x-amz-sns-message-type', 'Notification');
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ message: 'Other message type' });
      expect(axios.get).not.to.have.been.called();
    });

    it('validates SubscribeURL has correct host and action before subscribing', async () => {
      const res = await request
        .post('/sns')
        .set('x-amz-sns-message-type', 'SubscriptionConfirmation')
        .send({
          SubscribeURL: `https://sns.us-west-1.amazonaws.com/?Action=FakeAction&TopicArn=${SNS_TOPIC_ARN}`,
        });
      expect(res).to.have.status(400);
      expect(res.body).to.deep.equal({ message: 'Invalid SubscribeURL.' });
      expect(axios.get).not.to.have.been.called();
    });

    it('validates SubscribeURL has correct SNS topic ARN before subscribing', async () => {
      const res = await request
        .post('/sns')
        .set('x-amz-sns-message-type', 'SubscriptionConfirmation')
        .send({
          SubscribeURL:
            'https://sns.us-west-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-1:123456789012:FakeTopic',
        });
      expect(res).to.have.status(400);
      expect(res.body).to.deep.equal({ message: 'Invalid SubscribeURL.' });
      expect(axios.get).not.to.have.been.called();
    });

    it('treats failed request to SubscribeURL as validation error', async () => {
      axios.get.rejects('Error from SubscribeURL');
      const res = await request
        .post('/sns')
        .set('x-amz-sns-message-type', 'SubscriptionConfirmation')
        .send({ SubscribeURL: SUBSCRIBE_URL });
      expect(res).to.have.status(400);
      expect(res.body).to.deep.equal({
        message: `GET ${SUBSCRIBE_URL} failed with error:\nError from SubscribeURL.`,
      });
      expect(axios.get).to.have.been.calledOnce();
      expect(axios.get).to.have.been.calledWithExactly(SUBSCRIBE_URL);
    });

    it('auto confirms an SNS topic subscription', async () => {
      axios.get.resolves({ data: 'Confirmation response data' });
      const res = await request
        .post('/sns')
        .set('x-amz-sns-message-type', 'SubscriptionConfirmation')
        .send({ SubscribeURL: SUBSCRIBE_URL });
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ message: 'OK' });
      expect(axios.get).to.have.been.calledOnce();
      expect(axios.get).to.have.been.calledWithExactly(SUBSCRIBE_URL);
    });
  });
};

export default autoConfirmSubscriptionSpec;
