import axios from 'axios';
import log from '../../common/logger';

const SNS_HOST_PATTERN = '^https://sns..+.amazonaws.com.*Action=ConfirmSubscription';

/**
 * Creates an Express middleware for autoconfirming an SNS subscription.
 *
 * @param snsTopicARN - SNS topic ARN to subscribe to.
 * @return Express middleware for handling SubscriptionConfirmation message type.
 */
const autoConfirmSubscription = (snsTopicARN: string) => {
  const snsTopicARNParameter = `TopicArn=${snsTopicARN}`;
  return async function autoConfirmSubscription(req, res, next) {
    const messageType = req.headers['x-amz-sns-message-type'];
    if (messageType !== 'SubscriptionConfirmation') {
      return next();
    }

    const message = req.body;
    log.info(`Attempting to confirm SubscribeURL: ${message.SubscribeURL}...`);

    if (
      !message.SubscribeURL ||
      !message.SubscribeURL.match(SNS_HOST_PATTERN) ||
      !message.SubscribeURL.includes(snsTopicARNParameter)
    ) {
      log.error(`Invalid SubscribeURL: ${message.SubscribeURL}.`);
      return res.status(400).send({ message: 'Invalid SubscribeURL.' });
    }

    try {
      const response = await axios.get(message.SubscribeURL);
      log.info(`${snsTopicARN} subscription confirmed:\n${response.data}`);
    } catch (err) {
      log.error(`Error confirming subscription: ${err}`);
      return res.status(400).send({
        message: `GET ${message.SubscribeURL} failed with error:\n${err}.`,
      });
    }

    return res.send({ message: 'OK' });
  };
};

export default autoConfirmSubscription;
