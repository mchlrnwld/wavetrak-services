import { expect } from 'chai';
import sinon from 'sinon';
import mockExpressRequests from '../../mockExpressRequests.spec';
import * as reportedSurfHeightsModel from '../../models/reportedSurfHeights';
import * as reportedRatingsModel from '../../models/reportedRatings';
import * as reports from '../../external/reports';
import * as spots from '../../external/spots';
import spotReportUpdated from './spotReportUpdated';
import { roundDateToNearestHour } from './spotReportUpdated';
import newrelic from 'newrelic';

const spotReportUpdatedSpec = () => {
  describe('spotReportUpdated', () => {
    let request;

    before(() => {
      request = mockExpressRequests(app => {
        app.use('/sns/spot-report-updated', spotReportUpdated);
      });

      sinon.stub(reportedSurfHeightsModel, 'updateReportedSurfHeights');
      sinon.stub(reportedRatingsModel, 'updateReportedRatings');
      sinon.stub(reports, 'getLatestSpotReport');
      sinon.stub(spots, 'getSpot');
      sinon.stub(newrelic, 'addCustomAttribute');
    });

    afterEach(() => {
      reportedSurfHeightsModel.updateReportedSurfHeights.resetHistory();
      reportedRatingsModel.updateReportedRatings.resetHistory();
      reports.getLatestSpotReport.resetHistory();
      spots.getSpot.resetHistory();
    });

    after(() => {
      reportedSurfHeightsModel.updateReportedSurfHeights.restore();
      reportedRatingsModel.updateReportedRatings.restore();
      reports.getLatestSpotReport.restore();
      spots.getSpot.restore();
    });

    it('returns 400 for invalid message types', async () => {
      const res = await request
        .post('/sns/spot-report-updated')
        .set('x-amz-sns-message-type', 'Unsubscribe');
      expect(res).to.have.status(400);
      expect(res.body).to.deep.equal({ message: 'Unable to handle message type Unsubscribe.' });
      expect(spots.getSpot).not.to.have.been.called();
      expect(reports.getLatestSpotReport).not.to.have.been.called();
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).not.to.have.been.called();
      expect(reportedRatingsModel.updateReportedRatings).not.to.have.been.called();
    });

    it('rounds date to the next hour', () => {
      const date = new Date('2020-04-15T20:42:57Z');
      const nearestHour = roundDateToNearestHour(date);
      expect(nearestHour.getTime()).to.equal(new Date('2020-04-15T21:00:00.000Z').getTime());
    });

    it('rounds date to hour of the next day', () => {
      const date = new Date('2020-04-15T23:42:57Z');
      const nearestHour = roundDateToNearestHour(date);
      expect(nearestHour.getTime()).to.equal(new Date('2020-04-16T00:00:00.000Z').getTime());
    });

    it('rounds date in middle of hour to the next hour', () => {
      const date = new Date('2020-04-15T21:30:00Z');
      const nearestHour = roundDateToNearestHour(date);
      expect(nearestHour.getTime()).to.equal(new Date('2020-04-15T22:00:00.000Z').getTime());
    });

    it('rounds date to the current hour', () => {
      const date = new Date('2020-04-15T21:27:00Z');
      const nearestHour = roundDateToNearestHour(date);
      expect(nearestHour.getTime()).to.equal(new Date('2020-04-15T21:00:00.000Z').getTime());
    });

    it('parses spotId out of a JSON message and stores a reported surf height and rating for the latest report if the spot has a point of interest', async () => {
      const spotId = '5842041f4e65fad6a77088ed';
      const pointOfInterestId = '00046210-122e-46d1-9d0d-b8066cc90f1b';
      const report = {
        rating: 'FAIR',
        minHeight: 2,
        maxHeight: 3,
        createdAt: '2020-04-15T20:42:57Z',
      };
      spots.getSpot.resolves({ pointOfInterestId });
      reports.getLatestSpotReport.resolves(report);

      const res = await request
        .post('/sns/spot-report-updated')
        .set('x-amz-sns-message-type', 'Notification')
        .send({ Message: `{"spotId":"${spotId}"}` });

      expect(res).to.have.status(200);
      expect(spots.getSpot).to.have.been.calledOnce();
      expect(spots.getSpot).to.have.been.calledWithExactly(spotId);
      expect(reports.getLatestSpotReport).to.have.been.calledOnce();
      expect(reports.getLatestSpotReport).to.have.been.calledWithExactly(spotId);
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).to.have.been.calledOnce();
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).to.have.been.calledWithExactly([
        {
          pointOfInterestId,
          min: report.minHeight,
          max: report.maxHeight,
          forecastTime: new Date('2020-04-15T21:00:00Z'),
        },
      ]);
      expect(reportedRatingsModel.updateReportedRatings).to.have.been.calledOnce();
      expect(reportedRatingsModel.updateReportedRatings).to.have.been.calledWithExactly([
        {
          pointOfInterestId,
          rating: report.rating,
          forecastTime: new Date('2020-04-15T21:00:00Z'),
        },
      ]);
    });
    it('parses spotId out of a JSON message and stores a reported surf height and rating for the latest report if the spot has a point of interest', async () => {
      const spotId = '5842041f4e65fad6a77088ed';
      const pointOfInterestId = '00046210-122e-46d1-9d0d-b8066cc90f1b';
      const report = {
        rating: 'FAIR',
        minHeight: 2,
        maxHeight: 3,
        createdAt: '2020-04-15T20:29:57Z',
      };
      spots.getSpot.resolves({ pointOfInterestId });
      reports.getLatestSpotReport.resolves(report);

      const res = await request
        .post('/sns/spot-report-updated')
        .set('x-amz-sns-message-type', 'Notification')
        .send({ Message: `{"spotId":"${spotId}"}` });

      expect(res).to.have.status(200);
      expect(spots.getSpot).to.have.been.calledOnce();
      expect(spots.getSpot).to.have.been.calledWithExactly(spotId);
      expect(reports.getLatestSpotReport).to.have.been.calledOnce();
      expect(reports.getLatestSpotReport).to.have.been.calledWithExactly(spotId);
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).to.have.been.calledOnce();
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).to.have.been.calledWithExactly([
        {
          pointOfInterestId,
          min: report.minHeight,
          max: report.maxHeight,
          forecastTime: new Date('2020-04-15T20:00:00Z'),
        },
      ]);
      expect(reportedRatingsModel.updateReportedRatings).to.have.been.calledOnce();
      expect(reportedRatingsModel.updateReportedRatings).to.have.been.calledWithExactly([
        {
          pointOfInterestId,
          rating: report.rating,
          forecastTime: new Date('2020-04-15T20:00:00Z'),
        },
      ]);
    });

    it('does nothing if spot does not have point of interest', async () => {
      const spotId = '5842041f4e65fad6a77088ed';
      spots.getSpot.resolves({ pointOfInterestId: null });

      const res = await request
        .post('/sns/spot-report-updated')
        .set('x-amz-sns-message-type', 'Notification')
        .send({ Message: `{"spotId":"${spotId}"}` });

      expect(res).to.have.status(200);
      expect(spots.getSpot).to.have.been.calledOnce();
      expect(spots.getSpot).to.have.been.calledWithExactly(spotId);
      expect(reports.getLatestSpotReport).not.to.have.been.called();
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).not.to.have.been.called();
      expect(reportedRatingsModel.updateReportedRatings).not.to.have.been.called();
    });

    it('does nothing if report missing wave heights', async () => {
      const spotId = '5842041f4e65fad6a77088ed';
      const pointOfInterestId = '00046210-122e-46d1-9d0d-b8066cc90f1b';
      const report = {
        minHeight: -1,
        maxHeight: -1,
        createdAt: '2020-04-15T20:29:57Z',
      };
      spots.getSpot.resolves({ pointOfInterestId });
      reports.getLatestSpotReport.resolves(report);

      const res = await request
        .post('/sns/spot-report-updated')
        .set('x-amz-sns-message-type', 'Notification')
        .send({ Message: `{"spotId":"${spotId}"}` });

      expect(res).to.have.status(200);
      expect(spots.getSpot).to.have.been.calledOnce();
      expect(spots.getSpot).to.have.been.calledWithExactly(spotId);
      expect(reports.getLatestSpotReport).to.have.been.calledOnce();
      expect(reports.getLatestSpotReport).to.have.been.calledWithExactly(spotId);
      expect(reportedSurfHeightsModel.updateReportedSurfHeights).not.to.have.been.called();
      expect(reportedRatingsModel.updateReportedRatings).not.to.have.been.called();
    });
  });
};

export default spotReportUpdatedSpec;
