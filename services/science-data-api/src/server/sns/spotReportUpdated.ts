import { getLatestSpotReport } from '../../external/reports';
import { getSpot } from '../../external/spots';
import { updateReportedSurfHeights } from '../../models/reportedSurfHeights';
import { updateReportedRatings } from '../../models/reportedRatings';
import newrelic from 'newrelic';
import log from '../../common/logger';

/**
 * Rounds Date object to the nearest hour.
 */
export const roundDateToNearestHour = (date: Date): Date => {
  const msInHour = 60 * 60 * 1000;
  return new Date(Math.round(date.getTime() / msInHour) * msInHour);
};

/**
 * Express middleware handles spot report updated notification message. New reported rating and surf height
 * will be stored in database for the spot's point of interest based on the latest report for the
 * spot.
 */
async function spotReportUpdated(req, res) {
  const messageType = req.headers['x-amz-sns-message-type'];
  if (messageType !== 'Notification') {
    return res.status(400).send({ message: `Unable to handle message type ${messageType}.` });
  }

  const { spotId } = JSON.parse(req.body.Message);

  newrelic.addCustomAttribute('spot', spotId);

  let pointOfInterestId;
  try {
    pointOfInterestId = (await getSpot(spotId)).pointOfInterestId;
    log.info(`Got point of interest id: ${pointOfInterestId} for spot: ${spotId}`);
  } catch (err) {
    log.error(`getSpot with spot id: ${spotId} failed:\n${err}`);
    if (err.response.status == 404) {
      return res.status(400).send({
        message: `Spot id: ${spotId} not found:\n${err}.`,
      });
    }
    throw err;
  }

  if (!pointOfInterestId) {
    return res.send({ message: 'OK' });
  }

  const { minHeight: min, maxHeight: max, createdAt, rating } = await getLatestSpotReport(spotId);
  // TODO: "Delete" report if heights are missing.
  if (min >= 0 && max >= 0) {
    const forecastTime = roundDateToNearestHour(new Date(createdAt));
    await Promise.all([
      updateReportedSurfHeights([
        {
          pointOfInterestId,
          min,
          max,
          forecastTime,
        },
      ]),
      updateReportedRatings([
        {
          pointOfInterestId,
          rating,
          forecastTime,
        },
      ]),
    ]);
  }

  return res.send({ message: 'OK' });
}

export default spotReportUpdated;
