import autoConfirmSubscriptionSpec from './autoConfirmSubscription.spec';
import spotReportUpdatedSpec from './spotReportUpdated.spec';

describe('/sns', () => {
  autoConfirmSubscriptionSpec();
  spotReportUpdatedSpec();
});
