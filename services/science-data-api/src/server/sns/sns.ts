import { Router } from 'express';
import { text } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import log from '../../common/logger';
import verifyMessage from './verifyMessage';
import autoConfirmSubscription from './autoConfirmSubscription';
import spotReportUpdated from './spotReportUpdated';
import config from '../../config';

const sns = Router();

// HACK: SNS messages are JSON but sent with Content-Type: text/plain; charset=UTF-8.
sns.use(text(), (req, res, next) => {
  if (!req.headers['content-type'] || !req.headers['content-type'].includes('application/json')) {
    try {
      req.body = JSON.parse(req.body);
    } catch (err) {
      log.error(`Error parsing SNS message payload:\n${err}`);
      return res.status(400).send({ message: 'Message payload invalid.' });
    }
  }
  return next();
});

sns.post(
  '/spot-report-updated',
  wrapErrors(verifyMessage(config.SPOT_REPORT_UPDATED_SNS_TOPIC)),
  wrapErrors(autoConfirmSubscription(config.SPOT_REPORT_UPDATED_SNS_TOPIC)),
  wrapErrors(spotReportUpdated),
);

export default sns;
