/**
 * Creates an Express middleware for verifying a message signature (TODO) and topic ARN.
 *
 * @param snsTopicARN - SNS topic ARN to subscribe to.
 * @return Express middleware for verifying a message.
 */
const verifyMessage = (snsTopicARN: string) =>
  async function verifyMessage(req, res, next) {
    const message = req.body;
    if (message.TopicArn !== snsTopicARN) {
      return res.status(400).send({ message: 'Invalid SNS topic ARN.' });
    }

    return next();
  };

export default verifyMessage;
