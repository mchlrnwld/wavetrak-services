import { ErrorRequestHandler } from 'express';
import { setupExpress } from '@surfline/services-common';
import log from '../common/logger';
import trackRequests from '../common/trackRequests';
import forecasts from './forecasts';
import modelRuns from './modelRuns';
import modelGrids from './modelGrids';
import pointsOfInterestGridPoints from './pointsOfInterestGridPoints';
import sns from './sns';
import graphql from './graphql';
import config from '../config';
import ValidationError from '../common/errors/ValidationError';

/**
 * @description A custom error handler that sits above the
 * common error handler from `services-common` that is set
 * up by `setupExpress`. This error handler will catch errors
 * specific to this service to handle them appropriately. Generic
 * errors will be thrown up to the next error handler using
 * `next(error)`
 */
const customErrorHander: ErrorRequestHandler = (error: ValidationError | Error, _, res, next) => {
  // If the error is a validation error then create custom response
  if (error instanceof ValidationError) {
    return res.status(error.status).send({
      message: error.message || 'Error encountered',
      ...error.body,
    });
  }

  // If it is a generic error, bubble it up to the next
  // error handler
  return next(error);
};

const server = () =>
  setupExpress({
    log,
    port: (config.EXPRESS_PORT as number) || 8081,
    name: 'science-data-service',
    allowedMethods: ['GET', 'OPTIONS'],
    handlers: [
      ['*', trackRequests],
      ['/forecasts', forecasts],
      ['/model_runs', modelRuns],
      ['/model_grids', modelGrids],
      ['/points_of_interest_grid_points', pointsOfInterestGridPoints],
      ['/sns', sns],
      customErrorHander,
    ],
    callback: app => {
      graphql.applyMiddleware({ app });
    },
  });

export default server;
