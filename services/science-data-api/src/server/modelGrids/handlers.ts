import { queryModelGrids } from '../../models/modelGrid';

// eslint-disable-next-line import/prefer-default-export
export async function getModelGridsHandler(req, res) {
  const { query } = req;
  const params = {
    agency: query.agency,
    model: query.model,
  };
  const modelGrids = await queryModelGrids(params);
  return res.send(modelGrids);
}
