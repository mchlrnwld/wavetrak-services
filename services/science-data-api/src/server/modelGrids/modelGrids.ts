import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { getModelGridsHandler } from './handlers';

const modelGrids = Router();

modelGrids.get('/', wrapErrors(getModelGridsHandler));

export default modelGrids;
