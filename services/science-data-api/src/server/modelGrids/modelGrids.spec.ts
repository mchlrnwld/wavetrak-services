import { expect } from 'chai';
import sinon from 'sinon';
import mockExpressRequests from '../../mockExpressRequests.spec';
import * as modelGridsModel from '../../models/modelGrid';
import modelGridsFixture from '../../fixtures/modelGrids';
import modelGrids from './modelGrids';

const modelGridsSpec = () => {
  let request;

  before(() => {
    request = mockExpressRequests(app => {
      app.use('/model_grids', modelGrids);
    });

    sinon.stub(modelGridsModel, 'queryModelGrids').resolves(modelGridsFixture);
  });

  describe('GET', () => {
    it('queries model grids and returns agency, model, and grid', async () => {
      const query = {
        agency: 'Wavetrak',
        model: 'Lotus-WW3',
      };
      const res = await request.get('/model_grids').query(query);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(modelGridsFixture);
      expect(modelGridsModel.queryModelGrids).to.have.been.calledOnce();
      expect(modelGridsModel.queryModelGrids).to.have.been.calledWithExactly(query);
    });
  });
};

export default modelGridsSpec;
