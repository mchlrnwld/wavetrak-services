import { queryModelRuns, updateModelRuns } from '../../models/modelRun';

export async function getModelRunsHandler(req, res) {
  const { query } = req;
  const params = {
    agency: query.agency,
    model: query.model,
    run: query.run,
    statuses: query.status ? query.status.split(',') : ['ONLINE'],
    types: query.types ? query.types.split(',') : null,
  };
  const modelRuns = await queryModelRuns(params);
  return res.send(modelRuns);
}

export async function putModelRunsHandler(req, res) {
  const modelRuns = req.body;
  await updateModelRuns(modelRuns);
  return res.send({ message: 'OK' });
}
