import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { getModelRunsHandler, putModelRunsHandler } from './handlers';

const modelRuns = Router();

modelRuns.get('/', wrapErrors(getModelRunsHandler));
modelRuns.put('/', wrapErrors(putModelRunsHandler));

export default modelRuns;
