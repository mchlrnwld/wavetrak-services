import { expect } from 'chai';
import sinon from 'sinon';
import mockExpressRequests from '../../mockExpressRequests.spec';
import modelRunsFixture from '../../fixtures/modelRuns';
import * as modelRunsModel from '../../models/modelRun';
import modelRuns from './modelRuns';

const modelRunsSpec = () => {
  let request;

  before(() => {
    request = mockExpressRequests(app => {
      app.use('/model_runs', modelRuns);
    });

    sinon.stub(modelRunsModel, 'queryModelRuns').resolves(modelRunsFixture);
    sinon.stub(modelRunsModel, 'updateModelRuns').resolves();
  });

  afterEach(() => {
    modelRunsModel.queryModelRuns.resetHistory();
    modelRunsModel.updateModelRuns.resetHistory();
  });

  after(() => {
    modelRunsModel.queryModelRuns.restore();
    modelRunsModel.updateModelRuns.restore();
  });

  describe('GET', () => {
    it('queries model runs (online by default) and returns results', async () => {
      const query = {
        agency: 'NOAA',
        model: 'GFS',
        run: '2018113000',
      };
      const res = await request.get('/model_runs').query(query);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(modelRunsFixture);
      expect(modelRunsModel.queryModelRuns).to.have.been.calledOnce();
      expect(modelRunsModel.queryModelRuns).to.have.been.calledWithExactly({
        ...query,
        statuses: ['ONLINE'],
        types: null,
      });
    });

    it('parses status and types query params as a CSV', async () => {
      const res = await request.get('/model_runs').query({
        status: 'ONLINE,PENDING',
        types: 'POINT_OF_INTEREST,FULL_GRID',
      });
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(modelRunsFixture);
      expect(modelRunsModel.queryModelRuns).to.have.been.calledOnce();
      expect(modelRunsModel.queryModelRuns).to.have.been.calledWithExactly({
        agency: undefined,
        model: undefined,
        run: undefined,
        statuses: ['ONLINE', 'PENDING'],
        types: ['POINT_OF_INTEREST', 'FULL_GRID'],
      });
    });
  });

  describe('PUT', () => {
    it('updates a list of model runs', async () => {
      const res = await request.put('/model_runs').send(modelRunsFixture);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ message: 'OK' });
      expect(modelRunsModel.updateModelRuns).to.have.been.calledOnce();
      expect(modelRunsModel.updateModelRuns).to.have.been.calledWithExactly(modelRunsFixture);
    });
  });
};

export default modelRunsSpec;
