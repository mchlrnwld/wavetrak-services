import nr from 'newrelic';
import db from '../../db';

const table = 'model_grids';

// eslint-disable-next-line import/prefer-default-export
export const search = ({ agency, model, grid }) =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client } = db.replica;
    const whereEqualsPredicate: any = {};

    if (agency) {
      whereEqualsPredicate.agency = agency;
    }

    if (model) {
      whereEqualsPredicate.model = model;
    }

    if (grid) {
      whereEqualsPredicate.grid = grid;
    }

    const query = client(table)
      .select(
        'agency',
        'model',
        'grid',
        'description',
        'start_latitude',
        'end_latitude',
        'start_longitude',
        'end_longitude',
        'resolution',
      )
      .where(whereEqualsPredicate);

    return query;
  });
