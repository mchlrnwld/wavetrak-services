export interface ModelGrid {
  agency: string;
  model: string;
  grid: string;
  description: string;
  resolution: number | null;
  startLatitude: number | null;
  endLatitude: number | null;
  startLongitude: number | null;
  endLongitude: number | null;
}
