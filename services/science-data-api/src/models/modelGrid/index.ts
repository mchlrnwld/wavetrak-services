import { search } from './queries';

// eslint-disable-next-line import/prefer-default-export
export const queryModelGrids = async params => {
  const modelGrids = await search(params);
  return modelGrids;
};
