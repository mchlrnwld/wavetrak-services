import round from 'lodash/round';
import { convertUnits } from '../../common/units';

interface SwellsEnsembleUnits {
  waveHeight: string;
  windSpeed: string;
}

interface SwellsEnsembleRecord {
  timestamp: number;
  member: number;
  windDirection: number;
  windSpeed: number;
  windWaveDirection: number;
  windWaveHeight: number;
  windWavePeriod: number;
  windAndSwellHeight: number;
  primaryWaveDirection: number;
  primaryWavePeriod: number;
  swellWave1Direction: number;
  swellWave1Height: number;
  swellWave1Period: number;
  swellWave2Direction: number;
  swellWave2Height: number;
  swellWave2Period: number;
}

const nullValueCheck = (value: number, transform: Function): number =>
  value === 9999 ? null : transform(value);
const roundTo5 = (value: number): number => round(value, 5);

const transformSwellsEnsemble = (
  swellsEnsembleRecord: SwellsEnsembleRecord,
  inputUnits: SwellsEnsembleUnits,
  outputUnits: SwellsEnsembleUnits
) => {
  const { timestamp, member } = swellsEnsembleRecord;
  const convertWaveHeight = convertUnits(inputUnits.waveHeight, outputUnits.waveHeight);
  const convertWindSpeed = convertUnits(inputUnits.windSpeed, outputUnits.windSpeed);

  return {
    timestamp,
    member,
    wind: {
      direction: nullValueCheck(swellsEnsembleRecord.windDirection, roundTo5),
      speed: nullValueCheck(swellsEnsembleRecord.windSpeed, v => roundTo5(convertWindSpeed(v))),
    },
    swells: {
      combined: {
        height: nullValueCheck(swellsEnsembleRecord.windAndSwellHeight, v =>
          roundTo5(convertWaveHeight(v))
        ),
        direction: nullValueCheck(swellsEnsembleRecord.primaryWaveDirection, roundTo5),
        period: nullValueCheck(swellsEnsembleRecord.primaryWavePeriod, roundTo5),
      },
      components: [...Array(3)].map((_, i) => {
        const prefix = i < 2 ? `swellWave${i + 1}` : 'windWave';
        return {
          height: nullValueCheck(swellsEnsembleRecord[`${prefix}Height`], v =>
            roundTo5(convertWaveHeight(v))
          ),
          direction: nullValueCheck(swellsEnsembleRecord[`${prefix}Direction`], roundTo5),
          period: nullValueCheck(swellsEnsembleRecord[`${prefix}Period`], roundTo5),
        };
      }),
    },
  };
};

export default transformSwellsEnsemble;
