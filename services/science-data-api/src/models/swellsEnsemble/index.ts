import nr from 'newrelic';
import { getRecords } from '../../external/smdb';
import transformSwellsEnsemble from './transformSwellsEnsemble';
import models from '../../stores/models';

// eslint-disable-next-line import/prefer-default-export
export const getSwellsEnsemble = (
  { agency, model, run, grid: { key: grid, lat, lon } },
  start,
  end,
  units
) => nr.startSegment('smdb:ensemble', true, async () => {
  const records = await getRecords({ agency, model, run, grid, lat, lon, start, end });
  const vals = records.map(record =>
    transformSwellsEnsemble(record, models[agency][model].storedUnits, units)
  );
  return vals;
});
