import { expect } from 'chai';
import transformSwellsEnsemble from './transformSwellsEnsemble';

const transformSwellsEnsembleSpec = () => {
  describe('transformSwellsEnsemble', () => {
    it('converts wave height and wind speed units', () => {
      const swellsEnsemble = {
        timestamp: 1566367200,
        member: 1,
        windDirection: 202.33,
        windSpeed: 5.45,
        windWaveHeight: 0.06,
        windWaveDirection: 159.14,
        windWavePeriod: 13.4,
        windAndSwellHeight: 1.26,
        primaryWaveDirection: 207.49,
        primaryWavePeriod: 13.05,
        swellWave1Height: 1.05,
        swellWave1Direction: 174.88,
        swellWave1Period: 8.08,
        swellWave2Height: 0.73,
        swellWave2Direction: 205.55,
        swellWave2Period: 11.54,
      };
      const inputUnits = { waveHeight: 'm', windSpeed: 'm/s' };
      const outputUnits = { waveHeight: 'ft', windSpeed: 'km/h' };
      expect(transformSwellsEnsemble(swellsEnsemble, inputUnits, outputUnits)).to.deep.equal({
        timestamp: 1566367200,
        member: 1,
        wind: {
          direction: 202.33,
          speed: 19.62,
        },
        swells: {
          combined: {
            height: 4.13386,
            direction: 207.49,
            period: 13.05,
          },
          components: [
            {
              height: 3.44488,
              direction: 174.88,
              period: 8.08,
            },
            {
              height: 2.39501,
              direction: 205.55,
              period: 11.54,
            },
            {
              height: 0.19685,
              direction: 159.14,
              period: 13.4,
            },
          ],
        },
      });
    });

    it('converts 9999 values to null', () => {
      const swellsEnsemble = {
        timestamp: 1566367200,
        member: 1,
        windDirection: 9999,
        windSpeed: 9999,
        windWaveHeight: 9999,
        windWaveDirection: 9999,
        windWavePeriod: 9999,
        windAndSwellHeight: 9999,
        primaryWaveDirection: 9999,
        primaryWavePeriod: 9999,
        swellWave1Height: 9999,
        swellWave1Direction: 9999,
        swellWave1Period: 9999,
        swellWave2Height: 9999,
        swellWave2Direction: 9999,
        swellWave2Period: 9999,
      };
      const inputUnits = { waveHeight: 'm', windSpeed: 'm/s' };
      const outputUnits = { waveHeight: 'ft', windSpeed: 'km/h' };
      expect(transformSwellsEnsemble(swellsEnsemble, inputUnits, outputUnits)).to.deep.equal({
        timestamp: 1566367200,
        member: 1,
        wind: {
          direction: null,
          speed: null,
        },
        swells: {
          combined: {
            height: null,
            direction: null,
            period: null,
          },
          components: [
            {
              height: null,
              direction: null,
              period: null,
            },
            {
              height: null,
              direction: null,
              period: null,
            },
            {
              height: null,
              direction: null,
              period: null,
            },
          ],
        },
      });
    });
  });
};

export default transformSwellsEnsembleSpec;
