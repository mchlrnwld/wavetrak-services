import round from 'lodash/round';
import isNumber from 'lodash/isNumber';
import { convertUnits } from '../../common/units';
import { SurfUnits, SurfRecord } from './types';

const nullValueCheck = (value: number, transform: Function): number =>
  !isNumber(value) || value === 9999 ? null : transform(value);
const roundTo5 = (value: number): number => round(value, 5);

const transformSurf = (surfRecord: SurfRecord, inputUnits: SurfUnits, outputUnits: SurfUnits) => {
  const { breakingWaveHeightMin, breakingWaveHeightMax, breakingWaveHeightAlgorithm } = surfRecord;

  const convertWaveHeight = convertUnits(inputUnits.waveHeight, outputUnits.waveHeight);

  return {
    breakingWaveHeightMin: nullValueCheck(breakingWaveHeightMin, v =>
      roundTo5(convertWaveHeight(v)),
    ),
    breakingWaveHeightMax: nullValueCheck(breakingWaveHeightMax, v =>
      roundTo5(convertWaveHeight(v)),
    ),
    breakingWaveHeightAlgorithm: breakingWaveHeightAlgorithm,
  };
};

export default transformSurf;
