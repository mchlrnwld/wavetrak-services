export interface SurfUnits {
  waveHeight: string;
}

export interface Surf {
  breakingWaveHeightMin?: number;
  breakingWaveHeightMax?: number;
  breakingWaveHeightAlgorithm?: string;
}

export interface SurfRecord extends Surf {
  forecastTime: Date;
  run: number;
}

export interface SurfForecast {
  timestamp: number;
  run: number;
  surf: Surf;
}
