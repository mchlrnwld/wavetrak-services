import { expect } from 'chai';
import sinon from 'sinon';
import modelRunsFixture from '../../fixtures/modelRuns';
import * as queries from './queries';
import { runTimeToDate } from '../../db/runTimeConversions';
import { queryModelRuns, updateModelRuns } from '.';

describe('models / modelRun', () => {
  describe('queryModelRuns', () => {
    before(() => {
      sinon.stub(queries, 'search').resolves(
        modelRunsFixture.map(modelRun => ({
          ...modelRun,
          run: runTimeToDate(modelRun.run),
        })),
      );
    });

    afterEach(() => {
      queries.search.resetHistory();
    });

    after(() => {
      queries.search.restore();
    });

    it('searches model runs by query params', async () => {
      const params = { statuses: ['ONLINE', 'PENDING'] };
      const result = await queryModelRuns(params);
      expect(result).to.deep.equal(modelRunsFixture);
      expect(queries.search).to.have.been.calledOnce();
      expect(queries.search).to.have.been.calledWithExactly(params);
    });
  });

  describe('updateModelRuns', () => {
    before(() => {
      sinon.stub(queries, 'upsert');
    });

    afterEach(() => {
      queries.upsert.resetHistory();
    });

    after(() => {
      queries.upsert.restore();
    });

    it('upserts a list of model runs', async () => {
      await updateModelRuns(modelRunsFixture);
      expect(queries.upsert).to.have.been.calledOnce();
      expect(queries.upsert).to.have.been.calledWithExactly(
        modelRunsFixture.map(run => ({ ...run, type: 'POINT_OF_INTEREST' })),
      );
    });

    it('defaults status to PENDING', async () => {
      const modelRun = {
        agency: 'NOAA',
        model: 'GFS',
        run: 2018113006,
        type: 'POINT_OF_INTEREST',
      };
      await updateModelRuns([modelRun]);
      expect(queries.upsert).to.have.been.calledOnce();
      expect(queries.upsert).to.have.been.calledWithExactly([
        {
          ...modelRun,
          status: 'PENDING',
          type: 'POINT_OF_INTEREST',
        },
      ]);
    });

    it('validates model runs before upserting', async () => {
      try {
        await updateModelRuns([{}]);
        throw new Error('Validation error expected');
      } catch (err) {
        expect(queries.upsert).not.to.have.been.called();
        expect(err.name).to.equal('ValidationError');
        expect(err).to.have.status(400);
        expect(err.body.errors).to.be.ok();
        expect(err.body.errors[0]).to.be.ok();
      }
    });
  });
});
