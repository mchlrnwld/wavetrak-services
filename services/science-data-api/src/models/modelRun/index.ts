import joi from 'joi';
import { createValidationError } from '../../common/errors/ValidationError';
import { formatRunTime } from '../../db/runTimeConversions';
// eslint-disable-next-line no-unused-vars
import { search, upsert, ModelRunQuery, ModelRunUpsertBody } from './queries';
import schema from './schema';

const validate = modelRun => joi.validate(modelRun, schema, { abortEarly: false });

const parseModelRun = modelRun => ({
  ...modelRun,
  run: formatRunTime(modelRun.run),
});

export const queryModelRuns = async (params: ModelRunQuery) => {
  const modelRuns = await search(params);
  return modelRuns.map(parseModelRun);
};

export const updateModelRuns = (modelRuns: Array<ModelRunUpsertBody>) => {
  const validationResults = modelRuns.map(validate);
  const errors = validationResults.map(({ error }) => error).filter(error => error);
  if (errors.length) {
    throw createValidationError(
      'Error validating model runs',
      errors.map(({ details }) => details.map(({ message }) => message)),
    );
  }
  const result = upsert(validationResults.map(({ value }) => value));
  return result;
};
