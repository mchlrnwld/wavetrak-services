import joi from 'joi';

export const agency = joi.string().max(25);
export const model = joi.string().max(25);
export const run = joi
  .number()
  .min(1970010100)
  .max(9999123123);
export const status = joi.string().valid('ONLINE', 'PENDING', 'OFFLINE');
export const type = joi.string().valid('POINT_OF_INTEREST', 'FULL_GRID');

const schema = joi.object().keys({
  agency: agency.required(),
  model: model.required(),
  run: run.required(),
  status: status.default('PENDING'),
  type: type.required(),
});

export default schema;
