import nr from 'newrelic';
import db from '../../db';
import { runTimeToISOString } from '../../db/runTimeConversions';

const tables = {
  FULL_GRID: 'model_runs_full_grid',
  POINT_OF_INTEREST: 'model_runs_point_of_interest',
};

export interface ModelRunQuery {
  agency?: string;
  model?: string;
  run?: string | number; // Format yyyymmddhh
  statuses?: Array<string>;
  types?: Array<string>;
}

export interface ModelRunUpsertBody {
  agency: string;
  model: string;
  run: string | number; // Format yyyymmddhh
  status: string;
  type: string;
}

export const search = async ({ agency, model, run, statuses, types }: ModelRunQuery) =>
  nr.startSegment('select:model_runs', false, async () => {
    const { client } = db.replica;
    const whereEqualsPredicate: any = {};

    if (agency) {
      whereEqualsPredicate.agency = agency;
    }

    if (model) {
      whereEqualsPredicate.model = model;
    }

    if (run) {
      whereEqualsPredicate.run = runTimeToISOString(run);
    }

    const results = await Promise.all(
      (types || Object.keys(tables)).map(async type => {
        if (!tables[type]) {
          throw new Error('Unknown model run type.');
        }
        let query = client(tables[type])
          .select('agency', 'model', 'run', 'status')
          .where(whereEqualsPredicate)
          .orderBy('run', 'DESC');

        if (statuses) {
          query = query.andWhere('status', 'in', statuses);
        }

        return (await query).map(row => ({ ...row, type }));
      }),
    );

    // Flatten results
    return [].concat(...results);
  });

export const upsert = (modelRuns: Array<ModelRunUpsertBody>) =>
  db.source.client.transaction(trx =>
    Promise.all(
      modelRuns.map(modelRun => {
        const insertQuery = trx(tables[modelRun.type])
          .insert({
            agency: modelRun.agency,
            model: modelRun.model,
            run: runTimeToISOString(modelRun.run),
            status: modelRun.status,
          })
          .toString();

        const updateQuery = trx
          .update({
            status: modelRun.status,
          })
          .toString();

        const upsertQuery = `${insertQuery} ON CONFLICT (agency, model, run) DO ${updateQuery}`;
        return trx.raw(upsertQuery);
      }),
    ),
  );
