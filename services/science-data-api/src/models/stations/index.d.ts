export interface BoundingBox {
  east: number;
  north: number;
  south: number;
  west: number;
}

export interface Point {
  latitude: number;
  longitude: number;
}

export enum StationStatus {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}

export interface StationUnits {
  waveHeight: string;
  windSpeed: string;
  waterTemperature: string;
  airTemperature: string;
  dewPoint: string;
  pressure: string;
}

export interface StationInfoRecord {
  id: string;
  source: string;
  sourceId: string;
  status: StationStatus;
  name: string;
  latitude: number;
  longitude: number;
  latestTimestamp: number;
}

export interface RawStationDataRecord {
  timestamp: number;

  significantHeight: number;
  peakPeriod: number;
  meanWaveDirection: number;
  waterTemperature: number;
  airTemperature: number;

  windSpeed: number;
  windGust: number;
  windDirection: number;

  pressure: number;
  dewPoint: number;

  swellWave_1Height: number;
  swellWave_1Direction: number;
  swellWave_1Period: number;
  swellWave_1Impact: number;

  swellWave_2Height: number;
  swellWave_2Direction: number;
  swellWave_2Period: number;
  swellWave_2Impact: number;

  swellWave_3Height: number;
  swellWave_3Direction: number;
  swellWave_3Period: number;
  swellWave_3Impact: number;

  swellWave_4Height: number;
  swellWave_4Direction: number;
  swellWave_4Period: number;
  swellWave_4Impact: number;

  swellWave_5Height: number;
  swellWave_5Direction: number;
  swellWave_5Period: number;
  swellWave_5Impact: number;

  swellWave_6Height: number;
  swellWave_6Direction: number;
  swellWave_6Period: number;
  swellWave_6Impact: number;
}

export interface StationDataRecord {
  timestamp: number;

  height: number;
  period: number;
  direction: number;
  waterTemperature: number;
  airTemperature: number;

  wind: { speed: number; gust: number; direction: number } | null;

  pressure: number;
  dewPoint: number;

  swells: { height: number; direction: number; period: number; impact: number }[];
}
