import { expect } from 'chai';
import transformStationData from './transformStationData';
import stations from '../../stores/stations';

const transformStationDataSpec = () => {
  describe('transformStationData', () => {
    it('converts station data records into expected response shape', () => {
      const stationData = [
        {
          timestamp: 0,
          significantHeight: 1.32,
          peakPeriod: 11,
          meanWaveDirection: 200,
          waterTemperature: 1,
          airTemperature: 2,
          windSpeed: 4,
          windDirection: 50,
          windGust: 2,
          pressure: 9.0,
          dewPoint: 1,
          swellWave_1Height: 1.05,
          swellWave_1Direction: 174.88,
          swellWave_1Period: 8,
          swellWave_1Impact: 0.5,
          swellWave_2Height: 0.73,
          swellWave_2Direction: 205.55,
          swellWave_2Period: 12,
          swellWave_2Impact: 0.2,
          swellWave_3Height: 0.27,
          swellWave_3Direction: 205.63,
          swellWave_3Period: 16,
          swellWave_3Impact: 0.1,
          swellWave_4Height: 0.06,
          swellWave_4Direction: 207.87,
          swellWave_4Period: 21,
          swellWave_4Impact: 0.0,
          swellWave_5Height: 0.06,
          swellWave_5Direction: 159.14,
          swellWave_5Period: 13,
          swellWave_5Impact: 0.0,
          swellWave_6Height: 0.06,
          swellWave_6Direction: 159.14,
          swellWave_6Period: 13,
          swellWave_6Impact: 0.0,
        },
      ];
      expect(
        transformStationData(stationData, stations.storedUnits, stations.storedUnits)[0],
      ).to.deep.equal({
        timestamp: 0,
        height: 1.3,
        period: 11,
        direction: 200,
        waterTemperature: 1,
        airTemperature: 2,
        wind: {
          speed: 4,
          direction: 50,
          gust: 2,
        },
        pressure: 9,
        dewPoint: 1,
        swells: [
          {
            direction: 175,
            height: 1.1,
            period: 8,
            impact: 0.5,
          },
          {
            direction: 206,
            height: 0.7,
            period: 12,
            impact: 0.2,
          },
          {
            direction: 206,
            height: 0.3,
            period: 16,
            impact: 0.1,
          },
          {
            direction: 208,
            height: 0.1,
            period: 21,
            impact: 0,
          },
          {
            direction: 159,
            height: 0.1,
            period: 13,
            impact: 0,
          },
          {
            direction: 159,
            height: 0.1,
            period: 13,
            impact: 0,
          },
        ],
      });
    });

    it('returns null for wind when windSpeed or windDirection are null', () => {
      const stationData = [
        {
          stationId: '123-34-43',
          timestamp: 0,
          significantHeight: 1.32,
          peakPeriod: 11,
          meanWaveDirection: 200,
          waterTemperature: 1,
          airTemperature: 2,
          windSpeed: null,
          windDirection: 240,
          windGust: 2,
          pressure: 9.0,
          dewPoint: 1,
          swellWave_1Height: 1.05,
          swellWave_1Direction: 174.88,
          swellWave_1Period: 8,
          swellWave_1Impact: 0.5,
          swellWave_2Height: 0.73,
          swellWave_2Direction: 205.55,
          swellWave_2Period: 12,
          swellWave_2Impact: 0.2,
          swellWave_3Height: 0.27,
          swellWave_3Direction: 205.63,
          swellWave_3Period: 16,
          swellWave_3Impact: 0.1,
          swellWave_4Height: 0.06,
          swellWave_4Direction: 207.87,
          swellWave_4Period: 21,
          swellWave_4Impact: 0.0,
          swellWave_5Height: 0.06,
          swellWave_5Direction: 159.14,
          swellWave_5Period: 13,
          swellWave_5Impact: 0.0,
          swellWave_6Height: 0.06,
          swellWave_6Direction: 159.14,
          swellWave_6Period: 13,
          swellWave_6Impact: 0.0,
        },
      ];
      expect(
        transformStationData(stationData, stations.storedUnits, stations.storedUnits)[0],
      ).to.deep.equal({
        timestamp: 0,
        height: 1.3,
        period: 11,
        direction: 200,
        waterTemperature: stationData[0].waterTemperature,
        airTemperature: 2,
        wind: null,
        pressure: 9,
        dewPoint: 1,
        swells: [
          {
            direction: 175,
            height: 1.1,
            period: 8,
            impact: 0.5,
          },
          {
            direction: 206,
            height: 0.7,
            period: 12,
            impact: 0.2,
          },
          {
            direction: 206,
            height: 0.3,
            period: 16,
            impact: 0.1,
          },
          {
            direction: 208,
            height: 0.1,
            period: 21,
            impact: 0,
          },
          {
            direction: 159,
            height: 0.1,
            period: 13,
            impact: 0,
          },
          {
            direction: 159,
            height: 0.1,
            period: 13,
            impact: 0,
          },
        ],
      });
    });

    it('converts and rounds data to the desired units', () => {
      const desiredUnits = {
        waveHeight: 'ft',
        windSpeed: 'mi/h',
        waterTemperature: 'F',
        airTemperature: 'F',
        dewPoint: 'F',
        pressure: 'mb',
      };
      const stationData = [
        {
          stationId: '123-34-43',
          timestamp: 0,
          significantHeight: 1.32,
          peakPeriod: 11.11,
          meanWaveDirection: 200.11,
          waterTemperature: 1.11,
          airTemperature: 2.11,
          windSpeed: 1.11,
          windDirection: 240.11,
          windGust: 2.11,
          pressure: 9.01,
          dewPoint: 1.11,
          swellWave_1Height: 1.05,
          swellWave_1Direction: 174.88,
          swellWave_1Period: 8.11,
          swellWave_1Impact: 0.5,
          swellWave_2Height: 0.73,
          swellWave_2Direction: 205.55,
          swellWave_2Period: 12.11,
          swellWave_2Impact: 0.2,
          swellWave_3Height: 0.27,
          swellWave_3Direction: 205.63,
          swellWave_3Period: 16.11,
          swellWave_3Impact: 0.1,
          swellWave_4Height: 0.06,
          swellWave_4Direction: 207.87,
          swellWave_4Period: 21.11,
          swellWave_4Impact: 0.0,
          swellWave_5Height: 0.06,
          swellWave_5Direction: 159.14,
          swellWave_5Period: 13.11,
          swellWave_5Impact: 0.0,
          swellWave_6Height: 0.06,
          swellWave_6Direction: 159.14,
          swellWave_6Period: 13.11,
          swellWave_6Impact: 0.0,
        },
      ];
      const transformedData = transformStationData(
        stationData,
        stations.storedUnits,
        desiredUnits,
      )[0];
      expect(transformedData.height).to.be.closeTo(4.3, 0.00001);
      expect(transformedData.period).to.be.closeTo(11, 0.00001);
      expect(transformedData.direction).to.be.closeTo(200, 0.00001);
      expect(transformedData.waterTemperature).to.be.closeTo(34, 0.00001);
      expect(transformedData.airTemperature).to.be.closeTo(36, 0.00001);
      expect(transformedData.pressure).to.be.closeTo(9.01, 0.00001);
      expect(transformedData.dewPoint).to.be.closeTo(34, 0.00001);
      expect(transformedData.wind.speed).to.be.closeTo(2, 0.00001);
      expect(transformedData.wind.direction).to.be.closeTo(240, 0.00001);
      expect(transformedData.wind.gust).to.be.closeTo(5, 0.00001);
      expect(transformedData.swells[0].height).to.be.closeTo(3.4, 0.00001);
      expect(transformedData.swells[0].period).to.be.closeTo(8, 0.00001);
      expect(transformedData.swells[0].direction).to.be.closeTo(175, 0.00001);
      expect(transformedData.swells[0].impact).to.be.closeTo(0.5, 0.00001);
    });

    it('handles null data for each data point properly', () => {
      const desiredUnits = {
        waveHeight: 'ft',
        windSpeed: 'mi/h',
        waterTemperature: 'F',
        airTemperature: 'F',
        dewPoint: 'F',
        pressure: 'mb',
      };
      const stationData = [
        {
          stationId: '123-34-43',
          timestamp: 0,
          significantHeight: 1.32,
          peakPeriod: 11.11,
          meanWaveDirection: null,
          waterTemperature: null,
          airTemperature: null,
          windSpeed: 4,
          windDirection: 50,
          windGust: null,
          pressure: null,
          dewPoint: null,
          swellWave_1Height: null,
          swellWave_1Direction: null,
          swellWave_1Period: null,
          swellWave_1Impact: null,
          swellWave_2Height: null,
          swellWave_2Direction: null,
          swellWave_2Period: null,
          swellWave_2Impact: null,
          swellWave_3Height: null,
          swellWave_3Direction: null,
          swellWave_3Period: null,
          swellWave_3Impact: null,
          swellWave_4Height: null,
          swellWave_4Direction: null,
          swellWave_4Period: null,
          swellWave_4Impact: null,
          swellWave_5Height: null,
          swellWave_5Direction: null,
          swellWave_5Period: null,
          swellWave_5Impact: null,
          swellWave_6Height: null,
          swellWave_6Direction: null,
          swellWave_6Period: null,
          swellWave_6Impact: null,
        },
      ];
      const transformedData = transformStationData(
        stationData,
        stations.storedUnits,
        desiredUnits,
      )[0];
      expect(transformedData.height).to.be.closeTo(4.3, 0.00001);
      expect(transformedData.period).to.be.closeTo(11, 0.00001);
      expect(transformedData.direction).to.be.equal(null);
      expect(transformedData.waterTemperature).to.be.equal(null);
      expect(transformedData.airTemperature).to.be.equal(null);
      expect(transformedData.pressure).to.be.equal(null);
      expect(transformedData.dewPoint).to.be.equal(null);
      expect(transformedData.wind.speed).to.be.closeTo(9, 0.00001);
      expect(transformedData.wind.direction).to.be.closeTo(50, 0.00001);
      expect(transformedData.wind.gust).to.be.equal(null);
      expect(transformedData.swells[0].height).to.be.equal(null);
      expect(transformedData.swells[0].period).to.be.equal(null);
      expect(transformedData.swells[0].direction).to.be.equal(null);
      expect(transformedData.swells[0].impact).to.be.equal(null);
    });
  });
};

export default transformStationDataSpec;
