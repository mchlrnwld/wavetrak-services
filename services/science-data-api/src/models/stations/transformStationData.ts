import { isNumber, round } from 'lodash';
import { convertUnits } from '../../common/units';
import { StationDataRecord, StationUnits, RawStationDataRecord } from './index.d';

const transformStationData = (
  stationData: RawStationDataRecord[],
  inputUnits: StationUnits,
  outputUnits: StationUnits,
): StationDataRecord[] =>
  stationData.map(data => ({
    timestamp: data.timestamp / 1000,
    waterTemperature: isNumber(data.waterTemperature) ? round(
      convertUnits(
        inputUnits.waterTemperature,
        outputUnits.waterTemperature,
        data.waterTemperature,
      ),
      0,
    ) : null,
    airTemperature: isNumber(data.airTemperature) ? round(
      convertUnits(inputUnits.airTemperature, outputUnits.airTemperature, data.airTemperature),
      0,
    ) : null,
    dewPoint: isNumber(data.dewPoint) ? round(
      convertUnits(inputUnits.dewPoint, outputUnits.dewPoint, data.dewPoint),
      0,
    ) : null,
    pressure: isNumber(data.pressure) ? round(
      convertUnits(outputUnits.pressure, outputUnits.pressure, data.pressure),
      2,
    ) : null,
    height: round(
      convertUnits(inputUnits.waveHeight, outputUnits.waveHeight, data.significantHeight),
      1,
    ),
    period: round(data.peakPeriod, 0),
    direction: data.meanWaveDirection ? round(data.meanWaveDirection, 0) : null,
    wind:
      data.windSpeed && data.windDirection
        ? {
            speed: round(
              convertUnits(inputUnits.windSpeed, outputUnits.windSpeed, data.windSpeed),
              0,
            ),
            gust: data.windGust ? round(
              convertUnits(inputUnits.windSpeed, outputUnits.windSpeed, data.windGust),
              0,
            ) : null,
            direction: round(data.windDirection, 0),
          }
        : null,
    swells: [...Array(6)].map((_, i) => ({
      height: isNumber(data[`swellWave_${i + 1}Height`]) ? round(
        convertUnits(
          inputUnits.waveHeight,
          outputUnits.waveHeight,
          data[`swellWave_${i + 1}Height`],
        ),
        1,
      ) : null,
      direction: isNumber(data[`swellWave_${i + 1}Direction`]) ? round(data[`swellWave_${i + 1}Direction`], 0) : null,
      period: isNumber(data[`swellWave_${i + 1}Period`]) ? round(data[`swellWave_${i + 1}Period`], 0) : null,
      impact: isNumber(data[`swellWave_${i + 1}Impact`]) ? round(data[`swellWave_${i + 1}Impact`], 5) : null,
    })),
  }));

export default transformStationData;
