import nr from 'newrelic';
import db from '../../db';
import unixTimeToISOString from '../../db/unixTimeToISOString';
import {
  BoundingBox,
  Point,
  RawStationDataRecord,
  StationInfoRecord,
  StationStatus,
} from './index.d';

const dataTable = 'station_data';
const table = 'stations';

export const getData = (
  id: string,
  start: number,
  end: number,
  limit: number,
): Promise<RawStationDataRecord[]> =>
  nr.startSegment(`select:${dataTable}`, false, () => {
    const { client } = db.replica;

    const whereEqualsPredicate: any = { stationId: id };

    const fields = [
      'timestamp',
      'significant_height',
      'peak_period',
      'mean_wave_direction',
      'water_temperature',
      'air_temperature',
      'wind_speed',
      'wind_direction',
      'wind_gust',
      'pressure',
      'dew_point',
      'elevation',
      'swell_wave_1_height',
      'swell_wave_1_period',
      'swell_wave_1_direction',
      'swell_wave_1_impact',
      'swell_wave_2_height',
      'swell_wave_2_period',
      'swell_wave_2_direction',
      'swell_wave_2_impact',
      'swell_wave_3_height',
      'swell_wave_3_period',
      'swell_wave_3_direction',
      'swell_wave_3_impact',
      'swell_wave_4_height',
      'swell_wave_4_period',
      'swell_wave_4_direction',
      'swell_wave_4_impact',
      'swell_wave_5_height',
      'swell_wave_5_period',
      'swell_wave_5_direction',
      'swell_wave_5_impact',
      'swell_wave_6_height',
      'swell_wave_6_period',
      'swell_wave_6_direction',
      'swell_wave_6_impact',
    ];

    let query = client(dataTable)
      .select(fields)
      .where(whereEqualsPredicate)
      .orderBy('timestamp', 'desc');

    if (limit) {
      return query.limit(limit);
    }

    if (start) {
      query = query.andWhere('timestamp', '>=', unixTimeToISOString(start));
    }

    if (end) {
      query = query.andWhere('timestamp', '<=', unixTimeToISOString(end));
    }

    return query;
  });

export const getInBoundingBox = (
  { east, north, south, west }: BoundingBox,
  status?: StationStatus,
): Promise<StationInfoRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client, st } = db.replica;

    const fields = [
      'id',
      'source',
      'source_id',
      'status',
      'name',
      'latitude',
      'longitude',
      'latest_timestamp',
    ];

    // account for a request that wraps around the globe the long way
    const queryWest = west > east ? west - 360 : west;

    let query = client(table)
      .select(fields)
      .where(builder =>
        builder
          .where(
            st.intersects(st.makeEnvelope(queryWest, south, east, north, 4326), 'geometry_point'),
          )
          .orWhere(
            st.intersects(
              st.makeEnvelope(queryWest - 360, south, east - 360, north, 4326),
              'geometry_point',
            ),
          ),
      );

    if (status) query = query.andWhere('status', '=', status);

    return query;
  });

export const getInfo = (id: string): Promise<StationInfoRecord> =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client } = db.replica;

    const whereEqualsPredicate: any = { id };

    const fields = [
      'id',
      'source',
      'source_id',
      'status',
      'name',
      'latitude',
      'longitude',
      'latest_timestamp',
    ];

    return client(table)
      .first(fields)
      .where(whereEqualsPredicate);
  });

/**
 * Run PostGIS SQL query to get stations within a distance (kilometers) from a point
 * @param point geographic point
 * @param distance distance in kilometers
 * @param status optional status value to filter stations
 * @param limit max number of stations to return
 */
export const getWithinDistance = (
  point: Point,
  distance: number,
  status: StationStatus,
  limit: number,
): Promise<StationInfoRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client, st } = db.replica;

    const fields = [
      'id',
      'source',
      'source_id',
      'status',
      'name',
      'latitude',
      'longitude',
      'latest_timestamp',
    ];

    const postGisPoint = st.point(point.longitude, point.latitude);

    let query = client(table)
      .select(fields)
      .where(st.dwithin(st.setSRID(postGisPoint, 4326), 'geography_point', distance * 1000))
      .orderBy(st.distance(postGisPoint, 'geography_point'))
      .limit(limit);

    if (status) query = query.andWhere('status', '=', status);

    return query;
  });
