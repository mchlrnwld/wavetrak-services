import { getData, getInBoundingBox, getInfo, getWithinDistance } from './queries';
import transformStationData from './transformStationData';
import stations from '../../stores/stations';
import {
  BoundingBox,
  Point,
  StationDataRecord,
  StationInfoRecord,
  StationStatus,
  StationUnits,
} from './index.d';

export async function getSingleStationData({
  id,
  start,
  end,
  limit,
  units,
}: {
  id: string;
  start?: number;
  end?: number;
  limit?: number;
  units: StationUnits;
}): Promise<StationDataRecord[] | null> {
  const data = await getData(id, start, end, limit);
  return data ? transformStationData(data, stations.storedUnits, units) : null;
}

export const getStationsInBoundingBox = async (
  box: BoundingBox,
  status: StationStatus,
): Promise<StationInfoRecord[]> => {
  const stations = await getInBoundingBox(box, status);
  return stations.map(s => ({ ...s, latestTimestamp: s.latestTimestamp / 1000 }));
};

export const getStationsWithinDistance = async (
  point: Point,
  distance: number,
  status: StationStatus,
  limit = 4,
): Promise<StationInfoRecord[]> => {
  const stations = await getWithinDistance(point, distance, status, limit);
  return stations.map(s => ({ ...s, latestTimestamp: s.latestTimestamp / 1000 }));
};

export const getStationInfo = async (id: string): Promise<StationInfoRecord | null> => {
  const info = await getInfo(id);
  return info? { ...info, latestTimestamp: info.latestTimestamp / 1000 } : null;
};
