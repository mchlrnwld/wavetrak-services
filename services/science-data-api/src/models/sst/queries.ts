import Knex, { QueryBuilder } from 'knex';
import nr from 'newrelic';
import db from '../../db';
import unixTimeToISOString from '../../db/unixTimeToISOString';
import { SeaSurfaceTemperatureRecord } from './types';

const table = 'sst';

// eslint-disable-next-line import/prefer-default-export
export const surfSpotSST = (
  { agency, model, grid, pointOfInterestId, start, end },
  client: Knex = db.replica.client,
  cache = false,
): QueryBuilder<SeaSurfaceTemperatureRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    const whereEqualsPredicate: any = {
      pointOfInterestId,
      agency,
      model,
      grid,
    };

    let query = client(table)
      .select('forecast_time', 'temperature')
      .where(whereEqualsPredicate)
      .orderBy('forecast_time');

    if (start) {
      query = query.andWhere('forecast_time', '>=', unixTimeToISOString(start));
    }

    if (end) {
      query = query.andWhere('forecast_time', '<=', unixTimeToISOString(end));
    }

    return cache ? query.cache() : query;
  });
