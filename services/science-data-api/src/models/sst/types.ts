export interface SeaSurfaceTemperatureRecord {
  forecastTime: number;
  temperature: number;
}

export interface SeaSurfaceTemperature {
  timestamp: number;
  sst: {
    temperature: number;
  };
}
