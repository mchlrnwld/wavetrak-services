export interface SwellProbabilityRecord {
  forecastTime: number;
  run: number;
  probability: number;
}
