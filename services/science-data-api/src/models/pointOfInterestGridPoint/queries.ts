import nr from 'newrelic';
import db from '../../db';

const table = 'points_of_interest_grid_points';

export const find = ({ pointOfInterestId, agency, model, grid, offset, limit }) =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client } = db.replica;
    const whereEqualsPredicate: any = {};

    if (pointOfInterestId) {
      whereEqualsPredicate[`${table}.pointOfInterestId`] = pointOfInterestId;
    }

    if (agency) {
      whereEqualsPredicate[`${table}.agency`] = agency;
    }

    if (model) {
      whereEqualsPredicate[`${table}.model`] = model;
    }

    if (grid) {
      whereEqualsPredicate[`${table}.grid`] = grid;
    }

    let query = client(table)
      .select(
        `${table}.point_of_interest_id`,
        `${table}.agency`,
        `${table}.model`,
        `${table}.grid`,
        `${table}.latitude AS lat`,
        `${table}.longitude AS lon`,
      )
      .where(whereEqualsPredicate)
      .orderBy([`agency`, `model`, `grid`, `point_of_interest_id`]);

    if (limit) {
      query = query.limit(limit);
    }

    if (offset) {
      query = query.offset(offset);
    }

    return query;
  });

export interface POIGridPointData {
  agency: string;
  model: string;
  grid: string;
  lat: number;
  lon: number;
}

export const remove = (
  pointOfInterestId: string,
  gridPoints: Array<{ agency: string; model: string; grid: string }>,
) =>
  db.source.client.transaction(trx =>
    Promise.all(
      gridPoints.map(async gridPt => {
        const clause = {
          pointOfInterestId,
          ...gridPt,
        };
        const count = await trx(table)
          .where(clause)
          .del();
        if (count === 0) {
          throw new Error(`Point-of-interest grid point not found: ${JSON.stringify(clause)}`);
        } else if (count > 1) {
          throw new Error(
            `Multiple point-of-interest grid points matching: ${JSON.stringify(clause)}`,
          );
        }
        return count;
      }),
    ),
  );

// Helper function to construct an "upsert" query. Requires that the target
// table has primary key ("point_of_interest_id", "agency", "model", "grid").
const doUpsert = (trx, tbl, pkFields, dataFields) => {
  // knex treats `null` and `undefined` differently. In particular, the update part will
  // set to NULL for nulls in the JS object, but will ignore entries that are set to
  // undefined in the object. But in this case we want both to set to NULL, so update
  // the dataFields object.
  const data = { ...dataFields };
  Object.keys(data).forEach(key => {
    if (data[key] === undefined) {
      data[key] = null;
    }
  });
  const insertQuery = trx(tbl)
    .insert({ ...pkFields, ...data })
    .toSQL();
  const updateQuery = trx.update({ ...data }).toSQL();
  const upsertQuery = `${insertQuery.sql} ON CONFLICT (point_of_interest_id, agency, model, grid) DO ${updateQuery.sql}`;
  const bindings = insertQuery.bindings.concat(updateQuery.bindings);
  return trx.raw(upsertQuery, bindings);
};

const upsertOne = async (
  trx,
  pointOfInterestId: string,
  { agency, model, grid, lat, lon }: POIGridPointData,
) => {
  const pkData = {
    pointOfInterestId,
    agency,
    model,
    grid,
  };
  await doUpsert(trx, table, pkData, {
    latitude: lat,
    longitude: lon,
  });
};

export const upsert = async (pointOfInterestId: string, gridPoints: Array<POIGridPointData>) =>
  db.source.client.transaction(trx =>
    Promise.all(gridPoints.map(gridPt => upsertOne(trx, pointOfInterestId, gridPt))),
  );
