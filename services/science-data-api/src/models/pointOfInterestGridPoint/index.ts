import { find, remove, upsert } from './queries';

export { POIGridPointData } from './queries';

export const queryPointsOfInterestGridPoints = params => find(params);

export const removePointOfInterestGridPoints = (id, data) => {
  return remove(id, data);
};

export const upsertPointsOfInterestGridPoints = (id, data) => {
  return upsert(id, data);
};
