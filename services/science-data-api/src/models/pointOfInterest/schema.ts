import joi from 'joi';

export const name = joi.string().min(2);

const schema = joi.object().keys({
  name: name.required(),
});

export default schema;
