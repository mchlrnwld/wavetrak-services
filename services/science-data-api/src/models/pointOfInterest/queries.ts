import nr from 'newrelic';
import db from '../../db';

const table = 'points_of_interest';

const formatPOITableRow = ({ id, name, latitude, longitude }) => ({
  pointOfInterestId: id,
  name,
  lat: latitude,
  lon: longitude,
});

export interface POIData {
  name: String;
  lat: Number;
  lon: Number;
}

export interface HasId {
  pointOfInterestId: String;
}

export const find = ({ pointOfInterestId, limit }: { pointOfInterestId: String; limit: Number }) =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client } = db.replica;
    const whereEqualsPredicate: any = {};

    if (pointOfInterestId) {
      whereEqualsPredicate.id = pointOfInterestId;
    }

    const query = client(table)
      .select('id as point_of_interest_id', 'name', 'latitude AS lat', 'longitude AS lon')
      .where(whereEqualsPredicate);

    if (limit) {
      query.limit(limit);
    }

    return query;
  });

export const insert = async ({ name, lat, lon }: POIData): Promise<POIData & HasId> => {
  const { client } = db.source;
  const result = await client(table)
    .insert({ name, latitude: lat, longitude: lon })
    .returning(['id', 'name', 'latitude', 'longitude']);

  if (!result || result.length !== 1) {
    throw new Error('Unknown insert failure');
  }

  return result.map(formatPOITableRow).pop();
};

export const update = async ({
  pointOfInterestId,
  name,
  lat,
  lon,
}: POIData & HasId): Promise<POIData & HasId> => {
  const { client } = db.source;
  const result = await client(table)
    .where('id', pointOfInterestId)
    .update({
      name,
      latitude: lat,
      longitude: lon,
    })
    .returning(['id', 'name', 'latitude', 'longitude']);

  if (!result || result.length !== 1) {
    throw new Error('No records updated. Probably no such Point-of-Interest ID.');
  }

  return result.map(formatPOITableRow).pop();
};
