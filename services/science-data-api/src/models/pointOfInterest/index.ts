import joi from 'joi';
import { createValidationError } from '../../common/errors/ValidationError';
// eslint-disable-next-line no-unused-vars
import { find, insert, update, POIData, HasId } from './queries';
import schema from './schema';

const validate = poi =>
  joi.validate(poi, schema, {
    abortEarly: false,
    allowUnknown: true,
  });

export const queryPointOfInterest = async ({ pointOfInterestId }: HasId) => {
  if (!pointOfInterestId) {
    return null;
  }
  return (await find({ pointOfInterestId, limit: 1 })).pop();
};

const doMutation = async (mutation, data) => {
  const validationResult = validate(data);

  if (validationResult.error) {
    throw createValidationError(
      'Error validating Point of Interest data',
      validationResult.error.details.map(({ message }) => message),
    );
  }

  try {
    const result = await mutation(validationResult.value);
    return result;
  } catch (e) {
    if (e.detail) {
      throw createValidationError(e.message, [e.detail]);
    }
    throw e;
  }
};

export const insertPointOfInterest = async (data: POIData): Promise<POIData & HasId> =>
  doMutation(insert, data);

export const updatePointOfInterest = async (data: POIData & HasId): Promise<POIData & HasId> =>
  doMutation(update, data);
