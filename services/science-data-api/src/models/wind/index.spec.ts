import calculateWindSpec from './calculateWind.spec';
import transformWindSpec from './transformWind.spec';
import transformWindOldSpec from './transformWindOld.spec';
import calculateWindDirectionTypeSpec from './calculateWindDirectionType.spec';

describe('models / wind', () => {
  calculateWindSpec();
  transformWindSpec();
  transformWindOldSpec();
  calculateWindDirectionTypeSpec();
});
