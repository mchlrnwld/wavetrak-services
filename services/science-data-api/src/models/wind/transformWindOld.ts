import round from 'lodash/round';
import calculateWind from './calculateWind';
import { convertUnits } from '../../common/units';
import { WindUnits } from './types';
const roundTo5 = (value: number): number => round(value, 5);

const transformWindOld = (
  { u, v, gust }: { u: number; v: number; gust: number },
  inputUnits: WindUnits,
  outputUnits: WindUnits,
) => {
  const wind = calculateWind(u, v);
  const convertWindSpeed = convertUnits(inputUnits.windSpeed, outputUnits.windSpeed);
  return {
    direction: roundTo5(wind.direction),
    speed: roundTo5(convertWindSpeed(wind.speed)),
    gust: roundTo5(convertWindSpeed(Math.max(wind.speed, gust))),
  };
};

export default transformWindOld;
