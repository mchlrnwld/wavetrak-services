const calculateWindDirectionType = (
  windDirection: number,
  optimumDirection: number,
) => {
  const distance = Math.abs(windDirection - optimumDirection);
  const diff = distance > 180 ? 360 - distance : distance;

  if (diff <= 65) {
    return 'Offshore';
  }
  if (diff <= 115) {
    return 'Cross-shore';
  }
  return 'Onshore';
};

export default calculateWindDirectionType;
