export interface WindUnits {
  windSpeed: string;
}

export interface WindRecord {
  forecastTime: Date;
  run: number;
  u: number;
  v: number;
  gust: number;
}

export interface WindForecast {
  run: number;
  timestamp: number;
  wind: {
    direction: number;
    gust: number;
    speed: number;
  };
}
