const calculateWind = (u, v) => ({
  direction: (180 * Math.atan2(u, v)) / Math.PI + 180,
  speed: Math.sqrt(u ** 2 + v ** 2),
});

export default calculateWind;
