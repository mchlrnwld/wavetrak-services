import { expect } from 'chai';
import calculateWindDirectionType from './calculateWindDirectionType';

const calculateWindDirectionTypeSpec = () => {
  describe('calculateWindDirectionType', () => {
    it('computes the correct wind direction type', () => {
      const wind1 = calculateWindDirectionType(1, 1);
      expect(wind1).to.be.equal('Offshore');

      const wind2 = calculateWindDirectionType(332.42515, 66);
      expect(wind2).to.be.equal('Cross-shore');

      const wind3 = calculateWindDirectionType(170.56373, 66);
      expect(wind3).to.be.equal('Cross-shore');

      const wind4 = calculateWindDirectionType(201.2878, 66);
      expect(wind4).to.be.equal('Onshore');

      const wind5 = calculateWindDirectionType(131.94466, 66);
      expect(wind5).to.be.equal('Cross-shore');

      // Wind direction less than optimum direction
      const wind6 = calculateWindDirectionType(39.72693, 66);
      expect(wind6).to.be.equal('Offshore');
    });
  });
};

export default calculateWindDirectionTypeSpec;
