import Knex, { QueryBuilder } from 'knex';
import nr from 'newrelic';
import db from '../../db';
import { runTimeToUnix, runTimeToISOString } from '../../db/runTimeConversions';
import unixTimeToISOString from '../../db/unixTimeToISOString';
import { WindRecord } from './types';

const table = 'wind';

// eslint-disable-next-line import/prefer-default-export
export const surfSpotWind = (
  { agency, model, run, grid, pointOfInterestId, start, end },
  client: Knex = db.replica.client,
  cache = false,
): QueryBuilder<WindRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    const isoRunTime = runTimeToISOString(run);
    const whereEqualsPredicate: any = { pointOfInterestId, agency, model, grid };

    const fields = ['forecast_time', 'run', 'u', 'v', 'gust'];

    let query = client(table)
      .select(fields)
      .where(whereEqualsPredicate)
      .orderBy('forecast_time');

    if (start) {
      query = query.andWhere('forecast_time', '>=', unixTimeToISOString(start));
    } else {
      query = query.andWhere('forecast_time', '>=', isoRunTime);
    }

    if (end) {
      query = query.andWhere('forecast_time', '<=', unixTimeToISOString(end));
    }

    const unixRunTime = runTimeToUnix(run);

    if (!start || start >= unixRunTime) {
      query = query.andWhere('run', isoRunTime);
      return cache ? query.cache() : query;
    }

    // Start is provided and is less than `run`, so `run` should be considered a max
    // run time allowed, and need the query to allow run times less than the passed
    // `run` for forecast hours prior to `run`. But there may be several such runs
    // available; so use window function to limit to the most up-to-date run for
    // each hour we want.
    query = query
      .andWhere(function() {
        this.where('forecast_time', '<', isoRunTime).orWhere('run', isoRunTime);
      })
      .select(
        client.raw('ROW_NUMBER() OVER (PARTITION BY forecast_time ORDER BY run DESC) AS priority'),
      );

    query = client
      .select(fields)
      .from(query.as('t1'))
      .where('priority', 1)
      .orderBy('forecast_time');

    return cache ? query.cache() : query;
  });
