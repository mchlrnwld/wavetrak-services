import { expect } from 'chai';
import transformWindOld from './transformWindOld';

const transformWindOldSpec = () => {
  describe('transformWindOld', () => {
    it('should return gust value when gust is larger than wind speed', () => {
      const windRecordWithLargerGust = { u: 1, v: 1, gust: 10 };
      const result = transformWindOld(
        windRecordWithLargerGust,
        { windSpeed: 'm/s' },
        { windSpeed: 'm/s' },
      );
      expect(result.gust).to.be.equal(10);
    });

    it('should return wind speed value when wind speed is larger than gust', () => {
      const windRecordWithLargerWindSpeed = { u: 5, v: 0, gust: 1 };
      const result = transformWindOld(
        windRecordWithLargerWindSpeed,
        { windSpeed: 'm/s' },
        { windSpeed: 'm/s' },
      );
      expect(result.gust).to.be.equal(5);
    });
  });
};

export default transformWindOldSpec;
