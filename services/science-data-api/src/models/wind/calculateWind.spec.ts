import { expect } from 'chai';
import calculateWind from './calculateWind';

const calculateWindSpec = () => {
  describe('calculateWind', () => {
    it('computes direction and speed from u/v wind components', () => {
      const epsilon = 0.00001;

      const wind1 = calculateWind(1, 1);
      expect(wind1.direction).to.be.closeTo(225, epsilon);
      expect(wind1.speed).to.be.closeTo(Math.sqrt(2), epsilon);

      const wind2 = calculateWind(1, -Math.sqrt(3));
      expect(wind2.direction).to.be.closeTo(330, epsilon);
      expect(wind2.speed).to.be.closeTo(2, epsilon);

      const wind3 = calculateWind(-Math.sqrt(3), -1);
      expect(wind3.direction).to.be.closeTo(60, epsilon);
      expect(wind3.speed).to.be.closeTo(2, epsilon);

      const wind4 = calculateWind(-1, 1);
      expect(wind4.direction).to.be.closeTo(135, epsilon);
      expect(wind4.speed).to.be.closeTo(Math.sqrt(2), epsilon);
    });
  });
};

export default calculateWindSpec;
