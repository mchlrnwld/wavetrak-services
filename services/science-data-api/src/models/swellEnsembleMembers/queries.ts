import Knex, { QueryBuilder } from 'knex';
import nr from 'newrelic';
import db from '../../db';
import { runTimeToUnix, runTimeToISOString } from '../../db/runTimeConversions';
import unixTimeToISOString from '../../db/unixTimeToISOString';
import { SwellEnsembleMembersRecord } from './types';

const table = 'swell_ensemble_members';

// eslint-disable-next-line import/prefer-default-export
export const surfSpotSwellEnsembleMembers = (
  { agency, model, run, grid, pointOfInterestId, start, end },
  client: Knex = db.replica.client,
  cache = false,
): QueryBuilder<SwellEnsembleMembersRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    const isoRunTime = runTimeToISOString(run);
    const whereEqualsPredicate: any = { pointOfInterestId, agency, model, grid };

    const fields = [
      'forecast_time',
      'run',
      'member',
      'combined_wave_height',
      'primary_wave_direction',
      'primary_wave_period',
      'swell_wave_1_direction',
      'swell_wave_1_period',
      'swell_wave_1_height',
      'swell_wave_2_direction',
      'swell_wave_2_period',
      'swell_wave_2_height',
      'wind_wave_direction',
      'wind_wave_period',
      'wind_wave_height',
      'wind_u',
      'wind_v',
    ];

    const orderFields = ['forecast_time', 'member'];

    let query = client(table)
      .select(fields)
      .where(whereEqualsPredicate)
      .orderBy(orderFields);

    if (start) {
      query = query.andWhere('forecast_time', '>=', unixTimeToISOString(start));
    } else {
      query = query.andWhere('forecast_time', '>=', isoRunTime);
    }

    if (end) {
      query = query.andWhere('forecast_time', '<=', unixTimeToISOString(end));
    }

    const unixRunTime = runTimeToUnix(run);

    if (!start || start >= unixRunTime) {
      query = query.andWhere('run', isoRunTime);
      return cache ? query.cache() : query;
    }

    // Start is provided and is less than `run`, so `run` should be considered a max
    // run time allowed, and need the query to allow run times less than the passed
    // `run` for forecast hours prior to `run`. But there may be several such runs
    // available; so use window function to limit to the most up-to-date run for
    // each hour we want.
    query = query
      .andWhere(function() {
        this.where('forecast_time', '<', isoRunTime).orWhere('run', isoRunTime);
      })
      .select(
        client.raw(
          'ROW_NUMBER() OVER (PARTITION BY forecast_time, member ORDER BY run DESC) AS priority',
        ),
      );

    query = client
      .select(fields)
      .from(query.as('t1'))
      .where('priority', 1)
      .orderBy(orderFields);

    return cache ? query.cache() : query;
  });
