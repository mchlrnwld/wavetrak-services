import round from 'lodash/round';
import { convertUnits } from '../../common/units';

interface EnsembleWindUnits {
  windSpeed: string;
}

interface EnsembleWindRecord {
  // Wind component from west to east (i.e. positive value = Westerly wind)
  windU: number;
  // Wind component from south to north (i.e. positive value = Southerly wind)
  windV: number;
}

const roundTo5 = (value: number): number => round(value, 5);

const transformEnsembleWind = (
  { windU, windV }: EnsembleWindRecord,
  inputUnits: EnsembleWindUnits,
  outputUnits: EnsembleWindUnits
) => {
  if (windU === 0 && windV === 0) {
    return {
      speed: 0,
      direction: 0,
    };
  }

  const convertWindSpeed = convertUnits(inputUnits.windSpeed, outputUnits.windSpeed);

  return {
    speed: roundTo5(convertWindSpeed(Math.sqrt(windU * windU + windV * windV))),
    direction: roundTo5((3*Math.PI/2 - Math.atan2(windV, windU))*180/Math.PI) % 360,
  };
};

export default transformEnsembleWind;
