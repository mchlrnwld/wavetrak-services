/* eslint-disable camelcase */
import round from 'lodash/round';
import { convertUnits } from '../../common/units';

interface EnsembleSwellsUnits {
  waveHeight: string;
}

interface EnsembleSwellsRecord {
  combinedWaveHeight: number;
  primaryWaveDirection: number;
  primaryWavePeriod: number;
  swellWave_1Direction: number;
  swellWave_1Period: number;
  swellWave_1Height: number;
  swellWave_2Direction: number;
  swellWave_2Period: number;
  swellWave_2Height: number;
  windWaveDirection: number;
  windWavePeriod: number;
  windWaveHeight: number;
}

const roundTo5 = (value: number): number => round(value, 5);

const transformEnsembleSwells = (
  ensembleSwellsRecord: EnsembleSwellsRecord,
  inputUnits: EnsembleSwellsUnits,
  outputUnits: EnsembleSwellsUnits
) => {
  const {
    combinedWaveHeight: height,
    primaryWavePeriod: period,
    primaryWaveDirection: direction,
  } = ensembleSwellsRecord;
  const convertWaveHeight = convertUnits(inputUnits.waveHeight, outputUnits.waveHeight);

  return {
    combined: {
      height: roundTo5(convertWaveHeight(height)),
      direction: roundTo5(direction),
      period: roundTo5(period),
    },
    components: [...Array(3)].map((_, i) => {
      const prefix = i < 2 ? `swellWave_${i + 1}` : 'windWave';
      return {
        height: roundTo5(convertWaveHeight(ensembleSwellsRecord[`${prefix}Height`])),
        direction: roundTo5(ensembleSwellsRecord[`${prefix}Direction`]),
        period: roundTo5(ensembleSwellsRecord[`${prefix}Period`]),
      };
    }).filter(({ height }) => height),
  };
};

export default transformEnsembleSwells;
