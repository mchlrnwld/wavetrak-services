export interface SwellEnsembleMembersRecord {
  forecastTime: number;
  run: number;
  member: number;
  combinedWaveHeight: number;
  primaryWaveDirection: number;
  primaryWavePeriod: number;
  swellWave_1Direction: number;
  swellWave_1Period: number;
  swellWave_1Height: number;
  swellWave_2Direction: number;
  swellWave_2Period: number;
  swellWave_2Height: number;
  windWaveDirection: number;
  windWavePeriod: number;
  windWaveHeight: number;
  windU: number;
  windV: number;
}
