import { expect } from 'chai';
import transformEnsembleWind from './transformEnsembleWind';

describe('transformEnsembleWind', () => {
  const units = {
    windSpeed: 'm/s',
  };

  it('calculates speed', () => {
    const { speed } = transformEnsembleWind({ windU: 3, windV: 4 }, units, units);
    expect(speed).to.equal(5.0);
  });

  it('identifies north wind', () => {
    const { direction } = transformEnsembleWind({ windU: 0, windV: -1 }, units, units);
    expect(direction).to.equal(0);
  });

  it('identifies north-east wind', () => {
    const { direction } = transformEnsembleWind({ windU: -1, windV: -1 }, units, units);
    expect(direction).to.equal(45);
  });

  it('identifies east wind', () => {
    const { direction } = transformEnsembleWind({ windU: -1, windV: 0}, units, units);
    expect(direction).to.equal(90);
  });

  it('identifies south-east wind', () => {
    const { direction } = transformEnsembleWind({ windU: -1, windV: 1}, units, units);
    expect(direction).to.equal(135);
  });

  it('identifies south wind', () => {
    const { direction } = transformEnsembleWind({ windU: 0, windV: 1}, units, units);
    expect(direction).to.equal(180);
  });

  it('identifies south-west wind', () => {
    const { direction } = transformEnsembleWind({ windU: 1, windV: 1}, units, units);
    expect(direction).to.equal(225);
  });

  it('identifies west wind', () => {
    const { direction } = transformEnsembleWind({ windU: 1, windV: 0}, units, units);
    expect(direction).to.equal(270);
  });

  it('identifies north-west wind', () => {
    const { direction } = transformEnsembleWind({ windU: 1, windV: -1}, units, units);
    expect(direction).to.equal(315);
  });

  it('doens\'t blow up with no wind', () => {
    const { direction } = transformEnsembleWind({ windU: 0, windV: 0}, units, units);
    expect(direction).to.equal(0);
  });
});
