import { expect } from 'chai';
import { partial } from 'lodash';
import { valueFromRating, ratingFromValue } from './transformations';

const transformationsSpec = () => {
  describe('transformations', () => {
    describe('ratingFromValue', () => {
      it('maps numeric values to the correct ratings', () => {
        expect(ratingFromValue(1)).to.equal('VERY_POOR');
        expect(ratingFromValue(2)).to.equal('POOR');
        expect(ratingFromValue(3)).to.equal('POOR_TO_FAIR');
        expect(ratingFromValue(4)).to.equal('FAIR');
        expect(ratingFromValue(5)).to.equal('FAIR_TO_GOOD');
        expect(ratingFromValue(6)).to.equal('GOOD');
        expect(ratingFromValue(7)).to.equal('EPIC');
      });

      it('rounds floating point values before mapping', () => {
        expect(ratingFromValue(1.1)).to.equal('VERY_POOR');
        expect(ratingFromValue(1.5)).to.equal('POOR');
      });

      it('fits input values within [1, 7]', () => {
        expect(ratingFromValue(0)).to.equal('VERY_POOR');
        expect(ratingFromValue(8)).to.equal('EPIC');
      });
    });

    describe('valueFromRating', () => {
      it('maps ratings to integer values', () => {
        expect(valueFromRating('VERY_POOR')).to.equal(1);
        expect(valueFromRating('POOR')).to.equal(2);
        expect(valueFromRating('POOR_TO_FAIR')).to.equal(3);
        expect(valueFromRating('FAIR')).to.equal(4);
        expect(valueFromRating('FAIR_TO_GOOD')).to.equal(5);
        expect(valueFromRating('GOOD')).to.equal(6);
        expect(valueFromRating('EPIC')).to.equal(7);
      });

      it('throws an error if an invalid rating type passed', () => {
        expect(partial(valueFromRating, 'INVALID_RATING')).to.throw();
      });
    });
  });
};

export default transformationsSpec;
