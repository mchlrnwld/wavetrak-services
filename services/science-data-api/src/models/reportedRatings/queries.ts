import Knex from 'knex';
import nr from 'newrelic';
import db from '../../db';
import { ReportedRatingInput } from './index.d';
import unixTimeToISOString from '../../db/unixTimeToISOString';

// TODO: Remove this once completely cutover to 7 ratings
const CONDITION_MAP = {
  FLAT: 'VERY_POOR',
  FAIR_TO_GOOD: 'GOOD',
  VERY_GOOD: 'GOOD',
  GOOD_TO_EPIC: 'GOOD',
};

const conditionMap = (condition: string): string =>
  condition in CONDITION_MAP ? CONDITION_MAP[condition] : condition;

const table = 'reported_ratings';

/**
 * Upserts a list of reported ratings to database in an atomic transaction.
 *
 * @param reportedRatings - List of reported ratings to upsert.
 */
export const upsert = (reportedRatings: ReportedRatingInput[]): Promise<void> =>
  db.source.client.transaction(trx =>
    Promise.all(
      reportedRatings.map(reportedRating => {
        const insertQuery = trx(table)
          .insert({
            pointOfInterestId: reportedRating.pointOfInterestId,
            forecastTime: reportedRating.forecastTime.toISOString(),
            rating: conditionMap(reportedRating.rating),
          })
          .toString();

        const updateQuery = trx
          .update({
            rating: conditionMap(reportedRating.rating),
          })
          .toString();

        const upsertQuery = `${insertQuery} ON CONFLICT (point_of_interest_id, forecast_time) DO ${updateQuery}`;
        return trx.raw(upsertQuery);
      }),
    ),
  );

export const find = ({ pointOfInterestId, start, end }, client: Knex = db.replica.client) =>
  nr.startSegment(`select:${table}`, false, () => {
    const whereEqualsPredicate: any = { pointOfInterestId };
    const fields = ['forecast_time', 'rating'];

    let query = client(table)
      .select(fields)
      .where(whereEqualsPredicate)
      .orderBy('forecast_time');

    if (start) {
      query = query.andWhere('forecast_time', '>=', unixTimeToISOString(start));
    }

    if (end) {
      query = query.andWhere('forecast_time', '<=', unixTimeToISOString(end));
    }

    return query;
  });
