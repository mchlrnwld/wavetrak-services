const orderedRatings = [
  'VERY_POOR',
  'POOR',
  'POOR_TO_FAIR',
  'FAIR',
  'FAIR_TO_GOOD',
  'GOOD',
  'EPIC',
];

const ratingValues = new Map(orderedRatings.map((rating, i) => [rating, i + 1]));

export const ratingFromValue = (value: number): string => {
  const boundedIntegerValue = Math.min(7, Math.max(1, Math.round(value)));
  return orderedRatings[boundedIntegerValue - 1];
};

export const valueFromRating = (rating: string): number => {
  if (ratingValues.has(rating)) {
    return ratingValues.get(rating);
  }
  throw new Error('Unknown rating type.');
};
