export interface ReportedRatingInput {
    pointOfInterestId: string;
    forecastTime: Date;
    rating: string;
}

export interface ReportedRatingRecord {
    pointOfInterestId: string;
    forecastTime: Date;
    rating: string;
}
