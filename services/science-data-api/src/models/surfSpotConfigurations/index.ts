import joi from 'joi';
import { upsert } from './queries';
import { createValidationError } from '../../common/errors/ValidationError';
import schema from './schema';

export const upsertSurfSpotConfiguration = (
  pointOfInterestId: string,
  data: {
    offshoreDirection: number;
    optimalSwellDirection: number;
    breakingWaveHeightCoefficient: number;
    breakingWaveHeightIntercept: number;
    spectralRefractionMatrix: number[][];
    breakingWaveHeightAlgorithm: string;
    ratingsMatrix: number[][];
    ratingsOptions: any;
  },
) => {
  const validationResult = joi.validate(data, schema, {
    abortEarly: false,
    allowUnknown: true,
  });

  if (validationResult.error) {
    throw createValidationError(
      'Error validating Surf Spot Configuration data',
      validationResult.error.details.map(({ message }) => message),
    );
  }

  return upsert(pointOfInterestId, data);
};
