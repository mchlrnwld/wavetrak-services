import joi from 'joi';

const schema = joi.object().keys({
  offshoreDirection: joi
    .number()
    .min(0)
    .less(360)
    .allow(null),
  optimalSwellDirection: joi
    .number()
    .min(0)
    .less(360)
    .allow(null),
  breakingWaveHeightCoefficient: joi
    .number()
    .min(0)
    .allow(null),
  spectralRefractionMatrix: joi
    .array()
    .items(
      joi
        .array()
        .items(joi.number())
        .length(18),
    )
    .length(24)
    .allow(null),
});

export default schema;
