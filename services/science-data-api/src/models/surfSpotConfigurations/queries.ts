import nr from 'newrelic';
import Knex, { QueryBuilder } from 'knex';
import db from '../../db';
import { SurfSpotConfigurationRecord } from './types';

const table = 'surf_spot_configurations';

export const find = (
  pointOfInterestIds: readonly string[],
  client: Knex = db.replica.client,
): QueryBuilder<SurfSpotConfigurationRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    // https://postgres.cz/wiki/PostgreSQL_SQL_Tricks_I#Predicate_IN_optimalization
    const valuesExpression = `values ${pointOfInterestIds.map(id => `('${id}'::uuid)`).join(',')}`;
    return client<SurfSpotConfigurationRecord>(table)
      .select(
        'point_of_interest_id',
        'offshore_direction',
        'optimal_swell_direction',
        'breaking_wave_height_coefficient',
        'breaking_wave_height_intercept',
        'spectral_refraction_matrix',
        'breaking_wave_height_algorithm',
        'ratings_matrix',
        'ratings_algorithm',
        'ratings_options',
      )
      .where(client.raw(`point_of_interest_id in (${valuesExpression})`));
  });

export const upsert = (
  pointOfInterestId: string,
  data: {
    offshoreDirection: number;
    optimalSwellDirection: number;
    breakingWaveHeightCoefficient: number;
    breakingWaveHeightIntercept: number;
    spectralRefractionMatrix: number[][];
    breakingWaveHeightAlgorithm: string;
    ratingsMatrix: number[][];
    ratingsOptions: any;
  },
): QueryBuilder<SurfSpotConfigurationRecord> => {
  // knex treats `null` and `undefined` differently. In particular, the update part will
  // set to NULL for nulls in the JS object, but will ignore entries that are set to
  // undefined in the object. But in this case we want both to set to NULL.
  const dataToUpsert = {
    optimalSwellDirection: null,
    spectralRefractionMatrix: null,
    ratingsMatrix: null,
    ...data,
  };

  const { client } = db.source;

  const insertQuery = client(table)
    .insert({ pointOfInterestId, ...dataToUpsert })
    .toSQL();

  const updateQuery = client.update({ ...dataToUpsert }).toSQL();

  const upsertQuery = `${insertQuery.sql} ON CONFLICT ("point_of_interest_id") DO ${updateQuery.sql}`;

  const bindings = insertQuery.bindings.concat(updateQuery.bindings);

  return client.raw(upsertQuery, bindings);
};
