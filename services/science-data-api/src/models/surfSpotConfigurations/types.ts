export interface SurfSpotConfigurationRecord {
  breakingWaveHeightAlgorithm: string;
  breakingWaveHeightCoefficient: number;
  breakingWaveHeightIntercept: number;
  offshoreDirection: number;
  optimalSwellDirection: number;
  pointOfInterestId: string;
  ratingsAlgorithm: string;
  ratingsMatrix: number[][];
  ratingsOptions: any;
  spectralRefractionMatrix: number[][];
}
