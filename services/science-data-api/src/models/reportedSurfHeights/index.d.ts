export interface ReportedSurfHeightInput {
  pointOfInterestId: string;
  forecastTime: Date;
  min: number;
  max: number;
}

export interface ReportedSurfHeight {
  timestamp: number;
  pointOfInterestId?: string;
  min: number;
  max: number;
}

export interface ReportedSurfHeightRecord {
  pointOfInterestId: string;
  forecastTime: Date;
  min: number;
  max: number;
}

export interface ReportedSurfHeightUnits {
  height: string;
}
