import round from 'lodash/round';
import { convertUnits } from '../../common/units';
import { ReportedSurfHeightRecord, ReportedSurfHeightUnits } from './index.d';
import { SurfUnits } from '../surf/types';

const roundTo5 = (value: number): number => round(value, 5);

const transformSurfHeights = (
  surfRecord: ReportedSurfHeightRecord,
  inputUnits: ReportedSurfHeightUnits,
  outputUnits: SurfUnits,
) => {
  const { min, max } = surfRecord;

  const convertWaveHeight = convertUnits(inputUnits.height, outputUnits.waveHeight);

  return {
    min: roundTo5(convertWaveHeight(min)),
    max: roundTo5(convertWaveHeight(max)),
  };
};

export default transformSurfHeights;
