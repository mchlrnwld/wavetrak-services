import Knex from 'knex';
import nr from 'newrelic';
import db from '../../db';
import { ReportedSurfHeightInput } from './index.d';
import unixTimeToISOString from '../../db/unixTimeToISOString';

const table = 'reported_surf_heights';

/**
 * Upserts a list of reported surf heights to database in an atomic transaction.
 *
 * @param reportedSurfHeights - List of reported surf heights to upsert.
 */
export const upsert = (reportedSurfHeights: ReportedSurfHeightInput[]): Promise<void> =>
  db.source.client.transaction(trx =>
    Promise.all(
      reportedSurfHeights.map(reportedSurfHeight => {
        const insertQuery = trx(table)
          .insert({
            pointOfInterestId: reportedSurfHeight.pointOfInterestId,
            forecastTime: reportedSurfHeight.forecastTime.toISOString(),
            min: reportedSurfHeight.min,
            max: reportedSurfHeight.max,
          })
          .toString();

        const updateQuery = trx
          .update({
            min: reportedSurfHeight.min,
            max: reportedSurfHeight.max,
          })
          .toString();

        const upsertQuery = `${insertQuery} ON CONFLICT (point_of_interest_id, forecast_time) DO ${updateQuery}`;
        return trx.raw(upsertQuery);
      }),
    ),
  );

// eslint-disable-next-line import/prefer-default-export
export const find = (
  { pointOfInterestId, start, end },
  client: Knex = db.replica.client,
  cache = false,
) =>
  nr.startSegment(`select:${table}`, false, () => {
    const whereEqualsPredicate: any = { pointOfInterestId };
    const fields = ['forecast_time', 'min', 'max'];

    let query = client(table)
      .select(fields)
      .where(whereEqualsPredicate)
      .orderBy('forecast_time');

    if (start) {
      query = query.andWhere('forecast_time', '>=', unixTimeToISOString(start));
    }

    if (end) {
      query = query.andWhere('forecast_time', '<=', unixTimeToISOString(end));
    }

    return cache ? query.cache() : query;
  });
