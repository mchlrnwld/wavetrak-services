import { getRecords } from '../../external/smdb';
import transformWeatherOld from './transformWeatherOld';
import models from '../../stores/models';

export const getWeather = async (
  { agency, model, run, grid: { key: grid, lat, lon } },
  start,
  end,
  units,
) => {
  const records = await getRecords({ agency, model, run, grid, lat, lon, start, end });
  return records.map(record => ({
    timestamp: record.timestamp,
    weather: transformWeatherOld(record, models[agency][model].storedUnits, units),
  }));
};
