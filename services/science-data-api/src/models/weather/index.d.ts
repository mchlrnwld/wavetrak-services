interface WeatherUnits {
  temperature: string;
  pressure: string;
  visibility: string;
  precipitation: string;
}

interface WeatherUnitsOld {
  windSpeed: string;
  temperature: string;
  pressure: string;
  visibility: string;
  precipitation: string;
}

export interface WeatherRecord {
  run: number;
  forecastTime: number;
  pressure: number;
  temperature: number;
  visibility: number;
  dewpoint: number;
  humidity: number;
  precipitationType: string;
  precipitationVolume: number;
  weatherConditions: string;
}

interface WeatherRecordOld {
  windU: number;
  windV: number;
  windGust: number;
  pressure: number;
  temperature: number;
  visibility: number;
  dewpoint: number;
  humidity: number;
  precipitationType: string;
  precipitationVolume: number;
  weatherConditions: string;
}

export interface WeatherForecast {
  timestamp: number;
  run: number;
  weather: WeatherRecord;
}
