import round from 'lodash/round';
import { convertUnits } from '../../common/units';
import { WeatherRecordOld, WeatherUnitsOld } from './index.d';
import transformWindOld from '../wind/transformWindOld';

const roundTo5 = (value: number): number => round(value, 5);

const transformWeatherOld = (
  {
    windU,
    windV,
    windGust,
    pressure,
    temperature,
    visibility,
    dewpoint,
    humidity,
    precipitationType,
    precipitationVolume,
    weatherConditions: conditions,
  }: WeatherRecordOld,
  inputUnits: WeatherUnitsOld,
  outputUnits: WeatherUnitsOld,
) => {
  const convertTemperature = convertUnits(inputUnits.temperature, outputUnits.temperature);
  const convertPressure = convertUnits(inputUnits.pressure, outputUnits.pressure);
  const convertVisibility = convertUnits(inputUnits.visibility, outputUnits.visibility);
  const convertPrecipitation = convertUnits(inputUnits.precipitation, outputUnits.precipitation);

  return {
    wind: transformWindOld({ u: windU, v: windV, gust: windGust }, inputUnits, outputUnits),
    pressure: roundTo5(convertPressure(pressure)),
    temperature: roundTo5(convertTemperature(temperature)),
    visibility: roundTo5(convertVisibility(visibility)),
    dewpoint: roundTo5(convertTemperature(dewpoint)),
    humidity: roundTo5(humidity),
    precipitation: {
      type: precipitationType,
      volume: roundTo5(convertPrecipitation(precipitationVolume)),
    },
    conditions,
  };
};

export default transformWeatherOld;
