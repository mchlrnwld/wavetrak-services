import { expect } from 'chai';
import transformWeatherOld from './transformWeatherOld';
import { WeatherRecordOld, WeatherUnitsOld } from './index.d';

export const transformWeatherOldSpec = () => {
  describe('transformWeatherOld', () => {
    it('returns a wind object that contains direction, speed, and gust values', () => {
      const record: WeatherRecordOld = {
        windU: 1,
        windV: 0,
        windGust: 1,
        pressure: 1,
        temperature: 1,
        visibility: 1,
        dewpoint: 1,
        humidity: 1,
        precipitationType: '',
        precipitationVolume: 1,
        weatherConditions: '',
      };
      const units: WeatherUnitsOld = {
        windSpeed: 'm/s',
        temperature: 'F',
        pressure: 'bar',
        visibility: 'm',
        precipitation: 'mm3',
      };
      const transformedWeather = transformWeatherOld(record, units, units);
      if (expect(transformedWeather.wind).to.be != null) {
        expect(transformedWeather.wind.direction).to.be.equal(270);
        expect(transformedWeather.wind.speed).to.be.equal(1);
        expect(transformedWeather.wind.gust).to.be.equal(1);
      }
    });
  });
};

export default transformWeatherOldSpec;
