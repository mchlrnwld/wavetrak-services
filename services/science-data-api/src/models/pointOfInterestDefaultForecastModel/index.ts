import { find, upsert, remove } from './queries';
import { ForecastType, PointOfInterestDefaultForecastModel } from './index.d';

export const queryPointsOfInterestDefaultForecastModels = params => find(params);

export const upsertPointOfInterestDefaultForecastModels = (
  pointOfInterestId: string,
  defaultForecastModels: PointOfInterestDefaultForecastModel[],
) => upsert(pointOfInterestId, defaultForecastModels);

export const removePointOfInterestDefaultForecastModels = (
  pointOfInterestId: string,
  forecastTypes: ForecastType[],
) => remove(pointOfInterestId, forecastTypes);
