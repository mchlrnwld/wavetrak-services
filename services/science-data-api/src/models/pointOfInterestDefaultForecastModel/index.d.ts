export enum ForecastType {
  ENSEMBLE = 'ENSEMBLE',
  SST = 'SST',
  SURF = 'SURF',
  SWELLS = 'SWELLS',
  WEATHER = 'WEATHER',
  WIND = 'WIND',
}

export interface PointOfInterestDefaultForecastModel {
  pointOfInterestId?: string;
  forecastType: ForecastType;
  agency: string;
  model: string;
  grid: string;
}
