import nr from 'newrelic';
import db from '../../db';
import { ForecastType, PointOfInterestDefaultForecastModel } from './index.d';

const table = 'points_of_interest_default_forecast_models';

export const find = ({
  pointOfInterestId,
  forecastType,
  offset,
  limit,
}: {
  pointOfInterestId: string;
  forecastType: ForecastType;
  offset: number;
  limit: number;
}) =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client } = db.replica;
    const whereEqualsPredicate: any = {};

    if (pointOfInterestId) {
      whereEqualsPredicate[`${table}.pointOfInterestId`] = pointOfInterestId;
    }

    if (forecastType) {
      whereEqualsPredicate[`${table}.forecastType`] = forecastType;
    }

    let query = client(table)
      .select('point_of_interest_id', 'agency', 'model', 'grid', 'forecast_type')
      .where(whereEqualsPredicate);

    if (limit) {
      query = query.limit(limit);
    }

    if (offset) {
      query = query.offset(offset);
    }

    return query;
  });

export const remove = (pointOfInterestId: string, forecastTypes: ForecastType[]) =>
  db.source.client.transaction(trx =>
    Promise.all(
      forecastTypes.map(async forecastType => {
        const whereEqualsPredicate = {
          pointOfInterestId,
          forecastType,
        };
        const result = await trx(table)
          .where(whereEqualsPredicate)
          .del()
          .returning(['point_of_interest_id', 'forecast_type', 'agency', 'model', 'grid']);

        if (!result || result.length !== 1) {
          throw new Error(
            'No records updated, point of interest id and forecast type does not exist.',
          );
        }
      }),
    ),
  );

export const upsert = (
  pointOfInterestId: string,
  defaultForecastModels: PointOfInterestDefaultForecastModel[],
) =>
  db.source.client.transaction(transaction =>
    Promise.all(
      defaultForecastModels.map(async defaultForecastModel => {
        const insertQuery = transaction(table)
          .insert({ pointOfInterestId, ...defaultForecastModel })
          .toSQL();
        const updateQuery = transaction
          .update({
            agency: defaultForecastModel.agency,
            model: defaultForecastModel.model,
            grid: defaultForecastModel.grid,
          })
          .toSQL();
        const upsertQuery = `${insertQuery.sql} ON CONFLICT ("point_of_interest_id", "forecast_type") DO ${updateQuery.sql}`;
        const bindings = insertQuery.bindings.concat(updateQuery.bindings);
        return transaction.raw(upsertQuery, bindings);
      }),
    ),
  );
