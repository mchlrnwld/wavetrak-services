import nr from 'newrelic';
import db from '../../db';

const table = 'model_grid_points';
const modelGridsTable = 'model_grids';

// eslint-disable-next-line import/prefer-default-export
export const nearest = ({ agency, model, grid, lat, lon, within }) =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client, st } = db.replica;
    const point = st.point(lon, lat);
    const whereEqualsPredicate: any = {
      [`${table}.agency`]: agency,
      [`${table}.model`]: model,
    };

    if (grid) {
      whereEqualsPredicate[`${table}.grid`] = grid;
    }

    const query = client(table)
      .innerJoin(modelGridsTable, function() {
        ['agency', 'model', 'grid'].forEach(field =>
          this.on(`${table}.${field}`, '=', `${modelGridsTable}.${field}`),
        );
      })
      .select(`${table}.grid`, 'latitude as lat', 'longitude as lon')
      .where(whereEqualsPredicate)
      // ST_DWithin to filter before sorting to improve performance.
      // ST_DWithin also takes advantage of indices whereas ST_Distance will
      // always scan.
      .andWhere(st.dwithin(point, 'geography_point', within))
      .orderBy(st.distance(point, 'geography_point'))
      // Pick highest resolution in case grid is not specified.
      .orderBy('resolution')
      .limit(1);

    return query;
  });
