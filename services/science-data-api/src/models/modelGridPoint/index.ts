import { nearest } from './queries';

// eslint-disable-next-line import/prefer-default-export
export const getNearestModelGridPoint = async params => {
  const result = await nearest(params);
  if (!result.length) {
    return null;
  }
  return result[0];
};
