import { search } from './queries';

// eslint-disable-next-line import/prefer-default-export
export const queryModels = async params => {
  const models = await search(params);
  return models;
};
