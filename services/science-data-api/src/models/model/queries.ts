import nr from 'newrelic';
import db from '../../db';
import { MODEL_TYPES } from './constants';

const table = 'models';

// eslint-disable-next-line import/prefer-default-export
export const search = ({ agency, model, types }) =>
  nr.startSegment(`select:${table}`, false, () => {
    const { client } = db.replica;
    const whereEqualsPredicate: any = {};

    if (agency) {
      whereEqualsPredicate.agency = agency;
    }

    if (model) {
      whereEqualsPredicate.model = model;
    }

    if (types) {
      types
        .filter(t => MODEL_TYPES.includes(t))
        .forEach(t => {
          whereEqualsPredicate[t] = true;
        });
    }

    const query = client(table)
      .select('agency', 'model', ...MODEL_TYPES.map(t => t.toLowerCase()))
      .where(whereEqualsPredicate);

    return query;
  });
