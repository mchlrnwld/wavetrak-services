import { flow, sortBy, uniqBy, zip } from 'lodash/fp';
import { Swell, SwellForecast, SwellForecastWithEvents } from './index.d';

interface SwellEvent {
  swell: Swell;
  event: number;
}

/**
 * Calculates absolute difference in direction (°).
 *
 * @param direction1 - First direction to compare (°).
 * @param direction2 - Second direction to compare (°).
 * @return Absolute difference between direction1 and direction2.
 */
export const calculateDirectionDifference = (direction1: number, direction2: number): number => {
  const adjustedDirection1 = direction1 < 0 ? 360 + (direction1 % 360) : direction1 % 360;
  const adjustedDirection2 = direction2 < 0 ? 360 + (direction2 % 360) : direction2 % 360;

  const rawDirectionDifference = Math.abs(adjustedDirection1 - adjustedDirection2);
  const directionDifference = Math.min(rawDirectionDifference, 360 - rawDirectionDifference);

  return directionDifference;
};

/**
 * Matches current swells with swell events.
 *
 * @param currentSwells - Array of current swells.
 * @param swellEvents - Array of tracked swell events to compare current swells to.
 * @param directionThreshold - The absolute difference between two swells' directions need to be at
 *                             or below this threshold for the swells to be matched for the same
 *                             event.
 * @param periodThreshold - The absolute difference between two swells' periods need to be at or
 *                          below this threshold for the swells to be matched for the same event.
 * @return Array of swells with matching events.
 */
export const matchSwellEvents = (
  currentSwells: Swell[],
  swellEvents: SwellEvent[],
  directionThreshold = 11,
  periodThreshold = 2.5,
): SwellEvent[] => {
  const possibleSwellEventMatches = [];
  for (const currentSwell of currentSwells) {
    for (const swellEvent of swellEvents) {
      const { swell, event } = swellEvent;

      if (!swell) {
        continue;
      }

      const directionDifference = calculateDirectionDifference(
        currentSwell.direction,
        swell.direction,
      );
      const periodDifference = Math.abs(currentSwell.period - swell.period);

      if (directionDifference > directionThreshold || periodDifference > periodThreshold) {
        continue;
      }

      const heightDifference = Math.abs(currentSwell.height - swell.height);

      // Score based on "beauty principle". Lower score indicates better match.
      const score = 25 * heightDifference + 5 * periodDifference + directionDifference;

      possibleSwellEventMatches.push({
        swell: currentSwell,
        event,
        score,
      });
    }
  }

  // Swells and events are 1-1 starting with the lowest score.
  const matchedSwellEvents = flow(
    sortBy(({ score }) => score),
    uniqBy(({ event }) => event),
    uniqBy(({ swell }) => swell),
  )(possibleSwellEventMatches) as SwellEvent[];

  return matchedSwellEvents;
};

/**
 * Tracks events over an array of swells forecasts.
 *
 * @param swellsForecasts - Array of swells forecasts.
 * @param maxEvents - Maximum number of events to track. When a new event is created and maxEvents
 *                    is exceeded we will replace an empty event.
 * @return Array of swells forecasts with event property.
 */
const trackSwellEvents = (
  swellsForecasts: SwellForecast[],
  maxEvents = 6,
): SwellForecastWithEvents[] => {
  // Initialize tracked swell events with swells from first forecast hour.
  const swellEvents = [
    swellsForecasts[0].swells.components.map((swell, event) => ({
      swell,
      event,
    })),
  ];
  const latestSwellEvents = [...Array(maxEvents)].map((_, event) => {
    const { swell } = swellEvents[0][event] || { swell: null };
    return {
      swell,
      event,
      drought: swell ? 0 : 1,
    };
  });

  // Begin tracking swell events by comparing swell forecasts with previously tracked swells.
  for (const swellsForecast of swellsForecasts.slice(1)) {
    const {
      swells: { components: currentSwells },
    } = swellsForecast;
    const matchedSwellEvents = matchSwellEvents(currentSwells, latestSwellEvents);

    const unmatchedSwells = currentSwells.filter(
      swell => !matchedSwellEvents.find(({ swell: matchedSwell }) => swell === matchedSwell),
    );

    // Create new events for swells that did not match previous swell events. Replace swell event
    // with longest drought first.
    const unmatchedSwellEventIds = [...Array(maxEvents)]
      .map((_, i) => i)
      .filter(i => matchedSwellEvents.every(({ event }) => event !== i))
      .sort((a, b) => latestSwellEvents[b].drought - latestSwellEvents[a].drought);

    const unmatchedSwellEvents = zip(unmatchedSwells, unmatchedSwellEventIds)
      .filter(([swell, _]) => swell)
      .map(([swell, event]) => ({
        swell,
        event,
      }));

    const currentSwellEvents = [...matchedSwellEvents, ...unmatchedSwellEvents];
    swellEvents.push(
      currentSwells.map(swell => {
        const { event } = currentSwellEvents.find(swellEvent => swellEvent.swell === swell);
        return {
          swell,
          event,
        };
      }),
    );

    // Update latest swell events.
    latestSwellEvents.forEach((_, event) => {
      const currentSwellEvent = currentSwellEvents.find(swellEvent => event === swellEvent.event);
      if (currentSwellEvent) {
        latestSwellEvents[event].swell = currentSwellEvent.swell;
        latestSwellEvents[event].drought = 0;
      } else {
        latestSwellEvents[event].drought += 1;
      }
    });
  }

  const swellsForecastsWithEvents = zip(swellsForecasts, swellEvents).map(
    ([swellsForecast, swellEvents]) => ({
      ...swellsForecast,
      swells: {
        ...swellsForecast.swells,
        components: swellEvents.map(({ swell, event }) => ({
          ...swell,
          event,
        })),
      },
    }),
  );

  return swellsForecastsWithEvents;
};

export default trackSwellEvents;
