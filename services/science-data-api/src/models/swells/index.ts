import { getRecords } from '../../external/smdb';
import transformSwellsOld from './transformSwellsOld';
import models from '../../stores/models';

export const getSwells = async (
  { agency, model, run, grid: { key: grid, lat, lon } },
  start,
  end,
  units,
) => {
  const records = await getRecords({ agency, model, run, grid, lat, lon, start, end });
  return records.map(record => ({
    timestamp: record.timestamp,
    swells: transformSwellsOld(record, models[agency][model].storedUnits, units),
  }));
};
