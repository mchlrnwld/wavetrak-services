import round from 'lodash/round';
import { SwellsRecord } from './index.d';

const spectra1dPeriod = [...Array(27)].map((_, i) => i + 2);

const transformSwells = (swellsRecord: SwellsRecord) => {
  const {
    height,
    period,
    direction,
    spectra1dEnergy,
    spectra1dDirection,
    spectra1dDirectionalSpread,
  } = swellsRecord;

  return {
    combined: {
      height: round(height, 5),
      direction: round(direction, 5),
      period: round(period),
    },
    components: [...Array(6)]
      .map((_, i) => {
        const prefix = i < 5 ? `swellWave_${i + 1}` : 'windWave';
        return {
          height: round(swellsRecord[`${prefix}Height`], 5),
          direction: round(swellsRecord[`${prefix}Direction`], 5),
          period: round(swellsRecord[`${prefix}Period`]),
          spread: round(swellsRecord[`${prefix}Spread`], 5),
          impact: swellsRecord[`${prefix}Impact`],
        };
      })
      .filter(({ height }) => height),
    spectra1d: {
      has: !(spectra1dEnergy == null),
      period: spectra1dPeriod,
      energy: spectra1dEnergy ? spectra1dEnergy.map(x => round(x, 5)) : null,
      direction: spectra1dDirection ? spectra1dDirection.map(x => round(x, 1)) : null,
      directionalSpread: spectra1dDirectionalSpread
        ? spectra1dDirectionalSpread.map(x => round(x, 1))
        : null,
    },
  };
};

export default transformSwells;
