/* eslint-disable camelcase */
import round from 'lodash/round';
import { convertUnits } from '../../common/units';
import { SwellsUnits } from './index.d';

const roundTo5 = (value: number): number => round(value, 5);

const convertSwellsUnits = (swells, inputUnits: SwellsUnits, outputUnits: SwellsUnits) => {
  const { combined, components, ...rest } = swells;
  const convertWaveHeight = convertUnits(inputUnits.waveHeight, outputUnits.waveHeight);

  return {
    combined: {
      height: roundTo5(convertWaveHeight(combined.height)),
      direction: roundTo5(combined.direction),
      period: roundTo5(combined.period),
    },
    components: components.map(({ height, direction, period, spread, impact, event }) => ({
      height: roundTo5(convertWaveHeight(height)),
      direction: roundTo5(direction),
      period: roundTo5(period),
      spread: roundTo5(spread),
      impact: roundTo5(impact),
      event: event,
    })),
    ...rest,
  };
};

export default convertSwellsUnits;
