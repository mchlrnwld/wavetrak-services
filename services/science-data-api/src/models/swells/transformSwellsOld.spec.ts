import { expect } from 'chai';
import transformSwellsOld from './transformSwellsOld';

const transformSwellsOldSpec = () => {
  describe('transformSwellsOld', () => {
    it('converts wave height units', () => {
      const swells = {
        height: 1.32,
        direction: 205.7,
        period: 11.41,
        swellWave1Height: 1.05,
        swellWave1Direction: 174.88,
        swellWave1Period: 8.08,
        swellWave2Height: 0.73,
        swellWave2Direction: 205.55,
        swellWave2Period: 11.54,
        swellWave3Height: 0.27,
        swellWave3Direction: 205.63,
        swellWave3Period: 15.92,
        swellWave4Height: 0.06,
        swellWave4Direction: 207.87,
        swellWave4Period: 21.37,
        swellWave5Height: 0.06,
        swellWave5Direction: 159.14,
        swellWave5Period: 13.4,
        windWaveHeight: 0.06,
        windWaveDirection: 159.14,
        windWavePeriod: 13.4,
      };
      const inputUnits = { waveHeight: 'm' };
      const outputUnits = { waveHeight: 'ft' };
      expect(transformSwellsOld(swells, inputUnits, outputUnits)).to.deep.equal({
        combined: {
          direction: 205.7,
          height: 4.33071,
          period: 11.41,
        },
        components: [
          {
            direction: 174.88,
            height: 3.44488,
            period: 8.08,
          },
          {
            direction: 205.55,
            height: 2.39501,
            period: 11.54,
          },
          {
            direction: 205.63,
            height: 0.88583,
            period: 15.92,
          },
          {
            direction: 207.87,
            height: 0.19685,
            period: 21.37,
          },
          {
            direction: 159.14,
            height: 0.19685,
            period: 13.4,
          },
          {
            direction: 159.14,
            height: 0.19685,
            period: 13.4,
          },
        ],
      });
    });

    it('treats 9999 as null value', () => {
      const swells = {
        height: 9999,
        direction: 9999,
        period: 9999,
        swellWave1Height: 9999,
        swellWave1Direction: 9999,
        swellWave1Period: 9999,
        swellWave2Height: 9999,
        swellWave2Direction: 9999,
        swellWave2Period: 9999,
        swellWave3Height: 9999,
        swellWave3Direction: 9999,
        swellWave3Period: 9999,
        swellWave4Height: 9999,
        swellWave4Direction: 9999,
        swellWave4Period: 9999,
        swellWave5Height: 9999,
        swellWave5Direction: 9999,
        swellWave5Period: 9999,
        windWaveHeight: 9999,
        windWaveDirection: 9999,
        windWavePeriod: 9999,
      };
      const inputUnits = { waveHeight: 'm' };
      const outputUnits = { waveHeight: 'm' };
      expect(transformSwellsOld(swells, inputUnits, outputUnits)).to.deep.equal({
        combined: {
          height: null,
          direction: null,
          period: null,
        },
        components: [...Array(6)].map(() => ({
          height: null,
          direction: null,
          period: null,
        })),
      });
    });

    it('treats non-numbers as null value', () => {
      const swells = {
        height: 'not-a-number',
        direction: 'not-a-number',
        period: 'not-a-number',
        swellWave1Height: 'not-a-number',
        swellWave1Direction: 'not-a-number',
        swellWave1Period: 'not-a-number',
        swellWave2Height: 'not-a-number',
        swellWave2Direction: 'not-a-number',
        swellWave2Period: 'not-a-number',
        swellWave3Height: 'not-a-number',
        swellWave3Direction: 'not-a-number',
        swellWave3Period: 'not-a-number',
        swellWave4Height: 'not-a-number',
        swellWave4Direction: 'not-a-number',
        swellWave4Period: 'not-a-number',
        swellWave5Height: 'not-a-number',
        swellWave5Direction: 'not-a-number',
        swellWave5Period: 'not-a-number',
        windWaveHeight: 'not-a-number',
        windWaveDirection: 'not-a-number',
        windWavePeriod: 'not-a-number',
      };
      const inputUnits = { waveHeight: 'm' };
      const outputUnits = { waveHeight: 'm' };
      expect(transformSwellsOld(swells, inputUnits, outputUnits)).to.deep.equal({
        combined: {
          height: null,
          direction: null,
          period: null,
        },
        components: [...Array(6)].map(() => ({
          height: null,
          direction: null,
          period: null,
        })),
      });
    });
  });
};

export default transformSwellsOldSpec;
