export interface SwellsUnits {
  waveHeight: string;
}

export interface SwellsRecord {
  run: number;
  forecastTime: number;

  height: number;
  direction: number;
  period: number;

  swellWave_1Height: number;
  swellWave_1Direction: number;
  swellWave_1Period: number;
  swellWave_1Spread: number;
  swellWave_1Impact: number;

  swellWave_2Height: number;
  swellWave_2Direction: number;
  swellWave_2Period: number;
  swellWave_2Spread: number;
  swellWave_2Impact: number;

  swellWave_3Height: number;
  swellWave_3Direction: number;
  swellWave_3Period: number;
  swellWave_3Spread: number;
  swellWave_3Impact: number;

  swellWave_4Height: number;
  swellWave_4Direction: number;
  swellWave_4Period: number;
  swellWave_4Spread: number;
  swellWave_4Impact: number;

  swellWave_5Height: number;
  swellWave_5Direction: number;
  swellWave_5Period: number;
  swellWave_5Spread: number;
  swellWave_5Impact: number;

  windWaveHeight: number;
  windWaveDirection: number;
  windWavePeriod: number;
  windWaveSpread: number;
  windWaveImpact: number;

  spectra1dEnergy?: number[];
  spectra1dDirection?: number[];
  spectra1dDirectionalSpread?: number[];
}

interface Swell {
  height: number;
  direction: number;
  period: number;
  spread?: number;
  impact?: number;
}

interface SwellWithEvent extends Swell {
  event: number;
}

interface SwellForecast {
  run: number;
  timestamp: number;
  swells: {
    combined: Swell;
    components: Swell[];
  };
}

interface SwellForecastWithEvents {
  run: number;
  timestamp: number;
  swells: {
    combined: Swell;
    components: SwellWithEvent[];
  };
}
