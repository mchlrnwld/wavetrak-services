import { expect } from 'chai';
import transformSwells from './transformSwells';

const transformSwellsSpec = () => {
  describe('transformSwells', () => {
    it('converts swells records into expected response shape and filters 0 swells', () => {
      const swells = {
        run: 0,
        forecastTime: 0,
        height: 1.32,
        direction: 205.7,
        period: 11,
        swellWave_1Height: 1.05,
        swellWave_1Direction: 174.88,
        swellWave_1Period: 8,
        swellWave_1Spread: 20.11,
        swellWave_1Impact: 0.5,
        swellWave_2Height: 0.73,
        swellWave_2Direction: 205.55,
        swellWave_2Period: 12,
        swellWave_2Spread: 20.12,
        swellWave_2Impact: 0.2,
        swellWave_3Height: 0.27,
        swellWave_3Direction: 205.63,
        swellWave_3Period: 16,
        swellWave_3Spread: 20.13,
        swellWave_3Impact: 0.1,
        swellWave_4Height: 0.06,
        swellWave_4Direction: 207.87,
        swellWave_4Period: 21,
        swellWave_4Spread: 20.14,
        swellWave_4Impact: 0.0,
        swellWave_5Height: 0.06,
        swellWave_5Direction: 159.14,
        swellWave_5Period: 13,
        swellWave_5Spread: 20.15,
        swellWave_5Impact: 0.0,
        windWaveHeight: 0,
        windWaveDirection: 159.14,
        windWavePeriod: 13,
        windWaveSpread: 20.16,
        windWaveImpact: 0.0,
        spectra1dEnergy: [...Array(27)].map((_, i) => i*0.01),
        spectra1dDirection: [...Array(27)].map((_, i) => i*3),
        spectra1dDirectionalSpread: [...Array(27)].map(() => 10),
      };
      expect(transformSwells(swells)).to.deep.equal({
        combined: {
          direction: swells.direction,
          height: swells.height,
          period: swells.period,
        },
        components: [
          {
            direction: swells.swellWave_1Direction,
            height: swells.swellWave_1Height,
            period: swells.swellWave_1Period,
            spread: swells.swellWave_1Spread,
            impact: swells.swellWave_1Impact,
          },
          {
            direction: swells.swellWave_2Direction,
            height: swells.swellWave_2Height,
            period: swells.swellWave_2Period,
            spread: swells.swellWave_2Spread,
            impact: swells.swellWave_2Impact,
          },
          {
            direction: swells.swellWave_3Direction,
            height: swells.swellWave_3Height,
            period: swells.swellWave_3Period,
            spread: swells.swellWave_3Spread,
            impact: swells.swellWave_3Impact,
          },
          {
            direction: swells.swellWave_4Direction,
            height: swells.swellWave_4Height,
            period: swells.swellWave_4Period,
            spread: swells.swellWave_4Spread,
            impact: swells.swellWave_4Impact,
          },
          {
            direction: swells.swellWave_5Direction,
            height: swells.swellWave_5Height,
            period: swells.swellWave_5Period,
            spread: swells.swellWave_5Spread,
            impact: swells.swellWave_5Impact,
          },
        ],
        spectra1d: {
          has: true,
          period: [...Array(27)].map((_, i) => i + 2),
          energy: swells.spectra1dEnergy,
          direction: swells.spectra1dDirection,
          directionalSpread: swells.spectra1dDirectionalSpread,
        }
      });
    });
  });
};

export default transformSwellsSpec;
