import { expect } from 'chai';
import convertSwellsUnits from './convertSwellsUnits';

const convertSwellsUnitsSpec = () => {
  describe('convertSwellsUnits', () => {
    it('converts swells units', () => {
      const swells = {
        combined: {
          height: 1.32,
          direction: 205.7,
          period: 11.41,
        },
        components: [
          {
            height: 1.05,
            direction: 174.88,
            period: 8.08,
            spread: 20.11,
            impact: 0.5,
            event: 0,
          },
          {
            height: 0.73,
            direction: 205.55,
            period: 11.54,
            spread: 20.12,
            impact: 0.2,
            event: 1,
          },
          {
            height: 0.27,
            direction: 205.63,
            period: 15.92,
            spread: 20.13,
            impact: 0.1,
            event: 2,
          },
          {
            height: 0.06,
            direction: 207.87,
            period: 21.37,
            spread: 20.14,
            impact: 0.0,
            event: 3,
          },
          {
            height: 0.06,
            direction: 159.14,
            period: 13.4,
            spread: 20.15,
            impact: 0.0,
            event: 4,
          },
          {
            height: 0.06,
            direction: 159.14,
            period: 13.4,
            spread: 20.16,
            impact: 0.0,
            event: 5,
          },
        ],
      };
      const inputUnits = { waveHeight: 'm' };
      const outputUnits = { waveHeight: 'ft' };
      expect(convertSwellsUnits(swells, inputUnits, outputUnits)).to.deep.equal({
        combined: {
          direction: 205.7,
          height: 4.33071,
          period: 11.41,
        },
        components: [
          {
            direction: 174.88,
            height: 3.44488,
            period: 8.08,
            spread: 20.11,
            impact: 0.5,
            event: 0,
          },
          {
            direction: 205.55,
            height: 2.39501,
            period: 11.54,
            spread: 20.12,
            impact: 0.2,
            event: 1,
          },
          {
            direction: 205.63,
            height: 0.88583,
            period: 15.92,
            spread: 20.13,
            impact: 0.1,
            event: 2,
          },
          {
            direction: 207.87,
            height: 0.19685,
            period: 21.37,
            spread: 20.14,
            impact: 0.0,
            event: 3,
          },
          {
            direction: 159.14,
            height: 0.19685,
            period: 13.4,
            spread: 20.15,
            impact: 0.0,
            event: 4,
          },
          {
            direction: 159.14,
            height: 0.19685,
            period: 13.4,
            spread: 20.16,
            impact: 0.0,
            event: 5,
          },
        ],
      });
    });
  });
};

export default convertSwellsUnitsSpec;
