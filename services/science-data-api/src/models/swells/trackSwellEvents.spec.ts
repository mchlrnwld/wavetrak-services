import { expect } from 'chai';
import swellsForecastsFixture from '../../fixtures/swellsForecasts.json';
import trackSwellEvents, {
  calculateDirectionDifference,
  matchSwellEvents,
} from './trackSwellEvents';

const swellsWithoutEvents = swellsForecastsFixture.map(({ timestamp, swells: { components } }) => ({
  timestamp,
  swells: {
    components: components.map(({ height, period, direction }) => ({
      height,
      period,
      direction,
    })),
  },
}));

const trackSwellEventsSpec = () => {
  describe('trackSwellEvents', () => {
    it('tracks swell events over time', () => {
      expect(trackSwellEvents(swellsWithoutEvents)).to.deep.equal(swellsForecastsFixture);
    });

    describe('calculateDirectionDifference', () => {
      const epsilon = 0.000001;
      it('calculates absolute difference for directions in (-∞,∞)', () => {
        expect(calculateDirectionDifference(-180, 540)).to.be.closeTo(0, epsilon);
        expect(calculateDirectionDifference(-20.5, 351.1)).to.be.closeTo(11.6, epsilon);
      });

      it('accounts for directions crossing 0°', () => {
        expect(calculateDirectionDifference(359.1, 1.2)).to.be.closeTo(2.1, epsilon);
        expect(calculateDirectionDifference(-0.9, 1.2)).to.be.closeTo(2.1, epsilon);
      });
    });

    describe('matchSwellEvents', () => {
      it('matches current swells with previous swell events by comparing height, direction, and period', () => {
        const previousSwellEvents = [
          {
            swell: {
              height: 3.22,
              period: 16,
              direction: 181.41,
            },
            event: 0,
          },
          {
            swell: {
              height: 1.31,
              period: 6,
              direction: 274.22,
            },
            event: 1,
          },
          {
            swell: {
              height: 0.89,
              period: 11,
              direction: 168.75,
            },
            event: 2,
          },
          {
            swell: {
              height: 0.9,
              period: 11,
              direction: 168.75,
            },
            event: 3,
          },
        ];
        const currentSwells = [
          {
            // Matches event 0
            height: 3.22,
            period: 16,
            direction: 181.41,
          },
          {
            height: 1.8,
            period: 7,
            direction: 272.81,
            // Matches event 1
          },
          {
            height: 1.81,
            period: 7,
            direction: 272.81,
            // Matches event 1 (removed by scoring)
          },
          {
            height: 0.89,
            period: 11,
            direction: 168.75,
            // Matches event 2 and 3 (event 2 preferred by scoring)
          },
          {
            height: 0.59,
            period: 9,
            direction: 270,
            // Not a match
          },
        ];
        expect(matchSwellEvents(currentSwells, previousSwellEvents)).to.deep.equal([
          {
            swell: {
              height: 3.22,
              period: 16,
              direction: 181.41,
            },
            score: 0,
            event: 0,
          },
          {
            swell: {
              height: 0.89,
              period: 11,
              direction: 168.75,
            },
            score: 0,
            event: 2,
          },
          {
            swell: {
              height: 1.8,
              period: 7,
              direction: 272.81,
            },
            score: 18.660000000000025,
            event: 1,
          },
        ]);
      });
    });
  });
};

export default trackSwellEventsSpec;
