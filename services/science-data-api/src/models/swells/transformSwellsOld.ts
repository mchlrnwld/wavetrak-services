import round from 'lodash/round';
import isNumber from 'lodash/isNumber';
import { convertUnits } from '../../common/units';
import { SwellsRecord, SwellsUnits } from './index.d';

const nullValueCheck = (value: number, transform: Function): number =>
  !isNumber(value) || value === 9999 ? null : transform(value);
const roundTo5 = (value: number): number => round(value, 5);

const transformSwellsOld = (
  swellsRecord: SwellsRecord,
  inputUnits: SwellsUnits,
  outputUnits: SwellsUnits
) => {
  const { height, period, direction } = swellsRecord;
  const convertWaveHeight = convertUnits(inputUnits.waveHeight, outputUnits.waveHeight);

  return {
    combined: {
      height: nullValueCheck(height, v => roundTo5(convertWaveHeight(v))),
      direction: nullValueCheck(direction, roundTo5),
      period: nullValueCheck(period, roundTo5),
    },
    components: [...Array(6)].map((_, i) => {
      const prefix = i < 5 ? `swellWave${i + 1}` : 'windWave';
      return {
        height: nullValueCheck(swellsRecord[`${prefix}Height`], v =>
          roundTo5(convertWaveHeight(v))
        ),
        direction: nullValueCheck(swellsRecord[`${prefix}Direction`], roundTo5),
        period: nullValueCheck(swellsRecord[`${prefix}Period`], roundTo5),
      };
    }),
  };
};

export default transformSwellsOld;
