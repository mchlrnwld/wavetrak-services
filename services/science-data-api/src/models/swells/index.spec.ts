import convertSwellsUnits from './convertSwellsUnits.spec';
import trackSwellEventsSpec from './trackSwellEvents.spec';
import transformSwellsSpec from './transformSwells.spec';
import transformSwellsOldSpec from './transformSwellsOld.spec';

describe('models / swells', () => {
  convertSwellsUnits();
  trackSwellEventsSpec();
  transformSwellsSpec();
  transformSwellsOldSpec();
});
