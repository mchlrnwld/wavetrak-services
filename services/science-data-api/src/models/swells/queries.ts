import Knex, { QueryBuilder } from 'knex';
import nr from 'newrelic';
import db from '../../db';
import { runTimeToUnix, runTimeToISOString } from '../../db/runTimeConversions';
import unixTimeToISOString from '../../db/unixTimeToISOString';
import { SwellsRecord } from './index.d';

const table = 'swells';

// eslint-disable-next-line import/prefer-default-export
export const surfSpotSwells = (
  { agency, model, run, grid, pointOfInterestId, start, end },
  client: Knex = db.replica.client,
  cache = false,
): QueryBuilder<SwellsRecord[]> =>
  nr.startSegment(`select:${table}`, false, () => {
    const isoRunTime = runTimeToISOString(run);
    const whereEqualsPredicate: any = { pointOfInterestId, agency, model, grid };

    const fields = [
      'run',
      'forecast_time',
      'height',
      'period',
      'direction',
      'swell_wave_1_height',
      'swell_wave_1_period',
      'swell_wave_1_direction',
      'swell_wave_1_spread',
      'swell_wave_1_impact',
      'swell_wave_2_height',
      'swell_wave_2_period',
      'swell_wave_2_direction',
      'swell_wave_2_spread',
      'swell_wave_2_impact',
      'swell_wave_3_height',
      'swell_wave_3_period',
      'swell_wave_3_direction',
      'swell_wave_3_spread',
      'swell_wave_3_impact',
      'swell_wave_4_height',
      'swell_wave_4_period',
      'swell_wave_4_direction',
      'swell_wave_4_spread',
      'swell_wave_4_impact',
      'swell_wave_5_height',
      'swell_wave_5_period',
      'swell_wave_5_direction',
      'swell_wave_5_spread',
      'swell_wave_5_impact',
      'wind_wave_height',
      'wind_wave_period',
      'wind_wave_direction',
      'wind_wave_spread',
      'wind_wave_impact',
      'spectra1d_energy',
      'spectra1d_direction',
      'spectra1d_directional_spread',
    ];

    let query = client(table)
      .select(fields)
      .where(whereEqualsPredicate)
      .orderBy('forecast_time');

    if (start) {
      query = query.andWhere('forecast_time', '>=', unixTimeToISOString(start));
    } else {
      query = query.andWhere('forecast_time', '>=', isoRunTime);
    }

    if (end) {
      query = query.andWhere('forecast_time', '<=', unixTimeToISOString(end));
    }

    const unixRunTime = runTimeToUnix(run);

    if (!start || start >= unixRunTime) {
      query = query.andWhere('run', isoRunTime);
      return cache ? query.cache() : query;
    }

    // Start is provided and is less than `run`, so `run` should be considered a max
    // run time allowed, and need the query to allow run times less than the passed
    // `run` for forecast hours prior to `run`. But there may be several such runs
    // available; so use window function to limit to the most up-to-date run for
    // each hour we want.
    query = query
      .andWhere(function() {
        this.where('forecast_time', '<', isoRunTime).orWhere('run', isoRunTime);
      })
      .select(
        client.raw('ROW_NUMBER() OVER (PARTITION BY forecast_time ORDER BY run DESC) AS priority'),
      );

    query = client
      .select(fields)
      .from(query.as('t1'))
      .where('priority', 1)
      .orderBy('forecast_time');

    return cache ? query.cache() : query;
  });
