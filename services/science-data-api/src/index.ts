import 'newrelic';
import app from './server';
import log from './common/logger';
import { initializePostgresDB } from './db';
// import { setupPactServers } from './pact/providers';

const start = async () => {
  // if (process.env.NODE_ENV === 'test') {
  //   try {
  //     await setupPactServers();
  //   } catch (err) {
  //     log.error({
  //       message: 'App Initialization failed. Can\'t setup Pact servers',
  //       stack: err,
  //     });
  //   }
  // }

  try {
    await Promise.all([initializePostgresDB()]);
    app();
  } catch (err) {
    log.error({
      message: `App Initialization failed: ${err.message}`,
      stack: err,
    });
  }
};

start();
