import convert from 'convert-units';
import curry from 'lodash/curry';

// required to use convert-units API
const ABBR_MAP = {
  mb: 'hPa',
  'mi/h': 'm/h',
};

const mapConversionUnits = inputUnits => ABBR_MAP[inputUnits] || inputUnits;

// eslint-disable-next-line import/prefer-default-export
export const convertUnits = curry((from, to, value) =>
  convert(value)
    .from(mapConversionUnits(from))
    .to(mapConversionUnits(to)),
);
