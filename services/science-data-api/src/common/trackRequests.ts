import log from './logger';

const trackRequests = (req, _res, next) => {
  log.trace({
    action: req.path,
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default trackRequests;
