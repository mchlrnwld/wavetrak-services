class ValidationError extends Error {
  status: number;

  body?: any;

  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, ValidationError);
    this.name = 'ValidationError';
    this.status = 400;
  }
}

export const createValidationError = (message: string, errors?: any) => {
  const validationError = new ValidationError(message);
  if (errors) {
    validationError.body = { errors };
  }
  return validationError;
};

export default ValidationError;
