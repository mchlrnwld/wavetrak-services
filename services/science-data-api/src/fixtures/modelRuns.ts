export default [
  {
    agency: 'NOAA',
    model: 'GFS',
    run: 2018113000,
    status: 'OFFLINE',
    type: 'POINT_OF_INTEREST',
  },
  {
    agency: 'NOAA',
    model: 'GFS',
    run: 2018113006,
    status: 'ONLINE',
    type: 'POINT_OF_INTEREST',
  },
];
