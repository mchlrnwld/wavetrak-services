import { expect } from 'chai';
import transformVectorsToObjects from './transformVectorsToObjects';

describe('external / smdb', () => {
  describe('transformVectorsToObjects', () => {
    it('transform a headers vector and an array of values vectors into an array of objects', () => {
      expect(
        transformVectorsToObjects({
          headers: ['timestamp', 'visibility', 'dewpoint', 'windU', 'windV'],
          values: [
            [1548288000, 1, 2, 3, 4],
            [1548298800, 5, 6, 7, 8],
            [1548299600, 9, 10, 11, 12],
          ],
        })
      ).to.deep.equal([
        {
          timestamp: 1548288000,
          visibility: 1,
          dewpoint: 2,
          windU: 3,
          windV: 4,
        },
        {
          timestamp: 1548298800,
          visibility: 5,
          dewpoint: 6,
          windU: 7,
          windV: 8,
        },
        {
          timestamp: 1548299600,
          visibility: 9,
          dewpoint: 10,
          windU: 11,
          windV: 12,
        },
      ]);
    });
  });
});
