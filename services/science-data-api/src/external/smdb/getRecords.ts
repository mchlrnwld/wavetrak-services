import axios from 'axios';
import config from '../../config';
import transformVectorsToObjects from './transformVectorsToObjects';

interface RecordsParams {
  agency: string;
  model: string;
  run: string;
  grid: string;
  lat: number;
  lon: number;
  start: number;
  end: number;
}

const getRecords = async (params: RecordsParams) => {
  const { data } = await axios.get(`${config.SCIENCE_MODELS_DB}/records`, { params });
  return transformVectorsToObjects(data);
};

export default getRecords;
