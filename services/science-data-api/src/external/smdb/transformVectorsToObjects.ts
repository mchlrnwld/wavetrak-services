import { camelCase, fromPairs, zip } from 'lodash';

const transformVectorsToObjects = ({ headers, values }) => {
  const transformedHeaders = headers.map(camelCase);
  return values.map(v => fromPairs(zip(transformedHeaders, v)));
};

export default transformVectorsToObjects;
