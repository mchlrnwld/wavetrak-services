import axios from 'axios';
import config from '../../config';

export const getLatestSpotReport = async (spotId: string) => {
  const { data } = await axios.get(
    `${config.REPORTS_API}/admin/reports/spot?spotId=${spotId}&limit=1`,
  );
  return data.spotReports[0];
};
