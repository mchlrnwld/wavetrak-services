import axios from 'axios';
import config from '../../config';

export const getSpot = async (spotId: string) => {
  const { data: spot } = await axios.get(`${config.SPOTS_API}/admin/spots/${spotId}`);
  return spot;
};
