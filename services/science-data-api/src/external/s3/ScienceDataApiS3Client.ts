import { GetObjectCommand, ListObjectsV2Command, S3Client } from '@aws-sdk/client-s3';
import { Readable } from 'stream';
import config from '../../config';

export interface ObjectMetadata {
  Key?: string;
}

export default class ScienceDataApiS3Client {
  readonly client: S3Client;

  constructor() {
    // The S3 endpoint is only used for integration testing https://docs.localstack.cloud/aws/s3/
    this.client = new S3Client({
      endpoint: process.env.S3_ENDPOINT,
      region: config.AWS_DEFAULT_REGION,
    });
  }

  async listObjects(bucket: string, prefix: string): Promise<ObjectMetadata[]> {
    const res = await this.client.send(
      new ListObjectsV2Command({
        Bucket: bucket,
        Prefix: prefix,
      }),
    );
    return res.Contents;
  }

  async readObjectContentsAsJson(bucket: string, key: string): Promise<any> {
    const response = await this.client.send(new GetObjectCommand({ Bucket: bucket, Key: key }));
    const chunks = [];
    return new Promise((resolve, reject) => {
      if (response.Body instanceof Readable) {
        response.Body.on('data', chunk => chunks.push(Buffer.from(chunk)));
        response.Body.on('error', err => reject(err));
        response.Body.on('end', () => resolve(JSON.parse(Buffer.concat(chunks).toString('utf8'))));
      }
    });
  }
}
