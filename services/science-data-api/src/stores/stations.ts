export default {
  storedUnits: {
    waveHeight: 'm',
    windSpeed: 'm/s',
    waterTemperature: 'C',
    airTemperature: 'C',
    dewPoint: 'C',
    pressure: 'mb',
  },
};
