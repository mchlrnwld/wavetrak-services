export default {
  NOAA: {
    GFS: {
      timeRanges: [
        {
          start: -24 * 60 * 60,
          end: 120 * 60 * 60,
          interval: 1 * 60 * 60,
        },
        {
          start: 120 * 60 * 60,
          end: 384 * 60 * 60,
          interval: 3 * 60 * 60,
        },
      ],
      storedUnits: {
        windSpeed: 'm/s',
        temperature: 'C',
        pressure: 'mb',
        visibility: 'm',
        precipitation: 'mm', // (kg/m^2)
      },
    },
    NAM: {
      timeRanges: [
        {
          start: 0 * 60 * 60,
          end: 60 * 60 * 60,
          interval: 1 * 60 * 60,
        },
      ],
      storedUnits: {
        windSpeed: 'm/s',
        temperature: 'C',
        pressure: 'mb',
        visibility: 'm',
        precipitation: 'mm', // (kg/m^2)
      },
    },
    'WW3-Ensemble': {
      timeRanges: [
        {
          start: -24 * 60 * 60,
          end: 240 * 60 * 60,
          interval: 6 * 60 * 60,
        },
      ],
      storedUnits: {
        windSpeed: 'm/s',
        waveHeight: 'm',
      },
    },
    'GEFS-Wave': {
      timeRanges: [
        {
          start: 0,
          end: 240 * 60 * 60,
          interval: 3 * 60 * 60,
        },
        {
          start: 240 * 60 * 60,
          end: 384 * 60 * 60,
          interval: 6 * 60 * 60,
        },
      ],
      storedUnits: {
        windSpeed: 'm/s',
        waveHeight: 'm',
      },
    },
    WW3: {
      timeRanges: [
        {
          start: 0,
          end: 180 * 60 * 60,
          interval: 3 * 60 * 60,
        },
      ],
      storedUnits: {
        waveHeight: 'm',
      },
    },
  },
  Wavetrak: {
    'Lotus-WW3': {
      timeRanges: [
        {
          start: -24 * 60 * 60,
          end: 384 * 60 * 60,
          interval: 3 * 60 * 60,
        },
      ],
      storedUnits: {
        waveHeight: 'm',
      },
    },
    WRF: {
      timeRanges: [
        {
          start: 0,
          end: 60 * 60 * 60,
          interval: 1 * 60 * 60,
        },
      ],
      storedUnits: {
        windSpeed: 'm/s',
        temperature: 'C',
        pressure: 'mb',
        visibility: 'm',
        precipitation: 'mm', // (kg/m^2)
      },
    },
  },
};
