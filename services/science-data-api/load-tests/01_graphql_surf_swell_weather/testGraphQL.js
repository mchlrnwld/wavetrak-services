import { check } from 'k6';
import http from 'k6/http';
import { Rate, Trend } from 'k6/metrics';

export const rateErrors = new Rate('errors');
export const trendDuration = new Trend('duration');

export const options = {
  thresholds: {
    errors: [{ threshold: 'rate<0.01', abortOnFail: true }],
    duration: [{ threshold: 'avg<100', abortOnFail: true }],
  },
  duration: '1m',
  vus: 10,
  rps: 20,
};

const query = `
query surfSpot {
  surfSpotForecasts(pointOfInterestId: "${__ENV.ID}"){
    pointOfInterestId
    surf(agency: "Wavetrak", model: "Lotus-WW3", run: ${__ENV.RUN}, grid: "${__ENV.GRID}") {
      model {
        agency
        model
        run
      }
      units {
        waveHeight
      }
      data {
        surf {
          waveDirection
          waveEnergy
          breakingWaveHeightMin
          breakingWaveHeightMax
          breakingWaveHeightAlgorithm
        }
      }
    }
    swells(agency: "Wavetrak", model: "Lotus-WW3", run: ${__ENV.RUN}, grid: "${__ENV.GRID}"){
      model {
        agency
        model
        run
      }
      units {
        waveHeight
      }
      data {
        swells {
          combined {
            direction
            period
            spread
            height
          }
          components{
            direction
            period
            spread
            height
          }
        }
      }
    }
    weather(agency: "Wavetrak", model: "Lotus-WW3", run: ${__ENV.RUN}, grid: "${__ENV.GRID}"){
      model {
        agency
        model
        run
      }
    }
  }
}`;

export default () => {
  const headers = {
    'Content-Type': 'application/json',
  };
  const response = http.post(
    `http://${__ENV.HOST}/graphql`,
    JSON.stringify({ query }),
    { headers },
  );

  trendDuration.add(response.timings.duration);

  const validResponse = check(response, {
    'status is 200': r => r.status === 200,
    'Valid surf and swell data was returned in the response': r => JSON.parse(r.body).data.surfSpotForecasts.surf !== null && JSON.parse(r.body).data.surfSpotForecasts.swells !== null,
  });

  if (!validResponse) {
    rateErrors.add(1);
  }
};
