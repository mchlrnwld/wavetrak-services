# Load Tests

Load tests using [k6](https://k6.io/).

## Quickstart

Use the IP and port of a single instance of the service as the `HOST` variable. The `--out` parameter optionally logs granular results to a file.

```sh
$ k6 run -e HOST=10.12.75.16:33101 --out json=results.json test.js
```

For GraphQL test:

```sh
$ k6 run -e HOST=10.12.75.16:33101 -e RUN=2019103100 -e GRID=AUS_E_3m -e ID=1684f484-1e02-4fcc-abcb-c8105711c6e1 --out json=results.json testGraphQL.js
```

## Tips / Troubleshooting
If you are using mac os, and are load testing with increasingly higher RPS and VUs, you may hit encounter the following error, or something very similar:
```
socket: too many open files
````
This error is due to default file descriptor limit on mac os, which can be changed. This can be fixed by following these steps:

https://gist.github.com/tombigel/d503800a282fcadbee14b537735d202c
