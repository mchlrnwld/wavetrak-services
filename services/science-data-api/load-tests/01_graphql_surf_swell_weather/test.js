import { check } from 'k6';
import http from 'k6/http';
import { Rate, Trend } from 'k6/metrics';

export const rateErrors = new Rate('errors');
export const trendDuration = new Trend('duration');

export const options = {
  thresholds: {
    errors: [{ threshold: 'rate<0.01', abortOnFail: true }],
    duration: [{ threshold: 'avg<1000', abortOnFail: true }],
  },
  duration: '1m',
  vus: 100,
  rps: 50,
};

export default () => {
  const lat = (Math.random() * 180) - 90;
  const lon = Math.random();
  const res = http.get(`http://${__ENV.HOST}/forecasts/weather?agency=NOAA&model=GFS&grid=0p25&lat=${lat}&lon=${lon}`);

  trendDuration.add(res.timings.duration);

  const status200 = check(res, {
    'status is 200': r => r.status === 200,
  });

  if (!status200) {
    rateErrors.add(1);
  }
};
