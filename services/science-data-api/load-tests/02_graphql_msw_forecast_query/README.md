# Load Tests

Load tests using [k6](https://k6.io/).

## Quickstart

Use the IP and port of a single instance of the service as the `HOST` variable. Edit script.js to set the host, and adjust the import from `pointsOfInterest.js` according to the environment being benchmarked

For GraphQL test:

```sh
$ ./run.sh 10 30s all
```

The parameters are: k6 VUs, length of test, and a name to append to the JSON results file.

## Tips / Troubleshooting
If you are using mac os, and are load testing with increasingly higher RPS and VUs, you may hit encounter the following error, or something very similar:
```
socket: too many open files
````
This error is due to default file descriptor limit on mac os, which can be changed. This can be fixed by following these steps:

https://gist.github.com/tombigel/d503800a282fcadbee14b537735d202c
