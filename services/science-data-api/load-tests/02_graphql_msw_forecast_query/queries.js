export const POINT_OF_INTEREST_GRAPH_QUERY = `
query($id: String!) {
	pointOfInterest(pointOfInterestId: $id) {
		gridPoints {
			optimalSwellDirection
			offshoreDirection
			grid {
				agency
				model
				grid
				description
				resolution
			}
		}
	}
}`;

export const FORECAST_GRAPH_QUERY = `
query(
	$pointOfInterestId: String!
	$agency: String!
	$model: String!
	$grid: String!
	$interval: Int=10800
	$start: Int!
	$end: Int!
	$sst_start: Int!
) {
	surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
		surf(
			agency: $agency
			model: $model
			grid: $grid
			interval: $interval
			start: $start
			end: $end
			waveHeight: "m"
		) {
			data {
				timestamp
				surf {
					breakingWaveHeightMin,breakingWaveHeightMax
					hasSpectra
				}
			}
		}
		swells(
			agency: $agency
			model: $model
			grid: $grid
			interval: $interval
			start: $start
			end: $end
			waveHeight: "m"
		) {
			data {
				timestamp,run
				swells {
					combined {height,direction,period}
					components {height,direction,period,impact}
				}
			}
		}
		wind(
			agency: "NOAA"
			model: "GFS"
			grid: "0p25"
			interval: $interval
			start: $start
			end: $end
			windSpeed: "m/s"
		) {
			data {
				timestamp,run
				wind {speed,direction,gust}
			}
		}
		weather(
			agency: "NOAA"
			model: "GFS"
			grid: "0p25"
			interval: $interval
			start: $start
			end: $end
			temperature: "C"
			pressure: "mb"
		) {
			data {
				timestamp
				weather {temperature,conditions,pressure,humidity}
			}
		}
		sst (
			agency: "NASA"
			model: "MUR-SST"
			grid: "0p01"
			temperature: "C"
			interval: 86400
			start: $sst_start
			end: $end
		) {
			data {
				timestamp
				sst {
					temperature
				}
			}
		}
		swellProbabilities (
			agency: "NOAA"
			model: "WW3-Ensemble"
			grid: "glo_30m"
			interval: $interval
			start: $start
			end: $end
		) {
			data {
				timestamp
				probability
			}
		}
	}
}`;
