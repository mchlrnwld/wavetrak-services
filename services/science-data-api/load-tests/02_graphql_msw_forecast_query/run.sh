#!/bin/env bash

VUS="$1"
DURATION="$2"
NAME="$3"

k6 run \
	--duration "$DURATION" \
	--vus "$VUS" \
	--summary-export="export_${NAME}_${VUS}_${DURATION}.json" \
	script.js
