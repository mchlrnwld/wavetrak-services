import { sleep, check } from 'k6';
import http from 'k6/http';
import { Rate, Trend } from 'k6/metrics';

import { FORECAST_GRAPH_QUERY } from './queries.js'
import { pointsOfInterest_prod as pointsOfInterest } from './pointsOfInterest.js'

export const rateErrors = new Rate('errors');

const start = Math.floor(Date.now()/(1000*24*3600))*24*3600;
const end = start + 16*24*3600;
const sst_start = start - 7*24*3600;

export const options = {
  thresholds: {
    errors: [{ threshold: 'rate<0.01', abortOnFail: true }],
  },
};

export default function() {
  const url = 'https://services.surfline.com/graphql?api_key=temporary_and_insecure_wavetrak_api_key';
  const params = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  // Distribute the VUs around the POI IDs, and choose the next one each iteration
  const index = (Math.floor(pointsOfInterest.length*(__VU - 1)/options.vus) + __ITER) % pointsOfInterest.length
  const id = pointsOfInterest[index];

  // Make main forecast request
  const response = http.post(url, JSON.stringify({
    query: FORECAST_GRAPH_QUERY,
    variables: {
      pointOfInterestId: id,
      agency: 'Wavetrak',
      model: 'Lotus-WW3',
      grid: 'GLOB_15m',
      interval: 3600,
      start: start,
      end: end,
      sst_start: sst_start,
    },
  }), params);

  const validResponse = check(response, {
    'status is 200': r => r.status === 200,
    'Valid surf and swell data was returned in the response': r => r.json().data.surfSpotForecasts.surf !== null && r.json().data.surfSpotForecasts.swells !== null,
  });

  if (!validResponse) {
    rateErrors.add(1);
  }

  sleep(0.1);
}
