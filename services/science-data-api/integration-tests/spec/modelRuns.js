import { expect } from 'chai';
import fetch from 'node-fetch';
import deepFreeze from 'deep-freeze';
import { setupPostgres, teardownPostgres } from './helpers/postgres';
import config from './config';

const findModelRun = (query, modelRuns) => modelRuns.find(({
  agency,
  model,
  run,
}) => (
  agency === query.agency
  && model === query.model
  && Number(run) === Number(query.run)
));

const modelRunsSpec = () => {
  describe('/model_runs', () => {
    let psql;

    const models = deepFreeze([
      { agency: 'NOAA', model: 'GFS' },
      { agency: 'Wavetrak', model: 'Proteus-WW3' },
    ]);
    const existingModelRuns = deepFreeze([
      {
        agency: 'NOAA',
        model: 'GFS',
        run: new Date(2018, 10, 30, 0),
        status: 'OFFLINE',
      },
      {
        agency: 'NOAA',
        model: 'GFS',
        run: new Date(2018, 10, 30, 6),
        status: 'ONLINE',
      },
      {
        agency: 'Wavetrak',
        model: 'Proteus-WW3',
        run: new Date(2018, 10, 30, 6),
        status: 'ONLINE',
      },
      {
        agency: 'NOAA',
        model: 'GFS',
        run: new Date(2018, 10, 30, 12),
        status: 'PENDING',
      },
    ]);

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await Promise.all(models.map(model => psql('models').insert(model)));
      await Promise.all(
        existingModelRuns.map(modelRun => psql('model_runs_point_of_interest').insert(modelRun)),
      );
    });

    afterEach(async () => {
      await psql('model_runs_point_of_interest').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('GET', () => {
      it('queries online model runs by default', async () => {
        const response = await fetch(`${config.SCIENCE_DATA_SERVICE}/model_runs`);
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.have.length(2);
        expect(body.every(({ status }) => status === 'ONLINE')).to.be.true();
      });

      it('queries model runs by agency, model, and run', async () => {
        const response = await fetch(
          `${config.SCIENCE_DATA_SERVICE}/model_runs?agency=NOAA&model=GFS&run=2018113006`,
        );
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.have.length(1);
        expect(body[0]).to.deep.equal({
          agency: 'NOAA',
          model: 'GFS',
          run: 2018113006,
          status: 'ONLINE',
          type: 'POINT_OF_INTEREST',
        });
      });

      it('can query model runs by a list of statuses', async () => {
        const response = await fetch(
          `${config.SCIENCE_DATA_SERVICE}/model_runs?status=ONLINE,PENDING`,
        );
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.have.length(3);

        const noaaGFS6 = findModelRun(
          {
            agency: 'NOAA',
            model: 'GFS',
            run: 2018113006,
          },
          body,
        );
        expect(noaaGFS6).to.be.ok();
        expect(noaaGFS6.status).to.equal('ONLINE');

        const proteusWW3 = findModelRun(
          {
            agency: 'Wavetrak',
            model: 'Proteus-WW3',
            run: 2018113006,
          },
          body,
        );
        expect(proteusWW3).to.be.ok();
        expect(proteusWW3.status).to.equal('ONLINE');

        const noaaGFS12 = findModelRun(
          {
            agency: 'NOAA',
            model: 'GFS',
            run: 2018113012,
          },
          body,
        );
        expect(noaaGFS12).to.be.ok();
        expect(noaaGFS12.status).to.equal('PENDING');
      });
    });

    describe('PUT', () => {
      it('upserts a list of model runs with agency, model, run as a key', async () => {
        const body = JSON.stringify([
          // Update status to OFFLINE
          {
            agency: 'NOAA',
            model: 'GFS',
            run: 2018113006,
            status: 'OFFLINE',
            type: 'POINT_OF_INTEREST',
          },
          // Update status to ONLINE
          {
            agency: 'NOAA',
            model: 'GFS',
            run: 2018113012,
            status: 'ONLINE',
            type: 'POINT_OF_INTEREST',
          },
          // Add PENDING model run
          {
            agency: 'Wavetrak',
            model: 'Proteus-WW3',
            run: 2018113012,
            status: 'PENDING',
            type: 'POINT_OF_INTEREST',
          },
        ]);
        const response = await fetch(`${config.SCIENCE_DATA_SERVICE}/model_runs`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body,
        });
        expect(response.status).to.equal(200);
        const modelRunsData = await psql('model_runs_point_of_interest').select(
          'agency',
          'model',
          'run',
          'status',
        );
        expect(modelRunsData).to.be.ok();
        expect(modelRunsData).to.have.length(5);

        const noaaGFS0 = findModelRun(
          {
            agency: 'NOAA',
            model: 'GFS',
            run: new Date(2018, 10, 30, 0),
          },
          modelRunsData,
        );
        expect(noaaGFS0).to.be.ok();
        expect(noaaGFS0.status).to.equal('OFFLINE');

        const proteusWW36 = findModelRun(
          {
            agency: 'Wavetrak',
            model: 'Proteus-WW3',
            run: new Date(2018, 10, 30, 6),
          },
          modelRunsData,
        );
        expect(proteusWW36).to.be.ok();
        expect(proteusWW36.status).to.equal('ONLINE');

        const noaaGFS6 = findModelRun(
          {
            agency: 'NOAA',
            model: 'GFS',
            run: new Date(2018, 10, 30, 6),
          },
          modelRunsData,
        );
        expect(noaaGFS6).to.be.ok();
        expect(noaaGFS6.status).to.equal('OFFLINE');

        const noaaGFS12 = findModelRun(
          {
            agency: 'NOAA',
            model: 'GFS',
            run: new Date(2018, 10, 30, 12),
          },
          modelRunsData,
        );
        expect(noaaGFS12).to.be.ok();
        expect(noaaGFS12.status).to.equal('ONLINE');

        const proteusWW312 = findModelRun(
          {
            agency: 'Wavetrak',
            model: 'Proteus-WW3',
            run: new Date(2018, 10, 30, 12),
          },
          modelRunsData,
        );
        expect(proteusWW312).to.be.ok();
        expect(proteusWW312.status).to.equal('PENDING');
      });

      it('returns 400 when trying to upsert invalid model runs', async () => {
        const response = await fetch(`${config.SCIENCE_DATA_SERVICE}/model_runs`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify([
            {
              agency: 'NOAA',
              model: 'GFS',
              run: 0,
              status: 'BAD_STATUS',
              type: 'POINT_OF_INTEREST',
            },
          ]),
        });
        expect(response.status).to.equal(400);

        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.deep.equal({
          message: 'Error validating model runs',
          errors: [
            [
              '"run" must be larger than or equal to 1970010100',
              '"status" must be one of [ONLINE, PENDING, OFFLINE]',
            ],
          ],
        });

        const modelRunsData = await psql('model_runs_point_of_interest').select(
          'agency',
          'model',
          'run',
          'status',
        );
        expect(modelRunsData).to.be.ok();
        expect(modelRunsData).to.have.length(4);
        existingModelRuns.forEach((mr) => {
          expect(findModelRun(mr, modelRunsData)).to.be.ok();
        });
      });
    });
  });
};

export default modelRunsSpec;
