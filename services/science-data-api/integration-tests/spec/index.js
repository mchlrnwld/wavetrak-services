import chai from 'chai';
import dirtyChai from 'dirty-chai';
import modelRuns from './modelRuns';
import pointsOfInterestGridPoints from './pointsOfInterestGridPoints';
import graphQL from './graphQL';

chai.use(dirtyChai);

describe('Science Data Service', () => {
  modelRuns();
  pointsOfInterestGridPoints();
  graphQL();
});
