export default {
  MAPTILER_TILEDB_BUCKET: process.env.MAPTILER_TILEDB_BUCKET,
  S3_ENDPOINT: process.env.S3_ENDPOINT,
  SCIENCE_DATA_SERVICE: 'http://application',
};
