import { expect } from 'chai';
import { removePOIGridPoints, upsertPOIGridPoints } from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';

const pointsOfInterestGridPointSpec = () => {
  describe('Points of Interest Grid Points', () => {
    let psql;
    let pointOfInterestId;

    const pointOfInterest = {
      name: 'POI1',
      latitude: 0,
      longitude: 0,
    };
    const model = {
      agency: 'Wavetrak',
      model: 'Lotus-WW3',
    };
    const modelGrids = [
      {
        ...model,
        grid: 'UK_3m',
      },
      {
        ...model,
        grid: 'GLOB_15m',
      },
    ];
    const pointOfInterestGridPoint = {
      ...modelGrids[1],
      latitude: 0,
      longitude: 0,
    };

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      const [row] = await psql('pointsOfInterest')
        .insert(pointOfInterest)
        .returning(['id']);
      pointOfInterestId = row.id;
      await psql('models').insert(model);
      await Promise.all(modelGrids.map(point => psql('modelGrids').insert(point)));
      await psql('pointsOfInterestGridPoints').insert({
        pointOfInterestId,
        ...pointOfInterestGridPoint,
      });
      await psql('surfSpotConfigurations').insert({
        pointOfInterestId,
        offshoreDirection: 0,
        breakingWaveHeightCoefficient: 0.5,
        breakingWaveHeightAlgorithm: 'SPECTRAL_REFRACTION',
      });
    });

    afterEach(async () => {
      await psql('surfSpotConfigurations').del();
      await psql('pointsOfInterestGridPoints').del();
      await psql('pointsOfInterest').del();
      await psql('modelGrids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('upsertPointOfInterestGridPoints', () => {
      it('rejects on bad location data', async () => {
        const result = await upsertPOIGridPoints({
          id: pointOfInterestId,
          points: [
            {
              ...modelGrids[1],
              lat: 10,
              lon: 10,
            },
            {
              ...modelGrids[0],
              lat: 91,
              lon: 10,
            },
          ],
        });
        expect(result.errors).to.not.be.empty();
        expect(result.errors.length).to.equal(1);
        expect(result.errors[0].message).to.match(/Latitude value 91 not in range \[-90, \+90\]/);
      });

      it('upserts correct data', async () => {
        const gridDataA = {
          ...modelGrids[1],
          lat: 10,
          lon: 10,
        };
        const gridDataB = {
          ...modelGrids[0],
          lat: 77,
          lon: 88,
        };

        // This should (i) update the lat/lon of GLOB_15m grid point that is
        // already present, and (ii) add a new grid point for UK_3m.
        const result = (
          await upsertPOIGridPoints({
            id: pointOfInterestId,
            points: [
              gridDataA,
              {
                ...gridDataB,
                // Offset longitude by 360 --- it should get normalized
                lon: gridDataB.lon - 360,
              },
            ],
          })
        ).data.upsertPointOfInterestGridPoints;

        expect(result.success).to.equal(true);
        expect(result.errorJSON).to.be.null();
        expect(result.pointOfInterest.name).to.equal(pointOfInterest.name);

        // Check database
        const gridPoints = await psql('pointsOfInterestGridPoints')
          .select(['agency', 'model', 'grid', 'latitude as lat', 'longitude as lon'])
          .where({ pointOfInterestId })
          .orderBy('createdAt');

        expect(gridPoints.length).to.equal(2);
        expect(gridPoints[0]).to.deep.include(gridDataA);
        expect(gridPoints[1]).to.deep.include(gridDataB);
      });
    });

    describe('removePointOfInterestGridPoints', () => {
      it('should reject if the all the data does not match an existing grid points', async () => {
        const result = (
          await removePOIGridPoints({
            id: pointOfInterestId,
            points: [
              // This matches
              modelGrids[1],
              {
                // This does not match anything
                agency: 'Wavetrak',
                model: 'Lotus-WW3',
                grid: 'GLOB_1m',
              },
            ],
          })
        ).data.removePointOfInterestGridPoints;
        expect(result.success).to.equal(false);
        // Even on failed update, expect that the POI is returned if requested
        expect(result.pointOfInterest).not.to.be.null();
        expect(result.pointOfInterest.name).to.equal(pointOfInterest.name);
        // Check database -- should still have data present
        const gridPoints = await psql('pointsOfInterestGridPoints')
          .select()
          .where({ pointOfInterestId });
        expect(gridPoints.length).to.equal(1);
      });

      it('should remove points if they all match', async () => {
        const result = (
          await removePOIGridPoints({
            id: pointOfInterestId,
            points: [modelGrids[1]],
          })
        ).data.removePointOfInterestGridPoints;
        expect(result.success).to.equal(true);
        // Check database -- should still have data present
        const gridPoints = await psql('pointsOfInterestGridPoints')
          .select()
          .where({ pointOfInterestId });
        expect(gridPoints.length).to.equal(0);
      });
    });
  });
};

export default pointsOfInterestGridPointSpec;
