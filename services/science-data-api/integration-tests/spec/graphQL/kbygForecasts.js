import { expect } from 'chai';
import moment from 'moment-timezone';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import models from '../../fixtures/models.json';
import modelGrids from '../../fixtures/modelGrids.json';
import modelRunsPointOfInterest from '../../fixtures/modelRunsPointOfInterest.json';
import pointsOfInterest from '../../fixtures/pointsOfInterest.json';
import pointsOfInterestDefaultForecastModels from '../../fixtures/pointsOfInterestDefaultForecastModels.json';
import pointsOfInterestGridPoints from '../../fixtures/pointsOfInterestGridPoints.json';
import surf from '../../fixtures/surf.json';
import swells from '../../fixtures/swells.json';
import wind from '../../fixtures/wind.json';
import surfSpotConfigurations from '../../fixtures/surfSpotConfigurations.json';
import { getKBYGWaveForecast, getKBYGWindForecast } from '../helpers/graphQL';

// This function is copied from kbyg-product-api to make sure we calculate start/end times the same way in testing
const midnightOfDate = (timezone, date) => moment(date).tz(timezone).startOf('day').toDate();

// This function is copied from kbyg-product-api to make sure we calculate start/end times the same way in testing
const dateRange = (days, timezone, date) => {
  const start = moment(midnightOfDate(timezone, date));
  // Use noon instead of midnight to ensure correct date
  // Related to the following issue: https://github.com/moment/moment/issues/4743
  const noon = moment(start).tz(timezone).add(12, 'hours');
  const end = moment(noon).add(days, 'days').startOf('day');
  return { start: start.unix(), end: end.unix() };
};

const expectForecastToBeCorrectShape = (forecast) => {
  expect(forecast.length).to.equal(17 * 24 + 1);
};

const expectForecastToStartAndEndAtCorrectTimes = (forecast, start, end) => {
  expect(forecast[0].timestamp).to.equal(start);
  expect(forecast[forecast.length - 1].timestamp).to.equal(end);
};

const kbygWaveForecastSpec = () => {
  describe('KBYG', function () {
    let psql;

    // https://mochajs.org/#timeouts
    this.timeout(5000);

    before(async function () {
      psql = setupPostgres();
      await psql.batchInsert('models', models);
      await psql.batchInsert('modelGrids', modelGrids);
      await psql.batchInsert('modelRunsPointOfInterest', modelRunsPointOfInterest);
      await psql.batchInsert('pointsOfInterest', pointsOfInterest);
      await psql.batchInsert('pointsOfInterestGridPoints', pointsOfInterestGridPoints);
      await psql.batchInsert(
        'pointsOfInterestDefaultForecastModels',
        pointsOfInterestDefaultForecastModels,
      );
      await psql.batchInsert('surfSpotConfigurations', surfSpotConfigurations);
      await psql.batchInsert('surf', surf);
      await psql.batchInsert('swells', swells);
      await psql.batchInsert('wind', wind);
    });

    after(async function () {
      await psql('surf').del();
      await psql('swells').del();
      await psql('wind').del();
      await psql('modelRunsPointOfInterest').del();
      await psql('pointsOfInterestDefaultForecastModels').del();
      await psql('pointsOfInterestGridPoints').del();
      await psql('surfSpotConfigurations').del();
      await psql('pointsOfInterest').del();
      await psql('modelGrids').del();
      await psql('models').del();
      await teardownPostgres(psql);
    });

    describe('KBYG wave forecast', function () {
      it('returns the correct Lotus forecast for spot in Australia time zone', async function () {
        const { start, end } = dateRange(17, 'Australia/Brisbane', '2022-01-05T14:00');
        const parameters = {
          pointOfInterestId: '4931cfb5-0f6f-434f-a77e-77168a9db4cc',
          interval: 3600,
          start,
          end,
          swellHeight: 'ft',
          waveHeight: 'ft',
          corrected: true,
        };
        const response = await getKBYGWaveForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.surf.data);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.swells.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.surf.data,
          start,
          end,
        );
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.swells.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in UK time zone', async function () {
        const { start, end } = dateRange(17, 'Europe/London', '2022-01-05T00:00');
        const parameters = {
          pointOfInterestId: 'c93a9839-8e3a-4337-8293-d81478ea5c9b',
          interval: 3600,
          start,
          end,
          swellHeight: 'ft',
          waveHeight: 'ft',
          corrected: true,
        };
        const response = await getKBYGWaveForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.surf.data);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.swells.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.surf.data,
          start,
          end,
        );
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.swells.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in eastern US time zone', async function () {
        const { start, end } = dateRange(17, 'America/New_York', '2022-01-05T05:00');
        const parameters = {
          pointOfInterestId: 'f085a0a1-f80e-4d1f-8565-a8e2f9a3e7c3',
          interval: 3600,
          start,
          end,
          swellHeight: 'ft',
          waveHeight: 'ft',
          corrected: true,
        };
        const response = await getKBYGWaveForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.surf.data);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.swells.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.surf.data,
          start,
          end,
        );
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.swells.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in western US time zone', async function () {
        const { start, end } = dateRange(17, 'America/Tijuana', '2022-01-05T08:00');
        const parameters = {
          pointOfInterestId: '8b4d5adf-3913-47b6-ab67-0596446ab355',
          interval: 3600,
          start,
          end,
          swellHeight: 'ft',
          waveHeight: 'ft',
          corrected: true,
        };
        const response = await getKBYGWaveForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.surf.data);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.swells.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.surf.data,
          start,
          end,
        );
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.swells.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in Hawaii time zone', async function () {
        const { start, end } = dateRange(17, 'Pacific/Honolulu', '2022-01-05T10:00');
        const parameters = {
          pointOfInterestId: '7eeaaba1-6e45-4f3d-a21f-5d2d52d9238a',
          interval: 3600,
          start,
          end,
          swellHeight: 'ft',
          waveHeight: 'ft',
          corrected: true,
        };
        const response = await getKBYGWaveForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.surf.data);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.swells.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.surf.data,
          start,
          end,
        );
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.swells.data,
          start,
          end,
        );
      });
    });

    describe('KBYG wind forecast', function () {
      it('returns the correct wind forecast for spot in Australia time zone', async function () {
        const { start, end } = dateRange(17, 'Australia/Brisbane', '2022-01-05T14:00');
        const parameters = {
          pointOfInterestId: '4931cfb5-0f6f-434f-a77e-77168a9db4cc',
          interval: 3600,
          start,
          end,
          windSpeed: 'm/h',
        };
        const response = await getKBYGWindForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.wind.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.wind.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in UK time zone', async function () {
        const { start, end } = dateRange(17, 'Europe/London', '2022-01-05T00:00');
        const parameters = {
          pointOfInterestId: 'c93a9839-8e3a-4337-8293-d81478ea5c9b',
          interval: 3600,
          start,
          end,
          windSpeed: 'm/h',
        };
        const response = await getKBYGWindForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.wind.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.wind.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in eastern US time zone', async function () {
        const { start, end } = dateRange(17, 'America/New_York', '2022-01-05T05:00');
        const parameters = {
          pointOfInterestId: 'f085a0a1-f80e-4d1f-8565-a8e2f9a3e7c3',
          interval: 3600,
          start,
          end,
          windSpeed: 'm/h',
        };
        const response = await getKBYGWindForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.wind.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.wind.data,
          start,
          end,
        );
      });

      it('returns the correct Lotus forecast for spot in western US time zone', async function () {
        const { start, end } = dateRange(17, 'America/Tijuana', '2022-01-05T08:00');
        const parameters = {
          pointOfInterestId: '8b4d5adf-3913-47b6-ab67-0596446ab355',
          interval: 3600,
          start,
          end,
          windSpeed: 'm/h',
        };
        const response = await getKBYGWindForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.wind.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.wind.data,
          start,
          end,
        );
      });

      it('returns the correct wind forecast for spot in Hawaii time zone', async function () {
        const { start, end } = dateRange(17, 'Pacific/Honolulu', '2022-01-05T10:00');
        const parameters = {
          pointOfInterestId: '7eeaaba1-6e45-4f3d-a21f-5d2d52d9238a',
          interval: 3600,
          start,
          end,
          windSpeed: 'm/h',
        };
        const response = await getKBYGWindForecast(parameters);
        expectForecastToBeCorrectShape(response.data.surfSpotForecasts.wind.data);
        expectForecastToStartAndEndAtCorrectTimes(
          response.data.surfSpotForecasts.wind.data,
          start,
          end,
        );
      });
    });
  });
};

export default kbygWaveForecastSpec;
