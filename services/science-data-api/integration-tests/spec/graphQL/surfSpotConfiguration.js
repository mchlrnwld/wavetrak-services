import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { upsertSurfSpotConfiguration } from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';

const pointsOfInterestSpec = () => {
  describe('Surf Spot Configuration', () => {
    let psql;
    let pointOfInterestId;

    const pointOfInterest = deepFreeze({
      name: 'POI1',
      latitude: 0,
      longitude: 0,
    });

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      const [row] = await psql('pointsOfInterest')
        .insert(pointOfInterest)
        .returning(['id']);
      pointOfInterestId = row.id;
    });

    afterEach(async () => {
      await psql('surfSpotConfigurations').del();
      await psql('pointsOfInterest').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('upsertSurfSpotConfiguration', async () => {
      it('upserts correct data', async () => {
        const surfSpotConfiguration = {
          offshoreDirection: 123.123,
          breakingWaveHeightAlgorithm: 'POLYNOMIAL',
          ratingsOptions: {
            foo: true,
            bar: 7,
            baz: 'buzz',
          },
        };

        const result = (
          await upsertSurfSpotConfiguration({
            pointOfInterestId,
            surfSpotConfiguration,
          })
        ).data.upsertSurfSpotConfiguration;

        expect(result.success).to.equal(true);
        expect(result.errorJSON).to.be.null();
        const { surfSpotConfiguration: surfSpotConfiguration1 } = result.pointOfInterest;
        expect(surfSpotConfiguration1.offshoreDirection).to.equal(
          surfSpotConfiguration.offshoreDirection,
        );
        expect(surfSpotConfiguration1.breakingWaveHeightCoefficient).to.equal(1.0);
        expect(surfSpotConfiguration1.breakingWaveHeightIntercept).to.equal(0.0);
        expect(surfSpotConfiguration1.ratingsOptions).to.include({
          foo: true,
          bar: 7,
          baz: 'buzz',
        });
      });

      it('sets values not passed to null and breaking wave height values to default', async () => {
        const surfSpotConfiguration = {
          offshoreDirection: 123.123,
          optimalSwellDirection: 10.1,
          breakingWaveHeightAlgorithm: 'POLYNOMIAL',
        };

        const result1 = (
          await upsertSurfSpotConfiguration({
            pointOfInterestId,
            surfSpotConfiguration,
          })
        ).data.upsertSurfSpotConfiguration;

        const { surfSpotConfiguration: surfSpotConfiguration1 } = result1.pointOfInterest;
        expect(result1.success).to.equal(true);
        expect(result1.errorJSON).to.be.null();
        expect(surfSpotConfiguration1.breakingWaveHeightCoefficient).to.equal(1.0);
        expect(surfSpotConfiguration1.breakingWaveHeightIntercept).to.equal(0.0);

        const surfSpotConfigurationWithMissingValues = {
          offshoreDirection: 321.321,
          breakingWaveHeightAlgorithm: 'POLYNOMIAL',
        };

        const result2 = (
          await upsertSurfSpotConfiguration({
            pointOfInterestId,
            surfSpotConfiguration: surfSpotConfigurationWithMissingValues,
          })
        ).data.upsertSurfSpotConfiguration;

        const { surfSpotConfiguration: surfSpotConfiguration2 } = result2.pointOfInterest;
        expect(surfSpotConfiguration2.optimalSwellDirection).to.be.null();
        expect(surfSpotConfiguration2.spectralRefractionMatrix).to.be.null();
        expect(surfSpotConfiguration2.breakingWaveHeightCoefficient).to.equal(1.0);
        expect(surfSpotConfiguration2.breakingWaveHeightIntercept).to.equal(0.0);
      });

      it('validates and fails entire transaction on bad data', async () => {
        const result = (
          await upsertSurfSpotConfiguration({
            pointOfInterestId,
            surfSpotConfiguration: {
              // the following 3 items all have errors
              offshoreDirection: -7,
              optimalSwellDirection: -7,
              breakingWaveHeightCoefficient: -7,
              breakingWaveHeightAlgorithm: 'POLYNOMIAL',
            },
          })
        ).data.upsertSurfSpotConfiguration;

        expect(result.success).to.equal(false);
        expect(result.errorJSON).to.not.be.empty();
        const errors = JSON.parse(result.errorJSON);
        expect(errors.errors.length).to.equal(3);
        // Even on failed update, expect that the POI is returned if requested
        expect(result.pointOfInterest).not.to.be.null();
        expect(result.pointOfInterest.name).to.equal(pointOfInterest.name);

        // Check database
        const surfSpots2 = await psql('surfSpotConfigurations')
          .select()
          .where({ pointOfInterestId });
        expect(surfSpots2).to.be.empty();
      });
    });
  });
};

export default pointsOfInterestSpec;
