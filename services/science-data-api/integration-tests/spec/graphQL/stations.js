import { expect } from 'chai';
import {
  getHistoricalStationData,
  getStationsInBoundingBox,
  getStationsWithinDistance,
} from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import { uuidObjectToString } from '../helpers/common';

const stationsSpec = () => {
  describe('Stations', () => {
    let psql;
    let id;

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await psql('station_sources').insert({ source: 'NDBC' });

      const idObject = await psql('stations')
        .insert({
          source: 'NDBC',
          sourceId: '12345',
          status: 'ONLINE',
          name: 'huntington',
          latitude: 34.5,
          longitude: 100.2,
          latestTimestamp: '2021-05-29 01:00:00',
        })
        .returning('id');

      id = uuidObjectToString(idObject[0]);

      await psql.batchInsert('station_data', [
        {
          stationId: id,
          timestamp: '2021-05-29 01:00:00',
          significantHeight: 1.2,
          peakPeriod: 4,
          meanWaveDirection: 205,
        },
        {
          stationId: id,
          timestamp: '2021-05-29 00:00:00',
          significantHeight: 1.1,
          peakPeriod: 3,
          meanWaveDirection: 205,
        },
        {
          stationId: id,
          timestamp: '2021-05-28 23:00:00',
          significantHeight: 1.3,
          peakPeriod: 3,
          meanWaveDirection: 205,
        },
        {
          stationId: id,
          timestamp: '2021-05-28 22:00:00',
          significantHeight: 1.4,
          peakPeriod: 3,
          meanWaveDirection: 205,
        },
        {
          stationId: id,
          timestamp: '2021-05-28 21:00:00',
          significantHeight: 1.0,
          peakPeriod: 3,
          meanWaveDirection: 205,
        },
        {
          stationId: id,
          timestamp: '2021-05-28 20:00:00',
          significantHeight: 1.2,
          peakPeriod: 3,
          meanWaveDirection: 205,
        },
      ]);
    });

    afterEach(async () => {
      await psql('station_data').del();
      await psql('stations').del();
      await psql('station_sources').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('station query', () => {
      it('returns data only for the latest timestamp when start and end are not given', async () => {
        const result = await getHistoricalStationData({ id });
        expect(result.data.station.data.length).to.equal(1);
        expect(result.data.station.data[0].timestamp).to.equal(1622250000);
      });

      it('returns all data for the given start and end timestamps', async () => {
        const result = await getHistoricalStationData({ id, start: 1622232000, end: 1622250000 });
        const timestamps = [
          '2021-05-29T01:00:00.000Z',
          '2021-05-29T00:00:00.000Z',
          '2021-05-28T23:00:00.000Z',
          '2021-05-28T22:00:00.000Z',
          '2021-05-28T21:00:00.000Z',
          '2021-05-28T20:00:00.000Z',
        ];
        const resultTimestamps = result.data.station.data.map(data =>
          new Date(data.timestamp * 1000).toISOString(),
        );
        expect(resultTimestamps).to.eql(timestamps);
      });

      it('returns null when the station does not exist', async () => {
        const result = await getHistoricalStationData({ id: 'ae3054e2-ca13-11eb-a512-0242015eddd9' });
        expect(result.data.station.station).to.be.null();
        expect(result.data.station.data).to.be.empty();
      });
    });

    describe('stationsBoundingBox query', () => {
      const boundingBoxWithStationInside = {
        east: 105.0,
        north: 40.0,
        south: 30.0,
        west: 95.0,
      };

      const boundingBoxWithoutStationInside = {
        east: 100.0,
        north: 40.0,
        south: 30.0,
        west: 95.0,
      };

      it('returns latest data for stations within geographic bounding box', async () => {
        const variables = { boundingBox: boundingBoxWithStationInside };
        const result = await getStationsInBoundingBox(variables);
        expect(result.data.stationsBoundingBox).to.not.be.empty();
        expect(result.data.stationsBoundingBox[0].latestData.height).to.be.greaterThan(0);
      });

      it('returns empty array when there are no stations within geographic bounding box', async () => {
        const variables = { boundingBox: boundingBoxWithoutStationInside };
        const result = await getStationsInBoundingBox(variables);
        expect(result.data.stationsBoundingBox).to.be.empty();
      });

      it('does not return station with incorrect status', async () => {
        const variables = { boundingBox: boundingBoxWithStationInside, status: 'OFFLINE' };
        const result = await getStationsInBoundingBox(variables);
        expect(result.data.stationsBoundingBox).to.be.empty();
      });

      it('does not fail when a station does not have data', async () => {
        await psql('stations').insert({
          source: 'NDBC',
          sourceId: '65432',
          status: 'ONLINE',
          name: 'station_with_no_data_intentionally',
          latitude: 10.0,
          longitude: 50.0,
          latestTimestamp: '2021-05-29 01:00:00',
        });

        const variables = { boundingBox: { east: 51, north: 11, south: 9, west: 49 } };
        const result = await getStationsInBoundingBox(variables);
        expect(result.data.stationsBoundingBox).to.not.be.empty();
        expect(result.data.stationsBoundingBox[0].latestData).to.be.null();
      });
    });

    describe('stationsRadius query', () => {
      let anotherId;

      beforeEach(async () => {
        const anotherIdObject = await psql('stations')
          .insert({
            source: 'NDBC',
            sourceId: '54321',
            status: 'ONLINE',
            name: 'station_intentionally_close_to_huntington',
            latitude: 34.6,
            longitude: 100.2,
            latestTimestamp: '2021-05-29 01:00:00',
          })
          .returning('id');
        anotherId = uuidObjectToString(anotherIdObject[0]);
      });

      const pointAndDistanceWithOneStationInside = {
        point: { longitude: 100.2, latitude: 34.4 },
        distance: 12,
      };

      const pointAndDistanceWithoutStationInside = {
        point: { longitude: 100.0, latitude: 34.0 },
        distance: 1,
      };

      const pointAndDistanceWithMultipleStationsInside = {
        point: { longitude: 100.2, latitude: 34.4 },
        distance: 10000,
      };

      it('returns latest data for stations within distance of specified point', async () => {
        const result = await getStationsWithinDistance(pointAndDistanceWithOneStationInside);
        expect(result.data.stationsRadius).to.have.length(1);
      });

      it('returns empty array when there are no stations within distance of specified point', async () => {
        const result = await getStationsWithinDistance(pointAndDistanceWithoutStationInside);
        expect(result.data.stationsRadius).to.have.length(0);
      });

      it('does not return station with incorrect status', async () => {
        const variables = { ...pointAndDistanceWithOneStationInside, status: 'OFFLINE' };
        const result = await getStationsWithinDistance(variables);
        expect(result.data.stationsRadius).to.have.length(0);
      });

      it('returns stations in order of distance to the given point', async () => {
        const result = await getStationsWithinDistance(pointAndDistanceWithMultipleStationsInside);
        expect(result.data.stationsRadius).to.have.length(2);
        expect(result.data.stationsRadius[0].station.id).to.equal(id);
        expect(result.data.stationsRadius[1].station.id).to.equal(anotherId);
      });

      it('returns a collection of stations limited in length by the limit parameter', async () => {
        const variables = { ...pointAndDistanceWithMultipleStationsInside, limit: 1 };
        const result = await getStationsWithinDistance(variables);
        expect(result.data.stationsRadius).to.have.length(1);
        expect(result.data.stationsRadius[0].station.id).to.equal(id);
      });
    });
  });
};

export default stationsSpec;
