import { expect } from 'chai';
import { removePOIDefaultForecastModels, upsertPOIDefaultForecastModels } from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';

const pointsOfInterestDefaultForecastModelsSpec = () => {
  describe('Points of Interest Default Forecast Models', () => {
    let psql;
    let pointOfInterestId;

    const pointOfInterest = {
      name: 'point',
      latitude: 0,
      longitude: 0,
    };
    const model = {
      agency: 'Wavetrak',
      model: 'Lotus-WW3',
    };
    const modelGrids = [
      {
        ...model,
        grid: 'UK_3m',
      },
      {
        ...model,
        grid: 'GLOB_15m',
      },
    ];

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      const [row] = await psql('pointsOfInterest')
        .insert(pointOfInterest)
        .returning(['id']);
      pointOfInterestId = row.id;
      const pointsOfInterestGridPoints = modelGrids.map(modelGrid => ({
        ...modelGrid,
        latitude: 0,
        longitude: 0,
        pointOfInterestId,
      }));

      await psql('models').insert(model);
      await Promise.all(modelGrids.map(point => psql('modelGrids').insert(point)));
      await Promise.all(
        pointsOfInterestGridPoints.map(point => psql('pointsOfInterestGridPoints').insert(point)),
      );
    });

    afterEach(async () => {
      await psql('pointsOfInterestDefaultForecastModels').del();
      await psql('pointsOfInterestGridPoints').del();
      await psql('pointsOfInterest').del();
      await psql('modelGrids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('upsertPointOfInterestDefaultForecastModel', () => {
      it('inserts default forecast model for swells', async () => {
        const grid = 'UK_3m';
        const result = (
          await upsertPOIDefaultForecastModels({
            pointOfInterestId,
            defaultForecastModels: [
              {
                forecastType: 'SWELLS',
                agency: 'Wavetrak',
                model: 'Lotus-WW3',
                grid,
              },
            ],
          })
        ).data.upsertPointOfInterestDefaultForecastModels;
        expect(result.success).to.equal(true);
        expect(result.errorJSON).to.be.null();
        expect(result.pointOfInterest.pointOfInterestId).to.equal(pointOfInterestId);
      });

      it('updates existing default forecast model for swells', async () => {
        await upsertPOIDefaultForecastModels({
          pointOfInterestId,
          defaultForecastModels: [
            {
              forecastType: 'SWELLS',
              agency: 'Wavetrak',
              model: 'Lotus-WW3',
              grid: 'UK_3m',
            },
          ],
        });
        const grid = 'GLOB_15m';
        const result = (
          await upsertPOIDefaultForecastModels({
            pointOfInterestId,
            defaultForecastModels: [
              { forecastType: 'SWELLS', agency: 'Wavetrak', model: 'Lotus-WW3', grid },
            ],
          })
        ).data.upsertPointOfInterestDefaultForecastModels;
        expect(result.success).to.equal(true);
        expect(result.errorJSON).to.be.null();
        expect(result.pointOfInterest.pointOfInterestId).to.equal(pointOfInterestId);
        // Check database for grid change.
        const poi = await psql('pointsOfInterestDefaultForecastModels')
          .select(['point_of_interest_id', 'forecast_type', 'agency', 'model', 'grid'])
          .where({ pointOfInterestId, forecastType: 'SWELLS' });
        expect(poi[0].grid).to.equal(grid);
      });

      it('fails to upsert for POI grid point that does not exist ', async () => {
        const result = (
          await upsertPOIDefaultForecastModels({
            pointOfInterestId,
            defaultForecastModels: [
              { forecastType: 'SWELLS', agency: 'NOAA', model: 'WW3', grid: 'glo_30m' },
            ],
          })
        ).data.upsertPointOfInterestDefaultForecastModels;
        expect(result.success).to.equal(false);
      });
    });

    describe('removePointOfInterestDefaultForecastModel', () => {
      it('removes default forecast model for POI', async () => {
        await upsertPOIDefaultForecastModels({
          pointOfInterestId,
          defaultForecastModels: [
            {
              forecastType: 'SWELLS',
              agency: 'Wavetrak',
              model: 'Lotus-WW3',
              grid: 'UK_3m',
            },
          ],
        });
        const result = (
          await removePOIDefaultForecastModels({
            pointOfInterestId,
            forecastTypes: ['SWELLS'],
          })
        ).data.removePointOfInterestDefaultForecastModels;
        expect(result.errorJSON).to.be.null();
        expect(result.success).to.equal(true);
        expect(result.pointOfInterest.pointOfInterestId).to.equal(pointOfInterestId);
        // Check database
        const poi = await psql('pointsOfInterestDefaultForecastModels')
          .select(['point_of_interest_id', 'forecast_type', 'agency', 'model', 'grid'])
          .where({ pointOfInterestId, forecastType: 'SWELLS' });

        expect(poi).to.be.empty();
      });

      it('fails when POI does not exist', async () => {
        const result = (
          await removePOIDefaultForecastModels({
            pointOfInterestId: 'e57f22a1-22a9-4c8c-a024-dc22c7709e69',
            forecastTypes: ['SWELLS'],
          })
        ).data.removePointOfInterestDefaultForecastModels;
        expect(result.success).to.equal(false);
        expect(result.pointOfInterest).to.be.equal(null);
        expect(result.message).to.be.equal(
          'No records updated, point of interest id and forecast type does not exist.',
        );
      });
    });
  });
};

export default pointsOfInterestDefaultForecastModelsSpec;
