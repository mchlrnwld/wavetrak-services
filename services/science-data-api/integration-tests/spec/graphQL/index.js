import pointsOfInterest from './pointsOfInterest';
import pointsOfInterestGridPoints from './pointsOfInterestGridPoints';
import surf from './surf';
import swell from './swell';
import swellEnsembleMembers from './swellEnsembleMembers';
import swellProbabilities from './swellProbabilities';
import weather from './weather';
import wind from './wind';
import pointsOfInterestDefaultForecastModels from './pointsOfInterestDefaultForecastModel';
import stations from './stations';
import surfSpotConfiguration from './surfSpotConfiguration';
import rating from './ratings';
import charts from './charts';
import kbygForecasts from './kbygForecasts';

const graphQLSpec = () => {
  describe('GraphQL', () => {
    charts();
    kbygForecasts();
    pointsOfInterest();
    pointsOfInterestDefaultForecastModels();
    pointsOfInterestGridPoints();
    rating();
    surf();
    surfSpotConfiguration();
    stations();
    swell();
    swellEnsembleMembers();
    swellProbabilities();
    weather();
    wind();
  });
};

export default graphQLSpec;
