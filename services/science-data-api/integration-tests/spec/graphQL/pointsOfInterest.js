import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { createPOI, updatePOI } from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';

const pointsOfInterestSpec = () => {
  describe('Points of Interest', () => {
    let psql;
    let pointOfInterestId;

    const pointOfInterest = deepFreeze({
      name: 'POI1',
      latitude: 0,
      longitude: 0,
    });

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      const [row] = await psql('pointsOfInterest').insert(pointOfInterest).returning(['id']);
      pointOfInterestId = row.id;
    });

    afterEach(async () => {
      await psql('pointsOfInterest').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('updatePointOfInterest', () => {
      it('validates point-of-interest latitude', async () => {
        const result = await updatePOI({
          ...pointOfInterest,
          id: pointOfInterestId,
          name: 'a',
          latitude: 91,
          longitude: -4,
        });
        expect(result.errors).to.not.be.empty();
        expect(result.errors.length).to.equal(1);
        expect(result.errors[0].message).to.match(/Latitude value 91 not in range \[-90, \+90\]/);
      });

      it('validates point-of-interest name', async () => {
        const result = (
          await updatePOI({
            ...pointOfInterest,
            id: pointOfInterestId,
            name: 'a',
            latitude: 81,
            longitude: -4,
          })
        ).data.updatePointOfInterest;
        expect(result.success).to.equal(false);
        expect(result.errorJSON).to.not.be.empty();
        const errors = JSON.parse(result.errorJSON);
        expect(errors.errors.length).to.equal(1);
        expect(result.pointOfInterest).not.to.be.null();
        expect(result.pointOfInterest.name).to.equal(pointOfInterest.name);
      });

      it('updates correct point-of-interest data', async () => {
        const newData = {
          name: 'abcd',
          latitude: 89,
          longitude: -6,
        };
        const result = (
          await updatePOI({
            id: pointOfInterestId,
            ...pointOfInterest,
            ...newData,
          })
        ).data.updatePointOfInterest;
        expect(result.success).to.equal(true);
        expect(result.errorJSON).to.be.null();
        expect(result.pointOfInterest).to.deep.equal({
          pointOfInterestId,
          name: newData.name,
          lat: newData.latitude,
          lon: newData.longitude,
        });
        const databaseRow = await psql('pointsOfInterest')
          .select(['name', 'latitude', 'longitude'])
          .where({ id: pointOfInterestId });
        expect(databaseRow[0]).to.deep.equal({
          ...newData,
          longitude: newData.longitude + 360,
        });
      });
    });

    describe('createPointOfInterest', () => {
      it('validates point-of-interest data', async () => {
        const result = await createPOI({
          name: 'a',
          latitude: 91,
          longitude: -4,
        });
        expect(result.errors).to.not.be.empty();
        expect(result.errors.length).to.equal(1);
        expect(result.errors[0].message).to.match(/Latitude value 91 not in range \[-90, \+90\]/);
      });

      it('inserts correct point-of-interest data', async () => {
        const newData = {
          name: 'abcd',
          latitude: 89,
          longitude: -6,
        };
        const result = (await createPOI(newData)).data.createPointOfInterest;
        expect(result.success).to.equal(true);
        expect(result.errorJSON).to.be.null();

        expect(result.pointOfInterest.pointOfInterestId).to.not.be.empty();
        expect(result.pointOfInterest.name).to.equal(newData.name);
        expect(result.pointOfInterest.lat).to.equal(newData.latitude);
        expect(result.pointOfInterest.lon).to.equal(newData.longitude);
        const databaseRow = await psql('pointsOfInterest')
          .select(['name', 'latitude', 'longitude'])
          .where({ id: result.pointOfInterest.pointOfInterestId });
        expect(databaseRow[0]).to.deep.equal({
          ...newData,
          longitude: newData.longitude + 360,
        });
      });
    });
  });
};

export default pointsOfInterestSpec;
