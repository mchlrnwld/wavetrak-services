import { expect } from 'chai';
import { flatMap } from 'lodash';
import moment from 'moment';
import { getSwellForecast } from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import { uuidObjectToString } from '../helpers/common';

const swellSpec = () => {
  describe('Swell', () => {
    let psql;
    let pointsOfInterestIds;

    const model = { agency: 'Wavetrak', model: 'Lotus-WW3' };
    const modelGrid = { ...model, grid: 'UK_3m' };
    const runMoments = ['2020-02-18T00', '2020-02-18T06', '2020-02-18T12'].map(x => moment.utc(x));
    const modelRuns = runMoments.map(run => ({
      ...model,
      run: run.toISOString(),
      status: 'ONLINE',
    }));
    const pointsOfInterest = [
      { name: 'Bantham', latitude: 50.28, longitude: 356.12 },
      { name: 'Bigbury', latitude: 50.28, longitude: 356.11 },
    ];

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await psql('models').insert(model);
      await Promise.all([
        psql('model_grids').insert(modelGrid),
        psql.batchInsert('model_runs_point_of_interest', modelRuns),
      ]);
      pointsOfInterestIds = (
        await psql.batchInsert('points_of_interest', pointsOfInterest).returning('id')
      ).map(uuidObjectToString);
      await psql.batchInsert(
        'points_of_interest_grid_points',
        pointsOfInterest.map((poi, i) => ({
          pointOfInterestId: pointsOfInterestIds[i],
          longitude: poi.longitude,
          latitude: poi.latitude,
          ...modelGrid,
        })),
      );
      await psql('points_of_interest_default_forecast_models').insert({
        pointOfInterestId: pointsOfInterestIds[1],
        forecastType: 'SWELLS',
        ...modelGrid,
      });

      // Create 9 hours of data for each POI grid point for each
      // run, so there is a little overlap
      await psql.batchInsert(
        'swells',
        flatMap(runMoments, (run, r) =>
          flatMap(pointsOfInterest, (poi, i) =>
            [...Array(9)].map((_, h) => ({
              pointOfInterestId: pointsOfInterestIds[i],
              ...modelGrid,
              latitude: poi.latitude,
              longitude: poi.longitude,
              run: run.toISOString(),
              forecastTime: run
                .clone()
                .add(h, 'hours')
                .toISOString(),
              // Vary the data by run, so we can identify which run it come from in tests
              height: r,
              period: 10,
              direction: 270,
              swell_wave_1_height: r + 1,
            })),
          ),
        ),
      );
    });

    afterEach(async () => {
      await psql('swells').del();
      await psql('points_of_interest_default_forecast_models').del();
      await psql('points_of_interest_grid_points').del();
      await psql('points_of_interest').del();
      await psql('model_runs_point_of_interest').del();
      await psql('model_grids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    it('returns a full run if not passed start or end', async () => {
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const result = await getSwellForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        ...modelGrid,
      });
      const forecast = result.data.surfSpotForecasts.swells.data;
      const start = runMoments[1].unix();
      expect(forecast.length).to.equal(9);
      forecast.forEach((row, i) => {
        expect(row.run).to.equal(run);
        expect(row.timestamp).to.equal(start + i * 3600);
        expect(row.swells.combined.height).to.equal(1);
        expect(row.swells.components[0].height).to.equal(2);
      });
    });

    it('returns data for start < run, but restricts to runs <= the run passed', async () => {
      const previousRun = parseInt(runMoments[0].format('YYYYMMDDHH'), 10);
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const start = runMoments[1].unix() - 3 * 3600;
      const result = await getSwellForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        start,
        ...modelGrid,
      });
      const forecast = result.data.surfSpotForecasts.swells.data;
      // 12 rows becuase the full 9 rows from `run` and 3 rows from the previous run.
      expect(forecast.length).to.equal(12);
      forecast.slice(0, 3).forEach((row, i) => {
        expect(row.run).to.equal(previousRun);
        expect(row.timestamp).to.equal(start + i * 3600);
        expect(row.swells.combined.height).to.equal(0);
        expect(row.swells.components[0].height).to.equal(1);
      });
      forecast.slice(3, 12).forEach((row, i) => {
        // Note: the last three rows here overlap the last run, but data should only come
        // form the requested run.
        expect(row.run).to.equal(run);
        expect(row.timestamp).to.equal(start + (i + 3) * 3600);
        expect(row.swells.combined.height).to.equal(1);
        expect(row.swells.components[0].height).to.equal(2);
      });
    });

    it('uses default forecast model if model not passed', async () => {
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const result = await getSwellForecast({
        id: pointsOfInterestIds[1],
        interval: 3600,
        run,
      });
      expect(result.data.surfSpotForecasts.swells.models).to.deep.equal([
        { agency: 'Wavetrak', model: 'Lotus-WW3', grid: { key: 'UK_3m' } },
      ]);
    });

    it('does not fail on request with start/end minutes and seconds', async () => {
      const start = runMoments[2].unix() + 125; // 2 minutes, 5 seconds
      const run = parseInt(runMoments[2].format('YYYYMMDDHH'), 10);
      const result = await getSwellForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        start,
        end: start + 24 * 3600,
        ...modelGrid,
      });
      const forecast = result.data.surfSpotForecasts.swells.data;
      expect(forecast.length).to.equal(25);
    });

    it('pads data at the end of the request to fulfill the requested forecast data length', async () => {
      const start = runMoments[2].unix() + 125; // 2 minutes, 5 seconds
      const run = parseInt(runMoments[2].format('YYYYMMDDHH'), 10);
      const result = await getSwellForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        start,
        end: start + 16 * 24 * 3600,
        ...modelGrid,
      });
      const forecast = result.data.surfSpotForecasts.swells.data;
      expect(forecast.length).to.equal(385);
    });

    it('returns the correct forecast data length and does not fail on 17 day request', async () => {
      const start = runMoments[2].unix() + 125; // 2 minutes, 5 seconds
      const run = parseInt(runMoments[2].format('YYYYMMDDHH'), 10);
      const result = await getSwellForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        start,
        end: start + 17 * 24 * 3600,
        ...modelGrid,
      });
      const forecast = result.data.surfSpotForecasts.swells.data;
      expect(forecast.length).to.equal(409);
    });
  });
};

export default swellSpec;
