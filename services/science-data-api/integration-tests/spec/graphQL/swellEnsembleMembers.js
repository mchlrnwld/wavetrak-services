import { expect } from 'chai';
import fetch from 'node-fetch';
import gql from 'graphql-tag';
import { print } from 'graphql';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import config from '../config';
import { uuidObjectToString } from '../helpers/common';

const swellEnsembleMembersSpec = () => {
  describe('Swell Ensemble Members', () => {
    let psql;
    let pointsOfInterestIds;
    let swellEnsembleMembers;

    const run = 2020021800;
    const runISO = '2020-02-18T00:00';
    const model = { agency: 'NOAA', model: 'GEFS-Wave' };
    const modelGrid = { ...model, grid: 'global.0p25' };
    const modelRun = { ...model, run: runISO, status: 'ONLINE' };
    const pointsOfInterest = [
      { name: 'HB Pier, Southside', latitude: 33.654, longitude: -118.003 },
      { name: 'Pipeline', latitude: 21.665, longitude: -158.052 },
    ];

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await psql('models').insert(model);
      await Promise.all([
        psql('model_grids').insert(modelGrid),
        psql('model_runs_point_of_interest').insert(modelRun),
      ]);
      pointsOfInterestIds = (
        await psql.batchInsert('points_of_interest', pointsOfInterest).returning('id')
      ).map(uuidObjectToString);
      await psql.batchInsert('points_of_interest_grid_points', [
        {
          pointOfInterestId: pointsOfInterestIds[0],
          longitude: pointsOfInterest[0].longitude,
          latitude: pointsOfInterest[0].latitude,
          ...modelGrid,
        },
        {
          pointOfInterestId: pointsOfInterestIds[1],
          longitude: pointsOfInterest[1].longitude,
          latitude: pointsOfInterest[1].latitude,
          ...modelGrid,
        },
      ]);
      const swellEnsembleMemberValues = {
        windU: 0,
        windV: 0,
        combinedWaveHeight: 0,
        primaryWaveDirection: 0,
        primaryWavePeriod: 0,
        swellWave_1Direction: 0,
        swellWave_1Height: 0,
        swellWave_1Period: 0,
        swellWave_2Direction: 0,
        swellWave_2Height: 0,
        swellWave_2Period: 0,
        windWaveDirection: 0,
        windWaveHeight: 0,
        windWavePeriod: 0,
      };
      swellEnsembleMembers = [
        {
          pointOfInterestId: pointsOfInterestIds[0],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[0].latitude,
          longitude: pointsOfInterest[0].longitude,
          forecastTime: new Date('2020-02-18T00:00'),
          member: 0,
          ...swellEnsembleMemberValues,
        },
        {
          pointOfInterestId: pointsOfInterestIds[0],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[0].latitude,
          longitude: pointsOfInterest[0].longitude,
          forecastTime: new Date('2020-02-18T00:00'),
          member: 1,
          ...swellEnsembleMemberValues,
        },
        {
          pointOfInterestId: pointsOfInterestIds[0],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[0].latitude,
          longitude: pointsOfInterest[0].longitude,
          forecastTime: new Date('2020-02-18T03:00'),
          member: 0,
          ...swellEnsembleMemberValues,
        },
        {
          pointOfInterestId: pointsOfInterestIds[0],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[0].latitude,
          longitude: pointsOfInterest[0].longitude,
          forecastTime: new Date('2020-02-18T03:00'),
          member: 1,
          ...swellEnsembleMemberValues,
        },
        {
          pointOfInterestId: pointsOfInterestIds[1],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[1].latitude,
          longitude: pointsOfInterest[1].longitude,
          forecastTime: new Date('2020-02-18T00:00'),
          member: 0,
          ...swellEnsembleMemberValues,
        },
      ];
      await psql.batchInsert('swell_ensemble_members', swellEnsembleMembers);
    });

    afterEach(async () => {
      await psql('swell_ensemble_members').del();
      await psql('points_of_interest_grid_points').del();
      await psql('points_of_interest').del();
      await psql('model_runs_point_of_interest').del();
      await psql('model_grids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    it('returns swell ensemble members for a point of interest', async () => {
      const query = gql`
        query {
          surfSpotForecasts(pointOfInterestId: "${pointsOfInterestIds[0]}") {
            swellEnsembleMembers(run: ${run}) {
              data {
                timestamp
                member
                swells {
                  combined {
                    height
                    direction
                    period
                  }
                  components {
                    height
                    direction
                    period
                  }
                }
              }
            }
          }
        }
      `;
      const response = await fetch(`${config.SCIENCE_DATA_SERVICE}/graphql`, {
        method: 'POST',
        body: JSON.stringify({ query: print(query) }),
        headers: { 'Content-Type': 'application/json' },
      });
      const results = await response.json();
      expect(
        results.data.surfSpotForecasts.swellEnsembleMembers.data.map(({ timestamp, member }) => ({
          timestamp,
          member,
        })),
      ).to.deep.equal(
        swellEnsembleMembers
          .filter(({ pointOfInterestId }) => pointOfInterestId === pointsOfInterestIds[0])
          .map(({ forecastTime, member }) => ({ timestamp: forecastTime / 1000, member })),
      );
    });
  });
};

export default swellEnsembleMembersSpec;
