import { expect } from 'chai';
import moment from 'moment';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import { uuidObjectToString } from '../helpers/common';
import { getSwellProbabilities } from '../helpers/graphQL';

const swellProbabilitiesSpec = () => {
  describe('Swell Probabilities', () => {
    let psql;
    let pointsOfInterestIds;
    let swellProbabilities;

    const run = 2020021800;
    const runISO = '2020-02-18T00:00';
    const runMoment = moment.utc(runISO);
    const model = { agency: 'NOAA', model: 'GEFS-Wave' };
    const modelGrid = { ...model, grid: 'global.0p25' };
    const modelRun = { ...model, run: runISO, status: 'ONLINE' };
    const pointsOfInterest = [
      { name: 'HB Pier, Southside', latitude: 33.654, longitude: -118.003 },
      { name: 'Pipeline', latitude: 21.665, longitude: -158.052 },
    ];

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await psql('models').insert(model);
      await Promise.all([
        psql('model_grids').insert(modelGrid),
        psql('model_runs_point_of_interest').insert(modelRun),
      ]);
      pointsOfInterestIds = (
        await psql.batchInsert('points_of_interest', pointsOfInterest).returning('id')
      ).map(uuidObjectToString);
      await psql.batchInsert('points_of_interest_grid_points', [
        {
          pointOfInterestId: pointsOfInterestIds[0],
          longitude: pointsOfInterest[0].longitude,
          latitude: pointsOfInterest[0].latitude,
          ...modelGrid,
        },
        {
          pointOfInterestId: pointsOfInterestIds[1],
          longitude: pointsOfInterest[1].longitude,
          latitude: pointsOfInterest[1].latitude,
          ...modelGrid,
        },
      ]);
      swellProbabilities = [
        {
          pointOfInterestId: pointsOfInterestIds[0],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[0].latitude,
          longitude: pointsOfInterest[0].longitude,
          forecastTime: new Date('2020-02-18T00:00'),
          probability: 1,
        },
        {
          pointOfInterestId: pointsOfInterestIds[0],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[0].latitude,
          longitude: pointsOfInterest[0].longitude,
          forecastTime: new Date('2020-02-18T03:00'),
          probability: 0.5,
        },
        {
          pointOfInterestId: pointsOfInterestIds[1],
          ...modelGrid,
          run: modelRun.run,
          latitude: pointsOfInterest[1].latitude,
          longitude: pointsOfInterest[1].longitude,
          forecastTime: new Date('2020-02-18T00:00'),
          probability: 1,
        },
      ];
      await psql.batchInsert('swell_probabilities', swellProbabilities);
    });

    afterEach(async () => {
      await psql('swell_probabilities').del();
      await psql('points_of_interest_grid_points').del();
      await psql('points_of_interest').del();
      await psql('model_runs_point_of_interest').del();
      await psql('model_grids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    it('returns swell probabilites for a point of interest', async () => {
      const results = await getSwellProbabilities({
        pointOfInterestId: pointsOfInterestIds[0],
        run,
      });
      expect(results.data.surfSpotForecasts.swellProbabilities.data).to.deep.equal([
        {
          timestamp: swellProbabilities[0].forecastTime / 1000,
          probability: swellProbabilities[0].probability,
        },
        {
          timestamp: swellProbabilities[1].forecastTime / 1000,
          probability: swellProbabilities[1].probability,
        },
      ]);
    });

    it('does not fail on request start time more than 1 hour after a data point', async () => {
      const start = runMoment.unix() + 2 * 3600;
      const result = await getSwellProbabilities({
        pointOfInterestId: pointsOfInterestIds[0],
        interval: 3600,
        run,
        start,
        end: start + 16 * 24 * 3600,
      });
      const forecast = result.data.surfSpotForecasts.swellProbabilities.data;
      expect(forecast.length).to.equal(1);
    });
  });
};

export default swellProbabilitiesSpec;
