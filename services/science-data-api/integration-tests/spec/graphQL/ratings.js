import { expect } from 'chai';
import { flatMap } from 'lodash';
import moment from 'moment';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import { uuidObjectToString } from '../helpers/common';
import { getRatingForecast } from '../helpers/graphQL';

const ratingSpec = () => {
  describe('Rating', () => {
    let psql;
    let pointsOfInterestIds;

    const models = [
      { agency: 'NOAA', model: 'GFS' },
      { agency: 'NOAA', model: 'NAM' },
      { agency: 'Wavetrak', model: 'Lotus-WW3' },
    ];
    const modelGrids = [
      { ...models[0], grid: '0p25' },
      { ...models[1], grid: 'uk' },
      { ...models[2], grid: 'UK_3m' },
      { ...models[2], grid: 'GLOB_30m' },
    ];
    const runMoments = ['2020-02-18T00', '2020-02-18T06', '2020-02-18T12'].map(x => moment.utc(x));
    const modelRuns = flatMap(models, model =>
      flatMap(runMoments, run => ({
        ...model,
        run: run.toISOString(),
        status: 'ONLINE',
      })),
    );
    const pointsOfInterest = [
      { name: 'Bantham', latitude: 50.28, longitude: 356.12 },
      { name: 'Bigbury', latitude: 50.28, longitude: 356.11 },
    ];
    const mockRatingsMatrix = [[0, 1000, -180, 180, 0, 1000, 777]];

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await psql.batchInsert('models', models);
      await Promise.all([
        psql.batchInsert('model_grids', modelGrids),
        psql.batchInsert('model_runs_point_of_interest', modelRuns),
      ]);
      pointsOfInterestIds = (
        await psql.batchInsert('points_of_interest', pointsOfInterest).returning('id')
      ).map(uuidObjectToString);
      await psql.batchInsert('surf_spot_configurations', [
        {
          pointOfInterestId: pointsOfInterestIds[0],
          offshoreDirection: 180,
          breakingWaveHeightAlgorithm: 'POLYNOMIAL',
          ratingsAlgorithm: 'MATRIX',
        },
        {
          pointOfInterestId: pointsOfInterestIds[1],
          offshoreDirection: 45,
          breakingWaveHeightAlgorithm: 'POLYNOMIAL',
          ratingsAlgorithm: 'MATRIX',
          ratingsMatrix: mockRatingsMatrix,
        },
      ]);
      await psql.batchInsert(
        'points_of_interest_grid_points',
        flatMap(modelGrids, modelGrid =>
          flatMap(pointsOfInterest, (poi, i) => ({
            pointOfInterestId: pointsOfInterestIds[i],
            longitude: poi.longitude,
            latitude: poi.latitude,
            ...modelGrid,
          })),
        ),
      );
      await psql.batchInsert(
        'points_of_interest_default_forecast_models',
        pointsOfInterest.map((poi, i) => ({
          pointOfInterestId: pointsOfInterestIds[i],
          forecastType: 'WIND',
          ...modelGrids[0],
        })),
      );
      await psql.batchInsert(
        'points_of_interest_default_forecast_models',
        pointsOfInterest.map((poi, i) => ({
          pointOfInterestId: pointsOfInterestIds[i],
          forecastType: 'SURF',
          ...modelGrids[3],
        })),
      );
      // Create 9 hours of data for each POI grid point for each
      // run, so there is a little overlap
      await psql.batchInsert(
        'wind',
        flatMap(runMoments, run =>
          flatMap(pointsOfInterest, (poi, i) =>
            [...Array(9)].map((_, h) => ({
              pointOfInterestId: pointsOfInterestIds[i],
              ...modelGrids[0],
              latitude: poi.latitude,
              longitude: poi.longitude,
              run: run.toISOString(),
              forecastTime: run
                .clone()
                .add(h, 'hours')
                .toISOString(),
              u: Math.random() * 50,
              v: Math.random() * 50,
              gust: 0,
            })),
          ),
        ),
      );
      // Create 9 hours of data for each POI grid point for each
      // run, so there is a little overlap
      await psql.batchInsert(
        'surf',
        flatMap(runMoments, (run, r) =>
          flatMap(pointsOfInterest, (poi, i) =>
            [...Array(9)].map((_, h) => ({
              pointOfInterestId: pointsOfInterestIds[i],
              ...modelGrids[3],
              latitude: poi.latitude,
              longitude: poi.longitude,
              run: run.toISOString(),
              forecastTime: run
                .clone()
                .add(h, 'hours')
                .toISOString(),
              breakingWaveHeightMin: r,
              breakingWaveHeightMax: 2 + r,
            })),
          ),
        ),
      );
    });

    afterEach(async () => {
      await psql('surf').del();
      await psql('wind').del();
      await psql('points_of_interest_default_forecast_models').del();
      await psql('points_of_interest_grid_points').del();
      await psql('surf_spot_configurations').del();
      await psql('points_of_interest').del();
      await psql('model_runs_point_of_interest').del();
      await psql('model_grids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    it('uses the ratings matrix from surf spot configuration when it is available', async () => {
      const result = await getRatingForecast({
        id: pointsOfInterestIds[1],
        interval: 3600,
        run: parseInt(runMoments[0].format('YYYYMMDDHH'), 10),
      });
      const forecast = result.data.surfSpotForecasts.rating.data;
      expect(forecast.length).to.equal(9);
      expect(
        forecast.filter(({ rating: { key, value } }) => key === 'EPIC' && value === 777),
      ).to.have.length(9);
    });

    it('returns ratings for each forecast time available', async () => {
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const result = await getRatingForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
      });
      const forecast = result.data.surfSpotForecasts.rating.data;
      expect(forecast.length).to.equal(9);
    });

    it('returns ratings between specified start and end', async () => {
      const start = runMoments[0].unix();
      const end = runMoments[0].add(4, 'hours').unix();
      const result = await getRatingForecast({
        end,
        id: pointsOfInterestIds[0],
        interval: 3600,
        start,
      });
      const forecast = result.data.surfSpotForecasts.rating.data;
      expect(forecast.length).to.equal(5);
    });
  });
};

export default ratingSpec;
