import { expect } from 'chai';
import moment from 'moment';
import path from 'path';
import config from '../config';
import { createBucket, putObject } from '../helpers/localstack';
import { getForecastChart } from '../helpers/graphQL';

const chartsSpec = () => {
  describe('Charts', () => {
    const testBucketName = config.MAPTILER_TILEDB_BUCKET;
    const runTypesToCreateMockData = ['wave', 'wind'];
    const runPrefixesToCreateMockData = ['2021121300', '2021121306', '2021121312'];
    const objectNamesToCreateMockData = ['000.json', '003.json', '006.json', '009.json'];

    before(async () => {
      await createBucket(testBucketName);
      const promises = [];
      runTypesToCreateMockData.forEach(t => {
        runPrefixesToCreateMockData.forEach(r => {
          objectNamesToCreateMockData.forEach(n => {
            promises.push(
              putObject(testBucketName, path.join(t, r, n), {
                tileId: `${t}-${Math.random().toString()}`,
              }),
            );
          });
        });
      });
      await Promise.all(promises);
    });

    it('should return forecast chart tiles between start (inclusive) and end (exclusive) times', async () => {
      const startMoment = moment('2021-12-13T00:00Z');
      const endMoment = moment('2021-12-13T06:00Z');
      const response = await getForecastChart({
        type: 'WAVE',
        start: startMoment.unix(),
        end: endMoment.unix(),
      });
      const charts = response.data.forecastCharts;
      expect(charts.length).to.equal(1);
      const chart = charts[0];
      expect(chart.type).to.equal('WAVE');
      expect(chart.tiles.length).to.equal(2);
      expect(chart.tiles[0].timestamp).to.equal(startMoment.unix());
      expect(chart.tiles[1].timestamp).to.be.lessThan(endMoment.unix());
    });
  });
};

export default chartsSpec;
