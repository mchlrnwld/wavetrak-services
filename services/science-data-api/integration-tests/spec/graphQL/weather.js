import { expect } from 'chai';
import { flatMap } from 'lodash';
import moment from 'moment';
import { getWeatherForecast } from '../helpers/graphQL';
import { setupPostgres, teardownPostgres } from '../helpers/postgres';
import { uuidObjectToString } from '../helpers/common';

const weatherSpec = () => {
  describe('Weather', () => {
    let psql;
    let pointsOfInterestIds;

    const models = [
      { agency: 'NOAA', model: 'GFS' },
      { agency: 'NOAA', model: 'NAM' },
    ];
    const modelGrids = [
      { ...models[0], grid: '0p25' },
      { ...models[1], grid: 'uk' },
    ];
    const runMoments = ['2020-02-18T00', '2020-02-18T06', '2020-02-18T12'].map(x => moment.utc(x));
    const modelRuns = flatMap(models, model =>
      flatMap(runMoments, run => ({
        ...model,
        run: run.toISOString(),
        status: 'ONLINE',
      })),
    );
    const pointsOfInterest = [
      { name: 'Bantham', latitude: 50.28, longitude: 356.12 },
      { name: 'Bigbury', latitude: 50.28, longitude: 356.11 },
    ];

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await psql.batchInsert('models', models);
      await Promise.all([
        psql.batchInsert('model_grids', modelGrids),
        psql.batchInsert('model_runs_point_of_interest', modelRuns),
      ]);
      pointsOfInterestIds = (
        await psql.batchInsert('points_of_interest', pointsOfInterest).returning('id')
      ).map(uuidObjectToString);
      await psql.batchInsert(
        'points_of_interest_grid_points',
        flatMap(modelGrids, modelGrid =>
          flatMap(pointsOfInterest, (poi, i) => ({
            pointOfInterestId: pointsOfInterestIds[i],
            longitude: poi.longitude,
            latitude: poi.latitude,
            ...modelGrid,
          })),
        ),
      );
      await psql('points_of_interest_default_forecast_models').insert({
        pointOfInterestId: pointsOfInterestIds[0],
        forecast_type: 'WEATHER',
        ...modelGrids[0],
      });

      // Create 9 hours of data for each POI grid point for each
      // run, so there is a little overlap
      await psql.batchInsert(
        'weather',
        flatMap(runMoments, run =>
          flatMap(pointsOfInterest, (poi, i) =>
            [...Array(9)].map((_, h) => ({
              pointOfInterestId: pointsOfInterestIds[i],
              ...modelGrids[0],
              latitude: poi.latitude,
              longitude: poi.longitude,
              run: run.toISOString(),
              forecastTime: run
                .clone()
                .add(h, 'hours')
                .toISOString(),
              temperature: 0,
              dewpoint: 0,
              visibility: 0,
              humidity: 0,
              pressure: 0,
              precipitation_volume: 0,
              precipitation_type: 'FREEZING_RAIN',
              weather_conditions: 'SEVERE_SNOW_SQUALLS',
            })),
          ),
        ),
      );
    });

    afterEach(async () => {
      await psql('weather').del();
      await psql('points_of_interest_default_forecast_models').del();
      await psql('points_of_interest_grid_points').del();
      await psql('points_of_interest').del();
      await psql('model_runs_point_of_interest').del();
      await psql('model_grids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    it('returns a full run if not passed start or end', async () => {
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const result = await getWeatherForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        ...modelGrids[0],
      });
      const forecast = result.data.surfSpotForecasts.weather.data;
      const start = runMoments[1].unix();
      expect(forecast.length).to.equal(9);
      forecast.forEach((row, i) => {
        expect(row.run).to.equal(run);
        expect(row.timestamp).to.equal(start + i * 3600);
      });
    });

    it('returns data for start < run, but restricts to runs <= the run passed', async () => {
      const previousRun = parseInt(runMoments[0].format('YYYYMMDDHH'), 10);
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const start = runMoments[1].unix() - 3 * 3600;
      const result = await getWeatherForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
        start,
        ...modelGrids[0],
      });
      const forecast = result.data.surfSpotForecasts.weather.data;
      // 12 rows becuase the full 9 rows from `run` and 3 rows from the previous run.
      expect(forecast.length).to.equal(12);
      forecast.slice(0, 3).forEach((row, i) => {
        expect(row.run).to.equal(previousRun);
        expect(row.timestamp).to.equal(start + i * 3600);
      });
      forecast.slice(3, 12).forEach((row, i) => {
        // Note: the last three rows here overlap the last run, but data should only come
        // form the requested run.
        expect(row.run).to.equal(run);
        expect(row.timestamp).to.equal(start + (i + 3) * 3600);
      });
    });

    it('uses default forecast model if not passed', async () => {
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const result = await getWeatherForecast({
        id: pointsOfInterestIds[0],
        interval: 3600,
        run,
      });
      expect(result.data.surfSpotForecasts.weather.models).to.deep.equal([
        { agency: 'NOAA', model: 'GFS', grid: { key: '0p25' } },
      ]);
    });

    it('defaults to blended model', async () => {
      const run = parseInt(runMoments[1].format('YYYYMMDDHH'), 10);
      const result = await getWeatherForecast({
        id: pointsOfInterestIds[1],
        interval: 3600,
        run,
      });
      expect(result.data.surfSpotForecasts.weather.models).to.deep.equal([
        { agency: 'NOAA', model: 'GFS', grid: { key: '0p25' } },
        { agency: 'NOAA', model: 'NAM', grid: { key: 'uk' } },
      ]);
    });
  });
};

export default weatherSpec;
