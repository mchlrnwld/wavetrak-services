import { expect } from 'chai';
import fetch from 'node-fetch';
import deepFreeze from 'deep-freeze';
import { setupPostgres, teardownPostgres } from './helpers/postgres';
import config from './config';

const pointsOfInterestGridPointsSpec = () => {
  describe('/points_of_interest_grid_points', () => {
    let psql;

    const models = deepFreeze([
      { agency: 'NOAA', model: 'GFS' },
      { agency: 'Wavetrak', model: 'Lotus-WW3' },
    ]);
    const modelGrids = deepFreeze([
      { agency: 'NOAA', model: 'GFS', grid: '0p25' },
      { agency: 'Wavetrak', model: 'Lotus-WW3', grid: 'GLOB_30m' },
      { agency: 'Wavetrak', model: 'Lotus-WW3', grid: 'AUS_E_3m' },
    ]);
    const modelGridPoints = deepFreeze([
      {
        agency: 'NOAA',
        model: 'GFS',
        grid: '0p25',
        latitude: 0.25,
        longitude: 0.25,
      },
      {
        agency: 'NOAA',
        model: 'GFS',
        grid: '0p25',
        latitude: 0.5,
        longitude: 0.5,
      },
      {
        agency: 'Wavetrak',
        model: 'Lotus-WW3',
        grid: 'GLOB_30m',
        latitude: 0,
        longitude: 0,
      },
      {
        agency: 'Wavetrak',
        model: 'Lotus-WW3',
        grid: 'AUS_E_3m',
        latitude: -50,
        longitude: 90,
      },
    ]);
    const pointsOfInterest = deepFreeze([
      {
        id: '67a021e4-6439-4b34-953a-503e6e76cef1',
        name: 'POI1',
        latitude: 0,
        longitude: 0,
      },
      {
        id: '6070a1ac-ecf9-437c-85be-f6eeaf1ed9ae',
        name: 'POI2',
        latitude: -50,
        longitude: 90,
      },
    ]);
    const pointsOfInterestGridPoints = deepFreeze([
      {
        pointOfInterestId: '67a021e4-6439-4b34-953a-503e6e76cef1',
        agency: 'NOAA',
        model: 'GFS',
        grid: '0p25',
        latitude: 0.25,
        longitude: 0.25,
      },
      {
        pointOfInterestId: '67a021e4-6439-4b34-953a-503e6e76cef1',
        agency: 'Wavetrak',
        model: 'Lotus-WW3',
        grid: 'GLOB_30m',
        latitude: 0,
        longitude: 0,
      },
      {
        pointOfInterestId: '6070a1ac-ecf9-437c-85be-f6eeaf1ed9ae',
        agency: 'NOAA',
        model: 'GFS',
        grid: '0p25',
        latitude: 0.5,
        longitude: 0.5,
      },
      {
        pointOfInterestId: '6070a1ac-ecf9-437c-85be-f6eeaf1ed9ae',
        agency: 'Wavetrak',
        model: 'Lotus-WW3',
        grid: 'AUS_E_3m',
        latitude: -50,
        longitude: 90,
      },
    ]);

    before(() => {
      psql = setupPostgres();
    });

    beforeEach(async () => {
      await Promise.all(models.map(model => psql('models').insert(model)));
      await Promise.all(modelGrids.map(modelGrid => psql('modelGrids').insert(modelGrid)));
      await Promise.all(
        modelGridPoints.map(modelGridPoint => psql('modelGridPoints').insert(modelGridPoint)),
      );
      await Promise.all(
        pointsOfInterest.map(pointOfInterest => psql('pointsOfInterest').insert(pointOfInterest)),
      );
      await Promise.all(
        pointsOfInterestGridPoints.map(pointOfInterestGridPoint => psql('pointsOfInterestGridPoints').insert(pointOfInterestGridPoint)),
      );
    });

    afterEach(async () => {
      await psql('pointsOfInterestGridPoints').del();
      await psql('pointsOfInterest').del();
      await psql('modelGridPoints').del();
      await psql('modelGrids').del();
      await psql('models').del();
    });

    after(async () => {
      await teardownPostgres(psql);
    });

    describe('GET', () => {
      it('queries all points of interest grid points by default', async () => {
        const response = await fetch(
          `${config.SCIENCE_DATA_SERVICE}/points_of_interest_grid_points`,
        );

        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.have.length(4);
      });

      it('queries points of interest grid points for a model grid', async () => {
        const response = await fetch(
          `${config.SCIENCE_DATA_SERVICE}/points_of_interest_grid_points?agency=NOAA&model=GFS&grid=0p25`,
        );

        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.have.length(2);
        body.forEach((p) => {
          expect(p.agency).to.equal('NOAA');
          expect(p.model).to.equal('GFS');
          expect(p.grid).to.equal('0p25');
        });
      });

      it('supports pagination with offset and limit parameters', async () => {
        const response = await fetch(
          `${config.SCIENCE_DATA_SERVICE}/points_of_interest_grid_points?offset=1&limit=1`,
        );

        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body).to.have.length(1);
        expect(body[0].pointOfInterestId).to.equal('67a021e4-6439-4b34-953a-503e6e76cef1');
      });
    });
  });
};

export default pointsOfInterestGridPointsSpec;
