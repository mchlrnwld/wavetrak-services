import {
  CreateBucketCommand,
  ListObjectsV2Command,
  PutObjectCommand,
  S3Client,
  PutBucketPolicyCommand,
} from '@aws-sdk/client-s3';
import config from '../config';

const client = new S3Client({ endpoint: config.S3_ENDPOINT });

export const createBucket = async name => {
  const createResponse = await client.send(new CreateBucketCommand({ Bucket: name }));

  const readOnlyAnonUserPolicy = {
    Version: '2012-10-17',
    Statement: [
      {
        Sid: 'PublicRead',
        Effect: 'Allow',
        Principal: '*',
        Action: ['s3:GetObject', 's3:GetObjectVersion'],
        Resource: [`arn:aws:s3::: + ${name} + /*`],
      },
    ],
  };

  const bucketPolicyParams = {
    Bucket: name,
    Policy: JSON.stringify(readOnlyAnonUserPolicy),
  };

  await client.send(new PutBucketPolicyCommand(bucketPolicyParams));

  return createResponse.Location;
};

export const putObject = async (bucket, key, object) => {
  await client.send(
    new PutObjectCommand({ Body: JSON.stringify(object), Bucket: bucket, Key: key }),
  );
};

export const listObjects = async (bucket, prefix) => {
  const res = await client.send(
    new ListObjectsV2Command({
      Bucket: bucket,
      Prefix: prefix,
    }),
  );
  return res.Contents;
};
