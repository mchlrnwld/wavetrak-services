import fetch from 'node-fetch';
import gql from 'graphql-tag';
import { print } from 'graphql';
import config from '../config';
import UpsertSurfSpotConfigurationMutation from './graphql/upsertSurfSpotConfiguration.graphql';
import GetRatingForecast from './graphql/getRatingForecast.graphql';
import GetForecastChart from './graphql/getForecastChart.graphql';
import GetKBYGWaveForecast from './graphql/getKBYGWaveForecast.graphql';
import GetKBYGWindForecast from './graphql/getKBYGWindForecast.graphql';

const makeFunc = (query) => (variables) =>
  fetch(`${config.SCIENCE_DATA_SERVICE}/graphql/`, {
    method: 'post',
    body: JSON.stringify({ query: print(query), variables }),
    headers: { 'Content-Type': 'application/json' },
  }).then((res) => res.json());

export const upsertSurfSpotConfiguration = makeFunc(UpsertSurfSpotConfigurationMutation);

export const getRatingForecast = makeFunc(GetRatingForecast);

export const getForecastChart = makeFunc(GetForecastChart);

export const getKBYGWaveForecast = makeFunc(GetKBYGWaveForecast);

export const getKBYGWindForecast = makeFunc(GetKBYGWindForecast);

export const updatePOI = makeFunc(gql`
  mutation UpdatePOI($id: String!, $name: String!, $latitude: Latitude!, $longitude: Longitude!) {
    updatePointOfInterest(pointOfInterestId: $id, name: $name, lat: $latitude, lon: $longitude) {
      success
      message
      errorJSON
      pointOfInterest {
        pointOfInterestId
        lat
        lon
        name
      }
    }
  }
`);

export const createPOI = makeFunc(gql`
  mutation CreatePOI($name: String!, $latitude: Latitude!, $longitude: Longitude!) {
    createPointOfInterest(name: $name, lat: $latitude, lon: $longitude) {
      success
      message
      errorJSON
      pointOfInterest {
        pointOfInterestId
        lat
        lon
        name
      }
    }
  }
`);

export const upsertPOIGridPoints = makeFunc(gql`
  mutation ($id: String!, $points: [PointOfInterestGridPointInput]!) {
    upsertPointOfInterestGridPoints(pointOfInterestId: $id, gridPoints: $points) {
      success
      message
      errorJSON
      pointOfInterest {
        name
      }
    }
  }
`);

export const removePOIGridPoints = makeFunc(gql`
  mutation ($id: String!, $points: [RemovePointOfInterestGridPointInput]!) {
    removePointOfInterestGridPoints(pointOfInterestId: $id, gridPoints: $points) {
      success
      message
      errorJSON
      pointOfInterest {
        name
      }
    }
  }
`);

export const upsertPOIDefaultForecastModels = makeFunc(gql`
  mutation (
    $pointOfInterestId: String!
    $defaultForecastModels: [PointOfInterestDefaultForecastModelInput]!
  ) {
    upsertPointOfInterestDefaultForecastModels(
      pointOfInterestId: $pointOfInterestId
      defaultForecastModels: $defaultForecastModels
    ) {
      success
      message
      errorJSON
      pointOfInterest {
        name
        pointOfInterestId
      }
    }
  }
`);

export const removePOIDefaultForecastModels = makeFunc(gql`
  mutation ($pointOfInterestId: String!, $forecastTypes: [ForecastType]!) {
    removePointOfInterestDefaultForecastModels(
      pointOfInterestId: $pointOfInterestId
      forecastTypes: $forecastTypes
    ) {
      success
      message
      errorJSON
      pointOfInterest {
        name
        pointOfInterestId
      }
    }
  }
`);

export const getSurfForecast = makeFunc(gql`
  query (
    $id: String!
    $run: Int!
    $agency: String
    $model: String
    $grid: String
    $start: Int
    $end: Int
    $interval: Int
  ) {
    surfSpotForecasts(pointOfInterestId: $id) {
      surf(
        agency: $agency
        model: $model
        run: $run
        grid: $grid
        start: $start
        end: $end
        interval: $interval
      ) {
        models {
          agency
          model
          grid {
            key
          }
        }
        data {
          timestamp
          run
          surf {
            breakingWaveHeightMin
            breakingWaveHeightMax
          }
        }
      }
    }
  }
`);

export const getSwellForecast = makeFunc(gql`
  query (
    $id: String!
    $run: Int!
    $agency: String
    $model: String
    $grid: String
    $start: Int
    $end: Int
    $interval: Int
  ) {
    surfSpotForecasts(pointOfInterestId: $id) {
      swells(
        agency: $agency
        model: $model
        run: $run
        grid: $grid
        start: $start
        end: $end
        interval: $interval
      ) {
        models {
          agency
          model
          grid {
            key
          }
        }
        data {
          timestamp
          run
          swells {
            combined {
              height
            }
            components {
              height
            }
          }
        }
      }
    }
  }
`);

export const getSwellProbabilities = makeFunc(gql`
  query ($pointOfInterestId: String!, $run: Int!, $start: Int, $end: Int, $interval: Int) {
    surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
      swellProbabilities(run: $run, start: $start, end: $end, interval: $interval) {
        data {
          timestamp
          probability
        }
      }
    }
  }
`);

export const getWeatherForecast = makeFunc(gql`
  query (
    $id: String!
    $run: Int!
    $agency: String
    $model: String
    $grid: String
    $start: Int
    $end: Int
    $interval: Int
  ) {
    surfSpotForecasts(pointOfInterestId: $id) {
      weather(
        agency: $agency
        model: $model
        run: $run
        grid: $grid
        start: $start
        end: $end
        interval: $interval
      ) {
        models {
          agency
          model
          grid {
            key
          }
        }
        data {
          timestamp
          run
        }
      }
    }
  }
`);

export const getWindForecast = makeFunc(gql`
  query (
    $id: String!
    $run: Int!
    $agency: String
    $model: String
    $grid: String
    $start: Int
    $end: Int
    $interval: Int
  ) {
    surfSpotForecasts(pointOfInterestId: $id) {
      wind(
        agency: $agency
        model: $model
        run: $run
        grid: $grid
        start: $start
        end: $end
        interval: $interval
      ) {
        models {
          agency
          model
          grid {
            key
          }
        }
        data {
          timestamp
          run
        }
      }
    }
  }
`);

export const getHistoricalStationData = makeFunc(gql`
  query ($id: String!, $start: Int, $end: Int) {
    station(id: $id) {
      station {
        id
        name
        latestTimestamp
      }
      data(start: $start, end: $end) {
        timestamp
        height
        period
        swells {
          height
          period
          direction
          impact
        }
      }
    }
  }
`);

export const getStationsInBoundingBox = makeFunc(gql`
  query stationsBoundingBox($boundingBox: BoundingBoxInput!, $status: StationStatus) {
    stationsBoundingBox(boundingBox: $boundingBox, status: $status) {
      station {
        id
        name
        longitude
        latitude
        latestTimestamp
      }
      latestData {
        wind {
          speed
          direction
          gust
        }
        height
        period
        timestamp
      }
    }
  }
`);

export const getStationsWithinDistance = makeFunc(gql`
  query ($point: PointInput!, $distance: Float!, $status: StationStatus, $limit: Int) {
    stationsRadius(point: $point, distance: $distance, status: $status, limit: $limit) {
      station {
        id
        name
        longitude
        latitude
        latestTimestamp
        sourceId
      }
      latestData {
        wind {
          speed
          direction
          gust
        }
        height
        period
        timestamp
      }
    }
  }
`);
