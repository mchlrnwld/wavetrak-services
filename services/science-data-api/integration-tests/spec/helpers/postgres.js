import knex from 'knex';
import knexStringcase from 'knex-stringcase';
import { builtins } from 'pg-types';
import { types } from 'pg';
import toNumber from 'lodash/toNumber';
import moment from 'moment';

// Parse PSQL NUMERIC type to number
types.setTypeParser(builtins.NUMERIC, toNumber);
types.setTypeParser(builtins.TIMESTAMP, v => moment.utc(v).toDate());

export const setupPostgres = () => knex(knexStringcase({
  client: 'pg',
  connection: {
    host: 'database',
    user: 'postgres',
  },
}));

export const teardownPostgres = psql => psql.destroy();
