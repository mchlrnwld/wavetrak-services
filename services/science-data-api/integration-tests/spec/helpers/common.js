export const uuidObjectToString = uuid => [...Array(36)].map((_, i) => uuid[`${i}`]).join('');
