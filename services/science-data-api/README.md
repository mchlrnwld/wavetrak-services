# Science Data Service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents

- [Endpoints](#endpoints)
  - [/graphql](#graphql)
  - [/sns/spot-report-updated](#snsspot-report-updated)
    - [POST](#post)
      - [Sample Request](#sample-request)
      - [Sample Response](#sample-response)
  - [/model_runs](#model_runs)
    - [GET](#get)
      - [Query Parameters](#query-parameters)
      - [Sample Request](#sample-request-1)
      - [Sample Response](#sample-response-1)
    - [PUT](#put)
      - [Sample Request](#sample-request-2)
      - [Sample Response](#sample-response-2)
  - [/points_of_interest_grid_points](#points_of_interest_grid_points)
    - [GET](#get-1)
      - [Query Parameters](#query-parameters-1)
      - [Sample Request](#sample-request-3)
      - [Sample Response](#sample-response-3)
  - [/forecasts](#forecasts)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Endpoints

### /graphql

GraphQL endpoint supports queries and mutations and should be used in favor of other endpoints. In lower tier environments `GET /graphql` opens up a [GraphQL Playground](https://github.com/prisma-labs/graphql-playground), which is useful for exploring the graph.

### /sns/spot-report-updated

#### POST

Handles spot report updated SNS topic subscriptions.

##### Sample Request

```
HTTP/1.1 POST /sns/spot-report-updated
Content-Type: text/plain; charset=utf-8
x-amz-sns-message-type: Notification

{
  "TopicArn": "arn:aws:sns:us-west-1:123456789012:spot_report_updated",
  "Message": "{\"spotId\":\"5842041f4e65fad6a77088ed\"}"
}
```

##### Sample Response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
  message: "OK"
}
```

### /model_runs

#### GET

Search model runs by agency, model, run, and status.

##### Query Parameters

| Parameter | Required | Description |
| --------- | -------- | ----------- |
| `agency` | No | e.g. `NOAA`, `Wavetrak` |
| `model` | No | e.g. `GFS`, `Lotus-WW3` |
| `run` | No | Run date/time in the format `YYYYMMDDHH` e.g. `2018122400` |
| `status` | No | Comma separated list of statuses (`ONLINE`, `PENDING`, `OFFLINE`). Defaults to `ONLINE`. e.g. `PENDING,ONLINE` |

##### Sample Request

```
HTTP/1.1 GET /model_runs?agency=NOAA&model=GFS&run=2018122400&status=ONLINE,PENDING
```

##### Sample Response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

[
  {
    "agency": "NOAA",
    "model": "GFS",
    "run": 2018122400,
    "status": "ONLINE"
  }
]
```

#### PUT

Insert a list of model runs. If a model run already exists (defined by `agency`, `model`, `run` as a key), the status is updated.

##### Sample Request

```
HTTP/1.1 PUT /model_runs
Content-Type: application/json; charset=utf-8

[
  {
    "agency": "NOAA",
    "model": "GFS",
    "run": 2018122318,
    "status": "OFFLINE"
  },
  {
    "agency": "NOAA",
    "model": "GFS",
    "run": 2018122400,
    "status": "PENDING"
  }
]
```

##### Sample Response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
  message: 'OK'
}
```

### /points_of_interest_grid_points

#### GET

Search points of interests and the grid points they are configured to use.

##### Query Parameters

| Parameter | Required | Description |
| --------- | -------- | ----------- |
| `agency` | No | e.g. `NOAA`, `Wavetrak` |
| `model` | No | e.g. `GFS`, `Lotus-WW3` |
| `grid` | No | e.g. `0p25`, `GLOB_30m` |
| `offset` | No | Number of results to skip e.g. `10` |
| `limit` | No | Number of results to return e.g. `10` |

##### Sample Request

```
HTTP/1.1 GET /points_of_interest_grid_points?agency=NOAA&model=GFS&grid=0p25&offset=10&limit=1
```

##### Sample Response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

[
  {
    "pointOfInterestId": "0021938a-8a7f-490f-bf24-1dd45be55602",
    "agency": "NOAA",
    "model": "GFS",
    "grid": "0p25",
    "lat": 37.25,
    "lon": 351.25
  }
]
```

### /forecasts

See [Wavetrak Documentation](http://sl-docs.s3-us-west-1.amazonaws.com/wavetrak/index.html).

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `science-data-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TnpFd01EUTNOelkifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE56RXdNRFEzTnpZIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Go to the graphql Apollo UI and run queries against the service. It is located at: http://science-data-service.sandbox.surfline.com/graphql.