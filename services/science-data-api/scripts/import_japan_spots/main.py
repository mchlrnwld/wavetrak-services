import os
import logging
from distutils.util import strtobool

import psycopg2
from bson.objectid import ObjectId
from paramiko import AutoAddPolicy, SSHClient
from pymongo import MongoClient
from scp import SCPClient, SCPException

MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
OCEAN_HOST = os.environ['OCEAN_HOST']
PEM_FILE = os.environ['PEM_FILE']
POSTGRES_CONNECTION_STRING = os.environ['POSTGRES_CONNECTION_STRING']
COMMIT = strtobool(os.getenv('COMMIT', 'false'))

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
)


class POIRecord:
    def __init__(self, spot_id, name, lat, lon, lotus_lat, lotus_lon, optimal_swell_direction, optimal_wind_direction):
        self.poi_id = None
        self.spot_id = spot_id
        self.name = name
        self.lat = lat
        self.lon = lon
        self.lotus_lat = lotus_lat
        self.lotus_lon = lotus_lon
        self.optimal_swell_direction = optimal_swell_direction
        self.offshore_direction = optimal_wind_direction

    def __repr__(self):
        return f'<POIRecord spot_id: {self.spot_id}, name: {self.name}, lat: {self.lat}, lon: {self.lon}, lotus_lat: {self.lotus_lat}, lotus_lon: {self.lotus_lon}, optimal_swell_direction: {self.optimal_swell_direction}, offshore_direction: {self.offshore_direction}>'


def main():
    logging.info('Parsing POIs...')
    with open('japan_spots.csv') as f:
        f.readline()  # Skip header row
        poi_records = [
            POIRecord(
                csv_record[0],
                csv_record[1],
                float(csv_record[2]),
                float(csv_record[3]),
                float(csv_record[4]),
                float(csv_record[5]),
                float(csv_record[7]),
                float(csv_record[8]),
            )
            for csv_record in [
                line.strip().split(',')
                for line in f
            ]
        ]

    logging.info(f'{len(poi_records)} POIs parsed.')

    with psycopg2.connect(POSTGRES_CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            logging.info('Checking for POIs that already exist...')
            cur.execute(
                """
SELECT id, name, latitude, longitude
FROM points_of_interest
WHERE (name, latitude, longitude) IN %s;
                """,
                (
                    tuple([
                        (poi_record.name, poi_record.lat, poi_record.lon)
                        for poi_record in poi_records
                    ]),
                )
            )

            existing_pois = cur.fetchall()
            for poi_record in poi_records:
                existing_poi_id = next(
                    (
                        poi_id
                        for poi_id, name, lat, lon in existing_pois
                        if name == poi_record.name
                        if lat == poi_record.lat
                        if lon == poi_record.lon
                    ),
                    None,
                )
                if existing_poi_id:
                    poi_record.poi_id = existing_poi_id

            logging.info('Generating GFS grid points...')
            for poi_record in poi_records:
                cur.execute(
                    """
SELECT latitude, longitude
FROM model_grid_points
WHERE agency = 'NOAA'
AND model = 'GFS'
AND grid = '0p25'
AND ST_DWithin(ST_Point(%s, %s), geography_point, 40000)
ORDER BY ST_Distance(ST_Point(%s, %s), geography_point)
LIMIT 1;
                    """,
                    (poi_record.lon, poi_record.lat, poi_record.lon, poi_record.lat),
                );
                gfs_lat, gfs_lon = cur.fetchone()
                poi_record.gfs_lat = gfs_lat
                poi_record.gfs_lon = gfs_lon

    logging.info('Getting spectral refraction matrices...')
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy)
    ssh.connect(
        OCEAN_HOST,
        username='airflow',
        key_filename=os.path.expanduser(PEM_FILE),
    )
    with SCPClient(ssh.get_transport()) as scp:
        for poi_record in poi_records:
            local_file_path = os.path.join('data', f'{poi_record.spot_id}.wtm')
            if not os.path.exists(local_file_path):
                scp.get(
                    f'/ocean/static/bestwave/wtms/{poi_record.spot_id}.wtm',
                    local_file_path,
                )

            with open(local_file_path) as f:
                f.readline()  # Skip header row
                poi_record.spectral_refraction_matrix = [
                    [
                        float(value)
                        for value in line.strip().split()
                    ]
                    for line in f
                ]

    logging.info('Writing to databases...')
    client = MongoClient(MONGO_CONNECTION_STRING)
    with psycopg2.connect(POSTGRES_CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            for poi_record in poi_records:
                if not poi_record.poi_id:
                    cur.execute(
                        """
    INSERT INTO points_of_interest (name, latitude, longitude)
    VALUES (%s, %s, %s)
    RETURNING id;
                        """,
                        (poi_record.name, poi_record.lat, poi_record.lon),
                    )
                    poi_record.poi_id = cur.fetchone()[0]

                cur.execute(
                    """
INSERT INTO points_of_interest_grid_points (point_of_interest_id, agency, model, grid, latitude, longitude)
VALUES (%s, 'NOAA', 'GFS', '0p25', %s, %s),
       (%s, 'Wavetrak', 'Lotus-WW3', 'GLOB_15m', %s, %s)
ON CONFLICT (point_of_interest_id, agency, model, grid)
DO UPDATE
SET latitude = EXCLUDED.latitude,
    longitude = EXCLUDED.longitude;
                    """,
                    (
                        poi_record.poi_id,
                        poi_record.gfs_lat,
                        poi_record.gfs_lon,
                        poi_record.poi_id,
                        poi_record.lotus_lat,
                        poi_record.lotus_lon,
                    ),
                )

                cur.execute(
                    """
INSERT INTO points_of_interest_default_forecast_models (point_of_interest_id, agency, model, grid, forecast_type)
VALUES (%s, 'Wavetrak', 'Lotus-WW3', 'GLOB_15m', 'SURF'),
       (%s, 'Wavetrak', 'Lotus-WW3', 'GLOB_15m', 'SWELLS')
ON CONFLICT (point_of_interest_id, forecast_type)
DO NOTHING;
                    """,
                    (poi_record.poi_id, poi_record.poi_id),
                )

                cur.execute(
                    """
INSERT INTO surf_spot_configurations (point_of_interest_id, offshore_direction, optimal_swell_direction, spectral_refraction_matrix, breaking_wave_height_algorithm)
VALUES (%s, %s, %s, %s, 'SPECTRAL_REFRACTION')
ON CONFLICT (point_of_interest_id)
DO UPDATE
SET offshore_direction = EXCLUDED.offshore_direction,
    optimal_swell_direction = EXCLUDED.optimal_swell_direction,
    spectral_refraction_matrix = EXCLUDED.spectral_refraction_matrix,
    breaking_wave_height_algorithm = EXCLUDED.breaking_wave_height_algorithm;
                    """,
                    (
                        poi_record.poi_id,
                        poi_record.offshore_direction,
                        poi_record.optimal_swell_direction,
                        poi_record.spectral_refraction_matrix,
                    ),
                )

                if COMMIT:
                    client.KBYG.Spots.update_one({ '_id': ObjectId(poi_record.spot_id) }, { '$set': { 'pointOfInterestId': poi_record.poi_id } })

        if COMMIT:
            logging.info('Committing...')
        else:
            logging.info('Rolling back...')
            conn.rollback()

if __name__ == '__main__':
    main()
