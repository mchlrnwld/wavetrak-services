# Import Japan Spots

Script to import POI and related configurations for Japan spots.

The script is idempotent so it can be run multiple times without side effects.

1. Execute to:
  - Generate grid points for GFS and LOTUS.
  - Get spectral refraction matrices from ocean drive.
  - Upsert POI grid points and default forecast models for LOTUS and GFS.
  - Upsert POI surf spot configurations including spectral refraction matrices and offshore directions.
  - Update Spots collection `pointOfInterestId` in MongoDB.
2. Follow up with `generate_sst_grid_points` to generate SST grid points.

## Quickstart

```sh
conda devenv
conda activate import-japan-spots
env $(xargs < .env) python main.py
```
