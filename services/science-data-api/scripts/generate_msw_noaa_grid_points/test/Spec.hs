import Distance

import Control.Monad (void)
import Test.HUnit

-- | Wraper for approximate equality
newtype Approx = Approx Double
  deriving Show
instance Eq Approx where
  Approx a == Approx b = floor (a * 10000) == floor (b * 10000)

tests =
 test [ "haversine measures an equater section" ~:
          Approx (pi/4.0) ~=? Approx (haversine 1 (0, 0) (0, -45))
      , "haversine measures equater to pole" ~:
          Approx (pi/2.0) ~=? Approx (haversine 1 (0, 25) (90, -36)) -- Longitude values should not matter
      , "nearestCorner rounding down" ~:
          (51.0, -4.0) ~=? nearestCorner 0.5 (51.2, -3.8)
      , "nearestCorner rounding up" ~:
          (51.5, -3.5) ~=? nearestCorner 0.5 (51.3, -3.65)
      , "nearestCorner, northern hemisphere north is closer" ~:
          (51.5, -3.5) ~=? nearestCorner 0.5 (51.25, -3.65)
      , "nearestCorner, southern hemisphere south is closer" ~:
          (-51.5, -3.5) ~=? nearestCorner 0.5 (-51.25, -3.65)
      ]

main :: IO ()
main = void $ runTestTT tests
