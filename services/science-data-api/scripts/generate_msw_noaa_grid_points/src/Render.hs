{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Render where

import Types

import Control.Lens
import Data.List (intercalate)

class ShowField a where
  showField :: a -> String

instance ShowField Double where
  showField = show

instance ShowField String where
  showField a = "'" ++ a ++ "'" -- shody, but works here where no strings with contain '

instance (ShowField a) => ShowField (Maybe a) where
  showField (Just a) = showField a
  showField Nothing = "NULL"

class RenderInsert a where
  renderInsert :: a -> String

instance RenderInsert SurfSpotConfiguration where
  renderInsert spotConfig =
    upsert "surf_spot_grid_point_configurations"
           [ "point_of_interest_id", "agency", "model", "grid" ]
           [ showField $ spotConfig^.sscPoiId
                     , showField $ spotConfig^.sscAgency
                     , showField $ spotConfig^.sscModel
                     , showField $ spotConfig^.sscGrid ]
           [ "offshore_direction"
                     , "optimal_swell_direction"
                     , "breaking_wave_height_coefficient"
                     , "breaking_wave_height_intercept"
                     , "spectral_refraction_matrix"
                     , "breaking_wave_height_algorithm" ]
           [showField $ spotConfig^.sscOffshoreDirection
                     , showField $ spotConfig^.sscOptimalSwellDirection
                     , showField $ spotConfig^.sscBreakingWaveHeightCoefficient
                     , "0"
                     , "null"
                     , "'POLYNOMIAL'"]


instance RenderInsert GridPoint where
  renderInsert gridpoint =
    upsert "points_of_interest_grid_points"
           ["point_of_interest_id", "agency", "model", "grid"]
           [ showField $ gridpoint^.gpPoiId
                     , showField $ gridpoint^.gpAgency
                     , showField $ gridpoint^.gpModel
                     , showField $ gridpoint^.gpGrid ]
           ["latitude", "longitude"]
           [showField $ gridpoint^.gpLocation^._1
                     , showField $ gridpoint^.gpLocation^._2]


upsert :: String -- ^ Table name
       -> [String] -- ^ pk fields names
       -> [String] -- ^ pk values
       -> [String] -- ^ Non-pk field names
       -> [String] -- ^ Non-pk values
       -> String
upsert tableName pkFields pkValues dependantFields dependantValues =
  intercalate
    " "
    [ "INSERT INTO"
    , tableName
    , "(" , intercalate "," pkFields , "," , intercalate "," dependantFields , ")"
    , "VALUES (" , intercalate "," pkValues , "," , intercalate "," dependantValues , ")"
    , "ON CONFLICT ON CONSTRAINT"
    , tableName++"_pkey"
    , "DO UPDATE SET"
    , intercalate "," (zipWith (\a b -> a++"="++b) dependantFields dependantValues)
    , ";"
    ]
