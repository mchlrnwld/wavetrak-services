{-# LANGUAGE TemplateHaskell #-}

module Types where

import Control.Lens
import Data.Map

-- | latitude and longitude in degrees
type Location = (Double, Double)

-- | State to thread through our StateT monad
data AppState = AppState
  { _count :: Int
  , _nearestGridPointCache :: Map (String, Location) Location
  , _cacheHit :: Int
  , _cacheMiss :: Int
  }

initialState = AppState
  { _count = 0
  , _nearestGridPointCache = fromList []
  , _cacheHit = 0
  , _cacheMiss = 0
  }

makeLenses ''AppState

-- | Spot information from MSW db
data Spot = Spot
  { _sId :: String
  , _sName :: String
  , _sNWW3Resolution :: String
  , _sPoiId :: String
  , _optimumSwellAngle :: Maybe Double
  , _optimumWindAngle :: Maybe Double
  , _breakingWaveModifier :: Maybe Double
  , _sLocation :: Location -- ^ Actual location
  , _sGlo30 :: Maybe Location -- ^ NOAA glo_30m location
  , _sGfs30 :: Maybe Location -- ^ NOAA gfs 30m location
  , _sGfs15 :: Maybe Location -- ^ NOAA gfs 15m location
  , _sNWW3 :: Maybe Location -- ^^ location of the spot's selected NOAA WW3 grid
  }

makeLenses ''Spot

-- | Data to write to SDS points_of_interest_grid_points table
data GridPoint = GridPoint
  { _gpPoiId :: String
  , _gpAgency :: String
  , _gpModel :: String
  , _gpGrid :: String
  , _gpLocation :: Location
  }

makeLenses ''GridPoint

-- | Data to write to SDS surf_spot_grid_point_configurations table
data SurfSpotConfiguration = SurfSpotConfiguration
  { _sscPoiId :: String
  , _sscAgency :: String
  , _sscModel :: String
  , _sscGrid :: String
  , _sscOffshoreDirection :: Maybe Double
  , _sscOptimalSwellDirection :: Maybe Double
  , _sscBreakingWaveHeightCoefficient :: Maybe Double
  }

makeLenses ''SurfSpotConfiguration
