{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib where

import Types

import Data.Aeson.Lens (key, _Double)
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Control.Exception as E
import Control.Lens
import Control.Monad.Trans (liftIO, MonadIO)
import Control.Monad.Except
import Control.Monad.State (gets, modify, MonadState)
import Network.HTTP.Client (HttpException(..), HttpExceptionContent(..))
import Network.Wreq

-- | Use the MSW dataserver API to find the model grid point nearest to a given location
getNearestGridPoint :: (MonadIO m, MonadError String m)
                    => String   -- ^ Grid
                    -> Location -- ^ Search near this point
                    -> m Location
getNearestGridPoint grid (lat, lon) = do
  r <- liftIO $ E.try $ getWith opts "http://dataservices.metcentral.com:8085/SYSTEM/products/forecastLocation.php"

  r' <- case r of Left e@(HttpExceptionRequest _ (StatusCodeException res _)) -> throwError $ "dataserver API: http "++show (res ^. responseStatus)
                  Left e -> throwError $ "Spot API: "++show e
                  Right res -> return res

  let result = do lat' <- r' ^? responseBody . key "waveLat" . _Double
                  lon' <- r' ^? responseBody . key "waveLon" . _Double
                  return (lat', lon')

  case result of
    Nothing -> throwError "Malformed response from dataserver"
    Just pt -> if fst pt == 0 && snd pt == 0 then throwError "Got 0,0 back from dataserver" else return pt

    where opts = defaults & param "model" .~ [T.pack grid]
                          & param "lat"   .~ [T.pack $ show lat]
                          & param "lon"   .~ [T.pack $ show lon]

-- | Use the MSW dataserver API to find the model grid point nearest to a given location, but with memoization
getNearestGridPoint' :: (MonadIO m, MonadError String m, MonadState AppState m)
                     => String   -- ^ Grid
                     -> Location -- ^ Search near this point
                     -> m Location
getNearestGridPoint' grid pt = do
  cache <- gets (^.nearestGridPointCache)
  case M.lookup (grid, pt) cache of
    Just nearestPoint -> do modify (cacheHit +~ 1)
                            return nearestPoint
    Nothing -> do modify (cacheMiss +~ 1)
                  nearestPoint <- getNearestGridPoint grid pt
                  modify (nearestGridPointCache %~ M.insert (grid, pt) nearestPoint) --save value for later
                  return nearestPoint
