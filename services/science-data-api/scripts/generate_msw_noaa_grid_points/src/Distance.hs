module Distance where

import Types

import GHC.Float (powerDouble)

-- | Haversine distance between two points as crow flys on a sphere
haversine :: Double -- ^ radius of sphere
          -> Location
          -> Location
          -> Double
haversine r (lat1, lon1) (lat2, lon2) =
  let [lat1', lon1', lat2', lon2'] = map (* (pi/180.0)) [lat1, lon1, lat2, lon2]
      a1 = powerDouble (sin $ (lat2' - lat1')/2.0) 2
      a2 = cos lat1' * cos lat2'
      a3 = powerDouble (sin $ (lon2' - lon1')/2.0) 2
      a = a1 + a2 * a3
      c =  2 * atan2 (sqrt a) (sqrt $ 1- a)
  in r * c

-- | Haversine distance in km between two points (as crow flys, spherical earth).
distance_km :: Location
            -> Location
            -> Double
distance_km = haversine 6378


-- | find minimum, by a callback
minBy :: Ord b
      => (a -> b)
      -> [a] -- ^ error if this is empty
      -> a
minBy f (x:xs) = foldr (\a b -> if f a < f b then a else b) x xs


-- | Find the nearest of a list of points to a given point
nearest :: Location
        -> [Location]
        -> Location
nearest pt = minBy $ haversine 1 pt

-- | Find the grid corners around the given point, assuming a global
-- grid containing 0,0.
corners :: Double -- ^ grid resolution
        -> Location
        -> [Location]
corners r (lat, lon) =
    let m = getIndex lat
        n = getIndex lon
    in map (scale r . add (m, n)) [(0, 0), (1, 0), (0, 1), (1, 1)]

  where getIndex = fromIntegral . floor . (/r)
        scale x (a, b) = (x*a, x*b)
        (a, b) `add` (c, d) = (a + c, b + d)

-- | Find the nearest corner to a given point, assuming a global
-- grid containing 0,0.
nearestCorner :: Double -- ^ grid resolution
              -> Location
              -> Location
nearestCorner r pt = nearest pt $ corners r pt
