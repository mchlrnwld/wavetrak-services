# msw-noaa-points

This is used to generate insert/replace SQL for grid points for NOAA WW3,
Ensemble and GFS from a csv of MSW spot data.

## Building

Use [Haskell Stack](https://docs.haskellstack.org/), and build with
`stack build`. It'll take quite a while the first time.

## Operation

Input is read from standard in, results are written to standard out, and other
logging, including error messages are written to standard error.

The `glo_30m` grid point is found via a MSW dataserver API. Things can fail due
to spots being misconfigured in the MSW API, and data not being available at
certain points during MSW's import process. For any spots that fails, the
offending input line is written to standard error, so after a complete run, the
subset of inputs that failed is easily obtained from the error log with some
`grep`ing.

The GFS 0p25 grid point is taken as the nearest point to the one currently
configured for MSW, or the spot's location if there is nothing configured.

## Running

1. Create a csv file from the MSW production databases and `scp` to this folder.

```sql
  SELECT s.id,
    s.name,
    s.nww3Resolution,
    s.pointOfInterestId,
    s.optimumSwellAngle,
    s.optimumWindAngle,
    s.breakingWaveModifier,
    s.lat,
    s.lon,
    nww3.lat as nww3_lat,
    nww3.lon as nww3_lon,
    glo30.lat as glo30_lat,
    glo30.lon as glo30_lon,
    gfs.lat as gfs_lat,
    gfs.lon as gfs_lon,
    gfs15.lat as gfs15_lat,
    gfs15.lon as gfs15_lon
  FROM (SELECT *, IF(isDataSpot='yes', id, dataSpotId) AS dId FROM spot) AS s
  LEFT JOIN spotModelLatLon AS glo30 ON dId=glo30.dataSpotId AND glo30.dataset='glo_30m'
  LEFT JOIN spotModelLatLon AS gfs ON dId=gfs.dataSpotId AND gfs.dataset='gfs'
  LEFT JOIN spotModelLatLon AS gfs15 ON dId=gfs15.dataSpotId AND gfs15.dataset='gfs.0p25'
  LEFT JOIN spotModelLatLon AS nww3 ON dId=nww3.dataSpotId AND nww3.dataset=s.nww3Resolution
  WHERE s.pointOfInterestId IS NOT NULL
  INTO OUTFILE "/tmp/spots2.csv"
    FIELDS OPTIONALLY ENCLOSED BY '"'
    TERMINATED BY ',';
```

2. In order to access the MSW dataserver API (which is IP restricted) either
connect the the MSW UK Office VPN, or open an SSH tunnel like this

```bash
ssh -L 127.0.0.1:8085:dataservices.metcentral.com:80 makian.metcentral.com
```

and add a line in your `/etc/hosts` file directing dataservices.metcentral.com
to 127.0.0.1.

3. To run the tool, while both echoing and preserving the error log, use
`tee` --- something like this:

```bash
stack run<spots.csv > upsert_noaa.sql 2> >(tee -a error_log)
```
