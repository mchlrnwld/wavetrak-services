{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Distance
import Lib
import Render
import Types

import Control.Lens
import Control.Monad (forM_, unless)
import Control.Monad.Except (runExceptT)
import Control.Monad.Trans (liftIO, MonadIO)
import Control.Monad.State (execStateT, gets, modify, MonadState)
import qualified Data.ByteString as BS (readFile, writeFile)
import Data.Maybe (fromJust, fromMaybe)
import Data.Serialize (encode, decode)
import System.IO (stderr, hPutStrLn, putStrLn)
import System.Directory (doesFileExist)
import Text.CSV
import Text.Read (readMaybe)

-- | A bit like JavaScript ||.
(<|>) :: Maybe a -- ^ this if it's not Nothing.
      -> a       -- ^ otherwise this.
      -> a
infixr 0 <|>
(<|>) = flip fromMaybe

-- | Put line to stderr
putStrErr = hPutStrLn stderr

-- | Process one row of the CSV
processRow :: (MonadIO m, MonadState AppState m)
           => [String]
           -> m ()
processRow line = unless (line == [""]) $ do
  let id:name:nww3grid:poiId:swellAngle:windAngle:modifier:coordinates = line
      locx:locy:nww3x:nww3y:globx:globy:gfsx:gfsy:gfs15x:gfs15y:_ = map readMaybe coordinates :: [Maybe Double]
      spot = Spot
              { _sId = id
              , _sName = name
              , _sNWW3Resolution = nww3grid
              , _sPoiId = poiId
              , _optimumSwellAngle = readMaybe swellAngle
              , _optimumWindAngle = readMaybe windAngle
              , _breakingWaveModifier = readMaybe modifier
              -- Yes fromJust is terrible. But all the spots really should have a lat/lon
              , _sLocation = fromJust $ pairM locx locy
              , _sGlo30 = pairM globx globy
              , _sGfs30 = pairM gfsx gfsy
              , _sGfs15 = pairM gfs15x gfs15y
              , _sNWW3 = pairM nww3x nww3y
              }

  -- Log progress
  modify (count +~ 1)
  n <- gets (^.count)
  liftIO $ putStrErr $ show n++" processing "++spot^.sName++" ("++spot^.sPoiId++")"

  -- Ask the MSW dataserver what glo_30m grid point it would pick for the given coordinates.
  case spot^.sGlo30  of
    Nothing -> liftIO $ putStrErr $ "skiping ensemble for "++spot^.sName++". No MSW point."
    Just glo30Point -> do
      point <- runExceptT $ getNearestGridPoint' "glo_30m" glo30Point
      liftIO $ case point of
        -- All good, grid insert for ensemble
        Right pt -> putStrLn . renderInsert . GridPoint poiId "NOAA" "WW3-Ensemble" "glo_30m" $ norm pt
        -- Not good, print errors
        Left err -> do putStrErr $ "ERROR getting glo_30m location for "++spot^.sName++". "++err
                       putStrErr $ "RAW:" ++ printCSV [line]

  -- Ask the MSW dataserver what grid point it would pick for the given coordinates for the default NOAA Grid
  case spot^.sNWW3 of
    Nothing -> liftIO $ putStrErr $ "skiping nww3 for "++spot^.sName++". No MSW point."
    Just nww3Point -> do
      point <- runExceptT $ getNearestGridPoint' (spot^.sNWW3Resolution) nww3Point
      liftIO $ case point of
        -- All good, grid insert nww3
        Right pt -> do putStrLn . renderInsert . GridPoint poiId "NOAA" "WW3" (spot^.sNWW3Resolution) $ norm pt
                       putStrLn . renderInsert . SurfSpotConfiguration poiId "NOAA" "WW3" (spot^.sNWW3Resolution) (spot^.optimumWindAngle) (spot^.optimumSwellAngle) $ spot^.breakingWaveModifier
        -- Not good, print errors
        Left err -> do putStrErr $ "ERROR getting "++(spot^.sNWW3Resolution)++" location for "++spot^.sName++". "++err
                       putStrErr $ "RAW:" ++ printCSV [line]

  -- Find the nearest gfs 15m grid point (no need to check with dataserver as all points
  -- are valid), and print the INSERT. In order of preference, try the spots GFS 15m,
  -- GFS 30m, and actual location.
  let gfsLoc = nearestCorner 0.25 $ spot^.sGfs15 <|> spot^.sGfs30 <|> spot^.sLocation
  liftIO $ putStrLn . renderInsert . GridPoint poiId "NOAA" "GFS" "0p25" $ norm gfsLoc


 where
    -- Convert two maybe's into maybe a pair
    pairM a b = do
      a' <- a
      b' <- b
      return (a', b')
    -- Move lon into range [0, 360)
    norm pt@(lat, lon)
      | lon < 0 = norm (lat, lon + 360)
      | lon >= 360 = norm (lat, lon - 360)
      | otherwise = pt


main :: IO ()
main = do
  input <- getContents
  cacheFileExists <- doesFileExist "./pointcache.bin"
  initState <- if cacheFileExists
                then do cacheRaw <- BS.readFile "./pointcache.bin"
                        case decode cacheRaw of
                          Right cache -> return initialState {_nearestGridPointCache=cache}
                          Left error -> do putStrErr $ "Error parsing point cache: "++error
                                           return initialState
                else return initialState
  case parseCSV "stdin" input of
    Left e     -> putStrErr $ "ERROR reading csv data"++show e
    Right rows -> do
      putStrLn "BEGIN TRANSACTION;"
      finalState <- execStateT (forM_ rows processRow) initState
      putStrLn "COMMIT;"
      putStrErr "----------------------------------------"
      putStrErr $ "Cache hits: "++show (finalState^.cacheHit)
      putStrErr $ "Cache misses: "++show (finalState^.cacheMiss)
      BS.writeFile "./pointcache.bin" . encode $ finalState^.nearestGridPointCache
