BEGIN {
  FS=","
  print "use KBYG;"
}
NR>1 {
  print "db.Spots.update({\"_id\": ObjectId(\"" $1 "\")}, {$set: {\"lotusGrid\": \"" $4 "\"}});";
}
