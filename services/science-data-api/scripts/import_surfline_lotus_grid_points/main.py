import csv
import logging
import os

from pymongo import MongoClient

LOTUS_POINTS_CSV = os.environ['LOTUS_POINTS_CSV']
MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']

LOGGER = logging.getLogger('import-surfline-lotus-grid-points')
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler())


def main():
    connection_str = f'mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}'
    client = MongoClient(connection_str)
    spots_collection = client[MONGO_DATABASE].Spots

    spot_ids_to_poi_ids = {
        str(spot['_id']): spot['pointOfInterestId']
        for spot in spots_collection.find({'status': 'PUBLISHED'}, {'pointOfInterestId': 1})
    }

    with open(LOTUS_POINTS_CSV, mode='r', newline='') as lotus_file:
        header = lotus_file.readline().strip()
        if header != (
            'SpotId,GridLon,GridLat,DefaultGrid'
        ):
            raise AssertionError(
                f'{LOTUS_POINTS_CSV} does not have expected header'
            )
        reader = csv.reader(lotus_file)
        for spot_id, grid_lon, grid_lat, default_grid in reader:
            if spot_id not in spot_ids_to_poi_ids:
                LOGGER.error(f'ERROR: No POI ID for spot ID {spot_id}')
                continue

            poi_id = spot_ids_to_poi_ids[spot_id]
            print(f"SELECT '{poi_id}';")

            lon, lat = float(grid_lon), float(grid_lat)
            if lon < 0:
                lon += 360

            print(
                f"""
DELETE from surf_spot_grid_point_configurations
WHERE point_of_interest_id = '{poi_id}'
AND agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid != '{default_grid}';

DELETE from points_of_interest_grid_points
WHERE point_of_interest_id = '{poi_id}'
AND agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid != '{default_grid}';

INSERT INTO points_of_interest_grid_points
(point_of_interest_id, agency, model, grid, latitude, longitude)
VALUES ('{poi_id}', 'Wavetrak', 'Lotus-WW3', '{default_grid}', {lat}, {lon:.3f})
ON CONFLICT ON CONSTRAINT points_of_interest_grid_points_pkey
DO UPDATE SET latitude={lat}, longitude={lon:.3f};
                """
            )


if __name__ == '__main__':
    main()
