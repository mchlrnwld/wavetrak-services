# Import Surfline Lotus grid points

Generates:
1. SQL to 'upsert' `points_of_interest_grid_points` for Surfline/Lotus from a
Lotus point CSV provided by data-science, and a CSV mapping SL spot IDs to SDS
POI IDs.
1. Mongo update statements to set the `lotusGrid` parameter on each spot, from
the value specifies in the data-science CSV.

## Input files

`Lotus_SpotList.csv` is provided by data-science team. The spots IDs are
those from `prod`

`sl_spots_published_{env}.csv` are exports from the Mongo `Spots` collection

## Update SDS Points of Interest

```sh
$ conda env create -f environment.yml
$ source activate import_surfline_lotus_grid_points
$ cp .env.sample .env
```

Update the `.env` file.

```sh
$ env $(xargs < .env) python main.py > update_poi_lotus_points.sql
```

Run the generated SQL files against the SDS database of the corresponding
environment.

## Update the default Lotus grid in the Mongo Spots collections

```sh
awk -f main.awk Lotus_SpotList_Surfline.csv >update_sl_spots.mongo
```

Then run `update_sl_spots.mongo` on the relevant Mongo shell. Note that the spot
IDs are from `prod`, but it should mostly work for `dev` too as many (most?) of
the spot IDs are the same.
