from argparse import ArgumentParser
import atexit
from datetime import datetime
import json
import logging
import math
import os
import requests

logger = logging.getLogger('qa')


# (label, map for SDS, map for best WX)
weather_values_to_check = [
    (
        'pressure',
        lambda x: round(x),
        lambda x: x,
    ),
    (
        'temperature',
        lambda x: round(x, 1),
        lambda x: x,
    ),
    (
        'visibility',
        lambda x: round(x),
        lambda x: round(x * 1000),
    ),
    (
        'dewpoint',
        lambda x: round(x, 1),
        lambda x: x,
    ),
    (
        'humidity',
        lambda x: round(x),
        lambda x: x,
    ),
    (
        'precipitation',
        lambda x: round(x['volume']),
        lambda x: x['volume'],
    ),
]


def check_weather_value(
    label,
    timestamp,
    sds_value,
    best_wx_value,
    map_sds,
    map_best_wx,
):
    mapped_sds_value = map_sds(sds_value)
    mapped_best_wx_value = map_best_wx(best_wx_value)
    absolute_error = abs(mapped_sds_value - mapped_best_wx_value)
    relative_error = (absolute_error / mapped_best_wx_value
                      if mapped_best_wx_value > 0
                      else absolute_error)
    if relative_error > 0.1:
        logger.debug(f'"{label}" not a match for {sds_value} '
                     f'({mapped_sds_value}) and {best_wx_value} '
                     f'({mapped_best_wx_value}) at {timestamp}')
        return False

    return True


def check_wind_direction(
    timestamp,
    sds_direction,
    best_wx_direction,
):
    sds_radians = math.radians(sds_direction)
    sds_y_positive = math.sin(sds_radians) > 0
    sds_x_positive = math.cos(sds_radians) > 0

    best_wx_radians = math.radians(best_wx_direction)
    best_wx_y_positive = math.sin(best_wx_radians) > 0
    best_wx_x_positive = math.cos(best_wx_radians) > 0

    if (
        sds_y_positive != best_wx_y_positive or
        sds_x_positive != best_wx_x_positive
    ):
        logger.debug(f'"direction" in different quadrants for {sds_direction} '
                     f'and {best_wx_direction} at {timestamp}')
        return False

    return True


def qa_data_point(sds_point, best_wx_point, best_wind_point):
    sds_timestamp = sds_point['timestamp']
    best_wx_timestamp = best_wx_point['timestamp']
    best_wind_timestamp = best_wind_point['timestamp']
    if (
        sds_timestamp != best_wx_timestamp or
        sds_timestamp != best_wind_timestamp
    ):
        logger.debug(f'"timestamp" not a match for {sds_timestamp}, '
                     f'{best_wx_timestamp}, and {best_wind_timestamp}')
        return False

    sds_weather = sds_point['weather']
    best_weather = best_wx_point['weather']

    weather_check_results = {label: check_weather_value(label,
                                                        sds_timestamp,
                                                        sds_weather[label],
                                                        best_weather[label],
                                                        map_sds,
                                                        map_best_wx)
                             for (label,
                                  map_sds,
                                  map_best_wx) in weather_values_to_check}

    sds_wind = sds_weather['wind']
    best_wind = best_wind_point['wind']
    wind_check_results = {
        'wind.speed': check_weather_value('speed',
                                          sds_timestamp,
                                          sds_wind['speed'],
                                          best_wind['speed'],
                                          lambda x: x,
                                          lambda x: x),
        'wind.direction': check_wind_direction(sds_timestamp,
                                               sds_wind['direction'],
                                               best_wind['direction']),
    }

    return {**weather_check_results, **wind_check_results}


def qa_lat_lon(lat, lon, sds_host, best_wx_host, hours):
    sds_params = {
        'agency': 'NOAA',
        'model': 'GFS',
        'grid': '0p25',
        'lat': lat,
        'lon': lon,
        'interval': 10800,
    }
    sds_response = requests.get(f'{sds_host}/forecasts/weather',
                                params=sds_params)
    sds_response.raise_for_status()
    sds_data = sds_response.json()['data'][:hours]
    start = sds_data[0]['timestamp']
    end = sds_data[-1]['timestamp'] + 1

    best_wx_params = {
        'lat': lat,
        'lon': lon,
        'start': start,
        'end': end,
        'interval': 180,
    }
    best_wx_response = requests.get(f'{best_wx_host}/weather',
                                    params=best_wx_params)
    best_wx_response.raise_for_status()
    best_wx_data = best_wx_response.json()['data']

    best_wind_response = requests.get(f'{best_wx_host}/wind',
                                      params=best_wx_params)
    best_wind_response.raise_for_status()
    best_wind_data = best_wind_response.json()['data']

    qa_results_by_hour = [
        qa_data_point(sds_point, best_wx_point, best_wind_point)
        for sds_point, best_wx_point, best_wind_point
        in zip(sds_data, best_wx_data, best_wind_data)]

    return qa_results_by_hour


def main():
    parser = ArgumentParser(description=(
        'QA Science Data Service weather forecast data against '
        'Best Weather API forecast data.'))
    parser.add_argument(
        '--log-dir',
        default='logs',
        help='log directory',
    )
    parser.add_argument(
        '--hours',
        type=int,
        default=128,
        help='hours to compare',
    )
    parser.add_argument(
        '--sds-host',
        default='http://science-data-service.sandbox.surfline.com',
        help='Science Data Service host',
    )
    parser.add_argument(
        '--best-wx-host',
        default='http://weather-api.sandbox.surfline.com',
        help='Best Weather API host',
    )
    args = parser.parse_args()

    log_path = os.path.join(args.log_dir, f'{datetime.now()}.log')
    os.makedirs(args.log_dir, exist_ok=True)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    file_handler = logging.FileHandler(log_path)
    file_handler.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)

    lat = 0
    results = {
        'correct_labels': {},
        'total_labels': 0,
        'correct_points': 0,
        'total_points': 0,
    }

    def log_accuracy_results():
        logger.info('\nResults:')
        for label, count in results['correct_labels'].items():
            accuracy = 100 * (count / results['total_labels'])
            logger.info(f'"{label}" {accuracy}% accurate')

        overall_accuracy = (100 * (results['correct_points'] /
                                   results['total_points']))
        logger.info(f'Overall points {overall_accuracy}% accurate')

    # Log results at exit to allow logging partial results when script is
    # killed.
    atexit.register(log_accuracy_results)

    for lon4 in range(-180 * 4, 180 * 4, 1):
        lon = lon4 / 4
        qa_results_by_hour = qa_lat_lon(
            lat,
            lon,
            args.sds_host,
            args.best_wx_host,
            args.hours,
        )
        correct_acc = 0
        total_acc = 0
        for qa_result in qa_results_by_hour:
            for label, is_match in qa_result.items():
                if is_match:
                    correct_acc += 1
                    if label in results['correct_labels']:
                        results['correct_labels'][label] += 1
                    else:
                        results['correct_labels'][label] = 1
                total_acc += 1
            results['total_labels'] += 1

        results['total_points'] += 1

        if correct_acc == total_acc:
            results['correct_points'] += 1

        logger.info(f'({lat}, {lon}) '
                    f'{"" if correct_acc == total_acc else "✗"}')


if __name__ == '__main__':
    main()
