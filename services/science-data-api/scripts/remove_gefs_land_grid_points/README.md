# Remove GEFS-Wave land grid points from model_grid_points table

Python script for removing GEFS-Wave land grid points from the model_grid_points table.

## Setup

```sh
$ conda env create -f environment.yml
$ conda activate remove_gefs_land_grid_points
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the PSQL connection string.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python main.py
    ```
