import os
import logging
from timeit import default_timer
import psycopg2
import boto3
import pygrib
import numpy

PSQL_URI = os.environ['PSQL_URI']
TEMP_FILE_NAME = 'temp_gefs_global_0p25_land_locations.csv'

logger = logging.getLogger(__name__)


def copy_s3_object_to_local_file(bucket, key, local_file) -> None:
    client = boto3.client('s3')
    client.download_file(bucket, key, local_file)


def create_coord_csv_from_masked_in_grib(grib_file: str) -> None:
    with pygrib.open(grib_file) as grib:
        variables = grib.read()
        (lats, lons) = variables[0].latlons()
        name = 'Significant height of combined wind waves and swell'
        variable_index = next(
            x for x, val in enumerate(variables) if name in f'{val}'
        )
        values = variables[variable_index].data()[0]
        masked_x, masked_y = numpy.where(values.mask)
        masked_indices = list(zip(masked_x, masked_y))
        masked_coordinates = [
            (round(lons[i[0]][i[1]], 2), round(lats[i[0]][i[1]], 2))
            for i in masked_indices
        ]
        with open(TEMP_FILE_NAME, 'w') as f:
            f.write(
                '\n'.join(
                    [
                        ','.join([str(longitude), str(latitude)])
                        for longitude, latitude in masked_coordinates
                    ]
                )
            )


def remove_points_from_model_grid_points_table(commit: bool = False) -> None:
    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    CREATE TEMP TABLE temp_gefs_global_0p25_land_locations (
                        longitude       FLOAT NOT NULL,
                        latitude        FLOAT NOT NULL
                    );
                """
            )

            with open(TEMP_FILE_NAME) as csv:
                cur.copy_expert(
                    f'COPY temp_gefs_global_0p25_land_locations (longitude, latitude) FROM STDIN WITH CSV',
                    csv,
                )

            cur.execute(
                """
                DELETE FROM model_grid_points
                WHERE agency = 'NOAA'
                    AND model = 'GEFS-Wave'
                    AND grid = 'global.0p25'
                    AND (longitude, latitude) IN (
                        SELECT longitude, latitude FROM temp_gefs_global_0p25_land_locations
                    );
                """,
                [TEMP_FILE_NAME],
            )

        if not commit:
            conn.rollback()


def main():
    s3_bucket = os.environ['S3_BUCKET']
    s3_object_key = os.environ['S3_OBJECT_KEY']
    commit = os.environ['COMMIT'].lower() == 'true'
    local_file = s3_object_key.split("/")[-1]

    logger.info(f'Copying object from s3')
    start = default_timer()
    copy_s3_object_to_local_file(s3_bucket, s3_object_key, local_file)
    elapsed = default_timer() - start
    logger.info(f'Copied in {elapsed}s')

    logger.info(f'Creating CSV of coordinates from all masked points in grib')
    start = default_timer()
    create_coord_csv_from_masked_in_grib(local_file)
    elapsed = default_timer() - start
    logger.info(f'Created CSV in {elapsed}s')

    logger.info(f'Removing points from model_grid_points table')
    start = default_timer()
    remove_points_from_model_grid_points_table(commit)
    elapsed = default_timer() - start
    logger.info(f'Removed points in {elapsed}s')


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main()
