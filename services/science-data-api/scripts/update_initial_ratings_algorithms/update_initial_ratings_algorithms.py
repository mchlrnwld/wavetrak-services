import logging
import os
from csv import DictReader
from timeit import default_timer
from typing import List, Optional

import psycopg2
from bson import ObjectId
from pymongo import MongoClient

BIG_WAVE_CSV_FILE = os.environ['BIG_WAVE_CSV_FILE']
VALIDATED_SPOTS_CSV_FILE = os.environ['VALIDATED_SPOTS_CSV_FILE']
MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']
PSQL_URI = os.environ['PSQL_URI']

logger = logging.getLogger('update_initial_ratings_algorithms')


class RowFromCsv:
    def __init__(self, row: dict):
        self.spot_id = row['SpotId']
        self.name = row['Name']


class Spot:
    def __init__(self, data: dict):
        self.spot_id = str(data['_id'])
        self.point_of_interest_id = data['pointOfInterestId']


def get_rows_from_csv(file_name: str) -> [RowFromCsv]:
    with open(file_name, mode='r', newline='') as data_file:
        reader = DictReader(data_file)
        return [RowFromCsv(row) for row in reader]


def get_spots_from_mongo(spot_ids: List[str]) -> List[Spot]:
    connection_str = f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}"
    mongo_client = MongoClient(connection_str)
    spots_collection = mongo_client[MONGO_DATABASE].Spots
    return [
        Spot(record)
        for record in spots_collection.find(
            {'_id': {'$in': [ObjectId(s) for s in spot_ids]}},
            {'_id': 1, 'pointOfInterestId': 1},
        )
    ]


def report_any_spots_from_csv_not_found_in_mongo(
    spots: List[Spot], csv_rows: List[RowFromCsv]
) -> None:
    found_spot_ids = [str(spot.spot_id) for spot in spots]
    spots_ids_not_found = list(
        set(found_spot_ids).difference([x.spot_id for x in csv_rows])
    )
    logger.info(
        f'There were {len(spots_ids_not_found)} spots that were not found:'
    )
    logger.info(spots_ids_not_found)


def update_ratings_algorithms(
    spots: Optional[List[Spot]],
    ratings_algorithm: Optional[str],
    commit: bool = False,
) -> None:

    query = (
        "UPDATE surf_spot_configurations SET ratings_algorithm = %s WHERE point_of_interest_id in %s"
        if spots
        else "UPDATE surf_spot_configurations SET ratings_algorithm = %s"
    )

    args = (
        [ratings_algorithm, tuple(x.point_of_interest_id for x in spots)]
        if spots
        else [ratings_algorithm]
    )

    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            cur.execute(query, args)

        if not commit:
            conn.rollback()


def read_spot_ids_from_csv_and_update_ratings_algorithm(
    csv_file_name: str, ratings_algorithm: Optional[str]
) -> None:
    logger.info(f'Reading spots from {csv_file_name}')
    start = default_timer()
    csv_rows = get_rows_from_csv(csv_file_name)
    elapsed = default_timer() - start
    logger.info(f'Read {len(csv_rows)} rows in {elapsed}s.')

    logger.info(f'Reading spot info from mongo')
    start = default_timer()
    spots = get_spots_from_mongo([row.spot_id for row in csv_rows])
    elapsed = default_timer() - start
    logger.info(f'Read {len(spots)} spots in {elapsed}s.')

    report_any_spots_from_csv_not_found_in_mongo(spots, csv_rows)

    logger.info(f'Updating surf spot configurations')
    start = default_timer()
    commit = os.environ['COMMIT'].lower() == 'true'
    update_ratings_algorithms(spots, ratings_algorithm, commit)
    elapsed = default_timer() - start
    logger.info(f'Finished updating surf spot configurations in {elapsed}s.')


def main():
    logger.info(f'Updating all surf spot configurations to ML_RATINGS_MINIMAL')
    start = default_timer()
    commit = os.environ['COMMIT'].lower() == 'true'
    update_ratings_algorithms(
        spots=None, ratings_algorithm='ML_RATINGS_MINIMAL', commit=commit
    )
    elapsed = default_timer() - start
    logger.info(
        f'Finished updating all surf spot configurations in {elapsed}s.'
    )

    read_spot_ids_from_csv_and_update_ratings_algorithm(
        BIG_WAVE_CSV_FILE, None
    )

    read_spot_ids_from_csv_and_update_ratings_algorithm(
        VALIDATED_SPOTS_CSV_FILE, 'ML_SPECTRA_REF_MATRIX'
    )


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main()
