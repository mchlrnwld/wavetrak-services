# Surfline update initial ratings algorithms

Python script for initializing the automated surf ratings algorithms per instructions from data science: 
- `ML_RATINGS_MINIMAL` will be applied to all spots
- `ML_SPECTRA_REF_MATRIX` will be applied to select spots with refraction matrices that have been validated (`validated_spots.csv`)
- `null` will be applied to big wave spots (`big_wave_spots.csv`)

## Setup

```sh
$ conda env create -f environment.yml
$ conda activate update_initial_ratings_algorithms
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the mongodb connection information for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python update_initial_ratings_algorithms.py
    ```
