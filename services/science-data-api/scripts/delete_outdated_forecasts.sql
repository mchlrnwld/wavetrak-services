BEGIN;
EXPLAIN (FORMAT JSON) WITH duplicates AS (
    SELECT
        point_of_interest_id,
        agency,
        model,
        grid,
        forecast_time,
        COUNT(run)
    FROM weather
    GROUP BY point_of_interest_id, agency, model, grid, forecast_time
    HAVING COUNT(run) > 1
), ranks AS (
    SELECT
        weather.point_of_interest_id,
        weather.agency,
        weather.model,
        weather.run,
        weather.grid,
        weather.forecast_time,
        ROW_NUMBER() OVER (PARTITION BY weather.point_of_interest_id, weather.agency, weather.model, weather.grid, weather.forecast_time ORDER BY weather.run DESC) AS row_number
    FROM weather
    INNER JOIN duplicates
    ON weather.point_of_interest_id = duplicates.point_of_interest_id
    AND weather.agency = duplicates.agency
    AND weather.model = duplicates.model
    AND weather.grid = duplicates.grid
    AND weather.forecast_time = duplicates.forecast_time
)
DELETE FROM weather
USING ranks
WHERE weather.point_of_interest_id = ranks.point_of_interest_id
AND weather.agency = ranks.agency
AND weather.model = ranks.model
AND weather.grid = ranks.grid
AND weather.run = ranks.run
AND weather.forecast_time = ranks.forecast_time
AND ranks.row_number > 1;
ROLLBACK;
