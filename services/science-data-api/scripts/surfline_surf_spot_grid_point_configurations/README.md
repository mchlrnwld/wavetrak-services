# Surfline surf_spot_grid_point_configurations Sync

Python script for syncing surf spot grid point configurations (offshore direction, breaking wave height corrections, spectral refraction matrix, and breaking wave height algorithm) to the Science Data Service's Postgres database for points of interest.

## Setup

```sh
$ conda env update -f environment.yml
$ conda activate surfline-surf-spot-grid-point-configurations
$ cp .env.sample .env
```

## Run
```sh
$ env $(xargs < .env) python main.py
Getting Surfline spots from MongoDB...
4148 Surfline spots found in 0.64430186s.
Downloading WTM files from ocean to data...
scp: /ocean/static/bestwave/wtms/5ed9645dcd13351ea7979ed6.wtm: No such file or directory
Finished downloading WTM files in 684.80609295s.
Parsing configurations for spots...
[Errno 2] No such file or directory: 'data/5ed9645dcd13351ea7979ed6.wtm'
Finished parsing configurations in 1.2427435539999578s.
Writing to Postgres (commit=True)...
Finished writing to Postgres in 470.0540000159999s.
```
