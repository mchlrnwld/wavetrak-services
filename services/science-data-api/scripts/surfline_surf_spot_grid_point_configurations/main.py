import logging
import os
import sys
from timeit import default_timer
from typing import Any, Dict, List, Optional

import psycopg2  # type: ignore
from paramiko import AutoAddPolicy, SSHClient  # type: ignore
from pymongo import MongoClient  # type: ignore
from scp import SCPClient, SCPException  # type: ignore

logger = logging.getLogger('surfline-surf-spot-grid-point-configurations')


class SurfSpotConfiguration:
    def __init__(
        self,
        point_of_interest_id: str,
        offshore_direction: float,
        spectral_refraction_matrix: Optional[List[List[float]]],
    ):
        self.point_of_interest_id = point_of_interest_id
        self.offshore_direction = offshore_direction
        self.spectral_refraction_matrix = spectral_refraction_matrix


def find_surfline_spots(mongo_uri: str) -> List[Dict[str, Any]]:
    client = MongoClient(mongo_uri)
    db = client['KBYG']
    spots = db['Spots']
    return list(
        spots.find(
            {'status': 'PUBLISHED'},
            {'_id': 1, 'pointOfInterestId': 1, 'offshoreDirection': 1},
        )
    )


def wtm_file(spot_id: str) -> str:
    return f'{spot_id}.wtm'


def download_spectral_refraction_matrices(
    spot_ids: List[str], destination: str, ocean_host: str, pem_file: str,
):
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy)
    ssh.connect(
        ocean_host,
        username='airflow',
        key_filename=os.path.expanduser(pem_file),
    )

    start = default_timer()
    with SCPClient(ssh.get_transport()) as scp:
        for counter, spot_id in enumerate(spot_ids):
            local_file_path = os.path.join(destination, wtm_file(spot_id))
            if not os.path.exists(local_file_path):
                try:
                    scp.get(
                        os.path.join(
                            '/ocean/static/bestwave/wtms', wtm_file(spot_id)
                        ),
                        local_file_path,
                    )
                except SCPException as e:
                    logger.error(e)

            current_time = default_timer()
            elapsed = current_time - start

            # Display download status
            if elapsed > 1:
                print(
                    f'WTM files downloaded: {counter}',
                    file=sys.stderr,
                    end='\r',
                )
                start = current_time


def parse_spectral_refraction_matrix(
    wtm_file_path: str,
) -> Optional[List[List[float]]]:
    try:
        with open(wtm_file_path) as f:
            return [
                [float(value) for value in line.strip().split()]
                for line in f.read().strip().split('\n')[1:]
            ]
    except FileNotFoundError as e:
        logger.error(e)
        return None


def upsert_surf_spot_configurations(
    surf_spot_configurations: List[SurfSpotConfiguration],
    psql_uri: str,
    commit: bool = False,
):
    with psycopg2.connect(psql_uri) as conn:
        with conn.cursor() as cur:
            for configuration in surf_spot_configurations:
                cur.execute(
                    """
SELECT grid
FROM points_of_interest_grid_points
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND point_of_interest_id = (%s)
""",
                    [configuration.point_of_interest_id],
                )

                for (grid,) in cur.fetchall():
                    cur.execute(
                        """
INSERT INTO surf_spot_grid_point_configurations (
    point_of_interest_id,
    agency,
    model,
    grid,
    offshore_direction,
    breaking_wave_height_coefficient,
    breaking_wave_height_intercept,
    spectral_refraction_matrix,
    breaking_wave_height_algorithm
)
VALUES (
    (%s),
    'Wavetrak',
    'Lotus-WW3',
    (%s),
    (%s),
    1,
    0,
    (%s),
    'SPECTRAL_REFRACTION'
)
ON CONFLICT
ON CONSTRAINT surf_spot_grid_point_configurations_pkey
DO UPDATE SET
    offshore_direction = EXCLUDED.offshore_direction,
    breaking_wave_height_coefficient = EXCLUDED.breaking_wave_height_coefficient,
    breaking_wave_height_intercept = EXCLUDED.breaking_wave_height_intercept,
    spectral_refraction_matrix = EXCLUDED.spectral_refraction_matrix,
    breaking_wave_height_algorithm = EXCLUDED.breaking_wave_height_algorithm
""",
                        [
                            configuration.point_of_interest_id,
                            grid,
                            configuration.offshore_direction,
                            configuration.spectral_refraction_matrix,
                        ],
                    )

        if not commit:
            conn.rollback()


def main(
    mongo_uri: str,
    psql_uri: str,
    ocean_host: str,
    pem_file: str,
    data_dir: str,
    commit: bool,
):
    os.makedirs(data_dir, exist_ok=True)

    logger.info('Getting Surfline spots from MongoDB...')
    start = default_timer()
    surfline_spots = find_surfline_spots(mongo_uri)
    elapsed = default_timer() - start
    logger.info(f'{len(surfline_spots)} Surfline spots found in {elapsed}s.')

    logger.info(f'Downloading WTM files from ocean to {data_dir}...')
    start = default_timer()
    download_spectral_refraction_matrices(
        [spot['_id'] for spot in surfline_spots],
        data_dir,
        ocean_host,
        pem_file,
    )
    elapsed = default_timer() - start
    logger.info(f'Finished downloading WTM files in {elapsed}s.')

    logger.info('Parsing configurations for spots...')
    start = default_timer()
    surf_spot_configurations = []
    for spot in surfline_spots:
        surf_spot_configurations.append(
            SurfSpotConfiguration(
                spot['pointOfInterestId'],
                spot['offshoreDirection'],
                parse_spectral_refraction_matrix(
                    os.path.join(data_dir, wtm_file(spot['_id']))
                ),
            )
        )

    elapsed = default_timer() - start
    logger.info(f'Finished parsing configurations in {elapsed}s.')

    logger.info(f'Writing to Postgres (commit={commit})...')
    start = default_timer()
    upsert_surf_spot_configurations(surf_spot_configurations, psql_uri, commit)
    elapsed = default_timer() - start
    logger.info(f'Finished writing to Postgres in {elapsed}s.')


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    main(
        os.environ['MONGO_URI'],
        os.environ['PSQL_URI'],
        os.environ['OCEAN_HOST'],
        os.environ['PEM_FILE'],
        os.environ['DATA_DIR'],
        os.environ['COMMIT'].lower() == 'true',
    )
