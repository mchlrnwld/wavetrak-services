import logging
import os
import csv
from timeit import default_timer
from typing import Any, Dict, List
import psycopg2
from pymongo import MongoClient

OFFSHORE_DIRECTIONS_CSV = os.environ['OFFSHORE_DIRECTIONS_CSV']
MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']
PSQL_URI = os.environ['PSQL_URI']

logger = logging.getLogger('import_offshore_direction_from_spots')


class SurfSpotConfiguration:
    def __init__(self, point_of_interest_id: str, offshore_direction: float):
        self.point_of_interest_id = point_of_interest_id
        self.offshore_direction = offshore_direction


def find_surfline_spots() -> List[Dict[str, Any]]:
    connection_str = f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}"
    client = MongoClient(connection_str)
    spots_collection = client[MONGO_DATABASE].Spots
    return list(
        spots_collection.find(
            {
                'pointOfInterestId': {'$exists': True, '$ne': None},
                'offshoreDirection': {'$exists': True, '$ne': None},
            },
            {'_id': 1, 'pointOfInterestId': 1, 'offshoreDirection': 1},
        )
    )


def read_missing_offshore_directions_file() -> List[SurfSpotConfiguration]:
    with open(OFFSHORE_DIRECTIONS_CSV, mode='r', newline='') as data_file:
        reader = csv.reader(data_file)
        return [
            SurfSpotConfiguration(
                point_of_interest_id,
                offshore_direction
            )
            for point_of_interest_id, offshore_direction in reader
        ]


def update_surf_spot_configurations(
    surf_spot_configurations: List[SurfSpotConfiguration],
    commit: bool = False,
) -> None:
    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            for config in surf_spot_configurations:
                cur.execute(
                    """
UPDATE surf_spot_grid_point_configurations
SET optimal_swell_direction = %s
WHERE point_of_interest_id = %s AND optimal_swell_direction IS NULL
""",
                    [config.offshore_direction, config.point_of_interest_id],
                )

        if not commit:
            conn.rollback()


def main(commit: bool):

    logger.info('Getting Surfline spots from MongoDB...')
    start = default_timer()
    surfline_spots = find_surfline_spots()
    elapsed = default_timer() - start
    logger.info(f'{len(surfline_spots)} Surfline spots found in {elapsed}s.')

    logger.info('Parsing configurations for spots...')
    start = default_timer()
    surf_spot_configurations = [
        SurfSpotConfiguration(
            spot['pointOfInterestId'],
            spot['offshoreDirection']
        )
        for spot in surfline_spots
    ]
    elapsed = default_timer() - start
    logger.info(f'Finished parsing configurations in {elapsed}s.')

    logger.info(f'Writing to Postgres (commit={commit})...')
    start = default_timer()
    update_surf_spot_configurations(surf_spot_configurations, commit)
    elapsed = default_timer() - start
    logger.info(f'Finished writing to Postgres in {elapsed}s.')

    logger.info(f'Reading spots from {OFFSHORE_DIRECTIONS_CSV}')
    start = default_timer()
    spots_from_csv = read_missing_offshore_directions_file()
    elapsed = default_timer() - start
    logger.info(f'Finished reading {len(spots_from_csv)} spots from {OFFSHORE_DIRECTIONS_CSV} in {elapsed}s.')

    logger.info(f'Writing to Postgres (commit={commit})...')
    start = default_timer()
    update_surf_spot_configurations(spots_from_csv, commit)
    elapsed = default_timer() - start
    logger.info(f'Finished writing to Postgres in {elapsed}s.')


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main(os.environ['COMMIT'].lower() == 'true')
