# Surfline surf_spot_grid_point_configurations offshore direction sync

Python script for syncing surf spot grid point configurations (offshore direction) to the Science Data Service's Postgres database for points of interest.

## Setup

```sh
$ conda env create -f environment.yml
$ conda activate import_offshore_direction_from_spots
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the mongodb connection information for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python import_offshore_direction_from_spots.py
    ```
