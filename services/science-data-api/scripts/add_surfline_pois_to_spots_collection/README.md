# Add Point of Interest ID to Spots Collection

Adds point of interest id to the Spots Collection. This maps entries from the points_of_interest table in SDS Postgres to the Spots collection in mongodb.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate add-surfline-pois-to-spots-collection
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the mongodb connection information and postgres connection string for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python add_poi_id_to_spots_collection.py
    ```
