import asyncio
from functools import partial
import os

from pymongo import MongoClient
from bson.objectid import ObjectId
import psycopg2

MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']
POSTGRES_CONNECTION_STRING = os.environ['POSTGRES_CONNECTION_STRING']


class PointOfInterest:
    def __init__(
        self,
        name,
        longitude,
        latitude,
        spot_id=None,
        point_of_interest_id=None,
    ):
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
        self.spot_id = spot_id
        self.point_of_interest_id = point_of_interest_id


async def get_surfline_points_of_interest(
    conn, latitude, longitude, spot_id, name
):
    loop = asyncio.get_event_loop()
    with conn.cursor() as cur:
        await loop.run_in_executor(
            None,
            partial(
                cur.execute,
                (
                    f"""
SELECT name, longitude, latitude, id
FROM points_of_interest
WHERE product = 'SL' and latitude = %s and longitude = %s and name = %s;
        """
                ),
                [latitude, longitude, name],
            ),
        )
        point = await loop.run_in_executor(None, cur.fetchone)
        return PointOfInterest(
            name=point[0] if point else name,
            longitude=point[1] if point else longitude,
            latitude=point[2] if point else latitude,
            point_of_interest_id=point[3] if point else None,
            spot_id=spot_id if point else spot_id,
        )


async def main():
    with psycopg2.connect(POSTGRES_CONNECTION_STRING) as conn:
        connection_str = f'mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}'
        client = MongoClient(connection_str)
        spots_collection = client[MONGO_DATABASE].Spots

        surfline_spots = spots_collection.find({'status': 'PUBLISHED'})
        surfline_spots_pois = [
            PointOfInterest(
                name=spot['name'],
                longitude=spot['location']['coordinates'][0],
                latitude=spot['location']['coordinates'][1],
                spot_id=spot['_id'],
            )
            for spot in surfline_spots
        ]
        print(f'Found {len(surfline_spots_pois)} Surfline Spots')

        surfline_sds_pois = await asyncio.gather(
            *[
                get_surfline_points_of_interest(
                    conn, poi.latitude, poi.longitude, poi.spot_id, poi.name
                )
                for poi in surfline_spots_pois
            ]
        )

        print(
            f'Adding {len(surfline_sds_pois)} point of interest IDs to the Spots collection'
        )

        spots_updated, spots_failed, spots_skipped = ([],[],[])
        for poi in surfline_sds_pois:
            if poi.point_of_interest_id is not None:
                result = spots_collection.update_one(
                    {'_id': ObjectId(poi.spot_id)},
                    {"$set": {"pointOfInterestId": poi.point_of_interest_id}},
                    upsert=False,
                )
                if result.modified_count == 1 and result.matched_count == 1:
                    spots_updated.append(poi)
                    print(
                        f'Update succeeded for Spot with name: {poi.name} id: {poi.spot_id} and latitude: {poi.latitude} and longitude: {poi.longitude}'
                    )
                else:
                    spots_failed.append(poi)
                    print(
                        f'Update failed for Spot with name: {poi.name} id: {poi.spot_id} and latitude: {poi.latitude} and longitude: {poi.longitude}'
                    )
            else:
                spots_skipped.append(poi)
                print(
                    f'Update skipped for Spot with name: {poi.name} id: {poi.spot_id} and latitude: {poi.latitude} and longitude: {poi.longitude} due to no match from the Forecast Platform'
                )
        print(f'{len(spots_updated)} spots updated out of {len(surfline_sds_pois)}')
        print(f'{len(spots_skipped)} spots skipped out of {len(surfline_sds_pois)}')
        print(f'{len(spots_failed)} spots failed to update out of {len(surfline_sds_pois)}')



if __name__ == '__main__':
    asyncio.run(main())
