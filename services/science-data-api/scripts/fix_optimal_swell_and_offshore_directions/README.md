# Fix Optimal Swell and Offshore Direction for Select Points of Interest

Python script to update optimal swell and offshore directions for points of interest that were found to have 0/0 or 180/180 values.

## Setup

```sh
$ conda env create -f environment.yml
$ conda activate fix_optimal_swell_and_offshore_directions
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the connection information for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python main.py
    ```
