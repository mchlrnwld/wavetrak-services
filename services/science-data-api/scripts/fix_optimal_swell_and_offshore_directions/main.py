import logging
import os
from csv import DictReader
from timeit import default_timer
from typing import List, Dict

import psycopg2
from bson import ObjectId
from pymongo import MongoClient

SPOT_CSV = os.environ['SPOT_CSV']
MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']
PSQL_URI = os.environ['PSQL_URI']

logger = logging.getLogger('fix_optimal_swell_and_offshore_directions')


class RowFromCsv:
    def __init__(self, row: dict):
        self.spot_id = row['id']
        self.name = row['spotName']
        self.offshore_direction = row['windDir']
        self.optimal_swell_direction = row['swellDir']


class Spot:
    def __init__(self, data: dict):
        self.spot_id = str(data['_id'])
        self.point_of_interest_id = data['pointOfInterestId']


def get_csv_rows_by_spot_id() -> Dict[str, RowFromCsv]:
    with open(SPOT_CSV, mode='r', newline='') as data_file:
        reader = DictReader(data_file)
        return {row['id']: RowFromCsv(row) for row in reader}


def get_spots_from_mongo(spot_ids: List[str]) -> List[Spot]:
    connection_str = f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}"
    mongo_client = MongoClient(connection_str)
    spots_collection = mongo_client[MONGO_DATABASE].Spots
    return [
        Spot(record)
        for record in spots_collection.find(
            {'_id': {'$in': [ObjectId(s) for s in spot_ids]}},
            {'_id': 1, 'pointOfInterestId': 1},
        )
    ]


def report_any_spots_from_csv_not_found_in_mongo(
    spots: List[Spot], csv_rows_by_spot_id: Dict[str, RowFromCsv]
) -> None:
    found_spot_ids = [str(spot.spot_id) for spot in spots]
    spots_not_found = list(
        filter(
            lambda x: x not in found_spot_ids, list(csv_rows_by_spot_id.keys())
        )
    )
    logger.info(
        f'There were {len(spots_not_found)} spots that were not found:'
    )
    logger.info(spots_not_found)


def set_offshore_direction_and_optimal_swell_direction(
    spots: List[Spot], rows: Dict[str, RowFromCsv], commit: bool = False,
) -> None:
    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            for spot in spots:
                cur.execute(
                    """
                    UPDATE surf_spot_configurations
                    SET offshore_direction = %s, optimal_swell_direction = %s
                    WHERE point_of_interest_id = %s
                    """,
                    [
                        rows[spot.spot_id].offshore_direction,
                        rows[spot.spot_id].optimal_swell_direction,
                        spot.point_of_interest_id,
                    ],
                )

        if not commit:
            conn.rollback()


def main():
    logger.info(f'Reading spot data from CSV')
    start = default_timer()
    csv_rows_by_spot_id = get_csv_rows_by_spot_id()
    elapsed = default_timer() - start
    logger.info(f'Read {len(csv_rows_by_spot_id)} rows in {elapsed}s.')

    logger.info(f'Reading spots from mongo')
    start = default_timer()
    spots = get_spots_from_mongo(list(csv_rows_by_spot_id.keys()))
    elapsed = default_timer() - start
    logger.info(f'Read {len(spots)} spots in {elapsed}s.')

    report_any_spots_from_csv_not_found_in_mongo(spots, csv_rows_by_spot_id)

    logger.info(f'Updating surf spot configurations')
    start = default_timer()
    commit = os.environ['COMMIT'].lower() == 'true'
    set_offshore_direction_and_optimal_swell_direction(
        spots, csv_rows_by_spot_id, commit
    )
    elapsed = default_timer() - start
    logger.info(f'Finished updating surf spot configurations in {elapsed}s.')


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main()
