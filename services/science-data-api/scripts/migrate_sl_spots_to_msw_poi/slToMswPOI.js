db = db.getSiblingDB("KBYG");

var input = cat('./poi.txt');
input = input.split('\n');

var columns = input[0].split('\t');

var id = columns.indexOf('_id');
var mswPOI = columns.indexOf('MSW POI ID');
var slPOI = columns.indexOf('SL POI ID');

var updatedCount = 0;

for(i = 1; i < input.length; i++) {
    var row = input[i].split('\t');

    print('id: ' + row[id]);
    var cursor = db.Spots.find({
        "_id" : ObjectId(row[id])
    });
    var curr;
    while(cursor.hasNext()) {
        curr = cursor.next()
    }
    if(curr && curr.pointOfInterestId === row[slPOI]) {
        curr.pointOfInterestId = row[mswPOI];
        var result = db.getCollection("Spots").update({
            "_id": ObjectId(row[id])
        }, {
            $set: { "pointOfInterestId": row[mswPOI] }
        });

        if(result.nMatched === 1 && result.nModified === 1) {
            updatedCount += 1;
            print(`updated ${row[id]} pointOfInterestId from ${row[slPOI]} to ${row[mswPOI]}`);
        } else {
            print(`An error occured: ${result}`);
        }
    }

}

print(`updated ${updatedCount} rows`);
