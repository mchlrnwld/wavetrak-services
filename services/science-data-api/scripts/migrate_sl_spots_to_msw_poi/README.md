# Migrate SL spots to MSW POIs in UK

Iterate through list of UK Spots and update `pointOfInterestId` from `SL POI ID` to `MSW POI ID` from `poi.txt`.

# Setup

Using Studio3T, navigate to `Spots` collection, and open script in query console.
