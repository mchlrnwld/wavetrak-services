# Seed NOAA Nam Grid Points

Seed NOAA NAM grid points into the `model_grid_points` table in Postgres. This script downloads NAM file for each grid if the files have not already been downloaded. Set the `COMMIT` environment variable to `false` to test seeding the grid points to Postgres, or set to `true` to commit the changes to Postgres.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate seed-noaa-nam-grid-points
$ cp .env.sample .env
```

## Running

### Generate Surfline Points of Interest CSV

1. Edit your `.env` file with the Postgres connection string for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python main.py
    ```
