from distutils.util import strtobool
import logging
import os
from typing import Dict

import psycopg2
import pygrib
import re
import requests

from bs4 import BeautifulSoup

COMMIT = strtobool(os.environ['COMMIT'])
NOAA_NAM_URL = os.environ['NOAA_NAM_URL']
POSTGRES_CONNECTION_STRING = os.environ['POSTGRES_CONNECTION_STRING']

GRIDS = ['conus', 'prico', 'hawaii']

logger = logging.getLogger('seed-noaa-nam-grid-points')


def parse_latlons_from_grib(file: str):
    with pygrib.open(file) as grib:
        variables = grib.read()
        return variables[0].latlons()


def list_http_contents(url: str) -> list:
    response = requests.get(url)
    response.raise_for_status()
    soup = BeautifulSoup(response.text, 'html.parser')
    return [
        node.get('href')
        for node in soup.find_all('a')
        if 'Parent Directory' not in node.contents
    ]


def get_nam_grid_files_to_download() -> Dict[str, str]:
    date_regex = re.compile(r'^nam\.\d{8}\/?$')
    model_date_dir = [
        dir.replace('/', '')
        for dir in list_http_contents(NOAA_NAM_URL)
        if date_regex.match(dir)
    ][0]

    model_date = model_date_dir.replace('nam.', '')

    model_date_url = f'{NOAA_NAM_URL}/{model_date_dir}'

    available_files = list_http_contents(model_date_url)

    nam_grid_files_to_download = {}
    for grid in GRIDS:
        nam_file_nam_regex = re.compile(
            rf'^nam\.'
            rf't\d{{2}}z\.'
            rf'({grid})nest\.'
            rf'hiresf\d{{2}}\.'
            rf'tm\d{{2}}\.'
            rf'grib2'
            rf'$'
        )
        nam_grid_file = sorted(
            [
                file
                for file in available_files
                if nam_file_nam_regex.match(file)
            ]
        )[0]
        nam_grid_files_to_download[grid] = nam_grid_file

    return nam_grid_files_to_download, model_date


def download_nam_files(nam_files: Dict[str, str], model_date: str) -> str:
    for _, nam_file in nam_files.items():
        if os.path.exists(nam_file):
            logger.info(f'nam file: {nam_file} already downloaded')
            continue

        nam_url = f'{NOAA_NAM_URL}/nam.{model_date}/' f'{nam_file}'
        response = requests.get(nam_url)
        response.raise_for_status()

        with open(nam_file, 'wb') as file:
            for chunk in response.iter_content(chunk_size=128):
                file.write(chunk)
            logger.info(f'Downloaded {nam_file}')


def append_to_csv(grid, longitude, latitude):
    csv_name = 'noaa_nam_grid_points.csv'
    write_header = True
    if os.path.exists(csv_name):
        write_header = False
    with open('noaa_nam_grid_points.csv', 'a') as csv:
        if write_header:
            csv.write('agency,model,grid,longitude,latitude\n')
        line = ','.join(['NOAA', 'NAM', grid, str(longitude), str(latitude)])
        csv.write(f'{line}\n')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    nam_files, model_date = get_nam_grid_files_to_download()
    download_nam_files(nam_files, model_date)

    for grid, nam_file in nam_files.items():
        logger.info(f'Writing model grid points to csv for grid: {grid}')
        lats, lons = parse_latlons_from_grib(nam_file)
        for i in range(lats.shape[0]):
            for j in range(lats.shape[1]):
                lat = lats[i][j]
                lon = lons[i][j]
                append_to_csv(grid, lons[i][j], lats[i][j])

    with psycopg2.connect(POSTGRES_CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            logger.info('Copying model grid points to Postgres')
            with open('noaa_nam_grid_points.csv') as csv:
                columns = csv.readline()
                csv.seek(0)
                cur.copy_expert(
                    f'COPY model_grid_points ({columns}) FROM STDIN WITH CSV HEADER',
                    csv,
                )

        if not COMMIT:
            conn.rollback()


if __name__ == "__main__":
    main()
