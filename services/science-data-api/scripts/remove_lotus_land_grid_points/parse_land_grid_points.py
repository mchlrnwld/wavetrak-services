#!/usr/bin/env python

import logging
import os
import re
from timeit import default_timer

from netCDF4 import Dataset  # type: ignore

logger = logging.getLogger()

DATA_DIR = 'data'
LOTUS_SAMPLE_DIR = os.path.join(DATA_DIR, 'lotus')
FILE_GRID_REGEX = re.compile(r'ww3\_lotus\_(.+\_\d+m)\.\d{10}\.\d{3}\.nc')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    files = os.listdir(LOTUS_SAMPLE_DIR)
    all_land_grid_points = []
    for file in files:
        start = default_timer()

        logger.info(file)
        grid_match = FILE_GRID_REGEX.match(file)
        if not grid_match:
            raise Exception('Invalid file name.')

        grid = grid_match.group(1)
        logger.info(grid)

        nc = Dataset(os.path.join(LOTUS_SAMPLE_DIR, file))
        significant_wave_height = nc['hs'][:].tolist()[0]
        latitudes = nc['latitude']
        longitudes = nc['longitude']
        land_grid_points = [
            (grid, latitude, longitude)
            for i, latitude in enumerate(latitudes)
            for j, longitude in enumerate(longitudes)
            if significant_wave_height[i][j] is None
        ]
        logger.info(
            f'{len(land_grid_points)} land grid points found out of '
            f'{len(latitudes) * len(longitudes)}.'
        )

        all_land_grid_points.extend(land_grid_points)

        elapsed = default_timer() - start
        logger.info(f'Finished in {elapsed}s.')

    output_path = os.path.join(DATA_DIR, 'land_grid_points.csv')
    logger.info(f'Writing to {output_path}...')
    with open(output_path, 'w') as f:
        f.write(
            '\n'.join(
                [
                    ','.join([grid, str(latitude), str(longitude)])
                    for grid, latitude, longitude in all_land_grid_points
                ]
            )
        )


if __name__ == '__main__':
    main()
