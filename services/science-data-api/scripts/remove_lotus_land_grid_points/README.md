# Remove LOTUS Land Grid Points

Removes LOTUS land grid points from `model_grid_points` table. Land grid points determined by significant wave height property not containing data.

## Quickstart

Run a few scripts to generate the data for land grid points and then execute script to delete these grid points from Postgres.

```sh
$ conda devenv
$ conda activate remove-lotus-land-grid-points
$ ./get_lotus_samples.sh
$ ./parse_land_grid_points.py
$ psql -h $PSQL_HOST -U $PSQL_USER -d SciencePlatform
SciencePlatform=> \i delete_land_grid_points.sql
```
