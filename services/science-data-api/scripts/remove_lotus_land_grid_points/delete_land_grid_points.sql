BEGIN;

CREATE TEMP TABLE temp_land_locations (
    grid            VARCHAR(25) NOT NULL,
    latitude        FLOAT NOT NULL,
    longitude       FLOAT NOT NULL
);

\copy temp_land_locations from 'data/land_grid_points.csv' delimiter ',' csv

DELETE FROM model_grid_points
WHERE agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND (grid, latitude, longitude) IN (
    SELECT grid, latitude, longitude FROM temp_land_locations
);

ROLLBACK;
