#!/usr/bin/env bash

RUN=$(aws s3 ls s3://surfline-science-s3-dev/lotus/archive/grids/GLOB_30m/ | head -n1 | awk '{print $NF}' | sed 's/\///')
mkdir -p data/lotus
aws s3 ls s3://surfline-science-s3-dev/lotus/archive/grids/ |\
  awk '{print $NF}' |\
  sed 's/\///' |\
  xargs -I{} aws s3 cp "s3://surfline-science-s3-dev/lotus/archive/grids/{}/${RUN}/ww3_lotus_{}.${RUN}.000.nc" data/lotus/
