# Seed Wavetrak WRF Grid Points

Seed Wavetrak WRF grid points into the `model_grid_points` table in Postgres. This script downloads WRF file for each grid from `/ocean` if the files have not already been downloaded. Set the `COMMIT` environment variable to `false` to test seeding the grid points to Postgres, or set to `true` to commit the changes to Postgres.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate seed-wavetrak-wrf-grid-points
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the Postgres connection string, the hostname of the EC2 instance where `/ocean` is mounted, and the PEM file needed to connect to the EC2 instance.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python main.py
    ```
