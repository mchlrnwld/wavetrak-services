import logging
import os
from distutils.util import strtobool
from typing import Dict, Tuple

import numpy as np  # type: ignore
import psycopg2  # type: ignore
from netCDF4 import Dataset
from paramiko import AutoAddPolicy, SSHClient  # type: ignore
from scp import SCPClient, SCPException  # type: ignore

COMMIT = strtobool(os.environ['COMMIT'])
OCEAN_HOST = os.environ['OCEAN_HOST']
PEM_FILE = os.environ['PEM_FILE']
POSTGRES_CONNECTION_STRING = os.environ['POSTGRES_CONNECTION_STRING']

GRIDS = [
    "hawaii_1km",
    "tahiti_1km",
    "aust_nsw",
    "aust_qland",
    "aust_south",
    "aust_vic_tas",
    "aust_west",
    "baja",
    "bali",
    "brazil_rio",
    "fiji",
    "med_east",
    "mexico_central",
    "new_zealand",
    "puerto_rico",
    "sumatra_new",
    "uk",
    "iberia",
]

logger = logging.getLogger('seed-wavetrak-wrf-grid-points')


def download_wrf_files(ocean_host: str, pem_file: str,) -> Dict[str, str]:
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy)
    ssh.connect(
        ocean_host,
        username='airflow',
        key_filename=os.path.expanduser(pem_file),
    )

    wrf_files = {}

    with SCPClient(ssh.get_transport()) as scp:
        for grid in GRIDS:
            if os.path.exists(grid):
                grid_file = os.listdir(grid)[0]
                logger.info(
                    f'File: {grid_file} already downloaded for grid: {grid}'
                )
                wrf_files[grid] = grid_file
                continue

            os.makedirs(grid)

            # Each WRF grid has multiple different resolutions. Pick the
            # highest resolution grid file.
            _, stdout, _ = ssh.exec_command(
                f'ls /ocean_aws_prod/wrf/wrfout-U10-V10/{grid}'
            )
            highest_resolution_grid_file = sorted(
                [line.replace('\n', '') for line in stdout.readlines()],
                reverse=True,
            )[0]

            wrf_files[grid] = highest_resolution_grid_file
            file_path = f'{grid}/{highest_resolution_grid_file}'
            logger.info(
                f'Downloading file: {highest_resolution_grid_file} for grid: {grid}'
            )
            try:
                scp.get(
                    os.path.join(
                        '/ocean_aws_prod/wrf/wrfout-U10-V10', file_path
                    ),
                    file_path,
                )
            except SCPException as e:
                logger.error(e)

    return wrf_files


def parse_lat_lon_from_wrf_file(file_path: str) -> Tuple[np.array, np.array]:
    with Dataset(file_path) as wrf_file:
        return wrf_file.variables['XLAT'][:], wrf_file.variables['XLONG'][:]


def append_to_csv(csv_name, grid, longitude, latitude):
    write_header = True
    if os.path.exists(csv_name):
        write_header = False
    with open(csv_name, 'a') as csv:
        if write_header:
            csv.write('agency,model,grid,longitude,latitude\n')
        line = ','.join(['Wavetrak', 'WRF', grid, str(longitude), str(latitude)])
        csv.write(f'{line}\n')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    csv_name = 'wavetrak_wrf_grid_points.csv'

    wrf_files = download_wrf_files(OCEAN_HOST, PEM_FILE)

    for grid, file in wrf_files.items():
        logger.info(f'Adding points for grid: {grid}')
        lats, lons = parse_lat_lon_from_wrf_file(f'{grid}/{file}')
        for i in range(lats.shape[1]):
            for j in range(lats.shape[2]):
                append_to_csv(
                    csv_name, grid, lons[0][i][j], lats[0][i][j]
                )

    with psycopg2.connect(POSTGRES_CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            with open(csv_name) as csv:
                columns = csv.readline()
                csv.seek(0)
                cur.copy_expert(
                    f'COPY model_grid_points ({columns}) FROM STDIN WITH CSV HEADER',
                    csv,
                )
            if not COMMIT:
                conn.rollback()


if __name__ == "__main__":
    main()
