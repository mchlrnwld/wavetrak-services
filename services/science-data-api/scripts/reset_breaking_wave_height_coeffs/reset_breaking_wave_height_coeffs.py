import os
import logging
from typing import List
import psycopg2
import csv
from timeit import default_timer

PSQL_URI = os.environ['PSQL_URI']
SPOT_CSV = os.environ['SPOT_CSV']

logger = logging.getLogger('reset_breaking_wave_height_coefficients')


def read_spot_csv_file() -> List[str]:
    with open(SPOT_CSV, mode='r', newline='') as data_file:
        reader = csv.reader(data_file)
        next(reader)
        return [
            spot_poi_id
            for spot_name, spot_id, spot_poi_id, lotus_raw_mae, lotus_corrected_mae, model_ob_matches in reader
        ]

def reset_breaking_wave_height_coefficients(
    point_of_interest_ids: List[str],
    commit: bool = False,
) -> None:
    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            for poi in point_of_interest_ids:
                cur.execute(
                    """
                    UPDATE surf_spot_configurations
                    SET breaking_wave_height_coefficient = 1, breaking_wave_height_intercept = 0
                    WHERE point_of_interest_id = %s
                    """,
                    [poi]
                )

        if not commit:
            conn.rollback()

def main(commit: bool):
    logger.info(f'Reading spot data from {SPOT_CSV} ...')
    start = default_timer()
    poi_ids = read_spot_csv_file()
    elapsed = default_timer() - start
    logger.info(f'Finished reading {len(poi_ids)} spots in {elapsed}s.')

    logger.info(f'Resetting breaking wave height coefficients for spots ...')
    start = default_timer()
    reset_breaking_wave_height_coefficients(poi_ids, commit)
    elapsed = default_timer() - start
    logger.info(f'Finished resetting breaking wave height coefficients in {elapsed}s.')

if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main(os.environ['COMMIT'].lower() == 'true')
