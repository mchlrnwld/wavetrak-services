# Surfline point of interest breaking wave height coefficient reset

Python script for resetting some Surfline point of interest breaking wave height coefficients.

## Setup

```sh
$ conda env create -f environment.yml
$ conda activate reset_breaking_wave_height_coeffs
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the connection information for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python reset_breaking_wave_height_coeffs.py
    ```
