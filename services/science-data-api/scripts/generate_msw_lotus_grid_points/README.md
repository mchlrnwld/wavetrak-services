# Generate MSW Lotus grid points

Generates MSW Lotus grid point insert/update statements from a Lotus point CSV
provided by data-science, and a CSV mapping MSW spot IDs to SDS POI IDs.

## Input files

`Lotus_SpotList.csv` is provided by data-science team.

`MSW_Spot_POIs_prod.csv` can be generated like this:

```sh
mysql -u $USERNAME -p -D magicseaweed -h obi.metcentral.com \
  <<<"select id, breakingWaveModifier, pointOfInterestId from spot;" \
  | sed 's/\t/,/g' \
  > MSW_Spot_POIs_prod.csv
```

## Setup and running

```sh
$ conda env create -f environment.yml
$ source activate generate_lotus_grid_points
$ cp .env.sample .env
```

Update the `.env` file.

```sh
$ env $(xargs < .env) python main.py > update_poi_lotus_points.sql
```

Run the generated SQL files against the SDS database of the corresponding
environment.

## Update the default Lotus grid in the MSW database

```sh
awk -F, 'NR>1 { print "UPDATE spot SET lotusGrid=\"" $5 "\" WHERE id=" $1 ";" }' \
  <Lotus_SpotList.csv \
  >update_msw_spots.sql
```

Then run `update_msw_spots.sql` on the MSW databases.
