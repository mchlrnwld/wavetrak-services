import csv
import os
import sys

LOTUS_POINTS_CSV = os.environ['LOTUS_POINTS_CSV']
SPOT_POI_CSV = os.environ['SPOT_POI_CSV']


def main():
    with open(SPOT_POI_CSV, mode='r', newline='') as spots_file:
        header = spots_file.readline()
        if header != 'id,breakingWaveModifier,pointOfInterestId\n':
            raise AssertionError(
                f'{SPOT_POI_CSV} does not have expected header'
            )

        reader = csv.reader(spots_file)
        poi_ids = {
            spot_id: (poi_id, multiplier)
            for spot_id, multiplier, poi_id in reader
            if poi_id != 'NULL'
        }

    with open(LOTUS_POINTS_CSV, mode='r', newline='') as lotus_file:
        header = lotus_file.readline().strip()
        if header != (
            'SpotId,SpotLon,SpotLat,OptimalWindDirection,DefaultGrid,Grid,'
            'GridLon,GridLat,GridSwellDirection'
        ):
            raise AssertionError(
                f'{LOTUS_POINTS_CSV} does not have expected header'
            )
        reader = csv.reader(lotus_file)
        for (
            spot_id,
            _,
            _,
            optimal_wind,
            default_grid,
            *grid_parameters,
        ) in reader:
            if spot_id not in poi_ids:
                print(
                    f'ERROR: No POI ID for spot ID {spot_id}',
                    file=sys.stderr,
                )
                continue

            poi_id, multiplier = poi_ids[spot_id]
            print(f"SELECT '{poi_id}';")

            for i in range(0, int(len(grid_parameters)/4)):
                grid, lon, lat, optimal_swell = grid_parameters[i*4:(i+1)*4]
                if grid != default_grid:
                    continue

                lon = float(lon)
                if lon < 0:
                    lon += 360

                print(
                    f"""
DELETE from surf_spot_grid_point_configurations
WHERE point_of_interest_id = '{poi_id}'
AND agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid != '{grid}';

DELETE from points_of_interest_grid_points
WHERE point_of_interest_id = '{poi_id}'
AND agency = 'Wavetrak'
AND model = 'Lotus-WW3'
AND grid != '{grid}';

INSERT INTO points_of_interest_grid_points
(point_of_interest_id, agency, model, grid, latitude, longitude)
VALUES ('{poi_id}', 'Wavetrak', 'Lotus-WW3', '{grid}', {lat}, {lon:.3f})
ON CONFLICT ON CONSTRAINT points_of_interest_grid_points_pkey
DO UPDATE SET latitude={lat}, longitude={lon:.3f};

INSERT INTO surf_spot_grid_point_configurations (
    point_of_interest_id,
    agency,
    model,
    grid,
    offshore_direction,
    optimal_swell_direction,
    breaking_wave_height_coefficient,
    breaking_wave_height_intercept,
    spectral_refraction_matrix,
    breaking_wave_height_algorithm
)
VALUES (
    '{poi_id}',
    'Wavetrak',
    'Lotus-WW3',
    '{grid}',
    {optimal_wind},
    {optimal_swell},
    {multiplier},
    0,
    NULL,
    'POLYNOMIAL'
)
ON CONFLICT ON CONSTRAINT surf_spot_grid_point_configurations_pkey
DO UPDATE SET
  offshore_direction={optimal_wind},
  optimal_swell_direction={optimal_swell},
  breaking_wave_height_coefficient={multiplier},
  breaking_wave_height_intercept=0,
  spectral_refraction_matrix=NULL,
  breaking_wave_height_algorithm='POLYNOMIAL';
                    """
                )


if __name__ == '__main__':
    main()
