import os
from contextlib import ExitStack

import psycopg2
import pymysql.cursors
from psycopg2.errors import UniqueViolation

MSW_DB_HOST = os.environ['MSW_DB_HOST']
MSW_DB_USER = os.environ['MSW_DB_USER']
MSW_DB_PORT = int(os.environ['MSW_DB_PORT'])
MSW_DB_PASS = os.environ['MSW_DB_PASS']
MSW_DB_NAME = os.environ['MSW_DB_NAME']

SCIENCE_PLATFORM_DB_HOST = os.environ['SCIENCE_PLATFORM_DB_HOST']
SCIENCE_PLATFORM_DB_DATABASE = os.environ['SCIENCE_PLATFORM_DB_DATABASE']
SCIENCE_PLATFORM_DB_USER = os.environ['SCIENCE_PLATFORM_DB_USER']
SCIENCE_PLATFORM_DB_PASSWORD = os.environ['SCIENCE_PLATFORM_DB_PASSWORD']


def connect_to_msw():
    return pymysql.connect(
        host=MSW_DB_HOST,
        port=MSW_DB_PORT,
        user=MSW_DB_USER,
        password=MSW_DB_PASS,
        db=MSW_DB_NAME,
    )


def main():
    with ExitStack() as stack:
        sds_connection = stack.enter_context(
            psycopg2.connect(
                dbname=SCIENCE_PLATFORM_DB_DATABASE,
                user=SCIENCE_PLATFORM_DB_USER,
                password=SCIENCE_PLATFORM_DB_PASSWORD,
                host=SCIENCE_PLATFORM_DB_HOST,
            )
        )

        sds_cursor = stack.enter_context(sds_connection.cursor())

        msw_cursor = stack.enter_context(connect_to_msw())
        spots_iterator = stack.enter_context(connect_to_msw())

        spots_iterator.execute(
            'SELECT id, name, lat, lon FROM spot '
            'WHERE pointOfInterestId IS NULL;'
        )
        total = spots_iterator.rowcount
        i = 0
        print(f'Iterating spots ({total})')
        for (spot_id, name, lat, lon) in spots_iterator:
            i += 1
            print(f'({i}/{total}) {spot_id}, {name} ... ', end='')

            try:
                sds_cursor.execute(
                    'INSERT INTO points_of_interest '
                    '(name, latitude, longitude, product) '
                    'VALUES (%s, %s, %s, \'MSW\') '
                    'RETURNING id',
                    (name, lat, lon),
                )
            except UniqueViolation:
                sds_connection.rollback()
                print(f'ERROR, POI at ({lat}, {lon}) already exists')
                continue
            except Exception:
                sds_connection.rollback()
                print(f'ERROR, unknown')
                continue

            poi_id = sds_cursor.fetchall()[0][0]
            print(f'New POI ID {poi_id}... ', end='')

            msw_cursor.execute(
                'UPDATE spot SET pointOfInterestId=%s WHERE id=%s;',
                (poi_id, spot_id),
            )

            sds_connection.commit()
            msw_cursor.connection.commit()
            print('Done!')


if __name__ == '__main__':
    main()
