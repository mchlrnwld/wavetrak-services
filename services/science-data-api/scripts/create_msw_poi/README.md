# Create MSW Point of Interest

Create MSW Point of Interest using direct db access. Reads spots from the MSW
databases that don't have a pointOfInterestId set, creates a POI in the
Science Data Service databases and writes the ID back to the MSW spot table.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate create-mws-poi
$ cp .env.sample .env
```

Edit your `.env` file appropriately

```sh
$ env $(cat .env | xargs) python main.py
```
