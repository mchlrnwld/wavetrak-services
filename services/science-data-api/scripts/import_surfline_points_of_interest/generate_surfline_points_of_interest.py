import os

from pymongo import MongoClient

MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']

class PointOfInterest:
    def __init__(
        self,
        name,
        longitude,
        latitude
    ):
        self.name = name
        self.longitude = longitude
        self.latitude = latitude


def write_surfline_points_of_interest_csv(points_of_interest):
    with open(f'surfline_points_of_interest.csv', 'w') as csv:
        csv.write(
            'name,latitude,longitude,product\n'
        )
        for point in points_of_interest:
            line = ','.join(
                [
                    f'"{point.name}"',
                    str(point.latitude),
                    str(point.longitude),
                    'SL'
                ]
            )
            csv.write(f'{line}\n')


def main():
    connection_str = f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}"
    client = MongoClient(connection_str)
    spots_collection = client[MONGO_DATABASE].Spots

    surfline_spots = spots_collection.find({'status': 'PUBLISHED'})
    surfline_points_of_interest = [
        PointOfInterest(
            spot['name'],
            spot['location']['coordinates'][0],
            spot['location']['coordinates'][1]
        )
        for spot in surfline_spots
    ]
    print(f'Found {len(surfline_points_of_interest)} from the Spots Collection')
    print(f'Writing {len(surfline_points_of_interest)} points of interest to CSV')
    write_surfline_points_of_interest_csv(surfline_points_of_interest)


if __name__ == '__main__':
    main()
