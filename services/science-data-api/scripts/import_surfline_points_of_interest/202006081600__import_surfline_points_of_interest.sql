BEGIN;

DELETE FROM reported_surf_heights
USING points_of_interest
WHERE reported_surf_heights.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM sst
USING points_of_interest
WHERE sst.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM surf
USING points_of_interest
WHERE surf.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM swell_ensemble_members
USING points_of_interest
WHERE swell_ensemble_members.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM swell_probabilities
USING points_of_interest
WHERE swell_probabilities.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM swells_ensemble
USING points_of_interest
WHERE swells_ensemble.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM swells
USING points_of_interest
WHERE swells.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM weather
USING points_of_interest
WHERE weather.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM wind
USING points_of_interest
WHERE wind.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM points_of_interest_default_forecast_models
USING points_of_interest
WHERE points_of_interest_default_forecast_models.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM surf_spot_grid_point_configurations
USING points_of_interest
WHERE surf_spot_grid_point_configurations.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

DELETE FROM points_of_interest_grid_points
USING points_of_interest
WHERE points_of_interest_grid_points.point_of_interest_id = points_of_interest.id
AND points_of_interest.product = 'SL';

-- Disable foreign key triggers for performance
SET session_replication_role = replica;
DELETE FROM points_of_interest
WHERE product = 'SL';
SET session_replication_role = DEFAULT;

\copy points_of_interest (name, latitude, longitude, product) FROM 'surfline_points_of_interest.csv' WITH CSV HEADER;

SELECT COUNT(*) FROM points_of_interest WHERE product ='SL';

COMMIT;
