# Generate Surfline Points of Interest

Generate Surfline Point of Interest using the Spots Collection from mongodb. Only `PUBLISHED` Spots are used.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate generate-surfline-points-of-interest
$ cp .env.sample .env
```

## Running

### Generate Surfline Points of Interest CSV

1. Edit your `.env` file with the mongodb connection information for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python generate_surfline_points_of_interest.py
    ```

### Import Surfline Points of Interest CSV to Postgres

The following command deletes all forecast records, configurations, grid points, and points of interest for Surfline before importing new points of interset from CSV:
```sh
psql -U <user-name> -d <db-name> -a -f 202006081600__import_surfline_points_of_interest.sql
```

**NOTE:** `ROLLBACK` is the default action in the `202006081600__import_surfline_points_of_interest.sql` script. This allows you to safely test the changes without committing them. To commit the changes, replace the following line:
```psql
ROLLBACK;
```
With:
```psql
COMMIT;
```
