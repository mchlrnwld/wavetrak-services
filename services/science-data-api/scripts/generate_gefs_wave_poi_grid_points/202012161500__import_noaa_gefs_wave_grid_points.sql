BEGIN;

INSERT INTO points_of_interest_grid_points (point_of_interest_id, agency, model, grid, longitude, latitude)
SELECT point_of_interest_id, 'NOAA', 'GEFS-Wave', 'global.0p25', longitude, latitude
FROM points_of_interest_grid_points
WHERE agency = 'NOAA'
AND model = 'WW3-Ensemble';

SELECT COUNT(*) FROM points_of_interest_grid_points WHERE agency = 'NOAA' and model = 'GEFS-Wave';

ROLLBACK;
