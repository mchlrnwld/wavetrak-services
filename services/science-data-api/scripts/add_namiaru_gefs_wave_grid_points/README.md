# GEFS-Wave grid points for Namiaru points of interest

Python script for adding GEFS-Wave grid points to the Namiaru points of interest.

## Setup

```sh
$ conda env create -f environment.yml
$ conda activate add_namiaru_gefs_wave_grid_points
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the mongodb connection information for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python main.py
    ```
