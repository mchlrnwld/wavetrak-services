import os
import logging
from csv import DictReader
from timeit import default_timer
from typing import Any, Dict, List
from pymongo import MongoClient
import psycopg2
from bson import ObjectId

NAMIARU_SPOT_CSV = os.environ['NAMIARU_SPOT_CSV']
MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']
PSQL_URI = os.environ['PSQL_URI']

logger = logging.getLogger('add_namiaru_gefs_wave_grid_points')


class Spot:
    def __init__(self, data: Dict):
        self.spot_id = data['_id']
        self.point_of_interest_id = data['pointOfInterestId']


class PointOfInterest:
    def __init__(self, data: tuple):
        self.id = data[0]
        self.point = data[1]


def get_spot_ids_from_csv() -> List[str]:
    with open(NAMIARU_SPOT_CSV, mode='r', newline='') as data_file:
        reader = DictReader(data_file)
        full_list = [row['spot_id'] for row in reader]
        return list(set(full_list))


def get_spots_from_mongo(spot_ids: List[str],) -> List[Spot]:
    connection_str = f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}"
    client = MongoClient(connection_str)
    spots_collection = client[MONGO_DATABASE].Spots
    return [
        Spot(record)
        for record in spots_collection.find(
            {'_id': {'$in': [ObjectId(s) for s in spot_ids]}},
            {'_id': 1, 'pointOfInterestId': 1},
        )
    ]


def get_points_of_interest(spots: List[Spot]) -> List[PointOfInterest]:
    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT
                    id,
                    ST_ASEWKT(ST_MAKEPOINT(longitude, latitude)::geography) AS point
                FROM points_of_interest
                WHERE id in %s
                """,
                [tuple(x.point_of_interest_id for x in spots)],
            )
            return [PointOfInterest(result) for result in cur.fetchall()]


def create_point_of_interest_grid_points(
    points_of_interest: List[PointOfInterest], commit: bool = False
) -> None:
    with psycopg2.connect(PSQL_URI) as conn:
        with conn.cursor() as cur:
            for poi in points_of_interest:
                cur.execute(
                    """
                    INSERT INTO points_of_interest_grid_points (
                        point_of_interest_id,
                        agency,
                        model,
                        grid,
                        longitude,
                        latitude
                    )
                    (
                        SELECT
                            %s,
                            'NOAA',
                            'GEFS-Wave',
                            'global.0p25',
                            mgp.longitude,
                            mgp.latitude
                        FROM model_grid_points mgp
                        WHERE mgp.agency = 'NOAA'
                            AND mgp.model = 'GEFS-Wave'
                            AND mgp.grid = 'global.0p25'
                            AND ST_DWITHIN(
                                mgp.geography_point,
                                %s::geography,
                                100000
                            )
                        ORDER BY geography_point <-> %s::geography
                        LIMIT 1
                    )
                    ON CONFLICT
                    ON CONSTRAINT points_of_interest_grid_points_pkey
                    DO UPDATE SET
                        longitude = EXCLUDED.longitude,
                        latitude = EXCLUDED.latitude
                    """,
                    [poi.id, poi.point, poi.point],
                )

        if not commit:
            conn.rollback()


def main():

    logger.info(f'Reading spots from CSV')
    start = default_timer()
    spot_ids = get_spot_ids_from_csv()
    elapsed = default_timer() - start
    logger.info(
        f'Finished reading {len(spot_ids)} spots from CSV in {elapsed}s.'
    )

    logger.info(f'Getting spot data from MongoDB')
    start = default_timer()
    spots = get_spots_from_mongo(spot_ids)
    elapsed = default_timer() - start
    logger.info(
        f'Finished getting {len(spots)} spot records from mongo in {elapsed}s.'
    )

    logger.info(f'Getting POI data from postgres')
    start = default_timer()
    points_of_interest = get_points_of_interest(spots)
    elapsed = default_timer() - start
    logger.info(
        f'Finished getting {len(points_of_interest)} POI records from postgres in {elapsed}s.'
    )

    spot_poi_ids = [spot.point_of_interest_id for spot in spots]
    found_poi_ids = [poi.id for poi in points_of_interest]
    not_found_poi_ids = list(
        filter(lambda x: x not in found_poi_ids, spot_poi_ids)
    )
    logger.info(
        f'There were {len(not_found_poi_ids)} point of interest IDs that were not found:'
    )
    logger.info(not_found_poi_ids)

    logger.info(f'Creating POI grid points')
    start = default_timer()
    commit = os.environ['COMMIT'].lower() == 'true'
    create_point_of_interest_grid_points(points_of_interest, commit)
    elapsed = default_timer() - start
    logger.info(f'Finished creating POI grid points in {elapsed}s.')


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main()
