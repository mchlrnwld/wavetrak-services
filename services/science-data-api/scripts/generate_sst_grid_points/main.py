"""
Pick the sst points
"""
import os
from contextlib import ExitStack

from netCDF4 import Dataset
import numpy.ma as ma
import psycopg2

SAMPLE_SST_NETCDF_FILE = os.environ['SAMPLE_SST_NETCDF_FILE']
SCIENCE_PLATFORM_DB_HOST = os.environ['SCIENCE_PLATFORM_DB_HOST']
SCIENCE_PLATFORM_DB_DATABASE = os.environ['SCIENCE_PLATFORM_DB_DATABASE']
SCIENCE_PLATFORM_DB_USER = os.environ['SCIENCE_PLATFORM_DB_USER']
SCIENCE_PLATFORM_DB_PASSWORD = os.environ['SCIENCE_PLATFORM_DB_PASSWORD']


class NoSSTPointsError(ValueError):
    pass


def get_index(lat, lon):
    """
    Get the nearest grid point indexes for the given lat lon for the
    NASA MUR SST 0p01 grid
    """
    while lon <= -180:
        lon += 360
    while lon > 180:
        lon -= 360
    return (round(lat * 100) + 8999, round(lon * 100) + 17999)

def distance(lat1, lon1, lat2, lon2):
    """
    A lat/lon distance metric that is wrong, but good enough to pick a nearby point
    from a set of surrounding points
    """
    lat_diff = abs(lat2 - lat1)
    lon_diff = abs(lon2 - lon1) % 360
    lon_diff = min(lon_diff, 360 - lon_diff)
    return pow(lon_diff * lon_diff + lat_diff * lat_diff, 0.5)


def get_sst_point(data, lat_values, lon_values, spot_lat, spot_lon):
    """
    Find the nearest valic sst point. Only consider points up to five
    grid steps away.
    """
    lat_index, lon_index = get_index(spot_lat, spot_lon)
    points = [
        (lat_index + j, lon_index + i)
        for i in range(-5, 6)
        for j in range(-5, 6)
        if not ma.is_masked(data[0, lat_index + j, lon_index + i])
    ]
    if not points:
        raise NoSSTPointsError()
    return min(points, key=lambda pt: distance(spot_lat, spot_lon, lat_values[pt[0]], lon_values[pt[1]]))


def main():
    """
    Do the things.
    """
    with ExitStack() as stack:
        rootgrp = stack.enter_context(Dataset(
            SAMPLE_SST_NETCDF_FILE,
            'r',
            format='NETCDF4'
        ))

        sds_connection = stack.enter_context(
            psycopg2.connect(
                dbname=SCIENCE_PLATFORM_DB_DATABASE,
                user=SCIENCE_PLATFORM_DB_USER,
                password=SCIENCE_PLATFORM_DB_PASSWORD,
                host=SCIENCE_PLATFORM_DB_HOST,
            )
        )

        sds_cursor = stack.enter_context(sds_connection.cursor())
        sds_insert = stack.enter_context(sds_connection.cursor())

        sds_cursor.execute(
            'SELECT id, latitude, longitude FROM points_of_interest '
            'WHERE id NOT IN ('
            '   SELECT point_of_interest_id FROM points_of_interest_grid_points '
            '   WHERE agency=\'NASA\' AND model=\'MUR-SST\' AND grid=\'0p01\''
            ');'
        )

        lat_values = rootgrp['lat']
        lon_values = rootgrp['lon']
        data = rootgrp['analysed_sst']

        for (id, spot_lat, spot_lon) in sds_cursor:
            print(id, spot_lat, spot_lon, end='\t')
            try:
                best_pt = get_sst_point(data, lat_values, lon_values, spot_lat, spot_lon)
            except NoSSTPointsError:
                print('. ERROR no nearby SST point.')
                continue

            print(best_pt, end='\t')
            print(lat_values[best_pt[0]], lon_values[best_pt[1]], end='\t')

            try:
                sst_lat = round(float(lat_values[best_pt[0]]), 2)
                sst_lon = round(float(lon_values[best_pt[1]]), 2)
                if sst_lon < 0:
                    sst_lon = sst_lon + 360
                print(sst_lat, sst_lon)
                sds_insert.execute(
                    f'INSERT INTO points_of_interest_grid_points '
                    f'   (point_of_interest_id, agency, model, grid, latitude, longitude) '
                    f'VALUES (\'{id}\', \'NASA\', \'MUR-SST\', \'0p01\', {sst_lat}, {sst_lon});'
                )
            except Exception as e:
                sds_connection.rollback()
                print(f'ERROR', e)
                continue

            sds_connection.commit()


if __name__ == '__main__':
    main()
