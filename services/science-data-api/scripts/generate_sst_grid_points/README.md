# Generate SST grid points

Generates NASA MUR-SST grid points for any POI lacking one.

Reads POI data directly from the SDS database, and writes grid point data
directly back to the SDS database.

## Setup and running

```sh
$ conda env create -f environment.yml
$ source activate generate_sst_grid_points
$ cp .env.sample .env
```

Edit your `.env` file appropriately, obtain a sample SST datafile (look in
`s3://surfline-science-s3-dev/nasa/mur-sst/`), and run:

```sh
$ env $(xargs <.env) python main.py
```
