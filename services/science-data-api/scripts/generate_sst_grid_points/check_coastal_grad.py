"""
Scan lines of latutide at rougly 5 deg intervals looking for coastline
and cheking the deltas from the edge grid point to the next point into
the ocean, plus 10, and 25 grid points in.

Prints these deltas and highlights any where this is >0.5 degress K.
"""
import os

from netCDF4 import Dataset
from numpy.ma import is_masked


SAMPLE_SST_NETCDF_FILE = os.environ['SAMPLE_SST_NETCDF_FILE']

def main():
    """
    Do the things
    """
    with Dataset(
            SAMPLE_SST_NETCDF_FILE,
            'r',
            format='NETCDF4'
    ) as rootgrp:
        lat_values = rootgrp['lat']
        lon_values = rootgrp['lon']
        data = rootgrp['analysed_sst']

        print('Δ1, Δ10, Δ25')

        for lat_index in range(1999, 15999, 500):
            for lon_index in range(0, lon_values.shape[0]-26):
                if (is_masked(data[0, lat_index, lon_index])
                    and not is_masked(data[0, lat_index+1, lon_index])
                    and not is_masked(data[0, lat_index+2, lon_index])
                    and not is_masked(data[0, lat_index+11, lon_index])
                    and not is_masked(data[0, lat_index+26, lon_index])
                    ):
                    d1 = round(abs(data[0, lat_index+2, lon_index] - data[0, lat_index+1, lon_index]), 4)
                    d10 = round(abs(data[0, lat_index+11, lon_index] - data[0, lat_index+1, lon_index]), 4)
                    d25 = round(abs(data[0, lat_index+26, lon_index] - data[0, lat_index+1, lon_index]), 4)
                    big = d1 > 0.5 or d10 > 0.5 or d25 > 0.5
                    print(d1, ', ',d10, ', ', d25, f'  **** {lat_values[lat_index]}, {lon_values[lon_index]}' if big else '')


if __name__ == '__main__':
    main()
