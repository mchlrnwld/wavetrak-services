# LOLA Surf Spot Grid Points

Script to generate LOLA surf spot grid points.

## Quickstart

Generates `data/spots.csv` and `data/grid_points.csv` files.

```sh
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```
