import asyncio
from functools import partial
import json
import logging
import os
from os import path
from uuid import uuid4
import sys

from pymongo import MongoClient  # type: ignore
import psycopg2  # type: ignore

MONGO_STRING = os.environ['MONGO_STRING']
PSQL_STRING = os.environ['PSQL_STRING']

logger = logging.getLogger('lola-surf-spot-grid-points')


def get_lola_model_name(lat, lon):
    """For a given latitude and longitude, returns the lola model
    abbreviation which defines the lola model used for output
    at the provided latitude and longitude

    Args:
        Latitude
        Longitude

    Returns:
        A lola model abbreviation i.e. glz, wnz, enz, etc.
    """
    # LOLA MODEL SECTION BASED ON SPOT MAP LAT/LON ###

    model = 'glz'

    if lat >= 5 and lat < 60 and lon >= -170 and lon < -98.5:
        model = 'enz'

    if lat >= 0 and lat < 50 and lon >= -74.5 and lon < -30:
        model = 'wnz'

    if lat >= 15 and lat < 50 and lon >= -90 and lon < -30:
        model = 'wnz'

    if lon >= -98.5 and lon < -90.3:
        if lat >= 5 and lat < 16.8:
            model = 'enz'
        if lat >= 16.8 and lat < 35:
            model = 'wnz'

    if lon >= -90.3 and lon < -84.8:
        if lat >= 5 and lat < 14.2:
            model = 'enz'
        if lat >= 14.2 and lat < 35:
            model = 'wnz'

    if lon >= -84.8 and lon < -83.5:
        if lat >= 5 and lat < 10.16:
            model = 'enz'
        if lat >= 10.16 and lat < 35:
            model = 'wnz'

    if lon >= -83.5 and lon < -80:
        if lat >= 5 and lat < 8.7:
            model = 'enz'
        if lat >= 8.7 and lat < 35:
            model = 'wnz'

    if lon >= -83.5 and lon < -80:
        if lat >= 5 and lat < 8.7:
            model = 'enz'
        if lat >= 8.7 and lat < 35:
            model = 'wnz'

    if lon >= -80 and lon < -78:
        if lat >= 5 and lat < 9.1:
            model = 'enz'
        if lat >= 9.1 and lat < 35:
            model = 'wnz'

    if lon >= -78 and lon < -75:
        if lat >= 5 and lat < 7.9:
            model = 'enz'
        if lat >= 7.9 and lat < 35:
            model = 'wnz'

    if lon >= -5.5 and lon <= 37 and lat >= 30 and lat <= 43:
        model = 'mdz'

    if lon >= 0 and lon <= 21 and lat >= 40 and lat <= 47:
        model = 'mdz'

    if lon >= -92.5 and lon < -75 and lat >= 41 and lat < 50:
        model = 'gkz'

    if lon >= -15 and lon < 13 and lat >= 48 and lat < 61:
        model = 'nrz'

    if lon >= 9.9 and lon < 31.2 and lat >= 53.1 and lat < 62.9:
        model = 'btz'

    if lon >= 15 and lon < 27.5 and lat >= 59.9 and lat < 66.2:
        model = 'btz'

    if lon >= 31 and lon < 65.2 and lat >= -6 and lat < 31.2:
        model = 'rez'

    if lon >= 26.7 and lon < 42.2 and lat >= 40 and lat < 48:
        model = 'bkz'

    if lon > -121 and lon < -116.95 and lat > 32 and lat < 35.05:
        model = 'scz'

    return model


class Spot:
    def __init__(self, id, name, latitude, longitude):
        self.id = id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.grid = get_lola_model_name(latitude, longitude)


def spot_from_json(spot_json):
    return Spot(
        str(uuid4()),
        spot_json['name'],
        spot_json['forecastLocation']['coordinates'][1],
        spot_json['forecastLocation']['coordinates'][0],
    )


def get_spots():
    with MongoClient(MONGO_STRING) as client:
        spots = [
            spot_from_json(spot)
            for spot in client['KBYG']['Spots'].find(
                {'status': 'PUBLISHED'}, {'name': 1, 'forecastLocation': 1}
            )
        ]

    keyed_spots = {f'{spot.latitude},{spot.longitude}': spot for spot in spots}
    return list(keyed_spots.values())


async def find_existing_spot(conn, spot):
    loop = asyncio.get_event_loop()
    with conn.cursor() as cur:
        await loop.run_in_executor(
            None,
            partial(
                cur.execute,
                '''
SELECT id
FROM points_of_interest
WHERE latitude = %s
AND longitude = %s
                ''',
                (spot.latitude, spot.longitude),
            ),
        )
        result = await loop.run_in_executor(None, cur.fetchone)
        return (
            None
            if result is None
            else Spot(result[0], spot.name, spot.latitude, spot.longitude)
        )


class GridPoint:
    def __init__(self, id, grid, latitude, longitude):
        self.id = id
        self.grid = grid
        self.latitude = latitude
        self.longitude = longitude


async def get_model_grid_point(conn, spot):
    loop = asyncio.get_event_loop()
    with conn.cursor() as cur:
        await loop.run_in_executor(
            None,
            partial(
                cur.execute,
                '''
SELECT latitude, longitude
FROM model_grid_points
WHERE agency = 'Wavetrak'
AND model = 'LOLA-WW3'
AND grid = %s
AND ST_DWithin(ST_Point(%s, %s), geography_point, 100000)
ORDER BY ST_Distance(ST_Point(%s, %s), geography_point)
LIMIT 1;
                ''',
                (
                    spot.grid,
                    spot.longitude,
                    spot.latitude,
                    spot.longitude,
                    spot.latitude,
                ),
            ),
        )
        result = await loop.run_in_executor(None, cur.fetchone)
        return GridPoint(spot.id, spot.grid, result[0], result[1])


async def write_points_of_interest_csv(spots):
    with open(path.join('data', 'points_of_interest.csv'), 'w') as csv:
        csv.write('id,name,latitude,longitude\n')
        for spot in spots:
            line = ','.join(
                [
                    str(spot.id),
                    f'"{spot.name}"',
                    str(spot.latitude),
                    str(spot.longitude),
                ]
            )
            csv.write(f'{line}\n')


async def write_grid_points_csv(grid_points):
    with open(path.join('data', 'grid_points.csv'), 'w') as csv:
        csv.write('id,agency,model,grid,grid_latitude,grid_longitude\n')
        for grid_point in grid_points:
            line = ','.join(
                [
                    str(grid_point.id),
                    'Wavetrak',
                    'LOLA-WW3',
                    str(grid_point.grid),
                    str(grid_point.latitude),
                    str(grid_point.longitude),
                ]
            )
            csv.write(f'{line}\n')


async def write_surf_spot_configurations_csv(grid_points):
    with open(path.join('data', 'surf_spot_configurations.csv'), 'w') as csv:
        csv.write(
            'id,agency,model,grid,grid_latitude,grid_longitude,breaking_wave_height_algorithm\n'  # noqa
        )
        for grid_point in grid_points:
            line = ','.join(
                [
                    str(grid_point.id),
                    'Wavetrak',
                    'LOLA-WW3',
                    str(grid_point.grid),
                    str(grid_point.latitude),
                    str(grid_point.longitude),
                    'SPECTRAL_REFRACTION',
                ]
            )
            csv.write(f'{line}\n')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler(sys.stderr))

    logger.info('Getting spots from Mongo...')
    new_spots = get_spots()
    logger.info(f'Found {len(new_spots)} from Mongo.')

    with psycopg2.connect(PSQL_STRING) as conn:
        logger.info('Checking for spots that already exist...')
        existing_spots = await asyncio.gather(
            *[find_existing_spot(conn, spot) for spot in new_spots]
        )
        existing_spots_count = len(
            [spot for spot in existing_spots if spot is not None]
        )
        logger.info(f'Found {existing_spots_count} spots already exist.')

        merged_spots = [
            (
                new_spot if existing_spot is None else existing_spot,
                existing_spot is None,
            )
            for new_spot, existing_spot in zip(new_spots, existing_spots)
        ]

        logger.info('Looking up nearest grid points from PSQL...')
        grid_points = await asyncio.gather(
            *[get_model_grid_point(conn, spot) for spot, _ in merged_spots]
        )

    logger.info('Writing results to CSV...')
    await asyncio.gather(
        write_points_of_interest_csv(
            [spot for spot, new in merged_spots if new]
        ),
        write_grid_points_csv(grid_points),
        write_surf_spot_configurations_csv(grid_points),
    )


if __name__ == '__main__':
    asyncio.run(main())
