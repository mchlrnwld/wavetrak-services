# Surfline Editorial Web

## Table of Contents <!-- omit in toc -->

- [Getting Started](#getting-started)
- [WordPress Development](#wordpress-development)
  - [Working on Themes](#working-on-themes)
    - [Editing Theme CSS](#editing-theme-css)
  - [Installing Plugins](#installing-plugins)
  - [Updating Plugins](#updating-plugins)
  - [Developing inside WordPress Admin](#developing-inside-wordpress-admin)
  - [Additional Commands](#additional-commands)
  - [Surfline header and footer](#surfline-header-and-footer)
  - [File Structure](#file-structure)
- [WordPress API](#wordpress-api)
  - [Documentation](#documentation)
  - [Base Path](#base-path)
  - [Example Requests](#example-requests)
    - [Get all posts](#get-all-posts)
    - [Get all categories](#get-all-categories)
    - [Get all posts matching a category](#get-all-posts-matching-a-category)
    - [Get a specific post](#get-a-specific-post)
- [CDN Assets](#cdn-assets)
- [Deployment](#deployment)
  - [Verification](#verification)
- [Monitoring](#monitoring)
  - [New Relic](#new-relic)
    - [Enabling New Relic PHP Agent](#enabling-new-relic-php-agent)
    - [Enabling New Relic Browser](#enabling-new-relic-browser)
- [Roadmap](#roadmap)

## Getting Started

This is the bare minimum to get started developing WordPress within this repository. All commands listed should be run from the root of this repository.

1. Install [Docker](https://www.docker.com/products/overview)

1. Build application

    ```bash
    docker-compose -f docker/docker-compose.yml build
    ```

1. Start application

    ```bash
    docker-compose -f docker/docker-compose.yml up
    ```

1. Browse to WordPress application

    This assumes Docker is using `localhost` and not binding to its own network. Older versions of Docker require you to run `docker-machine ip` to determine your local ip address. If that's the case, use that address in place of `localhost`.

    ```bash
    http://localhost:8080/
    ```

    If you load the docker configuration into a docker-machine, like `192.168.99.100`, you'll have to navigate to `http://192.168.99.100:8080/wp-admin/options-general.php` and change `Site Address` and `WordPress Address` to your hosted address.

1. Browse to WordPress admin application

    ```bash
    http://localhost:8080/wp-admin/
    ```

1. Stop application

    ```bash
    docker-compose -f docker/docker-compose.yml down
    ```

## WordPress Development

### Working on Themes

Run WordPress in a docker container, edit theme files locally, and the changes should be reflected in the running WP application immediately.

1. Start Docker using directions in "Getting Started"
1. Edit theme files in `wordpress/content/themes/surfline`
1. Reload in browser to view changes

#### Editing Theme CSS

Styles are maintained in `wordpress/content/themes/surfline/css` in .css files. Sass pattern was deprecated to avoid confusion and restrict styles to one source of truth.

### Installing Plugins

Rather than installing plugins with the WP admin UI, plugins should be installed using the WordPress CLI and stored within the Git repo.

1. Connect to running WordPress container

    ```bash
    docker exec -it docker_wordpress_1 /bin/bash
    ```

1. Install plugin using WP CLI

    ```bash
    wp --allow-root plugin install <plugin-name>
    ```

1. Exit WordPress container

1. Change to the plugins directory using `cd wordpress/content/plugins`

1. Rebundle the plugins with `./bundle_plugins.sh`

1. Commit `plugins.tgz` and `plugins-manifest.txt` to git

### Updating Plugins

1. Check to see if any plugins are out-of-date.

    ```bash
    make plugincheck
    +----------------------------------+--------+-----------+---------+
    | name                             | status | update    | version |
    +----------------------------------+--------+-----------+---------+
    | advanced-custom-fields-pro       | active | available | 5.5.5   |
    | amazon-web-services              | active | none      | 1.0.1   |
    | mailchimp-for-wp                 | active | none      | 4.0.12  |
    | simple-taxonomy-wysiwyg          | active | none      | 1.3.1   |
    | amazon-s3-and-cloudfront-pro     | active | none      | 1.3.1   |
    | wp-rocket                        | active | none      | 2.9.2   |
    | yet-another-related-posts-plugin | active | none      | 4.3.6   |
    | wordpress-seo                    | active | none      | 4.1     |
    +----------------------------------+--------+-----------+---------+
    ```

1. Connect to running WordPress container

    ```bash
    docker exec -it docker_wordpress_1 /bin/bash
    ```

1. Update plugin using WP CLI

    ```bash
    wp --allow-root plugin update <plugin-name>
    ```

1. Exit WordPress container

1. Change to the plugins directory using `cd wordpress/content/plugins`

1. Rebundle the plugins with `./bundle_plugins.sh`

1. Commit `plugins.tgz` and `plugins-manifest.txt` to git

### Developing inside WordPress Admin

User information is in MySQL's user table. Credentials are in `docker-compose.yml` under `MYSQL_USER`. Use db imports from `mysql/tools/*sh` to pull a `mysql/exports/*sql` dump.

### Additional Commands

The following convenience commands are available. View the contents of `Makefile` to see how these commands translate to docker commands.

```bash
# build containers, start dev stack
make dev

# stop and start dev stack
make dev-start
make dev-stop
make dev-restart

# run bash in the running wordpress container
make wpshell

# run mysql shell in the running mysql container
make mysqlshell

# list installed wp plugins and whether updates are available
make plugincheck

# load `mysql/exports/latest.sql` into running mysql container
# and replace home and siteurl with value of `SITEURL` variable
# ex. `make bootstrap SITEURL=localhost:8080`
make bootstrap SITEURL=<local_hostname:port>

# dump db to `mysql/exports/latest.sql` and replace home and
# siteurl with value of `SITEURL` variable
# ex. `make dumpdbs SITEURL=localhost:8080`
make dumpdbs SITEURL=<local_hostname:port>
```

### Surfline header and footer

These quiver react components are managed via [web/backplane](https://github.com/Surfline/surfline-web/blob/master/backplane/README.md).

### File Structure

```bash
├── docker
├── mysql
│   ├── exports
│   └── tools
├── common
│   └── page-container
└── wordpress
    ├── config
    └── content
        ├── plugins
        │   ├── advanced-custom-fields-pro
        │   ├── amazon-s3-and-cloudfront-pro
        │   ├── amazon-web-services
        │   ├── mailchimp-for-wp
        │   ├── simple-taxonomy-wysiwyg
        │   ├── wordpress-seo
        │   ├── wp-rocket
        │   └── yet-another-related-posts-plugin
        ├── themes
        │   ├── surfline
        │   └── twentyseventeen
        └── uploads
```

## WordPress API

### Documentation

<https://developer.wordpress.org/rest-api/>

### Base Path

```bash
/wp-json/wp/v2
```

### Example Requests

#### Get all posts

```bash
curl "http://localhost:8080/wp-json/wp/v2/posts"
```

#### Get all categories

```bash
curl "http://localhost:8080/wp-json/wp/v2/categories"
```

#### Get all posts matching a category

```bash
curl "http://localhost:8080/wp-json/wp/v2/posts?categories=1"
```

#### Get a specific post

```bash
curl "http://localhost:8080/wp-json/wp/v2/posts/1"
```

## Lambda Functions

### Overview

The Surfline Web Editorial uses AWS Lambda functions to populate Feed data in Elasticsearch and to clear Varnish caches. These Lambda functions are triggered by publishing to the `sl-editorial-cms-article-update-sns_staging` SNS topic.

## CDN Assets

[BWP Minify](http://betterwp.net/wordpress-plugins/bwp-minify/) detects JS/CSS to bundle and hosts the bundles from its plugin cache directory. It only works with scripts/styles added with WP's `enqueue` functions.

The cache directory's `.htaccess` files includes the friendly URL modifications. The plugin options still need to be configured through WP-admin.

Config options:

- Advanced Options > Enable friendly minify urls
- Advanced Options > Enable CDN Support
- Advanced Options > CDN hostname
- Manage enqueued JS/CSS files. You'll have to navigate through page after enabling the plugin to recognize files to bundle. Styles and JS should be included in the header.

## Deployment

**Sandbox** tier is intended to be a continuous integration environment, **Staging** a test environment and **Production** is the public-facing website.

All of this environments are configured to be deployed from the [Web Editorial Jenkins Job](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-editorial-to-ecs/). You'll need to select which of the services needs to be deployed, in which environment and which will be the tag or branch involved.

### Verification

Of course, the Jenkins Job should run successfully, but aside of the previous tests the applications may be execute before building a new Docker image, we have a few endpoints to verify if everything still works as planned.

All of them correspond to `prod` but it's not difficult to find an article or a video streaming for `sandbox` and `staging` as well:

```bash
curl -X GET -i \
  https://www.surfline.com/surf-news/true-surf-five-tips-surfings-best-ever-video-game/48810/amp
```

```bash
curl -X GET -i \
  https://www.surfline.com/category
```

```bash
curl -X GET -i \
  https://www.surfline.com/tag
```

```bash
curl -X GET -i \
  https://www.surfline.com/series
```

```bash
curl -X GET -i \
  https://internal-sl-int-core-srvs-3-prod-1117391437.us-west-1.elb.amazonaws.com/wp-json/sl/v1/posts?id=2
```

```bash
curl -X GET -i \
  https://www.surfline.com/terms-of-use
```

```bash
curl -X GET -i \
  https://www.surfline.com/sitemaps/news.xml
```

```bash
curl -X GET -i \
  http://www.surfline.com/video
```

## Monitoring

### New Relic

New Relic PHP Agent and Browser are installed in WordPress images but are by
default turned off.

#### Enabling New Relic PHP Agent

Set the `NEW_RELIC_ENABLED` environment variable to `true`

#### Enabling New Relic Browser

- Activate plugin: `/wp-admin/plugins.php?action=activate&plugin=rt-newrelic-browser%2FNewRelicBrowser.php&plugin_status=all&paged=1&s&_wpnonce=9ce5c5f90a`
- Configure plugin: `/wp-admin/options-general.php?page=new-relic-browser`

## Roadmap

- Add Varnish container and config to stack.
- Create Jenkins build jobs
- Add deployment instructions
- Add production Docker config
- Manage WP config migrations between environments
- Integrate Quiver navigation with WordPress theme
- Integrate WP Rocket/Varnish cache clearing mechanisms
