#!/usr/bin/env bash

# [working directory]/[nanoseconds from epoch] as project name for reasonably unique container names

# Clean up containers and volumes
function cleanup {
	docker-compose \
		-f docker-compose.test.yml \
		-f docker-compose.integration.yml \
		rm \
		-v \
		--stop \
		--force
}

trap cleanup EXIT

set -xe

# Build images, start containers, and run tests
docker-compose \
	-f docker-compose.test.yml \
	-f docker-compose.integration.yml \
	up \
	--build \
	--force-recreate \
	--renew-anon-volumes \
	--exit-code-from integration-tests
