variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_name" {
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "alb_listener_arn_https" {
}

variable "alb_listener_arn_http" {
}

variable "load_balancer_arn" {
}

variable "default_vpc" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_request_count"
}

variable "auto_scaling_min_size" {
  default = ""
}

variable "auto_scaling_max_size" {
  default = ""
}

variable "auto_scaling_up_count" {
  default = ""
}

variable "auto_scaling_down_count" {
  default = ""
}

variable "auto_scaling_target_value" {
  default = ""
}

variable "auto_scaling_alb_arn_suffix" {
  default = ""
}

# New Relic alert variables

variable "newrelic_app_name" {
  description = "Name of the APM service being monitored in New Relic"
  default     = "Surfline Web - Editorial (sbox)"
}

variable "newrelic_policy_name" {
  description = "Name of the New Relic policy"
  default     = "Surfline Editorial Web (sbox)"
}

variable "newrelic_apdex_create_alert" {
  default = "false"
}

variable "newrelic_error_rate_create_alert" {
  default = "false"
}

variable "newrelic_create_additional_editorial_alerts" {
  default = "false"
}

variable "rds_editorial_display_name" {
  default = "tf-00ca674d1cba8fd3fee16e1324"
}

variable "newrelic_apdex_alert_threshold" {
  default = ".70"
}

variable "newrelic_apdex_warning_threshold" {
  default = ".85"
}

variable "newrelic_error_rate_alert_threshold" {
  default = ".05"
}

variable "newrelic_error_rate_warning_threshold" {
  default = ".03"
}

variable "newrelic_editorial_response_time_warning_threshold" {
  default = "0.4"
}

variable "newrelic_editorial_response_time_critical_threshold" {
  default = "0.5"
}


variable "newrelic_feed_etl_lambda_warning_threshold" {
  default = "10"
}

variable "newrelic_feed_etl_lambda_critical_threshold" {
  default = "20"
}

variable "newrelic_travel_page_lambda_critical_threshold" {
  default = "1"
}

variable "newrelic_varnish_cache_lambda_warning_threshold" {
  default = "120"
}

variable "newrelic_varnish_cache_lambda_critical_threshold" {
  default = "200"
}

variable "newrelic_end_apdex_warning_threshold" {
  default = "0.8"
}

variable "newrelic_end_apdex_critical_threshold" {
  default = "0.5"
}

variable "newrelic_rds_cpu_warning_threshold" {
  default = "50"
}

variable "newrelic_rds_cpu_critical_threshold" {
  default = "60"
}

variable "newrelic_rds_storage_warning_threshold" {
  default = "20000000000"
}

variable "newrelic_rds_storage_critical_threshold" {
  default = "10000000000"
}

variable "newrelic_rds_swap_warning_threshold" {
  default = "500000"
}

variable "newrelic_rds_swap_critical_threshold" {
  default = "1000000"
}
