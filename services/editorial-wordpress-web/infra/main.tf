data "aws_route53_zone" "dns_zone" {
  name = "${var.environment}.surfline.com."
}

# Rule for the https listener
resource "aws_alb_listener_rule" "wp_wildcard_rule" {
  listener_arn = var.alb_listener_arn_https
  priority     = "130"

  action {
    type             = "forward"
    target_group_arn = module.editorial_wp_service.target_group
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

# Rule for the http listener
resource "aws_alb_listener_rule" "wp_routing_rule" {
  listener_arn = var.alb_listener_arn_http
  priority     = "910"

  action {
    type             = "forward"
    target_group_arn = module.editorial_wp_service.target_group
  }

  condition {
    path_pattern {
      values = ["*"]
    }
  }
}

module "editorial_wp_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = data.aws_route53_zone.dns_zone.zone_id
  ecs_cluster = "sl-core-svc-${var.environment}"

  service_td_name             = "sl-cms-${var.environment}-editorial-wp"
  service_td_container_name   = "wp"
  service_td_count            = var.service_td_count
  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/wordpress/health/"
  service_alb_priority        = 420
  service_port                = 80

  alb_listener      = var.alb_listener_arn_https
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = false
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = ""
  auto_scaling_min_size       = ""
  auto_scaling_max_size       = ""
  auto_scaling_alb_arn_suffix = ""

  # New Relic alert configuration

  newrelic_app_name    = var.newrelic_app_name
  newrelic_policy_name = var.newrelic_policy_name

  newrelic_apdex_create_alert      = var.newrelic_apdex_create_alert
  newrelic_apdex_alert_threshold   = var.newrelic_apdex_alert_threshold
  newrelic_apdex_warning_threshold = var.newrelic_apdex_warning_threshold

  newrelic_error_rate_create_alert      = var.newrelic_error_rate_create_alert
  newrelic_error_rate_alert_threshold   = var.newrelic_error_rate_alert_threshold
  newrelic_error_rate_warning_threshold = var.newrelic_error_rate_warning_threshold
}
