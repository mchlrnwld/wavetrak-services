provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-editorial-wp/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "staging"
  application = "editorial-wp"

  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/a39da1ea44320d7c"
  alb_listener_arn_http  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/e10b8d509912ed71"
  iam_role_arn           = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn      = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-staging/bca6896a1b354474"

  default_vpc = "vpc-981887fd"
  dns_name    = "editorial-wp.${local.environment}.surfline.com"

  service_td_count = 2
}

module "editorial_wp_service" {
  source = "../../"

  company      = local.company
  application  = local.application
  environment  = local.environment
  service_name = local.application

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_td_count = local.service_td_count
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  alb_listener_arn_https = local.alb_listener_arn_https
  alb_listener_arn_http  = local.alb_listener_arn_http
  iam_role_arn           = local.iam_role_arn
  load_balancer_arn      = local.load_balancer_arn
}
