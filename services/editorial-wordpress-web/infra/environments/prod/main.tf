provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-editorial-wp/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "prod"
  application = "editorial-wp"

  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/7d4f5548c2239855"
  alb_listener_arn_http  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/4796722fa18acc2f"
  iam_role_arn           = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn      = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b"

  default_vpc = "vpc-116fdb74"
  dns_name    = "editorial-wp.${local.environment}.surfline.com"

  service_td_count = 6
}

module "editorial_wp_service" {
  source = "../../"

  company      = local.company
  application  = local.application
  environment  = local.environment
  service_name = local.application

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_td_count = local.service_td_count
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  alb_listener_arn_https = local.alb_listener_arn_https
  alb_listener_arn_http  = local.alb_listener_arn_http
  iam_role_arn           = local.iam_role_arn
  load_balancer_arn      = local.load_balancer_arn

  # New Relic alerts

  newrelic_app_name                           = "Surfline Web - Editorial"
  newrelic_policy_name                        = "Surfline Editorial Web"
  newrelic_apdex_create_alert                 = true
  newrelic_error_rate_create_alert            = true
  newrelic_create_additional_editorial_alerts = true
  rds_editorial_display_name                  = "tf-00b6dd8bc15e8564fc9602188f"
}
