# Additional New Relic alerts

data "newrelic_alert_policy" "policy" {
  count = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name  = var.newrelic_policy_name
}

data "newrelic_entity" "apm_service_name" {
  count  = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name   = var.newrelic_app_name
  type   = "APPLICATION"
  domain = "APM"
}

resource "newrelic_alert_condition" "editorial_response_time_web" {
  count           = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name            = "Editorial RDS ${var.environment} - High Response Time"
  enabled         = "true"
  entities        = [data.newrelic_entity.apm_service_name[0].application_id]
  policy_id       = data.newrelic_alert_policy.policy[0].id
  type            = "apm_app_metric"
  condition_scope = "application"
  metric          = "response_time_web"

  term {
    priority      = "warning"
    duration      = 5
    operator      = "above"
    threshold     = var.newrelic_editorial_response_time_warning_threshold
    time_function = "all"
  }

  term {
    priority      = "critical"
    duration      = 10
    operator      = "above"
    threshold     = var.newrelic_editorial_response_time_critical_threshold
    time_function = "all"
  }

}

# Feed ETL Lambda Function Error
resource "newrelic_nrql_alert_condition" "feed_etl_lambda_error" {
  count                        = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name                         = "Feed ETL Lambda Function Error"
  policy_id                    = data.newrelic_alert_policy.policy[0].id
  type                         = "static"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("${path.module}/resources/nrql-feed-etl-lambda-query.tmpl", { environment = var.environment })
    evaluation_offset = 20
  }

  warning {
    operator              = "above"
    threshold             = var.newrelic_feed_etl_lambda_warning_threshold
    threshold_duration    = 300
    threshold_occurrences = "all"
  }

  critical {
    operator              = "above"
    threshold             = var.newrelic_feed_etl_lambda_critical_threshold
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}

# Travel Page Lambda Function Error
resource "newrelic_nrql_alert_condition" "travel_page_lambda_error" {
  count                        = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name                         = "Travel Page ETL Lambda Function Error"
  policy_id                    = data.newrelic_alert_policy.policy[0].id
  type                         = "static"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("${path.module}/resources/nrql-travel-page-lambda-query.tmpl", { environment = var.environment })
    evaluation_offset = 20
  }

  critical {
    operator              = "above"
    threshold             = var.newrelic_travel_page_lambda_critical_threshold
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}

# Update Varnish Cache Lambda Function Error
resource "newrelic_nrql_alert_condition" "varnish_cache_lambda_error" {
  count                        = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name                         = "Update Varnish Cache Lambda Function Error"
  policy_id                    = data.newrelic_alert_policy.policy[0].id
  type                         = "static"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("${path.module}/resources/nrql-varnish-cache-lambda-query.tmpl", { environment = var.environment })
    evaluation_offset = 20
  }

  warning {
    operator              = "above"
    threshold             = var.newrelic_varnish_cache_lambda_warning_threshold
    threshold_duration    = 300
    threshold_occurrences = "all"
  }

  critical {
    operator              = "above"
    threshold             = var.newrelic_varnish_cache_lambda_critical_threshold
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}

# End User Apdex Alert
resource "newrelic_alert_condition" "end_user_apdex_alert" {
  count           = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  name            = "Editorial RDS ${var.environment} - Low End User Apdex"
  enabled         = "true"
  entities        = [data.newrelic_entity.apm_service_name[0].application_id]
  policy_id       = data.newrelic_alert_policy.policy[0].id
  type            = "browser_metric"
  condition_scope = "application"
  metric          = "end_user_apdex"

  term {
    priority      = "warning"
    duration      = 5
    operator      = "below"
    threshold     = var.newrelic_end_apdex_warning_threshold
    time_function = "all"
  }

  term {
    priority      = "critical"
    duration      = 10
    operator      = "below"
    threshold     = var.newrelic_end_apdex_critical_threshold
    time_function = "all"
  }
}

# Editorial RDS High CPU
resource "newrelic_infra_alert_condition" "editorial_rds_high_cpu_alert" {
  count     = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  policy_id = data.newrelic_alert_policy.policy[0].id

  name                 = "Editorial RDS ${var.environment} - High CPU"
  type                 = "infra_metric"
  select               = "provider.cpuUtilization.Average"
  comparison           = "above"
  where                = "displayName = '${var.rds_editorial_display_name}'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = var.newrelic_rds_cpu_warning_threshold
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = var.newrelic_rds_cpu_critical_threshold
    time_function = "all"
  }
}

# Editorial RDS Low Storage
resource "newrelic_infra_alert_condition" "editorial_rds_low_storage_alert" {
  count     = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  policy_id = data.newrelic_alert_policy.policy[0].id

  name                 = "Editorial RDS ${var.environment} - Low Storage"
  type                 = "infra_metric"
  select               = "provider.freeStorageSpaceBytes.Average"
  comparison           = "below"
  where                = "displayName = '${var.rds_editorial_display_name}'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = var.newrelic_rds_storage_warning_threshold
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = var.newrelic_rds_storage_critical_threshold
    time_function = "all"
  }
}

# Editorial RDS High Swap Usage
resource "newrelic_infra_alert_condition" "editorial_rds_high_swap_alert" {
  count     = var.newrelic_create_additional_editorial_alerts ? 1 : 0
  policy_id = data.newrelic_alert_policy.policy[0].id

  name                 = "Editorial RDS ${var.environment} - High Swap Usage"
  type                 = "infra_metric"
  select               = "provider.swapUsageBytes.Average"
  comparison           = "above"
  where                = "displayName = '${var.rds_editorial_display_name}'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = var.newrelic_rds_swap_warning_threshold
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = var.newrelic_rds_swap_critical_threshold
    time_function = "all"
  }
}
