<?php
/**
 * Template Name: Index Template
 */
get_header();
?>
<div id="main_container">
    <h1 class="front-page-headline">Top Surf Stories</h1>

    <div id="main-content" class="container">

        <div class="category-filter">
            <nav>
              <?php wp_nav_menu(array('theme_location' => 'surf_menu')); ?>
            </nav>
        </div><!--category-filter-->

        <div class="all-items">
            <div class="row row-eq-height post-listing">

              <?php
              $premium_ribbon = '<svg class="sl-premium-ribbon-icon" width="40px" height="50px" viewBox="0 0 40 50"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-71.000000, -280.000000)" fill="#22D737" fill-rule="nonzero"><polygon points="71 280 111 280 111 330 91 320.955653 71 330"></polygon></g></g></svg>';
              $args = array(
                'posts_per_page' => INFINITE_SCROLL_POSTS_PER_PAGE,
                'post_type' => 'post',
                'post_status' => 'publish',
                'suppress_filters' => true,
                "tax_query" => array(
                  array(
                    "taxonomy" => "promotion",
                    "field" => "slug",
                    "terms" => "curated"
                  )
                )
              );

              $posts_array = get_posts($args);

              // LOOP
              $counter = 1;
              foreach ($posts_array as $inx => $post) {
                $premium = get_field('premium');
                $video = get_field('show_video');
                $category = get_the_category();
                ?>

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <?php if($premium) { echo $premium_ribbon; } ?>
                      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                          <div class="item">

                              <div class="thumbnail"
                                onclick="window.open('<?php the_permalink(); ?>','<?php echo post_link_target($post); ?>');">

                                <?php
                                if (!empty($video)) {
                                  echo '<div class="video-play">
									   <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
										 </div>';
                                }
                                ?>
                                <?php
                                $title = get_the_title();
                                the_post_thumbnail('large', array('title' => $title, 'alt' => $title));
                                ?>
                              </div>

                              <div class="meta">

                                <?php
                                foreach ($category as $c) {
                                  $cat_name = $c->cat_name;
                                  $cat_slug = $c->slug;

                                  echo '<a href="../category/' . $cat_slug . '">' . $cat_name . '</a>&nbsp;&nbsp;';
                                  break;
                                }
                                ?>

                                  <span>
                                    <?php 
                                    $linkAttribution = post_link_attribution($post);
                                    if ($linkAttribution) {
                                      echo '<img 
                                        src="'.get_template_directory_uri().'/img/clock.svg"
                                        class="clock"
                                      />';
                                      echo get_the_date('Y-m-d').' | ';
                                      echo $linkAttribution;
                                    } else { 
                                      echo '<img 
                                        src="'.get_template_directory_uri().'/img/clock.svg"
                                        class="clock"
                                      />';
                                      echo get_the_date('Y-m-d');
                                      $before = " | <strong>Updated</strong>&nbsp;";
                                      if (get_the_modified_time('U') != get_the_time('U')) {
                                        echo $before;
                                        echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                                      }
                                    } ?>
                                  </span>

                              </div>
                              <a 
                                href="<?php echo get_permalink($post->ID); ?>"
                                class="overlay-link"
                                target="<?php echo post_link_target($post); ?>"
                              >
                                <?php
                                $title = get_the_title();
                                $title = mb_strimwidth($title, 0, 50, '...');
                                ?>
                                  <div class="headline"><h4><?php echo $title; ?></h4></div>
                              </a>

                          </div>

                      </div>
                  </div>
                <?php
                if ($counter === 4 || $counter % 12 === 0) {
                  ?>
                    <!-- /1024858/Box -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div id='div-gpt-ad-page-unit-2-<?php echo $counter; ?>' class="ad-unit-inline-mpu">
                            <script>
                              googletag.cmd.push(function () {
                                var slotId = "div-gpt-ad-page-unit-2-<?php echo $counter; ?>";

                                googletag.defineSlot('/1024858/Box', [300, 250], slotId)
                                  .addService(googletag.pubads());

                                googletag.display(slotId);
                              });
                            </script>
                        </div>
                    </div>
                  <?php
                }
                ?>
                <?php
                if ($counter == 5) {
                  ?>

                  <?php
                  //  START Mega Featured

                  $args = array(
                    "posts_per_page" => 2,
                    "post_type" => "post",
                    'meta_query' => array(
                      'relation' => 'AND',
                      array(
                        'key' => 'large_img'
                      ),
                      array(
                        'key' => 'medium_img'
                      ),
                      array(
                        'key' => 'small_img'
                      ),
                      array(
                        'key' => 'hide_mega_feature_text'
                      ),
                    ),
                    "tax_query" => array(
                      array(
                        "taxonomy" => "promotion",
                        "field" => "slug",
                        "terms" => "mega-featured"
                      ),
                      array(
                        "taxonomy" => "promotion",
                        "field" => "slug",
                        "terms" => "curated"
                      ),
                    ),
                  );

                  $the_query = new WP_Query($args);
                  if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {

                      $the_query->the_post();
                      $large_img = get_field('large_img');
                      $medium_img = get_field('medium_img');
                      $small_img = get_field('small_img');
                      $featured_text = get_field('hide_mega_feature_text');
                      $category = get_the_category();
                      ?>

                        <div class="featured col-md-12 col-sm-12 col-xs-12">

                            <img 
                              onclick="window.open('<?php the_permalink(); ?>','<?php echo post_link_target($post); ?>');"
                              class="alignnone size-full wp-image-16"
                              src="<?php echo $large_img; ?>" alt=""
                              srcset="<?php echo $large_img; ?> 1900w, <?php echo $small_img; ?> 300w, <?php echo $medium_img; ?> 768w, <?php echo $large_img; ?> 1024w"
                              sizes="(max-width: 1900px) 100vw, 1900px"
                            />

                            <div class="mega-headline">
                              <?php
                              if ($featured_text != 1) {
                                foreach ($category as $c) {
                                  $cat_name = $c->cat_name;
                                  $cat_slug = $c->slug;

                                  echo '<div class="cat-name"><a href="../category/' . $cat_slug . '">' . $cat_name . '</a></div>';
                                  break;
                                }

                                echo '<a href="' . get_permalink() . '" target="'. post_link_target($post) .'"><div class="title">' . get_the_title() . '</div>';
                                echo "<div class='subtitle'>";
                                echo the_field('subtitle');
                                echo "</div></a>";
                              }
                              ?>
                            </div><!--mega-headline-->

                        </div>

                      <?php
                    }
                    wp_reset_postdata();
                  }
                  //  END Mega Featered
                }

                $counter++;
              }
              ?>

            </div><!--class="row-eq-height"-->
        </div><!--all-items-->

    </div>

    <div id="loading-image"><img
                src="<?php echo get_home_url(); ?>/wp-content/themes/surfline/img/loading.gif"
                alt="loading"/></div>
</div><!--main_container-->
<script>
  $(".item:first-of-type").addClass("active");
</script>

<div id="offsets" style="display:none;"><?php echo INFINITE_SCROLL_POSTS_PER_PAGE ?></div>
<div id="offsets_end" style="display:none;"><?php echo "false"; ?></div>
<?php get_footer(); ?>
