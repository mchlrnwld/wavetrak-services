<script>
  if ($(window).width() < 480) {
    $(".notice h2 a, .carousel-caption h2, .featured .mega-headline .title").each(function (e, i) {
      var _this = $(this);
      var _thisText = _this.text();

      if (_thisText.length > 51) {
        _this.text(_thisText.substring(0, 50) + "...");
      }
    });
  }

</script>
<!-- script for filter menu for mobile -->
<script>
  // DOM ready
  $(function () {

    // Create the dropdown base
    $("<select name='nav' />").appendTo(".category-filter nav");

    // Create default option "Go to..."
    $("<option />", {
      "value": "",
      "text": "CATEGORY"
    }).appendTo(".category-filter nav select");

    // Populate dropdown with menu items
    $("nav a").each(function () {
      var el = $(this);
      $("<option />", {
        "value": el.attr("href"),
        "text": el.text()
      }).appendTo(".category-filter nav select");
    });


    $("nav select").change(function () {
      window.location = $(this).find("option:selected").val();
    });

    $("nav select option[value='" + window.location.href + "']").prop("selected", true);


  });
</script>
<script>
  $('.carousel-fade').carousel({
    interval: 3000,
    pause: "null"
  })
  $('.carousel').bcSwipe({threshold: 50});
</script>

<script>
  var disqus_config = function () {
    this.page.identifier = '<?php echo get_permalink($post) ?>';
  };
  $(document).ready(function () {
      var disqus_shortname = 'surfline-comments'; // Replace this value with *your* username.
      // ajax request to load the disqus javascript
      $.ajax({
        type: "GET",
        url: "//" + disqus_shortname + ".disqus.com/embed.js",
        dataType: "script",
        cache: true
      });
  });
</script>

<script>
  $(".no-thanks").click(function () {
    $("#newsletter").slideToggle("slow", function () {
    });
    return false;

    //  START Set Cookie
    $.cookie("surfline", "newsletter", {expires: 365}, {path: '/'});
    //  END Set Cookie
  });
</script>

<div id="backplane-footer"><?php echo $GLOBALS["footer"] ?></div>

<?php
$cookie_name = "surfline";
$cookie_value = "newsletter";
If (isset($_COOKIE[$cookie_name])) {
  ?>
    <script>
      $("#newsletter").hide();
    </script>
  <?php
}
?>

<?php
  $page = false;
  $category = 'editorial';
  $props = array(
    "channel" => "editorial"
  );

  if (is_front_page() || is_page("all")) {
    $page = "Surf News Home";
  } else if (is_category()) {
    $cat = $wp_query->get_queried_object();

    if ($cat instanceof WP_Term) {
      $page = $cat->name;

      $props["categoryName"] = $page;
      $props["categoryId"] = $cat->term_id;
    }
  } else if (is_tag()) {
    $cat = $wp_query->get_queried_object();

    if ($cat instanceof WP_Term) {
      $page = $cat->name;

      $props["tagName"] = $page;
      $props["tagId"] = $cat->term_id;
    }
  } else if (is_tax()) {
    $cat = $wp_query->get_queried_object();

    if ($cat instanceof WP_Term) {
      $page = $cat->name;

      $props["seriesName"] = $page;
      $props["seriesId"] = $cat->term_id;
    }
  } else if (is_single()) {
    $post = $wp_query->get_queried_object();

    $page = $post->post_title;

    $props["postId"] = $post->ID;
    $props["postDate"] = date("c", strtotime($post->post_date_gmt));

    $cats = wp_get_post_categories($post->ID, array("fields" => "names"));
    if (is_array($cats) && !empty($cats)) {
      $cats = implode(",", $cats);

      if ($cats !== "Uncategorized") {
        $props["postCategories"] = $cats;
      }
    }

    $tags = wp_get_post_tags($post->ID, array("fields" => "names"));
    if (is_array($tags) && !empty($tags)) {
      // Normalize tags which can be mispelt much easier than a cat name.
      $props["postTags"] = strtolower(implode(",", $tags));
    }

    $series = wp_get_post_terms($post->ID, "series", array("fields" => "names"));
    if (is_array($series) && !empty($series)) {
      $props["postSeries"] = implode(",", $series);
    }

    $promotion = wp_get_post_terms($post->ID, "promotion", array("fields" => "names"));
    if (is_array($promotion) && !empty($promotion)) {
      $props["postCurrentPromotion"] = implode(",", $promotion);
    }

    // Location is the DB name but it's actually content pillar.
    $pillar = wp_get_post_terms($post->ID, "location", array("fields" => "names"));
    if (is_array($pillar) && !empty($pillar)) {
      $props["postContentPillar"] = implode(",", $pillar);
    }

    $props["authorName"] = get_the_author();
    $props["authorId"] = get_the_author_meta("ID");

    $video = get_field('show_video', $post->ID);

    $props["isVideo"] = $video === true;
  }

  if ($page && $props) {
    echo '<script>
            if (analytics) {
              var props = '.json_encode($props).';

              if (window.nativeApp !== false) {
                props = $.extend(props, {
                  "viewedWithinNative": window.nativeApp
                });
              }

              analytics.page("'.$category.'","'.$page.'",props);
            }
          </script>';
  }
?>

<?php wp_footer(); ?>

<style type="text/css">
      .quiver-taxonomy-navigator__render-geoname__button-wrapper {
        text-transform: capitalize;
        font-family: Source Sans Pro, Helvetica, sans-serif;
        letter-spacing: 0.4px;
      }

      button.quiver-recently-visited__header {
        text-transform: capitalize;
        font-size: 16px;
        letter-spacing: 0.4px;
      }

      .quiver-taxonomy-navigator__header {
        margin-top: 16px;
        padding: 24px 0;
      }

      .quiver-taxonomy-navigator__header > button {
        padding: 0;
      }

      .quiver-taxonomy-navigator__breadcrumb-node__button {
        text-transform: capitalize;
        width: auto;
      }

      .quiver-taxonomy-navigator__render-geoname > button {
        display: block;
        width: auto;
      }

</style>

</body>
</html>
