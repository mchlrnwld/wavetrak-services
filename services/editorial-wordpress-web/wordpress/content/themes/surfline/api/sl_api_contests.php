<?php
// Add custom Contest API endpoint
function sl_contest_endpoint_handler( $request ) {
  $contestTax = get_term_by('slug', $request['contest'], 'sl_contest_contests')->term_id;
  $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
  $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;
  $args = array(
  	'numberposts'	=> -1,
  	'post_type'		=> 'sl_contests',
  	'meta_query'	=> array(
      'relation' => 'AND',
      array(
        'key'	  	=> 'contest',
        'value'	  	=> $contestTax,
        'compare' 	=> '=',
      ),
      array(
        'key'	  	=> 'region',
        'value'	  	=> $regionTax,
        'compare' 	=> '=',
      ),
      array(
        'key'	  	=> 'period',
        'value'	  	=> $periodTax,
        'compare' 	=> '=',
      )
  	),
  );
  try {
    $contest = new WP_Query($args);

    if (empty( $contest ) ) {
      return null;
    }

    // Use acf endpoint for formatted custom field meta data
    $contestId = $contest->post->ID;
    $contestRoute = '/acf/v3/sl_contests/'.$contestId;
    $request = new WP_REST_Request( 'GET', $contestRoute );
    $request->set_query_params( [ 'per_page' => 12 ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, false );

    $sl_contest_response_body = [
      'post' => $contest->post,
      'meta' => $data,
    ];
    return $sl_contest_response_body;
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_endpoint() {
  register_rest_route( 'sl-contests/v1', '/contest', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_endpoint_handler',
    'args' => [
      'contest',
      'region',
      'period',
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_endpoint' );

// Add custom Contest Entries API endpoint
function sl_contest_entries_endpoint_handler( $request ) {
  $months = array(
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July ',
    'August',
    'September',
    'October',
    'November',
    'December',
  );
  $limit = 12;
  $offset = 0;
  if ($request['limit'] && $request['limit'] <= $limit) {
    $limit = $request['limit'];
  }
  if ($request['offset']) {
    $offset = $request['offset'];
  }
  if ($request['sort']) {
    $sort = $request['sort'];
  }
  if ($request['location']) {
    $location = $request['location'];
  }
  if ($request['surfer']) {
    $surfer = $request['surfer'];
  }
  if ($request['month']) {
    $month = $request['month'];
  }

  $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
  $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;

  $args = array(
    'post_type'         => 'sl_contest_entries',
    'post_status'       => 'publish',
    'posts_per_page'    => 2000,
    'meta_query'	=> array(
      $regionTax ?
    		array(
    			'key'		=> 'region',
    			'value'		=> $regionTax,
    			'compare'	=> '='
        ) : null,
      $periodTax ?
    		array(
    			'key'		=> 'period',
    			'value'		=> $periodTax,
    			'compare'	=> '='
        ) : null,
    )
  );
  try {
    $posts = get_posts($args);

    if (empty( $posts ) ) {
      return null;
    }

    $data = [];
    foreach ($posts as $post) {
      $sl_contest_entries_response_body = [
        'id'  => $post->ID,
        'name'  => $post->post_title,
        'surfer' => get_term_by('id', get_field('surfer', $post->ID), 'sl_contest_surfers'),
        'photographer' => get_term_by('id', get_field('photographer', $post->ID), 'sl_contest_photographers'),
        'spot' => get_term_by('id', get_field('spot', $post->ID), 'sl_contest_spots'),
        'date' => get_field('date', $post->ID, false),
        'videoUrl' => get_field('video_url', $post->ID, false),
        'entryId' => get_field('entry_number', $post->ID, false),
        'mediaHost' => get_field('media_host', $post->ID, false),
        'posterUrl' => get_field('poster_image_url', $post->ID, false),
        'instagramHandle' => get_field('instagram_handle', $post->ID, false),
        'updatedAt' => $post->post_modified,
        'voteCount' => (int)get_field('vote_count', $post->ID)
      ];
      $data[] = $sl_contest_entries_response_body;
    }

    // START :: Sort asc/desc
    usort($data, function($a, $b) {
      return $a["entryId"] < $b["entryId"];
    });
    switch ($sort) {
      case 'name-asc':
        usort($data, function($a, $b) {
          return $a["name"] > $b["name"];
        });
        break;
      case 'name-desc':
        usort($data, function($a, $b) {
          return $a["name"] < $b["name"];
        });
        break;
      case 'location-asc':
        usort($data, function($a, $b) {
          return $a["spot"]->name > $b["spot"]->name;
        });
        break;
      case 'location-desc':
        usort($data, function($a, $b) {
          return $a["spot"]->name < $b["spot"]->name;
        });
        break;
      case 'date-asc':
        usort($data, function($a, $b) {
          return $a["date"] > $b["date"];
        });
        break;
      case 'date-desc':
        usort($data, function($a, $b) {
          return $a["date"] < $b["date"];
        });
        break;
    }
    // END :: Sort asc/desc

    // START :: Filter
    if ($location) {
      $data = array_values(array_filter($data, function($post) use($location){
        return strtolower($post["spot"]->slug) === strtolower($location);
      }));
    }
    if ($surfer && !empty($surfer)) {
      $data = array_values(array_filter($data, function($post) use($surfer){
        return strtolower($post["surfer"]->name) === strtolower($surfer);
      }));
    }
    if ($month) {
      $data = array_values(array_filter($data, function($post) use($month, $months){
        $monthNumber = intval(substr($post["date"], -4, 2));
        return strtolower($months[$monthNumber - 1]) === strtolower($month);
      }));
    }
    // END :: Filter

    return array_slice($data, $offset, $limit);
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_entries_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_entries_endpoint() {
  register_rest_route( 'sl-contests/v1', '/entries', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_entries_endpoint_handler',
    'args' => [
      'contest',
      'region',
      'period'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_entries_endpoint' );

// Add custom Contest Entries Locations API endpoint
function sl_contest_entries_locations_endpoint_handler( $request ) {
  $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
  $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;

  $args = array(
    'post_type'         => 'sl_contest_entries',
    'post_status'       => 'publish',
    'posts_per_page'    => 2000,
    'meta_query'	=> array(
      $regionTax ?
    		array(
    			'key'		=> 'region',
    			'value'		=> $regionTax,
    			'compare'	=> '='
        ) : null,
      $periodTax ?
    		array(
    			'key'		=> 'period',
    			'value'		=> $periodTax,
    			'compare'	=> '='
        ) : null,
    )
  );
  try {
    $posts = get_posts($args);

    if (empty( $posts ) ) {
      return null;
    }

    $data = [];
    foreach ($posts as $post) {
      $data[] = get_term_by('id', get_field('spot', $post->ID), 'sl_contest_spots')->slug;
    }

    return array_values(array_unique($data));
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_entries_locations_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_entries_locations_endpoint() {
  register_rest_route( 'sl-contests/v1', '/entries/locations', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_entries_locations_endpoint_handler',
    'args' => [
      'contest',
      'region',
      'date',
      'month',
      'photographer',
      'spot',
      'surfer'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_entries_locations_endpoint' );

// Add custom Contest Entries Months API endpoint
function sl_contest_entries_months_endpoint_handler( $request ) {
  $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
  $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;

  $args = array(
    'post_type'         => 'sl_contest_entries',
    'post_status'       => 'publish',
    'posts_per_page'    => 2000,
    'meta_query'	=> array(
      $regionTax ?
    		array(
    			'key'		=> 'region',
    			'value'		=> $regionTax,
    			'compare'	=> '='
        ) : null,
      $periodTax ?
    		array(
    			'key'		=> 'period',
    			'value'		=> $periodTax,
    			'compare'	=> '='
        ) : null,
    )
  );
  try {
    $posts = get_posts($args);

    if (empty( $posts ) ) {
      return null;
    }

    $months = array(
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July ',
      'August',
      'September',
      'October',
      'November',
      'December',
    );

    $data = [];
    foreach ($posts as $post) {
      $data[] = $months[intval(substr(get_field('date', $post->ID, false), -4, 2)) - 1];
    }

    return array_values(array_unique($data));
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_entries_months_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_entries_months_endpoint() {
  register_rest_route( 'sl-contests/v1', '/entries/months', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_entries_months_endpoint_handler',
    'args' => [
      'contest',
      'region',
      'date',
      'month',
      'photographer',
      'spot',
      'surfer'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_entries_months_endpoint' );

function sl_contest_entries_votes_endpoint_handler($request) {
  try {
    $key = 'vote_count';
    $entry_id = $request['id'];
    $current_count = (int)get_post_meta($entry_id, $key, true);
    update_post_meta($entry_id, $key, ++$current_count);
    return new WP_REST_Response($current_count, 200);
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_entries_votes_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_entries_votes_endpoint() {
  register_rest_route('sl-contests/v1', '/entries/(?P<id>[0-9]+)/votes', array(
    'methods' => 'PATCH',
    'callback' => 'sl_contest_entries_votes_endpoint_handler',
    'args' => [
      'id' => [
        'validate_callback' => function ($param){
          return is_numeric($param);
        }
      ]
    ],
  ));
}
add_action('rest_api_init', 'register_sl_contest_entries_votes_endpoint');

// Add custom Contest Entries Surfers API endpoint
function sl_contest_entries_surfers_endpoint_handler( $request ) {
  $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
  $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;

  $args = array(
    'post_type'         => 'sl_contest_entries',
    'post_status'       => 'publish',
    'posts_per_page'    => 2000,
    'meta_query'	=> array(
      $regionTax ?
    		array(
    			'key'		=> 'region',
    			'value'		=> $regionTax,
    			'compare'	=> '='
        ) : null,
      $periodTax ?
    		array(
    			'key'		=> 'period',
    			'value'		=> $periodTax,
    			'compare'	=> '='
        ) : null,
    )
  );
  try {
    $posts = get_posts($args);

    if (empty( $posts ) ) {
      return null;
    }

    $data = [];
    foreach ($posts as $post) {
      $data[] = get_term_by('id', get_field('surfer', $post->ID), 'sl_contest_surfers')->name;
    }

    return array_values(array_unique($data));
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_entries_surfers_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_entries_surfers_endpoint() {
  register_rest_route( 'sl-contests/v1', '/entries/surfers', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_entries_surfers_endpoint_handler',
    'args' => [
      'contest',
      'region',
      'date',
      'month',
      'photographer',
      'spot',
      'surfer'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_entries_surfers_endpoint' );

// Add custom Contest Entry API endpoint
function sl_contest_entry_endpoint_handler( $request ) {
  $entryId = $request['id'];
  try {
    $post = get_post($entryId);

    $sl_contest_entry_response_body = [
      'id'  => $post->ID,
      'name'  => $post->post_title,
      'surfer' => get_term_by('id', get_field('surfer', $post->ID), 'sl_contest_surfers'),
      'photographer' => get_term_by('id', get_field('photographer', $post->ID), 'sl_contest_photographers'),
      'spot' => get_term_by('id', get_field('spot', $post->ID), 'sl_contest_spots'),
      'date' => get_field('date', $post->ID, false),
      'videoUrl' => get_field('video_url', $post->ID, false),
      'entryId' => get_field('entry_number', $post->ID, false),
      'updatedAt' => $post->post_modified,
      'tname' => get_term_by('slug', 'pipeline', 'sl_contest_spots'),
      'voteCount' => (int)get_field('vote_count', $post->ID)
    ];
    return $sl_contest_entry_response_body;
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_entry_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_entry_endpoint() {
  register_rest_route( 'sl-contests/v1', '/entry', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_entry_endpoint_handler',
    'args' => [
      'id',
      'name'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_entry_endpoint' );

// Add custom Contest Submit API endpoint
function sl_contest_submit_endpoint_handler( $request ) {
  try {
    $body = json_decode($request->get_body());
    $post_id = wp_insert_post(array (
       'post_type' => 'sl_contest_entries',
       'post_title' => $body->title,
       'post_status' => 'draft',
    ));
    if ($post_id) {
      $surfer = get_term_by('name', $body->surfer, 'sl_contest_surfers');
      $photographer = get_term_by('name', $body->photographer, 'sl_contest_photographers');
      $spot = get_term_by('name', $body->location, 'sl_contest_spots');

      if ($surfer) {
        $surfer = [
          'term_id' => $surfer->term_id,
          'term_taxonomy_id' => $surfer->term_taxonomy_id
        ];
      } else {
        $surfer = wp_insert_term( $body->surfer, 'sl_contest_surfers' );
      }
      if ($photographer) {
        $photographer = [
          'term_id' => $photographer->term_id,
          'term_taxonomy_id' => $photographer->term_taxonomy_id
        ];
      } else {
        $photographer = wp_insert_term( $body->photographer, 'sl_contest_photographers' );
      }
      if ($spot) {
        $spot = [
          'term_id' => $spot->term_id,
          'term_taxonomy_id' => $spot->term_taxonomy_id
        ];
      } else {
        $spot = wp_insert_term( $body->location, 'sl_contest_spots' );
      }
       update_field('contest', $body->contest, $post_id);
       update_field('period', $body->period, $post_id);
       update_field('region', $body->region, $post_id);
       update_field('surfer', $surfer, $post_id);
       update_field('photographer', $photographer, $post_id);
       update_field('spot', $spot, $post_id);
       update_field('entrant_name', $body->name, $post_id);
       update_field('entrant_email', $body->email, $post_id);
       update_field('title', $body->title, $post_id);
       update_field('date', $body->date, $post_id);
       update_field('video_url', $body->videoUrl, $post_id);
       update_field('media_host', $body->mediaHost, $post_id);
       update_field('poster_image_url', $body->posterUrl, $post_id);
       update_field('entry_number', $post_id, $post_id);
       update_field('instagram_handle', $body->instagramHandle, $post_id);
    }
    return $post_id;
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_submit_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_submit_endpoint() {
  register_rest_route( 'sl-contests/v1', '/submit', array(
    'methods' => 'POST',
    'callback' => 'sl_contest_submit_endpoint_handler',
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_submit_endpoint' );

/**
 * Returns array of Categories
 *
 * @param $postId
 */
function build_categories($postId) {
  $categories = array();

  // Get categories
  $fetched_categories = get_the_category($postId);
  foreach ($fetched_categories as $category) {
    $term_id = $category->term_id;
    $taxonomy = $category->taxonomy;
    // Category background image
    $background = get_field('cat_background', $taxonomy.'_'.$term_id) ?: null;
    // Category link
    $url = get_term_link($category);
    array_push($categories, (object) array('name' => $category->name, 'url' => $url));
  }

  // Get primary category
  $wpseo_primary_term = new WPSEO_Primary_Term('category', $postId);
  if ($wpseo_primary_term) {
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $wpseo_primary_term = get_term($wpseo_primary_term);
    $primary_category = $wpseo_primary_term;
  }
  // Reorder primary category to top of categories array
  foreach($categories as $key => $value) {
    if ($value->name == $primary_category->name) {
      unset($categories[$key]);
      array_unshift($categories, $value);
      break;
    }
  }

  return $categories;
}

/**
 * Builds array of Series
 *
 * @param $postId
 */
function build_series($postId) {
  $seriesArr = array();

  // Get series
  $fetched_series = wp_get_post_terms($postId, 'series', array("fields" => "all"));

  if (count($fetched_series) > 0) {
    foreach ($fetched_series as $series) {
      $url = modify_url(get_term_link($series));
      array_push($seriesArr, (object) array('name' => $series->name, 'url' => $url));
    }

    // Get primary series
    $wpseo_primary_term = new WPSEO_Primary_Term('series', $postId);
    if ($wpseo_primary_term) {
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $wpseo_primary_term = get_term($wpseo_primary_term);
      $primary_category = $wpseo_primary_term;
    }
    // Reorder primary series to top of series array
    foreach($seriesArr as $key => $value) {
      if ($value->name == $primary_category->name) {
        unset($seriesArr[$key]);
        array_unshift($seriesArr, $value);
        break;
      }
    }
  }
  return $seriesArr;
}

  /**
   * Builds Tags array with this properties:
   * [
   *  {
   *    "name": "Wave of the Winter",
   *    "url": "http:\/\/localhost:8080\/series\/wave-of-the-winter"
   *  },
   *  {
   *    "name": "Breaking",
   *    "url": "http:\/\/localhost:8080\/category\/news\/breaking"
   *  },
   *  ...
   * ]
   *
   * @param $posts
   */
  function build_tags($posts) {
    $data = array();
    foreach($posts as $post) {
      $post_id = $post['id'];
      $categories = build_categories($post_id);
      $series = build_series($post_id);
      $tags = array_merge($categories, $series);
      unset($post['tags']);
      $post['tags'] = $tags;
      array_push($data, $post);
    }
    return $data;
  }

// Add custom Contest News API endpoint
function sl_contest_news_endpoint_handler( $request ) {
  $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
  $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;
  // Use wp posts endpoint for formatted post data
  try {
    $request = new WP_REST_Request( 'GET', '/wp/v2/posts' );
    $request->set_query_params( [ 'per_page' => 7, 'sl_contest_regions' => $regionTax, 'sl_contest_periods' => $periodTax ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, false );
    $new_data = build_tags($data);
    return $new_data;
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_news_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_news_endpoint() {
  register_rest_route( 'sl-contests/v1', '/news', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_news_endpoint_handler',
    'args' => [
      'region'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_news_endpoint' );


// Add custom Contest Details API endpoint
function sl_contest_details_endpoint_handler( $request ) {
  try {
    $regionTax = get_term_by('slug', $request['region'], 'sl_contest_regions')->term_id;
    $periodTax = get_term_by('slug', $request['period'], 'sl_contest_periods')->term_id;
    $request = new WP_REST_Request( 'GET', '/wp/v2/pages' );
    $request->set_query_params( [ 'per_page' => 1, 'sl_contest_regions' => $regionTax, 'sl_contest_periods' => $periodTax ]);
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, false );
    return $data[0];
  } catch (Exception $e) {
    newrelic_notice_error('sl_contest_details_endpoint_handler ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_contest_details_endpoint() {
  register_rest_route( 'sl-contests/v1', '/details', array(
    'methods' => 'GET',
    'callback' => 'sl_contest_details_endpoint_handler',
    'args' => [
      'region'
      // 'period'
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_contest_details_endpoint' );
