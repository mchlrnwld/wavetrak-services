<?php
function get_posts_for_term( $term, $limit, $offset, $geotarget ) {
  $posts = array();

  $megaFeaturedArgs = array(
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'posts_per_page'    => 1,
    'offset'            => round((int)$offset / (int)$limit),
    'tax_query' => array(
      array(
        'taxonomy' => $term->taxonomy,
        'field' => 'slug',
        'terms' => $term->slug,
      ),
      'relation'		=> 'AND',
      array(
        'taxonomy' => 'promotion',
        'field' => 'slug',
        'terms' => 'mega-featured',
      ),
    ),
  );

  $defaultMegaFeaturePosts = get_posts($megaFeaturedArgs);

  $args = array(
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'posts_per_page'    => $limit,
    'offset'            => $offset,
    'tax_query' => array(
      array(
        'taxonomy' => $term->taxonomy,
        'field' => 'slug',
        'terms' => $term->slug,
      ),
      array(
        'taxonomy' => 'promotion',
        'field' => 'slug',
        'terms' => array('mega-featured'),
        'operator' => 'NOT IN',
      ),
    ),
  );

  $defaultPosts = get_posts($args);

  if (empty( $defaultPosts ) ) {
    return null;
  }

  foreach ($defaultPosts as $defaultPost) {
    array_push($posts, build_related_post($defaultPost, $geotarget));
  }

  $data = new stdClass();
  $data->posts = $posts;
  $data->featured = array(build_related_post($defaultMegaFeaturePosts[0], $geotarget));

  return $data;
}

function get_taxonomy_posts( $taxonomy, $limit, $offset, $geotarget ) {
  $posts = array();

  $megaFeaturedArgs = array(
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'posts_per_page'    => 1,
    'offset'            => round((int)$offset / (int)$limit),
    'tax_query' => array(
      array(
        'taxonomy' => $taxonomy,
        'field' => 'taxonomy',
        'terms' => $taxonomy,
      ),
      'relation'		=> 'AND',
      array(
        'taxonomy' => 'promotion',
        'field' => 'slug',
        'terms' => 'mega-featured',
      ),
    ),
  );

  $defaultMegaFeaturePosts = get_posts($megaFeaturedArgs);

  $args = array(
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'posts_per_page'    => $limit,
    'offset'            => $offset,
    'tax_query' => array(
      array(
        'taxonomy' => $taxonomy,
        'field' => 'taxonomy',
        'terms' => $taxonomy,
      ),
      array(
        'taxonomy' => 'promotion',
        'field' => 'slug',
        'terms' => array('mega-featured'),
        'operator' => 'NOT IN',
      ),
    ),
  );

  $defaultPosts = get_posts($args);

  if (empty( $defaultPosts ) ) {
    return null;
  }

  foreach ($defaultPosts as $defaultPost) {
    array_push($posts, build_related_post($defaultPost, $geotarget));
  }

  $data = new stdClass();
  $data->posts = $posts;
  $data->featured = array(build_related_post($defaultMegaFeaturePosts[0], $geotarget));

  return $data;
}


function sl_taxonomy_posts_controller( $request ) {
  $termSlug = $request['termSlug'] ?: null;
  $taxonomySlug = $request['taxonomySlug'];
  $limit = 12;
  $offset = 0;
  $geotarget = 'US';

  if ($taxonomySlug === 'tag') {
    $taxonomySlug = 'post_tag';
  }

  if ($request['limit'] && $request['limit'] <= $limit) {
    $limit = $request['limit'];
  }

  if ($request['offset']) {
    $offset = $request['offset'];
  }

  if ($request['geotarget']) {
    $geotarget = $request['geotarget'];
  }

  try {
    $taxObject = new stdClass();
    if ($termSlug) {
      $taxObject = get_posts_for_term(get_term_by('slug', $termSlug, $taxonomySlug), $limit, $offset, $geotarget);
    } else {
      $taxObject = get_taxonomy_posts($taxonomySlug, $limit, $offset, $geotarget);
    }

    return $taxObject;
  } catch (Exception $e) {
    newrelic_notice_error('sl_taxonomy_posts_controller ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }

}

function register_sl_taxonomyname_posts_endpoint() {
  register_rest_route( 'sl/v1', '/taxonomy/posts/(?P<taxonomySlug>[\S]+)/(?P<termSlug>[\S]+)', array(
    'methods' => 'GET',
    'callback' => 'sl_taxonomy_posts_controller',
    'args' => [
      'termSlug',
      'taxonomySlug',
      'offset',
      'limit',
      'geotarget',
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_taxonomyname_posts_endpoint' );

function register_sl_taxonomytype_posts_endpoint() {
  register_rest_route( 'sl/v1', '/taxonomy/posts/(?P<taxonomySlug>[\S]+)', array(
    'methods' => 'GET',
    'callback' => 'sl_taxonomy_posts_controller',
    'args' => [
      'taxonomySlug',
      'offset',
      'limit',
      'geotarget',
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_taxonomytype_posts_endpoint' );
