<?php

/* Custom endpoint to return single event
**
** /feed/event/:id
**
*/
function sl_swell_event( $request ) {
  try {
    $query = new WP_Query(array(
      'p' => $request['id'],
      'post_type' => 'sl_swell_event',
      'post_status' => 'publish',
      'posts_per_page' => 1,
    ));
    if ($query->post_count === 0) {
      return new WP_Error( 'Not found', 'Cannot find swell event for id', array( 'status' => 404 ) );
    }
    return build_event($query->post, true);
  } catch (Exception $e) {
    newrelic_notice_error('sl_swell_event ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_swell_event() {
  register_rest_route( 'sl/v1', '/events/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'sl_swell_event',
    'args' => [
      'id'
    ]
  ) );
}
add_action( 'rest_api_init', 'register_sl_swell_event' );


/* Custom endpoint to return all events
**
** /feed/events
**
*/
function sl_swell_events( $request ) {
  // Requesting all events
  try {
    $events = array();
    $query = new WP_Query(array(
      'post_type' => 'sl_swell_event',
      'post_status' => 'publish',
      'posts_per_page' => -1,
    ));
    $allBaseEvents = $query->posts;
    foreach ($allBaseEvents as $baseEvent) {
      $event = build_event($baseEvent, false);
      if ($event) {
        array_push($events, $event);
      }
    }
    return $events;
  } catch (Exception $e) {
    newrelic_notice_error('sl_swell_events ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_swell_events() {
  register_rest_route( 'sl/v1', '/events', array(
    'methods' => 'GET',
    'callback' => 'sl_swell_events',
  ) );
}
add_action( 'rest_api_init', 'register_sl_swell_events' );

function build_event($event, $include_updates) {
  if ($include_updates) {
    $eventObject = (object) array(
      'id' => '',
      'permalink' => '',
      'newWindow' => false,
      'createdAt' => '',
      'updatedAt' => '',
      'name' => '',
      'summary' => '',
      'thumbnailUrl' => '',
      'basins' => array(),
      'dfpKeyword' => '',
      'jwPlayerVideoId' => '',
      'taxonomies' => array(),
      'updates' => array(),
      'active' => true,
      'isLive' => false,
    );
  } else {
    $eventObject = (object) array(
      'id' => '',
      'permalink' => '',
      'newWindow' => false,
      'createdAt' => '',
      'updatedAt' => '',
      'name' => '',
      'summary' => '',
      'thumbnailUrl' => '',
      'basins' => array(),
      'dfpKeyword' => '',
      'jwPlayerVideoId' => '',
      'taxonomies' => array(),
      'active' => true,
      'isLive' => false,
    );
  }

  $active = get_field('active', $event->ID);
  $is_live = get_field('live', $event->ID);

  if ($active or $include_updates) {
    // Permalink
    $permalink = '';
    $customUrl = get_field('custom_swell_event_url', $event->ID);
    $newWindow = get_field('swell_event_new_window', $event->ID);
    if ($customUrl) {
      $permalink = $customUrl;
    } else {
      $defaultLink = get_permalink($event->ID);
      $explodedLink = explode('/', $defaultLink);
      $slug = slugify($event->post_title);
      $permalink = $explodedLink[0] . '//' . $explodedLink[2] . '/surf-news' . '/swell-alert' . '/' . $slug . '/' . $event->ID;
    }

    // Media
    $featuredMediaId = get_post_thumbnail_id($event->ID);
    $thumbnail = $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'thumbnail') : '';
    $banner = $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed2x') : '';

    // ACF fields
    $summary = get_field('summary', $event->ID);
    $basins = get_field('basin', $event->ID);
    $dfpKeyword = get_field('dfp_keyword', $event->ID);
    $jwPlayerVideoId = get_field('jw_player_video_id', $event->ID);
    $taxonomies = get_field('dynlocation', $event->ID);
    $targetAllLocations = false;
    if (count($taxonomies) <= 0 && get_field('all_locations', $event->ID) == 'Target All locations') {
      $targetAllLocations = true;
    }

    if (!$taxonomies) {
      $taxonomies = array();
    }

    if ($include_updates) {
      $updates = array();
      $forecasts = array();
      $customOrder = array();
      $customSwellEventPostOrder = get_field('custom_swell_event_post_order', $event->ID);
      $orderedSwellEventPosts = get_field('swell_event_post_order', $event->ID);

      // get default order
      $forecasts = get_posts(array(
        'post_type' => array(
          'sl_premium_analysis',
          'sl_realtime_forecast',
          'sl_seasonal_forecast',
          'sl_local_news'
        ),
        'meta_query' => array(
          array(
            'key' => 'parent_swell_event',
            'value' => '"' . $event->ID . '"',
            'compare' => 'LIKE'
          )
        ),
        'posts_per_page' => -1,
      ));
      if ($forecasts) {
        // order by date modified
        usort($forecasts, 'date_compare');
      }

      // merge custom order
      if ($customSwellEventPostOrder && $orderedSwellEventPosts) {
        $customOrder = $orderedSwellEventPosts;
        $forecasts = array_unique(array_merge($customOrder, $forecasts), SORT_REGULAR);
      }

      if ($forecasts) {
        foreach ($forecasts as $forecast) {
          array_push($updates, strval($forecast->ID));
        }
        $eventObject->updates = $updates;
      }
    }

    $title_tag = get_field('alert_title_tag', $event->ID);

    $date_created = dateStringToISO($event->post_date_gmt);
    $date_modified = dateStringToISO($event->post_modified_gmt);

    // Build object
    $eventObject->id = strval($event->ID);
    $eventObject->permalink = $permalink;
    $eventObject->newWindow = $newWindow;
    $eventObject->createdAt = $date_created;
    $eventObject->updatedAt = $date_modified;
    $eventObject->name = $event->post_title;
    $eventObject->summary = $summary;
    $eventObject->titleTag = $title_tag;
    $eventObject->banner = $banner;
    $eventObject->thumbnailUrl = $thumbnail;
    $eventObject->basins = $basins;
    $eventObject->dfpKeyword = $dfpKeyword;
    $eventObject->jwPlayerVideoId = $jwPlayerVideoId;
    $eventObject->taxonomies = $taxonomies;
    $eventObject->targetAllLocations = $targetAllLocations;
    $eventObject->active = $active;
    $eventObject->isLive = $is_live;

    return $eventObject;
  } else {
    return NULL;
  }
}

function date_compare($fcst1, $fcst2) {
  $t1 = strtotime($fcst1->post_modified_gmt);
  $t2 = strtotime($fcst2->post_modified_gmt);

  return $t2 - $t1;
}
