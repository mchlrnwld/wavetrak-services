<?php

function modify_url($url) {
  if (strpos($url, "/surf-news/series/") !== false) {
    return str_replace("/surf-news/", "/", $url);
  }
  return $url;
}

// Add custom endpoint to return tag names for all posts
function sl_post_tags_names( $request ) {
  $limit = 12;
  $offset = 0;
  if ($request[limit] && $request[limit] <= $limit) {
    $limit = $request[limit];
  }
  if ($request[offset]) {
    $offset = $request[offset];
  }

  try {
    $posts = $posts = get_posts([
      'post_type' => 'post',
      'post_status' => 'publish',
      'numberposts' => $limit,
      'offset' => $offset
    ]);
    $data = [];
    $taxNames = [];

    $data = [];
    foreach ($posts as $post) {
      $response_body = [
        'id'  => $post->ID,
        'tags'  => getTagNamesByPostId($post->ID)
      ];
      $data[] = $response_body;
    }
    return $data;
  } catch (Exception $e) {
    newrelic_notice_error('sl_post_tags_names ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }

}

function getTagNamesByPostId( $postId ) {
  $postCategories = getTaxNameById(wp_get_post_categories($postId), 'categories');
  $postSeries = getTaxNameById(wp_get_post_terms($postId, 'series'), 'series');
  $postTags = getTaxNameById(get_the_tags($postId), 'tags');
  $tagNames = array_merge($postCategories, $postSeries, $postTags);
  return $tagNames;
}

function getTaxNameById( $postTags, $type ) {
  if ($postTags) {
    foreach ($postTags as $postTag) {
      if ($type = 'categories') {
        $postTag = get_category($postTag);
      }
      $tagName = $postTag->name;
      $taxNames[] = $tagName;
    }
  }
  if ($taxNames) {
    return $taxNames;
  }
  return [];
}

function register_sl_post_tags_names() {
  register_rest_route( 'sl/v1', '/post-tags-names', array(
    'methods' => 'GET',
    'callback' => 'sl_post_tags_names',
    'args' => [
      'limit',
      'offset',
    ],
  ) );
}
add_action( 'rest_api_init', 'register_sl_post_tags_names' );


// Custom endpoint to return a single post
function sl_posts( $request ) {
  global $post;
  $tags = array();
  $promotions = array();
  $contentPillar = array();

  // Default post response
  $postObject = (object) array(
    'id' => 0,
    'createdAt' => '',
    'updatedAt' => '',
    'status' => '',
    'permalink' => '',
    'premium' => (object) array(
      'premium' => false,
      'paywall_heading' => '',
      'paywall_description' => '',
      'teaser' => ''
    ),
    'author' => (object) array(
      'name' => '',
      'iconUrl' => ''
    ),
    'content' => (object) array(
      'title' => '',
      'subtitle' => '',
      'displayTitle' => '',
      'body' => ''
    ),
    'auContent' => (object) array(
      'title' => '',
      'subtitle' => '',
      'displayTitle' => '',
    ),
    'media' => (object) array(
      'type' => '',
      'feed1x' => '',
      'feed2x' => '',
    ),
    'categories' => array(),
    'tags' => array(),
    'series' => array(),
    'contentPillar' => array(),
    'promotions' => array(),
    'displayOptions' => (object) array(
      'hideComments' => false,
      'hideAds' => false,
      'hideAuthor' => false
    ),
    'sponsoredArticle' => (object) array(
      'sponsoredArticle' => '',
      'attributionText' => '',
      'sponsorName' => '',
      'dfpKeyword' => '',
      'partnerContent' => '',
      'showAttributionInFeed' => '',
    ),
    'externalLink' => (object) array(
      'externalSource' => NULL,
      'newWindow' => false,
      'externalUrl' => ''
    ),
    'hero' => (object) array(
      'showHero' => '',
      'type' => '',
      'imageUrl' => '',
      'location' => NULL,
      'align' => (object) array(
        'horizontal' => '',
        'vertical' => ''
      ),
      'credit' => (object) array(
        'text' => '',
        'alignment' => ''
      ),
      'gradient' => '',
      'hideTitle' => NULL
    ),
    'yoastMeta' => (object) array(),
    'relatedPosts' => array(),
  );

  try {
    $post = get_post($request['id']);
    $preview = $request['preview'];
    $geotarget = 'US';

    if ($request['geotarget']) {
      $geotarget = $request['geotarget'];
    }

    // 404 Handler
    if ($post == NULL || ($post->post_status != 'publish' && $preview != 'true') ) {
      return new WP_Error( 'Not Found', 'Post not found', array( 'status' => 404 ) );
    }

    $authorFetch = get_userdata($post->post_author);
    $cfAvatar = get_cupp_meta($authorFetch->ID);
    $gravatar = get_avatar_url($authorFetch->ID);

    $author = (object) array('name' => $authorFetch->display_name, 'iconUrl' => $cfAvatar ?: $gravatar);

    $permalink = get_permalink($post->ID);
    $categories = build_post_categories($post);
    $status = $post->post_status;

    // Featured Image
    $featuredMediaId = get_post_thumbnail_id($post->ID);
    $media = array(
      'type' => 'image',
      'feed1x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed1x') : '',
      'feed2x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed2x') : ''
    );

    // tags
    foreach (wp_get_post_tags($post->ID, array("fields" => "all")) as $tag) {
      $url = get_term_link($tag);
      array_push($tags, (object) array('name' => $tag->name, 'slug' => $tag->slug, 'url' => $url, 'taxonomy' => $tag->taxonomy));
    }

    // series
    $series = build_post_series($post);

    // promotions
    foreach (wp_get_post_terms($post->ID, 'promotion', array("fields" => "all")) as $promo) {
      $url = get_term_link($promo);
      array_push($promotions, (object) array('name' => $promo->name, 'slug' => $promo->slug, 'url' => $url, 'taxonomy' => $promo->taxonomy));
    }

    // content pillar
    foreach (wp_get_post_terms($post->ID, 'location', array("fields" => "all")) as $ser) {
      $link = get_term_link($ser);
      array_push($contentPillar, (object) array('name' => $ser->name, 'slug' => $ser->slug, 'link' => $link, 'taxonomy' => $ser->taxonomy));
    }

    // Get Related Posts from plugin Related Posts by Taxonomy
    $relatedPosts = array();
    $args = array('posts_per_page' => 6);
    $relatedPostsDefault = km_rpbt_get_related_posts($post->ID, $args);
    // Get Select Posts (overriding related posts)
    $selectPosts = get_field('select_posts', $post->ID);
    // Combine both if necessary
    $related = build_posts_array($relatedPostsDefault, $selectPosts, 6, $geotarget);

    // It's safer to fetch the fields individually than to fetch the acf object and pull from that
    // The acf object doesn't return objects at all if they aren't checked
    $attributionText = get_field('attribution_text', $post->ID);
    $sponsorName = get_field('sponsor_name', $post->ID);
    $sponsoredArticleBool = get_field('sponsored_article', $post->ID);
    $dfpKeyword = get_field('dfp_keyword', $post->ID);
    $partnerContent = get_field('partner_content', $post->ID);
    $showAttributionInFeed = get_field('show_attribution_in_feed', $post->ID);
    $externalSource = get_field('externalSource', $post->ID);
    $newWindow = get_field('newWindow', $post->ID);
    $externalUrl = get_field('externalUrl', $post->ID);
    $showHero = get_field('hero_show', $post->ID);
    $heroType = get_field('hero_type', $post->ID);
    $heroImage = wp_get_attachment_url(get_field('hero_large', $post->ID));
    $title = $post->post_title;
    $subtitle = get_field('subtitle', $post->ID);
    $displayTitle = get_field('display_title', $post->ID);
    $au_title = get_field('au_title', $post->ID);
    $au_subtitle = get_field('au_subtitle', $post->ID);
    $au_display_title = get_field('au_display_title', $post->ID);
    $hideComments = get_field('hide_comments', $post->ID);
    $hideAds = get_field('hide_ads', $post->ID);
    $hideAuthor = get_field('hide_author', $post->ID);
    $titleLocation = get_field('title_location', $post->ID);
    $title_hero_align = get_field('title_aligment', $post->ID);
    $title_hero_valign = get_field('v_alignment', $post->ID);
    $creditText = get_field('credit', $post->ID);
    $creditAlignment = get_field('credit_alignment', $post->ID);
    $gradient = get_field('hero_gradient', $post->ID);
    $hideTitle = get_field('hide_title', $post->ID);

    // Premium content
    $premium = get_field('premium', $post->ID);
    $paywall_header = get_field('paywall_heading', $post->ID);
    $paywall_desc = get_field('paywall_description', $post->ID);
    // Building paywall posts
    $paywall_posts = get_field('paywall_posts', $post->ID);

    $queryArgs = array(
      'numberposts'	=> 3,
      'post_type'		=> 'post',
      'meta_key'	  => 'premium',
      'meta_value'	=> true,
    );
    $paywallQuery = new WP_Query($queryArgs);
    $defaultPaywallPosts = $paywallQuery->posts;
    // Combine base and override posts
    $paywallPosts = build_posts_array($defaultPaywallPosts, $paywall_posts, 3, $geotarget);

    $date_created = dateStringToISO($post->post_date_gmt);
    $date_modified = dateStringToISO($post->post_modified_gmt);

    // YOAST SEO metadata
    $yoast = get_yoast_meta($post, $permalink, $media, $categories, $heroImage, $geotarget, $date_created, $date_modified);

    // Build post teaser
    $body = format_sl_post_content($post);
    $teaser = explode('<div id="premium-divider"', $body)[0];

    // Modify default post object
    $postObject->id = $post->ID;
    $postObject->createdAt = $date_created;
    $postObject->updatedAt = $date_modified;
    $postObject->status = $status;
    $postObject->permalink = $permalink;
    $postObject->author = $author;
    $postObject->premium = (object) array(
      'premium' => $premium,
      'paywallHeading' => $premium ? $paywall_header : null,
      'paywallDescription' => $premium ? $paywall_desc : null,
      'teaser' => $premium ? $teaser : null,
      'paywallPosts' => $premium ? $paywallPosts : null,
    );
    $postObject->content = (object) array(
      'title' => build_headline($au_title, $title, $geotarget),
      'subtitle' => build_headline($au_subtitle, $subtitle, $geotarget),
      'displayTitle' => build_headline($au_display_title, $displayTitle, $geotarget),
      'body' => $body
    );
    $postObject->auContent = (object) array(
      'title' => build_headline($au_title, $title, $geotarget),
      'subtitle' => build_headline($au_subtitle, $subtitle, $geotarget),
      'displayTitle' => build_headline($au_display_title, $displayTitle, $geotarget),
    );
    $postObject->media = $media;
    $postObject->categories = $categories;
    $postObject->tags = $tags;
    $postObject->series = $series;
    $postObject->promotions = $promotions;
    $postObject->contentPillar = $contentPillar;
    $postObject->displayOptions = (object) array(
      'hideComments' => $hideComments,
      'hideAds' => $hideAds,
      'hideAuthor' => $hideAuthor
    );
    $postObject->sponsoredArticle = (object) array(
      'sponsoredArticle' => $sponsoredArticleBool,
      'attributionText' => $attributionText,
      'sponsorName' => $sponsorName,
      'dfpKeyword' => $dfpKeyword,
      'partnerContent' => $partnerContent,
      'showAttributionInFeed' => $showAttributionInFeed,
    );
    $postObject->externalLink = (object) array(
      'externalSource' => $externalSource,
      'newWindow' => $newWindow,
      'externalUrl' => $externalUrl
    );
    $postObject->hero = (object) array(
      'showHero' => $showHero,
      'type' => $heroType,
      'imageUrl' => $heroImage,
      'location' => $titleLocation,
      'align' => (object) array(
        'horizontal' => $title_hero_align,
        'vertical' => $title_hero_valign
      ),
      'credit' => (object) array(
        'text' => $creditText,
        'alignment' => $creditAlignment
      ),
      'gradient' => $gradient,
      'hideTitle' => $hideTitle
    );
    $postObject->yoastMeta = $yoast;
    $postObject->relatedPosts = $related;

    return $postObject;
  } catch (Exception $e) {
    newrelic_notice_error('sl_posts ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}


function build_posts_array($base, $overrides, $limit, $geotarget) {
  if ($overrides) {
    $postArray = array_slice(array_merge($overrides, $base), 0, $limit);
  } else {
    $postArray = $base;
  }
  $formatPosts = array();
  foreach ($postArray as $post) {
    $builtPost = build_related_post($post, $geotarget);
    array_push($formatPosts, $builtPost);
  }
  return $formatPosts;
}

function format_sl_post_content($post) {
  setup_postdata($post->ID);
  $formatted_content = apply_filters('the_content', get_the_content());
  wp_reset_postdata($post);
  return $formatted_content;
}

function get_yoast_meta($post, $permalink, $media, $categories, $heroImg, $geotarget, $createdAt, $updatedAt) {
  // Overwritten values will appear in these parameters
  $yoast_title = get_post_meta($post->ID, '_yoast_wpseo_title', true);
  $yoast_description = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
  $yoast_canonical = get_post_meta($post->ID, '_yoast_wpseo_canonical', true);
  $yoast_opengraph_title = get_post_meta($post->ID, '_yoast_wpseo_opengraph-title', true);
  $yoast_opengraph_description = get_post_meta($post->ID, '_yoast_wpseo_opengraph-description', true);
  $yoast_opengraph_image = get_post_meta($post->ID, '_yoast_wpseo_opengraph-image', true);
  $yoast_twitter_title = get_post_meta($post->ID, '_yoast_wpseo_twitter-title', true);
  $yoast_twitter_description = get_post_meta($post->ID, '_yoast_wpseo_twitter-description', true);
  $yoast_twitter_image = get_post_meta($post->ID, '_yoast_wpseo_twitter-image', true);

  $yoastMeta = array(
    'title' => $yoast_title ?: build_headline(get_field('au_title', $post->ID), $post->post_title, $geotarget),
    'description' => $yoast_description ?: build_headline(get_field('au_subtitle', $post->ID), get_field('subtitle', $post->ID), $geotarget),
    'og:type' => 'article',
    'og:site_name' => 'Surfline',
    'og:title' => $yoast_opengraph_title ?: $post->post_title,
    'og:image' => $yoast_opengraph_image ?: $media['feed2x'] ?: $heroImg,
    'og:description' => $yoast_opengraph_description ?: build_headline(get_field('au_subtitle', $post->ID), get_field('subtitle', $post->ID), $geotarget),
    'og:url' => $permalink,
    'og:updated_time' => $updatedAt,
    'canonical' => $yoast_canonical ?: $permalink,
    'article:section' => $categories[0]->name,
    'article:published_time' => $createdAt,
    'article:updated_time' => $updatedAt,
    'twitter:card' => $yoast_twitter_description ?: build_headline(get_field('au_subtitle', $post->ID), get_field('subtitle', $post->ID), $geotarget),
    'twitter:title' => $yoast_twitter_title ?: build_headline(get_field('au_title', $post->ID), $post->post_title, $geotarget),
    'twitter:image' => $yoast_twitter_image ?: $media['feed2x'] ?: $heroImg,
    'twitter:description' => $yoast_twitter_description ?: build_headline(get_field('au_subtitle', $post->ID), get_field('subtitle', $post->ID), $geotarget),
    'twitter:url' => $permalink,
  );
  return $yoastMeta;
}

function build_related_post($post, $geotarget) {
  $id = $post->ID;
  $title = build_headline(get_field('au_title', $post->ID), $post->post_title, $geotarget);
  $createdAt = $post->post_date;
  $featuredMediaId = get_post_thumbnail_id($post->ID);
  $premium = get_field('premium', $post->ID);
  $media = array(
    'type' => 'image',
    'feed1x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed1x') : '',
    'feed2x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed2x') : ''
  );
  $permalink = get_permalink($post->ID);
  $subtitle = build_headline(get_field('au_subtitle', $post->ID),get_field('subtitle', $post->ID), $geotarget);
  $categories = build_post_categories($post);
  $series = build_post_series($post);

  $relatedPost = (object) array(
    'id' => $id,
    'title' => $title,
    'subtitle' => $subtitle,
    'premium' => $premium,
    'permalink' => $permalink,
    'createdAt' => $createdAt,
    'categories' => $categories,
    'series' => $series,
    'media' => $media,
  );
  return $relatedPost;
}

function build_post_categories($post) {
  $categories = array();
  $postId = $post->ID;

  // Get categories
  $fetched_categories = get_the_category($postId);
  foreach ($fetched_categories as $category) {
    $term_id = $category->term_id;
    $taxonomy = $category->taxonomy;
    // Category background image
    $background = get_field('cat_background', $taxonomy.'_'.$term_id) ?: null;
    // Category link
    $url = get_term_link($category);
    array_push($categories, (object) array('name' => $category->name, 'slug' => $category->slug, 'url' => $url, 'imageUrl' => $background, 'taxonomy' => $category->taxonomy));
  }

  // Get primary category
  $wpseo_primary_term = new WPSEO_Primary_Term('category', $postId);
  if ($wpseo_primary_term) {
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $wpseo_primary_term = get_term($wpseo_primary_term);
    $primary_category = $wpseo_primary_term;
  }
  // Reorder primary category to top of categories array
  foreach($categories as $key => $value) {
    if ($value->name == $primary_category->name) {
      unset($categories[$key]);
      array_unshift($categories, $value);
      break;
    }
  }

  return $categories;
}

function build_post_series($post) {
  $seriesArr = array();
  $postId = $post->ID;

  // Get series
  $fetched_series = wp_get_post_terms($postId, 'series', array("fields" => "all"));
  foreach ($fetched_series as $series) {
    $taxonomy = $series->taxonomy;
    $url = modify_url(get_term_link($series));
    array_push($seriesArr, (object) array('name' => $series->name, 'slug' => $series->slug, 'url' => $url, 'taxonomy' => $series->taxonomy));
  }

  // Get primary series
  $wpseo_primary_term = new WPSEO_Primary_Term('series', $postId);
  if ($wpseo_primary_term) {
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $wpseo_primary_term = get_term($wpseo_primary_term);
    $primary_category = $wpseo_primary_term;
  }
  // Reorder primary series to top of series array
  foreach($seriesArr as $key => $value) {
    if ($value->name == $primary_category->name) {
      unset($seriesArr[$key]);
      array_unshift($seriesArr, $value);
      break;
    }
  }
  return $seriesArr;
}

function build_headline($au_value, $value, $geotarget) {
  if ($geotarget == 'AU') {
    return ($au_value ?: $value) ?: ($value ?: $au_value);
  } else {
    return ($value ?: $au_value) ?: ($au_value ?: $value);
  }
}

function register_sl_posts() {
  register_rest_route( 'sl/v1', '/posts', array(
    'methods' => 'GET',
    'callback' => 'sl_posts',
    'args' => [
      'id',
      'preview',
      'geotarget',
    ]
  ) );
}
add_action( 'rest_api_init', 'register_sl_posts' );

function sl_post_sponsor( $request ) {
  try {
    $post = get_post($request['id']);
    // 404 Handler
    if ($post == NULL) {
      return new WP_Error( 'Not Found', 'Post not found', array( 'status' => 404 ) );
    }
    $dfpKeyword = get_field('dfp_keyword', $post->ID);
    $hideAds = get_field('hide_ads', $post->ID);

    $responseBody = (object) array(
      'dfpKeyword' => $dfpKeyword,
      'hideAds' => $hideAds,
    );
    return $responseBody;
  } catch (Exception $e) {
    newrelic_notice_error('sl_post_sponsor ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_post_sponsor() {
  register_rest_route( 'sl/v1', '/post/sponsor', array(
    'methods' => 'GET',
    'callback' => 'sl_post_sponsor',
    'args' => [
      'id'
    ]
  ) );
}
add_action( 'rest_api_init', 'register_sl_post_sponsor' );
