<?php

function get_related_series( $term, $randomize ) {
  $relatedSeries = array();
  $series = get_field('related_series', $term->taxonomy . '_' . $term->term_id) ?: null;
  foreach ($series as $seriesId) {
    array_push($relatedSeries, build_term(get_term($seriesId), false, false, false, false, null));
  }
  if ($randomize) {
    shuffle($relatedSeries);
  }
  return $relatedSeries;
}

function get_breadcrumbs( $term ) {
  $ancestors = array();
  $reversedRawAncestry = get_ancestors($term->term_id, $term->taxonomy);
  $rawAncestors = array_reverse($reversedRawAncestry);

  foreach ($rawAncestors as $rawAncestorId) {
    $rawAncestor = get_term($rawAncestorId);
    array_push($ancestors,
      (object) array(
        'id' => $rawAncestor->term_id,
        'taxonomy' => $rawAncestor->taxonomy,
        'name' => $rawAncestor->name,
        'slug' => $rawAncestor->slug,
        'permalink' => build_permalink($rawAncestor, false, false)
      )
    );
  }

  return $ancestors;
}

function get_subcategories( $term, $geotarget ) {
  $subCategories = array();
  $categories = get_categories(array( 'parent' => $term->term_id ));
  foreach ($categories as $category) {
    array_push($subCategories, build_term(get_term($category), false, true, false, false, $geotarget));
  }
  return $subCategories;
}

function get_term_posts( $term, $geotarget ) {
  $posts = array();
  $args = array(
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'posts_per_page'    => 4,
    'tax_query' => array(
      array(
          'taxonomy' => $term->taxonomy,
          'field' => 'slug',
          'terms' => $term->slug,
      )
    )
  );
  $defaultPosts = get_posts($args);
  if (empty($defaultPosts)) {
    return null;
  }

  foreach ($defaultPosts as $defaultPost) {
    // This uses the function build_related_post from the ./sl_api_posts.php file
    array_push($posts, build_related_post($defaultPost, $geotarget));
  }
  return $posts;
}

function build_permalink($term, $isPremiumSeries, $isParentTax) {
  $root = $isPremiumSeries  ? '/premium-series' : '/' . $term->taxonomy;
  $base_permalink = get_permalink($term->term_id);
  $explodedLink = explode('/', $base_permalink);
  $permalink = $explodedLink[0] . '//' . $explodedLink[2] . '/surf-news' . $root;
  $permalink = $isParentTax ? $permalink : $permalink . '/' . $term->slug;
  return $permalink;
}

function build_yoast_meta( $term, $permalink ) {
  // Had to use a different method than Posts here, as yoast data is not availabile in
  // the same place for Taxonomies
  $yoast_tax_meta = get_option("wpseo_taxonomy_meta");
  $yoast_block = $yoast_tax_meta[$term->taxonomy][$term->term_id];
  $yoast_title = $yoast_block['wpseo_opengraph-title'] ?: $yoast_block['wpseo_twitter-title'] ?: $term->name;
  $background = get_field('cat_background', $term->taxonomy . '_' . $term->term_id);
  $default_description = term_description($term->term_id);

  $yoastMeta = array(
    'title' => $yoast_title,
    'description' => $yoast_block['wpseo_opengraph-description'] ?: $default_description,
    'og:type' => 'index',
    'og:site_name' => 'Surfline',
    'og:title' => $yoast_block['wpseo_opengraph-title'] ?: $yoast_title,
    'og:image' => $yoast_block['wpseo_opengraph-image'] ?: $background ?: null,
    'og:description' => $yoast_block['wpseo_opengraph-description'] ?: $default_description,
    'og:url' => $permalink,
    'twitter:card' => 'summary',
    'twitter:title' => $yoast_block['wpseo_twitter-title'] ?: $yoast_title,
    'twitter:image' => $yoast_block['wpseo_twitter-image'] ?: $background ?: null,
    'twitter:description' => $yoast_block['wpseo_twitter-description'] ?: $default_description,
    'twitter:url' => $permalink,
  );
  return $yoastMeta;
}

function build_top_level_categories() {
  $topLevelCategories = array();

  $rootCategories = get_terms(
    array(
      'parent' => 0,
      'taxonomy' => 'category',
    )
  );
  foreach ($rootCategories as $rootCategory) {
    array_push($topLevelCategories,
      (object) array(
        'id' => $rootCategory->term_id,
        'name' => $rootCategory->name,
        'taxonomy' => $rootCategory->taxonomy,
        'permalink' => build_permalink($rootCategory, false, false),
        'slug' => $rootCategory->slug,
      )
    );
  }

  return $topLevelCategories;
}

function build_term( $term, $buildRelated, $includePosts, $includeTopLevelCat, $randomizePremiumSeries = false, $geotarget ) {
  if ($term->taxonomy === 'series') {
    $isPremiumSeries = get_field('premium_series', $term->taxonomy . '_' . $term->term_id);
  }
  $permalink = build_permalink($term, $isPremiumSeries, false);

  $taxCorrected = $term->taxonomy === 'post_tag' ? 'tag' : $term->taxonomy;

  $taxonomy = (object) array(
    'id' => $term->term_id,
    'name' => $term->name,
    'description' => term_description($term->term_id),
    'permalink' => $permalink,
    'slug' => $term->slug,
    'taxonomy' => $taxCorrected,
    'premium' => $term->taxonomy === 'series' ? $isPremiumSeries : null,
    'image' => get_field('cat_background', $term->taxonomy . '_' . $term->term_id) ?: null,
    'logo' => get_field('series_logo', $term->taxonomy . '_' . $term->term_id) ?: null,
    'cardImage' => get_field('series_card', $term->taxonomy . '_' . $term->term_id) ?: null,
    'yoast' => build_yoast_meta($term, $permalink),
    // 'relatedSeries' => $buildRelated ? get_related_series($term, $randomizePremiumSeries) : null ?: null,
    'subCategories' => get_subcategories($term, $geotarget),
    'posts' => $includePosts ? (get_term_posts($term, $geotarget) ?: null) : null,
    'breadcrumbs' => get_breadcrumbs($term) ?: null,
    'navTerms' => $includeTopLevelCat ? build_top_level_categories() : null,
  );
  return $taxonomy;
}

function sl_taxonomy_controller( $request ) {
  // Default query parameters
  $slug = $request['slug'] ?: null;
  $type = $request['type'];
  $randomizePremiumSeries = $request['randomizePremiumSeries'] === 'true';
  $acceptedTypes = array('category', 'series', 'premium-series', 'tag');
  $geotarget = 'US';

  if ($request['geotarget']) {
    $geotarget = $request['geotarget'];
  }

  if (!in_array($type, $acceptedTypes)) {
    return new WP_Error( 'WP API Error', 'Invalid taxonomy', array( 'status' => 400 ) );
  }

  if ($type === 'tag') {
    $type = 'post_tag';
  }

  try {
    if ($slug) {
      // Single taxonomy
      $term = get_term_by('slug', $slug, $type);
      if (!$term) {
        return new WP_Error( 'WP API Error', 'No taxonomy found for slug', array( 'status' => 404 ) );
      }
      $taxObject = build_term($term, true, false, true, $randomizePremiumSeries, $geotarget);
      return $taxObject;
    } else {
      $allTaxonomies = array();
      // All Taxonomies for type
      $taxonomy = get_taxonomy($type === 'premium-series' ? 'series' : $type);
      if ($type !== 'post_tag') {
        $taxSearchArgs = array(
          'taxonomy' => $type === 'premium-series' ? 'series' : $type,
          'include_children' => true,
        );
        $terms = get_terms($taxSearchArgs);
        foreach ($terms as $term) {
          $taxonomyObj = build_term($term, true, true, true, $randomizePremiumSeries, $geotarget);
          if ($type === 'premium-series') {
            if ($taxonomyObj->premium) {
              array_push($allTaxonomies, $taxonomyObj);
            }
          } else {
            array_push($allTaxonomies, $taxonomyObj);
          }
        }
      }

      $permalink = build_permalink($terms[0], $isPremiumSeries, true);

      $taxonomyMod = (object) array(
        'name' => $taxonomy->labels->singular_name,
        'description' => $taxonomy->description,
        'permalink' => $permalink,
        'slug' => $taxonomy->rewrite['slug'],
        'taxonomy' => $taxonomy->rewrite['slug'],
        'premium' => $taxonomy->name === 'series' ? $isPremiumSeries : null,
        'image' => get_field('cat_background', $taxonomy->rewrite->slug) ?: null,
        'yoast' => build_yoast_meta($taxonomy, $permalink),
        'subCategories' => $allTaxonomies,
        'navTerms' => build_top_level_categories(),
      );

      return $taxonomyMod;
    }
  } catch (Exception $e) {
    newrelic_notice_error('sl_taxonomy_controller ', $e->getMessage());
    return new WP_Error( 'Internal Server Error', $e->getMessage(), array( 'status' => 500 ) );
  }
}

function register_sl_single_taxonomy() {
  register_rest_route( 'sl/v1', '/taxonomy/details/(?P<type>[\S]+)/(?P<slug>[\S]+?)', array(
    'methods' => 'GET',
    'callback' => 'sl_taxonomy_controller',
    'args' => [
      'slug',
      'type',
      'randomizePremiumSeries',
      'geotarget',
    ]
  ));
}
add_action( 'rest_api_init', 'register_sl_single_taxonomy' );

function register_sl_taxonomies() {
  register_rest_route( 'sl/v1', '/taxonomy/details/(?P<type>[\S]+)', array(
    'methods' => 'GET',
    'callback' => 'sl_taxonomy_controller',
    'args' => [
      'type',
      'randomizePremiumSeries',
      'geotarget',
    ]
  ));
}
add_action( 'rest_api_init', 'register_sl_taxonomies' );
