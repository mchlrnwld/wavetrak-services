
- [Get Posts](#get-posts)
  - [Request Query Parameters](#request-query-parameters)
  - [Sample Request](#sample-request)
    - [Sample Response](#sample-response)
- [Get Taxonomy](#get-taxonomy)
  - [Request Route Parameters](#request-route-parameters)
  - [Request Query Parameters](#request-query-parameters-1)
  - [Sample Request (List of Taxonomies)](#sample-request-list-of-taxonomies)
    - [Sample Response](#sample-response-1)
  - [Sample Request (Single Taxonomy)](#sample-request-single-taxonomy)
    - [Sample Response](#sample-response-2)
- [Get Taxonomy Posts](#get-taxonomy-posts)
  - [Request Route Parameters](#request-route-parameters-1)
  - [Request Query Parameters](#request-query-parameters-2)
  - [Sample Request](#sample-request-1)
- [Get Events](#get-events)
  - [Request Query Parameters](#request-query-parameters-3)
  - [Sample Request](#sample-request-2)
    - [Sample Response](#sample-response-3)
- [Get contests](#get-contests)
  - [Request Query Parameters](#request-query-parameters-4)
  - [Sample Request](#sample-request-3)
    - [Sample Response](#sample-response-4)

## Get Posts

Applies to: All API versions

Posts endpoint returns wordpress posts with relevant display information (acf, related posts, categories, etc.) in the response

### Request Query Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| id | int | The WP Post ID for the desired post | 2049 | Yes       | null    |

### Sample Request

```
HTTP/1.1 GET /wp-json/sl/v1/posts?id=2094
```

#### Sample Response
```JSON
// 20190304120301
// https://sandbox.surfline.com/wp-json/sl/v1/posts?id=2094

{
  "id": 2094,
  "createdAt": "2019-02-11T23:16:17+00:00",
  "updatedAt": "2019-06-04T19:02:45+00:00",
  "status": "publish",
  "permalink": "http://localhost:8080",
  "premium": {
    "premium": "1",
    "paywallHeading": "Paywall Heading Here",
    "paywallDescription": "Paywall Description Blah",
    "teaser": "<p><strong>One of the best things about researching pro surfing’s extraordinary past is that it gives you a chance to re-assess the people involved.</strong></p>\n<div id=\"preview-separator\" />\n<p>Fred Hemmings was 1968 world champion, an original member of Duke Kahanamoku’s personal surf team, and the first really committed promoter and driver of professional surfing. (He may also be the only person ever to make an actual living out of running pro events, but let’s leave that for right now.) Fred took on the Smirnoff Pro-Am in Hawaii, invented the contest that became the Pipeline Masters, set up the original pro tour rankings body International Professional Surfers in 1976, developed the World Cup and numerous other events, eventually founded the Hawaiian Triple Crown, and never surfed in any of ‘em.</p>\n",
    "paywallPosts": [
      {
        "id": 3477,
        "title": "Cam Embed - Editorial",
        "subtitle": "Testing it OUUUTT",
        "premium": "1",
        "permalink": "https://sandbox.surfline.com/surf-news/cam-embed-editorial/3477",
        "createdAt": "2019-12-13 13:48:03",
        "categories": [
          {
            "name": "Surf News",
            "slug": "surf-news",
            "url": "https://sandbox.surfline.com/category/surf-news",
            "imageUrl": null,
            "taxonomy": "category"
          }
        ],
        "series": [
          
        ],
        "media": {
          "type": "image",
          "feed1x": "",
          "feed2x": ""
        }
      },
      ...
    ]
  },
  "author": {
    "name": "Ben Holtzclaw",
    "iconUrl": "https://secure.gravatar.com/avatar/412e8d86271169376b0e5b20423f253c?s=96&d=mm&r=g"
  },
  "content": {
    "title": "A R/Evolutionary Idea for the WSL",
    "subtitle": "And its unexpected source! Fred Hemmings suggests it’s time to cross the gender divide in pro surfing.",
    "displayTitle": "This is a Display Title",
    "body": "<strong>One of the best things about researching pro surfing’s extraordinary past is that it gives you a chance to re-assess the people involved.</strong>\r\n\r\nFred Hemmings was 1968 world champion, an original member of Duke Kahanamoku’s personal surf team, and the first really committed promoter and driver of professional surfing. (He may also be the only person ever to make an actual living out of running pro events, but let’s leave that for right now.) Fred took on the Smirnoff Pro-Am in Hawaii, invented the contest that became the Pipeline Masters, set up the original pro tour rankings body International Professional Surfers in 1976, developed the World Cup and numerous other events, eventually founded the Hawaiian Triple Crown, and never surfed in any of ‘em.\r\n\r\n[caption id=\"attachment_44674\" align=\"alignnone\" width=\"2000\"]..."
  },
  "media": {
    "type": "image",
    "feed1x": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/09/13075247/Screen-Shot-2018-09-13-at-10.52.18-AM-780x412.png",
    "feed2x": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/09/13075247/Screen-Shot-2018-09-13-at-10.52.18-AM-1560x825.png"
  },
  "categories": [
    {
      "name": "Surf News",
      "slug": "surf-news",
      "link": "https://sandbox.surfline.com/category/surf-news",
      "background_image": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
      "taxonomy": "category"
    },
    {
      "name": "Contests",
      "slug": "contests",
      "link": "https://sandbox.surfline.com/category/contests",
      "background_image": null,
      "taxonomy": "category"
    },
    {
      "name": "Features",
      "slug": "features",
      "link": "https://sandbox.surfline.com/category/features",
      "background_image": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084456/large01.jpg",
      "taxonomy": "category"
    }
  ],
  "tags": [
    {
      "name": "Tag One",
      "slug": "tag-one",
      "link": "https://sandbox.surfline.com/tag/tag-one",
      "taxonomy": "post_tag"
    },
    {
      "name": "Tag Three",
      "slug": "tag-three",
      "link": "https://sandbox.surfline.com/tag/tag-three",
      "taxonomy": "post_tag"
    },
    {
      "name": "Tag Two",
      "slug": "tag-two",
      "link": "https://sandbox.surfline.com/tag/tag-two",
      "taxonomy": "post_tag"
    }
  ],
  "series": [
    {
      "name": "Best Bet",
      "slug": "best-bet",
      "link": "https://sandbox.surfline.com/surf-news/series/best-bet",
      "taxonomy": "series"
    },
    {
      "name": "Good to Epic",
      "slug": "good-to-epic",
      "link": "https://sandbox.surfline.com/surf-news/series/good-to-epic",
      "taxonomy": "series"
    }
  ],
  "promotions": [
    {
      "name": "Editors Pick",
      "slug": "editors-pick",
      "link": "https://sandbox.surfline.com/surf-news/promotion/editors-pick",
      "taxonomy": "promotion"
    },
    {
      "name": "Feed Large",
      "slug": "feed-large",
      "link": "https://sandbox.surfline.com/surf-news/promotion/feed-large",
      "taxonomy": "promotion"
    }
  ],
  "displayOptions": {
    "hideComments": true,
    "hideAds": true
  },
  "sponsoredArticle": {
    "sponsoredArticle": true,
    "attributionText": "Presented By",
    "sponsorName": "sponsor name",
    "dfpKeyword": "",
    "partnerContent": false,
    "showAttributionInFeed": true
  },
  "externalLink": {
    "externalSource": "http://localhost:8080",
    "newWindow": true,
    "externalUrl": "http://localhost:8080"
  },
  "hero": {
    "showHero": true,
    "type": "Fixed",
    "img": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/12/17112536/klein_lowers_2018_hurricane_07944.jpg",
    "align": {
      "horizontal": "right",
      "vertical": "bottom"
    },
    "credit": "Image Credit here",
    "gradient": false
  },
  "yoastMeta": {
    "yoast_wpseo_focuskw": "",
    "yoast_wpseo_title": "",
    "yoast_wpseo_metadesc": "",
    "yoast_wpseo_linkdex": "",
    "yoast_wpseo_metakeywords": "",
    "yoast_wpseo_meta-robots-noindex": "",
    "yoast_wpseo_meta-robots-nofollow": "",
    "yoast_wpseo_meta-robots-adv": "",
    "yoast_wpseo_canonical": "",
    "yoast_wpseo_redirect": "",
    "yoast_wpseo_opengraph-title": "A Revolutionary Idea for the WSL",
    "yoast_wpseo_opengraph-description": "",
    "yoast_wpseo_opengraph-image": "",
    "yoast_wpseo_twitter-title": "",
    "yoast_wpseo_twitter-description": "",
    "yoast_wpseo_twitter-image": ""
  },
  "relatedPosts": [
    {
      "id": null,
      "title": null,
      "subtitle": false,
      "premium": false,
      "permalink": "http://localhost:8080",
      "createdAt": null,
      "categories": [
        {
          "name": "Breaking",
          "slug": "breaking",
          "url": "https://sandbox.surfline.com/category/surf-news/breaking",
          "imageUrl": null,
          "taxonomy": "category"
        },
        {
          "name": "Contests",
          "slug": "contests",
          "url": "https://sandbox.surfline.com/category/contests",
          "imageUrl": null,
          "taxonomy": "category"
        },
        {
          "name": "Surf News",
          "slug": "surf-news",
          "url": "https://sandbox.surfline.com/category/surf-news",
          "imageUrl": null,
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/09/13075247/Screen-Shot-2018-09-13-at-10.52.18-AM-780x412.png",
        "feed2x": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/09/13075247/Screen-Shot-2018-09-13-at-10.52.18-AM-1560x825.png"
      }
    },
    ...
  ]
}
    ...
```


## Get Taxonomy

Applies to: All API versions

Taxonomy endpoint returns details for a series/category

### Request Route Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| type | string | The taxonomy (series, category, premium-series) | category | Yes       | null    |
| slug | string | The WP ID for the desired taxonomy | 11 | No       | null    |

### Request Query Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| randomizePremiumSeries | boolean | Whether or not to randomize related series | true | No       | false    |

### Sample Request (List of Taxonomies)

```
HTTP/1.1 GET /wp-json/sl/v1/taxonomy/details/series
```

#### Sample Response
```JSON
// https://staging.surfline.com/wp-json/sl/v1/taxonomy/details/series
{
  "name": "Post Term",
  "description": "",
  "permalink": "https://staging.surfline.com/surf-news/series",
  "slug": "series",
  "taxonomy": "series",
  "premium": null,
  "image": null,
  "yoast": {
    "title": "series",
    "description": "",
    "og:type": "index",
    "og:site_name": "Surfline",
    "og:title": "series",
    "og:image": null,
    "og:description": "",
    "og:url": "https://staging.surfline.com/surf-news/series",
    "twitter:card": "summary",
    "twitter:title": "series",
    "twitter:image": null,
    "twitter:description": "",
    "twitter:url": "https://staging.surfline.com/surf-news/series"
  },
  "subCategories": [
    {
      "id": 74,
      "name": "Best Bet",
      "description": "",
      "permalink": "https://staging.surfline.com/surf-news/series/best-bet",
      "slug": "best-bet",
      "taxonomy": "series",
      "premium": false,
      "image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/23105910/breaking_header.jpg",
      "logo": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/09/18140948/WOTW-Logo-TM.png",
      "cardImage": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/09/26144517/WOTW-Logo1.png",
      "yoast": {
        "title": "Best Bet",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Best Bet",
        "og:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/23105910/breaking_header.jpg",
        "og:description": "",
        "og:url": "https://staging.surfline.com/surf-news/series/best-bet",
        "twitter:card": "summary",
        "twitter:title": "Best Bet",
        "twitter:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/23105910/breaking_header.jpg",
        "twitter:description": "",
        "twitter:url": "https://staging.surfline.com/surf-news/series/best-bet"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1256,
          "title": "Image Caption Debug",
          "subtitle": "Checking out captions and ads",
          "premium": null,
          "permalink": "https://staging.surfline.com/surf-news/image-caption-debug/1256",
          "createdAt": "2017-05-10 15:07:19",
          "categories": [
            {
              "name": "Features",
              "slug": "features",
              "url": "https://staging.surfline.com/category/features",
              "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
              "taxonomy": "category"
            }
          ],
          "series": [
            {
              "name": "Best Bet",
              "slug": "best-bet",
              "url": "https://staging.surfline.com/series/best-bet",
              "taxonomy": "series"
            }
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/10134150/1920x1245-WelcomeToTheInternet-780x505.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/10134150/1920x1245-WelcomeToTheInternet-1560x1011.jpg"
          }
        },
        ...
      ],
      "breadcrumbs": null,
      "navTerms": [
        {
          "id": 81,
          "name": "Español",
          "taxonomy": "category",
          "permalink": "https://staging.surfline.com/surf-news/category/espanol",
          "slug": "espanol"
        },
        ...
      ]
    },
    {
      "id": 177,
      "name": "Education",
      "description": "",
      "permalink": "///surf-news/series/education",
      "slug": "education",
      "taxonomy": "series",
      "premium": null,
      "image": null,
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Education",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Education",
        "og:image": null,
        "og:description": "",
        "og:url": "///surf-news/series/education",
        "twitter:card": "summary",
        "twitter:title": "Education",
        "twitter:image": null,
        "twitter:description": "",
        "twitter:url": "///surf-news/series/education"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1842,
          "title": "Mechanics of Huntington state beach",
          "subtitle": "Breaking down Orange County's most consistent wave.",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/lets-go-surfing-calendar-test/1842",
          "createdAt": "2020-07-22 08:20:33",
          "categories": [
            {
              "name": "Travel",
              "slug": "travel",
              "url": "https://staging.surfline.com/category/travel",
              "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/30054157/lineup8654BWmargaretriver17cestari_mm.jpg",
              "taxonomy": "category"
            }
          ],
          "series": [
            {
              "name": "Education",
              "slug": "education",
              "url": "https://staging.surfline.com/series/education",
              "taxonomy": "series"
            }
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25172612/grom-award-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25172612/grom-award-1560x889.jpg"
          }
        }
      ],
      "breadcrumbs": null,
      "navTerms": [
        {
          "id": 81,
          "name": "Español",
          "taxonomy": "category",
          "permalink": "https://staging.surfline.com/surf-news/category/espanol",
          "slug": "espanol"
        },
      ]
    },
    {
      "id": 72,
      "name": "Good to Epic",
      "description": "",
      "permalink": "https://staging.surfline.com/surf-news/series/good-to-epic",
      "slug": "good-to-epic",
      "taxonomy": "series",
      "premium": null,
      "image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/10134536/2304x1440_blue_pond.jpeg",
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Good to Epic",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Good to Epic",
        "og:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/10134536/2304x1440_blue_pond.jpeg",
        "og:description": "",
        "og:url": "https://staging.surfline.com/surf-news/series/good-to-epic",
        "twitter:card": "summary",
        "twitter:title": "Good to Epic",
        "twitter:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/10134536/2304x1440_blue_pond.jpeg",
        "twitter:description": "",
        "twitter:url": "https://staging.surfline.com/surf-news/series/good-to-epic"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1917,
          "title": "Sub Series Test",
          "subtitle": "Test",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/sub-series-test/1917",
          "createdAt": "2020-04-10 11:55:57",
          "categories": [
            {
              "name": "Breaking",
              "slug": "breaking",
              "url": "https://staging.surfline.com/category/news/breaking",
              "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/23105910/breaking_header.jpg",
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            {
              "name": "Good to Epic Sub Series Test",
              "slug": "good-to-epic-sub-series-test",
              "url": "https://staging.surfline.com/series/good-to-epic-sub-series-test",
              "taxonomy": "series"
            }
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25171817/wotw-movie-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25171817/wotw-movie-1560x889.jpg"
          }
        },
        ...
      ],
      "breadcrumbs": null,
      "navTerms": [
        {
          "id": 81,
          "name": "Español",
          "taxonomy": "category",
          "permalink": "https://staging.surfline.com/surf-news/category/espanol",
          "slug": "espanol"
        },
        ...
      ]
    },
    {
      "id": 151,
      "name": "Good to Epic Sub Series Test",
      "description": "",
      "permalink": "///surf-news/series/good-to-epic-sub-series-test",
      "slug": "good-to-epic-sub-series-test",
      "taxonomy": "series",
      "premium": false,
      "image": null,
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Good to Epic Sub Series Test",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Good to Epic Sub Series Test",
        "og:image": null,
        "og:description": "",
        "og:url": "///surf-news/series/good-to-epic-sub-series-test",
        "twitter:card": "summary",
        "twitter:title": "Good to Epic Sub Series Test",
        "twitter:image": null,
        "twitter:description": "",
        "twitter:url": "///surf-news/series/good-to-epic-sub-series-test"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1917,
          "title": "Sub Series Test",
          "subtitle": "Test",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/sub-series-test/1917",
          "createdAt": "2020-04-10 11:55:57",
          "categories": [
            {
              "name": "Breaking",
              "slug": "breaking",
              "url": "https://staging.surfline.com/category/news/breaking",
              "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/23105910/breaking_header.jpg",
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            {
              "name": "Good to Epic Sub Series Test",
              "slug": "good-to-epic-sub-series-test",
              "url": "https://staging.surfline.com/series/good-to-epic-sub-series-test",
              "taxonomy": "series"
            }
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25171817/wotw-movie-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25171817/wotw-movie-1560x889.jpg"
          }
        }
      ],
      "breadcrumbs": [
        {
          "id": 72,
          "taxonomy": "series",
          "name": "Good to Epic",
          "slug": "good-to-epic",
          "permalink": "https://staging.surfline.com/surf-news/series/good-to-epic"
        }
      ],
      "navTerms": [
        {
          "id": 81,
          "name": "Español",
          "taxonomy": "category",
          "permalink": "https://staging.surfline.com/surf-news/category/espanol",
          "slug": "espanol"
        },
        ...
      ]
    },
    {
      "id": 73,
      "name": "Sixty Seconds",
      "description": "",
      "permalink": "https://staging.surfline.com/surf-news/series/sixty-seconds",
      "slug": "sixty-seconds",
      "taxonomy": "series",
      "premium": null,
      "image": null,
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Sixty Seconds",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Sixty Seconds",
        "og:image": null,
        "og:description": "",
        "og:url": "https://staging.surfline.com/surf-news/series/sixty-seconds",
        "twitter:card": "summary",
        "twitter:title": "Sixty Seconds",
        "twitter:image": null,
        "twitter:description": "",
        "twitter:url": "https://staging.surfline.com/surf-news/series/sixty-seconds"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1712,
          "title": "Ryan Test",
          "subtitle": "Subtitle here",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/ryan-test/1712",
          "createdAt": "2019-03-04 13:18:36",
          "categories": [
            {
              "name": "Science",
              "slug": "science",
              "url": "https://staging.surfline.com/category/science",
              "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12094709/klein_surfline2009_014.jpg",
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            {
              "name": "Wave of the Winter",
              "slug": "wave-of-the-winter",
              "url": "https://staging.surfline.com/series/wave-of-the-winter",
              "taxonomy": "series"
            },
            ...
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25171223/trailer-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/25171223/trailer-1560x889.jpg"
          }
        },
        ...
      ],
      "breadcrumbs": null,
      "navTerms": [
        {
          "id": 81,
          "name": "Español",
          "taxonomy": "category",
          "permalink": "https://staging.surfline.com/surf-news/category/espanol",
          "slug": "espanol"
        },
        ...
      ]
    },
    {
      "id": 71,
      "name": "Wave of the Winter",
      "description": "",
      "permalink": "https://staging.surfline.com/surf-news/series/wave-of-the-winter",
      "slug": "wave-of-the-winter",
      "taxonomy": "series",
      "premium": null,
      "image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12115735/megafeatured_LG.jpg",
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Wave of the Winter",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Wave of the Winter",
        "og:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12115735/megafeatured_LG.jpg",
        "og:description": "",
        "og:url": "https://staging.surfline.com/surf-news/series/wave-of-the-winter",
        "twitter:card": "summary",
        "twitter:title": "Wave of the Winter",
        "twitter:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12115735/megafeatured_LG.jpg",
        "twitter:description": "",
        "twitter:url": "https://staging.surfline.com/surf-news/series/wave-of-the-winter"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1868,
          "title": "Test for Glen",
          "subtitle": "Test",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/test-for-glen/1868",
          "createdAt": "2020-02-25 15:53:18",
          "categories": [
            {
              "name": "Features",
              "slug": "features",
              "url": "https://staging.surfline.com/category/features",
              "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            {
              "name": "Wave of the Winter",
              "slug": "wave-of-the-winter",
              "url": "https://staging.surfline.com/series/wave-of-the-winter",
              "taxonomy": "series"
            }
          ],
          "media": {
            "type": "image",
            "feed1x": "",
            "feed2x": ""
          }
        },
        ...
      ],
      "breadcrumbs": null,
      "navTerms": [
        {
          "id": 81,
          "name": "Español",
          "taxonomy": "category",
          "permalink": "https://staging.surfline.com/surf-news/category/espanol",
          "slug": "espanol"
        },
        ...
      ]
    }
  ],
  "navTerms": [
    {
      "id": 81,
      "name": "Español",
      "taxonomy": "category",
      "permalink": "https://staging.surfline.com/surf-news/category/espanol",
      "slug": "espanol"
    },
    ...
  ]
}
```

### Sample Request (Single Taxonomy)

```
HTTP/1.1 GET /wp-json/sl/v1/taxonomy/details/series
```

#### Sample Response
```JSON
// https://staging.surfline.com/wp-json/sl/v1/taxonomy/details/category/science
{
  "id": 69,
  "name": "Science",
  "description": "",
  "permalink": "https://staging.surfline.com/surf-news/category/science",
  "slug": "science",
  "taxonomy": "category",
  "premium": null,
  "image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12094709/klein_surfline2009_014.jpg",
  "logo": null,
  "cardImage": null,
  "yoast": {
    "title": "Science",
    "description": "",
    "og:type": "index",
    "og:site_name": "Surfline",
    "og:title": "Science",
    "og:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12094709/klein_surfline2009_014.jpg",
    "og:description": "",
    "og:url": "https://staging.surfline.com/surf-news/category/science",
    "twitter:card": "summary",
    "twitter:title": "Science",
    "twitter:image": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/12094709/klein_surfline2009_014.jpg",
    "twitter:description": "",
    "twitter:url": "https://staging.surfline.com/surf-news/category/science"
  },
  "subCategories": [
    {
      "id": 132,
      "name": "Meteorology",
      "description": "",
      "permalink": "https://staging.surfline.com/surf-news/category/meteorology",
      "slug": "meteorology",
      "taxonomy": "category",
      "premium": null,
      "image": null,
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Meteorology",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Meteorology",
        "og:image": null,
        "og:description": "",
        "og:url": "https://staging.surfline.com/surf-news/category/meteorology",
        "twitter:card": "summary",
        "twitter:title": "Meteorology",
        "twitter:image": null,
        "twitter:description": "",
        "twitter:url": "https://staging.surfline.com/surf-news/category/meteorology"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1701,
          "title": "New Large Test",
          "subtitle": "Test - Olympics + More",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/new-large-test/1701",
          "createdAt": "2019-01-31 14:48:08",
          "categories": [
            {
              "name": "Olympics",
              "slug": "olympics",
              "url": "https://staging.surfline.com/category/news/olympics",
              "imageUrl": null,
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/26083036/big-drop-award1-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/26083036/big-drop-award1-1560x889.jpg"
          }
        }
      ],
      "breadcrumbs": [
        {
          "id": 69,
          "taxonomy": "category",
          "name": "Science",
          "slug": "science",
          "permalink": "https://staging.surfline.com/surf-news/category/science"
        }
      ],
      "navTerms": null
    },
    {
      "id": 131,
      "name": "Oceanography",
      "description": "",
      "permalink": "///surf-news/category/oceanography",
      "slug": "oceanography",
      "taxonomy": "category",
      "premium": null,
      "image": null,
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Oceanography",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Oceanography",
        "og:image": null,
        "og:description": "",
        "og:url": "///surf-news/category/oceanography",
        "twitter:card": "summary",
        "twitter:title": "Oceanography",
        "twitter:image": null,
        "twitter:description": "",
        "twitter:url": "///surf-news/category/oceanography"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1701,
          "title": "New Large Test",
          "subtitle": "Test - Olympics + More",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/new-large-test/1701",
          "createdAt": "2019-01-31 14:48:08",
          "categories": [
            {
              "name": "Olympics",
              "slug": "olympics",
              "url": "https://staging.surfline.com/category/news/olympics",
              "imageUrl": null,
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/26083036/big-drop-award1-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/26083036/big-drop-award1-1560x889.jpg"
          }
        },
        ...
      ],
      "breadcrumbs": [
        {
          "id": 69,
          "taxonomy": "category",
          "name": "Science",
          "slug": "science",
          "permalink": "https://staging.surfline.com/surf-news/category/science"
        }
      ],
      "navTerms": null
    },
    {
      "id": 133,
      "name": "Science Guides",
      "description": "",
      "permalink": "///surf-news/category/science-guides",
      "slug": "science-guides",
      "taxonomy": "category",
      "premium": null,
      "image": null,
      "logo": null,
      "cardImage": null,
      "yoast": {
        "title": "Science Guides",
        "description": "",
        "og:type": "index",
        "og:site_name": "Surfline",
        "og:title": "Science Guides",
        "og:image": null,
        "og:description": "",
        "og:url": "///surf-news/category/science-guides",
        "twitter:card": "summary",
        "twitter:title": "Science Guides",
        "twitter:image": null,
        "twitter:description": "",
        "twitter:url": "///surf-news/category/science-guides"
      },
      "subCategories": [
        
      ],
      "posts": [
        {
          "id": 1701,
          "title": "New Large Test",
          "subtitle": "Test - Olympics + More",
          "premium": false,
          "permalink": "https://staging.surfline.com/surf-news/new-large-test/1701",
          "createdAt": "2019-01-31 14:48:08",
          "categories": [
            {
              "name": "Olympics",
              "slug": "olympics",
              "url": "https://staging.surfline.com/category/news/olympics",
              "imageUrl": null,
              "taxonomy": "category"
            },
            ...
          ],
          "series": [
            
          ],
          "media": {
            "type": "image",
            "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/26083036/big-drop-award1-780x444.jpg",
            "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2018/10/26083036/big-drop-award1-1560x889.jpg"
          }
        }
      ],
      "breadcrumbs": [
        {
          "id": 69,
          "taxonomy": "category",
          "name": "Science",
          "slug": "science",
          "permalink": "https://staging.surfline.com/surf-news/category/science"
        }
      ],
      "navTerms": null
    }
  ],
  "posts": null,
  "breadcrumbs": null,
  "navTerms": [
    {
      "id": 81,
      "name": "Español",
      "taxonomy": "category",
      "permalink": "https://staging.surfline.com/surf-news/category/espanol",
      "slug": "espanol"
    },
    ...
  ]
}
```

## Get Taxonomy Posts

Taxonomy posts endpoint returns posts from taxonomy and from specific taxonomy terms. 

### Request Route Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| taxonomySlug | string | The WP term slug for the desired taxonomy term | category | Yes       | null    |
| termSlug | sting | The WP term slug for the desired taxonomy term. If not set, response will return posts based on taxonomy slug | features | No       | null    |


### Request Query Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| limit | int | The number of posts to limit | 10 | No       | -1    |
| offset | int | The number of posts to be offset | 20 | No       | 0    |

### Sample Request

```JSON
 // HTTP/1.1 GET /wp-json/sl/v1/taxonomy/posts/category/features?limit=10&offset=20
{
  "posts": [
    {
      "id": 759,
      "title": "Slater Wavepool Proposed for Florida",
      "subtitle": "Is Kelly bringing his artificial wave tech to his home state?",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/slater-wavepool-proposed-for-florida/759",
      "createdAt": "2017-03-21 09:58:08",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        {
          "name": "Best Bet",
          "slug": "best-bet",
          "url": "https://staging.surfline.com/series/best-bet",
          "taxonomy": "series"
        }
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/16145114/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/16145114/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg"
      }
    },
    {
      "id": 832,
      "title": "This Week In Waves",
      "subtitle": "Hawaii pumps, California tries to get its act together and Mexico is warm and beautiful",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/this-week-in-waves/832",
      "createdAt": "2017-03-22 15:30:58",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/14085417/fanning-hero_M.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/14085417/fanning-hero_M.jpg"
      }
    },
    {
      "id": 413,
      "title": "Ben Test 0207",
      "subtitle": "Curated Promotion",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/ben-test-0207/413",
      "createdAt": "2017-02-27 14:08:20",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/01/10220246/img01-780x481.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/01/10220246/img01-1560x962.jpg"
      }
    },
    {
      "id": 682,
      "title": "Mick Fanning's First Surf Back at J-Bay",
      "subtitle": "3x World Champ catches first waves at iconic righthander",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682",
      "createdAt": "2017-03-13 12:25:32",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/13123150/mick_cover-780x463.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/13123150/mick_cover.jpg"
      }
    },
    {
      "id": 698,
      "title": "Mick Fanning: Will Return to Full-Time Competition and shred with this extra long captions",
      "subtitle": "3x world champ announces end of sabbatical, says he's committed to 2017 season test",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/mick-fanning-will-return-to-full-time-competition/698",
      "createdAt": "2017-03-14 08:49:06",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/14085417/fanning-hero_M.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/14085417/fanning-hero_M.jpg"
      }
    },
    {
      "id": 930,
      "title": "Margaret River Surf Forecast",
      "subtitle": "Solid swell on the way for the Drug Aware Pro in Western Australia",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/margaret-river-surf-forecast/930",
      "createdAt": "2017-03-29 13:23:09",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/29132956/margs-780x439.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/29132956/margs-1560x878.jpg"
      }
    },
    {
      "id": 723,
      "title": "Mick Fanning Will Return to Full-Time Competition",
      "subtitle": "3x world champ announces end of sabbatical, says he's committed to 2017 season",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/mick-fanning-will-return-to-full-time-competition-2/723",
      "createdAt": "2017-03-14 11:21:50",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/14112116/mick-2__new_M.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/14112116/mick-2__new_M.jpg"
      }
    },
    {
      "id": 636,
      "title": "Image CDN Test Post",
      "subtitle": "This is an image CDN test post",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/image-cdn-test-post/636",
      "createdAt": "2017-03-13 00:39:24",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/12192120/klein_surfline2009_008-780x520.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/12192120/klein_surfline2009_008.jpg"
      }
    },
    {
      "id": 1289,
      "title": "New Test Post",
      "subtitle": "This is a subtitle",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/new-test-post/1289",
      "createdAt": "2017-11-02 14:37:08",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        },
        {
          "name": "Travel",
          "slug": "travel",
          "url": "https://staging.surfline.com/category/travel",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/30054157/lineup8654BWmargaretriver17cestari_mm.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        {
          "name": "Best Bet",
          "slug": "best-bet",
          "url": "https://staging.surfline.com/series/best-bet",
          "taxonomy": "series"
        }
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/15141435/01-wright_t2977rio17smorigo_n-780x520.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/05/15141435/01-wright_t2977rio17smorigo_n-1560x1040.jpg"
      }
    },
    {
      "id": 300,
      "title": "Bigger Surf Lining Up For SEAT Pro Netanya Israel",
      "subtitle": "Candy canes biscuit jelly dragée chocolate bar apple pie",
      "premium": null,
      "permalink": "https://staging.surfline.com/surf-news/bigger-surf-lining-up-for-seat-pro-netanya-israel/300",
      "createdAt": "2017-01-25 16:28:38",
      "categories": [
        {
          "name": "Features",
          "slug": "features",
          "url": "https://staging.surfline.com/category/features",
          "imageUrl": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/04/26145006/features1.jpg",
          "taxonomy": "category"
        }
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/01/10220245/img05-780x481.jpg",
        "feed2x": "https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/01/10220245/img05-1560x962.jpg"
      }
    }
  ],
  "featured": [
    {
      "id": null,
      "title": null,
      "subtitle": null,
      "premium": null,
      "permalink": false,
      "createdAt": null,
      "categories": [
        
      ],
      "series": [
        
      ],
      "media": {
        "type": "image",
        "feed1x": "",
        "feed2x": ""
      }
    }
  ]
}
```

## Get Events

Applies to: All API versions

Events endpoint returns Wordpress events with relevant display information (id, name, summary, etc.) in the response

### Request Query Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| id | int | The WP Event ID for the desired event | 3519 | Yes       | null    |

### Sample Request

```
HTTP/1.1 GET /wp-json/sl/v1/events/3519
```

#### Sample Response
```JSON
// 20210302135803
// https://sandbox.surfline.com/wp-json/sl/v1/events/3519
{
  "id": "3519",
  "permalink": "https:\/\/sandbox.surfline.com\/surf-news\/swell-alert\/cam-embed-swell-event\/3519",
  "createdAt": "2020-01-14T20:31:45+00:00",
  "updatedAt": "2020-02-25T18:13:49+00:00",
  "name": "Cam Embed Swell Event",
  "summary": "Cam Embed",
  "thumbnailUrl": "",
  "basins": [
    "north_atlantic"
  ],
  "dfpKeyword": "",
  "jwPlayerVideoId": "",
  "taxonomies": [],
  "updates": [
    "3515",
    "3516",
    "3518",
    "3517"
  ],
  "active": "1",
  "titleTag": "Swell Alert",
  "banner": ""
}
```
## Get contests

Applies to: All API versions

Contests endpoint returns Wordpress contests with relevant display information (post data, acf contest data, etc.) in the response

### Request Query Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| contest | string | The WP Contest name for the desired contest | wave-of-the-winter | Yes       | null    |
| region | string | The WP Contest region for the desired contest | north-shore | Yes       | null    |
| period | string | The WP Contest period for the desired contest | 2020-2021 | Yes       | null    |


### Sample Request

```
HTTP/1.1 GET /wp-json/sl-contests/v1/contest?contest=wave-of-the-winter&region=north-shore&period=2020-2021
```

#### Sample Response
```JSON
// 20210302135803
// https://staging.surfline.com/wp-json/sl-contests/v1/contest?contest=wave-of-the-winter&region=north-shore&period=2020-2021
{
  "post": {
    "ID": 2060,
    "post_author": "22",
    "post_date": "2020-10-01 09:33:51",
    "post_date_gmt": "2020-10-01 16:33:51",
    "post_content": "",
    "post_title": "O’Neill Wave of the Winter 2020-2021 at North Shore (Hawaii)",
    "post_excerpt": "",
    "post_status": "publish",
    "comment_status": "closed",
    "ping_status": "closed",
    "post_password": "",
    "post_name": "oneill-wave-winter-2020-2021-north-shore-hawaii",
    "to_ping": "",
    "pinged": "",
    "post_modified": "2020-10-16 08:39:11",
    "post_modified_gmt": "2020-10-16 15:39:11",
    "post_content_filtered": "",
    "post_parent": 0,
    "guid": "https:\/\/staging.surfline.com\/?post_type=sl_contests&#038;p=2060",
    "menu_order": 0,
    "post_type": "sl_contests",
    "post_mime_type": "",
    "comment_count": "0",
    "filter": "raw"
  },
  "meta": {
    "acf": {
      "contest": 84,
      "region": 85,
      "period": 162,
      "logo": "https:\/\/d2pn5sjh0l9yc4.cloudfront.net\/wp-content\/uploads\/2020\/10\/15151005\/WOTW11-Micro-Site-logo-300x90.png",
      "background_image": "https:\/\/d2pn5sjh0l9yc4.cloudfront.net\/wp-content\/uploads\/2020\/10\/15175157\/WOTW11-Microsite-background-2.png",
      "contest_details": " OFFICIAL RULES By participating, Surfers and Videographers acknowledge they are participating at their own risk and that neither Surfline nor any Sponsor is responsible for any physical harm or death that may befall any Surfer or Videographer as a result of participation in these Contests.  NO PURCHASE NECESSARY. A PURCHASE OR PAYMENT OF ANY KIND WILL NOT INCREASE YOUR CHANCES OF WINNING.  ",
      "vote_url": "",
      "ad_unit_1": "",
      "ad_unit_2": "",
      "primary_background_color": "#2F2613",
      "secondary_background_color": "#EC7B03"
    }
  }
}
```
