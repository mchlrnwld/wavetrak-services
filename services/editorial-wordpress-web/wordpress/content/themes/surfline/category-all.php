<?php
/**
 * Template Name: All Categories Template
 */
get_header();
?>

<div id="main_container">
    <div id="main-content" class="container">

        <div class="category-filter">
            <nav>
              <?php wp_nav_menu(array('theme_location' => 'surf_menu')); ?>

            </nav>

        </div><!--category-filter-->

        <div class="all-items">
            <div class="row row-eq-height post-listing-all">

              <?php
              $args = array(
                'posts_per_page' => 17,
                'post_type' => 'post',
                'post_status' => 'publish',
                'suppress_filters' => true
              );

              $posts_array = get_posts($args);

              // LOOP
              $counter = 1;
              foreach ($posts_array as $inx => $post) {
                $video = get_field('show_video');
                ?>

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                          <div class="item">

                              <div class="thumbnail"
                                   onclick="document.location.href = '<?php the_permalink(); ?>'; return false">

                                <?php
                                if (!empty($video)) {
                                  echo '<div class="video-play">
										   <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
											 </div>';
                                }
                                ?>
                                <?php
                                $title = get_the_title();
                                the_post_thumbnail('large', array('title' => $title, 'alt' => $title));
                                ?>
                              </div>

                              <div class="meta">

                                <?php
                                $category = get_the_category();
                                foreach ($category as $c) {
                                  $cat_name = $c->cat_name;
                                  $cat_slug = $c->slug;
                                  echo '<a href="../category/' . $cat_slug . '">' . $cat_name . '</a>';
                                  break;
                                }
                                ?>

                                  <span><img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                             class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                                    <?php
                                    $before = "| <strong>Updated</strong>&nbsp;";
                                    if (get_the_modified_time('U') != get_the_time('U')) {
                                      echo $before;
                                      echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                                    }
                                    ?>
                                    </span>

                              </div>
                              <a href="<?php echo get_permalink($post->ID); ?>"
                                 class="overlay-link">
                                <?php
                                $title = get_the_title();
                                $title = mb_strimwidth($title, 0, 50, '...');
                                ?>
                                  <div class="headline"><?php echo $title; ?></div>
                              </a>

                          </div>
                      </div>
                  </div>
                <?php
                if ($counter == 4) {
                  ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <!-- /1024858/Box -->
                        <div id='div-gpt-ad-page-unit-2' class="hidden">
                            <script>
                              googletag.cmd.push(function () {

                                var slotId = "div-gpt-ad-page-unit-2",
                                  unitId = '/1024858/Box';

                                var thisSlot = googletag.defineSlot(unitId, [300, 250], slotId)
                                  .addService(googletag.pubads());

                                googletag.pubads().addEventListener('slotRenderEnded', function (event) {
                                  if (event.slot === thisSlot) {
                                    $("#" + slotId).removeClass("hidden");
                                  }
                                });

                                googletag.display(slotId);
                              });
                            </script>
                        </div>
                    </div>
                  <?php
                }
                ?>
                <?php
                if ($counter == 5) {
                  ?>
                  <?php
                  //  START Mega Featured

                  $args = array(
                    "posts_per_page" => -1,
                    "post_type" => "post",
                    'meta_query' => array(
                      'relation' => 'AND',
                      array(
                        'key' => 'large_img'
                      ),
                      array(
                        'key' => 'medium_img'
                      ),
                      array(
                        'key' => 'small_img'
                      ),
                      array(
                        'key' => 'hide_mega_feature_text'
                      ),
                    ),
                    "tax_query" => array(
                      array(
                        "taxonomy" => "promotion",
                        "field" => "slug",
                        "terms" => "mega-featured"
                      )
                    ),
                  );

                  $the_query = new WP_Query($args);
                  if ($the_query->have_posts()):
                    while ($the_query->have_posts()):
                      $the_query->the_post();
                      $large_img = get_field('large_img');
                      $medium_img = get_field('medium_img');
                      $small_img = get_field('small_img');
                      $featured_text = get_field('hide_mega_feature_text');
                      $category = get_the_category();
                      ?>
                        <div class="featured col-md-12 col-sm-12 col-xs-12">
                            <picture
                                    onclick="document.location.href = '<?php the_permalink(); ?>';return false"
                                    class="alignnone size-full wp-image-16">
                                <source srcset="<?php echo $large_img; ?>"
                                        media="(min-width: 1025px)">
                                <source srcset="<?php echo $medium_img; ?> "
                                        media="(min-width: 768px)">
                                <img srcset="<?php echo $small_img; ?>" alt="">
                            </picture>

                            <div class="mega-headline">
                              <?php
                              if ($featured_text != 1) {
                                foreach ($category as $c) {
                                  $cat_name = $c->cat_name;
                                  $cat_slug = $c->slug;

                                  echo '<div class="cat-name"><a href="../category/' . $cat_slug . '">' . $cat_name . '</a></div>';

                                  break;
                                }

                                echo '<a href="' . get_permalink() . '"><div class="title">' . get_the_title() . '</div>';
                                echo "<div class='subtitle'>";
                                echo the_field('subtitle');
                                echo "</div></a>";
                              }
                              ?>
                            </div><!--mega-headline-->

                        </div>

                      <?php
                    endwhile;
                    wp_reset_postdata();
                  endif;
                  //  END Mega Featered
                }
                $counter++;
              }
              ?>

            </div><!--class="row-eq-height"-->
        </div><!--all-items-->

    </div>
    <div id="loading-image"><img src="/wp-content/themes/surfline/img/loading.gif" alt="loading"/>
    </div>
</div><!--main_container-->

<script>
  $(".item:first-of-type").addClass("active");
</script>

<div id="field-function_purpose" style="display:none;"><?php $cat = single_cat_title(); ?></div>
<div id="offsets" style="display:none;">17</div>
<div id="offsets_end" style="display:none;"><?php echo "false"; ?></div>
<?php get_footer(); ?>
