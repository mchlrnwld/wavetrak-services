<?php
/*
  YARPP Template: Simple
  Author: mitcho (Michael Yoshitaka Erlewine)
  Description: A simple example YARPP template.
 */
?>
<?php
$override = get_field('overwrite_posts');
$over_count = 1;
$premium_ribbon = '<svg class="sl-premium-ribbon-icon" width="40px" height="50px" viewBox="0 0 40 50"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-71.000000, -280.000000)" fill="#22D737" fill-rule="nonzero"><polygon points="71 280 111 280 111 330 91 320.955653 71 330"></polygon></g></g></svg>';
if ($override == "1") {

  $select_posts = get_field('select_posts');

  if ($select_posts) {
    ?>

    <?php
    foreach ($select_posts as $post) { // variable must be called $post (IMPORTANT) ?
      if ($over_count <= 6) {
        setup_postdata($post);
        $premium = get_field('premium');

        ?>
          <li class="col-xs-12 col-sm-6 col-md-6">
              <?php if($premium) { echo $premium_ribbon; } ?>
              <div class="trending-pic"
                   onclick="document.location.href = '<?php the_permalink(); ?>'; return false"><?php the_post_thumbnail('medium', array('alt' => get_the_title())); ?></div>
              <div class="trending-cat"><?php the_category('&nbsp; '); ?></div>
              <div class="trending-title"><a href="<?php the_permalink(); ?>"
                                             rel="bookmark"><?php the_title(); ?></a></div>
          </li>

        <?php
        $over_count++;
      }
    }

    $over_alt = $over_count;
    if ($over_alt < 7) {

      if (have_posts()):
        ?>

        <?php while (have_posts()) : the_post();
        if ($count_alt < 7) {
          $premium = get_field('premium');
          ?>
            <li class="col-xs-12 col-sm-6 col-md-6">
                <?php if($premium) { echo $premium_ribbon; } ?>
                <div class="trending-pic"
                     onclick="document.location.href = '<?php the_permalink(); ?>'; return false"><?php the_post_thumbnail('medium'); ?></div>
                <div class="trending-cat"><?php the_category('&nbsp; '); ?></div>
                <div class="trending-title"><h6><a href="<?php the_permalink(); ?>"
                                                   rel="bookmark"><?php the_title(); ?></a></h6>
                </div>
                <!-- (<?php the_score(); ?>)-->
            </li>
          <?php
        }
        $count_alt++;
      endwhile;
        ?>

      <?php else: ?>

        <?php
      endif;
    }
    ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly  ?>
    <?php
  }
} else {
  $count = 1;
  if (have_posts()):
    ?>

    <?php while (have_posts()) : the_post();
    if ($count <= 6) {
      $premium = get_field('premium');
      ?>
        <li class="col-xs-12 col-sm-6 col-md-6">
            <?php if($premium) { echo $premium_ribbon; } ?>
            <div class="trending-pic"
                 onclick="document.location.href = '<?php the_permalink(); ?>'; return false"><?php the_post_thumbnail('medium'); ?></div>
            <div class="trending-cat"><?php the_category('&nbsp; '); ?></div>
            <div class="trending-title"><h6><a href="<?php the_permalink(); ?>"
                                               rel="bookmark"><?php the_title(); ?></a></h6></div>
            <!-- (<?php the_score(); ?>)-->
        </li>
      <?php
    }
    $count++;
  endwhile;
    ?>

  <?php else: ?>
      <p>No related posts.</p>
    <?php
  endif;
}
?>
