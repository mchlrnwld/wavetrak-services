jQuery(function ($) {

  $('.post-listing').append('<span class="load-more"></span>');
  var button = $('.post-listing .load-more');
  var page = 2;
  var loading = false;
  var scrollHandling = {
    allow: true,
    reallow: function () {
      scrollHandling.allow = true;
    }
  };

  $(window).scroll(function () {
    var offt = $('#offsets').html();
    var finished = $("#offsets_end").html();
    if (finished == "false") {
      if (!loading && scrollHandling.allow) {
        scrollHandling.allow = false;
        setTimeout(scrollHandling.reallow, scrollHandling);
        var offset = $(button).offset().top - $(window).scrollTop();
        if (700 > offset) {
          loading = true;
          $('#loading-image').show();
          $('#loading-image').bind('ajaxStart', function () {
            $(this).show();
          }).bind('ajaxStop', function () {
            $(this).hide();
          });
          var data = {
            action: 'be_ajax_load_more',
            page: page,
            offt: offt,
            query: beloadmore.query,
          };
          $.post(beloadmore.url, data, function (res) {
            $("#offsets").remove();
            $("#offsets_end").remove();
            if (res.success) {
              $('.post-listing').append(res.data);
              $('.post-listing').append(button);
              page = page + 1;
              loading = false;
              $('#loading-image').hide();
            } else {
              //console.log(res);
              // console.log('hi');
            }
          }).fail(function (xhr, textStatus, e) {
            //console.log(xhr.responseText);
            //console.log('fail');
          });

        }
      }
    } else {
      $('#loading-image').hide();
    }
  });
});


