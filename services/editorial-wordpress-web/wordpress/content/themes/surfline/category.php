<?php
/**
 * Template Name: Index Template
 */
get_header();
?>
    <div id="main_container">
        <!--  START Carousel  -->

        <div class="postinner-h1">

          <?php
          $term = get_queried_object();
          if ($term):
            $bgimg = get_field('cat_background', $term->taxonomy . '_' . $term->term_id);
            $verticle = get_field('cat_title_v_location', $term->taxonomy . '_' . $term->term_id);
            $title_location = get_field('cat_title_location', $term->taxonomy . '_' . $term->term_id);
            $title = $term->name;
          endif;
          if ($bgimg):
            ?>

              <div id="cat_splash" style="background:url('<?php echo $bgimg; ?>');">
                  <div class="cat-item">
                      <h1 class="category-title <?php echo $title_location; ?> <?php echo $verticle; ?>"><?php echo $title; ?></h1>
                  </div>

              </div>
          <?php endif;
          ?>
        </div>

        <div id="main-content" class="container">

            <div class="category-filter">
                <nav>
                  <?php wp_nav_menu(array('theme_location' => 'surf_menu')); ?>

            </div><!--category-filter-->

            <div class="all-items">
                <div class="row row-eq-height post-listing-cat">

                  <?php
                  $premium_ribbon = '<svg class="sl-premium-ribbon-icon" width="40px" height="50px" viewBox="0 0 40 50"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-71.000000, -280.000000)" fill="#22D737" fill-rule="nonzero"><polygon points="71 280 111 280 111 330 91 320.955653 71 330"></polygon></g></g></svg>';
                  $cat = $wp_query->get_queried_object();
                  $cat = $cat->cat_name;
                  $args = array(
                    'posts_per_page' => 17,
                    'post_type' => 'post',
                    'category_name' => $cat,
                    'post_status' => 'publish',
                    'suppress_filters' => true,
                  );

                  $posts_array = get_posts($args);

                  // LOOP
                  $counter = 1;
                  foreach ($posts_array as $inx => $post) {
                    $video = get_field('show_video');
                    $premium = get_field('premium');
                    $category = get_the_category();
                    ?>

                      <div class="col-md-4 col-sm-6 col-xs-12">
                          <?php if($premium) { echo $premium_ribbon; } ?>
                          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                              <div class="item">

                                  <div class="thumbnail"
                                    onclick="window.open('<?php the_permalink(); ?>','<?php echo post_link_target($post); ?>');">

                                    <?php
                                    if (!empty($video)) {
                                      echo '<div class="video-play">
										   <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
											 </div>';
                                    }
                                    ?>
                                    <?php
                                    $title = get_the_title();
                                    the_post_thumbnail('large', array('title' => $title, 'alt' => $title));
                                    ?>
                                  </div>

                                  <div class="meta">

                                    <?php
                                    foreach ($category as $c) {
                                      $cat_name = $c->cat_name;
                                      $cat_slug = $c->slug;
                                      if ($cat_name == $cat) {

                                        echo '<a href="../category/' . $cat_slug . '">' . $cat_name . '</a>&nbsp;&nbsp;';
                                      }
                                    }
                                    ?>

                                    <span>
                                      <?php 
                                        $linkAttribution = post_link_attribution($post);
                                        if ($linkAttribution) {
                                          echo '<img 
                                            src="'.get_template_directory_uri().'/img/clock.svg"
                                            class="clock"
                                          />';
                                          echo get_the_date('Y-m-d').' | ';
                                          echo $linkAttribution;
                                        } else { 
                                          echo '<img 
                                            src="'.get_template_directory_uri().'/img/clock.svg"
                                            class="clock"
                                          />';
                                          echo get_the_date('Y-m-d');
                                          $before = " | <strong>Updated</strong>&nbsp;";
                                          if (get_the_modified_time('U') != get_the_time('U')) {
                                            echo $before;
                                            echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                                          }
                                        } ?>
                                    </span>

                                  </div>
                                  <a href="<?php echo get_permalink($post->ID); ?>"
                                     class="overlay-link"
                                     target="<?php echo post_link_target($post); ?>"
                                  >
                                    <?php
                                    $title = get_the_title();
                                    $title = mb_strimwidth($title, 0, 50, '...');
                                    ?>
                                      <div class="headline"><h4><?php echo $title; ?></h4></div>
                                  </a>

                              </div>

                          </div>
                      </div>
                    <?php
                    if ($counter == 4) {
                      ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <!-- /1024858/Box -->
                            <div id='div-gpt-ad-page-unit-2-<?php echo $counter; ?>' class="ad-unit-inline-mpu">
                                <script>
                                  googletag.cmd.push(function () {

                                    var slotId = "div-gpt-ad-page-unit-2-<?php echo $counter; ?>",
                                      unitId = '/1024858/Box';

                                    var thisSlot = googletag.defineSlot(unitId, [300, 250], slotId)
                                      .addService(googletag.pubads());

                                    googletag.pubads().addEventListener('slotRenderEnded', function (event) {
                                      if (event.slot === thisSlot) {
                                        $("#" + slotId).removeClass("hidden");
                                      }
                                    });

                                    googletag.display(slotId);
                                  });
                                </script>
                            </div>
                        </div>
                      <?php
                    }
                    ?>

                      <!--  START Mega Featured  -->
                    <?php
                    if ($counter == 5) {
                      ?>
                      <?php
                      $args = array(
                        "posts_per_page" => -1,
                        "post_type" => "post",
                        'category_name' => $cat,
                        'meta_query' => array(
                          'relation' => 'AND',
                          array(
                            'key' => 'large_img'
                          ),
                          array(
                            'key' => 'medium_img'
                          ),
                          array(
                            'key' => 'small_img'
                          ),
                          array(
                            'key' => 'hide_mega_feature_text'
                          ),
                        ),
                        "tax_query" => array(
                          array(
                            "taxonomy" => "promotion",
                            "field" => "slug",
                            "terms" => "mega-featured"
                          ),
                        ),
                      );

                      $the_query = new WP_Query($args);
                      if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {

                          $the_query->the_post();
                          $large_img = get_field('large_img');
                          $medium_img = get_field('medium_img');
                          $small_img = get_field('small_img');
                          $featured_text = get_field('hide_mega_feature_text');
                          $category = get_the_category();
                          ?>

                            <div class="featured col-md-12 col-sm-12 col-xs-12">

                                <img 
                                  onclick="window.open('<?php the_permalink(); ?>','<?php echo post_link_target($post); ?>');"
                                  class="alignnone size-full wp-image-16"
                                  src="<?php echo $large_img; ?>" alt=""
                                  srcset="<?php echo $large_img; ?> 1900w, <?php echo $small_img; ?> 300w, <?php echo $medium_img; ?> 768w, <?php echo $large_img; ?> 1024w"
                                  sizes="(max-width: 1900px) 100vw, 1900px"
                                />

                                <div class="mega-headline">
                                  <?php
                                  if ($featured_text != 1) {
                                    foreach ($category as $c) {
                                      $cat_name = $c->cat_name;
                                      $cat_slug = $c->slug;

                                      echo '<div class="cat-name"><a href="../category/' . $cat_slug . '">' . $cat_name . '</a></div>';
                                      break;
                                    }

                                    echo '<a href="' . get_permalink() . '"><div class="title">' . get_the_title() . '</div>';
                                    echo "<div class='subtitle'>";
                                    echo the_field('subtitle');
                                    echo "</div></a>";
                                  }
                                  ?>
                                </div><!--mega-headline-->

                            </div>
                          <?php
                        }
                        wp_reset_postdata();
                      }
                    }
                    $counter++;
                  };
                  ?>

                    <!--  END Mega Featured  -->

                </div><!--class="row-eq-height"-->
            </div><!--all-items-->

        </div>

        <div id="loading-image"><img src="/wp-content/themes/surfline/img/loading.gif"
                                     alt="loading"/></div>
    </div><!--main_container-->

    <div id="field-function_purpose" style="display:none;"><?php $cat = single_cat_title(); ?></div>
    <div id="offsets" style="display:none;">17</div>
    <div id="offsets_end" style="display:none;"><?php echo "false"; ?></div>

<?php get_footer(); ?>
