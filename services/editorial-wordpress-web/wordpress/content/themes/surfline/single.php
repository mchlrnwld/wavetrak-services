<!-- DEPRECATED -->
<!-- This page has been moved to the travel React app, under the EditorialArticle template -->
<?php /* get_header(); ?>

    <div id="main_container">
      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php
                $hero_image = get_field('hero_large');
                $feed1x_size = is_numeric($hero_image) ? wp_get_attachment_image_url( $hero_image, 'feed1x' ) : $hero_image; // 780w
                $promobox1x_size = is_numeric($hero_image) ? wp_get_attachment_image_url( $hero_image, 'promobox1x' ) : $hero_image; // 1200w
                $promobox2x_size = is_numeric($hero_image) ? wp_get_attachment_image_url( $hero_image, 'promobox2x' ) : $hero_image; // 2000w

                if (!empty($hero_image)) {
                  ?>
                    <style>
                      @media (max-width : 780px) {
                        #article-hero-full {
                          background-image: url("<?php echo $feed1x_size; ?>");
                        }
                      }
                      @media (min-width : 780px) {
                        #article-hero-full {
                          background-image: url("<?php echo $promobox1x_size; ?>");
                        }
                      }
                      @media (min-width : 1430px) {
                        #article-hero-full {
                          background-image: url("<?php echo $promobox2x_size; ?>");
                        }
                      }
                    </style>
                <?php } ?>


                <?php if (get_field('hero_show')): ?>
                    <div class="container-fluid" id="hero" style="padding: 0;">
                        <div class="article-hero-full" id="article-hero-full">
                            <div id="single-hero" class="article-hero-full-div">
                              <?php if (get_field('title_location')): ?>
                                  <h5><?php the_category(' '); ?></h5>
                                <?php the_title('<h1>', '</h1>'); ?>
                                  <h2><?php the_field("subtitle"); ?></h2>
                              <?php endif; ?>
                            </div>
                        </div>
                    </div><!--hero-->
                <?php endif; ?>

                  <section class="article-details">
                      <div class="copy">
                          <div id="article-headline" class="article-headline">
                            <?php if (!get_field('title_location')): ?>
                                <div class="article-category">
                                    <h5><?php the_category(' '); ?><?php
                                      $term_list = wp_get_post_terms($post->ID, 'series', array("fields" => "all"));
                                      foreach ($term_list as $term_single) {
                                        $name = $term_single->name;
                                        $link = $term_single->slug;
                                        echo '<a href="' . get_home_url() . '/series/' . $link . '">' . $name . '</a>'; //do something here;
                                      }
                                      ?></h5>
                                </div><!--article-category-->

                              <?php the_title('<h1>', '</h1>'); ?>
                                <h2><?php the_field("subtitle"); ?></h2>
                            <?php endif; ?>
                          </div><!--headline-->

                          <div class="sponsor">
                              <!-- /1024858/Logo -->
                              <div id='div-gpt-ad-article-unit-2' class="hidden">
                                  <span class="presented-by">presented by</span>
                                  <script>
                                    googletag.cmd.push(function () {

                                      var slotId = "div-gpt-ad-article-unit-2",
                                        unitId = '/1024858/Logo';

                                      var thisSlot = googletag.defineSlot(unitId, [100, 40], slotId)
                                        .addService(googletag.pubads());

                                      googletag.pubads().addEventListener('slotRenderEnded', function (event) {
                                        if (event.slot === thisSlot) {
                                          $("#" + slotId).removeClass("hidden");
                                        }
                                      });

                                      googletag.display(slotId);
                                    });
                                  </script>
                              </div>
                          </div>

                          <!--sponsor-->

                          <div class="date">
                              <p>
                                  <img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                       class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                                <?php
                                $before = "| <strong>Updated</strong>&nbsp;";
                                if (get_the_modified_time('U') != get_the_time('U')) {
                                  echo $before;
                                  echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                                }
                                ?>
                              </p>
                          </div><!--date-->
                          <?php
                            $tags = get_the_tags($post->ID);
                            $categories = get_the_category();
                          ?>
                          <script>
                            var trackShareEvent = function (shareChannel) {
                              if (analytics) {
                                 var properties = {
                                  title: '<?php echo get_the_title(); ?>',
                                  contentType: 'Editorial',
                                  contentId: <?php echo the_ID(); ?>,
                                  tags: '<?php echo is_array($tags) ? implode(",", array_map(function ($tag) { return $tag->name; }, get_the_tags($post->ID))) : ''; ?>',
                                  locationCategory: '<?php echo is_array($categories) ? implode(",", array_map(function ($category) { return $category->cat_name; }, $categories)) : ''; ?>',
                                  destinationUrl: '<?php echo urlencode(get_permalink()); ?>',
                                  shareChannel: shareChannel,
                                 };
                                analytics.track(
                                  'Clicked Share Icon',
                                  properties,
                                );
                              }
                            }
                            jQuery(function () {
                              jQuery('.facebook-share').click(function (event) {
                                trackShareEvent('facebook');
                                var width = 575,
                                  height = 400,
                                  left = (jQuery(window).width() - width) / 2,
                                  top = (jQuery(window).height() - height) / 2,
                                  url = "https://www.facebook.com/share.php?u=<?php echo urlencode(get_permalink()); ?>",
                                  opts = 'status=1' +
                                    ',width=' + width +
                                    ',height=' + height +
                                    ',top=' + top +
                                    ',left=' + left;
                                window.open(url, 'facebook', opts);
                                return false;
                              });
                            });

                            jQuery(function () {
                              jQuery('.twitter-share').click(function (event) {
                                trackShareEvent('twitter');
                                var width = 575;
                                var height = 400;
                                var left = (jQuery(window).width() - width) / 2;
                                var top = (jQuery(window).height() - height) / 2;
                                var url = "https://twitter.com/intent/tweet?text=<?php echo urlencode(get_permalink()); ?>&amp;hashtags=surfline";
                                var opts = 'status=1' +
                                  ',width=' + width +
                                  ',height=' + height +
                                  ',top=' + top +
                                  ',left=' + left;
                                window.open(url, 'twitter', opts);
                                return false;
                              });
                            });
                          </script>

                          <div id="sharing">
                              <div class="share-btn facebook-share">
                                  Facebook
                              </div>

                              <div class="share-btn twitter-share">
                                  Twitter
                              </div>
                          </div><!--sharing-->

                          <hr/>

                          <div class="entry-meta clearfix">
                              <div class="profile">
                                <?php echo get_avatar(get_the_author_email(), '48'); ?>
                              </div>
                              <div class="author-details">
                                  <div class="name">
                                    <?php echo $author = get_the_author(); ?>
                                  </div>
                                  <div class="position">
                                    <?php echo $author_bio = get_the_author_meta('description'); ?>
                                  </div>
                              </div>
                          </div><!--entry-meta-->
                      </div>

                    <?php the_content(); ?>

                  </section><!--article-details-->

                  <div class="tags copy">
                    <?php the_tags(' ', ' ', ' '); ?>
                  </div><!--tags-->

                  <div id="disqus-container" class="copy">
                      <hr/>
                      <!-- The empty element required for Disqus to loads comments into -->
                      <div id="disqus_thread"></div>

                  </div><!--disqus-container-->

              </article><!--post-->

        <?php endwhile; ?>

      <?php else : ?>

          <div class="alert alert-info">
              <strong>No content in this loop</strong>
          </div>

      <?php endif; ?>

        <div class="trending-newsletter-section">
            <div id="trending-content">
                <div class="container">
                    <h3>trending content <span><small>by</small>
                    <svg width="15px" height="21px" viewBox="19 0 15 21" version="1.1"
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <path d="M26.4359171,18.3144685 C26.0154987,18.3086107 25.6088739,18.2497393 25.2220775,18.1431264 C27.3279053,17.6200204 28.9029656,15.6989383 28.9334266,13.3912382 C28.9417602,12.7058697 29.4935055,12.1578678 30.1662324,12.1678261 C30.8372351,12.1771987 31.3743247,12.7392594 31.3654163,13.4228706 C31.3286333,16.1622943 29.1222269,18.3507872 26.4359171,18.3144685 L26.4359171,18.3144685 Z M22.837216,14.5469921 C22.1647764,14.5367408 21.6273995,13.9761445 21.6365953,13.2913618 C21.6728036,10.552231 23.8792099,8.36285945 26.5660945,8.4000568 C26.9856508,8.40474308 27.3928503,8.46537183 27.7805088,8.5722776 C25.6738189,9.09391917 24.0993333,11.0132438 24.068585,13.3238729 C24.059964,14.0086556 23.5082187,14.5563646 22.837216,14.5469921 L22.837216,14.5469921 Z M33.2970995,10.1251937 C32.4559753,7.92820695 26.5002874,0.753804744 26.5002874,0.753804744 C26.5002874,0.753804744 20.5437373,7.92820695 19.7034752,10.1251937 C19.2525959,11.1069694 19,12.2020946 19,13.3560911 C19,17.5787225 22.3578873,21 26.5002874,21 C30.6424001,21 34,17.5787225 34,13.3560911 C34,12.2020946 33.7474041,11.1069694 33.2970995,10.1251937 L33.2970995,10.1251937 Z"
                          id="Fill-1-Copy" stroke="none" fill="#333333" fill-rule="evenodd"></path>
                    </svg></span></h3>
                    <ul class="row">

                      <?php if (function_exists('related_posts')) related_posts(); ?>
                    </ul>
                </div>
            </div>
        </div>

    </div><!--main_container-->

    <script>
      if ($("iframe").length) {

        // existing video-container items, do the wrap, ignore video 100%, but full wrap on non-styled videos.
        $( "iframe" ).each(function() {

          if($(this).parent("p")) {
            $(this).unwrap();
          }

          if($(this).parent().get(0).className == 'video-wrap-container') {
            $(this).unwrap();
            // rewrap with the correct two enclosing classes, in the right order
            $(this).wrap("<div class='video-main-wrap-container'></div>").wrap("<div class='video-wrap-container'></div>");
          }
          else if($(this).parent().get(0).className != 'video-wrap col-md-12') {
            $(this).wrap("<div class='video-main-wrap-container'></div>").wrap("<div class='video-wrap-container'></div>");
          }
        });
      }

      if ($(".img-cover img").length) {
        // if img-cover img has a p wrapping, unwrap it to avoid p styling conflicts
        $( ".img-cover img" ).each(function() {

          if($(this).parent("p")) {
            $(this).unwrap();
          }

        });
      }

      if ($(".img-fluid img").length) {
        // if img-fluid img has a p wrapping, unwrap it to avoid p styling conflicts
        $( ".img-fluid img" ).each(function() {

          if($(this).parent("p")) {
            $(this).unwrap();
          }

        });
      }

      if ($(".wp-caption-text").length) {
        // wrap caption text to avoid global article-details p from taking over.
        $( ".wp-caption-text" ).each(function() {

          $(this).wrap("<div class='wp-caption-wrap'></div>");

        });
      }

      $(document).ready(function () {

        var deployAfter = 4;
        var adDeployed = false;
        var pCounter = 0;
        var pCount = $('section.article-details').children("p").length;

        var sectionClassList = ["copy",
          "img-cover",
          "img-fluid",
          "wp-caption",
          "video-wrap",
          "video-main-wrap-container"];

        var sections = $('section.article-details').children();
        var totalSections = sections.length;
        $.each(sections, function (outerIndex, v) {

          var lastSection = false;
          if (outerIndex === totalSections - 1)
            lastSection = true;

          var _this = $(v);

          $.each(sectionClassList, function (innerIndex, s) {
            if (_this.hasClass(s)) {
              var deployNow = false;
              if (lastSection)
                deployNow = true;

              if (!deployNow && (_this.hasClass("video-main-wrap-container") || _this.hasClass("video-wrap") || _this.hasClass("img-cover") || _this.hasClass("img-fluid") || _this.hasClass("wp-caption"))) {
                deployNow = true;
              }

              var deployInBetween = false;
              if (!deployNow && _this.hasClass("copy") && _this.find("div.author-details").length !== 0) {
                var thisPCount = pCount;
                pCounter += thisPCount;

                if (pCounter > deployAfter) {
                  var wentOver = pCounter - deployAfter;
                  var nthP = thisPCount - wentOver;

                  deployInBetween = true;
                  deployNow = true;

                }

                if (pCounter == deployAfter)
                  deployNow = true;
              }
              if (deployNow) {
                var adCode = "<!-- /1024858/Box --><div id='div-gpt-ad-article-unit-3' class='hidden'><script>googletag.cmd.push(function() {var slotId = 'div-gpt-ad-article-unit-3',unitId = '/1024858/Box';var thisSlot = googletag.defineSlot(unitId, [300, 250], slotId).addService(googletag.pubads());googletag.pubads().addEventListener('slotRenderEnded', function(event) {if (event.slot === thisSlot) {$('#'+slotId).removeClass('hidden');}});googletag.display(slotId);});<\/script></div>";

                if (deployInBetween) {
                    $('section.article-details').children("p:nth-child(" + nthP + ")").after(adCode);
                } else {
                    $('section.article-details').children("p:last").after(adCode);
                }

                adDeployed = true;
                return false;
              }
            }
          });

          if (adDeployed)
            return false;

        });
      });
    </script>

<?php
$short_hero = get_field("hero_type");

if ($short_hero == 'Fixed') {
  echo '<script>$("#article-hero-full").addClass("hero-short");</script>';
}

$h1_title = get_field("title_aligment");
$h1_vtitle = get_field("v_alignment");

if ($h1_title == 'Left') {
  echo '<script>$("#single-hero").addClass("left");</script>';
}
if ($h1_title == 'Center') {
  echo '<script>$("#single-hero").addClass("center");</script>';
}
if ($h1_title == 'Right') {
  echo '<script>$("#single-hero").addClass("right");</script>';
}
if ($h1_vtitle == 'Top') {
  echo '<script>$("#single-hero").addClass("top");</script>';
}
if ($h1_vtitle == 'Middle') {
  echo '<script>$("#single-hero").addClass("middle");</script>';
}
if ($h1_vtitle == 'Bottom') {
  echo '<script>$("#single-hero").addClass("bottom");</script>';
}
?>
<?php if (get_field('hide_title')): ?>
    <script>$("#single-hero, #article-headline").addClass("hidden");</script>
<?php endif; ?>

<?php if (get_field('hero_gradient')): ?>
    <script>$("#article-hero-full").addClass("gradient");</script>
<?php endif; ?>

    <script>
      function SetCookie(c_name, value, expiredays) {
        var exdate = new Date()
        exdate.setDate(exdate.getDate() + expiredays)
        document.cookie = c_name + "=" + escape(value) +
          ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
        location.reload()
      }


      // $('.img-cover').each(function () {
      //   var div1 = $(this).children(".wp-caption").children("img").attr('src');
      //   var div2 = $(this).children("p").children("img").attr('src');

      //   if (div1) {
      //     coverSrc = div1;
      //     $(this).css("margin-bottom", "70px");
      //   }
      //   else
      //     coverSrc = div2;

      //   $(this).css("background", "url(" + coverSrc + ")");
      //   $(this).children("p").remove();
      //   $(this).children(".wp-caption").children("img").remove();
      //   $(this).children(".wp-caption").css("padding-top", "100vh");


      // });

    </script>

<?php get_footer(); */?>
