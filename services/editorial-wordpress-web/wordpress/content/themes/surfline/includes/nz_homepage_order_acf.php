<?php
if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(array(
    'key' => 'group_1aa56llusuj8f',
    'title' => 'New Zealand Homepage Order',
    'fields' => array(
      array(
        'key' => 'field_7yh3gwgto4te5',
        'label' => 'nz_homepage_order',
        'name' => 'nz_homepage_order',
        'type' => 'relationship',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'post_type' => array(
          0 => 'post',
        ),
        'taxonomy' => array(
          0 => 'promotion:nz-homepage-promo',
        ),
        'filters' => array(
          0 => 'search',
        ),
        'elements' => '',
        'min' => '',
        'max' => 10,
        'return_format' => 'object',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'sl_nz_homepage_order',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
  ));

endif;
