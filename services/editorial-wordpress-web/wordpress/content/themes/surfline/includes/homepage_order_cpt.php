<?php
// Add Storm Feed Default Homepage Order custom post type
function sl_us_homepage_order_cpt() {
  register_post_type('sl_us_homepage_order',
    array(
      'labels' => array(
        'name' => 'Default Homepage Order',
        'singular_name' => 'Default Homepage Order',
        'add_new' => 'Add New Default Homepage Order',
        'add_new_item' => 'Add New Default Homepage Order',
        'edit' => 'Edit',
        'edit_item' => 'Edit Default Homepage Order',
        'new_item' => 'New Default Homepage Order',
        'view' => 'View',
        'view_item' => 'View Default Homepage Order',
        'search_items' => 'Search Default Homepage Order',
        'not_found' => 'No Default Homepage Order found',
        'not_found_in_trash' => 'No Default Homepage Order found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 14,
      'hierarchical' => false,
      'supports' => array('title', 'thumbnail'),
      'menu_icon'  => 'dashicons-sort',
      'rewrite' => [ 'slug' => 'homepage-order' ],
      'has_archive' => true,
      'show_in_rest' => true,
      'query_var' => true
    )
  );
}
add_action('init', 'sl_us_homepage_order_cpt');
