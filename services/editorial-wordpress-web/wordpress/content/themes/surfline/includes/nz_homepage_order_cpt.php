<?php
// Add Storm Feed New Zealand Homepage Order custom post type
function sl_nz_homepage_order_cpt() {
  register_post_type('sl_nz_homepage_order',
    array(
      'labels' => array(
        'name' => 'New Zealand Homepage Order',
        'singular_name' => 'New Zealand Homepage Order',
        'add_new' => 'Add New New Zealand Homepage Order',
        'add_new_item' => 'Add New New Zealand Homepage Order',
        'edit' => 'Edit',
        'edit_item' => 'Edit New Zealand Homepage Order',
        'new_item' => 'New New Zealand Homepage Order',
        'view' => 'View',
        'view_item' => 'View New Zealand Homepage Order',
        'search_items' => 'Search New Zealand Homepage Order',
        'not_found' => 'No New Zealand Homepage Order found',
        'not_found_in_trash' => 'No New Zealand Homepage Order found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 14,
      'hierarchical' => false,
      'supports' => array('title', 'thumbnail'),
      'menu_icon'  => 'dashicons-sort',
      'rewrite' => [ 'slug' => 'nz-homepage-order' ],
      'has_archive' => true,
      'show_in_rest' => true,
      'query_var' => true
    )
  );
}
add_action('init', 'sl_nz_homepage_order_cpt');
