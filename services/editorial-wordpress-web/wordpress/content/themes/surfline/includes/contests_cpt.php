<?php
// Add contests custom post type
function sl_contests_cpt() {
  register_post_type('sl_contests',
    array(
      'labels' => array(
        'name' => 'Contests',
        'singular_name' => 'Contest',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Contest',
        'edit' => 'Edit',
        'edit_item' => 'Edit Contest',
        'new_item' => 'New Contest',
        'view' => 'View',
        'view_item' => 'View Contest',
        'search_items' => 'Search Contests',
        'not_found' => 'No Contests found',
        'not_found_in_trash' => 'No Contest found in Trash'
      ),
      'public' => false,
      'publicly_queryable' => false,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'show_in_rest' => true,
      'menu_position' => 16,
      'hierarchical' => false,
      'supports' => array('title'),
      'menu_icon'  => 'dashicons-palmtree',
      'rewrite' => [ 'slug' => 'entries' ],
      'has_archive' => true,
      'show_in_rest' => true,
      'query_var' => true
    )
  );
}
add_action('init', 'sl_contests_cpt');

// Add contest entries custom post type
function sl_contest_entries_cpt() {
  register_post_type('sl_contest_entries',
    array(
      'labels' => array(
        'name' => 'Contest Entries',
        'singular_name' => 'Contest Entry',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Contest Entry',
        'edit' => 'Edit',
        'edit_item' => 'Edit Contest Entry',
        'new_item' => 'New Contest Entry',
        'view' => 'View',
        'view_item' => 'View Contest Entry',
        'search_items' => 'Search Contest Entries',
        'not_found' => 'No Contest Entries found',
        'not_found_in_trash' => 'No Contest Entry found in Trash'
      ),
      'public' => false,
      'publicly_queryable' => false,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'show_in_rest' => true,
      'menu_position' => 17,
      'hierarchical' => false,
      'supports' => array('title'),
      'menu_icon'  => 'dashicons-palmtree',
      'rewrite' => [ 'slug' => 'entries' ],
      'has_archive' => true,
      'show_in_rest' => true,
      'query_var' => true
    )
  );
}
add_action('init', 'sl_contest_entries_cpt');
add_filter('months_dropdown_results', '__return_empty_array');

// Add extra dropdown filters to the Contest Entry dashboard
function contest_entry_filter_query($post_type, $filter_by){
  global $wpdb;
  /** Grab the results from the DB */
  $query = $wpdb->prepare('
      SELECT DISTINCT pm.meta_value FROM %1$s pm
      LEFT JOIN %2$s p ON p.ID = pm.post_id
      WHERE pm.meta_key = "%3$s"
      AND p.post_type = "%4$s"
      ORDER BY "%3$s"',
      $wpdb->postmeta,
      $wpdb->posts,
      $filter_by,
      $post_type
  );
  return $wpdb->get_col($query);
}

function is_contest_edit_page($contest_filter_id){
  global $pagenow;
  $current_page = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';
  return is_admin() &&
    $current_page =='sl_contest_entries'  &&
    $pagenow == 'edit.php' &&
    isset( $_GET[$contest_filter_id] ) &&
    $_GET[$contest_filter_id] != -1 &&
    $_GET[$contest_filter_id] != null;
 }

function add_contest_entry_region_filter($post_type){
    /** Ensure this is the correct Post Type*/
    if($post_type !== 'sl_contest_entries')
        return;

    /** Ensure there are options to show */
    $results = contest_entry_filter_query($post_type, 'region');
    if(empty($results))
        return;

    /** Grab all of the options that should be shown */
    $options[] = sprintf('<option value="-1">%1$s</option>', __('All Contest Regions', 'your-text-domain'));
    $current_selected = $_GET['sl-contest-regions-select'];
    foreach($results as $result) :
        $region_obj = get_term_by('id', $result, 'sl_contest_regions');
        $selected = $region_obj->term_id == $current_selected ? "selected" : "no";
        if($region_obj->term_id) {
          $options[] = sprintf(
            '<option value="%1$s" %3$s>%2$s</option>',
            $region_obj->term_id, $region_obj->name, $selected
          );
        }
    endforeach;

    /** Output the dropdown menu */
    echo '<select class="" id="sl-contest-regions-select" name="sl-contest-regions-select">';
    echo join("\n", $options);
    echo '</select>';
}
add_action('restrict_manage_posts', 'add_contest_entry_region_filter');

function  parse_contest_entry_region_filter($query) {
   if (is_contest_edit_page('sl-contest-regions-select')) {
    $region = $_GET['sl-contest-regions-select'];
    $query->query_vars['meta_key'] = 'region';
    $query->query_vars['meta_value'] = $region;
    $query->query_vars['meta_compare'] = '=';
  }
}
add_filter( 'parse_query', 'parse_contest_entry_region_filter' );

function add_contest_entry_month_filter($post_type){
    /** Ensure this is the correct Post Type*/
    if($post_type !== 'sl_contest_entries')
        return;

    /** Ensure there are options to show */
    $results = contest_entry_filter_query($post_type, 'date');
    if(empty($results))
        return;

    /** Grab all of the options that should be shown */
    $options[] = sprintf('<option value="-1">%1$s</option>', __('All Contest Months', 'your-text-domain'));
    $current_selected = $_GET['sl-contest-month-select'];
    $unique_options = [];
    foreach($results as $date) :
        $month = date('F', strtotime($date));
        $selected = ($current_selected != -1 && $month == date('F', strtotime($current_selected))) ? "selected" : "";
        if(!in_array($month, $unique_options)) {
          $options[] = sprintf(
            '<option value="%1$s" %3$s>%2$s</option>',
            $date, $month, $selected
          );
          array_push($unique_options, $month);
        }
    endforeach;

    /** Output the dropdown menu */
    echo '<select class="" id="sl-contest-month-select" name="sl-contest-month-select">';
    echo join("\n", $options);
    echo '</select>';
}
add_action('restrict_manage_posts', 'add_contest_entry_month_filter');

function  parse_contest_entry_month_filter($query) {
   if (is_contest_edit_page('sl-contest-month-select')) {
    $date = $_GET['sl-contest-month-select'];

    /** Set start and end dates to get all possible dates in selected month */
    $start_date = substr_replace($date, '01', 6, 2);
    $end_date = substr_replace($date, '31', 6, 2);

    /** Find all posts for the selected month */
    $query->query_vars['meta_query'] = array(
      'post_type' => 'sl_contest_entries',
  		array(
  			'key'     => 'date',
  			'value'   => array($start_date, $end_date),
        'compare' => 'BETWEEN',
        'type'   => 'NUMERIC',
  		),
    );
  }
  return $query;
}
add_filter( 'parse_query', 'parse_contest_entry_month_filter' );
