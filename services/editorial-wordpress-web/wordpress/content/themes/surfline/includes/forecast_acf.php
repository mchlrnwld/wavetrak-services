<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5cc8a309d0484',
	'title' => 'Premium Forecast',
	'fields' => array(
		array(
			'key' => 'field_5cc8a317fc7cf',
			'label' => 'Premium',
			'name' => 'premium',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
	),
	'location' => array(
		array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_premium_analysis',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_realtime_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_seasonal_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_local_news',
			),
        ),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_58d3614e9aeab',
    'title' => 'Location',
    'fields' => array(
        array(
            'key' => 'field_5904253899aef',
            'label' => 'Location Targeting',
            'name' => 'dynlocation',
            'type' => 'select',
            'instructions' => 'Select spots/regions/subregions/cities/countries/planets to target',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'multiple' => 1,
            'allow_null' => 0,
            'choices' => array(
            ),
            'default_value' => array(
            ),
            'ui' => 1,
            'ajax' => 1,
            'placeholder' => '',
            'return_format' => 'array',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_premium_analysis',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_realtime_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_seasonal_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_local_news',
			),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

/*
Show in Feed appears to be deprecated, removing for now.

acf_add_local_field_group(array(
    'key' => 'group_5cc8a72f6ba96',
    'title' => 'Display Options',
    'fields' => array(
        array(
            'key' => 'field_5cc8a82d7e9fb',
            'label' => 'Show in Feed',
            'name' => 'show_in_feed',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 1,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_premium_analysis',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_realtime_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_seasonal_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_local_news',
			),
        ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));
*/

acf_add_local_field_group(array(
    'key' => 'group_5cef08d1e832d',
    'title' => 'Swell Event',
    'fields' => array(
        array(
            'key' => 'field_5cef08d1ef9c8',
            'label' => 'Swell Event',
            'name' => 'swell_event',
            'type' => 'true_false',
            'instructions' => 'Is this Forecast part of a Swell Event?',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_5cef08d1efb47',
            'label' => 'Parent Swell Event',
            'name' => 'parent_swell_event',
            'type' => 'relationship',
            'instructions' => 'Pick which Swell Event(s) this Forecast should show up under',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_5cef08d1ef9c8',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'post_type' => array(
                0 => 'sl_swell_event',
            ),
            'taxonomy' => '',
            'filters' => array(
                0 => 'search',
            ),
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'id',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_premium_analysis',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_realtime_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_seasonal_forecast',
            ),
        ),
        array(
            array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_local_news',
			),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;
