<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_58d3614e9aeba',
    'title' => 'Location',
    'fields' => array(
      array(
        'key' => 'field_hdgcbofpn8atl',
        'label' => 'Location',
        'name' => 'all_locations',
        'type' => 'radio',
        'instructions' => 'Select Target Locations or Target All Locations',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'choices' => array(
          'Select Target Locations' => 'Select Target Locations',
          'Target All locations' => 'Target All locations',
        ),
        'allow_null' => 0,
        'other_choice' => 0,
        'default_value' => 'Swell Event',
        'layout' => 'horizontal',
        'return_format' => 'value',
        'save_other_choice' => 0,
      ),
      array(
        'key' => 'field_5904353899aef',
        'label' => 'Location Targeting',
        'name' => 'dynlocation',
        'type' => 'select',
        'instructions' => 'Select spots/regions/subregions/cities/countries/planets to target',
        'required' => 1,
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_hdgcbofpn8atl',
              'operator' => '==',
              'value' => 'Select Target Locations',
            ),
          ),
        ),
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
        ),
        'multiple' => 1,
        'allow_null' => 0,
        'choices' => array(
        ),
        'choices' => array(
          'Target locations' => 'Target locations',
          'Target all locations' => 'Target all locations',
        ),
        'default_value' => array(
        ),
        'ui' => 1,
        'ajax' => 1,
        'placeholder' => '',
        'return_format' => 'value',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'sl_swell_event',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => 'Location',
  ));

acf_add_local_field_group(array(
    'key' => 'group_5cc9e8a1d629b',
    'title' => 'Event Summary',
    'fields' => array(
        array(
            'key' => 'field_5cc9e8a1dc128',
            'label' => 'Summary',
            'name' => 'summary',
            'type' => 'text',
            'instructions' => '100 characters max',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'maxlength' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => 100,
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'seamless',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5cd466689a64a',
    'title' => 'Status',
    'fields' => array(
        array(
            'key' => 'field_5cd4666cc42c0',
            'label' => 'Active',
            'name' => 'active',
            'type' => 'true_false',
            'instructions' => 'Is this swell event currently active?',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_z223c6loui5hw',
            'label' => 'Live',
            'name' => 'live',
            'type' => 'true_false',
            'instructions' => 'Is this swell event currently live?',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5cca099c92921',
    'title' => 'Sponsorship',
    'fields' => array(
        array(
            'key' => 'field_5cca09aa084c0',
            'label' => 'DFP Keyword',
            'name' => 'dfp_keyword',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 1,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5cd0a2bd1ed7c',
    'title' => 'Basin',
    'fields' => array(
        array(
            'key' => 'field_5cd0a2c589bfc',
            'label' => 'Basin',
            'name' => 'basin',
            'type' => 'checkbox',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array(
                'north_atlantic' => 'North Atlantic',
                'south_atlantic' => 'South Atlantic',
                'north_pacific' => 'North Pacific',
                'south_pacific' => 'South Pacific',
                'indian' => 'Indian',
                'mediterranean' => 'Mediterranean',
                'great_lakes' => 'Great Lakes',
            ),
            'allow_custom' => 0,
            'default_value' => array(
            ),
            'layout' => 'vertical',
            'toggle' => 0,
            'return_format' => 'value',
            'save_custom' => 0,
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5cd0b1c1979dd',
    'title' => 'Featured Video',
    'fields' => array(
        array(
            'key' => 'field_5cd0b1cbad39d',
            'label' => 'JW Player Video ID',
            'name' => 'jw_player_video_id',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5d66b3a96e7dc',
    'title' => 'Title Tag',
    'fields' => array(
        array(
            'key' => 'field_5d66b3b805687',
            'label' => 'Title Tag',
            'name' => 'alert_title_tag',
            'type' => 'radio',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array(
              'Swell Event' => 'Swell Event',
              '20ft+' => '20ft+'
            ),
            'allow_null' => 0,
            'other_choice' => 0,
            'default_value' => 'Swell Event',
            'layout' => 'vertical',
            'return_format' => 'value',
            'save_other_choice' => 0,
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_6179b8f5cc126',
	'title' => 'Swell Event URL',
	'fields' => array(
		array(
			'key' => 'field_6179b938ae42d',
			'label' => 'Swell Event URL',
			'name' => 'swell_event_url',
			'type' => 'radio',
			'instructions' => 'Select Custom URL to set a custom link for this event\'s Purple Bar',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'Default URL' => 'Default URL',
				'Custom URL' => 'Custom URL',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'Swell Event',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_6179b9e0ae42e',
			'label' => 'Custom Swell Event URL',
			'name' => 'custom_swell_event_url',
			'type' => 'url',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_6179b938ae42d',
						'operator' => '==',
						'value' => 'Custom URL',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array(
			'key' => 'field_6179c646c54fb',
			'label' => 'Open URL in New Window',
			'name' => 'swell_event_new_window',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_6179b938ae42d',
						'operator' => '==',
						'value' => 'Custom URL',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Open URL in new window?',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sl_swell_event',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => 'Custom Link for Purple Bar',
));

acf_add_local_field_group(array(
    'key' => 'group_61b7816bc7a7b',
    'title' => 'Swell Event Post Order',
    'fields' => array(
        array(
            'key' => 'field_61ba17569146f',
            'label' => 'Custom Swell Event Post Order',
            'name' => 'custom_swell_event_post_order',
            'type' => 'true_false',
            'instructions' => 'Select to manually curate and sort pinned posts to the top of the Swell Event feed',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Enable Custom Swell Event Post Order?',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_61b7816bd0b07',
            'label' => 'Swell Event Post Order',
            'name' => 'swell_event_post_order',
            'type' => 'relationship',
            'instructions' => 'Search posts from the left column and add to the right column to pin to top of Swell Event. Drag and drop in right column to set order (top is first)',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_61ba17569146f',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'post_type' => '',
            'taxonomy' => '',
            'filters' => array(
                0 => 'search',
                1 => 'post_type',
                2 => 'taxonomy',
            ),
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'object',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'sl_swell_event',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;
