<?php
// Add Storm Feed Events custom post type
function sl_swell_events_cpt() {
  register_post_type('sl_swell_event',
    array(
      'labels' => array(
        'name' => 'Swell Events',
        'singular_name' => 'Swell Event',
        'add_new' => 'Add New Swell Event',
        'add_new_item' => 'Add New Swell Event',
        'edit' => 'Edit',
        'edit_item' => 'Edit Swell Event',
        'new_item' => 'New Swell Event',
        'view' => 'View',
        'view_item' => 'View Swell Event',
        'search_items' => 'Search Swell Events',
        'not_found' => 'No Swell Events found',
        'not_found_in_trash' => 'No Swell Event found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 14,
      'hierarchical' => false,
      'supports' => array('title', 'thumbnail'),
      'menu_icon'  => 'dashicons-location-alt',
      'rewrite' => [ 'slug' => 'swell-alert' ],
      'has_archive' => true,
      'show_in_rest' => true,
      'query_var' => true
    )
  );
}
add_action('init', 'sl_swell_events_cpt');
