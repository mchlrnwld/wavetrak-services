<?php
// Add contest surfers taxonomy
function create_sl_contest_surfers_taxonomy() {
	$labels = array(
		'name'              => _x( 'Surfers', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Surfer', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Surfers', 'textdomain' ),
		'all_items'         => __( 'All Surfers', 'textdomain' ),
		'parent_item'       => __( 'Parent Surfers', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Surfer:', 'textdomain' ),
		'edit_item'         => __( 'Edit Surfer', 'textdomain' ),
		'update_item'       => __( 'Update Surfer', 'textdomain' ),
		'add_new_item'      => __( 'Add New Surfer', 'textdomain' ),
		'new_item_name'     => __( 'New Surfer', 'textdomain' ),
		'menu_name'         => __( 'Surfers', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'contest-surfers' ),
    'show_in_rest' => true
	);
	register_taxonomy( 'sl_contest_surfers', array( '' ), $args );
}
add_action( 'init', 'create_sl_contest_surfers_taxonomy', 0 );

// Add contest photographers taxonomy
function create_sl_contest_photographers_taxonomy() {
	$labels = array(
		'name'              => _x( 'Photographers', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Photographer', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Photographers', 'textdomain' ),
		'all_items'         => __( 'All Photographers', 'textdomain' ),
		'parent_item'       => __( 'Parent Photographers', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Photographer:', 'textdomain' ),
		'edit_item'         => __( 'Edit Photographer', 'textdomain' ),
		'update_item'       => __( 'Update Photographer', 'textdomain' ),
		'add_new_item'      => __( 'Add New Photographer', 'textdomain' ),
		'new_item_name'     => __( 'New Photographer', 'textdomain' ),
		'menu_name'         => __( 'Photographers', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'contest-photographers' ),
    'show_in_rest' => true
	);
	register_taxonomy( 'sl_contest_photographers', array( '' ), $args );
}
add_action( 'init', 'create_sl_contest_photographers_taxonomy', 0 );

// Add contest contests taxonomy
function create_sl_contest_contests_taxonomy() {
	$labels = array(
		'name'              => _x( 'Contests', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Contest', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Contests', 'textdomain' ),
		'all_items'         => __( 'All Contests', 'textdomain' ),
		'parent_item'       => __( 'Parent Contests', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Contest:', 'textdomain' ),
		'edit_item'         => __( 'Edit Contest', 'textdomain' ),
		'update_item'       => __( 'Update Contest', 'textdomain' ),
		'add_new_item'      => __( 'Add New Contest', 'textdomain' ),
		'new_item_name'     => __( 'New Contest', 'textdomain' ),
		'menu_name'         => __( 'Contests', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'contest-contests' ),
    'show_in_rest' => true
	);
	register_taxonomy( 'sl_contest_contests', array( 'post' ), $args );
}
add_action( 'init', 'create_sl_contest_contests_taxonomy', 0 );

// Add contest regions taxonomy
function create_sl_contest_regions_taxonomy() {
	$labels = array(
		'name'              => _x( 'Contest Regions', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Region', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Regions', 'textdomain' ),
		'all_items'         => __( 'All Regions', 'textdomain' ),
		'parent_item'       => __( 'Parent Regions', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Region:', 'textdomain' ),
		'edit_item'         => __( 'Edit Region', 'textdomain' ),
		'update_item'       => __( 'Update Region', 'textdomain' ),
		'add_new_item'      => __( 'Add New Region', 'textdomain' ),
		'new_item_name'     => __( 'New Region', 'textdomain' ),
		'menu_name'         => __( 'Contest Regions', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'contest-regions' ),
    'show_in_rest' => true
	);
	register_taxonomy( 'sl_contest_regions', array( 'post', 'page' ), $args );
}
add_action( 'init', 'create_sl_contest_regions_taxonomy', 0 );

// Add contest spots taxonomy
function create_sl_contest_spots_taxonomy() {
	$labels = array(
		'name'              => _x( 'Spots', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Spot', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Spots', 'textdomain' ),
		'all_items'         => __( 'All Spots', 'textdomain' ),
		'parent_item'       => __( 'Parent Spots', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Spot:', 'textdomain' ),
		'edit_item'         => __( 'Edit Spot', 'textdomain' ),
		'update_item'       => __( 'Update Spot', 'textdomain' ),
		'add_new_item'      => __( 'Add New Spot', 'textdomain' ),
		'new_item_name'     => __( 'New Spot', 'textdomain' ),
		'menu_name'         => __( 'Spots', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'contest-spots' ),
    'show_in_rest' => true
	);
	register_taxonomy( 'sl_contest_spots', array( '' ), $args );
}
add_action( 'init', 'create_sl_contest_spots_taxonomy', 0 );

// Add contest periods taxonomy
function create_sl_contest_periods_taxonomy() {
	$labels = array(
		'name'              => _x( 'Periods', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Period', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Periods', 'textdomain' ),
		'all_items'         => __( 'All Periods', 'textdomain' ),
		'parent_item'       => __( 'Parent Periods', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Period:', 'textdomain' ),
		'edit_item'         => __( 'Edit Period', 'textdomain' ),
		'update_item'       => __( 'Update Period', 'textdomain' ),
		'add_new_item'      => __( 'Add New Period', 'textdomain' ),
		'new_item_name'     => __( 'New Period', 'textdomain' ),
		'menu_name'         => __( 'Periods', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'contest-periods' ),
    'show_in_rest' => true
	);
	register_taxonomy( 'sl_contest_periods', array( 'post', 'page' ), $args );
}
add_action( 'init', 'create_sl_contest_periods_taxonomy', 0 );
