<?php
// Add Storm Feed Australia Homepage Order custom post type
function sl_au_homepage_order_cpt() {
  register_post_type('sl_au_homepage_order',
    array(
      'labels' => array(
        'name' => 'Australia Homepage Order',
        'singular_name' => 'Australia Homepage Order',
        'add_new' => 'Add New Australia Homepage Order',
        'add_new_item' => 'Add New Australia Homepage Order',
        'edit' => 'Edit',
        'edit_item' => 'Edit Australia Homepage Order',
        'new_item' => 'New Australia Homepage Order',
        'view' => 'View',
        'view_item' => 'View Australia Homepage Order',
        'search_items' => 'Search Australia Homepage Order',
        'not_found' => 'No Australia Homepage Order found',
        'not_found_in_trash' => 'No Australia Homepage Order found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 14,
      'hierarchical' => false,
      'supports' => array('title', 'thumbnail'),
      'menu_icon'  => 'dashicons-sort',
      'rewrite' => [ 'slug' => 'au-homepage-order' ],
      'has_archive' => true,
      'show_in_rest' => true,
      'query_var' => true
    )
  );
}
add_action('init', 'sl_au_homepage_order_cpt');
