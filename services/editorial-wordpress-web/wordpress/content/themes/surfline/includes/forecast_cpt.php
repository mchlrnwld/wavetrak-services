<?php
// Add Premium Analysis custom post type
function sl_premium_analysis_cpt() {
  register_post_type('sl_premium_analysis',
    array(
      'labels' => array(
        'name' => 'Premium Analysis',
        'singular_name' => 'Premium Analysis',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Premium Analysis',
        'edit' => 'Edit',
        'edit_item' => 'Edit Premium Analysis',
        'new_item' => 'New Premium Analysis',
        'view' => 'View',
        'view_item' => 'View Premium Analysis',
        'search_items' => 'Search Premium Analyses',
        'not_found' => 'No Premium Analyses found',
        'not_found_in_trash' => 'No Premium Analyses found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 10,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'author'),
      'menu_icon'  => 'dashicons-admin-site',
      'has_archive' => true,
      'show_in_rest' => true,
      'rest_base' => 'sl_premium_analysis',
      'query_var' => true
    )
  );
}
add_action('init', 'sl_premium_analysis_cpt');

// Add Seasonal Forecast custom post type
function sl_seasonal_forecast_cpt() {
  register_post_type('sl_seasonal_forecast',
    array(
      'labels' => array(
        'name' => 'Seasonal Forecast',
        'singular_name' => 'Seasonal Forecast',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Seasonal Forecast',
        'edit' => 'Edit',
        'edit_item' => 'Edit Seasonal Forecast',
        'new_item' => 'New Seasonal Forecast',
        'view' => 'View',
        'view_item' => 'View Seasonal Forecast',
        'search_items' => 'Search Seasonal Forecast',
        'not_found' => 'No Seasonal Forecast found',
        'not_found_in_trash' => 'No Seasonal Forecast found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 11,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'author'),
      'menu_icon'  => 'dashicons-admin-site',
      'has_archive' => true,
      'show_in_rest' => true,
      'rest_base' => 'sl_seasonal_forecast',
      'query_var' => true
    )
  );
}
add_action('init', 'sl_seasonal_forecast_cpt');

// Add Realtime Forecast custom post type
function sl_realtime_forecast_cpt() {
  register_post_type('sl_realtime_forecast',
    array(
      'labels' => array(
        'name' => 'Realtime Forecast',
        'singular_name' => 'Realtime Forecast',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Realtime Forecast',
        'edit' => 'Edit',
        'edit_item' => 'Edit Realtime Forecast',
        'new_item' => 'New Realtime Forecast',
        'view' => 'View',
        'view_item' => 'View Realtime Forecast',
        'search_items' => 'Search Realtime Forecast',
        'not_found' => 'No Realtime Forecast found',
        'not_found_in_trash' => 'No Realtime Forecast found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 12,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'author'),
      'menu_icon'  => 'dashicons-admin-site',
      'has_archive' => true,
      'show_in_rest' => true,
      'rest_base' => 'sl_realtime_forecast',
      'query_var' => true
    )
  );
}
add_action('init', 'sl_realtime_forecast_cpt');

// Add Local News custom post type
function sl_local_news_cpt() {
  register_post_type('sl_local_news',
    array(
      'labels' => array(
        'name' => 'Local News',
        'singular_name' => 'Local News',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Local News',
        'edit' => 'Edit',
        'edit_item' => 'Edit Local News',
        'new_item' => 'New Local News',
        'view' => 'View',
        'view_item' => 'View Local News',
        'search_items' => 'Search Local News',
        'not_found' => 'No Local News found',
        'not_found_in_trash' => 'No Local News found in Trash'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'menu_position' => 13,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'author', 'thumbnail'),
      'menu_icon'  => 'dashicons-admin-site',
      'has_archive' => true,
      'show_in_rest' => true,
      'rest_base' => 'sl_local_news',
      'query_var' => true,
      'taxonomies' => array('post_tag')
    )
  );
}
add_action('init', 'sl_local_news_cpt');