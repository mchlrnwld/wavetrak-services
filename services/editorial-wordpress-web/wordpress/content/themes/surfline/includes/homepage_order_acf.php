<?php
if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(array(
    'key' => 'group_6064b47586c75',
    'title' => 'Default Homepage Order',
    'fields' => array(
      array(
        'key' => 'field_6064b4ac609e7',
        'label' => 'us_homepage_order',
        'name' => 'us_homepage_order',
        'type' => 'relationship',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'post_type' => array(
          0 => 'post',
        ),
        'taxonomy' => array(
          0 => 'promotion:homepage-promo',
        ),
        'filters' => array(
          0 => 'search',
        ),
        'elements' => '',
        'min' => '',
        'max' => 10,
        'return_format' => 'object',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'sl_us_homepage_order',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
  ));

endif;
