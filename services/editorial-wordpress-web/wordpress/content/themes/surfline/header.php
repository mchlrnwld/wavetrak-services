<?php
if (isset($_GET["qaFlag"])) {
  define("QAFLAG", filter_var($_GET["qaFlag"], FILTER_SANITIZE_STRING));
} else {
  define("QAFLAG", false);
}

$backplane = getenv("WEB_BACKPLANE") ? getenv("WEB_BACKPLANE") : "http://web-backplane.sandbox.surfline.com";
$forcedMVP = $_SERVER['HTTP_X_SURFLINE_MVP_FORCED'] ? '&renderOptions[mvpForced]=true' : '';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $backplane. "/render.json?bundle=margins&skipFetch=true&renderOptions[hideCTA]=true" . $forcedMVP);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$margins = curl_exec($ch);
curl_close($ch);
$margins = json_decode($margins, true);

$GLOBALS["header"] = $margins["data"]["components"]["header"];
$GLOBALS["footer"] = $margins["data"]["components"]["footer"];

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta property="fb:app_id" content="218714858155230"/>
    <meta property="fb:page_id" content="255110695512" />
    <link rel="publisher" href="https://plus.google.com/+Surfline" />
    <meta property="og:site_name" content="Surfline"/>
    <link href="https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png" rel="apple-touch-icon">
    <link href="/favicon.ico" rel="icon">

    <?php
      if (!isset($_GET['native'])) {
        $requestUri = preg_split('/\//', $_SERVER['REQUEST_URI'], -1, PREG_SPLIT_NO_EMPTY);
        $maybeId = array_pop($requestUri);
        $maybeType = array_pop($requestUri);

        $appArgument = ",app-argument=surfline://feed";
        if (is_numeric($maybeId)
          || $maybeType == 'category'
          || $maybeType == 'tag'
          || $maybeType == 'series'
        ) {
          $appArgument = ",app-argument=surfline://web?url=" . urlencode("https://new.surfline.com" . $_SERVER['REQUEST_URI']);
        }

        echo '<meta name="apple-itunes-app" content="app-id=393782096,affiliate-data=at=1000lb9Z&ct=surfline-website-smart-banner&pt=261378' . $appArgument . '"">';
        echo '<link rel="manifest" href="/manifest.json">';
      }
    ?>

    <?php wp_head(); ?>

    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">
    <script src="//use.fontawesome.com/8f4099a2d9.js"></script>
    <script src="<?php echo $margins["associated"]["vendor"]["js"] ?>" defer></script>

    <link rel="stylesheet" href="<?php echo $margins["data"]["css"] ?>">
    <script src="<?php echo $margins["data"]["js"] ?>" defer></script>

    <script>
      function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      var nativePlatform = getParameterByName("native");

      // Force true to mean iOS since current iOS version does this. Future versions pass platform identifier.
      if (nativePlatform === "true") {
        nativePlatform = "ios"
      }

      window.nativeApp = (nativePlatform === "ios" || nativePlatform === "android") ? nativePlatform : false;
    </script>

    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      googletag.cmd.push(function () {
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs(true); //enable this in live

        var qaFlag = getParameterByName("qaFlag");
        if (qaFlag) {
          googletag.pubads().setTargeting("qaFlag", qaFlag);
        }

        <?php if (is_front_page() || is_page("all")) { ?>

        googletag.pubads().setTargeting("viewType", "CONTENT_HOME");
        <?php } elseif (is_category()) { ?>
        googletag.pubads().setTargeting("viewType", "CONTENT_CATEGORY");
        googletag.pubads().setTargeting("contentCategoryId", "<?php echo $catid = get_queried_object_id(); ?>");

        <?php } elseif (is_tag()) { ?>
        googletag.pubads().setTargeting("viewType", "CONTENT_TAG");
        googletag.pubads().setTargeting("contentTagId", "<?php echo $tagid = get_queried_object_id(); ?>");

        <?php } elseif (is_tax()) { ?>
        googletag.pubads().setTargeting("viewType", "CONTENT_SERIES");
        googletag.pubads().setTargeting("contentSeriesId", "<?php echo $taxid = get_queried_object_id(); ?>");

        <?php } elseif (is_single()) { ?>
        googletag.pubads().setTargeting("viewType", "CONTENT_ARTICLE");

        var categoryList = [];
        <?php foreach(wp_get_post_categories(get_queried_object_id()) as $postCat){ ?>
        categoryList.push("<?php echo $postCat ?>");
        <?php } ?>

        googletag.pubads().setTargeting("contentCategoryId", categoryList);

        var tagList = [];
        <?php foreach(wp_get_post_tags(get_queried_object_id()) as $postTag){ ?>
        tagList.push("<?php echo $postTag->term_id ?>");
        <?php } ?>

        googletag.pubads().setTargeting("contentTagId", tagList);

        var seriesList = [];
        <?php foreach(wp_get_post_terms(get_queried_object_id(), "series") as $postTax){ ?>
        seriesList.push("<?php echo $postTax->term_id ?>");
        <?php } ?>

        googletag.pubads().setTargeting("contentSeriesId", seriesList);

        googletag.pubads().setTargeting("contentKeyword", "<?php echo $customKeyword = get_post_meta(get_queried_object_id(), "dfp_keyword", true);?>");

        <?php } ?>

      });
    </script>

    <style type="text/css">
        .css-ueuzok .css-et0a3j,
        .css-ueuzok .css-c7nl0w,
        .css-ueuzok .css-1g3391b,
        .css-ueuzok .css-pvnp1j {
            width: auto;
            text-transform: unset;
            letter-spacing: unset;
        }
        .css-ueuzok .css-et0a3j:hover{
          background-color: unset;
          border: unset;
        }
        .css-ueuzok .css-c7nl0w:hover{
          background-color: unset;
          border-top: unset;
          background-image: linear-gradient(to top, rgba(67, 90, 111, 0.06), rgba(67, 90, 111, 0.024));
          box-shadow: rgb(67 90 111 / 26%) 0px 0px 0px 1px inset, rgb(67 90 111 / 11%) 0px -1px 1px 0px inset;
          text-transform: unset;
        }
        .css-ueuzok .css-1g3391b:hover{
          background-color: unset;
          border-top: unset;
          background-image: linear-gradient(to top, rgb(55, 165, 109), rgb(63, 175, 119));
          text-transform: unset;
        }
        .css-ueuzok .css-pvnp1j:hover{
          background-color: unset;
          border-top: unset;
        }
        .css-ueuzok .css-19720g2 label{
          text-transform: unset;
          font-weight: unset;
        }
        .css-ueuzok .css-1er9f2w{
          letter-spacing: unset;
        }
    </style>
    <script type="text/javascript">
      window.consentManagerConfig = function(exports) {
        var React = exports.React
        var bannerContent = React.createElement(
          'span',
          null,
          'Use this form to opt out of the sale of your data as per Surfline \\ Wavetrak Inc.’s privacy policy and applicable laws. Further information about Surfline’s privacy practices is available here:',
          ' ',
          React.createElement(
            'a',
            { href: 'https://www.surfline.com/privacy-policy', target: '_blank' },
            'https://www.surfline.com/privacy-policy'
          ),
          '.'
        )
        var bannerSubContent = 'You can change your preferences at any time.'
        var preferencesDialogTitle = 'Right to Opt-Out Form'
        var preferencesDialogContent = React.createElement(
          'span',
          null,
          React.createElement(
            'p',
            null,
            "Use this form to opt out of the sale of your data as per Surfline \\ Wavetrak Inc.’s privacy policy and applicable laws.",
          ),
          React.createElement(
            'p',
            null,
            'Further information about Surfline’s privacy practices is available here:',
          ),
          React.createElement(
            'a',
            { href: 'https://www.surfline.com/privacy-policy', target: '_blank', rel: "noopener noreferrer" },
            'https://www.surfline.com/privacy-policy'
          ),
        )
        var cancelDialogTitle = 'Are you sure you want to cancel?'
        var cancelDialogContent =
          'Your preferences have not been saved. By continuing to use our website, you՚re agreeing to our Website Data Collection Policy.'

        var consentCustomCategories = {
          'Sale of Personal Data': {
            purpose: 'Choose "No" to opt out of the sale of your data',
            integrations: [
              'Facebook App Events',
              'Facebook Pixel',
              'Google Ads',
              'Google Analytics',
              'Omniture',
              'AdRoll',
              'Adjust',
            ],
          },
        }

        return {
          container: '#consent-manager',
          writeKey: "<?php echo getenv('SEGMENT_WRITE_KEY') ? getenv('SEGMENT_WRITE_KEY') : 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO'; ?>",
          shouldRequireConsent: function() { return false },
          bannerContent: bannerContent,
          bannerSubContent: bannerSubContent,
          preferencesDialogTitle: preferencesDialogTitle,
          preferencesDialogContent: preferencesDialogContent,
          cancelDialogTitle: cancelDialogTitle,
          cancelDialogContent: cancelDialogContent,
          implyConsentOnInteraction: true,
          defaultDestinationBehavior: 'imply',
          customCategories: consentCustomCategories
        }
      }
    </script>

    <script src="https://unpkg.com/@segment/consent-manager@5.0.0/standalone/consent-manager.js" defer></script>

    <script type="text/javascript">
      !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="<?php echo getenv('SEGMENT_WRITE_KEY') ? getenv('SEGMENT_WRITE_KEY') : 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO'; ?>";analytics.SNIPPET_VERSION="4.13.2";
      }}();
    </script>

    <script>
      window.mvp_forced = <?php echo $_SERVER['HTTP_X_SURFLINE_MVP_FORCED'] ? "true" : "false" ?>;
      window.environmentConfiguration = {
        serviceUrl: "<?php echo getenv('SERVICE_URL'); ?>",
        environment: "<?php echo getenv('APP_ENV'); ?>",
        splitioAuthKey: "<?php echo getenv('SPLITIO_AUTH_KEY'); ?>"
      };
    </script>

    <script>
      document.addEventListener("backplane_loaded", function (e) {
        var entitlements = e.detail.entitlements ? e.detail.entitlements.entitlements : [];
        var googletagUserType = entitlements && entitlements.includes('sl_premium') ? 'PREMIUM' : 'ANONYMOUS';
        console.log(googletagUserType);
        if (window.googletag && window.googletag.cmd) {
          window.googletag.cmd.push(() => window.googletag.pubads().setTargeting('usertype', googletagUserType));
          window.googletag.cmd.push(function() {
            googletag.enableServices();
            var unitId = '/1024858/Horizontal_Variable';
            var mapping = googletag.sizeMapping()
                .addSize([1200, 0], ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]])
                .addSize([992, 0], ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]])
                .addSize([768, 0], ['fluid', [750, 250], [750, 160], [728, 90]])
                .addSize([320, 0], ['fluid', [320, 80], [320, 50]])
                .addSize([0, 0], ['fluid', [320, 50]])
                .build();
            googletag.defineSlot(unitId, ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [320, 50], [970, 90], [970, 160], [320, 80], [300, 250], [750, 250], [750, 160], [728, 90]], 'div-gpt-ad-1493340496386-0')
                .defineSizeMapping(mapping)
                .setTargeting('position', 'SPONSORSHIP')
                .addService(googletag.pubads());
            googletag.pubads().addEventListener('slotRenderEnded', function(event) {
              if (event.size !== null && event.size[0] === 0 && event.size[1] === 0) {
                  // We've got a fluid ad.
                $('.hero-ad').addClass('hero-ad-fluid');
              }
            });
            googletag.display('div-gpt-ad-1493340496386-0');
          });
        }
      });
    </script>

    <script>
      //load the apstag.js library
      !function(a9,a,p,s,t,A,g){if(a[a9])return;function q(c,r){a[a9]._Q.push([c,r])}a[a9]={init:function(){q("i",arguments)},fetchBids:function(){q("f",arguments)},setDisplayBids:function(){},targetingKeys:function(){return[]},_Q:[]};A=p.createElement(s);A.async=!0;A.src=t;g=p.getElementsByTagName(s)[0];g.parentNode.insertBefore(A,g)}("apstag",window,document,"script","//c.amazon-adsystem.com/aax2/apstag.js");

      //initialize the apstag.js library on the page to allow bidding
      apstag.init({
          pubID: '15e39e15-7749-4909-bf7b-f5adb0a415e7',
          adServer: 'googletag'
      });

      apstag.fetchBids({
          slots: [{
              slotID: 'div-gpt-ad-page-unit-2',
              slotName: '/1024858/Box',
              sizes: [[300,250]]
          },
          {
              slotID: 'div-gpt-ad-page-unit-2-4',
              slotName: '/1024858/Box',
              sizes: [[300,250]]
          },
          {
              slotID: 'div-gpt-ad-1493340496386-0',
              slotName: '/1024858/Horizontal_Variable', //example: '12345/leaderboard-1'
              sizes: [[970, 250], [320, 50], [970, 90], [728, 90]] //example: [[728,90]]
          }],
          timeout: 2e3
      }, function(bids) {
          googletag.cmd.push(function(){
              apstag.setDisplayBids();
              googletag.pubads().refresh();
          });
      });
    </script>

</head>

<body <?php body_class(isset($class) ? $class : ''); ?>>
  <div id="consent-manager"></div>

  <div class="hero-ad">
    <div id='div-gpt-ad-1493340496386-0'></div>
  </div>

  <?php include('static-header.php'); ?>
