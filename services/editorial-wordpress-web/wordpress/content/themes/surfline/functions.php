<?php
define("INFINITE_SCROLL_POSTS_PER_PAGE", 16);

// setup classic backlinks
add_filter('rest_post_query', function( $args, $request ) {
  $classic_article_id = $request->get_param('classic_article_id');

  if (!is_null($classic_article_id)) {
    $args['meta_query'] = array(
      array(
        'key'     => 'classic_article_id',
        'value'   => $classic_article_id,
      )
    );
  }

  return $args;
}, 10, 2 );

if(function_exists('acf_add_local_field_group')) {
  acf_add_local_field_group(array (
    'key' => 'group_5ac7e591418bc',
    'title' => 'Classic Backlinks',
    'fields' => array (
      array (
        'key' => 'field_5ac7e5a18d5cb',
        'label' => 'Article ID',
        'name' => 'classic_article_id',
        'type' => 'number',
        'instructions' => 'Enter the ID of the classic Surfline article. When a user navigates to the article ID, they will be redirected to this WordPress post.',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '',
        'max' => '',
        'step' => '',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'post',
        ),
      ),
    ),
    'menu_order' => 5,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
  ));
}
// end classic backlinks

if( ! function_exists( 'get_field' ) ) {
  function get_field( $key, $post_id = false, $format_value = true ) {
      if( false === $post_id ) {
          global $post;
          $post_id = $post->ID;
      }
      return get_post_meta( $post_id, $key, true );
  }
}

// Slugify function
function slugify($string){
  $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
  $lowerSlug = strtolower($slug);
  return $lowerSlug;
}

function external_link($link, $post) {
    $external_url = get_field('externalUrl', $post->ID);
    $external_link = get_field('externalLink', $post->ID);
    $url  = esc_url(filter_var($external_url, FILTER_VALIDATE_URL));
    return $url && $external_link === 'External URL' ? $url : $link;
}
add_filter('post_link', 'external_link', 10, 2);

function post_link_target($post) {
    return get_field('externalLink', $post->ID) == 'External URL' && get_field('newWindow', $post->ID) ? '_blank' : '_self';
}

function post_link_attribution($post) {
  $externalSource = get_field('externalSource', $post->ID);
  return get_field('externalLink', $post->ID) == 'External URL' && $externalSource ? 'Via '.$externalSource : null;
}

//Add thumbnail, automatic feed links and title tag support
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('title-tag');

//Add content width (desktop default)
if (!isset($content_width)) {
  $content_width = 768;
}

//Add menu support and register main menu
if (function_exists('register_nav_menus')) {
  register_nav_menus(
    array(
      'main_menu' => 'Main Menu'
    )
  );
}

function dateStringToISO($dateStr) {
  $date_reformatted = strtotime($dateStr);
  return date('c', $date_reformatted);
}

// Add travel page custom post type
function travel_pages() {
  register_post_type('travel_page',
    array(
      'labels' => array(
        'name' => 'Travel Pages',
        'singular_name' => 'Travel Page',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Travel Page',
        'edit' => 'Edit',
        'edit_item' => 'Edit Travel Page',
        'new_item' => 'New Travel Page',
        'view' => 'View',
        'view_item' => 'View Travel Page',
        'search_items' => 'Search Travel Page',
        'not_found' => 'No Travel Page found',
        'not_found_in_trash' => 'No Travel Page found in Trash',
        'parent' => 'Parent Travel Page'
      ),
      'public' => false,
      'publicly_queryable' => true,
      'show_in_nav_menus' => true,
      'show_ui' => true,
      'show_in_rest' => true,
      'menu_position' => 18,
      'supports' => array('title', 'editor', 'comments', 'thumbnail', 'custom-fields'),
      'taxonomies' => array(''),
      'menu_icon'  => 'dashicons-location',
      'has_archive' => true
    )
  );
}
add_action('init', 'travel_pages');

// Set theme image sizes
add_image_size('yarpp-thumbnail', 540, 362, true);
add_image_size('feed1x', 780);
add_image_size('feed2x', 1560);
add_image_size('promobox1x', 1200);
add_image_size('promobox2x', 2400);

// filter the Gravity Forms button type
add_filter('gform_submit_button', 'form_submit_button', 10, 2);

function form_submit_button($button, $form) {
  return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
}

// Register sidebar
add_action('widgets_init', 'theme_register_sidebar');

function theme_register_sidebar() {
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'id' => 'sidebar-1',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
  }
}

// Bootstrap_Walker_Nav_Menu setup

add_action('after_setup_theme', 'bootstrap_setup');

if (!function_exists('bootstrap_setup')):

  function bootstrap_setup() {

    add_action('init', 'register_menu');

    function register_menu() {
      register_nav_menu('top-bar', 'Bootstrap Top Menu');
    }

    class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {

      function start_lvl(&$output, $depth = 0, $args = array()) {

        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
      }

      function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {

        if (!is_object($args)) {
          return; // menu has not been configured
        }

        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $li_attributes = '';
        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = ($args->has_children) ? 'dropdown' : '';
        $classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = ' class="' . esc_attr($class_names) . '"';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        $attributes .= ($args->has_children) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
      }

      function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {

        if (!$element)
          return;

        $id_field = $this->db_fields['id'];

        //display this element
        if (is_array($args[0]))
          $args[0]['has_children'] = !empty($children_elements[$element->$id_field]);
        else if (is_object($args[0]))
          $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        $cb_args = array_merge(array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'start_el'), $cb_args);

        $id = $element->$id_field;

        // descend only when the depth is right and there are childrens for this element
        if (($max_depth == 0 || $max_depth > $depth + 1) && isset($children_elements[$id])) {

          foreach ($children_elements[$id] as $child) {

            if (!isset($newlevel)) {
              $newlevel = true;
              //start the child delimiter
              $cb_args = array_merge(array(&$output, $depth), $args);
              call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
            }
            $this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
          }
          unset($children_elements[$id]);
        }

        if (isset($newlevel) && $newlevel) {
          //end the child delimiter
          $cb_args = array_merge(array(&$output, $depth), $args);
          call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
        }

        //end this element
        $cb_args = array_merge(array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'end_el'), $cb_args);
      }

    }
  }

endif;

// Add custom styles to theme options area
add_action('admin_head', 'custom_style');

function custom_style() {
  echo '<style>
    .appearance_page_pu_theme_options .wp-editor-wrap {
      width: 75%;
    }
    .regular-textcss_class {
      width: 50%;
    }
    .appearance_page_pu_theme_options h3 {
      font-size: 2em;
      padding-top: 40px;
    }
  </style>';
}

// END THEME OPTIONS

/**
 * Load site scripts.
 */
function bootstrap_theme_enqueue_scripts() {
  $template_url = get_template_directory_uri();

  //Include sitewide JS/CSS
  wp_enqueue_style('bootstrap', $template_url . '/css/bootstrap.css');
  wp_enqueue_style('style', $template_url . '/css/style.css');
  wp_enqueue_style('responsive', $template_url . '/css/responsive.css');
  wp_enqueue_style('ads', $template_url . '/css/ads.css');

  wp_enqueue_script('jq', $template_url . '/js/jquery.min.js'); // TODO why 2 jquery libs?
  wp_enqueue_script('jquery.bcSwipe', $template_url . '/js/jquery.bcSwipe.min.js');
  wp_enqueue_script('bootstrap', $template_url . '/js/bootstrap.min.js');
  wp_enqueue_script('picturefill', $template_url . '/js/picturefill.js');
  // Load Thread comments WordPress script.
  if (is_singular() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}

add_action('wp_enqueue_scripts', 'bootstrap_theme_enqueue_scripts', 1);

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2($buttons) {
  array_unshift($buttons, 'styleselect');

  return $buttons;
}

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats($init_array) {
  // Define the style_formats array
  $style_formats = array(
    // Each array child is a format with it's own settings
    array(
      'title' => 'Quote Mono',
      'block' => 'div',
      'classes' => 'quote-mono',
      'wrapper' => true,
    ),
    array(
      'title' => 'Quote Bright',
      'block' => 'div',
      'classes' => 'quote-bright',
      'wrapper' => true,
    ),
    array(
      'title' => 'Image Cover',
      'block' => 'div',
      'classes' => 'img-cover',
      'wrapper' => true,
    ),
    array(
      'title' => 'Video 100%',
      'block' => 'div',
      'classes' => 'video-wrap col-md-12',
      'wrapper' => true,
    ),
  );
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode($style_formats);

  return $init_array;
}

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');

//  Change the display date
function time_ago_date($the_date) {
  return human_time_diff(get_the_time('U'), current_time('timestamp')) . ' ' . __('ago');
}

add_filter('get_the_date', 'time_ago_date', 10, 1);

//  START Infinite Scroll
function be_ajax_load_more() {
  $count = 0;
  $num = isset($_POST['offt']) ? $_POST['offt'] : false;
  $offset = (int)$num;

  $args = array(
    'posts_per_page' => INFINITE_SCROLL_POSTS_PER_PAGE,
    'offset' => $offset,
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true,
    "tax_query" => array(
      array(
        "taxonomy" => "promotion",
        "field" => "slug",
        "terms" => "curated"
      )
    )
  );
  ob_start();
  $posts_array = get_posts($args);
  $post_count = INFINITE_SCROLL_POSTS_PER_PAGE;
  global $post;

  foreach ($posts_array as $index => $post) {
    if ($count <= INFINITE_SCROLL_POSTS_PER_PAGE) {
      $field_name = "show_video";
      $id = $post->ID;
      $video = get_field($field_name, $id);
      $category = $category[0]->cat_name;
      $title = $post->post_title;
      $title = mb_strimwidth($title, 0, 50, '...');
      $tumbnail = get_the_post_thumbnail($id);
      $category = get_the_category();
      ?>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div id="post-<?php echo $id; ?>" <?php post_class(); ?>>
                <div class="item"
                     onclick="document.location.href = '<?php the_permalink($post->ID); ?>'; return false">

                    <div class="thumbnail">

                      <?php
                      if (!empty($video)) {
                        echo '<div class="video-play">
                      <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
                                            </div>';
                      }
                      ?>
                      <?php
                      $title = get_the_title();
                      the_post_thumbnail($id, array('title' => $title, 'alt' => $title));
                      ?>         </div>

                    <div class="meta">

                      <?php
                      foreach ($category as $c) {
                        $cat_name = $c->cat_name;
                        $cat_slug = $c->slug;
                        echo '<a href="../' . $cat_slug . '">' . $cat_name . '</a>';
                        break;
                      }
                      ?>

                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                   class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                          <?php
                          $before = "| <strong>Updated</strong>&nbsp;";
                          if (get_the_modified_time('U') != get_the_time('U')) {
                            echo $before;
                            echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                          }
                          ?>
                        </span>

                    </div>
                    <a href="<?php echo get_permalink($post->ID); ?>" class="overlay-link">
                      <?php
                      $title = get_the_title();
                      $title = mb_strimwidth($title, 0, 50, '...');
                      ?>
                        <div class="headline"><h4><?php echo $title; ?></h4></div>
                    </a>

                </div>
            </div>

        </div>

        <?php
          if ($index === 15) {
        ?>
            <!-- /1024858/Box -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div id='div-gpt-ad-page-unit-2-<?php echo $index."-".$offset; ?>' class="ad-unit-inline-mpu">
                    <script>
                      googletag.cmd.push(function () {
                        var slotId = "div-gpt-ad-page-unit-2-<?php echo $index."-".$offset; ?>";

                        googletag.defineSlot('/1024858/Box', [300, 250], slotId)
                          .addService(googletag.pubads());

                        googletag.display(slotId);
                      });
                    </script>
                </div>
            </div>
        <?php
          }
        ?>

      <?php
      ?>

      <?php
      $count++;
    }  //  END IF
  } //  END Foreach

  $offset = $offset + INFINITE_SCROLL_POSTS_PER_PAGE;
  if ($count < $post_count) {
    $finished = "true";
  } else {
    $finished = "false";
  }
  ?>
    <div id="offsets" style="display:none;"><?php echo $offset; ?></div>

    <div id="offsets_end" style="display:none;"><?php echo $finished; ?></div>
  <?php
  wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success($data);
  wp_die();
}

add_action('wp_ajax_be_ajax_load_more', 'be_ajax_load_more');
add_action('wp_ajax_nopriv_be_ajax_load_more', 'be_ajax_load_more');

function be_load_more_js() {
  global $wp_query;
  $args = array(
    'url' => admin_url('admin-ajax.php'),
    'query' => $wp_query->query,
  );

  if (is_home()) {
    wp_enqueue_script('be-load-more', get_stylesheet_directory_uri() . '/js/load-more.js', array('jquery'), '1.0', true);
    wp_localize_script('be-load-more', 'beloadmore', $args);
  }
}

add_action('wp_enqueue_scripts', 'be_load_more_js');

//  END Infinite Scroll
//
//
//  START Category Infinite Scroll
function be_ajax_load_more_cat() {
  $count = 0;
  $cat = isset($_POST['cat']) ? $_POST['cat'] : false;

  $num = isset($_POST['offt']) ? $_POST['offt'] : false;
  $offset = (int)$num;

  $args = array(
    'posts_per_page' => 18,
    'offset' => $offset,
    'post_type' => 'post',
    'category_name' => $cat,
    'post_status' => 'publish'
  );
  ob_start();
  $posts_array = get_posts($args);
  $post_count = 18;
  global $post;

  foreach ($posts_array as $post) {
    if ($count <= 18) {
      $field_name = "show_video";
      $id = $post->ID;
      $video = get_field($field_name, $id);
      $category = $category[0]->cat_name;
      $title = $post->post_title;
      $title = mb_strimwidth($title, 0, 50, '...');
      $tumbnail = get_the_post_thumbnail($id);
      $category = get_the_category();
      ?>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div id="post-<?php echo $id; ?>" <?php post_class(); ?>>
                <div class="item"
                     onclick="document.location.href = '<?php the_permalink($post->ID); ?>'; return false">

                    <div class="thumbnail">

                      <?php
                      if (!empty($video)) {
                        echo '<div class="video-play">
                                            <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
                                            </div>';
                      }
                      ?>
                      <?php
                      $title = get_the_title();
                      the_post_thumbnail($id, array('title' => $title, 'alt' => $title));
                      ?>         </div>

                    <div class="meta">

                      <?php
                      foreach ($category as $c) {
                        $cat_name = $c->cat_name;
                        $cat_slug = $c->slug;
                        if ($cat_name == $cat) {
                          echo '<a href="../' . $cat_slug . '">' . $cat_name . '</a>';
                          break;
                        }
                      }
                      ?>

                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                   class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                          <?php
                          $before = "| <strong>Updated</strong>&nbsp;";
                          if (get_the_modified_time('U') != get_the_time('U')) {
                            echo $before;
                            echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                          }
                          ?>
                            </span>

                    </div>
                    <a href="<?php echo get_permalink($post->ID); ?>" class="overlay-link">
                      <?php
                      $title = get_the_title();
                      $title = mb_strimwidth($title, 0, 50, '...');
                      ?>
                        <div class="headline"><h4><?php echo $title; ?></h4></div>
                    </a>

                </div>
            </div>
        </div>
      <?php

      $count++;
    }  //  END IF
  }//  END Foreach
  $offset = $offset + 18;
  if ($count < $post_count) {
    $finished = "true";
  } else {
    $finished = "false";
  }
  ?>
    <div id="offsets" style="display:none;"><?php echo $offset; ?></div>

    <div id="offsets_end" style="display:none;"><?php echo $finished; ?></div>
  <?php
  wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success($data);
  wp_die();
}

add_action('wp_ajax_be_ajax_load_more_cat', 'be_ajax_load_more_cat');
add_action('wp_ajax_nopriv_be_ajax_load_more_cat', 'be_ajax_load_more_cat');

function be_load_more_js_cat() {
  global $wp_query;
  $args = array(
    'url' => admin_url('admin-ajax.php'),
    'query' => $wp_query->query,
  );

  if (is_category()) {
    wp_enqueue_script('be-load-more_cat', get_stylesheet_directory_uri() . '/js/load-more-cat.js', array('jquery'), '1.0', true);
    wp_localize_script('be-load-more_cat', 'beloadmore', $args);
  }
}

add_action('wp_enqueue_scripts', 'be_load_more_js_cat');

//  END Category Infinite Scroll
//  START ALL Category Infinite Scroll
function be_ajax_load_more_all() {
  $count = 0;
  $num = isset($_POST['offt']) ? $_POST['offt'] : false;
  $offset = (int)$num;

  $args = array(
    'posts_per_page' => 18,
    'offset' => $offset,
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true
  );
  ob_start();
  $posts_array = get_posts($args);
  $post_count = 18;
  global $post;
  foreach ($posts_array as $post) {

    if ($count <= 18) {
      $field_name = "show_video";
      $id = $post->ID;
      $video = get_field($field_name, $id);
      $category = $category[0]->cat_name;
      $title = $post->post_title;
      $title = mb_strimwidth($title, 0, 50, '...');
      $tumbnail = get_the_post_thumbnail($id);
      $category = get_the_category();
      ?>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div id="post-<?php echo $id; ?>" <?php post_class(); ?>>
                <div class="item"
                     onclick="document.location.href = '<?php the_permalink($post->ID); ?>'; return false">

                    <div class="thumbnail">
                      <?php
                      if (!empty($video)) {
                        echo '<div class="video-play">
                                            <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
                                            </div>';
                      }
                      ?>
                      <?php
                      $title = get_the_title();
                      the_post_thumbnail($id, array('title' => $title, 'alt' => $title));
                      ?>         </div>

                    <div class="meta">

                      <?php
                      foreach ($category as $c) {
                        $cat_name = $c->cat_name;
                        $cat_slug = $c->slug;
                        echo '<a href="../' . $cat_slug . '">' . $cat_name . '</a>';
                        break;
                      }
                      ?>

                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                   class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                          <?php
                          $before = "| <strong>Updated</strong>&nbsp;";
                          if (get_the_modified_time('U') != get_the_time('U')) {
                            echo $before;
                            echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                          }
                          ?>
                            </span>

                    </div>
                    <a href="<?php echo get_permalink($post->ID); ?>" class="overlay-link">
                      <?php
                      $title = get_the_title();
                      $title = mb_strimwidth($title, 0, 50, '...');
                      ?>
                        <div class="headline"><h4><?php echo $title; ?></h4></div>
                    </a>

                </div>
            </div>
        </div>
      <?php

      $count++;
    }  //  END IF
  }  //  END Foreach

  $offset = $offset + 18;
  if ($count < $post_count) {
    $finished = "true";
  } else {
    $finished = "false";
  }
  ?>
    <div id="offsets" style="display:none;"><?php echo $offset; ?></div>

    <div id="offsets_end" style="display:none;"><?php echo $finished; ?></div>
  <?php
  wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success($data);
  wp_die();
}

add_action('wp_ajax_be_ajax_load_more_all', 'be_ajax_load_more_all');
add_action('wp_ajax_nopriv_be_ajax_load_more_all', 'be_ajax_load_more_all');

function be_load_more_js_all() {
  global $wp_query;
  $args = array(
    'url' => admin_url('admin-ajax.php'),
    'query' => $wp_query->query,
  );

  if (is_page('all')) {

    wp_enqueue_script('be-load-more_all', get_stylesheet_directory_uri() . '/js/load-more-all.js', array('jquery'), '1.0', true);
    wp_localize_script('be-load-more_all', 'beloadmore', $args);
  }
}

add_action('wp_enqueue_scripts', 'be_load_more_js_all');

//  END ALL Category Infinite Scroll
//
//
//
//  START TAG Infinite Scroll
function be_ajax_load_more_tag() {
  $count = 0;
  $tag = isset($_POST['tag']) ? $_POST['tag'] : false;

  $num = isset($_POST['offt']) ? $_POST['offt'] : false;
  $offset = (int)$num;

  $args = array(
    'posts_per_page' => 18,
    'offset' => $offset,
    'post_type' => 'post',
    'post_status' => 'publish',
    "tag" => $tag
  );
  ob_start();
  $posts_array = get_posts($args);
  $post_count = 18;
  global $post;
  foreach ($posts_array as $post) {
    if ($count <= 18) {
      $field_name = "show_video";
      $id = $post->ID;
      $video = get_field($field_name, $id);
      $category = $category[0]->cat_name;
      $title = $post->post_title;
      $title = mb_strimwidth($title, 0, 50, '...');
      $tumbnail = get_the_post_thumbnail($id);
      $category = get_the_category();
      ?>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div id="post-<?php echo $id; ?>" <?php post_class(); ?>>
                <div class="item"
                     onclick="document.location.href = '<?php the_permalink($post->ID); ?>'; return false">

                    <div class="thumbnail">

                      <?php
                      if (!empty($video)) {
                        echo '<div class="video-play">
                                            <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
                                            </div>';
                      }
                      ?>
                      <?php
                      $title = get_the_title();
                      the_post_thumbnail($id, array('title' => $title, 'alt' => $title));
                      ?>         </div>

                    <div class="meta">

                      <?php
                      foreach ($category as $c) {
                        $cat_name = $c->cat_name;
                        $cat_slug = $c->slug;
                        echo '<a href="../' . $cat_slug . '">' . $cat_name . '</a>';
                        break;
                      }
                      ?>

                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                   class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                          <?php
                          $before = "| <strong>Updated</strong>&nbsp;";
                          if (get_the_modified_time('U') != get_the_time('U')) {
                            echo $before;
                            echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                          }
                          ?>
                        </span>

                    </div>
                    <a href="<?php echo get_permalink($post->ID); ?>" class="overlay-link">
                      <?php
                      $title = get_the_title();
                      $title = mb_strimwidth($title, 0, 50, '...');
                      ?>
                        <div class="headline"><h4><?php echo $title; ?></h4></div>
                    </a>

                </div>
            </div>
        </div>
      <?php
      $count++;
    }  //  END IF
  }  //  END Foreach
  $offset = $offset + 18;
  if ($count < $post_count) {
    $finished = "true";
  } else {
    $finished = "false";
  }
  ?>
    <div id="offsets" style="display:none;"><?php echo $offset; ?></div>

    <div id="offsets_end" style="display:none;"><?php echo $finished; ?></div>
  <?php
  wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success($data);
  wp_die();
}

add_action('wp_ajax_be_ajax_load_more_tag', 'be_ajax_load_more_tag');
add_action('wp_ajax_nopriv_be_ajax_load_more_tag', 'be_ajax_load_more_tag');

function be_load_more_js_tag() {
  global $wp_query;
  $args = array(
    'url' => admin_url('admin-ajax.php'),
    'query' => $wp_query->query,
  );

  if (is_tag()) {

    wp_enqueue_script('be-load-more_tag', get_stylesheet_directory_uri() . '/js/load-more-tag.js', array('jquery'), '1.0', true);
    wp_localize_script('be-load-more_tag', 'beloadmore', $args);
  }
}

add_action('wp_enqueue_scripts', 'be_load_more_js_tag');

//  END TAG Infinite Scroll
//
//
//
//  START SERIES Infinite Scroll
function be_ajax_load_more_series() {
  $count = 0;
  $series = isset($_POST['series']) ? $_POST['series'] : false;

  $num = isset($_POST['offt']) ? $_POST['offt'] : false;
  $offset = (int)$num;

  $args = array(
    'posts_per_page' => 18,
    'offset' => $offset,
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true,
    'tax_query' => array(
      array(
        'taxonomy' => 'series',
        'field' => 'name',
        'terms' => $series
      )
    ),
  );
  ob_start();
  $posts_array = get_posts($args);
  $post_count = 18;
  global $post;
  foreach ($posts_array as $post) {
    if ($count <= 18) {
      $field_name = "show_video";
      $id = $post->ID;
      $video = get_field($field_name, $id);
      $category = $category[0]->cat_name;
      $title = $post->post_title;
      $title = mb_strimwidth($title, 0, 50, '...');
      $tumbnail = get_the_post_thumbnail($id);
      $category = get_the_category();
      ?>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div id="post-<?php echo $id; ?>" <?php post_class(); ?>>
                <div class="item"
                     onclick="document.location.href = '<?php the_permalink($post->ID); ?>'; return false">

                    <div class="thumbnail">

                      <?php
                      if (!empty($video)) {
                        echo '<div class="video-play">
                                            <img src="' . get_template_directory_uri() . '/img/playbtn.svg" alt="video" class="video-play-controller">
                                            </div>';
                      }
                      ?>
                        $title = get_the_title();
                        the_post_thumbnail($id, array('title' => $title, 'alt' => $title));
                        ?>
                    </div>

                    <div class="meta">

                      <?php
                      foreach ($category as $c) {
                        $cat_name = $c->cat_name;
                        $cat_slug = $c->slug;
                        echo '<a href="../' . $cat_slug . '">' . $cat_name . '</a>';
                        break;
                      }
                      ?>

                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/clock.svg"
                                   class="clock"/><?php echo get_the_date('Y-m-d'); ?>
                          <?php
                          $before = "| <strong>Updated</strong>&nbsp;";
                          if (get_the_modified_time('U') != get_the_time('U')) {
                            echo $before;
                            echo human_time_diff(get_the_modified_date('U'), current_time('timestamp')) . ' ' . __('ago');
                          }
                          ?>
                        </span>

                    </div>
                    <a href="<?php echo get_permalink($post->ID); ?>" class="overlay-link">
                      <?php
                      $title = get_the_title();
                      $title = mb_strimwidth($title, 0, 50, '...');
                      ?>
                        <div class="headline"><h4><?php echo $title; ?></h4></div>
                    </a>

                </div>
            </div>
        </div>
      <?php
      $count++;
    }  //  END IF
  }  //  END Foreach
  $offset = $offset + 18;
  if ($count < $post_count) {
    $finished = "true";
  } else {
    $finished = "false";
  }
  ?>
    <div id="offsets" style="display:none;"><?php echo $offset; ?></div>

    <div id="offsets_end" style="display:none;"><?php echo $finished; ?></div>
  <?php
  wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success($data);
  wp_die();
}

add_action('wp_ajax_be_ajax_load_more_series', 'be_ajax_load_more_series');
add_action('wp_ajax_nopriv_be_ajax_load_more_series', 'be_ajax_load_more_series');

function be_load_more_js_series() {
}

add_action('wp_enqueue_scripts', 'be_load_more_js_series');

//  END SERIES Infinite Scroll

// Add rewrite so that /surf-news/ shows WP homepage content
function custom_rewrite_surfnews() {
  add_rewrite_rule('^surf-news/?$', 'index.php', 'top');
}
add_action('init', 'custom_rewrite_surfnews');

// Add rewrite so that /series/ shows series taxonomy content
function custom_rewrite_series() {
  add_rewrite_rule('^series/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$', 'index.php?series=$matches[1]&feed=$matches[2]', 'top');
  add_rewrite_rule('^series/([^/]+)/(feed|rdf|rss|rss2|atom)/?$', 'index.php?series=$matches[1]&feed=$matches[2]', 'top');
  add_rewrite_rule('^series/([^/]+)/embed/?$', 'index.php?series=$matches[1]&embed=true', 'top');
  add_rewrite_rule('^series/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?series=$matches[1]&paged=$matches[2]', 'top');
  add_rewrite_rule('^series/([^/]+)/?$', 'index.php?series=$matches[1]', 'top');
}
add_action('init', 'custom_rewrite_series');

add_filter( 'wpseo_sitemap_entry', function( $url, $type, $post ) {
  if (strpos($url['loc'], "/series/") !== false) {
    $url['loc'] = str_replace("/surf-news/", "/", $url['loc']);
    return $url;
  }
  return $url;
}, 10, 3 );

// TODO: This doesn't work properly
function custom_rewrite_surfnews_all() {
  add_rewrite_rule('^surf-news/all/?$', '/all', 'top');
}
add_action('init', 'custom_rewrite_surfnews_all');

// if site is set to run on SSL, then force-enable SSL detection
if (stripos(get_option('siteurl'), 'https://') === 0) {
  $_SERVER['HTTPS'] = 'on';
}

//  START Get rid of jQuery Migrate code that Wordpress places in the Console
add_action('wp_default_scripts', function ($scripts) {
  if (!empty($scripts->registered['jquery'])) {
    $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, array('jquery-migrate'));
  }
});
//  END Get rid of jQuery Migrate code that Wordpress places in the Console

// set var to allow `amazon-web-services` plugin to use role-based security
// this allows dev to use aws keys and sandbox/staging/prod to use roles
$appEnv = getenv('APP_ENV') ? getenv('APP_ENV') : 'development';
if ($appEnv != 'development') {
  define('AWS_USE_EC2_IAM_ROLE', true);
}

// set services_url for frontend resource resolution
// Where can I set this so it doesn't need to be recalculated for every requests?
if ($appEnv == 'development' || $appEnv == 'sandbox') {
  putenv('SERVICE_URL=https://services.sandbox.surfline.com');
} else if ($appEnv == 'staging') {
  putenv('SERVICE_URL=https://services.staging.surfline.com');
} else {
  putenv('SERVICE_URL=https://services.surfline.com');
}

// Change post preview button url
function surfline_preview_link() {
  $slug = basename(get_permalink());
  $mydir = '/surf-news/';
  $mynewpurl = "$mydir$slug&amp;preview=true";

  return "$mynewpurl";
}

add_filter('preview_post_link', 'surfline_preview_link');

// Expose series taxonomy through REST API
add_action('init', 'series_taxonomy_rest_support', 25);
function series_taxonomy_rest_support() {
  global $wp_taxonomies;

  $taxonomy_name = 'series';
  if (isset($wp_taxonomies[$taxonomy_name])) {
    $wp_taxonomies[$taxonomy_name]->show_in_rest = true;
    $wp_taxonomies[$taxonomy_name]->rest_base = $taxonomy_name;
    $wp_taxonomies[$taxonomy_name]->rest_controller_class = 'WP_REST_Terms_Controller';
  }
}

// Expose promotion taxonomy through REST API
add_action('init', 'promotion_taxonomy_rest_support', 25);
function promotion_taxonomy_rest_support() {
  global $wp_taxonomies;

  $taxonomy_name = 'promotion';
  if (isset($wp_taxonomies[$taxonomy_name])) {
    $wp_taxonomies[$taxonomy_name]->show_in_rest = true;
    $wp_taxonomies[$taxonomy_name]->rest_base = $taxonomy_name;
    $wp_taxonomies[$taxonomy_name]->rest_controller_class = 'WP_REST_Terms_Controller';
  }
}

// Filter select values for taxonomy location selection
function location_tagging_filter($field) {
  // we're either responding to a typeahead ajax call or an initial page load
  $typeahead = isset($_REQUEST['s']) && $_REQUEST['s'] != '' ? $_REQUEST['s'] : '';

  $taxonomyLocations = [];

  // build choices for typeahead...
  if ($typeahead != '') {
    $url = getenv('SERVICE_URL') . '/search/spots?includeTaxonomyTypes=region,subregion,geoname&suggestionSize=1&querySize=20&q=' . $typeahead;
    $response = wp_remote_get(esc_url_raw($url));
    $res = json_decode(wp_remote_retrieve_body($response), true);

    $taxonomyLocations = $res[1]['hits']['hits'];
  } else {
    // ...otherwise build choices for page load

    // get saved values (remove/add filter to avoid infinite loop)
    global $post;
    remove_filter('acf/load_field/name=dynlocation', 'location_tagging_filter');
    $db_value = get_field($field['name'], $post->ID, false);
    add_filter('acf/load_field/name=dynlocation', 'location_tagging_filter');

    // for all saved values, query the search service to get their labels
    if ($db_value != '') {
      $ids = '';
      if (is_array($db_value)) {
        $ids = implode(',', $db_value);
      } else {
        $ids = $db_value;
      }
      $url = getenv('SERVICE_URL') . '/search/taxonomy?ids=' . $ids;
      $response = wp_remote_get(esc_url_raw($url));
      $res = json_decode(wp_remote_retrieve_body($response), true);

      $taxonomyLocations = $res['hits']['hits'];
    }
  }

  $field['choices'] = array();
  foreach ($taxonomyLocations as $loc) {
    $id = $loc['_id'];
    $name = $loc['_source']['name'];
    $type = $loc['_type'];

    $breadcrumbs = '';
    if (isset($loc['_source']['breadCrumbs']) && sizeof($loc['_source']['breadCrumbs']) > 0) {
      $breadcrumbs = implode(' / ', $loc['_source']['breadCrumbs']) . ' / ';
    }

    $field['choices'][$id] = "$breadcrumbs$name ($type)";
  }

  return $field;
}

add_filter('acf/load_field/name=dynlocation', 'location_tagging_filter');

// Filter select values for taxonomy location selection
function location_tagging_travel_filter($field) {
  // we're either responding to a typeahead ajax call or an initial page load
  $typeahead = isset($_REQUEST['s']) && $_REQUEST['s'] != '' ? $_REQUEST['s'] : '';

  $taxonomyLocations = [];

  // build choices for typeahead...
  if ($typeahead != '') {
    $url = getenv('SERVICE_URL') . '/search/spots?includeTaxonomyTypes=region,subregion,geoname&suggestionSize=1&querySize=20&q=' . $typeahead;
    $response = wp_remote_get(esc_url_raw($url));
    $res = json_decode(wp_remote_retrieve_body($response), true);

    $taxonomyLocations = $res[1]['hits']['hits'];
  } else {
    // ...otherwise build choices for page load

    // get saved values (remove/add filter to avoid infinite loop)
    global $post;
    remove_filter('acf/load_field/name=dynlocationtravel', 'location_tagging_travel_filter');
    $db_value = get_field($field['name'], $post->ID, false);
    add_filter('acf/load_field/name=dynlocationtravel', 'location_tagging_travel_filter');

    // for all saved values, query the search service to get their labels
    if ($db_value != '') {
      $ids = '';
      if (is_array($db_value)) {
        $ids = implode(',', $db_value);
      } else {
        $ids = $db_value;
      }
      $url = getenv('SERVICE_URL') . '/search/taxonomy?ids=' . $ids;
      $response = wp_remote_get(esc_url_raw($url));
      $res = json_decode(wp_remote_retrieve_body($response), true);

      $taxonomyLocations = $res['hits']['hits'];
    }
  }

  $field['choices'] = array();
  foreach ($taxonomyLocations as $loc) {
    $id = $loc['_id'];
    $name = $loc['_source']['name'];
    $type = $loc['_type'];

    $breadcrumbs = '';
    if (isset($loc['_source']['breadCrumbs']) && sizeof($loc['_source']['breadCrumbs']) > 0) {
      $breadcrumbs = implode(' / ', $loc['_source']['breadCrumbs']) . ' / ';
    }

    $field['choices'][$id] = "$breadcrumbs$name ($type)";
  }

  return $field;
}

add_filter('acf/load_field/name=dynlocationtravel', 'location_tagging_travel_filter');

# Filter Swell Event Post Order ACF Relationship Field Choices
function sl_swell_event_post_order_choices($args, $field, $post_id) {
  $meta_query = array(
    'key' => 'parent_swell_event',
    'value' => '"' . $post_id . '"',
    'compare' => 'LIKE'
  );
  $args['meta_query'] = array($meta_query);
	return $args;
}

add_filter('acf/fields/relationship/query/name=swell_event_post_order', 'sl_swell_event_post_order_choices', 10, 3);

# Override SEO og:title/twitter:title from yoast for FB/Twitter
# Remove content after and including separator
# Ex: My cool title - Surfline becomes just My cool title
# Leaves page title in tact, just affects og:title and twitter:title
function override_ogtitle($title) {
  $newOGTitle = str_replace('- Surfline', '', $title);
  return trim($newOGTitle);
}

add_filter('wpseo_opengraph_title','override_ogtitle');
add_filter('wpseo_twitter_title', 'override_ogtitle');


# Override canonical links for main path to allow /surf-news to display there.
# If it exists on other series (tax) type urls, remove it
function override_canonical($cano) {
  $urlParts = parse_url($cano);

  if ($urlParts['path'] === '/') {
    $cano = $urlParts['scheme'].'://'.$urlParts['host'].(isset($urlParts['port']) ? ':'.$urlParts['port'] : '').'/surf-news'.$urlParts['path'];
  }

  return $cano;
}

add_action('wpseo_canonical', 'override_canonical');

# Remove next/prev rel links
add_filter( 'wpseo_next_rel_link', '__return_false' );
add_filter( 'wpseo_prev_rel_link', '__return_false' );

add_action('rest_api_init', 'add_menu_order_post_field_api');

function add_menu_order_post_field_api() {
  register_rest_field(
    'post',
    'menu_order',
    array(
      'get_callback'    => 'get_custom_fields',
      'update_callback' => null,
      'schema'          => array (
      'description'     => __('menu_order for sorting'),
      'type'            => 'string',
      'context'         => array ('view'),
      ),
    )
  );
}

function get_custom_fields($object, $field_name, $request) {
  return $object[$field_name];
}

/**
 * Customize the preview button in the WordPress admin to point to the headless client.
 *
 * @param  str $link The WordPress preview link.
 * @return str The headless WordPress preview link.
 */

function set_headless_preview_link( $link, $post ) {
  $appEnv = getenv('APP_ENV') ? getenv('APP_ENV') : 'development';
  $slAPIUrl = '';
  if ($appEnv == 'development') {
    $slAPIUrl = 'http://localhost:8080';
  } else if ($appEnv == 'sandbox') {
    $slAPIUrl = 'https://sandbox.surfline.com';
  } else if ($appEnv == 'staging') {
    $slAPIUrl = 'https://staging.surfline.com';
  } else {
    $slAPIUrl = 'https://www.surfline.com';
  }
  if ($post->post_type === 'sl_swell_event') {
    return $slAPIUrl
      . '/surf-news/swell-alert/'
      . slugify(get_the_title())
      . '/'
      . get_the_ID();
  } else {
    return $slAPIUrl
      . '/surf-news/_preview/'
      . get_the_ID();
  }
}

add_filter( 'preview_post_link', 'set_headless_preview_link', 10, 2 );

function load_camera_embeds($field) {
  // Get cameras from cams service
  $url = getenv('SERVICE_URL') . '/cameras/embeds';
  $response = wp_remote_get(esc_url_raw($url));
  $camerasArray = json_decode(wp_remote_retrieve_body($response), true);
  $field['choices'] = array();

  // Populate field with camera choices
  $cameras = array();
  foreach($camerasArray as $camera) {
    $cameras[$camera['_id']] = $camera['title'];
  }

  if (is_array($cameras)) {
    foreach($cameras as $camera) {
      $field['choices'] = $cameras;
    }
  }
  return $field;
}
add_filter('acf/load_field/key=field_5dc1fd3cde877', 'load_camera_embeds');


function camera_embed_selected() {
  $new_value = $_POST['new_value'];

  if( !isset( $new_value ) || $new_value = '' ) {
    die(
      json_encode(
        array(
          'success' => false,
          'message' => 'Missing required information.'
        )
      )
    );
  }

  die(
    json_encode(
      array(
          'success' => true,
          'message' => $new_value
      )
    )
  );
}
add_action( 'wp_ajax_embed-selected', 'camera_embed_selected' );

function embeds_jquery() {
  ?>
  <script type="text/javascript">
    jQuery(function($) {
      $(document).ready(function() {
        $('#acf-field_5dc1fd3cde877').on("change", function(e) {
          var ajaxurl = "https://" + window.location.host + "/wp-admin/admin-ajax.php";
          var newValue = jQuery("#acf-field_5dc1fd3cde877").val();
          newValue = newValue.toString();

          $.ajax({
            url: ajaxurl + "?action=embed-selected",
            type: "POST",
            data: {
              action: "embed-selected",
              new_value: newValue
            },
            dataType: "json"
          })
            .done(function(json) {
              $("#acf-field_5dc303c68c968").val('<div class="wp-cam-embed" data-sl-cam-id="' + newValue +  '"></div>');
            })
            .fail(function() {
              alert("The Ajax call failed.");
            })
        });
      });
    });
  </script>
  <?php
}
add_action('admin_footer', 'embeds_jquery');

function get_postmeta() {
  register_rest_field( 'post', 'metadata', array(
    'get_callback' => function ( $data ) {
        return get_post_meta( $data['id'], '', '' );
    }, ));
}
add_action( 'rest_api_init', 'get_postmeta');


function acf_modify_hompage_order_height() {

  ?>
    <style type="text/css">
        .acf-bl {
            height: 280px !important;
        }
    </style>
    <?php

}
add_action('acf/input/admin_head', 'acf_modify_hompage_order_height');

// import Contests
require_once( __DIR__ . '/includes/contests_acf.php');
require_once( __DIR__ . '/includes/contests_cpt.php');
require_once( __DIR__ . '/includes/contests_tax.php');
// import Forecasts
require_once( __DIR__ . '/includes/forecast_acf.php');
require_once( __DIR__ . '/includes/forecast_cpt.php');
require_once( __DIR__ . '/includes/forecast_tax.php');
// import Events
require_once( __DIR__ . '/includes/events_acf.php');
require_once( __DIR__ . '/includes/events_cpt.php');
require_once( __DIR__ . '/includes/events_tax.php');
// import US Homepage Order
require_once( __DIR__ . '/includes/homepage_order_cpt.php');
require_once( __DIR__ . '/includes/homepage_order_acf.php');
// import AU Homepage Order
require_once( __DIR__ . '/includes/au_homepage_order_cpt.php');
require_once( __DIR__ . '/includes/au_homepage_order_acf.php');
// import NZ Homepage Order
require_once( __DIR__ . '/includes/nz_homepage_order_cpt.php');
require_once( __DIR__ . '/includes/nz_homepage_order_acf.php');
// import posts acf
require_once( __DIR__ . '/includes/sl_posts_acf.php');
// import series acf
require_once( __DIR__ . '/includes/sl_series_acf.php');
// import categories acf
require_once( __DIR__ . '/includes/sl_categories_acf.php');

// import custom API routes
require_once( __DIR__ . '/api/sl_api_contests.php');
require_once( __DIR__ . '/api/sl_api_posts.php');
require_once( __DIR__ . '/api/sl_api_events.php');
require_once( __DIR__ . '/api/sl_api_taxonomy_posts.php');
require_once( __DIR__ . '/api/sl_api_taxonomy.php');
