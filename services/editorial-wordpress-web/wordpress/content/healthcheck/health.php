<?php
header('Content-Type: application/json');

$version = getenv('APP_VERSION') ? getenv('APP_VERSION') : 'unknown';
$response = [
    'status' => 200,
    'message' => 'OK',
    'version' => $version
];
echo json_encode($response);
