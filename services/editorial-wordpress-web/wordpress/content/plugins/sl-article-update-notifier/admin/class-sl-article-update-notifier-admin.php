<?php
// Use AWS client from `amazon-web-services`/`amazon-s3-and-cloudfront` plugins
use Aws\Sns\SnsClient;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://github.com/Surfline
 * @since      1.0.0
 *
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/admin
 * @author     Matt Walker <mwalker@surfline.com>
 */
class Sl_Article_Update_Notifier_Admin {

  /**
   * The ID of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string $plugin_name The ID of this plugin.
   */
  private $plugin_name;

  /**
   * The version of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string $version The current version of this plugin.
   */
  private $version;

  /**
   * Initialize the class and set its properties.
   *
   * @since    1.0.0
   * @param      string $plugin_name The name of this plugin.
   * @param      string $version The version of this plugin.
   */
  public function __construct($plugin_name, $version) {

    $this->plugin_name = $plugin_name;
    $this->version = $version;

  }

  /**
   * Register the stylesheets for the admin area.
   *
   * @since    1.0.0
   */
  public function enqueue_styles() {

    /**
     * This function is provided for demonstration purposes only.
     *
     * An instance of this class should be passed to the run() function
     * defined in Sl_Article_Update_Notifier_Loader as all of the hooks are defined
     * in that particular class.
     *
     * The Sl_Article_Update_Notifier_Loader will then create the relationship
     * between the defined hooks and the functions defined in this
     * class.
     */

    wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/sl-article-update-notifier-admin.css', array(), $this->version, 'all');

  }

  /**
   * Used to accurately delete a post if it transitions off of 'publish'
   *
   * https://codex.wordpress.org/Post_Status_Transitions
   * The available post statuses are:
   * new – When there’s no previous status (this means these hooks are always run whenever "save_post" runs.
   * publish – A published post or page.
   * pending – A post pending review.
   * draft – A post in draft status.
   * auto-draft – A newly created post with no content.
   * future – A post scheduled to publish in the future.
   * private – Not visible to users who are not logged in.
   * inherit – A revision or attachment (see get_children()).
   * trash – Post is in the trash (added with Version 2.9).
   *
   * @param $new_status
   * @param $old_status
   * @param $post
   */
  public function transition_post_status($new_status, $old_status, $post) {
    $action = '';
    if (($new_status == 'trash') || ($old_status == 'publish' && $new_status != 'publish')) {
      $action = 'DELETE_POST';
    }

    // If the above logic doesn't result in an action, return
    if ($action == '') {
      return;
    }

    $this->build_sns_message($post, $action);
  }

  /**
   * Send SNS notification on post update if publishing
   *
   * Post updating needs to happen after saving so we can access the updated custom fields
   *
   * https://developer.wordpress.org/reference/functions/wp_insert_post/
   *
   * @since    1.0.0
   */
  public function wp_insert_post($post_id, $post, $update) {

    $post_type = $post->post_type;

    if ($post->post_status != 'publish') {
      return;
    }

    if (in_array($post_type, array('sl_us_homepage_order', 'sl_au_homepage_order', 'sl_nz_homepage_order'))) {
      $this->update_postmeta_order($post_type, $post_id);
    }

    $action = 'UPDATE_POST';
    $this->build_sns_message($post, $action);
    }

    /**
    * Send SNS notification on pre-post-update if it's a forecast article
    *
    * Post updating needs to happen before the actual update because we need to access
    * and update the previous swell events, if any
    *
    * https://developer.wordpress.org/reference/functions/wp_insert_post/
    *
    * @since    1.0.0
    */
    public function pre_post_update($post_id, $post_data) {

    $post = get_post($post_id);
    $post_type = $post->post_type;
    $action = 'UPDATE_POST';

    if ($post->post_status != 'publish') {
      return;
    }

    if (in_array($post_type, array('sl_us_homepage_order', 'sl_au_homepage_order', 'sl_nz_homepage_order'))) {
      $this->reset_postmeta_order($post_type, $post_id);
    }

    if (in_array($post->post_type, array(
          'sl_premium_analysis',
          'sl_realtime_forecast',
          'sl_seasonal_forecast',
          'sl_local_news'
          )
        )) {

      $swellEvents = get_field('parent_swell_event', $post_id, false);
      foreach($swellEvents as $swellEvent) {
        $this->build_sns_message($swellEvent, $action);
      }
    }

    $action = 'UPDATE_POST';
    $this->build_sns_message($post, $action);
    }

  /**
   * Send SNS notification when sorting posts
   *
   * Iterate over $_POST['order] array, load post and build/send sns message to topic
   *
   * ONLY for post type = 'post' and post_status = 'publish'
   *
   * @since    1.0.0
   */
  public function wp_send_sorted() {

    if (!isset($_POST['order'])) {
      return;
    }

    // Treat these sorted posts as an UPDATE
    $action = 'UPDATE_POST';

    // Sends an order array of post ids
    $str = $_POST['order'];
    parse_str($str, $orderArray);

      // iterate over post ids and build sns message for each
    foreach ($orderArray['post'] as $postId) {
      $post = get_post($postId, $output);
      $post_type = $post->post_type;
      $post_status = $post->post_status;
      // only for published 'post' articles not travel etc.
      if ($post_type == 'post' && $post_status == 'publish') {
        $this->build_sns_message($post, $action);
      }
    }
    return;
  }


  /**
   * Register the JavaScript for the admin area.
   *
   * @since    1.0.0
   */
  public function enqueue_scripts() {

    /**
     * This function is provided for demonstration purposes only.
     *
     * An instance of this class should be passed to the run() function
     * defined in Sl_Article_Update_Notifier_Loader as all of the hooks are defined
     * in that particular class.
     *
     * The Sl_Article_Update_Notifier_Loader will then create the relationship
     * between the defined hooks and the functions defined in this
     * class.
     */

    wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/sl-article-update-notifier-admin.js', array('jquery'), $this->version, false);

  }

  /**
   * @param $topic_arn
   * @param $msg
   */
  public function send_sns_message($topic_arn, $msg) {
    if ($topic_arn == '') {
      error_log('Error: No SNS topic arn is set in environment. Cannot post article update message.');
      return;
    }

    // Get region from SNS arn
    $region = '';
    $arn_pieces = explode(':', $topic_arn);
    if (count($arn_pieces) > 4) {
      $region = $arn_pieces[3];
    }

    if ($region == '') {
      error_log('Error: No SNS region could be pulled from topic arn. Cannot post article update message.');
      return;
    }

    // Get AWS SNS client
    // If we have local credentials, use them. Otherwise default to EC2 credentials.
    $args = array('region' => $region);
    if (getenv('AWS_ACCESS_KEY_ID') && getenv('AWS_SECRET_ACCESS_KEY')) {
      $args = array(
        'key' => getenv('AWS_ACCESS_KEY_ID'),
        'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        'region' => $region
      );
    }
    $client = SnsClient::factory($args);

    // Send SNS message
    $result = $client->publish(array(
      'TopicArn' => $topic_arn,
      'Message' => $msg,
    ));

    error_log('SNS output: ' . $result);
    error_log('send_sns_message within sl-article-update-notified plugin. '
      . '$msg: ' . $msg . ' $topic_arn: ' . $topic_arn);
  }


  /**
   * Builds SNS message for downstream consumption by feed-etl, travel-page-etl, and update-varnish-cache
   *
   * @param $post
   * @param $action
   */
  public function build_sns_message($post, $action) {
    // on "trash" status the $_POST values are not available, but $post->$post_type is for all statuses.
    $post_type = $post->post_type;

    switch ($post_type) {
      case 'post':
        // Include taxonomy/meta information within SNS notification
        $promotions = [];
        $primary_category = '';
        $categories = [];
        $series = [];
        $tags = [];
        $contest_news = false;

        $authorFetch = get_userdata($post->post_author);
        $cfAvatar = get_cupp_meta($authorFetch->ID);
        $gravatar = get_avatar_url($authorFetch->ID);
        $author = (object) array('name' => $authorFetch->display_name, 'iconUrl' => $cfAvatar ?: $gravatar);

        // tags
        foreach (wp_get_post_tags($post->ID, array("fields" => "all")) as $tag) {
          array_push($tags, $tag->slug);
        }

        // categories
        foreach (wp_get_post_categories($post->ID, array("fields" => "all")) as $cat) {
          array_push($categories, $cat->slug);
        }

        $wpseo_primary_term = new WPSEO_Primary_Term('category', $post->ID);
        if ($wpseo_primary_term) {
          $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
          $wpseo_primary_term = get_term($wpseo_primary_term);
          $primary_category = $wpseo_primary_term->slug;
        }

        // series
        foreach (wp_get_post_terms($post->ID, 'series', array("fields" => "all")) as $ser) {
          array_push($series, $ser->slug);
        }

        // promotions
        foreach (wp_get_post_terms($post->ID, 'promotion', array("fields" => "all")) as $promo) {
          array_push($promotions, $promo->slug);
        }

        // contest news
        $contest_news = has_term( '', 'sl_contest_regions',  $post->ID);

        $carouselId = get_field("carousel", $post->ID, false);
        $featuredMediaId = get_post_thumbnail_id($post->ID);
        $media = array(
          'type' => 'image',
          'feed1x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed1x') : '',
          'feed2x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed2x') : '',
          'promobox1x' => $carouselId ? wp_get_attachment_image_url($carouselId, 'promobox1x') : '',
          'promobox2x' => $carouselId ? wp_get_attachment_image_url($carouselId, 'promobox2x') : ''
        );

        $msg = json_encode([
          'id' => $post->ID,
          'post' => true,
          'action' => $action,
          'promotions' => $promotions,
          'categories' => $categories,
          'primary_category' => $primary_category,
          'series' => $series,
          'tags' => $tags,
          'contest_news' => $contest_news,
          'media' => $media,
          'author' => $author,
        ]);
        // Get SNS topic arn from environment
        $topic_arn = getenv('SNS_ARTICLE_UPDATE_TOPIC') ? getenv('SNS_ARTICLE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        break;

      case 'sl_contests':
        $msg = json_encode([
          'id' => $post->ID,
          'action' => $action,
          'contest' => true,
        ]);
        // Get SNS topic arn from environment
        $topic_arn = getenv('SNS_ARTICLE_UPDATE_TOPIC') ? getenv('SNS_ARTICLE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        break;

      case 'sl_contest_entries':
        $msg = json_encode([
          'id' => $post->ID,
          'action' => $action,
          'contest_entry' => true,
        ]);
        // Get SNS topic arn from environment
        $topic_arn = getenv('SNS_ARTICLE_UPDATE_TOPIC') ? getenv('SNS_ARTICLE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        break;

      case 'travel_page':
        $featuredMediaId = get_post_thumbnail_id();
        $media = array(
          'type' => 'image',
          'thumbnail' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'thumbnail') : '',
          'medium' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'medium') : '',
          'large' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'large') : '',
          'full' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'full') : ''
        );
        // get taxId from dynlocationtravel for use in DELETE_POST, 3rd param false ensure it returns just the string.
        $taxId = get_field("dynlocationtravel", $post->ID, false);
        $msg = json_encode([
          'id' => $post->ID,
          'taxId' => $taxId,
          'action' => $action,
          'media' => $media
        ]);
        // Get SNS topic arn from environment
        $topic_arn = getenv('SNS_ARTICLE_UPDATE_TOPIC') ? getenv('SNS_ARTICLE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        break;

      case 'page':
        $contest_details = false;
        $contest_details = has_term( '', 'sl_contest_regions',  $post->ID);
        $msg = json_encode([
          'id' => $post->ID,
          'contest_details' => $contest_details,
          'action' => $action
        ]);
        // Get SNS topic arn from environment
        $topic_arn = getenv('SNS_TRAVEL_PAGE_UPDATE_TOPIC') ? getenv('SNS_TRAVEL_PAGE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        break;

      case ($post_type == 'sl_premium_analysis' ||
            $post_type == 'sl_realtime_forecast' ||
            $post_type == 'sl_seasonal_forecast' ||
            $post_type == 'sl_local_news'):

        $authorFetch = get_userdata($post->post_author);
        $cfAvatar = get_cupp_meta($authorFetch->ID);
        $gravatar = get_avatar_url($authorFetch->ID);
        $author = (object) array('name' => $authorFetch->display_name, 'iconUrl' => $cfAvatar ?: $gravatar);

        // Force web wrap on mobile apps
        if ($post_type == 'sl_local_news') {
          $featuredMediaId = get_post_thumbnail_id($post->ID);
          $media = array(
            'type' => 'image',
            'feed1x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed1x') : '',
            'feed2x' => $featuredMediaId ? wp_get_attachment_image_url($featuredMediaId, 'feed2x') : ''
          );
        }

        $msg = json_encode([
          'id' => $post->ID,
          'forecast' => true,
          'forecastType' => $post_type,
          'author' => $author,
          'action' => $action,
          'media' => $media ?: null,
        ]);
        $topic_arn = getenv('SNS_ARTICLE_UPDATE_TOPIC') ? getenv('SNS_ARTICLE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        $swellEvents = get_field('parent_swell_event', $post->ID, false);
        foreach ($swellEvents as $swellEvent) {
          $msg = json_encode([
            'id' => $swellEvent,
            'event' => true,
            'action' => $action
          ]);
          $timezone  = -7; //(GMT -7:00)
          $now = gmdate("c", time() + 3600*($timezone+date("I")));
          $event = array(
            'ID'           => $swellEvent,
            'post_modified_gmt' => $now
          );

          // Update the post into the database
          wp_update_post($event);
          // Get SNS topic arn from environment
          $this->send_sns_message($topic_arn, $msg);
        }
        break;

      case 'sl_swell_event':
        $msg = json_encode([
          'id' => $post->ID,
          'event' => true,
          'action' => $action
        ]);
        // Get SNS topic arn from environment
        $topic_arn = getenv('SNS_ARTICLE_UPDATE_TOPIC') ? getenv('SNS_ARTICLE_UPDATE_TOPIC') : '';
        $this->send_sns_message($topic_arn, $msg);
        break;
    }
  }
  /**
   * Update/insert a record for `wp_postmeta` table
   * This record will be used by ElasticSearch to order posts in homepage page
   * If the record exists the order will be updated if not, it  will insert a new record
   *
   * @param $post_type
   * @param $post_id
   */
  public function update_postmeta_order($post_type, $post_id) {

    $meta_key = str_replace('sl_', '', $post_type);
    $posts = get_field($meta_key, $post_id, false);
    $resetedRecords = $this->get_reseted_records_in_hp_order($meta_key);
    $posts_to_update = array_unique(array_merge($posts, $resetedRecords));

    foreach ($posts as $index => $post_id) {
      $postmeta_id = $this->exist_record_in_table($post_id, $meta_key);
      $meta_id = $postmeta_id[0]->meta_id;

      if (isset($meta_id)) {
        update_post_meta($post_id, $meta_key, $index, '');
      } else {
        add_post_meta($post_id, $meta_key, $index, false);
      }
    }

    foreach ($posts_to_update as $post_id) {
      $post = get_post($post_id);
      $action = 'UPDATE_POST';
      $this->build_sns_message($post, $action);
    }
    // truncate table `wp_homepage_order`
    $this->truncate_hp_order();
  }
  /**
   * Reset records in `wp_postmeta` table
   *
   * @param $post_type
   * @param $post_id
   */
  public function reset_postmeta_order($post_type, $post_id) {

    $meta_key = str_replace('sl_', '', $post_type);
    $posts = get_field($meta_key, $post_id, false);

    foreach ($posts as $post_id) {
      $index = null;
      $postmeta_id = $this->exist_record_in_table($post_id, $meta_key);
      $meta_id = $postmeta_id[0]->meta_id;

      if (isset($meta_id)) {
        update_post_meta($post_id, $meta_key, $index, '');
        $this->add_hp_order($post_id, $meta_key, 'true');
      } else {
        add_post_meta($post_id, $meta_key, $index, false);
        $this->add_hp_order($post_id, $meta_key, 'true');
      }
    }
  }
  /**
   * Returns all the reseted records in `wp_postmeta` table by meta_key
   *
   * @param $meta_key
   */
  public function get_reseted_records_in_hp_order($meta_key) {

    global $wpdb;
    $table_name =  $wpdb->prefix . 'homepage_order';
    $sql = "SELECT post_id
              FROM $table_name
             WHERE meta_key = '$meta_key'
               AND meta_value = 'true'";
    $results = $wpdb->get_results($sql);
    // Turning Array Object into a multidimentional array
    $array = json_decode(json_encode($results), true);
    // Flatten a multidimentional array
    $objTmp = (object) array('aFlat' => array());
    array_walk_recursive($array, create_function('&$v, $k, &$t', '$t->aFlat[] = $v;'), $objTmp);
    $results = $objTmp->aFlat;
    return $results;
  }
  /**
   * Check if the record exists in `wp_postmeta` or in `wp_homepage_order` tables
   *
   * @param $post_id
   * @param $meta_key
   */
  public function exist_record_in_table($post_id, $meta_key)
  {

    global $wpdb;
    $table_name =  $wpdb->prefix . 'postmeta';
    $sql = "SELECT meta_id
              FROM $table_name
             WHERE post_id = '$post_id'
               AND meta_key = '$meta_key'";
    $results = $wpdb->get_results($sql);
    return $results;
  }
  /**
   * Insert record in `wp_homepage_order` table
   *
   * @param $post_id
   * @param $meta_key
   * @param $meta_value
   */
  public function add_hp_order($post_id, $meta_key, $meta_value)
  {

    global $wpdb;
    $table_name =  $wpdb->prefix . 'homepage_order';
    $wpdb->insert($table_name, array('post_id' => $post_id, 'meta_key' => $meta_key, 'meta_value' => $meta_value));
  }
  /**
   * Truncate `wp_homepage_order` table
   *
   */
  public function truncate_hp_order()
  {

    global $wpdb;
    $table_name =  $wpdb->prefix . 'homepage_order';
    $wpdb->query("TRUNCATE TABLE $table_name");
  }
}
