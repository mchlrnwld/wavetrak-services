<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://github.com/Surfline
 * @since      1.0.0
 *
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
