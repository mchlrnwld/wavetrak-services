<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://github.com/Surfline
 * @since             1.0.0
 * @package           Sl_Article_Update_Notifier
 *
 * @wordpress-plugin
 * Plugin Name:       Surfline Article Update Notifier
 * Plugin URI:        https://github.com/Surfline/surfline-web-editorial
 * Description:       The Surfline Article Update Notifier posts messages to SNS on article create/update/delete.
 * Version:           1.0.0
 * Author:            Matt Walker
 * Author URI:        https://github.com/Surfline
 * License:
 * License URI:
 * Text Domain:       sl-article-update-notifier
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
  die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sl-article-update-notifier-activator.php
 */
function activate_sl_article_update_notifier() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-sl-article-update-notifier-activator.php';
  Sl_Article_Update_Notifier_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sl-article-update-notifier-deactivator.php
 */
function deactivate_sl_article_update_notifier() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-sl-article-update-notifier-deactivator.php';
  Sl_Article_Update_Notifier_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_sl_article_update_notifier');
register_deactivation_hook(__FILE__, 'deactivate_sl_article_update_notifier');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-sl-article-update-notifier.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sl_article_update_notifier() {

  $plugin = new Sl_Article_Update_Notifier();
  $plugin->run();

}

run_sl_article_update_notifier();
