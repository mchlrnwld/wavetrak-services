<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://github.com/Surfline
 * @since      1.0.0
 *
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/includes
 * @author     Matt Walker <mwalker@surfline.com>
 */
class Sl_Article_Update_Notifier_Deactivator {

  /**
   * Short Description. (use period)
   *
   * Long Description.
   *
   * @since    1.0.0
   */
  public static function deactivate() {

  }

}
