<?php

/**
 * Fired during plugin activation
 *
 * @link       https://github.com/Surfline
 * @since      1.0.0
 *
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/includes
 * @author     Matt Walker <mwalker@surfline.com>
 */
class Sl_Article_Update_Notifier_Activator {

  /**
   * Short Description. (use period)
   *
   * Long Description.
   *
   * @since    1.0.0
   */
  public static function activate() {

  }

}
