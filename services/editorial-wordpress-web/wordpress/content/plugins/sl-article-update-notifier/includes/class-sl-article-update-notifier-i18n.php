<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://github.com/Surfline
 * @since      1.0.0
 *
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Sl_Article_Update_Notifier
 * @subpackage Sl_Article_Update_Notifier/includes
 * @author     Matt Walker <mwalker@surfline.com>
 */
class Sl_Article_Update_Notifier_i18n {


  /**
   * Load the plugin text domain for translation.
   *
   * @since    1.0.0
   */
  public function load_plugin_textdomain() {

    load_plugin_textdomain(
      'sl-article-update-notifier',
      false,
      dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
    );

  }


}
