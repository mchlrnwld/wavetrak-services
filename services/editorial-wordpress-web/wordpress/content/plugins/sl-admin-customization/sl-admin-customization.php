<?php
/*
Plugin Name: Surfline Admin Plugin
Description: Extension made for adding custom metaboxes to admin pages.
Version: 1.0
Author:  Ryan Oillataguerre
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

class WPAdmin_Custom_Meta_Box {
    public function __construct() {
        if ( is_admin() ) {
            add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
            add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
        }
    }
    public function init_metabox() {
        add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
        add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
    }
    public function add_metabox() {
        add_meta_box(
            'event-forecasts',
            __( 'Event Forecasts', 'textdomain' ),
            array( $this, 'render_metabox' ),
            'sl_swell_event',
            'advanced',
            'default'
        );
    }
 
    /**
     * Renders the meta box.
     */
    public function render_metabox( $post ) {
        $forecasts = get_posts(array(
            'post_type' => array(
              'sl_premium_analysis',
              'sl_realtime_forecast',
              'sl_seasonal_forecast',
              'sl_local_news'
            ),
            'meta_query' => array(
                array(
                    'key' => 'parent_swell_event',
                    'value' => '"' . $post->ID . '"',
                    'compare' => 'LIKE'
                )
            ),
            'posts_per_page' => -1,
        ));
        if ($forecasts) {
            // Order by date modified
            usort($forecasts, 'date_compare');
        }
        foreach ($forecasts as $forecast) {
            echo '<a href="' . get_home_url() . '/wp-admin/post.php?post=' . $forecast->ID . '&action=edit' . '">' . $forecast->post_title . '</a><br>';
        }
    }
    
    // Function to save state of metabox
    // Not necessary for our first metabox, but may be useful in the future
    public function save_metabox( $post_id, $post ) {
        // Add nonce for security and authentication.
        $nonce_name   = isset( $_POST['custom_nonce'] ) ? $_POST['custom_nonce'] : '';
        $nonce_action = 'custom_nonce_action';
 
        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }
 
        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
 
        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }
 
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }
    }
}
 
new WPAdmin_Custom_Meta_Box();