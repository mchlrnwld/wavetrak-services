#!/bin/sh

# Generate plugins bundle
tar czvf plugins.tgz \
  --exclude='.gitignore' \
  --exclude='bundle_plugins.sh' \
  --exclude='plugins-manifest.txt' \
  --exclude='plugins.tgz' \
  --exclude='sl-*' \
  --exclude='wordpress-seo' \
  .

# Generate plugin manifest from tarfile contents
tar tzvf plugins.tgz \
  | awk '{print $9}' \
  | sed -e 's/^\.\///' -e 's/\/.*//' -e '/^$/d' \
  | sort \
  | uniq \
  | tee plugins-manifest.txt
