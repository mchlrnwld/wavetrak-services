<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://github.com/Surfline
 * @since             1.0.0
 * @package           Sl_Thumbnail_Upscaler
 *
 * @wordpress-plugin
 * Plugin Name:       Surfline Thumbnail Upscaler
 * Plugin URI:        https://github.com/Surfline/surfline-web-editorial
 * Description:       The Surfline thumbnail upscaler works on small images
 * Version:           1.0.0
 * Author:            Surfline
 * Author URI:        https://github.com/Surfline
 * License:
 * License URI:
 * Text Domain:       sl-thumbnail-upscale
 *
 * Forked from http://wordpress.org/plugins/thumbnail-upscale/
 */


class SurflineThumbnailUpscaler
{
	/** http://wordpress.stackexchange.com/questions/50649/how-to-scale-up-featured-post-thumbnail **/
	static function image_resize_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop)
	{
    if(!$crop) {
      $size_ratio = $new_w / $orig_w;
      $new_h = $orig_h * $size_ratio;
    } else {
      $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);
    }

		$crop_w = round($new_w / $size_ratio);
		$crop_h = round($new_h / $size_ratio);

		$s_x = floor( ($orig_w - $crop_w) / 2 );
		$s_y = floor( ($orig_h - $crop_h) / 2 );

		if(is_array($crop)) {

			//Handles left, right and center (no change)
			if($crop[ 0 ] === 'left') {
				$s_x = 0;
			} else if($crop[ 0 ] === 'right') {
				$s_x = $orig_w - $crop_w;
			}

			//Handles top, bottom and center (no change)
			if($crop[ 1 ] === 'top') {
				$s_y = 0;
			} else if($crop[ 1 ] === 'bottom') {
				$s_y = $orig_h - $crop_h;
			}
		}

		return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}
}

add_filter('image_resize_dimensions', array('SurflineThumbnailUpscaler', 'image_resize_dimensions'), 10, 6);
