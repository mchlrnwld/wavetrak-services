<?php
/*
Plugin Name: Surfline AMP Custom Style Extension
Description: Extension made for AMP for WP to add custom styles to our theme.
Version: 1.0
Author:  Ryan Oillataguerre
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

add_action('ampforwp_after_post_content','amp_custom_extension_insert_view_comments');
function amp_custom_extension_insert_view_comments() { ?>
	<div class="sl-template-view-comments">
        <a href="./#sl-editorial-article-footer">
            <button class="sl-template-view-comments-btn">View Comments</button>
        </a>
	</div>
	<?php 
}

add_action('amp_post_template_css', 'amp_custom_extension_styling');
function amp_custom_extension_styling() { ?>
    .sl-template-view-comments {
        text-align: center;
        border-top: 1px solid #e5e5ea;
        border-bottom: 1px solid #e5e5ea;
        padding: 20px 0;
    }
    
    .sl-template-view-comments-btn {
        width: 50%;
        height: 40px;
        border-color: #42A5FC;
        border-width: 3px;
        background-color: #42A5FC;
        color: white;
        font-size: 16px;
    }
    .sl-template-view-comments-btn:hover {
        background-color: #2283DD;
        border-color: #2283DD;
        cursor: pointer;
    }
    button:focus {
        outline: none;
    }
<?php }
