import chai from 'chai';
import dirtyChai from 'dirty-chai';
import tests from './tests';

chai.use(dirtyChai);

describe('Surfline Web Editorial', () => {
  tests();
});
