import { expect } from 'chai';
import fetch from 'node-fetch';
import config from './config';
import postFixture from '../fixtures/post_fixture';

const tests = () => {
  describe('/posts', () => {
    describe('GET', () => {
      // Begin testing Posts by ID endpoint
      it('asserts that the posts endpoint exists', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/posts?id=1786`);
        expect(response.status).to.equal(200);
      });
      it('asserts 404 on incorrect post ID request', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/posts?id=17860`);
        expect(response.status).to.equal(404);
      });
      it('asserts that the post response equals the fixture', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/posts?id=1786`).then(res => res.json());
        Object.keys(response).map(key => expect(response[key]).to.deep.equal(postFixture[key]));
      });
      // End testing Posts by ID endpoint
    });
  });

  describe('/post/sponsor', () => {
    describe('GET', () => {
      // Begin testing Posts by ID endpoint
      it('asserts that the posts endpoint exists', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/post/sponsor?id=1808`);
        expect(response.status).to.equal(200);
      });
      it('asserts 404 on incorrect post ID request', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/post/sponsor?id=18080`);
        expect(response.status).to.equal(404);
      });
      it('asserts that the post response hideAds returns when set', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/post/sponsor?id=1786`).then(res => res.json());
        expect(response).to.deep.equal({ dfpKeyword: '', hideAds: true });
      });
      it('asserts that the post response dfpKeyword returns when set', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/post/sponsor?id=1808`).then(res => res.json());
        expect(response).to.deep.equal({ dfpKeyword: 'testCode', hideAds: false });
      });
      it('asserts 200 on draft post with preview param', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/posts?id=1805&preview=true&uuid=${Date.now()}`);
        expect(response.status).to.equal(200);
      });
      it('asserts that the post response equals the fixture', async () => {
        const response = await fetch(`${config.WORDPRESS}/wp-json/sl/v1/posts?id=1786`).then(res => res.json());
        Object.keys(response).map(key => expect(response[key]).to.deep.equal(postFixture[key]));
      });
      // End testing Post Sponsor by ID endpoint
    });
  });
};

export default tests;
