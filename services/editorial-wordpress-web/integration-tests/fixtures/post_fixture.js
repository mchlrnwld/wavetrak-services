export default {
  id: 1786,
  createdAt: "2018-10-26T20:12:55+00:00",
  updatedAt: "2019-03-14T20:18:13+00:00",
  status: "publish",
  permalink: "http://localhost:8080",
  premium: {
    premium: null,
    paywallHeading: null,
    paywallDescription: null,
    teaser: null,
    paywallPosts: null,
  },
  author: {
    name: "Surfline Development",
    iconUrl:
      "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/13123139/mick_container-150x150.jpg",
  },
  content: {
    title: "DO NOT EDIT THIS POST",
    subtitle: "c test",
    displayTitle: "displayTitle",
    body: "<p>This is the body</p>\n",
  },
  auContent: {
    title: "DO NOT EDIT THIS POST",
    subtitle: "c test",
    displayTitle: "displayTitle",
  },
  media: {
    type: "image",
    feed1x:
      "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/04/27151650/IMG_0689-780x438.jpg",
    feed2x:
      "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/04/27151650/IMG_0689-1560x877.jpg",
  },
  categories: [
    {
      name: "Contests",
      slug: "contests",
      url: "http://localhost:8080/category/contests",
      imageUrl: null,
      taxonomy: "category",
    },
    {
      name: "Features",
      slug: "features",
      url: "http://localhost:8080/category/features",
      imageUrl:
        "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084456/large01.jpg",
      taxonomy: "category",
    },
    {
      name: "Surf News",
      slug: "surf-news",
      url: "http://localhost:8080/category/surf-news",
      imageUrl:
        "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
      taxonomy: "category",
    },
  ],
  tags: [
    {
      name: "tag1",
      slug: "tag1",
      url: "http://localhost:8080/tag/tag1",
      taxonomy: "post_tag",
    },
    {
      name: "tag2",
      slug: "tag2",
      url: "http://localhost:8080/tag/tag2",
      taxonomy: "post_tag",
    },
    {
      name: "tag3",
      slug: "tag3",
      url: "http://localhost:8080/tag/tag3",
      taxonomy: "post_tag",
    },
  ],
  series: [
    {
      name: "Good to Epic",
      slug: "good-to-epic",
      url: "http://localhost:8080/series/good-to-epic",
      taxonomy: "series",
    },
    {
      name: "Sixty Seconds",
      slug: "sixty-seconds",
      url: "http://localhost:8080/series/sixty-seconds",
      taxonomy: "series",
    },
  ],
  contentPillar: [],
  promotions: [
    {
      name: "Carousel",
      slug: "carousel",
      url: "http://localhost:8080/surf-news/promotion/carousel",
      taxonomy: "promotion",
    },
    {
      name: "Contest Carousel",
      slug: "contest-carousel",
      url: "http://localhost:8080/surf-news/promotion/contest-carousel",
      taxonomy: "promotion",
    },
    {
      name: "Homepage Promo",
      slug: "homepage-promo",
      url: "http://localhost:8080/surf-news/promotion/homepage-promo",
      taxonomy: "promotion",
    },
  ],
  displayOptions: {
    hideComments: true,
    hideAds: true,
    hideAuthor: null,
  },
  sponsoredArticle: {
    sponsoredArticle: true,
    attributionText: "Presented By",
    sponsorName: "sponsor name",
    dfpKeyword: "",
    partnerContent: true,
    showAttributionInFeed: true,
  },
  externalLink: {
    externalSource: "Link Attribution",
    newWindow: false,
    externalUrl: "http://localhost:8080",
  },
  hero: {
    showHero: true,
    type: "Cover",
    imageUrl:
      "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/04/27151657/IMG_2857.jpg",
    location: true,
    align: {
      horizontal: "Right",
      vertical: "Bottom",
    },
    credit: {
      text: null,
      alignment: null,
    },
    gradient: null,
    hideTitle: null,
  },
  yoastMeta: {
    title: "DO NOT EDIT THIS POST",
    description: "c test",
    "og:type": "article",
    "og:site_name": "Surfline",
    "og:title": "Facebook Title",
    "og:image":
      "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/04/27151650/IMG_0689-1560x877.jpg",
    "og:description": "c test",
    "og:url": "http://localhost:8080",
    "og:updated_time": "2019-03-14T20:18:13+00:00",
    canonical: "http://localhost:8080",
    "article:section": "Contests",
    "article:published_time": "2018-10-26T20:12:55+00:00",
    "article:updated_time": "2019-03-14T20:18:13+00:00",
    "twitter:card": "c test",
    "twitter:title": "DO NOT EDIT THIS POST",
    "twitter:image":
      "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2018/04/27151650/IMG_0689-1560x877.jpg",
    "twitter:description": "c test",
    "twitter:url": "http://localhost:8080",
  },
  relatedPosts: [
    {
      id: "1156",
      title: "Cache test",
      subtitle: "cache",
      premium: null,
      permalink: "http://localhost:8080/surf-news/cache-test/1156",
      createdAt: "2017-07-19 16:01:35",
      categories: [
        {
          name: "Contests",
          slug: "contests",
          url: "http://localhost:8080/category/contests",
          imageUrl: null,
          taxonomy: "category",
        },
        {
          name: "Features",
          slug: "features",
          url: "http://localhost:8080/category/features",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084456/large01.jpg",
          taxonomy: "category",
        },
        {
          name: "Forecast",
          slug: "forecast",
          url: "http://localhost:8080/category/forecast",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084915/large03.jpg",
          taxonomy: "category",
        },
        {
          name: "Gear",
          slug: "gear",
          url: "http://localhost:8080/category/gear",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084456/large01.jpg",
          taxonomy: "category",
        },
        {
          name: "Science",
          slug: "science",
          url: "http://localhost:8080/category/science",
          imageUrl: null,
          taxonomy: "category",
        },
        {
          name: "Surf News",
          slug: "surf-news",
          url: "http://localhost:8080/category/surf-news",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
          taxonomy: "category",
        },
        {
          name: "Tips",
          slug: "tips",
          url: "http://localhost:8080/category/tips",
          imageUrl: null,
          taxonomy: "category",
        },
        {
          name: "Travel",
          slug: "travel",
          url: "http://localhost:8080/category/travel",
          imageUrl: null,
          taxonomy: "category",
        },
        {
          name: "Videos",
          slug: "videos",
          url: "http://localhost:8080/category/videos",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084923/large05.jpg",
          taxonomy: "category",
        },
      ],
      series: [],
      media: {
        type: "image",
        feed1x:
          "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/07/19160442/jacklead-780x519.jpg",
        feed2x:
          "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/07/19160442/jacklead-1560x1039.jpg",
      },
    },
    {
      id: "1832",
      title: "Paywall Posts Test",
      subtitle: "Subtitle",
      premium: true,
      permalink: "http://localhost:8080/surf-news/paywall-posts-test/1832",
      createdAt: "2019-04-26 10:28:03",
      categories: [
        {
          name: "Features",
          slug: "features",
          url: "http://localhost:8080/category/features",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084456/large01.jpg",
          taxonomy: "category",
        },
        {
          name: "Gear",
          slug: "gear",
          url: "http://localhost:8080/category/gear",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084456/large01.jpg",
          taxonomy: "category",
        },
        {
          name: "Science",
          slug: "science",
          url: "http://localhost:8080/category/science",
          imageUrl: null,
          taxonomy: "category",
        },
      ],
      series: [
        {
          name: "Sixty Seconds",
          slug: "sixty-seconds",
          url: "http://localhost:8080/series/sixty-seconds",
          taxonomy: "series",
        },
      ],
      media: {
        type: "image",
        feed1x: "",
        feed2x: "",
      },
    },
    {
      id: "1773",
      title: "test",
      subtitle: "test",
      premium: true,
      permalink: "http://localhost:8080/surf-news/test-2/1773",
      createdAt: "2018-10-26 11:45:34",
      categories: [
        {
          name: "Surf News",
          slug: "surf-news",
          url: "http://localhost:8080/category/surf-news",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
          taxonomy: "category",
        },
      ],
      series: [],
      media: {
        type: "image",
        feed1x: "",
        feed2x: "",
      },
    },
    {
      id: "1445",
      title: "New Post Test",
      subtitle: "Bowen testing",
      premium: null,
      permalink: "http://localhost:8080/surf-news/new-post-test/1445",
      createdAt: "2018-04-03 11:03:15",
      categories: [
        {
          name: "Surf News",
          slug: "surf-news",
          url: "http://localhost:8080/category/surf-news",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
          taxonomy: "category",
        },
      ],
      series: [],
      media: {
        type: "image",
        feed1x: "",
        feed2x: "",
      },
    },
    {
      id: "1440",
      title: "Teaser Tester",
      subtitle: "Hi",
      premium: true,
      permalink: "http://localhost:8080/surf-news/teaser-tester/1440",
      createdAt: "2018-04-02 15:37:15",
      categories: [
        {
          name: "Surf News",
          slug: "surf-news",
          url: "http://localhost:8080/category/surf-news",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
          taxonomy: "category",
        },
      ],
      series: [],
      media: {
        type: "image",
        feed1x: "",
        feed2x: "",
      },
    },
    {
      id: "1433",
      title: "This is a new test post for og data",
      subtitle: "test post",
      premium: null,
      permalink:
        "http://localhost:8080/surf-news/new-test-post-og-data-surfline/1433",
      createdAt: "2018-02-14 10:28:59",
      categories: [
        {
          name: "Surf News",
          slug: "surf-news",
          url: "http://localhost:8080/category/surf-news",
          imageUrl:
            "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/20084911/large02.jpg",
          taxonomy: "category",
        },
      ],
      series: [],
      media: {
        type: "image",
        feed1x: "",
        feed2x: "",
      },
    },
  ],
};
