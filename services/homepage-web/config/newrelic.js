let newRelicAppName = 'Surfline Web - Homepage';
if (process.env.APP_ENV !== 'production') {
  newRelicAppName += ` (${process.env.APP_ENV})`;
}

console.log(`New Relic enabled: ${process.env.NEW_RELIC_ENABLED}`);
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 *
 * https://github.com/newrelic/node-newrelic/blob/master/lib/config/default.js
 */
exports.config = {
  /**
   * @env NEW_RELIC_APP_NAME
   */

  app_name: [newRelicAppName],
  browser_monitoring: {
    /**
     * Enable browser monitoring header generation.
     *
     * This does not auto-instrument, rather it enables the agent to generate headers.
     * The newrelic module can generate the appropriate <script> header, but you must
     * inject the header yourself, or use a module that does so.
     *
     * Usage:
     *
     *     var newrelic = require('newrelic');
     *
     *     router.get('/', function (req, res) {
     *       var header = newrelic.getBrowserTimingHeader();
     *       res.write(header)
     *       // write the rest of the page
     *     });
     *
     * This generates the <script>...</script> header necessary for Browser Monitoring
     * This script must be manually injected into your templates, as high as possible
     * in the header, but _after_ any X-UA-COMPATIBLE HTTP-EQUIV meta tags.
     * Otherwise you may hurt IE!
     *
     * This method must be called _during_ a transaction, and must be called every
     * time you want to generate the headers.
     *
     * Do *not* reuse the headers between users, or even between requests.
     *
     * @env NEW_RELIC_BROWSER_MONITOR_ENABLE
     */
    enable: true,
  },
};
