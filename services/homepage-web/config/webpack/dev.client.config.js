/** @prettier */

import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import MiniCssExtractPlugin from 'extract-css-chunks-webpack-plugin';
import { ReactLoadablePlugin } from 'react-loadable/webpack';
import paths from '../paths';

const clientAssetPath = '//localhost:3001';
const { clientSrcPath, srcPath, buildPath, userRootPath } = paths;

export default () => ({
  mode: 'development',
  target: 'web',
  entry: [
    `webpack-hot-middleware/client?reload=true&path=${clientAssetPath}/__webpack_hmr`,
    'webpack/hot/only-dev-server',
    'react-hot-loader/patch',
    path.resolve(clientSrcPath, 'index.js'),
  ],
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
      react: require.resolve('react'),
    },
  },
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    path: buildPath,
    publicPath: 'http://localhost:3001/',
  },
  context: srcPath,
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    contentBase: buildPath,
    publicPath: 'http://localhost:3001',
  },
  optimization: {
    splitChunks: {
      name: 'manifest',
      minChunks: Infinity,
    },
    // Keep the runtime chunk separated to enable long term caching
    // https://twitter.com/wSokra/status/969679223278505985
    // https://github.com/facebook/create-react-app/issues/5358
    runtimeChunk: {
      name: (entrypoint) => `runtime-${entrypoint.name}`,
    },
  },
  module: {
    rules: [
      {
        test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
        loader: 'file-loader',
      },
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
        exclude: [/node_modules/, paths.buildPath],
      },
      {
        test: /\.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          'css-loader?modules&sourceMap',
          'resolve-url-loader',
        ],
      },
      {
        test: /\.scss$/,
        use: [
          { loader: MiniCssExtractPlugin.loader, options: { hot: true } },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 3,
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [autoprefixer({ grid: true })],
              },
            },
          },
          'resolve-url-loader',
          'sass-loader?sourceMap',
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new WebpackManifestPlugin({
      fileName: 'clientAssets.json',
      writeToFileEmit: true,
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        context: userRootPath,
        output: { path: buildPath },
      },
    }),

    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),

    new ReactLoadablePlugin({
      filename: path.resolve(buildPath, 'react-loadable.json'),
    }),
  ],
});
