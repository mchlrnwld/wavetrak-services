import { ReactLoadablePlugin } from 'react-loadable/webpack';
import isWsl from 'is-wsl';
import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import MiniCssExtractPlugin from 'extract-css-chunks-webpack-plugin';
import safePostCssParser from 'postcss-safe-parser';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import paths from '../paths';
import config from '../../src/config';

module.exports = () => {
  const { clientSrcPath, assetsBuildPath, buildPath } = paths;

  return {
    target: 'web',
    mode: 'production',
    devtool: 'source-map',

    entry: {
      main: [path.resolve(clientSrcPath, 'index.js')],
    },

    externals: {
      react: 'vendor.React',
      'react-dom': 'vendor.ReactDOM',
    },

    output: {
      path: assetsBuildPath,
      filename: '[name]-[chunkhash].js',
      chunkFilename: '[name]-[chunkhash].js',
      publicPath: config.cdn,
      // TODO: remove this when upgrading to webpack 5
      futureEmitAssets: true,
    },

    optimization: {
      minimize: true,
      minimizer: [
        // This is only used in production mode
        new TerserPlugin({
          terserOptions: {
            parse: {
              // We want terser to parse ecma 8 code. However, we don't want it
              // to apply any minification steps that turns valid ecma 5 code
              // into invalid ecma 5 code. This is why the 'compress' and 'output'
              // sections only apply transformations that are ecma 5 safe
              // https://github.com/facebook/create-react-app/pull/4234
              ecma: 8,
            },
            compress: {
              // Disabled because of an issue with Uglify breaking seemingly valid code:
              // https://github.com/facebook/create-react-app/issues/2376
              // Pending further investigation:
              // https://github.com/mishoo/UglifyJS2/issues/2011
              warnings: false,
              // Disabled because of an issue with Terser breaking valid code:
              // https://github.com/facebook/create-react-app/issues/5250
              // Pending further investigation:
              // https://github.com/terser-js/terser/issues/120
              comparisons: false,
              inline: 2,
            },
            mangle: {
              safari10: true,
            },
            output: {
              ecma: 5,
              comments: false,
              // Turned on because emoji and regex is not minified properly using default
              // https://github.com/facebook/create-react-app/issues/2488
              ascii_only: true,
            },
          },
          // Use multi-process parallel running to improve the build speed
          // Default number of concurrent runs: os.cpus().length - 1
          // Disabled on WSL (Windows Subsystem for Linux) due to an issue with Terser
          // https://github.com/webpack-contrib/terser-webpack-plugin/issues/21
          parallel: !isWsl,
          cache: true,
          sourceMap: true,
        }),
        // This is only used in production mode
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            parser: safePostCssParser,
            map: {
              // `inline: false` forces the sourcemap to be output into a
              // separate file
              inline: false,
              // `annotation: true` appends the sourceMappingURL to the end of
              // the css file, helping the browser find the sourcemap
              annotation: true,
            },
          },
        }),
      ],
      splitChunks: {
        name: 'manifest',
        minChunks: Infinity,
      },
      // Keep the runtime chunk separated to enable long term caching
      // https://twitter.com/wSokra/status/969679223278505985
      // https://github.com/facebook/create-react-app/issues/5358
      runtimeChunk: {
        name: (entrypoint) => `runtime-${entrypoint.name}`,
      },
    },
    module: {
      rules: [
        {
          test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
          loader: 'file-loader',
        },
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
            { loader: 'resolve-url-loader' },
          ],
        },
        {
          test: /\.scss$/,
          use: [
            { loader: MiniCssExtractPlugin.loader },
            { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [autoprefixer({ grid: true })],
                },
              },
            },
            'resolve-url-loader',
            'sass-loader?sourceMap',
          ],
        },
      ],
    },

    plugins: [
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: true,
        options: {
          context: process.cwd(),
          output: { path: buildPath },
        },
      }),

      new MiniCssExtractPlugin({
        filename: '[name]-[contenthash].css',
        chunkFilename: '[id]-[contenthash].css',
      }),

      new WebpackManifestPlugin({
        fileName: path.resolve(buildPath, 'clientAssets.json'),
        writeToFileEmit: true,
      }),

      new ReactLoadablePlugin({
        filename: path.resolve(buildPath, 'react-loadable.json'),
      }),
    ],
  };
};
