/** @prettier */

import path from 'path';
import webpack from 'webpack';
import nodeExternals from 'webpack-node-externals';
import paths from '../paths';
import config from '../../src/config';

const { serverSrcPath } = paths;

export default () => ({
  target: 'node',
  mode: 'development',
  node: {
    __dirname: false,
    __filename: false,
  },

  externals: nodeExternals(),

  entry: {
    main: [path.resolve(serverSrcPath, 'index.js')],
  },

  resolve: {
    alias: {
      react: require.resolve('react'),
      'react-dom': require.resolve('react-dom'),
    },
  },

  output: {
    path: paths.serverBuildPath,
    filename: '[name].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: config.cdn,
    libraryTarget: 'commonjs2',
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: 'css-loader',
      },
      {
        test: /\.scss$/,
        use: [{ loader: 'css-loader', options: { importLoaders: 1 } }, 'sass-loader'],
      },
    ],
  },

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
  ],
});
