import path from 'path';
import nodeExternals from 'webpack-node-externals';
import paths from '../paths';
import config from '../../src/config';

const { serverBuildPath, serverSrcPath } = paths;

export default () => ({
  target: 'node',
  mode: 'production',
  node: {
    __dirname: false,
    __filename: false,
  },

  externals: nodeExternals(),

  entry: {
    main: path.resolve(serverSrcPath, 'index.js'),
  },

  output: {
    path: serverBuildPath,
    filename: '[name].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: config.cdn,
    libraryTarget: 'commonjs2',
  },

  module: {
    rules: [
      {
        test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
        loader: 'file-loader',
      },
      {
        test: /\.css$/,
        use: ['css-loader'],
      },
      {
        test: /\.scss$/,
        use: [{ loader: 'css-loader', options: { importLoaders: 1 } }, 'sass-loader'],
      },
    ],
  },
});
