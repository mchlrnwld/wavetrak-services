/** @prettier */

import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import paths from '../paths';

export default () => {
  const { buildPath } = paths;
  return {
    node: {
      __dirname: true,
      __filename: true,
    },

    devtool: 'source-map',

    resolve: {
      extensions: ['.js', '.json'],
      alias: {
        'homepage-assets': path.resolve(__dirname, '../../src/assets'),
      }
    },

    plugins: [
      new webpack.DefinePlugin({
        'process.env.APP_ENV': JSON.stringify(process.env.APP_ENV),
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.CLIENT_PORT': JSON.stringify(process.env.CLIENT_PORT),
        SERVER_PORT: JSON.stringify(process.env.SERVER_PORT),
        CLIENT_PORT: JSON.stringify('8081'),
        PUBLIC_PATH: JSON.stringify('/public'),
        PUBLIC_DIR: JSON.stringify('./build/public'),
        ASSETS_MANIFEST: JSON.stringify(path.join(buildPath, 'clientAssets.json')),
      }),

      new webpack.LoaderOptionsPlugin({
        options: {
          postcss: [autoprefixer()],
          context: '/',
        },
      }),
    ],

    module: {
      rules: [
        {
          test: /\.html$/,
          loader: 'file?name=[name].[ext]',
        },
        {
          test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
          loader: 'file-loader',
        },
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: [/node_modules/, buildPath],
        },
      ],
    },
  };
};
