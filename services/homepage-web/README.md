# Surfline Web Homepage

## Developing

```
npm run dev
```

precommit hook runs `npm lint && npm test`.

## Bundling and Deploying

Building a non development distribution

```
make build
```

Running the non development distribution

```
npm start
```

### 

## Testing

Uses chai, mocha sinon and enzyme. Coverage w/ istanbul.

```
npm test
```

## Project Structure

```
  build/
  
  src/
    actions/
    client/
    common/
    components/
    config/
    containers/
    intl/
    public/
    reducers/
    routes/
    server/
    styles/
    stores/
    config/
    webpack/
  tools/
````

## Configuration

### Environment Variables

|Key|Description|
|---|-----------|
|`SERVER_PORT`|Port allocated for express. This serves the app.|
|`NODE_ENV`|The current environment. Valid options are `development`, `sandbox`, `staging`, and `production`.|
|`NEWRELIC_ENABLED`|Valid options are `true` or `false`.|

### Named Environment Configurations

Environment configurations are located in `src/config`. These variables are made
available to the client. Any sensitive keys should use an environment variable.

### Tool configurations

Configuration for build tools are located in `config/`.

## Internationalization

TODO


## Performance and Monitoring

Instruments...
* New Relic APM
* New Relic Browser

### Server Side Rendering Performance

TODO. Future Work...

Caching and profiling is setup. http://www.electrode.io/docs/server_side_render_cache.html
Render above the fold https://github.com/electrode-io/above-the-fold-only-server-render

## Analytics

Segment is setup.

### Tracking Events in Segment

Redux middleware to handle segment tracking.

## Minimum Software Requirements
* Node v6.9.2
* NPM 3.10.9
* React 15.4
* Redux 3
* Quiver
* Sass
* Webpack 2

## Minimum Browser Requirements
* Chrome latest
* FF latest
* Safari 8
* Edge, IE 11
* Android 4.0.3 (Ice Cream Sandwich)
* Mobile Safari iOS 8

### Editor Support

TODO

#### Visual Studio Code

TODO
