import chalk from 'chalk';

const log = console.log;
const logger = {
  info: msg => log(chalk.yellow(msg)),
  warning: msg => log(chalk.yellow(msg)),
  error: msg => log(chalk.red(msg)),
  success: msg => log(chalk.success(msg)),
};

export default logger;
