import { useEffect, useMemo } from 'react';

/**
 * @Description A simple hook that allows dispatching a redux action
 * based on a set or arguments and a condition. This hook will run everytime
 * the values of `condition` and any elements of `args` fail an equality check
 *
 * @param {{
 *   action: Function
 *   momoizedArgs?: Array
 *   condition?: Boolean
 *   mountOnly?: Boolean
 * }} props { action: Function, args = [], condition = true }
 * @param props.mountOnly This should be set to true if you want the dependencies array
 * to be an empty array `[]` which will result in the hook only being run on mount (and
 * it's cleanup function on unmount if it exists)
 */
export const useReduxAction = ({
  action,
  memoizedArgs = false,
  condition = true,
  mountOnly = false,
}) => {
  const defaultArgsArray = useMemo(() => [], []);
  const args = memoizedArgs || defaultArgsArray;
  useEffect(
    () => {
      if (condition) {
        action(...args);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    mountOnly ? [] : [action, args, condition]
  );
};
