/** @prettier */
import { useEffect } from 'react';
import { useToggle } from './useToggle';

/**
 * Simple Hook which will initiate a `framer-motion` animation
 * when the `start` property becomes `true` it will then call
 * the optional callback with the value `{complete: true}`
 *
 * @param {Object} props
 * @param {function} props.controls
 * @param {Object|string} props.variant
 * @param {boolean} props.start
 * @param {Array} [props.reset] Array of properties that should allow the animation to reset
 * @param {Function} [props.cb]
 */
export const useFramerAnimation = ({ controls, variant, start, reset = false, cb = false }) => {
  const [animationComplete, setAnimationComplete] = useToggle(false);

  useEffect(() => {
    async function run() {
      if (start) {
        await controls.start(variant);
        if (cb) {
          cb({ complete: true });
        }
        setAnimationComplete(true);
      }
    }
    run();
  }, [cb, controls, setAnimationComplete, start, variant, reset]);

  return [animationComplete, setAnimationComplete];
};
