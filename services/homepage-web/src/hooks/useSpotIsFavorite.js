import { getUserFavorites } from '@surfline/web-common';
import { useMemo } from 'react';
import { shallowEqual, useSelector } from 'react-redux';

/**
 * @param {string} spotId
 */
export const useSpotIsFavorite = (spotId) => {
  const favorites = useSelector(getUserFavorites, shallowEqual);
  const isFavorite = useMemo(
    () => !!favorites.find((favorite) => favorite._id === spotId),
    [favorites, spotId]
  );

  return isFavorite;
};
