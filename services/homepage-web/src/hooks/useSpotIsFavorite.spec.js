import React from 'react';
import { expect } from 'chai';

import { useSpotIsFavorite } from './useSpotIsFavorite';
import { mountWithReduxAndRouter } from '../utils/test-utils';

describe('useSpotIsFavorite', () => {
  const spot = { _id: '123' };
  const favorites = [{ _id: '123' }];

  const Component = () => {
    const isFavorite = useSpotIsFavorite(spot._id);

    return <div className="favorite">{`${isFavorite}`}</div>;
  };

  it('should return true if a spot is a favorite', () => {
    const { wrapper } = mountWithReduxAndRouter(
      Component,
      {},
      { initialState: { backplane: { user: { favorites } } } }
    );
    const text = wrapper.text();

    expect(text).to.equal('true');
  });
});
