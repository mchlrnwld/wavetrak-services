/** @prettier */

import { useCallback, useEffect } from 'react';
import { isString } from 'lodash';
import { useEventListener } from './useEventListener';

/**
 * @description Simple validation for the keys to ensure correct usage
 *
 * @param {Array<string>} keys
 */
const validateKeys = (keys) => {
  keys.forEach((key) => {
    if (!isString(key)) {
      console.error(
        `The keys passed into useKeyPress must be strings or else the event will never fire. Please check the following key: ${key}`
      );
    }
  });
};

/**
 * @description Given a memoized callback and a key to listen for, this hook will call
 * the memoized callback everytime the given key is pressed.
 *
 * @param {function} cb callback to call when the key is pressed (ensure the reference does not change every render)
 * @param {...string} keys Name of the key to listen for https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values
 */
export const useKeyPress = (cb, ...keys) => {
  // Ensure the keys are strings
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => validateKeys(keys), []);

  // We create a memoized callback so as to not trigger `useEventListener` more than needed
  const onKeyPress = useCallback(
    (event) => {
      if (!event.shiftKey && keys.includes(event.key)) {
        cb(event.key);
      }
    },
    [cb, keys]
  );

  // use hook to handle creating/removing the event listeners
  useEventListener(onKeyPress, 'keydown');
};
