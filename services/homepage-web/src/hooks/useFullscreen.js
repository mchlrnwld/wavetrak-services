import fscreen from 'fscreen';
import { useCallback, useState } from 'react';
import { useEventListener } from './useEventListener';

const useFullscreen = (refObj) => {
  const ref = refObj?.current;
  const [isFullscreen, setIsFullscreen] = useState(false);

  const toggleFullscreen = async () => {
    if (fscreen.fullscreenElement !== ref) {
      // this prevents the state change when there's an error (doesn't work on safari though)
      await fscreen.requestFullscreen(ref);
      setIsFullscreen(true);
    } else {
      await fscreen.exitFullscreen();
      setIsFullscreen(false);
    }
  };

  const onFullscreenChange = useCallback(() => {
    setIsFullscreen(fscreen.fullscreenElement === ref);
  }, [ref, setIsFullscreen]);
  useEventListener(onFullscreenChange, 'fullscreenchange', fscreen);

  return [isFullscreen, toggleFullscreen];
};

export default useFullscreen;
