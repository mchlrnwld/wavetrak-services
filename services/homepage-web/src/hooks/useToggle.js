import { useState, useRef } from 'react';

/**
 * @description Simple hook for encapsulating toggle states
 *
 * @param {boolean} [defaultState=false]
 *
 * @returns {[ boolean, setToggle, Function ]} [isToggledOn, doToggle, toggleOff]
 */
export const useToggle = (defaultState = false) => {
  const [isToggledOn, setToggle] = useState(defaultState);

  const doToggle = useRef(() => setToggle((curr) => !curr));
  return [isToggledOn, setToggle, doToggle.current];
};
