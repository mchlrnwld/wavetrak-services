import React from 'react';
import { once } from 'lodash';
import { createBrowserHistory } from 'history';
import { SWRConfig } from 'swr';
import { ConnectedRouter } from 'connected-react-router';
import { hydrate, unmountComponentAtNode } from 'react-dom';
import Loadable from 'react-loadable';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';

import {
  createSplitFilter,
  setWavetrakFeatureFrameworkCB,
  setWavetrakSplitFilters,
  getWavetrakSplitFilters,
} from '@surfline/web-common';

import configureStore from '../stores';
import * as treatments from '../common/treatments';

import '../styles/base.scss';

const TWENTY_MIN_DEDUPE_INTERVAL = 1200000;
const root = document.querySelector('#root');
const preloadedState = window.__DATA__; // eslint-disable-line no-underscore-dangle

// Create a history of your choosing (we're using a browser history in this case)
const history = createBrowserHistory();

const store = configureStore(preloadedState, history);

window.wavetrakStore = store;

const doHydrate = () => {
  // eslint-disable-next-line global-require,import/newline-after-import
  const Routes = hot(module)(require('../components/Routes').default);

  Loadable.preloadReady().then(() => {
    hydrate(
      <CookiesProvider>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <SWRConfig
              value={{
                shouldRetryOnError: false,
                revalidateOnFocus: false,
                dedupingInterval: TWENTY_MIN_DEDUPE_INTERVAL,
              }}
            >
              <React.StrictMode>
                <Routes />
              </React.StrictMode>
            </SWRConfig>
          </ConnectedRouter>
        </Provider>
      </CookiesProvider>,
      root
    );
  });
};

setWavetrakFeatureFrameworkCB(
  once(() => {
    doHydrate();
  })
);

const mount = async () => {
  try {
    // Get the list on enabled splits and create the list of treatment names to load
    const splitFilters = createSplitFilter(treatments);

    // Set the treatment names on the window so that backplane can access them
    setWavetrakSplitFilters([...splitFilters, ...getWavetrakSplitFilters()]);
  } catch (error) {
    window.newrelic?.noticeError(error);
  }
};

mount();

if (module.hot) {
  module.hot.accept('../components/Routes', () => {
    unmountComponentAtNode(root);
    doHydrate();
  });
}
