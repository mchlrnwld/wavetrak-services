/* eslint-disable no-console */

import newrelic from 'newrelic';
import express from 'express';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import Loadable from 'react-loadable';
import { CookiesProvider } from 'react-cookie';
import { createMemoryHistory } from 'history';
import { matchRoutes } from 'react-router-config';
import { renderFetch, hydrate, tokenHandler, getClientIp } from '@surfline/web-common';

import path from 'path';
import React from 'react';
import { Helmet } from 'react-helmet';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';

import { getBundles } from 'react-loadable/webpack';
import cookiesMiddleware from 'universal-cookie-express';

import skipOnboardingForUserWithFavorites from '../utils/skipOnboardingForUserWithFavorites';
import { getStatusCode } from '../selectors/status';
import template from './template';
import Routes from '../components/Routes';
import setupRoutes from './setupRoutes';
import config from '../config';
import logger from '../common/logger';
import { doExternalFetch } from '../common/baseFetch';

import createStore from '../stores';

const clientAssets = require(ASSETS_MANIFEST); // eslint-disable-line import/no-dynamic-require, no-undef, max-len

const serverPort = process.env.SERVER_PORT ? parseInt(process.env.SERVER_PORT, 10) : 8080;
const log = logger('web-homepage');

function getBackplaneOptions(pathname, query) {
  if (pathname.indexOf('/contests/spirit-of-surfing/cuervo-challenge') > -1) {
    return { 'renderOptions[bannerAd]': 'contest_sos' };
  }

  if (pathname.indexOf('/contests/wave-of-the-winter/north-shore/2020-2021') > -1) {
    return { 'renderOptions[bannerAd]': 'wotw_2021' };
  }

  if (pathname.indexOf('/contests/wave-of-the-winter/regional/2020-2021') > -1) {
    return { 'renderOptions[bannerAd]': 'wotw_regional_2021' };
  }

  if (
    pathname.indexOf('/premium-benefits') > -1 ||
    pathname.indexOf('/contests') > -1 ||
    pathname.indexOf('/surf-cams') > -1
  ) {
    return {
      'renderOptions[bannerAd]': false,
    };
  }

  if (pathname.indexOf('/search') > -1) {
    let searchRenderOpts = { 'renderOptions[bannerAd]': 'search_results' };
    if (query.qaFlag) {
      searchRenderOpts = { ...searchRenderOpts, 'renderOptions[qaFlag]': query.qaFlag };
    }
    return searchRenderOpts;
  }

  let defaultRenderOpts = {
    'renderOptions[largeFavoritesBar]': 'true',
    'renderOptions[bannerAd]': 'homepage',
    'renderOptions[showAdBlock]': 'true',
  };

  if (query.qaFlag) {
    defaultRenderOpts = { ...defaultRenderOpts, 'renderOptions[qaFlag]': query.qaFlag };
  }

  return defaultRenderOpts;
}

/**
 * SA-3062: reset anonymous IDs
 * @param {import('express').Request} req
 */
const resetAnonymousIdCookie = (req) => {
  const hasExistingResetCookie = req.cookies.sl_reset_anonymous_id_v3;

  if (!hasExistingResetCookie) {
    // Remove the cookie from the req object so it does not get sent to backplane
    delete req.cookies.ajs_anonymous_id;
  }
  return !hasExistingResetCookie;
};

const server = (loadableManifest, routes) => {
  const app = express();

  app.set('query parser', 'simple');
  app.enable('trust proxy');
  app.disable('x-powered-by');
  app.use(compression());
  app.use(cookieParser());
  app.use(cookiesMiddleware());

  app.get('/health', (req, res) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    })
  );

  app.use('/homepage/static', express.static(path.join(process.cwd(), './build/public')));
  app.use('/favicon.ico', express.static(path.join(process.cwd(), './src/public/favicon.ico')));
  app.use(tokenHandler(`${config.servicesURL}/trusted`, log));
  app.use('/contests/oembed', async (req, res) => {
    try {
      const { url } = req.query;
      const fbAccessToken = process.env.FB_ACCESS_TOKEN;
      let embedUrl = url;
      if (fbAccessToken && url.indexOf('insta') > -1) {
        embedUrl = `${url}&access_token=${fbAccessToken}`;
      }
      const response = await doExternalFetch(embedUrl);
      return res.json(response);
    } catch (err) {
      let errorMessage = 'Error fetching video. Please try again.';
      if (err.statusCode === 404) errorMessage = 'Error: Video not found';
      return res.status(err.statusCode).send(errorMessage);
    }
  });

  app.use(async (req, res) => {
    const shouldResetAnonymousIdCookie = resetAnonymousIdCookie(req);

    try {
      const { qaFlag } = req.query;
      const backplaneOpts = getBackplaneOptions(req.path, req.query);
      const opts = {
        ...(req.userId ? { userId: req.userId } : null),
        ...(req.cookies.ajs_anonymous_id
          ? { anonymousId: JSON.parse(req.cookies.ajs_anonymous_id) }
          : null),
        bundle: 'margins',
        'renderOptions[mvpForced]': req.headers['x-surfline-mvp-forced'],
        apiVersion: 'v2',
        accessToken: req.cookies.access_token ?? null,
        ...backplaneOpts,
      };

      if (qaFlag) {
        opts['renderOptions[qaFlag]'] = qaFlag;
      }

      const backplane = await renderFetch(process.env.BACKPLANE_HOST, getClientIp(req), opts);
      const history = createMemoryHistory({
        initialEntries: [req.url],
      });
      // Create a store with the backplane state hydrated
      const store = createStore(backplane.data.redux, history);

      // Iterate all routes matching the URL and get the components and params to send to hydrate
      const matchedRoutes = matchRoutes(routes, req.path);
      const params = Object.assign({}, ...matchedRoutes.map(({ match }) => match.params));
      const components = matchedRoutes.map(({ route }) => route.component);
      await hydrate(store, { components, params }, req, backplane.data.api);
      await skipOnboardingForUserWithFavorites(store);
      const initialState = store.getState();

      // React Router uses the context prop to return any redirects resulting from the render
      const context = {};
      const modules = [];

      const root = renderToString(
        <CookiesProvider cookies={req.universalCookies}>
          <Provider store={store}>
            <StaticRouter location={req.url} context={context}>
              <Loadable.Capture report={(moduleName) => modules.push(moduleName)}>
                <Routes />
              </Loadable.Capture>
            </StaticRouter>
          </Provider>
        </CookiesProvider>
      );

      const loadableBundles = getBundles(loadableManifest, modules);

      // Handle the 404 route
      if (context.status === 404) {
        return res.status(404).send('Not Found');
      }
      // Handle redirect status
      if (context.url) {
        return res.redirect(302, `${context.url}`);
      }

      const head = Helmet.rewind();
      const showFooter =
        req.path.indexOf('/surf-cams') === -1 && req.path.indexOf('/contests') === -1;
      return res.status(getStatusCode(initialState)).send(
        template({
          root,
          jsBundle: clientAssets['main.js'],
          manifest: clientAssets['manifest.js'],
          clientAssets,
          initialState: JSON.stringify(initialState),
          identity: req.identity,
          backplane,
          head,
          config,
          loadableBundles,
          showFooter,
          shouldResetAnonymousIdCookie,
        })
      );
    } catch (err) {
      console.log(err);
      newrelic.noticeError(err);
      return res.status(500).send('Something went wrong');
    }
  });

  app.use((err, req, res) => {
    if (err) {
      console.log(err);
      newrelic.noticeError(err);
    }
    return res.status(500).send('Something went wrong');
  });

  return app;
};

Promise.all([
  // eslint-disable-next-line import/no-unresolved
  import('../../build/react-loadable.json'),
  setupRoutes(),
  Loadable.preloadAll(),
])
  .then(([loadableManifest, routes]) => {
    server(loadableManifest, routes).listen(serverPort, () => {
      console.log(`Express Server listening on ${serverPort}`);
    });
  })
  .catch((err) => {
    console.error(err);
    console.log('Exiting...');
    process.exit(-1);
  });
