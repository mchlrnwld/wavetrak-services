import HomePageContainer from '../containers/routes/HomepagePageContainer';
import routesConfig from '../routesConfig';

const setupRoutes = async () => [
  {
    component: HomePageContainer,
    routes: await Promise.all(
      routesConfig.map(async ({ routeProps, loader }) => ({
        ...routeProps,
        component: (await loader()).default,
      }))
    ),
  },
];

export default setupRoutes;
