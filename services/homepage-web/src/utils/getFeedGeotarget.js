import { getUserSettings, getUser } from '@surfline/web-common';

const getFeedGeotarget = (state) => {
  const user = getUser(state);
  let geotarget = user && user?.countryCode ? user?.countryCode : 'US';
  const settings = getUserSettings(state);
  if (settings && settings?.feed && settings?.feed?.localized) {
    const localized = settings?.feed?.localized;
    geotarget = localized !== 'LOCALIZED' ? localized : geotarget;
  }

  return geotarget;
};

export default getFeedGeotarget;
