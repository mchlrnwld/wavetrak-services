import { getUserType } from '@surfline/web-common';

const adConfigMap = {
  takeover_box: {
    adUnit: '/1024858/Box',
    adId: 'Box',
    adSizes: [[300, 250]],
    adSizeMappings: [[[0, 0], [[300, 250]]]],
    adClass: 'sl-takeover__box',
    adViewType: 'HOMEPAGE',
  },
  feed: {
    adUnit: '/1024858/SL_HP_RR',
    adId: 'SL_HP_RR',
    adSizes: [[300, 600]],
    adSizeMappings: [
      [[1022, 200], [[300, 600]]], // tablet-landscape -> desktop-xl-width
    ],
    adClass: 'sl-feed-ad__ad-item',
    adViewType: 'HOMEPAGE',
  },
  feed_mobile: {
    adUnit: '/1024858/SL_HP_RR',
    adId: 'SL_HP_RR_mobile',
    adSizes: [[300, 250]],
    adSizeMappings: [
      [[0, 0], [[300, 250]]], // small mobile -> tablet-landscape
    ],
    adClass: 'sl-feed-ad__ad-item',
    adViewType: 'HOMEPAGE',
  },
  contest_sos_top: {
    adUnit: '/1024858/SL_Cuervo_Top',
    adId: 'SL_Cuervo_Top',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_sos_mid: {
    adUnit: '/1024858/SL_Cuervo_MidPage',
    adId: 'SL_Cuervo_MidPage',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_sos_bottom: {
    adUnit: '/1024858/SL_Cuervo_Bottom',
    adId: 'SL_Cuervo_Bottom',
    adSizes: [
      [300, 250],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_top_2019: {
    adUnit: '/1024858/SL_WOTW_Top',
    adId: 'SL_WOTW_Top',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_mid_2019: {
    adUnit: '/1024858/SL_WOTW_MidPage',
    adId: 'SL_WOTW_MidPage',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_bottom_2019: {
    adUnit: '/1024858/SL_WOTW_Bottom',
    adId: 'SL_WOTW_Bottom',
    adSizes: [
      [300, 250],
      [728, 90],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_top_2019: {
    adUnit: '/1024858/SL_WOTW_Regional_Top',
    adId: 'SL_WOTW_Regional_Top',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_mid_2019: {
    adUnit: '/1024858/SL_WOTW_Regional_Mid',
    adId: 'SL_WOTW_Regional_Mid',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_bottom_2019: {
    adUnit: '/1024858/SL_WOTW_Regional_Bottom',
    adId: 'SL_WOTW_Regional_Bottom',
    adSizes: [
      [300, 250],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_top_2020: {
    adUnit: '/1024858/SL_WOTW_Top20',
    adId: 'SL_WOTW_Top20',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_mid_2020: {
    adUnit: '/1024858/SL_WOTW_MidPage20',
    adId: 'SL_WOTW_MidPage20',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_bottom_2020: {
    adUnit: '/1024858/SL_WOTW_Bottom20',
    adId: 'SL_WOTW_Bottom20',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_top_2020: {
    adUnit: '/1024858/SL_WOTW_Regional_Top20',
    adId: 'SL_WOTW_Regional_Top20',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_mid_2020: {
    adUnit: '/1024858/SL_WOTW_Regional_Mid20',
    adId: 'SL_WOTW_Regional_Mid20',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_bottom_2020: {
    adUnit: '/1024858/SL_WOTW_Regional_Bottom20',
    adId: 'SL_WOTW_Regional_Bottom20',
    adSizes: [
      [300, 250],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_top_2021: {
    adUnit: '/1024858/SL_WOTW_Top21',
    adId: 'SL_WOTW_Top21',
    adSizes: [
      [300, 250],
      [728, 90],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_mid_2021: {
    adUnit: '/1024858/SL_WOTW_MidPage21',
    adId: 'SL_WOTW_MidPage21',
    adSizes: [
      [300, 250],
      [728, 90],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_bottom_2021: {
    adUnit: '/1024858/SL_WOTW_Bottom21',
    adId: 'SL_WOTW_Bottom21',
    adSizes: [
      [300, 250],
      [728, 90],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_top_2021: {
    adUnit: '/1024858/SL_WOTW_Regional_Top21',
    adId: 'SL_WOTW_Regional_Top21',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_mid_2021: {
    adUnit: '/1024858/SL_WOTW_Regional_Mid21',
    adId: 'SL_WOTW_Regional_Mid21',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  contest_regional_bottom_2021: {
    adUnit: '/1024858/SL_WOTW_Regional_Bottom21',
    adId: 'SL_WOTW_Regional_Bottom21',
    adSizes: [
      [300, 250],
      [970, 250],
    ],
    adSizeDesktop: [970, 250],
    adSizeMobile: [300, 250],
    adViewType: 'CONTEST',
  },
  homepage_feed_top: {
    adUnit: '/1024858/SL_HP_Feed_Top',
    adId: 'SL_HP_Feed_Top',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeMappings: [
      [
        [1256, 90],
        [728, 90],
      ],
      [
        [976, 250],
        [300, 250],
      ],
      [
        [728, 90],
        [728, 90],
      ],
      [
        [0, 0],
        [300, 250],
      ],
    ],
    adViewType: 'FEED',
  },
  homepage_feed_bottom: {
    adUnit: '/1024858/SL_HP_Feed_Bottom',
    adId: 'SL_HP_Feed_Bottom',
    adSizes: [
      [300, 250],
      [728, 90],
    ],
    adSizeMappings: [
      [
        [1256, 90],
        [728, 90],
      ],
      [
        [976, 250],
        [300, 250],
      ],
      [
        [728, 90],
        [728, 90],
      ],
      [
        [0, 0],
        [300, 250],
      ],
    ],
    adViewType: 'FEED',
  },
  homepage_bottom: {
    adUnit: '/1024858/SL_HP_Bottom',
    adId: 'SL_HP_Bottom',
    adSizes: [
      [970, 250],
      [970, 90],
      [728, 90],
      [300, 250],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [
          [970, 250],
          [970, 90],
          [728, 90],
        ],
      ],
      [
        [728, 90],
        [728, 90],
      ],
      [
        [0, 0],
        [300, 250],
      ],
    ],
    adViewType: 'FEED',
  },
  sponsoredArticle: {
    adUnit: '/1024858/SL_Edit_Logo_White',
    adId: 'SL_Edit_Logo_White',
    adSizes: [[100, 45]],
    adSizeDesktop: [100, 45],
    adSizeMobile: [100, 45],
    adViewType: 'CONTENT_ARTICLE',
  },
  'sl-multi-cam-companion': {
    adUnit: '/1024858/Position2',
    adId: 'Position2',
    adSizes: [[300, 250]],
    adSizeDesktop: [300, 250],
    adSizeMobile: [300, 250],
    adViewType: 'MULTI_CAM',
    adClass: 'sl-multi-cam-companion',
  },
  'sl-multi-cam-companion-mobile': {
    adUnit: '/1024858/Position2',
    adId: 'Position2',
    adSizes: [[320, 50]],
    adSizeDesktop: [320, 50],
    adSizeMobile: [320, 50],
    adViewType: 'MULTI_CAM',
    adClass: 'sl-multi-cam-companion-mobile',
  },
};

const loadAdConfig = (adIdentifier, adTargets = [], entitlements = [], isUser = false) => ({
  ...adConfigMap[adIdentifier],
  adTargets: [...adTargets, ['usertype', getUserType(entitlements, isUser)]],
});

export default loadAdConfig;
