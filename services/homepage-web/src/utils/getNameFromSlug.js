const getNameFromSlug = (str) =>
  str.replace(/(-|^)([^-]?)/g, (_, prep, letter) => (prep && ' ') + letter.toUpperCase());

export default getNameFromSlug;
