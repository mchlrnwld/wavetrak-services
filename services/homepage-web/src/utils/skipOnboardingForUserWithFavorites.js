import { getUserSettings, getUserFavorites } from '@surfline/web-common';
import { doSkipUserOnboarding } from '../actions/user';

const skipOnboardingForUserWithFavorites = async (store) => {
  const state = store.getState();
  const settings = getUserSettings(state);
  const favorites = getUserFavorites(state);
  if (
    settings &&
    (!settings.onboarded || !settings.onboarded.includes('FAVORITE_SPOTS')) &&
    favorites &&
    favorites.length > 0
  ) {
    await store.dispatch(doSkipUserOnboarding());
  }
};

export default skipOnboardingForUserWithFavorites;
