/** @prettier */

import React from 'react';
import { mount, shallow } from 'enzyme';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
// eslint-disable-next-line no-unused-vars
import { createMemoryHistory, MemoryHistory } from 'history';
// eslint-disable-next-line no-unused-vars
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Router } from 'react-router-dom';
import { ErrorBoundary } from '@surfline/quiver-react';

import defaultState from './fixtures/store';

// eslint-disable-next-line react/prop-types
const Wrapper = ({ children, store, history }) => (
  <Provider store={store}>
    <Router history={history}>
      <ErrorBoundary>{children}</ErrorBoundary>
    </Router>
  </Provider>
);

/**
 * @param {React.ReactNode} Component
 * @param {React.Props} props
 * @param {{
 *     initialState: {},
 *     route: String,
 *     store: MockStoreEnhanced,
 *     history: MemoryHistory
 *   }} options
 */
export const mountWithReduxAndRouter = (
  Component,
  props,
  {
    initialState = defaultState,
    store = configureStore([thunk])(initialState),
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {}
) => {
  const wrapper = mount(<Component {...props} />, {
    wrappingComponent: Wrapper,
    wrappingComponentProps: {
      history,
      store,
    },
  });
  return {
    wrapper,
    store,
    history,
  };
};

/**
 * @param {React.ReactNode} Component
 * @param {React.Props} props
 * @param {{
 *     initialState: {},
 *     route: String,
 *     store: MockStoreEnhanced,
 *     history: MemoryHistory
 *   }} options
 */
export const shallowWithReduxAndRouter = (
  Component,
  props,
  {
    initialState = defaultState,
    store = configureStore([thunk])(initialState),
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {}
) => {
  const wrapper = shallow(<Component {...props} />, {
    wrappingComponent: Wrapper,
    wrappingComponentProps: {
      history,
      store,
    },
  });
  return {
    wrapper,
    store,
    history,
  };
};
