import React from 'react';
import ReactDOM from 'react-dom';
import EmbedContainer from 'react-oembed-container';
import { fetchOembed } from '@surfline/web-common';

/* eslint-disable import/prefer-default-export */
export const renderIgElements = () => {
  const igElements = document.querySelectorAll('[data-sl-ig]');
  if (igElements.length) {
    [...igElements].map(async (ig) => {
      const {
        oembedObj: { html },
      } = await fetchOembed(ig.dataset.slIg, '/contests/oembed');
      ReactDOM.render(
        <EmbedContainer markup={html}>
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </EmbedContainer>,
        ig
      );
    });
  }
};
