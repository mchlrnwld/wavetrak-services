import { slugify } from '@surfline/web-common';

export const subregionForecastPaths = {
  base: 'surf-forecasts',
};

const subregionForecastPath = (subregionId, subregionName) =>
  `${subregionForecastPaths.base}/${slugify(subregionName)}/${subregionId}`;

export default subregionForecastPath;
