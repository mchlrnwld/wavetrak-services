import { slugify } from '@surfline/web-common';

export const spotReportPaths = {
  base: 'surf-report',
};

const spotReportPath = (_id, name) => `${spotReportPaths.base}/${slugify(name)}/${_id}`;

export default spotReportPath;
