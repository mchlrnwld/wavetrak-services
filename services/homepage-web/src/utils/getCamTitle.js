export const getCamTitle = (title) => {
  if (title) {
    return title.split('- ')[1] || title;
  }
  return '';
};
