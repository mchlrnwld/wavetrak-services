/** @prettier */
import favoritesFixture from './favorites.json';

const COLLECTIONS = 'collections';
const SUBREGIONS = 'subregions';

export const collectionTypesFixture = [
  { name: 'Collections', type: COLLECTIONS, rank: 1 },
  { name: 'Favorite Regions', type: SUBREGIONS, rank: 2, border: true },
];

export default [
  {
    name: 'All Favorites',
    _id: '1',
    type: COLLECTIONS,
    loading: false,
    error: false,
    fetchedSpots: true,
    spots: favoritesFixture,
  },
  {
    name: 'North Orange County',
    _id: '123',
    type: SUBREGIONS,
    loading: false,
    error: false,
    fetchedSpots: false,
    spots: favoritesFixture,
  },
  {
    name: 'Big Wave',
    type: COLLECTIONS,
    _id: '123',
    loading: false,
    error: false,
    fetchedSpots: false,
    spots: favoritesFixture,
  },
  {
    name: 'EmptySpotsCollection',
    type: COLLECTIONS,
    _id: '123',
    loading: false,
    error: false,
    fetchedSpots: false,
    spots: [],
  },
];
