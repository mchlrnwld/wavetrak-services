import collectionsFixture, { collectionTypesFixture } from './collections';

/** @prettier */

export default {
  router: {
    location: {},
  },
  status: {
    code: 200,
  },
  backplane: {
    user: {
      details: {
        firstName: 'Luigi',
        lastName: 'Doge',
        email: 'luigi@surfline.com',
      },
      settings: {
        _id: 'abc123',
        user: 'xyzpdq',
        preferredWaveChart: 'SURF',
        location: {
          coordinates: [0, 0],
          type: 'Point',
        },
        onboarded: [],
        units: {
          temperature: 'F',
          windSpeed: 'KTS',
          tideHeight: 'FT',
          swellHeight: 'FT',
          surfHeight: 'FT',
        },
      },
      favorites: [
        {
          _id: '5842041f4e65fad6a77088e4',
          name: 'South Side/13th Street',
          cameras: [
            {
              title: 'South Side/13th Street',
              streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/91961185/playlist.m3u8',
              stillUrl:
                'https://camstills.cdn-surfline.com/sealbeachsscam/latest_small_pixelated.png',
              rewindBaseUrl:
                'http://live-cam-archive.cdn-surfline.com/sealbeachsscam/sealbeachsscam',
              _id: '58dad5a4d572cb0012fdb4b1',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
          conditions: {
            human: false,
            value: null,
          },
          wind: {
            speed: 11.81,
            direction: 342.58,
          },
          waveHeight: {
            human: false,
            min: 0,
            max: 1,
            occasional: null,
            humanRelation: null,
          },
          tide: {
            previous: {
              type: 'HIGH',
              height: 5,
              timestamp: 1490721448,
              utcOffset: -7,
            },
            next: {
              type: 'LOW',
              height: 0,
              timestamp: 1490743709,
              utcOffset: -7,
            },
          },
          thumbnail:
            'https://spot-thumbnails.cdn-surfline.com/spots/5842041f4e65fad6a77088e4/5842041f4e65fad6a77088e4_1500.jpg',
          rank: 0,
        },
      ],
      entitlements: [
        'sl_no_ads',
        'sl_premium',
        'Single Surf Alert address',
        'Local photos',
        'Uploads',
        'Galleries',
        'Favorites',
        'Follow Photographers',
      ],
      loading: false,
    },
  },
  multiCam: {
    active: collectionsFixture[0]._id,
    collections: collectionsFixture,
    collectionTypes: collectionTypesFixture,
    numCams: 4,
    primarySpotIndex: 0,
    error: null,
    selectedSpots: [...collectionsFixture[0].spots.slice(0, 9)],
    collectionsLoading: false,
  },
};
