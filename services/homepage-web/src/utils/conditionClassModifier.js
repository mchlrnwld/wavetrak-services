const conditionClassModifier = (condition) =>
  condition && condition.toLowerCase().replace(/_/g, '-');

export default conditionClassModifier;
