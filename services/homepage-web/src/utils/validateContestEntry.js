const validateEmail = (email) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const validateUrl = (url) => {
  const re =
    /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  return re.test(String(url).toLowerCase());
};

const capitalizeFirstLetter = (string) => `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

const validateContestEntry = (entry) => {
  const errorDetails = [];
  Object.keys(entry).map((key) =>
    !entry[key] ? errorDetails.push(`${capitalizeFirstLetter(key)} is required`) : null
  );
  if (entry.email && !validateEmail(entry.email)) errorDetails.push('Invalid email address');
  if (entry.videoUrl && !validateUrl(entry.videoUrl))
    errorDetails.push('Valid video URL is required');
  return errorDetails;
};

export default validateContestEntry;
