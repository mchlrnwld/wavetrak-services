const preserveNextStart = (articles, nextCuratedStart, nextFeedLargeStart, nextFeedSmallStart) => {
  const { associated } = articles;
  if (nextCuratedStart && !associated.nextCuratedStart) {
    associated.nextCuratedStart = nextCuratedStart;
  }
  if (nextFeedLargeStart && !associated.nextFeedLargeStart) {
    associated.nextFeedLargeStart = nextFeedLargeStart;
  }
  if (nextFeedSmallStart && !associated.nextFeedSmallStart) {
    associated.nextFeedSmallStart = nextFeedSmallStart;
  }
  return { ...articles, associated };
};

export default preserveNextStart;
