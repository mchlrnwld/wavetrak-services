const getContestAdId = (region, position, period, contestType) => {
  const [, periodEnd] = period.split('-');
  if (contestType === 'sos') return `contest_${contestType}_${position}`;
  return region === 'north-shore'
    ? `contest_${position}_${periodEnd}`
    : `contest_regional_${position}_${periodEnd}`;
};

export default getContestAdId;
