const getContestType = (contest) => {
  switch (contest) {
    case 'spirit-of-surfing':
      return 'sos';
    default:
      return 'wotw';
  }
};

export default getContestType;
