import createReducer from './createReducer';
import {
  FETCH_SEARCH_RESULTS,
  FETCH_SEARCH_RESULTS_SUCCESS,
  SHOW_ALL_TOGGLE,
} from '../actions/search';

const initialState = {
  results: [],
  sections: {
    'spot-suggest': {
      showAll: false,
      name: 'Surf Spots',
      initialCount: 20,
    },
    'subregion-suggest': {
      showAll: false,
      name: 'Regional Forecast',
      initialCount: 8,
    },
    'geoname-suggest': {
      showAll: false,
      name: 'Map Area',
      initialCount: 8,
    },
    'feed-tag-suggest': {
      showAll: false,
      name: 'Surf News',
      initialCount: 5,
    },
    'travel-suggest': {
      showAll: false,
      name: 'Travel Guide',
      initialCount: 5,
    },
  },
};

const handlers = {};

handlers[FETCH_SEARCH_RESULTS] = (state) => ({
  ...state,
});

handlers[FETCH_SEARCH_RESULTS_SUCCESS] = (state, { results }) => ({
  ...state,
  results,
});

handlers[SHOW_ALL_TOGGLE] = (state, { section }) => {
  const sections = { ...state.sections };
  sections[section].showAll = !sections[section].showAll;
  return {
    ...state,
    sections,
  };
};

export default createReducer(handlers, initialState);
