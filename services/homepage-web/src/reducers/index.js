import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import contests from './contests';
import homecam from './homecam';
import homepage from './homepage';
import status from './status';
import browser from './browser';
import search from './search';
import swellEvents from './swellEvents';
import multiCam from './multiCam';

/**
 * @param {import('history').History} history
 * @param {{[key: string]: import('redux').Reducer}} asyncReducers
 */
const rootReducer = (history, asyncReducers) =>
  combineReducers({
    router: connectRouter(history),
    contests,
    homepage,
    homecam,
    status,
    browser,
    search,
    swellEvents,
    multiCam,
    ...asyncReducers,
  });

export default rootReducer;
