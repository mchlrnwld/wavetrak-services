import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_HOME_CAM_SUCCESS,
  SELECT_HOME_CAM_OPTION,
  TOGGLE_HOME_CAM_SELECT_VIEW,
  TOGGLE_HOME_CAM_VISIBILITY,
  UPDATE_HOME_CAM,
  UPDATE_HOME_CAM_SUCCESS,
  UPDATE_HOME_CAM_FAILURE,
} from '../actions/homecam';
import homecam from './homecam';

describe('reducers / homecam', () => {
  const initialState = deepFreeze({
    loading: false,
    success: false,
    visible: true,
    displaySelectView: false,
    camera: null,
    spot: null,
    currentSelection: null,
  });

  it('initializes with default values', () => {
    expect(homecam()).to.deep.equal(initialState);
  });

  it('fetches homecam on FETCH_HOME_CAM_SUCCESS', () => {
    const camera = {
      streamUrl: 'http://stream.com',
      stillUrl: 'http://still.com',
      status: {
        isDown: false,
        message: '',
      },
    };
    const spot = {
      _id: '123',
      name: '54th Street',
      conditions: {
        human: false,
        value: 'NONE',
      },
      waveHeight: {
        human: false,
        min: 3,
        max: 4,
        occassional: 5,
        humanRelation: 'GOOD',
        plus: false,
      },
    };
    const action = deepFreeze({
      type: FETCH_HOME_CAM_SUCCESS,
      camera,
      spot,
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      camera,
      spot,
      loading: false,
      success: true,
      initialLoad: true,
    });
  });

  it('sets visible on TOGGLE_HOME_CAM_VISIBILITY', () => {
    const action = deepFreeze({
      type: TOGGLE_HOME_CAM_VISIBILITY,
      visible: true,
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      visible: true,
    });
  });

  it('sets selection and success on SELECT_HOME_CAM_OPTION', () => {
    const action = deepFreeze({
      type: SELECT_HOME_CAM_OPTION,
      currentSelection: '12345',
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      currentSelection: '12345',
      success: false,
    });
  });

  it('sets displaySelectView on TOGGLE_HOME_CAM_SELECT_VIEW', () => {
    const action = deepFreeze({
      type: TOGGLE_HOME_CAM_SELECT_VIEW,
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      displaySelectView: true,
      success: false,
    });
  });

  it('sets loading on UPDATE_HOME_CAM', () => {
    const action = deepFreeze({
      type: UPDATE_HOME_CAM,
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });

  it('sets loading, success and id on UPDATE_HOME_CAM_SUCCESS', () => {
    const action = deepFreeze({
      type: UPDATE_HOME_CAM_SUCCESS,
      cameraId: '12345',
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
      success: true,
      cameraId: '12345',
    });
  });

  it('sets loading and error on UPDATE_HOME_CAM_FAILURE', () => {
    const action = deepFreeze({
      type: UPDATE_HOME_CAM_FAILURE,
      error: 'Error message',
    });
    expect(homecam(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
      error: 'Error message',
    });
  });
});
