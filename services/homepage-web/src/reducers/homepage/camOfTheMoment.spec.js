import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_CAM_OF_THE_MOMENT_SUCCESS } from '../../actions/camOfTheMoment';
import camOfTheMoment from './camOfTheMoment';

const initialState = null;

describe('reducers / homepage / camOfTheMoment', () => {
  it('initializes with no cam of the moment', () => {
    expect(camOfTheMoment()).to.equal(initialState);
  });

  it('sets cam of the moment object on FETCH_CAM_OF_THE_MOMENT_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_CAM_OF_THE_MOMENT_SUCCESS,
      camOfTheMoment: {},
    });
    expect(camOfTheMoment(initialState, action)).to.deep.equal(action.camOfTheMoment);
  });
});
