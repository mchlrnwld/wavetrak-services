import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_PROMOBOX_SLIDES_SUCCESS } from '../../actions/homepage';
import promobox from './promobox';

const initialState = deepFreeze({
  cards: [],
  slides: [],
});

describe('reducers / homepage / promobox', () => {
  it('initializes with no slides and mobile image size', () => {
    expect(promobox()).to.deep.equal(initialState);
  });

  it('sets slides on FETCH_PROMOBOX_SLIDES_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_PROMOBOX_SLIDES_SUCCESS,
      cards: [
        { id: '1262', createdAt: 1499612779 },
        { id: '1289', createdAt: 1495489028 },
      ],
      slides: [
        { id: '1265', createdAt: 1499612779 },
        { id: '1290', createdAt: 1495489028 },
      ],
    });
    expect(promobox(initialState, action)).to.deep.equal({
      cards: action.cards,
      slides: action.slides,
    });
  });
});
