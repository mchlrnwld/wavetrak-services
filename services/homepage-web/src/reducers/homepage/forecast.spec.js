import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_FORECAST_ARTICLES,
  FETCH_FORECAST_ARTICLES_SUCCESS,
  FETCH_FORECAST_ARTICLES_FAILURE,
  FETCH_SUBREGION_FORECAST,
  FETCH_SUBREGION_FORECAST_SUCCESS,
  FETCH_SUBREGION_FORECAST_FAILURE,
  SET_SUBREGION_FORECAST,
  SET_SUBREGION_FORECAST_SUCCESS,
  SET_SUBREGION_FORECAST_FAILURE,
} from '../../actions/forecast';
import forecast from './forecast';

describe('reducers / forecast', () => {
  const initialState = deepFreeze({
    loading: false,
    error: false,
    success: false,
    associated: null,
    overview: null,
    conditions: null,
    subregions: [],
    articles: {
      regional: [],
      error: null,
      loading: false,
      nextStart: null,
      pageCount: 0,
    },
  });

  it('initializes with default values', () => {
    expect(forecast()).to.deep.equal(initialState);
  });

  it('sets loading on FETCH_SUBREGION_FORECAST', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_FORECAST,
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });

  it('sets forecast and overview on FETCH_SUBREGION_FORECAST_SUCCESS', () => {
    const overviewPayload = {
      associated: {
        utcOffset: -10,
        units: {
          waveHeight: 'FT',
        },
      },
      data: {
        name: 'North Shore',
      },
    };
    const forecastPayload = {
      data: {
        conditions: [
          { 'a set': 'of conditions', 'for a': 'subregion' },
          { 'a set': 'of conditions', 'for a': 'subregion' },
        ],
      },
    };
    const action = deepFreeze({
      type: FETCH_SUBREGION_FORECAST_SUCCESS,
      forecast: forecastPayload,
      overview: overviewPayload,
      subregions: [],
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
      success: true,
      subregions: [],
      associated: overviewPayload.associated,
      overview: overviewPayload.data,
      conditions: forecastPayload.data.conditions,
    });
  });

  it('sets loading and error on FETCH_SUBREGION_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_FORECAST_FAILURE,
      error: 'Error message',
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
      error: 'Error message',
    });
  });

  it('sets loading on SET_SUBREGION_FORECAST', () => {
    const action = deepFreeze({
      type: SET_SUBREGION_FORECAST,
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });

  it('sets loading on SET_SUBREGION_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: SET_SUBREGION_FORECAST_SUCCESS,
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
    });
  });

  it('sets loading and error on SET_SUBREGION_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: SET_SUBREGION_FORECAST_FAILURE,
      error: 'Error message',
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
      error: 'Error message',
    });
  });

  it('sets loading on FETCH_FORECAST_ARTICLES', () => {
    const action = deepFreeze({
      type: FETCH_FORECAST_ARTICLES,
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      articles: {
        ...initialState.articles,
        loading: true,
      },
    });
  });

  it('sets loading and articles on FETCH_FORECAST_ARTICLES_SUCCESS', () => {
    const articles = {
      associated: {
        nextStart: null,
      },
      data: {
        regional: [],
      },
    };
    const action = deepFreeze({
      type: FETCH_FORECAST_ARTICLES_SUCCESS,
      articles,
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      articles: {
        ...initialState.articles,
        regional: initialState.articles.regional.concat(articles.data.regional),
        loading: false,
        nextStart: articles.associated.nextStart,
        pageCount: initialState.articles.pageCount + 1,
      },
    });
  });

  it('sets loading and error on FETCH_FORECAST_ARTICLES_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_FORECAST_ARTICLES_FAILURE,
      error: 'Error message',
    });
    expect(forecast(initialState, action)).to.deep.equal({
      ...initialState,
      articles: {
        ...initialState.articles,
        loading: false,
        error: 'Error message',
      },
    });
  });
});
