import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { ENABLE_INFINITE_SCROLLING } from '../../actions/homepage';
import infiniteScrollingEnabled from './infiniteScrollingEnabled';

const initialState = false;

describe('reducers / homepage / infiniteScrollingEnabled', () => {
  it('initializes to false', () => {
    expect(infiniteScrollingEnabled()).to.equal(initialState);
  });

  it('is true on ENABLE_INFINITE_SCROLLING', () => {
    const action = deepFreeze({ type: ENABLE_INFINITE_SCROLLING });
    expect(infiniteScrollingEnabled(initialState, action)).to.be.true();
  });
});
