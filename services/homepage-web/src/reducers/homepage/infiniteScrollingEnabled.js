import createReducer from '../createReducer';
import { ENABLE_INFINITE_SCROLLING } from '../../actions/homepage';

const initialState = false;

const handlers = {};

handlers[ENABLE_INFINITE_SCROLLING] = () => true;

export default createReducer(handlers, initialState);
