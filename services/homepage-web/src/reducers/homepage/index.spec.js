import { expect } from 'chai';
import homepage from '.';

describe('reducers / homepage', () => {
  it('combines homepage reducers', () => {
    const state = homepage();
    expect(Object.keys(state)).to.deep.equal([
      'camOfTheMoment',
      'feed',
      'infiniteScrollingEnabled',
      'promobox',
      'forecast',
    ]);
  });
});
