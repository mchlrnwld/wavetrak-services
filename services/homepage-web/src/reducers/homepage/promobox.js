import createReducer from '../createReducer';
import { FETCH_PROMOBOX_SLIDES_SUCCESS } from '../../actions/homepage';

const initialState = {
  cards: [],
  slides: [],
};

const handlers = {};

handlers[FETCH_PROMOBOX_SLIDES_SUCCESS] = (state, { cards, slides }) => ({
  ...state,
  cards,
  slides,
});

export default createReducer(handlers, initialState);
