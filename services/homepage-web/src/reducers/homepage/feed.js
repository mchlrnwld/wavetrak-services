import createReducer from '../createReducer';
import { FETCH_FEED, FETCH_FEED_SUCCESS, FETCH_FEED_FAILURE } from '../../actions/homepage';

const initialState = {
  error: null,
  loading: false,
  pageCount: 0,
  curated: {
    articles: [],
    limit: 9,
    nextStart: null,
    type: 'curated',
  },
  feedLarge: {
    articles: [],
    limit: 3,
    nextStart: null,
    type: 'feedLarge',
  },
  feedSmall: {
    articles: [],
    limit: 2,
    nextStart: null,
    type: 'feedSmall',
  },
};

const handlers = {};

handlers[FETCH_FEED] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_FEED_SUCCESS] = (state, { articles: { associated, data } }) => ({
  ...state,
  curated: {
    ...state.curated,
    articles: state.curated.articles.concat(data.curated),
    nextStart: associated.nextCuratedStart,
  },
  feedLarge: {
    ...state.feedLarge,
    articles: state.feedLarge.articles.concat(data.feedLarge),
    nextStart: associated.nextFeedLargeStart,
  },
  feedSmall: {
    ...state.feedSmall,
    articles: state.feedSmall.articles.concat(data.feedSmall),
    nextStart: associated.nextFeedSmallStart,
  },
  loading: false,
  pageCount: state.pageCount + 1,
});

handlers[FETCH_FEED_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
