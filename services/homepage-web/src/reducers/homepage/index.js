import { combineReducers } from 'redux';
import camOfTheMoment from './camOfTheMoment';
import feed from './feed';
import infiniteScrollingEnabled from './infiniteScrollingEnabled';
import promobox from './promobox';
import forecast from './forecast';

export default combineReducers({
  camOfTheMoment,
  feed,
  infiniteScrollingEnabled,
  promobox,
  forecast,
});
