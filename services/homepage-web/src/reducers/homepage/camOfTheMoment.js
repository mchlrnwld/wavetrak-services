import createReducer from '../createReducer';
import { FETCH_CAM_OF_THE_MOMENT_SUCCESS } from '../../actions/camOfTheMoment';

const initialState = null;

const handlers = {};

handlers[FETCH_CAM_OF_THE_MOMENT_SUCCESS] = (state, { camOfTheMoment }) => camOfTheMoment;

export default createReducer(handlers, initialState);
