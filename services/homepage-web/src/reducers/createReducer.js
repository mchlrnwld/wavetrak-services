/**
 * @description Create a reducer.
 * @param {import('redux').PreloadedState<{}>} initialState
 * @param {{ [key: string]: import('redux').Reducer }} handlers
 */

const createReducer =
  (handlers, initialState) =>
  (state = initialState, action) => {
    const handler = action && action.type ? handlers[action.type] : null;

    if (handler) return handler(state, action);
    return state;
  };

export default createReducer;
