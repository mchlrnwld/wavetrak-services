import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_BROWSER_DATA,
  FETCH_BROWSER_DATA_SUCCESS,
  FETCH_BROWSER_DATA_FAILURE,
} from '../actions/browser';
import browser from './browser';

describe('reducers / browser', () => {
  const initialState = deepFreeze({
    browserData: {
      name: null,
      version: null,
    },
    loading: false,
  });

  it('initializes with default values', () => {
    expect(browser()).to.deep.equal(initialState);
  });

  it('sets loading true on FETCH_BROWSER_DATA', () => {
    const action = deepFreeze({
      type: FETCH_BROWSER_DATA,
    });
    expect(browser(initialState, action)).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });

  it('sets browserData on FETCH_BROWSER_DATA_SUCCESS', () => {
    const browserData = {
      name: 'Chrome',
      version: 59,
    };
    const action = deepFreeze({
      type: FETCH_BROWSER_DATA_SUCCESS,
      browserData,
    });
    expect(browser(initialState, action)).to.deep.equal({
      ...initialState,
      browserData,
    });
  });

  it('sets loading and error on FETCH_BROWSER_DATA_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_BROWSER_DATA_FAILURE,
      error: 'Error message',
    });
    expect(browser(initialState, action)).to.deep.equal({
      ...initialState,
      loading: false,
      error: 'Error message',
    });
  });
});
