import { expect } from 'chai';
import { createMemoryHistory } from 'history';
import reducers from '.';

describe('reducers', () => {
  it('combines routing, homepage', () => {
    const history = createMemoryHistory({
      initialEntries: ['/'],
    });
    const state = reducers(history)();
    expect(state.router).to.be.ok();
    expect(state.homepage).to.be.ok();
  });
});
