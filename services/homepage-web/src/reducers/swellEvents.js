import createReducer from './createReducer';
import {
  FETCH_SWELL_EVENTS,
  FETCH_SWELL_EVENTS_SUCCESS,
  FETCH_SWELL_EVENTS_FAILURE,
} from '../actions/swellEvents';

const initialState = {
  error: null,
  events: [],
  loading: false,
  success: false,
};

const handlers = {};

handlers[FETCH_SWELL_EVENTS] = () => ({
  ...initialState,
  loading: true,
});

handlers[FETCH_SWELL_EVENTS_SUCCESS] = (state, { events }) => ({
  ...state,
  events,
  loading: false,
  success: true,
});

handlers[FETCH_SWELL_EVENTS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducer(handlers, initialState);
