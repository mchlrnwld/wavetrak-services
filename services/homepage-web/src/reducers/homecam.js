import createReducer from './createReducer';
import {
  FETCH_HOME_CAM,
  FETCH_HOME_CAM_SUCCESS,
  FETCH_HOME_CAM_FAILURE,
  SELECT_HOME_CAM_OPTION,
  TOGGLE_HOME_CAM_SELECT_VIEW,
  TOGGLE_HOME_CAM_VISIBILITY,
  UPDATE_HOME_CAM,
  UPDATE_HOME_CAM_SUCCESS,
  UPDATE_HOME_CAM_FAILURE,
} from '../actions/homecam';

const initialState = {
  loading: false,
  success: false,
  visible: true,
  displaySelectView: false,
  camera: null,
  spot: null,
  currentSelection: null,
};

const handlers = {};

handlers[FETCH_HOME_CAM] = (state) => ({
  ...state,
  loading: true,
  success: false,
});

handlers[FETCH_HOME_CAM_SUCCESS] = (state, { camera, spot }) => ({
  ...state,
  camera,
  spot,
  loading: false,
  success: true,
  initialLoad: true,
});

handlers[FETCH_HOME_CAM_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[SELECT_HOME_CAM_OPTION] = (state, { currentSelection }) => ({
  ...state,
  currentSelection,
  success: false,
});

handlers[TOGGLE_HOME_CAM_SELECT_VIEW] = (state) => ({
  ...state,
  displaySelectView: !state.displaySelectView,
  success: false,
});

handlers[TOGGLE_HOME_CAM_VISIBILITY] = (state, { visible }) => ({
  ...state,
  visible,
});

handlers[UPDATE_HOME_CAM] = (state) => ({
  ...state,
  loading: true,
});

handlers[UPDATE_HOME_CAM_SUCCESS] = (state, { cameraId }) => ({
  ...state,
  loading: false,
  success: true,
  cameraId,
});

handlers[UPDATE_HOME_CAM_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducer(handlers, initialState);
