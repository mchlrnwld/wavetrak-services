import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SEARCH_RESULTS_SUCCESS } from '../actions/search';
import search from './search';

const initialState = {
  results: [],
  sections: {
    'spot-suggest': {
      showAll: false,
      name: 'Surf Spots',
      initialCount: 20,
    },
    'subregion-suggest': {
      showAll: false,
      name: 'Regional Forecast',
      initialCount: 8,
    },
    'geoname-suggest': {
      showAll: false,
      name: 'Map Area',
      initialCount: 8,
    },
    'feed-tag-suggest': {
      showAll: false,
      name: 'Surf News',
      initialCount: 5,
    },
    'travel-suggest': {
      showAll: false,
      name: 'Travel Guide',
      initialCount: 5,
    },
  },
};

describe('reducers / search', () => {
  it('initializes with no search result', () => {
    expect(search()).to.deep.equal(initialState);
  });

  it('sets search results object on FETCH_SEARCH_RESULTS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SEARCH_RESULTS_SUCCESS,
      results: [1],
    });
    expect(search(initialState, action)).to.deep.equal({
      ...initialState,
      results: action.results,
    });
  });
});
