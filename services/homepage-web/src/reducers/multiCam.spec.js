/** @prettier */

import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  SET_ACTIVE_COLLECTION,
  FETCH_COLLECTIONS_SUCCESS,
  FETCH_COLLECTIONS_FAILURE,
  SET_SELECTED_SPOTS,
  CHANGE_PRIMARY_SPOT_INDEX,
  CHANGE_PRIMARY_SPOT,
  SWAP_PRIMARY_SPOT,
  SET_NUM_CAMS,
} from '../actions/multiCam';
import multiCam from './multiCam';
import favoritesFixture from '../utils/fixtures/favorites.json';

describe('reducers / multiCam', () => {
  const initialState = deepFreeze({
    active: null,
    collectionTypes: [],
    collectionsLoading: true,
    collections: [],
    selectedSpots: [],
    numCams: 4,
    primarySpotIndex: 0,
    error: null,
  });

  it('initializes with default values', () => {
    expect(multiCam()).to.deep.equal(initialState);
  });

  it('sets the active collection properly', () => {
    const collection = {
      name: 'TEST COLLECTION',
    };
    const action = deepFreeze({
      type: SET_ACTIVE_COLLECTION,
      active: collection,
    });
    expect(multiCam(initialState, action)).to.deep.equal({
      ...initialState,
      active: collection,
    });
  });

  it('sets the collections properly with a default', () => {
    const allFavorites = {
      name: 'All Favorites',
      spots: [],
    };
    const collections = {
      collections: {
        collections: [allFavorites],
        name: 'Collections',
      },
    };

    const action = deepFreeze({
      type: FETCH_COLLECTIONS_SUCCESS,
      active: allFavorites,
      collections,
    });
    expect(multiCam(initialState, action)).to.deep.equal({
      ...initialState,
      active: allFavorites,
      collectionsLoading: false,
      collections,
    });
  });

  it('sets the error if fetching collections fails', () => {
    const action = deepFreeze({
      type: FETCH_COLLECTIONS_FAILURE,
      error: 'This is an error',
    });
    expect(multiCam(initialState, action)).to.deep.equal({
      ...initialState,
      error: 'This is an error',
      collectionsLoading: false,
    });
  });

  it('should set the selected spots', () => {
    const action = deepFreeze({
      type: SET_SELECTED_SPOTS,
      selectedSpots: favoritesFixture,
    });
    expect(multiCam(initialState, action)).to.deep.equal({
      ...initialState,
      selectedSpots: favoritesFixture,
    });
  });

  it('should change the primary spot index', () => {
    const action = deepFreeze({
      type: CHANGE_PRIMARY_SPOT_INDEX,
      newIndex: 4,
    });
    expect(multiCam(initialState, action)).to.deep.equal({
      ...initialState,
      primarySpotIndex: 4,
    });
  });

  it('should change the primary spot index', () => {
    const [, , , , newSpot] = favoritesFixture;

    const action = deepFreeze({
      type: CHANGE_PRIMARY_SPOT,
      newSpot,
    });
    const selectedSpots = favoritesFixture;

    const store = multiCam({ ...initialState, selectedSpots }, action);
    expect(store).to.deep.equal({
      ...initialState,
      selectedSpots: Object.assign([...selectedSpots], {
        [initialState.primarySpotIndex]: newSpot,
      }),
    });
  });

  it('should swap the primary spot', () => {
    const newSpotIndex = 3;
    const action = deepFreeze({
      type: SWAP_PRIMARY_SPOT,
      newSpotIndex,
    });
    const selectedSpots = favoritesFixture;
    const store = multiCam({ ...initialState, selectedSpots }, action);
    expect(store).to.deep.equal({
      ...initialState,
      selectedSpots: Object.assign([...selectedSpots], {
        [initialState.primarySpotIndex]: selectedSpots[newSpotIndex],
        [newSpotIndex]: selectedSpots[initialState.primarySpotIndex],
      }),
    });
  });

  it('should set the number of cams', () => {
    const action = deepFreeze({
      type: SET_NUM_CAMS,
      numCams: 1,
    });
    const store = multiCam(initialState, action);
    expect(store).to.deep.equal({
      ...initialState,
      numCams: 1,
    });
  });
});
