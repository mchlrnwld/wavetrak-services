import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import { Alert, Button, Input, Select } from '@surfline/quiver-react';
import config from '../../config';

const ContestForm = ({
  fieldValues,
  onChange,
  onSubmit,
  period,
  region,
  loading,
  error,
  success,
}) => (
  <div className="sl-contest-form">
    <p className="sl-contest-form__description">
      Submit video links of your best rides from anywhere in these five regions:
      <ul>
        <li>Northwest (Washington border to Point Conception)</li>
        <li>SoCal (Point Conception to Mexico border)</li>
        <li>Caribbean (Caribbean Islands and Central America Caribbean)</li>
        <li>Southeast (Virginia to Florida)</li>
        <li>Northeast (Maine to Maryland)</li>
      </ul>
    </p>
    <form>
      <div className="sl-contest-form__fields">
        <div className="sl-contest-form__fields__block">
          <h5 className="sl-contest-form__heading">Contact Info</h5>
          <Input
            label="Your Name"
            value={fieldValues.name}
            input={{ onChange: (evt) => onChange({ name: evt.target.value }) }}
            required
          />
          <Input
            label="Email Address"
            type="email"
            value={fieldValues.email}
            input={{ onChange: (evt) => onChange({ email: evt.target.value }) }}
          />
          <Input
            label="Instagram Handle"
            value={fieldValues.instagramHandle}
            input={{ onChange: (evt) => onChange({ instagramHandle: evt.target.value }) }}
          />
          <p>Used only for contest related communication.</p>
        </div>
        <div className="sl-contest-form__fields__block">
          <h5 className="sl-contest-form__heading">Entry Details</h5>
          <Input
            label="Entry Title"
            value={fieldValues.title}
            input={{ onChange: (evt) => onChange({ title: evt.target.value }) }}
          />
          <Input
            label="Surfer Name"
            value={fieldValues.surfer}
            input={{ onChange: (evt) => onChange({ surfer: evt.target.value }) }}
          />
          <Input
            label="Photographer Name"
            value={fieldValues.photographer}
            input={{ onChange: (evt) => onChange({ photographer: evt.target.value }) }}
          />
          <Input
            label="Date Filmed"
            type="date"
            value={fieldValues.date}
            input={{ onChange: (evt) => onChange({ date: evt.target.value }) }}
          />
          <Select
            label="Surf Region"
            options={[
              { text: 'Northwest', value: 'Northwest' },
              { text: 'SoCal', value: 'SoCal' },
              { text: 'Caribbean', value: 'Caribbean' },
              { text: 'Southeast', value: 'Southeast' },
              { text: 'Northeast', value: 'Northeast' },
            ]}
            defaultValue="northwest"
            select={{
              onChange: (evt) => onChange({ location: evt.target.value }),
            }}
          />
        </div>
        <div className="sl-contest-form__fields__block">
          <h5 className="sl-contest-form__heading">Video</h5>
          <Input
            label="Video URL"
            value={fieldValues.videoUrl}
            input={{ onChange: (evt) => onChange({ videoUrl: evt.target.value }) }}
          />
          <p>YouTube/Vimeo/Facebook/Instagram Share URL</p>
        </div>
        <div className="sl-contest-form__fields__block">
          <p className="sl-contest-form__terms">
            {`By submitting, you acknowledge that this content belongs to you, does not contain
            copyrighted material and you agree to all `}
            <Link to={`/${config.baseContestUrl(region, period)}details`}>
              contest rules and conditions
            </Link>
            .
          </p>
        </div>
      </div>
      {error.message ? (
        <Alert type="error">
          {error.message}
          <ul>
            {error.details.map((err) => (
              <li>{err}</li>
            ))}
          </ul>
        </Alert>
      ) : null}
      {success ? <Alert type="success">Entry successfully submitted!</Alert> : null}
    </form>
    <Button onClick={onSubmit} loading={loading} success={success}>
      Submit Video
    </Button>
  </div>
);

ContestForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  period: PropTypes.string.isRequired,
  region: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  error: PropTypes.shape({
    message: PropTypes.string,
    details: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  fieldValues: PropTypes.shape().isRequired,
};

export default ContestForm;
