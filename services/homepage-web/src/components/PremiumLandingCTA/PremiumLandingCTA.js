import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const PremiumLandingCTA = ({ doClickedSubscribeCTA, location }) => {
  const properties = { location: `Premium Landing - ${location}` };
  const handleCtaClick = () => {
    doClickedSubscribeCTA(properties);
    window.location.assign('/upgrade');
  };
  return (
    <div {...createAccessibleOnClick(handleCtaClick)} className="premium-landing-cta">
      <Button>Start Free Trial</Button>
    </div>
  );
};

PremiumLandingCTA.propTypes = {
  doClickedSubscribeCTA: PropTypes.func.isRequired,
  location: PropTypes.string.isRequired,
};

export default PremiumLandingCTA;
