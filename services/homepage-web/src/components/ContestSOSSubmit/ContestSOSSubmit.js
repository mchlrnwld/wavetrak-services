import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const ContestSOSSubmit = ({ period }) => {
  useEffect(() => {
    const contestId =
      period === '2021'
        ? 'register/SpiritOfSurfingCuervoChallenge2021'
        : 'profiletab/SpiritofSurfingCuervoChallenge';
    const script1 = document.createElement('script');
    script1.src = '//dcveehzef7grj.cloudfront.net/js/vtg_embed.js';
    script1.async = true;
    document.body.appendChild(script1);

    const script2 = document.createElement('script');
    script2.type = 'text/javascript';
    script2.async = true;
    script2.innerHTML = `var isOldIE = (navigator.userAgent.indexOf("MSIE") !== -1);try{ VTGEMBED.init(["//surfline.votigo.com","/fbcontests/${contestId} ","100%","1080"]); VTGEMBED.writeIframe(); } catch(err) { var embedTheSite = setInterval(function(){ VTGEMBED.init(["//surfline.votigo.com","/fbcontests/${contestId} ","100%","1080"]); VTGEMBED.writeIframe(); if(document.getElementById("vtg-container").innerHTML != ""){ clearInterval(embedTheSite); } }, 3000); }`;
    document.body.appendChild(script2);

    const script3 = document.createElement('script');
    script3.src =
      '//dcveehzef7grj.cloudfront.net/davidjbradshaw-iframe-resizer-7eb43c4/js/iframeResizer.min.js';
    script3.async = true;
    document.body.appendChild(script3);

    const script4 = document.createElement('script');
    script4.type = 'text/javascript';
    script4.async = true;
    script4.innerHTML = `try { iFrameResize({log:false,heightCalculationMethod: isOldIE ? 'max' : 'lowestElement',checkOrigin:false, enablePublicMethods: true, minHeight: 700}); } catch(err) {}`;
    document.body.appendChild(script4);

    return () => {
      document.body.removeChild(script1);
      document.body.removeChild(script2);
      document.body.removeChild(script3);
      document.body.removeChild(script4);
    };
  }, [period]);
  return <div id="vtg-container" />;
};

ContestSOSSubmit.propTypes = {
  period: PropTypes.string.isRequired,
};

export default ContestSOSSubmit;
