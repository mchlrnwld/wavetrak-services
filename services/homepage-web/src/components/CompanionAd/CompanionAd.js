import PropTypes from 'prop-types';
import React from 'react';
import { GoogleDFP } from '@surfline/quiver-react';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

const CompanionAd = ({ entitlements, adConfigName }) => {
  const adConfig = loadAdConfig(adConfigName, [], entitlements);
  return <GoogleDFP companion adConfig={adConfig} isHtl isTesting={config.htl.isTesting} />;
};

CompanionAd.propTypes = {
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  adConfigName: PropTypes.string.isRequired,
};

export default CompanionAd;
