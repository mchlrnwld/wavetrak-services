import React from 'react';
import PropTypes from 'prop-types';
import { SURF, SWELL } from '@surfline/web-common';
import { PillButton, SettingsCog, HoverMenu } from '@surfline/quiver-react';

import './MultiCamGraphsHeader.scss';

const AREA_GRAPH = 'AREA_GRAPH';
const BAR_GRAPH = 'BAR_GRAPH';

const MultiCamGraphsHeader = ({
  setSurfDataType,
  surfDataType,
  surfGraphType,
  setSurfGraphType,
  spotReportUrl,
}) => {
  const menuOptions = [
    { name: 'Area Graph', value: AREA_GRAPH, selected: surfGraphType === AREA_GRAPH },
    { name: 'Bar Graph', value: BAR_GRAPH, selected: surfGraphType === BAR_GRAPH },
  ];

  return (
    <div className="sl-multi-cam-graphs-header">
      <div className="sl-multi-cam-graphs-header__data-toggle">
        <PillButton isActive={surfDataType === SURF} onClick={() => setSurfDataType(SURF)}>
          {SURF}
        </PillButton>
        <PillButton isActive={surfDataType === SWELL} onClick={() => setSurfDataType(SWELL)}>
          {SWELL}
        </PillButton>
      </div>
      {surfDataType === SURF && (
        <div className="sl-multi-cam-graphs-header__graph-toggle">
          <HoverMenu
            Icon={SettingsCog}
            menuOptions={menuOptions}
            onClickMenuItem={setSurfGraphType}
            useSelectOnMobile
            defaultValue={menuOptions.find((item) => item.selected)?.name}
            selectDescription="- SELECT A GRAPH TYPE -"
          />
        </div>
      )}
      {spotReportUrl && (
        <a className="sl-multi-cam-graphs-header__report-link" href={spotReportUrl}>
          Full 16-Day
        </a>
      )}
    </div>
  );
};

MultiCamGraphsHeader.propTypes = {
  setSurfDataType: PropTypes.func.isRequired,
  surfDataType: PropTypes.oneOf([SWELL, SURF]).isRequired,
  surfGraphType: PropTypes.oneOf([AREA_GRAPH, BAR_GRAPH]).isRequired,
  setSurfGraphType: PropTypes.func.isRequired,
  spotReportUrl: PropTypes.string,
};

MultiCamGraphsHeader.defaultProps = {
  spotReportUrl: '',
};

export default MultiCamGraphsHeader;
