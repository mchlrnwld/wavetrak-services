/** @prettier */

import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import Loadable from 'react-loadable';
import { ErrorBoundary } from '@surfline/quiver-react';
import HomepagePageContainer from '../../containers/routes/HomepagePageContainer';
import routesConfig from '../../routesConfig';

/**
 * NOTE: For server side rendering to work properly, we have to instantiate the Loadable components
 * outside of the JSX so that they are preloaded when we call `Loadable.preloadAll()` on the server
 * side.
 */
const routes = routesConfig.map(({ routeProps, loader, loading, modules, webpack }) => ({
  routeProps,
  component: Loadable({ loader, loading, modules, webpack }),
}));

const Routes = () => (
  <ErrorBoundary>
    <div>
      <Route path="/" component={HomepagePageContainer} />
      <Switch>
        {/* temporary redirect for beta users */}
        <Redirect from="/multi-cam" to="/surf-cams" />
        {routes.map(({ routeProps, component }) => (
          <Route key={routeProps.path} {...routeProps} component={component} />
        ))}
      </Switch>
    </div>
  </ErrorBoundary>
);

export default Routes;
