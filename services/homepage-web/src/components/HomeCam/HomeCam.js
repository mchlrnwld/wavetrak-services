import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import { CameraIcon } from './Icons';
import HomeCamAddFavorite from '../HomeCamAddFavorite';
import HomeCamGoPremium from '../HomeCamGoPremium';
import HomeCamPremium from '../HomeCamPremium';
import browserDataPropShape from '../../propTypes/browserData';
import homeCamPropType from '../../propTypes/homecam';
import userFavoritesPropType from '../../propTypes/userFavorites';
import userSettingsPropType from '../../propTypes/userSettings';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const HomeCam = ({
  homecam,
  isPremium,
  userFavorites,
  userSettings,
  doSelectHomeCamOption,
  doToggleHomeCamVisibility,
  doToggleHomeCamSelectView,
  doUpdateHomeCam,
  browserData,
}) => {
  const getHomeClasses = () =>
    classnames({
      'sl-home-cam': true,
      'sl-home-cam--minimized': !homecam.visible,
    });

  const getHomeCamContent = () => {
    if (!isPremium) {
      return <HomeCamGoPremium doToggleHomeCamVisibility={doToggleHomeCamVisibility} />;
    }
    if (!homecam.camera) {
      return <HomeCamAddFavorite doToggleHomeCamVisibility={doToggleHomeCamVisibility} />;
    }
    return (
      <HomeCamPremium
        doSelectHomeCamOption={doSelectHomeCamOption}
        doToggleHomeCamSelectView={doToggleHomeCamSelectView}
        doUpdateHomeCam={doUpdateHomeCam}
        homecam={homecam}
        userFavorites={userFavorites}
        userSettings={userSettings}
        doToggleHomeCamVisibility={doToggleHomeCamVisibility}
        browserData={browserData}
      />
    );
  };
  return (
    <div>
      {!homecam.visible ? (
        <div className={getHomeClasses()} {...createAccessibleOnClick(doToggleHomeCamVisibility)}>
          <CameraIcon />
        </div>
      ) : (
        <div className={getHomeClasses()}>{getHomeCamContent()}</div>
      )}
    </div>
  );
};

HomeCam.propTypes = {
  homecam: homeCamPropType.isRequired,
  isPremium: PropTypes.bool.isRequired,
  userFavorites: userFavoritesPropType.isRequired,
  userSettings: userSettingsPropType.isRequired,
  doSelectHomeCamOption: PropTypes.func.isRequired,
  doToggleHomeCamVisibility: PropTypes.func.isRequired,
  doToggleHomeCamSelectView: PropTypes.func.isRequired,
  doUpdateHomeCam: PropTypes.func.isRequired,
  browserData: browserDataPropShape.isRequired,
};

export default HomeCam;
