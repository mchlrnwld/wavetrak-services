import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import PremiumLandingCTA from '../PremiumLandingCTA';
import en from '../../intl/translations/en';

const getSectionContainerClassname = (orientation, contentPosition, backgroundImage) =>
  classNames({
    'premium-landing-section__container': true,
    'premium-landing-section__container--content-top': contentPosition === 'top',
    'premium-landing-section__container--with-background': !!backgroundImage,
    [`premium-landing-section__container--${orientation}`]: true,
  });

const getSectionClassname = (backgroundImage, contentPosition) =>
  classNames({
    'premium-landing-section': true,
    'premium-landing-section--with-background': !!backgroundImage,
    'premium-landing-section--content-top': contentPosition === 'top',
  });

const PremiumLandingSection = ({
  section,
  orientation,
  anchor,
  copy,
  creative,
  doClickedSubscribeCTA,
  backgroundImage,
  contentPosition,
}) => {
  const background = backgroundImage ? `url(${backgroundImage})` : null;

  return (
    <section
      id={anchor}
      className={getSectionClassname(backgroundImage, contentPosition)}
      style={{ backgroundImage: background }}
    >
      <div className={getSectionContainerClassname(orientation, contentPosition, backgroundImage)}>
        <div className="premium-landing-section__copy">
          <div className="premium-landing-section__icon">{copy.icon}</div>
          <div className="premium-landing-section__title">{copy.title}</div>
          <div className="premium-landing-section__text">{copy.text}</div>
          {section === 'Proprietary Buoys' && (
            <a
              className="premium-landing-section__learnmorebuoys"
              href={en.PremiumLanding.learnmorebuoys.link}
            >
              {en.PremiumLanding.learnmorebuoys.text}
            </a>
          )}
          <PremiumLandingCTA doClickedSubscribeCTA={doClickedSubscribeCTA} location={section} />
        </div>
        <div className="premium-landing-section__creative">{creative}</div>
      </div>
    </section>
  );
};

PremiumLandingSection.propTypes = {
  section: PropTypes.string.isRequired,
  orientation: PropTypes.string.isRequired,
  anchor: PropTypes.string.isRequired,
  copy: PropTypes.shape({
    icon: PropTypes.node,
    title: PropTypes.string,
    text: PropTypes.string,
  }).isRequired,
  creative: PropTypes.node.isRequired,
  doClickedSubscribeCTA: PropTypes.func.isRequired,
  backgroundImage: PropTypes.string,
  contentPosition: PropTypes.string,
};

PremiumLandingSection.defaultProps = {
  backgroundImage: null,
  contentPosition: 'bottom',
};

export default PremiumLandingSection;
