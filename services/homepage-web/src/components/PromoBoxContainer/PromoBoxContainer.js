import PropTypes from 'prop-types';
import React from 'react';
import PromoBoxCards from '../PromoBoxCards';
import PromoBoxSlides from '../PromoBoxSlides';
import articlePropType from '../../propTypes/article';

const PromoBoxContainer = ({ promoBox, feedLocale, imageResizing }) => (
  <div>
    {promoBox.slides.length ? (
      <PromoBoxSlides
        promoBoxSlides={promoBox.slides}
        feedLocale={feedLocale}
        imageResizing={imageResizing}
      />
    ) : null}
    {promoBox.cards.length ? (
      <PromoBoxCards
        promoBoxCards={promoBox.cards}
        feedLocale={feedLocale}
        imageResizing={imageResizing}
      />
    ) : null}
  </div>
);

PromoBoxContainer.propTypes = {
  feedLocale: PropTypes.string.isRequired,
  promoBox: PropTypes.shape({
    cards: PropTypes.arrayOf(articlePropType).isRequired,
    slides: PropTypes.arrayOf(articlePropType).isRequired,
  }).isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

export default PromoBoxContainer;
