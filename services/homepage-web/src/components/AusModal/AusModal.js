import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import Modal from 'react-modal';
import { useDeviceSize } from '@surfline/quiver-react';
import { trackEvent, canUseDOM } from '@surfline/web-common';

/**
 * Modal for CW => SL migration in Q4 2020. Most likely can be deprecated after that.
 * https://wavetrak.atlassian.net/browse/KBYG-3441
 */
const AusModal = ({ isPremium, pageName, treatment: variant }) => {
  const [cookie, setCookie] = useCookies(['sl-aus-launch']);
  const [modalIsOpen, toggleModal] = useState(false);
  const [modalOpening, setModalOpening] = useState(false);
  const device = useDeviceSize();

  const hideModal =
    (variant !== 'Variant_1' &&
      variant !== 'Variant_2' &&
      variant !== 'Variant_3' &&
      variant !== 'Variant_4') ||
    isPremium ||
    (cookie['sl-aus-launch'] && !modalIsOpen);

  const isMobileView = device.width <= 976;
  const variantClass = !hideModal ? `v${variant[variant.length - 1]}` : '';
  let bodyText;
  let buttonText;
  switch (variantClass) {
    case 'v1':
      bodyText = 'Enjoy 3 free months of premium.';
      buttonText = 'Try Surfline Premium Free';
      break;
    case 'v2':
      bodyText = 'Score more waves';
      buttonText = 'Try Surfline Premium Free';
      break;
    case 'v3':
      bodyText = 'Get 3 months of Premium for free';
      buttonText = 'Get 3 Months Free';
      break;
    case 'v4':
      bodyText = 'Three months free';
      buttonText = 'Get 3 Months Free';
      break;
    default:
      break;
  }
  const popupName = 'Australian Launch Modal';

  useEffect(() => {
    if (canUseDOM && !hideModal && !modalIsOpen) {
      if (!modalOpening) {
        setTimeout(() => {
          trackEvent('Served Popup', {
            popupName,
            pageName,
            isMobileView,
            bodyText,
          });
          toggleModal(true);
          setCookie('sl-aus-launch', true, { maxAge: 172800 });
        }, 3000);
        setModalOpening(true);
      }
    }
  }, [hideModal, modalIsOpen, modalOpening, pageName, isMobileView, bodyText, setCookie]);

  const trackClicked = () => {
    trackEvent('Clicked Popup', {
      popupName,
      pageName,
      isMobileView,
      bodyText,
      buttonText,
    });
    setTimeout(() => window.location.assign('https://www.surfline.com/australia90'), 300);
  };

  const getVariantDescription = () => {
    if (variantClass === 'v1') {
      return (
        <div className="sl-aus-modal-bottom-panel">
          <div className="sl-aus-modal-klein" />
          <div className="sl-aus-modal-cta">
            <p className="sl-aus-modal-tagline">{bodyText}</p>
            <ul className="sl-aus-modal-description">
              <li>Ad-Free and Premium Cams</li>
              <li>Trusted Forecast Team</li>
              <li>Exclusive Forecast Tools</li>
              <li>Cam Rewind</li>
            </ul>
            <button
              type="button"
              className="sl-aus-modal-learn-more"
              onClick={() => trackClicked()}
            >
              {buttonText}
            </button>
            <p className="sl-aus-modal-cancel">Cancel Anytime</p>
          </div>
        </div>
      );
    }
    if (variantClass === 'v2') {
      return (
        <div className="sl-aus-modal-bottom-panel">
          <div className="sl-aus-modal-klein" />
          <div className="sl-aus-modal-cta">
            <p className="sl-aus-modal-tagline">{bodyText}</p>
            <p className="sl-aus-modal-description">Enjoy 3 free months of Surfline Premium.</p>
            <p className="sl-aus-modal-description">Cancel anytime.</p>
            <button
              type="button"
              className="sl-aus-modal-learn-more"
              onClick={() => trackClicked()}
            >
              {buttonText}
            </button>
          </div>
        </div>
      );
    }
    if (variantClass === 'v3') {
      return (
        <div className="sl-aus-modal-bottom-panel">
          <div className="sl-aus-modal-klein" />
          <div className="sl-aus-modal-cta">
            <p className="sl-aus-modal-tagline">{bodyText}</p>
            {/* <ul className="sl-aus-modal-description">
              <li>• Enjoy 700+ Cams</li>
              <li>• 16-day Forecasts</li>
              <li>• Expert Analysis</li>
              <li>• Cam Rewind</li>
            </ul> */}
            <div className="sl-aus-modal-description">
              <p>• Enjoy 700+ Cams</p>
              <p>• 16-day Forecasts</p>
              <p>• Expert Analysis</p>
              <p>• Cam Rewind</p>
            </div>
            <button
              type="button"
              className="sl-aus-modal-learn-more"
              onClick={() => trackClicked()}
            >
              {buttonText}
            </button>
            <p className="sl-aus-modal-cancel">
              Then $6.50/month after trial ends.
              <br /> Billed annually. Cancel anytime.
            </p>
          </div>
        </div>
      );
    }
    return (
      <div className="sl-aus-modal-bottom-panel">
        <div className="sl-aus-modal-klein" />
        <div className="sl-aus-modal-cta">
          <p className="sl-aus-modal-tagline">{bodyText}</p>
          <p className="sl-aus-modal-description">Then $6.50/month after trial ends.</p>
          <p className="sl-aus-modal-description">Billed annually. Cancel anytime.</p>
          <button type="button" className="sl-aus-modal-learn-more" onClick={() => trackClicked()}>
            {buttonText}
          </button>
        </div>
      </div>
    );
  };

  return hideModal ? null : (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={() => toggleModal(false)}
      className={`sl-aus-modal-container-${variantClass}`}
      overlayClassName="sl-aus-modal-overlay"
      ariaHideApp={false}
      shouldCloseOnOverlayClick
      shouldCloseOnEsc
    >
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic%7CIBM+Plex+Sans:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic"
        media="all"
      />
      {!isMobileView || variantClass === 'v3' || variantClass === 'v4' ? (
        <div className="sl-aus-modal-top-banner">
          {variantClass === 'v3' || variantClass === 'v4' ? (
            <div className="sl-aus-modal-gradient" />
          ) : null}
          <div className="sl-aus-modal-lockup" />
        </div>
      ) : null}
      {getVariantDescription()}
      {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
      <div className="sl-aus-modal-close" onClick={() => toggleModal(false)} />
    </Modal>
  );
};

AusModal.propTypes = {
  isPremium: PropTypes.bool.isRequired,
  pageName: PropTypes.string.isRequired,
  treatment: PropTypes.string.isRequired,
};

export default AusModal;
