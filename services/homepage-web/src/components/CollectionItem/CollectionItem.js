/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { motion } from 'framer-motion';
import classNames from 'classnames';
import { trackEvent } from '@surfline/web-common';
import { collectionVariants } from './variants';
import createAccessibleOnClick, { BTN } from '../../utils/createAccessibleOnClick';
import { setActiveCollection, SUBREGIONS } from '../../actions/multiCam';
import spotPropType from '../../propTypes/spot';

const itemClassName = (isSelected) =>
  classNames({
    'sl-collection-selector__category__collections_collection-container': true,
    'sl-collection-selector__category__collections_collection-container--selected': isSelected,
  });

const CollectionItem = ({ collection, isSelected, category }) => {
  const dispatch = useDispatch();

  const doSetActiveCollection = () => {
    dispatch(setActiveCollection(collection));
    trackEvent('Clicked Multi-Cam Collections', {
      category: 'cams & reports',
      collectionType: category,
      name: collection.name,
      view: collection.type === SUBREGIONS ? 'Favorite Subregion' : collection.name,
      subregionId: collection.type === SUBREGIONS ? collection._id : '',
      subregionName: collection.type === SUBREGIONS ? collection.name : '',
    });
  };

  return (
    <motion.button
      variants={collectionVariants}
      disabled={isSelected}
      {...createAccessibleOnClick(doSetActiveCollection, BTN)}
      className={itemClassName(isSelected)}
    >
      <p className="sl-collection-selector__category__collections_collection">{collection.name}</p>
    </motion.button>
  );
};

CollectionItem.propTypes = {
  collection: PropTypes.shape({
    name: PropTypes.string.isRequired,
    spots: PropTypes.arrayOf(spotPropType).isRequired,
    subregionId: PropTypes.string,
    subregionName: PropTypes.string,
    type: PropTypes.string,
    _id: PropTypes.string,
  }).isRequired,
  isSelected: PropTypes.bool.isRequired,
  category: PropTypes.string.isRequired,
};

export default CollectionItem;
