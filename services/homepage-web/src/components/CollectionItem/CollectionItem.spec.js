/** @prettier */

import { expect } from 'chai';
import sinon from 'sinon';

import CollectionItem from './CollectionItem';
import collectionsFixture from '../../utils/fixtures/collections';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import * as multiCam from '../../actions/multiCam';

describe('components / CollectionItem', () => {
  let setActiveCollectionStub;
  const defaultProps = {
    collection: collectionsFixture[0],
    isSelected: false,
  };

  beforeEach(() => {
    setActiveCollectionStub = sinon
      .stub(multiCam, 'setActiveCollection')
      .returns({ type: 'TEST', action: null });
  });
  afterEach(() => {
    setActiveCollectionStub.restore();
  });

  it('should render the collection item', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionItem, props);

    expect(wrapper).to.have.length(1);
  });

  it('should handle clicking on a collection item', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionItem, props);

    expect(wrapper).to.have.length(1);

    wrapper
      .find('button.sl-collection-selector__category__collections_collection-container')
      .simulate('click');
    expect(setActiveCollectionStub).to.have.been.calledOnce();
    expect(setActiveCollectionStub).to.have.been.calledWithExactly(collectionsFixture[0]);

    wrapper
      .find('button.sl-collection-selector__category__collections_collection-container')
      .simulate('keyDown', { key: ' ', keyCode: 32, which: 32 });
    wrapper.update();
    expect(setActiveCollectionStub).to.have.been.calledTwice();
  });

  it('should give the container a "selected" className if selected', () => {
    const props = {
      ...defaultProps,
      isSelected: true,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionItem, props);

    expect(wrapper).to.have.length(1);

    expect(
      wrapper.find(
        'button.sl-collection-selector__category__collections_collection-container--selected'
      )
    ).to.have.length(1);
  });
});
