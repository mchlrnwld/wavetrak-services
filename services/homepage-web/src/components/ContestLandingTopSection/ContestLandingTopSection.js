import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { get as _get } from 'lodash';
import { Spinner, useDeviceSize } from '@surfline/quiver-react';
import PromoBoxSlides from '../PromoBoxSlides';
import articlePropType from '../../propTypes/article';
import contestParamsPropType from '../../propTypes/contestParams';
import config from '../../config';

const ContestLandingTopSection = ({
  contestNews,
  region,
  period,
  contest,
  contestType,
  match: { params },
  imageResizing,
  feedLocale,
}) => {
  const device = useDeviceSize();
  const newsWithSuperHeader = contestNews.news.map((slide) => {
    const { acf } = slide;
    const feedSuperheader = acf && `${acf.attribution_text} ${acf.sponsor_name}`;
    return {
      ...slide,
      feedSuperheader,
    };
  });
  const contestURL =
    period === '2021'
      ? '/surf-news/how-to-enter-the-spirit-of-surfing-cuervo-challenge-2021/118322'
      : '/surf-news/enter-spirit-surfing-cuervo-challenge/91126';
  return (
    <div className="sl-contest-landing__top">
      {(device.width > 976 || contestType === 'wotw' || contestType === 'sos') && (
        <div className="sl-contest-landing__top__promobox">
          {contestNews.loading ? <Spinner /> : null}
          {contestNews.news.length ? (
            <PromoBoxSlides
              promoBoxSlides={newsWithSuperHeader}
              imageResizing={imageResizing}
              feedLocale={feedLocale}
            />
          ) : null}
        </div>
      )}
      <div className="sl-contest-landing__top__nav">
        {_get(contest, 'meta.acf.logo', null) ? (
          <div className="sl-contest-landing__top__nav__logo">
            <img src={contest.meta.acf.logo} alt={contest.name} />
          </div>
        ) : null}
        <div className="sl-contest-landing__top__nav__links">
          <a href={`/${config.baseContestUrl(region, period, params.contest)}entries`}>
            View entries
          </a>
          {contestType === 'sos' ? (
            <a href={contestURL}>How to Enter</a>
          ) : (
            <a href={`/${config.baseContestUrl(region, period, params.contest)}details`}>
              Contest Details
            </a>
          )}
          <a href={`/${config.baseContestUrl(region, period, params.contest)}submit`}>
            Submit a video
          </a>
          {contest && contest.meta.acf.vote_url && contestType !== 'sos' ? (
            <a href={contest.meta.acf.vote_url}>Vote</a>
          ) : null}
        </div>
      </div>
    </div>
  );
};

ContestLandingTopSection.propTypes = {
  contestType: PropTypes.oneOf(['sos', 'wotw']),
  contestNews: {
    loading: PropTypes.bool,
    news: PropTypes.arrayOf(articlePropType),
  }.isRequired,
  region: PropTypes.string.isRequired,
  period: PropTypes.string.isRequired,
  contest: PropTypes.string.isRequired,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
  imageResizing: PropTypes.bool.isRequired,
  feedLocale: PropTypes.string.isRequired,
};

ContestLandingTopSection.defaultProps = {
  contestType: 'wotw',
};

export default withRouter(ContestLandingTopSection);
