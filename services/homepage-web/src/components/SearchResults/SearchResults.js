import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import {
  CamIcon,
  ForecastIcon,
  LocationIcon,
  TravelIcon,
  NewsIcon,
  TrackableLink,
} from '@surfline/quiver-react';
import { v1 as uuidV1 } from 'uuid';
import classnames from 'classnames';
import { canUseDOM, slugify } from '@surfline/web-common';
import searchSiteResults from '../../propTypes/searchSiteResults';
import searchResultsSection from '../../propTypes/searchResultsSection';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const getClassNames = (_type) =>
  classnames({
    result__name: true,
    result__name__news: _type === 'editorial' || _type === 'forecast',
  });

const SearchResultLink = ({ _source, searchResultIndex, term, result, _type, children }) => {
  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      searchId: uuidV1(),
      searchResultRank: searchResultIndex + 1,
      sourcePage: canUseDOM ? window.location.href : null,
      destinationPage: _source.href || _source.permalink,
      queryTerm: term,
      resultName: _source.name || _source.content?.title,
      searchLatency: result.took,
      resultType: _type === 'editorial' || _type === 'forecast' ? 'news' : _type,
      location: 'results page',
    }),
    [
      _source.href,
      _source.permalink,
      term,
      _source.name,
      _source.content?.title,
      result.took,
      _type,
      searchResultIndex,
    ]
  );

  return (
    <TrackableLink
      ref={linkRef}
      eventName="Clicked Search Results"
      eventProperties={eventProperties}
    >
      <a ref={linkRef} href={_source.href || _source.permalink}>
        {children}
      </a>
    </TrackableLink>
  );
};

const SearchResults = ({ doShowAllToggle, results, sections, term }) => (
  <div className="sl-search__results">
    {results.map((result) => {
      const [key] = Object.keys(result.suggest);
      const section = sections[key];
      const { hits } = result.hits;
      const suggestions = result.suggest[key][0].options;
      const transformSuggest = suggestions
        .filter((option) => hits.map((hit) => hit._id).indexOf(option._id) <= -1)
        .map((filteredOption) => ({ ...filteredOption, isSuggestion: true }));
      const searchResults = [...hits, ...transformSuggest].slice(0, 50);
      const count = sections[key].showAll ? searchResults.length : null;

      return (
        searchResults.length > 0 && (
          <section id={slugify(section.name)} key={key}>
            <div>
              <h2>{section.name}</h2>
              {searchResults
                .slice(0, count || section.initialCount)
                .map(({ _source, _id, _type, _index, isSuggestion }, searchResultIndex) => (
                  <div className="result" key={`${key}-${_id}`}>
                    <SearchResultLink
                      _source={_source}
                      searchResultIndex={searchResultIndex}
                      term={term}
                      result={result}
                      _type={_type}
                    >
                      {(() => {
                        if (key === 'spot-suggest') {
                          const hasCams =
                            _source.cams && _source.cams !== null && _source.cams.length > 0;
                          const isMultiCam = hasCams && _source.cams.length > 1;
                          return hasCams ? (
                            <CamIcon isMultiCam={isMultiCam} />
                          ) : (
                            <div className="blank" />
                          );
                        }
                        return null;
                      })()}
                      {key === 'subregion-suggest' ? <ForecastIcon /> : null}
                      {key === 'geoname-suggest' ? <LocationIcon /> : null}
                      {key === 'travel-suggest' ? <TravelIcon /> : null}
                      {key === 'feed-tag-suggest' ? (
                        <div>
                          <NewsIcon />
                        </div>
                      ) : null}
                      <div className="result__container">
                        {(() => {
                          const newsResult = _index === 'feed';
                          const searchedField = newsResult ? _source.content.title : _source.name;
                          const indexOfSearchTerm = searchedField
                            .toLowerCase()
                            .indexOf(term.toLowerCase());
                          if (newsResult && isSuggestion) {
                            return <span className={getClassNames(_type)}>{searchedField}</span>;
                          }
                          if (indexOfSearchTerm > -1) {
                            const searchTermLength = term.length;
                            const resultTermLength = searchedField.length;
                            const endOfHighlight = indexOfSearchTerm + searchTermLength;
                            return (
                              <span className={getClassNames(_type)}>
                                {searchedField.substring(0, indexOfSearchTerm)}
                                <highlight key={indexOfSearchTerm}>
                                  {searchedField.substring(indexOfSearchTerm, endOfHighlight)}
                                </highlight>
                                {searchedField.substring(endOfHighlight, resultTermLength)}
                              </span>
                            );
                          }
                          return (
                            <span className={getClassNames(_type)}>
                              {searchedField
                                .toLowerCase()
                                .split('')
                                .map((char, index) =>
                                  term.toLowerCase().indexOf(char) > -1 &&
                                  index <= searchedField.toLowerCase().indexOf(char) ? (
                                    <highlight key={searchedField[index]}>
                                      {searchedField[index]}
                                    </highlight>
                                  ) : (
                                    searchedField[index]
                                  )
                                )}
                            </span>
                          );
                        })()}
                        {(() => {
                          if (_type === 'editorial') {
                            return _source.tags && _source.tags.length > 0 ? (
                              <span className="result__breadcrumb">{_source.tags[0].name}</span>
                            ) : null;
                          }
                          //eslint-disable-line
                          return _source.breadCrumbs && _source.breadCrumbs.length > 1 ? (
                            <span className="result__breadcrumb">
                              {_source.breadCrumbs[0]} /{' '}
                              {_source.breadCrumbs[_source.breadCrumbs.length - 1]}
                            </span>
                          ) : null;
                        })()}
                      </div>
                    </SearchResultLink>
                  </div>
                ))}
            </div>
            {searchResults.length > section.initialCount ? (
              <footer>
                <span {...createAccessibleOnClick(() => doShowAllToggle(key))}>
                  {sections[key].showAll ? (
                    <span>Hide {searchResults.length - section.initialCount} results</span>
                  ) : (
                    <span>See all {searchResults.length} results</span>
                  )}
                </span>
              </footer>
            ) : null}
          </section>
        )
      );
    })}
  </div>
);

SearchResults.propTypes = {
  doShowAllToggle: PropTypes.func.isRequired,
  results: searchSiteResults.isRequired,
  sections: searchResultsSection.isRequired,
  term: PropTypes.string.isRequired,
};

SearchResultLink.propTypes = {
  _source: PropTypes.shape({
    href: PropTypes.string,
    permalink: PropTypes.string,
    name: PropTypes.string,
    content: PropTypes.shape({
      title: PropTypes.string,
    }),
  }).isRequired,
  _type: PropTypes.string.isRequired,
  result: PropTypes.shape({
    took: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  }).isRequired,
  searchResultIndex: PropTypes.number.isRequired,
  term: PropTypes.string.isRequired,
  children: PropTypes.element,
};

SearchResults.defaultProps = {};

SearchResultLink.defaultProps = {
  children: null,
};

export default SearchResults;
