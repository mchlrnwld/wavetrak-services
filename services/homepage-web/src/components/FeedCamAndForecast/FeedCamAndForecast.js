import PropTypes from 'prop-types';
import React from 'react';
import CamOfTheMoment from '../CamOfTheMoment';
import FeedForecast from '../FeedForecast';
import camOfTheMomentProptype from '../../propTypes/camOfTheMoment';
import forecastPropType from '../../propTypes/forecast';
import config from '../../config';

const FeedCamAndForecast = ({
  camOfTheMoment,
  isPremium,
  adblock,
  onDetectedAdBlock,
  onSetHomeForecast,
  qaFlag,
  forecast,
  doFetchForecastArticles,
  cotmId,
  isLoggedIn,
}) => (
  <div className="sl-feed-cam-and-forecast">
    {camOfTheMoment ? (
      <CamOfTheMoment
        id={cotmId}
        isPremium={isPremium}
        adblock={adblock}
        camOfTheMoment={camOfTheMoment}
        onDetectedAdBlock={onDetectedAdBlock}
        qaFlag={qaFlag}
      />
    ) : (
      <div className="sl-feed-cam-and-forecast__cam-placeholder" />
    )}
    {!isLoggedIn ? (
      <div className="sl-feed-cam-and-forecast__register">
        <p>
          <a href={config.createAccountUrl}>Sign up</a>
          {' or '}
          <a href={config.signInUrl}>log in</a>
          {' to personalize your headlines and forecasts.'}
        </p>
      </div>
    ) : null}
    {forecast.success ? (
      <FeedForecast
        articles={forecast.articles}
        doFetchForecastArticles={doFetchForecastArticles}
        subregionId={forecast.overview._id}
        subregionName={forecast.overview.name}
        summary={forecast.overview.forecastSummary}
        breadcrumb={forecast.overview.breadcrumb}
        conditions={forecast.conditions}
        utcOffset={forecast.associated.utcOffset}
        units={forecast.associated.units}
        onSetHomeForecast={onSetHomeForecast}
        subregions={forecast.subregions}
        isLoggedIn={isLoggedIn}
        loading={forecast.loading}
      />
    ) : null}
  </div>
);

FeedCamAndForecast.propTypes = {
  camOfTheMoment: camOfTheMomentProptype,
  isPremium: PropTypes.bool,
  adblock: PropTypes.bool,
  onDetectedAdBlock: PropTypes.func,
  onSetHomeForecast: PropTypes.func.isRequired,
  qaFlag: PropTypes.string,
  forecast: forecastPropType.isRequired,
  doFetchForecastArticles: PropTypes.func.isRequired,
  cotmId: PropTypes.string.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
};

FeedCamAndForecast.defaultProps = {
  camOfTheMoment: null,
  isPremium: false,
  adblock: false,
  qaFlag: null,
  onDetectedAdBlock: () => {},
};

export default FeedCamAndForecast;
