import React from 'react';
import classNames from 'classnames';
import PopularLinksList from '../PopularLinksList';
import popularLinksMap from './popularLinksMap.json';
import ca from './images/ca.jpg';
import fl from './images/fl.jpg';
import hi from './images/hi.jpg';
import au from './images/au.jpg';

const getPopularLinksTitleClassName = (location) =>
  classNames({
    'sl-popular-links__location-title': true,
    'sl-popular-links__location-title--img': location.imgSrc,
  });

const popularLinksImgMap = { ca, fl, hi, au };

const PopularLinks = () => (
  <div className="sl-popular-links">
    <h2 className="sl-popular-links__title">Popular Cams & Forecasts</h2>
    <div className="sl-popular-links__locations">
      {popularLinksMap.map((location) => (
        <div className="sl-popular-links__location" key={location.title}>
          <h3
            className={getPopularLinksTitleClassName(location)}
            style={
              location.imgSrc
                ? { backgroundImage: `url('${popularLinksImgMap[location.imgSrc]}')` }
                : {}
            }
          >
            <a className="sl-popular-links__location-title__link" href={location.path}>
              {location.title} Surf Reports & Forecasts
            </a>
          </h3>
          <div className="sl-popular-links__location__content">
            <PopularLinksList title="Regional Surf Forecasts & Maps" list={location.forecasts} />
            <PopularLinksList title="Surf Spots" list={location.spots} />
            <a className="sl-popular-links__travel" href={location.travel.path}>
              Surfing in {location.travel.title} Guide
            </a>
          </div>
        </div>
      ))}
    </div>
  </div>
);

export default PopularLinks;
