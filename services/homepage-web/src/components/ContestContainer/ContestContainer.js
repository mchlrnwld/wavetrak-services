import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import { useCookies } from 'react-cookie';
import { get as _get } from 'lodash';
import { withRouter } from 'react-router-dom';
import AgeVerification from '../AgeVerification';
import ContestNav from '../ContestNav';
import contestPropType from '../../propTypes/contest';
import contestParamsPropType from '../../propTypes/contestParams';

const Contests = ({
  children,
  contest,
  period,
  region,
  showLogo,
  match: { params },
  bgColorPrimary,
  page,
}) => {
  const bgImage = _get(contest, 'meta.acf.background_image', null);
  let bgColor = _get(contest, 'meta.acf.background_color', null);
  if (bgColorPrimary) bgColor = bgColorPrimary;
  const contestStyles = {
    backgroundImage: bgImage ? `url(${bgImage})` : null,
    backgroundColor: bgColor || null,
  };
  const [displayModal, setModal] = useState(false);
  const [cookies] = useCookies(['sos-age-verification']);
  useEffect(() => {
    setModal(params.contest === 'spirit-of-surfing' && cookies && !cookies['sos-age-verification']);
  }, [params.contest, cookies]);

  return (
    <div
      className={classnames({
        'sl-contest': true,
        'sl-contest--sos': params.contest === 'spirit-of-surfing',
        'sl-contest--wotw': params.contest === 'wave-of-the-winter',
      })}
      style={contestStyles}
    >
      {displayModal ? <AgeVerification pageName={page} /> : null}
      <ContestNav contest={contest} region={region} period={period} showLogo={showLogo} />
      <div className="sl-contest-container">{children}</div>
    </div>
  );
};

Contests.propTypes = {
  children: PropTypes.node.isRequired,
  contest: contestPropType.isRequired,
  period: PropTypes.string,
  region: PropTypes.string,
  showLogo: PropTypes.bool,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
  bgColorPrimary: PropTypes.string,
  page: PropTypes.string,
};

Contests.defaultProps = {
  period: null,
  region: null,
  showLogo: true,
  bgColorPrimary: null,
  page: 'Unknown',
};

export default withRouter(Contests);
