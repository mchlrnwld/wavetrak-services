import React from 'react';
import PropTypes from 'prop-types';

const PremiumLandingFeatures = ({ features }) => (
  <div id="premium-landing-features" className="premium-landing-features">
    <div className="premium-landing-features__container">
      {features.map(({ icon, name }) => {
        const anchor = name.replace(/\s+/g, '-').toLowerCase();
        return (
          <a href={`#${anchor}`} key={name} className="premium-landing-features__feature">
            <div className="premium-landing-features__icon">{icon}</div>
            <div className="premium-landing-features__name">{name}</div>
          </a>
        );
      })}
    </div>
  </div>
);

PremiumLandingFeatures.propTypes = {
  features: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.node.isRequired,
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default PremiumLandingFeatures;
