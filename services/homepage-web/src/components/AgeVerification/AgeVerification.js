import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import { Button, ModalContainer, Checkbox, useDeviceSize } from '@surfline/quiver-react';
import { canUseDOM, trackEvent } from '@surfline/web-common';

const AgeVerification = ({ pageName }) => {
  // eslint-disable-next-line no-unused-vars
  const [_, setCookie] = useCookies(['sos-age-verification']);
  const [modalIsOpen, toggleModal] = useState(true);
  const [rememberMe, toggleRememberMe] = useState(true);
  const device = useDeviceSize();
  const isMobileView = device.width < 976;
  const bodyText = 'Are you of legal drinking age?';
  const popupName = 'Jose Cuervo Age Verification';

  useEffect(() => {
    if (canUseDOM && modalIsOpen) {
      trackEvent('Served Popup', {
        popupName,
        pageName,
        isMobileView,
        bodyText,
      });
    }
  }, [modalIsOpen, isMobileView, pageName]);

  const trackVerfication = (buttonText) => {
    trackEvent('Clicked Popup', {
      popupName,
      pageName,
      isMobileView,
      bodyText,
      buttonText,
    });
  };

  const onYesClick = () => {
    trackVerfication('yes');
    setCookie('sos-age-verification', true, {
      maxAge: rememberMe ? 86400 : 1800, // 1 day or 30 mins in seconds
    });
    toggleModal(false);
  };

  const onNoClick = () => {
    if (canUseDOM) {
      trackVerfication('no');
      window.location.replace('https://www.responsibility.org/');
    }
  };

  return (
    <ModalContainer
      isOpen={modalIsOpen}
      contentLabel={bodyText}
      onClose={() => toggleModal(false)}
      location="age-verification"
    >
      <div className="sl-age-verification">
        <h3 className="sl-age-verification__heading">{bodyText}</h3>
        <div className="sl-age-verification__buttons">
          <Button onClick={onYesClick}>Yes, I am</Button>
          <Button onClick={onNoClick} type="info">
            No, I’m not
          </Button>
        </div>
        <Checkbox
          copy="Remember Me?"
          checked={rememberMe}
          onClick={() => toggleRememberMe(!rememberMe)}
          checkboxName="remember-age-verification"
        />
        <p className="sl-age-verification__message">
          You must be of legal drinking age in your country to enter
        </p>
      </div>
    </ModalContainer>
  );
};

AgeVerification.propTypes = {
  pageName: PropTypes.string.isRequired,
};

export default AgeVerification;
