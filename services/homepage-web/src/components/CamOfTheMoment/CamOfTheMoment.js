import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classnames from 'classnames';
import { CamPlayer, SurfConditions, SurfHeight, TideArrow } from '@surfline/quiver-react';
import { trackClickedSubscribeCTA } from '@surfline/web-common';

import en from '../../intl/translations/en';
import config from '../../config';
import camOfTheMomentProptypes from '../../propTypes/camOfTheMoment';
import spotReportPath from '../../utils/urls/spotReportPath';

class CamOfTheMoment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      camIsStreaming: false,
      midRollIsPlaying: false,
      preRollIsPlaying: false,
    };
  }

  onToggleMidRoll = (midRollIsPlaying) =>
    this.setState({
      midRollIsPlaying,
    });

  onTogglePreRoll = (preRollIsPlaying) =>
    this.setState({
      preRollIsPlaying,
    });

  onToggleStreaming = (camIsStreaming) =>
    this.setState({
      camIsStreaming,
    });

  getCamClasses = (displayCamControls) =>
    classnames({
      'sl-cam-of-the-moment': true,
      'sl-cam-of-the-moment--controls-visible': displayCamControls,
    });

  getCamTitleClasses = (displaySpotDetails) =>
    classnames({
      'sl-cam-of-the-moment__title': true,
      'sl-cam-of-the-moment__title--visible': displaySpotDetails,
    });

  getSpotDetailsClasses = (displaySpotDetails) =>
    classnames({
      'sl-cam-of-the-moment__details': true,
      'sl-cam-of-the-moment__details--visible': displaySpotDetails,
    });

  displayCamControls = () => {
    let displayControls = true;
    const { camIsStreaming, midRollIsPlaying, preRollIsPlaying } = this.state;
    if (camIsStreaming || midRollIsPlaying) displayControls = false;
    else if (!camIsStreaming && !preRollIsPlaying) displayControls = false;
    return displayControls;
  };

  getIsCamFavorite = () => {
    const { favorites, camOfTheMoment } = this.props;
    const { spot } = camOfTheMoment;

    const spotId = spot._id;

    return !!favorites.find((favorite) => favorite._id === spotId);
  };

  render() {
    const { preRollIsPlaying, midRollIsPlaying } = this.state;
    const { id, camOfTheMoment, isPremium, adblock, onDetectedAdBlock, qaFlag } = this.props;
    const { spot, units, advertising } = camOfTheMoment;
    const conditions = spot.conditions.value || 'LOLA';

    const midRollSegmentProperties = {
      location: 'mid roll',
      pageName: 'hompage',
      spotId: spot.spotId,
    };

    const adBlockSegmentProperties = {
      location: 'cam player ad blocker',
      pageName: 'homepage',
      spotId: spot.spotId,
    };

    const onClickAdBlock = () => {
      trackClickedSubscribeCTA(adBlockSegmentProperties);
    };
    const onClickUpSell = () => {
      trackClickedSubscribeCTA(midRollSegmentProperties);
    };

    return (
      <div className={this.getCamClasses(this.displayCamControls())}>
        <div className={this.getCamTitleClasses(!midRollIsPlaying && !preRollIsPlaying)}>
          {en.Feed.camOfTheMoment.title}
        </div>
        <CamPlayer
          onToggleMidRoll={this.onToggleMidRoll}
          onTogglePreRoll={this.onTogglePreRoll}
          onToggleStreaming={this.onToggleStreaming}
          playerId={`sl-cam-of-the-moment__player--${id}`}
          camera={spot.camera}
          advertisingIds={advertising}
          spotName={spot.name}
          spotId={spot._id}
          legacySpotId={spot.legacySpotId}
          isPremium={isPremium}
          adblock={adblock && !isPremium}
          funnelUrl={config.funnelUrl}
          companionHandler={() => true}
          onDetectedAdBlock={() => onDetectedAdBlock(spot._id, spot.spotName)}
          onClickAdBlock={onClickAdBlock}
          onClickUpSell={onClickUpSell}
          viewtype="HOME"
          adTag={{ iu: '/1024858/Video_Home' }}
          qaFlag={qaFlag}
          location="Cam Of The Moment"
          isFavorite={this.getIsCamFavorite()}
        />
        <div className={this.getSpotDetailsClasses(!midRollIsPlaying && !preRollIsPlaying)}>
          <div className="sl-cam-of-the-moment__details__spot-name">
            <SurfConditions
              conditions={conditions}
              expired={spot.conditions.expired && spot.conditions.human}
            />
            <a href={spotReportPath(spot._id, spot.name)}>{spot.name}</a>
          </div>
          <div className="sl-cam-of-the-moment__details__conditions">
            <span className="sl-cam-of-the-moment__details__conditions__surf">
              <SurfHeight
                min={spot.waveHeight.min}
                max={spot.waveHeight.max}
                units={units.waveHeight}
                plus={spot.waveHeight.plus}
              />
            </span>
            {spot.tide ? (
              <span className="sl-cam-of-the-moment__details__conditions__tide">
                {spot.tide.current.height}
                {units.tideHeight}
                <TideArrow direction={spot.tide.next.type === 'LOW' ? 'DOWN' : 'UP'} />
              </span>
            ) : null}
            <span className="sl-cam-of-the-moment__details__conditions__wind">
              {spot.wind.speed}
              {units.windSpeed}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

CamOfTheMoment.propTypes = {
  id: PropTypes.string.isRequired,
  isPremium: PropTypes.bool,
  adblock: PropTypes.bool,
  camOfTheMoment: camOfTheMomentProptypes,
  onDetectedAdBlock: PropTypes.func,
  qaFlag: PropTypes.string,
  favorites: PropTypes.arrayOf(PropTypes.shape({ _id: PropTypes.string })),
};

CamOfTheMoment.defaultProps = {
  isPremium: false,
  adblock: false,
  camOfTheMoment: {},
  onDetectedAdBlock: () => {},
  qaFlag: null,
  favorites: [],
};

export default CamOfTheMoment;
