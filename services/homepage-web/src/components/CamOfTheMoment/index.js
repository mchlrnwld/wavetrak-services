import { getUserFavorites } from '@surfline/web-common';
import { connect } from 'react-redux';
import CamOfTheMoment from './CamOfTheMoment';

export default connect((state) => ({
  favorites: getUserFavorites(state),
}))(CamOfTheMoment);
