import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import { CamPlayerV2, Favorite } from '@surfline/quiver-react';
import { Home } from './Icons';
import { Close } from '../HomeCam/Icons';
import browserDataPropShape from '../../propTypes/browserData';
import homeCamPropType from '../../propTypes/homecam';
import userSettingsPropType from '../../propTypes/userSettings';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';
import { useSpotIsFavorite } from '../../hooks/useSpotIsFavorite';

const getHomeCamPlayerClasses = (browser) =>
  classnames({
    'sl-home-cam-player': true,
    'sl-home-cam-player__ff': browser.name === 'Firefox',
  });

const HomeCamPlayer = ({
  doToggleHomeCamSelectView,
  doToggleHomeCamVisibility,
  homecam,
  userSettings,
  browserData,
}) => {
  const { spot } = homecam;
  const isFavorite = useSpotIsFavorite(spot._id);

  return (
    <div className={getHomeCamPlayerClasses(browserData)}>
      <div
        {...createAccessibleOnClick(doToggleHomeCamSelectView)}
        className="sl-home-cam-player__home"
      >
        <Home className="sl-home-cam-player__home-icon" />
      </div>
      <div
        {...createAccessibleOnClick(doToggleHomeCamVisibility)}
        className="sl-home-cam-player__close"
      >
        <Close className="sl-home-cam-player__close-icon" />
      </div>
      <div className="sl-home-cam-player__spot">
        <Favorite settings={userSettings} favorite={homecam.spot} onClickLink={() => {}} />
      </div>
      <div className="sl-home-cam-player__camera">
        <CamPlayerV2
          camera={homecam.camera}
          aspectRatio="16:9"
          playerId="homecam"
          location="Home Cam"
          isFavorite={isFavorite}
          spotId={spot._id}
          spotName={spot.name}
        />
      </div>
    </div>
  );
};

HomeCamPlayer.propTypes = {
  doToggleHomeCamSelectView: PropTypes.func.isRequired,
  doToggleHomeCamVisibility: PropTypes.func.isRequired,
  homecam: homeCamPropType.isRequired,
  userSettings: userSettingsPropType.isRequired,
  browserData: browserDataPropShape.isRequired,
};

export default HomeCamPlayer;
