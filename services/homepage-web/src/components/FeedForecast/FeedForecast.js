import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import {
  FeedArticlesList,
  ForecasterProfile,
  Spinner,
  TrackableLink,
} from '@surfline/quiver-react';
import ForecastConditions from '../ForecastConditions';
import FeedForecastHeader from '../FeedForecastHeader';
import articlePropType from '../../propTypes/article';
import subregionPropType from '../../propTypes/subregion';
import unitsPropType from '../../propTypes/units';
import forecastConditionPropType from '../../propTypes/forecastCondition';
import forecastSummaryPropType from '../../propTypes/forecastSummary';
import subregionForecastPath from '../../utils/urls/subregionForecastPath';

const FeedForecast = ({
  articles,
  doFetchForecastArticles,
  subregionId,
  subregionName,
  summary,
  breadcrumb,
  conditions,
  units,
  utcOffset,
  subregions,
  onSetHomeForecast,
  isLoggedIn,
  loading,
}) => {
  const forecastLinkRef = useRef();
  const forecastLink = subregionForecastPath(subregionId, subregionName);

  const forecastEventProperties = useMemo(
    () => ({
      linkLocation: 'forecast outlook',
      category: 'homepage',
      linkName: 'View 17-day forecast',
      linkUrl: forecastLink,
    }),
    [forecastLink]
  );

  return (
    <div className="sl-feed-forecast">
      {!loading ? (
        <div>
          <FeedForecastHeader
            onSetHomeForecast={onSetHomeForecast}
            subregionName={subregionName}
            subregions={subregions}
            isLoggedIn={isLoggedIn}
            mapUrl={breadcrumb[breadcrumb.length - 1].href}
          />
          {articles.regional.length ? (
            <FeedArticlesList
              articles={articles}
              doFetchForecastArticles={doFetchForecastArticles}
              subregionId={subregionId}
              subregionName={subregionName}
              location="homepage"
            />
          ) : null}
          <div className="sl-feed-forecast__heading__container">
            <h5 className="sl-feed-forecast__heading">{subregionName} Forecast</h5>
            <TrackableLink
              ref={forecastLinkRef}
              eventName="Clicked Link"
              eventProperties={forecastEventProperties}
            >
              <a
                ref={forecastLinkRef}
                className="sl-feed-forecast__heading__container__link"
                href={forecastLink}
              >
                View 17-day forecast
              </a>
            </TrackableLink>
          </div>
          <div className="sl-feed-forecast__observation">
            {summary.forecaster && summary.forecastStatus.status === 'active' ? (
              <ForecasterProfile
                forecasterName={summary.forecaster.name}
                forecasterTitle={summary.forecaster.title}
                url={summary.forecaster.iconUrl}
                small
              />
            ) : null}
          </div>
          <div className="sl-feed-forecast__conditions">
            {conditions.slice(0, 2).map((condition) => (
              <ForecastConditions
                forecastLink={forecastLink}
                key={condition.timestamp}
                timestamp={condition.timestamp}
                am={condition.am}
                pm={condition.pm}
                units={units}
                utcOffset={utcOffset}
              />
            ))}
          </div>
        </div>
      ) : (
        <div className="sl-feed-forecast--loading">
          <Spinner />
        </div>
      )}
    </div>
  );
};

FeedForecast.propTypes = {
  articles: PropTypes.shape({
    pageCount: PropTypes.number,
    regional: PropTypes.arrayOf(articlePropType),
  }),
  doFetchForecastArticles: PropTypes.func.isRequired,
  subregionId: PropTypes.string.isRequired,
  subregionName: PropTypes.string.isRequired,
  summary: forecastSummaryPropType.isRequired,
  breadcrumb: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string,
      name: PropTypes.string,
    })
  ).isRequired,
  conditions: PropTypes.arrayOf(forecastConditionPropType).isRequired,
  units: unitsPropType.isRequired,
  utcOffset: PropTypes.number.isRequired,
  onSetHomeForecast: PropTypes.func.isRequired,
  subregions: PropTypes.arrayOf(subregionPropType).isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  loading: PropTypes.bool,
};

FeedForecast.defaultProps = {
  articles: [],
  loading: false,
};

export default FeedForecast;
