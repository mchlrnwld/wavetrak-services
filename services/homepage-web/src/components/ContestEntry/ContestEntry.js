import PropTypes from 'prop-types';
import { Cookies } from 'react-cookie';
import React, { Component } from 'react';
import { format, parseISO } from 'date-fns';
import EmbedContainer from 'react-oembed-container';
import classnames from 'classnames';
import { ShareArticle } from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';
import { fetchOembed } from '@surfline/web-common';
import { contestVoteNamespace } from '../../utils/contests';
import contestEntryPropType from '../../propTypes/contestEntry';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';
import Shaka from '../Icons/Shaka';
import config from '../../config';

const cookies = new Cookies();

class ContestEntry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      entryHtml: null,
    };
  }

  componentDidMount() {
    const { doTrackEvent, entry, period, region } = this.props;
    if (entry.entryId) {
      // Prevents event from firing for past winner/doc videos
      doTrackEvent('Viewed Contest Entry', {
        contestName: 'Wave of the Winter',
        contestDate: period,
        contestRegion: region,
        surferName: entry.surfer.name,
        entryID: entry.entryId,
      });
    }
    this.getContestEntryHtml(entry.videoUrl);
  }

  onClickShareEntry = (shareChannel, entry, doTrackEvent) => {
    doTrackEvent('Clicked Share Icon', {
      title: entry.name,
      contentId: `${entry.id}`,
      tags: null,
      locationCategory: 'Wave of the Winter',
      destinationUrl: this.getContestEntryUrl(entry),
      mediaType: 'video',
      shareChannel,
    });
  };

  getContestEntryUrl = (entry, region, period) =>
    entry
      ? `${config.homepageUrl}${config.baseContestUrl(region, period)}entries/${entry.id}`
      : null;

  getEntryClassnames = (url) => {
    const isInstagram = url.indexOf('instagram') > -1;
    return classnames({
      'sl-contest-entry__video': true,
      'sl-contest-entry__video--instagram': isInstagram,
      'sl-contest-entry__video--video': !isInstagram,
    });
  };

  getVoteClassNames = (entryId) =>
    classnames({
      'sl-contest-entry__details__votes': true,
      'sl-contest-entry__details__votes--voted': this.hasVoted(entryId),
    });

  getVoteColor = (entryId) => {
    const { isLandingPage } = this.props;
    const hasVoted = this.hasVoted(entryId);

    if (!hasVoted) {
      return color('white');
    }

    return isLandingPage ? color('black') : '#41A4FB';
  };

  hasVoted = (entryId) => cookies.get(contestVoteNamespace(entryId));

  getContestEntryHtml = async (url) => {
    const {
      oembedObj: { html },
    } = await fetchOembed(url, '/contests/oembed');
    if (html) this.setState({ entryHtml: html });
  };

  render() {
    const { entry, displayShareIcons, doTrackEvent, period, region, doUpvoteContestEntry } =
      this.props;
    const { entryHtml } = this.state;
    return (
      <div>
        <div className={this.getEntryClassnames(entry.videoUrl)}>
          {entryHtml ? (
            <EmbedContainer markup={entryHtml}>
              {/* eslint-disable-next-line react/no-danger */}
              <div dangerouslySetInnerHTML={{ __html: entryHtml }} />
            </EmbedContainer>
          ) : null}
        </div>
        <div className="sl-contest-entry__content">
          <div className="sl-contest-entry__details">
            <h5>
              {entry.entryId ? `#${entry.entryId}` : null} {entry.name}
            </h5>
            {entry.date && parseISO(entry.date) !== 'Invalid Date' ? (
              <div>
                <p>{format(parseISO(entry.date), 'MMMM dd, yyyy')}</p>
                <p>
                  Location: <span>{entry.spot.name}</span>
                </p>
              </div>
            ) : null}
          </div>
          <div>
            {displayShareIcons ? (
              <ShareArticle
                fbAppId={config.appKeys.fbAppId}
                url={this.getContestEntryUrl(entry, region, period)}
                onClickLink={(shareChannel) => {
                  this.onClickShareEntry(shareChannel, entry, doTrackEvent);
                }}
              />
            ) : null}
            <div>
              <div
                className={this.getVoteClassNames(entry.id)}
                {...createAccessibleOnClick(() => doUpvoteContestEntry(entry.id))}
              >
                <div className="sl-contest-entry__details__votes__icon">
                  <Shaka fill={this.getVoteColor(entry.id)} />
                </div>
                <div className="sl-contest-entry__details__votes__count">{entry.voteCount}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ContestEntry.propTypes = {
  isLandingPage: PropTypes.bool,
  entry: contestEntryPropType.isRequired,
  displayShareIcons: PropTypes.bool,
  doTrackEvent: PropTypes.func,
  period: PropTypes.string,
  region: PropTypes.string,
  doUpvoteContestEntry: PropTypes.func.isRequired,
};

ContestEntry.defaultProps = {
  isLandingPage: false,
  displayShareIcons: true,
  doTrackEvent: null,
  period: null,
  region: null,
};

export default ContestEntry;
