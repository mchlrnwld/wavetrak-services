/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';
import { useSelector } from 'react-redux';
import classNames from 'classnames';

import { categoryVariants } from './variants';
import CollectionItem from '../CollectionItem';
import { getActiveCollection } from '../../selectors/multiCam';

const titleClassName = (border) =>
  classNames({
    'sl-collection-selector__category__name': true,
    'sl-collection-selector__category__name--with-border': border,
  });

const CollectionCategory = ({ category }) => {
  const activeCollection = useSelector(getActiveCollection);

  return (
    <div className="sl-collection-selector__category-container" key={category.name}>
      <motion.h6 variants={categoryVariants} className={titleClassName(category.border)}>
        {category.name}
      </motion.h6>
      {category.collections.map((collection) => (
        <CollectionItem
          key={collection.name}
          isSelected={activeCollection?.name === collection.name}
          collection={collection}
          category={category.name}
        />
      ))}
    </div>
  );
};

CollectionCategory.propTypes = {
  category: PropTypes.shape({
    border: PropTypes.bool,
    name: PropTypes.string.isRequired,
    collections: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
      })
    ),
  }).isRequired,
};

export default CollectionCategory;
