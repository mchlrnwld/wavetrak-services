/** @prettier */

import { expect } from 'chai';

import CollectionCategory from './CollectionCategory';
import CollectionItem from '../CollectionItem';
import collectionsFixture, { collectionTypesFixture } from '../../utils/fixtures/collections';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('components / CollectionCategory', () => {
  const defaultProps = {
    category: {
      ...collectionTypesFixture[0],
      collections: collectionsFixture.filter(
        (collection) => collection.type === collectionTypesFixture[0].type
      ),
    },
  };
  const initialState = {
    multiCam: { active: collectionsFixture[0]._id, collections: collectionsFixture },
  };

  beforeEach(() => {
    // sinon stubs
  });
  afterEach(() => {
    // sinon restores
  });

  it('should render the collection category', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionCategory, props);

    expect(wrapper).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the collection category with the proper name', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionCategory, props);

    expect(wrapper).to.have.length(1);
    expect(wrapper.find('h6.sl-collection-selector__category__name').text()).to.be.equal(
      props.category.name
    );
    wrapper.unmount();
  });

  it('should render the collection category with the border', () => {
    const props = {
      ...defaultProps,
      category: {
        ...defaultProps.category,
        border: true,
      },
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionCategory, props);

    expect(wrapper).to.have.length(1);
    expect(wrapper.find('h6.sl-collection-selector__category__name--with-border')).to.have.length(
      1
    );
    wrapper.unmount();
  });

  it('should render the collection category without the border', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionCategory, props);

    expect(wrapper).to.have.length(1);
    expect(wrapper.find('h6.sl-collection-selector__category__name--with-border')).to.have.length(
      0
    );
    wrapper.unmount();
  });

  it('should render a selected category', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionCategory, props, {
      initialState,
    });

    expect(wrapper).to.have.length(1);

    const selectedItemWrapper = wrapper
      .find(CollectionItem)
      .filterWhere((node) => !!node.prop('isSelected'));

    expect(selectedItemWrapper).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the collection items', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionCategory, props, {
      initialState,
    });

    expect(wrapper).to.have.length(1);

    const selectedItemWrappers = wrapper.find(CollectionItem);

    expect(selectedItemWrappers).to.have.length(defaultProps.category.collections.length);
    wrapper.unmount();
  });
});
