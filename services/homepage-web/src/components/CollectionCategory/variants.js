export const categoryVariants = {
  hidden: { x: -200 },
  shown: {
    x: 0,
    transition: {
      type: 'tween',
      duration: 0.1,
    },
  },
};
