import winner2019 from './winners/2019.jpg';
import winner2018 from './winners/2018.jpg';
import winner2017 from './winners/2017.jpg';
import winner2016 from './winners/2016.jpg';
import winner2015 from './winners/2015.jpg';
import winner2014 from './winners/2014.jpg';
import winner2013 from './winners/2013.jpg';
import winner2012 from './winners/2012.jpg';
import winner2011 from './winners/2011.jpg';

const pastWinners = [
  {
    year: '2020',
    name: "Jamie O'Brien",
    imageUrl: winner2015,
    date: '20191124',
    photographer: {
      name: 'Marcelo Dado',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: "Jamie O'Brien",
    },
    videoUrl: 'https://www.youtube.com/watch?v=hm4fRUOw0Cw',
    fullMovie: {
      name: 'Wave of the Winter Movie 2020',
      videoUrl: 'https://www.youtube.com/watch?v=684uHFSt4BM',
    },
  },
  {
    year: '2019',
    name: 'Keito Matsuoka',
    imageUrl: winner2019,
    date: '20190114',
    photographer: {
      name: 'Larry Haynes',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: 'Keito Matsuoka',
    },
    videoUrl: 'https://www.youtube.com/watch?v=Gp4tyFDVstc&list=PLpi1EAR1PAPdW4XC6lbB8PVtLEmkqRXtG',
    fullMovie: {
      name: "O'Neill Wave of the Winter Movie 2019",
      videoUrl: 'https://www.youtube.com/watch?v=P5ZQXxITXPE',
    },
  },
  {
    year: '2018',
    name: 'Nathan Florence',
    imageUrl: winner2018,
    date: '20180112',
    photographer: {
      name: 'Chris Kincade',
    },
    spot: {
      name: 'Backdoor',
    },
    surfer: {
      name: 'Nathan Florence',
    },
    videoUrl: 'https://www.youtube.com/watch?v=grjgpZd9iw0',
    fullMovie: {
      name: "O'Neill Wave of the Winter Movie 2018",
      videoUrl: 'https://www.youtube.com/watch?v=7yVP8AQuOlg',
    },
  },
  {
    year: '2017',
    name: 'Koa Rothman',
    imageUrl: winner2017,
    date: '20170114',
    photographer: {
      name: 'Alex Kilauano',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: 'Koa Rothman',
    },
    videoUrl: 'https://www.youtube.com/watch?v=rlpTFHy87gg',
    fullMovie: {
      name: "O'Neill Wave of the Winter Movie 2017",
      videoUrl: 'https://www.youtube.com/watch?v=MvBnKONcOaM',
    },
  },
  {
    year: '2016',
    name: "Mikey O'Shaughnessy",
    imageUrl: winner2016,
    date: '20160213',
    photographer: {
      name: 'Matty Raynor',
    },
    spot: {
      name: 'Off-The-Wall',
    },
    surfer: {
      name: "Mikey O'Shaughnessy",
    },
    videoUrl: 'https://www.youtube.com/watch?v=AKwUPQUiPzk',
    fullMovie: {
      name: "O'Neill Wave of the Winter Movie 2016",
      videoUrl: 'https://www.youtube.com/watch?v=16feOtjHlmA',
    },
  },
  {
    year: '2015',
    name: "Jamie O'Brien",
    imageUrl: winner2015,
    date: '20141221',
    photographer: {
      name: 'Tatiane Araujo',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: "Jamie O'Brien",
    },
    videoUrl: 'https://www.youtube.com/watch?v=Rnniogg52NQ',
    fullMovie: {
      name: 'Wave of the Winter Movie 2015',
      videoUrl: 'https://www.youtube.com/watch?v=LPNX5AdcSl4',
    },
  },
  {
    year: '2014',
    name: 'Kelly Slater',
    imageUrl: winner2014,
    date: '20131221',
    photographer: {
      name: 'Bruno Lemos',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: 'Kelly Slater',
    },
    videoUrl: 'https://www.youtube.com/watch?v=5bYqqqLdEF8',
    fullMovie: {
      name: 'Wave of the Winter Movie 2014',
      videoUrl: 'https://www.youtube.com/watch?v=szdsYSgKwcA',
    },
  },
  {
    year: '2013',
    name: 'Ricardo Dos Santos',
    imageUrl: winner2013,
    date: '20121230',
    photographer: {
      name: 'Marc Beaty',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: 'Ricardo Dos Santos',
    },
    videoUrl: 'https://www.youtube.com/watch?v=7aeSg4532AI',
    fullMovie: {
      name: 'Wave of the Winter Movie 2013',
      videoUrl: 'https://www.youtube.com/watch?v=P4ynv-vRHKQ',
    },
  },
  {
    year: '2012',
    name: 'Reef McIntosh',
    imageUrl: winner2012,
    date: '20111202',
    photographer: {
      name: 'Lachlan McKinnon',
    },
    spot: {
      name: 'Off-The-Wall',
    },
    surfer: {
      name: 'Reef McIntosh',
    },
    videoUrl: 'https://www.youtube.com/watch?v=HKcyWOfAFY8',
    fullMovie: {
      name: 'Wave of the Winter Movie 2012',
      videoUrl: 'https://www.youtube.com/watch?v=3fr8KB0iXrM',
    },
  },
  {
    year: '2011',
    name: 'Kalani Chapman',
    imageUrl: winner2011,
    date: '20101115',
    photographer: {
      name: 'John D. Hall',
    },
    spot: {
      name: 'Pipeline',
    },
    surfer: {
      name: 'Kalani Chapman',
    },
    videoUrl: 'https://www.youtube.com/watch?v=ziFh39lezY4',
  },
];

export default pastWinners;
