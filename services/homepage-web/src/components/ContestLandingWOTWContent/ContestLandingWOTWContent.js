import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { GoogleDFP, Spinner } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import ContestCard from '../ContestCard';
import ContestAwardCard from '../ContestAwardCard';
import ContestEntryCard from '../ContestEntryCard';
import ContestWinnerCard from '../ContestWinnerCard';
import ContestRegionalDetails from '../ContestRegionalDetails';
import allContests from './allContests';
import pastWinners from './pastWinners';
import contestAwards from './contestAwards';
import contestEntryPropType from '../../propTypes/contestEntry';
import loadAdConfig from '../../utils/adConfig';
import getContestAdId from '../../utils/getContestAdId';
import config from '../../config';

const ContestLandingWOTWContent = ({
  region,
  period,
  contest,
  contestEntries,
  contestType,
  bgColorSecondary,
  doUpvoteContestEntry,
  imageResizing,
}) => {
  const periodRange = period.split('-');
  // eslint-disable-next-line no-unused-vars
  const [_, currentPeriodYear] = periodRange;
  const periodAwarePastWinners = pastWinners.filter((winner) => winner.year < currentPeriodYear);
  return (
    <>
      <div
        className="sl-contest-landing__entries__container"
        style={{ backgroundColor: bgColorSecondary }}
      >
        <div className="sl-contest__ad-unit">
          <GoogleDFP
            adConfig={loadAdConfig(getContestAdId(region, 'top', period, contestType), [
              ['Box', 'Banner'],
            ])}
            isHtl
            isTesting={config.htl.isTesting}
          />
        </div>
        <h3>Latest Entries</h3>
        <div className="sl-contest-landing__entries">
          {contestEntries.loading ? <Spinner /> : null}
          {contestEntries.entries.length
            ? contestEntries.entries.map((entry) => (
                <ContestEntryCard
                  isLandingPage
                  entry={entry}
                  doTrackEvent={trackEvent}
                  period={period}
                  region={region}
                  key={entry.id}
                  doUpvoteContestEntry={doUpvoteContestEntry}
                  imageResizing={imageResizing}
                />
              ))
            : null}
          {!contestEntries.entries.length && !contestEntries.loading ? (
            <p className="sl-contest-landing__no-entries">
              No entries yet. Be the first to
              <Link to={`/${config.baseContestUrl(region, period)}submit`}> Submit</Link>
            </p>
          ) : null}
        </div>
        {contestEntries.entries.length ? (
          <Link
            className="sl-contest-landing__entries__button"
            to={`/${config.baseContestUrl(region, period)}entries`}
          >
            View all entries
          </Link>
        ) : null}
      </div>
      {region === 'north-shore' ? (
        <div className="sl-contest-winners__container">
          <h3>Previous Winners</h3>
          <div className="sl-contest-winners">
            {periodAwarePastWinners.length
              ? periodAwarePastWinners.map((winner) => (
                  <ContestWinnerCard
                    winner={winner}
                    key={winner.year}
                    imageResizing={imageResizing}
                  />
                ))
              : null}
          </div>
        </div>
      ) : null}
      <div className="sl-contest__ad-unit">
        <GoogleDFP
          adConfig={loadAdConfig(getContestAdId(region, 'mid', period, contestType), [
            ['Box', 'Banner'],
          ])}
          isHtl
          isTesting={config.htl.isTesting}
        />
      </div>
      {period !== '2020-2021' && (
        <div
          className="sl-contest-awards__container"
          style={{ backgroundColor: region !== 'regional' ? bgColorSecondary : '#222' }}
        >
          <h3>{region === 'north-shore' ? 'Special Awards' : 'Contest Details'}</h3>
          <div className="sl-contest-awards">
            {region !== 'regional' && contestAwards[period] && contestAwards[period][region]
              ? contestAwards[period][region].map((award) => (
                  <ContestAwardCard award={award} key={award.title} imageResizing={imageResizing} />
                ))
              : null}
          </div>
        </div>
      )}
      {period === '2020-2021' && region === 'regional' && (
        <div className="sl-contest-regional-details__container">
          <div className="sl-contest-regional-details">
            <ContestRegionalDetails />
          </div>
        </div>
      )}
      <div className="sl-contest-contests__container">
        {region !== 'regional' && (
          <>
            <h3>Wave of the Winter: Regional Edition</h3>
            <p>
              {`Welcome to a new Wave of the Winter for the people, created to celebrate
                unsung local heroes who charge at beaches around the country, all winter long.
                Surfline’s inaugural Regional O’Neill Wave of the Winter contests will be judged
                via online user voting after the four-month holding period. (December 1, 2019
                to March 31, 2020.) `}
            </p>
          </>
        )}
        {allContests[period] ? (
          <div className="sl-contest-contests">
            {region === 'regional' && (
              <ContestCard
                contest={allContests[period]['regional-region']}
                large
                regional
                imageResizing={imageResizing}
              />
            )}
            {region !== 'north-shore' && region !== 'regional' ? (
              <ContestCard contest={allContests[period]['north-shore']} large />
            ) : null}
            {region !== 'regional' && (
              <div className="sl-contest-contests__list">
                {contest &&
                  allContests[period].regional.map((contestRegion) => (
                    <ContestCard contest={contestRegion} key={contest.name} />
                  ))}
              </div>
            )}
          </div>
        ) : null}
      </div>
      <div className="sl-contest__ad-unit">
        <GoogleDFP
          adConfig={loadAdConfig(getContestAdId(region, 'bottom', period, contestType), [
            ['Box', 'Banner'],
          ])}
          isHtl
          isTesting={config.htl.isTesting}
        />
      </div>
    </>
  );
};

ContestLandingWOTWContent.propTypes = {
  contestType: PropTypes.oneOf(['sos', 'wotw']),
  region: PropTypes.string.isRequired,
  period: PropTypes.string.isRequired,
  contest: PropTypes.string.isRequired,
  contestEntries: {
    loading: PropTypes.bool,
    entries: PropTypes.arrayOf(contestEntryPropType),
  }.isRequired,
  bgColorSecondary: PropTypes.string,
  doUpvoteContestEntry: PropTypes.func.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

ContestLandingWOTWContent.defaultProps = {
  contestType: 'wotw',
  bgColorSecondary: null,
};

export default ContestLandingWOTWContent;
