import ns from './contests/ns.jpg';
import ne from './contests/ne.jpg';
import nw from './contests/nw.jpg';
import pr from './contests/pr.jpg';
import se from './contests/se.jpg';
import sw from './contests/sw.jpg';

import regional from './contests/regional.png';

const allContests = {
  '2018-2019': {
    'north-shore': {
      name: 'North Shore',
      region: 'north-shore',
      period: '2018-2019',
      abbreviation: 'Hawaii',
      imageUrl: ns,
    },
    regional: [
      {
        name: 'US East Coast South',
        region: 'us-east-coast-south',
        period: '2018-2019',
        abbreviation: 'SE',
        imageUrl: se,
      },
      {
        name: 'US East Coast North',
        region: 'us-east-coast-north',
        period: '2018-2019',
        abbreviation: 'NE',
        imageUrl: ne,
      },
      {
        name: 'Puerto Rico and Caribbean islands',
        region: 'puerto-rico-and-caribbean-islands',
        period: '2018-2019',
        abbreviation: 'PR',
        imageUrl: pr,
      },
      {
        name: 'US West Coast South',
        region: 'us-west-coast-south',
        period: '2018-2019',
        abbreviation: 'SW',
        imageUrl: sw,
      },
      {
        name: 'US West Coast North',
        region: 'us-west-coast-north',
        period: '2018-2019',
        abbreviation: 'NW',
        imageUrl: nw,
      },
    ],
  },
  '2019-2020': {
    'north-shore': {
      name: 'North Shore',
      region: 'north-shore',
      period: '2019-2020',
      abbreviation: 'Hawaii',
      imageUrl: ns,
    },
    regional: [
      {
        name: 'US East Coast South',
        region: 'us-east-coast-south',
        period: '2019-2020',
        abbreviation: 'SE',
        imageUrl: se,
      },
      {
        name: 'US East Coast North',
        region: 'us-east-coast-north',
        period: '2019-2020',
        abbreviation: 'NE',
        imageUrl: ne,
      },
      {
        name: 'Puerto Rico and Caribbean islands',
        region: 'puerto-rico-and-caribbean-islands',
        period: '2019-2020',
        abbreviation: 'PR',
        imageUrl: pr,
      },
      {
        name: 'US West Coast South',
        region: 'us-west-coast-south',
        period: '2019-2020',
        abbreviation: 'SW',
        imageUrl: sw,
      },
      {
        name: 'US West Coast North',
        region: 'us-west-coast-north',
        period: '2019-2020',
        abbreviation: 'NW',
        imageUrl: nw,
      },
    ],
  },
  '2020-2021': {
    'north-shore': {
      name: 'North Shore',
      region: 'north-shore',
      period: '2020-2021',
      abbreviation: 'Hawaii',
      imageUrl: ns,
    },
    'regional-region': {
      name: 'Regional Edition',
      region: 'regional',
      period: '2020-2021',
      abbreviation: 'Regional',
      imageUrl: regional,
    },
    regional: [
      {
        name: 'US East Coast South',
        region: 'regional',
        period: '2020-2021',
        abbreviation: 'SE',
        imageUrl: se,
      },
      {
        name: 'US East Coast North',
        region: 'regional',
        period: '2020-2021',
        abbreviation: 'NE',
        imageUrl: ne,
      },
      {
        name: 'Puerto Rico and Caribbean islands',
        region: 'regional',
        period: '2020-2021',
        abbreviation: 'PR',
        imageUrl: pr,
      },
      {
        name: 'US West Coast South',
        region: 'regional',
        period: '2020-2021',
        abbreviation: 'SW',
        imageUrl: sw,
      },
      {
        name: 'US West Coast North',
        region: 'regional',
        period: '2020-2021',
        abbreviation: 'NW',
        imageUrl: nw,
      },
    ],
  },
};

export default allContests;
