// 2018-2019
import award1 from './awards/award1.jpg';
import award2 from './awards/award2.jpg';
import award3 from './awards/award3.jpg';
import ne from './awards/east-coast-north.jpg';
import nw from './awards/west-coast-north.jpg';
import pr from './awards/puerto-rico.jpg';
import se from './awards/east-coast-south.jpg';
import sw from './awards/west-coast-south.jpg';

// 2019-2020
import decadeAward from './awards/decadeAward.jpg';
import womensAward from './awards/womensAward.jpg';
import breakThrough from './awards/special-awards-break-through.jpg';
import overallPerformance from './awards/special-awards-overall.jpg';

const contestAwards = {
  '2018-2019': {
    'north-shore': [
      {
        title: 'Breakthrough Performance',
        description: `The Sambazon Breakthrough Performer Award -- a youth-focused division running
        in concert with the O'Neill Wave of the Winter and Clif Bar Overall Performance Award --
        celebrates the accomplishments of one surfer, 19 years old or younger, who rises above all
        other youths with sustained, standout performances along the Seven Mile Miracle from November
        through February. The winner will receive $5,000.`,
        imageUrl: award1,
      },
      {
        title: 'Overall Performance Award',
        description: `The Clif Bar Overall Performance Award is given to the surfer who consistently
        demonstrates courage, skill and style while riding the most challenging waves on the North
        Shore during the Wave of the Winter period, while also demonstrating a positive commitment to
        both the sport and local community. The winner will receive $5,000, a year's supply of CLIF
        products, plus another $10,000 donation made in his or her name to be given to an
        environmental non-profit of their choice.`,
        imageUrl: award2,
      },
      {
        title: 'Active Skin Repair Heaviest Drop Award',
        description: `The Heaviest Drop Award celebrates that split-second confidence of hucking
        oneself over the ledge on a critical wave -- and sticking it. ("Sticking it" means making
        it to the bottom of the wave, standing on the board and in control, with the board and
        body away from the lip at the moment of impact.) Winner will be judged based on Wave of
        the Winter entries for the 2018/19 season and will receive $5,000.`,
        imageUrl: award3,
      },
    ],
    'puerto-rico-and-caribbean-islands': [
      {
        title: 'Puerto Rico and Caribbean Islands',
        description: `This winter, as solid North Atlantic swells bear down on the myriad reefs,
        points and beaches of Puerto Rico and the Caribbean, local surfers will -- as always --
        be hucking themselves over the ledge. Some will eat it. Some will get the wave of their
        lives. And this year, it's for more than just hoots from the shoulder or adulations from
        friends. This year, there's a thousand bucks -- and Internet fame -- up for grabs.`,
        imageUrl: pr,
      },
    ],
    'us-east-coast-north': [
      {
        title: 'US East Coast North',
        description: `This winter, as solid North Atlantic swells bear down on the myriad reefs,
        points and beaches of the Northeast, local surfers will -- as always -- be hucking
        themselves over the ledge. Some will eat it. Some will get the wave of their lives.
        And this year, it's for more than just hoots from the shoulder or adulations from friends.
        This year, there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries: Maine
        to Maryland.)`,
        imageUrl: ne,
      },
    ],
    'us-east-coast-south': [
      {
        title: 'US East Coast South',
        description: `This winter, as solid North Atlantic swells bear down on the myriad punchy
        beachbreaks of the southeastern US, local surfers will -- as always -- be hucking themselves
        over the ledge. Some will eat it. Some will get the wave of their lives. And this year,
        it's for more than just hoots from the shoulder or adulations from friends. This year,
        there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries: Virginia to
        Florida.)`,
        imageUrl: se,
      },
    ],
    'us-west-coast-north': [
      {
        title: 'US West Coast North',
        description: `This winter, as solid North Pacific swells bear down on the myriad reefs,
        points and beaches of Central and Northern California, local surfers will -- as always --
        be hucking themselves over the ledge. Some will eat it. Some will get the wave of their
        lives. And this year, it's for more than just hoots from the shoulder or adulations from
        friends. This year, there's a thousand bucks -- and Internet fame -- up for grabs.
        (Boundaries: Oregon border to Point Conception.)`,
        imageUrl: nw,
      },
    ],
    'us-west-coast-south': [
      {
        title: 'US West Coast South',
        description: `This winter, as solid North Pacific swells bear down on the myriad reefs,
        points and beaches of Southern California, local surfers will -- as always -- be hucking
        themselves over the ledge. Some will eat it. Some will get the wave of their lives.
        And this year, it's for more than just hoots from the shoulder or adulations from friends.
        This year, there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries:
        Point Conception to Mexico border.)`,
        imageUrl: sw,
      },
    ],
  },
  '2019-2020': {
    'north-shore': [
      {
        title: '4ocean Wave of the Decade powered by Surfline',
        description: `Now in its 10th year, Wave of the Winter showcases, celebrates and rewards the very best 
        rides that happen on Oahu's North Shore each winter season. With past winners ranging from Kelly Slater 
        to Jamie O'Brien to local and visiting underground Pipe specialists, Wave of the Winter has become one of 
        the surf world’s most prestigious awards. This year, we'll crown our 10th winner -- and then take that winning 
        wave and judge it against the past nine winners to determine the 4ocean Wave of the Decade. The winner will 
        receive $10,000.`,
        imageUrl: decadeAward,
      },
      {
        title: `O'Neill Women's Overall Performance Award`,
        description: `The O'Neill Women's Overall Performance Award is given to the female surfer who consistently demonstrates 
        courage, skill and style while riding the most challenging waves on the North Shore during the Wave of the Winter waiting 
        period. The winner will be chosen by our elite judging panel based on the entire winter season and will receive $5,000.`,
        imageUrl: womensAward,
      },
      {
        title: 'Surfline Breakthrough Performance',
        description: `The Surfline Breakthrough Performer Award -- a youth-focused division running in concert with the 
        O'Neill Wave of the Winter -- celebrates the accomplishments of one surfer, 19 years old or younger, who rises 
        above all other youths with sustained, standout performances along the Seven Mile Miracle from November through 
        February. The winner will receive $1,000.`,
        imageUrl: breakThrough,
      },
      {
        title: 'Surfline Overall Performance Award',
        description: `The Surfline Overall Performance Award is given to the surfer who consistently demonstrates courage, 
        skill and style while riding the most challenging waves on the North Shore during the Wave of the Winter period, 
        while also demonstrating a positive commitment to both the sport and local community. The winner will receive $1,000.`,
        imageUrl: overallPerformance,
      },
    ],
    'puerto-rico-and-caribbean-islands': [
      {
        title: 'Puerto Rico and Caribbean Islands',
        description: `This winter, as solid North Atlantic swells bear down on the myriad reefs,
        points and beaches of Puerto Rico and the Caribbean, local surfers will -- as always --
        be hucking themselves over the ledge. Some will eat it. Some will get the wave of their
        lives. And this year, it's for more than just hoots from the shoulder or adulations from
        friends. This year, there's a thousand bucks -- and Internet fame -- up for grabs.`,
        imageUrl: pr,
      },
    ],
    'us-east-coast-north': [
      {
        title: 'US East Coast North',
        description: `This winter, as solid North Atlantic swells bear down on the myriad reefs,
        points and beaches of the Northeast, local surfers will -- as always -- be hucking
        themselves over the ledge. Some will eat it. Some will get the wave of their lives.
        And this year, it's for more than just hoots from the shoulder or adulations from friends.
        This year, there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries: Maine
        to Maryland.)`,
        imageUrl: ne,
      },
    ],
    'us-east-coast-south': [
      {
        title: 'US East Coast South',
        description: `This winter, as solid North Atlantic swells bear down on the myriad punchy
        beachbreaks of the southeastern US, local surfers will -- as always -- be hucking themselves
        over the ledge. Some will eat it. Some will get the wave of their lives. And this year,
        it's for more than just hoots from the shoulder or adulations from friends. This year,
        there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries: Virginia to
        Florida.)`,
        imageUrl: se,
      },
    ],
    'us-west-coast-north': [
      {
        title: 'US West Coast North',
        description: `This winter, as solid North Pacific swells bear down on the myriad reefs,
        points and beaches of Central and Northern California, local surfers will -- as always --
        be hucking themselves over the ledge. Some will eat it. Some will get the wave of their
        lives. And this year, it's for more than just hoots from the shoulder or adulations from
        friends. This year, there's a thousand bucks -- and Internet fame -- up for grabs.
        (Boundaries: Oregon border to Point Conception.)`,
        imageUrl: nw,
      },
    ],
    'us-west-coast-south': [
      {
        title: 'US West Coast South',
        description: `This winter, as solid North Pacific swells bear down on the myriad reefs,
        points and beaches of Southern California, local surfers will -- as always -- be hucking
        themselves over the ledge. Some will eat it. Some will get the wave of their lives.
        And this year, it's for more than just hoots from the shoulder or adulations from friends.
        This year, there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries:
        Point Conception to Mexico border.)`,
        imageUrl: sw,
      },
    ],
  },
  '2020-2021': {
    'north-shore': [
      {
        title: 'Surfline Breakthrough Performance',
        description: `The Surfline Breakthrough Performer Award -- a youth-focused division running in concert with the 
        O'Neill Wave of the Winter -- celebrates the accomplishments of one surfer, 19 years old or younger, who rises 
        above all other youths with sustained, standout performances along the Seven Mile Miracle from November through 
        February. The winner will receive $1,000.`,
        imageUrl: breakThrough,
      },
      {
        title: 'Surfline Overall Performance Award',
        description: `The Surfline Overall Performance Award is given to the surfer who consistently demonstrates courage, 
        skill and style while riding the most challenging waves on the North Shore during the Wave of the Winter period, 
        while also demonstrating a positive commitment to both the sport and local community. The winner will receive $1,000.`,
        imageUrl: overallPerformance,
      },
    ],
    'puerto-rico-and-caribbean-islands': [
      {
        title: 'Puerto Rico and Caribbean Islands',
        description: `This winter, as solid North Atlantic swells bear down on the myriad reefs,
        points and beaches of Puerto Rico and the Caribbean, local surfers will -- as always --
        be hucking themselves over the ledge. Some will eat it. Some will get the wave of their
        lives. And this year, it's for more than just hoots from the shoulder or adulations from
        friends. This year, there's a thousand bucks -- and Internet fame -- up for grabs.`,
        imageUrl: pr,
      },
    ],
    'us-east-coast-north': [
      {
        title: 'US East Coast North',
        description: `This winter, as solid North Atlantic swells bear down on the myriad reefs,
        points and beaches of the Northeast, local surfers will -- as always -- be hucking
        themselves over the ledge. Some will eat it. Some will get the wave of their lives.
        And this year, it's for more than just hoots from the shoulder or adulations from friends.
        This year, there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries: Maine
        to Maryland.)`,
        imageUrl: ne,
      },
    ],
    'us-east-coast-south': [
      {
        title: 'US East Coast South',
        description: `This winter, as solid North Atlantic swells bear down on the myriad punchy
        beachbreaks of the southeastern US, local surfers will -- as always -- be hucking themselves
        over the ledge. Some will eat it. Some will get the wave of their lives. And this year,
        it's for more than just hoots from the shoulder or adulations from friends. This year,
        there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries: Virginia to
        Florida.)`,
        imageUrl: se,
      },
    ],
    'us-west-coast-north': [
      {
        title: 'US West Coast North',
        description: `This winter, as solid North Pacific swells bear down on the myriad reefs,
        points and beaches of Central and Northern California, local surfers will -- as always --
        be hucking themselves over the ledge. Some will eat it. Some will get the wave of their
        lives. And this year, it's for more than just hoots from the shoulder or adulations from
        friends. This year, there's a thousand bucks -- and Internet fame -- up for grabs.
        (Boundaries: Oregon border to Point Conception.)`,
        imageUrl: nw,
      },
    ],
    'us-west-coast-south': [
      {
        title: 'US West Coast South',
        description: `This winter, as solid North Pacific swells bear down on the myriad reefs,
        points and beaches of Southern California, local surfers will -- as always -- be hucking
        themselves over the ledge. Some will eat it. Some will get the wave of their lives.
        And this year, it's for more than just hoots from the shoulder or adulations from friends.
        This year, there's a thousand bucks -- and Internet fame -- up for grabs. (Boundaries:
        Point Conception to Mexico border.)`,
        imageUrl: sw,
      },
    ],
  },
};

export default contestAwards;
