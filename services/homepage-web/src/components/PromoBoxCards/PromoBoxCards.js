import PropTypes from 'prop-types';
import React from 'react';
import FeedArticle from '../FeedArticle';
import articlePropType from '../../propTypes/article';

const PromoCards = ({ promoBoxCards, feedLocale, imageResizing }) => (
  <div className="sl-promobox-cards">
    {promoBoxCards.map((article, index) => (
      <FeedArticle
        article={article}
        key={article.id}
        type="promo"
        promoPosition={index + 7}
        feedLocale={feedLocale}
        imageResizing={imageResizing}
      />
    ))}
  </div>
);

PromoCards.propTypes = {
  promoBoxCards: PropTypes.arrayOf(articlePropType).isRequired,
  feedLocale: PropTypes.string,
  imageResizing: PropTypes.bool.isRequired,
};

PromoCards.defaultProps = {
  feedLocale: null,
};

export default PromoCards;
