import React from 'react';
import PropTypes from 'prop-types';

const Shaka = ({ size, fill }) => (
  <svg
    width={size}
    height={size}
    viewBox={`0 0 ${size + 1} ${size + 1}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M14.2574 19.0621L15.4101 20.2501L21.75 16.0921L20.5973 14.3101L13.681 18.4681L14.2574 19.0621Z"
      fill={fill}
    />
    <path
      d="M14.2277 17.1013C13.0835 17.7934 11.6194 17.6446 10.6378 16.7365L3.13785 9.79853C2.67711 9.37231 2.71834 8.63195 3.22355 8.25953L3.97897 7.70266C4.35119 7.42827 4.86299 7.44488 5.21664 7.74282L8.75 10.7196L9.34091 11.3004L9.84561 9.96579L14.8099 7.22355C15.35 6.92518 16.0288 7.18241 16.2356 7.76381L17.0227 9.9772L18.9965 4.77722C19.2056 4.22621 19.8435 3.97466 20.3724 4.23461L21.75 4.9116L20.3268 11.9058C20.104 13.0007 19.4334 13.9526 18.4773 14.5309L14.2277 17.1013Z"
      fill={fill}
    />
  </svg>
);

Shaka.propTypes = {
  size: PropTypes.number,
  fill: PropTypes.string,
};

Shaka.defaultProps = {
  size: 22,
  fill: '#FFFFFF',
};

export default Shaka;
