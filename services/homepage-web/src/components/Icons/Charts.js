import React from 'react';

const Charts = () => (
  <svg
    width="32px"
    height="32px"
    viewBox="0 0 32 32"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g id="Current-Designs" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Artboard" strokeWidth="1.44">
        <g id="Charts">
          <rect id="Rectangle-30" stroke="#FFFFFF" x="0.72" y="16.72" width="4.56" height="14.56" />
          <rect
            id="Rectangle-30-Copy"
            stroke="#22D737"
            x="13.72"
            y="0.72"
            width="4.56"
            height="30.56"
          />
          <rect
            id="Rectangle-30-Copy-2"
            stroke="#FFFFFF"
            x="26.72"
            y="10.72"
            width="4.56"
            height="20.56"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Charts;
