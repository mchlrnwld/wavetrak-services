import React from 'react';

const PremiumTick = () => (
  <svg
    width="19px"
    height="18px"
    viewBox="0 0 19 18"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-927.000000, -10766.000000)">
        <g transform="translate(425.000000, 10570.000000)">
          <g transform="translate(472.400000, 24.000000)">
            <g transform="translate(30.400000, 172.000000)">
              <circle fill="#22D736" cx="9.09473684" cy="9.09473684" r="9.09473684" />
              <polygon
                fill="#FEFEFE"
                points="12.9472101 6.06345265 8.08412319 10.9265396 5.99989629 8.84231268 5.30526316 9.53727332 7.38949006 11.6211727 8.08412319 12.3158058 8.77875633 11.6211727 13.6418433 6.75808578"
              />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default PremiumTick;
