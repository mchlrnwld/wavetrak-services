import React from 'react';

const Cams = () => (
  <svg
    width="32px"
    height="32px"
    viewBox="0 0 32 32"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g id="Current-Designs" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Artboard-Copy" strokeWidth="1.44">
        <g id="Ad-Free" transform="translate(0.000000, 0.000000)">
          <g id="Group-18-Copy">
            <path
              d="M23.9809524,10.9113344 L23.9809524,17.6384983 L20.6518519,15.8294314 L20.6518519,17.8625428 C20.6518519,18.3494946 20.2762183,18.7441472 19.8127336,18.7441472 L9.83911828,18.7441472 C9.37563351,18.7441472 9,18.3494946 9,17.8625428 L9,10.8816043 C9,10.3946525 9.37563351,10 9.83911828,10 L19.8127336,10 C20.2762183,10 20.6518519,10.3946525 20.6518519,10.8816043 L20.6518519,12.7204013 L23.9809524,10.9113344 Z"
              id="Fill-1-Copy-2"
              stroke="#22D737"
            />
            <path
              d="M0.72,0.72 L0.72,30.9451005 L16,24.9659701 L31.28,30.9451005 L31.28,0.72 L0.72,0.72 Z"
              id="Rectangle-25-Copy-2"
              stroke="#FFFFFF"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default Cams;
