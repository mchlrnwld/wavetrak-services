import React from 'react';

const Buoy = () => (
  <svg
    width="32px"
    height="32px"
    viewBox="0 0 32 32"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g id="Current-Designs" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Artboard-Copy-9" strokeWidth="1.44">
        <g id="Buoys" transform="translate(1.000000, -4.000000)">
          <g
            id="Group-28"
            transform="translate(13.768887, 17.689673) rotate(-13.000000) translate(-13.768887, -17.689673) translate(3.268887, 2.189673)"
          >
            <g id="Group-22" transform="translate(8.209460, -0.000000)" stroke="#22D737">
              <polyline
                id="Path-12"
                strokeLinecap="round"
                points="2.72201805 8.18599884 8.80067888 5.55140122 2.72201805 3.3559032"
              />
              <path d="M2.02387524,11.2375436 L2.02387524,1.3606955" id="Path-11" />
            </g>
            <path
              d="M1.91365297,24.7843642 C2.53552354,25.9946473 3.3835368,27.0632807 4.40412434,27.9226164 C6.88903731,27.8458575 9.034471,28.1904729 11.3159082,29.1453735 C11.8230796,29.3576512 12.3312141,29.5818104 12.8391461,29.8170498 C15.4387159,29.1048093 17.6391437,27.2646026 18.913059,24.7843642 L1.91365297,24.7843642 Z"
              id="Combined-Shape"
              stroke="#FFFFFF"
            />
            <path
              d="M5.60698534,15.8033591 L5.60698534,15.3716645 C5.60698534,13.1625255 7.39784634,11.3716645 9.60698534,11.3716645 L11.2257351,11.3716645 C13.4348741,11.3716645 15.2257351,13.1625255 15.2257351,15.3716645 C15.2257351,15.3941238 15.225546,15.4165826 15.2251677,15.4390386 L15.2190304,15.8033591 L5.60698534,15.8033591 Z"
              id="Path-14"
              stroke="#FFFFFF"
              strokeLinecap="round"
            />
            <path
              d="M7.31698495,15.8994744 L4.22345555,24.6136341 L16.6031281,24.6136341 L13.5095987,15.8994744 L7.31698495,15.8994744 Z"
              id="Rectangle-36"
              stroke="#FFFFFF"
            />
          </g>
          <path
            d="M0,34.528097 C7,33.5 10.7499442,29.8447542 17.5,31 C22.346819,31.8295142 27,33.5 30,35"
            id="Path-10-Copy-9"
            stroke="#FFFFFF"
            strokeLinecap="square"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Buoy;
