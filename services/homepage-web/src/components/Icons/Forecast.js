import React from 'react';

const Forecast = () => (
  <svg
    width="32px"
    height="32px"
    viewBox="0 0 32 32"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g id="Current-Designs" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Artboard-Copy-5" strokeWidth="1.368">
        <g id="Long-range-forecast">
          <path
            d="M0.684,3.684 L0.684,31.0788473 L4,30.0771389 L8,31.2854723 L12,30.0771389 L16,31.2854723 L20,30.0771389 L23.9971679,31.2846167 L27.8829944,30.0771176 L31.316,31.0860503 L31.316,3.684 L0.684,3.684 Z"
            id="Rectangle-7-Copy-4"
            stroke="#FFFFFF"
          />
          <path
            d="M8,26 C8,18.2399631 17.5454545,10.4799261 23,18.167666 C17.5454545,18.167666 17.5454545,24.0533764 23,25.3467159"
            id="Path-3"
            stroke="#22D737"
          />
          <path d="M32,11.5 L0,11.5" id="Rectangle-6" stroke="#FFFFFF" />
          <rect
            id="Rectangle-26"
            stroke="#FFFFFF"
            fill="#011D38"
            x="8.684"
            y="0.684"
            width="1"
            height="4.632"
          />
          <rect
            id="Rectangle-26-Copy"
            stroke="#FFFFFF"
            fill="#011D38"
            x="23.684"
            y="0.684"
            width="1"
            height="4.632"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Forecast;
