import React from 'react';

const Rewind = () => (
  <svg
    width="32px"
    height="32px"
    viewBox="0 0 32 32"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g id="Current-Designs" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Artboard-Copy-3">
        <g id="Cam-Rewind" transform="translate(1.000000, 0.000000)">
          <path
            d="M23,11.938 L23,18.862 L19.6666667,17 L19.6666667,19.0926 C19.6666667,19.5938 19.2905556,20 18.8264815,20 L8.84018519,20 C8.37611111,20 8,19.5938 8,19.0926 L8,11.9074 C8,11.4062 8.37611111,11 8.84018519,11 L18.8264815,11 C19.2905556,11 19.6666667,11.4062 19.6666667,11.9074 L19.6666667,13.8 L23,11.938 Z"
            id="Fill-1-Copy-2"
            stroke="#22D737"
            strokeWidth="1.44"
          />
          <path
            d="M0,15.5 C0,24.0604136 6.71572875,31 15,31 C23.2842712,31 30,24.0604136 30,15.5 C30,6.93958638 23.2842712,0 15,0 C13.0642763,0 11.2141922,0.378888738 9.51541306,1.06881189 C8.6205223,1.42219066 7.8525641,1.930838 7.21153846,2.59475391"
            id="Oval-6"
            stroke="#FFFFFF"
            strokeWidth="1.44"
          />
          <path
            d="M6.5,1.19078091 L4.0446222,5.4 L8.9553778,5.4 L6.5,1.19078091 Z"
            id="Rectangle-2"
            stroke="#FFFFFF"
            strokeWidth="1.2"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Rewind;
