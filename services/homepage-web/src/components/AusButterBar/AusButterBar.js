import { ButterBar, SurflineCWLogo, useTreatment } from '@surfline/quiver-react';
import { useCookies } from 'react-cookie';
import React, { useState } from 'react';
import { SL_WEB_AUS_LAUNCH_MODAL } from '../../common/treatments';

const AusButterBar = () => {
  const { treatment: variant } = useTreatment(SL_WEB_AUS_LAUNCH_MODAL);

  const [cookie, setCookie] = useCookies(['sl-aus-launch']);
  const [closed, setClosed] = useState(false);

  const isActive = variant === 'Variant_5';

  return isActive ? (
    <ButterBar
      canClose
      isClosed={closed || cookie['sl-aus-launch']}
      onRequestClose={() => {
        setCookie('sl-aus-launch', true, { maxAge: 31536000, path: '/' });
        setClosed(true);
      }}
      title={<SurflineCWLogo />}
      type="aus"
    >
      <p className="quiver-butterbar__body__tagline">Here from Coastalwatch? Welcome!</p>
      <p>
        This is the new home of Surfline with Coastalwatch: the best network of free high definition
        surf-cams in Australia. With long range, in-depth surf forecasts, and live reports to help
        you score more waves.
      </p>
    </ButterBar>
  ) : null;
};

export default AusButterBar;
