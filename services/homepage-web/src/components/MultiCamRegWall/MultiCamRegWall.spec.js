import { expect } from 'chai';
import sinon from 'sinon';

import { Button } from '@surfline/quiver-react';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import MultiCamRegWall from './MultiCamRegWall';
import * as location from '../../utils/assignLocation';

const wait = (milliseconds) => new Promise((resolve) => setTimeout(resolve, milliseconds));

describe('components / MultiCamRegWall', () => {
  const defaultProps = {};

  beforeEach(() => {
    sinon.stub(location, 'assignLocation');
  });

  afterEach(() => {
    location.assignLocation.restore();
  });

  it('should render the multi cam regwall', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCamRegWall, defaultProps);
    expect(wrapper.find(MultiCamRegWall)).to.have.length(1);
  });

  it('should render the multi cam regwall link', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCamRegWall, defaultProps);
    const link = wrapper.find('a');
    expect(link).to.have.length(1);
    expect(link.prop('href')).to.be.contain('/sign-in?redirectUrl=');
  });

  it('should render the multi cam regwall link', async () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCamRegWall, defaultProps);
    const buttonWrapper = wrapper.find(Button);

    expect(buttonWrapper).to.have.length(1);

    buttonWrapper.simulate('click');
    await wait(301);
    expect(location.assignLocation).to.have.been.calledOnceWithExactly('/create-account');
  });
});
