import React from 'react';
import { MultiCamIcon, Button } from '@surfline/quiver-react';

import { canUseDOM, trackClickedCreateAccountCTA } from '@surfline/web-common';
import en from '../../intl/translations/en';
import { assignLocation } from '../../utils/assignLocation';

const MultiCamRegWall = () => {
  const copy = en.MultiCam.regwall;
  const redirectUrl = canUseDOM ? encodeURIComponent(window.location) : '';
  return (
    <div className="sl-multi-cam-regwall">
      <div className="sl-multi-cam-regwall__outer-circle">
        <div className="sl-multi-cam-regwall__inner-circle">
          <MultiCamIcon />
        </div>
      </div>
      <h1 className="sl-multi-cam-regwall__header">{copy.header}</h1>
      <Button
        onClick={() => {
          trackClickedCreateAccountCTA({
            category: 'cams & reports',
            pageName: 'multi-cam',
          });
          setTimeout(() => assignLocation('/create-account'), 300);
        }}
      >
        {copy.createAccount}
      </Button>
      <span className="sl-multi-cam-regwall__disclaimer">{copy.disclaimer}</span>
      <span className="sl-multi-cam-regwall__link-container">
        {copy.preLinkCopy}{' '}
        <a className="sl-multi-cam-regwall__link" href={`/sign-in?redirectUrl=${redirectUrl}`}>
          {copy.link}
        </a>
      </span>
    </div>
  );
};

MultiCamRegWall.propTypes = {};

export default MultiCamRegWall;
