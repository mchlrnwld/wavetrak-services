import React, { useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { SurfConditions, SurfHeight, TideArrow, TrackableLink } from '@surfline/quiver-react';
import { getUserSettings } from '@surfline/web-common';
import classNames from 'classnames';

import spotPropTypes from '../../propTypes/spot';
import spotReportPath from '../../utils/urls/spotReportPath';
import cameraPropTypes from '../../propTypes/camera';
import { getCamTitle } from '../../utils/getCamTitle';

const getConditionsOverlayClass = (isVisible) =>
  classNames({
    'sl-cam-overlay__conditions': true,
    'sl-cam-overlay__conditions--visible': isVisible,
  });

const ConditionsOverlay = ({ spot, forecast, camAngle, visible }) => {
  const { units } = useSelector(getUserSettings);
  const spotLink = `/${spotReportPath(spot._id, spot.name)}`;

  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      linkUrl: spotLink,
      linkName: 'spot page cam overlay',
      linkLocation: 'multi-cam cam overlay',
      category: 'cams & reports',
    }),
    [spotLink]
  );

  return (
    <div className={getConditionsOverlayClass(visible)}>
      <div className="sl-cam-overlay__conditions__spot-name">
        <SurfConditions
          conditions={forecast?.conditions?.value || 'none'}
          expired={forecast.conditions.expired && forecast.conditions.human}
        />
        <TrackableLink eventName="Clicked Link" eventProperties={eventProperties} ref={linkRef}>
          <a ref={linkRef} href={spotLink}>
            {getCamTitle(camAngle.title) || spot.name}
          </a>
        </TrackableLink>
      </div>
      <div className="sl-cam-overlay__conditions-containers">
        <span className="sl-cam-overlay__conditions__surf">
          <SurfHeight
            min={forecast.waveHeight.min}
            max={forecast.waveHeight.max}
            units={units.surfHeight}
            plus={forecast.waveHeight.plus}
          />
        </span>
        {forecast.tide ? (
          <span className="sl-cam-overlay__conditions__tide">
            {forecast.tide.current.height}
            {units.tideHeight}
            <TideArrow direction={forecast.tide.next.type === 'LOW' ? 'DOWN' : 'UP'} />
          </span>
        ) : null}
        <span className="sl-cam-overlay__conditions__wind">
          {forecast.wind.speed}
          {units.windSpeed}
        </span>
      </div>
    </div>
  );
};

ConditionsOverlay.propTypes = {
  spot: spotPropTypes.isRequired,
  camAngle: cameraPropTypes,
  forecast: PropTypes.shape({
    conditions: PropTypes.shape({
      human: PropTypes.bool,
      value: PropTypes.string,
      expired: PropTypes.bool,
    }),
    waveHeight: PropTypes.shape({
      min: PropTypes.number,
      max: PropTypes.number,
      plus: PropTypes.bool,
    }),
    tide: PropTypes.shape({
      previous: PropTypes.shape({
        height: PropTypes.number,
        type: PropTypes.string,
      }),
      current: PropTypes.shape({
        height: PropTypes.number,
        type: PropTypes.string,
      }),
      next: PropTypes.shape({
        height: PropTypes.number,
        type: PropTypes.string,
      }),
    }),
    wind: PropTypes.shape({
      speed: PropTypes.number,
    }),
  }).isRequired,
  visible: PropTypes.bool,
};

ConditionsOverlay.defaultProps = {
  camAngle: {},
  visible: true,
};

export default ConditionsOverlay;
