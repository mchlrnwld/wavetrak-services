/** @prettier */

import { expect } from 'chai';
import { SurfConditions, SurfHeight, TideArrow } from '@surfline/quiver-react';

import ConditionsOverlay from './ConditionsOverlay';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import spotFixture from '../../utils/fixtures/spot';
import stateFixture from '../../selectors/fixtures/state.json';

describe('components / ConditionsOverlay', () => {
  const defaultProps = {
    spot: spotFixture,
    forecast: {
      tide: spotFixture.tide,
      waveHeight: spotFixture.waveHeight,
      wind: spotFixture.wind,
      conditions: spotFixture.conditions,
    },
  };

  const initialState = {
    backplane: { user: { ...stateFixture.user } },
  };

  beforeEach(() => {});
  afterEach(() => {});

  it('should render the spot conditions', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(ConditionsOverlay, props, { initialState });

    expect(wrapper.find(ConditionsOverlay)).to.have.length(1);
  });

  it('should render the spot name as a link', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(ConditionsOverlay, props, { initialState });

    const linkWrapper = wrapper.find('.sl-cam-overlay__conditions__spot-name a');
    expect(linkWrapper).to.have.length(1);
    expect(linkWrapper.text()).to.be.equal(props.spot.name);
    expect(linkWrapper.prop('href')).to.be.contain(props.spot._id);
  });

  it('should render the conditions', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(ConditionsOverlay, props, { initialState });

    const conditionsWrapper = wrapper.find(SurfConditions);
    expect(conditionsWrapper).to.have.length(1);
    expect(conditionsWrapper.prop('conditions')).to.have.equal(props.spot.conditions.value);
  });

  it('should render the surf height', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(ConditionsOverlay, props, { initialState });

    const surfHeightWrapper = wrapper.find(SurfHeight);
    expect(surfHeightWrapper).to.have.length(1);
    expect(surfHeightWrapper.prop('min')).to.be.equal(props.spot.waveHeight.min);
    expect(surfHeightWrapper.prop('max')).to.be.equal(props.spot.waveHeight.max);
  });

  it('should render the tide', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(ConditionsOverlay, props, { initialState });

    const tideWrapper = wrapper.find(TideArrow);
    expect(tideWrapper).to.have.length(1);
    const tideHeightWrapper = wrapper.find('.sl-cam-overlay__conditions__tide');
    expect(tideHeightWrapper.text()).to.contain(props.spot.tide.current.height);
  });

  it('should render the wind', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(ConditionsOverlay, props, { initialState });

    const windWrapper = wrapper.find('.sl-cam-overlay__conditions__wind');
    expect(windWrapper).to.have.length(1);
    expect(windWrapper.text()).to.contain(props.spot.wind.speed);
  });
});
