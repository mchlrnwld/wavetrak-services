import React, { useState } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import useSWR from 'swr';
import { AsyncDataContainer, ColoredConditionBar } from '@surfline/quiver-react';

import MultiCamSpotReportModule from '../MultiCamSpotReportModule';
import MultiCamSurfReportHeader from '../MultiCamSurfReportHeader';
import MultiCamGraphs from '../MultiCamGraphs';

import { getMultiCam } from '../../selectors/multiCam';
import { fetchReport } from '../../common/api/spot';

import './MultiCamSurfReport.scss';

const MultiCamSurfReport = React.memo(() => {
  /** State */
  const [spotReportLoadingSlow, setSpotReportLoadingSlow] = useState(false);
  const { selectedSpots, primarySpotIndex } = useSelector(getMultiCam, shallowEqual);
  const primarySpot = selectedSpots[primarySpotIndex];

  const {
    data: spotReport,
    isValidating: spotReportLoading,
    error: spotReportError,
  } = useSWR(primarySpot?._id, fetchReport, {
    onLoadingSlow: () => setSpotReportLoadingSlow(true),
  });

  if (!primarySpot) {
    return null;
  }

  const subregionUrl = spotReport?.associated?.subregionUrl;
  const spotReportUrl = spotReport?.associated?.href;
  const readings = spotReport?.forecast;
  const conditions = readings?.conditions;

  return (
    <section className="sl-multi-cam-surf-report">
      <MultiCamSurfReportHeader subregionUrl={subregionUrl} primarySpot={primarySpot} />
      {conditions?.value && (
        <ColoredConditionBar value={conditions?.value} expired={conditions?.expired} />
      )}
      <AsyncDataContainer
        width="100%"
        height="300px"
        error={spotReportError}
        loading={spotReportLoading || !spotReport}
        loadingSlow={spotReportLoadingSlow}
        loadingText="Loading the spot report"
      >
        <MultiCamSpotReportModule spotReport={spotReport} />
      </AsyncDataContainer>
      <MultiCamGraphs spotReportUrl={spotReportUrl} primarySpot={primarySpot} />
    </section>
  );
});

export default MultiCamSurfReport;
