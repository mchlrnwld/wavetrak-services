import PropTypes from 'prop-types';
import React from 'react';

const Lock = ({ className }) => (
  <svg width="16px" height="18px" viewBox="0 0 11 13" version="1.1" className={className}>
    <g id="Symbols" stroke="none" strokeWidth="1" fillRule="evenodd">
      <g id="premium-analysis-paywall" transform="translate(-114.000000, -181.000000)">
        <g id="Group-4">
          <g id="Group-3" transform="translate(114.000000, 180.000000)">
            <path
              d={`M5.5,1 C3.47416667,1 1.83333333,2.6228022 1.83333333,
              4.62637363 L1.83333333,6.43956044 L0,6.43956044 L0,13.6923077 L11,
              13.6923077 L11,6.43956044 L9.16666667,6.43956044 L9.16666667,
              4.62637363 C9.16666667,2.6228022 7.52583333,1 5.5,1 L5.5,
              1 Z M5.5,2.81318681 C6.59083333,2.81318681 7.33333333,3.54752747 7.33333333,
              4.62637363 L7.33333333,6.43956044 L3.66666667,6.43956044 L3.66666667,
              4.62637363 C3.66666667,3.54752747 4.40916667,2.81318681 5.5,2.81318681 Z M4.58333333,
              8.25274725 L6.41666667,8.25274725 L6.41666667,11.8791209 L4.58333333,
              11.8791209 L4.58333333,8.25274725 Z`}
              id="lock"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

Lock.propTypes = {
  className: PropTypes.string,
};

Lock.defaultProps = {
  className: '',
};

export default Lock;
