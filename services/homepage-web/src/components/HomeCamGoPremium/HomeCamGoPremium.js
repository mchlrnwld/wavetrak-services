import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import { Lock } from './Icons';
import { Close } from '../HomeCam/Icons';
import config from '../../config';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const eventProperties = {
  location: 'Home Cam',
};
const HomeCamGoPremium = ({ doToggleHomeCamVisibility }) => {
  const ref = useRef();
  return (
    <div className="sl-home-cam-go-premium">
      <Lock className="sl-home-cam-go-premium-lock" />
      <p>Set your homecam and never miss a set again.</p>
      <TrackableLink ref={ref} eventName="Clicked Subscribe CTA" eventProperties={eventProperties}>
        <a ref={ref} href={config.funnelUrl}>
          Start Free Trial
        </a>
      </TrackableLink>
      <div
        {...createAccessibleOnClick(doToggleHomeCamVisibility)}
        className="sl-home-cam-go-premium__close"
      >
        <Close className="sl-home-cam-go-premium__close-icon" />
      </div>
    </div>
  );
};

HomeCamGoPremium.propTypes = {
  doToggleHomeCamVisibility: PropTypes.func.isRequired,
};

export default HomeCamGoPremium;
