export const spinnerVariant = {
  initial: {
    y: 0,
    x: '-50%',
  },
  hidden: {
    x: -200,
    transition: { duration: 0.15 },
  },
};

export const categoryVariants = {
  hidden: {
    x: 320,
  },
  loaded: {
    x: 0,
    transition: {
      type: 'spring',
      damping: 30,
      stiffness: 350,
    },
  },
};

export const containerVariants = {
  hidden: {
    y: 0,
  },
  loaded: {
    y: 0,
    transition: {
      duration: 0.01,
      staggerChildren: 0.04,
    },
  },
};
