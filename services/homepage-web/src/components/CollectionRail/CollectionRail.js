/** @prettier */

import React, { useState, useCallback } from 'react';
import classnames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { AnimatePresence, motion } from 'framer-motion';
import { Favorite, Spinner, useDeviceSize, HoverMenu } from '@surfline/quiver-react';
import PropTypes from 'prop-types';

import { trackEvent } from '@surfline/web-common';
import conditionClassModifier from '../../utils/conditionClassModifier';
import userSettingsPropType from '../../propTypes/userSettings';
import { categoryVariants, containerVariants, spinnerVariant } from './variants';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';
import { changePrimarySpot, setActiveCollection, SUBREGIONS } from '../../actions/multiCam';
import { animationControlsPropType } from '../../propTypes/animation';
import { getNumCams, getCollections } from '../../selectors/multiCam';
import MoreFavorites from '../MoreFavorites/MoreFavorites';

const conditionClasses = (condition, isSelected, isPrimary) => {
  const classObject = {};
  const conditionClass = condition ? conditionClassModifier(condition) : 'none';
  classObject['sl-collection-rail__spot'] = true;
  classObject[`sl-collection-rail__spot--${conditionClass}`] = conditionClass;
  classObject['sl-collection-rail__spot--selected'] = !!isSelected;
  classObject['sl-collection-rail__spot--primary'] = !!isPrimary;
  return classnames(classObject);
};

const CollectionRail = React.memo(
  ({ activeCollection, settings, collectionControls, primarySpotId, selectedSpots }) => {
    const { width } = useDeviceSize();
    const [favoritesMenuOpened, setFavoritesMenuOpened] = useState(false);

    const dispatch = useDispatch();
    const numCams = useSelector(getNumCams);
    const collections = useSelector(getCollections);
    const collectionOptions = collections.map((item) => ({
      value: item,
      name: item.name,
      selected: item._id === activeCollection._id,
    }));

    const onClickMenuItem = useCallback(
      (collection) => {
        dispatch(setActiveCollection(collection));
        trackEvent('Clicked Multi-Cam Collections', {
          category: 'cams & reports',
          collectionType: collection.type,
          name: collection.name,
          view: collection.type === SUBREGIONS ? 'Favorite Subregion' : collection.name,
          subregionId: collection.type === SUBREGIONS ? collection._id : '',
          subregionName: collection.type === SUBREGIONS ? collection.name : '',
        });
      },
      [dispatch]
    );

    const renderSpinner = activeCollection?.loading;
    return (
      <div className="sl-collection-rail">
        <div className="sl-collection-rail-scroll-container">
          <AnimatePresence>
            {renderSpinner && (
              <motion.div
                key="multi-cam-spinner"
                className="sl-multi-cam-spinner"
                exit={spinnerVariant.hidden}
                initial={spinnerVariant.initial}
              >
                <Spinner />
              </motion.div>
            )}
          </AnimatePresence>
          <motion.div
            inherit={false}
            className="sl-collection-rail__items"
            animate={collectionControls}
            initial="hidden"
            variants={containerVariants}
          >
            <motion.h6 className="sl-collection-rail__title" variants={categoryVariants}>
              <HoverMenu
                disabled={width > 1201}
                label={activeCollection?.name}
                menuOptions={collectionOptions}
                onClickMenuItem={onClickMenuItem}
                useSelectOnMobile
                selectDescription="- SELECT A COLLECTION -"
              />
              <MoreFavorites opened={favoritesMenuOpened} onClickButton={setFavoritesMenuOpened} />
            </motion.h6>
            {activeCollection?.spots.map((spot) => {
              const matchSelectedSpot = (selectedSpot) => selectedSpot?._id === spot._id;
              const selectedIndex = selectedSpots.findIndex(matchSelectedSpot);

              const isSelected = selectedIndex !== -1 && selectedIndex < numCams;
              const isPrimary = spot._id === primarySpotId;

              const doChangePrimarySpot = () => dispatch(changePrimarySpot(spot));
              return (
                <motion.button
                  disabled={isPrimary}
                  key={spot.name}
                  className={conditionClasses(spot.conditions.value, isSelected, isPrimary)}
                  variants={categoryVariants}
                  {...createAccessibleOnClick(doChangePrimarySpot)}
                >
                  <Favorite
                    favorite={spot}
                    settings={settings}
                    disableLink
                    onClickLink={() => null}
                    withCamIcon
                  />
                </motion.button>
              );
            })}
          </motion.div>
        </div>
      </div>
    );
  }
);

CollectionRail.propTypes = {
  activeCollection: PropTypes.shape({
    _id: PropTypes.string,
    spots: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    name: PropTypes.string,
    loading: PropTypes.bool,
    fetchedSpots: PropTypes.bool,
  }),
  settings: userSettingsPropType.isRequired,
  primarySpotId: PropTypes.string,
  selectedSpots: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      cams: PropTypes.arrayOf(PropTypes.shape({})),
    })
  ).isRequired,
  collectionControls: animationControlsPropType.isRequired,
};

CollectionRail.defaultProps = {
  primarySpotId: '',
  activeCollection: {
    spots: [],
    name: '',
    loading: true,
    fetchedSpots: false,
  },
};

export default CollectionRail;
