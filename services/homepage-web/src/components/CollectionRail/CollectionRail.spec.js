/** @prettier */

import { expect } from 'chai';
import sinon from 'sinon';
import { Favorite } from '@surfline/quiver-react';

import CollectionRail from './CollectionRail';
import collectionsFixture from '../../utils/fixtures/collections';
import * as multiCam from '../../actions/multiCam';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('components / CollectionRail', () => {
  let swapPrimarySpotStub;
  let changePrimarySpotStub;
  const controls = {
    start: () => null,
    set: () => null,
    stop: () => null,
    subscribe: () => null,
  };

  const [activeCollection] = collectionsFixture;
  const defaultProps = {
    activeCollection,
    settings: { units: { surfHeight: 'FT', waveHeight: 'FT' } },
    spinnerControls: controls,
    collectionControls: controls,
    selectedSpots: activeCollection.spots.slice(0, 2),
    primarySpotId: activeCollection.spots[0]._id,
  };
  const initialState = {
    multiCam: {
      numCams: 2,
      collections: collectionsFixture,
    },
  };

  beforeEach(() => {
    swapPrimarySpotStub = sinon.spy(multiCam, 'swapPrimarySpot');
    changePrimarySpotStub = sinon.spy(multiCam, 'changePrimarySpot');
  });
  afterEach(() => {
    swapPrimarySpotStub.restore();
    changePrimarySpotStub.restore();
  });

  it('should render the collection list with all the spots in the active collection', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionRail, props, { initialState });

    expect(wrapper.find(Favorite)).to.have.length(activeCollection.spots.length);
    wrapper.unmount();
  });

  it('should render the collection rail with a title', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionRail, props, { initialState });

    expect(wrapper.find('h6.sl-collection-rail__title')).to.have.length(1);
    wrapper.unmount();
  });

  it('should change the primary spot if selecting a new spot', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionRail, props, { initialState });

    wrapper
      .find('button.sl-collection-rail__spot:not(.sl-collection-rail__spot--selected)')
      .at(0)
      .simulate('click');

    expect(changePrimarySpotStub).to.have.been.calledOnceWithExactly(activeCollection.spots[2]);
    wrapper.unmount();
  });
});
