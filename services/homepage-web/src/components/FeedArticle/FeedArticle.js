import PropTypes from 'prop-types';
import React from 'react';
import { FeedCard, FeatureCard } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import articlePropTypes from '../../propTypes/article';
import config from '../../config';

const getArticleImage = (media, imageResizing, feature = false) => {
  const mediaUrl = feature ? media?.promobox1x || media?.feed2x : media?.feed2x;
  const settings = feature ? 'w=784,q=85,f=auto' : 'w=300,q=75,f=auto';
  return imageResizing ? `${config.cloudflareImageResizingUrl(mediaUrl, settings)}` : mediaUrl;
};

const FeedArticle = ({ article, feature, type, promoPosition, feedLocale, imageResizing }) => (
  <div className="sl-feed-article">
    {feature ? (
      <FeatureCard
        isPremium={article.premium}
        small={type === 'feedSmall'}
        imageUrl={article.media ? getArticleImage(article.media, imageResizing, true) : null}
        newWindow={article.newWindow}
        permalink={article.permalink}
        title={article.content.title}
        displayTitle={article.content.displayTitle}
        feedSuperheader={article.feedSuperheader}
        onClickLink={() =>
          trackEvent('Clicked Home Feed Link', {
            destinationUrl: article.permalink,
            mediaType: article.media && article.media.type ? article.media.type : null,
            contentId: `${article.id}`,
            title: article.content.title,
            category: type === 'feedSmall' ? 'Feed Small Article' : 'Feed Large Article',
            contentType: 'Surf News',
            tags: article.tags,
            locationCategory: 'homepage curated feed',
            feedLocale,
          })
        }
      />
    ) : (
      <FeedCard
        isPremium={article.premium}
        category={article.tags[0]}
        imageUrl={article.media ? getArticleImage(article.media, imageResizing) : null}
        newWindow={article.newWindow}
        subtitle={article.content.subtitle}
        title={article.content.title}
        displayTitle={article.content.displayTitle}
        createdAt={article.createdAt}
        permalink={article.permalink}
        onClickLink={() =>
          trackEvent('Clicked Home Feed Link', {
            destinationUrl: article.permalink,
            mediaType: article.media && article.media.type ? article.media.type : null,
            contentId: `${article.id}`,
            title: article.content.title,
            contentType: 'Surf News',
            tags: article.tags,
            category: type === 'promo' ? 'Promo Article' : 'Curated Article',
            locationCategory: type === 'promo' ? 'homepage promobox' : 'homepage curated feed',
            promoPosition: promoPosition + 1 || null,
            feedLocale,
          })
        }
        onClickTag={(tag) =>
          trackEvent('Clicked Home Feed Link', {
            destinationUrl: tag.url,
            mediaType: tag.name,
            contentId: `${article.id}`,
            title: article.content.title,
            contentType: 'Surf News',
            tags: article.tags,
            category: type === 'promo' ? 'Promo Article' : 'Curated Article',
            locationCategory: type === 'promo' ? 'homepage promobox' : 'homepage curated feed',
            promoPosition: promoPosition + 1 || null,
            feedLocale,
          })
        }
      />
    )}
  </div>
);

FeedArticle.propTypes = {
  article: articlePropTypes.isRequired,
  feature: PropTypes.bool,
  promoPosition: PropTypes.number,
  feedLocale: PropTypes.string,
  type: PropTypes.oneOf(['curated', 'feedLarge', 'feedSmall', 'promo']).isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

FeedArticle.defaultProps = {
  feature: false,
  feedLocale: null,
  promoPosition: null,
};

export default FeedArticle;
