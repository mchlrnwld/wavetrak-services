import { expect } from 'chai';
import sinon from 'sinon';

import { Button } from '@surfline/quiver-react';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import MultiCamPaywall from './MultiCamPaywall';
import * as location from '../../utils/assignLocation';

const wait = (milliseconds) => new Promise((resolve) => setTimeout(resolve, milliseconds));

describe('components / MultiCamPaywall', () => {
  const defaultProps = {};

  beforeEach(() => {
    sinon.stub(location, 'assignLocation');
  });

  afterEach(() => {
    location.assignLocation.restore();
  });

  it('should render the multi cam paywall', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(MultiCamPaywall, props);
    expect(wrapper.find(MultiCamPaywall)).to.have.length(1);
  });

  it('should link to the premium benefits page', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(MultiCamPaywall, props);
    expect(wrapper.find('a')).to.have.length(1);
    expect(wrapper.find('a').prop('href')).to.be.equal('/premium-benefits');
  });

  it('should handle clicking the go premium button', async () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(MultiCamPaywall, props);
    const buttonWrapper = wrapper.find(Button);

    expect(buttonWrapper).to.have.length(1);

    buttonWrapper.simulate('click');
    await wait(301);
    expect(location.assignLocation).to.have.been.calledOnceWithExactly('/upgrade');
  });
});
