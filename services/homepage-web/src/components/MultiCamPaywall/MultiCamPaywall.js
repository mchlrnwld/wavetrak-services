import React from 'react';
import { CamIconWithRibbon, Button } from '@surfline/quiver-react';
import { trackClickedSubscribeCTA } from '@surfline/web-common';

import en from '../../intl/translations/en';
import { assignLocation } from '../../utils/assignLocation';

const doClickedSubscribe = () => {
  trackClickedSubscribeCTA({
    location: 'Multi Cam Page',
    pageName: 'multi-cam',
    category: 'cams & reports',
  });

  setTimeout(() => assignLocation('/upgrade'), 300);
};

const MultiCamPaywall = () => {
  const copy = en.MultiCam.paywall;

  return (
    <div className="sl-multi-cam-paywall">
      <CamIconWithRibbon isFilled isMulti />
      <h1 className="sl-multi-cam-paywall__header">{copy.header}</h1>
      <Button onClick={doClickedSubscribe}>{copy.goPremium}</Button>
      <a className="sl-multi-cam-paywall__link" href="/premium-benefits">
        {copy.link}
      </a>
    </div>
  );
};

MultiCamPaywall.propTypes = {};

export default MultiCamPaywall;
