import PropTypes from 'prop-types';
import React from 'react';
import { SurfHeight } from '@surfline/quiver-react';
import conditionClassModifier from '../../utils/conditionClassModifier';
import unitsPropType from '../../propTypes/units';

const ForecastAMPM = ({ type, minHeight, maxHeight, plus, rating, humanRelation, units }) => (
  <div
    className={`sl-forecast-am-pm sl-forecast-am-pm--${conditionClassModifier(
      rating
    )} sl-forecast-am-pm--${type.toLowerCase()}`}
  >
    <div className="sl-forecast-am-pm__indicator" />
    <div className="sl-forecast-am-pm__container">
      <div className="sl-forecast-am-pm__surf-and-type">
        <SurfHeight min={minHeight} max={maxHeight} plus={plus} units={units.waveHeight} />
        <span className="sl-forecast-am-pm__type">{type}</span>
      </div>
      <div className="sl-forecast-am-pm__rating">{rating.replace(/_/g, ' ')}</div>
      <div className="sl-forecast-am-pm__human-relation">{humanRelation}</div>
    </div>
  </div>
);

ForecastAMPM.propTypes = {
  type: PropTypes.oneOf(['AM', 'PM']).isRequired,
  minHeight: PropTypes.number.isRequired,
  maxHeight: PropTypes.number.isRequired,
  plus: PropTypes.bool,
  rating: PropTypes.string,
  humanRelation: PropTypes.string.isRequired,
  units: unitsPropType.isRequired,
};

ForecastAMPM.defaultProps = {
  plus: false,
  rating: 'LOLA',
};

export default ForecastAMPM;
