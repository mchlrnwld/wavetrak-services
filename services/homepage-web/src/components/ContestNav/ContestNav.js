/* eslint-disable react/jsx-no-target-blank */
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { get as _get } from 'lodash';
import allContests from '../ContestLandingWOTWContent/allContests';
import contestPropType from '../../propTypes/contest';
import contestParamsPropType from '../../propTypes/contestParams';
import config from '../../config';

const ContestNav = ({ contest, period, region, showLogo, match: { params } }) => (
  <div className="sl-contest-nav">
    <ul className="sl-contest-nav__menu">
      <li>
        <a href={`/${config.baseContestUrl(region, period, params.contest)}entries`}>Entries</a>
      </li>
      <li>
        <a href={`/${config.baseContestUrl(region, period, params.contest)}details`}>Details</a>
      </li>
      <li>
        <a href={`/${config.baseContestUrl(region, period, params.contest)}submit`}>Submit</a>
      </li>
      {params && params.contest === 'spirit-of-surfing' && (
        <li>
          <a href="https://cuervo.com/find-near-you/" target="_blank">
            Store Locator
          </a>
        </li>
      )}
      {contest &&
      contest.meta &&
      contest.meta.acf &&
      contest.meta.acf.vote_url &&
      params.contest !== 'spirit-of-surfing' ? (
        <li>
          <a href={contest.meta.acf.vote_url}>Vote</a>
        </li>
      ) : null}
      {params.contest === 'wave-of-the-winter' && period !== '2020-2021' && allContests[period] ? (
        <li>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a>
            Contests
            <ul className="sl-contest_nav__dropdown">
              <li>
                <a
                  href={`/${config.baseContestUrl(
                    allContests[period]['north-shore'].region,
                    allContests[period]['north-shore'].period,
                    params.contest
                  )}`}
                >
                  {allContests[period]['north-shore'].name}
                </a>
              </li>
              {allContests[period].regional.map((contestRegion) => (
                <li key={contestRegion.name}>
                  <a
                    href={`/${config.baseContestUrl(
                      contestRegion.region,
                      contestRegion.period,
                      params.contest
                    )}`}
                  >
                    {contestRegion.name}
                  </a>
                </li>
              ))}
            </ul>
          </a>
        </li>
      ) : null}
      {period === '2020-2021' && (
        <li>
          <a
            href={`/${config.baseContestUrl(
              region === 'regional' ? 'north-shore' : 'regional',
              period,
              params.contest
            )}`}
          >
            {region === 'regional' ? 'Hawaii' : 'Regional'}
          </a>
        </li>
      )}
    </ul>
    {showLogo && _get(contest, 'meta.acf.logo', null) ? (
      <a href={`/${config.baseContestUrl(region, period, params.contest)}`}>
        <img
          src={contest.meta.acf.logo}
          alt={contest.post.post_title}
          className="sl-contest-nav__img"
        />
      </a>
    ) : null}
  </div>
);

ContestNav.propTypes = {
  contest: contestPropType.isRequired,
  period: PropTypes.string.isRequired,
  region: PropTypes.string.isRequired,
  showLogo: PropTypes.bool.isRequired,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
};

export default withRouter(ContestNav);
