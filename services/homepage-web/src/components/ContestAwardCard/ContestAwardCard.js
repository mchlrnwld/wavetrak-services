import React from 'react';
import PropTypes from 'prop-types';
import contestAwardPropType from '../../propTypes/contestAward';
import config from '../../config';

const ContestAwardCard = ({ award, imageResizing }) => (
  <div className="sl-contest-award">
    <img
      className="sl-contest-award__image"
      src={config.cloudflareImageResizingUrl(award.imageUrl, imageResizing)}
      alt={award.title}
    />
    <div className="sl-contest-award__content">
      <h6 className="sl-contest-award__title">{award.title}</h6>
      <p className="sl-contest-award__description">{award.description}</p>
    </div>
  </div>
);

ContestAwardCard.propTypes = {
  award: contestAwardPropType.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

export default ContestAwardCard;
