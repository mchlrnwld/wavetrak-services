import React from 'react';
import PropTypes from 'prop-types';
import './MultiCamSurfReportHeader.scss';

const MultiCamSurfReportHeader = ({ subregionUrl, primarySpot }) => (
  <header className="sl-multi-cam-surf-report-header">
    <h1 className="sl-multi-cam-surf-report-header__title">{primarySpot.name}</h1>
    <nav className="sl-multi-cam-surf-report-header_link-container">
      <a
        className="sl-multi-cam-surf-report-header__link"
        href={`/surf-report/${primarySpot.name}/${primarySpot._id}`}
      >
        View Spot
      </a>
      {/* Since subregionURL comes from async data we can opt for not showing the links if the data is not there */}
      {subregionUrl && (
        <>
          <a className="sl-multi-cam-surf-report-header__link" href={subregionUrl}>
            Regional Forecast
          </a>
          <a
            className="sl-multi-cam-surf-report-header__link"
            href={`${subregionUrl}/premium-analysis`}
          >
            Premium Analysis
          </a>
        </>
      )}
    </nav>
  </header>
);

MultiCamSurfReportHeader.propTypes = {
  subregionUrl: PropTypes.string,
  primarySpot: PropTypes.shape({
    subregionId: PropTypes.string,
    _id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
};

MultiCamSurfReportHeader.defaultProps = {
  subregionUrl: null,
};

export default MultiCamSurfReportHeader;
