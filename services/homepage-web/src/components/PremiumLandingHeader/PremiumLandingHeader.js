import React from 'react';
import PropTypes from 'prop-types';
import PremiumLandingCTA from '../PremiumLandingCTA';

const PremiumLandingHeader = ({ backgroundImage, copy, doClickedSubscribeCTA }) => (
  <div className="premium-landing-header" style={{ backgroundImage: `url(${backgroundImage})` }}>
    <div className="premium-landing-header__container">
      <div className="premium-landing-header__title">{copy.title}</div>
      <PremiumLandingCTA doClickedSubscribeCTA={doClickedSubscribeCTA} location="Header" />
      <div className="premium-landing-header__bottom">
        <div className="premium-landing-header__text">{copy.text}</div>
        <div className="premium-landing-header__arrow">
          <svg width="43" height="12" viewBox="0 0 43 12">
            <path fill="none" fillRule="evenodd" strokeWidth="2" d="M1 1l20.494 9L42 1" />
          </svg>
        </div>
      </div>
    </div>
  </div>
);

PremiumLandingHeader.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  doClickedSubscribeCTA: PropTypes.func.isRequired,
  copy: PropTypes.shape({
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  }).isRequired,
};

export default PremiumLandingHeader;
