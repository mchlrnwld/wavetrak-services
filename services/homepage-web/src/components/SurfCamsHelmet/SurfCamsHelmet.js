import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { pageStructuredData } from '@surfline/web-common';
import config from '../../config';

const title = `Surf Cams - Watch Live Beach Webcams in HD - Surfline`;
const description = `Surfline’s global network of surf cams includes multiple views of spots, pinch-to-zoom effects and a host of new locations. Strategically placed, these real-time, HD-quality surf checks allow you to view waves and beach conditions from Manasquan to Malibu, Nazare to Newport, Oahu’s Seven Mile Miracle to Waco’s fresh-water skatepark, and hundreds of world-famous surf breaks in between.`;
const link = [{ rel: 'canonical', href: `${config.homepageSEOUrl}surf-cams` }];

const meta = [
  { name: 'description', content: description },
  { property: 'og:title', content: title },
  { property: 'og:url', content: link },
  { property: 'og:image', content: config.meta.og.image },
  { property: 'og:description', content: description },
  { property: 'twitter:title', content: title },
  { property: 'twitter:url', content: link },
  { property: 'twitter:image', content: config.meta.twitter.image },
  { property: 'twitter:description', content: description },
];

if (config.robots) {
  meta.push({
    name: 'robots',
    content: config.robots,
  });
}

const SurfCamsHelmet = ({ activeCollection }) => {
  const camStill = activeCollection?.spots?.[0]?.cameras?.[0]?.stillUrl;
  return (
    <Helmet title={title} meta={meta} link={link}>
      <script type="application/ld+json">
        {pageStructuredData({
          url: `${config.homepageSEOUrl}surf-cams`,
          headline: 'Premium Members Can Access up to Nine Cams at Once with Multi-Cam',
          dateModified: '10/1/2019',
          author: 'Surfline Forecast Team',
          description,
          cssSelector: '.sl-multi-cam-surf-report',
          image: camStill,
        })}
      </script>
    </Helmet>
  );
};

SurfCamsHelmet.propTypes = {
  activeCollection: PropTypes.shape({
    spots: PropTypes.arrayOf(
      PropTypes.shape({
        cameras: PropTypes.arrayOf(
          PropTypes.shape({
            stillUrl: PropTypes.string,
          })
        ),
      })
    ),
  }).isRequired,
};

export default SurfCamsHelmet;
