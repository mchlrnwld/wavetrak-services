import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { GoogleDFP } from '@surfline/quiver-react';
import IframeResizer from 'iframe-resizer-react';
import { canUseDOM } from '@surfline/web-common';
import loadAdConfig from '../../utils/adConfig';
import getContestAdId from '../../utils/getContestAdId';
import config from '../../config';

const ContestSOSEntries = ({ region, period, bgColorPrimary, location }) => {
  const [baseURL, setBaseURL] = useState('');
  const contestId =
    period === '2021' ? 'SpiritOfSurfingCuervoChallenge2021' : 'SpiritofSurfingCuervoChallenge';
  const encodedURL = encodeURIComponent(baseURL);
  const contestURL = `//surfline.votigo.com/fbcontests/profiletab/${contestId} ?hostingUrl=${encodedURL}`;

  useEffect(() => {
    if (canUseDOM) {
      setBaseURL(window.location.href);
    }
    const script1 = document.createElement('script');
    script1.src = '//dcveehzef7grj.cloudfront.net/js/vtg_embed.js';
    script1.async = true;
    document.body.appendChild(script1);

    return () => {
      document.body.removeChild(script1);
    };
  }, []);

  const onResized = ({ iframe }) => {
    const listener = () => {
      iframe?.iframeResizer?.removeListeners();
    };
    iframe.addEventListener('load', listener);

    iframe.removeEventListener('load', listener);

    const queryString = 'hostedapptype=fbcontestsisotope&showentry=';
    if (canUseDOM && window.location.href.includes(queryString)) {
      document.getElementById('vtg-container').scrollIntoView();
    } else {
      document.getElementById('backplane-header').scrollIntoView();
    }
  };

  return (
    <div
      className="sl-contest-landing__entries__container"
      style={{ backgroundColor: `${bgColorPrimary}` }}
    >
      {location === 'landing' ? <h3>Latest Entries</h3> : null}
      <div id="vtg-container" style={{ width: '100%' }}>
        <IframeResizer
          log={false}
          onResized={onResized}
          heightCalculationMethod="lowestElement"
          checkOrigin={false}
          scrolling={false}
          height="1080px"
          width="100%"
          style={{
            width: '100%',
            minWidth: '100%',
            border: '0',
          }}
          src={contestURL}
          allowFullScreen
          webkitallowfullscreen
          mozallowfullscreen
        />
      </div>
      <div className="sl-contest__ad-unit">
        <GoogleDFP
          adConfig={loadAdConfig(getContestAdId(region, 'bottom', period, 'sos'), [
            ['Box', 'Banner'],
          ])}
          isHtl
          isTesting={config.htl.isTesting}
        />
      </div>
    </div>
  );
};

ContestSOSEntries.propTypes = {
  region: PropTypes.string.isRequired,
  period: PropTypes.string.isRequired,
  bgColorPrimary: PropTypes.string,
  location: PropTypes.string,
};

ContestSOSEntries.defaultProps = {
  bgColorPrimary: null,
  location: null,
};

export default ContestSOSEntries;
