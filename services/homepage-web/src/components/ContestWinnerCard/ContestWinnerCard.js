import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ModalContainer } from '@surfline/quiver-react';
import ContestEntry from '../ContestEntry';
import contestWinnerPropType from '../../propTypes/contestWinner';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';
import config from '../../config';

class ContestWinnerCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: {
        isOpen: false,
        winner: {
          name: null,
          videoUrl: null,
        },
      },
    };
  }

  toggleModal = (winner) => {
    const { modal } = this.state;
    this.setState({
      modal: {
        isOpen: !modal.isOpen,
        winner: winner || { name: null, videoUrl: null },
      },
    });
  };

  render() {
    const { winner, imageResizing } = this.props;
    const { modal } = this.state;
    return (
      <div className="sl-contest-card">
        <div>
          <div>
            <div {...createAccessibleOnClick(() => this.toggleModal(winner))}>
              <div className="sl-contest-card__top">{winner.year}</div>
              <div className="sl-contest-card__image">
                <div
                  style={{
                    backgroundImage: `url(${
                      imageResizing
                        ? config.cloudflareImageResizingUrl(winner.imageUrl)
                        : winner.imageUrl
                    })`,
                  }}
                />
              </div>
              <h6 className="sl-contest-card__name">{winner.name}</h6>
            </div>
            <div className="sl-contest-card__links">
              <p {...createAccessibleOnClick(() => this.toggleModal(winner))}>Winning Wave</p>
              {winner.fullMovie ? (
                <p {...createAccessibleOnClick(() => this.toggleModal(winner.fullMovie))}>
                  Full {winner.year} Movie
                </p>
              ) : null}
            </div>
          </div>
        </div>
        {modal.winner.name ? (
          <ModalContainer
            isOpen={modal.isOpen}
            contentLabel={winner.name}
            onClose={this.toggleModal}
            location="contest-entry"
          >
            <div className="sl-contest-modal">
              <ContestEntry entry={modal.winner} displayShareIcons={false} />
            </div>
          </ModalContainer>
        ) : null}
      </div>
    );
  }
}

ContestWinnerCard.propTypes = {
  winner: contestWinnerPropType.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

export default ContestWinnerCard;
