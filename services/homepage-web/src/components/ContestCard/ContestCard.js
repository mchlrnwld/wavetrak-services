import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import contestCardPropType from '../../propTypes/contestCard';
import config from '../../config';

const getContestCardClassnames = (large) =>
  classnames({
    'sl-contest-card': true,
    'sl-contest-card--large': large,
  });

const ContestCard = ({ contest, large, regional, imageResizing }) => (
  <a
    href={`/${config.baseContestUrl(regional ? 'north-shore' : contest.region, contest.period)}`}
    className={getContestCardClassnames(large)}
  >
    {!regional && <div className="sl-contest-card__top">{contest.abbreviation}</div>}
    <div className="sl-contest-card__image">
      <div
        style={{
          backgroundImage: `url(${
            imageResizing ? config.cloudflareImageResizingUrl(contest.imageUrl) : contest.imageUrl
          })`,
        }}
      />
    </div>
    {!regional && <h6 className="sl-contest-card__name">{contest.name}</h6>}
  </a>
);

ContestCard.propTypes = {
  contest: contestCardPropType.isRequired,
  large: PropTypes.bool,
  regional: PropTypes.bool,
  imageResizing: PropTypes.bool.isRequired,
};

ContestCard.defaultProps = {
  large: false,
  regional: false,
};

export default ContestCard;
