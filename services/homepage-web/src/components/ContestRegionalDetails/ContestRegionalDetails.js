import React from 'react';
import regionalImage from '../ContestLandingWOTWContent/details/regional-wave-of-winter.png';

const ContestRegionalDetails = () => (
  <div className="sl-contest-regional-detail">
    <img
      className="sl-contest-regional-detail__image"
      src={regionalImage}
      alt="Regional wave of the winter"
    />
    <div className="sl-contest-regional-detail__content">
      <h6 className="sl-contest-regional-detail__title">WAVE OF THE WINTER: REGIONAL EDITION</h6>
      <p className="sl-contest-regional-detail__description">
        Welcome to the Wave of the Winter for the people, created to celebrate unsung local heroes
        who charge at beaches around the country, all winter long. Surfline’s Regional O’Neill Wave
        of the Winter contests will be judged via online user voting after the four-month holding
        period starting December 1, 2020 and running through March 31, 2021.
      </p>
      <ul className="sl-contest-regional-detail__regions">
        <li>
          <strong>Regions include:</strong>
        </li>
        <li>
          <strong>Northwest</strong> (Washington border to Point Conception)
        </li>
        <li>
          <strong>Southern California</strong> (Point Conception to Mexico border)
        </li>
        <li>
          <strong>Northeast</strong> (Maine to Maryland)
        </li>
        <li>
          <strong>Southeast</strong> (Virginia to Florida)
        </li>
        <li>
          <strong>Caribbean</strong> (Puerto Rico and Caribbean Islands)
        </li>
      </ul>
    </div>
  </div>
);

export default ContestRegionalDetails;
