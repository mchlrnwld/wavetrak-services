import classNames from 'classnames';

const FILLER_SPOT = { isFiller: true };
/**
 * Create fillers for when a user has not yet selected a cam for each cam slot
 * @param {Array<Object>} selectedSpots
 * @param {Number} numCams
 */
export const createFillers = (selectedSpots, numCams) => {
  const displayedSpots = selectedSpots.map((spot) => spot || FILLER_SPOT).slice(0, numCams);
  const filler = Array(numCams - displayedSpots.length).fill(FILLER_SPOT);
  return [...displayedSpots, ...filler];
};

/**
 * Creates class names for the multi cam component
 * @param {Number} numCams
 * @param {Boolean} timeout
 * @param {Boolean} isPaywalled
 */
export const multiCamClass = (numCams, timeout, isPaywalled) =>
  classNames({
    'sl-multi-cam__cams': true,
    'sl-multi-cam__cams--timed-out': timeout && !isPaywalled,
    'sl-multi-cam__cams--no-timeout': isPaywalled,
    'sl-multi-cam__cams--single': numCams === 1,
    'sl-multi-cam__cams--double': numCams === 2,
    'sl-multi-cam__cams--triple': numCams === 3,
    'sl-multi-cam__cams--quad': numCams === 4,
    'sl-multi-cam__cams--nintuple': numCams === 9,
  });

/**
 * Creates class names for the multi cam container
 * @param {Boolean} showUpSell
 * @param {Number} numCams
 * @param {boolean} isCamFullscreen
 */
export const multiCamContainerClass = (showUpSell, numCams, isCamFullscreen) =>
  classNames({
    'sl-multi-cam-container': true,
    'sl-multi-cam-container--upsell': showUpSell,
    'sl-multi-cam-container--triple-cam': numCams === 3 && !isCamFullscreen,
    'sl-multi-cam-container--triple-cam--fullscreen': numCams === 3 && isCamFullscreen,
  });

/**
 * Creates class names for the surfCams parent element
 * @param {Number} numCams
 */
export const surfCamsParentClass = (numCams) =>
  classNames({
    'sl-multi-cam': true,
    'sl-multi-cam--triple-cam': numCams === 3,
  });
