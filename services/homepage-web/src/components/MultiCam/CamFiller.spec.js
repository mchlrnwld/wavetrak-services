/** @prettier */

import { expect } from 'chai';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import CamFiller from './CamFiller';
import collectionsFixture, { collectionTypesFixture } from '../../utils/fixtures/collections';

describe('components / CamFiller', () => {
  const defaultProps = {
    camIndex: 0,
  };
  const initialState = {
    multiCam: {
      numCams: 2,
      active: collectionsFixture[0]._id,
      collectionTypes: collectionTypesFixture,
      collections: collectionsFixture,
    },
  };

  beforeEach(() => {});
  afterEach(() => {});

  it('should render the cam filler', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(CamFiller, props, { initialState });
    expect(wrapper).to.have.length(1);
    wrapper.unmount();
  });

  it('should tell the user to add spots if they have enough favorites', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(CamFiller, props, { initialState });
    expect(wrapper.text()).to.contain('add spot');
    wrapper.unmount();
  });

  it('should tell the user to add spots if they have enough favorites', () => {
    const props = {
      ...defaultProps,
    };
    const state = {
      ...initialState,
      multiCam: { ...initialState.multiCam, active: 'EmptySpotsCollection' },
    };

    const { wrapper } = mountWithReduxAndRouter(CamFiller, props, { initialState: state });
    expect(wrapper.text()).to.contain('add favorites');
    wrapper.unmount();
  });

  it('should redirect the user to add favorites', () => {
    const props = {
      ...defaultProps,
    };
    const state = {
      ...initialState,
      multiCam: {
        ...initialState.multiCam,
        active: 'EmptySpotsCollection',
        numCams: 1,
        primarySpotIndex: 0,
      },
    };

    const { wrapper } = mountWithReduxAndRouter(CamFiller, props, { initialState: state });

    const anchor = wrapper.find('a').simulate('click');
    expect(anchor.prop('href')).to.equal('/setup/favorite-surf-spots');
    wrapper.unmount();
  });
});
