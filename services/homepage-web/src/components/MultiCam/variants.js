export const camLoadingVariants = {
  initial: {
    backgroundPositionX: '-200%',
  },
  animate: {
    backgroundPositionX: '300%',
    transition: {
      type: 'tween',
      duration: 1.2,
      ease: 'easeInOut',
      loop: Infinity,
      repeatDelay: 0.03,
    },
  },
};

export const camVariants = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
    transition: {
      type: 'tween',
      ease: 'easeInOut',
      duration: 0.15,
    },
  },
};
