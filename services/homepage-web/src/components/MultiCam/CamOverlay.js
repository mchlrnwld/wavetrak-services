/** @prettier */

import React, { useMemo, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSWR from 'swr';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { FavoritesIndicator, useTimeout, PremiumCamCTA, HoverMenu } from '@surfline/quiver-react';
import { trackClickedSubscribeCTA, getUserFavorites } from '@surfline/web-common';

import { getCamTitle } from '../../utils/getCamTitle';
import ConditionsOverlay from '../ConditionsOverlay';
import CamFiller from './CamFiller';
import spotPropTypes from '../../propTypes/spot';
import conditionClassModifier from '../../utils/conditionClassModifier';
import cameraPropTypes from '../../propTypes/camera';
import MultiCamIcon from './icons/MultiCam';
import { doFavoriteSpot, doUnfavoriteSpot } from '../../actions/user';
import createAccessibleOnClick, { BTN } from '../../utils/createAccessibleOnClick';
import { fetchReport } from '../../common/api/spot';
import config from '../../config';
import FullscreenIcon from './icons/FullscreenIcon';

const overlayClass = (condition, visible) => {
  const conditionClass = condition ? conditionClassModifier(condition) : 'none';
  return classNames({
    'sl-cam-overlay': true,
    'sl-cam-overlay--visible': visible,
    [`sl-cam-overlay--${conditionClass}`]: conditionClass,
  });
};

const topOverlayClass = (visible) =>
  classNames({
    'sl-cam-overlay__top': true,
    'sl-cam-overlay__top--visible': visible,
  });

const onClickedSubscribeCTA = () =>
  trackClickedSubscribeCTA({
    location: 'premium only cam - multi',
    category: 'cams & reports',
    pageName: 'multi-cam',
  });

const CamOverlay = ({
  spot,
  camIndex,
  isFullscreen,
  toggleFullscreen,
  changeCamAngle,
  camAngle,
  showingPremiumCamCTA,
}) => {
  const dispatch = useDispatch();
  const favorites = useSelector(getUserFavorites);
  const [visible, setVisible] = useState(true);

  const { data: spotData } = useSWR(spot?._id, fetchReport, {
    refreshInterval: config.multiCamConditionsPollingInterval,
    dedupingInterval: config.multiCamConditionsPollingInterval - 1,
    revalidateOnFocus: true,
    focusThrottleInterval: config.multiCamConditionsPollingInterval,
  });

  const isFavorite = useMemo(
    () => !!favorites.find((favorite) => favorite._id === spot._id),
    [favorites, spot._id]
  );
  const doToggleFavorite = useCallback(() => {
    const spotId = spot._id;
    return dispatch(isFavorite ? doUnfavoriteSpot(spotId) : doFavoriteSpot(spotId));
  }, [dispatch, isFavorite, spot]);

  const { isFiller, cameras, conditions } = spot;
  const hasCam = cameras?.length > 0;
  const hasMultipleCams = cameras?.length > 1;
  const menuOptions = hasMultipleCams
    ? cameras.map((cam) => ({
        selected: getCamTitle(camAngle?.title) === getCamTitle(cam.title),
        value: cam,
        name: getCamTitle(cam.title),
      }))
    : [];

  // This hook fades the cam controls after 8 seconds on mount
  useTimeout({
    timeoutLength: 8000,
    initialState: true,
    cb: () => setVisible(false),
  });
  return (
    <div className={overlayClass(conditions?.value, visible)}>
      {showingPremiumCamCTA && (
        <PremiumCamCTA
          href="/upgrade"
          camStillUrl={camAngle?.stillUrl}
          onClickPaywall={onClickedSubscribeCTA}
        />
      )}
      {isFiller && <CamFiller camIndex={camIndex} />}
      <div className="sl-cam-overlay__bottom">
        {!isFiller && !showingPremiumCamCTA && spotData && (
          <ConditionsOverlay
            visible={visible}
            spot={spot}
            forecast={spotData.forecast}
            camAngle={camAngle}
          />
        )}
      </div>
      <div className={topOverlayClass(visible)}>
        {hasMultipleCams && (
          <HoverMenu
            Icon={MultiCamIcon}
            menuTitle="Cam Angle"
            menuOptions={menuOptions}
            onClickMenuItem={changeCamAngle}
            defaultValue={getCamTitle(camAngle?.title)}
            useSelectOnMobile
            selectDescription="- SELECT A CAM ANGLE -"
          />
        )}
        {!hasCam && !isFiller && <div className="sl-cam-overlay__no-cam">no cam</div>}
        <div
          className="sl-cam-overlay__favorite-toggle"
          type="button"
          title={isFavorite ? 'Remove Favorite' : 'Add Favorite'}
          {...createAccessibleOnClick(doToggleFavorite, BTN)}
        >
          {!isFiller && <FavoritesIndicator favorited={isFavorite} />}
        </div>
        <div
          className="sl-cam-overlay__fullscreen-icon"
          type="button"
          title="Toggle Fullscreen"
          {...createAccessibleOnClick(toggleFullscreen, BTN)}
        >
          {!isFiller && <FullscreenIcon isFullscreen={isFullscreen} onClick={toggleFullscreen} />}
        </div>
      </div>
    </div>
  );
};

CamOverlay.propTypes = {
  spot: spotPropTypes.isRequired,
  camIndex: PropTypes.number.isRequired,
  changeCamAngle: PropTypes.func.isRequired,
  isFullscreen: PropTypes.bool,
  toggleFullscreen: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  camRef: PropTypes.shape({ current: PropTypes.any }),
  camAngle: cameraPropTypes,
  showingPremiumCamCTA: PropTypes.bool,
};

CamOverlay.defaultProps = {
  camAngle: {},
  camRef: null,
  isFullscreen: false,
  toggleFullscreen: () => {},
  showingPremiumCamCTA: false,
};

export default CamOverlay;
