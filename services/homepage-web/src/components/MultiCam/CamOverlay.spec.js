/** @prettier */

import { expect } from 'chai';

import { PremiumCamCTA } from '@surfline/quiver-react';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import CamOverlay from './CamOverlay';
import CamFiller from './CamFiller';
import ConditionsOverlay from '../ConditionsOverlay';
import favoritesFixture from '../../utils/fixtures/favorites.json';
import storeFixture from '../../utils/fixtures/store';

describe('components / CamFiller', () => {
  const defaultProps = {
    camIndex: 0,
    spot: favoritesFixture[0],
    changeCamAngle: () => {},
    camAngle: favoritesFixture[0].cameras[0],
  };
  const initialState = {
    user: {
      ...storeFixture.user,
      favorites: favoritesFixture,
    },
  };

  beforeEach(() => {});
  afterEach(() => {});

  it('should render the cam overlay', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(CamOverlay, props, { initialState });
    expect(wrapper.find('.sl-cam-overlay')).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the cam filler if the spot is a filler', () => {
    const props = {
      ...defaultProps,
      spot: { isFiller: true },
    };

    const { wrapper } = mountWithReduxAndRouter(CamOverlay, props, { initialState });
    expect(wrapper.find(CamFiller)).to.have.length(1);
    expect(wrapper.find('.sl-cam-overlay__no-cam')).to.have.length(0);
    expect(wrapper.find(ConditionsOverlay)).to.have.length(0);
    wrapper.unmount();
  });

  it('should render the overlay with the condition class name', () => {
    const props = {
      ...defaultProps,
    };
    const state = {
      ...initialState,
    };

    const { wrapper } = mountWithReduxAndRouter(CamOverlay, props, { initialState: state });
    expect(wrapper.find('.sl-cam-overlay--none')).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the overlay with the premium cam CTA', () => {
    const props = {
      ...defaultProps,
      showingPremiumCamCTA: true,
    };
    const state = {
      ...initialState,
    };

    const { wrapper } = mountWithReduxAndRouter(CamOverlay, props, { initialState: state });
    const cta = wrapper.find(PremiumCamCTA);
    expect(cta).to.have.length(1);
    expect(cta.prop('camStillUrl')).to.equal(props.camAngle.stillUrl);
    expect(cta.prop('href')).to.equal('/upgrade');
    wrapper.unmount();
  });
});
