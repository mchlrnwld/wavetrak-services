import React, { useCallback, useMemo, useRef, useEffect, useState } from 'react';
import { getUserPremiumStatus, getUserTypeStatus, IS_BASIC } from '@surfline/web-common';
import { RailIcon, CamTimeout, CloseIcon, useDeviceSize } from '@surfline/quiver-react';

import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import Cam from './Cam';
import { getMultiCam, getActiveCollectionId } from '../../selectors/multiCam';
import CamControls from './CamControls';
import { useToggle } from '../../hooks/useToggle';
import { useKeyPress } from '../../hooks/useKeyPress';
import { changePrimarySpotIndex } from '../../actions/multiCam';
import MultiCamPaywall from '../MultiCamPaywall/MultiCamPaywall';
import createAccessibleOnClick, { BTN } from '../../utils/createAccessibleOnClick';
import { useCamTimeout } from './hooks/useCamTimeout';
import { createFillers, multiCamContainerClass, multiCamClass, surfCamsParentClass } from './utils';
import CamHeaderCTA from '../CamHeaderCTA';
import { useSaveMultiCamState } from './hooks/useSaveMultiCamState';

const useRedux = () => {
  const dispatch = useDispatch();
  const doChangePrimarySpotIndex = useCallback(
    (camIndex) => dispatch(changePrimarySpotIndex(camIndex)),
    [dispatch]
  );
  return { doChangePrimarySpotIndex };
};

const MultiCam = React.memo(
  ({
    canMountCams,
    isFullscreen,
    toggleFullscreen,
    timeoutTime,
    toggleRightRail,
    rightRailIsOpen,
    multiCamRef,
    children,
  }) => {
    /* Saved Multi Cam States */
    useSaveMultiCamState();

    /* Cam Container Ref */
    const camsRef = useRef();

    /** State */
    const { numCams, selectedSpots, primarySpotIndex } = useSelector(getMultiCam, shallowEqual);
    const isPremium = useSelector(getUserPremiumStatus);
    const userType = useSelector(getUserTypeStatus);
    const activeCollectionId = useSelector(getActiveCollectionId);
    useDeviceSize();

    /** Actions */
    const { doChangePrimarySpotIndex } = useRedux();

    /**
     * Memoized array of selected spot ID's `selectedSpots` should be immutable since it is in redux
     * so it is safe to use as a dependency in `useMemo`
     */
    const selectedSpotIds = useMemo(() => selectedSpots.map((spot) => spot?._id), [selectedSpots]);

    /** Control the cams playing states */
    const [isPlaying, setPlaying, togglePlaying] = useToggle(isPremium);

    /** Control the ad playing states */
    const firstCamSpot = selectedSpots.find((spot) => spot?.cameras?.length);

    const [timeout, resetTimeout] = useCamTimeout({
      setPlaying,
      isPlaying,
      selectedSpotIds,
      timeoutTime,
      hasCam: !!firstCamSpot,
    });

    const spotsToRender = useMemo(
      () => createFillers(selectedSpots, numCams),
      [numCams, selectedSpots]
    );

    // Allow users to select primary cam using 1, 2, 3, 4, etc...
    const primaryCamSelectionKeys = spotsToRender.map((_, index) => `${index + 1}`);
    useKeyPress(
      useCallback(
        (key) => doChangePrimarySpotIndex(parseInt(key, 10) - 1),
        [doChangePrimarySpotIndex]
      ),
      ...primaryCamSelectionKeys
    );

    const showPaywall = !isPremium;

    /** If a free user changes collections, require them to click play again */
    useEffect(() => {
      if (userType === IS_BASIC && numCams !== 9 && activeCollectionId) {
        setPlaying(false);
      }
    }, [activeCollectionId, numCams, setPlaying, userType]);

    const [isCamFullscreen, setIsCamFullscreen] = useState(false);

    return (
      <div className={surfCamsParentClass(numCams)}>
        <div
          className="sl-multi-cam__rail-toggle"
          {...createAccessibleOnClick(toggleRightRail, BTN)}
        >
          {rightRailIsOpen ? (
            <CloseIcon />
          ) : (
            <>
              <RailIcon />
              <span>spots</span>
            </>
          )}
        </div>
        <div ref={multiCamRef}>
          <div className={multiCamContainerClass(numCams, isCamFullscreen)}>
            <div ref={camsRef} className={multiCamClass(numCams, timeout, !isPremium)}>
              {spotsToRender.map((spot, camIndex) => (
                <Cam
                  // Since we can have the same cam in different cam players we have to use an index as a key
                  key={`${spot?._id || ''}${camIndex}`} // eslint-disable-line react/no-array-index-key
                  spot={spot}
                  isPlaying={isPlaying}
                  camIndex={camIndex}
                  primarySpotIndex={primarySpotIndex}
                  onClick={() => doChangePrimarySpotIndex(camIndex)}
                  canMountCams={canMountCams}
                  isPremium={isPremium}
                  camsRef={camsRef}
                  showingPaywall={showPaywall}
                  isParentFullscreen={isFullscreen}
                  setIsCamFullscreen={setIsCamFullscreen}
                />
              ))}
              <CamTimeout onClick={resetTimeout} />
              {showPaywall && <MultiCamPaywall />}
            </div>
          </div>
          <CamControls
            isPlaying={isPlaying}
            isFullscreen={isFullscreen}
            togglePlaying={togglePlaying}
            toggleFullscreen={toggleFullscreen}
          />
        </div>

        {!isPremium && <CamHeaderCTA segmentProperties={{ location: 'button top - multicam' }} />}
        {children}
      </div>
    );
  }
);

MultiCam.propTypes = {
  canMountCams: PropTypes.bool,
  isFullscreen: PropTypes.bool,
  timeoutTime: PropTypes.number,
  toggleFullscreen: PropTypes.func.isRequired,
  toggleRightRail: PropTypes.func.isRequired,
  rightRailIsOpen: PropTypes.bool,
  children: PropTypes.element,
  multiCamRef: PropTypes.shape().isRequired,
};

MultiCam.defaultProps = {
  canMountCams: true,
  isFullscreen: false,
  // 20 Minutes in milliseconds
  timeoutTime: 1200000,
  rightRailIsOpen: false,
  children: null,
};

export default MultiCam;
