/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import PlayPauseIcon from './icons/PlayPauseIcon';
import FullscreenIcon from './icons/FullscreenIcon';
import CamIcon from './icons/CamIcon';

const CamControls = ({ isFullscreen, isPlaying, camSetup, togglePlaying, toggleFullscreen }) => {
  const numCams = useSelector((state) => state?.multiCam?.numCams);

  return (
    <div className="sl-multi-cam__controls">
      <div className="sl-multi-cam__controls--left">
        <div className="sl-multi-cam__controls__view">
          {camSetup.map((camNum) => (
            <CamIcon key={camNum} active={camNum === numCams} camNum={camNum} />
          ))}
        </div>
      </div>
      <div className="sl-multi-cam__controls--right">
        <PlayPauseIcon isPlaying={isPlaying} onClick={togglePlaying} />
        <FullscreenIcon isFullscreen={isFullscreen} onClick={toggleFullscreen} />
      </div>
    </div>
  );
};

CamControls.propTypes = {
  isPlaying: PropTypes.bool.isRequired,
  isFullscreen: PropTypes.bool.isRequired,
  camSetup: PropTypes.arrayOf(PropTypes.number),
  togglePlaying: PropTypes.func.isRequired,
  toggleFullscreen: PropTypes.func.isRequired,
};

CamControls.defaultProps = {
  camSetup: [1, 2, 3, 4, 9],
};

export default CamControls;
