/** @prettier  */
import { useCallback } from 'react';
import { useTimeout } from '@surfline/quiver-react';

/**
 * Create a cam timeout (default 5 mins), which pauses the cams and sets `timeout`
 * to true once the timeout has been reached
 * @param {object} props
 * @param {boolean} props.setPlaying
 * @param {boolean} props.isPlaying
 * @param {boolean} props.hasCam
 */
export const useCamTimeout = ({ setPlaying, isPlaying, selectedSpotIds, timeoutTime, hasCam }) => {
  const doPause = useCallback(() => setPlaying(false), [setPlaying]);
  const triggers = [isPlaying, selectedSpotIds];
  const initialState = false;
  const [timeout, setTimeoutState] = useTimeout({
    timeoutLength: timeoutTime,
    triggers,
    initialState,
    cb: doPause,
    start: isPlaying && hasCam,
  });

  const resetTimeout = () => {
    setPlaying(true);
    setTimeoutState(false);
  };

  return [timeout, resetTimeout];
};
