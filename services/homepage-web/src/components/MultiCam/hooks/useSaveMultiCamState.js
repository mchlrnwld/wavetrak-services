import { useEffect, useRef } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { getActiveCollection, getMultiCam } from '../../../selectors/multiCam';
import { setCamAngleIndex } from '../../../actions/multiCam';
import { saveMultiCamPreference } from '../../../utils/multiCamLocalStorage';

/**
 * @description Custom hook which returns a function used to
 * dispatch the `SET_CAM_ANGLE_INDEX` action to update the
 * `selectedCamAngleIndex` on the spot in `selectedSpots[selectedSpotIndex]`
 */
export const useSetCamAngleIndex = () => {
  const dispatch = useDispatch();

  /**
   * @param {number} selectedSpotIndex
   * @param {number} camAngleIndex
   */
  const updateCamAngleIndex = (selectedSpotIndex, camAngleIndex) => {
    dispatch(setCamAngleIndex(selectedSpotIndex, camAngleIndex));
  };

  const cbRef = useRef(updateCamAngleIndex);

  return cbRef.current;
};

/**
 * @description Custom hook which looks for changes in `activeCollection`, `selectedSpots`, and `numCams`.
 * When any of those changes, it will update the local storage entry corresponding to the given `activeCollection._id`
 * and save the multi cam state for the given active collection.
 * The local storage state will be structured as follows (see example)
 * @example
 * ```javascript
 * {
 *    [activeCollection._id]: {
 *      spots: ['123', '11', '122251251', null, null, null],
 *    },
 *    numCams: {
 *      [activeCollection._id]: 4
 *    }
 * }
 * ```
 */
export const useSaveMultiCamState = () => {
  const { numCams, selectedSpots } = useSelector(getMultiCam, shallowEqual);
  const activeCollection = useSelector(getActiveCollection, shallowEqual);

  useEffect(() => {
    // Saves the users selected spots to local storage
    saveMultiCamPreference(
      activeCollection._id,
      activeCollection.fetchedSpots,
      selectedSpots,
      numCams
    );
  }, [activeCollection.fetchedSpots, activeCollection._id, selectedSpots, numCams]);
};
