/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import en from '../../intl/translations/en';
import BigPlus from './icons/BigPlus';
import { getMultiCam } from '../../selectors/multiCam';

const FillerWrapper = ({ isLink, children, isPrimary }) =>
  isLink ? (
    <a
      className="sl-multi-cam__cam-filler"
      href={isPrimary ? '/setup/favorite-surf-spots' : ''}
      tabIndex="0"
    >
      {children}
    </a>
  ) : (
    <div className="sl-multi-cam__cam-filler">{children}</div>
  );

const CamFiller = ({ camIndex }) => {
  const { numCams, active, primarySpotIndex } = useSelector(getMultiCam);

  const isPrimary = primarySpotIndex === camIndex;
  const hasEnoughFavorites = active?.spots?.length > numCams;

  return (
    <FillerWrapper isLink={!hasEnoughFavorites} isPrimary={isPrimary}>
      <BigPlus animate pulse />
      <h1 className="sl-multi-cam__cam-filler__text">
        {en.MultiCam.cam.filler(hasEnoughFavorites)}
      </h1>
    </FillerWrapper>
  );
};

FillerWrapper.propTypes = {
  isLink: PropTypes.bool,
  children: PropTypes.node,
  isPrimary: PropTypes.bool,
};

FillerWrapper.defaultProps = {
  isLink: false,
  children: null,
  isPrimary: false,
};

CamFiller.propTypes = {
  camIndex: PropTypes.number.isRequired,
};

export default CamFiller;
