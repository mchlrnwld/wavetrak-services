/** @prettier */

import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const className = (active) =>
  classNames({
    'sl-multi-cam-cam-icon': true,
    'sl-multi-cam-cam-icon--dual': true,
    'sl-multi-cam-cam-icon--active': !!active,
  });

const Inactive = () => (
  <g transform="translate(1.000000, 1.000000)" fillRule="nonzero">
    <rect
      id="Rectangle-16"
      stroke="#0058B0"
      strokeWidth="2"
      x="0"
      y="0"
      width="38"
      height="27"
      rx="2"
    />
    <path
      d="M22,4 L33,4 C33.5522847,4 34,4.44771525 34,5 L34,12 C34,12.5522847 33.5522847,13 33,13 L22,13 C21.4477153,13 21,12.5522847 21,12 L21,5 C21,4.44771525 21.4477153,4 22,4 Z"
      id="Rectangle-25-Copy-8"
      fill="#0058B0"
    />
  </g>
);

const Active = () => (
  <g transform="translate(1.000000, 1.000000)" fillRule="nonzero">
    <rect
      id="Rectangle-16"
      stroke="#0058B0"
      strokeWidth="2"
      fill="#0058B0"
      x="0"
      y="0"
      width="38"
      height="27"
      rx="2"
    />
    <path
      d="M22,4 L33,4 C33.5522847,4 34,4.44771525 34,5 L34,12 C34,12.5522847 33.5522847,13 33,13 L22,13 C21.4477153,13 21,12.5522847 21,12 L21,5 C21,4.44771525 21.4477153,4 22,4 Z"
      id="Rectangle-25-Copy-8"
      fill="#FFFFFF"
    />
  </g>
);

const DualCamIcon = ({ active }) => (
  <svg
    className={className(active)}
    width="40px"
    height="29px"
    viewBox="0 0 40 29"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="MVP" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      {active ? <Active /> : <Inactive />}
    </g>
  </svg>
);

DualCamIcon.propTypes = {
  active: PropTypes.bool,
};

DualCamIcon.defaultProps = {
  active: false,
};

export default DualCamIcon;
