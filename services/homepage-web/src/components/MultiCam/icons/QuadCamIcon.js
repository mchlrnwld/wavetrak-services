/** @prettier */

import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const className = (active) =>
  classNames({
    'sl-multi-cam-cam-icon': true,
    'sl-multi-cam-cam-icon--quad': true,
    'sl-multi-cam-cam-icon--active': !!active,
  });

const Active = () => (
  <g id="Group-16-Copy-3" transform="translate(1.000000, 1.000000)" fillRule="nonzero">
    <rect
      id="Rectangle-16"
      stroke="#0058B0"
      strokeWidth="2"
      fill="#0058B0"
      x="0"
      y="0"
      width="38"
      height="27"
      rx="2"
    />
    <path
      d="M5,4 L17,4 C17.5522847,4 18,4.44771525 18,5 L18,11 C18,11.5522847 17.5522847,12 17,12 L5,12 C4.44771525,12 4,11.5522847 4,11 L4,5 C4,4.44771525 4.44771525,4 5,4 Z M5,15 L17,15 C17.5522847,15 18,15.4477153 18,16 L18,22 C18,22.5522847 17.5522847,23 17,23 L5,23 C4.44771525,23 4,22.5522847 4,22 L4,16 C4,15.4477153 4.44771525,15 5,15 Z M21,4 L33,4 C33.5522847,4 34,4.44771525 34,5 L34,11 C34,11.5522847 33.5522847,12 33,12 L21,12 C20.4477153,12 20,11.5522847 20,11 L20,5 C20,4.44771525 20.4477153,4 21,4 Z M21,15 L33,15 C33.5522847,15 34,15.4477153 34,16 L34,22 C34,22.5522847 33.5522847,23 33,23 L21,23 C20.4477153,23 20,22.5522847 20,22 L20,16 C20,15.4477153 20.4477153,15 21,15 Z"
      id="Combined-Shape"
      fill="#FFFFFF"
    />
  </g>
);

const Inactive = () => (
  <g id="Group-16" transform="translate(1.000000, 1.000000)" fillRule="nonzero">
    <rect
      id="Rectangle-16"
      stroke="#0058B0"
      strokeWidth="2"
      x="0"
      y="0"
      width="38"
      height="27"
      rx="2"
    />
    <path
      d="M5,4 L17,4 C17.5522847,4 18,4.44771525 18,5 L18,11 C18,11.5522847 17.5522847,12 17,12 L5,12 C4.44771525,12 4,11.5522847 4,11 L4,5 C4,4.44771525 4.44771525,4 5,4 Z"
      id="Rectangle-25-Copy-5"
      fill="#0058B0"
    />
    <path
      d="M5,15 L17,15 C17.5522847,15 18,15.4477153 18,16 L18,22 C18,22.5522847 17.5522847,23 17,23 L5,23 C4.44771525,23 4,22.5522847 4,22 L4,16 C4,15.4477153 4.44771525,15 5,15 Z"
      id="Rectangle-25-Copy-14"
      fill="#0058B0"
    />
    <path
      d="M21,4 L33,4 C33.5522847,4 34,4.44771525 34,5 L34,11 C34,11.5522847 33.5522847,12 33,12 L21,12 C20.4477153,12 20,11.5522847 20,11 L20,5 C20,4.44771525 20.4477153,4 21,4 Z"
      id="Rectangle-25-Copy-8"
      fill="#0058B0"
    />
    <path
      d="M21,15 L33,15 C33.5522847,15 34,15.4477153 34,16 L34,22 C34,22.5522847 33.5522847,23 33,23 L21,23 C20.4477153,23 20,22.5522847 20,22 L20,16 C20,15.4477153 20.4477153,15 21,15 Z"
      id="Rectangle-25-Copy-15"
      fill="#0058B0"
    />
  </g>
);
const QuadCamIcon = ({ active }) => (
  <svg
    className={className(active)}
    width="40px"
    height="29px"
    viewBox="0 0 40 29"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      {active ? <Active /> : <Inactive />}
    </g>
  </svg>
);

QuadCamIcon.propTypes = {
  active: PropTypes.bool,
};

QuadCamIcon.defaultProps = {
  active: false,
};

export default QuadCamIcon;
