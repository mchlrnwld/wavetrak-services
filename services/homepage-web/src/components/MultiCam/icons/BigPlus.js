/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import { motion, useAnimation } from 'framer-motion';

import { useFramerAnimation } from '../../../hooks/useFramerAnimation';

const variants = {
  initial: {
    scale: 0.975,
    transition: {
      ease: 'easeInOut',
    },
  },
  pulse: {
    scale: 1,
    transition: {
      type: 'spring',
      damping: 0,
      stiffness: 30,
      mass: 2,
    },
  },
};

const BigPlus = ({ pulse }) => {
  const controls = useAnimation();
  useFramerAnimation({ controls, variant: pulse ? 'pulse' : 'initial', start: pulse });

  return (
    <motion.svg
      className="sl-icon-big-plus"
      tabIndex="-1"
      initial="initial"
      animate={controls}
      variants={variants}
      width="50px"
      height="50px"
      viewBox="0 0 50 50"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <title>Add Favorites</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" opacity="0.4">
        <g transform="translate(-349.000000, -487.000000)">
          <g transform="translate(323.000000, 488.000000)">
            <g transform="translate(27.000000, 0.000000)">
              <circle id="Oval" stroke="#96A9B2" strokeWidth="2" cx="24" cy="24" r="24" />
              <g transform="translate(9.000000, 9.000000)" fill="#96A9B2" fillRule="nonzero">
                <polygon points="14.5 0 14.5 30 16.5 30 16.5 0" />
                <polygon
                  transform="translate(15.500000, 15.000000) rotate(90.000000) translate(-15.500000, -15.000000) "
                  points="14.5 0 14.5 30 16.5 30 16.5 0"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </motion.svg>
  );
};

BigPlus.propTypes = {
  pulse: PropTypes.bool,
};

BigPlus.defaultProps = {
  pulse: false,
};

export default BigPlus;
