/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useHover } from '@surfline/quiver-react';

import createAccessibleOnClick from '../../../utils/createAccessibleOnClick';

const containerClass = (isHovering) =>
  classNames({
    'sl-play-pause-container': true,
    'sl-play-pause-container--hover': isHovering,
  });

/**
 * Component that renders either a play or pause icon depending on the `isPlaying` prop.
 *
 * @param {Object} props
 * @param {boolean} props.isPlaying determines whether to show the pause or the play icon
 */
const PlayPauseIcon = ({ isPlaying, onClick }) => {
  const { hoverProps, isHovering } = useHover({ className: 'sl-play-pause-button' });

  return (
    <div className={containerClass(isHovering)}>
      <button type="button" {...createAccessibleOnClick(onClick)} {...hoverProps}>
        <svg
          width="16px"
          height="20px"
          viewBox="0 0 16 20"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            {isPlaying ? (
              <g
                className="sl-play-pause-button__shape"
                transform="translate(-764.000000, -774.000000)"
                fill={isHovering ? '#0058B0' : '#333333'}
                fillRule="nonzero"
              >
                <g transform="translate(764.000000, 774.000000)">
                  <rect x="0" y="0" width="5" height="22" rx="1" />
                  <rect x="11" y="-1.58095759e-13" width="5" height="22" rx="1" />
                </g>
              </g>
            ) : (
              <polygon
                className="sl-play-pause-button__shape"
                fill={isHovering ? '#0058B0' : '#333333'}
                points="16 10 0 20 0 0"
              />
            )}
          </g>
        </svg>
      </button>
    </div>
  );
};

PlayPauseIcon.propTypes = {
  isPlaying: PropTypes.bool,
  onClick: PropTypes.func,
};

PlayPauseIcon.defaultProps = {
  isPlaying: false,
  onClick: () => {},
};

export default PlayPauseIcon;
