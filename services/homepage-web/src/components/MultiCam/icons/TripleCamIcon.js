/** @prettier */

import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const className = (active) =>
  classNames({
    'sl-multi-cam-cam-icon': true,
    'sl-multi-cam-cam-icon--triple': true,
    'sl-multi-cam-cam-icon--active': !!active,
  });

const Inactive = () => (
  <g id="Group-16-Copy" transform="translate(1.000000, 1.000000)" fillRule="nonzero">
    <rect
      id="Rectangle-16"
      stroke="#0058B0"
      strokeWidth="2"
      x="0"
      y="0"
      width="38"
      height="27"
      rx="2"
    />
    <path
      d="M4,6 L22,6 C22.5522847,6 23,6.44771525 23,7 L23,20 C23,20.5522847 22.5522847,21 22,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,7 C3,6.44771525 3.44771525,6 4,6 Z"
      id="Rectangle-25-Copy-5"
      fill="#0058B0"
    />
    <path
      d="M26,14 L34,14 C34.5522847,14 35,14.4477153 35,15 L35,20 C35,20.5522847 34.5522847,21 34,21 L26,21 C25.4477153,21 25,20.5522847 25,20 L25,15 C25,14.4477153 25.4477153,14 26,14 Z"
      id="Rectangle-25-Copy-14"
      fill="#0058B0"
    />
    <path
      d="M26,6 L34,6 C34.5522847,6 35,6.44771525 35,7 L35,11 C35,11.5522847 34.5522847,12 34,12 L26,12 C25.4477153,12 25,11.5522847 25,11 L25,7 C25,6.44771525 25.4477153,6 26,6 Z"
      id="Rectangle-25-Copy-8"
      fill="#0058B0"
    />
  </g>
);
const Active = () => (
  <g id="Group-16-Copy-5" transform="translate(1.000000, 1.000000)" fillRule="nonzero">
    <rect
      id="Rectangle-16"
      stroke="#0058B0"
      strokeWidth="2"
      fill="#0058B0"
      x="0"
      y="0"
      width="38"
      height="27"
      rx="2"
    />
    <path
      d="M4,6 L22,6 C22.5522847,6 23,6.44771525 23,7 L23,20 C23,20.5522847 22.5522847,21 22,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,7 C3,6.44771525 3.44771525,6 4,6 Z M26,14 L34,14 C34.5522847,14 35,14.4477153 35,15 L35,20 C35,20.5522847 34.5522847,21 34,21 L26,21 C25.4477153,21 25,20.5522847 25,20 L25,15 C25,14.4477153 25.4477153,14 26,14 Z M26,6 L34,6 C34.5522847,6 35,6.44771525 35,7 L35,11 C35,11.5522847 34.5522847,12 34,12 L26,12 C25.4477153,12 25,11.5522847 25,11 L25,7 C25,6.44771525 25.4477153,6 26,6 Z"
      id="Combined-Shape"
      fill="#FFFFFF"
    />
  </g>
);

const TripleCamIcon = ({ active }) => (
  <svg
    className={className(active)}
    width="40px"
    height="29px"
    viewBox="0 0 40 29"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      {active ? <Active /> : <Inactive />}
    </g>
  </svg>
);

TripleCamIcon.propTypes = {
  active: PropTypes.bool,
};

TripleCamIcon.defaultProps = {
  active: false,
};

export default TripleCamIcon;
