/** @prettier */

import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const className = (active) =>
  classNames({
    'sl-multi-cam-cam-icon': true,
    'sl-multi-cam-cam-icon--single': true,
    'sl-multi-cam-cam-icon--active': !!active,
  });

const Active = () => (
  <g
    id="1-non-selected"
    transform="translate(1.000000, 1.000000)"
    fill="#0058B0"
    stroke="#0058B0"
    strokeWidth="2"
  >
    <rect id="Rectangle-16" x="0" y="0" width="38" height="27" rx="2" />
  </g>
);

const Inactive = () => (
  <g id="1-non-selected" transform="translate(1.000000, 1.000000)" stroke="#0058B0" strokeWidth="2">
    <rect id="Rectangle-16" x="0" y="0" width="38" height="27" rx="2" />
  </g>
);

const SingleCamIcon = ({ active }) => (
  <svg
    className={className(active)}
    width="40px"
    height="29px"
    viewBox="0 0 40 29"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      {active ? <Active /> : <Inactive />}
    </g>
  </svg>
);

SingleCamIcon.propTypes = {
  active: PropTypes.bool,
};

SingleCamIcon.defaultProps = {
  active: false,
};

export default SingleCamIcon;
