/** @prettier */

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useDeviceSize, useHover } from '@surfline/quiver-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { trackEvent, getUserTypeStatus } from '@surfline/web-common';
import SingleCamIcon from './SingleCamIcon';
import DualCamIcon from './DualCamIcon';
import TripleCamIcon from './TripleCamIcon';
import QuadCamIcon from './QuadCamIcon';
import NintupleCamIcon from './NintupleCamIcon';
import { setNumCams } from '../../../actions/multiCam';
import createAccessibleOnClick, { BTN } from '../../../utils/createAccessibleOnClick';

const getContainerClass = (active, isHovering) =>
  classNames({
    'sl-multi-cam-cam-icon__container': true,
    'sl-multi-cam-cam-icon__container--active': !!active,
    'sl-multi-cam-cam-icon__container--hover': !!isHovering,
  });

const CamIcon = ({ active, camNum }) => {
  const dispatch = useDispatch();
  const device = useDeviceSize();
  const userType = useSelector(getUserTypeStatus);
  const { hoverProps, isHovering, disableHovering } = useHover();

  const onClick = () => {
    dispatch(setNumCams(camNum));
    trackEvent('Toggled Cam View', {
      category: 'cams & reports',
      name: 'multi-cam - view',
      isMobileView: device.width < 976,
      view: camNum > 1 ? 'MULTI' : 'SINGLE',
      camNumber: camNum,
      subscriptionEntitlement: userType,
    });
    // Since the height of the cam container changes sometimes when we change the
    // number of cams, this can prevent the `onMouseOut` or `onMouseLeave` events
    // from firing. We can fix the persistent hover state by explicity disabling
    disableHovering();
  };

  return (
    <button
      type="button"
      {...hoverProps}
      disabled={!!active}
      {...createAccessibleOnClick(onClick, BTN)}
      className={getContainerClass(active, isHovering)}
    >
      {camNum === 1 && <SingleCamIcon active={active} />}
      {camNum === 2 && <DualCamIcon active={active} />}
      {camNum === 3 && <TripleCamIcon active={active} />}
      {camNum === 4 && <QuadCamIcon active={active} />}
      {camNum === 9 && <NintupleCamIcon active={active} />}
    </button>
  );
};

CamIcon.propTypes = {
  active: PropTypes.bool.isRequired,
  camNum: PropTypes.number.isRequired,
};

export default CamIcon;
