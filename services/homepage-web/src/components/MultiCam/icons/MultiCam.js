/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';

const MultiCam = ({ title, description }) => (
  <svg
    width="21px"
    height="15px"
    viewBox="0 0 21 15"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <title>{title}</title>
    <desc>{description}</desc>
    <defs>
      <path
        id="sl-multi-cam-icon"
        d="M2.18749985,2.06249979 L2.18749985,11.4062496 C2.18749985,11.8032043 1.86570452,12.1249996 1.46874987,12.1249996 C1.07179521,12.1249996 0.749999881,11.8032043 0.749999881,11.4062496 L0.749999881,2.06249979 C0.749999881,1.26859048 1.39359054,0.624999821 2.18749985,0.624999821 L12.9687496,0.624999821 C13.3657043,0.624999821 13.6874996,0.946795151 13.6874996,1.34374981 C13.6874996,1.74070446 13.3657043,2.06249979 12.9687496,2.06249979 L2.18749985,2.06249979 Z M20.8749995,4.69855529 L20.8749995,13.5458884 L17.0416662,11.1666663 L17.0416662,13.840544 C17.0416662,14.4809662 16.6091384,14.9999998 16.0754533,14.9999998 L4.59121276,14.9999998 C4.05752759,14.9999998 3.62499982,14.4809662 3.62499982,13.840544 L3.62499982,4.65945529 C3.62499982,4.01903308 4.05752759,3.49999976 4.59121276,3.49999976 L16.0754533,3.49999976 C16.6091384,3.49999976 17.0416662,4.01903308 17.0416662,4.65945529 L17.0416662,7.07777747 L20.8749995,4.69855529 Z"
      />
    </defs>
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-27.000000, -51.000000)">
        <g transform="translate(27.000000, 51.000000)">
          <g>
            <mask id="sl-multi-cam-mask-2" fill="white">
              <use xlinkHref="#sl-multi-cam-icon" />
            </mask>
            <use id="Combined-Shape" fill="#96A9B2" xlinkHref="#sl-multi-cam-icon" />
            <g mask="url(#sl-multi-cam-mask-2)" fill="#FFFFFF">
              <g transform="translate(-5.000000, -8.000000)">
                <polygon points="0 0 31.9444438 0 31.9444438 31.9444438 0 31.9444438" />
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

MultiCam.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

MultiCam.defaultProps = {
  title: 'Multi Cam Icon',
  description: 'This spot has multiple cams',
};

export default MultiCam;
