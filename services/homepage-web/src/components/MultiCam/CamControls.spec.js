/** @prettier */

import { expect } from 'chai';
import sinon from 'sinon';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import CamControls from './CamControls';
import CamIcon from './icons/CamIcon';

describe('components / CamControls', () => {
  let togglePlayingStub;
  let toggleFullscreenStub;
  const defaultProps = {
    isFullscreen: false,
    isPlaying: false,
    camSetup: [1, 2],
  };
  const initialState = {
    user: {
      details: {},
      entitlements: [],
    },
    multiCam: {
      numCams: 2,
    },
  };

  beforeEach(() => {
    toggleFullscreenStub = sinon.stub();
    togglePlayingStub = sinon.stub();
  });
  afterEach(() => {
    toggleFullscreenStub.reset();
    togglePlayingStub.reset();
  });

  it('should render the desired cam setups', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(CamControls, props, { initialState });
    const CamIcons = wrapper.find(CamIcon);
    expect(CamIcons).to.have.length(2);
  });

  it('should render an active cam icon', () => {
    const props = {
      ...defaultProps,
    };

    const { wrapper } = mountWithReduxAndRouter(CamControls, props, { initialState });
    const ActiveCamIcons = wrapper.find(CamIcon).filterWhere((node) => node.prop('active'));
    expect(ActiveCamIcons).to.have.length(1);
  });

  it('should handle play button clicks', () => {
    const props = {
      ...defaultProps,
      togglePlaying: togglePlayingStub,
    };

    const { wrapper } = mountWithReduxAndRouter(CamControls, props, { initialState });
    const PlayButton = wrapper.find('button.sl-play-pause-button');

    PlayButton.simulate('click');

    expect(togglePlayingStub).to.have.been.calledOnce();
  });

  it('should handle fullscreen button clicks', () => {
    const props = {
      ...defaultProps,
      toggleFullscreen: toggleFullscreenStub,
    };

    const { wrapper } = mountWithReduxAndRouter(CamControls, props, { initialState });
    const FullscreenButton = wrapper.find('button.sl-fullscreen-toggle-button');

    FullscreenButton.simulate('click');

    expect(toggleFullscreenStub).to.have.been.calledOnce();
  });
});
