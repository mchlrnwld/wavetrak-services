import { expect } from 'chai';
import sinon from 'sinon';
import { CamPlayerV2 } from '@surfline/quiver-react';

import Cam from './Cam';
import CamOverlay from './CamOverlay';
import spotFixture from '../../utils/fixtures/spot';
import multiCamSpotFixture from '../../utils/fixtures/multiCamSpot';
import * as hooks from '../../hooks/useFramerAnimation';
import * as multiCam from '../../actions/multiCam';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import CamFiller from './CamFiller';
import favoritesFixture from '../../utils/fixtures/favorites.json';
import storeFixture from '../../utils/fixtures/store';

describe('components / Cam', () => {
  let useFramerAnimationStub;
  let changePrimarySpotIndexStub;
  const defaultProps = {
    spot: spotFixture,
    camIndex: 0,
    primarySpotIndex: 0,
    canMountCams: false,
    isPlaying: false,
    setIsCamFullscreen: () => null,
  };
  const initialState = {
    backplane: {
      user: {
        ...storeFixture.user,
        favorites: favoritesFixture,
      },
    },
  };

  beforeEach(() => {
    useFramerAnimationStub = sinon.stub(hooks, 'useFramerAnimation');
    changePrimarySpotIndexStub = sinon.spy(multiCam, 'changePrimarySpotIndex');
  });
  afterEach(() => {
    useFramerAnimationStub.restore();
    changePrimarySpotIndexStub.restore();
  });

  it('should render the Cam component', () => {
    const props = {
      ...defaultProps,
    };

    useFramerAnimationStub.returns([true]);
    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });
    const cam = wrapper.find('div.sl-multi-cam__cams_cam');
    expect(cam).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the CamPlayerV2 component', () => {
    const props = {
      ...defaultProps,
      camIndex: 1,
    };

    useFramerAnimationStub.returns([true, () => {}]);

    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });

    const camPlayer = wrapper.find(CamPlayerV2);
    expect(camPlayer.prop('isFavorite')).to.be.true();
    wrapper.unmount();
  });

  it('should render the CamOverlay component', () => {
    const props = {
      ...defaultProps,
    };

    useFramerAnimationStub.returns([true]);
    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });
    const camOverlay = wrapper.find(CamOverlay);
    expect(camOverlay).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the Cam filler is not spot is selected', () => {
    const props = {
      ...defaultProps,
      spot: { isFiller: true },
    };

    useFramerAnimationStub.returns([true, () => {}]);
    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });
    const camFiller = wrapper.find(CamFiller);
    expect(camFiller).to.have.length(1);

    expect(wrapper.find('.sl-multi-cam__cams_cam')).to.have.length(0);
    wrapper.unmount();
  });

  it('should render the Cam gradient if the cam is not ready', () => {
    const props = {
      ...defaultProps,
    };

    // Make it so cam animation not done
    useFramerAnimationStub.returns([false]);

    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });

    expect(wrapper.find('div.sl-multi-cam__loading-gradient')).to.have.length(1);
    wrapper.unmount();
  });

  it('should change the primary spot index when clicked', () => {
    const camIndex = 1;
    const props = {
      ...defaultProps,
      camIndex,
      onClick: changePrimarySpotIndexStub,
    };

    // Make it so cam animation not done
    useFramerAnimationStub.returns([false]);

    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });

    wrapper.find('div.sl-multi-cam__cam').simulate('click');
    // Accessibility test
    wrapper.find('div.sl-multi-cam__cam').simulate('keyDown', { key: ' ', keyCode: 32, which: 32 });

    expect(changePrimarySpotIndexStub).to.have.been.calledTwice();
    wrapper.unmount();
  });

  it('should not render the multi cam view menu for single cam spots', () => {
    const camIndex = 1;
    const props = {
      ...defaultProps,
      camIndex,
      onClick: changePrimarySpotIndexStub,
    };

    useFramerAnimationStub.returns([true]);

    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });

    const hoverMenu = wrapper.find('.quiver-hover-menu');

    expect(hoverMenu).to.have.length(0);
    wrapper.unmount();
  });

  it('should change the cam view when selecting a new one from the multi cam view menu', () => {
    const camIndex = 1;
    const props = {
      ...defaultProps,
      camIndex,
      onClick: changePrimarySpotIndexStub,
      spot: multiCamSpotFixture,
    };

    useFramerAnimationStub.returns([true, () => {}]);

    const { wrapper } = mountWithReduxAndRouter(Cam, props, { initialState });

    const camPlayerWrapper = wrapper.find(CamPlayerV2);

    expect(camPlayerWrapper.prop('camera')).to.deep.equal(multiCamSpotFixture.cameras[0]);

    wrapper
      .find('.quiver-hover-menu__menu-item:not(.quiver-hover-menu__menu-item--selected)')
      .simulate('click', { nativeEvent: { stopImmediatePropagation: () => {} } });

    const updatedCamPlayer = camPlayerWrapper.update().find(CamPlayerV2);

    expect(updatedCamPlayer.prop('camera')).to.deep.equal(multiCamSpotFixture.cameras[1]);
    wrapper.unmount();
  });
});
