import { expect } from 'chai';
import sinon from 'sinon';
import MultiCam from './MultiCam';
import favoritesFixture from '../../utils/fixtures/favorites.json';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import CamControls from './CamControls';
import * as useCamTimeout from './hooks/useCamTimeout';
import * as toggleHook from '../../hooks/useToggle';
import store from '../../utils/fixtures/store';
import collectionsFixture, { collectionTypesFixture } from '../../utils/fixtures/collections';
import MultiCamPaywall from '../MultiCamPaywall';

describe('components / MultiCam', () => {
  let useCamTimeoutStub;
  let useToggleStub;
  let setPlayingSpy;
  let setTimeoutSpy;

  const defaultProps = {
    selectedSpots: favoritesFixture,
    numCams: 4,
    primarySpotIndex: 0,
    toggleFullscreen: () => {},
  };
  const initialState = {
    ...store,
    multiCam: {
      active: collectionsFixture[0]._id,
      numCams: 4,
      selectedSpots: [favoritesFixture[0]],
      primarySpotIndex: 1,
      collections: collectionsFixture,
      collectionTypes: collectionTypesFixture,
    },
    flags: {
      treatments: {
        sl_web_multicam_free_user: 'on',
      },
    },
  };

  beforeEach(() => {
    setTimeoutSpy = sinon.spy();
    setPlayingSpy = sinon.spy();

    useCamTimeoutStub = sinon.stub(useCamTimeout, 'useCamTimeout').returns([false]);
    useToggleStub = sinon.stub(toggleHook, 'useToggle').returns([false, setPlayingSpy, () => {}]);
  });

  afterEach(() => {
    if (useCamTimeoutStub.restore) useCamTimeoutStub.restore();
    useToggleStub.restore();

    setTimeoutSpy.resetHistory();
    setPlayingSpy.resetHistory();
  });

  it('should render the desired number of cams', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, { initialState });
    const camWrappers = wrapper.find('.sl-multi-cam__cam');
    const primaryCamWrapper = wrapper.find('.sl-multi-cam__cam--primary');

    expect(camWrappers).to.have.length(4);
    expect(primaryCamWrapper).to.have.length(1);
    wrapper.unmount();
  });

  it('should render the cam controls', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, { initialState });
    const controlWrapper = wrapper.find(CamControls);
    expect(controlWrapper).to.have.length(1);
    wrapper.unmount();
  });

  it('should show a cam timeout once the timeout limit has been reached', async () => {
    /**
     * setting `timeoutTime` to 0 allows us to run in on the next tick of the event loop
     * so that we can easily test the timeout as it is really implemented rather than
     * using stubs
     */
    const props = { ...defaultProps, timeoutTime: 0 };
    useCamTimeoutStub.restore();
    useToggleStub.restore();
    useToggleStub = sinon.stub(toggleHook, 'useToggle').returns([true, setPlayingSpy, () => {}]);

    const { wrapper } = mountWithReduxAndRouter(MultiCam, props, { initialState });

    expect(wrapper).to.have.length(1);

    const rerender = wrapper.update();

    // In order to allow the `setTimeout` used by `useTimeout` to execute we have to await
    // a new one
    const sleep = (m) => new Promise((r) => setTimeout(r, m));
    await sleep(0);

    expect(rerender.update().find('.sl-multi-cam__cams--timed-out')).to.have.length(1);
    wrapper.unmount();
  });

  it('should start playing the cams when the "continue watching" button is pressed', () => {
    let didCall = false;
    // For some reason, returning a spy here makes the test hang, so this workaround is better
    useCamTimeoutStub.returns([
      true,
      () => {
        didCall = true;
      },
    ]);

    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, { initialState });

    wrapper.find('.quiver-cam-timeout button').simulate('click');

    expect(didCall).to.be.true();
    wrapper.unmount();
  });

  it('should autoplay for premium users', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, {
      initialState,
    });

    expect(useToggleStub).to.have.been.calledWithExactly(true);
    wrapper.unmount();
  });

  it('should not autoplay for non-premium users', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, {
      initialState: {
        ...initialState,
        user: {
          ...initialState.user,
          entitlements: [],
        },
      },
    });

    expect(useToggleStub).to.have.been.calledWithExactly(false);
    wrapper.unmount();
  });

  it('should show the pay wall for basic users', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, {
      initialState: {
        ...initialState,
        backplane: {
          ...initialState.backplane,
          user: {
            ...initialState.user,
            entitlements: [],
          },
        },
        multiCam: {
          ...initialState.multiCam,
          selectedSpots: favoritesFixture,
        },
      },
    });

    useToggleStub.returns([true, () => {}, () => {}]);

    const payWall = wrapper.find(MultiCamPaywall);

    expect(payWall).to.have.length(1);
    wrapper.unmount();
  });

  it('should show the pay wall for anonymous users', () => {
    const { wrapper } = mountWithReduxAndRouter(MultiCam, defaultProps, {
      initialState: {
        ...initialState,
        backplane: {
          ...initialState.backplane,
          user: {
            ...initialState.user,
            entitlements: [],
            details: null,
          },
        },
        flags: {
          treatments: {
            sl_web_multicam_free_user: 'on',
          },
        },
      },
    });

    // Force re-render with `setProps`
    const payWall = wrapper.find(MultiCamPaywall);

    expect(payWall).to.have.length(1);
    wrapper.unmount();
  });
});
