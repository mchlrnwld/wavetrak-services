import React, { useState, useRef, useEffect } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { CamPlayerV2, useDeviceSize, SpotThumbnailBackground } from '@surfline/quiver-react';
import classNames from 'classnames';
import { motion, useAnimation } from 'framer-motion';
import { trackEvent, getUserTypeStatus, IS_BASIC } from '@surfline/web-common';

import createAccessibleOnClick, { BTN } from '../../utils/createAccessibleOnClick';
import { camLoadingVariants, camVariants } from './variants';
import { useFramerAnimation } from '../../hooks/useFramerAnimation';
import { useToggle } from '../../hooks/useToggle';
import spotPropTypes, { spotDefaultProps } from '../../propTypes/spot';
import { getNumCams } from '../../selectors/multiCam';
import CamOverlay from './CamOverlay';
import { useSetCamAngleIndex } from './hooks/useSaveMultiCamState';
import useFullscreen from '../../hooks/useFullscreen';
import { useSpotIsFavorite } from '../../hooks/useSpotIsFavorite';

const camClassName = (isPrimary) =>
  classNames({
    'sl-multi-cam__cam': true,
    'sl-multi-cam__cam--primary': isPrimary,
  });

const getCamContainerClass = (camIndex, isDragging, draggable, isFullscreen) =>
  classNames({
    'sl-multi-cam__cam-container': true,
    [`sl-multi-cam__cam-container--${camIndex + 1}`]: true,
    'sl-multi-cam__cam-container--dragging': isDragging,
    'sl-multi-cam__cam-container--no-drag': !draggable || isFullscreen,
    'sl-multi-cam__cam-container--draggable': draggable && !isFullscreen,
    'sl-multi-cam__cam-container--fullscreen': isFullscreen,
  });

const Cam = React.memo(
  ({
    spot,
    camIndex,
    primarySpotIndex,
    canMountCams,
    isPlaying,
    onClick,
    camsRef,
    showingPaywall,
    isParentFullscreen,
    setIsCamFullscreen,
  }) => {
    const defaultCamAngleIndex = spot?.selectedCamAngleIndex || 0;
    // State
    const [camAngle, setCamAngle] = useState(spot?.cameras?.[defaultCamAngleIndex]);
    const [camReady, setCamReady] = useToggle();
    const [isDragging, setDragging] = useState(false);
    const device = useDeviceSize();

    // Redux
    const numCams = useSelector(getNumCams);
    const userType = useSelector(getUserTypeStatus);
    const setCamAngleIndex = useSetCamAngleIndex();

    // Custom Hooks
    const isFavorite = useSpotIsFavorite(spot._id);

    // Constants
    const hasCam = !!spot?.cameras?.length;
    const showPremiumCamCTA = !showingPaywall && userType === IS_BASIC && camAngle?.isPremium;
    const showingCam = !showingPaywall && !showPremiumCamCTA;
    const { isFiller } = spot;
    const draggable = camIndex === 1 && numCams === 2;
    const isPrimary = camIndex === primarySpotIndex;
    const showCamStill = hasCam && showingPaywall && !showPremiumCamCTA;

    // Cam Animations
    const camControls = useAnimation();
    const [camAnimationDone, setCamAnimationDone] = useFramerAnimation({
      controls: camControls,
      variant: 'visible',
      start: canMountCams && hasCam && camReady && showingCam,
      log: true,
    });
    const showCamLoadingGradient = hasCam && !camAnimationDone && !isFiller && showingCam;

    const camRef = useRef();
    const [isFullscreen, setFullscreen] = useState(false);
    const [isBrowserFullScreen, toggleBrowserFullScreen] = useFullscreen(camRef);

    const toggleFullscreen = () => {
      setFullscreen(!isFullscreen);
      setIsCamFullscreen(!isFullscreen);
      if (!isParentFullscreen && isBrowserFullScreen === isFullscreen) {
        toggleBrowserFullScreen();
      }
    };

    useEffect(() => {
      if (!isParentFullscreen) {
        setFullscreen(false);
        setIsCamFullscreen(false);
      }
    }, [isParentFullscreen, setIsCamFullscreen]);

    // requires a different useEffect as we don't want the above one to fire
    // when we set the cam to browser fullscreen
    useEffect(() => {
      if (!isBrowserFullScreen && !isParentFullscreen) {
        setFullscreen(false);
        setIsCamFullscreen(false);
      }
    }, [isBrowserFullScreen, isParentFullscreen, setIsCamFullscreen]);

    /**
     * @description resets the cam animation state and sets the new cam angle.
     * @param {object} cam Cam object
     * @param {string} cam._id
     */
    const changeCamAngle = (cam) => {
      camControls.set('hidden');
      setCamAnimationDone(false);
      setCamReady(false);
      setCamAngle(cam);
      const camAngleIndex = spot?.cameras.findIndex((camera) => camera._id === cam._id);
      setCamAngleIndex(camIndex, camAngleIndex);
      trackEvent('Toggled Cam View', {
        category: 'cams & reports',
        name: 'multi-cam - angle',
        subregionId: spot.subregionId,
        spotId: spot._id,
        camId: cam._id,
        camName: cam.title,
        isMobileView: device.width < 976,
        view: numCams > 1 ? 'MULTI' : 'SINGLE',
        camNumber: numCams,
        subscriptionEntitlement: userType,
      });
    };

    return (
      <motion.div
        drag={draggable}
        dragConstraints={camsRef}
        dragElastic={0.3}
        dragMomentum={false}
        onDragStart={() => setDragging(true)}
        onDragEnd={() => setDragging(false)}
        className={getCamContainerClass(
          camIndex,
          isDragging,
          draggable,
          isFullscreen || isBrowserFullScreen
        )}
      >
        <div
          type="button"
          className={camClassName(isPrimary)}
          ref={camRef}
          {...createAccessibleOnClick(() => !isPrimary && onClick(), BTN)}
        >
          {showCamLoadingGradient && (
            <motion.div
              className="sl-multi-cam__loading-gradient"
              animate="animate"
              initial="initial"
              variants={camLoadingVariants}
            />
          )}
          {!hasCam && <SpotThumbnailBackground spot={spot} />}
          {hasCam && showingCam && (
            <motion.div
              className="sl-multi-cam__cams_cam"
              variants={camVariants}
              initial="hidden"
              animate={camControls}
            >
              <CamPlayerV2
                key={`${camAngle?._id}${camIndex}`}
                controlledAds
                controlledPlayback
                handleCamReady
                camReady={camReady}
                setCamReady={setCamReady}
                playerId={`${camAngle?._id}${camIndex}`}
                camera={camAngle}
                spotId={spot?._id}
                spotName={spot?.name}
                isPlaying={isPlaying}
                isMultiCam
                isLocation="Surf Cams"
                isFavorite={isFavorite}
              />
            </motion.div>
          )}
          {showCamStill && (
            <div
              className="sl-cam-still"
              style={{ backgroundImage: `url(${camAngle?.stillUrl})` }}
            />
          )}
          <CamOverlay
            changeCamAngle={changeCamAngle}
            camAngle={camAngle}
            camIndex={camIndex}
            spot={spot}
            isFullscreen={isFullscreen || isBrowserFullScreen}
            toggleFullscreen={toggleFullscreen}
            showingPremiumCamCTA={showPremiumCamCTA}
          />
        </div>
      </motion.div>
    );
  }
);

Cam.propTypes = {
  spot: spotPropTypes,
  camIndex: PropTypes.number.isRequired,
  primarySpotIndex: PropTypes.number.isRequired,
  canMountCams: PropTypes.bool.isRequired,
  isPlaying: PropTypes.bool,
  onClick: PropTypes.func,
  isPremium: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  camsRef: PropTypes.shape({ current: PropTypes.any }),
  isPlayingAd: PropTypes.bool,
  showingPaywall: PropTypes.bool,
  isParentFullscreen: PropTypes.bool.isRequired,
  setIsCamFullscreen: PropTypes.func.isRequired,
};

Cam.defaultProps = {
  spot: {
    ...spotDefaultProps,
    isFiller: true,
  },
  isPlaying: false,
  onClick: () => null,
  isPremium: true,
  isPlayingAd: false,
  camsRef: null,
  showingPaywall: false,
};

export default Cam;
