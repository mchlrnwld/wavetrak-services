import PropTypes from 'prop-types';
import React from 'react';
import { find as _find } from 'lodash';
import { Button as QuiverButton, Select as QuiverSelect } from '@surfline/quiver-react';
import { BackArrow } from './Icons';
import homecamPropType from '../../propTypes/homecam';
import userFavoritesPropType from '../../propTypes/userFavorites';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const HomeCamSelect = ({
  homecam,
  userFavorites,
  doSelectHomeCamOption,
  doToggleHomeCamSelectView,
  doUpdateHomeCam,
}) => {
  const { camera, loading, success } = homecam;
  const options = userFavorites.reduce((opts, favorite) => {
    const favCam = _find(favorite.cameras, (cam) => cam.status && !cam.status.isDown);
    if (favCam) {
      opts.push({
        text: favorite.name,
        value: favCam._id,
      });
    }
    return opts;
  }, []);
  return (
    <div className="sl-home-cam-select">
      <div
        {...createAccessibleOnClick(doToggleHomeCamSelectView)}
        className="sl-home-cam-select__back"
      >
        <BackArrow className="sl-home-cam-select__back-icon" />
      </div>
      <div className="sl-home-cam-select__inner">
        <QuiverSelect
          select={{ onChange: (evt) => doSelectHomeCamOption(evt.target.value) }}
          options={options}
          defaultValue={camera._id}
        />
        <QuiverButton onClick={() => doUpdateHomeCam()} loading={loading} success={success}>
          Set as Homecam
        </QuiverButton>
      </div>
    </div>
  );
};

HomeCamSelect.propTypes = {
  homecam: homecamPropType.isRequired,
  userFavorites: userFavoritesPropType.isRequired,
  doSelectHomeCamOption: PropTypes.func.isRequired,
  doToggleHomeCamSelectView: PropTypes.func.isRequired,
  doUpdateHomeCam: PropTypes.func.isRequired,
};

export default HomeCamSelect;
