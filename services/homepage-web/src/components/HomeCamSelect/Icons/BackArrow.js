import PropTypes from 'prop-types';
import React from 'react';

const BackArrow = ({ className }) => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" className={className}>
    <g id="BackArrow" strokeWidth="1" fillRule="evenodd">
      <g id="Group" fillRule="nonzero">
        <g id="Shape">
          <path
            d={`M11.84,23.72 C5.32,23.72 0.04,18.44 0.04,11.92 C0.04,5.4 5.32,
            0.12 11.84,0.12 C18.36,0.12 23.64,5.4 23.64,11.92 C23.64,18.44 18.32,
            23.72 11.84,23.72 Z M11.84,1.08 C5.88,1.08 1.04,5.92 1.04,11.88 C1.04,
            17.84 5.88,22.68 11.84,22.68 C17.8,22.68 22.64,17.84 22.64,11.88 C22.64,
            5.92 17.8,1.08 11.84,1.08 Z`}
          />
          <path
            d={`M17.4,11.16 L8.44,11.16 L11.4,8.2 C11.64,7.96 11.64,7.52 11.4,7.28 C11.16,
              7.04 10.72,7.04 10.48,7.28 L6.68,11.08 C6.28,11.48 6.28,12.12 6.68,
              12.52 L10.48,16.32 C10.6,16.44 10.76,16.52 10.96,16.52 C11.16,
              16.52 11.28,16.44 11.44,16.32 C11.68,16.08 11.68,15.64 11.44,15.4 L8.48,
              12.44 L17.4,12.44 C17.76,12.44 18.04,12.16 18.04,11.8 C18.04,11.44 17.76,
              11.16 17.4,11.16 Z`}
          />
        </g>
      </g>
    </g>
  </svg>
);

BackArrow.propTypes = {
  className: PropTypes.string,
};

BackArrow.defaultProps = {
  className: '',
};

export default BackArrow;
