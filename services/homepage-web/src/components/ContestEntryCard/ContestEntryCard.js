import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Cookies } from 'react-cookie';
import { format, parseISO } from 'date-fns';
import classnames from 'classnames';
import { ModalContainer } from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';
import { contestVoteNamespace } from '../../utils/contests';
import ContestEntry from '../ContestEntry';
import Shaka from '../Icons/Shaka';
import contestEntryPropType from '../../propTypes/contestEntry';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';
import config from '../../config';

const cookies = new Cookies();

class ContestEntryCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: {
        isOpen: false,
        entry: null,
      },
    };
  }

  toggleModal = (entry) => {
    const { modal } = this.state;
    this.setState({
      modal: {
        isOpen: !modal.isOpen,
        entry,
      },
    });
  };

  hasVoted = (entryId) => cookies.get(contestVoteNamespace(entryId));

  getVoteClassNames = (entryId) =>
    classnames({
      'sl-contest-entry-card__details__votes': true,
      'sl-contest-entry-card__details__votes--voted': this.hasVoted(entryId),
    });

  getVoteColor = (entryId) => {
    const { isLandingPage } = this.props;
    const hasVoted = this.hasVoted(entryId);

    if (!hasVoted) {
      return color('white');
    }

    return isLandingPage ? color('black') : '#41A4FB';
  };

  render() {
    const { entry, doTrackEvent, region, period, doUpvoteContestEntry, imageResizing } = this.props;
    const { modal } = this.state;
    return (
      <div className="sl-contest-entry-card">
        <>
          <div className="sl-contest-entry-card__date">#{entry.entryId}</div>
          <div
            className="sl-contest-entry-card__video"
            {...createAccessibleOnClick(() => this.toggleModal(entry))}
          >
            <div
              style={{
                backgroundImage: `url(${
                  imageResizing
                    ? config.cloudflareImageResizingUrl(entry.posterUrl)
                    : entry.posterUrl
                })`,
              }}
            />
          </div>
          <div className="sl-contest-entry-card__details">
            <h5>{entry.name}</h5>
            <div className="sl-contest-entry-card__details__tags">
              <p className="sl-contest-entry-card__details__tags__spot">{entry.spot.name}</p>
              <p>{format(parseISO(entry.date), 'MMMM dd')}</p>
            </div>
            <div className={this.getVoteClassNames(entry.id)}>
              <Shaka fill={this.getVoteColor(entry.id)} />
              <span className="sl-contest-entry-card__details__votes__value">
                {entry.voteCount}
              </span>
            </div>
          </div>
        </>
        <ModalContainer
          isOpen={modal.isOpen}
          contentLabel={entry.surfer ? entry.surfer.name : null}
          onClose={this.toggleModal}
          location="contest-entry"
        >
          <div className="sl-contest-modal">
            <ContestEntry
              entry={entry}
              doTrackEvent={doTrackEvent}
              period={period}
              region={region}
              doUpvoteContestEntry={doUpvoteContestEntry}
            />
          </div>
        </ModalContainer>
      </div>
    );
  }
}

ContestEntryCard.propTypes = {
  isLandingPage: PropTypes.bool,
  entry: contestEntryPropType.isRequired,
  doTrackEvent: PropTypes.func.isRequired,
  period: PropTypes.string.isRequired,
  region: PropTypes.string.isRequired,
  doUpvoteContestEntry: PropTypes.func.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

ContestEntryCard.defaultProps = {
  isLandingPage: false,
};

export default ContestEntryCard;
