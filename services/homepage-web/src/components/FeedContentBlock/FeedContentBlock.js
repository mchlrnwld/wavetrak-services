import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import classNames from 'classnames';
import { GoogleDFP } from '@surfline/quiver-react';
import FeedArticle from '../FeedArticle';
import FeedCamAndForecast from '../FeedCamAndForecast';
import articleTypePropType from '../../propTypes/articleType';
import camOfTheMomentProptype from '../../propTypes/camOfTheMoment';
import userDetailsProptype from '../../propTypes/userDetails';
import userSettingsProptype from '../../propTypes/userSettings';
import forecastPropType from '../../propTypes/forecast';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

class FeedContentBlock extends PureComponent {
  getFeedAdClasses = (type) =>
    classNames({
      'sl-feed-content-block__ad': true,
      'sl-feed-content-block__ad--top': type === 'top',
      'sl-feed-content-block__ad--bottom': type === 'bottom',
    });

  getFeedRowClasses = (type) =>
    classNames({
      'sl-feed-content-block__row': true,
      'sl-feed-content-block__row--curated': type === 'curated',
      'sl-feed-content-block__row--feature-large': type === 'feedLarge',
      'sl-feed-content-block__row--feature-small': type === 'feedSmall',
    });

  generateAdConfig = (adIdentifier, userEntitlements, isUser, qaFlag = null) =>
    loadAdConfig(adIdentifier, [['qaFlag', qaFlag]], userEntitlements, isUser);

  renderFeedRow = (index, articleType, count, offset = 0) => {
    const { userSettings, imageResizing } = this.props;
    const { articles, limit, type } = articleType;
    const start = (index - 1) * limit + offset;
    const rowArticles = articles.slice(start, start + count);
    return rowArticles.map((article) => (
      <FeedArticle
        article={article}
        feature={type === 'feedLarge' || type === 'feedSmall'}
        key={article.id}
        type={type}
        feedLocale={userSettings?.feed?.localized}
        imageResizing={imageResizing}
      />
    ));
  };

  render() {
    const {
      curated,
      doFetchForecastArticles,
      feedLarge,
      feedSmall,
      blockIndex,
      isPremium,
      camOfTheMoment,
      forecast,
      onSetHomeForecast,
      adblock,
      qaFlag,
      userDetails,
      userEntitlements,
      onDetectedAdBlock,
      device,
    } = this.props;
    const initialLoad = blockIndex === 1;
    return (
      <div className="sl-feed-content-block">
        {initialLoad ? (
          <div className={this.getFeedAdClasses('top')}>
            <GoogleDFP
              adConfig={this.generateAdConfig(
                'homepage_feed_top',
                userEntitlements,
                !!userDetails,
                qaFlag
              )}
              isHtl
              isTesting={config.htl.isTesting}
            />
          </div>
        ) : null}
        {initialLoad ? (
          <FeedCamAndForecast
            cotmId="mobile"
            camOfTheMoment={device === 'MOBILE' ? camOfTheMoment : null}
            doFetchForecastArticles={doFetchForecastArticles}
            isPremium={isPremium}
            adblock={adblock}
            onDetectedAdBlock={onDetectedAdBlock}
            onSetHomeForecast={onSetHomeForecast}
            qaFlag={qaFlag}
            forecast={forecast}
            isLoggedIn={!!userDetails}
          />
        ) : null}
        <div className={this.getFeedRowClasses('feedLarge')}>
          {this.renderFeedRow(blockIndex, feedLarge, 1)}
        </div>
        <div className={this.getFeedRowClasses('feedSmall')}>
          {this.renderFeedRow(blockIndex, feedSmall, 2)}
        </div>
        <div className={this.getFeedRowClasses('feedLarge')}>
          {this.renderFeedRow(blockIndex, feedLarge, 1, 1)}
        </div>
        <div className={this.getFeedRowClasses('curated')}>
          {this.renderFeedRow(blockIndex, curated, 3)}
        </div>
        <div className={this.getFeedRowClasses('feedLarge')}>
          {this.renderFeedRow(blockIndex, feedLarge, 1, 2)}
        </div>
        {initialLoad ? (
          <div className={this.getFeedAdClasses('bottom')}>
            <GoogleDFP
              adConfig={this.generateAdConfig(
                'homepage_feed_bottom',
                userEntitlements,
                !!userDetails,
                qaFlag
              )}
              isHtl
              isTesting={config.htl.isTesting}
            />
          </div>
        ) : null}
        <div className={this.getFeedRowClasses('curated')}>
          {this.renderFeedRow(blockIndex, curated, 3, 3)}
        </div>
        <div className={this.getFeedRowClasses('curated')}>
          {this.renderFeedRow(blockIndex, curated, 3, 6)}
        </div>
      </div>
    );
  }
}

FeedContentBlock.propTypes = {
  blockIndex: PropTypes.number.isRequired,
  curated: articleTypePropType.isRequired,
  doFetchForecastArticles: PropTypes.func.isRequired,
  feedLarge: articleTypePropType.isRequired,
  feedSmall: articleTypePropType.isRequired,
  isPremium: PropTypes.bool,
  onSetHomeForecast: PropTypes.func.isRequired,
  camOfTheMoment: camOfTheMomentProptype,
  forecast: forecastPropType.isRequired,
  adblock: PropTypes.bool,
  qaFlag: PropTypes.string,
  userDetails: userDetailsProptype,
  userSettings: userSettingsProptype,
  userEntitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  device: PropTypes.string.isRequired,
  onDetectedAdBlock: PropTypes.func,
  imageResizing: PropTypes.bool.isRequired,
};

FeedContentBlock.defaultProps = {
  isPremium: false,
  camOfTheMoment: null,
  adblock: false,
  qaFlag: null,
  userDetails: null,
  userSettings: null,
  onDetectedAdBlock: () => {},
};

export default FeedContentBlock;
