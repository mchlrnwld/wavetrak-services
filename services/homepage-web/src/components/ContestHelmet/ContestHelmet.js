import PropTypes from 'prop-types';
import React from 'react';
import { Helmet } from 'react-helmet';
import sos2020Image from './sos2020Image.jpg';
import sos2021Image from './sos2021Image.jpg';
import config from '../../config';

const ContestHelmet = ({ title, description, path, period, region, contestType }) => {
  const contestTitle =
    contestType === 'sos' ? 'Spirit of Surfing Cuervo Challenge' : `${title} - Surfline`;
  const contestUrl = (baseHostType = 'local') => {
    const baseHost = baseHostType === 'seo' ? config.homepageSEOUrl : config.homepageUrl;
    const contest = contestType === 'sos' ? 'spirit-of-surfing' : 'wave-of-the-winter';
    const baseUrl = `${baseHost}${config.baseContestUrl(region, period, contest)}`;
    return path ? `${baseUrl}${path}` : baseUrl;
  };
  const sosImage = period === '2021' ? sos2021Image : sos2020Image;
  const getContestImage = (platform) =>
    contestType === 'sos' ? sosImage : config.meta[platform].image;

  const meta = [
    { name: 'description', content: description },
    { property: 'og:title', content: contestTitle },
    { property: 'og:url', content: contestUrl() },
    { property: 'og:image', content: getContestImage('og') },
    { property: 'og:description', content: description },
    { property: 'twitter:title', content: contestTitle },
    { property: 'twitter:url', content: contestUrl() },
    { property: 'twitter:image', content: getContestImage('twitter') },
    { property: 'twitter:description', content: description },
  ];

  if (config.robots) {
    meta.push({
      name: 'robots',
      content: config.robots,
    });
  }

  const link = [{ rel: 'canonical', href: contestUrl('seo') }];

  return <Helmet title={contestTitle} meta={meta} link={link} />;
};

ContestHelmet.propTypes = {
  description: PropTypes.string.isRequired,
  path: PropTypes.string,
  period: PropTypes.string,
  region: PropTypes.string,
  title: PropTypes.string.isRequired,
  contestType: PropTypes.string,
};

ContestHelmet.defaultProps = {
  path: null,
  period: null,
  region: null,
  contestType: null,
};

export default ContestHelmet;
