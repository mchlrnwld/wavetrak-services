import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  currentGraphMarkerXPercentageFn as currentMarkerXPercentageFn,
  SURF,
  SWELL,
  TABLET_WIDTH,
  midnightOfDate,
} from '@surfline/web-common';
import {
  ForecastGraphsSurf,
  ForecastGraphsSurfLine,
  ForecastGraphsWind,
  ForecastGraphsTide,
  useDeviceSize,
  ForecastGraphsSwell,
  spotPropType,
} from '@surfline/quiver-react';
import { addDays } from 'date-fns';
import { isNil } from 'lodash';
import { useGraphData } from './useGraphData';
import MultiCamGraphsHeader from '../MultiCamGraphsHeader';

import './MultiCamGraphs.scss';

const BAR_GRAPH = 'BAR_GRAPH';
const AREA_GRAPH = 'AREA_GRAPH';

const getRangeDates = (utcOffset) => {
  if (isNil(utcOffset)) return utcOffset;
  const localDate = new Date(midnightOfDate(utcOffset) * 1000);
  return {
    startDate: localDate,
    endDate: addDays(localDate, 1),
  };
};

const MultiCamGraphs = ({ spotReportUrl, primarySpot }) => {
  /** STATE */
  const [surfGraphType, setSurfGraphType] = useState(BAR_GRAPH);
  const [activeSwell, setActiveSwell] = useState();
  const [surfDataType, setSurfDataType] = useState(SURF);

  /** HOOKS */
  const device = useDeviceSize();

  const intervalHours = 1; // Multi Cam Graphs have hourly data
  const days = 1; // Multi Cam Graps only have 1 day of data
  const { wind, wave, tide, sunlightTimes } = useGraphData(primarySpot?._id, days, intervalHours);

  const showSwellGraph = surfDataType === SWELL;
  const showBarGraph = surfGraphType === BAR_GRAPH;
  const showAreaGraph = surfGraphType === AREA_GRAPH;

  const now = new Date() / 1000;
  const daysEnabled = days; // We only show 1 day of data on the multi cam page
  const columns = 24 / intervalHours; // We want one column for each data point during day
  const currentMarkerXPercentage = currentMarkerXPercentageFn(sunlightTimes, now);
  const graphDays = [0];
  const currentMarkerContinuousGraphXPercentage = currentMarkerXPercentage / graphDays.length;
  const isMobile = device.width < TABLET_WIDTH;
  const isHourly = !isMobile;
  const surfGraphColumns = isHourly ? 24 : 4;
  const { startDate, endDate } = !isNil(tide?.utcOffset) ? getRangeDates(tide.utcOffset) : {};

  return (
    <section className="sl-multi-cam-graphs">
      <MultiCamGraphsHeader
        surfDataType={surfDataType}
        setSurfDataType={setSurfDataType}
        surfGraphType={surfGraphType}
        setSurfGraphType={setSurfGraphType}
        spotReportUrl={spotReportUrl}
      />
      {showBarGraph && !showSwellGraph && (
        <ForecastGraphsSurf
          wave={wave}
          device={device}
          currGraphType={surfGraphType}
          changeGraphType={() => setSurfGraphType(AREA_GRAPH)}
          days={graphDays}
          daysEnabled={daysEnabled}
          columns={surfGraphColumns}
          sunlightTimes={sunlightTimes}
          currentMarkerXPercentage={currentMarkerXPercentage}
          showTitle={false}
          isHourly={isHourly}
          isMultiCamPage
          timestamps={wave.timestamps}
        />
      )}
      {showAreaGraph && !showSwellGraph && (
        <ForecastGraphsSurfLine
          currGraphType={surfGraphType}
          changeGraphType={() => setSurfGraphType(BAR_GRAPH)}
          wave={wave}
          days={graphDays}
          daysEnabled={daysEnabled}
          columns={surfGraphColumns}
          sunlightTimes={sunlightTimes}
          mobile={device?.mobile}
          device={device}
          currentMarkerXPercentage={currentMarkerXPercentage}
          showTitle={false}
          isHourly={isHourly}
          isMultiCamPage
          timestamps={wave.timestamps}
        />
      )}
      {showSwellGraph && (
        <ForecastGraphsSwell
          type="SPOT"
          showTitle={false}
          isHourly={isHourly}
          wave={wave}
          days={graphDays}
          daysEnabled={daysEnabled}
          sunlightTimes={sunlightTimes}
          currentMarkerXPercentage={currentMarkerXPercentage}
          mobile={device?.mobile}
          device={device}
          activeSwell={activeSwell}
          setActiveSwell={setActiveSwell}
          isMultiCamPage
          timestamps={wave.timestamps}
        />
      )}
      <ForecastGraphsWind
        device={device}
        wind={wind}
        isMultiCamPage
        days={graphDays}
        daysEnabled={daysEnabled}
        columns={columns}
        sunlightTimes={sunlightTimes}
        spotId={primarySpot._id}
        spotName={primarySpot.name}
        currentMarkerXPercentage={currentMarkerXPercentage}
      />
      {tide && tide.days && tide.days.length > 1 ? (
        <ForecastGraphsTide
          device={device}
          showTideTable
          tide={tide}
          days={graphDays}
          daysEnabled={daysEnabled}
          sunlightTimes={sunlightTimes}
          currentMarkerXPercentage={currentMarkerContinuousGraphXPercentage}
          isMultiCamPage
          startDate={startDate}
          endDate={endDate}
        />
      ) : null}
    </section>
  );
};

MultiCamGraphs.propTypes = {
  spotReportUrl: PropTypes.string,
  primarySpot: spotPropType.isRequired,
};

MultiCamGraphs.defaultProps = {
  spotReportUrl: '',
};

export default MultiCamGraphs;
