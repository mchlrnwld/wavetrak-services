import useSWR from 'swr';
import {
  transformGraphDataToDays as transformDataToDays,
  transformTideGraphDataToDays as transformTideDataToDays,
} from '@surfline/web-common';
import {
  fetchWaveForecast,
  fetchTideForecast,
  fetchWindForecast,
  fetchWeatherForecast,
} from '../../common/api/kbyg';

const maxSwell = (swells) => Math.max(...swells.map((swell) => swell.height));

const customWaveMapFunc = ({ timestamp, utcOffset, surf, swells }) => ({
  timestamp,
  utcOffset,
  surf,
  swells: swells.map((swell, index) => ({
    ...swell,
    index,
  })),
});

/**
 * @description Custom hook used to fetch all the data needed for fetching the data for the graphs on
 * the multi cam spot report
 */
export const useGraphData = (spotId, days, interval) => {
  const maxHeights = false;
  const {
    data: wave,
    isValidating: waveLoading,
    error: waveError,
  } = useSWR(
    // We have to include 'wave' as the last argument so that the key `swr` uses becomes unique to this fetch
    spotId ? [spotId, days, interval, maxHeights, 'wave'] : null,
    fetchWaveForecast
  );

  const {
    data: tide,
    isValidating: tideLoading,
    error: tideError,
  } = useSWR(
    // We have to include 'tide' as the last argument so that the key `swr` uses becomes unique to this fetch
    spotId ? [spotId, days, 'tide'] : null,
    fetchTideForecast
  );
  const {
    data: wind,
    isValidating: windLoading,
    error: windError,
  } = useSWR(
    // We have to include 'wind' as the last argument so that the key `swr` uses becomes unique to this fetch
    spotId ? [spotId, days, interval, 'wind'] : null,
    fetchWindForecast
  );

  const {
    data: weather,
    isValidating: weatherLoading,
    error: weatherError,
  } = useSWR(
    // We have to include 'weather' as the last argument so that the key `swr` uses becomes unique to this fetch
    spotId ? [spotId, days, interval, 'weather'] : null,
    fetchWeatherForecast
  );

  const waveData = wave ? transformDataToDays(wave.data.wave, interval, customWaveMapFunc) : [];

  return {
    wind: {
      error: windError,
      loading: windLoading || !wind,
      ...wind?.associated,
      overallMaxSpeed: wind ? Math.max(...wind?.data.wind.map(({ speed }) => speed)) : 0,
      days: wind ? transformDataToDays(wind?.data.wind, interval) : [],
    },
    wave: {
      error: waveError,
      loading: waveLoading || !wave,
      ...wave?.associated,
      overallMaxSurfHeight: wave ? Math.max(...wave.data.wave.map(({ surf }) => surf.max)) : 0,
      overallMaxSwellHeight: wave
        ? Math.max(...wave.data.wave.map(({ swells }) => maxSwell(swells)))
        : 0,
      hourly: waveData,
      days: waveData.map((day) => day.filter((_, index) => index % 6 === 0)),
      timestamps: waveData.flat().map(({ timestamp, utcOffset }) => ({
        timestamp,
        utcOffset,
      })),
    },
    tide: {
      error: tideError,
      loading: tideLoading || !tide,
      ...tide?.associated,
      days: tide ? transformTideDataToDays(tide.data.tides) : [],
    },
    sunlightTimes: {
      days: weather?.data.sunlightTimes,
      utcOffset: weather?.associated.utcOffset,
      loading: weatherLoading || !weather,
      error: weatherError,
    },
  };
};
