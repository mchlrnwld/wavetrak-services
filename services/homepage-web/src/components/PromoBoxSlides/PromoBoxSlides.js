import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import Slider from 'react-slick';
import classNames from 'classnames';
import {
  withWindow,
  windowPropType,
  PremiumRibbonIcon,
  TrackableLink,
} from '@surfline/quiver-react';
import articlePropType from '../../propTypes/article';
import config from '../../config';

const DEFAULT_PROMO_CONFIG = {
  dots: true,
  fade: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  adaptiveHeight: true,
  autoplay: true,
  autoplaySpeed: 5000,
  pauseOnHover: false,
};

const getTitleContainerClasses = (feedSuperheader) =>
  classNames({
    'sl-promobox-slides__slide__title__container': true,
    'sl-promobox-slides__slide__title__container--superheader': feedSuperheader,
  });

const SlideLink = ({ slide, index, feedLocale, children }) => {
  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      destinationUrl: slide.permalink,
      mediaType: slide.media.type,
      contentId: `${slide.id}`,
      title: slide.content.title,
      category: 'Promo Article',
      contentType: 'Surf News',
      tags: slide.tags,
      locationCategory: 'homepage promobox',
      promoPosition: index + 1,
      feedLocale,
    }),
    [
      index,
      slide.permalink,
      slide.media.type,
      slide.id,
      slide.content.title,
      slide.tags,
      feedLocale,
    ]
  );

  return (
    <TrackableLink
      ref={linkRef}
      eventName="Clicked Home Feed Link"
      eventProperties={eventProperties}
    >
      <a
        ref={linkRef}
        href={slide.permalink}
        target={slide.newWindow ? '_blank' : '_self'}
        className="sl-promobox-slides__slide"
        rel="noreferrer"
      >
        {children}
      </a>
    </TrackableLink>
  );
};

const getSlideImgSrc = (media, imageResizing, settings = 'w=640,q=85,f=auto,fit=contain') =>
  imageResizing ? `${config.cloudflareImageResizingUrl(media, settings)}` : media;

const getSlideImgSrcSet = (media, imageResizing) => {
  const commonSettings = 'q=85,f=auto,fit=contain';
  const srcSetOpts = [
    [300, 300],
    [600, 512],
    [940, 769],
    [640, 1256],
    [784, 1712],
    [1200, 2560],
  ];
  return srcSetOpts.reduce(
    (srcSet, currSrc) =>
      `${srcSet}${getSlideImgSrc(media, imageResizing, `w=${currSrc[0]}${commonSettings}`)} ${
        currSrc[1]
      }w,`,
    ''
  );
};

const PromoBox = ({ promoBoxSlides, window, feedLocale, imageResizing }) => {
  const sliderRef = useRef();

  const promoSettings = {
    ...DEFAULT_PROMO_CONFIG,
    infinite: !!window,
  };

  const end = window ? promoBoxSlides.length : 1;
  return (
    <div className="sl-promobox-slides">
      {promoBoxSlides.length ? (
        <Slider {...promoSettings} ref={sliderRef}>
          {promoBoxSlides.slice(0, end).map((slide, index) => (
            <SlideLink key={slide.id} slide={slide} index={index} feedLocale={feedLocale}>
              {slide.premium ? <PremiumRibbonIcon /> : null}
              <div className="sl-promobox-slides__slide__img__container">
                <img
                  alt={slide.content.title}
                  src={getSlideImgSrc(slide.media.promobox1x, imageResizing)}
                  srcSet={getSlideImgSrcSet(slide.media.promobox1x, imageResizing)}
                />
              </div>
              <div className={getTitleContainerClasses(slide.feedSuperheader)}>
                {slide.feedSuperheader ? (
                  <div className="sl-promobox-slides__slide__superheader">
                    {slide.feedSuperheader}
                  </div>
                ) : null}
                <h1
                  className="sl-promobox-slides__slide__title"
                  /* eslint-disable-next-line react/no-danger */
                  dangerouslySetInnerHTML={{
                    __html: slide.content.displayTitle || slide.content.title,
                  }}
                />
              </div>
            </SlideLink>
          ))}
        </Slider>
      ) : null}
    </div>
  );
};

SlideLink.propTypes = {
  slide: articlePropType.isRequired,
  index: PropTypes.number.isRequired,
  feedLocale: PropTypes.string.isRequired,
  children: PropTypes.element,
};

SlideLink.defaultProps = {
  children: null,
};

PromoBox.propTypes = {
  promoBoxSlides: PropTypes.arrayOf(articlePropType).isRequired,
  window: windowPropType,
  feedLocale: PropTypes.string,
  imageResizing: PropTypes.bool.isRequired,
};

PromoBox.defaultProps = {
  window: null,
  feedLocale: null,
};

export default withWindow(PromoBox);
