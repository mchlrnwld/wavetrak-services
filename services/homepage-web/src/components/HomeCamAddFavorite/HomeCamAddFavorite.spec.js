import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import HomeCamAddFavorite from './HomeCamAddFavorite';

describe('components / HomeCamAddFavorite', () => {
  it('renders link to add favorites with cams', () => {
    const wrapper = shallow(<HomeCamAddFavorite />);
    const linkWrapper = wrapper.find('a');
    expect(linkWrapper).to.have.length(2);
  });
});
