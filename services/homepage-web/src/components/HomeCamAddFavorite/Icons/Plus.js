import PropTypes from 'prop-types';
import React from 'react';

const Plus = ({ className }) => (
  <svg width="50px" height="50px" viewBox="0 0 67 67" version="1.1" className={className}>
    <g id="desktop" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-687.000000, -359.000000)" strokeWidth="2">
        <g id="NS-HB" transform="translate(525.000000, 292.000000)">
          <g transform="translate(163.000000, 68.000000)">
            <g id="Group-3">
              <circle id="Oval-4" cx="32.5" cy="32.5" r="32.5" />
              <g id="Group-2" transform="translate(19.571429, 19.524990)" strokeLinecap="square">
                <path d="M0.266233766,13 L25.5909091,13" id="Line" />
                <path
                  d="M0.266233766,13 L25.5909091,13"
                  id="Line"
                  transform={`translate(12.928571, 13.000000) rotate(90.000000)
                    translate(-12.928571, -13.000000)`}
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

Plus.propTypes = {
  className: PropTypes.string,
};

Plus.defaultProps = {
  className: '',
};

export default Plus;
