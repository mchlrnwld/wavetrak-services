import PropTypes from 'prop-types';
import React from 'react';
import { Plus } from './Icons';
import { Close } from '../HomeCam/Icons';
import config from '../../config';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const HomeCamAddFavorite = ({ doToggleHomeCamVisibility }) => (
  <div className="sl-home-cam__add">
    <h6 className="sl-home-cam__add__title">Set Homecam</h6>
    <a href={`${config.addFavoritesUrl}?redirectUrl=/`}>
      <Plus className="sl-home-cam__add__plus" />
    </a>
    <p className="sl-home-cam__add__text">
      Add your favorite camera
      <a href={`${config.addFavoritesUrl}?redirectUrl=/`} className="sl-home-cam__add__text__link">
        here
      </a>
    </p>
    <div
      {...createAccessibleOnClick(doToggleHomeCamVisibility)}
      className="sl-home-cam__add__close"
    >
      <Close className="sl-home-cam__add__close-icon" />
    </div>
  </div>
);

HomeCamAddFavorite.propTypes = {
  doToggleHomeCamVisibility: PropTypes.func.isRequired,
};

export default HomeCamAddFavorite;
