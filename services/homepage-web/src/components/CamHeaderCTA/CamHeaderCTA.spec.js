/** @prettier */

import { expect } from 'chai';

import CamHeaderCTA from './CamHeaderCTA';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('components / CamHeaderCTA', () => {
  const props = {
    href: '/',
    segmentProperties: {},
  };

  beforeEach(() => {});
  afterEach(() => {});

  it('should render the collection list with all the spots in the active collection', () => {
    const { wrapper } = mountWithReduxAndRouter(CamHeaderCTA, props);

    expect(wrapper).to.have.length(1);
    expect(wrapper.props()).to.deep.equal(props);
  });
});
