import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import en from '../../intl/translations/en';
import config from '../../config';

const CamHeaderCTA = ({ href, segmentProperties }) => {
  const ref = useRef();
  return (
    <div className="sl-cam-header-cta">
      <span className="sl-cam-header-cta__text">{en.premiumCTA.CamHeaderCTA.text}</span>
      <TrackableLink
        ref={ref}
        eventName="Clicked Subscribe CTA"
        eventProperties={segmentProperties}
      >
        <a ref={ref} href={href} className="sl-cam-header-cta__link">
          {en.premiumCTA.CamHeaderCTA.linkText}
        </a>
      </TrackableLink>
    </div>
  );
};

CamHeaderCTA.defaultProps = {
  href: config.funnelUrl,
  segmentProperties: {},
};

CamHeaderCTA.propTypes = {
  href: PropTypes.string,
  segmentProperties: PropTypes.shape({}),
};

export default CamHeaderCTA;
