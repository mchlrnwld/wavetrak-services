import PropTypes from 'prop-types';
import React from 'react';
import { Helmet } from 'react-helmet';
import config from '../../config';

const meta = [
  { name: 'description', content: config.meta.description },
  { property: 'og:title', content: config.meta.og.title },
  { property: 'og:url', content: config.homepageUrl },
  { property: 'og:image', content: config.meta.og.image },
  { property: 'og:description', content: config.meta.description },
  { property: 'twitter:title', content: config.meta.og.title },
  { property: 'twitter:url', content: config.homepageUrl },
  { property: 'twitter:image', content: config.meta.twitter.image },
  { property: 'twitter:description', content: config.meta.description },
];

if (config.robots) {
  meta.push({
    name: 'robots',
    content: config.robots,
  });
}

const link = [{ rel: 'canonical', href: config.homepageSEOUrl }];

const HomepageHelmet = ({ title }) => <Helmet title={title} meta={meta} link={link} />;

HomepageHelmet.propTypes = {
  title: PropTypes.string.isRequired,
};

export default HomepageHelmet;
