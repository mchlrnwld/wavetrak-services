import React from 'react';
import {
  readingsPropType,
  spotPropType,
  reportPropType,
  ForecasterProfile,
  SpotForecastSummary,
} from '@surfline/quiver-react';

import { postedDate, getTop3Swells, getEntitlements, getUserDetails } from '@surfline/web-common';

import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import unitsPropType from '../../propTypes/units';
import config from '../../config';

import './MultiCamSpotReportModule.scss';

const MultiCamSpotReportModule = ({ spotReport }) => {
  const entitlements = useSelector(getEntitlements);
  const userDetails = useSelector(getUserDetails);

  const associated = spotReport?.associated;
  const spot = spotReport?.spot;
  const report = spotReport?.report;
  const readings = spotReport?.forecast;
  const note = readings?.note;
  const { utcOffset, abbrTimezone } = associated;
  const conditions = readings?.conditions;

  return (
    <article className="sl-multi-cam-spot-report-module">
      <SpotForecastSummary
        showWetsuitAd={false}
        entitlements={entitlements}
        units={associated.units}
        waveHeight={readings.waveHeight}
        reportExpired={readings.conditions.expired}
        tide={readings.tide}
        wind={readings.wind}
        swells={getTop3Swells(
          readings.swells.map((swell, index) => ({
            ...swell,
            index,
          }))
        )}
        lat={spot.lat}
        lon={spot.lon}
        associated={associated}
        readings={readings}
        spot={spot}
        userDetails={userDetails}
      />
      {report && (
        <ForecasterProfile
          url={report.forecaster.iconUrl}
          forecasterName={report.forecaster.name}
          forecasterTitle={report.forecaster.title}
          lastUpdate={postedDate(report.timestamp, utcOffset, abbrTimezone)}
          condition={conditions?.value}
          expired={conditions?.expired}
          urlBase={config.homepageUrl}
        />
      )}
      {report && (
        <>
          {note && (
            <aside
              className="sl-spot-report__note"
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: note }}
            />
          )}
          {/* eslint-disable-next-line react/no-danger */}
          <section dangerouslySetInnerHTML={{ __html: report.body }} />
        </>
      )}
    </article>
  );
};

MultiCamSpotReportModule.propTypes = {
  spotReport: PropTypes.shape({
    associated: PropTypes.shape({
      units: unitsPropType.isRequired,
      utcOffset: PropTypes.number,
      abbrTimezone: PropTypes.string,
    }).isRequired,
    spot: spotPropType.isRequired,
    report: reportPropType,
    forecast: readingsPropType,
  }).isRequired,
};

export default MultiCamSpotReportModule;
