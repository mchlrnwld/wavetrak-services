import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import classNames from 'classnames';
import { CollapsibleModule, TrackableLink } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import subregionPropType from '../../propTypes/subregion';
import config from '../../config';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const getSetRegionClassNames = (isLoggedIn, subregions) =>
  classNames({
    'sl-feed-forecast-header__set-region__message': true,
    'sl-feed-forecast-header__set-region__message--list': isLoggedIn && !!subregions.length,
  });

const handleSetRegion = (sub, onSetHomeForecast) => {
  onSetHomeForecast(sub._id);
  trackEvent('Clicked Set Region', {
    linkLocation: 'forecast outlook',
    category: 'homepage',
    subregionId: sub._id,
    subregionName: sub.name,
  });
};

const FeedForecastHeader = ({
  isLoggedIn,
  onSetHomeForecast,
  subregionName,
  subregions,
  mapUrl,
}) => {
  const showAddFavorites = !isLoggedIn || !subregions.length;
  const linkRef = useRef();
  const mapLinkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      linkLocation: 'forecast outlook',
      category: 'homepage',
      linkName: showAddFavorites ? 'Add favorites' : 'Favorites',
      linkUrl: showAddFavorites ? config.onboardingUrl : config.favoritesUrl,
    }),
    [showAddFavorites]
  );
  const mapEventProperties = useMemo(
    () => ({
      linkLocation: 'forecast outlook',
      category: 'homepage',
      linkName: 'View Area on Map',
      linkUrl: mapUrl,
    }),
    [mapUrl]
  );

  return (
    <div className="sl-feed-forecast-header">
      <CollapsibleModule title={`${subregionName}`}>
        <div className="sl-feed-forecast-header__set-region">
          {isLoggedIn && subregions.length ? (
            <ul className="sl-feed-forecast-header__set-region__menu">
              {subregions.map((sub) => (
                <li
                  className="sl-feed-forecast-header__set-region__menu__item"
                  key={sub._id}
                  {...createAccessibleOnClick(() => handleSetRegion(sub, onSetHomeForecast))}
                >
                  {sub.name}
                </li>
              ))}
            </ul>
          ) : null}
          <div className={getSetRegionClassNames(isLoggedIn, subregions)}>
            <p>
              {!showAddFavorites ? 'Regions above are displayed based on spots in your ' : ''}
              <TrackableLink
                ref={linkRef}
                eventName="Clicked Link"
                eventProperties={eventProperties}
              >
                <a
                  ref={linkRef}
                  href={showAddFavorites ? config.onboardingUrl : config.favoritesUrl}
                >
                  {showAddFavorites ? 'Add favorites' : 'Favorites'}
                </a>
              </TrackableLink>
              {showAddFavorites ? ' to personalize your headlines and forecasts.' : ' list.'}
            </p>
          </div>
        </div>
      </CollapsibleModule>
      <TrackableLink ref={mapLinkRef} eventName="Clicked Link" eventProperties={mapEventProperties}>
        <a ref={mapLinkRef} className="sl-feed-forecast-header__link" href={mapUrl}>
          View Area on Map
        </a>
      </TrackableLink>
    </div>
  );
};

FeedForecastHeader.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  onSetHomeForecast: PropTypes.func.isRequired,
  subregionName: PropTypes.string.isRequired,
  subregions: PropTypes.arrayOf(subregionPropType).isRequired,
  mapUrl: PropTypes.string.isRequired,
};

export default FeedForecastHeader;
