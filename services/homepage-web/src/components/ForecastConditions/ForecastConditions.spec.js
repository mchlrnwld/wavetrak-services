import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import ForecastConditions from './ForecastConditions';
import ForecastAMPM from '../ForecastAMPM';

describe('components / ForecastConditions', () => {
  const defaultProps = {
    forecastLink: 'surf-forecasts/north-shore/58581a836630e24c44878fcb',
    timestamp: 1569578400,
    am: {
      maxHeight: 0.9,
      minHeight: 0.6,
      plus: false,
      humanRelation: 'knee to waist high',
      rating: null,
    },
    pm: {
      maxHeight: 0.9,
      minHeight: 0.6,
      plus: false,
      humanRelation: 'waist to stomach high',
      rating: null,
    },
    units: {
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KPH',
    },
    utcOffset: -10,
  };

  beforeEach(() => {});
  afterEach(() => {});

  it('should render the forecast conditions', () => {
    const wrapper = mount(<ForecastConditions {...defaultProps} />);

    expect(wrapper.props()).to.deep.equal(defaultProps);

    expect(wrapper.find('.sl-forecast-conditions__day').text()).to.be.oneOf([
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday',
    ]);

    expect(wrapper.find(ForecastAMPM).at(0).props()).to.deep.equal({
      type: 'AM',
      ...defaultProps.am,
      units: defaultProps.units,
      rating: 'LOLA',
    });
    expect(wrapper.find(ForecastAMPM).at(1).props()).to.deep.equal({
      type: 'PM',
      ...defaultProps.pm,
      units: defaultProps.units,
      rating: 'LOLA',
    });
  });
});
