import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import { format } from 'date-fns';
import { getLocalDate } from '@surfline/web-common';
import { TrackableLink } from '@surfline/quiver-react';
import unitsPropType from '../../propTypes/units';
import ampmPropType from '../../propTypes/ampm';
import ForecastAMPM from '../ForecastAMPM';

/**
 *
 * @param {object} props
 * @param {string} props.forecastLink link to the specific forecast shown
 * @param {number} props.timestamp Unix timestamp in seconds (eg: 1569578400)
 * @param {object} props.am conditions report for the morning
 * @param {object} props.pm conditions report for the afternoon
 * @param {number} props.utcOffset utcoffset for converting to local time
 * @param {object} props.units users units settings
 */
const ForecastConditions = ({ forecastLink, timestamp, am, pm, units, utcOffset }) => {
  const forecastLinkRef = useRef();
  const forecastEventProperties = useMemo(
    () => ({
      linkLocation: 'forecast outlook',
      category: 'homepage',
      linkName: 'View 17-day forecast',
      linkUrl: forecastLink,
    }),
    [forecastLink]
  );
  return (
    <TrackableLink
      ref={forecastLinkRef}
      eventName="Clicked Link"
      eventProperties={forecastEventProperties}
    >
      <a ref={forecastLinkRef} href={forecastLink} className="sl-forecast-conditions">
        <div className="sl-forecast-conditions__day">
          {format(getLocalDate(timestamp, utcOffset), 'EEEE')}
        </div>
        <ForecastAMPM
          type="AM"
          minHeight={am.minHeight}
          maxHeight={am.maxHeight}
          plus={am.plus}
          rating={am.rating || 'LOLA'}
          humanRelation={am.humanRelation}
          units={units}
        />
        <ForecastAMPM
          type="PM"
          minHeight={pm.minHeight}
          maxHeight={pm.maxHeight}
          plus={pm.plus}
          rating={pm.rating || 'LOLA'}
          humanRelation={pm.humanRelation}
          units={units}
        />
      </a>
    </TrackableLink>
  );
};

ForecastConditions.propTypes = {
  forecastLink: PropTypes.string.isRequired,
  timestamp: PropTypes.number.isRequired,
  am: ampmPropType.isRequired,
  pm: ampmPropType.isRequired,
  units: unitsPropType.isRequired,
  utcOffset: PropTypes.number.isRequired,
};

export default ForecastConditions;
