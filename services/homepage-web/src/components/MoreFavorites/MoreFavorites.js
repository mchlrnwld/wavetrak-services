import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import createAccessibleOnClick, { BTN } from '../../utils/createAccessibleOnClick';
import config from '../../config';

const menuClass = (opened) =>
  classNames({
    'sl-more-favorites__menu': true,
    'sl-more-favorites__menu--opened': opened,
  });

const MoreFavorites = ({ onClickButton, opened }) => (
  <div className="sl-collection-more-favorites" onMouseLeave={() => onClickButton(false)}>
    <div
      type="button"
      className="sl-collection-more-favorites__button"
      {...(!!onClickButton && {
        ...createAccessibleOnClick(() => onClickButton(!opened), BTN, false),
      })}
    >
      <svg version="1.1" height="18px" width="18px" viewBox="0 0 18 18">
        <circle fill="#333333" cx="9" cy="3" r="2" />
        <circle fill="#333333" cx="9" cy="9" r="2" />
        <circle fill="#333333" cx="9" cy="15" r="2" />
      </svg>
    </div>
    <ul className={menuClass(opened)}>
      <li className="sl-collection-more-favorites__link">
        <a
          href={`${config.homepageUrl}setup/favorite-surf-spots/add`}
          type="button"
          className="sl-collection-more-favorites-menu__button"
        >
          Add Favorites
        </a>
      </li>
      <li className="sl-collection-more-favorites__link">
        <a
          href={`${config.homepageUrl}account/favorites`}
          type="button"
          className="sl-collection-more-favorites-menu__button"
        >
          Edit Favorites
        </a>
      </li>
    </ul>
  </div>
);

MoreFavorites.propTypes = {
  onClickButton: PropTypes.func.isRequired,
  opened: PropTypes.bool,
};

MoreFavorites.defaultProps = {
  opened: false,
};

export default MoreFavorites;
