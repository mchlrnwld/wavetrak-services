import PropTypes from 'prop-types';
import React from 'react';
import HomeCamSelect from '../HomeCamSelect';
import HomeCamPlayer from '../HomeCamPlayer';
import browserDataPropShape from '../../propTypes/browserData';
import homeCamPropType from '../../propTypes/homecam';
import userFavoritesPropType from '../../propTypes/userFavorites';
import userSettingsPropType from '../../propTypes/userSettings';

const HomeCamPremium = ({
  homecam,
  userFavorites,
  userSettings,
  doSelectHomeCamOption,
  doToggleHomeCamSelectView,
  doUpdateHomeCam,
  doToggleHomeCamVisibility,
  browserData,
}) => (
  <div>
    {homecam.displaySelectView ? (
      <HomeCamSelect
        doSelectHomeCamOption={doSelectHomeCamOption}
        doToggleHomeCamSelectView={doToggleHomeCamSelectView}
        doUpdateHomeCam={doUpdateHomeCam}
        userFavorites={userFavorites}
        homecam={homecam}
      />
    ) : (
      <HomeCamPlayer
        doToggleHomeCamSelectView={doToggleHomeCamSelectView}
        doToggleHomeCamVisibility={doToggleHomeCamVisibility}
        homecam={homecam}
        userSettings={userSettings}
        browserData={browserData}
      />
    )}
  </div>
);

HomeCamPremium.propTypes = {
  homecam: homeCamPropType.isRequired,
  userFavorites: userFavoritesPropType.isRequired,
  userSettings: userSettingsPropType.isRequired,
  doSelectHomeCamOption: PropTypes.func.isRequired,
  doToggleHomeCamSelectView: PropTypes.func.isRequired,
  doToggleHomeCamVisibility: PropTypes.func.isRequired,
  doUpdateHomeCam: PropTypes.func.isRequired,
  browserData: browserDataPropShape.isRequired,
};

export default HomeCamPremium;
