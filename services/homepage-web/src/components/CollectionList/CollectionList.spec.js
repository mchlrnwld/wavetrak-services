/** @prettier */

import { expect } from 'chai';

import CollectionList from './CollectionList';
import CollectionItem from '../CollectionItem';
import CollectionCategory from '../CollectionCategory';

import collectionsFixture from '../../utils/fixtures/collections';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { SUBREGIONS, COLLECTIONS } from '../../actions/multiCam';

describe('components / CollectionList', () => {
  const controls = {
    start: () => null,
    set: () => null,
    stop: () => null,
    subscribe: () => null,
  };
  const defaultProps = {
    collections: collectionsFixture,
    collectionsLoading: false,
    spinnerControls: controls,
    leftRailControls: controls,
  };

  const initialState = {
    multiCam: {
      active: collectionsFixture[0]._id,
      collections: collectionsFixture,
      collectionsLoading: false,
      collectionTypes: [
        { name: 'Collections', type: COLLECTIONS, rank: 1 },
        { name: 'Favorite Regions', type: SUBREGIONS, rank: 2 },
      ],
    },
  };

  beforeEach(() => {
    // Stubbing
  });
  afterEach(() => {
    // Stubbing
  });

  it('should render the collection list with all the collections', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionList, props, {
      initialState,
    });

    const collectionsWrappers = wrapper.find(CollectionItem);

    expect(collectionsWrappers).to.have.length(collectionsFixture.length);
    wrapper.unmount();
  });

  it('should sort the collections by type', () => {
    const props = {
      ...defaultProps,
    };
    const { wrapper } = mountWithReduxAndRouter(CollectionList, props, {
      initialState,
    });

    const { collectionTypes } = initialState.multiCam;
    const collectionCategoriesWrappers = wrapper.find(CollectionCategory);

    expect(collectionCategoriesWrappers).to.have.length(collectionTypes.length);

    const sortedTypes = collectionTypes.sort((a, b) => a.rank > b.rank);

    sortedTypes.forEach((type, index) => {
      expect(collectionCategoriesWrappers.at(index).prop('rank')).to.be.equal(type.rank);
    });
    wrapper.unmount();
  });
});
