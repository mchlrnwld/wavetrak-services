/** @prettier */

/**
 *
 * @param {[{
 *  name: String,
 *  type: String,
 *  rank: Number,
 * }]} collectionTypes
 * @param {[{
 *  name: String,
 *  _id: String,
 *  type: String,
 *  loading: Boolean,
 *  fetchedSpots: Boolean,
 *  spots: [{}],
 * }]} collections
 * @returns {Function} function to be used by `useMemo`
 */
export const createCollectionCategories = (collectionTypes, collections) => () =>
  collectionTypes
    .sort((a, b) => a.rank > b.rank)
    .map((collectionType) => ({
      ...collectionType,
      collections: collections.filter((collection) => collection.type === collectionType.type),
    }));
