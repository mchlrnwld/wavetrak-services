export const categoryVariants = {
  hidden: { x: -200 },
  shown: {
    x: 0,
    transition: {
      type: 'tween',
      duration: 0.1,
    },
  },
};

export const spinnerVariant = {
  initial: {
    x: '-50%',
  },
  hidden: {
    x: 120,
    transition: { duration: 0.15 },
  },
};

export const containerVariants = {
  hidden: {
    opacity: 1,
  },
  shown: {
    opacity: 1,
    transition: {
      delayChildren: 0.01,
      staggerChildren: 0.06,
    },
  },
};
