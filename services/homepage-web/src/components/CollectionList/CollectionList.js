/** @prettier */

import React, { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Spinner, Chevron, useTimeout } from '@surfline/quiver-react';
import { motion } from 'framer-motion';

import { spinnerVariant, containerVariants } from './variants';
import CollectionCategory from '../CollectionCategory';
import { animationControlsPropType } from '../../propTypes/animation';
import {
  getCollectionTypes,
  getCollections,
  getCollectionsLoading,
} from '../../selectors/multiCam';
import { createCollectionCategories } from './createCollectionCategories';
import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

const getRailClassname = (open) =>
  classNames({
    'sl-collection-rail__left': true,
    'sl-collection-rail__left--opened': open,
  });

const getSpinnerClassname = (open) =>
  classNames({
    'sl-multi-cam-spinner': true,
    'sl-multi-cam-spinner--hidden': !open,
  });

const CollectionList = React.memo(
  ({ spinnerControls, leftRailControls, leftRailIsOpen, toggleLeftRail }) => {
    const collections = useSelector(getCollections);
    const collectionTypes = useSelector(getCollectionTypes);
    const collectionsLoading = useSelector(getCollectionsLoading);
    const [spinnerAnimating, setSpinnerAnimating] = useState(false);

    // This useTimeout call is meant to remove the spinner as it animates to the right side of the left page rail
    const [animating] = useTimeout({
      timeoutLength: 150,
      initialState: false,
      triggers: [spinnerAnimating],
    });

    // Memoize this value so that we don't need to perform the sort and map if props don't change
    // Disabling the lint rule so that we can keep the `createCollectionCategories` function from
    // being inline
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const collectionCategories = useMemo(createCollectionCategories(collectionTypes, collections), [
      collectionTypes,
      collections,
    ]);

    return (
      <div className={getRailClassname(leftRailIsOpen)}>
        <div
          className="sl-collection-selector-chevron"
          {...createAccessibleOnClick(toggleLeftRail)}
        >
          <Chevron direction={leftRailIsOpen ? 'left' : 'right'} />
        </div>
        <motion.div
          inherit={false}
          variants={containerVariants}
          initial="hidden"
          animate={leftRailControls}
          className="sl-collection-selector"
        >
          {!collectionsLoading &&
            collectionCategories.map((category) => (
              <CollectionCategory key={category.name} rank={category.rank} category={category} />
            ))}
        </motion.div>
        <motion.div
          inherit={false}
          style={{ display: animating ? 'none' : '' }}
          className={getSpinnerClassname(leftRailIsOpen)}
          variants={spinnerVariant}
          initial="initial"
          animate={spinnerControls}
          onAnimationStart={() => setSpinnerAnimating(true)}
        >
          <Spinner />
        </motion.div>
      </div>
    );
  }
);

CollectionList.propTypes = {
  spinnerControls: animationControlsPropType.isRequired,
  leftRailControls: animationControlsPropType.isRequired,
  leftRailIsOpen: PropTypes.bool.isRequired,
  toggleLeftRail: PropTypes.func.isRequired,
};

CollectionList.defaultProps = {};

export default CollectionList;
