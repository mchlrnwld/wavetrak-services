import PropTypes from 'prop-types';
import React from 'react';

const PopularLinksList = ({ title, list }) => (
  <div>
    <h5 className="sl-popular-links-list__title">Top {title}</h5>
    <ul className="sl-popular-links-list__list">
      {list.map((item, index) => (
        <li className="sl-popular-links-list__item" key={item.title}>
          <a className="sl-popular-links-list__link" href={item.path}>
            {item.title}
          </a>
          {index !== list.length - 1 ? ', ' : ''}
        </li>
      ))}
    </ul>
  </div>
);

PopularLinksList.propTypes = {
  title: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string,
      title: PropTypes.string,
    })
  ).isRequired,
};

export default PopularLinksList;
