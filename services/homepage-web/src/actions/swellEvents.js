import { fetchAllSwellEvents } from '../common/api/feed';
import getFeedGeotarget from '../utils/getFeedGeotarget';

export const FETCH_SWELL_EVENTS = 'FETCH_SWELL_EVENTS';
export const FETCH_SWELL_EVENTS_SUCCESS = 'FETCH_SWELL_EVENTS_SUCCESS';
export const FETCH_SWELL_EVENTS_FAILURE = 'FETCH_SWELL_EVENTS_FAILURE';

export const fetchSwellEvents = () => async (dispatch, getState) => {
  dispatch({
    type: FETCH_SWELL_EVENTS,
  });

  try {
    const state = getState();
    const geotarget = getFeedGeotarget(state);
    const { events } = await fetchAllSwellEvents(geotarget);
    dispatch({
      type: FETCH_SWELL_EVENTS_SUCCESS,
      events,
    });
  } catch (error) {
    dispatch({
      type: FETCH_SWELL_EVENTS_FAILURE,
      error,
    });
  }
};
