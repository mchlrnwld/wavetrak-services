import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  FETCH_BROWSER_DATA_SUCCESS,
  FETCH_BROWSER_DATA_FAILURE,
  FETCH_BROWSER_DATA,
  fetchBrowserData,
} from './browser';

describe('actions / browser', () => {
  let mockStore;
  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchBrowserData', () => {
    it('dispatches FETCH_BROWSER_DATA_SUCCESS with fetchBrowserData', () => {
      const headers = {
        'user-agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
      };
      const browserData = {
        name: 'Chrome',
        version: 59,
      };
      const store = mockStore({});
      store.dispatch(fetchBrowserData(headers));
      expect(store.getActions()).to.deep.equal([
        {
          type: FETCH_BROWSER_DATA,
        },
        {
          type: FETCH_BROWSER_DATA_SUCCESS,
          browserData,
        },
      ]);
    });

    it('dispatches FETCH_BROWSER_DATA_FAILURE with fetchBrowserData', () => {
      const headers = {
        'user-agent': '',
      };
      const store = mockStore({});
      store.dispatch(fetchBrowserData(headers));
      expect(store.getActions()[1].type).to.equal(FETCH_BROWSER_DATA_FAILURE);
    });
  });
});
