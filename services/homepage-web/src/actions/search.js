import * as searchAPI from '../common/api/search';

export const FETCH_SEARCH_RESULTS = 'FETCH_SEARCH_RESULTS';
export const FETCH_SEARCH_RESULTS_SUCCESS = 'FETCH_SEARCH_RESULTS_SUCCESS';
export const FETCH_SEARCH_RESULTS_FAILURE = 'FETCH_SEARCH_RESULTS_FAILURE';

export const SHOW_ALL_TOGGLE = 'SHOW_ALL_TOGGLE';

export const fetchSearchResults = (term, cookies) => async (dispatch) => {
  dispatch({
    type: FETCH_SEARCH_RESULTS,
  });
  try {
    const results = await searchAPI.fetchSearchResults(term, cookies);
    dispatch({
      type: FETCH_SEARCH_RESULTS_SUCCESS,
      results,
    });
  } catch (error) {
    dispatch({
      type: FETCH_SEARCH_RESULTS_FAILURE,
      error,
    });
  }
};

export const showAllToggle = (section) => async (dispatch) => {
  dispatch({
    type: SHOW_ALL_TOGGLE,
    section,
  });
};
