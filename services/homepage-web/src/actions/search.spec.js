import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as searchAPI from '../common/api/search';
import { searchResults as results } from '../common/api/fixtures';
import {
  FETCH_SEARCH_RESULTS,
  FETCH_SEARCH_RESULTS_SUCCESS,
  FETCH_SEARCH_RESULTS_FAILURE,
  fetchSearchResults,
} from './search';

describe('actions / search', () => {
  let mockStore;
  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchSearchResults', () => {
    beforeEach(() => {
      sinon.stub(searchAPI, 'fetchSearchResults');
    });

    afterEach(() => {
      searchAPI.fetchSearchResults.restore();
    });

    it('dispatches FETCH_SEARCH_RESULTS_SUCCESS with results', async () => {
      const store = mockStore({
        search: {
          results: {},
        },
      });
      searchAPI.fetchSearchResults.resolves(results);

      const expectedActions = [
        {
          type: FETCH_SEARCH_RESULTS,
        },
        {
          type: FETCH_SEARCH_RESULTS_SUCCESS,
          results,
        },
      ];
      await store.dispatch(fetchSearchResults());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(searchAPI.fetchSearchResults).to.have.been.calledOnce();
    });

    it('dispatches FETCH_SEARCH_RESULTS_FAILURE on error', async () => {
      const store = mockStore({
        search: {
          results: [],
        },
      });
      searchAPI.fetchSearchResults.rejects({
        message: 'Some error occurred',
        status: 500,
      });

      const expectedActions = [
        {
          type: FETCH_SEARCH_RESULTS,
        },
        {
          type: FETCH_SEARCH_RESULTS_FAILURE,
          error: {
            message: 'Some error occurred',
            status: 500,
          },
        },
      ];
      await store.dispatch(fetchSearchResults());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(searchAPI.fetchSearchResults).to.have.been.calledOnce();
    });
  });
});
