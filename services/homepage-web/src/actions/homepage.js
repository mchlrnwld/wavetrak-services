import { trackEvent } from '@surfline/web-common';
import * as feedAPI from '../common/api/feed';
import preserveNextStart from '../utils/preserveNextStart';
import getFeedGeotarget from '../utils/getFeedGeotarget';

export const FETCH_PROMOBOX_SLIDES = 'FETCH_PROMOBOX_SLIDES';
export const FETCH_PROMOBOX_SLIDES_SUCCESS = 'FETCH_PROMOBOX_SLIDES_SUCCESS';
export const FETCH_PROMOBOX_SLIDES_FAILURE = 'FETCH_PROMOBOX_SLIDES_FAILURE';

export const FETCH_FEED = 'FETCH_FEED';
export const FETCH_FEED_SUCCESS = 'FETCH_FEED_SUCCESS';
export const FETCH_FEED_FAILURE = 'FETCH_FEED_FAILURE';

export const ENABLE_INFINITE_SCROLLING = 'ENABLE_INFINITE_SCROLLING';

export const startInfiniteScrolling = () => ({
  type: ENABLE_INFINITE_SCROLLING,
});

export const fetchPromoBoxSlides = () => async (dispatch, getState) => {
  dispatch({
    type: FETCH_PROMOBOX_SLIDES,
  });

  try {
    const state = getState();
    const geotarget = getFeedGeotarget(state);
    const payload = await feedAPI.fetchPromoBoxSlides(10, geotarget);
    const slides = payload?.data?.promobox || [];
    let cards = [];
    if (slides.length > 3) {
      cards = [...slides.splice(-3)];
    }

    dispatch({
      type: FETCH_PROMOBOX_SLIDES_SUCCESS,
      cards,
      slides,
    });
  } catch (error) {
    dispatch({
      type: FETCH_PROMOBOX_SLIDES_FAILURE,
      error,
    });
  }
};

export const fetchFeed = () => async (dispatch, getState) => {
  dispatch({
    type: FETCH_FEED,
  });

  try {
    const state = getState();
    const geotarget = getFeedGeotarget(state);
    const { curated, feedLarge, feedSmall, pageCount } = state.homepage.feed;
    const curatedLimit = 9;
    const feedLargeLimit = 3;
    const feedSmallLimit = 2;
    let articles = await feedAPI.fetchFeedArticles(
      curated.nextStart,
      feedLarge.nextStart,
      feedSmall.nextStart,
      curatedLimit,
      feedLargeLimit,
      feedSmallLimit,
      geotarget
    );
    articles = preserveNextStart(
      articles,
      curated.nextStart,
      feedLarge.nextStart,
      feedSmall.nextStart
    );

    // Only send track call if infinite scroll is enabled
    if (pageCount > 0) {
      trackEvent('Scrolled Feed', {
        category: 'Home Page Feed',
        pageCounter: pageCount,
      });
    }

    dispatch({
      type: FETCH_FEED_SUCCESS,
      articles,
    });
  } catch (error) {
    dispatch({
      type: FETCH_FEED_FAILURE,
      error,
    });
  }
};
