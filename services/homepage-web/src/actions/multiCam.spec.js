/** @prettier */

import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import {
  SET_ACTIVE_COLLECTION,
  setActiveCollection,
  FETCH_COLLECTIONS_SUCCESS,
  FETCH_COLLECTIONS,
  fetchCollections,
  SET_SELECTED_SPOTS,
  setSelectedSpots,
  changePrimarySpotIndex,
  CHANGE_PRIMARY_SPOT_INDEX,
  CHANGE_PRIMARY_SPOT,
  changePrimarySpot,
  swapPrimarySpot,
  SET_NUM_CAMS,
  setNumCams,
  SWAP_PRIMARY_SPOT,
  FETCH_COLLECTION_SPOTS,
  FETCH_COLLECTION_SPOTS_SUCCESS,
  SUBREGIONS,
  COLLECTION_TYPES,
  COLLECTIONS,
} from './multiCam';
import * as spotApi from '../common/api/spot';
import * as multiCamLocalStorage from '../utils/multiCamLocalStorage';

describe('actions / multiCam', () => {
  let mockStore;
  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('setActiveCollection', () => {
    beforeEach(() => {
      sinon.stub(spotApi, 'fetchNearbySpots');
    });

    afterEach(() => {
      spotApi.fetchNearbySpots.restore();
    });

    it('dispatches SET_ACTIVE_COLLECTION with collection', async () => {
      const store = mockStore({
        multiCam: {
          active: null,
          collections: [],
          selectedSpots: [],
        },
      });
      const collection = {
        name: 'TEST COLLECTION',
        fetchedSpots: true,
        _id: '1',
      };

      const expectedActions = [
        {
          type: SET_ACTIVE_COLLECTION,
          active: collection._id,
        },
      ];
      await store.dispatch(setActiveCollection(collection));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('fetches collection sots and dispatches SET_ACTIVE_COLLECTION', async () => {
      const spots = [1];
      spotApi.fetchNearbySpots.resolves({ data: { spots } });
      const collection = {
        name: 'TEST COLLECTION',
        fetchedSpots: false,
        _id: '1',
        primarySpot: 'primarySpot',
        type: SUBREGIONS,
      };
      const store = mockStore({
        multiCam: {
          active: null,
          collections: [collection],
          selectedSpots: [],
        },
      });

      const expectedActions = [
        {
          type: FETCH_COLLECTION_SPOTS,
          collectionIndex: 0,
        },
        {
          type: SET_ACTIVE_COLLECTION,
          active: collection._id,
        },
        {
          type: FETCH_COLLECTION_SPOTS_SUCCESS,
          spots,
          collectionIndex: 0,
        },
      ];
      await store.dispatch(setActiveCollection(collection));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('fetchCollections', () => {
    beforeEach(() => {
      sinon.stub(spotApi, 'fetchFavoriteSubregions');
    });

    afterEach(() => {
      spotApi.fetchFavoriteSubregions.restore();
    });

    it('dispatches FETCH_COLLECTIONS_SUCCESS with collections and default active collection', async () => {
      const subregions = [{ _id: '2 ' }];
      spotApi.fetchFavoriteSubregions.resolves({ subregions });
      const store = mockStore({
        multiCam: {
          active: null,
          collections: [],
          selectedSpots: [],
        },
      });

      const favorites = [{ cams: [{ name: 'HB Southside' }] }];
      const allFavorites = {
        _id: 'all-favorites',
        name: 'All Favorites',
        loading: false,
        type: COLLECTIONS,
        error: false,
        fetchedSpots: true,
        spots: favorites,
      };

      const expectedCollections = [
        allFavorites,
        {
          ...subregions[0],
          type: SUBREGIONS,
          loading: false,
          error: false,
          fetchedSpots: false,
          spots: [],
        },
      ];

      const expectedActions = [
        {
          type: FETCH_COLLECTIONS,
          collectionTypes: COLLECTION_TYPES,
        },
        {
          type: FETCH_COLLECTIONS_SUCCESS,
          active: 'all-favorites',
          collections: expectedCollections,
        },
      ];
      await store.dispatch(fetchCollections(favorites));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('setSelectedSpots', () => {
    beforeEach(() => {
      // Insert stubs here
    });

    afterEach(() => {
      // Restore stubs here
    });

    it('dispatches SET_SELECTED_SPOTS with up to the first 9 cams', () => {
      const numCams = 2;
      const store = mockStore({
        multiCam: {
          active: null,
          collections: [],
          selectedSpots: [],
          numCams,
        },
      });

      const collection = {
        name: 'All Favorites',
        spots: [
          { cams: [{ name: 'HB Southside' }] },
          { cams: [{ name: 'HB Northside' }] },
          { cams: [{ name: 'HB Midside' }] },
          { cams: [{ name: 'HB Bottomside' }] },
          { cams: [{ name: 'HB Southside' }] },
          { cams: [{ name: 'HB Northside' }] },
          { cams: [{ name: 'HB Midside' }] },
          { cams: [{ name: 'HB Bottomside' }] },
          { cams: [{ name: 'HB Midside' }] },
          { cams: [{ name: 'HB Bottomside' }] },
        ],
      };
      const expectedSelectedSpots = [
        { cams: [{ name: 'HB Southside' }] },
        { cams: [{ name: 'HB Northside' }] },
        { cams: [{ name: 'HB Midside' }] },
        { cams: [{ name: 'HB Bottomside' }] },
        { cams: [{ name: 'HB Southside' }] },
        { cams: [{ name: 'HB Northside' }] },
        { cams: [{ name: 'HB Midside' }] },
        { cams: [{ name: 'HB Bottomside' }] },
        { cams: [{ name: 'HB Midside' }] },
      ];

      const expectedActions = [
        {
          type: SET_SELECTED_SPOTS,
          selectedSpots: expectedSelectedSpots,
          numCams: undefined,
        },
      ];
      store.dispatch(setSelectedSpots(collection));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
    it('dispatches SET_SELECTED_SPOTS with spots saved in local storage and preserves order', () => {
      const numCams = 2;
      const store = mockStore({
        multiCam: {
          active: null,
          collections: [],
          selectedSpots: [],
          numCams,
        },
      });

      const collection = {
        name: 'All Favorites',
        _id: 'all-favorites',
        spots: [
          { _id: '1', cams: [{ name: 'HB Southside' }] },
          { _id: '2', cams: [{ name: 'HB Northside' }] },
          { _id: '3', cams: [{ name: 'HB Midside' }] },
          { _id: '4', cams: [{ name: 'HB Bottomside' }] },
          { _id: '5', cams: [{ name: 'HB Southside' }] },
          { _id: '6', cams: [{ name: 'HB Northside' }] },
          { _id: '7', cams: [{ name: 'HB Midside' }] },
          { _id: '8', cams: [{ name: 'HB Bottomside' }] },
          { _id: '9', cams: [{ name: 'HB Midside' }] },
          { _id: '10', cams: [{ name: 'HB Bottomside' }] },
        ],
      };
      const expectedSelectedSpots = [
        { _id: '2', cams: [{ name: 'HB Northside' }] },
        { _id: '1', cams: [{ name: 'HB Southside' }] },
      ];

      const expectedActions = [
        {
          type: SET_SELECTED_SPOTS,
          selectedSpots: expectedSelectedSpots,
          numCams: 9,
        },
      ];
      sinon.stub(multiCamLocalStorage, 'getMultiCamPreferences').returns({
        'all-favorites': ['2', '1'],
        numCams: {
          'all-favorites': 9,
        },
      });

      store.dispatch(setSelectedSpots(collection));
      expect(store.getActions()).to.deep.equal(expectedActions);

      multiCamLocalStorage.getMultiCamPreferences.restore();
    });
  });

  describe('changePrimarySpotIndex', () => {
    beforeEach(() => {});

    afterEach(() => {});

    it('dispatches CHANGE_PRIMARY_SPOT_INDEX', async () => {
      const store = mockStore({
        multiCam: {
          primarySpotIndex: 0,
        },
      });

      const expectedActions = [
        {
          type: CHANGE_PRIMARY_SPOT_INDEX,
          newIndex: 1,
        },
      ];
      await store.dispatch(changePrimarySpotIndex(1));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('changePrimarySpot', () => {
    it('dispatches CHANGE_PRIMARY_SPOT', async () => {
      const store = mockStore({
        multiCam: {
          selectedSpots: [
            {
              test: 0,
              cameras: [],
            },
          ],
          primarySpotIndex: 0,
        },
      });

      const expectedActions = [
        {
          type: CHANGE_PRIMARY_SPOT,
          newSpot: { test: 1, cameras: [] },
        },
      ];
      await store.dispatch(changePrimarySpot({ test: 1, cameras: [] }));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('swapPrimarySpot', () => {
    it('dispatches SWAP_PRIMARY_SPOT', async () => {
      const store = mockStore({
        multiCam: {
          selectedSpots: [
            {
              test: 0,
            },
            {
              test: 1,
            },
          ],
          primarySpotIndex: 0,
        },
      });

      const expectedActions = [
        {
          type: SWAP_PRIMARY_SPOT,
          newSpotIndex: 1,
        },
      ];
      await store.dispatch(swapPrimarySpot(1));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('setNumCams', () => {
    it('dispatches SET_NUM_CAMS', async () => {
      const store = mockStore({
        multiCam: {
          numCams: 4,
        },
      });

      const expectedActions = [
        {
          type: SET_NUM_CAMS,
          numCams: 1,
        },
      ];
      await store.dispatch(setNumCams(1));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });
});
