import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import * as userApi from '../common/api/user';
import {
  FETCH_HOME_CAM,
  FETCH_HOME_CAM_SUCCESS,
  SELECT_HOME_CAM_OPTION,
  TOGGLE_HOME_CAM_SELECT_VIEW,
  TOGGLE_HOME_CAM_VISIBILITY,
  UPDATE_HOME_CAM,
  UPDATE_HOME_CAM_SUCCESS,
  UPDATE_HOME_CAM_FAILURE,
  setHomeCam,
  selectHomeCamOption,
  toggleHomeCamSelectView,
  toggleHomeCamVisibility,
  updateHomeCam,
} from './homecam';

describe('actions / homecam', () => {
  describe('setHomeCam', () => {
    let mockStore;

    before(() => {
      mockStore = configureStore([thunk]);
    });

    it('dispatches SET_HOME_CAM with null when no camera', async () => {
      const store = mockStore({
        backplane: {
          user: {
            entitlements: [],
            favorites: [],
            settings: [],
          },
        },
      });
      const expectedActions = [
        {
          type: FETCH_HOME_CAM,
        },
        {
          type: FETCH_HOME_CAM_SUCCESS,
          camera: null,
          spot: null,
        },
      ];
      await store.dispatch(setHomeCam());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('dispatches SET_HOME_CAM with null when not premium', async () => {
      const store = mockStore({
        backplane: {
          user: {
            entitlements: [],
            favorites: [],
            settings: [],
          },
        },
      });
      const camera = {
        streamUrl: 'http://stream.com',
        stillUrl: 'http://still.com',
        status: {
          isDown: false,
          message: '',
        },
      };
      const expectedActions = [
        {
          type: FETCH_HOME_CAM,
        },
        {
          type: FETCH_HOME_CAM_SUCCESS,
          camera: null,
          spot: null,
        },
      ];
      await store.dispatch(setHomeCam(camera));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('dispatches SET_HOME_CAM with favorites and premium', async () => {
      const store = mockStore({
        backplane: {
          user: {
            entitlements: ['sl_premium'],
            favorites: [
              {
                _id: '123',
                name: '54th Street',
                conditions: {
                  human: false,
                  value: 'NONE',
                },
                waveHeight: {
                  human: false,
                  min: 3,
                  max: 4,
                  occassional: 5,
                  humanRelation: 'GOOD',
                  plus: false,
                },
                cameras: [
                  {
                    streamUrl: 'http://stream.com',
                    stillUrl: 'http://still.com',
                    status: {
                      isDown: false,
                      message: '',
                    },
                  },
                ],
              },
            ],
            settings: [],
          },
        },
      });
      const expectedActions = [
        {
          type: FETCH_HOME_CAM,
        },
        {
          type: FETCH_HOME_CAM_SUCCESS,
          camera: {
            streamUrl: 'http://stream.com',
            stillUrl: 'http://still.com',
            status: {
              isDown: false,
              message: '',
            },
          },
          spot: {
            _id: '123',
            name: '54th Street',
            conditions: {
              human: false,
              value: 'NONE',
            },
            waveHeight: {
              human: false,
              min: 3,
              max: 4,
              occassional: 5,
              humanRelation: 'GOOD',
              plus: false,
            },
            cameras: [
              {
                streamUrl: 'http://stream.com',
                stillUrl: 'http://still.com',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
          },
        },
      ];
      await store.dispatch(setHomeCam());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('dispatches SET_HOME_CAM with homecam settings and premium', async () => {
      const store = mockStore({
        backplane: {
          user: {
            entitlements: ['sl_premium'],
            favorites: [
              {
                _id: '123',
                name: '54th Street',
                conditions: {
                  human: false,
                  value: 'NONE',
                },
                waveHeight: {
                  human: false,
                  min: 3,
                  max: 4,
                  occassional: 5,
                  humanRelation: 'GOOD',
                  plus: false,
                },
                cameras: [
                  {
                    _id: '12345',
                    streamUrl: 'http://stream.com',
                    stillUrl: 'http://still.com',
                    status: {
                      isDown: false,
                      message: '',
                    },
                  },
                ],
              },
            ],
            settings: { homecam: '12345' },
          },
        },
      });

      const expectedActions = [
        {
          type: FETCH_HOME_CAM,
        },
        {
          type: FETCH_HOME_CAM_SUCCESS,
          camera: {
            _id: '12345',
            streamUrl: 'http://stream.com',
            stillUrl: 'http://still.com',
            status: {
              isDown: false,
              message: '',
            },
          },
          spot: {
            _id: '123',
            name: '54th Street',
            conditions: {
              human: false,
              value: 'NONE',
            },
            waveHeight: {
              human: false,
              min: 3,
              max: 4,
              occassional: 5,
              humanRelation: 'GOOD',
              plus: false,
            },
            cameras: [
              {
                _id: '12345',
                streamUrl: 'http://stream.com',
                stillUrl: 'http://still.com',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
          },
        },
      ];
      await store.dispatch(setHomeCam());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('dispatches SET_HOME_CAM with camera passed in and premium', async () => {
      const store = mockStore({
        backplane: {
          user: {
            entitlements: ['sl_premium'],
            favorites: [
              {
                _id: '123',
                name: '54th Street',
                conditions: {
                  human: false,
                  value: 'NONE',
                },
                waveHeight: {
                  human: false,
                  min: 3,
                  max: 4,
                  occassional: 5,
                  humanRelation: 'GOOD',
                  plus: false,
                },
                cameras: [
                  {
                    _id: '12345',
                    streamUrl: 'http://stream.com',
                    stillUrl: 'http://still.com',
                    status: {
                      isDown: false,
                      message: '',
                    },
                  },
                ],
              },
            ],
            settings: null,
          },
        },
      });

      const expectedActions = [
        {
          type: FETCH_HOME_CAM,
        },
        {
          type: FETCH_HOME_CAM_SUCCESS,
          camera: {
            _id: '12345',
            streamUrl: 'http://stream.com',
            stillUrl: 'http://still.com',
            status: {
              isDown: false,
              message: '',
            },
          },
          spot: {
            _id: '123',
            name: '54th Street',
            conditions: {
              human: false,
              value: 'NONE',
            },
            waveHeight: {
              human: false,
              min: 3,
              max: 4,
              occassional: 5,
              humanRelation: 'GOOD',
              plus: false,
            },
            cameras: [
              {
                _id: '12345',
                streamUrl: 'http://stream.com',
                stillUrl: 'http://still.com',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
          },
        },
      ];
      await store.dispatch(setHomeCam('12345'));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('toggleHomeCamVisibility', () => {
    let mockStore;

    before(() => {
      mockStore = configureStore();
    });

    it('dispatches SELECT_HOME_CAM_OPTION with value', async () => {
      const store = mockStore({});
      const expectedActions = [
        {
          type: SELECT_HOME_CAM_OPTION,
          currentSelection: '12345',
        },
      ];
      await store.dispatch(selectHomeCamOption('12345'));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('toggleHomeCamSelectView', () => {
    let mockStore;

    before(() => {
      mockStore = configureStore([thunk]);
    });

    it('dispatches TOGGLE_HOME_CAM_SELECT_VIEW with segment event', async () => {
      const store = mockStore({ homecam: { displaySelectView: true } });
      const expectedActions = [
        {
          type: TOGGLE_HOME_CAM_SELECT_VIEW,
        },
      ];
      await store.dispatch(toggleHomeCamSelectView());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('dispatches TOGGLE_HOME_CAM_SELECT_VIEW without segment event', async () => {
      const store = mockStore({ homecam: { displaySelectView: false } });
      const expectedActions = [
        {
          type: TOGGLE_HOME_CAM_SELECT_VIEW,
        },
      ];
      await store.dispatch(toggleHomeCamSelectView());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('toggleHomeCamVisibility', () => {
    let mockStore;

    before(() => {
      mockStore = configureStore([thunk]);
    });

    it('dispatches TOGGLE_HOME_CAM_VISIBILITY', async () => {
      const store = mockStore({ backplane: { user: { entitlements: ['sl_premium'] } } });
      const visible = true;
      const expectedActions = [
        {
          type: TOGGLE_HOME_CAM_VISIBILITY,
          visible,
        },
      ];
      await store.dispatch(toggleHomeCamVisibility(true));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('updateHomeCam', () => {
    let mockStore;

    before(() => {
      mockStore = configureStore([thunk]);
    });

    beforeEach(() => {
      sinon.stub(userApi, 'updateUserSettings');
    });

    afterEach(() => {
      userApi.updateUserSettings.restore();
    });

    it('dispatches UPDATE_HOME_CAM_SUCCESS', async () => {
      userApi.updateUserSettings.resolves('ok');
      const store = mockStore({
        homecam: {
          currentSelection: '12345',
        },
        backplane: {
          user: {
            entitlements: ['sl_premium'],
            favorites: [
              {
                _id: '123',
                name: '54th Street',
                conditions: {
                  human: false,
                  value: 'NONE',
                },
                waveHeight: {
                  human: false,
                  min: 3,
                  max: 4,
                  occassional: 5,
                  humanRelation: 'GOOD',
                  plus: false,
                },
                cameras: [
                  {
                    _id: '12345',
                    streamUrl: 'http://stream.com',
                    stillUrl: 'http://still.com',
                    status: {
                      isDown: false,
                      message: '',
                    },
                  },
                ],
              },
            ],
          },
        },
      });
      const expectedActions = [
        {
          type: UPDATE_HOME_CAM,
        },
        {
          type: UPDATE_HOME_CAM_SUCCESS,
          cameraId: '12345',
        },
        {
          type: TOGGLE_HOME_CAM_SELECT_VIEW,
        },
        {
          type: FETCH_HOME_CAM,
        },
        {
          type: FETCH_HOME_CAM_SUCCESS,
          camera: {
            _id: '12345',
            streamUrl: 'http://stream.com',
            stillUrl: 'http://still.com',
            status: {
              isDown: false,
              message: '',
            },
          },
          spot: {
            _id: '123',
            name: '54th Street',
            conditions: {
              human: false,
              value: 'NONE',
            },
            waveHeight: {
              human: false,
              min: 3,
              max: 4,
              occassional: 5,
              humanRelation: 'GOOD',
              plus: false,
            },
            cameras: [
              {
                _id: '12345',
                streamUrl: 'http://stream.com',
                stillUrl: 'http://still.com',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
          },
        },
      ];
      await store.dispatch(updateHomeCam());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('dispatches UPDATE_HOME_CAM_FAILURE', async () => {
      const error = new Error('Error message');
      userApi.updateUserSettings.rejects(error);
      const store = mockStore({
        homecam: {
          currentSelection: '12345',
        },
        backplane: {
          user: {
            entitlements: ['sl_premium'],
            favorites: [
              {
                cameras: [
                  {
                    _id: '12345',
                    streamUrl: 'http://stream.com',
                    stillUrl: 'http://still.com',
                    status: {
                      isDown: false,
                      message: '',
                    },
                  },
                ],
              },
            ],
          },
        },
      });
      const expectedActions = [
        {
          type: UPDATE_HOME_CAM,
        },
        {
          type: UPDATE_HOME_CAM_FAILURE,
          error,
        },
      ];
      await store.dispatch(updateHomeCam());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });
});
