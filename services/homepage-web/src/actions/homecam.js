import { find as _find } from 'lodash';
import { Cookies } from 'react-cookie';
import {
  trackEvent,
  getUserPremiumStatus,
  SL_PREMIUM,
  SL_BASIC,
  ANONYMOUS,
  getUserSettings,
  getUserFavorites,
  getUserBasicStatus,
} from '@surfline/web-common';
import * as userApi from '../common/api/user';

const cookies = new Cookies();

export const FETCH_HOME_CAM = 'FETCH_HOME_CAM';
export const FETCH_HOME_CAM_SUCCESS = 'FETCH_HOME_CAM_SUCCESS';
export const FETCH_HOME_CAM_FAILURE = 'FETCH_HOME_CAM_FAILURE';
export const SELECT_HOME_CAM_OPTION = 'SELECT_HOME_CAM_OPTION';
export const TOGGLE_HOME_CAM_SELECT_VIEW = 'TOGGLE_HOME_CAM_SELECT_VIEW';
export const TOGGLE_HOME_CAM_VISIBILITY = 'TOGGLE_HOME_CAM_VISIBILITY';
export const UPDATE_HOME_CAM = 'UPDATE_HOME_CAM';
export const UPDATE_HOME_CAM_SUCCESS = 'UPDATE_HOME_CAM_SUCCESS';
export const UPDATE_HOME_CAM_FAILURE = 'UPDATE_HOME_CAM_FAILURE';

const findCamStatus = (cams) => _find(cams, (cam) => cam.status && !cam.status.isDown);

const findCamId = (cams, id) => _find(cams, (cam) => cam._id === id);

const findCameraById = (camId, favorites) =>
  new Promise((resolve) => {
    const favorite = _find(favorites, (fav) => fav.cameras.length && findCamId(fav.cameras, camId));
    if (favorite) {
      resolve({
        camera: findCamId(favorite.cameras, camId),
        spot: favorite,
      });
    } else {
      // update user settings if home cam has been deleted from favorites
      userApi
        .updateUserSettings({ homecam: null })
        .then(() => resolve({ camera: null, spot: null }))
        .catch(() => resolve({ camera: null, spot: null }));
    }
  });

const findCameraByUserFavorites = (favorites) =>
  new Promise((resolve) => {
    const favCam = _find(favorites, (fav) => fav.cameras.length && findCamStatus(fav.cameras));
    if (favCam) {
      resolve({
        spot: favCam,
        camera: findCamStatus(favCam.cameras),
      });
    }
    resolve({ camera: null, spot: null });
  });

export const setHomeCam = (camId) => async (dispatch, state) => {
  dispatch({
    type: FETCH_HOME_CAM,
  });
  try {
    let homecam;
    const currentState = state();
    const isPremium = getUserPremiumStatus(currentState);
    const settings = getUserSettings(currentState);
    const favorites = getUserFavorites(currentState);
    if (!isPremium) homecam = { camera: null, spot: null };
    else if (camId || (settings && settings.homecam)) {
      const id = camId || settings.homecam;
      homecam = await findCameraById(id, favorites);
    } else {
      homecam = await findCameraByUserFavorites(favorites);
    }
    dispatch({
      type: FETCH_HOME_CAM_SUCCESS,
      camera: homecam.camera,
      spot: homecam.spot,
    });
  } catch (error) {
    dispatch({
      type: FETCH_HOME_CAM_FAILURE,
      error,
    });
  }
};

export const updateHomeCam = () => async (dispatch, state) => {
  const selectedCamId = state().homecam.currentSelection;
  dispatch({
    type: UPDATE_HOME_CAM,
  });
  try {
    await userApi.updateUserSettings({ homecam: selectedCamId });
    await dispatch({
      type: UPDATE_HOME_CAM_SUCCESS,
      cameraId: selectedCamId,
    });
    dispatch({
      type: TOGGLE_HOME_CAM_SELECT_VIEW,
    });
    const favorites = getUserFavorites(state());
    trackEvent('Changed Home Cam', { favoriteCount: favorites.length });
    await dispatch(setHomeCam(selectedCamId));
  } catch (error) {
    dispatch({
      type: UPDATE_HOME_CAM_FAILURE,
      error,
    });
  }
};

export const selectHomeCamOption = (value) => ({
  type: SELECT_HOME_CAM_OPTION,
  currentSelection: value,
});

export const toggleHomeCamSelectView = () => async (dispatch, state) => {
  await dispatch({
    type: TOGGLE_HOME_CAM_SELECT_VIEW,
  });
  if (state().homecam.displaySelectView) {
    trackEvent('Clicked Change Home Cam');
  }
};

export const toggleHomeCamVisibility = (serverInit) => (dispatch, state) => {
  const currentState = state();
  const isPremium = getUserPremiumStatus(currentState) ? SL_PREMIUM : false;
  const isBasic = getUserBasicStatus(currentState) ? SL_BASIC : false;
  const subscriptionEntitlement = isPremium || isBasic || ANONYMOUS;
  let homeCamVisible;
  if (serverInit) {
    const homeCamVisibleCookie = cookies.get('home_cam_visible');
    homeCamVisible =
      typeof homeCamVisibleCookie !== 'undefined' ? homeCamVisibleCookie === 'true' : true;
  } else {
    cookies.set('home_cam_visible', !state().homecam.visible, { path: '/' });
    homeCamVisible = !state().homecam.visible;
  }

  dispatch({
    type: TOGGLE_HOME_CAM_VISIBILITY,
    visible: homeCamVisible,
  });
  if (!serverInit) {
    const trackEventText = homeCamVisible ? 'Open Home Cam' : 'Closed Home Cam';
    trackEvent(trackEventText, { subscriptionEntitlement });
  }
};
