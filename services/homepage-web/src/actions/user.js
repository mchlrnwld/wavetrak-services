import {
  favoriteSpot,
  unfavoriteSpot,
  skipUserOnboarding,
  updateUserSettings,
} from '@surfline/web-common';

/**  @param {string} spotId */
export const doFavoriteSpot = (spotId) => favoriteSpot({ spotId, useAccessTokenQueryParam: true });

/** @param {string} spotId */
export const doUnfavoriteSpot = (spotId) =>
  unfavoriteSpot({ spotId, useAccessTokenQueryParam: true });

/** @param {Object} updatedSetting */
export const doUpdateUserSettings = (updatedSetting) =>
  updateUserSettings({ updatedSetting, useAccessTokenQueryParam: true });

export const doSkipUserOnboarding = () => skipUserOnboarding({ useAccessTokenQueryParam: true });
