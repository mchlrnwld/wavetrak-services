import * as spotAPI from '../common/api/spot';

export const FETCH_CAM_OF_THE_MOMENT = 'FETCH_CAM_OF_THE_MOMENT';
export const FETCH_CAM_OF_THE_MOMENT_SUCCESS = 'FETCH_CAM_OF_THE_MOMENT_SUCCESS';
export const FETCH_CAM_OF_THE_MOMENT_FAILURE = 'FETCH_CAM_OF_THE_MOMENT_FAILURE';

export const fetchCamOfTheMoment = () => async (dispatch) => {
  dispatch({
    type: FETCH_CAM_OF_THE_MOMENT,
  });
  try {
    const camOfTheMoment = await spotAPI.fetchCamOfTheMoment();
    dispatch({
      type: FETCH_CAM_OF_THE_MOMENT_SUCCESS,
      camOfTheMoment,
    });
  } catch (error) {
    dispatch({
      type: FETCH_CAM_OF_THE_MOMENT_FAILURE,
      error,
    });
  }
};
