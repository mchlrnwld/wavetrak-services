import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as feedAPI from '../common/api/feed';
import { promobox } from '../common/api/fixtures';
import {
  FETCH_PROMOBOX_SLIDES,
  FETCH_PROMOBOX_SLIDES_SUCCESS,
  FETCH_PROMOBOX_SLIDES_FAILURE,
  FETCH_FEED,
  FETCH_FEED_FAILURE,
  fetchPromoBoxSlides,
  fetchFeed,
} from './homepage';
import cardsAndSlides from '../common/api/fixtures/cardsAndSlides';

describe('actions / homepage', () => {
  let mockStore;
  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchPromoBoxSlides', () => {
    beforeEach(() => {
      sinon.stub(feedAPI, 'fetchPromoBoxSlides');
    });

    afterEach(() => {
      feedAPI.fetchPromoBoxSlides.restore();
    });

    it('dispatches FETCH_PROMOBOX_SLIDES with cards and slides', async () => {
      const store = mockStore({
        homepage: {
          promobox: {
            cards: [],
            slides: [],
          },
        },
        backplane: {
          user: {
            entitlements: [],
            favorites: [],
            settings: [],
          },
        },
      });
      const slides = promobox?.data?.promobox;
      let cards = [];
      if (slides.length > 3) {
        cards = [...slides.splice(-3)];
      }
      feedAPI.fetchPromoBoxSlides.resolves(promobox);

      const expectedActions = [
        {
          type: FETCH_PROMOBOX_SLIDES,
        },
        {
          type: FETCH_PROMOBOX_SLIDES_SUCCESS,
          cards,
          slides,
        },
      ];
      await store.dispatch(fetchPromoBoxSlides());
      expect(cardsAndSlides).to.deep.equal(expectedActions);
      expect(feedAPI.fetchPromoBoxSlides).to.have.been.calledOnce();
    });

    it('dispatches FETCH_PROMOBOX_SLIDES_FAILURE on error', async () => {
      const store = mockStore({
        homepage: {
          promobox: {
            cards: [],
            slides: [],
          },
        },
        backplane: {
          user: {
            entitlements: [],
            favorites: [],
            settings: [],
          },
        },
      });
      feedAPI.fetchPromoBoxSlides.rejects({
        message: 'Some error occurred',
        status: 500,
      });

      const expectedActions = [
        {
          type: FETCH_PROMOBOX_SLIDES,
        },
        {
          type: FETCH_PROMOBOX_SLIDES_FAILURE,
          error: {
            message: 'Some error occurred',
            status: 500,
          },
        },
      ];
      await store.dispatch(fetchPromoBoxSlides());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(feedAPI.fetchPromoBoxSlides).to.have.been.calledOnce();
    });
  });

  describe('fetchFeed', () => {
    beforeEach(() => {
      sinon.stub(feedAPI, 'fetchFeedArticles');
    });

    afterEach(() => {
      feedAPI.fetchFeedArticles.restore();
    });

    it('dispatches FETCH_FEED_SUCCESS', async () => {
      const store = mockStore({
        homepage: {
          feed: {
            error: null,
            loading: false,
            pageCount: 0,
            curated: {
              articles: [],
              limit: 12,
              nextStart: null,
              type: 'curated',
            },
            feedLarge: {
              articles: [],
              limit: 3,
              nextStart: null,
              type: 'feedLarge',
            },
            feedSmall: {
              articles: [],
              limit: 2,
              nextStart: null,
              type: 'feedSmall',
            },
          },
          backplane: {
            user: {
              entitlements: [],
              favorites: [],
              settings: [],
            },
          },
        },
      });
      const feedArticles = {
        associated: {
          nextFeedLargeStart: null,
          nextFeedSmallStart: null,
          nextCuratedStart: null,
        },
        data: {
          feedLarge: [],
          feedSmall: [],
          curated: [],
        },
      };
      feedAPI.fetchFeedArticles.resolves(feedArticles);

      await store.dispatch(fetchFeed());
      expect(feedAPI.fetchFeedArticles).to.have.been.calledOnce();
    });

    it('dispatches FETCH_FEED_FAILURE on error', async () => {
      const store = mockStore({
        homepage: {
          feed: {
            error: null,
            loading: false,
            pageCount: 0,
            curated: {
              articles: [],
              limit: 12,
              nextStart: null,
              type: 'curated',
            },
            feedLarge: {
              articles: [],
              limit: 3,
              nextStart: null,
              type: 'feedLarge',
            },
            feedSmall: {
              articles: [],
              limit: 2,
              nextStart: null,
              type: 'feedSmall',
            },
          },
        },
        backplane: {
          user: {
            entitlements: [],
            favorites: [],
            settings: [],
          },
        },
      });

      feedAPI.fetchFeedArticles.rejects({
        message: 'Some error occurred',
        status: 500,
      });

      const expectedActions = [
        {
          type: FETCH_FEED,
        },
        {
          type: FETCH_FEED_FAILURE,
          error: {
            message: 'Some error occurred',
            status: 500,
          },
        },
      ];
      await store.dispatch(fetchFeed());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(feedAPI.fetchFeedArticles).to.have.been.calledOnce();
    });
  });
});
