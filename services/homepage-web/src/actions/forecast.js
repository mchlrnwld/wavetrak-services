import { getFavoriteSubregions, getNearestSubregion, getUserFavorites } from '@surfline/web-common';
import { fetchRegionalArticles } from '../common/api/feed';
import {
  fetchBatchSubregions,
  fetchNearestSpot,
  fetchRegionalOverview,
  fetchRegionalConditionForecast,
} from '../common/api/spot';
import { doUpdateUserSettings } from './user';

export const CLEAR_FORECAST_ARTICLES = 'CLEAR_FORECAST_ARTICLES';
export const FETCH_FORECAST_ARTICLES = 'FETCH_FORECAST_ARTICLES';
export const FETCH_FORECAST_ARTICLES_SUCCESS = 'FETCH_FORECAST_ARTICLES_SUCCESS';
export const FETCH_FORECAST_ARTICLES_FAILURE = 'FETCH_FORECAST_ARTICLES_FAILURE';

export const FETCH_SUBREGION_FORECAST = 'FETCH_SUBREGION_FORECAST';
export const FETCH_SUBREGION_FORECAST_SUCCESS = 'FETCH_SUBREGION_FORECAST_SUCCESS';
export const FETCH_SUBREGION_FORECAST_FAILURE = 'FETCH_SUBREGION_FORECAST_FAILURE';

export const SET_SUBREGION_FORECAST = 'SET_SUBREGION_FORECAST';
export const SET_SUBREGION_FORECAST_SUCCESS = 'SET_SUBREGION_FORECAST_SUCCESS';
export const SET_SUBREGION_FORECAST_FAILURE = 'SET_SUBREGION_FORECAST_FAILURE';

export const clearForecastArticles = () => ({
  type: CLEAR_FORECAST_ARTICLES,
});

export const fetchForecastArticles = (subregionId) => async (dispatch, getState) => {
  dispatch({
    type: FETCH_FORECAST_ARTICLES,
  });
  try {
    const { nextStart } = getState().homepage.forecast.articles;
    const articles = await fetchRegionalArticles(subregionId, nextStart, 6, true);
    dispatch({
      type: FETCH_FORECAST_ARTICLES_SUCCESS,
      articles,
    });
  } catch (error) {
    dispatch({
      type: FETCH_FORECAST_ARTICLES_FAILURE,
      error,
    });
  }
};

export const fetchSubregionForecast = (subregion) => async (dispatch, getState) => {
  dispatch({
    type: FETCH_SUBREGION_FORECAST,
  });
  try {
    const state = getState();
    let subregionId;
    let favoriteSubregions = {};
    const favorites = getUserFavorites(state);

    if (favorites.length) {
      favoriteSubregions = await getFavoriteSubregions(favorites, fetchBatchSubregions);
    }
    const subregions = favoriteSubregions.subregions || [];

    if (subregion) subregionId = subregion;
    else {
      subregionId =
        favoriteSubregions.topSubregion || (await getNearestSubregion(fetchNearestSpot));
    }
    const filteredSubregions = subregions.filter((sub) => sub._id !== subregionId);

    const [overview, forecast] = await Promise.all([
      fetchRegionalOverview(subregionId),
      fetchRegionalConditionForecast(subregionId, 2),
    ]);

    await dispatch(fetchForecastArticles(subregionId));

    dispatch({
      type: FETCH_SUBREGION_FORECAST_SUCCESS,
      overview,
      forecast,
      subregions: filteredSubregions,
    });
  } catch (error) {
    dispatch({
      type: FETCH_SUBREGION_FORECAST_FAILURE,
      error,
    });
  }
};

export const setHomeForecast = (subregionId) => async (dispatch) => {
  dispatch({
    type: SET_SUBREGION_FORECAST,
  });
  try {
    await dispatch(doUpdateUserSettings({ homeForecast: subregionId }));
    await dispatch(fetchSubregionForecast(subregionId));
    await dispatch(clearForecastArticles());
    await dispatch(fetchForecastArticles(subregionId));
    dispatch({
      type: SET_SUBREGION_FORECAST_SUCCESS,
    });
  } catch (error) {
    dispatch({
      type: SET_SUBREGION_FORECAST_FAILURE,
      error,
    });
  }
};
