/* eslint-disable comma-dangle */
/*
  Babel will yell at you in your command line if you have dangling commas in the `import` statements
  in this file. If you don't have them ESLint will get mad at you, so we need to disable the rule.
*/

export default [
  {
    routeProps: {
      path: '/',
      exact: true,
    },
    loader: () =>
      import(
        /* webpackChunkName: "Homepage" */
        './containers/routes/Homepage'
      ),
    loading: () => null,
    modules: ['./containers/routes/Homepage'],
    webpack: () => [require.resolveWeak('./containers/routes/Homepage')],
  },
  {
    routeProps: {
      path: '/search/:term',
    },
    loader: () =>
      import(
        /* webpackChunkName: "Search" */
        './containers/routes/Search'
      ),
    loading: () => null,
    modules: ['./containers/routes/Search'],
    webpack: () => [require.resolveWeak('./containers/routes/Search')],
  },
  {
    routeProps: {
      path: '/premium-benefits',
    },
    loader: () =>
      import(
        /* webpackChunkName: "PremiumLanding" */
        './containers/routes/PremiumLanding'
      ),
    loading: () => null,
    modules: ['./containers/routes/PremiumLanding'],
    webpack: () => [require.resolveWeak('./containers/routes/PremiumLanding')],
  },
  {
    routeProps: {
      path: '/contests/:contest/:region/:period/entries/:entry',
    },
    loader: () =>
      import(
        /* webpackChunkName: "ContestEntryContainer" */
        './containers/routes/ContestEntryContainer'
      ),
    loading: () => null,
    modules: ['./containers/routes/ContestEntryContainer'],
    webpack: () => [require.resolveWeak('./containers/routes/ContestEntryContainer')],
  },
  {
    routeProps: {
      path: '/contests/:contest/:region/:period/entries/',
    },
    loader: () =>
      import(
        /* webpackChunkName: "ContestEntries" */
        './containers/routes/ContestEntries'
      ),
    loading: () => null,
    modules: ['./containers/routes/ContestEntries'],
    webpack: () => [require.resolveWeak('./containers/routes/ContestEntries')],
  },
  {
    routeProps: {
      path: '/contests/:contest/:region/:period/details/',
    },
    loader: () =>
      import(
        /* webpackChunkName: "ContestDetails" */
        './containers/routes/ContestDetails'
      ),
    loading: () => null,
    modules: ['./containers/routes/ContestDetails'],
    webpack: () => [require.resolveWeak('./containers/routes/ContestDetails')],
  },
  {
    routeProps: {
      path: '/contests/:contest/:region/:period/submit/',
    },
    loader: () =>
      import(
        /* webpackChunkName: "ContestSubmit" */
        './containers/routes/ContestSubmit'
      ),
    loading: () => null,
    modules: ['./containers/routes/ContestSubmit'],
    webpack: () => [require.resolveWeak('./containers/routes/ContestSubmit')],
  },
  {
    routeProps: {
      path: '/contests/:contest/:region/:period/',
    },
    loader: () =>
      import(
        /* webpackChunkName: "ContestLanding" */
        './containers/routes/ContestLanding'
      ),
    loading: () => null,
    modules: ['./containers/routes/ContestLanding'],
    webpack: () => [require.resolveWeak('./containers/routes/ContestLanding')],
  },
  {
    routeProps: {
      path: '/surf-cams',
    },
    loader: () =>
      import(
        /* webpackChunkName: "SurfCams" */
        './containers/routes/SurfCams'
      ),
    loading: () => null,
    modules: ['./containers/routes/SurfCams'],
    webpack: () => [require.resolveWeak('./containers/routes/SurfCams')],
  },
  {
    routeProps: {
      path: '*',
      status: 404,
    },
    loader: () =>
      import(
        /* webpackChunkName: "NotFound" */
        './containers/routes/NotFound'
      ),
    loading: () => null,
    modules: ['./containers/routes/NotFound'],
    webpack: () => [require.resolveWeak('./containers/routes/NotFound')],
  },
];
