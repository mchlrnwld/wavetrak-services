export default {
  cdn: 'https://product-cdn.integration.surfline.com/homepage/',
  productAPI: 'https://services.sandbox.surfline.com',
  servicesURL: 'https://services.sandbox.surfline.com',
  homepageUrl: 'https://integration.surfline.com/',
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    amazonUAM: '15e39e15-7749-4909-bf7b-f5adb0a415e7',
  },
};
