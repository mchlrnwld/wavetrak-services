export default {
  cdn: 'https://wa.cdn-surfline.com/homepage/',
  productAPI: 'https://services.surfline.com',
  servicesURL: 'https://services.surfline.com',
  homepageUrl: 'https://www.surfline.com/',
  favoritesUrl: '/setup/favorite-surf-spots',
  legacyUrl: 'http://www.surfline.com',
  htl: {
    scriptUrl: 'https://htlbid.com/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/v3/surfline.com/htlbid.css',
    isTesting: 'no',
  },
  appKeys: {
    segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
    fbAppId: process.env.FB_APP_ID || '218714858155230',
    amazonUAM: '15e39e15-7749-4909-bf7b-f5adb0a415e7',
  },
};
