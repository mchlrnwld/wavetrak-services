export default {
  cdn: 'https://product-cdn.sandbox.surfline.com/homepage/',
  productAPI: 'https://services.sandbox.surfline.com',
  servicesURL: 'https://services.sandbox.surfline.com',
  homepageUrl: 'https://sandbox.surfline.com/',
  favoritesUrl: '/setup/favorite-surf-spots',
  robots: 'noindex,nofollow',
  legacyUrl: 'http://qa.surfline.com',
  htl: {
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
  },
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    amazonUAM: '15e39e15-7749-4909-bf7b-f5adb0a415e7',
  },
};
