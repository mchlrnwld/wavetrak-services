export default {
  cdn: 'https://product-cdn.staging.surfline.com/homepage/',
  productAPI: 'https://services.staging.surfline.com',
  servicesURL: 'https://services.staging.surfline.com',
  homepageUrl: 'https://staging.surfline.com/',
  favoritesUrl: '/setup/favorite-surf-spots',
  robots: 'noindex,nofollow',
  legacyUrl: 'http://staging.surfline.com',
  htl: {
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
  },
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    amazonUAM: '15e39e15-7749-4909-bf7b-f5adb0a415e7',
  },
};
