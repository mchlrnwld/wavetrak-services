export default {
  cdnUrl: 'http://e.cdn-surfline.com/images/sl_landing/',
  onboardingUrl: '/setup/favorite-surf-spots',
  addFavoritesUrl: '/setup/favorite-surf-spots/cams',
  funnelUrl: '/upgrade',
  signInUrl: '/sign-in',
  createAccountUrl: '/create-account',
  mobileWidth: 512,
  tabletWidth: 769,
  desktopWidth: 1201,
  largeDesktop: 1435,
  homepageSEOUrl: 'https://www.surfline.com/',
  meta: {
    description:
      'The most accurate and trusted surf reports, forecasts' +
      ' and coastal weather. Surfers from around the world choose Surfline for dependable' +
      ' and up to date surfing forecasts and high quality surf content, live surf cams and features.',
    title: {
      homepage:
        'SURFLINE.COM | Global Surf Reports, Surf Forecasts, Live Surf Cams and Coastal Weather',
      landing: 'Global Surf Reports, Surf Forecasts, Live Surf Cams - Surfline',
    },
    og: {
      image: '/facebook-og-default.png',
      title: 'Surfline.com',
    },
    twitter: {
      image: '/twitter-og-default.png',
    },
  },
  baseContestUrl: (region, period, contest = 'wave-of-the-winter') =>
    `contests/${contest}/${region}/${period}/`,
  cloudflareImageResizingUrl: (mediaUrl, settings = 'w=600,q=75,f=auto,fit=contain') =>
    `https://www.surfline.com/cdn-cgi/image/${settings}/${mediaUrl}`,
  multiCamConditionsPollingInterval: process.env.MULTI_CAM_CONDITIONS_POLLING_INTERVAL || 1200000,
};
