import PropTypes from 'prop-types';

export default PropTypes.shape({
  'spot-suggest': {
    showAll: PropTypes.bool,
    name: PropTypes.string,
    initialCount: PropTypes.number,
  },
  'subregion-suggest': {
    showAll: PropTypes.bool,
    name: PropTypes.string,
    initialCount: PropTypes.number,
  },
  'geoname-suggest': {
    showAll: PropTypes.bool,
    name: PropTypes.string,
    initialCount: PropTypes.number,
  },
  'feed-tag-suggest': {
    showAll: PropTypes.bool,
    name: PropTypes.string,
    initialCount: PropTypes.number,
  },
});
