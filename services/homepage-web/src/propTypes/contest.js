import PropTypes from 'prop-types';

export default PropTypes.shape({
  meta: PropTypes.shape({
    acf: PropTypes.shape({
      contest: PropTypes.number,
      logo: PropTypes.string,
      background_image: PropTypes.string,
      region: PropTypes.number,
      ad_unit_1: PropTypes.string,
      ad_unit_2: PropTypes.string,
    }),
  }),
  post: PropTypes.shape({
    ID: PropTypes.number,
    post_title: PropTypes.string,
  }),
});
