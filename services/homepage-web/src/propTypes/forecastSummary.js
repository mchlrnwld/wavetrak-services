import PropTypes from 'prop-types';

export default PropTypes.shape({
  forecaster: PropTypes.shape({
    name: PropTypes.string,
    title: PropTypes.string,
    iconUrl: PropTypes.string,
  }),
  highlights: PropTypes.arrayOf(PropTypes.string),
});
