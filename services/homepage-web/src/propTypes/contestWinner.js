import PropTypes from 'prop-types';

export default PropTypes.shape({
  imageUrl: PropTypes.string,
  href: PropTypes.string,
  name: PropTypes.string,
  year: PropTypes.string,
});
