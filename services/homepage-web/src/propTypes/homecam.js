import PropTypes from 'prop-types';

export default PropTypes.shape({
  visible: PropTypes.bool,
  spot: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    conditions: PropTypes.shape({
      human: PropTypes.bool,
      value: PropTypes.string,
    }),
    waveHeight: PropTypes.shape({
      human: PropTypes.bool,
      min: PropTypes.number,
      max: PropTypes.number,
      occassional: PropTypes.number,
      humanRelation: PropTypes.string,
      plus: PropTypes.bool,
    }),
  }),
  camera: PropTypes.shape({
    streamUrl: PropTypes.string,
    stillUrl: PropTypes.string,
    status: PropTypes.shape({
      isDown: PropTypes.bool,
      message: PropTypes.string,
    }),
  }),
});
