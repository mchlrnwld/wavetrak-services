import PropTypes from 'prop-types';

export default PropTypes.shape({
  units: PropTypes.shape({
    waveHeight: PropTypes.string,
  }),
  advertising: PropTypes.shape({
    spotId: PropTypes.string,
    subregionId: PropTypes.string,
  }),
  spot: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    legacyId: PropTypes.number,
    legacyRegionId: PropTypes.number,
    subregionId: PropTypes.string,
    lat: PropTypes.number,
    lon: PropTypes.number,
    conditions: PropTypes.shape({
      human: PropTypes.bool,
      value: PropTypes.string,
    }),
    waveHeight: PropTypes.shape({
      min: PropTypes.number,
      max: PropTypes.number,
      plus: PropTypes.bool,
    }),
    camera: PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string,
      streamUrl: PropTypes.string,
      stillUrl: PropTypes.string,
      rewindBaseUrl: PropTypes.string,
      status: PropTypes.shape({
        isDown: PropTypes.bool,
        message: PropTypes.string,
      }),
    }),
  }),
});
