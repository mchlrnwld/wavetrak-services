import PropTypes from 'prop-types';
import articlePropType from './article';
import contestPropType from './contest';
import contestEntryPropType from './contestEntry';

export default PropTypes.shape({
  contest: contestPropType,
  contestNews: {
    loading: PropTypes.bool,
    news: PropTypes.arrayOf(articlePropType),
  },
  loading: PropTypes.bool,
  contestEntries: {
    loading: PropTypes.bool,
    entries: PropTypes.arrayOf(contestEntryPropType),
    activeEntry: contestEntryPropType,
  },
  displayLoadMore: PropTypes.bool,
});
