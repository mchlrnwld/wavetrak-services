import PropTypes from 'prop-types';
import ampmPropType from './ampm';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  am: ampmPropType,
  pm: ampmPropType,
});
