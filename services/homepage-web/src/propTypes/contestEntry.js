import PropTypes from 'prop-types';

const taxonomyPropType = PropTypes.shape({
  term_id: PropTypes.number,
  name: PropTypes.string,
  slug: PropTypes.string,
  taxonomy: PropTypes.string,
});

export default PropTypes.shape({
  id: PropTypes.number,
  date: PropTypes.string,
  entryId: PropTypes.string,
  name: PropTypes.string,
  photographer: taxonomyPropType,
  posterUrl: PropTypes.string,
  videoUrl: PropTypes.string,
  spot: taxonomyPropType,
  surfer: taxonomyPropType,
});
