import PropTypes from 'prop-types';

export default PropTypes.shape({
  minHeight: PropTypes.number,
  maxHeight: PropTypes.number,
  plus: PropTypes.bool,
  rating: PropTypes.string,
  humanRelation: PropTypes.string,
});
