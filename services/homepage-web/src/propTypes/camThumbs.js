import PropTypes from 'prop-types';

export default PropTypes.shape({
  camThumbWidth: PropTypes.string.isRequired,
  camListItems: PropTypes.arrayOf(PropTypes.number.isRequired),
});
