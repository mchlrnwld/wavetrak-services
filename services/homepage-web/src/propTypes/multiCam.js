import PropTypes from 'prop-types';
import { SUBREGIONS, COLLECTIONS } from '../actions/multiCam';

export const collectionPropTypes = PropTypes.arrayOf(
  PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    spots: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  })
);

export const collectionTypesPropType = PropTypes.arrayOf(
  PropTypes.shape({
    type: PropTypes.oneOf([SUBREGIONS, COLLECTIONS]).isRequired,
    name: PropTypes.string.isRequired,
  })
);
