import PropTypes from 'prop-types';
import unitsPropType from './units';
import forecastConditionPropType from './forecastCondition';

export default PropTypes.shape({
  success: PropTypes.bool,
  overview: PropTypes.shape({
    name: PropTypes.string,
  }),
  associated: PropTypes.shape({
    utcOffset: PropTypes.number,
    units: unitsPropType,
  }),
  conditions: PropTypes.arrayOf(forecastConditionPropType),
});
