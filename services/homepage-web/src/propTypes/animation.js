/** @prettier */

import PropTypes from 'prop-types';

export const animationControlsPropType = PropTypes.shape({
  set: PropTypes.func.isRequired,
  start: PropTypes.func.isRequired,
  stop: PropTypes.func.isRequired,
});
