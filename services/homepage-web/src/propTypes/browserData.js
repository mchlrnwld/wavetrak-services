import PropTypes from 'prop-types';

export default PropTypes.shape({
  name: PropTypes.string,
  version: PropTypes.number,
});
