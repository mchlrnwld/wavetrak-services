import PropTypes from 'prop-types';

export default PropTypes.shape({
  abbreviation: PropTypes.string,
  imageUrl: PropTypes.string,
  name: PropTypes.string,
});
