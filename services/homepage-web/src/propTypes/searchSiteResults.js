import PropTypes from 'prop-types';

export default PropTypes.arrayOf(
  PropTypes.shape({
    took: PropTypes.number,
    timed_out: PropTypes.bool,
    _shards: PropTypes.shape({
      total: PropTypes.number,
      successful: PropTypes.number,
      failed: PropTypes.number,
    }),
    hits: PropTypes.shape({
      total: PropTypes.number,
      max_score: PropTypes.number,
      hits: PropTypes.arrayOf(
        PropTypes.shape({
          _index: PropTypes.string,
          _type: PropTypes.string,
          _id: PropTypes.string,
          _score: PropTypes.number,
          _source: PropTypes.shape({
            breadCrumbs: PropTypes.arrayOf(PropTypes.string),
            name: PropTypes.string,
            location: PropTypes.shape({
              lon: PropTypes.number,
              lat: PropTypes.number,
            }),
            cams: PropTypes.arrayOf(PropTypes.string),
            href: PropTypes.string,
          }),
        })
      ),
    }),
    suggest: PropTypes.shape({
      'spot-suggest': PropTypes.arrayOf(
        PropTypes.shape({
          text: PropTypes.string,
          offset: PropTypes.number,
          length: PropTypes.number,
          options: PropTypes.arrayOf(
            PropTypes.shape({
              text: PropTypes.string,
              _index: PropTypes.string,
              _type: PropTypes.string,
              _id: PropTypes.string,
              _score: PropTypes.number,
              _source: PropTypes.shape({
                breadCrumbs: PropTypes.arrayOf(PropTypes.string),
                name: PropTypes.string,
                location: PropTypes.shape({
                  lon: PropTypes.number,
                  lat: PropTypes.number,
                }),
                cams: PropTypes.arrayOf(PropTypes.string),
                href: PropTypes.string,
              }),
              contexts: PropTypes.shape({
                type: PropTypes.arrayOf(PropTypes.string),
                location: PropTypes.arrayOf(PropTypes.string),
              }),
            })
          ),
        })
      ),
    }),
    status: PropTypes.number,
  })
);
