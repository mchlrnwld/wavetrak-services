import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
});
