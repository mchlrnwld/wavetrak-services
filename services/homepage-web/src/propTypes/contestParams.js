import PropTypes from 'prop-types';

export default PropTypes.shape({
  contest: PropTypes.string,
  entry: PropTypes.string,
  period: PropTypes.string,
  region: PropTypes.string,
});
