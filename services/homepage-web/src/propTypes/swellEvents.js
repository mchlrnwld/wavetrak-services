import PropTypes from 'prop-types';
import articlePropType from './article';

export default PropTypes.arrayOf(
  PropTypes.shape({
    active: PropTypes.bool,
    dfpKeyword: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    isLive: PropTypes.bool,
    jwPlayerVideoId: PropTypes.string,
    name: PropTypes.string,
    permalink: PropTypes.string,
    summary: PropTypes.string,
    thumbnailUrl: PropTypes.string,
    updates: PropTypes.arrayOf(articlePropType),
  })
);
