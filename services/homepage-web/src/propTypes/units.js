import PropTypes from 'prop-types';

export default PropTypes.shape({
  temperature: PropTypes.string,
  windSpeed: PropTypes.string,
  tideHeight: PropTypes.string,
  waveHeight: PropTypes.string,
});
