/** @prettier */
import PropTypes from 'prop-types';
import cameraPropType from './camera';

export default PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
  legacyId: PropTypes.number,
  legacyRegionId: PropTypes.number,
  subregionId: PropTypes.string,
  lat: PropTypes.number,
  lon: PropTypes.number,
  conditions: PropTypes.shape({
    human: PropTypes.bool,
    value: PropTypes.string,
    expired: PropTypes.bool,
  }),
  waveHeight: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
    plus: PropTypes.bool,
  }),
  cameras: PropTypes.arrayOf(cameraPropType),
});

export const spotDefaultProps = {
  _id: '',
  name: '',
  conditions: {
    human: false,
    value: '',
    expired: false,
  },
  waveHeight: {
    min: 0,
    max: 0,
    plus: false,
  },
  cameras: [],
};
