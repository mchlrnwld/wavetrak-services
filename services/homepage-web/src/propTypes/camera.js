import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  title: PropTypes.string,
  streamUrl: PropTypes.string,
  stillUrl: PropTypes.string,
  rewindBaseUrl: PropTypes.string,
  status: PropTypes.shape({
    isDown: PropTypes.bool,
    message: PropTypes.string,
  }),
});
