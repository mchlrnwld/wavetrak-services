import PropTypes from 'prop-types';
import article from './article';

export default PropTypes.shape({
  articles: PropTypes.arrayOf(article),
  limit: PropTypes.number,
  nextStart: PropTypes.string,
  type: PropTypes.string,
});
