import PropTypes from 'prop-types';

export default PropTypes.shape({
  description: PropTypes.string,
  imageUrl: PropTypes.string,
  title: PropTypes.string,
});
