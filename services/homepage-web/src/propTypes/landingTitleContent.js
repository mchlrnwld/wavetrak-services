import PropTypes from 'prop-types';

export default PropTypes.shape({
  h6: PropTypes.string,
  h1: PropTypes.string,
  text: PropTypes.string,
  buttonText: PropTypes.string,
});
