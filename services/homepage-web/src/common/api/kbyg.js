import baseFetch, { createParamString } from '../baseFetch';

export const fetchWaveForecast = (spotId, days, intervalHours, maxHeights = false) =>
  baseFetch(
    `/kbyg/spots/forecasts/wave?${createParamString({ spotId, days, intervalHours, maxHeights })}`
  );

export const fetchTideForecast = (spotId, days) =>
  baseFetch(`/kbyg/spots/forecasts/tides?${createParamString({ spotId, days })}`);

export const fetchWeatherForecast = (spotId, days, intervalHours) =>
  baseFetch(`/kbyg/spots/forecasts/weather?${createParamString({ spotId, days, intervalHours })}`);

export const fetchWindForecast = (spotId, days, intervalHours) =>
  baseFetch(`/kbyg/spots/forecasts/wind?${createParamString({ spotId, days, intervalHours })}`);

export const fetchRegionalConditionForecast = (subregionId, days) =>
  baseFetch(`/kbyg/regions/forecasts/conditions?${createParamString({ subregionId, days })}`);
