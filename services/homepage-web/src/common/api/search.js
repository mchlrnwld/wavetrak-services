import baseFetch from '../baseFetch';

// eslint-disable-next-line import/prefer-default-export
export const fetchSearchResults = async (term, cookies) =>
  baseFetch(`/search/site?q=${term}&querySize=50&suggestionSize=50&newsSearch=true`, { cookies });
