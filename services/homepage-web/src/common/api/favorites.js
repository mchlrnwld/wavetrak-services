import baseFetch from '../baseFetch';

export const getUserFavorites = () => baseFetch('/favorites/?type=spots');

export const getSubregionCollections = () => baseFetch('/favorites/collections/subregions');

export const deleteFavorites = (spotIds) =>
  baseFetch('/favorites/?', {
    method: 'DELETE',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      type: 'spots',
      spotIds,
    }),
  });

export const saveFavorites = (spotIds) =>
  baseFetch('/favorites/?', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      type: 'spots',
      spotIds,
    }),
  });
