export default {
  data: {
    promoted: [
      {
        id: '2NvDL0XJXsdfawe8YAwgS',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Winter is Coming!',
          subtitle: 'So say we all.',
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          feed2x:
            'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
          name: 'Shaler Perry',
        },
        tags: [
          {
            name: 'Waves',
            url: '/',
          },
        ],
      },
      {
        id: 'waefawe2343q4f',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Late Season XL Swell on Track for Hawaii',
          subtitle: null,
          body: 'A bunch of text.........',
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Willis',
        },
        tags: [
          {
            name: 'Storm Feed',
            url: '/',
          },
        ],
      },
      {
        id: 'aq43regvaef',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Exclusive Interview: Chris Bertish on Achieving the Impossible',
          subtitle:
            "Waterman's 93-day, transatlantic SUP journey more than just a personal milestone",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145975',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/bertish-640/full/01-17265940_379106969138051_3659242523362066432_n.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/bertish-640/full/01-17265940_379106969138051_3659242523362066432_n.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/732a7c19e81048b6e1f7d9d654c450c7?d=mm',
          name: 'Chris Borg',
        },
        tags: [
          {
            name: 'Surf News',
            url: '/',
          },
        ],
      },
      {
        id: '24rfwergbsfa',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Surfers Take on the Sharks',
          subtitle: "Brothers behind Rareform recycled bag company featured on ABC's 'Shark Tank'",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145852',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Sharks',
            url: '/',
          },
        ],
      },
      {
        id: '2NvDL0XJXXcG8YAwgS',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Winter is Coming!',
          subtitle: 'So say we all.',
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          feed2x:
            'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
          name: 'Shaler Perry',
        },
        tags: [
          {
            name: 'Waves',
            url: '/',
          },
        ],
      },
      {
        id: 'asdefawefawe23',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Late Season XL Swell on Track for Hawaii',
          subtitle: null,
          body: 'A bunch of text.........',
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Willis',
        },
        tags: [
          {
            name: 'Storm Feed',
            url: '/',
          },
        ],
      },
      {
        id: 'srynszeaw4etga',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Exclusive Interview: Chris Bertish on Achieving the Impossible',
          subtitle:
            "Waterman's 93-day, transatlantic SUP journey more than just a personal milestone",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145975',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/bertish-640/full/01-17265940_379106969138051_3659242523362066432_n.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/bertish-640/full/01-17265940_379106969138051_3659242523362066432_n.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/732a7c19e81048b6e1f7d9d654c450c7?d=mm',
          name: 'Chris Borg',
        },
        tags: [
          {
            name: 'Surf News',
            url: '/',
          },
        ],
      },
      {
        id: 'aebteantw42',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Surfers Take on the Sharks',
          subtitle: "Brothers behind Rareform recycled bag company featured on ABC's 'Shark Tank'",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145852',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Sharks',
            url: '/',
          },
        ],
      },
      {
        id: '24ewgavzsdrea',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Surfers Take on the Sharks',
          subtitle: "Brothers behind Rareform recycled bag company featured on ABC's 'Shark Tank'",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145852',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Sharks',
            url: '/',
          },
        ],
      },
      {
        id: '234grbwsedgrae',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Surfers Take on the Sharks',
          subtitle: "Brothers behind Rareform recycled bag company featured on ABC's 'Shark Tank'",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145852',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Sharks',
            url: '/',
          },
        ],
      },
    ],
    targeted: [
      {
        id: '3NvXWZELLcG8YAwgS',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Spring Is in the Air - Will There Be Waves?',
          subtitle: null,
          body: "<p>The leaves are turning and the rains have started for the Pacific Northwest, it&#39;s a tinge cooler in Southern California, and Hawaii is staring down the most substantial swell of the early NPAC season to-date. Look at satellite imagery for the North Pacific today, and the seasonal flux is here. A large, complex low-pressure system is centered over the Aleutians, and another system sliding off northern Japan is slated to rapidly intensify. Further, as we head through the end of the week and weekend, storm activity looks to stretch from the Bearing Sea eastward through the Alaskan Gulf. Stay tuned for more as we breakdown swell implications for Hawaii, the Pacific NW, North/Central and Southern California.</p><p><img src='https://images.contentful.com/kl4m5s78bekv/6dH9KaoJDagsYqeE6Ku6CI/495c24f0371f8e7c8d9a26099b725bc2/Screen_Shot_2016-10-05_at_2.45.43_PM.png' alt='NPAC Surface Analysis'> <em>Image: NOAA OPC</em></p><p><img src='https://images.contentful.com/kl4m5s78bekv/1oQ9b9CU2IcAg6Mmmu2qK4/3cf351e12d35f29898b86f96dc797512/rgb-animated.gif' alt='NPAC Satellite'> <em>Image: NOAA SSD</em></p>",
        },
        permalink: null,
        media: null,
        author: null,
        tags: [],
      },
      {
        id: '35he4stanbhwetfa',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: true,
        content: {
          title: 'Hurricane Force Winter Storm Stella Developing off US East Coast',
          subtitle: null,
          body: null,
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Storm Feed',
            url: '/',
          },
          {
            name: 'Atlantic',
            url: '/',
          },
        ],
      },
      {
        id: 'aw4tq35hbe4',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Wave of the Day - Brent Dorrington',
          body: 'Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming.',
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Australia',
            url: '/',
          },
          {
            name: 'Snapper Rocks',
            url: '/',
          },
        ],
      },
      {
        id: 'q35hrneasgwrha3',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Is Kelly Slater Building a Left at the Surf Ranch?',
          subtitle:
            "Following Kelly Slater on social media is like following a reality TV show. It's jam-packed with cliffhangers, juicy drama, and what-will-he-do-next? entertainment.",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145760',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7ef72bd7b4a1f950432200762aa71b2d?d=mm',
          name: 'John Warren',
        },
        tags: [
          {
            name: 'Social',
            url: '/',
          },
          {
            name: 'Surf News',
            url: '/',
          },
        ],
      },
      {
        id: '345hjwensrwhres',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Wave of the Day - Brent Dorrington',
          body: 'Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming.',
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Australia',
            url: '/',
          },
          {
            name: 'Snapper Rocks',
            url: '/',
          },
        ],
      },
      {
        id: 'q13t4yhresrh',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Is Kelly Slater Building a Left at the Surf Ranch?',
          subtitle:
            "Following Kelly Slater on social media is like following a reality TV show. It's jam-packed with cliffhangers, juicy drama, and what-will-he-do-next? entertainment.",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145760',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7ef72bd7b4a1f950432200762aa71b2d?d=mm',
          name: 'John Warren',
        },
        tags: [
          {
            name: 'Social',
            url: '/',
          },
          {
            name: 'Surf News',
            url: '/',
          },
        ],
      },
      {
        id: 'serbhewrbhsr',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Wave of the Day - Brent Dorrington',
          body: 'Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming.',
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Australia',
            url: '/',
          },
          {
            name: 'Snapper Rocks',
            url: '/',
          },
        ],
      },
      {
        id: '35htserhrh',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Is Kelly Slater Building a Left at the Surf Ranch?',
          subtitle:
            "Following Kelly Slater on social media is like following a reality TV show. It's jam-packed with cliffhangers, juicy drama, and what-will-he-do-next? entertainment.",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145760',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7ef72bd7b4a1f950432200762aa71b2d?d=mm',
          name: 'John Warren',
        },
        tags: [
          {
            name: 'Social',
            url: '/',
          },
          {
            name: 'Surf News',
            url: '/',
          },
        ],
      },
      {
        id: 'jet6w45htseth',
        contentType: 'Forecast',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Wave of the Day - Brent Dorrington',
          body: 'Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming.',
        },
        permalink: null,
        media: null,
        author: {
          iconUrl: 'http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm',
          name: 'Kevin Wallis',
        },
        tags: [
          {
            name: 'Australia',
            url: '/',
          },
          {
            name: 'Snapper Rocks',
            url: '/',
          },
        ],
      },
      {
        id: 'aerhery3hjj',
        contentType: 'Editorial',
        createdAt: 1489194883454,
        updatedAt: 1489194883454,
        premium: false,
        content: {
          title: 'Is Kelly Slater Building a Left at the Surf Ranch?',
          subtitle:
            "Following Kelly Slater on social media is like following a reality TV show. It's jam-packed with cliffhangers, juicy drama, and what-will-he-do-next? entertainment.",
          body: null,
        },
        permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145760',
        media: {
          type: 'image',
          feed1x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          feed2x:
            'http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg',
          promobox1x: null,
          promobox2x: null,
        },
        author: {
          iconUrl: 'http://gravatar.com/avatar/7ef72bd7b4a1f950432200762aa71b2d?d=mm',
          name: 'John Warren',
        },
        tags: [
          {
            name: 'Social',
            url: '/',
          },
          {
            name: 'Surf News',
            url: '/',
          },
        ],
      },
    ],
  },
  associated: {
    nextPromotedStart: '1489194883454,2NvDL0XJXsdfawe8YAwgS',
    nextTargetedStart: '1492705402685,12CCDDpSuBQDe86u4GmssPPK',
  },
};
