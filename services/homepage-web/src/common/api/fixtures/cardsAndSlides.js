export default [
  {
    type: 'FETCH_PROMOBOX_SLIDES',
  },
  {
    type: 'FETCH_PROMOBOX_SLIDES_SUCCESS',
    cards: [
      {
        id: 'awrgawerwerawer',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: 'http://e.cdn-surfline.com/images/sl_landing/alex-gray-1.jpg',
          tablet: 'http://e.cdn-surfline.com/images/sl_landing/alex-gray-1.jpg',
          desktop: 'http://e.cdn-surfline.com/images/sl_landing/alex-gray-1.jpg',
          largeDesktop: 'http://e.cdn-surfline.com/images/sl_landing/alex-gray-1.jpg',
        },
        align: 'right',
        title: "Inside Alex Gray's North African Freight-Train Tube",
        subtitle: 'Another solid NW swell slams the rich port',
        categories: ['Swell Stories', 'Feature'],
        menuOrder: ['3'],
      },
      {
        id: '43whergaetgw3g34',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: 'http://e.cdn-surfline.com/images/sl_landing/namibia.jpg',
          tablet: 'http://e.cdn-surfline.com/images/sl_landing/namibia.jpg',
          desktop: 'http://e.cdn-surfline.com/images/sl_landing/namibia.jpg',
          largeDesktop: 'http://e.cdn-surfline.com/images/sl_landing/namibia.jpg',
        },
        align: 'center',
        title: "Exclusive: Namibia's Greatest Hits",
        subtitle:
          'Starring Anthony Walsh, Koa + Alex Smith, Koa Rothman, Benji Brand -- and Skeleton Bay',
        categories: ['Swell Stories', 'Feature'],
        menuOrder: ['4'],
      },
      {
        id: '33qh5q34gq34',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: 'http://e.cdn-surfline.com/images/sl_landing/supertubos.jpg',
          tablet: 'http://e.cdn-surfline.com/images/sl_landing/supertubos.jpg',
          desktop: 'http://e.cdn-surfline.com/images/sl_landing/supertubos.jpg',
          largeDesktop: 'http://e.cdn-surfline.com/images/sl_landing/supertubos.jpg',
        },
        align: 'right',
        title: 'Supersurfers at Supertubos',
        subtitle: 'Dane, Ando, Brendon & Bruce get drained in Peniche',
        categories: ['Swell Stories', 'Feature'],
        menuOrder: ['5'],
      },
    ],
    slides: [
      {
        id: 'eeh545hyser42g',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: 'http://e.cdn-surfline.com/images/sl_landing/mick.jpg',
          tablet: 'http://e.cdn-surfline.com/images/sl_landing/mick.jpg',
          desktop: 'http://e.cdn-surfline.com/images/sl_landing/mick.jpg',
          largeDesktop: 'http://e.cdn-surfline.com/images/sl_landing/mick.jpg',
        },
        align: 'right',
        title: 'Mick Fanning Will Return to Full-Time Competition',
        subtitle: "3x world champ announces end of sabbatical, says he's committed to 2017 season",
        categories: ['Feature'],
        menuOrder: ['1'],
      },
      {
        id: '34grseht45a',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: 'http://e.cdn-surfline.com/images/sl_landing/wsl.jpg',
          tablet: 'http://e.cdn-surfline.com/images/sl_landing/wsl.jpg',
          desktop: 'http://e.cdn-surfline.com/images/sl_landing/wsl.jpg',
          largeDesktop: 'http://e.cdn-surfline.com/images/sl_landing/wsl.jpg',
        },
        align: 'left',
        title: "5 Guys who should win their first 'CT in 2017",
        subtitle: '',
        categories: ['Feature'],
        menuOrder: ['2'],
      },
    ],
  },
];
