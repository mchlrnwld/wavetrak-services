export default {
  editorsPicks: [
    {
      id: '2NvDL0XJXsdfawe8YAwgS',
      contentType: 'Editorial',
      createdAt: 1489194883454,
      updatedAt: 1489194883454,
      premium: false,
      content: {
        title: 'Winter is Coming!',
        subtitle: 'So say we all.',
        body: null,
      },
      permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
      media: {
        type: 'image',
        feed1x:
          'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
        feed2x:
          'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
        promobox1x: null,
        promobox2x: null,
      },
      author: {
        iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
        name: 'Shaler Perry',
      },
      tags: [
        {
          name: 'Waves',
          url: '/',
        },
      ],
    },
  ],
  trendingArticles: [
    {
      id: '2NvDL0XJXsdfawe8YAwgS',
      contentType: 'Editorial',
      createdAt: 1489194883454,
      updatedAt: 1489194883454,
      premium: false,
      content: {
        title: 'Winter is Coming!',
        subtitle: 'So say we all.',
        body: null,
      },
      permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
      media: {
        type: 'image',
        feed1x:
          'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
        feed2x:
          'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
        promobox1x: null,
        promobox2x: null,
      },
      author: {
        iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
        name: 'Shaler Perry',
      },
      tags: [
        {
          name: 'Waves',
          url: '/',
        },
      ],
    },
  ],
};
