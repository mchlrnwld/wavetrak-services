import baseFetch, { createParamString } from '../baseFetch';

export const fetchCamOfTheMoment = async () => baseFetch('/kbyg/cotm?');

export const fetchRegionalOverview = (subregionId) =>
  baseFetch(`/kbyg/regions/overview?${createParamString({ subregionId })}`);

export const fetchRegionalConditionForecast = (subregionId, days) =>
  baseFetch(`/kbyg/regions/forecasts/conditions?${createParamString({ subregionId, days })}`);

export const fetchNearestSpot = (lat, lon) =>
  baseFetch(`/kbyg/mapview/spot?${createParamString({ lat, lon })}`);

export const fetchBatchSubregions = (subregionIds) =>
  baseFetch(`/subregions/batch?${createParamString({ subregionIds })}`);

export const fetchFavoriteSubregions = (cookies) =>
  baseFetch('/kbyg/favorites/subregions?sortBy=alphaDesc', { cookies });

export const fetchNearbySpots = (spotId) => baseFetch(`/kbyg/spots/nearby?spotId=${spotId}`);

export const fetchReport = (spotId, cookies) =>
  baseFetch(`/kbyg/spots/reports?spotId=${spotId}`, { cookies });
