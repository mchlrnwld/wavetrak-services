import baseFetch from '../baseFetch';

export const updateUserSettings = (update) =>
  baseFetch('/user/settings?', {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(update),
  });
