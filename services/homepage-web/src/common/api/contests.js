import baseFetch, { createParamString } from '../baseFetch';

export const fetchContestNews = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/news?${createParamString(params)}`, { cookies });

export const fetchContestEntries = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/entries?${createParamString(params)}`, { cookies });

export const fetchContestEntriesLocations = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/entries/locations?${createParamString(params)}`, { cookies });

export const fetchContestEntriesSurfers = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/entries/surfers?${createParamString(params)}`, { cookies });

export const fetchContestEntriesMonths = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/entries/months?${createParamString(params)}`, { cookies });

export const fetchContestEntry = async (id, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/entry?${createParamString({ id })}`, { cookies });

export const fetchContestDetails = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/details?${createParamString(params)}`, { cookies });

export const fetchContest = async (params, cookies) =>
  baseFetch(`/wp-json/sl-contests/v1/contest?${createParamString(params)}`, { cookies });

export const submitContestEntry = (entry) =>
  baseFetch('/wp-json/sl-contests/v1/submit', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(entry),
  });

export const upvoteContestEntry = (entryId) =>
  baseFetch(`/wp-json/sl-contests/v1/entries/${entryId}/votes`, {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
