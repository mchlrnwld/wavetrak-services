import baseFetch, { createParamString } from '../baseFetch';

/**
 * @param {number} [limit=10]
 */
export const fetchPromoBoxSlides = async (limit = 10, geotarget) =>
  baseFetch(`/feed/promobox?${createParamString({ limit, geotarget })}`);

export const fetchArticles = async (targetedStart, subregionIds, limit = 8) =>
  baseFetch(
    `/feed/personalized?${createParamString({
      targetedStart,
      limit,
      subregionIds: !subregionIds ? null : subregionIds,
    })}`
  );

export const fetchFeedArticles = async (
  curatedStart,
  feedLargeStart,
  feedSmallStart,
  curatedLimit = 9,
  feedLargeLimit = 3,
  feedSmallLimit = 2,
  geotarget
) =>
  baseFetch(
    `/feed/curated?${createParamString({
      curatedStart,
      feedLargeStart,
      feedSmallStart,
      curatedLimit,
      feedLargeLimit,
      feedSmallLimit,
      geotarget,
    })}`
  );

export const fetchRegionalArticles = async (subregionId, start, limit, includeLocalNews) =>
  baseFetch(
    `/feed/regional?${createParamString({
      subregionId,
      start,
      limit,
      includeLocalNews,
    })}`
  );

export const fetchArticleById = (id) => baseFetch(`/feed/local?${createParamString({ id })}`);

export const fetchAllSwellEvents = (geotarget) =>
  baseFetch(`/feed/events?${createParamString({ geotarget })}`);
