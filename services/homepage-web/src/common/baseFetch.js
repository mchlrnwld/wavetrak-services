/** @prettier */

import fetch from 'isomorphic-fetch';
import { Cookies } from 'react-cookie';
import { canUseDOM, doCookiesWarning } from '@surfline/web-common';
import config from '../config';

const checkStatus = (res) => {
  if (res.status >= 200 && res.status < 300) return res;
  const error = new Error(res.statusText);
  error.response = res;
  error.statusCode = res.status;
  throw error;
};

const parseJSON = (response) => response.json();

/**
 * @description Helper function for handling client and server side fetches
 * @param {string} path
 * @param {{
 *  cookies?: {}
 * }} options
 */
const doFetch = (path, options = {}) => {
  /**
   * With the update to react-cookie we need to pass cookies in
   * explicitly for server-side fetches that need an access token,
   * so lets warn just in case people don't know
   */
  doCookiesWarning(path, options);
  const cookies = new Cookies(canUseDOM ? undefined : options.cookies);

  const wpApi = path.indexOf('wp-json') > -1;
  const at = cookies.get('access_token');
  if (at && !wpApi) {
    // todo remove
    path += `&accesstoken=${at}`; // eslint-disable-line no-param-reassign
  }

  const baseUrl = wpApi ? config.homepageUrl : config.productAPI;
  return fetch(`${baseUrl}${path}`, {
    ...options,
  })
    .then(checkStatus)
    .then(parseJSON);
};

export const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined && params[key] !== null)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export const doExternalFetch = (url) => fetch(url).then(checkStatus).then(parseJSON);

export default doFetch;
