import React from 'react';

import { Analysis, Buoy, Cams, Charts, Forecast, Rewind } from '../../components/Icons';

export default {
  Feed: {
    camOfTheMoment: {
      title: 'Cam of The Moment',
    },
  },
  MultiCam: {
    cam: {
      filler: (hasEnoughFavorites) => (hasEnoughFavorites ? 'add spot' : 'add favorites'),
    },
    regwall: {
      header: 'Registered Users Can Stream up to Four Cams with Multi-Cam',
      disclaimer: '*No credit card needed',
      createAccount: 'create account',
      link: 'Sign in',
      preLinkCopy: 'Already a member?',
    },
    paywall: {
      header: 'Premium Members Can Access up to Nine Cams at Once with Multi-Cam',
      link: 'Learn more about Surfline Premium',
      goPremium: 'Start Free Trial',
    },
  },
  PremiumLanding: {
    header: {
      title: 'Score Better Waves, More Often',
      text: 'explore premium',
    },
    features: [
      { icon: <Cams />, name: 'ad-free hd cams' },
      { icon: <Analysis />, name: 'forecast team' },
      { icon: <Forecast />, name: 'long-range dashboard' },
      { icon: <Charts />, name: 'exclusive forecast tools' },
      { icon: <Buoy />, name: 'proprietary buoy data' },
      { icon: <Rewind />, name: 'cam\nrewind' },
    ],
    cams: {
      icon: <Cams />,
      title: 'Ad-Free HD Cams',
      text: 'Watch live conditions of your favorite spots completely ad-free — and access high resolution, Premium Only cams at iconic breaks like Lower Trestles, Ala Moana and New Smyrna.',
    },
    forecasters: {
      icon: <Analysis />,
      title: "The Forecast Team Trusted by the World's Best Surfers",
      text: 'Plan your next trip or session with guidance from a trusted team of meteorologists with over 100 years experience.',
    },
    forecasts: {
      icon: <Forecast />,
      title: 'Long-Range Forecast Dashboard',
      text: 'Access the preeminent forecast oracle – curated condition reports, projected surf heights, predicted winds and tides – that guides surfers to great waves across the planet.',
    },
    tools: {
      icon: <Charts />,
      title: 'Exclusive Forecast Tools',
      text: 'Dial in your next surgical strike or dawn patrol with cutting-edge swell, wind and weather charts carefully refined over 20+ years.',
    },
    buoys: {
      icon: <Buoy />,
      title: 'Live Buoy Data Found Nowhere Else',
      text: 'Stay on top of conditions with our proprietary buoy modeling system, which isolates and analyzes individual swells within a buoy reading — offering a more nuanced and accurate understanding of current and future conditions.',
    },
    rewind: {
      icon: <Rewind />,
      title: 'Cam Rewind',
      text: "Relive an epic session by downloading your rides -- or the waves that got away -- then share 'em with your friends",
    },
    benefits: {
      title: '',
      text: 'The Benefits of Surfline Premium',
    },
    learnmorebuoys: {
      text: 'LEARN MORE',
      link: 'https://www.surfline.com/surf-news/understanding-lola-real-time-buoys/1946?slcid=surfline-buoy-pawall&slcsource=internal&slcmedium=web-paywall&slccontent=cta&slcnmpname=buoys',
    },
  },
  premiumCTA: {
    CamHeaderCTA: {
      text: 'Stream Live Conditions ad free -',
      linkText: 'Start Free Trial',
    },
  },
};
