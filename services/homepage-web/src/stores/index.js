import { configureStore } from '@reduxjs/toolkit';
import { routerMiddleware } from 'connected-react-router';

import { createReducerInjecter } from '@surfline/web-common';
import rootReducer from '../reducers';
import createReducer from '../reducers/createReducer';

/**
 * @typedef {import("@surfline/web-common").injectReducer} injectReducer
 */

/**
 * @typedef {import("@surfline/web-common").AsyncReducers} AsyncReducers
 */

function createStore(preloadedState, history) {
  const extraReducers = {
    backplane: createReducer({}, null),
  };
  /** @type {import('@reduxjs/toolkit').EnhancedStore & {injectReducer: injectReducer, asyncReducers: AsyncReducers}} */
  const store = configureStore({
    preloadedState,
    middleware: (getDefaultMiddleware) => [...getDefaultMiddleware(), routerMiddleware(history)],
    // The initial backplane reducer is just for the server render, it doesn't need any
    // handlers. It just needs to be present to set the preloaded state from the server side
    reducer: rootReducer(history, extraReducers),
    devTools: process.env.APP_ENV !== 'production',
  });

  createReducerInjecter(store, (asyncReducers) => rootReducer(history, asyncReducers));

  if (module.hot) {
    module.hot.accept(
      '../reducers',
      // eslint-disable-next-line global-require
      () => store.replaceReducer(require('../reducers').default(history, store.asyncReducers))
    );
  }

  return store;
}

export default createStore;
