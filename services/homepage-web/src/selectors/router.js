import { parse } from 'query-string';

export const getQueryParams = (state) => {
  const data = state?.router;
  return data?.location?.search ? parse(data.location.search) : null;
};
