/* eslint-disable import/prefer-default-export */
export const getBrowserData = (state) => {
  const data = state.browser.browserData;
  if (data) {
    return data;
  }
  return null;
};
