import { expect } from 'chai';
import { getQueryParams } from './router';

describe('selectors / getQueryParams', () => {
  it('should destructure query params in location object', () => {
    expect(getQueryParams({ router: { location: { search: '?qaFlag=true&googfc' } } })).to.eql({
      googfc: null,
      qaFlag: 'true',
    });
  });

  it('should return null when location does not exist', () => {
    expect(getQueryParams({})).to.eql(null);
  });
});
