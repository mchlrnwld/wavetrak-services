export const getFeed = (state) => {
  const { curated, feedLarge, feedSmall, pageCount } = state.homepage.feed;
  return {
    curated,
    feedLarge,
    feedSmall,
    pageCount,
  };
};

export const getSubregions = (state) => state.homepage.subregions || [];

export const getCamOfTheMoment = (state) => {
  if (state.homepage.camOfTheMoment) {
    const { associated, data } = state.homepage.camOfTheMoment;
    if (data) {
      const { spot } = data;
      return {
        units: associated.units,
        advertising: associated.advertising,
        spot: {
          _id: spot._id,
          name: spot.name,
          legacyId: spot.legacyId,
          legacyRegionId: spot.legacyRegionId,
          subregionId: spot.subregionId,
          lat: spot.lat,
          lon: spot.lon,
          conditions: spot.conditions,
          waveHeight: spot.waveHeight,
          camera: spot.cameras[0],
          tide: spot.tide,
          wind: spot.wind,
        },
      };
    }
  }
  return null;
};

export const getSubregionForecast = (state) => state.homepage.forecast;
