// eslint-disable-next-line import/prefer-default-export
export const getSearchResults = (state) => state.search.results || [];
