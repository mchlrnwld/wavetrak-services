import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Sticky from 'react-stickynode';
import { throttle } from 'lodash';
import { ContentContainer, GoogleDFP, PageRail } from '@surfline/quiver-react';
import {
  getUserPremiumStatus,
  getUserDetails,
  getEntitlements,
  getUserSettings,
} from '@surfline/web-common';
import AusButterBar from '../../components/AusButterBar';
import PromoBoxContainer from '../../components/PromoBoxContainer';
import LoadMore from '../LoadMore';
import FeedContentBlock from '../../components/FeedContentBlock';
import FeedCamAndForecast from '../../components/FeedCamAndForecast';
import articlePropType from '../../propTypes/article';
import articleTypePropType from '../../propTypes/articleType';
import camOfTheMomentProptype from '../../propTypes/camOfTheMoment';
import userDetailsProptype from '../../propTypes/userDetails';
import userSettingsProptype from '../../propTypes/userSettings';
import forecastPropType from '../../propTypes/forecast';
import config from '../../config';

import { fetchCamOfTheMoment } from '../../actions/camOfTheMoment';
import {
  fetchForecastArticles,
  fetchSubregionForecast,
  setHomeForecast,
} from '../../actions/forecast';
import { getFeed, getCamOfTheMoment, getSubregionForecast } from '../../selectors/homepage';
import loadAdConfig from '../../utils/adConfig';

class Feed extends Component {
  constructor() {
    super();
    this.state = {
      device: 'MOBILE',
      displayStickyAd: false,
    };
  }

  componentDidMount() {
    const { doFetchCamOfTheMoment, doFetchSubregionForecast, userSettings } = this.props;

    doFetchCamOfTheMoment();

    const subregion = userSettings && userSettings.homeForecast ? userSettings.homeForecast : null;
    doFetchSubregionForecast(subregion);

    this.setDevice();
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  setDevice = () => {
    const { displayStickyAd } = this.state;
    const windowWidth = window?.innerWidth;
    const displayRightRail = windowWidth >= 976;
    const device = windowWidth < 1023 ? 'MOBILE' : 'DESKTOP';
    if (displayRightRail !== displayStickyAd) {
      this.setState({
        device,
        displayStickyAd: displayRightRail,
      });
    } else {
      this.setState({ device });
    }
  };

  // Disabling this lint rule since if we put the static variable before the constructor
  // then `this.setDevice` is instantiated in here as undefined.
  // eslint-disable-next-line react/sort-comp
  handleResize = throttle(this.setDevice, 250);

  render() {
    const { device, displayStickyAd } = this.state;
    const {
      feed: { curated, feedLarge, feedSmall, pageCount },
      promoBox,
      isPremium,
      camOfTheMoment,
      forecast,
      doFetchForecastArticles,
      onSetHomeForecast,
      adblock,
      qaFlag,
      userDetails,
      userEntitlements,
      userSettings,
    } = this.props;
    const isLoggedIn = !!userDetails;
    return (
      <div className="sl-feed">
        <ContentContainer>
          <AusButterBar />
          <PageRail side="left">
            <PromoBoxContainer
              promoBox={promoBox}
              feedLocale={userSettings?.feed?.localized}
              imageResizing
            />
            {[...Array(pageCount)].map((blockNum, index) => (
              <FeedContentBlock
                key={blockNum}
                blockIndex={index + 1}
                adblock={adblock}
                camOfTheMoment={camOfTheMoment}
                curated={curated}
                device={device}
                doFetchForecastArticles={doFetchForecastArticles}
                feedLarge={feedLarge}
                feedSmall={feedSmall}
                forecast={forecast}
                isPremium={isPremium}
                onSetHomeForecast={onSetHomeForecast}
                qaFlag={qaFlag}
                userDetails={userDetails}
                userEntitlements={userEntitlements}
                userSettings={userSettings}
                imageResizing
              />
            ))}
            <LoadMore />
          </PageRail>
          <PageRail side="right">
            <FeedCamAndForecast
              cotmId="desktop"
              camOfTheMoment={camOfTheMoment}
              doFetchForecastArticles={doFetchForecastArticles}
              isPremium={isPremium}
              adblock={adblock}
              onSetHomeForecast={onSetHomeForecast}
              qaFlag={qaFlag}
              forecast={forecast}
              isLoggedIn={isLoggedIn}
            />
            {displayStickyAd ? (
              <Sticky
                innerActiveClass="sticky-inner-wrapper--active"
                bottomBoundary=".quiver-page-rail--right"
              >
                <div className="sl-feed-ad-unit">
                  <GoogleDFP
                    adConfig={loadAdConfig('feed', [['qaFlag', qaFlag]])}
                    isHtl
                    isTesting={config.htl.isTesting}
                  />
                </div>
              </Sticky>
            ) : null}
          </PageRail>
        </ContentContainer>
        <ContentContainer>
          <div className="sl-feed__load-more">
            <div className="sl-feed__load-more__ad">
              <GoogleDFP
                adConfig={loadAdConfig(
                  'homepage_bottom',
                  [['qaFlag', qaFlag]],
                  userEntitlements,
                  !!userDetails
                )}
                isHtl
                isTesting={config.htl.isTesting}
              />
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

Feed.propTypes = {
  feed: PropTypes.shape({
    curated: articleTypePropType,
    feedLarge: articleTypePropType,
    feedSmall: articleTypePropType,
    pageCount: PropTypes.number,
  }).isRequired,
  promoBox: PropTypes.shape({
    cards: PropTypes.arrayOf(articlePropType).isRequired,
    slides: PropTypes.arrayOf(articlePropType).isRequired,
  }).isRequired,
  isPremium: PropTypes.bool,
  doFetchCamOfTheMoment: PropTypes.func.isRequired,
  doFetchForecastArticles: PropTypes.func.isRequired,
  doFetchSubregionForecast: PropTypes.func.isRequired,
  onSetHomeForecast: PropTypes.func.isRequired,
  camOfTheMoment: camOfTheMomentProptype,
  forecast: forecastPropType.isRequired,
  adblock: PropTypes.bool,
  qaFlag: PropTypes.string,
  userDetails: userDetailsProptype,
  userEntitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  userSettings: userSettingsProptype,
};

Feed.defaultProps = {
  isPremium: false,
  camOfTheMoment: null,
  adblock: false,
  qaFlag: null,
  userDetails: null,
  userSettings: null,
};

export default connect(
  (state) => ({
    isPremium: getUserPremiumStatus(state),
    promoBox: state.homepage.promobox,
    camOfTheMoment: getCamOfTheMoment(state),
    feed: getFeed(state),
    forecast: getSubregionForecast(state),
    adblock: state.adblock,
    userDetails: getUserDetails(state),
    userEntitlements: getEntitlements(state),
    userSettings: getUserSettings(state),
  }),
  (dispatch) => ({
    doFetchForecastArticles: (subregionId) => dispatch(fetchForecastArticles(subregionId)),
    doFetchCamOfTheMoment: () => dispatch(fetchCamOfTheMoment()),
    doFetchSubregionForecast: (subregionId) => dispatch(fetchSubregionForecast(subregionId)),
    onSetHomeForecast: (subregionId) => dispatch(setHomeForecast(subregionId)),
  })
)(Feed);
