import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { format, parseISO } from 'date-fns';
import { GoogleDFP } from '@surfline/quiver-react';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { clearContestEntry, fetchContest, fetchContestEntry } from '../../../actions/contests';

import ContestContainer from '../../../components/ContestContainer';
import ContestEntry from '../../../components/ContestEntry';
import ContestHelmet from '../../../components/ContestHelmet';
import contestsPropType from '../../../propTypes/contests';
import contestParamsPropType from '../../../propTypes/contestParams';
import loadAdConfig from '../../../utils/adConfig';
import getContestAdId from '../../../utils/getContestAdId';
import getContestType from '../../../utils/getContestType';
import config from '../../../config';

class ContestEntryContainer extends Component {
  // eslint-disable-next-line react/sort-comp
  static fetch = [
    (params, { cookies }) => fetchContest(params, cookies),
    ({ entry }, { cookies }) => fetchContestEntry(entry, cookies),
  ];

  componentDidMount() {
    const {
      contests: {
        contestEntries: { activeEntry },
      },
      doFetchContestEntry,
      match: { params },
    } = this.props;
    if (!activeEntry) doFetchContestEntry(params.entry);
    trackNavigatedToPage('Contest Entry', {
      contest: getContestType(params.contest),
      contestRegion: params.region,
      category: 'contests',
    });
  }

  componentWillUnmount() {
    const { doClearContestEntry } = this.props;
    doClearContestEntry();
  }

  getEntryMetaTitle = (entry) => {
    const formattedEntryDate = format(parseISO(entry.date), 'MM/dd/yyyy');

    return (
      "O'neill Wave of the Winter Entry: " +
      `${entry.surfer.name} at ${entry.spot.name} by ${entry.photographer.name}, ${formattedEntryDate}`
    );
  };

  getEntryMetaDescription = (entry, period) =>
    `Watch and share ${entry.surfer.name}'s ` +
    `entry for the O'neill Wave of the Winter in ${period}. Powered by Surfline.`;

  render() {
    const {
      contests: {
        contest,
        contestEntries: { activeEntry },
      },
      match: {
        params: { period, region },
        params,
      },
    } = this.props;

    let contestType;
    switch (params.contest) {
      case 'spirit-of-surfing':
        contestType = 'sos';
        break;
      default:
        contestType = 'wotw';
    }
    return (
      <ContestContainer contest={contest} period={period} region={region} page="Contest Entry">
        <ContestHelmet
          title={this.getEntryMetaTitle(activeEntry)}
          description={this.getEntryMetaDescription(activeEntry, period)}
          path={`entry/${activeEntry.id}`}
          period={period}
          region={region}
        />
        <div className="sl-contest-entry-container">
          {activeEntry ? (
            <ContestEntry
              entry={activeEntry}
              doTrackEvent={trackEvent}
              period={period}
              region={region}
            />
          ) : null}
          <div className="sl-contest__ad-unit">
            <GoogleDFP
              adConfig={loadAdConfig(getContestAdId(region, 'mid', period, contestType), [
                ['Box', 'Banner'],
              ])}
              isHtl
              isTesting={config.htl.isTesting}
            />
          </div>
        </div>
      </ContestContainer>
    );
  }
}

ContestEntryContainer.propTypes = {
  contests: contestsPropType.isRequired,
  doClearContestEntry: PropTypes.func.isRequired,
  doFetchContestEntry: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
};

export default connect(
  (state) => ({
    contests: state.contests,
  }),
  (dispatch) => ({
    doClearContestEntry: () => dispatch(clearContestEntry()),
    doFetchContestEntry: (id) => dispatch(fetchContestEntry(id)),
  })
)(ContestEntryContainer);
