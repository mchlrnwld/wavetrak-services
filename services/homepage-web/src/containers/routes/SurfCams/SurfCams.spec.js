/** @prettier */

import { expect } from 'chai';
import sinon from 'sinon';

import SurfCamsPage from './SurfCams';
import favoritesFixture from '../../../utils/fixtures/favorites.json';
import { mountWithReduxAndRouter } from '../../../utils/test-utils';
import store from '../../../utils/fixtures/store';
import collectionsFixture from '../../../utils/fixtures/collections';
import { COLLECTION_TYPES } from '../../../actions/multiCam';

describe('containers / SurfCams', () => {
  let doSetCollectionsStub;
  let doSetActiveCollectionStub;
  let doChangePrimarySpotStub;
  let doSetSelectedSpotsStub;
  let doDecrementPrimarySpotIndexStub;
  let doIncrementPrimarySpotIndexStub;

  const defaultProps = {
    favorites: favoritesFixture,
    numCams: 4,
    selectedSpots: favoritesFixture,
    collectionsLoading: false,
    settings: { units: {} },
    primarySpotIndex: 1,
  };

  const initialState = {
    ...store,
    multiCam: {
      active: collectionsFixture[0]._id,
      numCams: 4,
      selectedSpots: [],
      primarySpotIndex: 1,
      collections: collectionsFixture,
      collectionTypes: COLLECTION_TYPES,
    },
  };

  beforeEach(() => {
    doSetActiveCollectionStub = sinon.stub();
    doSetCollectionsStub = sinon.stub();
    doChangePrimarySpotStub = sinon.stub();
    doSetSelectedSpotsStub = sinon.stub();
    doDecrementPrimarySpotIndexStub = sinon.stub();
    doIncrementPrimarySpotIndexStub = sinon.stub();
  });

  afterEach(() => {
    doSetActiveCollectionStub.reset();
    doSetCollectionsStub.reset();
    doChangePrimarySpotStub.reset();
    doSetSelectedSpotsStub.reset();
    doDecrementPrimarySpotIndexStub.reset();
    doIncrementPrimarySpotIndexStub.reset();
  });

  it('should change the primary cam index using the left and right keys', () => {
    const props = {
      ...defaultProps,
      doSetCollections: doSetCollectionsStub,
      doSetActiveCollection: doSetActiveCollectionStub,
      doChangePrimarySpot: doChangePrimarySpotStub,
      doSetSelectedSpots: doSetSelectedSpotsStub,
      doDecrementPrimarySpotIndex: doDecrementPrimarySpotIndexStub,
      doIncrementPrimarySpotIndex: doIncrementPrimarySpotIndexStub,
    };

    // This is a way of stubbing the `addEventListener` function callbacks so that we can ensure
    // they are being called by the hooks
    const map = {};
    window.addEventListener = (event, cb) => {
      // React adds a bunch of error events for each component, it's just noise so we prevent it
      if (event !== 'error') {
        map[event] = map[event] ? [...map[event], cb] : [cb];
      }
    };

    const { wrapper } = mountWithReduxAndRouter(SurfCamsPage, props, { initialState });

    map.keydown.map((cb) => cb({ key: 'ArrowRight' }));
    map.keydown.map((cb) => cb({ key: 'ArrowLeft' }));

    expect(doIncrementPrimarySpotIndexStub).to.have.been.calledOnce();
    expect(doDecrementPrimarySpotIndexStub).to.have.been.calledOnce();
    wrapper.unmount();
  });

  it('should call the redux actions in `useReduxAction`', async () => {
    const props = {
      ...defaultProps,
      doSetCollections: doSetCollectionsStub,
      doSetActiveCollection: doSetActiveCollectionStub,
      doChangePrimarySpot: doChangePrimarySpotStub,
      doSetSelectedSpots: doSetSelectedSpotsStub,
      doDecrementPrimarySpotIndex: doDecrementPrimarySpotIndexStub,
      doIncrementPrimarySpotIndex: doIncrementPrimarySpotIndexStub,
    };

    const { wrapper } = mountWithReduxAndRouter(SurfCamsPage, props, { initialState });

    expect(doSetSelectedSpotsStub).to.have.been.called();
    wrapper.unmount();
  });
});
