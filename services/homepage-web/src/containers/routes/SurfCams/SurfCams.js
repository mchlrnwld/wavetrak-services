import React, { useMemo, useRef } from 'react';
import 'what-input';
import { PageRail, useDeviceSize } from '@surfline/quiver-react';
import { getUserPremiumStatus, getEntitlements } from '@surfline/web-common';
import { useAnimation, motion } from 'framer-motion';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import classNames from 'classnames';

import MultiCamSurfReport from '../../../components/MultiCamSurfReport';
import CollectionList from '../../../components/CollectionList';
import CollectionRail from '../../../components/CollectionRail';
import CompanionAd from '../../../components/CompanionAd';
import MultiCam from '../../../components/MultiCam';
import userSettingsPropType from '../../../propTypes/userSettings';
import { useReduxAction } from '../../../hooks/useReduxAction';
import { useKeyPress } from '../../../hooks/useKeyPress';
import { useFramerAnimation } from '../../../hooks/useFramerAnimation';
import { useToggle } from '../../../hooks/useToggle';
import { usePageCall } from '../../../hooks/usePageCall';
import { getActiveCollection } from '../../../selectors/multiCam';
import SurfCamsHelmet from '../../../components/SurfCamsHelmet';
import { fetchCollections } from '../../../actions/multiCam';
import useFullscreen from '../../../hooks/useFullscreen';

const getClass = (isFullscreen, rightRailIsOpen, showDesktopAd, showMobileAd) =>
  classNames({
    'sl-multi-cam-page': true,
    'sl-multi-cam-page--fullscreen': isFullscreen,
    'sl-multi-cam-page--right-rail': rightRailIsOpen,
    'sl-multi-cam-page--desktop-ad': showDesktopAd,
    'sl-multi-cam-page--mobile-ad': showMobileAd,
  });

const SurfCamsPage = React.memo(
  ({
    collectionsLoading,
    settings,
    doSetSelectedSpots,
    selectedSpots,
    primarySpotIndex,
    doDecrementPrimarySpotIndex,
    doIncrementPrimarySpotIndex,
  }) => {
    /** companion ad logic */
    const { isDesktop } = useDeviceSize();
    const isPremium = useSelector(getUserPremiumStatus);
    const entitlements = useSelector(getEntitlements, shallowEqual);

    /** Page Call */
    const properties = useRef({ category: 'cams & reports' });
    usePageCall('multi-cam', properties.current);

    const multiCamRef = useRef(null);
    const [isFullscreen, toggleFullscreen] = useFullscreen(multiCamRef);

    // eslint-disable-next-line no-unused-vars
    const [rightRailIsOpen, _, toggleRightRail] = useToggle(false);
    // eslint-disable-next-line no-unused-vars
    const [leftRailIsOpen, __, toggleLeftRail] = useToggle(true);

    const activeCollection = useSelector(getActiveCollection, shallowEqual);
    const showRightRail = !collectionsLoading && !!activeCollection;

    /** Animations */
    const leftRailSpinnerControls = useAnimation();
    const rightRailControls = useAnimation();
    const leftRailControls = useAnimation();
    const [startAnimations] = useFramerAnimation({
      controls: leftRailSpinnerControls,
      variant: 'hidden',
      start: showRightRail,
    });
    useFramerAnimation({
      controls: rightRailControls,
      variant: 'loaded',
      start: activeCollection?.fetchedSpots && !activeCollection?.loading,
      reset: activeCollection?._id,
    });
    const [leftRailAnimationComplete] = useFramerAnimation({
      controls: leftRailControls,
      variant: 'shown',
      start: startAnimations,
    });

    /** Redux Actions */
    const shouldResetSelectedSpots =
      !!activeCollection && !collectionsLoading && activeCollection.fetchedSpots;
    useReduxAction({
      action: doSetSelectedSpots,
      memoizedArgs: useMemo(() => [activeCollection], [activeCollection]),
      condition: shouldResetSelectedSpots,
    });

    /** Keyboard Event Listeners (Vim Keybinds for Jason Wang) */
    useKeyPress(doDecrementPrimarySpotIndex, 'ArrowLeft', 'h', 'H');
    useKeyPress(doIncrementPrimarySpotIndex, 'ArrowRight', 'l', 'L');
    useKeyPress((key) => toggleLeftRail(key === '['), '[', ']');

    const canMountCams = leftRailAnimationComplete;

    const showDesktopAd = !isPremium && isDesktop;
    const showMobileAd = !isPremium && !isDesktop;

    return (
      <div className={getClass(isFullscreen, rightRailIsOpen, showDesktopAd, showMobileAd)}>
        <SurfCamsHelmet activeCollection={activeCollection} />
        <PageRail open={leftRailIsOpen && !isFullscreen} side="left">
          <CollectionList
            activeCollection={activeCollection}
            leftRailControls={leftRailControls}
            leftRailIsOpen={leftRailIsOpen}
            toggleLeftRail={toggleLeftRail}
            spinnerControls={leftRailSpinnerControls}
          />
        </PageRail>
        <MultiCam
          canMountCams={canMountCams}
          isFullscreen={isFullscreen}
          toggleFullscreen={toggleFullscreen}
          toggleRightRail={toggleRightRail}
          rightRailIsOpen={rightRailIsOpen}
          multiCamRef={multiCamRef}
        >
          {showMobileAd && (
            <CompanionAd adConfigName="sl-multi-cam-companion-mobile" entitlements={entitlements} />
          )}
          <MultiCamSurfReport />
        </MultiCam>
        <PageRail side="right" open={rightRailIsOpen && !isFullscreen}>
          <motion.div className="sl-collection-rail-container">
            <CollectionRail
              settings={settings}
              activeCollection={activeCollection}
              primarySpotId={selectedSpots[primarySpotIndex] && selectedSpots[primarySpotIndex]._id}
              selectedSpots={selectedSpots}
              collectionControls={rightRailControls}
            />
          </motion.div>
          {showDesktopAd && (
            <CompanionAd adConfigName="sl-multi-cam-companion" entitlements={entitlements} />
          )}
        </PageRail>
      </div>
    );
  }
);

SurfCamsPage.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
  activeCollection: PropTypes.shape({
    name: PropTypes.string,
  }),
  selectedSpots: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      cameras: PropTypes.arrayOf(PropTypes.shape({})),
    })
  ).isRequired,
  doSetSelectedSpots: PropTypes.func.isRequired,
  collectionsLoading: PropTypes.bool.isRequired,
  settings: userSettingsPropType.isRequired,
  primarySpotIndex: PropTypes.number.isRequired,
  doDecrementPrimarySpotIndex: PropTypes.func.isRequired,
  doIncrementPrimarySpotIndex: PropTypes.func.isRequired,
  staticContext: PropTypes.shape({
    status: PropTypes.number,
    url: PropTypes.string,
  }),
};

SurfCamsPage.defaultProps = {
  activeCollection: null,
  staticContext: {},
};

// SRR Fetch
SurfCamsPage.fetch = [(_, req) => (dispatch) => dispatch(fetchCollections(null, req.cookies))];

export default SurfCamsPage;
