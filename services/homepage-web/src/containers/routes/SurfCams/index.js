import { connect } from 'react-redux';
import { getUserFavorites, getUserSettings } from '@surfline/web-common';
import {
  getActiveCollection,
  getCollectionsLoading,
  getNumCams,
  getSelectedSpots,
  getPrimarySpotIndex,
} from '../../../selectors/multiCam';
import {
  fetchCollections,
  setSelectedSpots,
  decrementPrimarySpotIndex,
  incrementPrimarySpotIndex,
} from '../../../actions/multiCam';
import SurfCamsPage from './SurfCams';

export default connect(
  (state) => ({
    activeCollection: getActiveCollection(state),
    favorites: getUserFavorites(state),
    collectionsLoading: getCollectionsLoading(state),
    settings: getUserSettings(state),
    numCams: getNumCams(state),
    selectedSpots: getSelectedSpots(state),
    primarySpotIndex: getPrimarySpotIndex(state),
  }),
  (dispatch) => ({
    doSetCollections: (favorites) => dispatch(fetchCollections(favorites)),
    doSetSelectedSpots: (activeCollection) => dispatch(setSelectedSpots(activeCollection)),
    doDecrementPrimarySpotIndex: () => dispatch(decrementPrimarySpotIndex()),
    doIncrementPrimarySpotIndex: () => dispatch(incrementPrimarySpotIndex()),
  })
)(SurfCamsPage);
