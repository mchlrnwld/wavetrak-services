import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import queryString from 'query-string';
import { throttle } from 'lodash';
import {
  ContentContainer,
  GoogleDFP,
  SwellEventCardContainer,
  Treatment,
} from '@surfline/quiver-react';
import {
  trackNavigatedToPage,
  trackEvent,
  getUserBasicStatus,
  getUserPremiumStatus,
  getUserFavorites,
  getUserSettings,
} from '@surfline/web-common';
import AusModal from '../../../components/AusModal';
import HomepageHelmet from '../../../components/HomepageHelmet';
import HomeCam from '../../../components/HomeCam';
import PopularLinks from '../../../components/PopularLinks';
import Feed from '../../Feed';
import { fetchBrowserData } from '../../../actions/browser';
import { fetchPromoBoxSlides, fetchFeed } from '../../../actions/homepage';
import {
  setHomeCam,
  selectHomeCamOption,
  toggleHomeCamSelectView,
  toggleHomeCamVisibility,
  updateHomeCam,
} from '../../../actions/homecam';
import { fetchSwellEvents } from '../../../actions/swellEvents';
import { getBrowserData } from '../../../selectors/browser';
import getSwellEvents from '../../../selectors/swellEvents';
import browserDataPropShape from '../../../propTypes/browserData';
import homeCamPropType from '../../../propTypes/homecam';
import swellEventsPropType from '../../../propTypes/swellEvents';
import userFavoritesPropType from '../../../propTypes/userFavorites';
import userSettingsPropType from '../../../propTypes/userSettings';
import config from '../../../config';
import loadAdConfig from '../../../utils/adConfig';
import { SL_WEB_AUS_LAUNCH_MODAL } from '../../../common/treatments';

class Homepage extends Component {
  constructor() {
    super();
    this.state = {
      displayMobileAd: false,
    };
  }

  componentDidMount() {
    const { doFetchSwellEvents, doSetHomeCam, doToggleHomeCamVisibility } = this.props;

    let userType = null;
    const { isBasic, isPremium } = this.props;
    if (isBasic) userType = 'sl_basic';
    else if (isPremium) userType = 'sl_premium';

    trackNavigatedToPage('Home', {
      userType,
      category: 'home',
      channel: 'home',
      subCategory: 'personalized',
    });

    doSetHomeCam();
    doToggleHomeCamVisibility(true);
    doFetchSwellEvents();
    this.setDisplayMobileAd();
    window.addEventListener('resize', this.handleResize);
    window.addEventListener('click', this.handleClick);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
    window.removeEventListener('click', this.handleClick);
  }

  onClickCard = (swellEvent) => {
    trackEvent('Clicked Swell Alert Card', {
      title: swellEvent.name,
      contentType: 'Swell Alert',
      basins: swellEvent.basins.join(','),
      locationCategory: 'Homepage - Top',
      destinationUrl: swellEvent.permalink,
    });
  };

  setDisplayMobileAd = () => {
    const { displayMobileAd } = this.state;
    const windowWidth = window?.innerWidth;
    const mobileWidth = windowWidth < 976;
    if (mobileWidth !== displayMobileAd) {
      this.setState({
        displayMobileAd: mobileWidth,
      });
    }
  };

  handleResize = throttle(this.setDisplayMobileAd, 250);

  handleClick = () => {
    const mobileNavMenu = document?.getElementsByClassName(
      'quiver-new-navigation-bar__mobile__nav__menu'
    );
    const quiverPageContainer = document?.getElementsByClassName('quiver-page-container');
    quiverPageContainer[0].style.position = '';
    quiverPageContainer[0].style.zIndex = '';
    const { className } = document?.activeElement;
    if (className === 'quiver-navigation-item__container__link' && mobileNavMenu.length > 0) {
      quiverPageContainer[0].style.position = 'fixed';
      quiverPageContainer[0].style.zIndex = '99999';
    }
  };

  render() {
    const {
      browserData,
      doSelectHomeCamOption,
      doToggleHomeCamSelectView,
      doToggleHomeCamVisibility,
      doUpdateHomeCam,
      homecam,
      isPremium,
      location: { search },
      swellEvents,
      userFavorites,
      userSettings,
    } = this.props;

    const { qaFlag } = queryString.parse(search);
    const { displayMobileAd } = this.state;

    return (
      <div className="sl-homepage">
        <HomepageHelmet title={config.meta.title.homepage} />
        {homecam.initialLoad ? (
          <HomeCam
            homecam={homecam}
            isPremium={isPremium}
            userFavorites={userFavorites}
            userSettings={userSettings}
            doSelectHomeCamOption={doSelectHomeCamOption}
            doToggleHomeCamSelectView={doToggleHomeCamSelectView}
            doToggleHomeCamVisibility={() => doToggleHomeCamVisibility()}
            doUpdateHomeCam={doUpdateHomeCam}
            browserData={browserData}
          />
        ) : null}
        {swellEvents.length ? (
          <ContentContainer>
            <SwellEventCardContainer
              events={swellEvents}
              onClickCard={(swellEvent) => this.onClickCard(swellEvent)}
            />
          </ContentContainer>
        ) : null}
        <Feed qaFlag={qaFlag} />
        <ContentContainer>
          <PopularLinks />
          {displayMobileAd ? (
            <div className="sl-homepage__footer-ad">
              <GoogleDFP
                adConfig={loadAdConfig('feed_mobile', [['qaFlag', qaFlag]])}
                isHtl
                isTesting={config.htl.isTesting}
              />
            </div>
          ) : null}
        </ContentContainer>
        <Treatment treatmentName={SL_WEB_AUS_LAUNCH_MODAL} showSpinner={false}>
          <AusModal pageName="homepage" isPremium={isPremium} />
        </Treatment>
      </div>
    );
  }
}

Homepage.fetch = [
  () => async (dispatch) => {
    // onboarding is injected into state from the root container before this
    // function is executed
    await Promise.all([dispatch(fetchPromoBoxSlides()), dispatch(fetchFeed())]);
  },
  (_, { headers }) => fetchBrowserData(headers),
];

Homepage.propTypes = {
  homecam: homeCamPropType,
  doFetchSwellEvents: PropTypes.func.isRequired,
  doSetHomeCam: PropTypes.func.isRequired,
  doSelectHomeCamOption: PropTypes.func.isRequired,
  doToggleHomeCamSelectView: PropTypes.func.isRequired,
  doToggleHomeCamVisibility: PropTypes.func.isRequired,
  doUpdateHomeCam: PropTypes.func.isRequired,
  isBasic: PropTypes.bool.isRequired,
  isPremium: PropTypes.bool.isRequired,
  userFavorites: userFavoritesPropType,
  userSettings: userSettingsPropType,
  browserData: browserDataPropShape.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
  swellEvents: swellEventsPropType,
};

Homepage.defaultProps = {
  homecam: {
    visible: true,
    camera: null,
  },
  location: {
    search: null,
  },
  swellEvents: [],
  userFavorites: [],
  userSettings: {
    homecam: null,
  },
};

export default connect(
  (state) => ({
    userFavorites: getUserFavorites(state),
    userSettings: getUserSettings(state),
    isBasic: getUserBasicStatus(state),
    isPremium: getUserPremiumStatus(state),
    homecam: state.homecam,
    browserData: getBrowserData(state),
    swellEvents: getSwellEvents(state),
  }),
  (dispatch) => ({
    doSetHomeCam: () => dispatch(setHomeCam()),
    doSelectHomeCamOption: (value) => dispatch(selectHomeCamOption(value)),
    doToggleHomeCamSelectView: () => dispatch(toggleHomeCamSelectView()),
    doToggleHomeCamVisibility: (value) => dispatch(toggleHomeCamVisibility(value)),
    doUpdateHomeCam: () => dispatch(updateHomeCam()),
    doFetchSwellEvents: () => dispatch(fetchSwellEvents()),
  })
)(withRouter(Homepage));
