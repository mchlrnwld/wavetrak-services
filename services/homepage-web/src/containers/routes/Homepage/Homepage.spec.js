// TO DO: need to rework this test
// import React from 'react';
// import { Provider } from 'react-redux';
// import configureStore from 'redux-mock-store';
// import { expect } from 'chai';
// import sinon from 'sinon';
// import { mount } from 'enzyme';
// import Homepage from './Homepage';
// import PromoBox from '../../components/PromoBox';
// import * as homepageActions from '../../actions/homepage';
// import promobox from '../../common/api/fixtures';

// describe('components / Homepage', () => {
//   let mockStore;

//   before(() => {
//     mockStore = configureStore();
//   });

//   beforeEach(() => {
//     sinon.stub(homepageActions, 'fetchPromoBoxSlides')
//         .returns({ type: 'fetchPromoBoxSlides was called' });
//     sinon.stub(homepageActions, 'setPromoBoxImageSize')
//         .returns({ type: 'setPromoBoxImageSize was called' });
//   });

//   afterEach(() => {
//     homepageActions.fetchPromoBoxSlides.restore();
//     homepageActions.setPromoBoxImageSize.restore();
//   });

// it('passes props into child components', () => {
//   const store = mockStore({
//     homepage: {
//       promoBox: {
//         slides: promobox,
//         imageSize: 'mobile',
//       },
//     },
//   });
//   const doFetchPromoBoxSlides = () => {};
//   const doSetPromoBoxImageSize = () => {};
//   const promoBoxSlides = promobox;
//   const promoBoxImageSize = 'mobile';
//   const wrapper = mount(
//     <Provider store={store}>
//       <Homepage
//         doFetchPromoBoxSlides={doFetchPromoBoxSlides}
//         doSetPromoBoxImageSize={doSetPromoBoxImageSize}
//         promoBoxSlides={promoBoxSlides}
//         promoBoxImageSize={promoBoxImageSize}
//       />
//     </Provider>,
//   );
//   const promoBoxWrapper = wrapper.find(PromoBox);

//   expect(promoBoxWrapper.prop('promoBoxSlides')).to.equal(promoBoxSlides);
//   expect(promoBoxWrapper.prop('promoBoxImageSize')).to.equal(promoBoxImageSize);
// });
// });
