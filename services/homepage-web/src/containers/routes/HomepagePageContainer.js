import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  getUserPremiumStatus,
  getUserDetails,
  getEntitlements,
  getUserSettings,
  getUserFavorites,
  getWavetrakIdentity,
  canUseDOM,
  LOGGED_IN,
} from '@surfline/web-common';

import { getStatusCode } from '../../selectors/status';
import NotFound from './NotFound';

class HomepagePageContainer extends Component {
  componentDidMount() {
    const identity = getWavetrakIdentity();
    if (canUseDOM && window?.analytics && window?.analytics?.user) {
      if (identity?.type === LOGGED_IN) {
        window.analytics.identify(identity.userId, { sourceOrigin: 'homepage' });
      }
    }
  }

  getClassName = () => {
    const { isPremium } = this.props;
    classNames({
      'sl-homepage-page-container': true,
      'sl-homepage-page-container--is-premium': isPremium,
    });
  };

  render() {
    const { children, status } = this.props;

    return (
      <>
        <div className={this.getClassName()}>{status === 404 ? <NotFound /> : children}</div>
      </>
    );
  }
}

HomepagePageContainer.propTypes = {
  children: PropTypes.node,
  status: PropTypes.number.isRequired,
  isPremium: PropTypes.bool.isRequired,
};

HomepagePageContainer.defaultProps = {
  children: null,
};

export default connect((state) => ({
  status: getStatusCode(state),
  userDetails: getUserDetails(state),
  userSettings: getUserSettings(state),
  userEntitlements: getEntitlements(state),
  userFavorites: getUserFavorites(state),
  isPremium: getUserPremiumStatus(state),
}))(HomepagePageContainer);
