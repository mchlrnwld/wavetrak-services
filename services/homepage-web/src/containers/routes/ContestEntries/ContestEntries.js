import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { get as _get } from 'lodash';
import { GoogleDFP, Spinner, Select } from '@surfline/quiver-react';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import {
  clearContestEntries,
  fetchContest,
  fetchContestEntries,
  fetchContestEntriesLocations,
  fetchContestEntriesSurfers,
  fetchContestEntriesMonths,
  upvoteContestEntry,
} from '../../../actions/contests';
import config from '../../../config';

import ContestContainer from '../../../components/ContestContainer';
import ContestHelmet from '../../../components/ContestHelmet';
import ContestEntryCard from '../../../components/ContestEntryCard';
import ContestSOSEntries from '../../../components/ContestSOSEntries/ContestSOSEntries';
import contestsPropType from '../../../propTypes/contests';
import contestParamsPropType from '../../../propTypes/contestParams';
import getNameFromSlug from '../../../utils/getNameFromSlug';
import loadAdConfig from '../../../utils/adConfig';
import getContestAdId from '../../../utils/getContestAdId';
import createAccessibleOnClick from '../../../utils/createAccessibleOnClick';
import getContestType from '../../../utils/getContestType';

class ContestEntries extends Component {
  // eslint-disable-next-line react/sort-comp
  static fetch = [
    (params, { cookies }) => fetchContest(params, cookies),
    (params) => fetchContestEntriesLocations(params),
    (params) => fetchContestEntriesSurfers(params),
    (params) => fetchContestEntriesMonths(params),
  ];

  componentDidMount() {
    const {
      doFetchContestEntries,
      doFetchContestEntriesLocations,
      doFetchContestEntriesSurfers,
      doFetchContestEntriesMonths,
      contests,
      match: { params },
    } = this.props;
    const locations = contests?.contestEntriesLocations?.locations;
    const surfers = contests?.contestEntriesSurfers?.surfers;
    const months = contests?.contestEntriesMonths?.months;
    if (params.contest === 'wave-of-the-winter') doFetchContestEntries(params, 24);
    if (!locations) doFetchContestEntriesLocations(params);
    if (!surfers) doFetchContestEntriesSurfers(params);
    if (!months) doFetchContestEntriesMonths(params);
    trackNavigatedToPage('Contest Entries', {
      contest: getContestType(params.contest),
      contestRegion: params.region,
      category: 'contests',
    });
  }

  componentWillUnmount() {
    const { doClearContestEntries } = this.props;
    doClearContestEntries();
  }

  entriesLocationChanged = (e) => {
    const {
      doFetchContestEntries,
      doClearContestEntries,
      match: { params },
      contests: {
        contestEntries: { sort, surfer, month },
      },
    } = this.props;
    doClearContestEntries();
    doFetchContestEntries(params, 12, false, e.target.value, sort, surfer, month);
  };

  entriesSurferChanged = (e) => {
    const {
      doFetchContestEntries,
      doClearContestEntries,
      match: { params },
      contests: {
        contestEntries: { sort, location, month },
      },
    } = this.props;
    doClearContestEntries();
    doFetchContestEntries(params, 12, false, location, sort, e.target.value, month);
  };

  entriesMonthChanged = (e) => {
    const {
      doFetchContestEntries,
      doClearContestEntries,
      match: { params },
      contests: {
        contestEntries: { sort, location, surfer },
      },
    } = this.props;
    doClearContestEntries();
    doFetchContestEntries(params, 12, false, location, sort, surfer, e.target.value);
  };

  entriesSortChanged = (e) => {
    const {
      doFetchContestEntries,
      doClearContestEntries,
      match: { params },
      contests: {
        contestEntries: { location, surfer, month },
      },
    } = this.props;
    doClearContestEntries();
    doFetchContestEntries(params, 12, false, location, e.target.value, surfer, month);
  };

  loadMoreEntries = () => {
    const {
      doFetchContestEntries,
      match: { params },
      contests: {
        contestEntries: { location, sort, surfer, month },
      },
    } = this.props;
    doFetchContestEntries(params, 12, true, location, sort, surfer, month);
  };

  render() {
    const {
      contests: {
        contest,
        contestEntries: { entries, displayLoadMore, loading },
      },
      contests,
      doUpvoteContestEntry,
      match: {
        params: { period, region },
        params,
      },
    } = this.props;
    const locations = contests?.contestEntriesLocations?.locations;
    const surfers = contests?.contestEntriesSurfers?.surfers;
    const months = contests?.contestEntriesMonths?.months;
    const locationOptions =
      locations &&
      locations.map((location) => ({
        text: location[0].toUpperCase() + location.slice(1),
        value: location,
      }));
    if (locationOptions)
      locationOptions.unshift({
        text: `- ${region === 'regional' ? 'Regions' : 'Locations'} -`,
        value: '',
      });
    const surferOptions =
      surfers &&
      surfers.map((surfer) => ({
        text: surfer[0].toUpperCase() + surfer.slice(1),
        value: surfer,
      }));
    if (surferOptions) surferOptions.unshift({ text: '- Surfers -', value: '' });
    const monthOptions =
      months &&
      months.map((month) => ({
        text: month[0].toUpperCase() + month.slice(1),
        value: month,
      }));
    if (monthOptions) monthOptions.unshift({ text: '- Months -', value: '' });
    const contestType = getContestType(params.contest);
    const bgColorPrimary = _get(contest, 'meta.acf.primary_background_color', null);
    const bgColorSecondary = _get(contest, 'meta.acf.secondary_background_color', null);
    return (
      <ContestContainer
        contest={contest}
        period={params.period}
        region={params.region}
        bgColorPrimary={bgColorPrimary}
        page="Contest Entries"
      >
        <ContestHelmet
          title={`Entries: ${_get(contest, 'post.post_title', null)}`}
          description={_get(contest, 'meta.acf.contest_details', null)}
          path="entries"
          period={params.period}
          region={params.region}
          contestType={contestType}
        />
        <h3 className="sl-contest-entries__title">{getNameFromSlug(params.region)} Entries</h3>
        {params.contest === 'spirit-of-surfing' ? (
          <ContestSOSEntries period={period} region={region} bgColorPrimary={bgColorPrimary} />
        ) : null}
        {params.contest === 'wave-of-the-winter' ? (
          <div
            className="sl-contest-entries__container"
            style={{ backgroundColor: `${bgColorPrimary}` }}
          >
            <div className="sl-contest-entries__container__options">
              {locations && (
                <Select
                  id="sl-contest-entries-locations"
                  options={locationOptions}
                  select={{
                    onChange: this.entriesLocationChanged,
                  }}
                  style={{ backgroundColor: bgColorSecondary }}
                />
              )}
              {surfers && (
                <Select
                  id="sl-contest-entries-surfers"
                  options={surferOptions}
                  select={{
                    onChange: this.entriesSurferChanged,
                  }}
                  style={{ backgroundColor: bgColorSecondary }}
                />
              )}
              {months && (
                <Select
                  id="sl-contest-entries-month"
                  options={monthOptions}
                  select={{
                    onChange: this.entriesMonthChanged,
                  }}
                  style={{ backgroundColor: bgColorSecondary }}
                />
              )}
              {locations && surfers && months && (
                <Select
                  id="sl-contest-entries-sort"
                  options={[
                    { text: '- Sort options -', value: '' },
                    { text: 'Surfer name (asc)', value: 'name-asc' },
                    { text: 'Surfer name (desc)', value: 'name-desc' },
                    { text: 'Location (asc)', value: 'location-asc' },
                    { text: 'Location (desc)', value: 'location-desc' },
                    { text: 'Date (asc)', value: 'date-asc' },
                    { text: 'Date (desc)', value: 'date-desc' },
                  ]}
                  select={{
                    onChange: this.entriesSortChanged,
                  }}
                  style={{ backgroundColor: bgColorSecondary }}
                />
              )}
            </div>
            <div className="sl-contest-entries">
              {entries.length
                ? entries.map((entry) => (
                    <ContestEntryCard
                      entry={entry}
                      doTrackEvent={trackEvent}
                      period={params.period}
                      region={params.region}
                      key={entry.id}
                      doUpvoteContestEntry={doUpvoteContestEntry}
                      imageResizing
                    />
                  ))
                : null}
            </div>
            {loading ? <Spinner /> : null}
            {entries.length && displayLoadMore && !loading ? (
              <div
                className="sl-contest-entries__button"
                {...createAccessibleOnClick(() => this.loadMoreEntries())}
              >
                Load More
              </div>
            ) : null}
            {!entries.length && !loading ? (
              <p className="sl-contest-landing__no-entries">
                No entries yet. Be the first to
                <Link to={`/${config.baseContestUrl(params.region, params.period)}submit`}>
                  {' '}
                  Submit
                </Link>
              </p>
            ) : null}
          </div>
        ) : null}
        <div className="sl-contest__ad-unit">
          <GoogleDFP
            adConfig={loadAdConfig(getContestAdId(params.region, 'bottom', params.period), [
              ['Box', 'Banner'],
            ])}
            isHtl
            isTesting={config.htl.isTesting}
          />
        </div>
      </ContestContainer>
    );
  }
}

ContestEntries.propTypes = {
  contests: contestsPropType.isRequired,
  doClearContestEntries: PropTypes.func.isRequired,
  doFetchContestEntries: PropTypes.func.isRequired,
  contestEntriesLocations: PropTypes.arrayOf(PropTypes.string).isRequired,
  doFetchContestEntriesLocations: PropTypes.func.isRequired,
  doFetchContestEntriesSurfers: PropTypes.func.isRequired,
  doFetchContestEntriesMonths: PropTypes.func.isRequired,
  doUpvoteContestEntry: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
};

export default connect(
  (state) => ({
    contests: state.contests,
  }),
  (dispatch) => ({
    doFetchContestEntriesLocations: (params) => dispatch(fetchContestEntriesLocations(params)),
    doFetchContestEntriesSurfers: (params) => dispatch(fetchContestEntriesSurfers(params)),
    doFetchContestEntriesMonths: (params) => dispatch(fetchContestEntriesMonths(params)),
    doClearContestEntries: () => dispatch(clearContestEntries()),
    doFetchContestEntries: (params, limit, loadMore, location, sort, surfer, month) =>
      dispatch(fetchContestEntries(params, limit, loadMore, location, sort, surfer, month)),
    doUpvoteContestEntry: (entryId) => dispatch(upvoteContestEntry(entryId)),
  })
)(ContestEntries);
