import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { trackNavigatedToPage } from '@surfline/web-common';
import { fetchSearchResults, showAllToggle } from '../../../actions/search';
import { getSearchResults } from '../../../selectors/search';

import searchSiteResults from '../../../propTypes/searchSiteResults';
import searchResultsSection from '../../../propTypes/searchResultsSection';
import SearchResults from '../../../components/SearchResults';

const Search = ({ doShowAllToggle, match: { params }, results, sections }) => {
  useEffect(() => {
    trackNavigatedToPage('Search Results', { category: 'Search' });
  }, []);

  let totalHits = 0;
  let totalSuggestions = 0;
  results.forEach((result) => {
    const [key] = Object.keys(result.suggest);
    const currentHits = result.hits.hits.length;
    totalHits += currentHits;
    totalSuggestions += result.suggest[key][0].options.length;
  });
  let total = 0;
  if (totalHits > 0 && totalSuggestions > 0) {
    total = totalHits + totalSuggestions;
  } else if (totalHits > 0) {
    total = totalHits;
  } else if (totalSuggestions > 0) {
    total = totalSuggestions;
  }
  return (
    <div className="sl-search">
      <Helmet title={`Search results for ${params.term}`} />
      <div className="sl-search__total-results">
        <h1>
          Showing {total} results for <span>{params.term}</span>
        </h1>
      </div>
      <SearchResults
        term={params.term}
        results={results}
        sections={sections}
        doShowAllToggle={doShowAllToggle}
      />
    </div>
  );
};

Search.propTypes = {
  doShowAllToggle: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      term: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  results: searchSiteResults,
  sections: searchResultsSection.isRequired,
};

Search.defaultProps = {
  results: [],
};

Search.fetch = [
  (props, { cookies }) =>
    async (dispatch) => {
      await dispatch(fetchSearchResults(props.term, cookies));
    },
];

export default connect(
  (state) => ({
    results: getSearchResults(state),
    sections: state.search.sections,
  }),
  (dispatch) => ({
    doShowAllToggle: (section) => dispatch(showAllToggle(section)),
  })
)(Search);
