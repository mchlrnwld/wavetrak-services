/** @prettier */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get as _get } from 'lodash';
import { Spinner } from '@surfline/quiver-react';
import { trackNavigatedToPage } from '@surfline/web-common';
import { fetchContest, fetchContestDetails } from '../../../actions/contests';
import ContestContainer from '../../../components/ContestContainer';
import ContestHelmet from '../../../components/ContestHelmet';
import contestsPropType from '../../../propTypes/contests';
import contestParamsPropType from '../../../propTypes/contestParams';
import { renderIgElements } from '../../../utils/renderCustomComponents';
import getContestType from '../../../utils/getContestType';

class ContestDetails extends Component {
  // eslint-disable-next-line react/sort-comp
  static fetch = [
    (params, { cookies }) => fetchContest(params, cookies),
    (params, { cookies }) => fetchContestDetails(params, cookies),
  ];

  constructor(props) {
    super(props);
    this.state = {
      renderedIgElements: null,
    };
  }

  componentDidMount() {
    const {
      doFetchContestDetails,
      contests: {
        contestDetails: { details },
      },
      match: { params },
    } = this.props;
    const { renderedIgElements } = this.state;
    if (!details) doFetchContestDetails(params);
    if (details && !renderedIgElements) this.renderInstagramElements();
    trackNavigatedToPage('Contest Details', {
      contest: getContestType(params.contest),
      contestRegion: params.region,
      category: 'contests',
    });
  }

  componentDidUpdate() {
    const {
      contests: {
        contestDetails: { details },
      },
    } = this.props;
    const { renderedIgElements } = this.state;
    if (details && !renderedIgElements) this.renderInstagramElements();
  }

  renderInstagramElements() {
    renderIgElements();
    this.setState({ renderedIgElements: true });
  }

  render() {
    const {
      contests: {
        contest,
        contestDetails: { details, loading },
      },
      match: {
        params: { period, region },
        params,
      },
    } = this.props;
    const contestType = getContestType(params.contest);
    const bgColorPrimary = _get(contest, 'meta.acf.primary_background_color', null);
    return (
      <ContestContainer
        contest={contest}
        period={period}
        region={region}
        bgColorPrimary={bgColorPrimary}
        page="Contest Details"
      >
        {loading ? <Spinner /> : null}
        <ContestHelmet
          title={`How to Enter: ${_get(contest, 'post.post_title', null)}`}
          description={_get(contest, 'meta.acf.contest_details', null)}
          path="details"
          period={period}
          region={region}
          contestType={contestType}
        />
        {/* eslint-disable-next-line react/no-danger */}
        <div className="sl-contest-details" dangerouslySetInnerHTML={{ __html: details }} />
      </ContestContainer>
    );
  }
}

ContestDetails.propTypes = {
  contests: contestsPropType.isRequired,
  doFetchContestDetails: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
};

export default connect(
  (state) => ({
    contests: state.contests,
  }),
  (dispatch) => ({
    doFetchContestDetails: (params) => dispatch(fetchContestDetails(params)),
  })
)(ContestDetails);
