import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { trackClickedSubscribeCTA, trackNavigatedToPage } from '@surfline/web-common';
import PremiumLandingFeatures from '../../../components/PremiumLandingFeatures';
import PremiumLandingHeader from '../../../components/PremiumLandingHeader';
import PremiumLandingSection from '../../../components/PremiumLandingSection';
import PremiumBenefitsCreative from '../../../components/PremiumBenefitsCreative';
import en from '../../../intl/translations/en';
import forecasterImage from './forecaster.png';
import forecastsImage from './forecasts.png';
import buoysImage from './buoys.png';
import camRewindMP4 from './cam-rewind.mp4';
import premiumCamMP4 from './premium-cam.mp4';
import chartGIF from './charts.gif';
import headerBackground from './header-background.jpg';
import forecastersBackground from './forecasters-background.jpg';
import chartsBackground from './charts-background.png';
import buoysBackground from './buoys-background.jpg';
import benefitsBackground from './benefits-background.jpg';
import CamRewindPoster from './cam-rewind-poster.jpg';
import HdCamsPoster from './ad-free-hd-cams-poster.jpg';

const PremiumLanding = () => {
  useEffect(() => {
    trackNavigatedToPage('Premium Benefits');
  }, []);

  const copy = en.PremiumLanding;

  return (
    <div className="premium-landing">
      <Helmet title="Premium Benefits - Surfline" />
      <PremiumLandingHeader
        backgroundImage={headerBackground}
        copy={copy.header}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
      />
      <PremiumLandingFeatures features={copy.features} />
      <PremiumLandingSection
        section="Ad Free HD Cams"
        anchor="ad-free-hd-cams"
        orientation="center"
        copy={en.PremiumLanding.cams}
        creative={<video src={premiumCamMP4} poster={HdCamsPoster} controls autoPlay loop muted />}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
      />
      <PremiumLandingSection
        section="Premium-Analysis"
        anchor="forecast-team"
        orientation="left"
        contentPosition="top"
        copy={en.PremiumLanding.forecasters}
        creative={<img src={forecasterImage} alt="Forecaster" />}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
        backgroundImage={forecastersBackground}
      />
      <PremiumLandingSection
        section="Long Range Forecast"
        anchor="long-range-dashboard"
        orientation="left"
        copy={en.PremiumLanding.forecasts}
        creative={<img src={forecastsImage} alt="Forecasts" />}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
      />
      <PremiumLandingSection
        section="Exclusive Forecast Tools"
        anchor="exclusive-forecast-tools"
        orientation="center"
        copy={en.PremiumLanding.tools}
        creative={<img src={chartGIF} alt="Charts" />}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
        backgroundImage={chartsBackground}
      />
      <PremiumLandingSection
        section="Proprietary Buoys"
        anchor="proprietary-buoy-data"
        orientation="left"
        copy={en.PremiumLanding.buoys}
        creative={<img src={buoysImage} alt="Buoys" />}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
        backgroundImage={buoysBackground}
      />
      <PremiumLandingSection
        section="Cam Rewind"
        anchor="cam-rewind"
        orientation="center"
        copy={en.PremiumLanding.rewind}
        creative={
          <video src={camRewindMP4} poster={CamRewindPoster} controls autoPlay loop muted />
        }
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
      />
      <PremiumLandingSection
        section="Premium-Benefits"
        anchor="premium-benefits"
        orientation="center"
        copy={en.PremiumLanding.benefits}
        creative={<PremiumBenefitsCreative />}
        doClickedSubscribeCTA={trackClickedSubscribeCTA}
        backgroundImage={benefitsBackground}
      />
    </div>
  );
};

export default PremiumLanding;
