/** @prettier */
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get as _get } from 'lodash';
import { trackNavigatedToPage } from '@surfline/web-common';
import {
  changeContestEntryForm,
  clearContestEntryForm,
  fetchContest,
  submitContestEntryForm,
} from '../../../actions/contests';
import ContestContainer from '../../../components/ContestContainer';
import ContestHelmet from '../../../components/ContestHelmet';
import ContestSOSSubmit from '../../../components/ContestSOSSubmit';
import contestsPropType from '../../../propTypes/contests';
import contestParamsPropType from '../../../propTypes/contestParams';
import ContestForm from '../../../components/ContestForm';
import getContestType from '../../../utils/getContestType';

class ContestSubmit extends Component {
  // eslint-disable-next-line react/sort-comp
  static fetch = [(params, { cookies }) => fetchContest(params, cookies)];

  componentDidMount() {
    const {
      match: { params },
    } = this.props;

    trackNavigatedToPage('Contest Submit', {
      contest: getContestType(params.contest),
      contestRegion: params.region,
      category: 'contests',
    });
  }

  componentWillUnmount() {
    const { doClearContestEntryForm } = this.props;
    doClearContestEntryForm();
  }

  render() {
    const {
      contests: { contest, entryForm },
      doChangeContestEntryForm,
      doSubmitContestEntryForm,
      match: {
        params: { period, region },
        params,
      },
      fieldValues,
    } = this.props;
    const contestType = getContestType(params.contest);
    const bgColorPrimary = _get(contest, 'meta.acf.primary_background_color', null);
    return (
      <ContestContainer
        contest={contest}
        period={period}
        region={region}
        bgColorPrimary={bgColorPrimary}
        page="Contest Submit"
      >
        <ContestHelmet
          title={`Submit: ${_get(contest, 'post.post_title', null)}`}
          description={_get(contest, 'meta.acf.contest_details', null)}
          path="submit"
          period={period}
          region={region}
          contestType={contestType}
        />
        <div className="sl-contest-submit">
          <h3 className="sl-contest-submit__title">Submit Entry</h3>
          {params.contest === 'wave-of-the-winter' && period === '2020-2021' ? (
            <div>
              {region === 'north-shore' ? (
                <iframe
                  id="JotFormIFrame-202254721979966"
                  allow="geolocation; microphone; camera"
                  src="https://form.jotform.com/202254721979966"
                  frameBorder="0"
                  title="contest submit"
                />
              ) : (
                <ContestForm
                  onChange={(field, value) => doChangeContestEntryForm(field, value)}
                  onSubmit={(evt) => {
                    evt.preventDefault();
                    doSubmitContestEntryForm();
                  }}
                  fieldValues={fieldValues}
                  period={period}
                  region={region}
                  loading={entryForm.loading}
                  error={entryForm.error}
                  success={entryForm.success}
                />
              )}
            </div>
          ) : null}
          {params.contest === 'wave-of-the-winter' && period === '2019-2020' ? (
            <div>
              {region === 'north-shore' ? (
                <iframe
                  id="JotFormIFrame-93005500839958"
                  allow="geolocation; microphone; camera"
                  src="https://form.jotform.com/93005500839958"
                  frameBorder="0"
                  title="contest submit"
                />
              ) : (
                <ContestForm
                  onChange={(field, value) => doChangeContestEntryForm(field, value)}
                  onSubmit={(evt) => {
                    evt.preventDefault();
                    doSubmitContestEntryForm();
                  }}
                  fieldValues={fieldValues}
                  period={period}
                  region={region}
                  loading={entryForm.loading}
                  error={entryForm.error}
                  success={entryForm.success}
                />
              )}
            </div>
          ) : null}
          {params.contest === 'spirit-of-surfing' ? <ContestSOSSubmit period={period} /> : null}
        </div>
      </ContestContainer>
    );
  }
}

ContestSubmit.propTypes = {
  fieldValues: PropTypes.shape(),
  contests: contestsPropType.isRequired,
  doChangeContestEntryForm: PropTypes.func.isRequired,
  doClearContestEntryForm: PropTypes.func.isRequired,
  doSubmitContestEntryForm: PropTypes.func.isRequired,

  match: PropTypes.shape({
    params: contestParamsPropType.isRequired,
  }).isRequired,
};

ContestSubmit.defaultProps = {
  fieldValues: null,
};

export default connect(
  (state) => ({
    contests: state.contests,
    fieldValues: state.contests.entryForm.fields,
  }),
  (dispatch) => ({
    doChangeContestEntryForm: (details) => dispatch(changeContestEntryForm(details)),
    doClearContestEntryForm: () => dispatch(clearContestEntryForm()),
    doSubmitContestEntryForm: () => dispatch(submitContestEntryForm()),
  })
)(ContestSubmit);
