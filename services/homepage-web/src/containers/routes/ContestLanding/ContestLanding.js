import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get as _get } from 'lodash';
import { trackNavigatedToPage, getUserSettings } from '@surfline/web-common';
import ContestContainer from '../../../components/ContestContainer';
import ContestHelmet from '../../../components/ContestHelmet';
import ContestSOSEntries from '../../../components/ContestSOSEntries/ContestSOSEntries';
import ContestLandingWOTWContent from '../../../components/ContestLandingWOTWContent/ContestLandingWOTWContent';
import ContestLandingTopSection from '../../../components/ContestLandingTopSection';
import {
  clearContestEntries,
  fetchContest,
  fetchContestNews,
  fetchContestEntries,
  upvoteContestEntry,
} from '../../../actions/contests';
import contestsPropType from '../../../propTypes/contests';
import userSettingsPropType from '../../../propTypes/userSettings';
import contestParamsPropType from '../../../propTypes/contestParams';
import getContestType from '../../../utils/getContestType';

class ContestLanding extends Component {
  // eslint-disable-next-line react/sort-comp
  static fetch = [
    (params, { cookies }) => fetchContest(params, cookies),
    (params, { cookies }) => fetchContestNews(params, cookies),
    (params) => fetchContestEntries(params),
  ];

  componentDidMount() {
    const {
      contests,
      doFetchContest,
      doFetchContestNews,
      doFetchContestEntries,
      match: { params },
    } = this.props;
    if (!contests.contest) doFetchContest(params);
    if (!contests.contestNews.news.length) doFetchContestNews(params);
    if (params.contest === 'wave-of-the-winter' && !contests.contestEntries.entries.length)
      doFetchContestEntries(params);
    trackNavigatedToPage('Contest Landing', {
      contest: getContestType(params.contest),
      contestRegion: params.region,
      category: 'contests',
    });
  }

  componentDidUpdate(prevProps) {
    const {
      contests,
      doFetchContest,
      doFetchContestNews,
      doFetchContestEntries,
      match: { params },
    } = this.props;
    if (prevProps.contests.contest !== contests.contest) {
      doFetchContest(params);
      doFetchContestNews(params);
      doFetchContestEntries(params.region);
    }
    if (
      prevProps.contests.contestEntries.entries.length &&
      !contests.contestEntries.entries.length
    ) {
      doFetchContestEntries(params);
    }
  }

  componentWillUnmount() {
    const { doClearContestEntries } = this.props;
    doClearContestEntries();
  }

  render() {
    const {
      contests: { contest, contestNews, contestEntries },
      match: {
        params: { period, region },
        params,
      },
      doUpvoteContestEntry,
      userSettings,
    } = this.props;

    const contestType = getContestType(params.contest);

    const bgColorPrimary = _get(contest, 'meta.acf.primary_background_color', null);
    const bgColorSecondary = _get(contest, 'meta.acf.secondary_background_color', null);

    return (
      <ContestContainer
        contest={contest}
        period={period}
        region={region}
        showLogo={false}
        bgColorPrimary={bgColorPrimary}
        page="Contest Landing"
      >
        <ContestHelmet
          title={_get(contest, 'post.post_title', null)}
          description={_get(contest, 'meta.acf.contest_details', null)}
          period={period}
          region={region}
          contestType={contestType}
        />
        <div className="sl-contest-landing">
          <ContestLandingTopSection
            contestType={contestType}
            contestNews={contestNews}
            region={region}
            period={period}
            contest={contest}
            imageResizing
            feedLocale={userSettings?.feed?.localized}
          />
          {contestType && contestType === 'wotw' ? (
            <ContestLandingWOTWContent
              period={period}
              region={region}
              contest={contest}
              contestEntries={contestEntries}
              contestType={contestType}
              bgColorSecondary={bgColorSecondary}
              doUpvoteContestEntry={doUpvoteContestEntry}
              imageResizing
            />
          ) : null}
          {contestType && contestType === 'sos' ? (
            <ContestSOSEntries
              period={period}
              region={region}
              contestType={contestType}
              bgColorPrimary={bgColorPrimary}
              location="landing"
            />
          ) : null}
        </div>
      </ContestContainer>
    );
  }
}

ContestLanding.propTypes = {
  contests: contestsPropType.isRequired,
  doClearContestEntries: PropTypes.func.isRequired,
  doFetchContest: PropTypes.func.isRequired,
  doFetchContestNews: PropTypes.func.isRequired,
  doFetchContestEntries: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: contestParamsPropType,
  }).isRequired,
  doUpvoteContestEntry: PropTypes.func.isRequired,
  userSettings: userSettingsPropType.isRequired,
};

export default connect(
  (state) => ({
    contests: state.contests,
    userSettings: getUserSettings(state),
  }),
  (dispatch) => ({
    doClearContestEntries: () => dispatch(clearContestEntries()),
    doFetchContest: (params) => dispatch(fetchContest(params)),
    doFetchContestNews: (params) => dispatch(fetchContestNews(params)),
    doFetchContestEntries: (params) => dispatch(fetchContestEntries(params)),
    doUpvoteContestEntry: (entryId) => dispatch(upvoteContestEntry(entryId)),
  })
)(ContestLanding);
