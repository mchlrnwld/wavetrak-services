import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Waypoint } from 'react-waypoint';
import { Button } from '@surfline/quiver-react';
import { fetchFeed, startInfiniteScrolling } from '../../actions/homepage';

const LoadMore = ({ infiniteScrollingEnabled, doFetchFeed, doStartInfiniteScrolling }) => (
  <div className="sl-load-more">
    {infiniteScrollingEnabled ? (
      <Waypoint onEnter={() => doFetchFeed()} />
    ) : (
      <Button onClick={() => doStartInfiniteScrolling()}>Load More</Button>
    )}
  </div>
);

LoadMore.propTypes = {
  doStartInfiniteScrolling: PropTypes.func.isRequired,
  doFetchFeed: PropTypes.func.isRequired,
  infiniteScrollingEnabled: PropTypes.bool.isRequired,
};

export default connect(
  (state) => ({
    infiniteScrollingEnabled: state.homepage.infiniteScrollingEnabled,
  }),
  (dispatch) => ({
    doFetchFeed: () => dispatch(fetchFeed()),
    doStartInfiniteScrolling: () => dispatch(startInfiniteScrolling()),
  })
)(LoadMore);
