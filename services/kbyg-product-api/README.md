# KBYG Product API Microservice

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Setup](#setup)
- [Development](#development)
- [Endpoint Authentication](#endpoint-authentication)
  - [Sample Authentication Header](#sample-authentication-header)
- [Endpoints](#endpoints)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Setup

Use Node v4.3.0

```sh
cp .env.sample .env
npm install
```

## Development

```sh
npm run startlocal
```

## Endpoint Authentication

KBYG endpoints are partially authenticated. An authenticated user with premium entitlements will receive more data than a non-premium user will receive.

### Sample Authentication Header

| Key           | Value                    | Description      |
| ------------- | ------------------------ | ---------------- |
| X-Auth-UserId | 582495992d06cd3b71d66229 | User's Mongo ID. |

## Endpoints

- [Batch](docs/batch.md)
- [Cam of the Moment](docs/camofthemoment.md)
- [Map View](docs/mapview.md)
- [Buoy Details](docs/buoydetails.md)
- [Buoy Report](docs/buoyreport.md)
- [Nearby Buoys](docs/nearbybuoys.md)
- [Nearby Spots](docs/nearbyspots.md)
- [Popular Spots](docs/popularspots.md)
- [Redirect](docs/redirect.md)
- [Region Overview](docs/regionoverview.md)
- [Region Forecasts](docs/regionforecasts.md)
- [Spot Forecasts](docs/spotforecasts.md)
- [Spot Reports](docs/spotreports.md)
- [Storm Alerts](docs/stormalerts.md)

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `kbyg-product-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd5TlRrek1UUTFNdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eU5Ua3pNVFExTXciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests that execute queries against endpoints detailed in the [Endpoints](#endpoints) section. 

**Ex:**

Test `GET` requests against the `/spots/forecasts` endpoint:

```
curl \
    -X GET \
    -i \
    http://kbyg-api.sandbox.surfline.com/spots/forecasts/conditions?spotId=5842041f4e65fad6a77088ed&days=3&offset=1
```

**Expected response:** The response should look something like this:

```
curl \
    -X GET \
    -i \
    http://kbyg-api.sandbox.surfline.com/spots/forecasts/conditions\?spotId\=5842041f4e65fad6a77088ed\&days\=3\&offset\=1

HTTP/1.1 200 OK
Date: Wed, 25 Nov 2020 14:37:40 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 1423
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Access-Control-Allow-Credentials: true
ETag: W/"58f-4XsiLiNb9PRPLk7vpO+BjNPWd4c"

{
  "associated": {
    "units": {
      "temperature": "C",
      "tideHeight": "M",
      "swellHeight": "M",
      "waveHeight": "M",
      "windSpeed": "KPH",
      "model": "LOLA"
    },
    "utcOffset": -8
  },
  "data": {
    "conditions": [
      {
        "timestamp": 1606291200,
        "forecaster": null,
        "human": false,
        "observation": "Fun sized mid period waves from the SSW. Light and variable winds with clean conditions.",
        "am": {
          "maxHeight": 0.9,
          "minHeight": 0.6,
          "plus": true,
          "humanRelation": "Waist to stomach high",
          "occasionalHeight": null,
          "rating": null
        },
 ...
```
