provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/kbyg-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-prod"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
}

locals {
  dns_name          = "kbyg-api.prod.surfline.com"
  dns_internal_name = "internal-kbyg-product-api.surfline.com"
  dns_zone_id       = "Z3LLOZIY0ZZQDE"
}

module "kbyg-product-api" {
  source = "../../"

  company     = "sl"
  application = "kbyg-api"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 30
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = local.dns_internal_name
    },
  ]

  alb_listener_arn     = data.aws_alb_listener.main_internal.arn
  data_lake_bucket_arn = "arn:aws:s3:::wt-data-lake-prod"
  iam_role_arn         = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn    = data.aws_alb.main_internal.arn

  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 1050
  auto_scaling_min_size     = 8
  auto_scaling_max_size     = 5000
}

resource "aws_route53_record" "kbyg-product-api_internal" {
  zone_id = "ZY7MYOQ65TY5X"
  name    = local.dns_internal_name
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
