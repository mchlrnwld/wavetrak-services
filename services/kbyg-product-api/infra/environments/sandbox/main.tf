provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/sandbox/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/sandbox/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/kbyg-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-sandbox"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
}

locals {
  dns_name          = "kbyg-api.sandbox.surfline.com"
  dns_internal_name = "internal-kbyg-product-api.sandbox.surfline.com"
  dns_zone_id       = "Z3DM6R3JR1RYXV"
}

module "kbyg-product-api" {
  source = "../../"

  company     = "sl"
  application = "kbyg-api"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = local.dns_internal_name
    },
  ]

  alb_listener_arn     = data.aws_alb_listener.main_internal.arn
  data_lake_bucket_arn = "arn:aws:s3:::wt-data-lake-dev"
  iam_role_arn         = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn    = data.aws_alb.main_internal.arn

  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id

  auto_scaling_enabled = false
}

resource "aws_route53_record" "kbyg-product-api_internal" {
  zone_id = local.dns_zone_id
  name    = local.dns_internal_name
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
