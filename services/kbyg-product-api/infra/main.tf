# Permit Lambda function to access Data Lake bucket
resource "aws_iam_policy" "data_lake_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-data-lake-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/data-lake-policy.json", {
    s3_bucket = var.data_lake_bucket_arn
  })
}

resource "aws_iam_role_policy_attachment" "data_lake_bucket_mgmt_policy_attachement" {
  role       = "kbyg_product_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.data_lake_bucket_mgmt_policy.arn
}

module "kbyg_product_api" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = "kbyg-product-api"

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-kbyg-product-api"
  service_td_count            = var.service_td_count
  service_td_container_name   = "kbyg-product-api"
  service_port                = 8080
  service_alb_priority        = 270

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  dns_name          = var.dns_name
  dns_zone_id       = var.dns_zone_id
  load_balancer_arn = var.load_balancer_arn
  default_vpc       = var.default_vpc
  ecs_cluster       = var.ecs_cluster

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
}
