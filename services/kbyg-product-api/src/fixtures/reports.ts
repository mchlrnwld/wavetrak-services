export const spotReport = {
  spotReports: [
    {
      _id: '5898edec2437cc0011caf281',
      createdAt: '2017-02-06T21:43:08.944Z',
      updatedAt: '2017-02-06T21:43:08.944Z',
      humanRelation: 'thigh to waist',
      maxHeight: 3,
      minHeight: 2,
      spot: '5842041f4e65fad6a77088ed',
      condition: 5,
      occasionalHeight: null,
      rating: 'FAIR',
      waterTemp: {
        min: 23,
        max: 45,
      },
      stale: false,
      regionalReport:
        '<p><strong>Testing Reg Rep change.</strong></p>\n<p><br></p>\n<p><strong>A little bit changed. A little bit more.</strong></p>',
      forecasterEmail: 'developer@surfline.com',
      forecasterName: 'Johnny Tsunami',
      forecasterTitle: 'Surfline Employee',
    },
  ],
};

export const noSpotReport = {
  spotReports: [],
};

export const regionalReport = {
  regionalReports: [
    {
      _id: '58b0d1501c11990011e33c03',
      updatedAt: '2017-02-25T00:35:28.247Z',
      createdAt: '2017-02-25T00:35:28.247Z',
      subregion: '58581a836630e24c44878fd6',
      waterTemp: {
        max: 58,
        min: 56,
      },
      report:
        '<p><strong>Afternoon Report for North OC:</strong> Old W-WNW swell mix continues into the afternoon with mainly knee-waist+ waves at exposures and occasional shoulder high sets for top breaks.</p>',
      legacySubregionId: 2143,
      forecasterEmail: 'aaron@surfline.com',
      forecasterName: 'Aaron Bernstein',
      forecasterTitle: 'Product Owner',
    },
  ],
};

export const noRegionalReport = {
  regionalReports: [],
};
