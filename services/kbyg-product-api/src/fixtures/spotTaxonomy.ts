export default {
  liesIn: ['58f7ed5fdadb30820bb3987c'],
  associated: {
    links: [
      {
        key: 'www',
        href: 'https://www.surfline.com/surf-report/scripps/5842041f4e65fad6a7708839',
      },
    ],
  },
  in: [
    {
      name: 'United States',
      type: 'geoname',
      liesIn: ['58f7ed5fdadb30820bb3987c'],
      depth: 1,
      associated: {
        links: [
          {
            key: 'www',
            href: 'http://www.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
          },
        ],
      },
    },
  ],
};
