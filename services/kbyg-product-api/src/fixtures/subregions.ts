export default {
  _id: '58581a836630e24c44878fd6',
  primarySpot: '5842041f4e65fad6a77088ed',
  offshoreSwellLocation: {
    coordinates: [-118, 33.5],
  },
  name: 'North Orange County',
  legacyId: 2143,
};
