export const northOCSpot = {
  _id: '5842041f4e65fad6a77088ed',
  location: {
    coordinates: [-118.0064678192, 33.656781041213],
  },
  forecastLocation: {
    coordinates: [-118.007, 33.656],
  },
  pointOfInterestId: 'd1c53ea2-0f77-4d5f-b64c-be16e75966cb',
  tideStation: 'Huntington Cliffs near Newport Bay',
  timezone: 'America/Los_Angeles',
  subregion: '58581a836630e24c44878fd6',
  subregionForecastStatus: 'active',
  humanReport: {
    status: 'ON',
  },
  swellWindow: [
    {
      max: 220,
      min: 170,
    },
    {
      max: 290,
      min: 240,
    },
  ],
  best: {
    sizeRange: [
      {
        max: 8,
        min: 4,
      },
    ],
    swellWindow: [
      {
        max: 200,
        min: 180,
      },
      {
        max: 280,
        min: 250,
      },
    ],
    swellPeriod: [
      {
        max: 33,
        min: 9,
      },
    ],
    windDirection: [
      {
        max: 95,
        min: 350,
      },
    ],
  },
};

export const southOCSpot = {
  _id: '5842041f4e65fad6a77088e3',
  location: {
    coordinates: [-117.7915988640743, 33.54268310353641],
  },
  forecastLocation: {
    coordinates: [-117.793, 33.542],
  },
  tideStation: 'Salt Creek ( San Clemente) based o',
  timezone: 'America/Los_Angeles',
  subregion: '58581a836630e24c4487900a',
  subregionForecastStatus: 'active',
  humanReport: {
    status: 'ON',
  },
};
