/* eslint-disable import/no-extraneous-dependencies */
import fs from 'fs';
import path from 'path';
import Mocha from 'mocha';

const findTestFiles = (dir) => {
  const contents = fs.readdirSync(dir);

  const filePaths = [];

  contents.forEach((element) => {
    const elementPath = path.join(dir, element);
    const stats = fs.statSync(elementPath);

    if (stats.isDirectory()) {
      Array.prototype.push.apply(filePaths, findTestFiles(elementPath));
    } else if (element.substr(-8) === '.spec.ts') {
      filePaths.push(elementPath);
    }
  });

  return filePaths;
};

const runMocha = (dir) =>
  new Promise((resolve, reject) => {
    try {
      const mocha = new Mocha();
      const files = findTestFiles(dir);
      files.forEach((file) => {
        mocha.addFile(file);
      });
      mocha.run(resolve);
    } catch (err) {
      reject(err);
    }
  });

const tests = async () => {
  const failures = await runMocha(__dirname);
  if (failures) throw failures;
};

tests()
  .then(() => process.exit(0))
  .catch((err) => {
    // eslint-disable-next-line no-console
    console.error(err);
    process.exit(-1);
  });
