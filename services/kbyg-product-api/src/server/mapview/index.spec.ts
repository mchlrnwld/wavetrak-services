import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { Types } from 'mongoose';
import { geoCountryIso } from '@surfline/services-common';
import * as getUnits from '../../utils/getUnits';
import * as getSpotReportViews from '../../model/SpotReportView/getSpotReportViews';
import * as getNearestSpotReportView from '../../model/SpotReportView/getNearestSpotReportView';
import * as RegionalReport from '../../model/RegionalReport';
import mapview from '.';

describe('/mapview', () => {
  let log;
  let request;
  let getNearestSpotReportViewStub;
  let getSpotReportViewsStub;
  let getUnitsStub;
  let RegionalReportStub;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(geoCountryIso, mapview(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    getNearestSpotReportViewStub = sinon.stub(getNearestSpotReportView, 'default');
    getSpotReportViewsStub = sinon.stub(getSpotReportViews, 'default');
    getUnitsStub = sinon.stub(getUnits, 'default');
    RegionalReportStub = sinon.stub(RegionalReport, 'default');
  });

  afterEach(() => {
    getNearestSpotReportViewStub.restore();
    getSpotReportViewsStub.restore();
    getUnitsStub.restore();
    RegionalReportStub.restore();
  });

  it('should return map view for bounding box and units', async () => {
    const spotReportViews = [
      {
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        rank: 1,
      },
      {
        name: 'HB Pier, Northside',
        lat: 33.656781041213,
        lon: -118.0064678192,
        rank: 0,
      },
    ];

    const subregions = [
      {
        _id: '58581a836630e24c44878fd6',
        subregion: {
          name: 'North Orange County',
          id: '58581a836630e24c44878fd6',
          forecasterEmail: 'developer@surfline.com',
        },
      },
    ];

    const nearestSpotReportView = { legacyRegionId: 2081, subregionId: '58581a836630e24c44878fd6' };
    getSpotReportViewsStub.resolves(spotReportViews);
    getNearestSpotReportViewStub.resolves(nearestSpotReportView);
    RegionalReportStub.resolves(subregions);

    const north = 33.830433;
    const south = 33.64194;
    const west = -118.335232;
    const east = -117.896912;

    const path = `/?south=${south}&west=${west}&north=${north}&east=${east}&units=m`;
    const res = await request.get(path).send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      associated: {
        units: {
          waveHeight: 'M',
          windSpeed: 'KTS',
          tideHeight: 'M',
        },
      },
      units: {
        waveHeight: 'M',
        windSpeed: 'KTS',
        tideHeight: 'M',
      },
      data: {
        regionalForecast: {
          iconUrl: 'https://somecdn.surfline.com/region1.png',
          legacyRegionId: 2081,
          subregionId: '58581a836630e24c44878fd6',
        },
        subregions,
        spots: [
          {
            ...spotReportViews[1],
            rank: [0, 0],
          },
          {
            ...spotReportViews[0],
            rank: [0, 1],
          },
        ],
      },
    });
    expect(getSpotReportViewsStub).to.have.been.calledOnce();
    expect(getSpotReportViewsStub.firstCall.args).to.deep.equal([
      { north, south, east, west },
      {
        windSpeed: 'KTS',
        waveHeight: 'M',
        tideHeight: 'M',
      },
      undefined,
    ]);
    expect(getNearestSpotReportViewStub).to.have.been.calledOnce();
    expect(getNearestSpotReportViewStub.firstCall.args).to.deep.equal([
      {
        lon: -118.116072,
        lat: 33.7361865,
      },
    ]);
  });

  it('should default units to IMPERIAL', async () => {
    const units = {
      windSpeed: 'KTS',
      waveHeight: 'FT',
      tideHeight: 'FT',
    };
    getUnitsStub.resolves(units);
    getSpotReportViewsStub.resolves([]);
    getNearestSpotReportViewStub.resolves({ legacyRegionId: 0 });

    const path = '/?south=33.641940&west=-118.335232&north=33.830433&east=-117.896912';
    await request.get(path).send();
    expect(getSpotReportViewsStub).to.have.been.calledOnce();
    expect(getSpotReportViewsStub.firstCall.args[1]).to.deep.equal(units);
  });

  it('should call getSpotReportViews with filterCamsOnly arg if camsOnly query is true', async () => {
    const units = {
      windSpeed: 'KTS',
      waveHeight: 'FT',
      tideHeight: 'FT',
    };
    getUnitsStub.resolves(units);
    getSpotReportViewsStub.resolves([]);
    getNearestSpotReportViewStub.resolves({ legacyRegionId: 0 });

    const path =
      '/?south=33.641940&west=-118.335232&north=33.830433&east=-117.896912&camsOnly=true';
    await request.get(path).send();
    expect(getSpotReportViewsStub).to.have.been.calledOnce();
    expect(getSpotReportViewsStub.firstCall.args[2]).to.deep.equal(true);
  });

  it('should default units to IMPERIAL', async () => {
    const units = {
      windSpeed: 'KTS',
      waveHeight: 'FT',
      tideHeight: 'FT',
    };
    getUnitsStub.resolves(units);
    getSpotReportViewsStub.resolves([]);
    getNearestSpotReportViewStub.resolves({ legacyRegionId: 0 });

    const path = '/?south=33.641940&west=-118.335232&north=33.830433&east=-117.896912';
    await request.get(path).send();
    expect(getSpotReportViewsStub).to.have.been.calledOnce();
    expect(getSpotReportViewsStub.firstCall.args[1]).to.deep.equal(units);
  });

  it('should return 400 if bounding box not appropriately defined', async () => {
    const res = await request.get('/').send();
    expect(res).to.have.status(400);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({
      message: 'Bounding box parameters "north", "south", "east", "west" are required',
    });

    const res1 = await request.get('/?north=a&south=b&east=c&west=d').send();
    expect(res1).to.have.status(400);
    expect(res1).to.be.json();
    expect(res1.body).to.deep.equal({
      message: 'Bounding box parameters "north", "south", "east", "west" are required',
    });
  });

  describe('/spot', () => {
    it('should return spot nearest to the location provided', async () => {
      const spot = {
        _id: new Types.ObjectId('5842041f4e65fad6a77088f6'),
        name: 'Newport Jetties',
        lat: 33.62270460093146,
        lon: -117.9474812414847,
      };
      getNearestSpotReportViewStub.resolves(spot);

      const res = await request.get('/spot?lat=33.654213041213&lon=-118.0032588192').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        spot: {
          _id: '5842041f4e65fad6a77088f6',
          name: 'Newport Jetties',
          lat: 33.62270460093146,
          lon: -117.9474812414847,
        },
      });
    });

    it('should return 400 if location not provided', async () => {
      const res = await request.get('/spot').send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        message: 'Location parameters "lat", "lon" are required',
      });
    });
  });
});
