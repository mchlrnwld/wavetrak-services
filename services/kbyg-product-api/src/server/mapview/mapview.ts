import { NextFunction, Response } from 'express';
import newrelic from 'newrelic';
import {
  getSpotReportViews,
  getNearestSpotReportView,
  sortSpotReportViews,
} from '../../model/SpotReportView';
import getBySubregionIdIn from '../../model/RegionalReport';
import { METRIC } from '../../common/constants';
import { getWaveHeightUnitsFromQuery } from '../../utils/getUnits';
import { KBYGRequest } from '../types';

export const trackMapViewRequests =
  (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
    log.trace({
      action: '/kbyg/mapview',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    return next();
  };

export const getMapViewSpotsHandler = async (req: KBYGRequest, res: Response) => {
  const north = Number(req.query.north);
  const south = Number(req.query.south);
  const east = Number(req.query.east);
  const west = Number(req.query.west);
  const units = req.query.units
    ? {
        windSpeed: 'KTS',
        waveHeight: getWaveHeightUnitsFromQuery(req.query.units),
        tideHeight: req.query.units === METRIC ? 'M' : 'FT',
      }
    : req.units;
  if (Number.isNaN(north) || Number.isNaN(south) || Number.isNaN(east) || Number.isNaN(west)) {
    return res.status(400).send({
      message: 'Bounding box parameters "north", "south", "east", "west" are required',
    });
  }

  const { camsOnly } = req.query;
  const filterCamsOnly = camsOnly && camsOnly.toString().toUpperCase() === 'TRUE';

  newrelic.addCustomAttribute('filterCamsOnly', filterCamsOnly);

  const spotReportViews = await getSpotReportViews(
    { north, south, east, west },
    units,
    filterCamsOnly,
  );

  newrelic.addCustomAttribute('retrievedSpotReportViews', true);

  const nearestSpotReportView = await getNearestSpotReportView({
    lon: (east + west) / 2,
    lat: (north + south) / 2,
  });

  newrelic.addCustomAttribute('retrievedNearestSpotReportViews', true);

  const subregions = await getBySubregionIdIn(spotReportViews);

  newrelic.addCustomAttribute('spotCount', spotReportViews.length || 0);

  return res.send({
    associated: {
      units: {
        windSpeed: units.windSpeed,
        waveHeight: units.waveHeight,
        tideHeight: units.tideHeight,
      },
    },
    // TODO: Deprecate this. Left for backwards compatibility.
    units: {
      windSpeed: units.windSpeed,
      waveHeight: units.waveHeight,
      tideHeight: units.tideHeight,
    },
    data: {
      regionalForecast: {
        iconUrl: 'https://somecdn.surfline.com/region1.png', // TODO deprecate
        legacyRegionId: nearestSpotReportView.legacyRegionId,
        subregionId: nearestSpotReportView.subregionId,
      },
      subregions,
      spots: sortSpotReportViews(spotReportViews),
    },
  });
};

export const getNearestSpotHandler = async (req: KBYGRequest, res: Response) => {
  const lat = parseFloat(String(req.query.lat || req.geoLatitude));
  const lon = parseFloat(String(req.query.lon || req.geoLongitude));

  if (Number.isNaN(lat) || Number.isNaN(lon)) {
    return res.status(400).send({
      message: 'Location parameters "lat", "lon" are required',
    });
  }

  const spot = await getNearestSpotReportView({ lat, lon });
  return res.send({ spot });
};
