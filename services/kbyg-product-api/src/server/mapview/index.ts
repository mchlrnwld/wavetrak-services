import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import userUnits from '../middleware/userUnits';
import { trackMapViewRequests, getMapViewSpotsHandler, getNearestSpotHandler } from './mapview';

const mapView = (log) => {
  const api = Router();

  api.use('*', trackMapViewRequests(log));
  api.get('/', userUnits, wrapErrors(getMapViewSpotsHandler));
  api.get('/spot', wrapErrors(getNearestSpotHandler));

  return api;
};

export default mapView;
