import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import trackSpotRequests from './trackSpotRequests';
import userUnits from '../middleware/userUnits';
import entitlements from '../middleware/entitlements';
import checkScopeParams from '../middleware/checkScopeParams';
import checkForecastParams from '../middleware/checkForecastParams';
import getSpot from '../middleware/getSpot';
import forecasts from './forecasts';
import getNearbySpotsHandler from './getNearbySpotsHandler';
import getNearbySpotsGeoHandler from './getNearbySpotsGeoHandler';
import getSpotReportHandler from './getSpotReportHandler';
import getSpotDetailsHandler from './getSpotDetailsHandler';
import getArchivedSpotReportHandler from './getArchivedSpotReportHandler';
import getTopSpotsHandler from './getTopSpotsHandler';
import getPopularSpotsHandler from './getPopularSpotsHandler';
import getBatchSpotsHandler from './getBatchSpotsHandler';

const spots = (log) => {
  const api = Router();

  api.use('*', trackSpotRequests(log));
  api.get('/details', userUnits, getSpot, wrapErrors(getSpotDetailsHandler));
  api.use(
    '/forecasts',
    userUnits,
    entitlements,
    checkScopeParams,
    checkForecastParams,
    getSpot,
    forecasts(log),
  );
  api.get('/reports', userUnits, getSpot, wrapErrors(getSpotReportHandler));
  api.get('/archivedreports', userUnits, getSpot, wrapErrors(getArchivedSpotReportHandler));
  api.get('/nearby', userUnits, getSpot, wrapErrors(getNearbySpotsHandler));
  api.get('/nearbygeo', userUnits, wrapErrors(getNearbySpotsGeoHandler));
  api.get('/topspots', userUnits, wrapErrors(getTopSpotsHandler));
  api.get('/popular', userUnits, wrapErrors(getPopularSpotsHandler));
  api.post('/batch', json(), userUnits, wrapErrors(getBatchSpotsHandler));

  return api;
};

export default spots;
