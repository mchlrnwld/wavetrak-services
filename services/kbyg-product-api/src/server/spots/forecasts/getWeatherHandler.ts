import { getWeatherForecasts } from '../../../external/forecasts';
import {
  createUTCOffsetCalculator,
  dateRange,
  day,
  fromUnixTimestamp,
} from '../../../utils/datetime';
import weatherCondition from '../../../utils/weatherCondition';
import utcOffsetFromTimezone from '../../../utils/utcOffsetFromTimezone';
import { WEATHER_ICON_PATH } from '../../../common/constants';
import buildSunlightTimes from '../helpers/buildSunlightTimes';

const getWeatherHandler = async (req, res) => {
  const {
    units,
    forecast: { days, intervalHours },
    spot,
  } = req;

  const { start, end } = dateRange(days, spot.timezone);

  const forecast = await getWeatherForecasts({
    units,
    pointOfInterestId: spot.pointOfInterestId,
    start,
    end,
    interval: intervalHours * 60,
  });

  const sunlightTimes = buildSunlightTimes({
    start,
    end,
    timezone: spot.timezone,
    lat: req.spot.location.coordinates[1],
    lon: req.spot.location.coordinates[0],
  });

  const sunlightTimesMap = new Map(
    sunlightTimes.map(({ midnight, dawn, dusk }) => [
      day(spot.timezone, fromUnixTimestamp(midnight)),
      { dawn, dusk },
    ]),
  );

  const utcOffsetCalculator = createUTCOffsetCalculator(spot.timezone);

  const weather = forecast.data.map(({ timestamp, weather: { temperature, conditions } }) => {
    const date = fromUnixTimestamp(timestamp);
    const { dawn, dusk } = sunlightTimesMap.get(day(spot.timezone, date));
    return {
      timestamp,
      utcOffset: utcOffsetCalculator(date),
      temperature,
      condition: weatherCondition(conditions, timestamp < dawn || dusk <= timestamp),
    };
  });

  return res.send({
    associated: {
      units,
      // TODO: Deprecate this property.
      utcOffset: utcOffsetFromTimezone(spot.timezone),
      weatherIconPath: WEATHER_ICON_PATH,
    },
    data: {
      sunlightTimes: sunlightTimes.slice(0, days),
      weather,
    },
  });
};

export default getWeatherHandler;
