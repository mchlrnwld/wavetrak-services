import { getWindForecasts } from '../../../external/forecasts';
import { createUTCOffsetCalculator, dateRange, fromUnixTimestamp } from '../../../utils/datetime';
import utcOffsetFromTimezone from '../../../utils/utcOffsetFromTimezone';
import { getOptimalWindScore } from '../../../utils/getOptimalScore';
import { getSpeedInKnots } from '../../../utils/convertUnits';

const getWindHandler = async (req, res) => {
  const {
    units,
    forecast: { days, intervalHours },
    spot,
  } = req;

  const { start, end } = dateRange(days, spot.timezone);

  const forecast = await getWindForecasts({
    units,
    pointOfInterestId: spot.pointOfInterestId,
    start,
    end,
    interval: intervalHours * 60,
  });

  const utcOffsetCalculator = createUTCOffsetCalculator(spot.timezone);

  const data = forecast.data.map(
    ({ timestamp, wind: { speed, direction, directionType, gust } }) => ({
      timestamp,
      utcOffset: utcOffsetCalculator(fromUnixTimestamp(timestamp)),
      speed,
      direction,
      directionType,
      gust,
      optimalScore: getOptimalWindScore(
        getSpeedInKnots(units.windSpeed, speed),
        direction,
        spot.best.windDirection,
      ),
    }),
  );

  return res.send({
    associated: {
      units,
      // TODO: Deprecate this property.
      utcOffset: utcOffsetFromTimezone(spot.timezone),
      location: {
        lon: spot.location.coordinates[0],
        lat: spot.location.coordinates[1],
      },
    },
    data: {
      wind: data,
    },
  });
};

export default getWindHandler;
