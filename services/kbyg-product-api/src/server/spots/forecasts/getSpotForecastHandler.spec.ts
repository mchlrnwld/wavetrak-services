import chai, { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import express from 'express';
import head from 'lodash/head';
import last from 'lodash/last';
import { authenticateRequest } from '@surfline/services-common';
import createParamString from '../../../external/createParamString';
import userUnits from '../../middleware/userUnits';
import getSpot from '../../middleware/getSpot';
import * as getSpotForecasts from '../helpers/getSpotForecasts';
import { WEATHER_ICON_PATH } from '../../../common/constants';
import { secondsPerHour, secondsPerDay } from '../../../utils/datetime';
import forecasts from './forecasts';
import { northOCSpot } from '../../../fixtures/spots';

interface Response {
  body: {
    data: {
      forecasts: {
        timestamp: number;
      }[];
    };
  };
}

const getSpotForecastHandlerSpec = () => {
  let clock;
  let request;
  let getSpotForecastsStub;

  const start = 1493708400; // 05/02/2017@00:00PDT
  const end3DaysPlus1Step =
    start + // 05/05/2017@03:00PDT
    3 * secondsPerDay +
    3 * secondsPerHour;
  const end17DaysPlus1Step = start + 17 * secondsPerDay + secondsPerHour; // 05/19/2017/01:00PDT
  const waveForecastMap = new Map(
    [...Array((end17DaysPlus1Step - start) / 3600)].map((_, i) => [
      start + i * 3600,
      {
        surf: { min: null, max: null },
        wave: { components: [] },
      },
    ]),
  );
  const windForecastMap = new Map(
    [...Array((end17DaysPlus1Step - start) / 3600)].map((_, i) => [
      start + i * 3600,
      {
        wind: { speed: null, direction: null },
      },
    ]),
  );
  const weatherForecastMap = new Map(
    [...Array((end17DaysPlus1Step - start) / 3600)].map((_, i) => [
      start + i * 3600,
      {
        weather: {
          temperature: null,
          skycondition: {
            text: null,
          },
        },
      },
    ]),
  );
  const tides = deepFreeze([]);

  before(() => {
    const app = express();
    const log = { info: sinon.spy() };
    app.use(authenticateRequest(), userUnits, getSpot, forecasts(log));
    request = chai.request(app).keepOpen().keepOpen();
    clock = sinon.useFakeTimers(start * 1000);
  });

  beforeEach(() => {
    getSpotForecastsStub = sinon.stub(getSpotForecasts, 'default');
  });

  afterEach(() => {
    getSpotForecastsStub.restore();
  });

  after(() => {
    clock.restore();
  });

  it('should return spot forecasts for spot ID', async () => {
    getSpotForecastsStub.resolves({
      tideLocation: null,
      waveForecastMap,
      weatherForecastMap,
      windForecastMap,
      tides,
    });

    const params = {
      spotId: northOCSpot._id,
      start,
      end: end3DaysPlus1Step,
    };

    const url = `/?${createParamString(params)}`;

    const res = await request.get(url).send();
    expect(res.body.associated.units).to.deep.equal({
      swellHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KPH',
    });
    expect(res.body.associated.utcOffset).to.equal(-7);
    expect(res.body.associated.weatherIconPath).to.equal(WEATHER_ICON_PATH);
    expect(res.body.data.tideLocation).to.be.null();
    expect(res.body.data.forecasts).to.be.ok();
    expect(res.body.data.tides).to.be.ok();
    expect(res.body.data.sunriseSunsetTimes).to.be.ok();
    expect(getSpotForecasts.default).to.have.been.calledOnce();
    expect(getSpotForecasts.default).to.have.been.calledWithExactly({
      pointOfInterestId: 'd1c53ea2-0f77-4d5f-b64c-be16e75966cb',
      tideStation: 'Huntington Cliffs near Newport Bay',
      start,
      end: end3DaysPlus1Step,
      interval: 3 * secondsPerHour,
      units: {
        swellHeight: 'M',
        temperature: 'C',
        tideHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      },
    });
  });

  it('should return data within allowable start and end range', async () => {
    getSpotForecastsStub.resolves({
      waveForecastMap,
      weatherForecastMap,
      windForecastMap,
      tides,
    });

    const paramsWithinAllowableRange = {
      spotId: northOCSpot._id,
      start: start + 10800,
      end: end3DaysPlus1Step - 10800,
    };
    const resWithinAllowableRange: Response = await request
      .get(`/?${createParamString(paramsWithinAllowableRange)}`)
      .send();
    expect(head(resWithinAllowableRange.body.data.forecasts).timestamp).to.equal(start + 10800);
    expect(last(resWithinAllowableRange.body.data.forecasts).timestamp).to.equal(
      end3DaysPlus1Step - 2 * 10800,
    );
    expect(getSpotForecastsStub).to.have.been.calledOnce();
    expect(getSpotForecastsStub.firstCall.args[0].start).to.equal(start + 10800);
    expect(getSpotForecastsStub.firstCall.args[0].end).to.equal(end3DaysPlus1Step - 10800);

    const paramsOutsideAllowableRange = {
      spotId: northOCSpot._id,
      start: start - 10800,
      end: end3DaysPlus1Step + 10800,
    };
    const resOutsideAllowableRange: Response = await request
      .get(`/?${createParamString(paramsOutsideAllowableRange)}`)
      .send();
    expect(head(resOutsideAllowableRange.body.data.forecasts).timestamp).to.equal(start);
    expect(last(resOutsideAllowableRange.body.data.forecasts).timestamp).to.equal(
      end3DaysPlus1Step - 10800,
    );
    expect(getSpotForecastsStub).to.have.been.calledTwice();
    expect(getSpotForecastsStub.secondCall.args[0].start).to.equal(start);
    expect(getSpotForecastsStub.secondCall.args[0].end).to.equal(end3DaysPlus1Step);
  });

  it('should return forecast data at defined interval depending on user', async () => {
    const premiumUserId = '582495992d06cd3b71d66229';
    const nonPremiumUserId = '56fae6ff19b74318a73a7875';
    getSpotForecastsStub.resolves({
      waveForecastMap,
      weatherForecastMap,
      windForecastMap,
      tides,
    });

    const premiumParams = {
      spotId: northOCSpot._id,
      start,
      end: end17DaysPlus1Step,
    };
    const premiumRes = await request
      .get(`/?${createParamString(premiumParams)}`)
      .set('X-Auth-UserId', premiumUserId)
      .send();
    expect(premiumRes.body.data.forecasts.length).to.equal(409);
    expect(getSpotForecastsStub).to.have.been.calledOnce();
    expect(getSpotForecastsStub.firstCall.args[0].interval).to.equal(secondsPerHour);

    const nonPremiumParams = {
      spotId: northOCSpot._id,
      start,
      end: end3DaysPlus1Step,
    };
    const nonPremiumRes = await request
      .get(`/?${createParamString(nonPremiumParams)}`)
      .set('X-Auth-UserId', nonPremiumUserId)
      .send();
    expect(nonPremiumRes.body.data.forecasts.length).to.equal(25);
    expect(getSpotForecastsStub).to.have.been.calledTwice();
    expect(getSpotForecastsStub.secondCall.args[0].interval).to.equal(3 * secondsPerHour);
  });

  it('should force start to match a step from current time', async () => {
    const premiumUserId = '582495992d06cd3b71d66229';
    const nonPremiumUserId = '56fae6ff19b74318a73a7875';
    getSpotForecastsStub.resolves({
      windForecastMap,
      weatherForecastMap,
      waveForecastMap,
      tides,
    });

    const premiumParams = {
      spotId: northOCSpot._id,
      start: start + 3600,
      end: end17DaysPlus1Step,
    };
    const premiumRes: Response = await request
      .get(`/?${createParamString(premiumParams)}`)
      .set('X-Auth-UserId', premiumUserId)
      .send();
    expect(head(premiumRes.body.data.forecasts).timestamp).to.equal(start + 3600);
    expect(getSpotForecastsStub).to.have.been.calledOnce();
    expect(getSpotForecastsStub.firstCall.args[0].start).to.equal(start + 3600);

    const nonPremiumParams = {
      spotId: northOCSpot._id,
      start: start + 3600,
      end: end3DaysPlus1Step,
    };
    const nonPremiumRes: Response = await request
      .get(`/?${createParamString(nonPremiumParams)}`)
      .set('X-Auth-UserId', nonPremiumUserId);
    expect(head(nonPremiumRes.body.data.forecasts).timestamp).to.equal(start);
    expect(getSpotForecastsStub).to.have.been.calledTwice();
    expect(getSpotForecastsStub.secondCall.args[0].start).to.equal(start);
  });

  it('should parse start/end parameters as integers to prevent 500 errors', async () => {
    getSpotForecastsStub.resolves({
      waveForecastMap,
      weatherForecastMap,
      windForecastMap,
      tides,
    });
    const params = {
      spotId: northOCSpot._id,
      start: start + 0.1,
      end: end3DaysPlus1Step + 0.2,
    };
    await request.get(`/?${createParamString(params)}`).send();
    expect(getSpotForecastsStub).to.have.been.calledOnce();
    expect(getSpotForecastsStub.firstCall.args[0].start).to.equal(start);
    expect(getSpotForecastsStub.firstCall.args[0].end).to.equal(end3DaysPlus1Step);
  });
};

export default getSpotForecastHandlerSpec;
