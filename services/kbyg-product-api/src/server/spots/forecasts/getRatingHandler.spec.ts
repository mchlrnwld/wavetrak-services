import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import createParamString from '../../../external/createParamString';
import * as forecastsAPI from '../../../external/forecasts/forecasts';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSpot from '../../middleware/getSpot';
import forecasts from './forecasts';
import ratingFixture from './fixtures/rating.json';

const getRatingHandlerSpec = () => {
  describe('/rating', () => {
    let clock;
    let request;
    let getRatingForecastsStub;
    const now = new Date('2021-03-15T00:00:00Z').getTime() / 1000; // 03/14/2021@17:00PDT
    const expectedStart = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021@00:00PST
    const expectedEnd = new Date('2021-03-16T07:00:00Z').getTime() / 1000; // 03/16/2021@00:00PDT
    const params = {
      spotId: '5842041f4e65fad6a77088ed',
      days: 2,
      intervalHours: 12,
      corrected: 'true',
      correctedSurf: 'true',
    };
    const url = `/rating?${createParamString(params)}`;

    before(() => {
      const app = express();
      const log = { info: sinon.spy() };
      app.use(userUnits, entitlements, checkForecastParams, getSpot, forecasts(log));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(now * 1000);
    });

    beforeEach(() => {
      getRatingForecastsStub = sinon.stub(forecastsAPI, 'getRatingForecasts');
    });

    afterEach(() => {
      getRatingForecastsStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns rating forecast data', async () => {
      getRatingForecastsStub.resolves(ratingFixture);

      const res = await request.get(url).send();

      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.location).to.deep.equal({
        lat: 33.656781041213,
        lon: -118.0064678192,
      });
      expect(res.body.data).to.be.ok();
      expect(res.body.data.rating).to.be.ok();
      expect(res.body.data.rating).to.have.length(4);
      expect(res.body.data.rating[0]).to.deep.equal({
        timestamp: 1615708800,
        utcOffset: -8,
        rating: {
          key: 'VERY_POOR',
          value: 1.1,
        },
      });
      expect(res.body.data.rating[1]).to.deep.equal({
        timestamp: 1615748400,
        utcOffset: -7,
        rating: {
          key: 'POOR',
          value: 1.9,
        },
      });
      expect(res.body.data.rating[2]).to.deep.equal({
        timestamp: 1615791600,
        utcOffset: -7,
        rating: {
          key: 'POOR_TO_FAIR',
          value: 3.2,
        },
      });
      expect(res.body.data.rating[3]).to.deep.equal({
        timestamp: 1615834800,
        utcOffset: -7,
        rating: {
          key: 'FAIR',
          value: 4.4,
        },
      });

      const getRatingForecastsArgs = getRatingForecastsStub.firstCall.args[0];
      expect(getRatingForecastsArgs.start).to.equal(expectedStart);
      expect(getRatingForecastsArgs.end).to.equal(expectedEnd);
      expect(getRatingForecastsArgs.interval).to.equal(params.intervalHours * 60);
      expect(getRatingForecastsArgs.corrected).to.be.true();
      expect(getRatingForecastsArgs.correctedSurf).to.be.true();
    });

    it('does not fail on null ratings data', async () => {
      getRatingForecastsStub.resolves({ data: null });

      const res = await request.get(url).send();

      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.location).to.deep.equal({
        lat: 33.656781041213,
        lon: -118.0064678192,
      });
      expect(res.body.data).to.deep.equal({ rating: null });
    });

    it('defaults the corrected and correctedSurf params to true if not specified', async () => {
      getRatingForecastsStub.resolves(ratingFixture);

      const paramsNoCorrectedArgs = {
        spotId: '5842041f4e65fad6a77088ed',
        days: 2,
        intervalHours: 12,
      };
      const urlNoCorrectedArgs = `/rating?${createParamString(paramsNoCorrectedArgs)}`;
      const res = await request.get(urlNoCorrectedArgs).send();

      expect(res).to.have.status(200);

      const getRatingForecastsArgs = getRatingForecastsStub.firstCall.args[0];
      expect(getRatingForecastsArgs.corrected).to.be.true();
      expect(getRatingForecastsArgs.correctedSurf).to.be.true();
    });
  });
};

export default getRatingHandlerSpec;
