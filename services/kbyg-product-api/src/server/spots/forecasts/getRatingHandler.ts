import { getRatingForecasts } from '../../../external/forecasts';
import { createUTCOffsetCalculator, dateRange, fromUnixTimestamp } from '../../../utils/datetime';

const getRatingHandler = async (req, res) => {
  const {
    forecast: { days, intervalHours },
    spot,
  } = req;

  const { ratingsAlgorithm, corrected = 'true', correctedSurf = 'true' } = req.query;

  const { start, end } = dateRange(days, spot.timezone);

  const forecast = await getRatingForecasts({
    pointOfInterestId: spot.pointOfInterestId,
    start,
    end,
    interval: intervalHours * 60,
    ratingsAlgorithm,
    corrected: corrected === 'true',
    correctedSurf: correctedSurf === 'true',
  });

  const utcOffsetCalculator = createUTCOffsetCalculator(spot.timezone);

  const data = forecast.data
    ? forecast.data.map(({ timestamp, rating }) => ({
        timestamp,
        utcOffset: utcOffsetCalculator(fromUnixTimestamp(timestamp)),
        rating,
      }))
    : null;

  return res.send({
    associated: {
      location: {
        lon: spot.location.coordinates[0],
        lat: spot.location.coordinates[1],
      },
    },
    data: {
      rating: data,
    },
  });
};

export default getRatingHandler;
