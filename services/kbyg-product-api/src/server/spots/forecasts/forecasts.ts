import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import correctedWaveHeights from '../../middleware/correctedWaveHeights';
import getSpotForecastHandler from './getSpotForecastHandler';
import getConditionsHandler from './getConditionsHandler';
import getRatingHandler from './getRatingHandler';
import getTidesHandler from './getTidesHandler';
import getWaveHandler from './getWaveHandler';
import getWeatherHandler from './getWeatherHandler';
import getWindHandler from './getWindHandler';
import trackSpotForecastRequests from './trackSpotForecastRequests';

const forecasts = (log) => {
  const api = Router();

  api.use('*', trackSpotForecastRequests(log));
  api.get('/', wrapErrors(getSpotForecastHandler));
  api.get('/conditions', wrapErrors(getConditionsHandler));
  api.get('/rating', wrapErrors(getRatingHandler));
  api.get('/tides', wrapErrors(getTidesHandler));
  api.get('/wave', correctedWaveHeights, wrapErrors(getWaveHandler));
  api.get('/weather', wrapErrors(getWeatherHandler));
  api.get('/wind', wrapErrors(getWindHandler));

  return api;
};

export default forecasts;
