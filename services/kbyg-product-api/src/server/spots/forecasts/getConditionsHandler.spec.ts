import chai, { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import sinon from 'sinon';
import express from 'express';
import moment from 'moment';
import { authenticateRequest } from '@surfline/services-common';
import createParamString from '../../../external/createParamString';
import * as SubregionsAPI from '../../../external/subregions';
import * as getConditions from '../../../model/getLOLAConditions/getConditions';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSpot from '../../middleware/getSpot';
import subregionForecastsFixture from '../../regions/fixtures/subregionForecasts.json';

import forecasts from './forecasts';
import { LOLAConditionsLengthErrorHandler } from '../../../errors/LOLAConditionsLengthError';
import { northOCSpot } from '../../../fixtures/spots';

const getSpotForecastConditionsHandlerSpec = () => {
  describe('/conditions', () => {
    let request;
    let clock;
    let getConditionsStub;
    let getSubregionForecastsStub;
    const start = deepFreeze(moment('2021-09-16 00:00:00').tz('UTC'));
    const spotConditionsFixture = (startsAt = start, days = 3) => ({
      start: startsAt.clone().unix(),
      conditions: [...Array(days)].map((_, i) => ({
        forecastDate: startsAt.clone().add(i, 'days').unix(),
        am: {
          timestamp: startsAt.clone().add(i, 'days').add(6, 'hours').unix(),
          rating: 'GOOD_TO_EPIC',
          minHeight: 1,
          maxHeight: 2,
          humanRelation: 'knee to thigh high',
          relation: 'waist to stomach high',
          observation:
            'Small short period wind waves from the N. Use extreme caution. Gale warnign with dangerous seas. Winds from the WNW.',
        },
        pm: {
          timestamp: startsAt.clone().add(i, 'days').add(12, 'hours').unix(),
          rating: 'GOOD_TO_EPIC',
          minHeight: 1,
          maxHeight: 2,
          humanRelation: 'knee to thigh high',
          relation: 'waist to stomach high',
          observation:
            'Small short period wind waves from the N. Use extreme caution. Gale warnign with dangerous seas. Winds from the WNW.',
        },
      })),
    });

    before(() => {
      const app = express();
      const log = { info: sinon.spy() };
      app.use(
        authenticateRequest(),
        userUnits,
        entitlements,
        checkForecastParams,
        getSpot,
        forecasts(log),
      );
      app.use(LOLAConditionsLengthErrorHandler);
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      const startDate = moment('2021-09-15').tz('America/Los_Angeles').toDate();
      clock = sinon.useFakeTimers(startDate);
      getConditionsStub = sinon.stub(getConditions, 'default');
      getSubregionForecastsStub = sinon.stub(SubregionsAPI, 'getSubregionForecasts');
    });

    afterEach(() => {
      clock.restore();
      getConditionsStub.restore();
      getSubregionForecastsStub.restore();
    });

    it('should return conditions given a spotId', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { spotId: northOCSpot._id };

      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data).to.be.ok();
      expect(res.body.data.conditions).to.be.ok();
      expect(res.body.data.conditions).to.have.length(17);
      expect(res.body.data.conditions[0].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[1].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[2].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[3].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[4].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[5].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[6].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[7].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[8].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[9].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[10].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[11].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[12].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[13].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[14].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[15].utcOffset).to.equal(-7);
      expect(res.body.data.conditions[16].utcOffset).to.equal(-7);

      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-09-15');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-09-16');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-09-17');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-09-18');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-09-19');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-09-20');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-09-21');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-09-22');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-09-23');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-09-24');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-09-25');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-09-26');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-09-27');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-09-28');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-09-29');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-09-30');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-10-01');
    });

    it('should return old ratings scale by default', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { spotId: northOCSpot._id };

      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data).to.be.ok();
      expect(res.body.data.conditions).to.be.ok();
      expect(res.body.data.conditions).to.have.length(17);

      expect(res.body.data.conditions[0].am.rating).to.equal('POOR');
      expect(res.body.data.conditions[0].pm.rating).to.equal('POOR_TO_FAIR');

      expect(res.body.data.conditions[1].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[1].pm.rating).to.equal('FAIR_TO_GOOD');

      expect(res.body.data.conditions[2].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[2].pm.rating).to.equal('VERY_GOOD');
    });

    it('should return old ratings scale if sevenRatings param is false', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { spotId: northOCSpot._id, sevenRatings: false };

      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data).to.be.ok();
      expect(res.body.data.conditions).to.be.ok();
      expect(res.body.data.conditions).to.have.length(17);

      expect(res.body.data.conditions[0].am.rating).to.equal('POOR');
      expect(res.body.data.conditions[0].pm.rating).to.equal('POOR_TO_FAIR');

      expect(res.body.data.conditions[1].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[1].pm.rating).to.equal('FAIR_TO_GOOD');

      expect(res.body.data.conditions[2].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[2].pm.rating).to.equal('VERY_GOOD');
    });

    it('should return new ratings scale if sevenRatings param is true', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { spotId: northOCSpot._id, sevenRatings: true };

      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data).to.be.ok();
      expect(res.body.data.conditions).to.be.ok();
      expect(res.body.data.conditions).to.have.length(17);

      expect(res.body.data.conditions[0].am.rating).to.equal('POOR');
      expect(res.body.data.conditions[0].pm.rating).to.equal('POOR_TO_FAIR');

      expect(res.body.data.conditions[1].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[1].pm.rating).to.equal('FAIR_TO_GOOD');

      expect(res.body.data.conditions[2].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[2].pm.rating).to.equal('GOOD');
    });

    it('should return 17 days for a premium user', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { spotId: northOCSpot._id };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-09-15');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-09-16');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-09-17');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-09-18');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-09-19');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-09-20');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-09-21');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-09-22');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-09-23');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-09-24');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-09-25');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-09-26');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-09-27');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-09-28');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-09-29');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-09-30');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-10-01');
    });

    it('should fail when data does not match', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start.clone().add(1, 'days'), 1));

      const params = { spotId: northOCSpot._id };

      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.have.status(500);
      expect(res).to.be.json();
      expect(res.body.message).to.equal(
        'Mismatch: Data length. Could not aggregate all required data components.',
      );
    });
  });
};

export default getSpotForecastConditionsHandlerSpec;
