import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import createParamString from '../../../external/createParamString';
import * as tidesAPI from '../../../external/tides';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSpot from '../../middleware/getSpot';
import forecasts from './forecasts';
import tidesFixture from './fixtures/tides.json';

const getTidesHandlerSpec = () => {
  describe('/tides', () => {
    let clock;
    let request;
    let tidesAPIStub;
    const now = new Date('2021-03-15T00:00:00Z').getTime() / 1000; // 03/14/2021@17:00PDT
    const expectedStart = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021@00:00PST
    const expectedEnd = new Date('2021-03-16T07:00:00Z').getTime() / 1000; // 03/16/2021@00:00PDT

    const params = {
      spotId: '5842041f4e65fad6a77088ed',
      days: 2,
    };
    const url = `/tides?${createParamString(params)}`;

    before(() => {
      const app = express();
      const log = { info: sinon.spy() };
      app.use(userUnits, entitlements, checkForecastParams, getSpot, forecasts(log));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(now * 1000);
    });

    beforeEach(() => {
      tidesAPIStub = sinon.stub(tidesAPI, 'default');
    });

    afterEach(() => {
      tidesAPIStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns tides forecast data', async () => {
      tidesAPIStub.resolves(tidesFixture);

      const res = await request.get(url).send();

      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.be.ok();
      expect(res.body.associated.utcOffset).to.equal(-7);
      expect(res.body.associated.tideLocation).to.deep.equal({
        name: 'Huntington Cliffs near Newport Bay',
        min: -1.08,
        max: 2.76,
        lat: 33.6603,
        lon: -117.998,
        mean: 0.84,
      });
      expect(res.body.data).to.be.ok();
      expect(res.body.data.tides).to.be.ok();
      expect(res.body.data.tides).to.have.length(4);
      expect(res.body.data.tides[0].utcOffset).to.equal(-8);
      expect(res.body.data.tides[1].utcOffset).to.equal(-7);
      expect(res.body.data.tides[2].utcOffset).to.equal(-7);
      expect(res.body.data.tides[3].utcOffset).to.equal(-7);

      const getTidesArgs = tidesAPIStub.firstCall.args[0];
      expect(getTidesArgs.start).to.equal(expectedStart);
      expect(getTidesArgs.end).to.equal(expectedEnd);
      expect(getTidesArgs.location_name).to.equal('Huntington Cliffs near Newport Bay');
    });
  });
};

export default getTidesHandlerSpec;
