import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import createParamString from '../../../external/createParamString';
import * as forecastsAPI from '../../../external/forecasts/forecasts';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSpot from '../../middleware/getSpot';
import forecasts from './forecasts';
import windFixture from './fixtures/wind.json';

const getWindHandlerSpec = () => {
  describe('/wind', () => {
    let clock;
    let request;
    let getWindForecastsStub;
    const now = new Date('2021-03-15T00:00:00Z').getTime() / 1000; // 03/14/2021@17:00PDT
    const expectedStart = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021@00:00PST
    const expectedEnd = new Date('2021-03-16T07:00:00Z').getTime() / 1000; // 03/16/2021@00:00PDT
    const params = {
      spotId: '5842041f4e65fad6a77088ed',
      days: 2,
      intervalHours: 12,
    };
    const url = `/wind?${createParamString(params)}`;

    before(() => {
      const app = express();
      const log = { info: sinon.spy() };
      app.use(userUnits, entitlements, checkForecastParams, getSpot, forecasts(log));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(now * 1000);
    });

    beforeEach(() => {
      getWindForecastsStub = sinon.stub(forecastsAPI, 'getWindForecasts');
    });

    afterEach(() => {
      getWindForecastsStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns wind forecast data with UTC offsets applied to each timestep', async () => {
      getWindForecastsStub.resolves(windFixture);

      const res = await request.get(url).send();

      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.be.ok();
      expect(res.body.associated.utcOffset).to.equal(-7);
      expect(res.body.associated.location).to.deep.equal({
        lat: 33.656781041213,
        lon: -118.0064678192,
      });
      expect(res.body.data).to.be.ok();
      expect(res.body.data.wind).to.be.ok();
      expect(res.body.data.wind).to.have.length(4);
      expect(res.body.data.wind[0].utcOffset).to.equal(-8);
      expect(res.body.data.wind[1].utcOffset).to.equal(-7);
      expect(res.body.data.wind[2].utcOffset).to.equal(-7);
      expect(res.body.data.wind[3].utcOffset).to.equal(-7);

      const getWindForecastsArgs = getWindForecastsStub.firstCall.args[0];
      expect(getWindForecastsArgs.start).to.equal(expectedStart);
      expect(getWindForecastsArgs.end).to.equal(expectedEnd);
      expect(getWindForecastsArgs.interval).to.equal(params.intervalHours * 60);
    });
  });
};

export default getWindHandlerSpec;
