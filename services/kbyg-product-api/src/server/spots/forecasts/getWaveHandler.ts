import waveForecasts from '../../../model/waveForecasts';
import limitToMaxWaveHeights from '../../../utils/limitToMaxWaveHeights';

const getWaveHandler = async (req, res) => {
  const {
    units,
    forecast: { days, intervalHours, maxHeights },
    spot,
    correctedWaveHeights,
  } = req;
  const { thirdParty } = req.query;

  const { utcOffset, offshoreLocation, data } = await waveForecasts(
    {
      spot,
      days,
      intervalHours,
      units,
    },
    correctedWaveHeights,
    thirdParty,
  );

  return res.send({
    associated: {
      units,
      utcOffset,
      location: {
        lon: spot.location.coordinates[0],
        lat: spot.location.coordinates[1],
      },
      forecastLocation: {
        lon: spot.forecastLocation.coordinates[0],
        lat: spot.forecastLocation.coordinates[1],
      },
      offshoreLocation: {
        lon: offshoreLocation.lon,
        lat: offshoreLocation.lat,
      },
    },
    data: {
      wave: maxHeights ? limitToMaxWaveHeights(data) : data,
    },
  });
};

export default getWaveHandler;
