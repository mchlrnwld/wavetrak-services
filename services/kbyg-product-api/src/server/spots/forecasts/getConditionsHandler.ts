import getLOLAConditions from '../../../model/getLOLAConditions';
import utcOffsetFromTimezone from '../../../utils/utcOffsetFromTimezone';
import { createUTCOffsetCalculator, fromUnixTimestamp } from '../../../utils/datetime';
import convertTo7Ratings from '../../../utils/convertTo7Ratings';

const getConditionsHandler = async (req, res) => {
  const {
    units,
    forecast: { days },
    spot,
  } = req;

  const sevenRatings = req.query.sevenRatings === 'true';

  const conditions = await getLOLAConditions({
    spot,
    days,
    units,
  });

  const utcOffsetCalculator = createUTCOffsetCalculator(spot.timezone);

  return res.send({
    associated: {
      units,
      utcOffset: utcOffsetFromTimezone(spot.timezone),
    },
    data: {
      conditions: conditions.map((condition) => ({
        ...condition,
        timestamp: condition.timestamp,
        utcOffset: utcOffsetCalculator(fromUnixTimestamp(condition.timestamp)),
        am: sevenRatings
          ? { ...condition.am, rating: convertTo7Ratings(condition.am.rating) }
          : condition.am,
        pm: sevenRatings
          ? { ...condition.pm, rating: convertTo7Ratings(condition.pm.rating) }
          : condition.pm,
      })),
    },
  });
};

export default getConditionsHandler;
