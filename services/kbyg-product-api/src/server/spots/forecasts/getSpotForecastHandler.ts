import { round } from 'lodash';
import getSpotForecasts from '../helpers/getSpotForecasts';
import allowableForecastRange from '../helpers/allowableForecastRange';
import buildSunlightTimes from '../helpers/buildSunlightTimes';
import getClosestValidStartTimestamp from '../helpers/getClosestValidStartTimestamp';
import weatherCondition from '../../../utils/weatherCondition';
import { fromUnixTimestamp, day } from '../../../utils/datetime';
import utcOffsetFromTimezone from '../../../utils/utcOffsetFromTimezone';
import { WEATHER_ICON_PATH } from '../../../common/constants';

const getSpotForecastHandler = async (req, res) => {
  const utcOffset = utcOffsetFromTimezone(req.spot.timezone);
  const { user } = req;
  const { minStart, maxEnd, interval } = await allowableForecastRange(
    req.spot.timezone,
    req.authenticatedUserId,
    user,
  );

  const requestedStart = parseInt(req.query.start, 10) || minStart;
  const requestedEnd = parseInt(req.query.end, 10) || maxEnd;

  const start = getClosestValidStartTimestamp(minStart, interval, requestedStart);
  const end = Math.min(requestedEnd, maxEnd);

  const { tideLocation, waveForecastMap, weatherForecastMap, windForecastMap, tides } =
    await getSpotForecasts({
      pointOfInterestId: req.spot.pointOfInterestId,
      tideStation: req.spot.tideStation,
      start,
      end,
      interval,
      units: req.units,
    });

  const sunriseSunsetTimes = buildSunlightTimes({
    start,
    end,
    timezone: req.spot.timezone,
    lat: req.spot.location.coordinates[1],
    lon: req.spot.location.coordinates[0],
  }).map(({ midnight, sunrise, sunset }) => ({
    midnight,
    sunrise,
    sunset,
  }));

  const sunriseSunsetTimesMap = new Map(
    sunriseSunsetTimes.map(({ midnight, sunrise, sunset }) => [
      day(req.spot.timezone, fromUnixTimestamp(midnight)),
      { sunrise, sunset },
    ]),
  );

  const numberOfForecasts = Math.floor((end - start) / interval);
  const forecasts = [...Array(numberOfForecasts)].map((_, i) => {
    const timestamp = start + i * interval;
    const waveForecast = waveForecastMap.get(timestamp);
    const weatherForecast = weatherForecastMap.get(timestamp);
    const windForecast = windForecastMap.get(timestamp);
    const { sunrise, sunset } = sunriseSunsetTimesMap.get(
      day(req.spot.timezone, fromUnixTimestamp(timestamp)),
    );
    return {
      timestamp,
      weather: {
        temperature: round(weatherForecast.weather.temperature),
        condition: weatherCondition(
          weatherForecast.weather.conditions,
          timestamp < sunrise || sunset <= timestamp,
        ),
      },
      wind: {
        speed: windForecast.wind.speed,
        direction: windForecast.wind.direction,
      },
      surf: {
        min: waveForecast.surf.min,
        max: waveForecast.surf.max,
      },
      swells: waveForecast.wave.components.map((swell) => ({
        height: swell.height,
        direction: swell.direction.mean,
        directionMin: swell.direction.min,
        period: swell.period,
      })),
    };
  });

  return res.send({
    associated: {
      units: req.units,
      utcOffset,
      weatherIconPath: WEATHER_ICON_PATH,
    },
    // TODO: Deprecate this. Left for backwards compatibility.
    units: req.units,
    utcOffset,
    data: {
      sunriseSunsetTimes,
      tideLocation,
      forecasts,
      tides,
    },
  });
};

export default getSpotForecastHandler;
