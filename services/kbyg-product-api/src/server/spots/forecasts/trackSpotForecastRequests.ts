import type { NextFunction, Response } from 'express';
import { KBYGSpotForecastRequest } from '../../types';

const trackSpotForecastRequests =
  (log) => (req: KBYGSpotForecastRequest, _res: Response, next: NextFunction) => {
    const { units, forecast, spot } = req;

    log.info({
      action: `/kbyg${req.baseUrl}`,
      spotId: spot._id,
      forecast,
      query: req.query,
      units,
    });

    return next();
  };

export default trackSpotForecastRequests;
