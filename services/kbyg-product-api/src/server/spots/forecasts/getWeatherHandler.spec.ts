import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';

import createParamString from '../../../external/createParamString';
import * as forecastsAPI from '../../../external/forecasts/forecasts';

import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import getSpot from '../../middleware/getSpot';
import checkForecastParams from '../../middleware/checkForecastParams';

import forecasts from './forecasts';
import * as spotsAPI from '../../../external/spots';
import spotFixture from '../../../utils/fixtures/spots.json';
import { WEATHER_ICON_PATH } from '../../../common/constants';

const getWeatherHandlerSpec = () => {
  describe('/weather', () => {
    /**  type {sinon.SinonFakeTimers} */
    let clock;

    let request;

    const params = {
      spotId: '5842041f4e65fad6a77088ed',
      days: 2,
      intervalHours: 12,
    };

    const url = `/weather?${createParamString(params)}`;

    /** @type {sinon.SinonStub} */
    let getSpotStub;

    /** @type {sinon.SinonStub} */
    let getWeatherForecastsStub;

    before(() => {
      const app = express();
      const log = { info: sinon.spy() };
      app.use(userUnits, entitlements, checkForecastParams, getSpot, forecasts(log));

      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      spotsAPI.getSpot.restore();
      clock = sinon.useFakeTimers();
      getWeatherForecastsStub = sinon.stub(forecastsAPI, 'getWeatherForecasts');
      getSpotStub = sinon.stub(spotsAPI, 'getSpot').resolves(spotFixture[0]);
    });

    afterEach(() => {
      clock.restore();
      getWeatherForecastsStub.restore();
      getSpotStub.restore();
    });

    it('returns weather forecast data with UTC offsets applied to each timestep', async () => {
      const now = new Date('2021-03-14T12:00:00Z').getTime() / 1000; // 03/14/2021 04:00 PDT
      const expectedStart = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021 00:00 PST
      const expectedEnd = new Date('2021-03-16T07:00:00Z').getTime() / 1000; // 03/16/2021 00:00 PDT

      const weatherDataFixture = {
        data: [
          {
            timestamp: 1615708800,
            weather: {
              temperature: 23.4,
              conditions: 'OVERCAST_NO_RAIN',
            },
          },
          {
            timestamp: 1615748400,
            weather: {
              temperature: 23.5,
              conditions: 'OVERCAST_NO_RAIN',
            },
          },
          {
            timestamp: 1615791600,
            weather: {
              temperature: 23.6,
              conditions: 'OVERCAST_NO_RAIN',
            },
          },
          {
            timestamp: 1615834800,
            weather: {
              temperature: 23.4,
              conditions: 'OVERCAST_NO_RAIN',
            },
          },
        ],
      };

      clock.setSystemTime(now * 1000);
      getWeatherForecastsStub.resolves(weatherDataFixture);

      const res = await request.get(url).send();

      const expectedUnits = {
        temperature: 'C',
        tideHeight: 'M',
        swellHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      };

      const expectedSunlightTimesReponse = [
        {
          midnight: 1615708800,
          midnightUTCOffset: -8,
          dawn: 1615729193,
          dawnUTCOffset: -7,
          sunrise: 1615730683,
          sunriseUTCOffset: -7,
          sunset: 1615773614,
          sunsetUTCOffset: -7,
          dusk: 1615775104,
          duskUTCOffset: -7,
        },
        {
          midnight: 1615791600,
          midnightUTCOffset: -7,
          dawn: 1615815512,
          dawnUTCOffset: -7,
          sunrise: 1615817002,
          sunriseUTCOffset: -7,
          sunset: 1615860060,
          sunsetUTCOffset: -7,
          dusk: 1615861550,
          duskUTCOffset: -7,
        },
      ];

      const expectedWeatherResponse = [
        {
          timestamp: 1615708800,
          utcOffset: -8,
          temperature: 23.4,
          condition: 'NIGHT_OVERCAST_NO_RAIN',
        },
        {
          timestamp: 1615748400,
          utcOffset: -7,
          temperature: 23.5,
          condition: 'OVERCAST_NO_RAIN',
        },
        {
          timestamp: 1615791600,
          utcOffset: -7,
          temperature: 23.6,
          condition: 'NIGHT_OVERCAST_NO_RAIN',
        },
        {
          timestamp: 1615834800,
          utcOffset: -7,
          temperature: 23.4,
          condition: 'OVERCAST_NO_RAIN',
        },
      ];

      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.deep.equal(expectedUnits);
      expect(res.body.associated.utcOffset).to.equal(-7);
      expect(res.body.associated.weatherIconPath).to.equal(WEATHER_ICON_PATH);
      expect(res.body.data).to.be.ok();

      expect(res.body.data.sunlightTimes).to.deep.equal(expectedSunlightTimesReponse);
      expect(res.body.data.weather).to.deep.equal(expectedWeatherResponse);

      const getWeatherForecastsArgs = getWeatherForecastsStub.firstCall.args[0];
      expect(getWeatherForecastsArgs.start).to.equal(expectedStart);
      expect(getWeatherForecastsArgs.end).to.equal(expectedEnd);
      expect(getWeatherForecastsArgs.interval).to.equal(params.intervalHours * 60);
    });

    it('should handle when PDT falls back to PST', async () => {
      const now = new Date('2021-11-06T12:00:00Z').getTime() / 1000; // 11/06/2021 05:00 PDT
      const expectedStart = new Date('2021-11-06T07:00:00Z').getTime() / 1000; // 11/06/2021 00:00 PDT
      const expectedEnd = new Date('2021-11-08T08:00:00Z').getTime() / 1000; // 11/09/2021 00:00 PST

      const weatherDataFixture = {
        data: [
          {
            timestamp: 1636268400,
            weather: {
              temperature: 23.5,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1636315200,
            weather: {
              temperature: 23.6,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1636358400,
            weather: {
              temperature: 23.6,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1636401600,
            weather: {
              temperature: 23.6,
              conditions: 'CLEAR',
            },
          },
        ],
      };

      clock.setSystemTime(now * 1000);
      getWeatherForecastsStub.resolves(weatherDataFixture);

      const res = await request.get(url).send();

      const expectedSunlightTimesReponse = [
        {
          midnight: 1636182000,
          midnightUTCOffset: -7,
          dawn: 1636206666,
          dawnUTCOffset: -7,
          sunrise: 1636208225,
          sunriseUTCOffset: -7,
          sunset: 1636246610,
          sunsetUTCOffset: -7,
          dusk: 1636248169,
          duskUTCOffset: -7,
        },
        {
          midnight: 1636268400,
          midnightUTCOffset: -7,
          dawn: 1636293119,
          dawnUTCOffset: -8,
          sunrise: 1636294681,
          sunriseUTCOffset: -8,
          sunset: 1636332963,
          sunsetUTCOffset: -8,
          dusk: 1636334525,
          duskUTCOffset: -8,
        },
      ];

      const expectedWeatherResponse = [
        {
          timestamp: 1636268400,
          utcOffset: -7,
          temperature: 23.5,
          condition: 'NIGHT_CLEAR',
        },
        {
          timestamp: 1636315200,
          utcOffset: -8,
          temperature: 23.6,
          condition: 'CLEAR',
        },
        {
          timestamp: 1636358400,
          utcOffset: -8,
          temperature: 23.6,
          condition: 'NIGHT_CLEAR',
        },
        {
          timestamp: 1636401600,
          utcOffset: -8,
          temperature: 23.6,
          condition: 'CLEAR',
        },
      ];

      expect(res.body).to.be.ok();
      expect(res.body.data).to.be.ok();

      expect(res.body.data.sunlightTimes).to.deep.equal(expectedSunlightTimesReponse);
      expect(res.body.data.weather).to.deep.equal(expectedWeatherResponse);

      const getWeatherForecastsArgs = getWeatherForecastsStub.firstCall.args[0];
      expect(getWeatherForecastsArgs.start).to.equal(expectedStart);
      expect(getWeatherForecastsArgs.end).to.equal(expectedEnd);
      expect(getWeatherForecastsArgs.interval).to.equal(params.intervalHours * 60);
    });

    it('should handle timezones that spring forward at midnight', async () => {
      const weatherDataFixture = {
        data: [
          {
            timestamp: 1630728000,
            weather: {
              temperature: 23.5,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1630771200,
            weather: {
              temperature: 23.6,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1630814400,
            weather: {
              temperature: 23.6,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1630857600,
            weather: {
              temperature: 23.6,
              conditions: 'CLEAR',
            },
          },
        ],
      };

      // Child jumps forward Sept 5th 00:00
      const now = new Date('2021-09-04T12:00:00Z').getTime() / 1000; // 09/04/2021 08:00 CLT
      const expectedStart = new Date('2021-09-04T04:00:00Z').getTime() / 1000; // 09/04/2021 00:00 CLT
      const expectedEnd = new Date('2021-09-06T03:00:00Z').getTime() / 1000; // 09/06/2021 00:00 CLST

      clock.setSystemTime(now * 1000);

      getSpotStub.resolves({ ...spotFixture[0], timezone: 'America/Santiago' });
      getWeatherForecastsStub.resolves(weatherDataFixture);

      const res = await request.get(url).send();

      const expectedSunlightTimesReponse = [
        {
          midnight: 1630728000,
          midnightUTCOffset: -4,
          dawn: 1630760660,
          dawnUTCOffset: -4,
          sunrise: 1630762176,
          sunriseUTCOffset: -4,
          sunset: 1630808116,
          sunsetUTCOffset: -4,
          dusk: 1630809633,
          duskUTCOffset: -4,
        },
        {
          midnight: 1630814400,
          midnightUTCOffset: -3,
          dawn: 1630847102,
          dawnUTCOffset: -3,
          sunrise: 1630848617,
          sunriseUTCOffset: -3,
          sunset: 1630894435,
          sunsetUTCOffset: -3,
          dusk: 1630895950,
          duskUTCOffset: -3,
        },
      ];

      const expectedWeatherResponse = [
        {
          timestamp: 1630728000,
          utcOffset: -4,
          temperature: 23.5,
          condition: 'NIGHT_CLEAR',
        },
        {
          timestamp: 1630771200,
          utcOffset: -4,
          temperature: 23.6,
          condition: 'CLEAR',
        },
        {
          timestamp: 1630814400,
          utcOffset: -3,
          temperature: 23.6,
          condition: 'NIGHT_CLEAR',
        },
        {
          timestamp: 1630857600,
          utcOffset: -3,
          temperature: 23.6,
          condition: 'CLEAR',
        },
      ];

      expect(res).to.have.status(200);

      expect(res.body).to.be.ok();
      expect(res.body.data).to.be.ok();

      expect(res.body.data.sunlightTimes).to.deep.equal(expectedSunlightTimesReponse);
      expect(res.body.data.weather).to.deep.equal(expectedWeatherResponse);

      const getWeatherForecastsArgs = getWeatherForecastsStub.firstCall.args[0];
      expect(getWeatherForecastsArgs.start).to.equal(expectedStart);
      expect(getWeatherForecastsArgs.end).to.equal(expectedEnd);
      expect(getWeatherForecastsArgs.interval).to.equal(params.intervalHours * 60);
    });

    it('should handle timezones that jump backwards at midnight', async () => {
      const chileWeatherDataFixture = {
        data: [
          {
            timestamp: 1617418800,
            weather: {
              temperature: 1,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1617462000,
            weather: {
              temperature: 2,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1617508800,
            weather: {
              temperature: 3,
              conditions: 'CLEAR',
            },
          },
          {
            timestamp: 1617552000,
            weather: {
              temperature: 4,
              conditions: 'CLEAR',
            },
          },
        ],
      };

      // Child jumps backwards at midnight April 4th 00:00
      const now = new Date('2021-04-03T12:00:00Z').getTime() / 1000; // 04/03/2021 09:00 CLST
      const expectedStart = new Date('2021-04-03T03:00:00Z').getTime() / 1000; // 04/03/2021 00:00 CLST
      const expectedEnd = new Date('2021-04-05T04:00:00Z').getTime() / 1000; // 04/05/2021 00:00 CST

      clock.setSystemTime(now * 1000);

      getSpotStub.resolves({ ...spotFixture[0], timezone: 'America/Santiago' });
      getWeatherForecastsStub.resolves(chileWeatherDataFixture);

      const res = await request.get(url).send();

      const expectedSunlightTimesReponse = [
        {
          midnight: 1617418800,
          midnightUTCOffset: -3,
          dawn: 1617455547,
          dawnUTCOffset: -3,
          sunrise: 1617457055,
          sunriseUTCOffset: -3,
          sunset: 1617502500,
          sunsetUTCOffset: -3,
          dusk: 1617504008,
          duskUTCOffset: -3,
        },
        {
          midnight: 1617508800,
          midnightUTCOffset: -4,
          dawn: 1617541865,
          dawnUTCOffset: -4,
          sunrise: 1617543375,
          sunriseUTCOffset: -4,
          sunset: 1617588944,
          sunsetUTCOffset: -4,
          dusk: 1617590454,
          duskUTCOffset: -4,
        },
      ];

      const expectedWeatherResponse = [
        {
          timestamp: 1617418800,
          utcOffset: -3,
          temperature: 1,
          condition: 'NIGHT_CLEAR',
        },
        {
          timestamp: 1617462000,
          utcOffset: -3,
          temperature: 2,
          condition: 'CLEAR',
        },
        {
          timestamp: 1617508800,
          utcOffset: -4,
          temperature: 3,
          condition: 'NIGHT_CLEAR',
        },
        {
          timestamp: 1617552000,
          utcOffset: -4,
          temperature: 4,
          condition: 'CLEAR',
        },
      ];

      expect(res).to.have.status(200);

      expect(res.body).to.be.ok();
      expect(res.body.data).to.be.ok();

      expect(res.body.data.sunlightTimes).to.deep.equal(expectedSunlightTimesReponse);
      expect(res.body.data.weather).to.deep.equal(expectedWeatherResponse);

      const getWeatherForecastsArgs = getWeatherForecastsStub.firstCall.args[0];
      expect(getWeatherForecastsArgs.start).to.equal(expectedStart);
      expect(getWeatherForecastsArgs.end).to.equal(expectedEnd);
      expect(getWeatherForecastsArgs.interval).to.equal(params.intervalHours * 60);
    });
  });
};

export default getWeatherHandlerSpec;
