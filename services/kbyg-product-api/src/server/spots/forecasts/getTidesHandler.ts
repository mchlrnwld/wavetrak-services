import { createUTCOffsetCalculator, dateRange, fromUnixTimestamp } from '../../../utils/datetime';
import utcOffsetFromTimezone from '../../../utils/utcOffsetFromTimezone';
import getTidesData from '../../../external/tides';

const getTidesHandler = async (req, res) => {
  const {
    units,
    forecast: { days },
    spot,
  } = req;
  const { start, end } = dateRange(days, spot.timezone);

  const forecast = spot.tideStation
    ? await getTidesData({
        location_name: spot.tideStation,
        start,
        end,
        units,
      })
    : null;

  const noTideLocation = {
    name: '',
    min: 0.0,
    max: 0.0,
    lon: 0.0,
    lat: 0.0,
    mean: 0.0,
  };

  const utcOffsetCalculator = createUTCOffsetCalculator(spot.timezone);

  const tides = forecast
    ? forecast.tides.map(({ timestamp, type, height }) => ({
        timestamp,
        utcOffset: utcOffsetCalculator(fromUnixTimestamp(timestamp)),
        type,
        height,
      }))
    : [];

  return res.send({
    associated: {
      utcOffset: utcOffsetFromTimezone(spot.timezone),
      units,
      tideLocation: forecast ? forecast.associated.tideLocation : noTideLocation,
    },
    data: {
      tides,
    },
  });
};

export default getTidesHandler;
