import getSpotForecastHandlerSpec from './getSpotForecastHandler.spec';
import getConditionsHandlerSpec from './getConditionsHandler.spec';
import getRatingHandlerSpec from './getRatingHandler.spec';
import getTidesHandlerSpec from './getTidesHandler.spec';
import getWaveHandlerSpec from './getWaveHandler.spec';
import getWeatherHandlerSpec from './getWeatherHandler.spec';
import getWindHandlerSpec from './getWindHandler.spec';

const forecastsSpec = () => {
  describe('/forecasts', () => {
    getSpotForecastHandlerSpec();
    getConditionsHandlerSpec();
    getRatingHandlerSpec();
    getTidesHandlerSpec();
    getWaveHandlerSpec();
    getWeatherHandlerSpec();
    getWindHandlerSpec();
  });
};

export default forecastsSpec;
