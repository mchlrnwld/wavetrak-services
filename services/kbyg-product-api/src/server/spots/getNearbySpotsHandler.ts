import { sortBy } from 'lodash/fp';
import { getSubregion } from '../../external/spots';
import { getSubregionSpotReportViews } from '../../model/SpotReportView';
import reverseSortBeachView from './helpers/reverseSortBeachView';
import { convertConditionsRating } from '../../utils/convertTo7Ratings';

const getNearbySpotsHandler = async (req, res) => {
  const sevenRatings = req.query.sevenRatings === 'true';
  const [spots, subregion] = await Promise.all([
    getSubregionSpotReportViews(req.units, req.spot.subregion),
    getSubregion(req.spot.subregion),
  ]);

  const primarySpot = spots.find(({ _id }) => `${_id}` === subregion.primarySpot);
  const sortedSpots = sortBy((s) => s.rank, spots);

  return res.send({
    associated: {
      units: req.units,
      reverseSortBeachView: reverseSortBeachView(sortedSpots, primarySpot),
    },
    // TODO: Deprecate this. Left for backwards compatibility.
    units: req.units,
    data: {
      spots: sevenRatings
        ? sortedSpots.map((spot) => ({
            ...spot,
            conditions: convertConditionsRating(spot.conditions),
          }))
        : sortedSpots,
    },
  });
};

export default getNearbySpotsHandler;
