import type { NextFunction, Response } from 'express';
import { KBYGRequest } from '../types';

const trackSpotRequests = (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
  log.trace({
    action: '/kbyg/spots',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default trackSpotRequests;
