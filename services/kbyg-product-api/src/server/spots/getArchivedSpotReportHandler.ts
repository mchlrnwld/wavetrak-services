/* eslint-disable consistent-return */
import moment from 'moment';
import S3 from 'aws-sdk/clients/s3';
import _get from 'lodash/get';
import _omit from 'lodash/omit';
import mapMongoIdsToArray from '../../utils/mapMongoIdsToArray';
import config from '../../config';

const s3 = new S3({ region: 'us-west-1' });

const checkS3File = async (key) => {
  const s3ParamsFile = {
    Bucket: config.DATA_LAKE_BUCKET,
    Key: `product/surfline/spotreportviews/${key}/spotReportViews.json`,
  };
  try {
    await s3.headObject(s3ParamsFile).promise();
  } catch (e) {
    return null;
  }
  return key;
};

const archivedSpotReportViews = async (req, res) => {
  const spotId = req.spot._id;
  const { startTimestamp } = req.query;
  const timestamp = startTimestamp ? parseInt(startTimestamp, 10) : new Date();
  const closestKey = moment(timestamp).utc().format('YYYY/MM/DD/HH');

  let s3Key = await checkS3File(closestKey);
  if (!s3Key) {
    const keysArray = [];
    for (let i = 1; i < 3; i += 1) {
      keysArray.push(moment(timestamp).add(i, 'hours').utc().format('YYYY/MM/DD/HH'));
      keysArray.push(moment(timestamp).subtract(i, 'hours').utc().format('YYYY/MM/DD/HH'));
    }

    const matchedKeys = await Promise.all(keysArray.map((key) => checkS3File(key)));
    const filteredKeys = matchedKeys.filter((key) => !!key);
    s3Key = filteredKeys.length ? filteredKeys[0] : null;
  }

  const NOT_FOUND = {
    message: 'Spot Report Not Found',
  };

  if (!s3Key) {
    return res.status(400).send(NOT_FOUND);
  }

  const s3Params = {
    Bucket: config.DATA_LAKE_BUCKET,
    Key: `product/surfline/spotreportviews/${s3Key}/spotReportViews.json`,
    ExpressionType: 'SQL',
    Expression: `select * from s3object s where s._id.$oid = '${spotId}' limit 1`,
    InputSerialization: { JSON: { Type: 'DOCUMENT' } },
    OutputSerialization: { JSON: {} },
  };

  s3.selectObjectContent(s3Params, (err, data) => {
    let spotReportData;
    if (err) {
      return res.status(400).send(NOT_FOUND);
    }

    // data.Payload is a Readable Stream
    const eventStream = data.Payload;

    // Read events as they are available
    // eslint-disable-next-line
    // @ts-ignore
    eventStream.on('data', async (event) => {
      if (event.Records) {
        // event.Records.Payload is a buffer containing
        // a single record, partial records, or multiple records
        const spotReportDataRaw = JSON.parse(event.Records.Payload.toString());
        spotReportData = {
          ...spotReportDataRaw,
          _id: _get(spotReportDataRaw, '_id.$oid', ''),
          updatedAt: _get(spotReportDataRaw, 'updatedAt.$date', ''),
          createdAt: _get(spotReportDataRaw, 'createdAt.$date', ''),
          subregionId: _get(spotReportDataRaw, 'subregionId.$oid', ''),
          cameras: mapMongoIdsToArray(spotReportDataRaw.cameras).map((cam) => {
            const lastPrerecordedClipStartTimeUTC = _get(
              cam,
              'lastPrerecordedClipStartTimeUTC.$date',
              '',
            );
            const lastPrerecordedClipEndTimeUTC = _get(
              cam,
              'lastPrerecordedClipEndTimeUTC.$date',
              '',
            );
            return {
              ...cam,
              lastPrerecordedClipStartTimeUTC,
              lastPrerecordedClipEndTimeUTC,
            };
          }),
          swells: spotReportDataRaw.swells.map((swell) => _omit(swell, '_id')),
          parentTaxonomy: mapMongoIdsToArray(spotReportDataRaw.parentTaxonomy),
        };
      } else if (event.Stats) {
        console.log(`Processed ${event.Stats.Details.BytesProcessed} bytes`);
      } else if (event.End) {
        if (spotReportData) {
          return res.send(spotReportData);
        }
        // no data, throw NOT_FOUND
        return res.status(400).send(NOT_FOUND);
      }
    });
  });
};

export default archivedSpotReportViews;
