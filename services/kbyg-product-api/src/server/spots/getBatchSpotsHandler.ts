import {
  getSpotReportViewsIn,
  getBatchSpotReportViewsByParentTaxonomy,
} from '../../model/SpotReportView';
import { convertConditionsRating } from '../../utils/convertTo7Ratings';

const getBatchSpotsHandler = async (req, res) => {
  const { spotIds } = req.body;
  const { parentTaxonomyIds } = req.body;
  const { units } = req;
  const sevenRatings = req.query.sevenRatings === 'true';

  let spotReportViews = [];
  if (parentTaxonomyIds) {
    const { abilityLevels, select } = req.body;
    spotReportViews = await Promise.all(
      parentTaxonomyIds.map((id) =>
        getBatchSpotReportViewsByParentTaxonomy(id, units, {}, abilityLevels, select),
      ),
    );
  } else {
    spotReportViews = await getSpotReportViewsIn(spotIds, units);
  }

  return res.send({
    associated: {
      units,
    },
    data: sevenRatings
      ? spotReportViews.map((spotReport) => ({
          ...spotReport,
          conditions: convertConditionsRating(spotReport.conditions),
        }))
      : spotReportViews,
  });
};
export default getBatchSpotsHandler;
