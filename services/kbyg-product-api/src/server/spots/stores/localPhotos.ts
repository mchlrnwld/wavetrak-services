export default {
  '5842041f4e65fad6a7708b83': {
    legacySpotId: 5755,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/central-texas/horace-caldwell-pier/',
  },
  '5842041f4e65fad6a7708b84': {
    legacySpotId: 5754,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/central-texas/fish-pass/',
  },
  '5842041f4e65fad6a7708b85': {
    legacySpotId: 5757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/central-texas/bob-hall-pier/',
  },
  '5842041f4e65fad6a7708b86': {
    legacySpotId: 5759,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/quintana/',
  },
  '5842041f4e65fad6a7708b87': {
    legacySpotId: 5760,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/octagon/',
  },
  '5842041f4e65fad6a7708b88': {
    legacySpotId: 5762,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/boilers/',
  },
  '5842041f4e65fad6a7708b89': {
    legacySpotId: 5761,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/assholes/',
  },
  '584204214e65fad6a7709cbc': {
    legacySpotId: 130578,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/seagull/',
  },
  '584204214e65fad6a7709c6a': {
    legacySpotId: 127814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-korea/busan/haeundae-beach/',
  },
  '5842041f4e65fad6a7708b7a': {
    legacySpotId: 5617,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/cape-san-blas/',
  },
  '5842041f4e65fad6a7708b7b': {
    legacySpotId: 5615,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/flagpole/',
  },
  '5842041f4e65fad6a7708b7c': {
    legacySpotId: 5616,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/el-governor/',
  },
  '5842041f4e65fad6a7708b7d': {
    legacySpotId: 5618,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/toilet-bowls/',
  },
  '5842041f4e65fad6a7708b7e': {
    legacySpotId: 5619,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/st--george-island/',
  },
  '5842041f4e65fad6a7708b7f': {
    legacySpotId: 5620,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/dog-island/',
  },
  '584204204e65fad6a7709472': {
    legacySpotId: 70743,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/pantin/',
  },
  '58bdf54982d034001252e3d5': {
    legacySpotId: 145542,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/leadbetter-beach/',
  },
  '5842041f4e65fad6a7708fb9': {
    legacySpotId: 48469,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/volta-da-jurema/',
  },
  '5842041f4e65fad6a7708b90': {
    legacySpotId: 5769,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/meacom-s-pier/',
  },
  '5842041f4e65fad6a7708b91': {
    legacySpotId: 5770,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/crystal-beach/',
  },
  '5842041f4e65fad6a7708b92': {
    legacySpotId: 5771,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/south-texas/boca-chica/',
  },
  '5842041f4e65fad6a7708b93': {
    legacySpotId: 5772,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/south-texas/isla-blanca-state-park/',
  },
  '5842041f4e65fad6a7708a07': {
    legacySpotId: 5165,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/32nd-street/',
  },
  '5842041f4e65fad6a7708b96': {
    legacySpotId: 5773,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/south-texas/port-mansfield/',
  },
  '5842041f4e65fad6a7708b98': {
    legacySpotId: 5779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/ollie-s-point/',
  },
  '5842041f4e65fad6a7708b99': {
    legacySpotId: 5780,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/witches-rock/',
  },
  '584204214e65fad6a7709c57': {
    legacySpotId: 127721,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/bouka--peloponnisos-/',
  },
  '5842041f4e65fad6a7708b8a': {
    legacySpotId: 5765,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/51st-street/',
  },
  '5842041f4e65fad6a7708b8b': {
    legacySpotId: 5763,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/west-galveston/',
  },
  '5842041f4e65fad6a7708b8c': {
    legacySpotId: 5764,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/61st-street/',
  },
  '5842041f4e65fad6a7708b8d': {
    legacySpotId: 5766,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/37th-street/',
  },
  '5842041f4e65fad6a7708b8e': {
    legacySpotId: 5767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/flagship/',
  },
  '5842041f4e65fad6a7708b8f': {
    legacySpotId: 5768,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/a-frames/',
  },
  '584204204e65fad6a77099c4': {
    legacySpotId: 109307,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-south-coast/surfer-s-point/',
  },
  '584204204e65fad6a77099c5': {
    legacySpotId: 109306,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-south-coast/south-point/',
  },
  '584204204e65fad6a7709483': {
    legacySpotId: 71442,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/southwest-spain/conil-de-la-frontera/',
  },
  '584204204e65fad6a7709480': {
    legacySpotId: 71441,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/southwest-spain/cadiz/',
  },
  '584204204e65fad6a77099c8': {
    legacySpotId: 109304,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-west-coast/maycocks/',
  },
  '584204204e65fad6a77099c7': {
    legacySpotId: 109420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/torquay/bells-beach/',
  },
  '584204204e65fad6a77099fa': {
    legacySpotId: 110450,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/faja-dos-vimes/',
  },
  '584204214e65fad6a7709c53': {
    legacySpotId: 127717,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/lourdas-beach--kefallonia-/',
  },
  '584204204e65fad6a7709000': {
    legacySpotId: 49100,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/sainte-barbe/',
  },
  '584204204e65fad6a7709001': {
    legacySpotId: 49102,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/hendaye/',
  },
  '584204204e65fad6a7709002': {
    legacySpotId: 49101,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/belharra/',
  },
  '584204204e65fad6a7709003': {
    legacySpotId: 49204,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/porsmilin/',
  },
  '584204204e65fad6a7709004': {
    legacySpotId: 49206,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/dalbos/',
  },
  '584204204e65fad6a7709005': {
    legacySpotId: 49210,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/kerloch/',
  },
  '584204204e65fad6a7709006': {
    legacySpotId: 49212,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/pointe-de-dinan/',
  },
  '584204204e65fad6a7709007': {
    legacySpotId: 49208,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/le-petit-minou/',
  },
  '584204204e65fad6a7709008': {
    legacySpotId: 49209,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/anse-de-pen-hat/',
  },
  '584204204e65fad6a7709009': {
    legacySpotId: 49213,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/la-palue/',
  },
  '5842041f4e65fad6a7708b9a': {
    legacySpotId: 5781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/mal-pais/',
  },
  '5842041f4e65fad6a7708b9b': {
    legacySpotId: 5782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/tamarindo/',
  },
  '5842041f4e65fad6a7708b9c': {
    legacySpotId: 5783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/south-costa-rica/dominical/',
  },
  '5842041f4e65fad6a7708b9d': {
    legacySpotId: 5784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/bejuco/',
  },
  '584204204e65fad6a7709689': {
    legacySpotId: 78314,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/punta-blanca/',
  },
  '584204214e65fad6a7709c89': {
    legacySpotId: 128413,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/southern-finland/sarkiniemi/',
  },
  '584204204e65fad6a7709a06': {
    legacySpotId: 111358,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/sharkbay/',
  },
  '584204204e65fad6a77099ff': {
    legacySpotId: 110660,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/short-sands/',
  },
  '584204204e65fad6a77099fd': {
    legacySpotId: 110659,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/indian-beach/',
  },
  '584204204e65fad6a77099fe': {
    legacySpotId: 110661,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/the-cove/',
  },
  '58dd748982d034001252e3dc': {
    legacySpotId: 146186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/pacific-blvd-/',
  },
  '584204214e65fad6a7709c58': {
    legacySpotId: 127791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--aegean-sea-/chorefto/',
  },
  '584204204e65fad6a7709010': {
    legacySpotId: 49274,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/saint-tugen/',
  },
  '584204204e65fad6a7709012': {
    legacySpotId: 49282,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/gwendrez/',
  },
  '584204204e65fad6a7709013': {
    legacySpotId: 49276,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/la-gamelle/',
  },
  '584204204e65fad6a7709014': {
    legacySpotId: 49316,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/lesconil/',
  },
  '584204204e65fad6a7709015': {
    legacySpotId: 49284,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/pors-carn/',
  },
  '584204204e65fad6a7709016': {
    legacySpotId: 49317,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/le-kerou/',
  },
  '584204204e65fad6a7709017': {
    legacySpotId: 49318,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/guidel/',
  },
  '584204204e65fad6a7709019': {
    legacySpotId: 49319,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/kaolins/',
  },
  '584204204e65fad6a7709443': {
    legacySpotId: 68653,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/pebbly-beach/',
  },
  '584204204e65fad6a7709442': {
    legacySpotId: 68654,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/depot-beach/',
  },
  '584204204e65fad6a770945e': {
    legacySpotId: 69164,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/playa-hermosa-north/',
  },
  '584204204e65fad6a7709405': {
    legacySpotId: 67097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/scarbourough/',
  },
  '584204214e65fad6a7709bf6': {
    legacySpotId: 125741,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/el-palito/',
  },
  '584204214e65fad6a7709c85': {
    legacySpotId: 128406,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/west-finland/ohtakari/',
  },
  '584204204e65fad6a770900a': {
    legacySpotId: 49214,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/cap-de-la-chevre/',
  },
  '584204204e65fad6a770900b': {
    legacySpotId: 49216,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/les-roches-blanches/',
  },
  '584204204e65fad6a770900c': {
    legacySpotId: 49215,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/pors-ar-vag/',
  },
  '584204204e65fad6a770900d': {
    legacySpotId: 49270,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/pors-theolen/',
  },
  '584204204e65fad6a770900e': {
    legacySpotId: 49272,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/baie-des-trepasses/',
  },
  '584204204e65fad6a770900f': {
    legacySpotId: 49275,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/pointe-de-lervily/',
  },
  '591b423c1907e50013cd93cc': {
    legacySpotId: 146783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/comporta/',
  },
  '584204204e65fad6a77095dc': {
    legacySpotId: 74022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/dierhagen-neuhaus/',
  },
  '584204214e65fad6a7709d01': {
    legacySpotId: 135852,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/oceano/',
  },
  '584204204e65fad6a7709020': {
    legacySpotId: 49330,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/les-dunes/',
  },
  '584204204e65fad6a7709021': {
    legacySpotId: 49332,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/l--aubraie/',
  },
  '584204204e65fad6a7709022': {
    legacySpotId: 49359,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/les-boulassiers/',
  },
  '584204204e65fad6a7709023': {
    legacySpotId: 49333,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/bud-bud/',
  },
  '584204204e65fad6a7709024': {
    legacySpotId: 49334,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/les-conches/',
  },
  '584204204e65fad6a7709025': {
    legacySpotId: 49360,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/chassiron/',
  },
  '584204204e65fad6a7709026': {
    legacySpotId: 49361,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/les-huttes/',
  },
  '584204204e65fad6a7709027': {
    legacySpotId: 49364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/phare-de-la-coubre/',
  },
  '584204204e65fad6a7709028': {
    legacySpotId: 49363,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/saint-trojan/',
  },
  '584204204e65fad6a7709029': {
    legacySpotId: 49362,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/grand-village/',
  },
  '584204214e65fad6a7709c91': {
    legacySpotId: 128966,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/mustafas/',
  },
  '584204204e65fad6a77095da': {
    legacySpotId: 74020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/k-hlungsborn/',
  },
  '584204204e65fad6a77095f6': {
    legacySpotId: 74052,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/maasvlakte/',
  },
  '584204204e65fad6a7709764': {
    legacySpotId: 91653,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/yuigahama/',
  },
  '584204204e65fad6a7709492': {
    legacySpotId: 71460,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/los-alamos/',
  },
  '584204204e65fad6a770901a': {
    legacySpotId: 49323,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/cote-sauvage/',
  },
  '584204204e65fad6a770901b': {
    legacySpotId: 49322,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/etel/',
  },
  '584204204e65fad6a770901c': {
    legacySpotId: 49321,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/toulhars/',
  },
  '584204204e65fad6a770901d': {
    legacySpotId: 49327,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/saint-gilles-croix-de-vie/',
  },
  '584204204e65fad6a770901e': {
    legacySpotId: 49325,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/donnant/',
  },
  '584204204e65fad6a770901f': {
    legacySpotId: 49328,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/la-sauzaie/',
  },
  '584204204e65fad6a77095df': {
    legacySpotId: 74026,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/terschelling/',
  },
  '584204214e65fad6a7709c90': {
    legacySpotId: 128963,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/nahr-brahim/',
  },
  '584204214e65fad6a7709d10': {
    legacySpotId: 139212,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/40th-street--miami/',
  },
  '584204214e65fad6a7709c92': {
    legacySpotId: 128965,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/casinos/',
  },
  '584204204e65fad6a7709030': {
    legacySpotId: 49437,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/montalivet/',
  },
  '584204204e65fad6a7709031': {
    legacySpotId: 49438,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/le-pin-sec/',
  },
  '584204204e65fad6a7709032': {
    legacySpotId: 49439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/hourtin-plage/',
  },
  '584204214e65fad6a7709d15': {
    legacySpotId: 139336,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/marcelino/',
  },
  '584204204e65fad6a7709034': {
    legacySpotId: 49440,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/carcans/',
  },
  '584204214e65fad6a7709d17': {
    legacySpotId: 139496,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/dorset/lyme-regis/',
  },
  '584204204e65fad6a7709662': {
    legacySpotId: 76459,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-china/shanghai/jinqimen-nongcuncun/',
  },
  '584204214e65fad6a7709d19': {
    legacySpotId: 139590,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/cardiff-reef/',
  },
  '584204204e65fad6a7709038': {
    legacySpotId: 49632,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/anse-du-brick/',
  },
  '584204204e65fad6a7709039': {
    legacySpotId: 49634,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/biville/',
  },
  '584204204e65fad6a77095f8': {
    legacySpotId: 74053,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/ouddorp/',
  },
  '584204204e65fad6a77093b8': {
    legacySpotId: 66098,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/wamberal-beach-spoonies/',
  },
  '584204204e65fad6a7709421': {
    legacySpotId: 67575,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/the-farm/',
  },
  '584204204e65fad6a77095f4': {
    legacySpotId: 74048,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/kijkduin/',
  },
  '584204204e65fad6a77095f5': {
    legacySpotId: 74050,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/s-gravenzande/',
  },
  '584204204e65fad6a77095f2': {
    legacySpotId: 74051,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/hoek-van-holland/',
  },
  '584204204e65fad6a77095f7': {
    legacySpotId: 74054,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/renesse/',
  },
  '584204204e65fad6a770902a': {
    legacySpotId: 49367,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/le-verdon/',
  },
  '584204204e65fad6a770902b': {
    legacySpotId: 49331,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/sauveterre/',
  },
  '584204204e65fad6a770902c': {
    legacySpotId: 49366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/pontaillac/',
  },
  '584204204e65fad6a770902d': {
    legacySpotId: 49369,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/l-amelie/',
  },
  '584204204e65fad6a770902e': {
    legacySpotId: 49368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/soulac/',
  },
  '584204204e65fad6a770902f': {
    legacySpotId: 49371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/le-gurp/',
  },
  '584204204e65fad6a77095b5': {
    legacySpotId: 73968,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/hanstholm/',
  },
  '584204204e65fad6a77095f3': {
    legacySpotId: 74049,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/ter-heijde/',
  },
  '584204204e65fad6a77094a6': {
    legacySpotId: 71543,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/margate/',
  },
  '584204204e65fad6a7709040': {
    legacySpotId: 49640,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/baubigny/',
  },
  '584204204e65fad6a7709041': {
    legacySpotId: 49646,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/cap-frehel/',
  },
  '584204204e65fad6a7709042': {
    legacySpotId: 49641,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/le-sillon/',
  },
  '584204204e65fad6a7709043': {
    legacySpotId: 49645,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/longchamps/',
  },
  '584204204e65fad6a7709044': {
    legacySpotId: 49649,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/trestraou/',
  },
  '584204204e65fad6a7709045': {
    legacySpotId: 49650,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/locquirec/',
  },
  '584204204e65fad6a7709046': {
    legacySpotId: 49651,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/dossen/',
  },
  '584204204e65fad6a7709047': {
    legacySpotId: 49652,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/la-mauvaise-greve/',
  },
  '584204204e65fad6a7709048': {
    legacySpotId: 49653,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/kerlouan/',
  },
  '584204204e65fad6a7709049': {
    legacySpotId: 49654,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/penfoul/',
  },
  '584204204e65fad6a77094a3': {
    legacySpotId: 71472,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/punta-prima/',
  },
  '584204204e65fad6a77093c8': {
    legacySpotId: 66175,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/pelican-beach/',
  },
  '584204214e65fad6a7709d1a': {
    legacySpotId: 139591,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/george-s/',
  },
  '584204204e65fad6a770903a': {
    legacySpotId: 49633,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/collignon/',
  },
  '584204204e65fad6a770903b': {
    legacySpotId: 49636,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/dielette/',
  },
  '584204204e65fad6a770903c': {
    legacySpotId: 49635,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/siouville/',
  },
  '584204204e65fad6a770903d': {
    legacySpotId: 49638,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/le-rozel/',
  },
  '584204204e65fad6a770903e': {
    legacySpotId: 49637,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/sciotot/',
  },
  '584204204e65fad6a77094a5': {
    legacySpotId: 71539,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/umhloti/',
  },
  '584204204e65fad6a7709685': {
    legacySpotId: 78307,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/bajamar/',
  },
  '584204204e65fad6a7709684': {
    legacySpotId: 78304,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/playa-del-socorro/',
  },
  '584204204e65fad6a7709646': {
    legacySpotId: 76313,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-china/guandong/xichong/',
  },
  '584204204e65fad6a77095ed': {
    legacySpotId: 74043,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/noordwijk/',
  },
  '584204204e65fad6a77095ef': {
    legacySpotId: 74044,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/katwijk-aan-zee/',
  },
  '584204214e65fad6a7709c42': {
    legacySpotId: 127568,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-korea/northern-south-korea/songdojin-ni/',
  },
  '584204214e65fad6a7709c7d': {
    legacySpotId: 128387,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/keramas-kfc/',
  },
  '584204204e65fad6a77095ee': {
    legacySpotId: 74045,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/wassenaar/',
  },
  '584204204e65fad6a7709057': {
    legacySpotId: 49940,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/pua-ena-point/',
  },
  '584204204e65fad6a7709059': {
    legacySpotId: 50072,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/senoritas/',
  },
  '584204204e65fad6a77095eb': {
    legacySpotId: 74041,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/zandvoort/',
  },
  '584204214e65fad6a7709d2a': {
    legacySpotId: 143343,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/godrevy/',
  },
  '584204214e65fad6a7709d2b': {
    legacySpotId: 142803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/cocoa-beach-pier-ns/',
  },
  '584204204e65fad6a770904a': {
    legacySpotId: 49655,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/le-gouerou/',
  },
  '584204204e65fad6a770904c': {
    legacySpotId: 49656,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/north-france/les-blancs-sablons/',
  },
  '584204204e65fad6a770904d': {
    legacySpotId: 49737,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/mondos/',
  },
  '584204204e65fad6a77095a0': {
    legacySpotId: 73028,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/50-road-beach/',
  },
  '584204204e65fad6a77095a1': {
    legacySpotId: 73030,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/jack-darling-park/',
  },
  '584204204e65fad6a77095a2': {
    legacySpotId: 73031,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/ashbridges-bay/',
  },
  '584204204e65fad6a77095a3': {
    legacySpotId: 73032,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/bluffers-park/',
  },
  '584204204e65fad6a77095a4': {
    legacySpotId: 73033,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/cathedral-bluffs/',
  },
  '584204204e65fad6a77095a5': {
    legacySpotId: 73035,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/frenchman-s-bay/',
  },
  '584204204e65fad6a7709061': {
    legacySpotId: 50077,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/el-huaico/',
  },
  '584204204e65fad6a77095a7': {
    legacySpotId: 73036,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/darlington-park/',
  },
  '584204204e65fad6a77095a8': {
    legacySpotId: 73037,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-on--ontario-/presqu-ile-point/',
  },
  '584204204e65fad6a77095a9': {
    legacySpotId: 73038,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-on--ontario-/consecon-north-beach/',
  },
  '584204214e65fad6a7709c29': {
    legacySpotId: 126961,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/cannes/',
  },
  '584204204e65fad6a770905a': {
    legacySpotId: 50073,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/oscar-point/',
  },
  '584204204e65fad6a770905c': {
    legacySpotId: 50074,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/playa-norte/',
  },
  '584204204e65fad6a770905d': {
    legacySpotId: 50075,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/el-paso/',
  },
  '584204204e65fad6a770905f': {
    legacySpotId: 50076,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/spot-kontiki/',
  },
  '584204204e65fad6a77095b1': {
    legacySpotId: 73962,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/skagen/',
  },
  '584204204e65fad6a77095b2': {
    legacySpotId: 73966,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/loekken/',
  },
  '584204204e65fad6a77095b3': {
    legacySpotId: 73964,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/b-nnerup-djurslands/',
  },
  '584204204e65fad6a77095b4': {
    legacySpotId: 73965,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/hirtshals/',
  },
  '584204204e65fad6a77093e4': {
    legacySpotId: 66504,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/blue-fish-point/',
  },
  '584204204e65fad6a77095b7': {
    legacySpotId: 73970,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/klitmoller-bunker/',
  },
  '584204204e65fad6a77093ce': {
    legacySpotId: 66481,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/palm-beach/',
  },
  '584204204e65fad6a77093cf': {
    legacySpotId: 66375,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/parking-lots/',
  },
  '584204204e65fad6a7709411': {
    legacySpotId: 67110,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/south-wollongong/',
  },
  '584204204e65fad6a7709078': {
    legacySpotId: 50522,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/el-transito/',
  },
  '584204214e65fad6a7709ba6': {
    legacySpotId: 122367,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/punta-mango/',
  },
  '584204214e65fad6a7709ba3': {
    legacySpotId: 121715,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/mizata/',
  },
  '584204214e65fad6a7709c1d': {
    legacySpotId: 126948,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/la-corniche/',
  },
  '584204204e65fad6a77095ab': {
    legacySpotId: 73039,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-on--ontario-/sandbanks-park/',
  },
  '584204204e65fad6a770961b': {
    legacySpotId: 74563,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taitung/jinzun-harbor/',
  },
  '5842041f4e65fad6a7708aa0': {
    legacySpotId: 5315,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/jacksonville-beach-pier/',
  },
  '5842041f4e65fad6a7708aa1': {
    legacySpotId: 5331,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/main-street-pier/',
  },
  '5842041f4e65fad6a7708aa2': {
    legacySpotId: 5332,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/daytona-beach/',
  },
  '5842041f4e65fad6a7708aa3': {
    legacySpotId: 5335,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/ft--pierce-inlet/',
  },
  '5842041f4e65fad6a7708aa4': {
    legacySpotId: 5333,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/ormond-beach/',
  },
  '5842041f4e65fad6a7708aa5': {
    legacySpotId: 5311,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/vilano/',
  },
  '5842041f4e65fad6a7708aa6': {
    legacySpotId: 5338,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/spanish-house/',
  },
  '5842041f4e65fad6a7708aa7': {
    legacySpotId: 5340,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/playalinda/',
  },
  '5842041f4e65fad6a7708aa8': {
    legacySpotId: 5339,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/jetty-park/',
  },
  '5842041f4e65fad6a7708aa9': {
    legacySpotId: 5341,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/kennedy-space-center/',
  },
  '584204204e65fad6a7709085': {
    legacySpotId: 50534,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/whitby-beach/',
  },
  '584204204e65fad6a7709086': {
    legacySpotId: 50560,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/hornsea/',
  },
  '584204204e65fad6a7709087': {
    legacySpotId: 50536,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/south-bay/',
  },
  '584204204e65fad6a7709088': {
    legacySpotId: 50535,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/north-bay/',
  },
  '584204204e65fad6a77095ff': {
    legacySpotId: 74070,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/blankenberge/',
  },
  '584204204e65fad6a77095ba': {
    legacySpotId: 73974,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/southern-denmark/borsmose/',
  },
  '584204204e65fad6a77095bb': {
    legacySpotId: 73973,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/hvide-sande/',
  },
  '584204204e65fad6a77095bc': {
    legacySpotId: 73978,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/smidstrup-strand/',
  },
  '584204204e65fad6a77095bd': {
    legacySpotId: 73975,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/southern-denmark/vejers-strand/',
  },
  '584204204e65fad6a77095be': {
    legacySpotId: 73977,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/raageleje/',
  },
  '584204204e65fad6a77093bb': {
    legacySpotId: 66099,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/ruins-barbed-wire/',
  },
  '584204204e65fad6a770907c': {
    legacySpotId: 50526,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/beadnell-bay/',
  },
  '584204204e65fad6a770907d': {
    legacySpotId: 50527,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/blyth-beach/',
  },
  '584204204e65fad6a770907f': {
    legacySpotId: 50528,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/whitley-bay/',
  },
  '584204214e65fad6a7709c30': {
    legacySpotId: 127089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/kerith-resort/',
  },
  '5842041f4e65fad6a7708ab1': {
    legacySpotId: 5350,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/vero-pier/',
  },
  '5842041f4e65fad6a7708ab2': {
    legacySpotId: 5351,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/spessard-holland/',
  },
  '584204204e65fad6a77095d4': {
    legacySpotId: 74013,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/lippe-hohwachter-bucht/',
  },
  '584204204e65fad6a77095d5': {
    legacySpotId: 74016,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/fehmarn-westermarkelsdorf/',
  },
  '584204204e65fad6a77095d6': {
    legacySpotId: 74015,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/dazendorf/',
  },
  '5842041f4e65fad6a7708ab7': {
    legacySpotId: 5355,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/the-radisson/',
  },
  '5842041f4e65fad6a7708ab8': {
    legacySpotId: 5356,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/picnic-tables/',
  },
  '5842041f4e65fad6a7708ab9': {
    legacySpotId: 5357,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/perkins/',
  },
  '584204204e65fad6a770968a': {
    legacySpotId: 78312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/playa-de-las-am-ricas/',
  },
  '584204214e65fad6a7709c1a': {
    legacySpotId: 126946,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/la-couronne/',
  },
  '584204204e65fad6a77095ca': {
    legacySpotId: 74004,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/langeoog/',
  },
  '5842041f4e65fad6a7708aab': {
    legacySpotId: 5344,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/lori-wilson-park/',
  },
  '584204204e65fad6a77095cc': {
    legacySpotId: 74008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/borkum/',
  },
  '5842041f4e65fad6a7708aad': {
    legacySpotId: 5346,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/wabasso/',
  },
  '5842041f4e65fad6a7708aae': {
    legacySpotId: 5347,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/p-v--martin-s/',
  },
  '5842041f4e65fad6a7708aaf': {
    legacySpotId: 5345,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/riomar/',
  },
  '584204214e65fad6a7709c35': {
    legacySpotId: 127099,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/teluk-cempedak/',
  },
  '584204204e65fad6a77093d6': {
    legacySpotId: 66489,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/basin-bay/',
  },
  '584204204e65fad6a77093d7': {
    legacySpotId: 66491,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/warriewood/',
  },
  '584204204e65fad6a77093d5': {
    legacySpotId: 66487,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/newport-reef/',
  },
  '5842041f4e65fad6a7708ac0': {
    legacySpotId: 5365,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/30th-street/',
  },
  '5842041f4e65fad6a7708ac1': {
    legacySpotId: 5366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/south-beach--miami/',
  },
  '5842041f4e65fad6a7708ac2': {
    legacySpotId: 5367,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/dania-pier/',
  },
  '5842041f4e65fad6a7708ac3': {
    legacySpotId: 5368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/franklin-street/',
  },
  '5842041f4e65fad6a7708ac4': {
    legacySpotId: 5369,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/john-lloyd-state-park/',
  },
  '5842041f4e65fad6a7708ac5': {
    legacySpotId: 5370,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/spoils/',
  },
  '5842041f4e65fad6a7708ac6': {
    legacySpotId: 5371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/commercial-pier/',
  },
  '5842041f4e65fad6a7708ac7': {
    legacySpotId: 5359,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/16th-st--south/',
  },
  '5842041f4e65fad6a7708ac8': {
    legacySpotId: 5372,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/2nd-street/',
  },
  '584204204e65fad6a77095e9': {
    legacySpotId: 74039,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/castricum-aan-zee/',
  },
  '584204204e65fad6a77099e9': {
    legacySpotId: 110433,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/ponta-do-queimado/',
  },
  '584204204e65fad6a7709a5d': {
    legacySpotId: 111706,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/koh-lanta/',
  },
  '584204204e65fad6a77094f2': {
    legacySpotId: 72786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/covert-park-beach/',
  },
  '5842041f4e65fad6a7708aba': {
    legacySpotId: 5358,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/rc-s/',
  },
  '584204204e65fad6a77095db': {
    legacySpotId: 74021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/warnem-nde/',
  },
  '5842041f4e65fad6a7708abc': {
    legacySpotId: 5361,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/2nd-light/',
  },
  '5842041f4e65fad6a7708abd': {
    legacySpotId: 5363,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/bonsteel-park/',
  },
  '5842041f4e65fad6a7708abe': {
    legacySpotId: 5362,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/hangers/',
  },
  '5842041f4e65fad6a7708abf': {
    legacySpotId: 5364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/walton-rocks/',
  },
  '584204204e65fad6a7709b2e': {
    legacySpotId: 117849,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/rock-island/',
  },
  '584204204e65fad6a7709ad3': {
    legacySpotId: 116310,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/martinique/martinique/tartane/',
  },
  '584204214e65fad6a7709d08': {
    legacySpotId: 136965,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/shell-beach-overview/',
  },
  '584204204e65fad6a7709a52': {
    legacySpotId: 111514,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/iztapa/',
  },
  '5842041f4e65fad6a7708ad0': {
    legacySpotId: 5382,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/blowing-rocks/',
  },
  '5842041f4e65fad6a7708ad2': {
    legacySpotId: 5376,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/delray-beach/',
  },
  '5842041f4e65fad6a7708ad3': {
    legacySpotId: 5384,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/fletcher-s/',
  },
  '584204204e65fad6a77094c7': {
    legacySpotId: 71790,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taipei/fulong/',
  },
  '5842041f4e65fad6a7708ad5': {
    legacySpotId: 5378,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/lantana-public-beach/',
  },
  '5842041f4e65fad6a7708ad6': {
    legacySpotId: 5386,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/5th-street/',
  },
  '5842041f4e65fad6a7708ad7': {
    legacySpotId: 5387,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/21st-street/',
  },
  '5842041f4e65fad6a7708ad8': {
    legacySpotId: 5389,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/haulover-inlet--south-jetty-/',
  },
  '5842041f4e65fad6a7708ad9': {
    legacySpotId: 5388,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/haulover-inlet--north-jetty-/',
  },
  '584204204e65fad6a77094f4': {
    legacySpotId: 72788,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/st--joeseph-south-beach/',
  },
  '584204204e65fad6a7709ad6': {
    legacySpotId: 116314,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/st--lucia/st--lucia/winjammer/',
  },
  '584204214e65fad6a7709d13': {
    legacySpotId: 138420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/santo-amaro/',
  },
  '584204204e65fad6a77095ea': {
    legacySpotId: 74040,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/ijmuiden/',
  },
  '5842041f4e65fad6a7708acb': {
    legacySpotId: 5375,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/atlantic-dunes-park/',
  },
  '584204204e65fad6a77095ec': {
    legacySpotId: 74042,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/langevelderslag/',
  },
  '5842041f4e65fad6a7708acd': {
    legacySpotId: 5379,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/hilton-hotel-backyard/',
  },
  '5842041f4e65fad6a7708ace': {
    legacySpotId: 5381,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/coral-cove-park/',
  },
  '5842041f4e65fad6a7708acf': {
    legacySpotId: 5380,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/palm-beach/',
  },
  '584204214e65fad6a7709d12': {
    legacySpotId: 139214,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/pelican-beach-park/',
  },
  '5842041f4e65fad6a7708ae0': {
    legacySpotId: 5396,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/corners/',
  },
  '5842041f4e65fad6a7708ae1': {
    legacySpotId: 5397,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/the-cove/',
  },
  '5842041f4e65fad6a7708ae2': {
    legacySpotId: 5398,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/peanut-island/',
  },
  '5842041f4e65fad6a7708ae3': {
    legacySpotId: 5399,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/japanese-rock/',
  },
  '5842041f4e65fad6a7708ae4': {
    legacySpotId: 5400,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/mexico-beach/',
  },
  '5842041f4e65fad6a7708ae5': {
    legacySpotId: 5402,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/shell-island/',
  },
  '5842041f4e65fad6a7708ae7': {
    legacySpotId: 5401,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/d-j--s/',
  },
  '5842041f4e65fad6a7708ae8': {
    legacySpotId: 5404,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/st--andrew-s-park/',
  },
  '5842041f4e65fad6a7708ae9': {
    legacySpotId: 5406,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/spinnaker/',
  },
  '590a2abd6a2e4300134fbed9': {
    legacySpotId: 146851,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/laniakea-inside/',
  },
  '584204204e65fad6a7709b54': {
    legacySpotId: 117897,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/maresciallo/',
  },
  '5842041f4e65fad6a7708ada': {
    legacySpotId: 5390,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/85th-street/',
  },
  '5842041f4e65fad6a7708adb': {
    legacySpotId: 5391,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/hillsboro-inlet/',
  },
  '584204214e65fad6a7709d22': {
    legacySpotId: 141191,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/woolacombe-front/',
  },
  '5842041f4e65fad6a7708add': {
    legacySpotId: 5393,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/hillsboro-beach/',
  },
  '5842041f4e65fad6a7708ade': {
    legacySpotId: 5394,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/red-reef-park/',
  },
  '5842041f4e65fad6a7708adf': {
    legacySpotId: 5395,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/boynton-beach-inlet--northside-/',
  },
  '584204214e65fad6a7709d23': {
    legacySpotId: 142796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/beverly-beach/',
  },
  '584204204e65fad6a770965b': {
    legacySpotId: 76450,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-china/shanghai/shengshi--huichen/',
  },
  '584204204e65fad6a7709681': {
    legacySpotId: 77599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-pacific/playa-venao/',
  },
  '5842041f4e65fad6a7708af0': {
    legacySpotId: 5413,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/seascape/',
  },
  '5842041f4e65fad6a7708af1': {
    legacySpotId: 5414,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/the-back-porch/',
  },
  '5842041f4e65fad6a7708af2': {
    legacySpotId: 5405,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/panama-city-pier/',
  },
  '5842041f4e65fad6a7708af3': {
    legacySpotId: 5415,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/whale-s-tail/',
  },
  '5842041f4e65fad6a7708af4': {
    legacySpotId: 5416,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/old-crystal-pier/',
  },
  '5842041f4e65fad6a7708af5': {
    legacySpotId: 5417,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/jetty-east/',
  },
  '5842041f4e65fad6a7708af6': {
    legacySpotId: 5418,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/hmo-s/',
  },
  '5842041f4e65fad6a7708af7': {
    legacySpotId: 5419,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/two-by-fours/',
  },
  '5842041f4e65fad6a7708af8': {
    legacySpotId: 5420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/the-pumphouse/',
  },
  '5842041f4e65fad6a7708af9': {
    legacySpotId: 5421,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/nco-club/',
  },
  '584204204e65fad6a7709adc': {
    legacySpotId: 116381,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/us-virgin-islands/st-croix/fareham/',
  },
  '584204204e65fad6a7709ab4': {
    legacySpotId: 115821,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/palmachim/',
  },
  '584204204e65fad6a7709ab5': {
    legacySpotId: 115822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/ashdod/',
  },
  '584204204e65fad6a7709656': {
    legacySpotId: 76337,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-china/fujian/jinmen-oucuo-beach/',
  },
  '5842041f4e65fad6a7708aea': {
    legacySpotId: 5407,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/wood-pier/',
  },
  '5842041f4e65fad6a7708aeb': {
    legacySpotId: 5408,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/inlet-beach-pier/',
  },
  '5842041f4e65fad6a7708aec': {
    legacySpotId: 5409,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/bud-and-alley-s/',
  },
  '5842041f4e65fad6a7708aed': {
    legacySpotId: 5410,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/grayton-beach/',
  },
  '5842041f4e65fad6a7708aee': {
    legacySpotId: 5411,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/blue-mountain-beach/',
  },
  '5842041f4e65fad6a7708aef': {
    legacySpotId: 5412,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/pompano-joe-s/',
  },
  '584204204e65fad6a770943b': {
    legacySpotId: 68645,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/flat-rock/',
  },
  '584204214e65fad6a7709d0b': {
    legacySpotId: 139023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/redondo-jetty/',
  },
  '5842041f4e65fad6a7708980': {
    legacySpotId: 5021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/waddell-creek/',
  },
  '584204214e65fad6a7709cb0': {
    legacySpotId: 130200,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/poland-baltic-sea/northern-poland/wladyslawowo/',
  },
  '584204214e65fad6a7709d0a': {
    legacySpotId: 137586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/rockaway-beach-77th/',
  },
  '5842041f4e65fad6a7708afa': {
    legacySpotId: 5422,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/okaloosa-pier/',
  },
  '5842041f4e65fad6a7708afb': {
    legacySpotId: 5423,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/dead-man-s/',
  },
  '5842041f4e65fad6a7708afc': {
    legacySpotId: 5428,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/pensacola-pier/',
  },
  '5842041f4e65fad6a7708afd': {
    legacySpotId: 5425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/navarre-beach/',
  },
  '5842041f4e65fad6a7708afe': {
    legacySpotId: 5424,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/blue-horizon-motel/',
  },
  '5842041f4e65fad6a7708aff': {
    legacySpotId: 5434,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/first-parking-lot/',
  },
  '584204204e65fad6a77095d1': {
    legacySpotId: 74014,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/weissenh-us/',
  },
  '58dd6fec45792b001324d858': {
    legacySpotId: 145981,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/gute-beach/',
  },
  '584204204e65fad6a770960a': {
    legacySpotId: 74086,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/northern-norway/skallelv/',
  },
  '584204204e65fad6a7709463': {
    legacySpotId: 69865,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/bathtub-beach/',
  },
  '584204204e65fad6a77095d8': {
    legacySpotId: 74018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/dahme-seebrucke/',
  },
  '584204204e65fad6a77094b7': {
    legacySpotId: 71564,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/muizenberg/',
  },
  '584204204e65fad6a7709611': {
    legacySpotId: 74113,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/ervika/',
  },
  '584204204e65fad6a7709612': {
    legacySpotId: 74115,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/karm-y/',
  },
  '584204214e65fad6a7709c9b': {
    legacySpotId: 128975,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/latvia/latvia--baltic-sea-/palmu-licis/',
  },
  '584204204e65fad6a7709615': {
    legacySpotId: 74119,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/hellest-/',
  },
  '584204204e65fad6a7709616': {
    legacySpotId: 74120,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/sele/',
  },
  '584204204e65fad6a7709618': {
    legacySpotId: 74121,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/bore/',
  },
  '58bdda3582d034001252e3d0': {
    legacySpotId: 145752,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/central-ocean-beach-north/',
  },
  '584204204e65fad6a7709617': {
    legacySpotId: 74122,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/reve-havn/',
  },
  '584204204e65fad6a7709584': {
    legacySpotId: 72988,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/ipperwash/',
  },
  '5842041f4e65fad6a7708982': {
    legacySpotId: 5022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/scott-creek/',
  },
  '584204204e65fad6a7709b19': {
    legacySpotId: 117807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/lillatro/',
  },
  '584204204e65fad6a77094b6': {
    legacySpotId: 71563,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/kommetjie/',
  },
  '584204204e65fad6a7709674': {
    legacySpotId: 77024,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/muriwai-beach/',
  },
  '584204204e65fad6a7709673': {
    legacySpotId: 77023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/ocean-beach-hawkes-bay/',
  },
  '584204214e65fad6a7709d27': {
    legacySpotId: 142797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/agate-beach/',
  },
  '584204204e65fad6a7709600': {
    legacySpotId: 74071,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/de-haan/',
  },
  '584204204e65fad6a770950a': {
    legacySpotId: 72810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/lake-forest-beach/',
  },
  '584204204e65fad6a770966e': {
    legacySpotId: 77020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/whangamata/',
  },
  '584204204e65fad6a7709604': {
    legacySpotId: 74076,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/ollie-s-point/',
  },
  '584204204e65fad6a7709605': {
    legacySpotId: 74077,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/raversijde/',
  },
  '584204204e65fad6a7709607': {
    legacySpotId: 74080,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/westende/',
  },
  '584204204e65fad6a7709608': {
    legacySpotId: 74083,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/oostduinkerke/',
  },
  '584204204e65fad6a770966f': {
    legacySpotId: 77019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/te-arai-point/',
  },
  '584204204e65fad6a7709606': {
    legacySpotId: 74078,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/middelkerke/',
  },
  '584204204e65fad6a770966d': {
    legacySpotId: 77017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/sandy-bay/',
  },
  '584204204e65fad6a77095d7': {
    legacySpotId: 74017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/fehmarn-staberhuk/',
  },
  '584204214e65fad6a7709c82': {
    legacySpotId: 128394,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--baltic-sea-/brantevik/',
  },
  '584204204e65fad6a7709676': {
    legacySpotId: 77029,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/castlepoint-beach/',
  },
  '584204214e65fad6a7709d1d': {
    legacySpotId: 140404,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/morro-bay-overview/',
  },
  '584204204e65fad6a77095fb': {
    legacySpotId: 74056,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/dishoek/',
  },
  '58bde90e0cec4200133464ef': {
    legacySpotId: 145717,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/boneyard/',
  },
  '584204204e65fad6a7709678': {
    legacySpotId: 77031,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/lyall-bay/',
  },
  '584204204e65fad6a7709603': {
    legacySpotId: 74075,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/mariakerke/',
  },
  '584204204e65fad6a7709602': {
    legacySpotId: 74074,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/oostende/',
  },
  '584204204e65fad6a7709aa0': {
    legacySpotId: 115799,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/zikim/',
  },
  '584204204e65fad6a7709513': {
    legacySpotId: 72822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/kohler-andrae-state-park/',
  },
  '584204214e65fad6a7709d1e': {
    legacySpotId: 141048,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/ne-14th-ct---fort-lauderdale/',
  },
  '584204204e65fad6a7709aa7': {
    legacySpotId: 115807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/sdot-yam/',
  },
  '584204214e65fad6a7709d1f': {
    legacySpotId: 140547,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/monterey-state-beach/',
  },
  '584204204e65fad6a7709aa6': {
    legacySpotId: 115805,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/jisr-az-zarqa/',
  },
  '584204204e65fad6a7709aeb': {
    legacySpotId: 116855,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southwestern-sri-lanka/matara/',
  },
  '584204204e65fad6a77092c3': {
    legacySpotId: 61374,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/cylinders/',
  },
  '584204204e65fad6a7709530': {
    legacySpotId: 72889,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/bay-village-huntington-beach/',
  },
  '584204204e65fad6a77095e8': {
    legacySpotId: 74038,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/egmond-aan-zee/',
  },
  '584204204e65fad6a77095de': {
    legacySpotId: 74023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/wustrow/',
  },
  '584204204e65fad6a77093a6': {
    legacySpotId: 66020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/glenrock-beach/',
  },
  '584204204e65fad6a77095e4': {
    legacySpotId: 74034,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/petten/',
  },
  '584204204e65fad6a77099e7': {
    legacySpotId: 110431,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/ribeira-quente-left/',
  },
  '584204204e65fad6a77095e6': {
    legacySpotId: 74036,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/hargen/',
  },
  '584204204e65fad6a77095e0': {
    legacySpotId: 74028,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/texel/',
  },
  '584204204e65fad6a77095e5': {
    legacySpotId: 74035,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/camperduin/',
  },
  '584204204e65fad6a77099e8': {
    legacySpotId: 110435,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/vila-nova/',
  },
  '584204204e65fad6a77099e6': {
    legacySpotId: 110430,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/ponta-garca/',
  },
  '584204204e65fad6a77095cb': {
    legacySpotId: 74005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/baltrum/',
  },
  '584204204e65fad6a77095cd': {
    legacySpotId: 74007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/juist/',
  },
  '584204204e65fad6a7709a5a': {
    legacySpotId: 111522,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/champerico/',
  },
  '584204204e65fad6a770967c': {
    legacySpotId: 77034,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/gore-bay/',
  },
  '584204214e65fad6a7709ca0': {
    legacySpotId: 129509,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/sao-tome-and-principe/sao-tome/alfonsita/',
  },
  '584204214e65fad6a7709ca1': {
    legacySpotId: 129511,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/sao-tome-and-principe/sao-tome/lavadoura/',
  },
  '584204214e65fad6a7709ca2': {
    legacySpotId: 129682,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/yoff-beach/',
  },
  '584204214e65fad6a7709ca3': {
    legacySpotId: 129683,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/le-virage/',
  },
  '584204204e65fad6a7709394': {
    legacySpotId: 62484,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/morna-point/',
  },
  '584204214e65fad6a7709ca5': {
    legacySpotId: 129687,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/club-med/',
  },
  '584204214e65fad6a7709ca6': {
    legacySpotId: 129685,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/ngor-rights/',
  },
  '584204204e65fad6a7709391': {
    legacySpotId: 64582,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/south-stockton-beach/',
  },
  '584204204e65fad6a7709390': {
    legacySpotId: 64581,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/the-spot/',
  },
  '584204214e65fad6a7709ca9': {
    legacySpotId: 129689,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/vivier/',
  },
  '584204204e65fad6a77095ce': {
    legacySpotId: 74006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/norderney/',
  },
  '584204204e65fad6a77095cf': {
    legacySpotId: 74009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/damp/',
  },
  '584204204e65fad6a77095fa': {
    legacySpotId: 74057,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/cadzand/',
  },
  '5842041f4e65fad6a7708c00': {
    legacySpotId: 6455,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/whale-beach/',
  },
  '5842041f4e65fad6a7708c01': {
    legacySpotId: 6474,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/merimbula-bar/',
  },
  '5842041f4e65fad6a7708c02': {
    legacySpotId: 6475,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/moruya-breakwall/',
  },
  '5842041f4e65fad6a7708c03': {
    legacySpotId: 6476,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/ulladulla-bommie/',
  },
  '5842041f4e65fad6a7708c04': {
    legacySpotId: 6477,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/golf-course-reef/',
  },
  '5842041f4e65fad6a7708c05': {
    legacySpotId: 6478,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/green-island/',
  },
  '5842041f4e65fad6a7708c06': {
    legacySpotId: 6479,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/black-rock/',
  },
  '5842041f4e65fad6a7708c07': {
    legacySpotId: 6480,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/werri-beach/',
  },
  '5842041f4e65fad6a7708c08': {
    legacySpotId: 6481,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/windang-island/',
  },
  '5842041f4e65fad6a7708c09': {
    legacySpotId: 6435,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/tallows/',
  },
  '584204204e65fad6a7709451': {
    legacySpotId: 68761,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/narooma-breakwall/',
  },
  '584204204e65fad6a77095d9': {
    legacySpotId: 74019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-baltic-sea/eastern-germany/timmendorfer-strand/',
  },
  '584204204e65fad6a77095bf': {
    legacySpotId: 73976,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/hundested-strand/',
  },
  '58bdea400cec4200133464f0': {
    legacySpotId: 144645,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/hb-pier-southside-overview/',
  },
  '584204214e65fad6a7709caa': {
    legacySpotId: 129692,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/corniche-ouest/',
  },
  '584204214e65fad6a7709cab': {
    legacySpotId: 129690,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/ouakam/',
  },
  '584204214e65fad6a7709cac': {
    legacySpotId: 129725,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/nigeria/north-coast/shipwreck/',
  },
  '584204214e65fad6a7709cad': {
    legacySpotId: 129726,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/nigeria/north-coast/lighthouse-beach/',
  },
  '584204214e65fad6a7709cae': {
    legacySpotId: 129906,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/us-virgin-islands/st-croix/judith-s-fancy-beach/',
  },
  '584204214e65fad6a7709caf': {
    legacySpotId: 129727,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/nigeria/north-coast/tarkwa-bay/',
  },
  '5842041f4e65fad6a7708c10': {
    legacySpotId: 6493,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/mornington-peninsula---phillip-island/woolamai/',
  },
  '5842041f4e65fad6a7708c11': {
    legacySpotId: 6437,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/duranbah/',
  },
  '5842041f4e65fad6a7708c12': {
    legacySpotId: 6488,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/torquay/winkipop/',
  },
  '5842041f4e65fad6a7708c13': {
    legacySpotId: 6492,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/mornington-peninsula---phillip-island/cat-bay/',
  },
  '5842041f4e65fad6a7708c14': {
    legacySpotId: 6440,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/south-wall-ballina/',
  },
  '5842041f4e65fad6a7708c15': {
    legacySpotId: 6494,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/mornington-peninsula---phillip-island/express-point/',
  },
  '584204214e65fad6a7709cc3': {
    legacySpotId: 130589,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/patsy-s/',
  },
  '5842041f4e65fad6a7708c17': {
    legacySpotId: 6519,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/abrolhos-islands/',
  },
  '584204214e65fad6a7709cc5': {
    legacySpotId: 130591,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/indian-harbour-beach/',
  },
  '5842041f4e65fad6a7708c19': {
    legacySpotId: 6521,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/the-bluff/',
  },
  '5842041f4e65fad6a7708c0a': {
    legacySpotId: 6482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/sandon-point/',
  },
  '5842041f4e65fad6a7708c0b': {
    legacySpotId: 6487,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/torquay/jan-juc/',
  },
  '5842041f4e65fad6a7708c0c': {
    legacySpotId: 6486,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/torquay/13th-beach/',
  },
  '5842041f4e65fad6a7708c0d': {
    legacySpotId: 6489,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/johanna/',
  },
  '5842041f4e65fad6a7708c0e': {
    legacySpotId: 6490,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/mornington-peninsula---phillip-island/gunnamatta/',
  },
  '5842041f4e65fad6a7708c0f': {
    legacySpotId: 6491,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/mornington-peninsula---phillip-island/point-leo/',
  },
  '584204214e65fad6a7709cbd': {
    legacySpotId: 130579,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/hightower-beach/',
  },
  '584204214e65fad6a7709cbe': {
    legacySpotId: 130583,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/coconut-point-park/',
  },
  '584204214e65fad6a7709cbf': {
    legacySpotId: 130585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/north-spessard-holland/',
  },
  '5842041f4e65fad6a7708c20': {
    legacySpotId: 6527,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/perth/cables/',
  },
  '5842041f4e65fad6a7708c21': {
    legacySpotId: 6530,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/supertubes/',
  },
  '5842041f4e65fad6a7708c22': {
    legacySpotId: 6529,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/yallingup/',
  },
  '5842041f4e65fad6a7708c23': {
    legacySpotId: 6528,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/perth/strickland-bay-rottnest-island/',
  },
  '5842041f4e65fad6a7708c24': {
    legacySpotId: 6532,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/injidup-carpark/',
  },
  '5842041f4e65fad6a7708c25': {
    legacySpotId: 6533,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/north-point/',
  },
  '5842041f4e65fad6a7708c26': {
    legacySpotId: 6531,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/smiths-beach/',
  },
  '5842041f4e65fad6a7708c27': {
    legacySpotId: 6534,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/south-point/',
  },
  '5842041f4e65fad6a7708c28': {
    legacySpotId: 6536,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/margaret-river/',
  },
  '5842041f4e65fad6a7708c29': {
    legacySpotId: 6537,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/gas-bay/',
  },
  '584204204e65fad6a77095b8': {
    legacySpotId: 73971,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/n-rre-vorup-r/',
  },
  '584204204e65fad6a77095b9': {
    legacySpotId: 73972,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-north-sea/northern-denmark/agger/',
  },
  '5842041f4e65fad6a7708e62': {
    legacySpotId: 44504,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/praia-grande/',
  },
  '5842041f4e65fad6a7708c1a': {
    legacySpotId: 6522,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/tombstones/',
  },
  '5842041f4e65fad6a7708c1b': {
    legacySpotId: 6523,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/warroora-station/',
  },
  '5842041f4e65fad6a7708c1c': {
    legacySpotId: 6485,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/two-mile-easter-reef/',
  },
  '5842041f4e65fad6a7708c1d': {
    legacySpotId: 6524,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/perth/trigg-point/',
  },
  '5842041f4e65fad6a7708c1e': {
    legacySpotId: 6525,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/perth/scarborough/',
  },
  '5842041f4e65fad6a7708c1f': {
    legacySpotId: 6526,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/perth/cottosloe/',
  },
  '5842041f4e65fad6a7708a24': {
    legacySpotId: 5207,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/sandbridge-beach/',
  },
  '5842041f4e65fad6a7708a23': {
    legacySpotId: 5211,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/north-end-83rd---86th-st-/',
  },
  '5842041f4e65fad6a7708c30': {
    legacySpotId: 6553,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/south-coast/middleton/',
  },
  '5842041f4e65fad6a7708c32': {
    legacySpotId: 6556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/superbank/',
  },
  '5842041f4e65fad6a7708c33': {
    legacySpotId: 6633,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/caribbean-costa-rica/cahuita/',
  },
  '584204214e65fad6a7709ce1': {
    legacySpotId: 132587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/steamer-lane-fixed/',
  },
  '5842041f4e65fad6a7708c35': {
    legacySpotId: 6636,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/avellanas/',
  },
  '5842041f4e65fad6a7708c36': {
    legacySpotId: 6637,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/playa-grande/',
  },
  '5842041f4e65fad6a7708c37': {
    legacySpotId: 6638,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/manuel-antonio/',
  },
  '5842041f4e65fad6a7708c38': {
    legacySpotId: 6639,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/playa-langosta/',
  },
  '5842041f4e65fad6a7708c39': {
    legacySpotId: 6641,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/rossnowlagh/',
  },
  '584204214e65fad6a7709ce7': {
    legacySpotId: 133157,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/1st-street-jetty-fixed/',
  },
  '584204214e65fad6a7709ce8': {
    legacySpotId: 133159,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/abalone-street--nags-head/',
  },
  '584204204e65fad6a7709082': {
    legacySpotId: 50530,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/south-shields/',
  },
  '584204204e65fad6a7709083': {
    legacySpotId: 50533,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/redcar/',
  },
  '584204204e65fad6a77095c8': {
    legacySpotId: 74003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/wangerooge/',
  },
  '584204204e65fad6a77095c9': {
    legacySpotId: 74000,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/sankt-peter-ording/',
  },
  '5842041f4e65fad6a7708c2a': {
    legacySpotId: 6535,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/lefthanders/',
  },
  '5842041f4e65fad6a7708c2b': {
    legacySpotId: 6550,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/yorke-peninsula/chinamans/',
  },
  '5842041f4e65fad6a7708c2c': {
    legacySpotId: 6549,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/midcoast/cactus/',
  },
  '5842041f4e65fad6a7708c2d': {
    legacySpotId: 6551,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/kangaroo-island/pennington-bay/',
  },
  '5842041f4e65fad6a7708c2e': {
    legacySpotId: 6555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/the-alley/',
  },
  '5842041f4e65fad6a7708c2f': {
    legacySpotId: 6552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/south-coast/waitpinga/',
  },
  '584204204e65fad6a77093bc': {
    legacySpotId: 66102,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/bulbaraing-bay/',
  },
  '584204214e65fad6a7709cde': {
    legacySpotId: 131676,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/lower-jetties/',
  },
  '584204214e65fad6a7709cdf': {
    legacySpotId: 131975,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/pipeline-fixed/',
  },
  '584204204e65fad6a77095c7': {
    legacySpotId: 74001,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/heligoland/',
  },
  '5842041f4e65fad6a7708c44': {
    legacySpotId: 6649,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/dunmoran/',
  },
  '584204204e65fad6a77095c5': {
    legacySpotId: 73984,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/r-dvig/',
  },
  '5842041f4e65fad6a7708c46': {
    legacySpotId: 6670,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/costa-azul/',
  },
  '5842041f4e65fad6a7708c47': {
    legacySpotId: 6681,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/middles/',
  },
  '5842041f4e65fad6a7708c48': {
    legacySpotId: 6680,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/jobos/',
  },
  '5842041f4e65fad6a7708c49': {
    legacySpotId: 6682,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/surfer-s-beach/',
  },
  '584204214e65fad6a7709b9c': {
    legacySpotId: 119396,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/katakor/',
  },
  '584204214e65fad6a7709cf8': {
    legacySpotId: 135841,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/morro-bay-harbor/',
  },
  '584204204e65fad6a77095c4': {
    legacySpotId: 73983,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/the-church/',
  },
  '584204204e65fad6a7709688': {
    legacySpotId: 78309,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/roque-dos-hermanos/',
  },
  '584204214e65fad6a7709b9e': {
    legacySpotId: 119813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/sunset-beach/',
  },
  '5842041f4e65fad6a7708c3a': {
    legacySpotId: 6642,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/tullan-strand/',
  },
  '5842041f4e65fad6a7708c3b': {
    legacySpotId: 6643,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/bundoran/',
  },
  '5842041f4e65fad6a7708c3c': {
    legacySpotId: 6644,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/mullaghmore-strand/',
  },
  '5842041f4e65fad6a7708c3d': {
    legacySpotId: 6645,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/easkey/',
  },
  '5842041f4e65fad6a7708c3e': {
    legacySpotId: 6646,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/kilcummin-harbour/',
  },
  '5842041f4e65fad6a7708c3f': {
    legacySpotId: 6647,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/streedagh-strand/',
  },
  '584204204e65fad6a77095c0': {
    legacySpotId: 73979,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/gilleleje-havn/',
  },
  '584204214e65fad6a7709cee': {
    legacySpotId: 134366,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/england/dorset/boscombe/',
  },
  '584204214e65fad6a7709cef': {
    legacySpotId: 134484,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/great-western/',
  },
  '5842041f4e65fad6a7708c50': {
    legacySpotId: 6691,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/bridges/',
  },
  '5842041f4e65fad6a7708c51': {
    legacySpotId: 6689,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/table-top/',
  },
  '5842041f4e65fad6a7708c53': {
    legacySpotId: 6692,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/antonio-s/',
  },
  '5842041f4e65fad6a7708c54': {
    legacySpotId: 6693,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/sandy-beach/',
  },
  '5842041f4e65fad6a7708c55': {
    legacySpotId: 6695,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/domes/',
  },
  '5842041f4e65fad6a7708c56': {
    legacySpotId: 6694,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/pools/',
  },
  '5842041f4e65fad6a7708c58': {
    legacySpotId: 6697,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/the-point/',
  },
  '5842041f4e65fad6a7708c59': {
    legacySpotId: 6698,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/little-malibu/',
  },
  '584204204e65fad6a7709373': {
    legacySpotId: 62229,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/little-gibber-rocks/',
  },
  '5842041f4e65fad6a7708cff': {
    legacySpotId: 7113,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/st-mikes/',
  },
  '5842041f4e65fad6a7708c4a': {
    legacySpotId: 6684,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/wishing-well/',
  },
  '5842041f4e65fad6a7708c4b': {
    legacySpotId: 6683,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/wilderness/',
  },
  '5842041f4e65fad6a7708c4c': {
    legacySpotId: 6685,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/gas-chambers/',
  },
  '5842041f4e65fad6a7708c4d': {
    legacySpotId: 6686,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/crash-boat/',
  },
  '584204214e65fad6a7709cfb': {
    legacySpotId: 135842,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/pismo-beach-north/',
  },
  '5842041f4e65fad6a7708c4f': {
    legacySpotId: 6688,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/indicators/',
  },
  '584204214e65fad6a7709cfd': {
    legacySpotId: 135844,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/c-street-fixed/',
  },
  '5842041f4e65fad6a7708cfe': {
    legacySpotId: 7112,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/doura/',
  },
  '584204214e65fad6a7709cff': {
    legacySpotId: 135847,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/malibu-overview/',
  },
  '5842041f4e65fad6a7708c60': {
    legacySpotId: 6706,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/guajataca/',
  },
  '5842041f4e65fad6a7708c61': {
    legacySpotId: 6708,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/los-tubos/',
  },
  '5842041f4e65fad6a7708c62': {
    legacySpotId: 6709,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/la-ocho/',
  },
  '5842041f4e65fad6a7708c63': {
    legacySpotId: 6710,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/chatarra/',
  },
  '5842041f4e65fad6a7708c64': {
    legacySpotId: 6711,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/aviones/',
  },
  '5842041f4e65fad6a7708c65': {
    legacySpotId: 6712,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/tocones/',
  },
  '5842041f4e65fad6a7708c66': {
    legacySpotId: 6785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/cuba/northwest-cuba/old-man-s/',
  },
  '5842041f4e65fad6a7708c67': {
    legacySpotId: 6713,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/southeast-puerto-rico/inches/',
  },
  '5842041f4e65fad6a7708c68': {
    legacySpotId: 6786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/cuba/northeast-cuba/gibara-coast/',
  },
  '5842041f4e65fad6a7708c69': {
    legacySpotId: 6787,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/cuba/northeast-cuba/baracoa-coast/',
  },
  '5842041f4e65fad6a7708c5a': {
    legacySpotId: 6699,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/tres-palmas/',
  },
  '5842041f4e65fad6a7708c5c': {
    legacySpotId: 6701,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/maria-s-overview/',
  },
  '5842041f4e65fad6a7708c5d': {
    legacySpotId: 6702,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/la-selva/',
  },
  '5842041f4e65fad6a7708c5e': {
    legacySpotId: 6703,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/la-pared/',
  },
  '5842041f4e65fad6a7708c5f': {
    legacySpotId: 6707,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/north-puerto-rico/margara/',
  },
  '584204204e65fad6a7709756': {
    legacySpotId: 91638,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/shirasuka/',
  },
  '5842041f4e65fad6a7708c70': {
    legacySpotId: 6793,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/jamaica/east-jamaica/boston-bay/',
  },
  '5842041f4e65fad6a7708c71': {
    legacySpotId: 6795,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominican-republic/north-dominican-republic/sosua/',
  },
  '5842041f4e65fad6a7708c72': {
    legacySpotId: 6796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominican-republic/north-dominican-republic/encuentro/',
  },
  '5842041f4e65fad6a7708c73': {
    legacySpotId: 6797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominican-republic/south-dominican-republic/patho-beach/',
  },
  '5842041f4e65fad6a7708c74': {
    legacySpotId: 6798,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominican-republic/south-dominican-republic/la-boya/',
  },
  '5842041f4e65fad6a7708c75': {
    legacySpotId: 6799,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominican-republic/south-dominican-republic/el-chinchorro/',
  },
  '5842041f4e65fad6a7708c76': {
    legacySpotId: 6801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/british-virgin-islands/northeast-bvi/josiah-s-bay/',
  },
  '5842041f4e65fad6a7708c77': {
    legacySpotId: 6800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominican-republic/south-dominican-republic/bahoruco/',
  },
  '5842041f4e65fad6a7708c78': {
    legacySpotId: 6802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/british-virgin-islands/northwest-bvi/apple-bay/',
  },
  '5842041f4e65fad6a7708c79': {
    legacySpotId: 6804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/trinidad-and-tobago/north-trinidad-and-tobago/airports/',
  },
  '584204214e65fad6a7709c34': {
    legacySpotId: 127098,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/balok-beach/',
  },
  '5842041f4e65fad6a7708c6a': {
    legacySpotId: 6789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/jamaica/east-jamaica/ranch/',
  },
  '5842041f4e65fad6a7708c6b': {
    legacySpotId: 6788,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/cuba/northeast-cuba/cajo-babo/',
  },
  '5842041f4e65fad6a7708c6c': {
    legacySpotId: 6791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/jamaica/east-jamaica/makka/',
  },
  '5842041f4e65fad6a7708c6d': {
    legacySpotId: 6790,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/jamaica/east-jamaica/lighthouse/',
  },
  '5842041f4e65fad6a7708c6e': {
    legacySpotId: 6794,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/jamaica/east-jamaica/zoo/',
  },
  '5842041f4e65fad6a7708c6f': {
    legacySpotId: 6792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/jamaica/east-jamaica/copa/',
  },
  '5842041f4e65fad6a7708c80': {
    legacySpotId: 6830,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-west-coast/duppies/',
  },
  '5842041f4e65fad6a7708c81': {
    legacySpotId: 6831,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-south-coast/brandon-s/',
  },
  '5842041f4e65fad6a7708c82': {
    legacySpotId: 6888,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/playa-de-rodo/',
  },
  '5842041f4e65fad6a7708c83': {
    legacySpotId: 6889,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/rodiles/',
  },
  '5842041f4e65fad6a7708c84': {
    legacySpotId: 6890,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/playa-de-los-caballos/',
  },
  '5842041f4e65fad6a7708c85': {
    legacySpotId: 6891,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/isla-de-santa-marina/',
  },
  '5842041f4e65fad6a7708c86': {
    legacySpotId: 6892,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/menakoz/',
  },
  '5842041f4e65fad6a7708c87': {
    legacySpotId: 6893,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/bakio/',
  },
  '5842041f4e65fad6a7708c88': {
    legacySpotId: 6894,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/mundaka/',
  },
  '5842041f4e65fad6a7708c89': {
    legacySpotId: 6895,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/roca-puta/',
  },
  '584204204e65fad6a770979a': {
    legacySpotId: 91707,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/omaezaki/chuushajyomae/',
  },
  '5842041f4e65fad6a7708c7b': {
    legacySpotId: 6807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/trinidad-and-tobago/north-trinidad-and-tobago/salybia-bay/',
  },
  '5842041f4e65fad6a7708c7c': {
    legacySpotId: 6808,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/trinidad-and-tobago/north-trinidad-and-tobago/balandra-bay/',
  },
  '5842041f4e65fad6a7708c7d': {
    legacySpotId: 6805,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/trinidad-and-tobago/north-trinidad-and-tobago/sans-souci-bay/',
  },
  '5842041f4e65fad6a7708c7e': {
    legacySpotId: 6828,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-east-coast/parlour/',
  },
  '5842041f4e65fad6a7708c7f': {
    legacySpotId: 6829,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-east-coast/cattle-wash/',
  },
  '5842041f4e65fad6a7708c90': {
    legacySpotId: 6904,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/doolin-point/',
  },
  '5842041f4e65fad6a7708c91': {
    legacySpotId: 6905,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/crab-island/',
  },
  '5842041f4e65fad6a7708c92': {
    legacySpotId: 6906,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/inch-reef/',
  },
  '5842041f4e65fad6a7708c93': {
    legacySpotId: 6908,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/trevaunance-cove/',
  },
  '5842041f4e65fad6a7708c94': {
    legacySpotId: 6907,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/perranporth/',
  },
  '5842041f4e65fad6a7708c95': {
    legacySpotId: 6910,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/gwithian/',
  },
  '5842041f4e65fad6a7708c96': {
    legacySpotId: 6911,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/porthmeor/',
  },
  '5842041f4e65fad6a7708c97': {
    legacySpotId: 6912,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/sennen/',
  },
  '5842041f4e65fad6a7708c98': {
    legacySpotId: 6909,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/porthtowan/',
  },
  '5842041f4e65fad6a7708c99': {
    legacySpotId: 6915,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/saltburn/',
  },
  '584204214e65fad6a7709cb2': {
    legacySpotId: 130097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/grenada/grenada/prickly-bay/',
  },
  '5842041f4e65fad6a7708c8a': {
    legacySpotId: 6896,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/playa-de-la-zurriola/',
  },
  '5842041f4e65fad6a7708c8b': {
    legacySpotId: 6898,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/northwest-france/la-torche/',
  },
  '5842041f4e65fad6a7708c8c': {
    legacySpotId: 6899,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/lafitenia/',
  },
  '5842041f4e65fad6a7708c8d': {
    legacySpotId: 6900,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/lacanau/',
  },
  '5842041f4e65fad6a7708c8e': {
    legacySpotId: 6902,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/hossegor---la-graviere/',
  },
  '5842041f4e65fad6a7708c8f': {
    legacySpotId: 6901,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/les-cavaliers/',
  },
  '5842041f4e65fad6a7708a64': {
    legacySpotId: 5246,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/kure-beach/',
  },
  '584204214e65fad6a7709c96': {
    legacySpotId: 128971,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/mozambique/south-mozambique/quissico/',
  },
  '5842041f4e65fad6a7708c9a': {
    legacySpotId: 6913,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-cornwall/praa-sands/',
  },
  '5842041f4e65fad6a7708c9b': {
    legacySpotId: 6914,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-cornwall/porthleven/',
  },
  '5842041f4e65fad6a7708c9c': {
    legacySpotId: 6917,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/sandsend/',
  },
  '5842041f4e65fad6a7708c9e': {
    legacySpotId: 6918,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/alnmouth/',
  },
  '5842041f4e65fad6a7708c9f': {
    legacySpotId: 6920,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/bamburgh/',
  },
  '59435c42e98ad90013191e1e': {
    legacySpotId: 147874,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/davis-park-marina/',
  },
  '584204204e65fad6a7709570': {
    legacySpotId: 72977,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/121-s-magney-state-park/',
  },
  '584204204e65fad6a7709115': {
    legacySpotId: 53412,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/blackies/',
  },
  '584204204e65fad6a770910c': {
    legacySpotId: 53310,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/el-astillero/',
  },
  '584204204e65fad6a7709120': {
    legacySpotId: 53639,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/ballyheirnan-bay/',
  },
  '584204204e65fad6a7709121': {
    legacySpotId: 53640,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/dunfanaghy/',
  },
  '584204204e65fad6a7709122': {
    legacySpotId: 53670,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/enniscrone/',
  },
  '584204204e65fad6a7709123': {
    legacySpotId: 53669,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/mullaghmore-head/',
  },
  '584204204e65fad6a7709124': {
    legacySpotId: 53671,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/keel-beach/',
  },
  '584204204e65fad6a7709125': {
    legacySpotId: 53672,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/carrowinskey/',
  },
  '584204204e65fad6a7709126': {
    legacySpotId: 53673,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/lahinch-beach/',
  },
  '584204204e65fad6a7709127': {
    legacySpotId: 53674,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/lahinch-cornish-left/',
  },
  '584204204e65fad6a7709128': {
    legacySpotId: 53675,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/ballybunion/',
  },
  '584204204e65fad6a7709136': {
    legacySpotId: 53807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/alaska/kodiak--alaska/',
  },
  '584204204e65fad6a7709139': {
    legacySpotId: 54140,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/bat-galim/',
  },
  '584204214e65fad6a7709cb3': {
    legacySpotId: 130201,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/poland-baltic-sea/northern-poland/rurociag/',
  },
  '584204204e65fad6a770912a': {
    legacySpotId: 53676,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/banna-beach/',
  },
  '584204204e65fad6a770912b': {
    legacySpotId: 53744,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/rossbeigh/',
  },
  '584204204e65fad6a770912c': {
    legacySpotId: 53677,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/garrywilliam-point/',
  },
  '584204204e65fad6a770912f': {
    legacySpotId: 53743,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/inch-strand/',
  },
  '584204214e65fad6a7709cb4': {
    legacySpotId: 130199,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/poland-baltic-sea/northern-poland/leba/',
  },
  '584204204e65fad6a7709540': {
    legacySpotId: 72905,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/port-burwell/',
  },
  '584204204e65fad6a7709140': {
    legacySpotId: 54551,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/camaronal/',
  },
  '584204204e65fad6a7709142': {
    legacySpotId: 54626,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/caribbean-costa-rica/playa-cocles/',
  },
  '584204204e65fad6a7709143': {
    legacySpotId: 54655,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/caribbean-costa-rica/westfalia/',
  },
  '584204204e65fad6a7709144': {
    legacySpotId: 54695,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/honu-beach/',
  },
  '584204204e65fad6a7709146': {
    legacySpotId: 54131,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/panga-drops/',
  },
  '584204204e65fad6a7709147': {
    legacySpotId: 54948,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/miramar/',
  },
  '584204204e65fad6a7709148': {
    legacySpotId: 55536,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/waikiki-beach/',
  },
  '584204204e65fad6a770913a': {
    legacySpotId: 54548,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/playa-negra/',
  },
  '584204204e65fad6a770913b': {
    legacySpotId: 54312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/hilton-beach/',
  },
  '584204204e65fad6a770913c': {
    legacySpotId: 54549,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/ostional/',
  },
  '584204204e65fad6a770913d': {
    legacySpotId: 54550,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/samara/',
  },
  '584204204e65fad6a770913e': {
    legacySpotId: 54623,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/south-costa-rica/zancudo/',
  },
  '584204204e65fad6a770913f': {
    legacySpotId: 54624,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/south-costa-rica/punta-banco/',
  },
  '584204204e65fad6a7709541': {
    legacySpotId: 72908,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/selkirk/',
  },
  '584204204e65fad6a7709542': {
    legacySpotId: 72906,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/long-point--southwest-beach-/',
  },
  '584204214e65fad6a7709ca7': {
    legacySpotId: 129686,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/baie-des-carpes/',
  },
  '584204204e65fad6a770914c': {
    legacySpotId: 56405,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/hainan-island/wenchang/moon-beach/',
  },
  '584204214e65fad6a7709c65': {
    legacySpotId: 127808,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/plakoto--ios-/',
  },
  '58e2b91045792b001324d85a': {
    legacySpotId: 146345,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/topanga-beach-overview/',
  },
  '5842041f4e65fad6a7708a7a': {
    legacySpotId: 5294,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/folly-beach-pier-southside/',
  },
  '584204204e65fad6a77096a0': {
    legacySpotId: 78338,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/rocky-point/',
  },
  '584204214e65fad6a7709c67': {
    legacySpotId: 127809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/mircos-point--rhodes-/',
  },
  '584204204e65fad6a77096a2': {
    legacySpotId: 78336,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/majanicho/',
  },
  '584204204e65fad6a77096a3': {
    legacySpotId: 78337,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/bristol/',
  },
  '584204204e65fad6a77096a4': {
    legacySpotId: 78339,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/corralejo-beach/',
  },
  '584204204e65fad6a77096a5': {
    legacySpotId: 78343,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/las-salinas/',
  },
  '584204204e65fad6a77096a6': {
    legacySpotId: 78341,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/pozo-negro/',
  },
  '584204204e65fad6a77096a7': {
    legacySpotId: 78344,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/la-pared/',
  },
  '584204204e65fad6a77096a8': {
    legacySpotId: 78345,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/playa-de-la-canteria/',
  },
  '584204204e65fad6a77096a9': {
    legacySpotId: 78346,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/jameos-del-agua/',
  },
  '584204214e65fad6a7709c1e': {
    legacySpotId: 126945,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/les-laurons/',
  },
  '584204214e65fad6a7709c6c': {
    legacySpotId: 127815,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-korea/busan/song-jung-beach/',
  },
  '584204214e65fad6a7709c66': {
    legacySpotId: 127807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/apollonas--naxos-/',
  },
  '584204214e65fad6a7709c1f': {
    legacySpotId: 126949,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/le-rouet/',
  },
  '584204214e65fad6a7709c1c': {
    legacySpotId: 126947,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/tamaris/',
  },
  '584204204e65fad6a77096b0': {
    legacySpotId: 90809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/lawrencetown-right/',
  },
  '584204204e65fad6a77096b1': {
    legacySpotId: 89981,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/ventura-point/',
  },
  '584204204e65fad6a77096b2': {
    legacySpotId: 90680,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/st-augustine-pier/',
  },
  '584204204e65fad6a77096b5': {
    legacySpotId: 90813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/lawrencetown-left-point/',
  },
  '584204204e65fad6a7709171': {
    legacySpotId: 56702,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-china/qingdao/ocean-bay-point/',
  },
  '584204214e65fad6a7709c80': {
    legacySpotId: 128390,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--baltic-sea-/toro-stenstrand/',
  },
  '584204204e65fad6a7709174': {
    legacySpotId: 56822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-china/qingdao/shell-beach/',
  },
  '584204204e65fad6a7709177': {
    legacySpotId: 56709,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/hainan-island/wanning/new-harbor-riyue-bay/',
  },
  '584204204e65fad6a7709178': {
    legacySpotId: 56824,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/hainan-island/wanning/rocky-rights-riyue-bay/',
  },
  '584204204e65fad6a7709179': {
    legacySpotId: 56823,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/hainan-island/wanning/riyue-bay-left-point/',
  },
  '5842041f4e65fad6a7708817': {
    legacySpotId: 4209,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/malibu/',
  },
  '584204204e65fad6a77096aa': {
    legacySpotId: 78347,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/playa-de-janubio/',
  },
  '584204204e65fad6a77096ab': {
    legacySpotId: 78350,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/caleta-de-caballo/',
  },
  '584204204e65fad6a77096ac': {
    legacySpotId: 78351,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/playa-de-famara/',
  },
  '584204204e65fad6a77096ad': {
    legacySpotId: 89900,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/cova-do-vapor/',
  },
  '584204204e65fad6a77096ae': {
    legacySpotId: 89901,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/pedra-branca-reef/',
  },
  '584204204e65fad6a77096af': {
    legacySpotId: 78348,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/el-golfo/',
  },
  '584204214e65fad6a7709c63': {
    legacySpotId: 127804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/leivada--tinos-/',
  },
  '5842041f4e65fad6a7708ba1': {
    legacySpotId: 5789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/playa-jaco/',
  },
  '5842041f4e65fad6a7708ba3': {
    legacySpotId: 5791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/playa-escondida/',
  },
  '5842041f4e65fad6a7708ba4': {
    legacySpotId: 5790,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/herradura/',
  },
  '5842041f4e65fad6a7708ba5': {
    legacySpotId: 5793,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/boca-barranca/',
  },
  '5842041f4e65fad6a7708ba6': {
    legacySpotId: 5792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/caldera-jetty/',
  },
  '5842041f4e65fad6a7708ba7': {
    legacySpotId: 5794,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/caribbean-costa-rica/salsa-brava/',
  },
  '5842041f4e65fad6a7708ba8': {
    legacySpotId: 5795,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/south-costa-rica/matapalo/',
  },
  '5842041f4e65fad6a7708ba9': {
    legacySpotId: 5796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/south-costa-rica/pavones/',
  },
  '584204214e65fad6a7709c75': {
    legacySpotId: 127984,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/negritos/',
  },
  '5842041f4e65fad6a7708806': {
    legacySpotId: 4189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/cowells/',
  },
  '584204214e65fad6a7709cc4': {
    legacySpotId: 130590,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/millennium-beach-park/',
  },
  '584204204e65fad6a770917a': {
    legacySpotId: 56825,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/hainan-island/wanning/rocky-lefts-riyue-bay/',
  },
  '5842041f4e65fad6a7708847': {
    legacySpotId: 4255,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/imperial-pier-ns/',
  },
  '5842041f4e65fad6a7708bb0': {
    legacySpotId: 5801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/sunzal/',
  },
  '5842041f4e65fad6a7708bb1': {
    legacySpotId: 6002,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/sand-dollar-beach/',
  },
  '58dd720e45792b001324d859': {
    legacySpotId: 145983,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/betset-beach/',
  },
  '5842041f4e65fad6a7708bb3': {
    legacySpotId: 6003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/carmel-beach/',
  },
  '5842041f4e65fad6a7708bb4': {
    legacySpotId: 6005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/big-sur-rivermouth/',
  },
  '5842041f4e65fad6a7708bb5': {
    legacySpotId: 6007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/mole-point/',
  },
  '5842041f4e65fad6a7708bb6': {
    legacySpotId: 6006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/boneyard/',
  },
  '5842041f4e65fad6a7708bb7': {
    legacySpotId: 6009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/asilomar/',
  },
  '5842041f4e65fad6a7708bb8': {
    legacySpotId: 6059,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/ghost-tree/',
  },
  '5842041f4e65fad6a77089cb': {
    legacySpotId: 5107,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/point-judith/',
  },
  '584204214e65fad6a7709c54': {
    legacySpotId: 127718,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/kalo-nero--peloponnisos-/',
  },
  '5842041f4e65fad6a77089d1': {
    legacySpotId: 5114,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/massachusetts-north-south-shore/nantasket-beach/',
  },
  '58bdf00c82d034001252e3d4': {
    legacySpotId: 145175,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/summerplace-dr-/',
  },
  '5842041f4e65fad6a7708baa': {
    legacySpotId: 5797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/playa-san-diego/',
  },
  '5842041f4e65fad6a7708bab': {
    legacySpotId: 5798,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/la-paz/',
  },
  '5842041f4e65fad6a7708bac': {
    legacySpotId: 5799,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/el-muelle--pier-/',
  },
  '5842041f4e65fad6a7708bad': {
    legacySpotId: 5800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/punta-roca/',
  },
  '5842041f4e65fad6a7708bae': {
    legacySpotId: 5802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/la-bocana/',
  },
  '5842041f4e65fad6a7708baf': {
    legacySpotId: 5803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/playa-conchalio/',
  },
  '584204204e65fad6a770918e': {
    legacySpotId: 56932,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/hainan-island/sanya/matos-houhaicun/',
  },
  '5842041f4e65fad6a77089d3': {
    legacySpotId: 5113,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/massachusetts-north-south-shore/egypt-beach/',
  },
  '5943589de98ad90013191e1d': {
    legacySpotId: 147875,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/davis-park-surf/',
  },
  '5842041f4e65fad6a7708bc0': {
    legacySpotId: 6064,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/carcavelos-fps/',
  },
  '5842041f4e65fad6a7708bc1': {
    legacySpotId: 6066,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/reef/',
  },
  '5842041f4e65fad6a7708bc2': {
    legacySpotId: 6067,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/ribeira-de-ilhas/',
  },
  '5842041f4e65fad6a7708bc3': {
    legacySpotId: 6069,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/supertubos/',
  },
  '5842041f4e65fad6a7708bc4': {
    legacySpotId: 6068,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/coxos/',
  },
  '5842041f4e65fad6a7708bc5': {
    legacySpotId: 6072,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/cabedelo/',
  },
  '5842041f4e65fad6a7708bc6': {
    legacySpotId: 6070,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/baleal/',
  },
  '5842041f4e65fad6a7708bc7': {
    legacySpotId: 6087,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/todos-santos---killers/',
  },
  '5842041f4e65fad6a7708bc8': {
    legacySpotId: 6083,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/hossegor---les-culs-nuls/',
  },
  '5842041f4e65fad6a7708bc9': {
    legacySpotId: 6088,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/san-miguel/',
  },
  '584204204e65fad6a770953a': {
    legacySpotId: 72899,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/seacliffe-beach/',
  },
  '584204204e65fad6a770952e': {
    legacySpotId: 72887,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/euclid-beach/',
  },
  '584204214e65fad6a7709bf1': {
    legacySpotId: 125759,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/chirimena/',
  },
  '5842041f4e65fad6a7708bbb': {
    legacySpotId: 6010,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/willow-creek/',
  },
  '5842041f4e65fad6a7708bbc': {
    legacySpotId: 6008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/lover-s-point/',
  },
  '5842041f4e65fad6a7708bbd': {
    legacySpotId: 6065,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/foz-de-lizandro/',
  },
  '584204214e65fad6a7709c73': {
    legacySpotId: 127983,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/punta-arena/',
  },
  '5842041f4e65fad6a7708bbf': {
    legacySpotId: 6063,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/moss-landing/',
  },
  '584204204e65fad6a770919f': {
    legacySpotId: 57471,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-china/fujian/pingtan-beachbreak/',
  },
  '5842041f4e65fad6a7708bd0': {
    legacySpotId: 6090,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/rosarito-beach/',
  },
  '5842041f4e65fad6a7708bd1': {
    legacySpotId: 6111,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/todos-santos/',
  },
  '5842041f4e65fad6a7708bd2': {
    legacySpotId: 6089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/salsipuedes/',
  },
  '5842041f4e65fad6a7708bd3': {
    legacySpotId: 6112,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/las-palmas/',
  },
  '5842041f4e65fad6a7708bd4': {
    legacySpotId: 6116,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/old-man-s/',
  },
  '5842041f4e65fad6a7708bd5': {
    legacySpotId: 6114,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/playa-cerritos/',
  },
  '5842041f4e65fad6a7708bd6': {
    legacySpotId: 6113,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/san-pedrito/',
  },
  '5842041f4e65fad6a7708bd7': {
    legacySpotId: 6117,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/the-rock/',
  },
  '5842041f4e65fad6a7708bd8': {
    legacySpotId: 6115,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/monuments/',
  },
  '5842041f4e65fad6a7708bd9': {
    legacySpotId: 6119,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/the-estuary/',
  },
  '5842041f4e65fad6a7708bca': {
    legacySpotId: 6080,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/biarritz/',
  },
  '5842041f4e65fad6a7708bcb': {
    legacySpotId: 6071,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/sao-lourenco/',
  },
  '584204214e65fad6a7709c61': {
    legacySpotId: 127802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/rafina/',
  },
  '5842041f4e65fad6a7708bcd': {
    legacySpotId: 6082,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-santocha/',
  },
  '5842041f4e65fad6a7708bce': {
    legacySpotId: 6081,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/anglet/',
  },
  '5842041f4e65fad6a7708bcf': {
    legacySpotId: 6079,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/la-cote-des-basques/',
  },
  '5842041f4e65fad6a7708be0': {
    legacySpotId: 6151,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/ninja-s/',
  },
  '5842041f4e65fad6a7708be1': {
    legacySpotId: 6152,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/lohi-s/',
  },
  '5842041f4e65fad6a7708be2': {
    legacySpotId: 6150,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/pasta-point/',
  },
  '5842041f4e65fad6a7708be3': {
    legacySpotId: 6189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/cabarita/',
  },
  '5842041f4e65fad6a7708be4': {
    legacySpotId: 6154,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/chickens/',
  },
  '5842041f4e65fad6a7708be5': {
    legacySpotId: 6190,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/snapper-rocks/',
  },
  '5842041f4e65fad6a7708be6': {
    legacySpotId: 6153,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/colas/',
  },
  '5842041f4e65fad6a7708be8': {
    legacySpotId: 6192,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/burleigh-heads/',
  },
  '5842041f4e65fad6a7708be9': {
    legacySpotId: 6191,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/kirra/',
  },
  '5842041f4e65fad6a7708bda': {
    legacySpotId: 6118,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/zippers/',
  },
  '5842041f4e65fad6a7708bdb': {
    legacySpotId: 6120,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/shipwreck/',
  },
  '5842041f4e65fad6a7708bdc': {
    legacySpotId: 6121,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/nine-palms/',
  },
  '5842041f4e65fad6a7708bdd': {
    legacySpotId: 6147,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/jailbreaks/',
  },
  '5842041f4e65fad6a7708bde': {
    legacySpotId: 6148,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/honky-s/',
  },
  '5842041f4e65fad6a7708bdf': {
    legacySpotId: 6149,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/maldives/maldives/sultans/',
  },
  '58bdf93c82d034001252e3d7': {
    legacySpotId: 144652,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/praia-grande-south-side/',
  },
  '5842041f4e65fad6a7708bf0': {
    legacySpotId: 6438,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/tuncurry-breakwall/',
  },
  '5842041f4e65fad6a7708bf1': {
    legacySpotId: 6441,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/north-wall-ballina/',
  },
  '5842041f4e65fad6a7708bf2': {
    legacySpotId: 6442,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/lennox-head/',
  },
  '5842041f4e65fad6a7708bf3': {
    legacySpotId: 6443,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/broken-head/',
  },
  '5842041f4e65fad6a7708bf4': {
    legacySpotId: 6444,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/pippies-yamba/',
  },
  '5842041f4e65fad6a7708bf5': {
    legacySpotId: 6447,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/maroubra/',
  },
  '5842041f4e65fad6a7708bf6': {
    legacySpotId: 6446,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/cronulla/',
  },
  '5842041f4e65fad6a7708bf7': {
    legacySpotId: 6448,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/manly/',
  },
  '5842041f4e65fad6a7708bf8': {
    legacySpotId: 6449,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/bondi/',
  },
  '5842041f4e65fad6a7708bf9': {
    legacySpotId: 6439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/angourie/',
  },
  '5842041f4e65fad6a7708bea': {
    legacySpotId: 6194,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/caloundra/',
  },
  '5842041f4e65fad6a7708beb': {
    legacySpotId: 6193,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/north-stradbroke-island/',
  },
  '5842041f4e65fad6a7708bec': {
    legacySpotId: 6195,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/mooloolaba/',
  },
  '5842041f4e65fad6a7708bed': {
    legacySpotId: 6319,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/mantoloking/',
  },
  '5842041f4e65fad6a7708bee': {
    legacySpotId: 6196,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/noosa-heads/',
  },
  '5842041f4e65fad6a7708bef': {
    legacySpotId: 6436,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/the-pass--byron-bay/',
  },
  '5842041f4e65fad6a7708fa5': {
    legacySpotId: 48223,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/abacateiro/',
  },
  '5842041f4e65fad6a7708fa6': {
    legacySpotId: 47717,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/lajao/',
  },
  '5842041f4e65fad6a7708fa7': {
    legacySpotId: 48225,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/sororoca/',
  },
  '5842041f4e65fad6a77089c7': {
    legacySpotId: 5097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sonoma-county/russian-rivermouth/',
  },
  '5842041f4e65fad6a77089c9': {
    legacySpotId: 5098,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sonoma-county/black-point-beach/',
  },
  '5842041f4e65fad6a77089c6': {
    legacySpotId: 5095,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sonoma-county/doran-beach/',
  },
  '5842041f4e65fad6a7708bfa': {
    legacySpotId: 6451,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/dee-why-point/',
  },
  '5842041f4e65fad6a7708bfb': {
    legacySpotId: 6450,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/curl-curl/',
  },
  '5842041f4e65fad6a7708bfc': {
    legacySpotId: 6452,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/north-narrabeen/',
  },
  '5842041f4e65fad6a7708bfd': {
    legacySpotId: 6453,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/avalon/',
  },
  '5842041f4e65fad6a7708bfe': {
    legacySpotId: 6454,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/newport/',
  },
  '5842041f4e65fad6a7708bff': {
    legacySpotId: 6473,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/pambula-rivermouth/',
  },
  '584204214e65fad6a7709c0a': {
    legacySpotId: 125992,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/vik/',
  },
  '584204214e65fad6a7709c27': {
    legacySpotId: 126957,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/les-sablettes/',
  },
  '584204214e65fad6a7709c8d': {
    legacySpotId: 128960,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/chekka/',
  },
  '584204214e65fad6a7709c21': {
    legacySpotId: 126952,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/la-ciotat/',
  },
  '584204214e65fad6a7709c23': {
    legacySpotId: 126955,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/six-floors/',
  },
  '58bdf7c682d034001252e3d6': {
    legacySpotId: 144904,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/imsouane/',
  },
  '584204214e65fad6a7709bca': {
    legacySpotId: 125292,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/erikli/',
  },
  '584204204e65fad6a7709802': {
    legacySpotId: 91811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/tottori/yabase/',
  },
  '584204204e65fad6a77090b0': {
    legacySpotId: 50865,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/trevone/',
  },
  '584204204e65fad6a77090b1': {
    legacySpotId: 50864,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/polzeath/',
  },
  '584204204e65fad6a77090b2': {
    legacySpotId: 50866,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/harlyn/',
  },
  '584204204e65fad6a77090b3': {
    legacySpotId: 50867,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/constantine/',
  },
  '584204204e65fad6a77090b4': {
    legacySpotId: 50766,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/westward-ho-/',
  },
  '584204204e65fad6a77090b5': {
    legacySpotId: 50868,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/treyarnon/',
  },
  '584204204e65fad6a77090b6': {
    legacySpotId: 50870,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/mawgan-porth/',
  },
  '584204204e65fad6a77090b7': {
    legacySpotId: 50871,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/whipsiderry/',
  },
  '584204204e65fad6a77090b8': {
    legacySpotId: 50872,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/porth/',
  },
  '584204204e65fad6a77090b9': {
    legacySpotId: 50873,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/lusty-gaze/',
  },
  '584204204e65fad6a77090aa': {
    legacySpotId: 50767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/sandymouth/',
  },
  '584204204e65fad6a77090ab': {
    legacySpotId: 50731,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/combesgate/',
  },
  '584204204e65fad6a77090ac': {
    legacySpotId: 50732,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/woolacombe-bay/',
  },
  '584204204e65fad6a77090ad': {
    legacySpotId: 50861,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/widemouth-bay/',
  },
  '584204204e65fad6a77090ae': {
    legacySpotId: 50863,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/trebarwith-strand/',
  },
  '584204204e65fad6a77090af': {
    legacySpotId: 50862,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/crackington-haven/',
  },
  '584204204e65fad6a7709810': {
    legacySpotId: 91823,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/yamaguchi/kiyo/',
  },
  '584204204e65fad6a77090c0': {
    legacySpotId: 50916,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/perran-sands/',
  },
  '584204204e65fad6a77090c1': {
    legacySpotId: 50917,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/chapel-porth/',
  },
  '584204204e65fad6a77090c2': {
    legacySpotId: 50918,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/portreath/',
  },
  '584204204e65fad6a77090c3': {
    legacySpotId: 50920,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-cornwall/swanpool/',
  },
  '584204204e65fad6a77090c4': {
    legacySpotId: 50921,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-cornwall/falmouth/',
  },
  '584204204e65fad6a77090c5': {
    legacySpotId: 50961,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-cornwall/whitsand-bay/',
  },
  '584204204e65fad6a77090c6': {
    legacySpotId: 50962,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-devon/wembury/',
  },
  '584204204e65fad6a77090c7': {
    legacySpotId: 50963,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-devon/challaborough/',
  },
  '584204204e65fad6a77090c8': {
    legacySpotId: 51079,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/aberdaron/',
  },
  '584204204e65fad6a77090c9': {
    legacySpotId: 50964,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/south-devon/bantham/',
  },
  '5842041f4e65fad6a77089e8': {
    legacySpotId: 5138,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/cupsogue/',
  },
  '584204204e65fad6a77099df': {
    legacySpotId: 110420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/mosteiros-right/',
  },
  '584204204e65fad6a77099de': {
    legacySpotId: 110421,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/santa-iria/',
  },
  '584204204e65fad6a77090ba': {
    legacySpotId: 50912,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/crantock/',
  },
  '584204204e65fad6a77090bb': {
    legacySpotId: 50874,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/tolcarne/',
  },
  '584204204e65fad6a77090bc': {
    legacySpotId: 50875,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/towan-great-western/',
  },
  '584204204e65fad6a77090bd': {
    legacySpotId: 50876,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/cribbar/',
  },
  '584204204e65fad6a77090be': {
    legacySpotId: 50914,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/holywell-bay/',
  },
  '584204204e65fad6a77090bf': {
    legacySpotId: 50919,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/gwenvor/',
  },
  '5842041f4e65fad6a7708d00': {
    legacySpotId: 7116,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/wedge/',
  },
  '5842041f4e65fad6a7708d01': {
    legacySpotId: 7118,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/new-pier/',
  },
  '5842041f4e65fad6a7708d02': {
    legacySpotId: 7115,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/cave-rock/',
  },
  '5842041f4e65fad6a7708d03': {
    legacySpotId: 7117,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/north-beach/',
  },
  '5842041f4e65fad6a7708d04': {
    legacySpotId: 7119,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/westbrook/',
  },
  '584204204e65fad6a77090d0': {
    legacySpotId: 51246,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/manorbier/',
  },
  '584204204e65fad6a77090d1': {
    legacySpotId: 51245,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/broadhaven/',
  },
  '584204204e65fad6a77090d2': {
    legacySpotId: 51250,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/rest-bay/',
  },
  '584204204e65fad6a77090d3': {
    legacySpotId: 51249,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/caswell-bay/',
  },
  '584204204e65fad6a77090d4': {
    legacySpotId: 51251,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/southerndown/',
  },
  '584204204e65fad6a77090d5': {
    legacySpotId: 51595,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/ibaraki/ohnuki-beach/',
  },
  '584204204e65fad6a77090d6': {
    legacySpotId: 51594,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/iwate/toyomazawa/',
  },
  '584204204e65fad6a77090d7': {
    legacySpotId: 51247,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/tenby---south-beach/',
  },
  '584204204e65fad6a77090d8': {
    legacySpotId: 51599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/ibaraki/hirai-beach/',
  },
  '584204204e65fad6a77090d9': {
    legacySpotId: 51598,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/ibaraki/kashima/',
  },
  '584204204e65fad6a77099e4': {
    legacySpotId: 110426,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/santa-cruz/',
  },
  '584204204e65fad6a77090ca': {
    legacySpotId: 51080,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/barmouth/',
  },
  '584204204e65fad6a77090cb': {
    legacySpotId: 51081,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/tywyn/',
  },
  '584204204e65fad6a77090cc': {
    legacySpotId: 51241,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/borth/',
  },
  '584204204e65fad6a77090cd': {
    legacySpotId: 51242,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/llangrannog/',
  },
  '584204204e65fad6a77090ce': {
    legacySpotId: 51243,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/whitesands/',
  },
  '584204204e65fad6a77090cf': {
    legacySpotId: 51244,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/newgale/',
  },
  '5842041f4e65fad6a7708e60': {
    legacySpotId: 44500,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/praia-de-mira/',
  },
  '5842041f4e65fad6a7708d10': {
    legacySpotId: 7131,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/elands-bay/',
  },
  '584204204e65fad6a7709831': {
    legacySpotId: 91858,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/kochi/hirano/',
  },
  '5842041f4e65fad6a7708d12': {
    legacySpotId: 7129,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/coffee-bay/',
  },
  '5842041f4e65fad6a7708d13': {
    legacySpotId: 7162,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southeastern-sri-lanka/okanda/',
  },
  '5842041f4e65fad6a7708d14': {
    legacySpotId: 7165,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southeastern-sri-lanka/pottuvil-point/',
  },
  '5842041f4e65fad6a7708d15': {
    legacySpotId: 7132,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/flowrider/',
  },
  '584204204e65fad6a77090e1': {
    legacySpotId: 51645,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/hamamatsu-maihama/shiomizaka---hamamatsu/',
  },
  '584204204e65fad6a77090e2': {
    legacySpotId: 51625,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/chigasaki-beach---shonan/',
  },
  '5842041f4e65fad6a7708d18': {
    legacySpotId: 7166,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southeastern-sri-lanka/peanut-farm/',
  },
  '584204204e65fad6a77090e4': {
    legacySpotId: 51647,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/irako/irago-loco/',
  },
  '584204204e65fad6a77090e5': {
    legacySpotId: 51646,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/irako/irago-long-beach/',
  },
  '584204204e65fad6a77090e6': {
    legacySpotId: 51648,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/mie-ise/kokufunohama/',
  },
  '584204204e65fad6a77090e7': {
    legacySpotId: 51649,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/mie-ise/ichigohama/',
  },
  '584204204e65fad6a77090e9': {
    legacySpotId: 51652,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/tokushima/ikumi/',
  },
  '5842041f4e65fad6a77089de': {
    legacySpotId: 5126,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/scarborough-beach-park/',
  },
  '5842041f4e65fad6a7708d0a': {
    legacySpotId: 7124,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/bettys-bay/',
  },
  '5842041f4e65fad6a7708d0b': {
    legacySpotId: 7125,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/j-bay/seal-point/',
  },
  '5842041f4e65fad6a7708d0c': {
    legacySpotId: 7126,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/j-bay/anne-avenue/',
  },
  '5842041f4e65fad6a7708d0d': {
    legacySpotId: 7127,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/eastern-cape/nahoon-reef/',
  },
  '5842041f4e65fad6a7708d0e': {
    legacySpotId: 7130,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/sunset-reef/',
  },
  '584204204e65fad6a77090da': {
    legacySpotId: 51615,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/shiinauchi/',
  },
  '5842041f4e65fad6a77089dc': {
    legacySpotId: 5123,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/higgins-beach/',
  },
  '584204204e65fad6a77090de': {
    legacySpotId: 51623,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/kamakura---shonan/',
  },
  '5842041f4e65fad6a7708d20': {
    legacySpotId: 7174,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/malibupeta/',
  },
  '5842041f4e65fad6a7708d21': {
    legacySpotId: 7176,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/jarawa-point/',
  },
  '5842041f4e65fad6a7708d22': {
    legacySpotId: 7178,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/mauritius/mauritius/souillac/',
  },
  '5842041f4e65fad6a7708d24': {
    legacySpotId: 7181,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/mauritius/mauritius/passe-de-l-ambulante/',
  },
  '5842041f4e65fad6a7708d25': {
    legacySpotId: 7180,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/mauritius/mauritius/one-eye-s/',
  },
  '5842041f4e65fad6a7708d26': {
    legacySpotId: 7182,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/mauritius/mauritius/tamarin-bay/',
  },
  '5842041f4e65fad6a7708d27': {
    legacySpotId: 7183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/reunion-island/reunion-island/etang-sale-les-bains/',
  },
  '5842041f4e65fad6a7708d28': {
    legacySpotId: 7184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/reunion-island/reunion-island/la-pointe-au-sel/',
  },
  '5842041f4e65fad6a7708d29': {
    legacySpotId: 7185,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/reunion-island/reunion-island/st-leu/',
  },
  '584204214e65fad6a7709c10': {
    legacySpotId: 126356,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/tullaghan-left/',
  },
  '5842041f4e65fad6a7708d1a': {
    legacySpotId: 7169,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/lighthouse-beach/',
  },
  '5842041f4e65fad6a7708d1c': {
    legacySpotId: 7171,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/east-point-beachbreak/',
  },
  '5842041f4e65fad6a7708d1d': {
    legacySpotId: 7172,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/lawson-s-bay/',
  },
  '5842041f4e65fad6a7708d1e': {
    legacySpotId: 7173,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/the-park/',
  },
  '5842041f4e65fad6a7708d1f': {
    legacySpotId: 7175,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/bheemi-point/',
  },
  '584204214e65fad6a7709c14': {
    legacySpotId: 126938,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/canet-plage/',
  },
  '584204204e65fad6a77099f6': {
    legacySpotId: 110444,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/faja-dos-cubres/',
  },
  '584204214e65fad6a7709bd0': {
    legacySpotId: 125299,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/sile/',
  },
  '584204204e65fad6a7709404': {
    legacySpotId: 67096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/coalcliff/',
  },
  '5842041f4e65fad6a7708d30': {
    legacySpotId: 7194,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/west-java/g-land/',
  },
  '5842041f4e65fad6a7708d31': {
    legacySpotId: 7198,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/west-java/baya/',
  },
  '5842041f4e65fad6a7708d32': {
    legacySpotId: 7195,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/west-java/turtles/',
  },
  '5842041f4e65fad6a7708d33': {
    legacySpotId: 7197,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/west-java/cimaja/',
  },
  '5842041f4e65fad6a7708d34': {
    legacySpotId: 7196,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/west-java/ombak-tujuh/',
  },
  '5842041f4e65fad6a7708d35': {
    legacySpotId: 7199,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/thunders-mentawais/',
  },
  '5842041f4e65fad6a7708d36': {
    legacySpotId: 7200,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/macaronis/',
  },
  '5842041f4e65fad6a7708d37': {
    legacySpotId: 7201,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/lances-right/',
  },
  '5842041f4e65fad6a7708d38': {
    legacySpotId: 7203,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/telescopes/',
  },
  '5842041f4e65fad6a7708d39': {
    legacySpotId: 7202,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/lances-left/',
  },
  '5842041f4e65fad6a7708d2a': {
    legacySpotId: 7186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/reunion-island/reunion-island/st--gilles-les-bains/',
  },
  '5842041f4e65fad6a7708d2b': {
    legacySpotId: 7190,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/panaitan-island/inside-lefts/',
  },
  '5842041f4e65fad6a7708d2c': {
    legacySpotId: 7191,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/panaitan-island/inside-rights/',
  },
  '5842041f4e65fad6a7708d2d': {
    legacySpotId: 7189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/panaitan-island/one-palm-point/',
  },
  '5842041f4e65fad6a7708d2e': {
    legacySpotId: 7192,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/panaitan-island/panaitan-rights/',
  },
  '5842041f4e65fad6a7708d2f': {
    legacySpotId: 7193,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/panaitan-island/bombies/',
  },
  '5842041f4e65fad6a7708d40': {
    legacySpotId: 7210,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/canggu/',
  },
  '5842041f4e65fad6a7708d41': {
    legacySpotId: 7211,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/balian/',
  },
  '5842041f4e65fad6a7708d42': {
    legacySpotId: 7213,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/impossibles/',
  },
  '5842041f4e65fad6a7708d43': {
    legacySpotId: 7212,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/medewi/',
  },
  '5842041f4e65fad6a7708d44': {
    legacySpotId: 7214,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/padang-padang/',
  },
  '5842041f4e65fad6a7708d46': {
    legacySpotId: 7216,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/nyang-nyang/',
  },
  '5842041f4e65fad6a7708d47': {
    legacySpotId: 7218,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/nusa-dua/',
  },
  '5842041f4e65fad6a7708d48': {
    legacySpotId: 7217,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/green-ball/',
  },
  '5842041f4e65fad6a7708d49': {
    legacySpotId: 7220,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/serangan/',
  },
  '584204204e65fad6a770985a': {
    legacySpotId: 91898,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/kumamoto/takahama/',
  },
  '5842041f4e65fad6a7708d3b': {
    legacySpotId: 7204,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/bingin/',
  },
  '5842041f4e65fad6a7708d3c': {
    legacySpotId: 7205,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/dreamland/',
  },
  '5842041f4e65fad6a7708d3d': {
    legacySpotId: 7207,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/airport-rights/',
  },
  '5842041f4e65fad6a7708d3e': {
    legacySpotId: 7209,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/kuta-beach/',
  },
  '5842041f4e65fad6a7708d3f': {
    legacySpotId: 7208,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/kuta-reef/',
  },
  '584204204e65fad6a77099ea': {
    legacySpotId: 110432,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/ribeira-quente-right/',
  },
  '584204204e65fad6a77099ef': {
    legacySpotId: 110439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/pescador/',
  },
  '5842041f4e65fad6a7708d50': {
    legacySpotId: 7225,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/lombok/grupuk/',
  },
  '5842041f4e65fad6a7708d51': {
    legacySpotId: 7227,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/lombok/belongas-bay/',
  },
  '5842041f4e65fad6a7708d52': {
    legacySpotId: 7229,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/sumbawa/lakey-peak/',
  },
  '5842041f4e65fad6a7708d53': {
    legacySpotId: 7230,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/sumbawa/periscopes/',
  },
  '5842041f4e65fad6a7708d54': {
    legacySpotId: 7231,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/sumbawa/yo-yos/',
  },
  '584204204e65fad6a7709875': {
    legacySpotId: 91927,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/kagoshima/oshikiri/',
  },
  '5842041f4e65fad6a7708d56': {
    legacySpotId: 7232,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/sumbawa/supersuck/',
  },
  '5842041f4e65fad6a7708d57': {
    legacySpotId: 7233,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/sumbawa/scar-reef/',
  },
  '5842041f4e65fad6a7708d58': {
    legacySpotId: 7282,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/king-kong-left--nagigia-island/',
  },
  '5842041f4e65fad6a7708d59': {
    legacySpotId: 7234,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/timor/t-land/',
  },
  '584204214e65fad6a7709bd2': {
    legacySpotId: 125296,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/suma-beach/',
  },
  '5842041f4e65fad6a7708d4a': {
    legacySpotId: 7219,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/sri-lanka/',
  },
  '5842041f4e65fad6a7708d4b': {
    legacySpotId: 7222,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/sanur/',
  },
  '5842041f4e65fad6a7708d4c': {
    legacySpotId: 7221,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/hyatt-reef/',
  },
  '5842041f4e65fad6a7708d4d': {
    legacySpotId: 7224,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/lombok/ekas/',
  },
  '5842041f4e65fad6a7708d4e': {
    legacySpotId: 7223,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/keramas/',
  },
  '5842041f4e65fad6a7708d4f': {
    legacySpotId: 7226,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/lombok/kuta/',
  },
  '584204204e65fad6a77099c3': {
    legacySpotId: 109305,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-west-coast/tropicana/',
  },
  '5842041f4e65fad6a7708d60': {
    legacySpotId: 7293,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/micronesia/pohnpei/napali/',
  },
  '5842041f4e65fad6a7708d61': {
    legacySpotId: 7292,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-north/papenoo-rivermouth/',
  },
  '5842041f4e65fad6a7708d62': {
    legacySpotId: 7289,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-south/papara/',
  },
  '5842041f4e65fad6a7708d63': {
    legacySpotId: 7291,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-south/vairao/',
  },
  '5842041f4e65fad6a7708d64': {
    legacySpotId: 7294,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/micronesia/pohnpei/center-channel/',
  },
  '5842041f4e65fad6a7708d65': {
    legacySpotId: 7297,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/micronesia/pohnpei/p-pass/',
  },
  '5842041f4e65fad6a7708d66': {
    legacySpotId: 7295,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/micronesia/pohnpei/mwahnd-pass/',
  },
  '5842041f4e65fad6a7708d67': {
    legacySpotId: 7296,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/micronesia/pohnpei/main-pass/',
  },
  '5842041f4e65fad6a7708d68': {
    legacySpotId: 7299,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/papua-new-guinea/kavieng/ral-island/',
  },
  '5842041f4e65fad6a7708d69': {
    legacySpotId: 7301,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/papua-new-guinea/kavieng/nago/',
  },
  '5842041f4e65fad6a7708d5a': {
    legacySpotId: 7284,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/namotu-left/',
  },
  '5842041f4e65fad6a7708d5b': {
    legacySpotId: 7288,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-south/maraa/',
  },
  '5842041f4e65fad6a7708d5c': {
    legacySpotId: 7285,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/wilkes-pass--namotu/',
  },
  '5842041f4e65fad6a7708d5d': {
    legacySpotId: 7283,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/swimming-pools--namotu/',
  },
  '5842041f4e65fad6a7708d5e': {
    legacySpotId: 7286,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/frigate-pass/',
  },
  '5842041f4e65fad6a7708d5f': {
    legacySpotId: 7290,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-south/taapuna/',
  },
  '584204204e65fad6a77099eb': {
    legacySpotId: 110434,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/quatro-ribeiras/',
  },
  '5842041f4e65fad6a7708d70': {
    legacySpotId: 7307,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/new-caledonia/new-caledonia/dumbea-left/',
  },
  '5842041f4e65fad6a7708d71': {
    legacySpotId: 7308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/new-caledonia/new-caledonia/skate-park-boulari-pass/',
  },
  '5842041f4e65fad6a7708d72': {
    legacySpotId: 7310,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/northern-philippines/sabang-beach/',
  },
  '5842041f4e65fad6a7708d73': {
    legacySpotId: 7311,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/southern-philippines/abc-s/',
  },
  '5842041f4e65fad6a7708d74': {
    legacySpotId: 7312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/northern-philippines/puraran-bay/',
  },
  '5842041f4e65fad6a7708d75': {
    legacySpotId: 7313,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/philippines---south-china-sea/monaliza/',
  },
  '5842041f4e65fad6a7708d76': {
    legacySpotId: 7314,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/philippines---south-china-sea/san-antonio/',
  },
  '5842041f4e65fad6a7708d77': {
    legacySpotId: 7315,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/northern-philippines/bagasbas-beach/',
  },
  '5842041f4e65fad6a7708d78': {
    legacySpotId: 7319,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/southern-philippines/pacifico/',
  },
  '5842041f4e65fad6a7708d79': {
    legacySpotId: 7316,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/philippines---south-china-sea/lefthanders/',
  },
  '584204204e65fad6a77099d7': {
    legacySpotId: 110387,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/praia-de-santa-barbara-areias-/',
  },
  '584204204e65fad6a77099d6': {
    legacySpotId: 110167,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/popham-beach/',
  },
  '584204204e65fad6a77099d5': {
    legacySpotId: 110193,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/fonte-da-telha/',
  },
  '5842041f4e65fad6a7708964': {
    legacySpotId: 4994,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/sands/',
  },
  '5842041f4e65fad6a7708d6a': {
    legacySpotId: 7300,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/papua-new-guinea/kavieng/pikinini/',
  },
  '5842041f4e65fad6a7708967': {
    legacySpotId: 4999,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/hammond-s/',
  },
  '5842041f4e65fad6a7708968': {
    legacySpotId: 4993,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/el-capitan/',
  },
  '5842041f4e65fad6a7708d6d': {
    legacySpotId: 7304,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/new-caledonia/new-caledonia/dumbea-right/',
  },
  '5842041f4e65fad6a7708d6e': {
    legacySpotId: 7305,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/new-caledonia/new-caledonia/st--vincent-pass/',
  },
  '5842041f4e65fad6a7708d6f': {
    legacySpotId: 7306,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/new-caledonia/new-caledonia/false-pass/',
  },
  '5842041f4e65fad6a7708d80': {
    legacySpotId: 7361,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/shipwreck-bay/',
  },
  '5842041f4e65fad6a7708d81': {
    legacySpotId: 7363,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/the-island/',
  },
  '5842041f4e65fad6a7708d82': {
    legacySpotId: 7364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/wainui-beach/',
  },
  '5842041f4e65fad6a7708d83': {
    legacySpotId: 7365,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/taronui-bay/',
  },
  '5842041f4e65fad6a7708d84': {
    legacySpotId: 7366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/mt-maunganui/',
  },
  '5842041f4e65fad6a7708d85': {
    legacySpotId: 7368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/hickory-bay/',
  },
  '5842041f4e65fad6a7708d86': {
    legacySpotId: 7369,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/motunau-beach-island/',
  },
  '5842041f4e65fad6a7708d87': {
    legacySpotId: 7371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/stoney-beach/',
  },
  '5842041f4e65fad6a7708d88': {
    legacySpotId: 7370,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/mangamanu/',
  },
  '5842041f4e65fad6a7708d89': {
    legacySpotId: 7372,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/aramoana/',
  },
  '584204204e65fad6a7709a23': {
    legacySpotId: 111392,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/estero-de-platano/',
  },
  '584204204e65fad6a7709a24': {
    legacySpotId: 111391,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/mompiche/',
  },
  '584204204e65fad6a7709a25': {
    legacySpotId: 111393,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/atacames/',
  },
  '5842041f4e65fad6a7708d7a': {
    legacySpotId: 7317,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/southern-philippines/cloud-9/',
  },
  '5842041f4e65fad6a7708d7b': {
    legacySpotId: 7318,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/southern-philippines/lanuza-rivermouth/',
  },
  '5842041f4e65fad6a7708978': {
    legacySpotId: 5012,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/pedro-point/',
  },
  '5842041f4e65fad6a7708979': {
    legacySpotId: 5014,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/rockaway/',
  },
  '5842041f4e65fad6a7708d7e': {
    legacySpotId: 7358,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/raglan/',
  },
  '5842041f4e65fad6a7708d7f': {
    legacySpotId: 7360,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/bogan-river/',
  },
  '584204204e65fad6a7709a20': {
    legacySpotId: 111387,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/la-bellaca/',
  },
  '584204204e65fad6a7709a1b': {
    legacySpotId: 111382,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/rio-chico/',
  },
  '584204204e65fad6a7709a1c': {
    legacySpotId: 111383,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/puerto-cayo/',
  },
  '584204204e65fad6a7709a1d': {
    legacySpotId: 111384,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/las-pinas-la-antena/',
  },
  '5842041f4e65fad6a7708d90': {
    legacySpotId: 7413,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/niijima/habushi-beach/',
  },
  '5842041f4e65fad6a7708d91': {
    legacySpotId: 7416,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/tsujido/',
  },
  '5842041f4e65fad6a7708d92': {
    legacySpotId: 7414,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/kouzushima/shirahama/',
  },
  '5842041f4e65fad6a7708d93': {
    legacySpotId: 7415,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/shonan/',
  },
  '5842041f4e65fad6a7708d94': {
    legacySpotId: 7417,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/kugenuma-beach---shonan/',
  },
  '5842041f4e65fad6a7708d96': {
    legacySpotId: 7419,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/kamogawa/',
  },
  '5842041f4e65fad6a7708d97': {
    legacySpotId: 7421,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/torami-beach/',
  },
  '5842041f4e65fad6a7708d98': {
    legacySpotId: 7420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/hebara-beach/',
  },
  '5842041f4e65fad6a7708d99': {
    legacySpotId: 7422,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/fukushima/kitaizumi/',
  },
  '584204204e65fad6a7709a34': {
    legacySpotId: 111458,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/pico-pico/',
  },
  '584204204e65fad6a7709a35': {
    legacySpotId: 111460,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/el-mystic/',
  },
  '584204204e65fad6a7709a36': {
    legacySpotId: 111459,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/pico-de-loro/',
  },
  '5842041f4e65fad6a7708d8a': {
    legacySpotId: 7373,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/haast-beach/',
  },
  '5842041f4e65fad6a7708d8b': {
    legacySpotId: 7374,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/little-wanganui/',
  },
  '5842041f4e65fad6a7708d8c': {
    legacySpotId: 7375,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/westport-breakwater/',
  },
  '5842041f4e65fad6a7708d8d': {
    legacySpotId: 7376,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/cobden/',
  },
  '5842041f4e65fad6a7708d8e': {
    legacySpotId: 7377,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/blaketown-wedge/',
  },
  '5842041f4e65fad6a7708d8f': {
    legacySpotId: 7379,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/stent-road/',
  },
  '584204204e65fad6a7709a0e': {
    legacySpotId: 111366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/fae/',
  },
  '584204204e65fad6a7709a31': {
    legacySpotId: 111407,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/barahona/',
  },
  '584204204e65fad6a7709a32': {
    legacySpotId: 111453,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/secret-right/',
  },
  '584204204e65fad6a7709a0b': {
    legacySpotId: 111363,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/la-diablica/',
  },
  '584204204e65fad6a7709a0c': {
    legacySpotId: 111365,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/ecuasal/',
  },
  '584204204e65fad6a7709a08': {
    legacySpotId: 111362,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/engabao/',
  },
  '584204204e65fad6a7709200': {
    legacySpotId: 59461,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-rosarito/puerto-san-andres/',
  },
  '584204204e65fad6a7709201': {
    legacySpotId: 59370,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/el-cardon/',
  },
  '584204204e65fad6a7709202': {
    legacySpotId: 59367,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-cono/',
  },
  '584204214e65fad6a7709bc1': {
    legacySpotId: 123341,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/zona-norte/',
  },
  '584204204e65fad6a7709a44': {
    legacySpotId: 111477,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/pradomar/',
  },
  '584204204e65fad6a7709205': {
    legacySpotId: 59465,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-rosarito/the-wall/',
  },
  '584204204e65fad6a7709206': {
    legacySpotId: 59466,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-rosarito/millers-landing/',
  },
  '584204204e65fad6a7709207': {
    legacySpotId: 59468,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-rosarito/playa-elefante--isla-cedros-/',
  },
  '584204204e65fad6a7709208': {
    legacySpotId: 59547,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/cuyutlan/',
  },
  '584204204e65fad6a7709a46': {
    legacySpotId: 111478,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/el-bolsillo/',
  },
  '5842041f4e65fad6a7708d9a': {
    legacySpotId: 7362,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/whangapoua-beach/',
  },
  '5842041f4e65fad6a7708d9b': {
    legacySpotId: 7423,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/miyagi/sendai-shinko/',
  },
  '5842041f4e65fad6a7708d9c': {
    legacySpotId: 7424,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/irako/irago/',
  },
  '5842041f4e65fad6a7708d9d': {
    legacySpotId: 7425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/kyoto/hacchouhama/',
  },
  '5842041f4e65fad6a7708d9f': {
    legacySpotId: 7427,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/okinawa/okinawa/sunabe-seawall/',
  },
  '584204214e65fad6a7709ca8': {
    legacySpotId: 129688,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/secret-spot/',
  },
  '584204204e65fad6a7709a42': {
    legacySpotId: 111474,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/las-velas/',
  },
  '584204204e65fad6a7709a43': {
    legacySpotId: 111473,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/cartagena-jetty/',
  },
  '584204204e65fad6a7709a40': {
    legacySpotId: 111472,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/hilton/',
  },
  '584204204e65fad6a7709a41': {
    legacySpotId: 111471,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/castillogrande/',
  },
  '584204204e65fad6a7709a17': {
    legacySpotId: 111378,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/ayampe/',
  },
  '584204204e65fad6a7709210': {
    legacySpotId: 59581,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/boca-de-apisa/',
  },
  '584204204e65fad6a7709211': {
    legacySpotId: 59583,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/abreojos/campo-renes/',
  },
  '584204204e65fad6a7709212': {
    legacySpotId: 59587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/abreojos/punta-santo-domingo/',
  },
  '584204204e65fad6a7709213': {
    legacySpotId: 59372,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-negra/',
  },
  '584204204e65fad6a7709214': {
    legacySpotId: 59371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-ositos/',
  },
  '584204204e65fad6a7709215': {
    legacySpotId: 59368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-maria/',
  },
  '584204204e65fad6a7709216': {
    legacySpotId: 59589,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/abreojos/la-laguna/',
  },
  '584204204e65fad6a7709217': {
    legacySpotId: 59590,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/las-brisas/',
  },
  '584204204e65fad6a7709218': {
    legacySpotId: 59599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/turtle-bay-east/',
  },
  '584204204e65fad6a7709219': {
    legacySpotId: 59603,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/huahua/',
  },
  '584204204e65fad6a7709a12': {
    legacySpotId: 111372,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/capaes/',
  },
  '584204204e65fad6a7709a13': {
    legacySpotId: 111373,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/coito/',
  },
  '584204204e65fad6a7709a14': {
    legacySpotId: 111374,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/montanita-beachbreak/',
  },
  '584204214e65fad6a7709ca4': {
    legacySpotId: 129684,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/ngor-left/',
  },
  '584204204e65fad6a770920a': {
    legacySpotId: 59556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/el-paraiso/',
  },
  '584204204e65fad6a770920b': {
    legacySpotId: 59554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/punta-eugenia/bahia-tortugas--turtle-bay-/',
  },
  '584204204e65fad6a770920c': {
    legacySpotId: 59561,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/punta-eugenia/asuncion-bay/',
  },
  '584204204e65fad6a770920d': {
    legacySpotId: 59564,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/pascuales/',
  },
  '584204204e65fad6a770920e': {
    legacySpotId: 59569,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/punta-eugenia/san-hipolito/',
  },
  '584204204e65fad6a770920f': {
    legacySpotId: 59575,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/punta-eugenia/la-bocana/',
  },
  '584204204e65fad6a7709a10': {
    legacySpotId: 111370,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/paco-illescas/',
  },
  '584204204e65fad6a7709a2c': {
    legacySpotId: 111402,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/cerro-gallina/',
  },
  '584204204e65fad6a7709a2d': {
    legacySpotId: 111401,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/tortuga-bay/',
  },
  '584204204e65fad6a7709a2e': {
    legacySpotId: 111403,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/seymour-norte/',
  },
  '584204204e65fad6a7709220': {
    legacySpotId: 59656,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/southern-points-region/punta-san-gregorio/',
  },
  '584204204e65fad6a7709222': {
    legacySpotId: 59657,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/southern-points-region/las-barrancas/',
  },
  '584204204e65fad6a7709223': {
    legacySpotId: 59658,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/southern-points-region/san-jorge/',
  },
  '584204204e65fad6a7709224': {
    legacySpotId: 59665,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/ixtapa/',
  },
  '584204204e65fad6a7709225': {
    legacySpotId: 59663,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/playa-linda/',
  },
  '584204204e65fad6a7709226': {
    legacySpotId: 59666,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/las-gatas/',
  },
  '584204204e65fad6a7709227': {
    legacySpotId: 59668,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/la-barrita/',
  },
  '584204204e65fad6a7709228': {
    legacySpotId: 59667,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/barra-de-potosi/',
  },
  '584204214e65fad6a7709cba': {
    legacySpotId: 130559,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/54th-st-/',
  },
  '584204214e65fad6a7709cbb': {
    legacySpotId: 130577,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/ponce-de-leon-park/',
  },
  '584204204e65fad6a770921a': {
    legacySpotId: 59602,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/la-ticla/',
  },
  '584204204e65fad6a770921b': {
    legacySpotId: 59605,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/playa-azul/',
  },
  '584204204e65fad6a770921c': {
    legacySpotId: 59604,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/rio-nexpa/',
  },
  '584204204e65fad6a770921d': {
    legacySpotId: 59606,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/the-ranch/',
  },
  '584204204e65fad6a770921e': {
    legacySpotId: 59607,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/la-saladita/',
  },
  '584204204e65fad6a770921f': {
    legacySpotId: 59608,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/troncones/',
  },
  '584204204e65fad6a7709230': {
    legacySpotId: 59817,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/conejo/playa-san-pedro/',
  },
  '584204204e65fad6a7709231': {
    legacySpotId: 59830,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/acapulco-area/papanoa/',
  },
  '584204204e65fad6a7709232': {
    legacySpotId: 59831,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/acapulco-area/playa-michigan/',
  },
  '584204204e65fad6a7709233': {
    legacySpotId: 59832,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/acapulco-area/playa-paraiso/',
  },
  '584204204e65fad6a7709234': {
    legacySpotId: 59833,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/acapulco-area/revolcadero/',
  },
  '584204204e65fad6a7709235': {
    legacySpotId: 59836,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/acapulco-area/barra-vieja/',
  },
  '584204204e65fad6a7709236': {
    legacySpotId: 59838,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/chacahua/',
  },
  '584204204e65fad6a7709237': {
    legacySpotId: 59839,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/cerro-hermosa/',
  },
  '584204204e65fad6a7709238': {
    legacySpotId: 60482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/carrizalillo/',
  },
  '584204204e65fad6a7709a4b': {
    legacySpotId: 111483,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/buritaca/',
  },
  '584204204e65fad6a7709a4c': {
    legacySpotId: 111484,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/viento-fresco/',
  },
  '584204204e65fad6a7709a4a': {
    legacySpotId: 111482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/casa-grande/',
  },
  '584204204e65fad6a770922a': {
    legacySpotId: 59669,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/lazaro-cardenas-area/puerto-vincante-guerro/',
  },
  '584204204e65fad6a770922b': {
    legacySpotId: 59810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/magdalena-bay/mag-bay/',
  },
  '584204204e65fad6a770922c': {
    legacySpotId: 59813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/conejo/punta-marquez/',
  },
  '584204204e65fad6a770922d': {
    legacySpotId: 59814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/conejo/beachbreak/',
  },
  '584204204e65fad6a770922e': {
    legacySpotId: 59815,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/conejo/la-bocana/',
  },
  '584204204e65fad6a770922f': {
    legacySpotId: 59816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/conejo/la-pastora/',
  },
  '584204214e65fad6a7709cc0': {
    legacySpotId: 130586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/watson-dr-/',
  },
  '584204204e65fad6a7709a4e': {
    legacySpotId: 111510,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/barra-de-la-gabina/',
  },
  '584204214e65fad6a7709cc1': {
    legacySpotId: 130587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/paradise-beach/',
  },
  '584204214e65fad6a7709cc2': {
    legacySpotId: 130588,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/canova-beach/',
  },
  '584204204e65fad6a7709240': {
    legacySpotId: 60497,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/san-agustin/',
  },
  '584204204e65fad6a7709241': {
    legacySpotId: 60555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/playa-la-bocana/',
  },
  '584204204e65fad6a7709242': {
    legacySpotId: 60556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/playa-el-mojon/',
  },
  '584204204e65fad6a7709243': {
    legacySpotId: 60686,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/punta-chipehua/',
  },
  '584204204e65fad6a7709244': {
    legacySpotId: 60580,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/barra-de-la-cruz/',
  },
  '584204204e65fad6a7709245': {
    legacySpotId: 60688,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/salina-cruz/',
  },
  '584204204e65fad6a7709246': {
    legacySpotId: 60708,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/puerto-madero/',
  },
  '584204204e65fad6a7709248': {
    legacySpotId: 60712,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/punta-gasparino/',
  },
  '584204204e65fad6a7709249': {
    legacySpotId: 60713,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/migrino/',
  },
  '584204204e65fad6a7709a3a': {
    legacySpotId: 111465,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/tribuga/',
  },
  '584204204e65fad6a7709a3b': {
    legacySpotId: 111464,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/nuqui/',
  },
  '584204204e65fad6a7709a01': {
    legacySpotId: 111349,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/512-km-812-km/',
  },
  '584204204e65fad6a7709a02': {
    legacySpotId: 111354,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/chabela/',
  },
  '584204204e65fad6a7709a03': {
    legacySpotId: 111353,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/los-patios/',
  },
  '584204204e65fad6a770923b': {
    legacySpotId: 60488,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/barra-de-colotepec/',
  },
  '584204204e65fad6a770923c': {
    legacySpotId: 60487,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/la-punta/',
  },
  '584204204e65fad6a770923d': {
    legacySpotId: 60491,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/san-agustinillo/',
  },
  '584204204e65fad6a770923e': {
    legacySpotId: 60494,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/zipolite/',
  },
  '584204204e65fad6a770923f': {
    legacySpotId: 60496,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/rio-de-coyula/',
  },
  '584204204e65fad6a7709a3c': {
    legacySpotId: 111466,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/jurubida/',
  },
  '584204214e65fad6a7709c71': {
    legacySpotId: 127980,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/cabo-blanquillo/',
  },
  '584204204e65fad6a7709a3d': {
    legacySpotId: 111468,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/derecha-secreta/',
  },
  '584204204e65fad6a7709a3e': {
    legacySpotId: 111470,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/colpipe/',
  },
  '584204204e65fad6a7709250': {
    legacySpotId: 60720,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/los-frailes/',
  },
  '584204204e65fad6a7709251': {
    legacySpotId: 60721,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/cabo-pulmo/',
  },
  '584204204e65fad6a7709252': {
    legacySpotId: 60722,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/yucatan-area/isla-mujeres/',
  },
  '584204204e65fad6a7709253': {
    legacySpotId: 60724,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/punta-arenas/',
  },
  '584204204e65fad6a7709254': {
    legacySpotId: 60723,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/el-rincon/',
  },
  '584204204e65fad6a7709255': {
    legacySpotId: 60725,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/punta-colorados/',
  },
  '584204204e65fad6a7709256': {
    legacySpotId: 60726,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/yucatan-area/playa-chac-mool/',
  },
  '584204204e65fad6a7709257': {
    legacySpotId: 60730,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/yucatan-area/playa-delphine/',
  },
  '584204204e65fad6a7709258': {
    legacySpotId: 60731,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/yucatan-area/punta-moreno/',
  },
  '584204204e65fad6a7709a55': {
    legacySpotId: 111517,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/la-barrita/',
  },
  '584204204e65fad6a7709a56': {
    legacySpotId: 111520,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/tecojate/',
  },
  '584204204e65fad6a7709a57': {
    legacySpotId: 111519,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/sipacate/',
  },
  '584204204e65fad6a7709a58': {
    legacySpotId: 111518,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/el-paredon/',
  },
  '584204204e65fad6a770924a': {
    legacySpotId: 60715,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/bahia-chileno/',
  },
  '584204204e65fad6a770924b': {
    legacySpotId: 60714,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/backwash/',
  },
  '584204204e65fad6a770924c': {
    legacySpotId: 60716,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/punta-pamilla/',
  },
  '584204204e65fad6a770924d': {
    legacySpotId: 60717,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/costa-azul/',
  },
  '584204204e65fad6a770924e': {
    legacySpotId: 60719,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/frailes-south/',
  },
  '584204204e65fad6a770924f': {
    legacySpotId: 60718,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-baja/cabo/boca-del-solado/',
  },
  '584204204e65fad6a7709a50': {
    legacySpotId: 111512,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/hawaii/',
  },
  '584204204e65fad6a7709a53': {
    legacySpotId: 111515,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/puerto-san-jose/',
  },
  '584204204e65fad6a7709a54': {
    legacySpotId: 111516,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/chulamar/',
  },
  '584204204e65fad6a7709a51': {
    legacySpotId: 111513,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/monterrico/',
  },
  '584204204e65fad6a7709261': {
    legacySpotId: 60935,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/kangaroo-island/cape-kersaint/',
  },
  '584204204e65fad6a7709262': {
    legacySpotId: 60937,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/maslin-beach/',
  },
  '584204204e65fad6a7709263': {
    legacySpotId: 60938,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/the-trough/',
  },
  '584204204e65fad6a7709264': {
    legacySpotId: 60943,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/yorke-peninsula/ethel-wreck/',
  },
  '584204204e65fad6a7709265': {
    legacySpotId: 60939,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/moana-beach/',
  },
  '584204204e65fad6a7709266': {
    legacySpotId: 60944,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/yorke-peninsula/pondi/',
  },
  '584204204e65fad6a7709267': {
    legacySpotId: 60942,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/y-steps/',
  },
  '584204204e65fad6a7709268': {
    legacySpotId: 60940,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/trigs-beach/',
  },
  '584204204e65fad6a7709269': {
    legacySpotId: 60941,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/seaford/',
  },
  '584204214e65fad6a7709cfc': {
    legacySpotId: 135846,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/county-line-overview/',
  },
  '584204204e65fad6a7709546': {
    legacySpotId: 72912,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/wyldewood-beach/',
  },
  '584204204e65fad6a7709545': {
    legacySpotId: 72910,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/rock-point/',
  },
  '584204214e65fad6a7709cfe': {
    legacySpotId: 135850,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/baywood-beach/',
  },
  '584204204e65fad6a770925a': {
    legacySpotId: 60732,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/yucatan-area/playa-bonita/',
  },
  '584204204e65fad6a7709544': {
    legacySpotId: 72909,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/south-cayuga/',
  },
  '584204204e65fad6a770925e': {
    legacySpotId: 60936,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/adelaide/gulls/',
  },
  '584204214e65fad6a7709cfa': {
    legacySpotId: 135843,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/pismo-beach-south/',
  },
  '584204204e65fad6a7709270': {
    legacySpotId: 60950,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/midcoast/caves/',
  },
  '584204204e65fad6a7709274': {
    legacySpotId: 60949,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/midcoast/cunns/',
  },
  '584204204e65fad6a7709275': {
    legacySpotId: 61028,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/northern-queensland/hervey-bay/',
  },
  '584204204e65fad6a7709276': {
    legacySpotId: 61029,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/northern-queensland/mackay-harbour/',
  },
  '584204204e65fad6a7709277': {
    legacySpotId: 61033,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/glenelg-rivermouth/',
  },
  '584204204e65fad6a7709278': {
    legacySpotId: 61030,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/northern-queensland/bundaberg/',
  },
  '584204204e65fad6a7709279': {
    legacySpotId: 61022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/northern-queensland/agnes-water/',
  },
  '584204204e65fad6a770926a': {
    legacySpotId: 60945,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/yorke-peninsula/trespassers/',
  },
  '584204204e65fad6a770926d': {
    legacySpotId: 60951,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/midcoast/chrushers/',
  },
  '584204214e65fad6a7709b81': {
    legacySpotId: 119359,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/quicombo/',
  },
  '584204204e65fad6a770926f': {
    legacySpotId: 60948,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/midcoast/witzig-s/',
  },
  '5842041f4e65fad6a7708ca0': {
    legacySpotId: 6919,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/embleton-bay/',
  },
  '5842041f4e65fad6a7708ca1': {
    legacySpotId: 6922,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/putsborough/',
  },
  '5842041f4e65fad6a7708ca2': {
    legacySpotId: 6921,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/lynmouth/',
  },
  '5842041f4e65fad6a7708ca3': {
    legacySpotId: 6936,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/europie/',
  },
  '5842041f4e65fad6a7708ca4': {
    legacySpotId: 6923,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/croyde/',
  },
  '584204204e65fad6a7709280': {
    legacySpotId: 61042,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/warnambool-beach/',
  },
  '584204204e65fad6a7709281': {
    legacySpotId: 61041,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/the-cutting/',
  },
  '584204204e65fad6a7709282': {
    legacySpotId: 61039,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/the-passage/',
  },
  '5842041f4e65fad6a7708ca8': {
    legacySpotId: 6937,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/pease-bay/',
  },
  '5842041f4e65fad6a7708ca9': {
    legacySpotId: 6941,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/torrisdale/',
  },
  '584204204e65fad6a7709285': {
    legacySpotId: 61043,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/flume/',
  },
  '584204204e65fad6a7709288': {
    legacySpotId: 61085,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/old-woman-ledge/',
  },
  '584204204e65fad6a7709289': {
    legacySpotId: 61088,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/maroochydore/',
  },
  '584204214e65fad6a7709b9a': {
    legacySpotId: 119399,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/fort-sebastian/',
  },
  '584204214e65fad6a7709cf2': {
    legacySpotId: 135289,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/runton/',
  },
  '584204214e65fad6a7709ce9': {
    legacySpotId: 133158,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/croatan-jetty/',
  },
  '584204204e65fad6a770927b': {
    legacySpotId: 61034,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/discovery-bay/',
  },
  '584204204e65fad6a770927d': {
    legacySpotId: 61038,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/port-fairy/',
  },
  '584204204e65fad6a770927e': {
    legacySpotId: 61040,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/the-lighthouse/',
  },
  '584204214e65fad6a7709cf9': {
    legacySpotId: 135840,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/morro-bay-north/',
  },
  '584204214e65fad6a7709cf7': {
    legacySpotId: 135838,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/cayucos-overview/',
  },
  '584204204e65fad6a77097d0': {
    legacySpotId: 91762,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/ishikawa/oohama/',
  },
  '5842041f4e65fad6a7708cb1': {
    legacySpotId: 6944,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/brims-ness/',
  },
  '5842041f4e65fad6a7708cb2': {
    legacySpotId: 6948,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/freshwater-west/',
  },
  '5842041f4e65fad6a7708cb3': {
    legacySpotId: 6949,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/llangennith/',
  },
  '5842041f4e65fad6a7708cb4': {
    legacySpotId: 6950,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/langland-bay/',
  },
  '584204204e65fad6a7709290': {
    legacySpotId: 61090,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/alexandra-headland/',
  },
  '584204204e65fad6a7709291': {
    legacySpotId: 61093,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/childers-cove/',
  },
  '584204204e65fad6a7709292': {
    legacySpotId: 61098,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/moo-cows/',
  },
  '584204204e65fad6a7709293': {
    legacySpotId: 61100,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/the-corner/',
  },
  '584204204e65fad6a7709294': {
    legacySpotId: 61091,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/logans-beach-japs/',
  },
  '584204214e65fad6a7709cf6': {
    legacySpotId: 135293,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/southwold/',
  },
  '584204204e65fad6a7709297': {
    legacySpotId: 61102,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/smyths/',
  },
  '584204204e65fad6a7709298': {
    legacySpotId: 61094,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/the-well/',
  },
  '584204204e65fad6a7709299': {
    legacySpotId: 61103,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/sledgehammers/',
  },
  '5842041f4e65fad6a7708caa': {
    legacySpotId: 6938,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/skirza/',
  },
  '584204204e65fad6a77097cb': {
    legacySpotId: 91757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/toyama/matsudaehama/',
  },
  '5842041f4e65fad6a7708cac': {
    legacySpotId: 6940,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/sandwood-bay/',
  },
  '5842041f4e65fad6a7708cad': {
    legacySpotId: 6942,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/melvich/',
  },
  '5842041f4e65fad6a7708cae': {
    legacySpotId: 6943,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/sandside-bay/',
  },
  '584204204e65fad6a770928a': {
    legacySpotId: 61082,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/point-arkwright/',
  },
  '584204204e65fad6a770928b': {
    legacySpotId: 61084,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/marcoola-beach/',
  },
  '584204204e65fad6a770928c': {
    legacySpotId: 61089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/pincushion/',
  },
  '584204204e65fad6a770928d': {
    legacySpotId: 61080,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/coolum-beach/',
  },
  '584204204e65fad6a770928e': {
    legacySpotId: 61087,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/mudjimba/',
  },
  '584204204e65fad6a770928f': {
    legacySpotId: 61095,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/luna-park-milansea-beach/',
  },
  '5842041f4e65fad6a7708d55': {
    legacySpotId: 7228,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/lombok/desert-point/',
  },
  '5842041f4e65fad6a7708cc0': {
    legacySpotId: 6972,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/puertecillo/',
  },
  '5842041f4e65fad6a7708cc1': {
    legacySpotId: 6976,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/papagayo/',
  },
  '5842041f4e65fad6a7708cc2': {
    legacySpotId: 6974,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/renaca/',
  },
  '5842041f4e65fad6a7708cc3': {
    legacySpotId: 6973,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/matanzas/',
  },
  '584204204e65fad6a77097e4': {
    legacySpotId: 91781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/fukui/toriihama/',
  },
  '5842041f4e65fad6a7708cc5': {
    legacySpotId: 6979,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/southern-chile/el-puerto/',
  },
  '5842041f4e65fad6a7708cc6': {
    legacySpotId: 6977,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/el-claron/',
  },
  '584204204e65fad6a77097e7': {
    legacySpotId: 91786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/mie-ise/kounohamamein/',
  },
  '5842041f4e65fad6a7708cc8': {
    legacySpotId: 6986,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/las-machas/',
  },
  '584204214e65fad6a7709c1b': {
    legacySpotId: 126943,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/carnon/',
  },
  '594214c030e9860013bc472d': {
    legacySpotId: 147873,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/davis-park-beach/',
  },
  '5842041f4e65fad6a7708cba': {
    legacySpotId: 6969,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/punta-de-lobos/',
  },
  '5842041f4e65fad6a7708cbb': {
    legacySpotId: 6947,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/hells-mouth/',
  },
  '5842041f4e65fad6a7708cbc': {
    legacySpotId: 6956,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/la-santa-right--morro-negro-/',
  },
  '5842041f4e65fad6a7708cbd': {
    legacySpotId: 6975,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/ritoque/',
  },
  '5842041f4e65fad6a7708cbe': {
    legacySpotId: 6970,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/infiernillo/',
  },
  '584204204e65fad6a770929a': {
    legacySpotId: 61153,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/old-woman-ledge--south-side-/',
  },
  '584204204e65fad6a770929b': {
    legacySpotId: 61097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/elliott-river/',
  },
  '584204204e65fad6a770929c': {
    legacySpotId: 61155,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/the-bluff/',
  },
  '584204204e65fad6a770929d': {
    legacySpotId: 61158,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/point-cartwright/',
  },
  '584204204e65fad6a770929f': {
    legacySpotId: 61172,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/buddina/',
  },
  '5842041f4e65fad6a7708cd0': {
    legacySpotId: 6995,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/cerro-azul/',
  },
  '5842041f4e65fad6a7708cd1': {
    legacySpotId: 6984,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/el-buey/',
  },
  '5842041f4e65fad6a7708cd2': {
    legacySpotId: 6994,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/cabo-blanco/',
  },
  '584204204e65fad6a77097f3': {
    legacySpotId: 91796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/isonoura/isonoura-mein/',
  },
  '5842041f4e65fad6a7708cd4': {
    legacySpotId: 6982,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/la-bestia/',
  },
  '5842041f4e65fad6a7708cd5': {
    legacySpotId: 6983,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/el-colegio/',
  },
  '5842041f4e65fad6a7708cd6': {
    legacySpotId: 6996,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/penascal/',
  },
  '5842041f4e65fad6a7708cd7': {
    legacySpotId: 6998,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/la-isla/',
  },
  '5842041f4e65fad6a7708cd8': {
    legacySpotId: 6997,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/punta-rocas/',
  },
  '5842041f4e65fad6a7708cd9': {
    legacySpotId: 6999,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/herradurra/',
  },
  '5842041f4e65fad6a7708ccb': {
    legacySpotId: 6978,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/southern-chile/pellines/',
  },
  '5842041f4e65fad6a7708ccc': {
    legacySpotId: 6991,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/central-peru/punta-huanchaco/',
  },
  '5842041f4e65fad6a7708ccd': {
    legacySpotId: 6992,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/central-peru/chicama/',
  },
  '5842041f4e65fad6a7708cce': {
    legacySpotId: 6990,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/mancora/',
  },
  '5842041f4e65fad6a7708ccf': {
    legacySpotId: 6993,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/central-peru/pacasmayo/',
  },
  '5842041f4e65fad6a7708ca6': {
    legacySpotId: 6925,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/watergate-bay/',
  },
  '5842041f4e65fad6a7708ce0': {
    legacySpotId: 7006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/barra-da-tijuca/',
  },
  '5842041f4e65fad6a7708ce1': {
    legacySpotId: 7008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/prainha/',
  },
  '5842041f4e65fad6a7708ce2': {
    legacySpotId: 7007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-da-macumba/',
  },
  '5842041f4e65fad6a7708ce3': {
    legacySpotId: 7004,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/playa-grande/',
  },
  '5842041f4e65fad6a7708ce4': {
    legacySpotId: 7009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/pepino/',
  },
  '5842041f4e65fad6a7708ce5': {
    legacySpotId: 7011,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-de-ipanema/',
  },
  '5842041f4e65fad6a7708ce6': {
    legacySpotId: 7012,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/arpoador/',
  },
  '5842041f4e65fad6a7708ce7': {
    legacySpotId: 7010,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/pont-o-do-leblon/',
  },
  '5842041f4e65fad6a7708ce8': {
    legacySpotId: 7013,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/copacabana/',
  },
  '5842041f4e65fad6a7708ce9': {
    legacySpotId: 7014,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-de-ita-na/',
  },
  '584204214e65fad6a7709ba2': {
    legacySpotId: 121282,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/la-serena/',
  },
  '5842041f4e65fad6a7708d3a': {
    legacySpotId: 7206,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/balangan/',
  },
  '5842041f4e65fad6a7708cda': {
    legacySpotId: 7000,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/el-cabo-corrientes/',
  },
  '5842041f4e65fad6a7708cdc': {
    legacySpotId: 7002,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/la-paloma/',
  },
  '5842041f4e65fad6a7708cdd': {
    legacySpotId: 6980,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/portofino/',
  },
  '5842041f4e65fad6a7708cde': {
    legacySpotId: 7003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/puerto-cardiel/',
  },
  '5842041f4e65fad6a7708cdf': {
    legacySpotId: 7005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/quebra-mar/',
  },
  '5842041f4e65fad6a7708d6c': {
    legacySpotId: 7303,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/papua-new-guinea/kavieng/nusa-lefts/',
  },
  '5842041f4e65fad6a7708d6b': {
    legacySpotId: 7302,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/papua-new-guinea/kavieng/long-long/',
  },
  '5842041f4e65fad6a7708cf0': {
    legacySpotId: 7024,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/ferrugem/',
  },
  '5842041f4e65fad6a7708cf1': {
    legacySpotId: 7020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/praia-mole/',
  },
  '5842041f4e65fad6a7708cf2': {
    legacySpotId: 7025,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/silveira-garopaba/',
  },
  '5842041f4e65fad6a7708cf3': {
    legacySpotId: 7104,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/the-boiler/',
  },
  '5842041f4e65fad6a7708cf4': {
    legacySpotId: 7021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/galhetas/',
  },
  '5842041f4e65fad6a7708801': {
    legacySpotId: 4152,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/maverick-s/',
  },
  '5842041f4e65fad6a7708cf6': {
    legacySpotId: 7107,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/dar-bouazza/',
  },
  '5842041f4e65fad6a7708cf7': {
    legacySpotId: 7106,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/sidi-ghouzia/',
  },
  '5842041f4e65fad6a7708d17': {
    legacySpotId: 7167,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/kumari-point/',
  },
  '5842041f4e65fad6a7708805': {
    legacySpotId: 4188,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/steamer-lane/',
  },
  '5842041f4e65fad6a7708d19': {
    legacySpotId: 7168,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/muddy-s/',
  },
  '5842041f4e65fad6a7708807': {
    legacySpotId: 4190,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/pleasure-point/',
  },
  '584204214e65fad6a7709bb8': {
    legacySpotId: 122471,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/trinidad-and-tobago/north-trinidad-and-tobago/mount-irvine/',
  },
  '58dd6ba582d034001252e3d9': {
    legacySpotId: 115822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/ashdod/',
  },
  '5842041f4e65fad6a7708cea': {
    legacySpotId: 7015,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/praia-de-geriba/',
  },
  '5842041f4e65fad6a7708d16': {
    legacySpotId: 7163,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southeastern-sri-lanka/aragum-bay/',
  },
  '5842041f4e65fad6a7708cec': {
    legacySpotId: 7018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/morro-das-pedras/',
  },
  '5842041f4e65fad6a7708ced': {
    legacySpotId: 7017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/campeche/',
  },
  '5842041f4e65fad6a7708cee': {
    legacySpotId: 7022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/mocambique/',
  },
  '5842041f4e65fad6a7708cef': {
    legacySpotId: 7023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/praia-da-vila-imbituba/',
  },
  '5842041f4e65fad6a7708d0f': {
    legacySpotId: 7128,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/j-bay/jeffreys-bay/',
  },
  '5842041f4e65fad6a7708d09': {
    legacySpotId: 7123,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/dungeons/',
  },
  '5842041f4e65fad6a7708d11': {
    legacySpotId: 7164,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southeastern-sri-lanka/the-wedge/',
  },
  '5842041f4e65fad6a7708811': {
    legacySpotId: 4201,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/ventura-harbor/',
  },
  '5842041f4e65fad6a7708813': {
    legacySpotId: 4203,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/county-line/',
  },
  '5842041f4e65fad6a7708814': {
    legacySpotId: 4197,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/rincon/',
  },
  '5842041f4e65fad6a7708d06': {
    legacySpotId: 7122,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/the-hoek/',
  },
  '5842041f4e65fad6a7708d07': {
    legacySpotId: 7114,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/ansteys/',
  },
  '5842041f4e65fad6a7708d08': {
    legacySpotId: 7121,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/the-dunes/',
  },
  '5842041f4e65fad6a7708819': {
    legacySpotId: 4211,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/venice-beach/',
  },
  '5842041f4e65fad6a7708cfa': {
    legacySpotId: 7103,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/killers/',
  },
  '5842041f4e65fad6a7708cfb': {
    legacySpotId: 7108,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/bouznika-plage/',
  },
  '5842041f4e65fad6a7708d05': {
    legacySpotId: 7120,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/sunrise/',
  },
  '5842041f4e65fad6a7708cfd': {
    legacySpotId: 7111,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/anchor-point/',
  },
  '5842041f4e65fad6a770880a': {
    legacySpotId: 4193,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/morro-bay/',
  },
  '5842041f4e65fad6a770880b': {
    legacySpotId: 4191,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/38th-ave-/',
  },
  '5842041f4e65fad6a770880d': {
    legacySpotId: 4198,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/gold-coast-beachbreaks/',
  },
  '5842041f4e65fad6a7708820': {
    legacySpotId: 4217,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/seal-beach/',
  },
  '5842041f4e65fad6a7708822': {
    legacySpotId: 4219,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/anderson-street/',
  },
  '5842041f4e65fad6a7708827': {
    legacySpotId: 4223,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/hb-pier--northside/',
  },
  '5842041f4e65fad6a7708828': {
    legacySpotId: 4200,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/c-st-/',
  },
  '5842041f4e65fad6a7708829': {
    legacySpotId: 4225,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/40-st--newport/',
  },
  '584204204e65fad6a770954b': {
    legacySpotId: 72923,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-on--superior-/red-sucker-point/',
  },
  '5842041f4e65fad6a770881e': {
    legacySpotId: 4210,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/topanga-beach/',
  },
  '5842041f4e65fad6a7708830': {
    legacySpotId: 4235,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/t-street/',
  },
  '5842041f4e65fad6a7708831': {
    legacySpotId: 4237,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/the-point-at-san-onofre/',
  },
  '5842041f4e65fad6a7708832': {
    legacySpotId: 4238,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/oceanside-harbor/',
  },
  '5842041f4e65fad6a7708835': {
    legacySpotId: 4241,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/oceanside-pier--northside/',
  },
  '584204204e65fad6a770954a': {
    legacySpotId: 72915,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/palmwood-point/',
  },
  '5842041f4e65fad6a7708837': {
    legacySpotId: 4242,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/tamarack/',
  },
  '5842041f4e65fad6a7708839': {
    legacySpotId: 4246,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/scripps/',
  },
  '5842041f4e65fad6a770882a': {
    legacySpotId: 4226,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/36th-st-/',
  },
  '5842041f4e65fad6a770882b': {
    legacySpotId: 4232,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/the-wedge/',
  },
  '5842041f4e65fad6a770882c': {
    legacySpotId: 4227,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/newport-pier/',
  },
  '5842041f4e65fad6a770882e': {
    legacySpotId: 4233,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/salt-creek/',
  },
  '5842041f4e65fad6a7708840': {
    legacySpotId: 4254,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/sunset-cliffs/',
  },
  '5842041f4e65fad6a7708841': {
    legacySpotId: 4250,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/pacific-beach/',
  },
  '5842041f4e65fad6a7708842': {
    legacySpotId: 4252,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/mission-beach/',
  },
  '5842041f4e65fad6a7708844': {
    legacySpotId: 4256,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/imperial-pier-ss/',
  },
  '5842041f4e65fad6a7708f13': {
    legacySpotId: 46191,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-grande/',
  },
  '5842041f4e65fad6a7708f15': {
    legacySpotId: 46194,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/ilha/',
  },
  '5842041f4e65fad6a7708f14': {
    legacySpotId: 46193,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-brava/',
  },
  '5842041f4e65fad6a770883b': {
    legacySpotId: 4245,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/blacks/',
  },
  '5842041f4e65fad6a770883c': {
    legacySpotId: 4248,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/windansea/',
  },
  '5842041f4e65fad6a770883d': {
    legacySpotId: 4247,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/horseshoe/',
  },
  '5842041f4e65fad6a770883f': {
    legacySpotId: 4253,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/ocean-beach--sd/',
  },
  '5842041f4e65fad6a7708f0e': {
    legacySpotId: 46119,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/itauna/',
  },
  '584204204e65fad6a7709557': {
    legacySpotId: 72943,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/hot-pond-beach/',
  },
  '5842041f4e65fad6a7708f09': {
    legacySpotId: 46109,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-da-vila/',
  },
  '5842041f4e65fad6a7708850': {
    legacySpotId: 4269,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/lincoln-blvd--long-beach/',
  },
  '5842041f4e65fad6a7708852': {
    legacySpotId: 4270,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/rockaway-beach-90th/',
  },
  '5842041f4e65fad6a7708853': {
    legacySpotId: 4276,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/monmouth-beach/',
  },
  '5842041f4e65fad6a7708856': {
    legacySpotId: 4278,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/manasquan-inlet/',
  },
  '5842041f4e65fad6a7708857': {
    legacySpotId: 4279,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/casino-pier/',
  },
  '5842041f4e65fad6a7708858': {
    legacySpotId: 4281,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/1st-street--ocean-city--nj/',
  },
  '5842041f4e65fad6a7708f06': {
    legacySpotId: 46107,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-das-asturias/',
  },
  '5842041f4e65fad6a770884a': {
    legacySpotId: 4263,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/seabrook/',
  },
  '584204204e65fad6a7709581': {
    legacySpotId: 72994,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/forester/',
  },
  '5842041f4e65fad6a770884e': {
    legacySpotId: 4266,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/narragansett/',
  },
  '5842041f4e65fad6a770884f': {
    legacySpotId: 4268,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/montauk/',
  },
  '584204204e65fad6a7709901': {
    legacySpotId: 94482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/ishikawa/taki-beach/',
  },
  '584204204e65fad6a77091b0': {
    legacySpotId: 58746,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/patoles/',
  },
  '584204204e65fad6a77091b1': {
    legacySpotId: 58747,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/dimas-rivermouth/',
  },
  '584204204e65fad6a77091b2': {
    legacySpotId: 58778,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/popotla/',
  },
  '584204204e65fad6a77091b3': {
    legacySpotId: 58748,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/las-labradas/',
  },
  '584204204e65fad6a77091b4': {
    legacySpotId: 58749,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/marmols/',
  },
  '584204204e65fad6a77091b5': {
    legacySpotId: 58777,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/alfonsos/',
  },
  '584204204e65fad6a77091b6': {
    legacySpotId: 58744,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/tiburon/',
  },
  '584204204e65fad6a77091b7': {
    legacySpotId: 58779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/calafia/',
  },
  '584204204e65fad6a77091b8': {
    legacySpotId: 58784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/rancho-santini/',
  },
  '584204204e65fad6a77091b9': {
    legacySpotId: 58785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/las-gaviotas/',
  },
  '584204204e65fad6a77091aa': {
    legacySpotId: 58218,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/hb-pier-northside-overview/',
  },
  '584204204e65fad6a77091ac': {
    legacySpotId: 58433,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/sunglow-pier-north/',
  },
  '584204204e65fad6a77091ad': {
    legacySpotId: 58745,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/milagro/',
  },
  '584204204e65fad6a77091ae': {
    legacySpotId: 58604,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/san-antonio-del-mar---1/',
  },
  '584204204e65fad6a77091af': {
    legacySpotId: 58605,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/marisol-north-point/',
  },
  '584204204e65fad6a77091c0': {
    legacySpotId: 58789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/cantamar-settlement/',
  },
  '584204204e65fad6a77091c1': {
    legacySpotId: 58606,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/marisol/',
  },
  '584204204e65fad6a77091c2': {
    legacySpotId: 58792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/campo-lopez--k-55-/',
  },
  '584204204e65fad6a77091c3': {
    legacySpotId: 58790,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/dunes/',
  },
  '584204204e65fad6a77091c4': {
    legacySpotId: 58794,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/k-55-1-2/',
  },
  '584204204e65fad6a77091c5': {
    legacySpotId: 58797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/la-mision/',
  },
  '584204204e65fad6a77091c6': {
    legacySpotId: 58795,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/alisitos/',
  },
  '584204204e65fad6a77091c7': {
    legacySpotId: 58796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/la-fonda/',
  },
  '584204204e65fad6a77091c8': {
    legacySpotId: 58801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/mirador-viewpoint/',
  },
  '584204204e65fad6a77091c9': {
    legacySpotId: 58800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/el-paso/',
  },
  '584204204e65fad6a77091ba': {
    legacySpotId: 58782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/k-38/',
  },
  '584204204e65fad6a77091bb': {
    legacySpotId: 58781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/el-morro-point--k37-5-/',
  },
  '584204204e65fad6a77091bc': {
    legacySpotId: 58783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/k-40/',
  },
  '584204204e65fad6a77091bd': {
    legacySpotId: 58787,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/primo-tapia/',
  },
  '584204204e65fad6a77091be': {
    legacySpotId: 58788,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/rauls/',
  },
  '584204204e65fad6a77091bf': {
    legacySpotId: 58607,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/renes/',
  },
  '584204204e65fad6a7709552': {
    legacySpotId: 72935,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/grand-sable-dunes/',
  },
  '5842041f4e65fad6a7708e00': {
    legacySpotId: 10848,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/ewa-beach/',
  },
  '5842041f4e65fad6a7708e01': {
    legacySpotId: 10852,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/molokai/molokai/kaunakakai--the-wharf-/',
  },
  '5842041f4e65fad6a7708e02': {
    legacySpotId: 10851,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/molokai/molokai/halawa-bay/',
  },
  '5842041f4e65fad6a7708e03': {
    legacySpotId: 10853,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/molokai/molokai/kawa/',
  },
  '5842041f4e65fad6a7708e04': {
    legacySpotId: 10854,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/molokai/molokai/kalaupapa/',
  },
  '584204204e65fad6a77091d0': {
    legacySpotId: 58840,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/ensenada-beaches/',
  },
  '5842041f4e65fad6a7708e06': {
    legacySpotId: 10859,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/lanai/lanai/kaiolohia-bay-shipwreck-beach/',
  },
  '5842041f4e65fad6a7708e07': {
    legacySpotId: 10860,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/lanai/lanai/pohakuloa/',
  },
  '584204204e65fad6a77091d3': {
    legacySpotId: 58844,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/lighthouse--punta-san-jose-/',
  },
  '584204204e65fad6a77091d4': {
    legacySpotId: 58843,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/playa-bruja/',
  },
  '584204204e65fad6a77091d5': {
    legacySpotId: 58845,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/punta-cabras/',
  },
  '584204204e65fad6a77091d6': {
    legacySpotId: 58847,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/k-181/',
  },
  '584204204e65fad6a77091d7': {
    legacySpotId: 58846,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/las-flores/',
  },
  '584204204e65fad6a77091d8': {
    legacySpotId: 58848,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/el-camaron/',
  },
  '584204204e65fad6a77091d9': {
    legacySpotId: 58936,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/quatro-casas/',
  },
  '584204204e65fad6a77091ca': {
    legacySpotId: 58798,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/la-salina/',
  },
  '584204204e65fad6a77091cb': {
    legacySpotId: 58791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/halfway-house/',
  },
  '5842041f4e65fad6a770887d': {
    legacySpotId: 4432,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/deerfield-beach-pier/',
  },
  '584204204e65fad6a77091cd': {
    legacySpotId: 58836,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/3ms/',
  },
  '584204204e65fad6a77091ce': {
    legacySpotId: 58838,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/cannery-stacks/',
  },
  '584204204e65fad6a77091cf': {
    legacySpotId: 58839,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/california-trailer-park/',
  },
  '5842041f4e65fad6a7708e10': {
    legacySpotId: 13429,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/saunton/',
  },
  '5842041f4e65fad6a7708e12': {
    legacySpotId: 13826,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-south/kamalino--niihau/',
  },
  '5842041f4e65fad6a7708e13': {
    legacySpotId: 10856,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/molokai/molokai/halena-beach/',
  },
  '5842041f4e65fad6a7708d7d': {
    legacySpotId: 7320,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/southern-philippines/tuesday-rock/',
  },
  '584204204e65fad6a77091e0': {
    legacySpotId: 58946,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/camalu/',
  },
  '5842041f4e65fad6a7708e16': {
    legacySpotId: 13828,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-hilo/pohoiki/',
  },
  '5842041f4e65fad6a7708d7c': {
    legacySpotId: 7359,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/piha/',
  },
  '584204204e65fad6a77091e3': {
    legacySpotId: 58952,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/el-socorro/',
  },
  '5842041f4e65fad6a7708895': {
    legacySpotId: 4755,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/waimea-bay/',
  },
  '584204204e65fad6a77091e5': {
    legacySpotId: 58953,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/casas/',
  },
  '584204204e65fad6a77091e6': {
    legacySpotId: 59146,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/valentinos/',
  },
  '584204204e65fad6a77091e7': {
    legacySpotId: 59147,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/los-pinos/',
  },
  '584204204e65fad6a77091e8': {
    legacySpotId: 59156,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/san-blas/',
  },
  '584204204e65fad6a77091e9': {
    legacySpotId: 59160,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/las-islitas/',
  },
  '584204204e65fad6a77098c4': {
    legacySpotId: 92526,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/izu/shirahama/',
  },
  '584204204e65fad6a770952a': {
    legacySpotId: 72883,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/geneva-chestnut-beach/',
  },
  '5842041f4e65fad6a770888a': {
    legacySpotId: 4740,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/lower-trestles/',
  },
  '584204204e65fad6a77091da': {
    legacySpotId: 58849,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/san-antonio-del-mar---2/',
  },
  '584204204e65fad6a77091db': {
    legacySpotId: 58850,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/cabo-colonet/',
  },
  '584204204e65fad6a77091dc': {
    legacySpotId: 58938,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/roberts-left/',
  },
  '584204204e65fad6a77091dd': {
    legacySpotId: 58944,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/rincon-de-baja/',
  },
  '584204204e65fad6a77091de': {
    legacySpotId: 58941,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/punta-san-jacinto--shipwrecks-/',
  },
  '584204204e65fad6a77091df': {
    legacySpotId: 58948,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/playa-san-ramon/',
  },
  '584204204e65fad6a7709533': {
    legacySpotId: 72892,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/nickel-plate-beach/',
  },
  '5842041f4e65fad6a7708e22': {
    legacySpotId: 25050,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/alaska/fossil-beach/',
  },
  '584204204e65fad6a7709943': {
    legacySpotId: 94548,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/saga/ouga/',
  },
  '5842041f4e65fad6a7708e24': {
    legacySpotId: 25053,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/alaska/the-wall/',
  },
  '584204204e65fad6a77091f0': {
    legacySpotId: 59260,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/chacala/',
  },
  '584204204e65fad6a77091f1': {
    legacySpotId: 59273,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/banderas/',
  },
  '584204204e65fad6a77091f2': {
    legacySpotId: 59277,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/la-lancha/',
  },
  '584204204e65fad6a77091f3': {
    legacySpotId: 59275,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/punta-burros/',
  },
  '584204204e65fad6a77091f4': {
    legacySpotId: 59279,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/boca-de-iguanas/',
  },
  '584204204e65fad6a77091f5': {
    legacySpotId: 59284,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/manzanillo/',
  },
  '584204204e65fad6a77091f6': {
    legacySpotId: 59278,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/playa-tenacatita/',
  },
  '584204204e65fad6a77091f7': {
    legacySpotId: 59361,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-baja/',
  },
  '584204204e65fad6a77091f8': {
    legacySpotId: 59280,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/manzanillo-area/barra-de-navidad/',
  },
  '584204204e65fad6a77091f9': {
    legacySpotId: 59363,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-san-antonio/',
  },
  '5842041f4e65fad6a7708e1a': {
    legacySpotId: 17513,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/ocean-avenue--melbourne-beach/',
  },
  '5842041f4e65fad6a7708e1b': {
    legacySpotId: 14160,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/baja-malibu/',
  },
  '5842041f4e65fad6a7708e1c': {
    legacySpotId: 17425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/west-scotland/machrihanish/',
  },
  '5842041f4e65fad6a7708e1d': {
    legacySpotId: 19018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/tasmania/tasmania/shipstern-bluff/',
  },
  '5842041f4e65fad6a7708e1e': {
    legacySpotId: 21037,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/colorados/',
  },
  '584204204e65fad6a77091ea': {
    legacySpotId: 59151,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/barron/',
  },
  '584204204e65fad6a77091eb': {
    legacySpotId: 59161,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/aticama/',
  },
  '584204204e65fad6a77091ec': {
    legacySpotId: 59158,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/stoners-point/',
  },
  '584204204e65fad6a77091ed': {
    legacySpotId: 59164,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/santa-cruz/',
  },
  '584204204e65fad6a77091ee': {
    legacySpotId: 59262,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/san-pancho/',
  },
  '584204204e65fad6a77091ef': {
    legacySpotId: 59167,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/punta-custodio/',
  },
  '584204204e65fad6a7709a8b': {
    legacySpotId: 111824,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/boar-rice/',
  },
  '5842041f4e65fad6a7708e30': {
    legacySpotId: 28417,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/sand-island/',
  },
  '5842041f4e65fad6a7708e31': {
    legacySpotId: 29038,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/santa-teresa/',
  },
  '5842041f4e65fad6a7708e32': {
    legacySpotId: 29346,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tonga/tonga/motels/',
  },
  '5842041f4e65fad6a7708e33': {
    legacySpotId: 30239,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/martha-s-vineyard/',
  },
  '5842041f4e65fad6a7708e34': {
    legacySpotId: 30174,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/nantucket/',
  },
  '5842041f4e65fad6a7708e36': {
    legacySpotId: 31148,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/33rd-st/',
  },
  '5842041f4e65fad6a7708e37': {
    legacySpotId: 38944,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/ravena/',
  },
  '584204204e65fad6a7709535': {
    legacySpotId: 72894,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/marblehead-light-house/',
  },
  '584204204e65fad6a7709534': {
    legacySpotId: 72893,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/cedar-point/',
  },
  '584204204e65fad6a7709a8e': {
    legacySpotId: 111829,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/cheray-beach/',
  },
  '584204204e65fad6a770977e': {
    legacySpotId: 91679,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/chisan/',
  },
  '584204204e65fad6a7709b3f': {
    legacySpotId: 117873,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/backdoor/',
  },
  '584204204e65fad6a770994b': {
    legacySpotId: 94556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/nagasaki/kazusa/',
  },
  '5842041f4e65fad6a7708e2c': {
    legacySpotId: 26882,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/marianas/guam/right-side-and-left-side/',
  },
  '5842041f4e65fad6a7708e2d': {
    legacySpotId: 26883,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/marianas/guam/point-perfection/',
  },
  '5842041f4e65fad6a7708e2e': {
    legacySpotId: 26884,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/marianas/guam/threes-and-red-buoy/',
  },
  '584204204e65fad6a77091fa': {
    legacySpotId: 59264,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/sayulita/',
  },
  '584204204e65fad6a77091fb': {
    legacySpotId: 59364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-san-carlos/',
  },
  '584204204e65fad6a77091fc': {
    legacySpotId: 59365,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-canoas/',
  },
  '584204204e65fad6a77091fd': {
    legacySpotId: 59366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-baja/punta-blanca/',
  },
  '584204204e65fad6a77091fe': {
    legacySpotId: 59459,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/punta-rosarito/punta-rosarito/',
  },
  '5842041f4e65fad6a7708e40': {
    legacySpotId: 41373,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/matunuck/',
  },
  '584204214e65fad6a7709bdf': {
    legacySpotId: 125742,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/la-playita/',
  },
  '584204214e65fad6a7709c4c': {
    legacySpotId: 127709,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/ammoudia-left/',
  },
  '584204214e65fad6a7709bbe': {
    legacySpotId: 123031,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/new-brunswick/banc-de-pabos/',
  },
  '584204214e65fad6a7709c86': {
    legacySpotId: 128408,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/west-finland/storsand/',
  },
  '584204204e65fad6a770954d': {
    legacySpotId: 72930,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/whitefish-bay/',
  },
  '5842041f4e65fad6a7708e49': {
    legacySpotId: 43009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/grumari/',
  },
  '584204204e65fad6a770954f': {
    legacySpotId: 72931,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/muskallonge-lake-state-park/',
  },
  '584204204e65fad6a770959a': {
    legacySpotId: 73025,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/niagra-shores-park/',
  },
  '584204214e65fad6a7709c88': {
    legacySpotId: 128410,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/west-finland/yyteri/',
  },
  '584204204e65fad6a7709591': {
    legacySpotId: 73013,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-on--ontario-/wolfe-island/',
  },
  '584204204e65fad6a77095c6': {
    legacySpotId: 73999,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/germany-north-sea/western-germany/westerland-sylt/',
  },
  '5842041f4e65fad6a7708e3a': {
    legacySpotId: 38945,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/cardoso/',
  },
  '584204214e65fad6a7709c3e': {
    legacySpotId: 127241,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/baie-des-singes/',
  },
  '5842041f4e65fad6a7708e3c': {
    legacySpotId: 40222,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/43rd-st/',
  },
  '5842041f4e65fad6a7708e3d': {
    legacySpotId: 40569,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/pet-den--satellite-beach/',
  },
  '5842041f4e65fad6a7708e3e': {
    legacySpotId: 39047,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/se-papa/',
  },
  '5842041f4e65fad6a7708e3f': {
    legacySpotId: 40029,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/caballeros/',
  },
  '584204204e65fad6a7709555': {
    legacySpotId: 72940,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/shelter-bay/',
  },
  '584204204e65fad6a7709556': {
    legacySpotId: 72942,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/shiras-park/',
  },
  '584204204e65fad6a7709798': {
    legacySpotId: 91705,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shizunami/susuki/',
  },
  '584204204e65fad6a7709554': {
    legacySpotId: 72937,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/au-train-bay/',
  },
  '584204214e65fad6a7709c83': {
    legacySpotId: 128395,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--baltic-sea-/nexo-harbor/',
  },
  '584204214e65fad6a7709c3c': {
    legacySpotId: 127239,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/chefs/',
  },
  '5842041f4e65fad6a7708e50': {
    legacySpotId: 43050,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/guarapari---ule/',
  },
  '5842041f4e65fad6a7708e51': {
    legacySpotId: 43052,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/rio-grande-do-sul/malvina/',
  },
  '584204214e65fad6a7709c59': {
    legacySpotId: 127792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--aegean-sea-/aegios-loannis/',
  },
  '5842041f4e65fad6a7708e53': {
    legacySpotId: 43055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/asr-india/',
  },
  '5842041f4e65fad6a7708e54': {
    legacySpotId: 43103,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/56th-st-/',
  },
  '5842041f4e65fad6a7708e55': {
    legacySpotId: 43842,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/philippines/northern-philippines/real-beach/',
  },
  '5842041f4e65fad6a7708e56': {
    legacySpotId: 44482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/buarcos/',
  },
  '5842041f4e65fad6a7708e57': {
    legacySpotId: 44485,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/viana-do-castelo/',
  },
  '5842041f4e65fad6a7708e58': {
    legacySpotId: 44484,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/afife/',
  },
  '5842041f4e65fad6a7708e59': {
    legacySpotId: 44488,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/matosinhos/',
  },
  '584204214e65fad6a7709c6b': {
    legacySpotId: 127812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/preveli--crete-/',
  },
  '584204214e65fad6a7709c56': {
    legacySpotId: 127719,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/agrili--peloponnisos-/',
  },
  '584204204e65fad6a7709b16': {
    legacySpotId: 117796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/bocchetta/',
  },
  '584204204e65fad6a7709b17': {
    legacySpotId: 117805,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/bagni-fiume/',
  },
  '584204214e65fad6a7709c55': {
    legacySpotId: 127720,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/lagkouvardos--peloponnisos-/',
  },
  '584204204e65fad6a7709580': {
    legacySpotId: 72997,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/albert-sleeper-state-park/',
  },
  '5842041f4e65fad6a7708e4a': {
    legacySpotId: 43010,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/cabo-frio-praia-do-forte/',
  },
  '584204204e65fad6a7709582': {
    legacySpotId: 72996,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/oak-beach/',
  },
  '5842041f4e65fad6a7708e4c': {
    legacySpotId: 43022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-felix/',
  },
  '5842041f4e65fad6a7708e4d': {
    legacySpotId: 43023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/guaruja---praia-do-tombo/',
  },
  '5842041f4e65fad6a7708e4e': {
    legacySpotId: 43026,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/jacaraipe---solemar/',
  },
  '584204214e65fad6a7709c52': {
    legacySpotId: 127716,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/ammes-beach--kefallonia-/',
  },
  '584204204e65fad6a7709558': {
    legacySpotId: 72948,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/big-bay/',
  },
  '584204204e65fad6a77091a9': {
    legacySpotId: 57749,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/southern-chile/piedra-de-la-iglesia/',
  },
  '584204214e65fad6a7709c51': {
    legacySpotId: 127714,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/kastrosikia/',
  },
  '584204214e65fad6a7709c50': {
    legacySpotId: 127715,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/shark-reef/',
  },
  '584204214e65fad6a7709c6f': {
    legacySpotId: 127818,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/fortunes-rock/',
  },
  '584204204e65fad6a7709980': {
    legacySpotId: 96373,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/mackenzie-beach/',
  },
  '5842041f4e65fad6a7708e61': {
    legacySpotId: 44501,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/praia-da-nazare/',
  },
  '584204204e65fad6a7709982': {
    legacySpotId: 96801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-north/orofara/',
  },
  '584204204e65fad6a7709983': {
    legacySpotId: 96904,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/shack-s-shifty-s/',
  },
  '5842041f4e65fad6a7708e64': {
    legacySpotId: 44506,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/praia-do-guincho-overview/',
  },
  '5842041f4e65fad6a7708e65': {
    legacySpotId: 44509,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/costa-da-caparica/',
  },
  '584204204e65fad6a7709986': {
    legacySpotId: 99404,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/populo/',
  },
  '5842041f4e65fad6a7708e67': {
    legacySpotId: 44528,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/playa-de-traba/',
  },
  '5842041f4e65fad6a7708e68': {
    legacySpotId: 44533,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/campelo/',
  },
  '584204204e65fad6a770956f': {
    legacySpotId: 72978,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/south-shore--manitoulin-island/',
  },
  '584204204e65fad6a7709a5b': {
    legacySpotId: 111523,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/tilapa/',
  },
  '584204204e65fad6a770956b': {
    legacySpotId: 72969,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/beaver-bay/',
  },
  '584204204e65fad6a7709598': {
    legacySpotId: 73021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/durand-eastman-beach/',
  },
  '584204204e65fad6a770956a': {
    legacySpotId: 72967,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/knife-river/',
  },
  '5842041f4e65fad6a7708e5a': {
    legacySpotId: 44486,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/agucadoura/',
  },
  '5842041f4e65fad6a7708e5b': {
    legacySpotId: 44487,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/leca/',
  },
  '5842041f4e65fad6a7708e5c': {
    legacySpotId: 44490,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/espinho/',
  },
  '5842041f4e65fad6a7708e5d': {
    legacySpotId: 44491,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/esmoriz/',
  },
  '5842041f4e65fad6a7708e5e': {
    legacySpotId: 44496,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-norte-portugal/cortegaca/',
  },
  '5842041f4e65fad6a7708e5f': {
    legacySpotId: 44499,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/praia-da-barra/',
  },
  '584204204e65fad6a770956d': {
    legacySpotId: 72972,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/cascade-river-state-park/',
  },
  '584204204e65fad6a770955e': {
    legacySpotId: 72951,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/bete-grise-bay/',
  },
  '584204204e65fad6a7709578': {
    legacySpotId: 72990,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/canatara-beach/',
  },
  '584204204e65fad6a7709579': {
    legacySpotId: 72986,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/grand-bend/',
  },
  '584204204e65fad6a7709576': {
    legacySpotId: 72983,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/kincardine/',
  },
  '584204204e65fad6a7709573': {
    legacySpotId: 72979,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/wasaga-beach/',
  },
  '584204204e65fad6a7709990': {
    legacySpotId: 106914,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/eckner-street--kitty-hawk/',
  },
  '5842041f4e65fad6a7708e71': {
    legacySpotId: 44539,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/los-locos/',
  },
  '584204204e65fad6a7709992': {
    legacySpotId: 106923,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/crystal-coast/oceanana-pier/',
  },
  '5842041f4e65fad6a7708e73': {
    legacySpotId: 44541,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/playa-de-somo/',
  },
  '5842041f4e65fad6a7708e74': {
    legacySpotId: 44542,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/santa-marina/',
  },
  '5842041f4e65fad6a7708e75': {
    legacySpotId: 44544,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/sopelana/',
  },
  '5842041f4e65fad6a7708e76': {
    legacySpotId: 44543,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/el-brusco/',
  },
  '5842041f4e65fad6a7708e77': {
    legacySpotId: 44547,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/sines/',
  },
  '5842041f4e65fad6a7708e78': {
    legacySpotId: 44545,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/orrua/',
  },
  '5842041f4e65fad6a7708e79': {
    legacySpotId: 44548,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/alentejo-portugal/sao-torpes/',
  },
  '584204214e65fad6a7709c31': {
    legacySpotId: 127091,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/pantai-kemasik/',
  },
  '584204214e65fad6a7709c32': {
    legacySpotId: 127096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/chendor-beach/',
  },
  '584204204e65fad6a7709b37': {
    legacySpotId: 117861,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/saraceno/',
  },
  '584204214e65fad6a7709bb1': {
    legacySpotId: 122410,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/stinkies-jersey/',
  },
  '584204214e65fad6a7709c8b': {
    legacySpotId: 128412,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/southern-finland/padva-beach/',
  },
  '584204214e65fad6a7709c8c': {
    legacySpotId: 128962,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/tam-tam-beach/',
  },
  '584204214e65fad6a7709c33': {
    legacySpotId: 127097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/cherating/',
  },
  '5842041f4e65fad6a7708e6a': {
    legacySpotId: 44530,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/playa-de-sabon/',
  },
  '5842041f4e65fad6a7708e6b': {
    legacySpotId: 44532,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/foz/',
  },
  '5842041f4e65fad6a7708e6c': {
    legacySpotId: 44534,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/tapia-de-casariego/',
  },
  '5842041f4e65fad6a7708e6d': {
    legacySpotId: 44535,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/salinas-y-espartal/',
  },
  '5842041f4e65fad6a7708e6e': {
    legacySpotId: 44536,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/xivares/',
  },
  '5842041f4e65fad6a7708e6f': {
    legacySpotId: 44538,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/playa-de-meron/',
  },
  '584204204e65fad6a770958f': {
    legacySpotId: 73012,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-on--ontario-/macdonald-park/',
  },
  '584204204e65fad6a770958e': {
    legacySpotId: 73009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/de-tour/',
  },
  '584204214e65fad6a7709c8f': {
    legacySpotId: 128964,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/yours/',
  },
  '584204204e65fad6a7709566': {
    legacySpotId: 72968,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/gooseberry-falls-state-park/',
  },
  '584204204e65fad6a7709567': {
    legacySpotId: 72965,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/french-river/',
  },
  '584204204e65fad6a7709565': {
    legacySpotId: 72960,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/lester-river/',
  },
  '5842041f4e65fad6a7708e80': {
    legacySpotId: 44555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/tonel/',
  },
  '5842041f4e65fad6a7708e81': {
    legacySpotId: 44557,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/praia-da-luz/',
  },
  '5842041f4e65fad6a7708e82': {
    legacySpotId: 44561,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/ilha-do-farol/',
  },
  '5842041f4e65fad6a7708e83': {
    legacySpotId: 44559,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/praia-da-rocha/',
  },
  '5842041f4e65fad6a7708e84': {
    legacySpotId: 44558,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/meia-praia/',
  },
  '5842041f4e65fad6a7708e85': {
    legacySpotId: 44560,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/falesia/',
  },
  '584204214e65fad6a7709c46': {
    legacySpotId: 127571,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-korea/northern-south-korea/gyongpo-beach/',
  },
  '5842041f4e65fad6a7708e88': {
    legacySpotId: 45163,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/ponta-das-galhetas/',
  },
  '5842041f4e65fad6a7708e89': {
    legacySpotId: 45162,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/guaiuba/',
  },
  '584204204e65fad6a7709564': {
    legacySpotId: 72955,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/misery-bay/',
  },
  '584204204e65fad6a770958d': {
    legacySpotId: 73008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/horseshoe-bay/',
  },
  '584204214e65fad6a7709c43': {
    legacySpotId: 127088,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/tanjong-jara/',
  },
  '584204214e65fad6a7709c7c': {
    legacySpotId: 128385,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--gulf-of-bothnia-/salusand/',
  },
  '584204214e65fad6a7709c45': {
    legacySpotId: 127570,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-korea/northern-south-korea/jukdo-beach/',
  },
  '584204214e65fad6a7709c7a': {
    legacySpotId: 128386,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--gulf-of-bothnia-/smitingen/',
  },
  '584204214e65fad6a7709c7b': {
    legacySpotId: 128304,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/southern-italy/capolinea/',
  },
  '584204214e65fad6a7709c44': {
    legacySpotId: 127569,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-korea/northern-south-korea/38th-parallel/',
  },
  '5842041f4e65fad6a7708e7a': {
    legacySpotId: 44550,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/alentejo-portugal/odeceixe/',
  },
  '5842041f4e65fad6a7708e7b': {
    legacySpotId: 44549,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/alentejo-portugal/malhao/',
  },
  '5842041f4e65fad6a7708e7c': {
    legacySpotId: 44554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/carrapateira/',
  },
  '5842041f4e65fad6a7708e7d': {
    legacySpotId: 44553,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/vale-figueiras/',
  },
  '5842041f4e65fad6a7708e7e': {
    legacySpotId: 44552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/arrifana/',
  },
  '5842041f4e65fad6a7708e7f': {
    legacySpotId: 44556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/algarve-portugal/mareta/',
  },
  '584204214e65fad6a7709c41': {
    legacySpotId: 127243,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/monseigneur/',
  },
  '584204204e65fad6a7709569': {
    legacySpotId: 72961,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/brighton-beach/',
  },
  '584204214e65fad6a7709c40': {
    legacySpotId: 127244,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/baie-de-galion/',
  },
  '5842041f4e65fad6a7708e90': {
    legacySpotId: 45232,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/sao-pedro/',
  },
  '5842041f4e65fad6a7708e91': {
    legacySpotId: 45233,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-iporanga/',
  },
  '5842041f4e65fad6a7708e92': {
    legacySpotId: 45234,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-preta/',
  },
  '5842041f4e65fad6a7708e93': {
    legacySpotId: 45228,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-mar-casado/',
  },
  '5842041f4e65fad6a7708e94': {
    legacySpotId: 45235,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-branca/',
  },
  '5842041f4e65fad6a7708e95': {
    legacySpotId: 45236,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-da-bertioga/',
  },
  '5842041f4e65fad6a7708e96': {
    legacySpotId: 45239,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/itaguere/',
  },
  '5842041f4e65fad6a7708e97': {
    legacySpotId: 45237,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/sao-lourenco/',
  },
  '5842041f4e65fad6a7708e98': {
    legacySpotId: 45245,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/barra-do-una/',
  },
  '5842041f4e65fad6a7708e99': {
    legacySpotId: 45240,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-da-borac-ia/',
  },
  '584204204e65fad6a7709359': {
    legacySpotId: 62143,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/spooneys/',
  },
  '584204204e65fad6a770955f': {
    legacySpotId: 72957,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/porcupine-mountains-state-park/',
  },
  '584204204e65fad6a770955d': {
    legacySpotId: 72952,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/great-sand-bay/',
  },
  '584204204e65fad6a770955a': {
    legacySpotId: 72941,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/laughing-fish-point-beach/',
  },
  '584204204e65fad6a770951b': {
    legacySpotId: 72827,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/rock-island/',
  },
  '584204204e65fad6a770951c': {
    legacySpotId: 72870,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/woodlawn/',
  },
  '5842041f4e65fad6a7708e8c': {
    legacySpotId: 45168,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/morro-do-maluf/',
  },
  '5842041f4e65fad6a7708e8d': {
    legacySpotId: 45170,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-da-enseada/',
  },
  '5842041f4e65fad6a7708e8e': {
    legacySpotId: 45229,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/pernambuco/',
  },
  '5842041f4e65fad6a7708e8f': {
    legacySpotId: 45231,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/pereque/',
  },
  '584204214e65fad6a7709c2d': {
    legacySpotId: 126951,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/cassis/',
  },
  '584204214e65fad6a7709c2e': {
    legacySpotId: 126965,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/eze/',
  },
  '584204204e65fad6a770955b': {
    legacySpotId: 72950,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/sand-bay/',
  },
  '584204204e65fad6a770955c': {
    legacySpotId: 72954,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/mclain-state-park/',
  },
  '584204204e65fad6a7709595': {
    legacySpotId: 73018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/sodus-point/',
  },
  '584204214e65fad6a7709b8c': {
    legacySpotId: 119416,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/factory-point/',
  },
  '584204214e65fad6a7709c95': {
    legacySpotId: 128967,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/mozambique/south-mozambique/barra-beach/',
  },
  '584204214e65fad6a7709c2c': {
    legacySpotId: 126964,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/villefranche-sur-mer/',
  },
  '584204214e65fad6a7709c93': {
    legacySpotId: 128968,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/mozambique/south-mozambique/tofinho/',
  },
  '584204214e65fad6a7709c94': {
    legacySpotId: 128969,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/mozambique/south-mozambique/jangamo-beach/',
  },
  '584204214e65fad6a7709c2b': {
    legacySpotId: 126963,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/nice/',
  },
  '584204204e65fad6a7709300': {
    legacySpotId: 61617,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/woody-head/',
  },
  '584204204e65fad6a7709301': {
    legacySpotId: 61618,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/frazers-reef/',
  },
  '584204204e65fad6a7709302': {
    legacySpotId: 61619,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/iluka-bluff/',
  },
  '584204204e65fad6a7709303': {
    legacySpotId: 61622,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/iluka-wall/',
  },
  '584204204e65fad6a7709304': {
    legacySpotId: 61623,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/turners-beach/',
  },
  '584204204e65fad6a7709305': {
    legacySpotId: 61624,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/wooli-bay/',
  },
  '584204204e65fad6a7709306': {
    legacySpotId: 61636,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/blue-holes/',
  },
  '584204204e65fad6a7709307': {
    legacySpotId: 61638,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/jake-s-point/',
  },
  '584204204e65fad6a7709308': {
    legacySpotId: 61648,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/little-shelley-beach/',
  },
  '584204204e65fad6a7709309': {
    legacySpotId: 61652,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/brooms-head/',
  },
  '584204204e65fad6a7709536': {
    legacySpotId: 72895,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/catawba/',
  },
  '5842041f4e65fad6a7708e9a': {
    legacySpotId: 45250,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-preta/',
  },
  '5842041f4e65fad6a7708e9b': {
    legacySpotId: 45249,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-juquei/',
  },
  '5842041f4e65fad6a7708e9c': {
    legacySpotId: 45305,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/barra-do-sahy/',
  },
  '5842041f4e65fad6a7708e9d': {
    legacySpotId: 45304,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/bananas-point/',
  },
  '5842041f4e65fad6a7708e9e': {
    legacySpotId: 45306,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-de-baleia/',
  },
  '5842041f4e65fad6a7708e9f': {
    legacySpotId: 45308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/camburi/',
  },
  '584204204e65fad6a7709392': {
    legacySpotId: 62481,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/samurai/',
  },
  '584204204e65fad6a77098d9': {
    legacySpotId: 94429,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/hokkaido/shioya/',
  },
  '584204214e65fad6a7709d06': {
    legacySpotId: 136964,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/shell-beach-south/',
  },
  '584204214e65fad6a7709d05': {
    legacySpotId: 135849,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/monterey-bay/',
  },
  '584204214e65fad6a7709d07': {
    legacySpotId: 137585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/dune-rd--west/',
  },
  '584204204e65fad6a7709563': {
    legacySpotId: 72966,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/stoney-point/',
  },
  '584204204e65fad6a7709310': {
    legacySpotId: 61770,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/red-rock/',
  },
  '584204204e65fad6a7709311': {
    legacySpotId: 61767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/station-creek/',
  },
  '584204204e65fad6a7709312': {
    legacySpotId: 61774,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/corindi-beach/',
  },
  '584204204e65fad6a7709313': {
    legacySpotId: 61777,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/arrawarra/',
  },
  '584204204e65fad6a7709314': {
    legacySpotId: 61779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/mullaway/',
  },
  '584204204e65fad6a7709315': {
    legacySpotId: 61778,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/woopi-headland/',
  },
  '584204204e65fad6a7709316': {
    legacySpotId: 61781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/safety-beach/',
  },
  '584204204e65fad6a7709317': {
    legacySpotId: 61782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/woolgoolga/',
  },
  '584204204e65fad6a7709318': {
    legacySpotId: 61783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sandy-beach/',
  },
  '584204204e65fad6a77098d2': {
    legacySpotId: 94437,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/aomori/okuki/',
  },
  '584204204e65fad6a770930a': {
    legacySpotId: 61656,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sandon/',
  },
  '584204204e65fad6a770930b': {
    legacySpotId: 61758,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/easter-island/easter-island/easter-island-southside/',
  },
  '584204204e65fad6a770930c': {
    legacySpotId: 61757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/easter-island/easter-island/easter-island-northside/',
  },
  '584204204e65fad6a770930d': {
    legacySpotId: 61762,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/minnie-water/',
  },
  '584204204e65fad6a770930e': {
    legacySpotId: 61764,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/diggers/',
  },
  '584204204e65fad6a770939a': {
    legacySpotId: 64586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/the-grouper/',
  },
  '584204204e65fad6a7709320': {
    legacySpotId: 61815,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/diggers-beach--coffs-harbour/',
  },
  '584204204e65fad6a7709321': {
    legacySpotId: 61816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/park-beach/',
  },
  '584204214e65fad6a7709bb6': {
    legacySpotId: 122414,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/petite-port-jersey/',
  },
  '584204204e65fad6a7709324': {
    legacySpotId: 61819,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/gallows/',
  },
  '584204204e65fad6a7709326': {
    legacySpotId: 61856,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sawtell-trapdoor/',
  },
  '584204204e65fad6a7709327': {
    legacySpotId: 61837,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/flagler-beach-pier/',
  },
  '584204204e65fad6a7709328': {
    legacySpotId: 61836,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/nosara-south/',
  },
  '584204204e65fad6a7709329': {
    legacySpotId: 61857,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sawtell-rock-point/',
  },
  '584204214e65fad6a7709c00': {
    legacySpotId: 125811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/sucre/carupano/',
  },
  '584204204e65fad6a770931b': {
    legacySpotId: 61802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/moonee-beach/',
  },
  '584204204e65fad6a770931c': {
    legacySpotId: 61800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/emerald-beach/',
  },
  '584204204e65fad6a770931d': {
    legacySpotId: 61803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sapphire-beach/',
  },
  '584204204e65fad6a770931e': {
    legacySpotId: 61804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/split-solitary-island/',
  },
  '584204204e65fad6a770931f': {
    legacySpotId: 61814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/korora/',
  },
  '584204214e65fad6a7709bb5': {
    legacySpotId: 122415,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/st--brelades-jersey/',
  },
  '584204204e65fad6a7709330': {
    legacySpotId: 61866,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/scotts-head/',
  },
  '584204204e65fad6a7709331': {
    legacySpotId: 61868,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/back-beach-at-trial-bay/',
  },
  '584204204e65fad6a7709332': {
    legacySpotId: 61993,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/stuarts-point/',
  },
  '584204204e65fad6a7709333': {
    legacySpotId: 61996,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/gap-beach/',
  },
  '584204204e65fad6a7709334': {
    legacySpotId: 61998,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/smoky-cape-beach/',
  },
  '584204204e65fad6a7709335': {
    legacySpotId: 61999,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/hat-head/',
  },
  '584204204e65fad6a7709336': {
    legacySpotId: 62001,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/crescent-head/',
  },
  '584204204e65fad6a7709337': {
    legacySpotId: 62003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/black-plomer/',
  },
  '584204204e65fad6a7709338': {
    legacySpotId: 62002,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/point-plomer/',
  },
  '584204204e65fad6a7709339': {
    legacySpotId: 61997,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/smoky-cape-point/',
  },
  '584204214e65fad6a7709bb2': {
    legacySpotId: 122411,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/secrets-jersey/',
  },
  '584204204e65fad6a770932a': {
    legacySpotId: 61862,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/kalang-river-mouth/',
  },
  '584204204e65fad6a770932b': {
    legacySpotId: 61861,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/bundagen-heads/',
  },
  '584204204e65fad6a770932c': {
    legacySpotId: 61864,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/valla-beach/',
  },
  '584204204e65fad6a770932d': {
    legacySpotId: 61863,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/wenona-head-reef/',
  },
  '584204204e65fad6a770932e': {
    legacySpotId: 61865,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/nambucca-heads/',
  },
  '584204204e65fad6a770932f': {
    legacySpotId: 61867,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/grassy-head/',
  },
  '584204204e65fad6a7709340': {
    legacySpotId: 62064,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/fairhaven/',
  },
  '584204204e65fad6a7709341': {
    legacySpotId: 62065,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/boundary-road/',
  },
  '584204204e65fad6a7709342': {
    legacySpotId: 62066,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/urquharts-bluff/',
  },
  '584204204e65fad6a7709343': {
    legacySpotId: 62067,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/hut-gully/',
  },
  '584204204e65fad6a7709344': {
    legacySpotId: 62076,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/drummonds/',
  },
  '584204204e65fad6a7709345': {
    legacySpotId: 62075,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/coronation-beach/',
  },
  '584204204e65fad6a7709346': {
    legacySpotId: 62074,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/bowes-rivermouth/',
  },
  '584204204e65fad6a7709347': {
    legacySpotId: 62077,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/hells-gate/',
  },
  '584204204e65fad6a7709348': {
    legacySpotId: 62080,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/back-beach/',
  },
  '584204204e65fad6a7709349': {
    legacySpotId: 62079,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/separation-point/',
  },
  '584204204e65fad6a77096c1': {
    legacySpotId: 91036,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/honeymoon-island/',
  },
  '584204204e65fad6a770933a': {
    legacySpotId: 62004,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/north-shore/',
  },
  '584204204e65fad6a770933b': {
    legacySpotId: 62005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/port-macquarie/',
  },
  '584204204e65fad6a770933c': {
    legacySpotId: 62000,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/hungry-head/',
  },
  '584204204e65fad6a770933d': {
    legacySpotId: 62006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/oxley-beach/',
  },
  '584204204e65fad6a770933e': {
    legacySpotId: 62007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/flynns-beach/',
  },
  '584204204e65fad6a770933f': {
    legacySpotId: 62055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/moggs-creek/',
  },
  '584204204e65fad6a7709547': {
    legacySpotId: 72919,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-on--superior-/neys-provincial-park/',
  },
  '584204204e65fad6a7709350': {
    legacySpotId: 62084,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/flat-rocks/',
  },
  '584204204e65fad6a7709351': {
    legacySpotId: 62118,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/o-donoghues/',
  },
  '584204204e65fad6a7709352': {
    legacySpotId: 62119,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/point-roadknight/',
  },
  '584204204e65fad6a7709353': {
    legacySpotId: 62134,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/nobbys-beach/',
  },
  '584204204e65fad6a7709354': {
    legacySpotId: 62138,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/miners-beach/',
  },
  '584204204e65fad6a7709356': {
    legacySpotId: 62140,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/lighthouse-beach/',
  },
  '5842041f4e65fad6a77087f8': {
    legacySpotId: 4127,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/ocean-beach--sf/',
  },
  '5842041f4e65fad6a77087f9': {
    legacySpotId: 4128,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/south-ocean-beach/',
  },
  '5842041f4e65fad6a7708da7': {
    legacySpotId: 7433,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/fukuoka/futamigaura/',
  },
  '584204214e65fad6a7709d11': {
    legacySpotId: 139213,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/broward---miami-dade/key-west/',
  },
  '584204214e65fad6a7709d14': {
    legacySpotId: 139340,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/del-mar-25th-st-/',
  },
  '584204204e65fad6a770934a': {
    legacySpotId: 62078,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/explosives/',
  },
  '584204204e65fad6a770934b': {
    legacySpotId: 62081,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/tarcoola-beach/',
  },
  '584204204e65fad6a770934c': {
    legacySpotId: 62082,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/greenough-rivermouth/',
  },
  '584204204e65fad6a770934d': {
    legacySpotId: 62089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/dongara-point/',
  },
  '584204204e65fad6a770934e': {
    legacySpotId: 62117,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/guvvos/',
  },
  '584204204e65fad6a770934f': {
    legacySpotId: 62086,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/coral-coast/port-denison/',
  },
  '584204214e65fad6a7709ba4': {
    legacySpotId: 122365,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/toro-de-oro/',
  },
  '584204214e65fad6a7709ba5': {
    legacySpotId: 122366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/k-61/',
  },
  '584204204e65fad6a77098a1': {
    legacySpotId: 91970,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/oshima/ketareef/',
  },
  '584204204e65fad6a77092a8': {
    legacySpotId: 61183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/shellys/',
  },
  '584204214e65fad6a7709ba0': {
    legacySpotId: 119814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-china/guandong/tai-long-wan/',
  },
  '584204214e65fad6a7709ba1': {
    legacySpotId: 121281,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/playa-waikiki/',
  },
  '584204204e65fad6a7709360': {
    legacySpotId: 62165,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/kylies-beach/',
  },
  '584204204e65fad6a7709361': {
    legacySpotId: 62166,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/crowdy-head/',
  },
  '584204204e65fad6a7709362': {
    legacySpotId: 62169,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/wallabi-point/',
  },
  '584204204e65fad6a7709363': {
    legacySpotId: 62168,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/old-bar/',
  },
  '584204204e65fad6a7709364': {
    legacySpotId: 62170,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/saltwater-point/',
  },
  '584204204e65fad6a7709365': {
    legacySpotId: 62137,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/shelly-beach/',
  },
  '584204204e65fad6a7709366': {
    legacySpotId: 62171,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/diamond-beach/',
  },
  '584204204e65fad6a7709367': {
    legacySpotId: 62183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/mcbride-beach/',
  },
  '584204204e65fad6a7709368': {
    legacySpotId: 62178,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/haydens-rock/',
  },
  '584204204e65fad6a7709369': {
    legacySpotId: 62172,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/black-head/',
  },
  '5842041f4e65fad6a7708fb6': {
    legacySpotId: 48470,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/portao/',
  },
  '5842041f4e65fad6a7708fb7': {
    legacySpotId: 48467,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/praia-do-futuro/',
  },
  '5842041f4e65fad6a7708fb8': {
    legacySpotId: 48468,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/titanzinho/',
  },
  '584204204e65fad6a770935a': {
    legacySpotId: 62141,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/lake-cathie/',
  },
  '584204204e65fad6a770935b': {
    legacySpotId: 62146,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/north-haven/',
  },
  '584204204e65fad6a770935c': {
    legacySpotId: 62147,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/pilot-beach/',
  },
  '584204204e65fad6a770935d': {
    legacySpotId: 62164,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/diamond-head/',
  },
  '584204204e65fad6a770935e': {
    legacySpotId: 62148,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/camden-head/',
  },
  '584204204e65fad6a770935f': {
    legacySpotId: 62167,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/manning-point/',
  },
  '5842041f4e65fad6a7708fb5': {
    legacySpotId: 48363,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/xilauzinho/',
  },
  '584204204e65fad6a77096b9': {
    legacySpotId: 89902,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/praia-azul/',
  },
  '584204204e65fad6a7709357': {
    legacySpotId: 62142,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/rainbow-beach/',
  },
  '584204204e65fad6a7709358': {
    legacySpotId: 62145,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/grants/',
  },
  '584204204e65fad6a7709371': {
    legacySpotId: 62190,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sandbar/',
  },
  '584204204e65fad6a77098b8': {
    legacySpotId: 91991,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chichijima/menou/',
  },
  '584204204e65fad6a7709374': {
    legacySpotId: 62235,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/hawks-nest/',
  },
  '584204204e65fad6a7709376': {
    legacySpotId: 62228,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/mungo-beach/',
  },
  '584204204e65fad6a7709378': {
    legacySpotId: 62238,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/shoal-bay/',
  },
  '584204204e65fad6a7709379': {
    legacySpotId: 62180,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/tanks/',
  },
  '584204204e65fad6a770936a': {
    legacySpotId: 62176,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/forster/',
  },
  '584204204e65fad6a770936b': {
    legacySpotId: 62184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/seven-mile-beach/',
  },
  '584204204e65fad6a770936c': {
    legacySpotId: 62189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/cellitos-beach/',
  },
  '584204204e65fad6a770936d': {
    legacySpotId: 62185,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/elizabeth-beach/',
  },
  '584204204e65fad6a770936e': {
    legacySpotId: 62187,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/blueys-beach/',
  },
  '584204204e65fad6a770936f': {
    legacySpotId: 62186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/boomerang-beach/',
  },
  '5842041f4e65fad6a7708da0': {
    legacySpotId: 7428,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/okinawa/okinawa/cape-zampa/',
  },
  '5842041f4e65fad6a7708da1': {
    legacySpotId: 7429,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/tanegashima/tanegashima/',
  },
  '5842041f4e65fad6a7708da2': {
    legacySpotId: 7430,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/miyazaki/aoshima-island/',
  },
  '5842041f4e65fad6a7708da3': {
    legacySpotId: 7432,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/miyazaki/oceandome/',
  },
  '5842041f4e65fad6a7708da4': {
    legacySpotId: 7431,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/miyazaki/uchiumi/',
  },
  '5842041f4e65fad6a7708da5': {
    legacySpotId: 7443,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-west/ma-alaea-harbor/',
  },
  '5842041f4e65fad6a7708da6': {
    legacySpotId: 7434,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/tokushima/kaifu-rivermouth/',
  },
  '584204204e65fad6a7709382': {
    legacySpotId: 62242,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/box-beach/',
  },
  '584204204e65fad6a7709383': {
    legacySpotId: 62485,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/birubi-point/',
  },
  '584204204e65fad6a7709384': {
    legacySpotId: 62486,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/stockton-beach/',
  },
  '584204204e65fad6a7709385': {
    legacySpotId: 62487,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/the-signa/',
  },
  '584204214e65fad6a7709d03': {
    legacySpotId: 136040,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/gazebbo-beach/',
  },
  '584204204e65fad6a7709389': {
    legacySpotId: 64110,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/lobitos/',
  },
  '584204214e65fad6a7709bc3': {
    legacySpotId: 123346,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/bahia-faro/',
  },
  '584204214e65fad6a7709bf0': {
    legacySpotId: 125758,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/la-sabana/',
  },
  '584204214e65fad6a7709d0f': {
    legacySpotId: 139183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/neptune-beach/',
  },
  '584204214e65fad6a7709bf4': {
    legacySpotId: 125799,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/isla-de-margarita/puerto-cruz/',
  },
  '584204214e65fad6a7709bdc': {
    legacySpotId: 125314,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/ahtony/',
  },
  '584204214e65fad6a7709bdd': {
    legacySpotId: 125313,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/varvara/',
  },
  '584204204e65fad6a770937a': {
    legacySpotId: 62236,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/port-stephens-boulders/',
  },
  '584204204e65fad6a770937b': {
    legacySpotId: 62179,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/pebbly-beach/',
  },
  '584204204e65fad6a770937c': {
    legacySpotId: 62181,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/bennetts-head/',
  },
  '584204204e65fad6a770937d': {
    legacySpotId: 62240,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/wreck-beach/',
  },
  '584204204e65fad6a770937e': {
    legacySpotId: 62239,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/zenith-beach/',
  },
  '584204204e65fad6a770937f': {
    legacySpotId: 62480,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/fingal-bay/',
  },
  '5842041f4e65fad6a7708db0': {
    legacySpotId: 8003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/martinique/',
  },
  '5842041f4e65fad6a7708db1': {
    legacySpotId: 8004,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/seaforth-right-point/',
  },
  '5842041f4e65fad6a7708db2': {
    legacySpotId: 8005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/osbourne/',
  },
  '5842041f4e65fad6a7708db3': {
    legacySpotId: 8008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/long-beach/',
  },
  '5842041f4e65fad6a7708db4': {
    legacySpotId: 8007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/rosie-s-bay/',
  },
  '5842041f4e65fad6a7708db5': {
    legacySpotId: 8009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/wickaninnish/',
  },
  '5842041f4e65fad6a7708db6': {
    legacySpotId: 8010,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/chesterman-beach/',
  },
  '5842041f4e65fad6a7708db7': {
    legacySpotId: 8011,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/cox-bay/',
  },
  '584204204e65fad6a7709393': {
    legacySpotId: 64584,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/nobbys-reef/',
  },
  '5842041f4e65fad6a7708db9': {
    legacySpotId: 8308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/popoyo-area-rovercam/',
  },
  '584204204e65fad6a7709395': {
    legacySpotId: 62371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/c-street--wrightsville-beach/',
  },
  '584204204e65fad6a7709396': {
    legacySpotId: 64583,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/newcastle-pipe/',
  },
  '584204204e65fad6a7709397': {
    legacySpotId: 64587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/crowrie-baths/',
  },
  '584204204e65fad6a7709398': {
    legacySpotId: 64585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/nobbys-beach/',
  },
  '584204204e65fad6a7709399': {
    legacySpotId: 64588,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/flat-rock/',
  },
  '584204204e65fad6a77098ca': {
    legacySpotId: 92723,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/northern-australia/darwin/casuarina-beach/',
  },
  '584204214e65fad6a7709c08': {
    legacySpotId: 125990,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/grindavik-lighthouse/',
  },
  '5842041f4e65fad6a7708dac': {
    legacySpotId: 7992,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/okinawa/okinawa/kudaka-jima/',
  },
  '5842041f4e65fad6a7708dad': {
    legacySpotId: 7994,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-japan/miyazaki/kisakihama/',
  },
  '5842041f4e65fad6a7708dae': {
    legacySpotId: 7993,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/shidashita-beach/',
  },
  '5842041f4e65fad6a7708daf': {
    legacySpotId: 8002,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/seaforth-left-point/',
  },
  '584204214e65fad6a7709be2': {
    legacySpotId: 125744,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/cumboto/',
  },
  '584204204e65fad6a770938c': {
    legacySpotId: 64347,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/marin-county/dillon-beach/',
  },
  '584204214e65fad6a7709be0': {
    legacySpotId: 125743,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/macuro/',
  },
  '584204214e65fad6a7709be1': {
    legacySpotId: 125745,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/la-bocaina/',
  },
  '584204204e65fad6a770938f': {
    legacySpotId: 64580,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/the-wedge/',
  },
  '5842041f4e65fad6a7708dc0': {
    legacySpotId: 8312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/playa-maderas/',
  },
  '5842041f4e65fad6a7708dc1': {
    legacySpotId: 8488,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/bocas-del-toro/',
  },
  '5842041f4e65fad6a7708dc2': {
    legacySpotId: 8494,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/restaurants--tavarua/',
  },
  '584204214e65fad6a7709be8': {
    legacySpotId: 125750,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/playita/',
  },
  '5842041f4e65fad6a7708dc5': {
    legacySpotId: 8493,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-south/cloudbreak/',
  },
  '584204214e65fad6a7709bec': {
    legacySpotId: 125755,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/punta-care/',
  },
  '584204214e65fad6a7709be4': {
    legacySpotId: 125740,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/playa-los-cuatro/',
  },
  '5842041f4e65fad6a7708dc8': {
    legacySpotId: 8558,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-pacific/punta-rocas/',
  },
  '584204214e65fad6a7709beb': {
    legacySpotId: 125753,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/otro-pais/',
  },
  '584204214e65fad6a7709be7': {
    legacySpotId: 125748,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/mamo/',
  },
  '584204214e65fad6a7709bea': {
    legacySpotId: 125752,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/puerto-azul/',
  },
  '584204214e65fad6a7709be6': {
    legacySpotId: 125747,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/el-malecon/',
  },
  '5842041f4e65fad6a7708dba': {
    legacySpotId: 8309,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/popoyo-outer-reef/',
  },
  '5842041f4e65fad6a7708dbb': {
    legacySpotId: 8006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/minutes/',
  },
  '584204204e65fad6a77098dc': {
    legacySpotId: 94445,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/akita/kisakata/',
  },
  '5842041f4e65fad6a7708dbd': {
    legacySpotId: 8311,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/manzanillo/',
  },
  '5842041f4e65fad6a7708dbe': {
    legacySpotId: 8055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/las-flores/',
  },
  '5842041f4e65fad6a7708dbf': {
    legacySpotId: 8366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-south/teahupoo/',
  },
  '584204204e65fad6a770939b': {
    legacySpotId: 64589,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/newcastle-point/',
  },
  '584204204e65fad6a770939c': {
    legacySpotId: 64608,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/graveyards/',
  },
  '584204204e65fad6a770939d': {
    legacySpotId: 64610,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/bar-beach-reef/',
  },
  '584204204e65fad6a770939e': {
    legacySpotId: 64590,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/newcastle-beach/',
  },
  '584204204e65fad6a770939f': {
    legacySpotId: 64612,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/bar-beach/',
  },
  '584204214e65fad6a7709bd9': {
    legacySpotId: 125310,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/harmani/',
  },
  '584204214e65fad6a7709bd7': {
    legacySpotId: 125308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/shabla/',
  },
  '584204214e65fad6a7709bd8': {
    legacySpotId: 125309,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/kabakam-beach/',
  },
  '584204214e65fad6a7709bfd': {
    legacySpotId: 125823,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-pacific/punta-teta/',
  },
  '584204214e65fad6a7709bd3': {
    legacySpotId: 125302,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/erba/',
  },
  '584204214e65fad6a7709bd4': {
    legacySpotId: 125300,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/woodyville/',
  },
  '584204214e65fad6a7709bfc': {
    legacySpotId: 125814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/sucre/pui-pui/',
  },
  '584204214e65fad6a7709bd6': {
    legacySpotId: 125303,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/kumcagiz/',
  },
  '584204214e65fad6a7709bfa': {
    legacySpotId: 125810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/sucre/punta-de-guiria/',
  },
  '584204214e65fad6a7709bfb': {
    legacySpotId: 125822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-pacific/punta-palmar/',
  },
  '584204214e65fad6a7709bd5': {
    legacySpotId: 125301,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/sarisu/',
  },
  '5842041f4e65fad6a7708dca': {
    legacySpotId: 9201,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/santos/',
  },
  '5842041f4e65fad6a7708dcc': {
    legacySpotId: 9202,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/pitangueiras/',
  },
  '5842041f4e65fad6a7708dcd': {
    legacySpotId: 9203,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/maresias/',
  },
  '584204204e65fad6a770999d': {
    legacySpotId: 108155,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/tavares-bay--maui/',
  },
  '584204214e65fad6a7709bcc': {
    legacySpotId: 125284,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/united-arab-emirates/dubai/jumeirah-beach/',
  },
  '584204204e65fad6a770999f': {
    legacySpotId: 108228,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/coast-guard-beach/',
  },
  '584204204e65fad6a770999e': {
    legacySpotId: 108229,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/nauset-light-beach/',
  },
  '5842041f4e65fad6a7708de0': {
    legacySpotId: 10809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-west/olowalu/',
  },
  '5842041f4e65fad6a7708de1': {
    legacySpotId: 10810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-south/the-cove/',
  },
  '5842041f4e65fad6a7708de2': {
    legacySpotId: 10811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-south/la-perouse-bay/',
  },
  '5842041f4e65fad6a7708de3': {
    legacySpotId: 10812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-west/rainbows/',
  },
  '5842041f4e65fad6a7708de4': {
    legacySpotId: 10814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/honolua-bay/',
  },
  '5842041f4e65fad6a7708de5': {
    legacySpotId: 10813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/kanaha/',
  },
  '5842041f4e65fad6a7708de6': {
    legacySpotId: 10816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/kahului-harbor/',
  },
  '5842041f4e65fad6a7708de7': {
    legacySpotId: 10815,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/honokohau-bay/',
  },
  '5842041f4e65fad6a7708de8': {
    legacySpotId: 10817,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/ho-okipa/',
  },
  '5842041f4e65fad6a7708de9': {
    legacySpotId: 10818,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/peahi--jaws-/',
  },
  '584204204e65fad6a7709984': {
    legacySpotId: 96802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/tahiti/tahiti-north/ahonu/',
  },
  '584204214e65fad6a7709d20': {
    legacySpotId: 140552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/cowells-overview/',
  },
  '5842041f4e65fad6a7708ddc': {
    legacySpotId: 9858,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/misquamicut-state-beach/',
  },
  '584204204e65fad6a7709b07': {
    legacySpotId: 117781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/arenzano/',
  },
  '5842041f4e65fad6a7708ddf': {
    legacySpotId: 10763,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/capitola/',
  },
  '584204214e65fad6a7709bb0': {
    legacySpotId: 122409,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/plemont-jersey/',
  },
  '584204204e65fad6a77092c5': {
    legacySpotId: 61370,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/moreton-island--northeast-/',
  },
  '5842041f4e65fad6a7708cf8': {
    legacySpotId: 7026,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/guarda-do-emba-uacute-/',
  },
  '5842041f4e65fad6a7708df0': {
    legacySpotId: 10826,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-south/south-point/',
  },
  '5842041f4e65fad6a7708df1': {
    legacySpotId: 10830,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-north/polihale/',
  },
  '5842041f4e65fad6a7708df2': {
    legacySpotId: 10832,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/turtle-bay-west/',
  },
  '5842041f4e65fad6a7708df3': {
    legacySpotId: 10831,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-north/mana-point/',
  },
  '5842041f4e65fad6a7708900': {
    legacySpotId: 4896,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/shitpipe/',
  },
  '5842041f4e65fad6a7708df5': {
    legacySpotId: 10834,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/haleiwa/',
  },
  '5842041f4e65fad6a7708df6': {
    legacySpotId: 10837,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/windward-side/sandy-beach/',
  },
  '5842041f4e65fad6a7708df7': {
    legacySpotId: 10838,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/windward-side/popoia-island-kailua/',
  },
  '5842041f4e65fad6a7708904': {
    legacySpotId: 4902,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/hermosa-beach/',
  },
  '5842041f4e65fad6a7708906': {
    legacySpotId: 4900,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/el-porto/',
  },
  '5842041f4e65fad6a7708907': {
    legacySpotId: 4901,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/manhattan-beach/',
  },
  '584204214e65fad6a7709bb4': {
    legacySpotId: 122412,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/la-rocco-jersey/',
  },
  '5842041f4e65fad6a7708a0d': {
    legacySpotId: 5172,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/10th-street-north-to-14th-street-north/',
  },
  '58bdfa7882d034001252e3d8': {
    legacySpotId: 144651,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/praia-da-nazare-overview/',
  },
  '584204214e65fad6a7709bb3': {
    legacySpotId: 122413,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/les-brayes-jersey/',
  },
  '5842041f4e65fad6a7708a0c': {
    legacySpotId: 5168,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/south-carolina-avenue/',
  },
  '5842041f4e65fad6a7708dea': {
    legacySpotId: 10820,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/hana-bay/',
  },
  '5842041f4e65fad6a7708deb': {
    legacySpotId: 10819,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-north/honomanu-bay/',
  },
  '5842041f4e65fad6a7708dec': {
    legacySpotId: 10822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-hilo/honoli-i/',
  },
  '5842041f4e65fad6a7708ded': {
    legacySpotId: 10823,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-kona/pinetrees/',
  },
  '5842041f4e65fad6a7708dee': {
    legacySpotId: 10825,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-kona/magics/',
  },
  '5842041f4e65fad6a7708def': {
    legacySpotId: 10824,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-kona/hapuna-bay/',
  },
  '584204214e65fad6a7709baa': {
    legacySpotId: 122369,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/negrei/',
  },
  '584204204e65fad6a770997d': {
    legacySpotId: 95156,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/mendocino/caspar-beach/',
  },
  '584204214e65fad6a7709bac': {
    legacySpotId: 122364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/el-pimental/',
  },
  '5842041f4e65fad6a7708a15': {
    legacySpotId: 5180,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/ortley-beach/',
  },
  '5842041f4e65fad6a7708a14': {
    legacySpotId: 5179,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/seaside-heights/',
  },
  '584204214e65fad6a7709bb9': {
    legacySpotId: 122563,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/jordan-river/',
  },
  '5842041f4e65fad6a7708914': {
    legacySpotId: 4883,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/sunset-point/',
  },
  '584204214e65fad6a7709d0c': {
    legacySpotId: 138421,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/paco-de-arcos/',
  },
  '5842041f4e65fad6a7708dfa': {
    legacySpotId: 10840,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/windward-side/crouching-lion/',
  },
  '5842041f4e65fad6a7708dfb': {
    legacySpotId: 10841,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/windward-side/kahuku/',
  },
  '5842041f4e65fad6a7708dfc': {
    legacySpotId: 10844,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/west-side/yokohama-bay/',
  },
  '5842041f4e65fad6a7708dfd': {
    legacySpotId: 10845,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/west-side/makaha/',
  },
  '5842041f4e65fad6a7708dfe': {
    legacySpotId: 10847,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/barbers-point/',
  },
  '5842041f4e65fad6a7708dff': {
    legacySpotId: 10846,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/west-side/ma-ili/',
  },
  '584204214e65fad6a7709bbf': {
    legacySpotId: 123340,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/la-rambla/',
  },
  '584204204e65fad6a770998c': {
    legacySpotId: 103681,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/huntington-state-beach/',
  },
  '584204214e65fad6a7709bbb': {
    legacySpotId: 122565,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/port-renfrew/',
  },
  '584204204e65fad6a770998e': {
    legacySpotId: 106528,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-centro-portugal/figueira-da-foz/',
  },
  '584204204e65fad6a770998d': {
    legacySpotId: 103685,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/bolsa-chica-overview/',
  },
  '584204214e65fad6a7709bbc': {
    legacySpotId: 122765,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/punta-galea/',
  },
  '5842041f4e65fad6a7708920': {
    legacySpotId: 4910,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/torrance-beach-haggerty-s/',
  },
  '5842041f4e65fad6a7708921': {
    legacySpotId: 4912,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/redondo-breakwater/',
  },
  '5842041f4e65fad6a7708927': {
    legacySpotId: 4931,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/cabrillo-beach/',
  },
  '584204204e65fad6a7709994': {
    legacySpotId: 107951,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/torrey-pines-state-beach/',
  },
  '584204204e65fad6a7709991': {
    legacySpotId: 106919,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/north-end-42nd---46th-st-/',
  },
  '584204204e65fad6a7709996': {
    legacySpotId: 108024,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/the-hook/',
  },
  '584204204e65fad6a7709b39': {
    legacySpotId: 117866,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/isola-delle-femmine/',
  },
  '595579ac31d48a00128db928': {
    legacySpotId: 146782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/maaravi/',
  },
  '584204204e65fad6a7709b38': {
    legacySpotId: 117862,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/capo-vieste/',
  },
  '584204204e65fad6a77092b5': {
    legacySpotId: 61343,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/wye-river/',
  },
  '584204204e65fad6a7709b36': {
    legacySpotId: 117859,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/silvi-marina/',
  },
  '584204214e65fad6a7709bc0': {
    legacySpotId: 123339,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/bunge-y-el-mar/',
  },
  '584204204e65fad6a77092b4': {
    legacySpotId: 61347,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/lorne-s-a-s/',
  },
  '584204204e65fad6a77092b1': {
    legacySpotId: 61326,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/yardie-creek/',
  },
  '584204204e65fad6a7709380': {
    legacySpotId: 62482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/one-mile-beach/',
  },
  '5842041f4e65fad6a7708930': {
    legacySpotId: 4940,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/chart-house/',
  },
  '584204204e65fad6a77096fa': {
    legacySpotId: 91548,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/yamagata/yura/',
  },
  '584204214e65fad6a7709bc8': {
    legacySpotId: 125281,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/united-arab-emirates/dubai/uaq-beach/',
  },
  '584204204e65fad6a7709381': {
    legacySpotId: 62483,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/anna-bay-reef/',
  },
  '584204214e65fad6a7709bc6': {
    legacySpotId: 125075,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/shepard-park/',
  },
  '5842041f4e65fad6a7708936': {
    legacySpotId: 4947,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/point-dume/',
  },
  '5842041f4e65fad6a7708937': {
    legacySpotId: 4944,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/latigo-point/',
  },
  '584204214e65fad6a7709d16': {
    legacySpotId: 139337,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/praia-do-barbas/',
  },
  '584204214e65fad6a7709bc2': {
    legacySpotId: 123345,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/varese/',
  },
  '5842041f4e65fad6a7708a3d': {
    legacySpotId: 5231,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/avon-pier/',
  },
  '5842041f4e65fad6a7708a3e': {
    legacySpotId: 5234,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/s-turns/',
  },
  '584204214e65fad6a7709d18': {
    legacySpotId: 139341,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/santa-barbara-harbor-overview/',
  },
  '584204214e65fad6a7709bc4': {
    legacySpotId: 123347,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/argentina/central-argentina/la-helice/',
  },
  '58bdf3a70cec4200133464f2': {
    legacySpotId: 145543,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/lagide/',
  },
  '5842041f4e65fad6a770892c': {
    legacySpotId: 4935,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/lunada-bay/',
  },
  '5842041f4e65fad6a770892d': {
    legacySpotId: 4936,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/pv-cove/',
  },
  '584204214e65fad6a7709d02': {
    legacySpotId: 136184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/morro-bay-harbor-entrance/',
  },
  '58bddc6082d034001252e3d1': {
    legacySpotId: 145718,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/doheny-rivermouth/',
  },
  '584204214e65fad6a7709d00': {
    legacySpotId: 135851,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/piedras-blancas/',
  },
  '584204204e65fad6a77091e4': {
    legacySpotId: 59149,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/stone-island/',
  },
  '5842041f4e65fad6a7708941': {
    legacySpotId: 4957,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/monterey/marina-state-beach/',
  },
  '584204204e65fad6a7709b48': {
    legacySpotId: 117884,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/porto-ferro/',
  },
  '584204214e65fad6a7709bf3': {
    legacySpotId: 125315,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/silister/',
  },
  '5842041f4e65fad6a7708944': {
    legacySpotId: 4960,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/stanley-s/',
  },
  '5842041f4e65fad6a7708945': {
    legacySpotId: 4961,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/oil-piers/',
  },
  '584204214e65fad6a7709d0e': {
    legacySpotId: 139184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/billy-mitchell--frisco/',
  },
  '584204204e65fad6a77091e1': {
    legacySpotId: 58949,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/playa-santa-maria/',
  },
  '584204204e65fad6a77091e2': {
    legacySpotId: 58950,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/san-quintin/cielito-lindo/',
  },
  '5842041f4e65fad6a770893a': {
    legacySpotId: 4949,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/zuma/',
  },
  '584204214e65fad6a7709bda': {
    legacySpotId: 125312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/melnitza/',
  },
  '5842041f4e65fad6a770893f': {
    legacySpotId: 4953,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/leo-carrillo/',
  },
  '584204214e65fad6a7709bf5': {
    legacySpotId: 125801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/isla-de-margarita/playa-el-agua/',
  },
  '584204204e65fad6a77092a0': {
    legacySpotId: 61171,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/kawana/',
  },
  '584204204e65fad6a77092a1': {
    legacySpotId: 61173,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/warana/',
  },
  '584204204e65fad6a77092a2': {
    legacySpotId: 61177,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/ann-street-reef/',
  },
  '584204204e65fad6a77092a3': {
    legacySpotId: 61176,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/long-track/',
  },
  '584204204e65fad6a77092a4': {
    legacySpotId: 61178,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/dickys/',
  },
  '584204204e65fad6a77092a5': {
    legacySpotId: 61179,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/moffat-cliffs/',
  },
  '584204204e65fad6a77092a6': {
    legacySpotId: 61181,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/moffats/',
  },
  '5842041f4e65fad6a7708959': {
    legacySpotId: 4980,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/emma-wood/',
  },
  '584204204e65fad6a77092a9': {
    legacySpotId: 61185,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/happys/',
  },
  '5842041f4e65fad6a7708a00': {
    legacySpotId: 5160,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/ocean-grove/',
  },
  '584204214e65fad6a7709be5': {
    legacySpotId: 125746,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/cuyagua/',
  },
  '584204204e65fad6a77091d1': {
    legacySpotId: 58842,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-baja/ensenada/boca-de-santo-tomas/',
  },
  '584204204e65fad6a77091d2': {
    legacySpotId: 58841,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/mazatlan-area/rucos/',
  },
  '5842041f4e65fad6a770894c': {
    legacySpotId: 4968,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/oxnard/',
  },
  '584204214e65fad6a7709c04': {
    legacySpotId: 125985,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/garour/',
  },
  '584204214e65fad6a7709c02': {
    legacySpotId: 125982,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/breidavik/',
  },
  '584204214e65fad6a7709c03': {
    legacySpotId: 125988,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/sandvik/',
  },
  '5842041f4e65fad6a7708a01': {
    legacySpotId: 5157,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/16th-ave-/',
  },
  '5842041f4e65fad6a7708a02': {
    legacySpotId: 5161,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/asbury-park/',
  },
  '584204214e65fad6a7709c01': {
    legacySpotId: 125984,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/hafnarfjordur-beach-reef/',
  },
  '5842041f4e65fad6a7708960': {
    legacySpotId: 4990,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/leadbetter-point/',
  },
  '5842041f4e65fad6a7708961': {
    legacySpotId: 4991,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/refugio/',
  },
  '5842041f4e65fad6a7708962': {
    legacySpotId: 4995,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/coal-oil-point/',
  },
  '5842041f4e65fad6a7708963': {
    legacySpotId: 4982,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/ventura-overhead/',
  },
  '584204204e65fad6a77092b3': {
    legacySpotId: 61341,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/kennett-river/',
  },
  '584204204e65fad6a7709922': {
    legacySpotId: 94515,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/shimane/kumura/',
  },
  '5842041f4e65fad6a7708966': {
    legacySpotId: 4998,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/sandspit/',
  },
  '584204204e65fad6a77092b6': {
    legacySpotId: 61345,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/cumbos/',
  },
  '584204204e65fad6a77092b7': {
    legacySpotId: 61346,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/blue-moons/',
  },
  '584204204e65fad6a77092b8': {
    legacySpotId: 61349,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/lorne-point/',
  },
  '584204204e65fad6a77092b9': {
    legacySpotId: 61350,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/lorne/',
  },
  '584204214e65fad6a7709bf7': {
    legacySpotId: 125805,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/isla-de-margarita/parguito/',
  },
  '5842041f4e65fad6a770895a': {
    legacySpotId: 4985,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/mussel-shoals--aka-little-rincon-/',
  },
  '584204204e65fad6a77092ab': {
    legacySpotId: 61324,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/dunes/',
  },
  '584204204e65fad6a77092ac': {
    legacySpotId: 61184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/wickham-point/',
  },
  '5842041f4e65fad6a770895e': {
    legacySpotId: 4988,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/summer-beach/',
  },
  '5842041f4e65fad6a770895f': {
    legacySpotId: 4989,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/solimar/',
  },
  '584204204e65fad6a77092af': {
    legacySpotId: 61339,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/kennett-point/',
  },
  '5842041f4e65fad6a7708970': {
    legacySpotId: 5008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/princeton-jetty/',
  },
  '584204204e65fad6a77092c0': {
    legacySpotId: 61360,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/osprey-bay/',
  },
  '5842041f4e65fad6a7708972': {
    legacySpotId: 5005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/tunitas-creek/',
  },
  '584204204e65fad6a77092c2': {
    legacySpotId: 61366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/bribie-island/',
  },
  '5842041f4e65fad6a7708974': {
    legacySpotId: 5011,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/montara/',
  },
  '5842041f4e65fad6a7708975': {
    legacySpotId: 5000,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/tarpits/',
  },
  '5842041f4e65fad6a7708976': {
    legacySpotId: 5013,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/pacifica-lindamar/',
  },
  '584204204e65fad6a77092c6': {
    legacySpotId: 61376,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/frenchman-beach/',
  },
  '584204204e65fad6a77092c7': {
    legacySpotId: 61375,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/point-lookout/',
  },
  '584204204e65fad6a77092c8': {
    legacySpotId: 61377,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/turtle-bay/',
  },
  '584204204e65fad6a77092c9': {
    legacySpotId: 61378,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/surf-point/',
  },
  '584204214e65fad6a7709bed': {
    legacySpotId: 125756,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/anare/',
  },
  '584204214e65fad6a7709be3': {
    legacySpotId: 125739,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/palma-sola/',
  },
  '584204204e65fad6a77092ba': {
    legacySpotId: 61352,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/spout-creek/',
  },
  '5842041f4e65fad6a770896c': {
    legacySpotId: 4997,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/campus-point/',
  },
  '584204204e65fad6a77092bc': {
    legacySpotId: 61358,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/turquoise-bay/',
  },
  '5842041f4e65fad6a770896e': {
    legacySpotId: 5001,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/santa-barbara/carpinteria-state-beach/',
  },
  '5842041f4e65fad6a770896f': {
    legacySpotId: 5007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/half-moon-bay/',
  },
  '584204204e65fad6a77092bf': {
    legacySpotId: 61364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/fencelines/',
  },
  '584204214e65fad6a7709c24': {
    legacySpotId: 126953,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/cap-saint-louis/',
  },
  '584204214e65fad6a7709c25': {
    legacySpotId: 126956,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/fabregas/',
  },
  '5842041f4e65fad6a7708f00': {
    legacySpotId: 46097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/bambui/',
  },
  '5842041f4e65fad6a7708f02': {
    legacySpotId: 46101,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-forte-dos-andradas/',
  },
  '5842041f4e65fad6a7708f03': {
    legacySpotId: 46102,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/ponta-negra/',
  },
  '5842041f4e65fad6a7708f04': {
    legacySpotId: 46104,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/jacone/',
  },
  '5842041f4e65fad6a7708981': {
    legacySpotId: 5023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/four-mile/',
  },
  '584204204e65fad6a77092d1': {
    legacySpotId: 61423,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/narrowneck/',
  },
  '5842041f4e65fad6a7708983': {
    legacySpotId: 5024,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/davenport/',
  },
  '5842041f4e65fad6a7708f08': {
    legacySpotId: 46110,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/barrinha/',
  },
  '584204204e65fad6a77092d4': {
    legacySpotId: 61430,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/nobbys-beach/',
  },
  '5842041f4e65fad6a7708986': {
    legacySpotId: 5027,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/natural-bridges/',
  },
  '5842041f4e65fad6a7708987': {
    legacySpotId: 5028,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/mitchell-s-cove/',
  },
  '5842041f4e65fad6a7708988': {
    legacySpotId: 5031,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/the-harbor/',
  },
  '584204204e65fad6a77092d8': {
    legacySpotId: 61468,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/currumbin/',
  },
  '584204204e65fad6a77092d9': {
    legacySpotId: 61470,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/flat-rock/',
  },
  '584204214e65fad6a7709c3f': {
    legacySpotId: 127242,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/libanona/',
  },
  '5842041f4e65fad6a770897a': {
    legacySpotId: 5015,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/fort-point/',
  },
  '584204204e65fad6a77092ca': {
    legacySpotId: 61380,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/south-stradbroke-island/',
  },
  '5842041f4e65fad6a770897c': {
    legacySpotId: 5017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/sharp-park/',
  },
  '5842041f4e65fad6a770897d': {
    legacySpotId: 5018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/pescadero/',
  },
  '5842041f4e65fad6a770897e': {
    legacySpotId: 5019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/pigeon-point/',
  },
  '5842041f4e65fad6a770897f': {
    legacySpotId: 5020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/ano-nuevo/',
  },
  '584204204e65fad6a77092cf': {
    legacySpotId: 61422,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/southport-yacht-club/',
  },
  '5842041f4e65fad6a7708f10': {
    legacySpotId: 46186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-seca/',
  },
  '5842041f4e65fad6a7708f11': {
    legacySpotId: 46187,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/figueira/',
  },
  '5842041f4e65fad6a7708f12': {
    legacySpotId: 46189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-de-massambaba/',
  },
  '5842041f4e65fad6a7708a3c': {
    legacySpotId: 5228,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/ocracoke/',
  },
  '5842041f4e65fad6a7708990': {
    legacySpotId: 5039,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-santa-barbara-county/point-conception/',
  },
  '5842041f4e65fad6a7708991': {
    legacySpotId: 5038,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-santa-barbara-county/jalama/',
  },
  '5842041f4e65fad6a7708f16': {
    legacySpotId: 46197,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/praia-do-forte/',
  },
  '5842041f4e65fad6a7708f17': {
    legacySpotId: 46195,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/praia-do-foguete/',
  },
  '5842041f4e65fad6a7708f18': {
    legacySpotId: 46323,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/praia-brava/',
  },
  '5842041f4e65fad6a7708f19': {
    legacySpotId: 46328,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/tucuns/',
  },
  '5842041f4e65fad6a7708996': {
    legacySpotId: 5043,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-santa-barbara-county/surf-beach/',
  },
  '584204204e65fad6a77092e6': {
    legacySpotId: 61504,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/casuarina/',
  },
  '584204204e65fad6a77092e7': {
    legacySpotId: 61505,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/cabarita-south/',
  },
  '584204204e65fad6a77092e8': {
    legacySpotId: 61506,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/hastings-point/',
  },
  '584204204e65fad6a77092e9': {
    legacySpotId: 61552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/pottsville-beach/',
  },
  '584204214e65fad6a7709c0b': {
    legacySpotId: 125993,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/stokksnes/',
  },
  '584204214e65fad6a7709c0c': {
    legacySpotId: 125994,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/dalkur/',
  },
  '5842041f4e65fad6a7708f0b': {
    legacySpotId: 46113,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/canal/',
  },
  '584204214e65fad6a7709c62': {
    legacySpotId: 127805,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/armenistis--ikiria-/',
  },
  '5842041f4e65fad6a7708f0d': {
    legacySpotId: 46116,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-forte-dos-andradas/',
  },
  '5842041f4e65fad6a770898a': {
    legacySpotId: 5030,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/26th-ave/',
  },
  '5842041f4e65fad6a7708f0f': {
    legacySpotId: 46120,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/backdoor/',
  },
  '584204204e65fad6a77092db': {
    legacySpotId: 61472,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/rainbow-bay/',
  },
  '5842041f4e65fad6a770898e': {
    legacySpotId: 5036,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/santa-cruz/manresa/',
  },
  '584204204e65fad6a77092de': {
    legacySpotId: 61496,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/point-danger/',
  },
  '584204204e65fad6a77092df': {
    legacySpotId: 61497,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/fingal-beach/',
  },
  '5842041f4e65fad6a7708f20': {
    legacySpotId: 46482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/marlin/',
  },
  '5842041f4e65fad6a7708f21': {
    legacySpotId: 46525,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/crystal-cove/',
  },
  '5842041f4e65fad6a7708f22': {
    legacySpotId: 46543,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/praia-grande/',
  },
  '5842041f4e65fad6a7708f23': {
    legacySpotId: 46541,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/regencia/',
  },
  '5842041f4e65fad6a7708f24': {
    legacySpotId: 46544,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/nova-almeida/',
  },
  '5842041f4e65fad6a7708f25': {
    legacySpotId: 46545,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/praia-da-baleia/',
  },
  '5842041f4e65fad6a7708f26': {
    legacySpotId: 46548,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/manguinhos--brazil/',
  },
  '5842041f4e65fad6a7708f27': {
    legacySpotId: 46549,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-da-cacao/',
  },
  '5842041f4e65fad6a7708f28': {
    legacySpotId: 46546,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/xa/',
  },
  '5842041f4e65fad6a7708f29': {
    legacySpotId: 46550,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/carapebus/',
  },
  '584204204e65fad6a77092f5': {
    legacySpotId: 61570,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/kings-beach/',
  },
  '584204204e65fad6a77092f6': {
    legacySpotId: 61569,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/south-broken-head/',
  },
  '584204204e65fad6a77092f7': {
    legacySpotId: 61597,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/7-mile-beach/',
  },
  '584204204e65fad6a77092f8': {
    legacySpotId: 61598,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/boulder-beach/',
  },
  '584204204e65fad6a77092f9': {
    legacySpotId: 61600,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/skennars-head/',
  },
  '584204214e65fad6a7709d25': {
    legacySpotId: 142795,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/manzanita/',
  },
  '5842041f4e65fad6a7708f1a': {
    legacySpotId: 46326,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/caravelas/',
  },
  '5842041f4e65fad6a7708f1b': {
    legacySpotId: 46329,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/praia-brava/',
  },
  '5842041f4e65fad6a7708f1c': {
    legacySpotId: 46439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/puerto-sandino/',
  },
  '5842041f4e65fad6a7708f1d': {
    legacySpotId: 46480,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/mangue-seco/',
  },
  '5842041f4e65fad6a7708f1e': {
    legacySpotId: 46473,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/guriri/',
  },
  '5842041f4e65fad6a7708f1f': {
    legacySpotId: 46481,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/conde/',
  },
  '5842041f4e65fad6a770899c': {
    legacySpotId: 5054,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/moonstone/',
  },
  '5842041f4e65fad6a770899d': {
    legacySpotId: 5053,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/pico-creek/',
  },
  '5842041f4e65fad6a770899e': {
    legacySpotId: 5055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/santa-rosa-creek/',
  },
  '584204204e65fad6a77092ee': {
    legacySpotId: 61557,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/golden-beach/',
  },
  '584204204e65fad6a77092ef': {
    legacySpotId: 61561,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/the-wreck/',
  },
  '5842041f4e65fad6a7708f30': {
    legacySpotId: 46556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/velzyland/',
  },
  '5842041f4e65fad6a7708f31': {
    legacySpotId: 46560,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/pompeia/',
  },
  '5842041f4e65fad6a7708f32': {
    legacySpotId: 46562,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/barrinha/',
  },
  '5842041f4e65fad6a7708f33': {
    legacySpotId: 46565,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/busca-vida/',
  },
  '5842041f4e65fad6a7708f34': {
    legacySpotId: 46570,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/ipitanga/',
  },
  '5842041f4e65fad6a7708f35': {
    legacySpotId: 46572,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/aleluia/',
  },
  '5842041f4e65fad6a7708f36': {
    legacySpotId: 46566,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/barrao--brazil/',
  },
  '5842041f4e65fad6a7708f37': {
    legacySpotId: 46568,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/vilas/',
  },
  '5842041f4e65fad6a7708f38': {
    legacySpotId: 46641,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/coral-um/',
  },
  '5842041f4e65fad6a7708f39': {
    legacySpotId: 46642,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-da-belina/',
  },
  '584204204e65fad6a77095e2': {
    legacySpotId: 74031,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/julianadorp/',
  },
  '5842041f4e65fad6a7708f2a': {
    legacySpotId: 46551,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/costa-do-sauipe/',
  },
  '5842041f4e65fad6a7708f2b': {
    legacySpotId: 46552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/imbassai/',
  },
  '5842041f4e65fad6a7708f2c': {
    legacySpotId: 46553,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/praia-do-forte/',
  },
  '5842041f4e65fad6a7708f2d': {
    legacySpotId: 46554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/itacimirim/',
  },
  '5842041f4e65fad6a7708f2e': {
    legacySpotId: 46555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-da-praia-mole/',
  },
  '5842041f4e65fad6a7708f2f': {
    legacySpotId: 46557,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/praia-de-camburi/',
  },
  '5842041f4e65fad6a7708a21': {
    legacySpotId: 5192,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/naval-jetties/',
  },
  '584204204e65fad6a77092fc': {
    legacySpotId: 61602,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/flat-rock-beach/',
  },
  '584204204e65fad6a77092fd': {
    legacySpotId: 61605,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/shelley-beach/',
  },
  '584204204e65fad6a77092fe': {
    legacySpotId: 61615,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/broadwater-beach/',
  },
  '584204204e65fad6a77092ff': {
    legacySpotId: 61616,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/evans-head/',
  },
  '584204204e65fad6a770952c': {
    legacySpotId: 72886,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/metropark/',
  },
  '5842041f4e65fad6a7708f40': {
    legacySpotId: 46707,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/bariri/',
  },
  '5842041f4e65fad6a7708f41': {
    legacySpotId: 46708,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/praia-do-morro/',
  },
  '5842041f4e65fad6a7708f42': {
    legacySpotId: 46749,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/maimba/',
  },
  '5842041f4e65fad6a7708f43': {
    legacySpotId: 46750,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/plator/',
  },
  '5842041f4e65fad6a7708f44': {
    legacySpotId: 46753,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/corrente/',
  },
  '5842041f4e65fad6a7708f45': {
    legacySpotId: 46751,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-do-ubu/',
  },
  '5842041f4e65fad6a7708f46': {
    legacySpotId: 46752,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/stela-maris/',
  },
  '5842041f4e65fad6a7708f47': {
    legacySpotId: 46754,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/seper/',
  },
  '5842041f4e65fad6a7708f48': {
    legacySpotId: 46755,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/pedro-do-sal/',
  },
  '5842041f4e65fad6a7708f49': {
    legacySpotId: 46756,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/marataizes/',
  },
  '584204204e65fad6a7709560': {
    legacySpotId: 72958,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/madeline-island-big-bay/',
  },
  '5842041f4e65fad6a7708f3a': {
    legacySpotId: 46644,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-da-fruta/',
  },
  '5842041f4e65fad6a7708f3b': {
    legacySpotId: 46702,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/coral-dois/',
  },
  '5842041f4e65fad6a7708f3c': {
    legacySpotId: 46703,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/dunas-de-ule/',
  },
  '5842041f4e65fad6a7708f3d': {
    legacySpotId: 46704,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/tropical/',
  },
  '5842041f4e65fad6a7708f3e': {
    legacySpotId: 46705,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-de-bororo/',
  },
  '5842041f4e65fad6a7708f3f': {
    legacySpotId: 46706,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/setiba/',
  },
  '5842041f4e65fad6a7708f50': {
    legacySpotId: 46850,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/psiu/',
  },
  '5842041f4e65fad6a7708f51': {
    legacySpotId: 46849,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/pescador/',
  },
  '5842041f4e65fad6a7708f52': {
    legacySpotId: 46855,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/meridien/',
  },
  '5842041f4e65fad6a7708f53': {
    legacySpotId: 46851,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/tubao/',
  },
  '5842041f4e65fad6a7708f54': {
    legacySpotId: 46853,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/amaralina/',
  },
  '5842041f4e65fad6a7708f55': {
    legacySpotId: 46967,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/sergipe/ponta-do-mangue/',
  },
  '5842041f4e65fad6a7708f57': {
    legacySpotId: 46973,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/tony/',
  },
  '5842041f4e65fad6a7708f58': {
    legacySpotId: 46976,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/sergipe/praia-do-meio/',
  },
  '5842041f4e65fad6a7708f59': {
    legacySpotId: 46974,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/barra-vento/',
  },
  '5842041f4e65fad6a7708f4a': {
    legacySpotId: 46757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/farol/',
  },
  '5842041f4e65fad6a7708f4b': {
    legacySpotId: 46758,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/espirito-santo/ponta-dos-castelhanos/',
  },
  '5842041f4e65fad6a7708f4c': {
    legacySpotId: 46759,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/itapoa/',
  },
  '5842041f4e65fad6a7708f4d': {
    legacySpotId: 46845,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/sesc/',
  },
  '5842041f4e65fad6a7708f4e': {
    legacySpotId: 46846,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/terceira-ponte/',
  },
  '5842041f4e65fad6a7708f4f': {
    legacySpotId: 46848,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/alex/',
  },
  '584204204e65fad6a770998f': {
    legacySpotId: 106736,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---west/niigata/gotsu/',
  },
  '5842041f4e65fad6a7708a1e': {
    legacySpotId: 5189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/bethany-beach/',
  },
  '5842041f4e65fad6a7708f60': {
    legacySpotId: 47060,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/mirante-da-sirena/',
  },
  '5842041f4e65fad6a7708f61': {
    legacySpotId: 46978,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/sergipe/aruana/',
  },
  '5842041f4e65fad6a7708f62': {
    legacySpotId: 47144,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/pontal/',
  },
  '5842041f4e65fad6a7708f63': {
    legacySpotId: 47062,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/garca-torta/',
  },
  '5842041f4e65fad6a7708f64': {
    legacySpotId: 47156,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/corals/',
  },
  '5842041f4e65fad6a7708f65': {
    legacySpotId: 47147,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/boca-da-barra/',
  },
  '5842041f4e65fad6a7708f66': {
    legacySpotId: 47158,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/tiririca/',
  },
  '5842041f4e65fad6a7708f67': {
    legacySpotId: 47159,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/prainha/',
  },
  '5842041f4e65fad6a7708f68': {
    legacySpotId: 47161,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/praia-de-jatiuca/',
  },
  '5842041f4e65fad6a7708f69': {
    legacySpotId: 47163,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/pedra-virada/',
  },
  '584204204e65fad6a770942e': {
    legacySpotId: 68000,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/moona-creek/',
  },
  '584204204e65fad6a770942b': {
    legacySpotId: 67585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/penguin-head/',
  },
  '5842041f4e65fad6a7708f5a': {
    legacySpotId: 46975,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/farol-da-barra/',
  },
  '5842041f4e65fad6a7708f5b': {
    legacySpotId: 46972,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/sergipe/pirambu/',
  },
  '5842041f4e65fad6a7708f5c': {
    legacySpotId: 46977,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/sergipe/hawaiizinho/',
  },
  '5842041f4e65fad6a7708f5d': {
    legacySpotId: 46980,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/sergipe/mosqueiro/',
  },
  '5842041f4e65fad6a7708f5e': {
    legacySpotId: 46979,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/espanhol/',
  },
  '5842041f4e65fad6a7708f5f': {
    legacySpotId: 47055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/japaratinga/',
  },
  '584204204e65fad6a7709434': {
    legacySpotId: 68007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/narrawallee/',
  },
  '584204204e65fad6a7709436': {
    legacySpotId: 68425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/south-texas/south-padre-island/',
  },
  '584204214e65fad6a7709c76': {
    legacySpotId: 128059,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/central-peru/puemape/',
  },
  '5842041f4e65fad6a7708f70': {
    legacySpotId: 47173,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/norte/',
  },
  '5842041f4e65fad6a7708f71': {
    legacySpotId: 47174,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/praia-do-sobral/',
  },
  '5842041f4e65fad6a7708f72': {
    legacySpotId: 47265,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/salgema/',
  },
  '5842041f4e65fad6a7708f74': {
    legacySpotId: 47276,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/sul/',
  },
  '5842041f4e65fad6a7708f75': {
    legacySpotId: 47275,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/pedra-da-cachorra/',
  },
  '5842041f4e65fad6a7708f76': {
    legacySpotId: 47277,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/milionarios/',
  },
  '5842041f4e65fad6a7708f77': {
    legacySpotId: 47278,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/jardim-atlantico/',
  },
  '5842041f4e65fad6a7708f78': {
    legacySpotId: 47391,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/praia-do-meio/',
  },
  '5842041f4e65fad6a7708f79': {
    legacySpotId: 47393,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/jacarecica/',
  },
  '5842041f4e65fad6a7708f6a': {
    legacySpotId: 47162,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/sao-jose/',
  },
  '5842041f4e65fad6a7708f6b': {
    legacySpotId: 47165,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/jeribucacu/',
  },
  '5842041f4e65fad6a7708f6c': {
    legacySpotId: 47166,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/engenhoca/',
  },
  '5842041f4e65fad6a7708f6d': {
    legacySpotId: 47169,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/itacarezinho/',
  },
  '5842041f4e65fad6a7708f6e': {
    legacySpotId: 47167,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/pico-da-barra--brazil/',
  },
  '5842041f4e65fad6a7708f6f': {
    legacySpotId: 47172,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/havaizinho/',
  },
  '5842041f4e65fad6a7708f80': {
    legacySpotId: 47266,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/praia-do-pontal/',
  },
  '5842041f4e65fad6a7708f81': {
    legacySpotId: 47400,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/pontal-do-coruripe/',
  },
  '5842041f4e65fad6a7708f82': {
    legacySpotId: 47264,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/pier-do-emissario/',
  },
  '5842041f4e65fad6a7708f83': {
    legacySpotId: 47406,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/pernambuco/gaibu/',
  },
  '5842041f4e65fad6a7708f84': {
    legacySpotId: 47408,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/pernambuco/cupe/',
  },
  '5842041f4e65fad6a7708f85': {
    legacySpotId: 47409,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/pernambuco/porto-de-galinhas/',
  },
  '5842041f4e65fad6a7708f86': {
    legacySpotId: 47402,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/pontal-de-peba/',
  },
  '5842041f4e65fad6a7708f87': {
    legacySpotId: 47412,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/pernambuco/impossivel/',
  },
  '5842041f4e65fad6a7708f88': {
    legacySpotId: 47512,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/touros-area/',
  },
  '5842041f4e65fad6a7708f89': {
    legacySpotId: 47513,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/cocoa-das-lavadeiras/',
  },
  '584204204e65fad6a770953d': {
    legacySpotId: 72902,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/rondeau/',
  },
  '584204204e65fad6a77092ad': {
    legacySpotId: 61186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/king-beach/',
  },
  '584204204e65fad6a770944a': {
    legacySpotId: 68741,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/congo/',
  },
  '5842041f4e65fad6a7708f7a': {
    legacySpotId: 47394,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/paraiba/cabedelo/',
  },
  '5842041f4e65fad6a7708f7b': {
    legacySpotId: 47396,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/paraiba/mar-dos-macacos/',
  },
  '5842041f4e65fad6a7708f7c': {
    legacySpotId: 47397,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/poxim/',
  },
  '5842041f4e65fad6a7708f7d': {
    legacySpotId: 47398,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/paraiba/bessa/',
  },
  '5842041f4e65fad6a7708f7e': {
    legacySpotId: 47399,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/forjos/',
  },
  '5842041f4e65fad6a7708f7f': {
    legacySpotId: 47268,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/praia-do-frances/',
  },
  '584204204e65fad6a770944c': {
    legacySpotId: 68661,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/barlings-beach/',
  },
  '584204204e65fad6a7709981': {
    legacySpotId: 95460,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/jennette-s-pier-ss/',
  },
  '584204204e65fad6a7709456': {
    legacySpotId: 68767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/picnic-point/',
  },
  '584204204e65fad6a7709455': {
    legacySpotId: 68769,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/tathra-beach/',
  },
  '584204204e65fad6a7709452': {
    legacySpotId: 68763,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/tilba-tilba/',
  },
  '5842041f4e65fad6a7708f90': {
    legacySpotId: 47584,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/cotovelo/',
  },
  '5842041f4e65fad6a7708f91': {
    legacySpotId: 47585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/pirangi-reef/',
  },
  '5842041f4e65fad6a7708f92': {
    legacySpotId: 47586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/xoxoline/',
  },
  '5842041f4e65fad6a7708f93': {
    legacySpotId: 47588,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/tabatinga/',
  },
  '5842041f4e65fad6a7708f95': {
    legacySpotId: 47589,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/tibau-do-sul/',
  },
  '5842041f4e65fad6a7708f96': {
    legacySpotId: 47643,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/pipe/',
  },
  '5842041f4e65fad6a7708f98': {
    legacySpotId: 47645,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/avalanche/',
  },
  '5842041f4e65fad6a7708f99': {
    legacySpotId: 47644,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/clubhouse/',
  },
  '5842041f4e65fad6a7708cb0': {
    legacySpotId: 6946,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/wales/wales/aberavon/',
  },
  '584204204e65fad6a77092c1': {
    legacySpotId: 61372,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/flinders-beach/',
  },
  '5842041f4e65fad6a7708f8a': {
    legacySpotId: 47514,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/urca-da-concei-ccedil--atilde-o/',
  },
  '5842041f4e65fad6a7708f8b': {
    legacySpotId: 47515,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/genipabu/',
  },
  '5842041f4e65fad6a7708f8c': {
    legacySpotId: 47580,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/praia-dos-artistas/',
  },
  '5842041f4e65fad6a7708f8d': {
    legacySpotId: 47581,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/areia-preta/',
  },
  '5842041f4e65fad6a7708f8e': {
    legacySpotId: 47582,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/miami/',
  },
  '5842041f4e65fad6a7708f8f': {
    legacySpotId: 47583,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/ponta-negra/',
  },
  '584204204e65fad6a7709446': {
    legacySpotId: 68658,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/malua/',
  },
  '584204204e65fad6a7709447': {
    legacySpotId: 68659,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/rosedale/',
  },
  '5842041f4e65fad6a7708df4': {
    legacySpotId: 10833,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/velzyland/',
  },
  '5842041f4e65fad6a77088a0': {
    legacySpotId: 4772,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/beacons/',
  },
  '584204204e65fad6a7709400': {
    legacySpotId: 67092,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/garie/',
  },
  '5842041f4e65fad6a77088a2': {
    legacySpotId: 4760,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/diamond-head/',
  },
  '5842041f4e65fad6a77088a3': {
    legacySpotId: 4770,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/moonlight-beach/',
  },
  '584204204e65fad6a7709403': {
    legacySpotId: 67093,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/era-north-south/',
  },
  '5842041f4e65fad6a77088a5': {
    legacySpotId: 4773,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/ponto-jetties/',
  },
  '5842041f4e65fad6a77088a6': {
    legacySpotId: 4775,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/terra-mar-point/',
  },
  '584204204e65fad6a7709406': {
    legacySpotId: 67094,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/burning-palms/',
  },
  '5842041f4e65fad6a7708df8': {
    legacySpotId: 10835,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/mokuleia/',
  },
  '584204204e65fad6a7709408': {
    legacySpotId: 67099,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/coledale/',
  },
  '584204204e65fad6a7709409': {
    legacySpotId: 67100,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/austinmer/',
  },
  '5842041f4e65fad6a7708f9a': {
    legacySpotId: 47647,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/millers/',
  },
  '5842041f4e65fad6a7708f9b': {
    legacySpotId: 47648,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/shark-rock-pier/',
  },
  '5842041f4e65fad6a7708f9c': {
    legacySpotId: 47651,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/denvils/',
  },
  '5842041f4e65fad6a7708f9d': {
    legacySpotId: 47650,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/humewood-pier/',
  },
  '5842041f4e65fad6a7708f9e': {
    legacySpotId: 47652,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/fence/',
  },
  '5842041f4e65fad6a7708f9f': {
    legacySpotId: 47649,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/port-elizabeth/baked-beans/',
  },
  '584204204e65fad6a770997f': {
    legacySpotId: 94737,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/crystal-coast/bogue-inlet-pier/',
  },
  '584204204e65fad6a770997e': {
    legacySpotId: 94638,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/bahamas/north-bahamas/rocky-bay/',
  },
  '5842041f4e65fad6a77088b0': {
    legacySpotId: 4785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/del-mar-rivermouth/',
  },
  '5842041f4e65fad6a77088b1': {
    legacySpotId: 4786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/cardiff/',
  },
  '5842041f4e65fad6a7708ca5': {
    legacySpotId: 6924,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/bude/',
  },
  '5842041f4e65fad6a77088b3': {
    legacySpotId: 4787,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/seaside-reef/',
  },
  '5842041f4e65fad6a77088b4': {
    legacySpotId: 4789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/swami-s/',
  },
  '5842041f4e65fad6a77088b5': {
    legacySpotId: 4790,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/pipes/',
  },
  '584204204e65fad6a7709415': {
    legacySpotId: 67113,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/entrance-bar/',
  },
  '5842041f4e65fad6a77088b7': {
    legacySpotId: 4792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/d-street/',
  },
  '5842041f4e65fad6a77088b8': {
    legacySpotId: 4791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/san-elijo-state-beach/',
  },
  '584204204e65fad6a7709418': {
    legacySpotId: 67117,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/the-pool/',
  },
  '584204204e65fad6a7709419': {
    legacySpotId: 67116,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/cowries/',
  },
  '584204204e65fad6a770943c': {
    legacySpotId: 68648,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/north-tabourie/',
  },
  '5842041f4e65fad6a7708ca7': {
    legacySpotId: 6926,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/fistral-beach/',
  },
  '584204214e65fad6a7709ccc': {
    legacySpotId: 130096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/grenada/grenada/cherry-hill/',
  },
  '584204204e65fad6a770940a': {
    legacySpotId: 67104,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/woonona/',
  },
  '584204204e65fad6a770940b': {
    legacySpotId: 67101,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/thirroul/',
  },
  '584204204e65fad6a770940c': {
    legacySpotId: 67102,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/peggys-waniora-point/',
  },
  '5842041f4e65fad6a77088ae': {
    legacySpotId: 4784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/del-mar-beachbreak/',
  },
  '5842041f4e65fad6a77088af': {
    legacySpotId: 4783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/del-mar/',
  },
  '584204204e65fad6a770940f': {
    legacySpotId: 67107,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/towradgi/',
  },
  '584204204e65fad6a7709543': {
    legacySpotId: 72907,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/lake-erie-beach--port-dover/',
  },
  '584204204e65fad6a7709420': {
    legacySpotId: 67360,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/malibu/',
  },
  '5842041f4e65fad6a77088c2': {
    legacySpotId: 4803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/pb-point/',
  },
  '584204204e65fad6a7709422': {
    legacySpotId: 67576,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/mystics/',
  },
  '5842041f4e65fad6a77088c4': {
    legacySpotId: 4804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/old-man-s-at-tourmaline/',
  },
  '584204204e65fad6a7709424': {
    legacySpotId: 67578,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/jones-beach/',
  },
  '584204204e65fad6a7709425': {
    legacySpotId: 67579,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/boneyards/',
  },
  '584204204e65fad6a7709426': {
    legacySpotId: 67582,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/seven-mile-beach/',
  },
  '584204204e65fad6a7709427': {
    legacySpotId: 67584,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/crookhaven/',
  },
  '584204204e65fad6a7709428': {
    legacySpotId: 67580,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bombo-beach/',
  },
  '584204204e65fad6a7709429': {
    legacySpotId: 67581,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/black-point/',
  },
  '584204204e65fad6a770941a': {
    legacySpotId: 67118,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/shellharbour-cove/',
  },
  '584204204e65fad6a770941b': {
    legacySpotId: 67120,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/redsands/',
  },
  '584204204e65fad6a770941c': {
    legacySpotId: 67119,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/the-shallows/',
  },
  '584204204e65fad6a770941e': {
    legacySpotId: 67161,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/puerto-escondido-report-photos/',
  },
  '584204204e65fad6a7709430': {
    legacySpotId: 68003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/cave-beach/',
  },
  '584204204e65fad6a7709431': {
    legacySpotId: 68005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bendalong-boat-ramp/',
  },
  '584204204e65fad6a7709432': {
    legacySpotId: 68004,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/cudmirrah-reef/',
  },
  '584204204e65fad6a7709433': {
    legacySpotId: 68006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bendalong-beach/',
  },
  '5842041f4e65fad6a77088d5': {
    legacySpotId: 4849,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/strands/',
  },
  '584204204e65fad6a7709435': {
    legacySpotId: 68366,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/oceanside-pier--southside/',
  },
  '5842041f4e65fad6a77088d7': {
    legacySpotId: 4848,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/doheny-state-beach/',
  },
  '584204204e65fad6a7709437': {
    legacySpotId: 68643,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/mollymook-beach/',
  },
  '584204204e65fad6a7709439': {
    legacySpotId: 68644,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/rennies-beach/',
  },
  '584204214e65fad6a7709bba': {
    legacySpotId: 122564,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/british-columbia/vancouver-island/sombrio/',
  },
  '5842041f4e65fad6a77088cb': {
    legacySpotId: 4814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/tijuana-slough/',
  },
  '5842041f4e65fad6a77088cc': {
    legacySpotId: 4812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/la-jolla-shores/',
  },
  '584204204e65fad6a770942c': {
    legacySpotId: 67587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/callala/',
  },
  '5842041f4e65fad6a77088ce': {
    legacySpotId: 4816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/coronado-beach/',
  },
  '5842041f4e65fad6a77088cf': {
    legacySpotId: 4843,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/san-clemente-state-beach/',
  },
  '584204204e65fad6a770942f': {
    legacySpotId: 68002,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/steamers-beach/',
  },
  '584204204e65fad6a7709440': {
    legacySpotId: 68652,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bawley-point/',
  },
  '584204204e65fad6a7709441': {
    legacySpotId: 68651,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/guillotines/',
  },
  '5842041f4e65fad6a77088e3': {
    legacySpotId: 4860,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/rockpile/',
  },
  '5842041f4e65fad6a77088e4': {
    legacySpotId: 4865,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/south-side-13th-street/',
  },
  '584204204e65fad6a7709444': {
    legacySpotId: 68655,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/durras-beach/',
  },
  '584204204e65fad6a7709445': {
    legacySpotId: 68657,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/mill-point/',
  },
  '5842041f4e65fad6a77088e7': {
    legacySpotId: 4867,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/surfside/',
  },
  '5842041f4e65fad6a77088e8': {
    legacySpotId: 4868,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/bolsa-chica-state-beach-south/',
  },
  '584204204e65fad6a7709448': {
    legacySpotId: 68660,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/tomakin-rivermouth/',
  },
  '584204204e65fad6a7709449': {
    legacySpotId: 68667,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/broulee-beach/',
  },
  '584204204e65fad6a770943a': {
    legacySpotId: 68646,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/dolphin-point/',
  },
  '5842041f4e65fad6a77088dc': {
    legacySpotId: 4855,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/aliso-creek/',
  },
  '5842041f4e65fad6a77088dd': {
    legacySpotId: 4856,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/brooks-street/',
  },
  '5842041f4e65fad6a77088de': {
    legacySpotId: 4857,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/thalia-street/',
  },
  '584204204e65fad6a770943e': {
    legacySpotId: 68649,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/dum-dums/',
  },
  '584204204e65fad6a770943f': {
    legacySpotId: 68650,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/termeil/',
  },
  '584204204e65fad6a7709450': {
    legacySpotId: 68758,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/dalmeny/',
  },
  '5842041f4e65fad6a77088f2': {
    legacySpotId: 4877,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/newport-point/',
  },
  '5842041f4e65fad6a77088f3': {
    legacySpotId: 4879,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/corona-del-mar/',
  },
  '584204204e65fad6a7709453': {
    legacySpotId: 68766,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bunga-beach/',
  },
  '584204204e65fad6a7709454': {
    legacySpotId: 68765,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/camel-rock/',
  },
  '5842041f4e65fad6a77088f6': {
    legacySpotId: 4876,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/newport-jetties/',
  },
  '5842041f4e65fad6a77088f7': {
    legacySpotId: 4886,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/santa-monica-pier/',
  },
  '584204204e65fad6a7709457': {
    legacySpotId: 68770,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/short-point/',
  },
  '584204204e65fad6a7709458': {
    legacySpotId: 68774,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/aslings-beach/',
  },
  '584204204e65fad6a7709459': {
    legacySpotId: 68771,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/leonard-island/',
  },
  '5842041f4e65fad6a77088ea': {
    legacySpotId: 4870,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/goldenwest/',
  },
  '5842041f4e65fad6a77088eb': {
    legacySpotId: 4871,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/17th-street/',
  },
  '584204204e65fad6a770944b': {
    legacySpotId: 68666,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/broulee-island/',
  },
  '5842041f4e65fad6a77088ed': {
    legacySpotId: 4874,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/hb-pier--southside/',
  },
  '5842041f4e65fad6a77088ee': {
    legacySpotId: 4875,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/santa-ana-river-jetties/',
  },
  '584204204e65fad6a770944e': {
    legacySpotId: 68746,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/potato-point/',
  },
  '584204204e65fad6a770944f': {
    legacySpotId: 68760,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/kianga/',
  },
  '584204204e65fad6a77099a0': {
    legacySpotId: 108230,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/nauset-beach/',
  },
  '584204204e65fad6a77099a1': {
    legacySpotId: 108234,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/marconi-beach/',
  },
  '584204204e65fad6a77099a3': {
    legacySpotId: 108349,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/manapad--tamil-nadu/',
  },
  '584204204e65fad6a77099a4': {
    legacySpotId: 108348,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/mahabalipuram--tamil-nadu/',
  },
  '584204204e65fad6a77099a5': {
    legacySpotId: 108351,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/bekal-fort--kerala/',
  },
  '584204204e65fad6a77099a8': {
    legacySpotId: 108426,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-north/tiavea-bay/',
  },
  '584204204e65fad6a7709464': {
    legacySpotId: 69923,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/bath-house/',
  },
  '584204204e65fad6a7709466': {
    legacySpotId: 70177,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/perth/garden-island/',
  },
  '584204204e65fad6a7709468': {
    legacySpotId: 70180,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/red-gate/',
  },
  '584204204e65fad6a7709469': {
    legacySpotId: 70182,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/boranup/',
  },
  '5955788e31d48a00128db927': {
    legacySpotId: 146781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/manta-ray/',
  },
  '584204204e65fad6a770945a': {
    legacySpotId: 68776,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/saltwater/',
  },
  '584204204e65fad6a770945c': {
    legacySpotId: 68787,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/brandon-bay/',
  },
  '584204204e65fad6a770945d': {
    legacySpotId: 68789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/coopers-beach/',
  },
  '5842041f4e65fad6a77088ff': {
    legacySpotId: 4895,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/dockweiler-state-beach/',
  },
  '584204204e65fad6a770945f': {
    legacySpotId: 69159,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/el-salvador-rovercam/',
  },
  '584204204e65fad6a77099b0': {
    legacySpotId: 108437,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/salani-left/',
  },
  '584204204e65fad6a77099b1': {
    legacySpotId: 108440,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/coconuts/',
  },
  '584204204e65fad6a77099b2': {
    legacySpotId: 108443,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/kovalam--kerala/',
  },
  '584204204e65fad6a77099b3': {
    legacySpotId: 108492,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/covelong--chennai/',
  },
  '584204204e65fad6a77099b4': {
    legacySpotId: 108583,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/the-leap/',
  },
  '584204204e65fad6a77099b5': {
    legacySpotId: 108585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/salailua-left/',
  },
  '584204204e65fad6a77099b6': {
    legacySpotId: 108584,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/salailua-right/',
  },
  '584204204e65fad6a77099b7': {
    legacySpotId: 108586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/aganoa/',
  },
  '584204204e65fad6a77099b8': {
    legacySpotId: 108587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/salamumu/',
  },
  '584204204e65fad6a77099b9': {
    legacySpotId: 108592,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/aufaga-village/',
  },
  '584204204e65fad6a7709475': {
    legacySpotId: 70749,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/louro/',
  },
  '584204204e65fad6a7709476': {
    legacySpotId: 71424,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/aucanada/',
  },
  '584204204e65fad6a7709477': {
    legacySpotId: 71425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/cala-mesquida/',
  },
  '584204204e65fad6a7709478': {
    legacySpotId: 71428,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/cala-ratjada/',
  },
  '584204204e65fad6a7709479': {
    legacySpotId: 71426,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/cala-agulla/',
  },
  '584204204e65fad6a77099ab': {
    legacySpotId: 108434,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/mulki--karnataka/',
  },
  '584204204e65fad6a77099ac': {
    legacySpotId: 108431,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-north/laulii/',
  },
  '584204204e65fad6a77099ad': {
    legacySpotId: 108435,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/salani-right/',
  },
  '584204204e65fad6a77099ae': {
    legacySpotId: 108436,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/mangalore-jetty/',
  },
  '584204204e65fad6a77099af': {
    legacySpotId: 108438,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/boulders/',
  },
  '584204204e65fad6a770946b': {
    legacySpotId: 70183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/deepdene/',
  },
  '584204204e65fad6a770946c': {
    legacySpotId: 70184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/cyclops/',
  },
  '584204204e65fad6a77099c0': {
    legacySpotId: 109303,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-south-coast/freights-bay/',
  },
  '5842041f4e65fad6a7708ea1': {
    legacySpotId: 45310,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/boicucanga/',
  },
  '5842041f4e65fad6a7708ea2': {
    legacySpotId: 45311,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/canto-de-moreira/',
  },
  '5842041f4e65fad6a7708ea3': {
    legacySpotId: 45323,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/ponta-do-bonete/',
  },
  '5842041f4e65fad6a7708ea4': {
    legacySpotId: 45315,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/pauba/',
  },
  '5842041f4e65fad6a7708ea5': {
    legacySpotId: 45319,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-do-guaeca/',
  },
  '584204204e65fad6a77099c6': {
    legacySpotId: 108960,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/faja-da-areia/',
  },
  '5842041f4e65fad6a7708ea7': {
    legacySpotId: 45325,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-dos-castelhanos/',
  },
  '5842041f4e65fad6a7708ea8': {
    legacySpotId: 45423,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/sape/',
  },
  '5842041f4e65fad6a7708ea9': {
    legacySpotId: 45425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/toninhas/',
  },
  '584204204e65fad6a7709a09': {
    legacySpotId: 111361,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/mal-paso/',
  },
  '584204204e65fad6a7709486': {
    legacySpotId: 71447,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--mediterranean-/la-mata/',
  },
  '584204204e65fad6a7709487': {
    legacySpotId: 71448,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/playa-serena/',
  },
  '584204204e65fad6a7709488': {
    legacySpotId: 71449,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/sitges/',
  },
  '584204204e65fad6a7709489': {
    legacySpotId: 71450,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/sagunto/',
  },
  '584204204e65fad6a77099ba': {
    legacySpotId: 108588,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/inner-siumu/',
  },
  '584204204e65fad6a77099bb': {
    legacySpotId: 108590,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/devils-island/',
  },
  '584204204e65fad6a77099bd': {
    legacySpotId: 108633,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/wct-events/ct-event-locations/billabong-rio-pro/barra-da-tijuca/',
  },
  '584204204e65fad6a77099be': {
    legacySpotId: 109182,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/san-sebastian/',
  },
  '584204204e65fad6a77099bf': {
    legacySpotId: 108589,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/samoa/samoa-south/outer-siumu/',
  },
  '584204204e65fad6a770947b': {
    legacySpotId: 71438,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/southwest-spain/barbate/',
  },
  '584204214e65fad6a7709cc6': {
    legacySpotId: 130592,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/bicentennial-beach-park/',
  },
  '584204204e65fad6a770947d': {
    legacySpotId: 71439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/barceloneta/',
  },
  '584204204e65fad6a770947f': {
    legacySpotId: 71440,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/ibiza/',
  },
  '5842041f4e65fad6a7708eb0': {
    legacySpotId: 45503,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/pereque-acu/',
  },
  '5842041f4e65fad6a7708eb1': {
    legacySpotId: 45504,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-vermelha-do-norte/',
  },
  '5842041f4e65fad6a7708eb2': {
    legacySpotId: 45505,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/itamambuca/',
  },
  '5842041f4e65fad6a7708eb3': {
    legacySpotId: 45506,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/rio-grande-do-sul/praia-do-cassino/',
  },
  '584204204e65fad6a77099d4': {
    legacySpotId: 109918,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/old-man-s-at-san-onofre/',
  },
  '5842041f4e65fad6a7708eb5': {
    legacySpotId: 45509,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/rio-grande-do-sul/tramandai-pier--northside/',
  },
  '5842041f4e65fad6a7708eb6': {
    legacySpotId: 45510,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/rio-grande-do-sul/praia-da-cal/',
  },
  '5842041f4e65fad6a7708eb7': {
    legacySpotId: 45512,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/rio-grande-do-sul/guarita/',
  },
  '5842041f4e65fad6a7708eb8': {
    legacySpotId: 45513,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-da-fazenda/',
  },
  '5842041f4e65fad6a7708eb9': {
    legacySpotId: 45514,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/rio-grande-do-sul/moles/',
  },
  '584204204e65fad6a7709a19': {
    legacySpotId: 111379,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/olon/',
  },
  '584204204e65fad6a7709496': {
    legacySpotId: 71464,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/platja-migjorn/',
  },
  '584204204e65fad6a7709497': {
    legacySpotId: 71465,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/playa-de-carchuna-calahonda/',
  },
  '584204204e65fad6a7709498': {
    legacySpotId: 71467,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/playa-de-levante/',
  },
  '584204204e65fad6a7709499': {
    legacySpotId: 71468,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/platja-de-xeraco/',
  },
  '5842041f4e65fad6a7708eaa': {
    legacySpotId: 45424,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-dura/',
  },
  '584204204e65fad6a7709a0a': {
    legacySpotId: 111364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/punta-carnero/',
  },
  '5842041f4e65fad6a7708eac': {
    legacySpotId: 45427,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-grande/',
  },
  '5842041f4e65fad6a7708ead': {
    legacySpotId: 45428,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/ponta-do-baguari/',
  },
  '584204204e65fad6a7709a0d': {
    legacySpotId: 111368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/la-chocolatera/',
  },
  '5842041f4e65fad6a7708eaf': {
    legacySpotId: 45430,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/vermelha-do-sul/',
  },
  '584204204e65fad6a7709a0f': {
    legacySpotId: 111369,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/playero-de-miramar/',
  },
  '584204204e65fad6a770948c': {
    legacySpotId: 71457,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/cala-nova/',
  },
  '584204204e65fad6a770948d': {
    legacySpotId: 71456,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/can-pujols/',
  },
  '584204204e65fad6a770948e': {
    legacySpotId: 71454,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/los-genoveses/',
  },
  '584204204e65fad6a770948f': {
    legacySpotId: 71459,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/cullera-jucar/',
  },
  '5842041f4e65fad6a7708ec0': {
    legacySpotId: 45603,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/barra-de-guaratiba/',
  },
  '5842041f4e65fad6a7708ec1': {
    legacySpotId: 45606,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-do-meio/',
  },
  '584204204e65fad6a7709a21': {
    legacySpotId: 111388,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/canoa/',
  },
  '584204204e65fad6a77099e3': {
    legacySpotId: 110428,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/agua-de-alto/',
  },
  '5842041f4e65fad6a7708ec4': {
    legacySpotId: 45609,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/macumba-curvao/',
  },
  '5842041f4e65fad6a7708ec5': {
    legacySpotId: 45610,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/laguna/',
  },
  '5842041f4e65fad6a7708ec6': {
    legacySpotId: 45611,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/itapiruba/',
  },
  '5842041f4e65fad6a7708ec7': {
    legacySpotId: 45695,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/gamboa/',
  },
  '5842041f4e65fad6a7708ec8': {
    legacySpotId: 45696,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/glaucio-gil/',
  },
  '5842041f4e65fad6a7708ec9': {
    legacySpotId: 45616,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/rosa-norte/',
  },
  '584204204e65fad6a7709a29': {
    legacySpotId: 111398,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/la-loberia/',
  },
  '58bdebbc82d034001252e3d2': {
    legacySpotId: 144646,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/huntington-street-overview/',
  },
  '5842041f4e65fad6a7708eba': {
    legacySpotId: 45515,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/tereza/',
  },
  '5842041f4e65fad6a7708ebb': {
    legacySpotId: 45597,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/aventureiro/',
  },
  '5842041f4e65fad6a7708ebc': {
    legacySpotId: 45598,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-do-sul/',
  },
  '5842041f4e65fad6a7708ebd': {
    legacySpotId: 45599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-do-leste/',
  },
  '5842041f4e65fad6a7708ebe': {
    legacySpotId: 45600,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/lopes-mendes/',
  },
  '5842041f4e65fad6a7708ebf': {
    legacySpotId: 45601,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/restinga-a-marambaia/',
  },
  '584204204e65fad6a7709a1f': {
    legacySpotId: 111386,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/manta/',
  },
  '584204204e65fad6a770949c': {
    legacySpotId: 71470,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/cala-jondal/',
  },
  '584204204e65fad6a770949d': {
    legacySpotId: 71451,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/montgat/',
  },
  '584204204e65fad6a770949f': {
    legacySpotId: 71473,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/sant-tomas/',
  },
  '5842041f4e65fad6a7708ed0': {
    legacySpotId: 45705,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/praia-brava/',
  },
  '584204204e65fad6a77099f1': {
    legacySpotId: 110443,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/terreiro/',
  },
  '5842041f4e65fad6a7708ed2': {
    legacySpotId: 45707,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/joatinga/',
  },
  '5842041f4e65fad6a7708ed3': {
    legacySpotId: 45709,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/sao-conrado/',
  },
  '5842041f4e65fad6a7708ed4': {
    legacySpotId: 45713,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/ponta-das-canas/',
  },
  '5842041f4e65fad6a7708ed5': {
    legacySpotId: 45767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/quatro-ilhas/',
  },
  '5842041f4e65fad6a7708ed6': {
    legacySpotId: 45770,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/camboriu/',
  },
  '5842041f4e65fad6a7708ed7': {
    legacySpotId: 45772,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/navegantes/',
  },
  '5842041f4e65fad6a7708ed8': {
    legacySpotId: 45710,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/santinho/',
  },
  '5842041f4e65fad6a7708ed9': {
    legacySpotId: 45773,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/atalaia/',
  },
  '584204204e65fad6a7709a39': {
    legacySpotId: 111463,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/el-cantil/',
  },
  '584204204e65fad6a7709417': {
    legacySpotId: 67115,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/north-shellharbour-beach/',
  },
  '5842041f4e65fad6a7708eca': {
    legacySpotId: 45698,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/reserva/',
  },
  '5842041f4e65fad6a7708ecb': {
    legacySpotId: 45614,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/praia-do-porto-imbituba/',
  },
  '5842041f4e65fad6a7708ecc': {
    legacySpotId: 45700,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/ponta-da-pinheira/',
  },
  '584204204e65fad6a77099ed': {
    legacySpotId: 110438,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/ponta-negra/',
  },
  '5842041f4e65fad6a7708ece': {
    legacySpotId: 45703,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/arma--o/',
  },
  '5842041f4e65fad6a7708ecf': {
    legacySpotId: 45704,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/postinho/',
  },
  '584204204e65fad6a7709a2f': {
    legacySpotId: 111404,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/el-faro/',
  },
  '5842041f4e65fad6a7708ee0': {
    legacySpotId: 45782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-do-diabo/',
  },
  '5842041f4e65fad6a7708ee1': {
    legacySpotId: 45783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/prainha/',
  },
  '5842041f4e65fad6a7708ee2': {
    legacySpotId: 45784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/ponta-da-tartaruga/',
  },
  '5842041f4e65fad6a7708ee3': {
    legacySpotId: 45785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/canto-do-leme/',
  },
  '5842041f4e65fad6a7708ee4': {
    legacySpotId: 45786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/parana/guaratuba/',
  },
  '5842041f4e65fad6a7708ee5': {
    legacySpotId: 45831,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/parana/caioba/',
  },
  '584204204e65fad6a7709a45': {
    legacySpotId: 111475,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/el-muelle/',
  },
  '5842041f4e65fad6a7708ee7': {
    legacySpotId: 45832,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/parana/matinhos/',
  },
  '584204204e65fad6a7709a47': {
    legacySpotId: 111480,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/arrecifes/',
  },
  '5842041f4e65fad6a7708ee9': {
    legacySpotId: 45838,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/iguape/',
  },
  '584204204e65fad6a7709a49': {
    legacySpotId: 111481,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/los-naranjos/',
  },
  '584204204e65fad6a7709407': {
    legacySpotId: 67098,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/wombarra/',
  },
  '584204204e65fad6a7709a48': {
    legacySpotId: 111479,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/punta-roca/',
  },
  '584204204e65fad6a77096e0': {
    legacySpotId: 91520,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/north-japan/iwate/yoshihama/',
  },
  '5842041f4e65fad6a7708eda': {
    legacySpotId: 45774,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/sumidouro/',
  },
  '584204204e65fad6a77099fb': {
    legacySpotId: 110627,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/28th-st/',
  },
  '5842041f4e65fad6a7708edc': {
    legacySpotId: 45775,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/itagua-ccedil-u/',
  },
  '5842041f4e65fad6a7708edd': {
    legacySpotId: 45779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/canto-do-leblon/',
  },
  '5842041f4e65fad6a7708ede': {
    legacySpotId: 45780,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/praia-grande/',
  },
  '5842041f4e65fad6a7708edf': {
    legacySpotId: 45712,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/ingleses/',
  },
  '584204204e65fad6a7709a3f': {
    legacySpotId: 111469,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/el-valle/',
  },
  '5842041f4e65fad6a7708ef0': {
    legacySpotId: 46018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/laje-do-pao-de-acucar/',
  },
  '5842041f4e65fad6a7708ef1': {
    legacySpotId: 46020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/praia-do-fora/',
  },
  '5842041f4e65fad6a7708ef2': {
    legacySpotId: 46021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/pirantininga/',
  },
  '5842041f4e65fad6a7708ef3': {
    legacySpotId: 46024,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/itaipu/',
  },
  '5842041f4e65fad6a7708ef4': {
    legacySpotId: 46023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/camboinhas/',
  },
  '5842041f4e65fad6a7708ef5': {
    legacySpotId: 46022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/itaipu/',
  },
  '5842041f4e65fad6a7708ef6': {
    legacySpotId: 46025,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/porta-do-sol/',
  },
  '5842041f4e65fad6a7708ef7': {
    legacySpotId: 46026,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/itacoatiara/',
  },
  '5842041f4e65fad6a7708ef8': {
    legacySpotId: 46087,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/ilha-porchat/',
  },
  '5842041f4e65fad6a7708ef9': {
    legacySpotId: 46091,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/canal-one/',
  },
  '584204204e65fad6a7709a59': {
    legacySpotId: 111521,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/tulate/',
  },
  '5842041f4e65fad6a7708eea': {
    legacySpotId: 45837,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/parana/ilha-comprida/',
  },
  '5842041f4e65fad6a7708eeb': {
    legacySpotId: 46012,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-dos-pescadores/',
  },
  '5842041f4e65fad6a7708eec': {
    legacySpotId: 46013,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/itanhaem/',
  },
  '5842041f4e65fad6a7708eed': {
    legacySpotId: 45840,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/peruibe/',
  },
  '5842041f4e65fad6a7708eee': {
    legacySpotId: 46015,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-grande/',
  },
  '5842041f4e65fad6a7708eef': {
    legacySpotId: 46014,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/mongagua/',
  },
  '584204204e65fad6a7709a4f': {
    legacySpotId: 111511,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/las-lisas/',
  },
  '584204204e65fad6a7709a60': {
    legacySpotId: 111708,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/kata-noi-beach/',
  },
  '584204204e65fad6a7709a61': {
    legacySpotId: 111709,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/kata-beach/',
  },
  '584204204e65fad6a7709a62': {
    legacySpotId: 111712,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/patong-beach/',
  },
  '584204204e65fad6a7709a63': {
    legacySpotId: 111711,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/relax-beach/',
  },
  '584204204e65fad6a7709a64': {
    legacySpotId: 111713,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/kalim/',
  },
  '584204204e65fad6a7709a65': {
    legacySpotId: 111715,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/surin-beach/',
  },
  '584204204e65fad6a7709a66': {
    legacySpotId: 111714,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/kamala-beach/',
  },
  '584204204e65fad6a7709a67': {
    legacySpotId: 111716,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/pansea-beach/',
  },
  '584204204e65fad6a7709a68': {
    legacySpotId: 111717,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/bang-tao/',
  },
  '584204204e65fad6a7709a69': {
    legacySpotId: 111719,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/airport-reef/',
  },
  '584204204e65fad6a7709410': {
    legacySpotId: 67108,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/fairy-meadow/',
  },
  '5842041f4e65fad6a7708efa': {
    legacySpotId: 46089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/quebra-mar/',
  },
  '5842041f4e65fad6a7708efb': {
    legacySpotId: 46092,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/gonzaga/',
  },
  '5842041f4e65fad6a7708efc': {
    legacySpotId: 46094,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/itaipuacu/',
  },
  '5842041f4e65fad6a7708efd': {
    legacySpotId: 46095,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/embare/',
  },
  '5842041f4e65fad6a7708efe': {
    legacySpotId: 46096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/sao-paulo/praia-da-ponta-grossa/',
  },
  '5842041f4e65fad6a7708eff': {
    legacySpotId: 46100,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-sul/barra-de-marica/',
  },
  '584204204e65fad6a7709a5f': {
    legacySpotId: 111710,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/karon-beach/',
  },
  '584204204e65fad6a7709a70': {
    legacySpotId: 111725,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/ko-phayam/',
  },
  '584204204e65fad6a7709a71': {
    legacySpotId: 111726,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/gulf-of-thailand/koh-phangan/',
  },
  '584204204e65fad6a7709a72': {
    legacySpotId: 111728,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/gulf-of-thailand/rayong-mae-ramphung-beach/',
  },
  '584204204e65fad6a7709a73': {
    legacySpotId: 111797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/puri/',
  },
  '584204204e65fad6a7709a74': {
    legacySpotId: 111798,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/sandy-point/',
  },
  '584204204e65fad6a7709a75': {
    legacySpotId: 111799,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/chintapalli/',
  },
  '584204204e65fad6a7709a76': {
    legacySpotId: 111727,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/gulf-of-thailand/ko-samet/',
  },
  '584204204e65fad6a7709a77': {
    legacySpotId: 111800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/mukkam/',
  },
  '584204204e65fad6a7709a78': {
    legacySpotId: 111801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/chippada/',
  },
  '584204204e65fad6a7709a79': {
    legacySpotId: 111802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/mangamari/',
  },
  '584204204e65fad6a7709a6a': {
    legacySpotId: 111720,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/similan-islands/',
  },
  '584204204e65fad6a7709a6b': {
    legacySpotId: 111721,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/nang-thong-beach/',
  },
  '584204204e65fad6a7709a6c': {
    legacySpotId: 111718,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/nai-thon/',
  },
  '584204204e65fad6a7709a6d': {
    legacySpotId: 111723,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/bang-niang-rivermouth/',
  },
  '584204204e65fad6a7709a6e': {
    legacySpotId: 111722,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/khao-lak/',
  },
  '584204204e65fad6a7709a6f': {
    legacySpotId: 111724,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/cape-pakarang/',
  },
  '584204204e65fad6a77092ae': {
    legacySpotId: 61281,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/sunshine-coast/bulcock-beach/',
  },
  '584204204e65fad6a770956e': {
    legacySpotId: 72975,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/grand-marais-beach--minnesota/',
  },
  '584204204e65fad6a7709a80': {
    legacySpotId: 111809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/yarada/',
  },
  '584204204e65fad6a7709a83': {
    legacySpotId: 111812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/pondicherry/',
  },
  '584204204e65fad6a7709a84': {
    legacySpotId: 111814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/rameshwaram/',
  },
  '584204204e65fad6a7709a85': {
    legacySpotId: 111816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/tiruchendur/',
  },
  '584204204e65fad6a7709a87': {
    legacySpotId: 111818,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/vattikottai/',
  },
  '584204204e65fad6a7709a88': {
    legacySpotId: 111819,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/poovar/',
  },
  '584204204e65fad6a7709a7a': {
    legacySpotId: 111803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/rushikonda-beach/',
  },
  '584204204e65fad6a7709a7b': {
    legacySpotId: 111804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/jodugullapullam/',
  },
  '584204204e65fad6a7709a7c': {
    legacySpotId: 111805,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/hanuman-point/',
  },
  '584204204e65fad6a7709a7d': {
    legacySpotId: 111807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/park-hotel/',
  },
  '584204204e65fad6a7709a7f': {
    legacySpotId: 111808,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/east-india/rama-krishna-beach/',
  },
  '584204204e65fad6a7709a2a': {
    legacySpotId: 111399,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/tonga-reef/',
  },
  '584204204e65fad6a7709a91': {
    legacySpotId: 111831,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/kannur/',
  },
  '584204204e65fad6a7709a93': {
    legacySpotId: 111837,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/bhatkal/',
  },
  '584204204e65fad6a7709a94': {
    legacySpotId: 111835,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/kapu-beach-lighthouse/',
  },
  '584204204e65fad6a7709a95': {
    legacySpotId: 111836,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/maravanthe/',
  },
  '584204204e65fad6a7709a96': {
    legacySpotId: 111838,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/murdeshwara/',
  },
  '584204204e65fad6a7709a97': {
    legacySpotId: 111840,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/gokarna/',
  },
  '584204204e65fad6a7709a98': {
    legacySpotId: 111839,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/kumta/',
  },
  '584204204e65fad6a7709a99': {
    legacySpotId: 111841,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/benaulim-beach/',
  },
  '5842041f4e65fad6a7708804': {
    legacySpotId: 4186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/roads-end/',
  },
  '584204204e65fad6a770953f': {
    legacySpotId: 72904,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/port-bruce/',
  },
  '584204204e65fad6a7709a8a': {
    legacySpotId: 111821,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/hawa-s-beach/',
  },
  '584204204e65fad6a7709a15': {
    legacySpotId: 111376,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/montanita/',
  },
  '584204204e65fad6a7709a8c': {
    legacySpotId: 111825,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/varkala/',
  },
  '584204204e65fad6a7709a8d': {
    legacySpotId: 111827,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/southern-india/thotapalli/',
  },
  '5842041f4e65fad6a7708a1c': {
    legacySpotId: 5187,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/indian-river-inlet/',
  },
  '584204204e65fad6a7709a11': {
    legacySpotId: 111371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/chulluipe/',
  },
  '584204204e65fad6a77093a0': {
    legacySpotId: 64609,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/susan-gilmore-beach/',
  },
  '584204204e65fad6a77093a2': {
    legacySpotId: 66018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/merewether-beach/',
  },
  '584204204e65fad6a77093a3': {
    legacySpotId: 66017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/dixon-park/',
  },
  '584204204e65fad6a77093a4': {
    legacySpotId: 66019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/ladies/',
  },
  '584204204e65fad6a77093a5': {
    legacySpotId: 66021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/dudley-beach/',
  },
  '584204204e65fad6a77092c4': {
    legacySpotId: 61373,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/blow-holes/',
  },
  '584204204e65fad6a77093a7': {
    legacySpotId: 66027,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/swansea/',
  },
  '584204204e65fad6a77093a8': {
    legacySpotId: 66025,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/redhead/',
  },
  '584204204e65fad6a77093a9': {
    legacySpotId: 66028,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/crabbs/',
  },
  '584204204e65fad6a7709a16': {
    legacySpotId: 111377,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/la-rinconada/',
  },
  '584204204e65fad6a77092b0': {
    legacySpotId: 61325,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/lighthouse-bombie/',
  },
  '584204204e65fad6a7709a9a': {
    legacySpotId: 111842,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/betalbatim-beach/',
  },
  '584204204e65fad6a7709a9b': {
    legacySpotId: 111844,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/baga-beach/',
  },
  '584204204e65fad6a7709a9c': {
    legacySpotId: 111843,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/calangute/',
  },
  '584204204e65fad6a7709a9d': {
    legacySpotId: 111845,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/arambol/',
  },
  '584204204e65fad6a7709a9e': {
    legacySpotId: 111858,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/jennette-s-pier-ns/',
  },
  '584204204e65fad6a7709575': {
    legacySpotId: 72984,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/goderich-salt-mines/',
  },
  '584204204e65fad6a77093b0': {
    legacySpotId: 66068,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/soldiers-beach/',
  },
  '584204204e65fad6a77093b1': {
    legacySpotId: 66064,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/cabbage-tree/',
  },
  '584204204e65fad6a77093b2': {
    legacySpotId: 66069,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/blue-bay/',
  },
  '584204204e65fad6a77093b3': {
    legacySpotId: 66074,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/toowoon-bay/',
  },
  '584204204e65fad6a77093b4': {
    legacySpotId: 66075,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/shelly-beach/',
  },
  '584204204e65fad6a77093b5': {
    legacySpotId: 66096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/the-suck/',
  },
  '584204204e65fad6a77093b6': {
    legacySpotId: 66076,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/crackneck/',
  },
  '584204204e65fad6a77093b7': {
    legacySpotId: 66097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/foresters-lefts/',
  },
  '584204204e65fad6a77092b2': {
    legacySpotId: 61328,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/gnaraloo/',
  },
  '584204204e65fad6a77093b9': {
    legacySpotId: 66100,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/terrigal-point/',
  },
  '584204204e65fad6a77092ed': {
    legacySpotId: 61555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/north-brunswick-head/',
  },
  '584204204e65fad6a77092ec': {
    legacySpotId: 61556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/new-brighton-beach/',
  },
  '584204204e65fad6a77093aa': {
    legacySpotId: 66029,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/caves-beach/',
  },
  '584204204e65fad6a77093ab': {
    legacySpotId: 66052,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/frazer-park/',
  },
  '584204204e65fad6a77093ac': {
    legacySpotId: 66049,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/catherine-hill-bay/',
  },
  '584204204e65fad6a77093ad': {
    legacySpotId: 66026,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/blacksmiths/',
  },
  '584204204e65fad6a77093ae': {
    legacySpotId: 66065,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/norah-head/',
  },
  '584204204e65fad6a77093af': {
    legacySpotId: 66067,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/gravelly/',
  },
  '584204214e65fad6a7709c12': {
    legacySpotId: 126357,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/spanish-point-reefs/',
  },
  '584204204e65fad6a77093c0': {
    legacySpotId: 66110,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/macmasters-point/',
  },
  '584204204e65fad6a77093c1': {
    legacySpotId: 66111,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/little-beach/',
  },
  '584204204e65fad6a77093c2': {
    legacySpotId: 66109,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/copacabana-beach/',
  },
  '584204204e65fad6a77093c3': {
    legacySpotId: 66163,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/tallow-beach/',
  },
  '584204204e65fad6a77093c4': {
    legacySpotId: 66162,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/killcare/',
  },
  '584204204e65fad6a77093c5': {
    legacySpotId: 66171,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/bateau-bay-beach/',
  },
  '584204204e65fad6a77093c6': {
    legacySpotId: 66173,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/the-entrance/',
  },
  '584204204e65fad6a77093c7': {
    legacySpotId: 66160,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/box-head/',
  },
  '5842041f4e65fad6a77089d9': {
    legacySpotId: 5120,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/the-main-beach/',
  },
  '584204204e65fad6a77093c9': {
    legacySpotId: 66176,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/jd-reef/',
  },
  '584204214e65fad6a7709c06': {
    legacySpotId: 125987,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/evan-s-reef/',
  },
  '584204204e65fad6a77093ba': {
    legacySpotId: 66101,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/north-avoca/',
  },
  '584204204e65fad6a77092f1': {
    legacySpotId: 61558,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/belongil-beach/',
  },
  '584204204e65fad6a77092f2': {
    legacySpotId: 61562,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/wategoes/',
  },
  '584204204e65fad6a77092f3': {
    legacySpotId: 61566,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/north-broken-head/',
  },
  '584204204e65fad6a77093be': {
    legacySpotId: 66104,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/pines/',
  },
  '584204204e65fad6a77093bf': {
    legacySpotId: 66108,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/copacabana-point/',
  },
  '584204204e65fad6a77092dd': {
    legacySpotId: 61475,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/tweed-river/',
  },
  '584204214e65fad6a7709c07': {
    legacySpotId: 125989,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/grindavik-antenas/',
  },
  '584204204e65fad6a7709413': {
    legacySpotId: 67111,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/windang-beach/',
  },
  '584204204e65fad6a7709414': {
    legacySpotId: 67114,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/barrack-point/',
  },
  '584204204e65fad6a77093d0': {
    legacySpotId: 66483,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/bilgola-beach/',
  },
  '584204204e65fad6a77093d1': {
    legacySpotId: 66482,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/little-avalon-beach/',
  },
  '584204204e65fad6a77093d2': {
    legacySpotId: 66484,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/the-peak/',
  },
  '584204204e65fad6a77093d3': {
    legacySpotId: 66488,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/bungan/',
  },
  '584204204e65fad6a77093d4': {
    legacySpotId: 66161,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/umina/',
  },
  '584204204e65fad6a77092ea': {
    legacySpotId: 61554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/south-brunswick-head/',
  },
  '584204204e65fad6a77092eb': {
    legacySpotId: 61553,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/wooyung-beach/',
  },
  '584204204e65fad6a7709a1a': {
    legacySpotId: 111381,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/viejamar/',
  },
  '584204204e65fad6a77093d8': {
    legacySpotId: 66490,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/mona-vale/',
  },
  '584204204e65fad6a77093d9': {
    legacySpotId: 66492,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/south-narrabeen/',
  },
  '584204204e65fad6a770940d': {
    legacySpotId: 67103,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bulli/',
  },
  '584204204e65fad6a77099e1': {
    legacySpotId: 110423,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/monte-verde/',
  },
  '584204204e65fad6a77093ca': {
    legacySpotId: 66177,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/lakes-beach/',
  },
  '584204204e65fad6a77093cb': {
    legacySpotId: 66180,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/birdie-beach/',
  },
  '584204204e65fad6a77093cc': {
    legacySpotId: 66211,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/shell-beach/',
  },
  '584204204e65fad6a77093cd': {
    legacySpotId: 66480,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/north-palm-beach/',
  },
  '584204204e65fad6a77099f9': {
    legacySpotId: 110445,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/faja-do-belo/',
  },
  '584204204e65fad6a77099f8': {
    legacySpotId: 110448,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/esquerda-da-igreja/',
  },
  '584204204e65fad6a77099f7': {
    legacySpotId: 110441,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/contendas/',
  },
  '584204204e65fad6a77093e0': {
    legacySpotId: 66499,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/freshwater/',
  },
  '584204204e65fad6a77093e1': {
    legacySpotId: 66501,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/queenscliffe/',
  },
  '584204204e65fad6a77093e2': {
    legacySpotId: 66503,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/winki-pop-fairy-bower/',
  },
  '584204204e65fad6a77093e3': {
    legacySpotId: 66502,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/north-south-steyne/',
  },
  '584204204e65fad6a77092f4': {
    legacySpotId: 61565,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/suffolk-park/',
  },
  '584204214e65fad6a7709b82': {
    legacySpotId: 119394,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/axim/',
  },
  '584204214e65fad6a7709b83': {
    legacySpotId: 119397,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/busua-beach/',
  },
  '584204214e65fad6a7709b84': {
    legacySpotId: 119398,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/betty-s/',
  },
  '584204214e65fad6a7709b85': {
    legacySpotId: 119400,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/elmina/',
  },
  '584204214e65fad6a7709b86': {
    legacySpotId: 119403,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/fete/',
  },
  '584204214e65fad6a7709b87': {
    legacySpotId: 119401,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/university/',
  },
  '584204214e65fad6a7709b88': {
    legacySpotId: 119404,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/krokrobite/',
  },
  '584204214e65fad6a7709b89': {
    legacySpotId: 119405,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/labadi-beach/',
  },
  '584204204e65fad6a77092be': {
    legacySpotId: 61369,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/moreton-island--northwest-/',
  },
  '584204204e65fad6a77092bd': {
    legacySpotId: 61368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/turtles/',
  },
  '584204204e65fad6a77093da': {
    legacySpotId: 66493,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/collaroy/',
  },
  '584204204e65fad6a77093db': {
    legacySpotId: 66496,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/butter-box/',
  },
  '584204204e65fad6a77093dc': {
    legacySpotId: 66495,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/little-makaha/',
  },
  '584204204e65fad6a77093dd': {
    legacySpotId: 66498,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/german-bank/',
  },
  '584204204e65fad6a77092d0': {
    legacySpotId: 61426,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/surfers-paradise/',
  },
  '584204214e65fad6a7709b7c': {
    legacySpotId: 119353,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/buraco/',
  },
  '584204214e65fad6a7709b7d': {
    legacySpotId: 119358,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/sumbe/',
  },
  '584204214e65fad6a7709b7e': {
    legacySpotId: 119356,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/barra-da-cuanza/',
  },
  '584204204e65fad6a77092bb': {
    legacySpotId: 61351,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/western-victoria/cathedral-rock/',
  },
  '584204214e65fad6a7709c9a': {
    legacySpotId: 128974,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/latvia/latvia--baltic-sea-/pavilosta/',
  },
  '584204204e65fad6a77099f4': {
    legacySpotId: 110446,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/direita-do-passe/',
  },
  '584204204e65fad6a77093f0': {
    legacySpotId: 66768,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/clovelly-reef/',
  },
  '584204204e65fad6a77093f1': {
    legacySpotId: 66771,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/lurline-bay/',
  },
  '584204204e65fad6a77093f2': {
    legacySpotId: 66770,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/wedding-cake-island/',
  },
  '584204204e65fad6a77093f3': {
    legacySpotId: 66772,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/malabar/',
  },
  '584204204e65fad6a77093f4': {
    legacySpotId: 66774,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/cruwee-cove/',
  },
  '584204214e65fad6a7709b92': {
    legacySpotId: 119421,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/spencer-bay/',
  },
  '584204214e65fad6a7709b93': {
    legacySpotId: 119422,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/hottentot-bay/',
  },
  '584204204e65fad6a77093f7': {
    legacySpotId: 66778,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/ours/',
  },
  '584204204e65fad6a77093f8': {
    legacySpotId: 66779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/voodoo/',
  },
  '584204204e65fad6a77093f9': {
    legacySpotId: 66781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/green-hills/',
  },
  '584204214e65fad6a7709b97': {
    legacySpotId: 119466,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/liberia/liberia/robertsport/',
  },
  '584204214e65fad6a7709b98': {
    legacySpotId: 119467,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/liberia/liberia/mumba-point/',
  },
  '584204214e65fad6a7709b99': {
    legacySpotId: 119468,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/liberia/liberia/tico-s/',
  },
  '584204214e65fad6a7709c20': {
    legacySpotId: 126950,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/le-prado/',
  },
  '5842041f4e65fad6a7708a1a': {
    legacySpotId: 5185,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/assateague/',
  },
  '584204204e65fad6a77093eb': {
    legacySpotId: 66766,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/tamarama-beach/',
  },
  '584204204e65fad6a77093ec': {
    legacySpotId: 66764,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/mackenzies/',
  },
  '584204214e65fad6a7709b8a': {
    legacySpotId: 119406,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/pram-pram/',
  },
  '584204204e65fad6a77093ee': {
    legacySpotId: 66769,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/coogee-beach/',
  },
  '584204204e65fad6a77093ef': {
    legacySpotId: 66767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/bronte-beach/',
  },
  '584204214e65fad6a7709b8d': {
    legacySpotId: 119417,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/cape-cross/',
  },
  '584204214e65fad6a7709b8e': {
    legacySpotId: 119402,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/st--charles/',
  },
  '584204214e65fad6a7709b8f': {
    legacySpotId: 119418,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/vineta-point/',
  },
  '5842041f4e65fad6a7708a28': {
    legacySpotId: 5212,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/fishermans-island/',
  },
  '584204214e65fad6a7709ceb': {
    legacySpotId: 133984,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/backdoor-fixed/',
  },
  '584204204e65fad6a77092d7': {
    legacySpotId: 61469,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/elephant-rock/',
  },
  '584204204e65fad6a77092d6': {
    legacySpotId: 61431,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/palm-beach/',
  },
  '5842041f4e65fad6a7708a22': {
    legacySpotId: 5206,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/camp-pendleton/',
  },
  '584204214e65fad6a7709c22': {
    legacySpotId: 126960,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/galiote/',
  },
  '584204214e65fad6a7709cec': {
    legacySpotId: 134306,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-cornwall/north-fistral-beach/',
  },
  '584204204e65fad6a77092d2': {
    legacySpotId: 61427,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/northcliffe/',
  },
  '584204204e65fad6a77092da': {
    legacySpotId: 61471,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/tugun-beach/',
  },
  '584204214e65fad6a7709c0f': {
    legacySpotId: 126355,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/tullaghan-right/',
  },
  '584204204e65fad6a77092d5': {
    legacySpotId: 61429,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/mermaid-waters/',
  },
  '584204204e65fad6a77093fa': {
    legacySpotId: 66782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/elouera/',
  },
  '584204204e65fad6a77093fb': {
    legacySpotId: 66784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/north-cronulla-beach/',
  },
  '584204204e65fad6a77099f2': {
    legacySpotId: 110447,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/feiticeiras/',
  },
  '584204204e65fad6a77093fd': {
    legacySpotId: 66783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/the-alley/',
  },
  '584204204e65fad6a77093fe': {
    legacySpotId: 66785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/cronulla-point/',
  },
  '584204204e65fad6a77093ff': {
    legacySpotId: 66786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/shark-island/',
  },
  '584204214e65fad6a7709bcb': {
    legacySpotId: 125283,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/united-arab-emirates/dubai/sunset-beach/',
  },
  '584204204e65fad6a770942d': {
    legacySpotId: 68001,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/plantation-point/',
  },
  '584204214e65fad6a7709b9f': {
    legacySpotId: 119811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-los-angeles/malibu-first-point/',
  },
  '584204204e65fad6a77092e4': {
    legacySpotId: 61502,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/kingscliff/',
  },
  '584204204e65fad6a77092e3': {
    legacySpotId: 61500,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/cook-island-northside/',
  },
  '584204214e65fad6a7709c0e': {
    legacySpotId: 125996,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/eyvik/',
  },
  '584204204e65fad6a77092e5': {
    legacySpotId: 61503,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/salt-beach/',
  },
  '58bdd5eba7ce4b00126f7baf': {
    legacySpotId: 145754,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/north-end-72nd-st-/',
  },
  '584204214e65fad6a7709c0d': {
    legacySpotId: 125995,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/husavik/',
  },
  '584204204e65fad6a77092ce': {
    legacySpotId: 61391,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/main-beach/',
  },
  '584204204e65fad6a77092cb': {
    legacySpotId: 61379,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/australia-north-coast/steep-point/',
  },
  '584204214e65fad6a7709cf4': {
    legacySpotId: 134367,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/dorset/bournemouth/',
  },
  '584204204e65fad6a77092e0': {
    legacySpotId: 61498,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/fingal-head/',
  },
  '584204204e65fad6a77092e1': {
    legacySpotId: 61499,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/cook-island-southside/',
  },
  '584204204e65fad6a77092e2': {
    legacySpotId: 61501,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/dreamland/',
  },
  '584204204e65fad6a77092cc': {
    legacySpotId: 61381,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/the-spit/',
  },
  '584204204e65fad6a77092cd': {
    legacySpotId: 61371,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/moreton-island--southeast-/',
  },
  '58bdee240cec4200133464f1': {
    legacySpotId: 145499,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/bolsa-chica-state-beach-n-/',
  },
  '584204214e65fad6a7709cf0': {
    legacySpotId: 134569,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/waimea-bay---shorebreak/',
  },
  '584204214e65fad6a7709cf3': {
    legacySpotId: 135291,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/cromer/',
  },
  '584204204e65fad6a770976c': {
    legacySpotId: 91661,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/kamakomae/',
  },
  '58bdecc682d034001252e3d3': {
    legacySpotId: 144653,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/seaside-road--brigantine/',
  },
  '5842041f4e65fad6a7708cb6': {
    legacySpotId: 6953,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/jardim-do-mar/',
  },
  '5842041f4e65fad6a7708cb7': {
    legacySpotId: 6952,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/paul-do-mar/',
  },
  '5842041f4e65fad6a7708cb9': {
    legacySpotId: 6955,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/los-lobos/',
  },
  '584204204e65fad6a7709b0b': {
    legacySpotId: 117779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/albenga/',
  },
  '5842041f4e65fad6a7708cb5': {
    legacySpotId: 6951,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/ponta-pequena/',
  },
  '584204214e65fad6a7709c81': {
    legacySpotId: 128392,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--baltic-sea-/vik/',
  },
  '5842041f4e65fad6a7708cb8': {
    legacySpotId: 6957,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/lanzarote-canaries/el-quemao/',
  },
  '5842041f4e65fad6a7708caf': {
    legacySpotId: 6945,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/thurso-east/',
  },
  '5842041f4e65fad6a7708cc4': {
    legacySpotId: 6971,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/central-chile/pichilemu/',
  },
  '5842041f4e65fad6a7708cc7': {
    legacySpotId: 6985,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/el-gringo/',
  },
  '584204204e65fad6a770966b': {
    legacySpotId: 77018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/mangawhai-heads/',
  },
  '5842041f4e65fad6a7708cab': {
    legacySpotId: 6939,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/scotland/north-scotland/gills-bay/',
  },
  '584204204e65fad6a7709b0f': {
    legacySpotId: 117786,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/entella/',
  },
  '584204204e65fad6a7709b0e': {
    legacySpotId: 117787,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/moneglia/',
  },
  '584204204e65fad6a77095c1': {
    legacySpotId: 73982,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/denmark-baltic-sea/zealand/hav-stokken/',
  },
  '584204204e65fad6a7709525': {
    legacySpotId: 72879,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/presque-isle-state-park/',
  },
  '584204204e65fad6a7709526': {
    legacySpotId: 72872,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/wendt-beach/',
  },
  '584204204e65fad6a7709588': {
    legacySpotId: 73003,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/bayview-park/',
  },
  '584204204e65fad6a7709b1d': {
    legacySpotId: 117812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/secchetto/',
  },
  '5842041f4e65fad6a7708cbf': {
    legacySpotId: 6954,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/fuerteventura-canaries/the-bubble--hierro-right-/',
  },
  '584204204e65fad6a7709589': {
    legacySpotId: 73004,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/thompson-park/',
  },
  '584204204e65fad6a7709587': {
    legacySpotId: 73001,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/harrisville/',
  },
  '584204204e65fad6a770957c': {
    legacySpotId: 72991,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/lakeside-park/',
  },
  '5842041f4e65fad6a7708cf5': {
    legacySpotId: 7105,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/the-money-wave/',
  },
  '584204204e65fad6a7709b05': {
    legacySpotId: 117777,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/andora-liguria/',
  },
  '584204204e65fad6a7709b06': {
    legacySpotId: 117778,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/alassio/',
  },
  '584204204e65fad6a7709b03': {
    legacySpotId: 117775,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/sanremo/',
  },
  '5942969ce98ad90013191e1c': {
    legacySpotId: 147876,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-devon/croyde-bay/',
  },
  '584204204e65fad6a7709500': {
    legacySpotId: 72800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/promontory-point-57th-street-beach/',
  },
  '5842041f4e65fad6a77089a2': {
    legacySpotId: 5057,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/cayucos-pier/',
  },
  '584204204e65fad6a7709b04': {
    legacySpotId: 117776,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/bistrot/',
  },
  '5842041f4e65fad6a77089a4': {
    legacySpotId: 5060,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/cable-landing/',
  },
  '584204204e65fad6a7709504': {
    legacySpotId: 72803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/montrose-avenue/',
  },
  '5842041f4e65fad6a7708cf9': {
    legacySpotId: 7110,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/mysteries/',
  },
  '584204204e65fad6a7709b01': {
    legacySpotId: 117315,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/careneros/',
  },
  '5842041f4e65fad6a77089a9': {
    legacySpotId: 5051,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/san-simeon/',
  },
  '584204204e65fad6a7709509': {
    legacySpotId: 72806,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/dempster-street/',
  },
  '5842041f4e65fad6a7708ceb': {
    legacySpotId: 7019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/santa-catarina/joaquina/',
  },
  '5842041f4e65fad6a77089b0': {
    legacySpotId: 5069,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/brookings-jetty/',
  },
  '5842041f4e65fad6a77089b1': {
    legacySpotId: 5071,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/port-orford/',
  },
  '5842041f4e65fad6a77089b2': {
    legacySpotId: 5072,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/florence-south-jetty/',
  },
  '5842041f4e65fad6a77089b3': {
    legacySpotId: 5070,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/gold-beach-south-jetty/',
  },
  '5842041f4e65fad6a77089b4': {
    legacySpotId: 5074,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/lincoln-city/',
  },
  '5842041f4e65fad6a77089b5': {
    legacySpotId: 5073,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/newport/',
  },
  '5842041f4e65fad6a77089b6': {
    legacySpotId: 5076,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/rockaway-beach/',
  },
  '5842041f4e65fad6a77089b7': {
    legacySpotId: 5075,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/pacific-city/',
  },
  '5842041f4e65fad6a77089b8': {
    legacySpotId: 5077,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/cannon-beach/',
  },
  '584204204e65fad6a7709518': {
    legacySpotId: 72821,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/harrington-beach/',
  },
  '584204204e65fad6a7709519': {
    legacySpotId: 72826,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/algoma/',
  },
  '5842041f4e65fad6a77089ab': {
    legacySpotId: 5064,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/avila-beach/',
  },
  '5842041f4e65fad6a77089ac': {
    legacySpotId: 5065,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/pismo-beach-pier/',
  },
  '5842041f4e65fad6a77089ad': {
    legacySpotId: 5067,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/washington/westport-olympic-peninsula/',
  },
  '5842041f4e65fad6a77089ae': {
    legacySpotId: 5066,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/washington/long-beach-peninsula/',
  },
  '5842041f4e65fad6a77089af': {
    legacySpotId: 5068,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/washington/la-push/',
  },
  '584204204e65fad6a770950f': {
    legacySpotId: 72818,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/bradford-beach/',
  },
  '584204204e65fad6a770959c': {
    legacySpotId: 73023,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/hamlin-beach/',
  },
  '584204204e65fad6a770959b': {
    legacySpotId: 73024,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/wilson-tuscarora-state-park/',
  },
  '5842041f4e65fad6a7708cd3': {
    legacySpotId: 6981,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/chile/northern-chile/la-cupula/',
  },
  '584204214e65fad6a7709c69': {
    legacySpotId: 127810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/acropolis--rhodes-/',
  },
  '5842041f4e65fad6a77089c0': {
    legacySpotId: 5088,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/south-beach-ca/',
  },
  '5842041f4e65fad6a77089c1': {
    legacySpotId: 5090,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/marin-county/stinson-beach/',
  },
  '5842041f4e65fad6a77089c2': {
    legacySpotId: 5091,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/marin-county/bolinas-jetty/',
  },
  '5842041f4e65fad6a77089c3': {
    legacySpotId: 5093,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/marin-county/point-reyes--north-/',
  },
  '5842041f4e65fad6a7708a03': {
    legacySpotId: 5162,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/loch-arbor-and-deal/',
  },
  '5842041f4e65fad6a7708a04': {
    legacySpotId: 5163,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/monmouth/',
  },
  '5842041f4e65fad6a7708a05': {
    legacySpotId: 5164,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/sandy-hook/',
  },
  '5842041f4e65fad6a7708a06': {
    legacySpotId: 5170,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/crystals/',
  },
  '5842041f4e65fad6a77089c8': {
    legacySpotId: 5096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sonoma-county/salmon-creek/',
  },
  '5842041f4e65fad6a7708a08': {
    legacySpotId: 5166,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/margate-pier/',
  },
  '5842041f4e65fad6a7708a09': {
    legacySpotId: 5167,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/ventnor-pier/',
  },
  '584204204e65fad6a7709b25': {
    legacySpotId: 117821,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/southern-italy/marinaretti/',
  },
  '584204214e65fad6a7709c5a': {
    legacySpotId: 127796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--aegean-sea-/sikias-beach/',
  },
  '584204204e65fad6a7709775': {
    legacySpotId: 91670,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/hikichigawa-kako/',
  },
  '5842041f4e65fad6a77089ba': {
    legacySpotId: 5083,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/shelter-cove/',
  },
  '5842041f4e65fad6a77089bb': {
    legacySpotId: 5084,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/humboldt-harbor-entrance/',
  },
  '5842041f4e65fad6a77089bc': {
    legacySpotId: 5085,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/north-jetty/',
  },
  '5842041f4e65fad6a77089bd': {
    legacySpotId: 5086,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/westhaven/',
  },
  '5842041f4e65fad6a77089be': {
    legacySpotId: 5087,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/humboldt/patricks-point/',
  },
  '5842041f4e65fad6a77089bf': {
    legacySpotId: 5089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/marin-county/fort-cronkite-rodeo-beach/',
  },
  '584204204e65fad6a770951f': {
    legacySpotId: 72873,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/bennett-beach/',
  },
  '5842041f4e65fad6a77089d0': {
    legacySpotId: 5111,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/first-beach/',
  },
  '5842041f4e65fad6a7708a10': {
    legacySpotId: 5176,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/hudson-avenue--harvey-cedars/',
  },
  '5842041f4e65fad6a77089d2': {
    legacySpotId: 5115,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/cape-cod/cape-cod/',
  },
  '5842041f4e65fad6a7708a12': {
    legacySpotId: 5178,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/seaside-park/',
  },
  '5842041f4e65fad6a77089d4': {
    legacySpotId: 5112,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/second-beach/',
  },
  '5842041f4e65fad6a77089d5': {
    legacySpotId: 5116,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/fox-hill/',
  },
  '5842041f4e65fad6a77089d6': {
    legacySpotId: 5117,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/straw-s-point/',
  },
  '5842041f4e65fad6a7708a16': {
    legacySpotId: 5181,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/lavallette/',
  },
  '5842041f4e65fad6a7708a17': {
    legacySpotId: 5182,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/bay-head/',
  },
  '5842041f4e65fad6a7708a18': {
    legacySpotId: 5183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/jenkinson-s/',
  },
  '584204204e65fad6a7709539': {
    legacySpotId: 72898,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/sterling-state-park/',
  },
  '584204204e65fad6a7709423': {
    legacySpotId: 67577,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/rangoon/',
  },
  '5842041f4e65fad6a77089ca': {
    legacySpotId: 5105,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/ogunquit/',
  },
  '5842041f4e65fad6a7708a0a': {
    legacySpotId: 5169,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/states-avenue/',
  },
  '5842041f4e65fad6a7708a0b': {
    legacySpotId: 5171,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/brigantine-jetty/',
  },
  '5842041f4e65fad6a77089cd': {
    legacySpotId: 5108,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/monahan-s---narragansett/',
  },
  '5842041f4e65fad6a77089ce': {
    legacySpotId: 5110,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/ruggles---newport/',
  },
  '5842041f4e65fad6a77089cf': {
    legacySpotId: 5106,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/short-sands-beach/',
  },
  '5842041f4e65fad6a7708a0f': {
    legacySpotId: 5174,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/holyoke/',
  },
  '584204204e65fad6a770952f': {
    legacySpotId: 72888,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/edgewater-pinched-loaf/',
  },
  '584204204e65fad6a770952b': {
    legacySpotId: 72884,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/headlands-beach/',
  },
  '5842041f4e65fad6a77089e0': {
    legacySpotId: 5125,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/gooch-s-beach/',
  },
  '5842041f4e65fad6a7708a20': {
    legacySpotId: 5191,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/gordons-pond/',
  },
  '5842041f4e65fad6a77089e2': {
    legacySpotId: 5132,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/lido-beach/',
  },
  '5842041f4e65fad6a77089e3': {
    legacySpotId: 5129,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/long-sands-beach/',
  },
  '5842041f4e65fad6a77089e4': {
    legacySpotId: 5135,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/gilgo-beach/',
  },
  '5842041f4e65fad6a77089e5': {
    legacySpotId: 5136,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/robert-moses/',
  },
  '5842041f4e65fad6a77089e6': {
    legacySpotId: 5137,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/fire-island/',
  },
  '5842041f4e65fad6a7708a26': {
    legacySpotId: 5208,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/croatan-pendleton/',
  },
  '5842041f4e65fad6a7708a27': {
    legacySpotId: 5210,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/15th-street-pier/',
  },
  '5842041f4e65fad6a77089e9': {
    legacySpotId: 5131,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/the-wall--hampton-beach/',
  },
  '584204204e65fad6a7709549': {
    legacySpotId: 72911,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/nickel-beach/',
  },
  '584204204e65fad6a77095e3': {
    legacySpotId: 74032,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/callantsoog/',
  },
  '584204204e65fad6a7709b6d': {
    legacySpotId: 118602,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/lugar-de-baixo/',
  },
  '584204204e65fad6a7709774': {
    legacySpotId: 91669,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/dozomae/',
  },
  '584204204e65fad6a77094c9': {
    legacySpotId: 71793,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/miaoli/chunan/',
  },
  '5842041f4e65fad6a77089da': {
    legacySpotId: 5122,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/sawyer-s-beach/',
  },
  '5842041f4e65fad6a77089db': {
    legacySpotId: 5121,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/rye-on-the-rocks---5-0-s/',
  },
  '5842041f4e65fad6a7708a1b': {
    legacySpotId: 5186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/north-end-to-ocean-city-inlet/',
  },
  '5842041f4e65fad6a77089dd': {
    legacySpotId: 5124,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/wells-beach/',
  },
  '5842041f4e65fad6a7708a1d': {
    legacySpotId: 5188,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/fenwick-island/',
  },
  '5842041f4e65fad6a77089df': {
    legacySpotId: 5127,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/old-orchard-beach/',
  },
  '5842041f4e65fad6a7708a1f': {
    legacySpotId: 5190,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/dewey-beach/',
  },
  '584204204e65fad6a7709b42': {
    legacySpotId: 117875,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/morghella/',
  },
  '584204204e65fad6a7709b12': {
    legacySpotId: 117790,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/marinella-di-sarzana/',
  },
  '584204204e65fad6a770958b': {
    legacySpotId: 73006,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/p-h--hoeft-state-park/',
  },
  '584204204e65fad6a7709b3e': {
    legacySpotId: 117871,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/san-vito/',
  },
  '5842041f4e65fad6a770883a': {
    legacySpotId: 4249,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-san-diego/birdrock/',
  },
  '5842041f4e65fad6a77089f0': {
    legacySpotId: 5133,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/lincoln-blvd--fixed/',
  },
  '5842041f4e65fad6a77089f1': {
    legacySpotId: 5146,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/poles/',
  },
  '584204204e65fad6a7709551': {
    legacySpotId: 72933,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/lake-superior-campground-beach/',
  },
  '5842041f4e65fad6a77089f3': {
    legacySpotId: 5139,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/the-bowl/',
  },
  '5842041f4e65fad6a77089f4': {
    legacySpotId: 5147,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/main-beach/',
  },
  '5842041f4e65fad6a77089f5': {
    legacySpotId: 5150,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/laurelton-blvd-/',
  },
  '5842041f4e65fad6a77089f6': {
    legacySpotId: 5148,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/terrace/',
  },
  '5842041f4e65fad6a77089f7': {
    legacySpotId: 5149,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/k-road/',
  },
  '5842041f4e65fad6a77089f8': {
    legacySpotId: 5152,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/84th-street/',
  },
  '5842041f4e65fad6a7708a38': {
    legacySpotId: 5230,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/cape-hatteras-lighthouse/',
  },
  '584204204e65fad6a7709559': {
    legacySpotId: 72945,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/little-presque-isle-beach/',
  },
  '584204204e65fad6a7709b1f': {
    legacySpotId: 117820,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/pura-vida/',
  },
  '584204204e65fad6a7709522': {
    legacySpotId: 72876,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/gratiot-point/',
  },
  '5842041f4e65fad6a77089ea': {
    legacySpotId: 5140,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/flys/',
  },
  '5842041f4e65fad6a77089eb': {
    legacySpotId: 5141,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/trailer-park/',
  },
  '5842041f4e65fad6a77089ec': {
    legacySpotId: 5142,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/ditch-plains/',
  },
  '584204204e65fad6a770954c': {
    legacySpotId: 72925,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-on--superior-/pukaskwa-national-park/',
  },
  '5842041f4e65fad6a77089ee': {
    legacySpotId: 5144,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/west-end/',
  },
  '584204204e65fad6a770954e': {
    legacySpotId: 72926,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-on--superior-/lake-superior-park-gargantua/',
  },
  '584204204e65fad6a7709b2f': {
    legacySpotId: 117851,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/fiorenzuola/',
  },
  '584204204e65fad6a7709597': {
    legacySpotId: 73019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/webster-park/',
  },
  '584204204e65fad6a7709596': {
    legacySpotId: 73017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/fair-haven/',
  },
  '584204204e65fad6a770951e': {
    legacySpotId: 72871,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/hamburg-beach/',
  },
  '584204204e65fad6a7709b29': {
    legacySpotId: 117824,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/southern-italy/salerno-rivermouth/',
  },
  '5842041f4e65fad6a7708a40': {
    legacySpotId: 5236,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/nags-head-pier/',
  },
  '5842041f4e65fad6a7708a41': {
    legacySpotId: 5238,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/avalon-pier/',
  },
  '5842041f4e65fad6a7708a42': {
    legacySpotId: 5237,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/1st-st-/',
  },
  '5842041f4e65fad6a7708a43': {
    legacySpotId: 5239,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/old-station-laundromats/',
  },
  '5842041f4e65fad6a7708a44': {
    legacySpotId: 5240,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/kitty-hawk-pier/',
  },
  '5842041f4e65fad6a7708a45': {
    legacySpotId: 5241,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/duck-pier/',
  },
  '5842041f4e65fad6a7708a46': {
    legacySpotId: 5242,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/corolla/',
  },
  '5842041f4e65fad6a7708a47': {
    legacySpotId: 5243,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/brunswick/oak-island/',
  },
  '5842041f4e65fad6a7708a48': {
    legacySpotId: 5244,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/brunswick/south-brunswick-beaches/',
  },
  '5842041f4e65fad6a7708a49': {
    legacySpotId: 5247,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/carolina-beach-pier/',
  },
  '584204204e65fad6a7709537': {
    legacySpotId: 72897,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/toledo-beach/',
  },
  '584204204e65fad6a7709b68': {
    legacySpotId: 118597,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/porto-da-cruz/',
  },
  '584204204e65fad6a7709599': {
    legacySpotId: 73022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/ontario-beach/',
  },
  '5842041f4e65fad6a77089fa': {
    legacySpotId: 5155,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/sea-girt/',
  },
  '5842041f4e65fad6a7708a3a': {
    legacySpotId: 5232,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/waves-and-salvo/',
  },
  '5842041f4e65fad6a7708a3b': {
    legacySpotId: 5229,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/frisco-pier/',
  },
  '5842041f4e65fad6a77089fd': {
    legacySpotId: 5151,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/30th---39th-streets/',
  },
  '5842041f4e65fad6a77089fe': {
    legacySpotId: 5158,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/avon/',
  },
  '5842041f4e65fad6a77089ff': {
    legacySpotId: 5159,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/bradley-beach/',
  },
  '5842041f4e65fad6a7708a3f': {
    legacySpotId: 5235,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/pea-island/',
  },
  '584204204e65fad6a7709b1e': {
    legacySpotId: 117816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/margidore/',
  },
  '584204204e65fad6a770957e': {
    legacySpotId: 72995,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/port-crescent-state-park/',
  },
  '584204204e65fad6a770957d': {
    legacySpotId: 72993,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/lexington/',
  },
  '584204204e65fad6a7709b1a': {
    legacySpotId: 117809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/scaglieri/',
  },
  '584204204e65fad6a7709b1c': {
    legacySpotId: 117814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/lacona/',
  },
  '5842041f4e65fad6a7708a50': {
    legacySpotId: 5257,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/crystal-coast/cape-lookout-national-seashore/',
  },
  '5842041f4e65fad6a7708a51': {
    legacySpotId: 5259,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/jekyll-island/',
  },
  '5842041f4e65fad6a7708a52': {
    legacySpotId: 5261,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/harbor-marker-buoys/',
  },
  '5842041f4e65fad6a7708a53': {
    legacySpotId: 5260,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/sea-island/',
  },
  '5842041f4e65fad6a7708a54': {
    legacySpotId: 5262,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/st--simons-island/',
  },
  '5842041f4e65fad6a7708a55': {
    legacySpotId: 5264,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/wolf-island/',
  },
  '5842041f4e65fad6a7708a56': {
    legacySpotId: 5263,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/litttle-st--simons-island/',
  },
  '5842041f4e65fad6a7708a57': {
    legacySpotId: 5265,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/sapelo-island/',
  },
  '5842041f4e65fad6a7708a58': {
    legacySpotId: 5252,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/surf-city-pier/',
  },
  '5842041f4e65fad6a7708a59': {
    legacySpotId: 5266,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/cabretta-island/',
  },
  '584204204e65fad6a7709520': {
    legacySpotId: 72874,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/point-breeze/',
  },
  '584204204e65fad6a7709b6e': {
    legacySpotId: 119183,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/new-brunswick/mispec-beach/',
  },
  '584204204e65fad6a7709b33': {
    legacySpotId: 117854,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/la-nave/',
  },
  '584204204e65fad6a7709b34': {
    legacySpotId: 117856,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/sasso/',
  },
  '584204204e65fad6a7709b35': {
    legacySpotId: 117858,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/marina-palmense/',
  },
  '584204204e65fad6a770959e': {
    legacySpotId: 73027,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/port-dalhousie/',
  },
  '5842041f4e65fad6a7708a4a': {
    legacySpotId: 5245,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/fort-fisher/',
  },
  '5842041f4e65fad6a7708a4b': {
    legacySpotId: 5253,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/topsail-beach/',
  },
  '5842041f4e65fad6a7708a4c': {
    legacySpotId: 5255,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/crystal-coast/iron-steamer/',
  },
  '5842041f4e65fad6a7708a4d': {
    legacySpotId: 5254,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/crystal-coast/emerald-isle/',
  },
  '5842041f4e65fad6a7708a4e': {
    legacySpotId: 5256,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/crystal-coast/atlantic-beach/',
  },
  '5842041f4e65fad6a7708a4f': {
    legacySpotId: 5258,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/cumberland-island/',
  },
  '584204204e65fad6a7709743': {
    legacySpotId: 91620,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/onjyuku/',
  },
  '584204204e65fad6a7709b09': {
    legacySpotId: 117780,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/varazze/',
  },
  '5842041f4e65fad6a7708fa0': {
    legacySpotId: 47710,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/madeiro/',
  },
  '5842041f4e65fad6a7708fa1': {
    legacySpotId: 47668,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/oceanside--or/',
  },
  '5842041f4e65fad6a7708fa2': {
    legacySpotId: 47712,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/cachimbinha/',
  },
  '584204204e65fad6a7709583': {
    legacySpotId: 72998,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/bay-city-beach/',
  },
  '5842041f4e65fad6a7708fa4': {
    legacySpotId: 47719,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/praia-da-pipa/',
  },
  '5842041f4e65fad6a7708a60': {
    legacySpotId: 5273,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/south-tybee-sandbars/',
  },
  '5842041f4e65fad6a7708a61': {
    legacySpotId: 5276,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/sugar-shack/',
  },
  '5842041f4e65fad6a7708a62': {
    legacySpotId: 5274,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/18th-st--jetty/',
  },
  '5842041f4e65fad6a7708fa8': {
    legacySpotId: 47411,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/pernambuco/serrambi/',
  },
  '5842041f4e65fad6a7708fa9': {
    legacySpotId: 48226,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/afogados/',
  },
  '5842041f4e65fad6a7708a65': {
    legacySpotId: 5250,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/c-street/',
  },
  '5842041f4e65fad6a7708a66': {
    legacySpotId: 5277,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/dewees-island/',
  },
  '5842041f4e65fad6a7708a67': {
    legacySpotId: 5280,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/cherry-grove/',
  },
  '584204204e65fad6a770957a': {
    legacySpotId: 72987,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/the-pinery/',
  },
  '5842041f4e65fad6a7708a69': {
    legacySpotId: 5251,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/figure-eight-and-shell-islands/',
  },
  '584204204e65fad6a7709b08': {
    legacySpotId: 117783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/capo-marina/',
  },
  '584204204e65fad6a7709586': {
    legacySpotId: 73000,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/oscoda-beach-park/',
  },
  '5842041f4e65fad6a7708a5a': {
    legacySpotId: 5267,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/blackbeard-island/',
  },
  '5842041f4e65fad6a7708a5b': {
    legacySpotId: 5270,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/wassau-island-south-end/',
  },
  '5842041f4e65fad6a7708a5c': {
    legacySpotId: 5268,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/st--catherine-s-island/',
  },
  '5842041f4e65fad6a7708a5d': {
    legacySpotId: 5269,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/ossabaw-island/',
  },
  '5842041f4e65fad6a7708a5e': {
    legacySpotId: 5271,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/wassau-island-north-end/',
  },
  '5842041f4e65fad6a7708a5f': {
    legacySpotId: 5272,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/little-tybee-island/',
  },
  '584204204e65fad6a7709593': {
    legacySpotId: 73015,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/brennans-beach/',
  },
  '584204204e65fad6a770951a': {
    legacySpotId: 72828,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/garden-peninsula/',
  },
  '5842041f4e65fad6a7708fb0': {
    legacySpotId: 48361,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/mar-aberto/',
  },
  '5842041f4e65fad6a7708fb1': {
    legacySpotId: 47410,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/pernambuco/maracaipe/',
  },
  '5842041f4e65fad6a7708fb2': {
    legacySpotId: 47272,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/bahia/baduska/',
  },
  '5842041f4e65fad6a7708fb3': {
    legacySpotId: 47401,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/central-brazil/alagoas/mihai/',
  },
  '5842041f4e65fad6a7708fb4': {
    legacySpotId: 48364,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/farol/',
  },
  '5842041f4e65fad6a7708a70': {
    legacySpotId: 5288,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/cape-romain/',
  },
  '5842041f4e65fad6a7708a71': {
    legacySpotId: 5284,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/the-pier/',
  },
  '5842041f4e65fad6a7708a72': {
    legacySpotId: 5283,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/south-litchfield/',
  },
  '5842041f4e65fad6a7708a73': {
    legacySpotId: 5249,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-onslow---new-hanover/crystal-pier/',
  },
  '5842041f4e65fad6a7708a74': {
    legacySpotId: 5279,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/myrtle-beach/',
  },
  '5842041f4e65fad6a7708a75': {
    legacySpotId: 5295,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/10th-street/',
  },
  '5842041f4e65fad6a7708a76': {
    legacySpotId: 5287,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/south-island/',
  },
  '5842041f4e65fad6a7708a77': {
    legacySpotId: 5296,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/kiawah-island-seabrook-island/',
  },
  '5842041f4e65fad6a7708a78': {
    legacySpotId: 5297,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/edisto-beach/',
  },
  '5842041f4e65fad6a7708a79': {
    legacySpotId: 5298,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/hunting-island/',
  },
  '584204204e65fad6a7709b10': {
    legacySpotId: 117788,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/levanto/',
  },
  '584204204e65fad6a7709b15': {
    legacySpotId: 117794,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/tito-del-molo/',
  },
  '5842041f4e65fad6a7708faa': {
    legacySpotId: 48228,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/lajinha/',
  },
  '584204204e65fad6a7709b0a': {
    legacySpotId: 117782,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/multedo/',
  },
  '5842041f4e65fad6a7708fac': {
    legacySpotId: 48231,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/picao/',
  },
  '5842041f4e65fad6a7708fad': {
    legacySpotId: 48232,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/baia-formosa/',
  },
  '5842041f4e65fad6a7708fae': {
    legacySpotId: 48233,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/mar-da-frente/',
  },
  '5842041f4e65fad6a7708faf': {
    legacySpotId: 48360,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/pontal/',
  },
  '5842041f4e65fad6a7708a6b': {
    legacySpotId: 5281,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/north-end/',
  },
  '5842041f4e65fad6a7708a6c': {
    legacySpotId: 5285,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/the-south-end/',
  },
  '5842041f4e65fad6a7708a6d': {
    legacySpotId: 5286,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/north-island/',
  },
  '5842041f4e65fad6a7708a6e': {
    legacySpotId: 5289,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/bull-island/',
  },
  '5842041f4e65fad6a7708a6f': {
    legacySpotId: 5275,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/georgia/tybee-island-pier/',
  },
  '5842041f4e65fad6a7708fc0': {
    legacySpotId: 48537,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/icarai/',
  },
  '5842041f4e65fad6a7708fc1': {
    legacySpotId: 48538,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/praia-do-pecem/',
  },
  '5842041f4e65fad6a7708fc2': {
    legacySpotId: 48544,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/curral/',
  },
  '584204204e65fad6a7709b22': {
    legacySpotId: 117819,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/porto/',
  },
  '584204204e65fad6a7709b23': {
    legacySpotId: 117818,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/banzai/',
  },
  '584204204e65fad6a7709b24': {
    legacySpotId: 117823,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/southern-italy/sant-agostino/',
  },
  '5842041f4e65fad6a7708a81': {
    legacySpotId: 5304,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/crescent-beach/',
  },
  '5842041f4e65fad6a7708a82': {
    legacySpotId: 5290,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/wild-dunes/',
  },
  '584204204e65fad6a7709b27': {
    legacySpotId: 117843,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/lignano-pineta/',
  },
  '5842041f4e65fad6a7708a84': {
    legacySpotId: 5292,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/bert-s-bar/',
  },
  '5842041f4e65fad6a7708a85': {
    legacySpotId: 5293,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/north-washout--folly-beach/',
  },
  '5842041f4e65fad6a7708a86': {
    legacySpotId: 5291,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/the-pier-windjammer/',
  },
  '5842041f4e65fad6a7708a87': {
    legacySpotId: 5308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/anastasia-park--the-entrance-/',
  },
  '5842041f4e65fad6a7708a89': {
    legacySpotId: 5310,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/blowholes/',
  },
  '584204204e65fad6a7709b21': {
    legacySpotId: 117817,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/ansedonia/',
  },
  '584204204e65fad6a7709b26': {
    legacySpotId: 117822,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/southern-italy/saporetti/',
  },
  '5842041f4e65fad6a7708fba': {
    legacySpotId: 48365,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/rio-grande-do-norte/saji/',
  },
  '5842041f4e65fad6a7708fbb': {
    legacySpotId: 48502,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/ponte-metalica/',
  },
  '584204204e65fad6a7709b1b': {
    legacySpotId: 117813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/marina-di-campo/',
  },
  '5842041f4e65fad6a7708fbd': {
    legacySpotId: 48535,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/leste-oeste/',
  },
  '5842041f4e65fad6a7708fbe': {
    legacySpotId: 48540,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/tabinha/',
  },
  '5842041f4e65fad6a7708fbf': {
    legacySpotId: 48536,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/barra-do-ceara/',
  },
  '5842041f4e65fad6a7708a7b': {
    legacySpotId: 5301,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/south-jetty/',
  },
  '5842041f4e65fad6a7708a7c': {
    legacySpotId: 5299,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/fripp-island/',
  },
  '5842041f4e65fad6a7708a7d': {
    legacySpotId: 5300,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/hilton-head-island/',
  },
  '5842041f4e65fad6a7708a7e': {
    legacySpotId: 5303,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/matanzas-inlet/',
  },
  '584204204e65fad6a770958c': {
    legacySpotId: 73007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/mackinac-island/',
  },
  '584204204e65fad6a7709529': {
    legacySpotId: 72882,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/saybrook/',
  },
  '584204204e65fad6a7709b30': {
    legacySpotId: 117852,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/lido/',
  },
  '584204204e65fad6a7709b31': {
    legacySpotId: 117853,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/cesano/',
  },
  '584204204e65fad6a7709b32': {
    legacySpotId: 117855,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/portonovo/',
  },
  '584204204e65fad6a7709b20': {
    legacySpotId: 117810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/procchio/',
  },
  '5842041f4e65fad6a7708a90': {
    legacySpotId: 5321,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/talbot-island/',
  },
  '5842041f4e65fad6a7708a91': {
    legacySpotId: 5322,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/main-beach/',
  },
  '5842041f4e65fad6a7708a92': {
    legacySpotId: 5323,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/ft--clinch-state-park/',
  },
  '5842041f4e65fad6a7708a93': {
    legacySpotId: 5324,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/amelia-pier/',
  },
  '584204204e65fad6a7709b18': {
    legacySpotId: 117806,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/central-italy/garagola/',
  },
  '5842041f4e65fad6a7708a95': {
    legacySpotId: 5326,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/bethune-beach/',
  },
  '5842041f4e65fad6a7708a96': {
    legacySpotId: 5327,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/flagler-avenue/',
  },
  '5842041f4e65fad6a7708a98': {
    legacySpotId: 5316,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/atlantic-beach/',
  },
  '5842041f4e65fad6a7708a99': {
    legacySpotId: 5309,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/middles/',
  },
  '584204204e65fad6a770958a': {
    legacySpotId: 73005,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/rogers-city-beach/',
  },
  '584204204e65fad6a7709b14': {
    legacySpotId: 117793,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/pontile/',
  },
  '584204204e65fad6a770957f': {
    legacySpotId: 72985,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/bayfield-harbor/',
  },
  '584204204e65fad6a7709b2a': {
    legacySpotId: 117847,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/lamone/',
  },
  '584204204e65fad6a7709b2b': {
    legacySpotId: 117846,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/carabiniere/',
  },
  '584204204e65fad6a7709b2c': {
    legacySpotId: 117848,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/la-diga/',
  },
  '584204204e65fad6a7709b2d': {
    legacySpotId: 117844,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/punta-sabbioni/',
  },
  '5842041f4e65fad6a7708a8a': {
    legacySpotId: 5312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/crossroads/',
  },
  '5842041f4e65fad6a7708a8b': {
    legacySpotId: 5314,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/sawgrass/',
  },
  '5842041f4e65fad6a7708a8c': {
    legacySpotId: 5317,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/18th-street/',
  },
  '5842041f4e65fad6a7708a8d': {
    legacySpotId: 5318,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/mayport-poles/',
  },
  '5842041f4e65fad6a7708a8e': {
    legacySpotId: 5319,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/blue-roofs/',
  },
  '5842041f4e65fad6a7708a8f': {
    legacySpotId: 5320,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/huguenot-park--north-jetty-/',
  },
  '584204204e65fad6a7709b0c': {
    legacySpotId: 117784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/bogliasco/',
  },
  '584204204e65fad6a7709b40': {
    legacySpotId: 117872,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/cornino/',
  },
  '584204204e65fad6a7709b13': {
    legacySpotId: 117791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/cinquale/',
  },
  '584204204e65fad6a7709b0d': {
    legacySpotId: 117785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/recco/',
  },
  '584204204e65fad6a7709b43': {
    legacySpotId: 117876,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/playa-di-catania/',
  },
  '584204204e65fad6a7709b44': {
    legacySpotId: 117882,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/badesi/',
  },
  '584204204e65fad6a7709b45': {
    legacySpotId: 117880,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/santa-reparata/',
  },
  '584204204e65fad6a7709b46': {
    legacySpotId: 117881,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/marinedda/',
  },
  '584204204e65fad6a7709b47': {
    legacySpotId: 117883,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/silver-rock/',
  },
  '5842041f4e65fad6a7708cfc': {
    legacySpotId: 7109,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/morocco/north-morocco/oued-cherrat/',
  },
  '584204204e65fad6a7709b49': {
    legacySpotId: 117885,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/la-maddalenetta/',
  },
  '584204204e65fad6a7709b11': {
    legacySpotId: 117789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/northern-italy/lerici/',
  },
  '584204204e65fad6a7709b3a': {
    legacySpotId: 117868,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/magaggiari/',
  },
  '584204204e65fad6a7709b3b': {
    legacySpotId: 117867,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/torre-muzzard/',
  },
  '584204204e65fad6a7709b3c': {
    legacySpotId: 117869,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/praiola/',
  },
  '584204204e65fad6a7709b3d': {
    legacySpotId: 117870,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/ciammarita/',
  },
  '5842041f4e65fad6a7708a9a': {
    legacySpotId: 5330,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/sunglow-pier/',
  },
  '5842041f4e65fad6a7708a9b': {
    legacySpotId: 5313,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/gate-station/',
  },
  '5842041f4e65fad6a7708a9c': {
    legacySpotId: 5334,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/buttenheim-park/',
  },
  '5842041f4e65fad6a7708a9d': {
    legacySpotId: 5329,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/ponce-inlet/',
  },
  '584204204e65fad6a77092fb': {
    legacySpotId: 61613,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/lighthouse-beach/',
  },
  '5842041f4e65fad6a7708a9f': {
    legacySpotId: 5336,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/sebastian-inlet/',
  },
  '584204204e65fad6a77099e5': {
    legacySpotId: 110429,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/vila-franca/',
  },
  '584204204e65fad6a7709b50': {
    legacySpotId: 117890,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/capo-mannu/',
  },
  '584204204e65fad6a7709b51': {
    legacySpotId: 117893,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/scivu/',
  },
  '584204204e65fad6a7709b52': {
    legacySpotId: 117894,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/portixeddu/',
  },
  '584204204e65fad6a7709b53': {
    legacySpotId: 117896,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/goroneddu/',
  },
  '584204204e65fad6a77092fa': {
    legacySpotId: 61599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/northern-nsw/sharpes-beach/',
  },
  '584204204e65fad6a7709b55': {
    legacySpotId: 117895,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/buggerru/',
  },
  '584204204e65fad6a7709b56': {
    legacySpotId: 117903,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/chia/',
  },
  '584204204e65fad6a7709b57': {
    legacySpotId: 117902,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/pipeline/',
  },
  '584204204e65fad6a7709b58': {
    legacySpotId: 117905,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/su-conventeddu/',
  },
  '584204204e65fad6a7709b59': {
    legacySpotId: 117906,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/poetto/',
  },
  '584204204e65fad6a7709532': {
    legacySpotId: 72891,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/vermilion-main-street-beach/',
  },
  '584204204e65fad6a7709770': {
    legacySpotId: 91665,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/shonan/shonanko/',
  },
  '584204204e65fad6a7709538': {
    legacySpotId: 72896,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/luna-pier/',
  },
  '584204204e65fad6a7709b4a': {
    legacySpotId: 117887,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/santa-caterina/',
  },
  '584204204e65fad6a7709b4b': {
    legacySpotId: 117886,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/la-speranza/',
  },
  '584204204e65fad6a7709b4c': {
    legacySpotId: 117888,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/s-archittu/',
  },
  '584204204e65fad6a7709b4d': {
    legacySpotId: 117889,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/sa-mesa-longa/',
  },
  '584204204e65fad6a7709b4e': {
    legacySpotId: 117892,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/piscinas/',
  },
  '584204204e65fad6a7709b4f': {
    legacySpotId: 117891,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/west-sardinia/mini-capo/',
  },
  '584204204e65fad6a77095f0': {
    legacySpotId: 74046,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/scheveningen/',
  },
  '584204204e65fad6a7709b60': {
    legacySpotId: 118015,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/alabama/west-pass/',
  },
  '584204204e65fad6a7709b61': {
    legacySpotId: 118018,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/alabama/spuds/',
  },
  '584204204e65fad6a7709b62': {
    legacySpotId: 118017,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/alabama/alabama-point/',
  },
  '584204204e65fad6a7709b63': {
    legacySpotId: 118020,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/louisiana/fourchon/',
  },
  '584204204e65fad6a7709b64': {
    legacySpotId: 118019,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/louisiana/holly-beach/',
  },
  '584204204e65fad6a7709b65': {
    legacySpotId: 118022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/louisiana/chandeleur-islands/',
  },
  '584204204e65fad6a7709b66': {
    legacySpotId: 118021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/louisiana/grande-isle/',
  },
  '584204204e65fad6a7709b67': {
    legacySpotId: 118596,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/supertubes--vila-baleira-/',
  },
  '584204204e65fad6a7709081': {
    legacySpotId: 50531,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/seaton-carew/',
  },
  '584204204e65fad6a7709523': {
    legacySpotId: 72877,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/lake-erie-state-park/',
  },
  '584204204e65fad6a7709524': {
    legacySpotId: 72878,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/freeport/',
  },
  '584204204e65fad6a7709521': {
    legacySpotId: 72875,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/ny---pa--erie-/evangola/',
  },
  '584204214e65fad6a7709c05': {
    legacySpotId: 125986,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/hafnir/',
  },
  '584204204e65fad6a7709527': {
    legacySpotId: 72880,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/conneaut-lighthouse-park/',
  },
  '584204204e65fad6a7709528': {
    legacySpotId: 72881,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/walnut-beach/',
  },
  '584204204e65fad6a7709562': {
    legacySpotId: 72959,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/park-point/',
  },
  '584204214e65fad6a7709c09': {
    legacySpotId: 125991,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/brimuro/',
  },
  '590927576a2e4300134fbed8': {
    legacySpotId: 146850,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/venice-breakwater/',
  },
  '584204204e65fad6a7709b5a': {
    legacySpotId: 117908,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/racca-point/',
  },
  '584204204e65fad6a7709b5b': {
    legacySpotId: 117909,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/solanas/',
  },
  '584204204e65fad6a7709b5c': {
    legacySpotId: 117910,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/porto-guinco/',
  },
  '584204204e65fad6a7709b5d': {
    legacySpotId: 118014,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/alabama/dauphin-island/',
  },
  '584204204e65fad6a7709b5e': {
    legacySpotId: 117911,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/italy/south-sardinia/sant-elmo/',
  },
  '584204204e65fad6a7708fc3': {
    legacySpotId: 48542,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/havaizinho/',
  },
  '584204204e65fad6a7708fc4': {
    legacySpotId: 48545,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/ronco-do-mar/',
  },
  '584204204e65fad6a7708fc5': {
    legacySpotId: 48541,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/outside/',
  },
  '584204204e65fad6a7708fc6': {
    legacySpotId: 48547,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/pedra-do-meio/',
  },
  '584204204e65fad6a7708fc7': {
    legacySpotId: 48546,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/ceara/boca-do-poco/',
  },
  '584204204e65fad6a7709b28': {
    legacySpotId: 117845,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/adriatic-sea/adria/',
  },
  '584204204e65fad6a7708fc9': {
    legacySpotId: 48552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/maranhao/s-atilde-o-marcos/',
  },
  '584204204e65fad6a7709b71': {
    legacySpotId: 119195,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/escolleras/',
  },
  '584204204e65fad6a7709b72': {
    legacySpotId: 119198,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/marti/',
  },
  '584204204e65fad6a7709b73': {
    legacySpotId: 119196,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/playa-norte/',
  },
  '584204204e65fad6a7709b74': {
    legacySpotId: 119199,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/destapador/',
  },
  '584204204e65fad6a7709b75': {
    legacySpotId: 119200,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/tampi/',
  },
  '584204204e65fad6a7709b76': {
    legacySpotId: 119201,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/costalitos/',
  },
  '584204204e65fad6a7709b77': {
    legacySpotId: 119350,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/chicala/',
  },
  '584204204e65fad6a7709b78': {
    legacySpotId: 119332,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/veracruz/camaronera/',
  },
  '584204204e65fad6a7709b79': {
    legacySpotId: 119355,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/miradouro/',
  },
  '584204214e65fad6a7709c13': {
    legacySpotId: 126937,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/paulilles/',
  },
  '5842041f4e65fad6a7708e72': {
    legacySpotId: 44540,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/santander--el-sardinero-/',
  },
  '584204214e65fad6a7709c15': {
    legacySpotId: 126940,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/la-guillotine/',
  },
  '584204214e65fad6a7709c16': {
    legacySpotId: 126939,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/gruissan/',
  },
  '584204214e65fad6a7709c17': {
    legacySpotId: 126941,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/sete/',
  },
  '584204214e65fad6a7709c18': {
    legacySpotId: 126942,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/palavas-les-flots/',
  },
  '584204214e65fad6a7709c19': {
    legacySpotId: 126944,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/saintes-maries/',
  },
  '584204204e65fad6a7709b6a': {
    legacySpotId: 118599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/ponta-delgada/',
  },
  '584204204e65fad6a7709b6b': {
    legacySpotId: 118600,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/ponta-de-tristao/',
  },
  '584204204e65fad6a7709b6c': {
    legacySpotId: 118601,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/madeira/madeira/madalena-do-mar/',
  },
  '584204204e65fad6a7708fd1': {
    legacySpotId: 48608,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/fernando-de-noronha/cacimba-do-padre/',
  },
  '584204204e65fad6a7708fd2': {
    legacySpotId: 48610,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/fernando-de-noronha/conceicao/',
  },
  '584204204e65fad6a7709b6f': {
    legacySpotId: 119184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/new-brunswick/dennis-beach/',
  },
  '584204204e65fad6a7708fd4': {
    legacySpotId: 48611,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/fernando-de-noronha/meio/',
  },
  '584204204e65fad6a7708fd5': {
    legacySpotId: 48613,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/fernando-de-noronha/abras/',
  },
  '584204204e65fad6a7708fd6': {
    legacySpotId: 48612,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/fernando-de-noronha/biboca/',
  },
  '584204204e65fad6a7708fd7': {
    legacySpotId: 48614,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/northern-brazil/fernando-de-noronha/baia-das-ratas/',
  },
  '584204204e65fad6a7708fd8': {
    legacySpotId: 48715,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/bicarrosse-plage/',
  },
  '584204204e65fad6a7708fd9': {
    legacySpotId: 48716,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/mimizan-plage/',
  },
  '584204204e65fad6a7709571': {
    legacySpotId: 72980,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/sauble-beach/',
  },
  '5842041f4e65fad6a7708e66': {
    legacySpotId: 44508,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/estoril/',
  },
  '584204204e65fad6a7708fca': {
    legacySpotId: 48553,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/maranhao/ponta-d-areia/',
  },
  '584204204e65fad6a7708fcb': {
    legacySpotId: 48555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/maranhao/olho-d-agu/',
  },
  '584204204e65fad6a7708fcc': {
    legacySpotId: 48554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/maranhao/calhau/',
  },
  '584204204e65fad6a7708fcd': {
    legacySpotId: 48556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/maranhao/ponta-do-farol/',
  },
  '584204204e65fad6a7708fce': {
    legacySpotId: 48557,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/maranhao/atins/',
  },
  '584204204e65fad6a7708fcf': {
    legacySpotId: 48558,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/north-brazil/para/camarauacu/',
  },
  '584204214e65fad6a7709c26': {
    legacySpotId: 126954,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/marseille/bandol/',
  },
  '584204204e65fad6a770959d': {
    legacySpotId: 73026,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/st--catherine-s-beach/',
  },
  '584204214e65fad6a7709c28': {
    legacySpotId: 126959,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/gigaro/',
  },
  '584204204e65fad6a7709506': {
    legacySpotId: 72807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/gilson-beach/',
  },
  '584204204e65fad6a7709b7a': {
    legacySpotId: 119352,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/mussulo/',
  },
  '584204204e65fad6a7708fe0': {
    legacySpotId: 48906,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/les-bourdaines/',
  },
  '584204204e65fad6a7708fe1': {
    legacySpotId: 48912,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/ondres-plage/',
  },
  '584204204e65fad6a7708fe2': {
    legacySpotId: 49047,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-porge/',
  },
  '584204204e65fad6a7708fe3': {
    legacySpotId: 49049,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/la-jenny/',
  },
  '584204204e65fad6a7708fe4': {
    legacySpotId: 49050,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-grand-crohot/',
  },
  '584204204e65fad6a7708fe5': {
    legacySpotId: 49051,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-truc-vert/',
  },
  '584204204e65fad6a7708fe6': {
    legacySpotId: 49052,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-petit-train/',
  },
  '584204204e65fad6a7708fe7': {
    legacySpotId: 49053,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/cap-ferret/',
  },
  '584204204e65fad6a7708fe8': {
    legacySpotId: 49055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/cap-ferret-chenal/',
  },
  '584204204e65fad6a7708fe9': {
    legacySpotId: 49056,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/lespecier-plage/',
  },
  '5842041f4e65fad6a7708e63': {
    legacySpotId: 44527,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northwest-spain/patos/',
  },
  '584204204e65fad6a7709b41': {
    legacySpotId: 117874,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/italy/sicily/ponente/',
  },
  '584204214e65fad6a7709cb5': {
    legacySpotId: 130202,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/poland-baltic-sea/northern-poland/chalupy/',
  },
  '584204204e65fad6a7709577': {
    legacySpotId: 72989,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/kettle-point/',
  },
  '584204204e65fad6a7708fda': {
    legacySpotId: 48718,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/contis-plage/',
  },
  '584204204e65fad6a7708fdb': {
    legacySpotId: 48720,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/cap-de-l-homy-plage/',
  },
  '584204204e65fad6a7708fdc': {
    legacySpotId: 48721,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/saint-girons-plage/',
  },
  '584204204e65fad6a7708fdd': {
    legacySpotId: 48722,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/moliets-plage/',
  },
  '584204204e65fad6a7708fde': {
    legacySpotId: 48723,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/vieux-boucau/',
  },
  '584204204e65fad6a7708fdf': {
    legacySpotId: 48903,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/les-casernes/',
  },
  '584204214e65fad6a7709c36': {
    legacySpotId: 127093,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/star-jetty-cruise/',
  },
  '584204214e65fad6a7709c37': {
    legacySpotId: 127232,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/ifaty/',
  },
  '584204214e65fad6a7709bd1': {
    legacySpotId: 125298,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/kilyos/',
  },
  '584204214e65fad6a7709c39': {
    legacySpotId: 127234,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/flame-balls/',
  },
  '584204204e65fad6a7708ff0': {
    legacySpotId: 49063,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/la-piste/',
  },
  '584204204e65fad6a7708ff1': {
    legacySpotId: 49087,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/la-barre/',
  },
  '584204204e65fad6a7708ff2': {
    legacySpotId: 49085,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/labenne-ocean/',
  },
  '584204204e65fad6a7708ff3': {
    legacySpotId: 49086,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/tarnos-plage/',
  },
  '584204204e65fad6a7708ff4': {
    legacySpotId: 49088,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/marinella/',
  },
  '584204204e65fad6a7708ff5': {
    legacySpotId: 49089,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/sables-d-or/',
  },
  '584204204e65fad6a7708ff6': {
    legacySpotId: 49090,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-club/',
  },
  '584204204e65fad6a7708ff7': {
    legacySpotId: 49091,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/chambre-d-amour/',
  },
  '584204204e65fad6a7708ff8': {
    legacySpotId: 49092,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/ilbaritz/',
  },
  '584204204e65fad6a7708ff9': {
    legacySpotId: 49093,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/erretegia/',
  },
  '584204214e65fad6a7709c2f': {
    legacySpotId: 127095,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/malaysia/east-peninsula/kijal/',
  },
  '584204214e65fad6a7709cb1': {
    legacySpotId: 130198,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/poland-baltic-sea/northern-poland/dabki/',
  },
  '584204204e65fad6a770950e': {
    legacySpotId: 72813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/north-point-beach-winthrop-harbor/',
  },
  '584204204e65fad6a77094a0': {
    legacySpotId: 71474,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/son-bou/',
  },
  '584204204e65fad6a77094a1': {
    legacySpotId: 71475,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/southwest-spain/yerbabuena/',
  },
  '584204204e65fad6a7708fea': {
    legacySpotId: 49057,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/la-penon/',
  },
  '584204204e65fad6a7708feb': {
    legacySpotId: 49058,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/les-estagnots/',
  },
  '584204204e65fad6a7708fec': {
    legacySpotId: 49059,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/hossegor---la-nord/',
  },
  '584204204e65fad6a7708fed': {
    legacySpotId: 49060,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/la-sud/',
  },
  '584204204e65fad6a7708fee': {
    legacySpotId: 49061,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/l-estacade/',
  },
  '584204204e65fad6a7708fef': {
    legacySpotId: 49062,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/le-prevent/',
  },
  '584204204e65fad6a77094a9': {
    legacySpotId: 71549,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/kleinmond/',
  },
  '584204214e65fad6a7709c49': {
    legacySpotId: 127708,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/krioneri/',
  },
  '584204204e65fad6a77094f1': {
    legacySpotId: 72785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/van-buren-state-park/',
  },
  '584204204e65fad6a770950d': {
    legacySpotId: 72814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/racine/',
  },
  '584204214e65fad6a7709c3a': {
    legacySpotId: 127235,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/jelly-babies/',
  },
  '584204214e65fad6a7709c3b': {
    legacySpotId: 127237,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/puss-puss/',
  },
  '584204204e65fad6a770950c': {
    legacySpotId: 72811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/north-avenue-beach/',
  },
  '584204214e65fad6a7709c3d': {
    legacySpotId: 127240,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/lavanono/',
  },
  '584204204e65fad6a7709084': {
    legacySpotId: 50529,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/tynemouth---longsands/',
  },
  '584204204e65fad6a770950b': {
    legacySpotId: 72812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/illinois-state-beach/',
  },
  '584204204e65fad6a77095fe': {
    legacySpotId: 74069,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/zeebrugge/',
  },
  '584204204e65fad6a77093bd': {
    legacySpotId: 66105,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/central-nsw/avoca-point/',
  },
  '584204204e65fad6a77094b0': {
    legacySpotId: 71556,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/buffels-bay/',
  },
  '584204204e65fad6a77094b1': {
    legacySpotId: 71557,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/plettenberg-bay/',
  },
  '584204204e65fad6a77094b2': {
    legacySpotId: 71559,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/sodwana-bay/',
  },
  '584204204e65fad6a7708ffa': {
    legacySpotId: 49094,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/bidart/',
  },
  '584204204e65fad6a7708ffb': {
    legacySpotId: 49096,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/les-alcyons/',
  },
  '584204204e65fad6a7708ffc': {
    legacySpotId: 49097,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/avalanche/',
  },
  '584204204e65fad6a7708ffd': {
    legacySpotId: 49095,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/guethary---parlementia/',
  },
  '584204204e65fad6a7708ffe': {
    legacySpotId: 49098,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/plage-de-mayarko/',
  },
  '584204204e65fad6a7708fff': {
    legacySpotId: 49099,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france/southwest-france/erromardie/',
  },
  '584204204e65fad6a77094b9': {
    legacySpotId: 71566,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/strand/',
  },
  '584204204e65fad6a7709512': {
    legacySpotId: 72819,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/big-whitefish-bay/',
  },
  '5842041f4e65fad6a770889f': {
    legacySpotId: 4771,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/grandview/',
  },
  '584204204e65fad6a7709510': {
    legacySpotId: 72815,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/wind-point/',
  },
  '584204204e65fad6a7709572': {
    legacySpotId: 72981,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southwest-on--huron-/macgregor-point/',
  },
  '5842041f4e65fad6a770889e': {
    legacySpotId: 4763,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-kona/kawaihae/',
  },
  '584204204e65fad6a7709517': {
    legacySpotId: 72825,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/point-beach/',
  },
  '584204204e65fad6a77095fd': {
    legacySpotId: 74068,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/knokke-heist/',
  },
  '584204204e65fad6a77094aa': {
    legacySpotId: 71550,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/hermanus/',
  },
  '584204204e65fad6a77094ab': {
    legacySpotId: 71551,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/still-bay/',
  },
  '5842041f4e65fad6a7708e2b': {
    legacySpotId: 26881,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/marianas/guam/ricks-reef/',
  },
  '584204204e65fad6a77094ad': {
    legacySpotId: 71552,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/mossel-bay/',
  },
  '584204204e65fad6a77094ae': {
    legacySpotId: 71554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/victoria-bay/',
  },
  '584204204e65fad6a77094af': {
    legacySpotId: 71555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/southern-cape/wilderness/',
  },
  '584204214e65fad6a7709c4d': {
    legacySpotId: 127710,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/loutsa/',
  },
  '584204214e65fad6a7709c4e': {
    legacySpotId: 127711,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/ligia-reef/',
  },
  '584204214e65fad6a7709c4f': {
    legacySpotId: 127713,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/stonebread/',
  },
  '5842041f4e65fad6a7708e2f': {
    legacySpotId: 26885,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/marianas/guam/ylig/',
  },
  '584204204e65fad6a77094c2': {
    legacySpotId: 71781,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taipei/paishawan/',
  },
  '584204204e65fad6a77094c3': {
    legacySpotId: 71784,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/yilan/wushi/',
  },
  '5842041f4e65fad6a7708e25': {
    legacySpotId: 25277,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/caribbean/aruba/aruba/arashi/',
  },
  '584204204e65fad6a77094c5': {
    legacySpotId: 71786,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/yilan/dashi/',
  },
  '5842041f4e65fad6a7708e27': {
    legacySpotId: 25279,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/aruba/aruba/dos-playas/',
  },
  '5842041f4e65fad6a7708e28': {
    legacySpotId: 25402,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/porlackshofn/',
  },
  '584204204e65fad6a77094c8': {
    legacySpotId: 71787,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/yilan/wai-ao/',
  },
  '584204204e65fad6a7709b5f': {
    legacySpotId: 118016,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/gulf-states/alabama/terry-s-cove/',
  },
  '5842041f4e65fad6a7708e23': {
    legacySpotId: 25052,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/alaska/cannon-beach/',
  },
  '584204214e65fad6a7709c68': {
    legacySpotId: 127811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/stalida-beach--crete-/',
  },
  '5842041f4e65fad6a770888e': {
    legacySpotId: 4748,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/rocky-point/',
  },
  '5842041f4e65fad6a770888f': {
    legacySpotId: 4749,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/gas-chambers/',
  },
  '5842041f4e65fad6a770888d': {
    legacySpotId: 4746,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/sunset-beach/',
  },
  '584204214e65fad6a7709c60': {
    legacySpotId: 127801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/limani-pasa-beach/',
  },
  '5842041f4e65fad6a7708e26': {
    legacySpotId: 25278,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/aruba/aruba/rodgers-beach/',
  },
  '584204204e65fad6a77094ba': {
    legacySpotId: 71567,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/melkbosstrand/',
  },
  '584204204e65fad6a77094bb': {
    legacySpotId: 71537,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/ballito/',
  },
  '584204204e65fad6a77094bd': {
    legacySpotId: 71691,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/maria-s-beachfront/',
  },
  '584204214e65fad6a7709c5b': {
    legacySpotId: 127793,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--aegean-sea-/llias-madraki--skiathos-/',
  },
  '584204204e65fad6a77094c6': {
    legacySpotId: 71785,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/yilan/toucheng/',
  },
  '584204214e65fad6a7709c5d': {
    legacySpotId: 127798,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--aegean-sea-/sarti-beach/',
  },
  '584204214e65fad6a7709c5e': {
    legacySpotId: 127800,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/vouliagmeni/',
  },
  '584204214e65fad6a7709c5f': {
    legacySpotId: 127803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/kolimpithra--tinos-/',
  },
  '584204204e65fad6a77094d0': {
    legacySpotId: 71814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/point-michaud/',
  },
  '584204204e65fad6a77094d1': {
    legacySpotId: 71906,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southeast-brazil/rio-de-janeiro-litoral-leste/praia-do-pecado/',
  },
  '584204204e65fad6a77094d2': {
    legacySpotId: 72187,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/pingtung/nanwan/',
  },
  '584204204e65fad6a77094d3': {
    legacySpotId: 72312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/brunswick/ocean-isle/',
  },
  '584204204e65fad6a77094d4': {
    legacySpotId: 72188,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/wavepark/',
  },
  '584204214e65fad6a7709c72': {
    legacySpotId: 127982,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/punta-lobo/',
  },
  '584204204e65fad6a77094d6': {
    legacySpotId: 72738,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/fort-wilkins-state-park/',
  },
  '584204214e65fad6a7709c74': {
    legacySpotId: 127981,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/punta-restin/',
  },
  '584204204e65fad6a77094d8': {
    legacySpotId: 72739,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/trails-end-bay/',
  },
  '584204204e65fad6a77094d9': {
    legacySpotId: 72741,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/sleeping-bear-dunes/',
  },
  '584204214e65fad6a7709c77': {
    legacySpotId: 128058,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/central-peru/puerto-eten/',
  },
  '584204214e65fad6a7709c78': {
    legacySpotId: 128060,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/central-peru/punta-prieta/',
  },
  '584204214e65fad6a7709c79': {
    legacySpotId: 128083,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-san-diego/ponto-north/',
  },
  '584204204e65fad6a77094ca': {
    legacySpotId: 71792,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/pingtung/jialeshuei/',
  },
  '584204204e65fad6a77094cb': {
    legacySpotId: 71806,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/cow-bay-rainbow-haven/',
  },
  '584204204e65fad6a77094cc': {
    legacySpotId: 71804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/summerville/',
  },
  '584204204e65fad6a7709592': {
    legacySpotId: 73014,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/southwicks/',
  },
  '584204204e65fad6a77094ce': {
    legacySpotId: 71812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/western-head/',
  },
  '584204204e65fad6a77094cf': {
    legacySpotId: 71807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/lawrencetown-beach/',
  },
  '584204214e65fad6a7709c6d': {
    legacySpotId: 127977,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/los-organos/',
  },
  '584204214e65fad6a7709c6e': {
    legacySpotId: 127978,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/el-nuro/',
  },
  '584204204e65fad6a7709548': {
    legacySpotId: 72914,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/pleasant-beach/',
  },
  '584204204e65fad6a7709594': {
    legacySpotId: 73016,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-ny--ontario-/selkirk-shores/',
  },
  '584204204e65fad6a77094e0': {
    legacySpotId: 72757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/manistee-pier/',
  },
  '584204204e65fad6a77094e1': {
    legacySpotId: 72759,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/ludington-state-park/',
  },
  '584204204e65fad6a77094e2': {
    legacySpotId: 72761,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/silver-lake/',
  },
  '584204204e65fad6a77094e3': {
    legacySpotId: 72760,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/charles-mears-state-park/',
  },
  '584204204e65fad6a77094e4': {
    legacySpotId: 72762,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/white-lake-channel/',
  },
  '584204204e65fad6a77094e5': {
    legacySpotId: 72765,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/duck-lake/',
  },
  '584204204e65fad6a77094e6': {
    legacySpotId: 72766,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/pioneer-park/',
  },
  '584204204e65fad6a77094e7': {
    legacySpotId: 72775,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/norton-shores/',
  },
  '584204204e65fad6a77094e8': {
    legacySpotId: 72767,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/muskegon/',
  },
  '584204204e65fad6a770942a': {
    legacySpotId: 67586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/currarong/',
  },
  '584204214e65fad6a7709c87': {
    legacySpotId: 128409,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/west-finland/poliisinlahti/',
  },
  '5842041f4e65fad6a770889c': {
    legacySpotId: 4761,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/south-shore/',
  },
  '5842041f4e65fad6a770889d': {
    legacySpotId: 4762,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-kona/banyans/',
  },
  '584204204e65fad6a77094da': {
    legacySpotId: 72740,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/charlevoix/',
  },
  '584204204e65fad6a77093fc': {
    legacySpotId: 66780,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/suck-rock/',
  },
  '584204204e65fad6a77094dc': {
    legacySpotId: 72742,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/empire-beach/',
  },
  '584204204e65fad6a77094dd': {
    legacySpotId: 72743,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/frankfort-beach/',
  },
  '584204204e65fad6a77094de': {
    legacySpotId: 72756,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/orchard-beach/',
  },
  '584204204e65fad6a77094df': {
    legacySpotId: 72745,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/north-mi--michigan-/elberta-beach/',
  },
  '5842041f4e65fad6a7708e1f': {
    legacySpotId: 21417,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/dorset/kimmeridge-bay/',
  },
  '584204214e65fad6a7709c7e': {
    legacySpotId: 128388,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--gulf-of-bothnia-/gaddviken/',
  },
  '584204214e65fad6a7709c7f': {
    legacySpotId: 128391,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/sweden/sweden--baltic-sea-/angjarnsudden/',
  },
  '584204214e65fad6a7709d29': {
    legacySpotId: 142802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/cocoa-beach-pier-ss/',
  },
  '584204214e65fad6a7709d28': {
    legacySpotId: 143342,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/praia-pequena/',
  },
  '584204204e65fad6a7709585': {
    legacySpotId: 72999,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-mi--huron-/tawas-point/',
  },
  '584204204e65fad6a77094f0': {
    legacySpotId: 72783,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/south-haven-north-beach/',
  },
  '5842041f4e65fad6a7708876': {
    legacySpotId: 4425,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/jensen-beach/',
  },
  '5842041f4e65fad6a7708873': {
    legacySpotId: 4422,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/indialantic-boardwalk/',
  },
  '584204204e65fad6a77094f3': {
    legacySpotId: 72787,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/st--joeseph-north-beach/',
  },
  '5842041f4e65fad6a7708879': {
    legacySpotId: 4428,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/reef-road/',
  },
  '584204204e65fad6a77094f5': {
    legacySpotId: 72789,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/warren-dunes/',
  },
  '584204204e65fad6a77094f6': {
    legacySpotId: 72790,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/gordon-beach/',
  },
  '584204204e65fad6a77094f7': {
    legacySpotId: 72791,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/new-buffalo/',
  },
  '584204204e65fad6a77094f8': {
    legacySpotId: 72792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/washington-park/',
  },
  '584204204e65fad6a77094f9': {
    legacySpotId: 72793,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/beverly-shores/',
  },
  '584204214e65fad6a7709c97': {
    legacySpotId: 128973,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/latvia/latvia--baltic-sea-/palmu-uzava/',
  },
  '584204214e65fad6a7709c98': {
    legacySpotId: 128970,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/mozambique/south-mozambique/praia-de-zavora/',
  },
  '584204214e65fad6a7709c99': {
    legacySpotId: 128972,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/mozambique/south-mozambique/ponta-de-ouro/',
  },
  '5842041f4e65fad6a770885b': {
    legacySpotId: 4280,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/long-beach-island/',
  },
  '584204204e65fad6a77094eb': {
    legacySpotId: 72780,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/saugatuck-dunes/',
  },
  '584204204e65fad6a77094ec': {
    legacySpotId: 72776,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/grandhaven/',
  },
  '584204204e65fad6a77094ed': {
    legacySpotId: 72778,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/port-sheldon/',
  },
  '584204204e65fad6a77094ee': {
    legacySpotId: 72779,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/holland-light-house/',
  },
  '584204204e65fad6a77094ef': {
    legacySpotId: 72781,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/west-mi--michigan-/oval-beach/',
  },
  '584204204e65fad6a770953c': {
    legacySpotId: 72900,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/point-pelee/',
  },
  '584204214e65fad6a7709c8e': {
    legacySpotId: 128961,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/lebanon/lebanon/batroun/',
  },
  '584204204e65fad6a770953e': {
    legacySpotId: 72903,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/duttona--beach/',
  },
  '584204204e65fad6a77095e7': {
    legacySpotId: 74037,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/bergen-aan-zee/',
  },
  '584204204e65fad6a770953b': {
    legacySpotId: 72901,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/south-on--erie-/wheatley/',
  },
  '584204214e65fad6a7709cf1': {
    legacySpotId: 135286,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/southern-brazil/parana/ilha-do-mel/',
  },
  '5842041f4e65fad6a7708885': {
    legacySpotId: 4736,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/trails/',
  },
  '5842041f4e65fad6a7708886': {
    legacySpotId: 4739,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/middles/',
  },
  '5842041f4e65fad6a7708887': {
    legacySpotId: 4738,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/upper-trestles/',
  },
  '5842041f4e65fad6a7708884': {
    legacySpotId: 4737,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/cottons/',
  },
  '584204204e65fad6a770944d': {
    legacySpotId: 68745,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/blackfellows-point/',
  },
  '5842041f4e65fad6a7708889': {
    legacySpotId: 4439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/surfside/',
  },
  '584204204e65fad6a7709a8f': {
    legacySpotId: 111830,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/india/west-india/mahe/',
  },
  '584204204e65fad6a7709679': {
    legacySpotId: 77032,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/nine-mile-beach/',
  },
  '584204204e65fad6a77093de': {
    legacySpotId: 66497,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/sydney/long-reef/',
  },
  '584204204e65fad6a77094fa': {
    legacySpotId: 72795,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/porter-beach/',
  },
  '584204204e65fad6a77094fb': {
    legacySpotId: 72794,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/indiana-dunes/',
  },
  '584204204e65fad6a77094fc': {
    legacySpotId: 72796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/dunes-acres-beach/',
  },
  '584204204e65fad6a77094fd': {
    legacySpotId: 72798,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/whiting/',
  },
  '584204204e65fad6a77094fe': {
    legacySpotId: 72797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/ogden-dunes-west-beach/',
  },
  '584204214e65fad6a7709c9c': {
    legacySpotId: 129507,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/sao-tome-and-principe/sao-tome/porto-alegre/',
  },
  '584204214e65fad6a7709c9d': {
    legacySpotId: 129506,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/sao-tome-and-principe/sao-tome/ecolodge/',
  },
  '584204214e65fad6a7709c9e': {
    legacySpotId: 129510,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/sao-tome-and-principe/sao-tome/radiation-point/',
  },
  '584204214e65fad6a7709c9f': {
    legacySpotId: 129681,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/senegal/senegal/st--louis/',
  },
  '589a447b904aae00147f95e0': {
    legacySpotId: 143998,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/rhode-island/point-judith-north/',
  },
  '5842041f4e65fad6a7708871': {
    legacySpotId: 4420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/new-smyrna-beach/',
  },
  '5842041f4e65fad6a7708872': {
    legacySpotId: 4421,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/central-florida/cocoa-beach-pier/',
  },
  '5842041f4e65fad6a7708896': {
    legacySpotId: 4758,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/hultin-s/',
  },
  '5842041f4e65fad6a7708897': {
    legacySpotId: 4757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/jocko-s/',
  },
  '5842041f4e65fad6a7708898': {
    legacySpotId: 4759,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/laniakea/',
  },
  '5842041f4e65fad6a7708892': {
    legacySpotId: 4753,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/rockpile/',
  },
  '584204214e65fad6a7709cea': {
    legacySpotId: 133757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-south-coast/hastings/',
  },
  '5842041f4e65fad6a7708891': {
    legacySpotId: 4751,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/backdoor/',
  },
  '5842041f4e65fad6a7708e14': {
    legacySpotId: 10857,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/lanai/lanai/manele-bay/',
  },
  '5842041f4e65fad6a7708890': {
    legacySpotId: 4750,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/pipeline/',
  },
  '5842041f4e65fad6a7708e18': {
    legacySpotId: 10858,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/lanai/lanai/lopa-beach/',
  },
  '5842041f4e65fad6a770888b': {
    legacySpotId: 4741,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/church/',
  },
  '5842041f4e65fad6a7708899': {
    legacySpotId: 4756,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/chun-s/',
  },
  '584204204e65fad6a7709550': {
    legacySpotId: 72934,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/grand-marais-beach--michigan/',
  },
  '5842041f4e65fad6a770887c': {
    legacySpotId: 4431,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/boca-raton-inlet/',
  },
  '5842041f4e65fad6a7708e15': {
    legacySpotId: 13827,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-hilo/kolekole/',
  },
  '584204204e65fad6a7709590': {
    legacySpotId: 73011,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-on--ontario-/patterson-park/',
  },
  '5842041f4e65fad6a7708e0e': {
    legacySpotId: 11969,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/kandui/',
  },
  '584204204e65fad6a7709aa2': {
    legacySpotId: 115802,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/the-peak/',
  },
  '584204204e65fad6a7709aa3': {
    legacySpotId: 115803,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/kadarim/',
  },
  '584204204e65fad6a7709aa4': {
    legacySpotId: 115804,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/stalbeach/',
  },
  '584204204e65fad6a7709aa5': {
    legacySpotId: 115806,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/caesarea/',
  },
  '5842041f4e65fad6a7708863': {
    legacySpotId: 4408,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/3rd-st-/',
  },
  '5842041f4e65fad6a7708864': {
    legacySpotId: 4407,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/virginia/1st-st--jetty-to-pier/',
  },
  '584204204e65fad6a7709aa8': {
    legacySpotId: 115808,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/beit-yanai/',
  },
  '584204204e65fad6a7709aa9': {
    legacySpotId: 115810,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/argamans-beach/',
  },
  '584204214e65fad6a7709b90': {
    legacySpotId: 119419,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/guns/',
  },
  '584204214e65fad6a7709b91': {
    legacySpotId: 119420,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/east-hill-point/',
  },
  '584204204e65fad6a7709553': {
    legacySpotId: 72936,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/michigan-u-p---superior-/twelve-mile-beach/',
  },
  '584204214e65fad6a7709b7b': {
    legacySpotId: 119354,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/shipwreck/',
  },
  '584204204e65fad6a7709601': {
    legacySpotId: 74073,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/bredene/',
  },
  '5842041f4e65fad6a7708867': {
    legacySpotId: 4410,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/hatteras-island/s-turns--rodanthe/',
  },
  '584204214e65fad6a7709b96': {
    legacySpotId: 119465,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/liberia/liberia/fisherman-s-point/',
  },
  '58bdd8fca7ce4b00126f7bb0': {
    legacySpotId: 145753,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/sf-san-mateo-county/central-ocean-beach-south/',
  },
  '584204214e65fad6a7709b94': {
    legacySpotId: 119424,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/affenrucken-point/',
  },
  '584204214e65fad6a7709b95': {
    legacySpotId: 119423,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/elizabeth-bay/',
  },
  '584204204e65fad6a7709531': {
    legacySpotId: 72890,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/avon-lake/',
  },
  '584204214e65fad6a7709d26': {
    legacySpotId: 142801,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/pacific-northwest/oregon/seaside/',
  },
  '584204204e65fad6a7709ab0': {
    legacySpotId: 115819,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/bat-yam/',
  },
  '584204204e65fad6a7709ab1': {
    legacySpotId: 115818,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/dolphinarium/',
  },
  '584204204e65fad6a7709ab2': {
    legacySpotId: 115820,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/rishon/',
  },
  '584204204e65fad6a7709ab3': {
    legacySpotId: 115823,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/hakshatot/',
  },
  '5842041f4e65fad6a7708893': {
    legacySpotId: 4754,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/log-cabins/',
  },
  '5842041f4e65fad6a7708894': {
    legacySpotId: 4752,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/off-the-wall/',
  },
  '584204204e65fad6a7709ab6': {
    legacySpotId: 115825,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/ashkelon/',
  },
  '584204204e65fad6a7709ab7': {
    legacySpotId: 115824,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/dromi---herzlyia-marina/',
  },
  '5842041f4e65fad6a7708e19': {
    legacySpotId: 13830,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/big-island-hawaii/hawaii-kona/kahalu-u-bay/',
  },
  '584204204e65fad6a7709aaa': {
    legacySpotId: 115811,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/sidna-ali/',
  },
  '584204204e65fad6a7709aab': {
    legacySpotId: 115809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/sironit-beach/',
  },
  '584204204e65fad6a7709aac': {
    legacySpotId: 115812,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/zvulun/',
  },
  '584204204e65fad6a7709aad': {
    legacySpotId: 115813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/marina/',
  },
  '584204204e65fad6a7709aae': {
    legacySpotId: 115815,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/southern-israel/topsy/',
  },
  '584204204e65fad6a7709aaf': {
    legacySpotId: 115814,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/hazuk-beach/',
  },
  '584204204e65fad6a77092d3': {
    legacySpotId: 61428,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/queensland/gold-coast/broadbeach/',
  },
  '584204214e65fad6a7709b8b': {
    legacySpotId: 119415,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/namibia/namibia/bocock-s-bay/',
  },
  '584204204e65fad6a7709ac2': {
    legacySpotId: 116173,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/bahia-tw/',
  },
  '584204204e65fad6a7709ac3': {
    legacySpotId: 116174,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/veneros/',
  },
  '584204204e65fad6a7709ac4': {
    legacySpotId: 116182,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/burgerworld/',
  },
  '584204204e65fad6a7709ac5': {
    legacySpotId: 116184,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/bankvaults/',
  },
  '584204204e65fad6a7709ac6': {
    legacySpotId: 116186,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/e-bay/',
  },
  '584204204e65fad6a7709ac7': {
    legacySpotId: 116185,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/pitstops/',
  },
  '584204204e65fad6a7709ac8': {
    legacySpotId: 116187,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/beng-bengs/',
  },
  '584204204e65fad6a7709ac9': {
    legacySpotId: 116188,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/nipussi/',
  },
  '584204214e65fad6a7709b7f': {
    legacySpotId: 119357,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/angola/north-angola/cabo-ledo/',
  },
  '584204204e65fad6a770969a': {
    legacySpotId: 78331,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/gran-canaria/maspalomas/',
  },
  '584204204e65fad6a7709abd': {
    legacySpotId: 116145,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/hirtles/',
  },
  '584204204e65fad6a7709abe': {
    legacySpotId: 116146,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/eastern-canada/nova-scotia/cherry-hill/',
  },
  '584204204e65fad6a7709abf': {
    legacySpotId: 116172,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/el-anclote/',
  },
  '584204204e65fad6a7709a28': {
    legacySpotId: 111396,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/el-bajo/',
  },
  '584204204e65fad6a7709a26': {
    legacySpotId: 111394,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/tonsupa/',
  },
  '584204204e65fad6a7709ad0': {
    legacySpotId: 116307,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/guadeloupe/guadeloupe/petit-havre/',
  },
  '584204204e65fad6a7709ad1': {
    legacySpotId: 116308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/dominica/dominica/turd-burger-s/',
  },
  '584204204e65fad6a7709ad2': {
    legacySpotId: 116309,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/martinique/martinique/anse-couleuvre/',
  },
  '584204204e65fad6a7709a22': {
    legacySpotId: 111389,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/pedernales/',
  },
  '584204204e65fad6a7709ad4': {
    legacySpotId: 116313,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/st--lucia/st--lucia/pigeon-point/',
  },
  '584204204e65fad6a7709ad5': {
    legacySpotId: 116319,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/turks-and-caicos/turks-and-caicos/pine-cay/',
  },
  '584204204e65fad6a7709a27': {
    legacySpotId: 111395,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/northern-ecuador/balao/',
  },
  '584204204e65fad6a7709ad7': {
    legacySpotId: 116316,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/curacao/curacao/playa-kanoa/',
  },
  '584204204e65fad6a7709ad8': {
    legacySpotId: 116317,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/caribbean/curacao/curacao/klein/',
  },
  '584204204e65fad6a7709ad9': {
    legacySpotId: 116359,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/us-virgin-islands/st-thomas/sprat-bay/',
  },
  '584204204e65fad6a77095e1': {
    legacySpotId: 74027,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/vlieland/',
  },
  '584204214e65fad6a7709c2a': {
    legacySpotId: 126962,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/france-mediterranean/cannes/la-tour/',
  },
  '584204204e65fad6a770946a': {
    legacySpotId: 70181,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/conto/',
  },
  '584204204e65fad6a7709670': {
    legacySpotId: 77021,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/tay-street/',
  },
  '584204204e65fad6a7709acb': {
    legacySpotId: 116189,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/hideaways/',
  },
  '584204204e65fad6a77094c1': {
    legacySpotId: 71779,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taipei/jin-shan/',
  },
  '584204204e65fad6a7709acd': {
    legacySpotId: 116193,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/mentawais/greenbush/',
  },
  '584204204e65fad6a7709ace': {
    legacySpotId: 116306,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/guadeloupe/guadeloupe/le-moule/',
  },
  '584204204e65fad6a7709acf': {
    legacySpotId: 116305,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/guadeloupe/guadeloupe/anse-bertrand/',
  },
  '584204214e65fad6a7709c8a': {
    legacySpotId: 128411,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/finland/southern-finland/tulliniemi/',
  },
  '584204214e65fad6a7709c38': {
    legacySpotId: 127233,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/madagascar/southern-madagascar/tt-s/',
  },
  '584204204e65fad6a7709516': {
    legacySpotId: 72820,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/port-washington/',
  },
  '584204204e65fad6a7709ae0': {
    legacySpotId: 116388,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/la-barra/',
  },
  '584204204e65fad6a7709ae1': {
    legacySpotId: 116389,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/el-emir/',
  },
  '584204204e65fad6a7709ae2': {
    legacySpotId: 116390,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/parque-del-plata/',
  },
  '584204204e65fad6a7709ae3': {
    legacySpotId: 116474,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/solomon-islands/solomon-islands-south/lulu-s-left/',
  },
  '584204204e65fad6a7709ae4': {
    legacySpotId: 116475,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/solomon-islands/solomon-islands-south/dari-rights/',
  },
  '584204204e65fad6a7709ae5': {
    legacySpotId: 116476,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/solomon-islands/solomon-islands-south/kumars/',
  },
  '584204204e65fad6a7709ae6': {
    legacySpotId: 116487,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/magnet-bay/',
  },
  '584204204e65fad6a7709ae7': {
    legacySpotId: 116831,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/virginia---outer-banks/northern-outer-banks/jennette-s-pier-fish-cam/',
  },
  '584204204e65fad6a7709ae8': {
    legacySpotId: 116852,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/cape-verde/cape-verde-south/coragi/',
  },
  '584204204e65fad6a7709ae9': {
    legacySpotId: 116851,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/cape-verde/cape-verde-north/ponta-preta/',
  },
  '584204204e65fad6a770943d': {
    legacySpotId: 68647,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/racecourse/',
  },
  '5842041f4e65fad6a7708e08': {
    legacySpotId: 10880,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/bahamas/north-bahamas/indicas/',
  },
  '5842041f4e65fad6a7708881': {
    legacySpotId: 4434,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/spyglass-drive--panama-city-beach/',
  },
  '584204204e65fad6a7709485': {
    legacySpotId: 71446,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--mediterranean-/puerto-mar-la-mange-del-mar-major/',
  },
  '584204204e65fad6a7709a1e': {
    legacySpotId: 111385,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/san-mateo/',
  },
  '584204204e65fad6a7709680': {
    legacySpotId: 77037,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/st-clair/',
  },
  '584204204e65fad6a7709ada': {
    legacySpotId: 116372,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/us-virgin-islands/st-john/turner-bay/',
  },
  '584204204e65fad6a7709adb': {
    legacySpotId: 116355,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/us-virgin-islands/st-thomas/hull-bay/',
  },
  '5842041f4e65fad6a7708e05': {
    legacySpotId: 10855,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/molokai/molokai/kepuhi-bay/',
  },
  '584204204e65fad6a7709add': {
    legacySpotId: 116375,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/us-virgin-islands/st-john/john-s-folly/',
  },
  '584204204e65fad6a7709ade': {
    legacySpotId: 116385,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/la-aguada/',
  },
  '584204204e65fad6a7709adf': {
    legacySpotId: 116386,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/zanja-honda/',
  },
  '5842041f4e65fad6a770886d': {
    legacySpotId: 4406,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/maryland-delaware/8th-street--ocean-city--md/',
  },
  '5842041f4e65fad6a770886b': {
    legacySpotId: 4415,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southeast/south-carolina/folly-beach-pier-northside/',
  },
  '584204204e65fad6a7709af0': {
    legacySpotId: 116860,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/southeast-australia/posties/',
  },
  '584204204e65fad6a7709af1': {
    legacySpotId: 116859,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/southeast-australia/robe-long-beach/',
  },
  '584204204e65fad6a7709af2': {
    legacySpotId: 116863,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/pacific-islands/fiji/fiji-north/rangs/',
  },
  '584204204e65fad6a7709af3': {
    legacySpotId: 116865,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taoyuan/taoyuan-beachbreak/',
  },
  '584204204e65fad6a7709af4': {
    legacySpotId: 117008,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southwestern-sri-lanka/unawatuna/',
  },
  '584204204e65fad6a7709af5': {
    legacySpotId: 117009,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southwestern-sri-lanka/weligama-bay/',
  },
  '584204204e65fad6a7709af6': {
    legacySpotId: 117007,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southwestern-sri-lanka/galle/',
  },
  '584204204e65fad6a7709af7': {
    legacySpotId: 117010,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southwestern-sri-lanka/mirissa-beach/',
  },
  '584204204e65fad6a7709af8': {
    legacySpotId: 117011,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/punta-negra/',
  },
  '584204204e65fad6a7709af9': {
    legacySpotId: 117012,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/uruguay/uruguay/el-barco/',
  },
  '5842041f4e65fad6a770886f': {
    legacySpotId: 4418,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/a-st--st-augustine/',
  },
  '584204204e65fad6a7709514': {
    legacySpotId: 72816,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/grant-park/',
  },
  '584204204e65fad6a7709511': {
    legacySpotId: 72817,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/mckinley-beach/',
  },
  '584204204e65fad6a7709609': {
    legacySpotId: 74082,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/belgium/belgium/nieuwpoort/',
  },
  '584204204e65fad6a7709682': {
    legacySpotId: 77839,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/suffolk-county/east-hampton-beach/',
  },
  '584204204e65fad6a7709aea': {
    legacySpotId: 116853,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/cape-verde/cape-verde-south/cocos-beach-praia/',
  },
  '5842041f4e65fad6a7708a13': {
    legacySpotId: 5175,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/7-11/',
  },
  '584204204e65fad6a7709aec': {
    legacySpotId: 116854,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/sri-lanka/southwestern-sri-lanka/hikkaduwa/',
  },
  '584204204e65fad6a7709aed': {
    legacySpotId: 116857,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/eastern-victoria/red-bluff/',
  },
  '584204204e65fad6a7709aee': {
    legacySpotId: 116858,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/south-australia/eyre-peninsula/flatrock/',
  },
  '584204204e65fad6a7709aef': {
    legacySpotId: 116856,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/victoria/eastern-victoria/lakes-entrance/',
  },
  '5842041f4e65fad6a7708a0e': {
    legacySpotId: 5173,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/the-woodenjetty/',
  },
  '584204204e65fad6a7709495': {
    legacySpotId: 71463,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--mediterranean-/alicante-platja-de-san-juan/',
  },
  '584204204e65fad6a7709677': {
    legacySpotId: 77028,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/titahi-bay/',
  },
  '584204204e65fad6a770948b': {
    legacySpotId: 71452,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/platja-de-cavalleria/',
  },
  '584204204e65fad6a7709675': {
    legacySpotId: 77027,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/opunake/',
  },
  '584204204e65fad6a7709a37': {
    legacySpotId: 111461,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/termales/',
  },
  '584204204e65fad6a7709610': {
    legacySpotId: 74114,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/hoddevik/',
  },
  '584204204e65fad6a770948a': {
    legacySpotId: 71453,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/cueva-del-lobo/',
  },
  '584204204e65fad6a7709a33': {
    legacySpotId: 111456,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/juan-tornillo/',
  },
  '584204204e65fad6a7709613': {
    legacySpotId: 74116,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/solastrand/',
  },
  '584204204e65fad6a7709614': {
    legacySpotId: 74118,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/vigdel/',
  },
  '584204204e65fad6a7709a38': {
    legacySpotId: 111462,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/colombia-pacific/terco/',
  },
  '584204204e65fad6a77094b3': {
    legacySpotId: 71558,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/richards-bay/',
  },
  '584204204e65fad6a77094b4': {
    legacySpotId: 71561,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/yzerfontein/',
  },
  '584204204e65fad6a77094b5': {
    legacySpotId: 71562,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/west-coast-cape-peninsula/cape-town/',
  },
  '584204204e65fad6a7709619': {
    legacySpotId: 74123,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/brusand/',
  },
  '584204214e65fad6a7709ba7': {
    legacySpotId: 122368,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/el-salvador/el-salvador/cristo-de-salinitas/',
  },
  '584204214e65fad6a7709ba8': {
    legacySpotId: 121792,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/the-right--walpole/',
  },
  '584204214e65fad6a7709ba9': {
    legacySpotId: 122403,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/vazon-bay-guernsey/',
  },
  '584204204e65fad6a7709afa': {
    legacySpotId: 117312,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/bluff-beach/',
  },
  '584204204e65fad6a7709afb': {
    legacySpotId: 117313,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/dumpers/',
  },
  '584204204e65fad6a7709afc': {
    legacySpotId: 117314,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/paunch/',
  },
  '584204204e65fad6a7709afd': {
    legacySpotId: 117317,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/silverbacks/',
  },
  '584204204e65fad6a7709afe': {
    legacySpotId: 117318,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-caribbean/wizard-beach/',
  },
  '584204204e65fad6a770960b': {
    legacySpotId: 74088,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/hustadvika-gjesteg-rd/',
  },
  '584204204e65fad6a770960c': {
    legacySpotId: 74087,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/northern-norway/unstad-beach/',
  },
  '584204204e65fad6a770960d': {
    legacySpotId: 74112,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/fl-/',
  },
  '584204204e65fad6a770960e': {
    legacySpotId: 74111,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/alnes-lighthouse--godoy-/',
  },
  '584204204e65fad6a770960f': {
    legacySpotId: 74110,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/ulla/',
  },
  '584204204e65fad6a7709507': {
    legacySpotId: 72809,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/rosewood-beach/',
  },
  '5842041f4e65fad6a7708b00': {
    legacySpotId: 5432,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/johnson-s-beach/',
  },
  '5842041f4e65fad6a7708b01': {
    legacySpotId: 5433,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/sandy-key/',
  },
  '584204204e65fad6a7709508': {
    legacySpotId: 72808,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/in---il--michigan-/tower-road/',
  },
  '5842041f4e65fad6a7708b03': {
    legacySpotId: 5426,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/navarre-pier/',
  },
  '5842041f4e65fad6a7708b04': {
    legacySpotId: 5430,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/pickens-point-dunes/',
  },
  '5842041f4e65fad6a7708b05': {
    legacySpotId: 5429,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/slabs/',
  },
  '5842041f4e65fad6a7708b06': {
    legacySpotId: 5431,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/pickens-point/',
  },
  '5842041f4e65fad6a7708b07': {
    legacySpotId: 5435,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/panhandle/bungalows/',
  },
  '5842041f4e65fad6a7708b08': {
    legacySpotId: 5436,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/880/',
  },
  '5842041f4e65fad6a7708b09': {
    legacySpotId: 5437,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/pier-60/',
  },
  '584204214e65fad6a7709bb7': {
    legacySpotId: 122417,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/corblets-bay-alderney/',
  },
  '584204204e65fad6a770968c': {
    legacySpotId: 78311,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/las-galletas/',
  },
  '5842041f4e65fad6a77089fc': {
    legacySpotId: 5156,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/spring-lake/',
  },
  '5842041f4e65fad6a77089fb': {
    legacySpotId: 5154,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/breezy-point/',
  },
  '584204204e65fad6a77095a6': {
    legacySpotId: 73034,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/rogue-rivermouth/',
  },
  '584204214e65fad6a7709b80': {
    legacySpotId: 119395,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/ghana/ghana/cape-three-points/',
  },
  '584204204e65fad6a770961a': {
    legacySpotId: 74124,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/west-norway/bausje-strand-borhaug/',
  },
  '584204204e65fad6a7709a2b': {
    legacySpotId: 111400,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/galapagos-islands/carola/',
  },
  '584204204e65fad6a770961c': {
    legacySpotId: 74125,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/norway/southeast-norway/saltstein/',
  },
  '584204204e65fad6a770961d': {
    legacySpotId: 74562,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taitung/donghe-rivermouth/',
  },
  '584204214e65fad6a7709bab': {
    legacySpotId: 122405,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/portinfer-guernsey/',
  },
  '584204204e65fad6a770961f': {
    legacySpotId: 74565,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/kaohsiung/cijin-island/',
  },
  '584204214e65fad6a7709bad': {
    legacySpotId: 122406,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/l-ancresse-bay-guernsey/',
  },
  '584204214e65fad6a7709bae': {
    legacySpotId: 122407,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/perelle-bay-guernsey/',
  },
  '584204214e65fad6a7709baf': {
    legacySpotId: 122408,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/greve-de-lecq-jersey/',
  },
  '584204204e65fad6a7709a18': {
    legacySpotId: 111380,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/central-ecuador/las-tunas/',
  },
  '584204204e65fad6a770959f': {
    legacySpotId: 73029,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/southeast-on--ontario-/van-wagners-beach/',
  },
  '5842041f4e65fad6a7708b10': {
    legacySpotId: 5450,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/lido-beach/',
  },
  '5842041f4e65fad6a7708b11': {
    legacySpotId: 5451,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/coolidge/',
  },
  '5842041f4e65fad6a7708b12': {
    legacySpotId: 5452,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/siesta-key/',
  },
  '5842041f4e65fad6a7708b13': {
    legacySpotId: 5453,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/stickney-point/',
  },
  '5842041f4e65fad6a7708b14': {
    legacySpotId: 5454,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/turtles/',
  },
  '5842041f4e65fad6a7708b15': {
    legacySpotId: 5455,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/venice-jetties--north-/',
  },
  '5842041f4e65fad6a7708b16': {
    legacySpotId: 5456,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/venice-jetties--south-/',
  },
  '5842041f4e65fad6a7708b17': {
    legacySpotId: 5457,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/venice-pier/',
  },
  '584204214e65fad6a7709bc5': {
    legacySpotId: 122416,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/channel-islands-england/la-mare-jersey/',
  },
  '5842041f4e65fad6a7708b19': {
    legacySpotId: 5446,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/holmes-beach-pier/',
  },
  '584204214e65fad6a7709bc7': {
    legacySpotId: 125267,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/north-florida/8th-avenue-south--jacksonville-beach/',
  },
  '584204204e65fad6a7709a5c': {
    legacySpotId: 111524,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/guatemala/guatemala-pacific/ocos/',
  },
  '584204214e65fad6a7709bc9': {
    legacySpotId: 125282,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/united-arab-emirates/dubai/surfers-beach/',
  },
  '584204204e65fad6a77094a7': {
    legacySpotId: 71544,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/mzimpuni/',
  },
  '584204204e65fad6a77094a8': {
    legacySpotId: 71546,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/eastern-cape/port-alfred/',
  },
  '5842041f4e65fad6a7708b0a': {
    legacySpotId: 5438,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/sand-key/',
  },
  '5842041f4e65fad6a7708b0b': {
    legacySpotId: 5439,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/belleair/',
  },
  '5842041f4e65fad6a7708b0c': {
    legacySpotId: 5440,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/indian-rocks-beach/',
  },
  '5842041f4e65fad6a7708b0d': {
    legacySpotId: 5441,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/redington-breakwater/',
  },
  '5842041f4e65fad6a7708b0e': {
    legacySpotId: 5443,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/middle-jetty/',
  },
  '5842041f4e65fad6a7708b0f': {
    legacySpotId: 5442,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/redington-pier/',
  },
  '584204204e65fad6a7709484': {
    legacySpotId: 71445,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--mediterranean-/la-manga-del-mar-menor/',
  },
  '584204204e65fad6a7709482': {
    legacySpotId: 71443,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/fuengirola/',
  },
  '584204204e65fad6a770947a': {
    legacySpotId: 71430,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/canyamel/',
  },
  '584204204e65fad6a7709686': {
    legacySpotId: 78308,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/punta-del-hidalgo/',
  },
  '584204204e65fad6a7709683': {
    legacySpotId: 78305,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/los-platos-la-orrotava/',
  },
  '584204214e65fad6a7709d24': {
    legacySpotId: 142785,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-los-angeles/el-porto-north/',
  },
  '584204204e65fad6a7709a07': {
    legacySpotId: 111360,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/la-posada/',
  },
  '5842041f4e65fad6a7708b20': {
    legacySpotId: 5464,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/tween-waters/',
  },
  '5842041f4e65fad6a7708b21': {
    legacySpotId: 5463,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/redfish-pass/',
  },
  '5842041f4e65fad6a7708b22': {
    legacySpotId: 5462,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/boca-grande/',
  },
  '5842041f4e65fad6a7708b23': {
    legacySpotId: 5465,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/blind-pass/',
  },
  '5842041f4e65fad6a7708b24': {
    legacySpotId: 5466,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/the-wall/',
  },
  '5842041f4e65fad6a7708b25': {
    legacySpotId: 5467,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/doctor-s-pass/',
  },
  '5842041f4e65fad6a7708b26': {
    legacySpotId: 5449,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/whitney-beach/',
  },
  '5842041f4e65fad6a7708b27': {
    legacySpotId: 5468,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/doctor-s-pass--south-jetty-/',
  },
  '5842041f4e65fad6a7708b28': {
    legacySpotId: 5445,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/upham-beach-park/',
  },
  '5842041f4e65fad6a7708b29': {
    legacySpotId: 5469,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/naples-pier/',
  },
  '584204204e65fad6a7709a5e': {
    legacySpotId: 111707,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/thailand/andaman-sea/nai-harn-beach/',
  },
  '584204204e65fad6a77094a4': {
    legacySpotId: 71542,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/africa/south-africa/wild-coast-kwazulu-natal/warner-beach/',
  },
  '5842041f4e65fad6a77089d7': {
    legacySpotId: 5118,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/plaice-cove/',
  },
  '5842041f4e65fad6a7708a11': {
    legacySpotId: 5177,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/island-beach-state-park/',
  },
  '584204204e65fad6a7709a04': {
    legacySpotId: 111356,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/pelado/',
  },
  '584204214e65fad6a7709d0d': {
    legacySpotId: 138701,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/central-california/san-luis-obispo-county/morro-bay-boat-launch/',
  },
  '584204214e65fad6a7709d21': {
    legacySpotId: 142784,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/long-island/nassau---queens-county/west-end--long-beach/',
  },
  '5842041f4e65fad6a7708b1a': {
    legacySpotId: 5444,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/sunset-beach/',
  },
  '5842041f4e65fad6a7708b1b': {
    legacySpotId: 5459,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/gasparilla-point/',
  },
  '5842041f4e65fad6a7708b1c': {
    legacySpotId: 5448,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/coquina-jetties/',
  },
  '5842041f4e65fad6a7708b1d': {
    legacySpotId: 5447,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/twin-piers/',
  },
  '5842041f4e65fad6a7708b1e': {
    legacySpotId: 5460,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/englewood/',
  },
  '5842041f4e65fad6a7708b1f': {
    legacySpotId: 5461,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida-gulf/west-florida/knight-s-point/',
  },
  '584204214e65fad6a7709bcd': {
    legacySpotId: 125286,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/united-arab-emirates/dubai/jebal-ali-lefts/',
  },
  '584204214e65fad6a7709bce': {
    legacySpotId: 125295,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/karaburun/',
  },
  '584204214e65fad6a7709bcf': {
    legacySpotId: 125294,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/turkey-black-sea/nw-turkey/kastro/',
  },
  '584204214e65fad6a7709bee': {
    legacySpotId: 125757,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/los-caracas/',
  },
  '5842041f4e65fad6a7708b30': {
    legacySpotId: 5526,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-south/poipu-outer-reef/',
  },
  '5842041f4e65fad6a7708b31': {
    legacySpotId: 5527,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-south/pakala/',
  },
  '5842041f4e65fad6a7708b32': {
    legacySpotId: 5533,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/populars/',
  },
  '5842041f4e65fad6a7708b33': {
    legacySpotId: 5530,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/publics/',
  },
  '5842041f4e65fad6a7708b34': {
    legacySpotId: 5529,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/black-point/',
  },
  '5842041f4e65fad6a7708b35': {
    legacySpotId: 5532,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/canoes/',
  },
  '5842041f4e65fad6a7708b36': {
    legacySpotId: 5531,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/queen-s/',
  },
  '5842041f4e65fad6a7708b37': {
    legacySpotId: 5528,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-west/lahaina-harbor---breakwall/',
  },
  '5842041f4e65fad6a7708b38': {
    legacySpotId: 5536,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/kaiser-s/',
  },
  '5842041f4e65fad6a7708b39': {
    legacySpotId: 5537,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/rockpile/',
  },
  '584204204e65fad6a770967f': {
    legacySpotId: 78303,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/tenerife-canaries/la-caleta/',
  },
  '584204204e65fad6a7709494': {
    legacySpotId: 71466,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/playa-de-la-carihuela/',
  },
  '584204214e65fad6a7709be9': {
    legacySpotId: 125751,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/los-cocos/',
  },
  '584204204e65fad6a770967e': {
    legacySpotId: 77036,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/taylors-mistake/',
  },
  '584204204e65fad6a770967b': {
    legacySpotId: 77033,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-tasman/punakaiki/',
  },
  '584204204e65fad6a7709490': {
    legacySpotId: 71458,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/southwest-spain/el-palmar/',
  },
  '584204204e65fad6a770967a': {
    legacySpotId: 77030,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/palliser-bay/',
  },
  '5842041f4e65fad6a7708b2a': {
    legacySpotId: 5520,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-south/big-beach---little-beach/',
  },
  '5842041f4e65fad6a7708b2b': {
    legacySpotId: 5521,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/maui/maui-south/dumps/',
  },
  '5842041f4e65fad6a7708b2c': {
    legacySpotId: 5522,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-north/hanalei-bay/',
  },
  '5842041f4e65fad6a7708b2d': {
    legacySpotId: 5523,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-north/ha-ena-bay--cannons-tunnels-/',
  },
  '584204214e65fad6a7709bdb': {
    legacySpotId: 125311,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/bulgaria-black-sea/east-bulgaria/arkutino/',
  },
  '5842041f4e65fad6a7708b2f': {
    legacySpotId: 5524,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-north/anahola-bay/',
  },
  '584204204e65fad6a770967d': {
    legacySpotId: 77035,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/south-island/south-island-pacific/new-brighton/',
  },
  '584204214e65fad6a7709bde': {
    legacySpotId: 125523,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/kauai/kauai-south/pks-centers/',
  },
  '584204204e65fad6a770952d': {
    legacySpotId: 72885,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mi---oh--erie-/overlook-beach-park/',
  },
  '584204204e65fad6a770940e': {
    legacySpotId: 67105,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/bellambi/',
  },
  '584204204e65fad6a7709515': {
    legacySpotId: 72824,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/east-wi--michigan-/north-point-the-elbow/',
  },
  '584204214e65fad6a7709d04': {
    legacySpotId: 136616,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/middle-east/israel/northern-israel/israel-rovercam/',
  },
  '584204204e65fad6a77099da': {
    legacySpotId: 110418,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/praia-dos-mosteiros/',
  },
  '5842041f4e65fad6a7708b40': {
    legacySpotId: 5543,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/mendocino/mackerricher/',
  },
  '5842041f4e65fad6a7708b41': {
    legacySpotId: 5544,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/mendocino/mendocino-township/',
  },
  '5842041f4e65fad6a7708b42': {
    legacySpotId: 5538,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/ala-moana-bowls/',
  },
  '5842041f4e65fad6a7708b43': {
    legacySpotId: 5550,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/puerto-escondido-area/puerto-escondido-cam/',
  },
  '5842041f4e65fad6a7708b44': {
    legacySpotId: 5551,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/nicaragua/nicaragua---pacific-side/santana/',
  },
  '584204214e65fad6a7709bf2': {
    legacySpotId: 125797,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/isla-de-margarita/funeral-point/',
  },
  '5842041f4e65fad6a7708b46': {
    legacySpotId: 5545,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/mendocino/point-arena/',
  },
  '5842041f4e65fad6a7708b47': {
    legacySpotId: 5553,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northeast-puerto-rico/san-juan/',
  },
  '5842041f4e65fad6a7708b48': {
    legacySpotId: 5555,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/barbados/barbados-east-coast/soup-bowl/',
  },
  '5842041f4e65fad6a7708b49': {
    legacySpotId: 5554,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/caribbean/puerto-rico/northwest-puerto-rico/puntas/',
  },
  '584204204e65fad6a770964b': {
    legacySpotId: 76327,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/south-china/guandong/repulse-bay/',
  },
  '584204214e65fad6a7709bf8': {
    legacySpotId: 125813,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/sucre/chagurama/',
  },
  '584204214e65fad6a7709bf9': {
    legacySpotId: 125807,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/isla-de-margarita/guacuco/',
  },
  '584204204e65fad6a77099e0': {
    legacySpotId: 110424,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-south/santa-clara/',
  },
  '584204204e65fad6a77094c4': {
    legacySpotId: 71780,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/asia/taiwan/taipei/green-bay/',
  },
  '584204204e65fad6a7709412': {
    legacySpotId: 67109,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/north-wollongong/',
  },
  '584204204e65fad6a770949b': {
    legacySpotId: 71469,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--alboran-/playa-del-dedo/',
  },
  '5842041f4e65fad6a7708b3a': {
    legacySpotId: 5535,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/four-s/',
  },
  '5842041f4e65fad6a7708b3b': {
    legacySpotId: 5539,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/north-shore/kaena-point/',
  },
  '5842041f4e65fad6a7708b3c': {
    legacySpotId: 5540,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/windward-side/makapu-u/',
  },
  '5842041f4e65fad6a7708b3d': {
    legacySpotId: 5534,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/oahu/south-shore/three-s/',
  },
  '584204204e65fad6a77099dd': {
    legacySpotId: 110422,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/rabo-de-peixe/',
  },
  '5842041f4e65fad6a7708b3f': {
    legacySpotId: 5542,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-california/mendocino/westport/',
  },
  '584204204e65fad6a77099db': {
    legacySpotId: 109796,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/northern-mexico/riviera-nayarit-area/el-faro/',
  },
  '584204204e65fad6a77099dc': {
    legacySpotId: 110419,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-miguel-north/mosteiros-left/',
  },
  '584204214e65fad6a7709bef': {
    legacySpotId: 125754,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/venezuela/west-venezuela/playa-pantaleta/',
  },
  '584204214e65fad6a7709c64': {
    legacySpotId: 127806,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--southern-islands-/abrami-beach--naxos-/',
  },
  '5842041f4e65fad6a7708b50': {
    legacySpotId: 5561,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/playa-hermosa-cam-south/',
  },
  '584204204e65fad6a7709671': {
    legacySpotId: 77025,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-tasman/fitzroy-beach/',
  },
  '584204204e65fad6a7709672': {
    legacySpotId: 77022,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/ohope-beach/',
  },
  '5842041f4e65fad6a7708b53': {
    legacySpotId: 5566,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/pico-alto/',
  },
  '5842041f4e65fad6a7708b54': {
    legacySpotId: 5565,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/panama/panama-pacific/playa-santa-catalina/',
  },
  '5842041f4e65fad6a7708b55': {
    legacySpotId: 5567,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/peruazul/',
  },
  '5842041f4e65fad6a7708b56': {
    legacySpotId: 5568,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/southern-peru/makaha/',
  },
  '5842041f4e65fad6a7708b57': {
    legacySpotId: 5570,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain/northeast-spain/zarautz/',
  },
  '5842041f4e65fad6a7708b58': {
    legacySpotId: 5569,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/ireland/west-ireland/strandhill/',
  },
  '5842041f4e65fad6a7708b59': {
    legacySpotId: 5571,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/new-zealand/north-island/north-island-pacific/mount-beach--bay-of-plenty/',
  },
  '584204204e65fad6a77099f5': {
    legacySpotId: 110449,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/sao-jorge-north/lago-do-linho/',
  },
  '584204214e65fad6a7709c84': {
    legacySpotId: 128396,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/avalon-10-14th-street/',
  },
  '584204204e65fad6a77099f0': {
    legacySpotId: 110442,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/salga/',
  },
  '584204204e65fad6a770956c': {
    legacySpotId: 72970,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/great-lakes/mn---wi--superior-/temperance-river-mouth/',
  },
  '584204214e65fad6a7709c5c': {
    legacySpotId: 127795,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--aegean-sea-/kalamitsi/',
  },
  '584204204e65fad6a77099f3': {
    legacySpotId: 110440,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/sao-fernando/',
  },
  '584204204e65fad6a7709401': {
    legacySpotId: 67095,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/stanwell-park/',
  },
  '584204204e65fad6a770966a': {
    legacySpotId: 76714,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/canary-islands/gran-canaria/la-barra---las-canteras/',
  },
  '5842041f4e65fad6a7708b4b': {
    legacySpotId: 5557,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/indian-ocean/indonesia/bali/uluwatu/',
  },
  '5842041f4e65fad6a7708b4d': {
    legacySpotId: 5559,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/north-costa-rica/nosara-guiones/',
  },
  '5842041f4e65fad6a7708b4e': {
    legacySpotId: 5560,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/playa-hermosa/',
  },
  '5842041f4e65fad6a7708b4f': {
    legacySpotId: 5562,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/central-america/costa-rica/central-costa-rica/playa-esterillos/',
  },
  '584204204e65fad6a7709402': {
    legacySpotId: 67046,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-mexico/huatulco-area/la-jolla/',
  },
  '584204214e65fad6a7709bfe': {
    legacySpotId: 125902,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/western-australia/southwest-australia/the-box/',
  },
  '584204214e65fad6a7709bff': {
    legacySpotId: 125983,
    photoUrl: 'http://www.surfline.com/photos/#!/gallery/location/europe/iceland/iceland/grotta/',
  },
  '584204204e65fad6a770973a': {
    legacySpotId: 91611,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/asia/central-japan---east/chiba/taito/',
  },
  '584204204e65fad6a77099fc': {
    legacySpotId: 110639,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/new-england/new-hampshire---maine/acadia-national-park/',
  },
  '5842041f4e65fad6a7708b60': {
    legacySpotId: 5582,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/steger-s-beach/',
  },
  '5842041f4e65fad6a7708b61': {
    legacySpotId: 5581,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/2nd-st--to-10th-st-/',
  },
  '5842041f4e65fad6a7708b62': {
    legacySpotId: 5586,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/stockton-avenue/',
  },
  '5842041f4e65fad6a7708b63': {
    legacySpotId: 5583,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/trestles-the-rocks/',
  },
  '5842041f4e65fad6a7708b64': {
    legacySpotId: 5585,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/queen-street/',
  },
  '5842041f4e65fad6a7708b65': {
    legacySpotId: 5584,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/poverty-beach/',
  },
  '5842041f4e65fad6a7708b66': {
    legacySpotId: 5588,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/broadway-beach/',
  },
  '5842041f4e65fad6a7708b67': {
    legacySpotId: 5587,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/grant-street-beach/',
  },
  '5842041f4e65fad6a7708b68': {
    legacySpotId: 5589,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/the-cove/',
  },
  '5842041f4e65fad6a7708b69': {
    legacySpotId: 5596,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/pumphouse/',
  },
  '584204204e65fad6a7709481': {
    legacySpotId: 71444,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/peniscola/',
  },
  '584204204e65fad6a7709a05': {
    legacySpotId: 111357,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/ecuador/southern-ecuador/olas-verdes/',
  },
  '584204204e65fad6a7709416': {
    legacySpotId: 67106,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/australia/new-south-wales/southern-nsw/corrimal/',
  },
  '584204214e65fad6a7709c4b': {
    legacySpotId: 127707,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/greece/greece--ionian-sea-/kastro-point/',
  },
  '5842041f4e65fad6a7708b5a': {
    legacySpotId: 5575,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/north-street-to-3rd-street-jetty/',
  },
  '5842041f4e65fad6a7708b5b': {
    legacySpotId: 5576,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/7th-street/',
  },
  '5842041f4e65fad6a7708b5c': {
    legacySpotId: 5578,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/36th-street-to-42nd-street/',
  },
  '5842041f4e65fad6a7708b5d': {
    legacySpotId: 5577,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/sumner-avenue/',
  },
  '5842041f4e65fad6a7708b5e': {
    legacySpotId: 5579,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/26th-to-30th-street-pier/',
  },
  '5842041f4e65fad6a7708b5f': {
    legacySpotId: 5580,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/mid-atlantic/new-jersey/nun-s-beach/',
  },
  '584204204e65fad6a7709491': {
    legacySpotId: 71461,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/spain-mediterranean/spain--balearic-/masnou/',
  },
  '584204204e65fad6a77099d9': {
    legacySpotId: 110197,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/zona-oeste-portugal/santa-cruz/',
  },
  '584204204e65fad6a77099d8': {
    legacySpotId: 110195,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/portugal/lisboa-portugal/castelo/',
  },
  '584204214e65fad6a7709c70': {
    legacySpotId: 127979,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/peru/north-peru/punta-panico/',
  },
  '5842041f4e65fad6a7708b70': {
    legacySpotId: 5603,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/kite-beach/',
  },
  '5842041f4e65fad6a7708b71': {
    legacySpotId: 5605,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/boynton-beach-inlet--southside-/',
  },
  '5842041f4e65fad6a7708b72': {
    legacySpotId: 5604,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/lake-worth-pier/',
  },
  '5842041f4e65fad6a7708b73': {
    legacySpotId: 5606,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/hobe-sound-public-beach/',
  },
  '5842041f4e65fad6a7708b75': {
    legacySpotId: 5608,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/stuart-public-beach/',
  },
  '5842041f4e65fad6a7708b76': {
    legacySpotId: 5609,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/blind-creek-park/',
  },
  '5842041f4e65fad6a7708b77': {
    legacySpotId: 5610,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/jupiter-inlet/',
  },
  '5842041f4e65fad6a7708b78': {
    legacySpotId: 5611,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/juno-pier/',
  },
  '584204204e65fad6a7709a4d': {
    legacySpotId: 111485,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/south-america/colombia/caribbean-colombia/las-gaviotas/',
  },
  '584204214e65fad6a7709cf5': {
    legacySpotId: 135292,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/england/north-east-england/lowestoft/',
  },
  '5842041f4e65fad6a7708b6a': {
    legacySpotId: 5597,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/sunrise/',
  },
  '5842041f4e65fad6a7708b6b': {
    legacySpotId: 5598,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/breakers/',
  },
  '5842041f4e65fad6a7708b6c': {
    legacySpotId: 5599,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/charlie-s-crab/',
  },
  '5842041f4e65fad6a7708b6d': {
    legacySpotId: 5600,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/ocean-reef-park/',
  },
  '5842041f4e65fad6a7708b6f': {
    legacySpotId: 5602,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/florida/treasure-coast---palm-beach/civic-center/',
  },
  '584204204e65fad6a77099ee': {
    legacySpotId: 110437,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/santa-catarina/',
  },
  '584204204e65fad6a77099ec': {
    legacySpotId: 110436,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/azores/terceira-northeast/praia-da-vitoria/',
  },
  '584204204e65fad6a77095f9': {
    legacySpotId: 74055,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/europe/netherlands/netherlands/domburg/',
  },
  '5842041f4e65fad6a7708957': {
    legacySpotId: 4981,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/ventura/pitas-point/',
  },
  '5842041f4e65fad6a7708b80': {
    legacySpotId: 5752,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/north-texas/matagorda/',
  },
  '5842041f4e65fad6a7708b81': {
    legacySpotId: 5753,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/central-texas/st--joe-s-island/',
  },
  '5842041f4e65fad6a7708b82': {
    legacySpotId: 5756,
    photoUrl:
      'http://www.surfline.com/photos/#!/gallery/location/north-america/texas/central-texas/j-p--luby-surf-park/',
  },
};
