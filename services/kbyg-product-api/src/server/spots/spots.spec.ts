import getBatchSpotsHandlerSpec from './getBatchSpotsHandler.spec';
import getNearbySpotsHandlerSpec from './getNearbySpotsHandler.spec';
import getNearbySpotsGeoHandlerSpec from './getNearbySpotsGeoHandler.spec';
import getSpotReportHandlerSpec from './getSpotReportHandler.spec';
import getTopSpotsHandlerSpec from './getTopSpotsHandler.spec';
import getPopularSpotsHandlerSpec from './getPopularSpotsHandler.spec';
import forecastsSpec from './forecasts/forecasts.spec';
import allowableForecastRangeSpec from './helpers/allowableForecastRange.spec';
import buildSunlightTimesSpec from './helpers/buildSunlightTimes.spec';
import getClosestValidStartTimestampSpec from './helpers/getClosestValidStartTimestamp.spec';
import getSpotForecastsSpec from './helpers/getSpotForecasts.spec';
import getUrlFromLiesInTaxonomySpec from './helpers/getUrlFromLiesInTaxonomy.spec';
import getWetsuitOfTheDaySSTSpec from './helpers/getWetsuitOfTheDaySST.spec';
import reverseSortBeachViewSpec from './helpers/reverseSortBeachView.spec';
import spotReportAvailableSpec from './helpers/spotReportAvailable.spec';
import getWetsuitRecommendationSpec from './helpers/getWetsuitRecommendation.spec';

describe('/spots', () => {
  getBatchSpotsHandlerSpec();
  getNearbySpotsHandlerSpec();
  getNearbySpotsGeoHandlerSpec();
  getSpotReportHandlerSpec();
  forecastsSpec();

  describe('helpers', () => {
    allowableForecastRangeSpec();
    buildSunlightTimesSpec();
    getClosestValidStartTimestampSpec();
    getSpotForecastsSpec();
    getUrlFromLiesInTaxonomySpec();
    getWetsuitOfTheDaySSTSpec();
    getWetsuitRecommendationSpec();
    reverseSortBeachViewSpec();
    spotReportAvailableSpec();
    getTopSpotsHandlerSpec();
    getPopularSpotsHandlerSpec();
  });
});
