import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import spots from './spots';
import spotReportViewsBatchFixture from './fixtures/spotReportViewsBatch.json';
import * as getSpotReportViewsIn from '../../model/SpotReportView/getSpotReportViewsIn';

const getBatchSpotsHandlerSpec = () => {
  describe('/batch', () => {
    let log;
    let request;

    /** @type {sinon.SinonStub} */
    let getSpotReportViewsInStub;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(async () => {
      getSpotReportViewsInStub = sinon
        .stub(getSpotReportViewsIn, 'default')
        .resolves(spotReportViewsBatchFixture);
    });

    afterEach(() => {
      getSpotReportViewsInStub.restore();
    });

    it('should return Spot Report Views for a batch of SpotIds', (done) => {
      const body = {
        spotids: ['584204204e65fad6a77091aa', '5842041f4e65fad6a77088ea'],
      };
      const units = {
        swellHeight: 'M',
        temperature: 'C',
        tideHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      };

      request
        .post('/batch')
        .send(body)
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.property('associated');
          expect(res.body.associated.units).to.deep.equal(units);
          expect(res.body.data).to.have.length(2);
          done();
        });
    });

    it('should return Spot Report View conditions with old ratings for a batch of SpotIds by default', async () => {
      const body = {
        spotids: ['584204204e65fad6a77091aa', '5842041f4e65fad6a77088ea'],
      };

      const res = await request.post('/batch').send(body);

      expect(res).to.have.status(200);
      expect(res.body.data[0].conditions.value).to.equal('VERY_GOOD');
      expect(res.body.data[1].conditions.value).to.equal('FLAT');
    });

    it('should return Spot Report View conditions with old ratings for a batch of SpotIds when sevenRatings param is false', async () => {
      const body = {
        spotids: ['584204204e65fad6a77091aa', '5842041f4e65fad6a77088ea'],
      };

      const res = await request.post('/batch?sevenRatings=false').send(body);

      expect(res).to.have.status(200);
      expect(res.body.data[0].conditions.value).to.equal('VERY_GOOD');
      expect(res.body.data[1].conditions.value).to.equal('FLAT');
    });

    it('should return Spot Report View conditions with new ratings for a batch of SpotIds when sevenRatings param is true', async () => {
      const body = {
        spotids: ['584204204e65fad6a77091aa', '5842041f4e65fad6a77088ea'],
      };

      const res = await request.post('/batch?sevenRatings=true').send(body);

      expect(res).to.have.status(200);
      expect(res.body.data[0].conditions.value).to.equal('GOOD');
      expect(res.body.data[1].conditions.value).to.equal('VERY_POOR');
    });
  });
};

export default getBatchSpotsHandlerSpec;
