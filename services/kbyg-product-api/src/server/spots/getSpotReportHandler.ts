import { slugify } from '@surfline/web-common';
import moment from 'moment-timezone';
import { getTaxonomy, getSubregion } from '../../external/spots';
import { getSpotReport, getRegionalReport } from '../../external/reports';
import { getSpotReportView } from '../../model/SpotReportView';
import getGravatarURL from '../../utils/getGravatarURL';
import { spot as spotBreadcrumbs } from '../../utils/breadcrumbs';
import { toUnixTimestamp } from '../../utils/datetime';
import utcOffsetFromTimezone from '../../utils/utcOffsetFromTimezone';
import { WEATHER_ICON_PATH } from '../../common/constants';
import getWetsuitOfTheDaySST from './helpers/getWetsuitOfTheDaySST';
import spotReportAvailable from './helpers/spotReportAvailable';
import getUrlFromLiesInTaxonomy from './helpers/getUrlFromLiesInTaxonomy';
import localPhotosData from './stores/localPhotos';
import limitThirdPartyData from './helpers/limitThirdPartyData';
import { getWetsuitRecommendation } from './helpers/getWetsuitRecommendation';
import { convertConditionsRating } from '../../utils/convertTo7Ratings';

const getSpotReportHandler = async (req, res) => {
  const {
    spot,
    query: { spotId, thirdParty },
  } = req;
  const sevenRatings = req.query.sevenRatings === 'true';

  let reports;
  let spotReportView;
  let taxonomy;

  try {
    [reports, spotReportView, taxonomy] = await Promise.all([
      getSpotReport(spotId),
      getSpotReportView(spotId, req.units),
      getTaxonomy(spotId, 'spot'),
    ]);
  } catch (err) {
    return res.status(400).send({ message: 'Valid spotId required' });
  }

  let report = reports.spotReports.length > 0 ? reports.spotReports[0] : null;

  if (!spotReportAvailable(spot, report)) {
    const regionalReports = await getRegionalReport(spot.subregion);
    report =
      regionalReports.regionalReports.length > 0 && !regionalReports.regionalReports[0].stale
        ? regionalReports.regionalReports[0]
        : null;
  }
  const subregion = spot.subregion ? await getSubregion(spot.subregion) : null;

  const breadcrumb = taxonomy ? spotBreadcrumbs(taxonomy.in) : [];

  const forecastStatus = spot.subregionForecastStatus || 'off';

  const associatedIds = {
    spotId: spotReportView.legacyId ? spotReportView.legacyId.toString() : spotReportView._id,
    subregionId: spotReportView.legacyRegionId
      ? spotReportView.legacyRegionId.toString()
      : spotReportView.subregionId,
  };

  const chartsUrl = subregion
    ? `/surf-charts/wave-height/${slugify(subregion.name)}/${subregion._id}`
    : null;

  const liesInTaxonomy = taxonomy.in.filter(({ _id }) => taxonomy.liesIn.includes(_id));
  const beachesUrl = getUrlFromLiesInTaxonomy(liesInTaxonomy, 'geoname', 'travel');
  const subregionUrl = getUrlFromLiesInTaxonomy(liesInTaxonomy, 'subregion', 'www');
  const spotUrl = taxonomy.associated.links.find((link) => link.key === 'www');

  const utcOffset = utcOffsetFromTimezone(spot.timezone);
  const abbrTimezone = moment().tz(spot.timezone).format('z');

  const localPhotosUrl = localPhotosData[spotId] ? localPhotosData[spotId].photoUrl : null;

  const { units, scopes } = req;

  const travelDetails =
    spot.travelDetails && spot.travelDetails.status === 'PUBLISHED' ? spot.travelDetails : {};

  const spotReport = {
    associated: {
      localPhotosUrl,
      chartsUrl,
      beachesUrl,
      subregionUrl,
      href: spotUrl ? spotUrl.href : null,
      units,
      advertising: {
        ...associatedIds,
        sst: getWetsuitOfTheDaySST(spotReportView.waterTemp.max, units.temperature),
      },
      analytics: { ...associatedIds },
      weatherIconPath: WEATHER_ICON_PATH,
      utcOffset,
      abbrTimezone,
    },
    // TODO: Deprecate this. Left for backwards compatibility.
    units,
    spot: {
      name: spotReportView.name,
      breadcrumb,
      lat: spotReportView.lat,
      lon: spotReportView.lon,
      cameras: spotReportView.cameras,
      subregion: {
        _id: spot.subregion || null,
        forecastStatus,
      },
      abilityLevels: spotReportView.abilityLevels,
      boardTypes: spotReportView.boardTypes,
      metaDescription: spot.metaDescription,
      titleTag: spot.titleTag,
      travelDetails,
      // TODO: Deprecate this. This is to support buoys and charts
      legacyId: spotReportView.legacyId,
      legacyRegionId: spotReportView.legacyRegionId,
    },
    forecast: {
      note: spotReportView.note && spotReportView.note.length ? spotReportView.note : null,
      conditions: sevenRatings
        ? convertConditionsRating(spotReportView.conditions)
        : spotReportView.conditions,
      wind: spotReportView.wind,
      waveHeight: spotReportView.waveHeight,
      tide: spotReportView.tide,
      waterTemp: spotReportView.waterTemp,
      wetsuit: getWetsuitRecommendation(spotReportView.waterTemp, units.temperature),
      weather: spotReportView.weather,
      swells: spotReportView.swells,
    },
    report: report
      ? {
          timestamp: toUnixTimestamp(new Date(report.createdAt)),
          forecaster: {
            name: report.forecasterName,
            iconUrl: getGravatarURL(report.forecasterEmail),
            title: report.forecasterTitle,
          },
          // spot report use regionalReport, regional report uses body
          body: report.regionalReport || report.report,
        }
      : null,
  };

  if (thirdParty) return res.send(limitThirdPartyData(spotReport, scopes));

  return res.send(spotReport);
};

export default getSpotReportHandler;
