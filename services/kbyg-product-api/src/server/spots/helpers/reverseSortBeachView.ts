import { head, last } from 'lodash/fp';
import { LatLonSpherical } from 'geodesy';

/**
 * Determines if the sort order needs to be reversed for beach
 * view perspective based on sign of k component of cross product.
 * @param  sortedSpots Sorted spots (lat, lon)
 * @param  primarySpot Primary spot for the subregion (offshoreDirection)
 * @return Null if offshore direction not found
 */
const reverseSortBeachView = (
  sortedSpots: { lat: number; lon: number }[],
  primarySpot?: { offshoreDirection?: number },
): boolean => {
  if (!primarySpot || (!primarySpot.offshoreDirection && primarySpot.offshoreDirection !== 0))
    return null;

  const start = head(sortedSpots);
  const end = last(sortedSpots);

  const startLatLonSpherical = new LatLonSpherical(start.lat, start.lon);
  const endLatLonSpherical = new LatLonSpherical(end.lat, end.lon);

  const sortBearing = startLatLonSpherical.bearingTo(endLatLonSpherical);
  const offshoreBearing = primarySpot.offshoreDirection;

  const sortDirection = -(sortBearing - 90) * (Math.PI / 180);
  const offshoreDirection = -(offshoreBearing - 90) * (Math.PI / 180);

  const sortVector = [Math.cos(sortDirection), Math.sin(sortDirection)];
  const offshoreVector = [Math.cos(offshoreDirection), Math.sin(offshoreDirection)];

  const normalVectorKComponent =
    offshoreVector[0] * sortVector[1] - offshoreVector[1] * sortVector[0];

  return normalVectorKComponent > 0;
};

export default reverseSortBeachView;
