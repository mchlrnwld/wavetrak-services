import { expect } from 'chai';
import reverseSortBeachView from './reverseSortBeachView';

const reverseSortBeachViewSpec = () => {
  describe('reverseSortBeachView', () => {
    const sortedSpots = [
      { lat: 33.738, lon: -118.109 },
      { lat: 0, lon: 0 },
      { lat: 33.559, lon: -117.821 },
    ];

    it('should return true if sort order needs to be reversed for beach view', () => {
      expect(reverseSortBeachView(sortedSpots, { offshoreDirection: 204 })).to.be.true();
      expect(reverseSortBeachView(sortedSpots, { offshoreDirection: 24 })).to.be.false();
    });

    it('should return false if primary spot not provided or primary spot has no offshore direction', () => {
      expect(reverseSortBeachView(sortedSpots)).to.be.null();
      expect(reverseSortBeachView(sortedSpots, {})).to.be.null();
    });
  });
};

export default reverseSortBeachViewSpec;
