import moment from 'moment';
import {
  createUTCOffsetCalculator,
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  sunriseSunset,
} from '../../../utils/datetime';

const buildSunlightTimes = ({ start, end, timezone, lat, lon }) => {
  const utcOffsetCalculator = createUTCOffsetCalculator(timezone);
  const startMidnight = midnightOfDate(timezone, fromUnixTimestamp(start));
  const numDays =
    moment(end * 1000)
      .tz(timezone)
      .diff(startMidnight, 'days') + 1;

  // We want to use noon to avoid falling on hours where a time-change has occured that may mess
  // with the date that we output
  // Related to the following issue: https://github.com/moment/moment/issues/4743
  const startNoon = moment(startMidnight).add(12, 'hours');

  const sunlightTimes = [...Array(numDays)].map((_, i) => {
    const dayTimestamp = moment(startNoon).add(i, 'days');
    const dayMidnight = midnightOfDate(timezone, dayTimestamp.toDate());
    const midnight = moment(dayMidnight).tz(timezone).toDate();

    // Use noon instead of midnight to ensure correct date
    const noon = moment(midnight).add(12, 'hours').toDate();
    const { dawn, sunrise, sunset, dusk } = sunriseSunset(noon, lat, lon);
    return {
      midnight: toUnixTimestamp(midnight),
      midnightUTCOffset: utcOffsetCalculator(midnight),
      dawn: toUnixTimestamp(dawn),
      dawnUTCOffset: utcOffsetCalculator(dawn),
      sunrise: toUnixTimestamp(sunrise),
      sunriseUTCOffset: utcOffsetCalculator(sunrise),
      sunset: toUnixTimestamp(sunset),
      sunsetUTCOffset: utcOffsetCalculator(sunset),
      dusk: toUnixTimestamp(dusk),
      duskUTCOffset: utcOffsetCalculator(dusk),
    };
  });
  return sunlightTimes;
};

export default buildSunlightTimes;
