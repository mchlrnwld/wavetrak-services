import { isPremium } from '../../../external/entitlements';
import {
  secondsPerDay,
  secondsPerHour,
  toUnixTimestamp,
  midnightOfDate,
} from '../../../utils/datetime';
import { isMeteredPremium } from '../../../utils/isMeteredPremium';

const allowableForecastRange = async (timezone, userId?, user?) => {
  const minStart = toUnixTimestamp(midnightOfDate(timezone, new Date()));
  const premium = (await isPremium(userId)) || (await isMeteredPremium(user));

  // Seconds
  const interval = (premium ? 1 : 3) * secondsPerHour;
  // Add one extra data point to include midnight of the day after the end
  const maxTimespan = (premium ? 17 : 3) * secondsPerDay + interval;
  const maxEnd = minStart + maxTimespan;
  return { minStart, maxEnd, interval };
};

export default allowableForecastRange;
