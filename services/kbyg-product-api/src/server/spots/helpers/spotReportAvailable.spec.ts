import { expect } from 'chai';
import spotReportAvailable from './spotReportAvailable';

const spotReportAvailableSpec = () => {
  describe('spotReportAvailable', () => {
    it('returns false if no report', () => {
      const spot = {
        humanReport: {
          status: 'ON',
        },
      };
      expect(spotReportAvailable(spot, null)).to.be.false();
    });

    it('returns false if report is stale', () => {
      const spot = {
        humanReport: {
          status: 'ON',
        },
      };
      const report = {
        stale: true,
      };
      expect(spotReportAvailable(spot, report)).to.be.false();
    });

    it('returns false if spot human report status is OFF', () => {
      const spot = {
        humanReport: {
          status: 'OFF',
        },
      };
      const report = {
        stale: false,
      };
      expect(spotReportAvailable(spot, report)).to.be.false();
    });

    it('returns true otherwise', () => {
      const spot = {
        humanReport: {
          status: 'ON',
        },
      };
      const report = {
        stale: false,
      };
      expect(spotReportAvailable(spot, report)).to.be.true();
    });
  });
};

export default spotReportAvailableSpec;
