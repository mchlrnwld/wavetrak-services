import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import { secondsPerHour } from '../../../utils/datetime';
import * as forecasts from '../../../external/forecasts/forecasts';
import * as getTides from '../../../external/tides';
import getSpotForecasts from './getSpotForecasts';

const getSpotForecastsSpec = () => {
  describe('getSpotForecasts', () => {
    const tideStation = 'Huntington Cliffs near Newport Bay';
    const start = 1486179320;
    const end = 1486438520;
    const interval = 3600;
    const units = deepFreeze({
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KPH',
    });
    const waveForecast = deepFreeze({
      data: [
        {
          timestamp: 1486179320,
          wind: { speed: 3.01, direction: 346 },
          surf: { min: 6.23, max: 7.22 },
        },
        {
          timestamp: 1486190120,
          wind: { speed: 3.18, direction: 243.2 },
          surf: { min: 6.23, max: 7.55 },
        },
      ],
    });
    const windForecast = deepFreeze({
      data: [
        {
          timestamp: 1486179320,
          wind: { speed: 3.9, direction: 150.9 },
          surf: { min: 5.58, max: 6.56 },
        },
        {
          timestamp: 1486190120,
          wind: { speed: 2.15, direction: 134.75 },
          surf: { min: 6.23, max: 7.22 },
        },
      ],
    });
    const weatherForecast = deepFreeze({
      data: [
        {
          timestamp: 1486179320,
          weather: { temperature: 74.1, condition: 'Partly Cloudy' },
        },
        {
          timestamp: 1486190120,
          weather: { temperature: 75.7, condition: 'Clear Skies' },
        },
      ],
    });
    const waveForecastMap = new Map(waveForecast.data.map((f) => [f.timestamp, f]));
    const weatherForecastMap = new Map(weatherForecast.data.map((f) => [f.timestamp, f]));
    const windForecastMap = new Map(windForecast.data.map((f) => [f.timestamp, f]));
    const tides = deepFreeze({
      associated: {
        tideLocation: {
          name: 'Huntington Cliffs near Newport Bay',
          min: -3.54,
          max: 9.06,
          lon: -117.998,
          lat: 33.6603,
          mean: 2.76,
        },
      },
      tides: [
        { timestamp: 1486157720, type: 'NORMAL' },
        { timestamp: 1486168520, type: 'HIGH' },
        { timestamp: 1486179320, type: 'LOW' },
        { timestamp: 1486190120, type: 'NORMAL' },
        { timestamp: 1486200920, type: 'HIGH' },
      ],
    });

    let getWaveForecastsStub;
    let getWeatherForecastsStub;
    let getWindForecastsStub;
    let getTidesStub;

    beforeEach(() => {
      getWaveForecastsStub = sinon.stub(forecasts, 'getWaveForecasts');
      getWeatherForecastsStub = sinon.stub(forecasts, 'getWeatherForecasts');
      getWindForecastsStub = sinon.stub(forecasts, 'getWindForecasts');
      getTidesStub = sinon.stub(getTides, 'default');
    });

    afterEach(() => {
      getWaveForecastsStub.restore();
      getWeatherForecastsStub.restore();
      getWindForecastsStub.restore();
      getTidesStub.restore();
    });

    it('should get wave, weather, and wind forecasts and return them as a maps', async () => {
      getWaveForecastsStub.resolves(waveForecast);
      getWeatherForecastsStub.resolves(weatherForecast);
      getWindForecastsStub.resolves(windForecast);

      const spotForecasts = await getSpotForecasts({
        pointOfInterestId: 'd1c53ea2-0f77-4d5f-b64c-be16e75966cb',
        start,
        end,
        interval,
        units,
      });

      expect(spotForecasts).to.deep.equal({
        tideLocation: null,
        waveForecastMap,
        weatherForecastMap,
        windForecastMap,
        tides: null,
      });
      expect(forecasts.getWaveForecasts).to.have.been.calledOnce();
      expect(forecasts.getWaveForecasts).to.have.been.calledWithExactly({
        pointOfInterestId: 'd1c53ea2-0f77-4d5f-b64c-be16e75966cb',
        start,
        end,
        interval: 60,
        units,
      });
      expect(forecasts.getWindForecasts).to.have.been.calledWithExactly({
        pointOfInterestId: 'd1c53ea2-0f77-4d5f-b64c-be16e75966cb',
        start,
        end,
        interval: 60,
        units,
      });
      expect(getTides.default).not.to.have.been.called();
    });

    it('should get tide data starting with most recent tide if tide station provided', async () => {
      getWaveForecastsStub.resolves(waveForecast);
      getWeatherForecastsStub.resolves(weatherForecast);
      getWindForecastsStub.resolves(windForecast);
      getTidesStub.resolves(tides);

      const spotForecasts = await getSpotForecasts({
        pointOfInterestId: 'd1c53ea2-0f77-4d5f-b64c-be16e75966cb',
        tideStation,
        start,
        end,
        interval: 60,
        units,
      });

      expect(spotForecasts).to.deep.equal({
        tideLocation: tides.associated.tideLocation,
        waveForecastMap,
        weatherForecastMap,
        windForecastMap,
        tides: tides.tides.slice(1),
      });
      expect(forecasts.getWaveForecasts).to.have.been.calledOnce();
      expect(forecasts.getWaveForecasts).to.have.been.calledOnce();
      expect(getTides.default).to.have.been.calledOnce();
      expect(getTides.default).to.have.been.calledWithExactly({
        location_name: tideStation,
        start: start - 18 * secondsPerHour,
        end,
        units,
      });
    });
  });
};

export default getSpotForecastsSpec;
