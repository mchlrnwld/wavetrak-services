import { expect } from 'chai';
import getWetsuitOfTheDaySST from './getWetsuitOfTheDaySST';

const getWetsuitOfTheDaySSTSpec = () => {
  describe('getWetsuitOfTheDaySST', () => {
    it('should return Fahrenheit SST 5 degree bucket range the given SST falls in', () => {
      expect(getWetsuitOfTheDaySST(20.5)).to.equal('21-25');
      expect(getWetsuitOfTheDaySST(24.2)).to.equal('21-25');
      expect(getWetsuitOfTheDaySST(54.5)).to.equal('51-55');
      expect(getWetsuitOfTheDaySST(55.4)).to.equal('51-55');
      expect(getWetsuitOfTheDaySST(55.5)).to.equal('56-60');
    });

    it('should convert Celsius to Fahrenheit', () => {
      expect(getWetsuitOfTheDaySST(-6, 'C')).to.equal('21-25');
      expect(getWetsuitOfTheDaySST(13, 'C')).to.equal('51-55');
    });

    it('should not return a range above 96-100', () => {
      expect(getWetsuitOfTheDaySST(101)).to.equal('96-100');
    });

    it('should not return a range below 21-25', () => {
      expect(getWetsuitOfTheDaySST(20)).to.equal('21-25');
    });
  });
};

export default getWetsuitOfTheDaySSTSpec;
