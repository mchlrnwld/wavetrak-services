import { expect } from 'chai';
import sinon from 'sinon';
import { secondsPerHour, secondsPerDay } from '../../../utils/datetime';
import * as getEntitlements from '../../../external/entitlements';
import allowableForecastRange from './allowableForecastRange';
import * as MeteredAnonymousUtil from '../../../utils/isMeteredPremium';

const allowableForecastRangeSpec = () => {
  describe('allowableForecastRange', () => {
    let clock;
    let isMeteredPremiumStub;
    let isPremiumStub;

    const premiumUserId = '582495992d06cd3b71d66229';
    const freeUserId = '56fae6ff19b74318a73a7875';
    const timezone = 'America/Los_Angeles';

    // Testing with a timestamp just past midnight to
    // see if we get midnight of the correct local day
    const current = 1486258500; // 02/05/2017@01:35
    const start = 1486195200; // 02/04/2017@08:00
    const end3DaysPlus1Step =
      start + // 02/07/2017@11:00
      3 * 24 * secondsPerHour +
      3 * secondsPerHour;
    const end17DaysPlus1Step = start + 17 * secondsPerDay + secondsPerHour; // 02/21/2017@09:00

    before(() => {
      clock = sinon.useFakeTimers(current * 1000);
    });

    beforeEach(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      getEntitlements.isPremium.restore();
      isMeteredPremiumStub = sinon.stub(MeteredAnonymousUtil, 'isMeteredPremium');
      isPremiumStub = sinon.stub(getEntitlements, 'isPremium');
      isPremiumStub.resolves(false);
      isPremiumStub.withArgs(premiumUserId).resolves(true);
    });

    afterEach(() => {
      isMeteredPremiumStub.restore();
      isPremiumStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('should return a range of 3 days from the current hour at 3 hour steps for non-premium', async () => {
      const result = await allowableForecastRange(timezone, freeUserId);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end3DaysPlus1Step,
        interval: 3 * secondsPerHour,
      });
      expect(isPremiumStub).to.have.been.calledOnce();
      expect(isPremiumStub).to.have.been.calledWithExactly(freeUserId);
    });

    it('should return a range of 17 days from now at 1 hour steps for premium', async () => {
      const result = await allowableForecastRange(timezone, premiumUserId);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end17DaysPlus1Step,
        interval: secondsPerHour,
      });
      expect(isPremiumStub).to.have.been.calledOnce();
      expect(isPremiumStub).to.have.been.calledWithExactly(premiumUserId);
    });

    it('should default to non-premium if userId not provided', async () => {
      const result = await allowableForecastRange(timezone);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end3DaysPlus1Step,
        interval: 3 * secondsPerHour,
      });
    });

    it('should start from midnight of date local to the UTC offset', async () => {
      expect((await allowableForecastRange('America/Los_Angeles')).minStart).to.equal(start);
      expect((await allowableForecastRange('Asia/Kolkata')).minStart).to.equal(1486233000);
    });

    it('should return a range of 17 days from now at 1 hour steps if the anonymous session is eligible for premium access', async () => {
      isMeteredPremiumStub.resolves(true);
      const userObject = {
        id: 'anonymousId',
        premium: true,
        entitlements: ['sl_metered'],
        type: 'anonymous',
      };
      const result = await allowableForecastRange(timezone, freeUserId, userObject);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end17DaysPlus1Step,
        interval: secondsPerHour,
      });
      expect(MeteredAnonymousUtil.isMeteredPremium).to.have.been.calledOnce();
      expect(MeteredAnonymousUtil.isMeteredPremium).to.have.been.calledWithExactly(userObject);
    });
    it('should return a range of 3 days from the current hour at 3 hour steps for non-premium if the anonymous session is NOT eligible for premium access', async () => {
      isMeteredPremiumStub.resolves(false);
      const userObject = {};
      const result = await allowableForecastRange(timezone, freeUserId, userObject);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end3DaysPlus1Step,
        interval: 3 * secondsPerHour,
      });
      expect(MeteredAnonymousUtil.isMeteredPremium).to.have.been.calledOnce();
      expect(MeteredAnonymousUtil.isMeteredPremium).to.have.been.calledWithExactly(userObject);
    });
    it('should return a range of 17 days from now at 1 hour steps if the registered non premium user is eligible for premium access', async () => {
      isMeteredPremiumStub.resolves(true);
      const userObject = {
        id: premiumUserId,
        premium: true,
        entitlements: ['sl_metered'],
        type: 'registered',
      };
      const result = await allowableForecastRange(timezone, freeUserId, userObject);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end17DaysPlus1Step,
        interval: secondsPerHour,
      });
      expect(MeteredAnonymousUtil.isMeteredPremium).to.have.been.calledOnce();
      expect(MeteredAnonymousUtil.isMeteredPremium).to.have.been.calledWithExactly(userObject);
    });
  });
};

export default allowableForecastRangeSpec;
