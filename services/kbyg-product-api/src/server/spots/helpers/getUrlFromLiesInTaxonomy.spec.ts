import { expect } from 'chai';
import getUrlFromLiesInTaxonomy from './getUrlFromLiesInTaxonomy';

const getUrlFromLiesInTaxonomySpec = () => {
  describe('getUrlFromLiesInTaxonomy', () => {
    const liesInTaxonomy = [
      {
        type: 'geoname',
        associated: {
          links: [
            {
              key: 'taxonomy',
              href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5852824&type=geoname',
            },
            {
              key: 'www',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/hawaii/honolulu-county/o-ahu/pupukea/5852824',
            },
            {
              key: 'travel',
              href: 'https://sandbox.surfline.com/travel/united-states/hawaii/honolulu-county/o-ahu/pupukea-surfing-and-beaches/5852824',
            },
          ],
        },
      },
      {
        type: 'subregion',
        associated: {
          links: [
            {
              key: 'taxonomy',
              href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=58581a836630e24c44878fcb&type=subregion',
            },
            {
              key: 'api',
              href: 'http://spots-api.sandbox.surfline.com/admin/subregions/58581a836630e24c44878fcb',
            },
            {
              key: 'www',
              href: 'https://sandbox.surfline.com/surf-forecasts/north-shore/58581a836630e24c44878fcb',
            },
          ],
        },
      },
    ];

    it('should return href for link found for correct type and key', () => {
      expect(getUrlFromLiesInTaxonomy(liesInTaxonomy, 'geoname', 'travel')).to.equal(
        'https://sandbox.surfline.com/travel/united-states/hawaii/honolulu-county/o-ahu/pupukea-surfing-and-beaches/5852824',
      );
      expect(getUrlFromLiesInTaxonomy(liesInTaxonomy, 'subregion', 'www')).to.equal(
        'https://sandbox.surfline.com/surf-forecasts/north-shore/58581a836630e24c44878fcb',
      );
    });

    it('should return null if node not found', () => {
      expect(getUrlFromLiesInTaxonomy(liesInTaxonomy, 'region', 'www')).to.be.null();
    });

    it('should return null if link not found', () => {
      expect(getUrlFromLiesInTaxonomy(liesInTaxonomy, 'subregion', 'travel')).to.be.null();
    });
  });
};

export default getUrlFromLiesInTaxonomySpec;
