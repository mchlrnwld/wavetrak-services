import { secondsPerHour } from '../../../utils/datetime';
import {
  getWaveForecasts,
  getWeatherForecasts,
  getWindForecasts,
} from '../../../external/forecasts';
import getTidesData from '../../../external/tides';

const msTimespan18Hours = 18 * secondsPerHour;

const getSpotForecasts = async ({
  pointOfInterestId,
  tideStation,
  start,
  end,
  interval,
  units,
}: {
  pointOfInterestId: string;
  tideStation?: string;
  start: number;
  end: number;
  interval: number;
  units: unknown;
}) => {
  const intervalMinutes = interval / 60;

  const [waveResponse, weatherResponse, windResponse, tideResponse] = await Promise.all([
    getWaveForecasts({
      pointOfInterestId,
      start,
      end,
      interval: intervalMinutes,
      units,
    }),
    getWeatherForecasts({
      pointOfInterestId,
      start,
      end,
      interval: intervalMinutes,
      units,
    }),
    getWindForecasts({
      pointOfInterestId,
      start,
      end,
      interval: intervalMinutes,
      units,
    }),
    tideStation
      ? getTidesData({
          location_name: tideStation,
          start: start - msTimespan18Hours, // 18 hour buffer to pick up the previous significant tide
          end,
          units,
        })
      : null,
  ]);

  const windForecastMap = new Map(
    windResponse.data.map((forecast) => [forecast.timestamp, forecast]),
  );
  const weatherForecastMap = new Map(
    weatherResponse.data.map((forecast) => [forecast.timestamp, forecast]),
  );
  const waveForecastMap = new Map(
    waveResponse.data.map((forecast) => [forecast.timestamp, forecast]),
  );

  const previousTideIndex = tideResponse
    ? tideResponse.tides.findIndex((tide) => tide.timestamp >= start)
    : null;

  return {
    tideLocation: tideResponse && tideResponse.associated.tideLocation,
    waveForecastMap,
    weatherForecastMap,
    windForecastMap,
    tides: tideResponse && tideResponse.tides.slice(previousTideIndex - 1),
  };
};

export default getSpotForecasts;
