const getClosestValidStartTimestamp = (minStart, interval, requestedStart?) => {
  if (!requestedStart || requestedStart < minStart) return minStart;
  return minStart + interval * Math.floor((requestedStart - minStart) / interval);
};

export default getClosestValidStartTimestamp;
