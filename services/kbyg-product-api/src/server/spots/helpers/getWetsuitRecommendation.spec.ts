import { expect } from 'chai';
import { getWetsuitRecommendation } from './getWetsuitRecommendation';

const getWetsuitRecommendationSpec = () => {
  describe('getWetsuitRecommendation', () => {
    it('should return thickest wetsuit option', () => {
      expect(getWetsuitRecommendation({ min: 37, max: 38 }, 'F')).to.deep.equal({
        thickness: '7/6/5mm+',
        type: 'Hooded fullsuit with botties and gloves',
      });
      expect(getWetsuitRecommendation({ min: 2, max: 3 }, 'C')).to.deep.equal({
        thickness: '7/6/5mm+',
        type: 'Hooded fullsuit with botties and gloves',
      });
    });
    it('should return wetsuit option for warmest temp', () => {
      expect(getWetsuitRecommendation({ min: 78.8, max: 80 }, 'F')).to.deep.equal({
        thickness: '0mm',
        type: 'Rashguard',
      });
      expect(getWetsuitRecommendation({ min: 26, max: 28 }, 'C')).to.deep.equal({
        thickness: '0mm',
        type: 'Rashguard',
      });
    });
  });
};

export default getWetsuitRecommendationSpec;
