import { expect } from 'chai';
import { secondsPerDay, secondsPerHour } from '../../../utils/datetime';
import buildSunlightTimes from './buildSunlightTimes';

const buildSunlightTimesSpec = () => {
  describe('buildSunlightTimes', () => {
    const timezone = 'America/Los_Angeles';
    const lat = 33.654213041213;
    const lon = -118.0032588192;

    it('returns sunrise, sunset, dawn, dusk, and midnight for date range at a location', () => {
      const start = new Date('2021-03-14T02:00:00Z').getTime() / 1000; // 03/13/2021@18:00-PST
      const end = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021@00:00-PST
      const sunlightTimes = buildSunlightTimes({ start, end, timezone, lat, lon });
      expect(sunlightTimes).to.have.length(2);
      expect(sunlightTimes[0].midnight).to.equal(new Date('2021-03-13T08:00:00Z').getTime() / 1000);
      expect(sunlightTimes[0].midnightUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].dawn).to.equal(new Date('2021-03-13T13:41:13Z').getTime() / 1000);
      expect(sunlightTimes[0].dawnUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].sunrise).to.equal(new Date('2021-03-13T14:06:03Z').getTime() / 1000);
      expect(sunlightTimes[0].sunriseUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].sunset).to.equal(new Date('2021-03-14T01:59:28Z').getTime() / 1000);
      expect(sunlightTimes[0].sunsetUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].dusk).to.equal(new Date('2021-03-14T02:24:19Z').getTime() / 1000);
      expect(sunlightTimes[0].duskUTCOffset).to.equal(-8);

      expect(sunlightTimes[1].midnight).to.equal(new Date('2021-03-14T08:00:00Z').getTime() / 1000);
      expect(sunlightTimes[1].midnightUTCOffset).to.equal(-8);
      expect(sunlightTimes[1].dawn).to.equal(new Date('2021-03-14T13:39:53Z').getTime() / 1000);
      expect(sunlightTimes[1].dawnUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].sunrise).to.equal(new Date('2021-03-14T14:04:43Z').getTime() / 1000);
      expect(sunlightTimes[1].sunriseUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].sunset).to.equal(new Date('2021-03-15T02:00:14Z').getTime() / 1000);
      expect(sunlightTimes[1].sunsetUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].dusk).to.equal(new Date('2021-03-15T02:25:04Z').getTime() / 1000);
      expect(sunlightTimes[1].duskUTCOffset).to.equal(-7);
    });

    it('accounts for moving forward an hour for daylight savings time', () => {
      const start = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021@00:00-PST
      const end = new Date('2021-03-15T08:00:00Z').getTime() / 1000; // 03/15/2021@01:00-PDT
      const sunlightTimes = buildSunlightTimes({ start, end, timezone, lat, lon });
      expect(sunlightTimes).to.have.length(2);
      expect(sunlightTimes[0].midnight).to.equal(new Date('2021-03-14T08:00:00Z').getTime() / 1000);
      expect(sunlightTimes[0].midnightUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].dawn).to.equal(new Date('2021-03-14T13:39:53Z').getTime() / 1000);
      expect(sunlightTimes[0].dawnUTCOffset).to.equal(-7);
      expect(sunlightTimes[0].sunrise).to.equal(new Date('2021-03-14T14:04:43Z').getTime() / 1000);
      expect(sunlightTimes[0].sunriseUTCOffset).to.equal(-7);
      expect(sunlightTimes[0].sunset).to.equal(new Date('2021-03-15T02:00:14Z').getTime() / 1000);
      expect(sunlightTimes[0].sunsetUTCOffset).to.equal(-7);
      expect(sunlightTimes[0].dusk).to.equal(new Date('2021-03-15T02:25:04Z').getTime() / 1000);
      expect(sunlightTimes[0].duskUTCOffset).to.equal(-7);

      expect(sunlightTimes[1].midnight).to.equal(new Date('2021-03-15T07:00:00Z').getTime() / 1000);
      expect(sunlightTimes[1].midnightUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].dawn).to.equal(new Date('2021-03-15T13:38:32Z').getTime() / 1000);
      expect(sunlightTimes[1].dawnUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].sunrise).to.equal(new Date('2021-03-15T14:03:22Z').getTime() / 1000);
      expect(sunlightTimes[1].sunriseUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].sunset).to.equal(new Date('2021-03-16T02:01:00Z').getTime() / 1000);
      expect(sunlightTimes[1].sunsetUTCOffset).to.equal(-7);
      expect(sunlightTimes[1].dusk).to.equal(new Date('2021-03-16T02:25:50Z').getTime() / 1000);
      expect(sunlightTimes[1].duskUTCOffset).to.equal(-7);
    });

    it('accounts for moving back an hour for daylight savings time', () => {
      const start = new Date('2021-11-07T07:00:00Z').getTime() / 1000; // 11/07/2021@00:00-PDT
      const end = new Date('2021-11-08:08:00Z').getTime() / 1000; // 11/08/2021@00:00-PST
      const sunlightTimes = buildSunlightTimes({ start, end, timezone, lat, lon });
      expect(sunlightTimes).to.have.length(2);
      expect(sunlightTimes[0].midnight).to.equal(new Date('2021-11-07T07:00:00Z').getTime() / 1000);
      expect(sunlightTimes[0].midnightUTCOffset).to.equal(-7);
      expect(sunlightTimes[0].dawn).to.equal(new Date('2021-11-07T13:51:59Z').getTime() / 1000);
      expect(sunlightTimes[0].dawnUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].sunrise).to.equal(new Date('2021-11-07T14:18:01Z').getTime() / 1000);
      expect(sunlightTimes[0].sunriseUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].sunset).to.equal(new Date('2021-11-08T00:56:03Z').getTime() / 1000);
      expect(sunlightTimes[0].sunsetUTCOffset).to.equal(-8);
      expect(sunlightTimes[0].dusk).to.equal(new Date('2021-11-08T01:22:05Z').getTime() / 1000);
      expect(sunlightTimes[0].duskUTCOffset).to.equal(-8);

      expect(sunlightTimes[1].midnight).to.equal(new Date('2021-11-08T08:00:00Z').getTime() / 1000);
      expect(sunlightTimes[1].midnightUTCOffset).to.equal(-8);
      expect(sunlightTimes[1].dawn).to.equal(new Date('2021-11-08T13:52:52Z').getTime() / 1000);
      expect(sunlightTimes[1].dawnUTCOffset).to.equal(-8);
      expect(sunlightTimes[1].sunrise).to.equal(new Date('2021-11-08T14:18:57Z').getTime() / 1000);
      expect(sunlightTimes[1].sunriseUTCOffset).to.equal(-8);
      expect(sunlightTimes[1].sunset).to.equal(new Date('2021-11-09T00:55:17Z').getTime() / 1000);
      expect(sunlightTimes[1].sunsetUTCOffset).to.equal(-8);
      expect(sunlightTimes[1].dusk).to.equal(new Date('2021-11-09T01:21:22Z').getTime() / 1000);
      expect(sunlightTimes[1].duskUTCOffset).to.equal(-8);
    });

    it('can build sunrise and sunset date object for a single day', () => {
      const singleDayStart = 1493708400; // 05/02/17@00:00-PDT
      const singleDayEnd = 1493791200; // 05/02/17@11:00-PDT
      const singleDay = buildSunlightTimes({
        start: singleDayStart,
        end: singleDayEnd,
        timezone,
        lat,
        lon,
      });
      expect(singleDay).to.have.length(1);
      expect(singleDay[0].midnight).to.be.a.ok();
      expect(singleDay[0].sunrise).to.be.a.ok();
      expect(singleDay[0].sunset).to.be.a.ok();
      expect(singleDay[0].dawn).to.be.a.ok();
      expect(singleDay[0].dusk).to.be.a.ok();
      expect(singleDay[0].midnight % secondsPerDay).to.equal(7 * secondsPerHour);
      expect(singleDay[0].midnight).to.be.at.most(singleDayStart);
      expect(singleDay[0].midnight).to.be.at.most(singleDayEnd);
      expect(singleDay[0].midnight).to.be.closeTo(singleDayStart, secondsPerDay);
      expect(singleDay[0].midnight).to.be.closeTo(singleDayEnd, secondsPerDay);
    });

    it('can build sunrise and sunset date objects for larger time spans', () => {
      const seventeenDayStart = 1493708400; // 05/02/17@00:00-PDT
      const seventeenDayEnd = 1495126740; // 05/18/17@11:59-PDT
      const seventeenDay = buildSunlightTimes({
        start: seventeenDayStart,
        end: seventeenDayEnd,
        timezone,
        lat,
        lon,
      });
      expect(seventeenDay).to.have.length(17);
      seventeenDay.forEach((day) => {
        expect(day.midnight).to.be.ok();
        expect(day.sunrise).to.be.ok();
        expect(day.sunset).to.be.ok();
        expect(day.dawn).to.be.ok();
        expect(day.dusk).to.be.ok();
        expect(day.midnight % secondsPerDay).to.equal(7 * secondsPerHour);
      });
      expect(seventeenDay[0].midnight).to.be.at.most(seventeenDayStart);
      expect(seventeenDay[0].midnight).to.be.closeTo(seventeenDayStart, secondsPerDay);
      expect(seventeenDay[16].midnight).to.be.at.most(seventeenDayEnd);
      expect(seventeenDay[16].midnight).to.be.closeTo(seventeenDayEnd, secondsPerDay);
    });
  });
};

export default buildSunlightTimesSpec;
