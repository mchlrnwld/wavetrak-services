import { pick } from 'lodash/fp';

const limitThirdPartyData = (report, scopes = []) => {
  const premiumCamPartner = scopes && scopes.includes('cameras:read:premium-partner');
  const thirdPartydata = {
    associated: {
      units: report.associated.units,
      utcOffset: report.associated.utcOffset,
    },
    spot: {
      name: report.spot.name,
      lat: report.spot.lat,
      lon: report.spot.lon,
      subregionId: report.spot.subregion._id,
      travelDetails: {
        best: report.spot.travelDetails ? report.spot.travelDetails.best : {},
      },
      cameras:
        premiumCamPartner && report.spot.cameras
          ? report.spot.cameras.map(
              pick(['title', 'streamUrl', 'stillUrl', 'status.isDown', 'isPrerecorded']),
            )
          : undefined, // undefined props are omitted when response is sent
    },
    forecast: {
      ...report.forecast,
      swells: report.forecast.swells.map(({ height, period, direction }) => ({
        height,
        period,
        direction,
      })),
    },
  };

  delete thirdPartydata.forecast.note;

  return thirdPartydata;
};

export default limitThirdPartyData;
