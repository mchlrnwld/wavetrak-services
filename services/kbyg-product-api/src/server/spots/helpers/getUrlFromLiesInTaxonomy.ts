const getUrlFromLiesInTaxonomy = (liesInTaxonomy, type, key) => {
  const node = liesInTaxonomy.find((n) => n.type === type);
  if (!node) return null;
  const link = node.associated.links.find((l) => l.key === key);
  if (!link) return null;
  return link.href;
};

export default getUrlFromLiesInTaxonomy;
