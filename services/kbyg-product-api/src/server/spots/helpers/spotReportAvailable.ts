const spotReportAvailable = (spot, report) =>
  !!report && !report.stale && spot.humanReport.status !== 'OFF';

export default spotReportAvailable;
