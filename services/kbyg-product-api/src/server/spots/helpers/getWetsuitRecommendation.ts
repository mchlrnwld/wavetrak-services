import { convertFahrenheitToCelsius } from '../../../utils/convertUnits';

// Range values are inclusive on min and exclusive on max
const WETSUIT_TYPE_BY_SST = {
  '26-': { thickness: '0mm', type: 'Rashguard' },
  '21-26': { thickness: '1mm', type: 'Jacket' },
  '18-21': { thickness: '2mm', type: 'Springsuit' },
  '13-18': { thickness: '3/2mm', type: 'Fullsuit' },
  '10-13': { thickness: '4/3mm', type: 'Fullsuit' },
  '7-10': { thickness: '5/4mm', type: 'Hooded fullsuit' },
  '3-7': { thickness: '6/5mm', type: 'Hooded fullsuit' },
  '-3': { thickness: '7/6/5mm+', type: 'Hooded fullsuit with botties and gloves' },
};

const getWetsuitBySST = (roundedSST) => {
  const range = Object.keys(WETSUIT_TYPE_BY_SST).find((tempRange) => {
    const [min, max] = tempRange.split('-');
    if ((min === '' && roundedSST < max) || (max === '' && roundedSST >= min)) {
      return tempRange;
    }
    return roundedSST >= min && roundedSST < max && tempRange;
  });
  return WETSUIT_TYPE_BY_SST[range];
};

/**
 * @typedef {object} WaterTemp
 * @property {number} min
 * @property {number} max
 */

/**
 * @description Helper function to fetch recommended wetsuit for given
 * min/max sea surfact temperature and unit (C or F). The min temperature
 * provided is converted to celsius and compared against a lookup table to
 * find the appropriate wetsuit option.
 *
 * @param {WaterTemp} waterTemp
 * @param {string} units
 */
export const getWetsuitRecommendation = (waterTemp, units) => {
  const { min } = waterTemp;
  const roundedSST = units === 'F' ? convertFahrenheitToCelsius(min) : min;
  return getWetsuitBySST(roundedSST);
};
