import { convertCelsiusToFahrenheit } from '../../../utils/convertUnits';

const getWetsuitOfTheDaySST = (sst, units = 'F') => {
  const roundedSST = Math.round(units === 'C' ? convertCelsiusToFahrenheit(sst) : sst);
  const remainder = roundedSST % 5 || 5;
  const lowerBound = Math.max(Math.min(roundedSST + 1 - remainder, 96), 21);
  const upperBound = lowerBound + 5 - 1;
  return `${lowerBound}-${upperBound}`;
};

export default getWetsuitOfTheDaySST;
