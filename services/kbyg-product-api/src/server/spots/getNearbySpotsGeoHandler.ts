import { sortBy } from 'lodash/fp';
import { getSpotReportViewsIn } from '../../model/SpotReportView';
import { getNearbySpots } from '../../external/spots';

const getNearbySpotsGeoHandler = async (req, res) => {
  const { lat, lon, abilityLevels, boardTypes, select } = req.query;
  let { distance, limit, offset } = req.query;
  distance = distance || 20;
  limit = limit || 20;
  offset = offset || 0;

  const spots =
    (await getNearbySpots(lat, lon, abilityLevels, boardTypes, distance, limit, offset)) || [];
  const spotIds = spots.map((spot) => spot._id);
  const spotReports = await getSpotReportViewsIn(spotIds, req.units, {}, select);
  const sortedSpots = sortBy((s) => s.rank, spotReports);

  return res.send({
    associated: {
      units: req.units,
    },
    units: req.units,
    data: {
      spots: sortedSpots,
    },
  });
};

export default getNearbySpotsGeoHandler;
