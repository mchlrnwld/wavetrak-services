const getSpotDetails = async (req, res) => {
  const { spot } = req;
  return res.send({
    associated: {},
    spot: {
      name: spot.name,
    },
  });
};

export default getSpotDetails;
