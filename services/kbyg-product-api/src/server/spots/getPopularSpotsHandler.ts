import { getPopularSpots } from '../../external/spots';
import { getSpotReportViewsIn } from '../../model/SpotReportView';

const getPopularSpotsHandler = async (req, res) => {
  const { units } = req;
  const {
    query: { sort, select },
  } = req;
  let spotIds = [];
  try {
    spotIds = await getPopularSpots();
  } catch (err) {
    if (err.status === 400) return res.status(400).send({ message: err.message });
    throw err;
  }
  spotIds = spotIds.map((spot) => spot._id);
  const popularSpots = await getSpotReportViewsIn(spotIds, units, sort, select);
  return res.send({
    associated: {
      units,
    },
    data: {
      spots: popularSpots,
    },
  });
};

export default getPopularSpotsHandler;
