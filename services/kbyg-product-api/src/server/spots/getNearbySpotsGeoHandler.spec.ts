import chai, { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import express from 'express';
import { authenticateRequest, geoCountryIso } from '@surfline/services-common';
import createParamString from '../../external/createParamString';
import * as spotsAPI from '../../external/spots';
import * as getSpotReportViewsIn from '../../model/SpotReportView/getSpotReportViewsIn';
import spots from './spots';

const getNearbySpotsGeoHandlerSpec = () => {
  describe('/nearbygeo', () => {
    let log;
    let request;
    let getSpotReportViewsInStub;
    let getNearbySpotsStub;

    const spotsResponse = deepFreeze([
      {
        _id: '5842041f4e65fad6a7708827',
        name: 'HB Pier, Northside',
        lat: 33.65617490126724,
        lon: -118.00457903563552,
        rank: 12,
      },
      {
        _id: '5977abb3b38c2300127471ec',
        name: 'HB Pier, South Closeup',
        lat: 33.65544147271585,
        lon: -118.00316870212555,
        offshoreDirection: 211,
        rank: 13,
      },
      {
        _id: '5842041f4e65fad6a77088ed',
        name: 'HB Pier, Southside',
        rank: 14,
      },
      {
        _id: '58bdea400cec4200133464f0',
        name: 'HB Pier, Southside Overview',
        rank: 15,
      },
    ]);

    const spotReportViewsFull = deepFreeze([
      {
        _id: '5842041f4e65fad6a7708827',
        name: 'HB Pier, Northside',
        lat: 33.65617490126724,
        lon: -118.00457903563552,
        rank: 12,
        subregionId: '58581a836630e24c44878fd6',
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
        legacyRegionId: 2143,
        timezone: 'America/Los_Angeles',
      },
      {
        _id: '5977abb3b38c2300127471ec',
        name: 'HB Pier, South Closeup',
        lat: 33.65544147271585,
        lon: -118.00316870212555,
        offshoreDirection: 211,
        rank: 13,
        subregionId: '58581a836630e24c44878fd6',
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
        legacyRegionId: 2143,
        timezone: 'America/Los_Angeles',
      },
      {
        _id: '5842041f4e65fad6a77088ed',
        name: 'HB Pier, Southside',
        rank: 14,
        subregionId: '58581a836630e24c44878fd6',
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
        legacyRegionId: 2143,
        timezone: 'America/Los_Angeles',
      },
      {
        _id: '58bdea400cec4200133464f0',
        name: 'HB Pier, Southside Overview',
        rank: 15,
        subregionId: '58581a836630e24c44878fd6',
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
        legacyRegionId: 2143,
        timezone: 'America/Los_Angeles',
      },
    ]);

    const spotReportViewsSelect = deepFreeze([
      {
        _id: '5842041f4e65fad6a7708827',
        name: 'HB Pier, Northside',
        lat: 33.65617490126724,
        lon: -118.00457903563552,
        rank: 12,
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
      },
      {
        _id: '5977abb3b38c2300127471ec',
        name: 'HB Pier, South Closeup',
        lat: 33.65544147271585,
        lon: -118.00316870212555,
        offshoreDirection: 211,
        rank: 13,
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
      },
      {
        _id: '5842041f4e65fad6a77088ed',
        name: 'HB Pier, Southside',
        rank: 14,
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
      },
      {
        _id: '58bdea400cec4200133464f0',
        name: 'HB Pier, Southside Overview',
        rank: 15,
        waterTemp: {
          min: 14,
          max: 15,
        },
        waveHeight: {
          human: false,
          min: 0.6,
          max: 0.9,
          occasional: null,
          humanRelation: '0.6-0.9 m – knee to waist high',
          plus: false,
        },
      },
    ]);

    const spotIds = [
      '5842041f4e65fad6a7708827',
      '5977abb3b38c2300127471ec',
      '5842041f4e65fad6a77088ed',
      '58bdea400cec4200133464f0',
    ];

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(authenticateRequest(), geoCountryIso, spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      getSpotReportViewsInStub = sinon.stub(getSpotReportViewsIn, 'default');
      getNearbySpotsStub = sinon.stub(spotsAPI, 'getNearbySpots');
    });

    afterEach(() => {
      getSpotReportViewsInStub.restore();
      getNearbySpotsStub.restore();
    });

    it('should return spot report views for a lat/lon and limit', async () => {
      getNearbySpotsStub.resolves(spotsResponse);
      getSpotReportViewsInStub.resolves(spotReportViewsFull);
      const lat = 33.654;
      const lon = -118.003;
      const limit = 4;
      const userId = '56fae6ff19b74318a73a7875';
      const params = { lat, lon, limit };
      const res = await request
        .get(`/nearbygeo?${createParamString(params)}`)
        .set('X-Auth-UserId', userId)
        .set('X-Geo-Country-ISO', 'US')
        .send();
      expect(res.body.associated.units).to.deep.equal({
        swellHeight: 'M',
        temperature: 'C',
        tideHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      });
      expect(res.body.data).to.deep.equal({
        spots: [
          spotReportViewsFull[0],
          spotReportViewsFull[1],
          spotReportViewsFull[2],
          spotReportViewsFull[3],
        ],
      });
      expect(getSpotReportViewsIn.default).to.have.been.calledOnce();
      expect(getSpotReportViewsIn.default).to.have.been.calledWithExactly(
        spotIds,
        {
          swellHeight: 'M',
          temperature: 'C',
          tideHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
        {},
        undefined,
      );
    });

    it('should return spot report views for a lat/lon, limit and select', async () => {
      getNearbySpotsStub.resolves(spotsResponse);
      getSpotReportViewsInStub.resolves(spotReportViewsSelect);
      const lat = 33.654;
      const lon = -118.003;
      const limit = 4;
      const select = 'waterTemp,waveHeight';
      const userId = '56fae6ff19b74318a73a7875';
      const params = { lat, lon, limit, select };

      const res = await request
        .get(`/nearbygeo?${createParamString(params)}`)
        .set('X-Auth-UserId', userId)
        .set('X-Geo-Country-ISO', 'US')
        .send();
      expect(res.body.associated.units).to.deep.equal({
        swellHeight: 'M',
        temperature: 'C',
        tideHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      });
      expect(res.body.data).to.deep.equal({
        spots: [
          spotReportViewsSelect[0],
          spotReportViewsSelect[1],
          spotReportViewsSelect[2],
          spotReportViewsSelect[3],
        ],
      });
      expect(getSpotReportViewsIn.default).to.have.been.calledOnce();
      expect(getSpotReportViewsIn.default).to.have.been.calledWithExactly(
        spotIds,
        {
          swellHeight: 'M',
          temperature: 'C',
          tideHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
        {},
        select,
      );
    });

    it('should return 200 with an empty spot array if unable to get spots by lat lon', async () => {
      getNearbySpotsStub.resolves(null);
      const params = { lat: 0, lon: 0 };
      const res = await request.get(`/nearbygeo?${createParamString(params)}`).send();
      expect(res).to.have.status(200);
      expect(res.body.data).to.deep.equal({
        spots: [],
      });
    });
  });
};

export default getNearbySpotsGeoHandlerSpec;
