import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import createParamString from '../../external/createParamString';
import * as spotsAPI from '../../external/spots';
import * as getTopSpotReportView from '../../model/SpotReportView/getTopSpotReportViews';
import topSpotsFixture from './fixtures/topSpots.json';
import spots from './spots';

const getSpotReportHandlerSpec = () => {
  describe('/topspots', () => {
    let log;
    let request;

    /** @type {sinon.SinonStub} */
    let getTopSpotReportViewsStub;

    /** @type {sinon.SinonStub} */
    let getTaxonomyStub;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(authenticateRequest(), spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      spotsAPI.getTaxonomy.restore();
      getTaxonomyStub = sinon.stub(spotsAPI, 'getTaxonomy');
      getTopSpotReportViewsStub = sinon.stub(getTopSpotReportView, 'default');
    });

    afterEach(() => {
      getTaxonomyStub.restore();
      getTopSpotReportViewsStub.restore();
    });

    it('should return top spots for taxonomy ID', async () => {
      getTaxonomyStub.resolves({ _id: '1234556789' });
      getTopSpotReportViewsStub.resolves(topSpotsFixture);

      const params = { id: '2077456', type: 'geoname' };
      const res = await request.get(`/topspots?${createParamString(params)}`).send();
      expect(res.body).to.deep.equal({
        associated: {
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
        },
        data: {
          topSpots: topSpotsFixture,
        },
      });
    });

    it('should return 400 if unable to get taxonomy', async () => {
      getTaxonomyStub.rejects({
        message: 'Unable to find suitable taxonomy for that node',
        status: 400,
      });

      const params = { id: '12', type: 'geoname' };
      const res = await request.get(`/topspots?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        message: 'Unable to find suitable taxonomy for that node',
      });
    });
  });
};

export default getSpotReportHandlerSpec;
