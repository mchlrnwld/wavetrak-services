import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import { regionalReport, spotReport } from '../../fixtures/reports';
import utcOffsetFromTimezone from '../../utils/utcOffsetFromTimezone';
import createParamString from '../../external/createParamString';
import * as reportsAPI from '../../external/reports';
import * as spotsAPI from '../../external/spots';
import * as getSpotReportView from '../../model/SpotReportView/getSpotReportView';
import { WEATHER_ICON_PATH } from '../../common/constants';
import * as getUrlFromLiesInTaxonomy from './helpers/getUrlFromLiesInTaxonomy';
import spotReportViewFixture from './fixtures/spotReportView.json';
import spotFixture from './fixtures/spot.json';
import unitsFixture from './fixtures/units.json';
import spotTaxonomyFixture from './fixtures/spotTaxonomy.json';
import getSpotReportHandler from './getSpotReportHandler';
import spots from './spots';

const getSpotReportHandlerSpec = () => {
  describe('/reports', () => {
    let log;
    let request;
    const beachesUrl =
      'https://sandbox.surfline.com/travel/united-states/california/orange-county/huntington-beach-surfing-and-beaches/5358705';
    const subregionUrl =
      'https://sandbox.surfline.com/surf-forecasts/north-orange-county/58581a836630e24c44878fd6';
    const spotHref =
      'https://sandbox.surfline.com/surf-report/hb-pier-southside/5977abb3b38c2300127471ec';
    const subregion = {
      _id: '58581a836630e24c44878fd6',
      name: 'North Orange County',
    };
    let clock;
    const now = 1577908800000; // 01/01/2020 12:00pm PDT

    let getRegionalReportStub;
    let getSpotStub;
    let getSpotReportStub;
    let getSpotReportViewStub;
    let getSubregionStub;
    let getTaxonomyStub;
    let getUrlFromLiesInTaxonomyStub;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(authenticateRequest(), spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(async () => {
      /* eslint-disable @typescript-eslint/ban-ts-comment */
      // @ts-ignore
      reportsAPI.getRegionalReport.restore();
      // @ts-ignore
      reportsAPI.getSpotReport.restore();
      // @ts-ignore
      spotsAPI.getSpot.restore();
      // @ts-ignore
      spotsAPI.getSubregion.restore();
      // @ts-ignore
      spotsAPI.getTaxonomy.restore();
      /* eslint-enable @typescript-eslint/ban-ts-comment */
      getRegionalReportStub = sinon.stub(reportsAPI, 'getRegionalReport');
      getSpotReportStub = sinon.stub(reportsAPI, 'getSpotReport');
      getSpotStub = sinon.stub(spotsAPI, 'getSpot');
      getSubregionStub = sinon.stub(spotsAPI, 'getSubregion');
      getTaxonomyStub = sinon.stub(spotsAPI, 'getTaxonomy');
      getSpotReportViewStub = sinon.stub(getSpotReportView, 'default');
      getUrlFromLiesInTaxonomyStub = sinon.stub(getUrlFromLiesInTaxonomy, 'default');
      getUrlFromLiesInTaxonomyStub.onCall(0).returns(beachesUrl);
      getUrlFromLiesInTaxonomyStub.onCall(1).returns(subregionUrl);
      clock = sinon.useFakeTimers(now);
    });

    afterEach(() => {
      getRegionalReportStub.restore();
      getSpotReportStub.restore();
      getSubregionStub.restore();
      getTaxonomyStub.restore();
      getSpotReportViewStub.restore();
      getUrlFromLiesInTaxonomyStub.restore();
      clock.restore();
    });

    it('should return spot report and political locations for spot ID', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getSpotReportStub.resolves(spotReport);
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res.body).to.deep.equal({
        associated: {
          chartsUrl: '/surf-charts/wave-height/north-orange-county/58581a836630e24c44878fd6',
          localPhotosUrl:
            'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/hb-pier--southside/',
          href: spotHref,
          beachesUrl,
          subregionUrl,
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
          advertising: {
            spotId: spotReportViewFixture.legacyId.toString(),
            subregionId: spotReportViewFixture.legacyRegionId.toString(),
            sst: '66-70',
          },
          analytics: {
            spotId: spotReportViewFixture.legacyId.toString(),
            subregionId: spotReportViewFixture.legacyRegionId.toString(),
          },
          weatherIconPath: WEATHER_ICON_PATH,
          utcOffset: utcOffsetFromTimezone('America/Los_Angeles'),
          abbrTimezone: 'PST',
        },
        units: {
          swellHeight: 'M',
          temperature: 'C',
          tideHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
        spot: {
          name: spotReportViewFixture.name,
          breadcrumb: [
            {
              name: 'United States',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
              name: 'California',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524',
              name: 'Orange County',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/huntington-beach/5358705',
              name: 'Huntington Beach',
            },
          ],
          lat: spotReportViewFixture.lat,
          lon: spotReportViewFixture.lon,
          cameras: spotReportViewFixture.cameras,
          subregion: {
            _id: spotReportViewFixture.subregion._id,
            forecastStatus: spotReportViewFixture.subregion.forecastStatus,
          },
          abilityLevels: spotReportViewFixture.abilityLevels,
          boardTypes: spotReportViewFixture.boardTypes,
          legacyId: spotReportViewFixture.legacyId,
          legacyRegionId: spotReportViewFixture.legacyRegionId,
          travelDetails: spotFixture.travelDetails,
        },
        forecast: {
          note: null,
          conditions: spotReportViewFixture.conditions,
          wind: spotReportViewFixture.wind,
          waveHeight: spotReportViewFixture.waveHeight,
          tide: spotReportViewFixture.tide,
          waterTemp: spotReportViewFixture.waterTemp,
          wetsuit: { thickness: '2mm', type: 'Springsuit' },
          weather: spotReportViewFixture.weather,
          swells: spotReportViewFixture.swells,
        },
        report: {
          timestamp: 1486417388,
          forecaster: {
            name: 'Johnny Tsunami',
            iconUrl: 'https://www.gravatar.com/avatar/212a0bc3f4da04290f3e54bac53f7597?d=mm',
            title: 'Surfline Employee',
          },
          body: '<p><strong>Testing Reg Rep change.</strong></p>\n<p><br></p>\n<p><strong>A little bit changed. A little bit more.</strong></p>',
        },
      });
    });

    it('should return spot report with old ratings scale in forecast.conditions by default', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getSpotReportStub.resolves(spotReport);
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res.body.forecast.conditions).to.deep.equal({
        ...spotReportViewFixture.conditions,
        value: 'VERY_GOOD',
      });
    });

    it('should return spot report with old ratings scale in forecast.conditions if sevenRatings param is false', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getSpotReportStub.resolves(spotReport);
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088ed', sevenRatings: false };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res.body.forecast.conditions).to.deep.equal({
        ...spotReportViewFixture.conditions,
        value: 'VERY_GOOD',
      });
    });

    it('should return spot report with new ratings scale in forecast.conditions if sevenRatings param is true', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getSpotReportStub.resolves(spotReport);
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088ed', sevenRatings: true };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res.body.forecast.conditions).to.deep.equal({
        ...spotReportViewFixture.conditions,
        value: 'GOOD',
      });
    });

    it('should return a simplified third party spot report given a spot ID', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getSpotReportStub.resolves(spotReport);
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088ed', thirdParty: true };
      const res = await request.get(`/reports?${createParamString(params)}`).send();

      expect(res.body).to.deep.equal({
        associated: {
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
          utcOffset: utcOffsetFromTimezone('America/Los_Angeles'),
        },
        spot: {
          name: spotReportViewFixture.name,
          lat: spotReportViewFixture.lat,
          lon: spotReportViewFixture.lon,
          subregionId: spotReportViewFixture.subregionId,
          travelDetails: { best: {} },
        },
        forecast: {
          conditions: spotReportViewFixture.conditions,
          wind: spotReportViewFixture.wind,
          waveHeight: spotReportViewFixture.waveHeight,
          tide: spotReportViewFixture.tide,
          waterTemp: spotReportViewFixture.waterTemp,
          wetsuit: { thickness: '2mm', type: 'Springsuit' },
          weather: spotReportViewFixture.weather,
          swells: spotReportViewFixture.swells.map(({ height, period, direction }) => ({
            height,
            period,
            direction,
          })),
        },
      });
    });

    it('should return a simplified third party spot report including cameras for premium camera partner', async () => {
      const res = { send: sinon.spy(), status: sinon.spy() };
      getSpotStub.resolves(spotFixture);
      getRegionalReportStub.resolves(regionalReport);
      getSpotReportStub.resolves({ spotReports: [] });
      getTaxonomyStub.resolves(spotTaxonomyFixture);
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getSubregionStub.resolves(subregion);

      // Note: we need to test the getSpotReportHandler middleware to simulate req.scopes
      // having the premium camera scope from the proxy
      const req = {
        spot: { ...spotFixture },
        units: { ...unitsFixture },
        query: { thirdParty: true },
        scopes: ['cameras:read:premium-partner'],
      };

      await getSpotReportHandler(req, res);

      const call1 = res.send.getCall(0).args[0];
      expect(call1).to.deep.equal({
        associated: {
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
          utcOffset: utcOffsetFromTimezone('America/Los_Angeles'),
        },
        spot: {
          name: spotReportViewFixture.name,
          lat: spotReportViewFixture.lat,
          lon: spotReportViewFixture.lon,
          subregionId: spotReportViewFixture.subregionId,
          travelDetails: { best: {} },
          cameras: [
            {
              title: 'HB Pier, Southside',
              streamUrl:
                'http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-hbpiersscam.smil/playlist.m3u8',
              stillUrl: 'http://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg',
              status: { isDown: false },
            },
          ],
        },
        forecast: {
          conditions: spotReportViewFixture.conditions,
          wind: spotReportViewFixture.wind,
          waveHeight: spotReportViewFixture.waveHeight,
          tide: spotReportViewFixture.tide,
          waterTemp: spotReportViewFixture.waterTemp,
          wetsuit: { thickness: '2mm', type: 'Springsuit' },
          weather: spotReportViewFixture.weather,
          swells: spotReportViewFixture.swells.map(({ height, period, direction }) => ({
            height,
            period,
            direction,
          })),
        },
      });
    });

    it('should return regional report if no spot report found', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getRegionalReportStub.resolves(regionalReport);
      getSpotReportStub.resolves({ spotReports: [] });
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();

      expect(res.body).to.deep.equal({
        associated: {
          chartsUrl: '/surf-charts/wave-height/north-orange-county/58581a836630e24c44878fd6',
          localPhotosUrl:
            'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/north-orange-county/hb-pier--southside/',
          beachesUrl,
          subregionUrl,
          href: spotHref,
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
          advertising: {
            spotId: spotReportViewFixture.legacyId.toString(),
            subregionId: spotReportViewFixture.legacyRegionId.toString(),
            sst: '66-70',
          },
          analytics: {
            spotId: spotReportViewFixture.legacyId.toString(),
            subregionId: spotReportViewFixture.legacyRegionId.toString(),
          },
          weatherIconPath: WEATHER_ICON_PATH,
          utcOffset: utcOffsetFromTimezone('America/Los_Angeles'),
          abbrTimezone: 'PST',
        },
        units: {
          swellHeight: 'M',
          temperature: 'C',
          tideHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
        spot: {
          name: spotReportViewFixture.name,
          breadcrumb: [
            {
              name: 'United States',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
              name: 'California',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524',
              name: 'Orange County',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/huntington-beach/5358705',
              name: 'Huntington Beach',
            },
          ],
          lat: spotReportViewFixture.lat,
          lon: spotReportViewFixture.lon,
          cameras: spotReportViewFixture.cameras,
          subregion: spotReportViewFixture.subregion,
          abilityLevels: spotReportViewFixture.abilityLevels,
          boardTypes: spotReportViewFixture.boardTypes,
          legacyId: spotReportViewFixture.legacyId,
          legacyRegionId: spotReportViewFixture.legacyRegionId,
          travelDetails: spotFixture.travelDetails,
        },
        forecast: {
          note: null,
          conditions: spotReportViewFixture.conditions,
          wind: spotReportViewFixture.wind,
          waveHeight: spotReportViewFixture.waveHeight,
          tide: spotReportViewFixture.tide,
          waterTemp: spotReportViewFixture.waterTemp,
          wetsuit: { thickness: '2mm', type: 'Springsuit' },
          weather: spotReportViewFixture.weather,
          swells: spotReportViewFixture.swells,
        },
        report: {
          body: '<p><strong>Afternoon Report for North OC:</strong> Old W-WNW swell mix continues into the afternoon with mainly knee-waist+ waves at exposures and occasional shoulder high sets for top breaks.</p>',
          forecaster: {
            iconUrl: 'https://www.gravatar.com/avatar/b73d6bfcc16e75b12553f698c79493fd?d=mm',
            name: 'Aaron Bernstein',
            title: 'Product Owner',
          },
          timestamp: 1487982928,
        },
      });
    });

    it('should return null report if no spot or regional report found', async () => {
      getSpotStub.resolves(spotFixture);
      getSubregionStub.resolves(subregion);
      getRegionalReportStub.resolves({ regionalReports: [] });
      getSpotReportStub.resolves({ spotReports: [] });
      getSpotReportViewStub.resolves(spotReportViewFixture);
      getTaxonomyStub.resolves(spotTaxonomyFixture);

      const params = { spotId: '5842041f4e65fad6a77088e3' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res.body).to.deep.equal({
        associated: {
          chartsUrl: '/surf-charts/wave-height/north-orange-county/58581a836630e24c44878fd6',
          localPhotosUrl:
            'http://www.surfline.com/photos/#!/gallery/location/north-america/southern-california/south-orange-county/rockpile/',
          beachesUrl,
          subregionUrl,
          href: spotHref,
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
          advertising: {
            spotId: spotReportViewFixture.legacyId.toString(),
            subregionId: spotReportViewFixture.legacyRegionId.toString(),
            sst: '66-70',
          },
          analytics: {
            spotId: spotReportViewFixture.legacyId.toString(),
            subregionId: spotReportViewFixture.legacyRegionId.toString(),
          },
          weatherIconPath: WEATHER_ICON_PATH,
          utcOffset: utcOffsetFromTimezone('America/Los_Angeles'),
          abbrTimezone: 'PST',
        },
        units: {
          swellHeight: 'M',
          temperature: 'C',
          tideHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
        spot: {
          name: spotReportViewFixture.name,
          breadcrumb: [
            {
              name: 'United States',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
              name: 'California',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524',
              name: 'Orange County',
            },
            {
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/huntington-beach/5358705',
              name: 'Huntington Beach',
            },
          ],
          lat: spotReportViewFixture.lat,
          lon: spotReportViewFixture.lon,
          cameras: spotReportViewFixture.cameras,
          subregion: spotReportViewFixture.subregion,
          abilityLevels: spotReportViewFixture.abilityLevels,
          boardTypes: spotReportViewFixture.boardTypes,
          legacyId: spotReportViewFixture.legacyId,
          legacyRegionId: spotReportViewFixture.legacyRegionId,
          travelDetails: spotFixture.travelDetails,
        },
        forecast: {
          note: null,
          conditions: spotReportViewFixture.conditions,
          wind: spotReportViewFixture.wind,
          waveHeight: spotReportViewFixture.waveHeight,
          tide: spotReportViewFixture.tide,
          waterTemp: spotReportViewFixture.waterTemp,
          wetsuit: { thickness: '2mm', type: 'Springsuit' },
          weather: spotReportViewFixture.weather,
          swells: spotReportViewFixture.swells,
        },
        report: null,
      });
    });

    it('should return 400 if unable to get spot', async () => {
      getSubregionStub.resolves(subregion);
      getSpotStub.resolves(null);
      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
    });

    it('should return 400 if unable to get spot report', async () => {
      getSubregionStub.resolves(subregion);
      getSpotReportStub.rejects();
      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
    });

    it('should return 400 if unable to get spot report view', async () => {
      getSubregionStub.resolves(subregion);
      getSpotReportViewStub.rejects();

      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/reports?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
    });
  });
};

export default getSpotReportHandlerSpec;
