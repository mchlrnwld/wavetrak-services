import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import createParamString from '../../external/createParamString';
import * as spotsAPI from '../../external/spots';
import spots from './spots';

const getSpotDetailsHandlerSpec = () => {
  describe('/details', () => {
    let log;
    let request;
    let getSpotStub;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(authenticateRequest(), spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      getSpotStub = sinon.stub(spotsAPI, 'getSpot');
    });

    afterEach(() => {
      getSpotStub.restore();
    });

    it('should return 400 if unable to get spot', async () => {
      getSpotStub.resolves(null);
      const params = { spotId: '5842041f4e65fad6a77088ed' };
      const res = await request.get(`/details?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
    });
  });
};

export default getSpotDetailsHandlerSpec;
