import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import * as spotsAPI from '../../external/spots';
import * as getSpotReportViewsIn from '../../model/SpotReportView/getSpotReportViewsIn';
import popularSpotsFixture from './fixtures/popularSpots.json';
import spots from './spots';

const getPopularSpotsHandlerSpec = () => {
  describe('/popular', () => {
    let log;
    let request;
    let getPopularSpotsStub;
    let getSpotReportViewsInStub;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(authenticateRequest(), spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(async () => {
      getPopularSpotsStub = sinon.stub(spotsAPI, 'getPopularSpots');
      getSpotReportViewsInStub = sinon.stub(getSpotReportViewsIn, 'default');
    });

    afterEach(() => {
      getPopularSpotsStub.restore();
      getSpotReportViewsInStub.restore();
    });

    it('should return popular spots', async () => {
      getPopularSpotsStub.resolves([
        {
          _id: '5842041f4e65fad6a7708890',
          name: 'Pipeline',
          hasThumbnail: true,
        },
        {
          _id: '5842041f4e65fad6a77088ed',
          name: 'HB Pier, Southside',
          hasThumbnail: true,
        },
      ]);
      getSpotReportViewsInStub.resolves(popularSpotsFixture);
      const res = await request.get('/popular').send();
      expect(res.body).to.deep.equal({
        associated: {
          units: {
            swellHeight: 'M',
            temperature: 'C',
            tideHeight: 'M',
            waveHeight: 'M',
            windSpeed: 'KPH',
          },
        },
        data: {
          spots: popularSpotsFixture,
        },
      });
    });

    it('should return 400 if unable to get popular spots', async () => {
      getPopularSpotsStub.rejects({
        message: 'Unable to find popular spots',
        status: 400,
      });

      const res = await request.get('/popular').send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Unable to find popular spots' });
    });
  });
};
export default getPopularSpotsHandlerSpec;
