import chai, { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import express from 'express';
import { geoCountryIso, authenticateRequest } from '@surfline/services-common';
import createParamString from '../../external/createParamString';
import * as spotsAPI from '../../external/spots';
import * as getSubregionSpotReportViews from '../../model/SpotReportView/getSubregionSpotReportViews';
import { northOCSpot } from '../../fixtures/spots';
import spots from './spots';

const getNearbySpotsHandlerSpec = () => {
  describe('/nearby', () => {
    let log;
    let request;
    let getSubregionSpotReportViewsStub;
    let getSpotStub;

    const spotReportViews = deepFreeze([
      {
        _id: '5842041f4e65fad6a7708820',
        name: 'Seal Beach Overview',
        lat: 33.738,
        lon: -118.109,
        rank: 0,
        conditions: {
          human: true,
          value: 'VERY_GOOD',
          sortableCondition: 3,
          expired: false,
        },
      },
      {
        _id: '5842041f4e65fad6a77088ed',
        name: 'HB Pier, Southside',
        lat: 33.559,
        lon: -117.821,
        offshoreDirection: 211,
        rank: 3,
        conditions: {
          human: true,
          value: 'FLAT',
          sortableCondition: 3,
          expired: false,
        },
      },
      {
        _id: '5842041f4e65fad6a7708827',
        name: 'HB Pier, Northside',
        rank: 1,
        conditions: {
          human: true,
          value: 'GOOD_TO_EPIC',
          sortableCondition: 3,
          expired: false,
        },
      },
      {
        _id: '584204204e65fad6a770998c',
        name: 'Huntington State Beach',
        rank: 2,
        conditions: {
          human: true,
          value: 'VERY_GOOD',
          sortableCondition: 3,
          expired: false,
        },
      },
    ]);

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(authenticateRequest(), geoCountryIso, spots(log));
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      spotsAPI.getSpot.restore();
      getSpotStub = sinon.stub(spotsAPI, 'getSpot');
      getSubregionSpotReportViewsStub = sinon.stub(getSubregionSpotReportViews, 'default');
    });

    afterEach(() => {
      getSubregionSpotReportViewsStub.restore();
      getSpotStub.restore();
    });

    it("should return spot report views for a spot's subregion ID", async () => {
      getSpotStub.resolves(northOCSpot);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      const spotId = '5842041f4e65fad6a77088ed';
      const userId = '56fae6ff19b74318a73a7875';
      const params = { spotId };
      const res = await request
        .get(`/nearby?${createParamString(params)}`)
        .set('X-Auth-UserId', userId)
        .set('X-Geo-Country-ISO', 'US')
        .send();
      expect(res.body.associated.units).to.deep.equal({
        swellHeight: 'M',
        temperature: 'C',
        tideHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      });
      expect(res.body.associated.reverseSortBeachView).to.be.true();
      expect(res.body.data).to.deep.equal({
        spots: [spotReportViews[0], spotReportViews[2], spotReportViews[3], spotReportViews[1]],
      });
      expect(getSubregionSpotReportViewsStub).to.have.been.calledOnce();
      expect(getSubregionSpotReportViewsStub).to.have.been.calledWithExactly(
        {
          swellHeight: 'M',
          temperature: 'C',
          tideHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
        '58581a836630e24c44878fd6',
      );
    });

    it('should return spot report view conditions with old ratings scale by default', async () => {
      getSpotStub.resolves(northOCSpot);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      const spotId = '5842041f4e65fad6a77088ed';
      const userId = '56fae6ff19b74318a73a7875';
      const params = { spotId };
      const res = await request
        .get(`/nearby?${createParamString(params)}`)
        .set('X-Auth-UserId', userId)
        .set('X-Geo-Country-ISO', 'US')
        .send();

      expect(res.body.data.spots[0].conditions.value).to.equal('VERY_GOOD');
      expect(res.body.data.spots[1].conditions.value).to.equal('GOOD_TO_EPIC');
      expect(res.body.data.spots[2].conditions.value).to.equal('VERY_GOOD');
      expect(res.body.data.spots[3].conditions.value).to.equal('FLAT');
    });

    it('should return spot report view conditions with old ratings scale if sevenRatings param is false', async () => {
      getSpotStub.resolves(northOCSpot);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      const spotId = '5842041f4e65fad6a77088ed';
      const userId = '56fae6ff19b74318a73a7875';
      const params = { spotId, sevenRatings: false };
      const res = await request
        .get(`/nearby?${createParamString(params)}`)
        .set('X-Auth-UserId', userId)
        .set('X-Geo-Country-ISO', 'US')
        .send();

      expect(res.body.data.spots[0].conditions.value).to.equal('VERY_GOOD');
      expect(res.body.data.spots[1].conditions.value).to.equal('GOOD_TO_EPIC');
      expect(res.body.data.spots[2].conditions.value).to.equal('VERY_GOOD');
      expect(res.body.data.spots[3].conditions.value).to.equal('FLAT');
    });

    it('should return spot report view conditions with new ratings scale if sevenRatings param is true', async () => {
      getSpotStub.resolves(northOCSpot);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      const spotId = '5842041f4e65fad6a77088ed';
      const userId = '56fae6ff19b74318a73a7875';
      const params = { spotId, sevenRatings: true };
      const res = await request
        .get(`/nearby?${createParamString(params)}`)
        .set('X-Auth-UserId', userId)
        .set('X-Geo-Country-ISO', 'US')
        .send();

      expect(res.body.data.spots[0].conditions.value).to.equal('GOOD');
      expect(res.body.data.spots[1].conditions.value).to.equal('GOOD');
      expect(res.body.data.spots[2].conditions.value).to.equal('GOOD');
      expect(res.body.data.spots[3].conditions.value).to.equal('VERY_POOR');
    });

    it('should return 400 if unable to get spot', async () => {
      getSpotStub.resolves(null);
      const params = { spotId: 'badspotid' };
      const res = await request.get(`/nearby?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
    });
  });
};

export default getNearbySpotsHandlerSpec;
