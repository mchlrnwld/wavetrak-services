import { getTaxonomy } from '../../external/spots';
import { getTopSpotReportViews } from '../../model/SpotReportView';

const getTopSpotsHandler = async (req, res) => {
  const {
    query: { id, type },
  } = req;
  let taxonomy;
  try {
    taxonomy = await getTaxonomy(id, type);
  } catch (err) {
    if (err.status === 400) return res.status(400).send({ message: err.message });
    throw err;
  }
  const topSpots = await getTopSpotReportViews(taxonomy._id, req.units);

  return res.send({
    associated: {
      units: req.units,
    },
    data: {
      topSpots,
    },
  });
};

export default getTopSpotsHandler;
