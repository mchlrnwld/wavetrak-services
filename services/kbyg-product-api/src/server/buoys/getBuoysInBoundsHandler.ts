import { Response } from 'express';
import { getBuoysInBoundingBox } from '../../external/buoys';
import { filterEmptySwells, getStationsWithOffset } from './utils';
import translateUnits from '../../external/forecasts/translateUnits';
import { KBYGRequest } from '../types';

const getBuoysInBounds = async (req: KBYGRequest, res: Response) => {
  const { units } = req;
  const { north, south, east, west } = req.bounds;

  const swellHeight = translateUnits(units.swellHeight);

  const { stationsBoundingBox } = await getBuoysInBoundingBox(
    { north, south, east, west },
    swellHeight,
  );

  const data = getStationsWithOffset(stationsBoundingBox);
  const filteredData = filterEmptySwells(data);

  return res.send({
    associated: {
      units,
    },
    data: filteredData,
  });
};

export default getBuoysInBounds;
