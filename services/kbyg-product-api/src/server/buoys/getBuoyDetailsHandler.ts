import moment from 'moment-timezone';
import tzLookup from 'tz-lookup';
import { NotFound } from '@surfline/services-common';
import { getBuoyDetails } from '../../external/buoys';
import { createUTCOffsetCalculator, fromUnixTimestamp } from '../../utils/datetime';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
const getBuoyDetailsHandler = async (req, res) => {
  const { buoyId } = req.params;

  const {
    station: { station },
  } = await getBuoyDetails(buoyId);

  if (!station) {
    throw new NotFound('Buoy Not Found');
  }

  const { latitude, longitude, latestTimestamp } = station;

  const timezone = tzLookup(latitude, longitude);
  const abbrTimezone = moment().tz(timezone).format('z');

  const utcOffsetCalculator = createUTCOffsetCalculator(timezone);
  const utcOffset = utcOffsetCalculator(fromUnixTimestamp(latestTimestamp));

  return res.send({
    associated: {
      abbrTimezone,
    },
    data: {
      ...station,
      utcOffset,
    },
  });
};

export default getBuoyDetailsHandler;
