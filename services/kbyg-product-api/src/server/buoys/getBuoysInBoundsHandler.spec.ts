import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import createParamString from '../../external/createParamString';
import * as buoysAPI from '../../external/buoys/buoys';
import userUnits from '../middleware/userUnits';
import buoys from './buoys';
import boundsBuoysFixture from './fixtures/boundsBuoys.json';
import translateUnits from '../../external/forecasts/translateUnits';

const getBuoysInBoundsHandlerSpec = () => {
  describe('/bounds', () => {
    let log;
    let request;
    let clock;
    const start = 1493708400; // 05/02/2017@00:00PDT

    /** @type {sinon.SinonStub} */
    let getBuoysInBoundsStub;

    const south = 36.739434379448674;
    const west = -122.66647338867188;
    const north = 37.359788198380755;
    const east = -121.58157348632814;

    const params = {
      north,
      south,
      east,
      west,
    };

    const url = `/bounds?${createParamString(params)}`;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(userUnits, buoys(log));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(start * 1000);
    });

    beforeEach(() => {
      getBuoysInBoundsStub = sinon.stub(buoysAPI, 'getBuoysInBoundingBox');
    });

    afterEach(() => {
      getBuoysInBoundsStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns buoy stations within bounds with UTC offset applied to lastest station data', async () => {
      getBuoysInBoundsStub.resolves(boundsBuoysFixture);

      const units = {
        temperature: 'C',
        tideHeight: 'M',
        swellHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      };

      const res = await request.get(url).send();
      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.deep.equal(units);
      expect(res.body.data).to.be.ok();
      expect(res.body.data).to.have.length(1);
      expect(res.body.data[0].id).to.equal('7f5b875a-ca13-11eb-a34f-0242015eddd9');
      expect(res.body.data[0].name).to.equal('MONTEREY - 27NM WNW of Monterey, CA');
      expect(res.body.data[0].sourceId).to.equal('46042');
      expect(res.body.data[0].latitude).to.equal(36.785);
      expect(res.body.data[0].longitude).to.equal(-122.398);
      expect(res.body.data[0].status).to.equal('ONLINE');
      expect(res.body.data[0].abbrTimezone).to.equal('PDT');

      expect(res.body.data[0].latestData.timestamp).to.equal(1626724200);
      expect(res.body.data[0].latestData.utcOffset).to.equal(-7);
      expect(res.body.data[0].latestData.height).to.equal(1.8);
      expect(res.body.data[0].latestData.period).to.equal(8);
      expect(res.body.data[0].latestData.direction).to.equal(317);

      // 3 non-empty swells are returned from nearbyBuoysFixture
      expect(res.body.data[0].latestData.swells).to.have.length(3);
      expect(res.body.data[0].latestData.swells[0].height).to.equal(1.67443);
      expect(res.body.data[0].latestData.swells[0].period).to.equal(7.14286);
      expect(res.body.data[0].latestData.swells[0].direction).to.equal(315);

      expect(getBuoysInBoundsStub).to.have.been.calledOnceWithExactly(
        { north, south, east, west },
        translateUnits(units.swellHeight),
      );
    });
  });
};

export default getBuoysInBoundsHandlerSpec;
