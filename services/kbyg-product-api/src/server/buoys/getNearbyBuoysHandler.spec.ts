import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import createParamString from '../../external/createParamString';
import * as buoysAPI from '../../external/buoys/buoys';
import userUnits from '../middleware/userUnits';
import buoys from './buoys';
import nearbyBuoysFixture from './fixtures/nearbyBuoys.json';

const getNearbyBuoysHandlerSpec = () => {
  describe('/nearby', () => {
    let log;
    let request;
    let getNearbyBuoysStub;
    const params = {
      latitude: 33.656781041213,
      longitude: -118.0064678192,
    };
    const url = `/nearby?${createParamString(params)}`;

    let clock;
    const start = 1493708400; // 05/02/2017@00:00PDT

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(userUnits, buoys(log));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(start * 1000);
    });

    beforeEach(() => {
      getNearbyBuoysStub = sinon.stub(buoysAPI, 'getNearbyBuoys');
    });

    afterEach(() => {
      getNearbyBuoysStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns nearby buoy stations with UTC offset applied to lastest station data', async () => {
      getNearbyBuoysStub.resolves(nearbyBuoysFixture);
      const units = {
        temperature: 'C',
        tideHeight: 'M',
        swellHeight: 'M',
        waveHeight: 'M',
        windSpeed: 'KPH',
      };
      const res = await request.get(url).send();
      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.deep.equal(units);
      expect(res.body.data).to.be.ok();
      expect(res.body.data).to.have.length(1);
      expect(res.body.data[0].id).to.equal('d0e556b8-cecb-11eb-95a6-024238d3b313');
      expect(res.body.data[0].name).to.equal('San Pedro South, CA (213)');
      expect(res.body.data[0].sourceId).to.equal('46042');
      expect(res.body.data[0].latitude).to.equal(33.576);
      expect(res.body.data[0].longitude).to.equal(-118.181);
      expect(res.body.data[0].status).to.equal('ONLINE');
      expect(res.body.data[0].abbrTimezone).to.equal('PDT');

      expect(res.body.data[0].latestData.timestamp).to.equal(1626296160);
      expect(res.body.data[0].latestData.utcOffset).to.equal(-7);
      expect(res.body.data[0].latestData.height).to.equal(0.9);
      expect(res.body.data[0].latestData.period).to.equal(15);
      expect(res.body.data[0].latestData.direction).to.equal(177);

      // 4 non-empty swells are returned from nearbyBuoysFixture
      expect(res.body.data[0].latestData.swells).to.have.length(4);
      expect(res.body.data[0].latestData.swells[0].height).to.equal(0.58201);
      expect(res.body.data[0].latestData.swells[0].period).to.equal(15.38462);
      expect(res.body.data[0].latestData.swells[0].direction).to.equal(180);

      const getNearbyBuoysArgs = getNearbyBuoysStub.firstCall.args;
      expect(getNearbyBuoysArgs[0]).to.equal(33.656781041213);
      expect(getNearbyBuoysArgs[1]).to.equal(-118.0064678192);
      expect(getNearbyBuoysArgs[2]).to.equal(160);
      expect(getNearbyBuoysArgs[3]).to.deep.equal(4);
      expect(getNearbyBuoysArgs[4]).to.deep.equal('m');
    });
  });
};

export default getNearbyBuoysHandlerSpec;
