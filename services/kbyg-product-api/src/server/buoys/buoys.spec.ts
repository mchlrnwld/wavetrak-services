import getNearbyBuoysHandlerSpec from './getNearbyBuoysHandler.spec';
import getBuoyDetailsHandlerSpec from './getBuoyDetailsHandler.spec';
import getBuoyReportHandlerSpec from './getBuoyReportHandler.spec';
import getBuoysInBoundsHandlerSpec from './getBuoysInBoundsHandler.spec';
import utilsSpec from './utils.spec';

describe('/buoys', () => {
  getNearbyBuoysHandlerSpec();
  getBuoyDetailsHandlerSpec();
  getBuoyReportHandlerSpec();
  getBuoysInBoundsHandlerSpec();
  utilsSpec();
});
