import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import trackBuoyRequests from './trackBuoyRequests';
import userUnits from '../middleware/userUnits';
import getBoundsFromQuery from '../middleware/getBoundsFromQuery';
import getNearbyBuoysHandler from './getNearbyBuoysHandler';
import getBuoyDetailsHandler from './getBuoyDetailsHandler';
import getBuoyReportHandler from './getBuoyReportHandler';
import getBuoysInBoundsHandler from './getBuoysInBoundsHandler';

const buoys = (log) => {
  const api = Router();

  api.use('*', trackBuoyRequests(log));
  api.get('/nearby', userUnits, wrapErrors(getNearbyBuoysHandler));
  api.get('/details/:buoyId', userUnits, wrapErrors(getBuoyDetailsHandler));
  api.get('/report/:buoyId', userUnits, wrapErrors(getBuoyReportHandler));
  api.get('/bounds', userUnits, getBoundsFromQuery, wrapErrors(getBuoysInBoundsHandler));

  return api;
};

export default buoys;
