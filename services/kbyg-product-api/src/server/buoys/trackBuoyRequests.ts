import { NextFunction, Response } from 'express';
import { KBYGRequest } from '../types';

const trackBuoyRequests = (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
  log.trace({
    action: '/kbyg/buoys',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default trackBuoyRequests;
