import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { errorHandlerMiddleware } from '@surfline/services-common';

import buoys from './buoys';
import * as buoysAPI from '../../external/buoys/buoys';
import userUnits from '../middleware/userUnits';
import buoyDetailsFixture from './fixtures/buoyDetails.json';

const getBuoyDetailsHandlerSpec = () => {
  describe('/details', () => {
    let clock;
    let log;
    let request;
    let getBuoyDetailsStub;
    const start = 1493708400; // 05/02/2017@00:00PDT
    const url = '/details/9897640a-cecd-11eb-a2c9-024238d3b313';

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(userUnits, buoys(log));
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      app.use(errorHandlerMiddleware({ error: sinon.spy() }));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(start * 1000);
    });

    beforeEach(() => {
      getBuoyDetailsStub = sinon.stub(buoysAPI, 'getBuoyDetails');
    });

    afterEach(() => {
      getBuoyDetailsStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns buoy details data', async () => {
      getBuoyDetailsStub.resolves(buoyDetailsFixture);

      const res = await request.get(url).send();
      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.abbrTimezone).to.equal('PDT');
      expect(res.body.data).to.be.ok();
      expect(res.body.data.id).to.equal('9897640a-cecd-11eb-a2c9-024238d3b313');
      expect(res.body.data.source).to.equal('NDBC');
      expect(res.body.data.sourceId).to.equal('46053');
      expect(res.body.data.name).to.equal(
        'EAST SANTA BARBARA  - 12NM Southwest of Santa Barbara, CA',
      );
      expect(res.body.data.status).to.equal('ONLINE');
      expect(res.body.data.latitude).to.equal(34.241);
      expect(res.body.data.longitude).to.equal(-119.839);
      expect(res.body.data.latestTimestamp).to.equal(1626360600);
      expect(res.body.data.utcOffset).to.equal(-7);

      const getBuoyDetailsArgs = getBuoyDetailsStub.firstCall.args;
      expect(getBuoyDetailsArgs[0]).to.equal('9897640a-cecd-11eb-a2c9-024238d3b313');
    });

    it('returns 404 when id is not found', async () => {
      // graphql returns null station when buoyId is not found
      getBuoyDetailsStub.resolves({
        station: { station: null },
      });

      const res = await request.get('/details/9999999a-abcd-11aa-a1a1-999999a9a999').send();
      expect(res).to.have.status(404);
      expect(res.body.message).to.be.equal('Buoy Not Found');
    });
  });
};

export default getBuoyDetailsHandlerSpec;
