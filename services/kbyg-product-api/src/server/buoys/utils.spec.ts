import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';

import {
  addUTCOffsetToReadings,
  truncateReportsToLocalMidnight,
  getEpochTimestamps,
  getStationOffsetForTimestamp,
  getStationsWithOffset,
  filterEmptySwells,
} from './utils';

const utilsSpec = () => {
  describe('utils', () => {
    let clock;
    const now = 1577908800000; // 01/01/2020 12:00pm PDT
    const nowSeconds = 1577908800; // 01/01/2020 12:00pm PDT
    const nowMidnight = 1577865600000; // 01/01/2020 12:00am PDT
    const nowSecondsMidnight = 1577865600; // 01/01/2020 12:00am PDT

    const data = [{ timestamp: now }, { timestamp: now - 3600 }, { timestamp: now - 7200 }];

    // filling buoy readings array with objects containing timestamps from [now - 4 days] to now
    const buoyReadings = [];
    for (let day = 0; day < 4; day += 1) {
      // subtract a full day in seconds for each iteration of the days loop
      const currentTimestamp = nowSeconds - day * 86400;
      for (let hour = 0; hour < 24; hour += 1) {
        buoyReadings.push({
          timestamp: currentTimestamp - 3600 * hour,
        });
      }
    }

    // filling buoy readings array with objects containing timestamps from [now - 4 days] to
    // one hour before now (using nowMidnight here)
    const buoyReadingsStartingAtMidnight = [];
    for (let day = 0; day < 4; day += 1) {
      const currentTimestamp = nowSecondsMidnight - day * 86400;
      for (let hour = 0; hour < 24; hour += 1) {
        if (!(day === 0 && hour === 0)) {
          buoyReadingsStartingAtMidnight.push({
            // not including current day at midnight to simulate "no data yet from today"
            timestamp: currentTimestamp - 3600 * hour,
          });
        }
      }
    }

    beforeEach(() => {
      clock = sinon.useFakeTimers(now);
    });

    afterEach(() => {
      clock.restore();
    });

    describe('addUTCOffsetToReadings', () => {
      it('should add utcOffset to each reading object', () => {
        const readings = addUTCOffsetToReadings(data, 'America/Los_Angeles');

        expect(readings[0].utcOffset).to.be.equal(-8);
        expect(readings[1].utcOffset).to.be.equal(-8);
        expect(readings[2].utcOffset).to.be.equal(-8);
      });
    });

    describe('truncateReportsToLocalMidnight', () => {
      it('should return data starting at now - days at midnight', () => {
        const readings = truncateReportsToLocalMidnight(buoyReadings, 3, 'America/Los_Angeles');
        const max = readings.reduce((prev, current) =>
          prev.timestamp > current.timestamp ? prev : current,
        );
        const min = readings.reduce((prev, current) =>
          prev.timestamp < current.timestamp ? prev : current,
        );
        // Wednesday, January 1, 2020 12:00:00 PM
        expect(max.timestamp).to.be.equal(nowSeconds);
        // Monday, December 30, 2019 12:00:00 AM
        expect(min.timestamp).to.be.equal(1577692800);
        // current reading
        expect(readings).to.deep.include({ timestamp: nowSeconds });
      });

      // In the case where no data has been returned for today yet (requested at midnight)
      // The purpose of this test is to make sure that when `x` days are requested,
      // only `x` unique days will be returned.
      it('should return data starting from 3 days ago at midnight up until the end of yesterday', () => {
        // simulate current time as Jan 1, 2020 at 12:00 AM (midnight)
        clock = sinon.useFakeTimers(nowMidnight);
        const readings = truncateReportsToLocalMidnight(
          buoyReadingsStartingAtMidnight,
          3,
          'America/Los_Angeles',
        );
        const max = readings.reduce((prev, current) =>
          prev.timestamp > current.timestamp ? prev : current,
        );
        const min = readings.reduce((prev, current) =>
          prev.timestamp < current.timestamp ? prev : current,
        );
        // Wednesday, January 1, 2020 12:00:00 AM
        expect(max.timestamp).to.be.lessThan(nowSecondsMidnight);
        // Sunday, December 29, 2019 12:00:00 AM
        expect(min.timestamp).to.be.equal(1577606400);
        // expect readings to start at midnight from 3 days ago and *not* include "now"
        expect(readings).to.not.deep.include({ timestamp: nowSecondsMidnight });
      });
    });

    describe('getEpochTimestamps', () => {
      it('should return start and end unix timestamps based on now, and now - days + 1', () => {
        const { start, end } = getEpochTimestamps(3);
        // 4 days (60 * 60 * 24 * 4)
        expect(end - start).to.be.equal(345600);
      });
    });

    describe('getStationOffsetForTimestamp', () => {
      it('should return the abbrTimezone and utcOffset', () => {
        const { abbrTimezone, utcOffset } = getStationOffsetForTimestamp(
          { latitude: 34.052235, longitude: -118.243683 },
          +new Date(),
        );

        expect(abbrTimezone).to.be.equal('PST');
        expect(utcOffset).to.be.equal(-8);
      });

      it('should return null utcOffset when no timestamp is given', () => {
        const { abbrTimezone, utcOffset } = getStationOffsetForTimestamp(
          { latitude: 34.052235, longitude: -118.243683 },
          null,
        );

        expect(abbrTimezone).to.be.equal('PST');
        expect(utcOffset).to.be.equal(null);
      });
    });

    describe('getStationsWithOffset', () => {
      it('should return the abbrTimezone and utcOffset', () => {
        const [result] = getStationsWithOffset([
          {
            station: { latitude: 34.052235, longitude: -118.243683 },
            latestData: { timestamp: +new Date() },
          },
        ]);

        expect(result.abbrTimezone).to.be.equal('PST');
        expect(result.latestData.utcOffset).to.be.equal(-8);
      });
      it('should not add utcOffset to latestData if latestData is null', () => {
        const [result] = getStationsWithOffset([
          {
            station: { latitude: 34.052235, longitude: -118.243683 },
            latestData: null,
          },
        ]);

        expect(result.abbrTimezone).to.be.equal('PST');
        expect(result.latestData).to.equal(null);
      });
    });

    describe('filterEmptySwells', () => {
      it('should return the swells array with empty swells filtered out', () => {
        const swells = filterEmptySwells([
          {
            swells: [
              { height: 0, period: 0, direction: 0 },
              { height: 0, period: 0, direction: 0 },
              { height: 0, period: 0, direction: 0 },
              { height: null, period: null, direction: null },
              { height: null, period: null, direction: null },
              { height: null, period: null, direction: null },
            ],
          },
        ]);

        expect(swells[0].swells).to.have.length(3);
      });
    });
  });
};

export default utilsSpec;
