import moment from 'moment-timezone';
import tzLookup from 'tz-lookup';
import { inRange, isNumber } from 'lodash';
import { APIError, NotFound } from '@surfline/services-common';
import { getBuoyReport } from '../../external/buoys';
import {
  addUTCOffsetToReadings,
  filterEmptySwells,
  getEpochTimestamps,
  truncateReportsToLocalMidnight,
} from './utils';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
const getBuoyReportHandler = async (req, res) => {
  const { units } = req;
  const { buoyId } = req.params;
  // Optional query param - if days is not passed in, single reading will ultimately be returned.
  const days = Number(req.query.days);
  // Consider `days` valid if no query param has been passed in.
  const isValidDays = !req.query.days || (isNumber(days) && inRange(days, 1, 4));

  if (!isValidDays) {
    throw new APIError(
      'Invalid Parameters: Valid days range required - must be integer between 1 and 3',
    );
  }

  // If `start` and `end` are returned as null (days parameter not included in original request),
  // pass null `start` and `end` values to gql query to fetch single reading.
  const { start, end } = getEpochTimestamps(days);

  const {
    station: { station, data },
  } = await getBuoyReport(buoyId, start, end, units);

  if (!station) {
    throw new NotFound('Buoy Not Found');
  }

  const { latitude, longitude } = station;

  const timezone = tzLookup(latitude, longitude);
  const abbrTimezone = moment().tz(timezone).format('z');

  const readingsTruncated = truncateReportsToLocalMidnight(data, days, timezone);
  const readingsFiltered = filterEmptySwells(readingsTruncated);
  const readings = addUTCOffsetToReadings(readingsFiltered, timezone);

  return res.send({
    associated: {
      abbrTimezone,
      units,
    },
    data: readings,
  });
};

export default getBuoyReportHandler;
