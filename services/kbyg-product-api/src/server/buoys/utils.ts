import moment from 'moment-timezone';
import tzLookup from 'tz-lookup';
import { createUTCOffsetCalculator, fromUnixTimestamp, midnightOfDate } from '../../utils/datetime';

/**
 * @typedef {object} Reading
 * @property {number} timestamp
 * @property {object} swells
 */

/**
 * @typedef {object} Station
 * @property {object} station
 * @property {number} station.latitude
 * @property {number} station.longitude
 * @property {object} latestData
 * @property {number} latestData.timestamp
 */

/**
 * @description Helper function to remove any swell object in `swells` array
 * that has all null values for height, period, and direction. The function
 * can access the `swells` array as long as the array is in `reading` or
 * `reading.latestData` within each index of the `data` param.
 *
 * @param {Array<Reading> | Array<Station>} data
 */
export const filterEmptySwells = (data) => {
  const swellNotNull = ({ height, period, direction }) =>
    [height, period, direction].map(Number.isFinite).filter(Boolean).length > 0;

  return data.map((reading) => {
    if (reading.swells) {
      return {
        ...reading,
        swells: reading.swells.filter(swellNotNull),
      };
    }
    if (reading.latestData && reading.latestData.swells) {
      return {
        ...reading,
        latestData: {
          ...reading.latestData,
          swells: reading.latestData.swells.filter(swellNotNull),
        },
      };
    }
    return reading;
  });
};

/**
 * @param {Array<Reading>} data
 * @param {string} timezone
 */
export const addUTCOffsetToReadings = (data, timezone) => {
  const utcOffsetCalculator = createUTCOffsetCalculator(timezone);

  return data.map((reading) => {
    const utcOffset = utcOffsetCalculator(fromUnixTimestamp(reading.timestamp));
    return { ...reading, utcOffset };
  });
};

/**
 * @param {Array<Reading>} data
 * @param {number} days
 * @param {string} timezone
 */
export const truncateReportsToLocalMidnight = (data, days, timezone) => {
  // If `days` is null (query param not passed in), return `data` as-is (no truncating).
  if (days) {
    // using timezone from gql response, find midnight on the day of [now - `days`]
    const start = moment(
      midnightOfDate(
        timezone,
        moment()
          .subtract(days - 1, 'days')
          .toDate(),
      ),
    ).unix();
    // Since we do not have the station latitude and longitude available before we
    // make the data fetch we cannot compute the timezone until after the fetch has
    // completed. Because of this we overfetch the data and truncate it after the
    // fact to end at midnight of the last day
    const filteredReadings = data.filter((reading) => reading.timestamp >= start);

    // If the /reports request is made exactly at midnight and no readings are in
    // `data` from the new current day, extend lookback time and re-truncate the
    // data to one more day prior. This ensures that there will always be `days`
    // number of unqiue dates included in the response.

    // response should include between (((`days` - 1) * 24) + 1) and (`days` * 24) readings
    if (filteredReadings.length <= (days - 1) * 24) {
      const startWithoutCurrentDay = moment(
        midnightOfDate(timezone, moment().subtract(days, 'days').toDate()),
      ).unix();
      // re-truncate original `data` array using earlier start timestamp
      return data.filter((reading) => reading.timestamp >= startWithoutCurrentDay);
    }
    return filteredReadings;
  }
  return data;
};

/**
 * @description Helper function to return `start` and `end` unix timestamps based on `days` input.
 * If the `days` parameter is passed in as a valid integer from 1 to 3, this function will calculate
 * `end` as the current unix timestamp, and `start` as another unix timestamp that is [ `days` + 1 ]
 * days behind the current timestamp. An extra day is added to the lookback since the response will
 * need to be truncated to 12am according to the accompanying timezone that is returned.
 *
 * @param {number} days
 */
export const getEpochTimestamps = (days) => {
  // If `days` is null (query param not passed in), return null for start and end.
  if (days) {
    return {
      start: moment()
        .subtract(days + 1, 'days')
        .unix(),
      end: moment().unix(),
    };
  }
  return { start: null, end: null };
};

/**
 * @param {Station} station
 * @param {number} timestamp
 */
export const getStationOffsetForTimestamp = (station, timestamp) => {
  const timezone = tzLookup(station.latitude, station.longitude);
  const abbrTimezone = moment().tz(timezone).format('z');
  const utcOffsetCalculator = createUTCOffsetCalculator(timezone);
  // If null timestamp is passed in, do not compute utcOffset
  const utcOffset = timestamp && utcOffsetCalculator(fromUnixTimestamp(timestamp));

  return { abbrTimezone, utcOffset };
};

/**
 * @param {Array<Station>} stations
 */
export const getStationsWithOffset = (stations) =>
  stations.map(({ station, latestData }) => {
    // Pass null timestamp if latestData is null
    const { abbrTimezone, utcOffset } = getStationOffsetForTimestamp(
      station,
      latestData ? latestData.timestamp : null,
    );

    return {
      ...station,
      abbrTimezone,
      latestData: latestData ? { ...latestData, utcOffset } : null,
    };
  });
