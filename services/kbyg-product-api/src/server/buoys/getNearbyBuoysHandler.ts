import { getNearbyBuoys } from '../../external/buoys';
import translateUnits from '../../external/forecasts/translateUnits';
import { filterEmptySwells, getStationsWithOffset } from './utils';

const NEARBY_BUOYS_DISTANCE = 160; // radius distance in km
const NEARBY_BUOYS_LIMIT = 4; // max number of buoy stations returned

const getNearbyBuoysHandler = async (req, res) => {
  const { units } = req;
  const { latitude, longitude, distance, limit } = req.query;
  if (!latitude || !longitude) {
    return res.status(400).send({ message: 'Valid latitude and longitude required' });
  }

  const { stationsRadius } = await getNearbyBuoys(
    parseFloat(latitude),
    parseFloat(longitude),
    distance ? parseFloat(distance) : NEARBY_BUOYS_DISTANCE,
    limit ? parseFloat(limit) : NEARBY_BUOYS_LIMIT,
    translateUnits(units.swellHeight),
  );

  const data = getStationsWithOffset(stationsRadius);
  const filteredData = filterEmptySwells(data);

  return res.send({
    associated: {
      units,
    },
    data: filteredData,
  });
};

export default getNearbyBuoysHandler;
