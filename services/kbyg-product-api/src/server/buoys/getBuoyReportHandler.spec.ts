import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { errorHandlerMiddleware } from '@surfline/services-common';
import buoys from './buoys';
import * as buoysAPI from '../../external/buoys/buoys';
import userUnits from '../middleware/userUnits';
import buoyReportFixture from './fixtures/buoyReport.json';

const getBuoyReportHandlerSpec = () => {
  describe('/report', () => {
    let clock;
    let log;
    let request;
    let getBuoyReportStub;

    const start = 1493708400; // 05/02/2017@00:00PDT
    const url = '/report/9897640a-cecd-11eb-a2c9-024238d3b313';

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(userUnits, buoys(log));
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      app.use(errorHandlerMiddleware({ error: sinon.spy() }));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(start * 1000);
    });

    beforeEach(() => {
      getBuoyReportStub = sinon.stub(buoysAPI, 'getBuoyReport');
    });

    afterEach(() => {
      getBuoyReportStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns buoy report data - latest reading', async () => {
      getBuoyReportStub.resolves(buoyReportFixture);

      const res = await request.get(url).send();
      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.abbrTimezone).to.equal('PDT');
      expect(res.body.associated.units).to.be.ok();
      expect(res.body.associated.units.temperature).to.equal('C');
      expect(res.body.associated.units.tideHeight).to.equal('M');
      expect(res.body.associated.units.swellHeight).to.equal('M');
      expect(res.body.associated.units.waveHeight).to.equal('M');
      expect(res.body.associated.units.windSpeed).to.equal('KPH');

      expect(res.body.data).to.be.ok();
      expect(res.body.data).to.have.length(1);
      expect(res.body.data[0].timestamp).to.equal(1626796200);
      expect(res.body.data[0].height).to.equal(0.9);
      expect(res.body.data[0].period).to.equal(6);
      expect(res.body.data[0].direction).to.equal(271);
      expect(res.body.data[0].waterTemperature).to.equal(18.6);
      expect(res.body.data[0].airTemperature).to.equal(16.4);
      expect(res.body.data[0].dewPoint).to.equal(16);
      expect(res.body.data[0].pressure).to.equal(1017.4);

      expect(res.body.data[0].wind.gust).to.equal(21.6);
      expect(res.body.data[0].wind.speed).to.equal(18);
      expect(res.body.data[0].wind.direction).to.equal(220);

      // 4 non-empty swells are returned from nearbyBuoysFixture
      expect(res.body.data[0].swells).to.have.length(4);
      expect(res.body.data[0].swells[0].height).to.equal(0.76855);
      expect(res.body.data[0].swells[0].period).to.equal(5.55556);
      expect(res.body.data[0].swells[0].direction).to.equal(270);

      const getBuoyReportArgs = getBuoyReportStub.firstCall.args;
      expect(getBuoyReportArgs[0]).to.equal('9897640a-cecd-11eb-a2c9-024238d3b313');
    });

    it('returns 404 when id is not found', async () => {
      getBuoyReportStub.resolves({
        station: { station: null },
      });

      const res = await request.get('/report/9999999a-abcd-11aa-a1a1-999999a9a999').send();
      expect(res).to.have.status(404);
      expect(res.body.message).to.be.equal('Buoy Not Found');
    });

    it('returns 400 when invalid days parameter is passed', async () => {
      getBuoyReportStub.resolves(buoyReportFixture);

      const res = await request
        .get('/report/9897640a-cecd-11eb-a2c9-024238d3b313?days=notANumber')
        .send();
      expect(res).to.have.status(400);
      expect(res.body.message).to.be.equal(
        'Invalid Parameters: Valid days range required - must be integer between 1 and 3',
      );
    });
  });
};

export default getBuoyReportHandlerSpec;
