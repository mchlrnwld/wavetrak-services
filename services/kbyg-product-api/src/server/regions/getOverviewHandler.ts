import { sortBy } from 'lodash/fp';
import moment from 'moment-timezone';
import { slugify } from '@surfline/web-common';
import { getSubregionSpotReportViews } from '../../model/SpotReportView';
import { getTaxonomy, getSpot } from '../../external/spots';
import { getSubregionForecasts } from '../../external/subregions';
import getGravatarURL from '../../utils/getGravatarURL';
import { subregion as subregionBreadcrumb } from '../../utils/breadcrumbs';
import utcOffsetFromTimezone from '../../utils/utcOffsetFromTimezone';
import { convertConditionsRating } from '../../utils/convertTo7Ratings';

const getOverviewHandler = async (req, res) => {
  const { units, subregion } = req;
  const { premium } = req.user;
  const sevenRatings = req.query.sevenRatings === 'true';

  // Get spots for subregion with conditions and waveHeight data
  const subregionSpots = await getSubregionSpotReportViews(units, subregion._id);
  const spots = subregionSpots.map(
    ({ _id, name, conditions, waveHeight, lat, lon, cameras, rank, wind, tide }) => ({
      _id,
      name,
      conditions: sevenRatings ? convertConditionsRating(conditions) : conditions,
      waveHeight,
      lat,
      lon,
      cameras,
      rank,
      wind,
      tide,
    }),
  );

  // associated info
  const primarySpot = subregion.primarySpot || spots[0]._id;
  const { timezone } = await getSpot(primarySpot);
  const utcOffset = utcOffsetFromTimezone(timezone);
  const abbrTimezone = moment().tz(timezone).format('z');
  const chartsUrl = `/surf-charts/wave-height/${slugify(subregion.name)}/${subregion._id}`;

  // Mobile uses adveristing.subregionId when subregion is inactive or off
  // Todo: follow up and remove this once fully live on the new system.
  const legacyId = subregion.legacyId ? subregion.legacyId.toString() : null;
  const associated = {
    advertising: {
      spotId: null,
      subregionId: legacyId,
    },
    abbrTimezone,
    timezone,
    utcOffset,
    units,
    chartsUrl,
    legacyId,
  };

  const { summary } = await getSubregionForecasts(subregion._id, timezone);
  const taxonomy = await getTaxonomy(primarySpot, 'spot');
  const breadcrumb = taxonomy.in.length ? await subregionBreadcrumb(taxonomy.in) : [];
  const forecaster = summary.forecaster.name
    ? {
        name: summary.forecaster.name,
        title: summary.forecaster.title,
        iconUrl: getGravatarURL(summary.forecaster.email),
      }
    : null;
  const highlights = summary.highlights.filter((highlight) => highlight.length > 0);
  const bestBets = summary.bestBets.filter((bet) => bet !== '');
  const bets = summary.bets || { best: null, worst: null };

  const forecastSummary = {
    forecastStatus: summary.forecastStatus,
    // TODO: Remove deprecated nextForecastTimestamp
    nextForecastTimestamp: summary.nextForecast.timestamp,
    nextForecast: {
      timestamp: summary.nextForecast.timestamp,
      utcOffset: summary.nextForecast.utcOffset,
    },
    forecaster,
    highlights,
    bestBets: premium ? bestBets : [],
    bets: premium ? bets : { best: null, worst: null },
    hasHighlights: highlights.length > 0,
    hasBets: !!bets.best || !!bets.worst,
  };

  const data = {
    _id: subregion._id,
    name: subregion.name,
    primarySpot,
    breadcrumb,
    timestamp: summary.updatedAt,
    forecastSummary,
    spots: sortBy((s) => s.rank, spots),
  };

  return res.send({
    associated,
    data,
  });
};

export default getOverviewHandler;
