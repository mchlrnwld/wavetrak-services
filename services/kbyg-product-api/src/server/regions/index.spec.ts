// TODO: Fix these unit tests
// import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import regionsAPI from '.';
// import SubregionForecastDay from '../../../model/SubregionForecastDayModel';
// import SubregionForecastSummary from '../../../model/SubregionForecastSummaryModel';
// import { updateForecastDay, updateForecastSummary } from '../../../common/forecasts';

describe('/admin/regions', () => {
  let log;
  // let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(regionsAPI(log));
    // request = chai.request(app).keepOpen();
  });

  // beforeEach(() => {});

  // afterEach(() => {});

  // describe('GET /conditions', () => {
  //   it('should return a set of conditions suitable for forms', async () => {
  //     const res = await request.get('/conditions').send();
  //     expect(res).to.have.status(200);
  //     expect(res).to.be.json();
  //     expect(res.body).to.have.property('conditions');
  //     expect(res.body).to.have.property('occ');
  //     expect(res.body).to.have.property('ratings');
  //     expect(res.body.conditions).to.be.instanceOf(Array);
  //     expect(res.body.occ).to.be.instanceOf(Array);
  //     expect(res.body.ratings).to.be.instanceOf(Array);
  //   });
  // });

  // describe('POST /', () => {
  //   const forecastSummary = {
  //     summary: {
  //       forecastDate: '12/20/2016',
  //       nextForecast: {
  //         day: 'Friday',
  //         time: '8 am',
  //       },
  //       highlights: ['highlight 1', 'highlight 2', 'highlight 3'],
  //       forecasterEmail: '',
  //       subregionId: '584204a24e65fad6a7709e46',
  //     },
  //   };

  //   const forecastSummaryResponse = {
  //     summary: {
  //       _id: '585debc9b8b9c411b103f116',
  //       createdAt: '2016-12-24T03:30:17.531Z',
  //       forecasterEmail: '',
  //       subregionId: '584204a24e65fad6a7709e46',
  //       updatedAt: '2016-12-27T16:56:46.397Z',
  //       __v: 8,
  //       nextForecast: {
  //         day: 'Friday',
  //         time: '8 am',
  //       },
  //       highlights: [
  //         'highlight 1',
  //         'highlight 2',
  //         'highlight 3',
  //       ],
  //       forecastDate: '2016-12-20T00:00:00.000Z',
  //     },
  //   };

  //   const forecastDays = {
  //     days: [{
  //       subregionId: '584204a24e65fad6a7709e46',
  //       forecastDate: '12/20/2016',
  //       forecast: {
  //         am: {
  //           condition: 3,
  //           occasionalHeight: 3,
  //           rating: 'NONE',
  //           observation: 'This is the observation',
  //         },
  //         pm: {
  //           condition: 3,
  //           occasionalHeight: 3,
  //           rating: 'NONE',
  //           observation: 'This is the observation',
  //         },
  //       },
  //     }, {
  //       subregionId: '584204a24e65fad6a7709e46',
  //       forecastDate: '12/21/2016',
  //       forecast: {
  //         am: {
  //           condition: 1,
  //           occasionalHeight: 10,
  //           rating: 'NONE',
  //           observation: 'This is the observation',
  //         },
  //         pm: {
  //           condition: 3,
  //           occasionalHeight: 3,
  //           rating: 'NONE',
  //           observation: 'This is the observation',
  //         },
  //       },
  //     }],
  //   };

  //   const forecastDaysResponse = {
  //     days: [
  //       {
  //         _id: '585df85ddfd48521244f6095',
  //         createdAt: '2016-12-24T04:23:57.618Z',
  //         subregionId: '584204a24e65fad6a7709e46',
  //         updatedAt: '2016-12-27T16:56:46.417Z',
  //         __v: 0,
  //         forecast: {
  //           am: {
  //             humanRelation: 'knee to thigh high',
  //             maxHeight: 2,
  //             minHeight: 1,
  //             condition: '3',
  //             occasionalHeight: 3,
  //             rating: 'None',
  //             observation: 'This is the observation',
  //           },
  //           pm: {
  //             humanRelation: 'knee to thigh high',
  //             maxHeight: 2,
  //             minHeight: 1,
  //             condition: '3',
  //             occasionalHeight: 3,
  //             rating: 'None',
  //             observation: 'This is the observation',
  //           },
  //         },
  //         forecastDate: '2016-12-20T00:00:00.000Z',
  //       },
  //       {
  //         _id: '585df85ddfd48521244f6095',
  //         createdAt: '2016-12-24T04:23:57.618Z',
  //         subregionId: '584204a24e65fad6a7709e46',
  //         updatedAt: '2016-12-27T16:56:46.422Z',
  //         __v: 0,
  //         forecast: {
  //           am: {
  //             humanRelation: 'Flat',
  //             maxHeight: 1,
  //             minHeight: 0,
  //             condition: '1',
  //             occasionalHeight: 10,
  //             rating: 'None',
  //             observation: 'This is the observation',
  //           },
  //           pm: {
  //             humanRelation: 'knee to thigh high',
  //             maxHeight: 2,
  //             minHeight: 1,
  //             condition: '3',
  //             occasionalHeight: 3,
  //             rating: 'None',
  //             observation: 'This is the observation',
  //           },
  //         },
  //         forecastDate: '2016-12-21T00:00:00.000Z',
  //       },
  //     ],
  //   };

  //   it('should post forecast days', async () => {
  //     try {
  //       // sinon.stub(SubregionForecastSummary.prototype, 'save')
  //       //   .resolves({ ...forecastSummaryResponse });
  //       // sinon.stub(SubregionForecastDay.prototype, 'save')
  //       //   .resolves({ ...forecastDaysResponse });
  //       sinon.stub(SubregionForecastSummary, 'findOne')
  //         .resolves({ ...forecastSummaryResponse });
  //       sinon.stub(SubregionForecastDay, 'findOne')
  //         .resolves({ ...forecastDaysResponse });

  //       const res = await request.post('/')
  //       .send({
  //         summary: forecastSummary.summary,
  //         days: forecastDays.days,
  //       });

  //       expect(res).to.have.status(200);
  //       expect(res).to.be.json();
  //       expect(res.body).to.deep.equal({ ...forecastSummaryResponse, ...forecastDaysResponse });
  //     } finally {
  //       SubregionForecastSummary.prototype.save.reset();
  //     }
  //   });
  // });
});
