import getOverviewHandlerSpec from './getOverviewHandler.spec';
import forecastsSpec from './forecasts/forecasts.spec';

describe('/regions', () => {
  getOverviewHandlerSpec();
  forecastsSpec();
});
