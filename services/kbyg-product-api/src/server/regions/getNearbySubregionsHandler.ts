import { getNearbySubregions } from '../../external/spots';

const getNearbySubregionsHandler = async (req, res) => {
  const subregions = await getNearbySubregions(req.subregion.region);

  return res.send({
    associated: {
      units: req.units,
    },
    data: {
      subregions: subregions
        .filter((subregion) => subregion.status === 'PUBLISHED' && subregion.primarySpot)
        .map((subregion) => {
          const { _id, name } = subregion;
          return {
            _id,
            name,
          };
        }),
    },
  });
};

export default getNearbySubregionsHandler;
