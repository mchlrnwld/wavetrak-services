import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { geoCountryIso } from '@surfline/services-common';
import * as getUnits from '../../utils/getUnits';
import * as getSubregionSpotReportViews from '../../model/SpotReportView/getSubregionSpotReportViews';
import * as spotsAPI from '../../external/spots';
import * as SubregionsAPI from '../../external/subregions';
import * as Breadcrumbs from '../../utils/breadcrumbs';
import regions from '.';
import {
  breadcrumb,
  forecastSummary,
  spot,
  spotReportViews,
  subregion,
  taxonomy,
  units,
} from './fixtures';
import subregionForecastsFixture from './fixtures/subregionForecasts.json';

const getOverviewHandlerSpec = () => {
  describe('/overview', () => {
    let log;
    let request;
    let clock;
    let subregionStub;
    let spotStub;
    let getSubregionStub;
    let getTaxonomyStub;
    let getSpotStub;
    let getSubregionForecastsStub;
    let getSubregionSpotReportViewsStub;
    let getUnitsStub;
    const now = 1535716800; // 08/31/2018 @ 05:00am (PST)

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(geoCountryIso, regions(log));
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(now * 1000);
    });

    after(() => {
      clock.restore();
    });

    beforeEach(() => {
      /* eslint-disable @typescript-eslint/ban-ts-comment */
      // @ts-ignore
      spotsAPI.getSpot.restore();
      // @ts-ignore
      spotsAPI.getSubregion.restore();
      // @ts-ignore
      spotsAPI.getTaxonomy.restore();
      /* eslint-enable @typescript-eslint/ban-ts-comment */
      subregionStub = sinon.stub(Breadcrumbs, 'subregion');
      spotStub = sinon.stub(Breadcrumbs, 'spot');
      getSubregionStub = sinon.stub(spotsAPI, 'getSubregion');
      getTaxonomyStub = sinon.stub(spotsAPI, 'getTaxonomy');
      getSpotStub = sinon.stub(spotsAPI, 'getSpot');
      getSubregionForecastsStub = sinon.stub(SubregionsAPI, 'getSubregionForecasts');
      getSubregionSpotReportViewsStub = sinon.stub(getSubregionSpotReportViews, 'default');
      getUnitsStub = sinon.stub(getUnits, 'default');
    });

    afterEach(() => {
      subregionStub.restore();
      spotStub.restore();
      getSubregionStub.restore();
      getTaxonomyStub.restore();
      getSpotStub.restore();
      getSubregionForecastsStub.restore();
      getSubregionSpotReportViewsStub.restore();
      getUnitsStub.restore();
    });

    it('should return subregion overview for a subregion', async () => {
      subregionStub.resolves(breadcrumb);
      getSubregionStub.resolves(subregion);
      getTaxonomyStub.resolves(taxonomy);
      getSpotStub.resolves(spot);
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      getUnitsStub.resolves(units);

      const subregionId = '58581a836630e24c44878fd6';
      const path = `/overview?subregionId=${subregionId}`;
      const res = await request.get(path).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          advertising: {
            spotId: null,
            subregionId: subregion.legacyId.toString(),
          },
          abbrTimezone: 'PDT',
          timezone: 'America/Los_Angeles',
          utcOffset: -7,
          chartsUrl: '/surf-charts/wave-height/north-orange-county/58581a836630e24c44878fd6',
          units,
          legacyId: subregion.legacyId.toString(),
        },
        data: {
          forecastSummary,
          _id: subregion._id,
          name: subregion.name,
          timestamp: 1631738097,
          primarySpot: subregion.primarySpot,
          breadcrumb,
          spots: [
            {
              ...spotReportViews[0],
            },
          ],
        },
      });
      expect(getSubregionSpotReportViewsStub).to.have.been.calledOnce();
      expect(getSubregionSpotReportViewsStub.firstCall.args).to.deep.equal([units, subregionId]);
    });

    it('should return subregion overview for a spotId', async () => {
      subregionStub.resolves(breadcrumb);
      getSubregionStub.resolves(subregion);
      getTaxonomyStub.resolves(taxonomy);
      getSpotStub.resolves(spot);
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      getUnitsStub.resolves(units);

      const spotId = '584204204e65fad6a77091aa';
      const subregionId = '58581a836630e24c44878fd6';

      const path = `/overview?spotId=${spotId}`;
      const res = await request.get(path).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          advertising: {
            spotId: null,
            subregionId: subregion.legacyId.toString(),
          },
          abbrTimezone: 'PDT',
          timezone: 'America/Los_Angeles',
          utcOffset: -7,
          chartsUrl: '/surf-charts/wave-height/north-orange-county/58581a836630e24c44878fd6',
          units,
          legacyId: subregion.legacyId.toString(),
        },
        data: {
          forecastSummary,
          _id: subregion._id,
          name: subregion.name,
          timestamp: 1631738097,
          primarySpot: subregion.primarySpot,
          breadcrumb,
          spots: [
            {
              ...spotReportViews[0],
            },
          ],
        },
      });
      expect(getSubregionSpotReportViewsStub).to.have.been.calledOnce();
      expect(getSubregionSpotReportViewsStub.firstCall.args).to.deep.equal([units, subregionId]);
    });

    it('should return subregion overview with old rating scale by default', async () => {
      subregionStub.resolves(breadcrumb);
      getSubregionStub.resolves(subregion);
      getTaxonomyStub.resolves(taxonomy);
      getSpotStub.resolves(spot);
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      getUnitsStub.resolves(units);

      const subregionId = '58581a836630e24c44878fd6';
      const path = `/overview?subregionId=${subregionId}`;
      const res = await request.get(path).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body.data.spots).to.deep.equal([
        {
          ...spotReportViews[0],
          conditions: { ...spotReportViews[0].conditions, value: 'VERY_GOOD' },
        },
      ]);
    });

    it('should return subregion overview with old rating scale if sevenRatings param is false', async () => {
      subregionStub.resolves(breadcrumb);
      getSubregionStub.resolves(subregion);
      getTaxonomyStub.resolves(taxonomy);
      getSpotStub.resolves(spot);
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      getUnitsStub.resolves(units);

      const subregionId = '58581a836630e24c44878fd6';
      const path = `/overview?subregionId=${subregionId}&sevenRatings=false`;
      const res = await request.get(path).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body.data.spots).to.deep.equal([
        {
          ...spotReportViews[0],
          conditions: { ...spotReportViews[0].conditions, value: 'VERY_GOOD' },
        },
      ]);
    });

    it('should return subregion overview with new rating scale if sevenRatings param is true', async () => {
      subregionStub.resolves(breadcrumb);
      getSubregionStub.resolves(subregion);
      getTaxonomyStub.resolves(taxonomy);
      getSpotStub.resolves(spot);
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getSubregionSpotReportViewsStub.resolves(spotReportViews);
      getUnitsStub.resolves(units);

      const subregionId = '58581a836630e24c44878fd6';
      const path = `/overview?subregionId=${subregionId}&sevenRatings=true`;
      const res = await request.get(path).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body.data.spots).to.deep.equal([
        {
          ...spotReportViews[0],
          conditions: { ...spotReportViews[0].conditions, value: 'GOOD' },
        },
      ]);
    });

    it('should return 400 if subregion is not a valid mongo id', async () => {
      const subregionIdBad = '123abc';

      const res = await request.get(`/overview?subregionId=${subregionIdBad}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        message: 'Valid spotId or subregionId required',
      });
    });

    it('should return 400 if no subregion is found', async () => {
      const subregionIdBad = '6179683113479532ce688f4b';

      const res = await request.get(`/overview?subregionId=${subregionIdBad}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        message: 'Valid spotId or subregionId required',
      });
    });

    it('should return 400 if spotId is not a valid mongo id', async () => {
      const spotIdBad = '5977abb3b38c2300127471ec4';

      const res = await request.get(`/overview?spotId=${spotIdBad}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        message: 'Valid spotId or subregionId required',
      });
    });

    it('should return 400 if no subregion is found from spotId', async () => {
      const spotIdBad = '617968639ac35811a2130538';

      const res = await request.get(`/overview?spotId=${spotIdBad}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        message: 'Valid spotId or subregionId required',
      });
    });
  });
};

export default getOverviewHandlerSpec;
