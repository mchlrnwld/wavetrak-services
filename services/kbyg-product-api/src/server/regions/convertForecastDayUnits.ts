import moment from 'moment';
import { convertStringByUnits } from '../../utils/convertUnits';
import convertWaveHeightsUnits from '../../utils/convertWaveHeightsUnits';

const baseConvertForecastDayUnits =
  (convertWaveHeights, convertString, offset) =>
  ({ forecastDate, forecast }) => {
    const forecastDateMoment = moment(forecastDate)
      .utc()
      .hour(-1 * offset);
    const utcForecastDayDate = forecastDateMoment.toDate().getTime() / 1000;
    return {
      forecastDate: utcForecastDayDate,
      ...forecast,
      am: {
        ...forecast.am,
        humanRelation: convertString(forecast.am.humanRelation),
        ...convertWaveHeights(forecast.am.minHeight, forecast.am.maxHeight, true),
      },
      pm: {
        ...forecast.pm,
        humanRelation: convertString(forecast.pm.humanRelation),
        ...convertWaveHeights(forecast.pm.minHeight, forecast.pm.maxHeight, true),
      },
    };
  };

const convertForecastDayUnits = (units, utcOffset) =>
  baseConvertForecastDayUnits(
    convertWaveHeightsUnits(units.waveHeight),
    convertStringByUnits(units.waveHeight),
    utcOffset,
  );

export default convertForecastDayUnits;
