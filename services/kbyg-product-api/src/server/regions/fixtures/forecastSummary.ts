export const forecastSummaryData = {
  forecaster: {
    name: 'Forecaster Name',
    title: 'Forecaster Title',
    iconUrl: 'https://www.gravatar.com/avatar/31f2f30d20e52cf0da0fb30ec70cc375?d=mm',
  },
  forecastStatus: {
    status: 'active',
    inactiveMessage: '',
  },
  nextForecastTimestamp: new Date('2021-11-05T9:00:00.000Z'),
  highlights: [
    'Shadowed SW swell builds through mid week',
    'Light AM wind',
    'More SW/SSW and NW swell long range',
  ],
  bestBets: [],
  bets: { best: null, worst: null },
  updatedAt: new Date('2021-11-01T11:48:32.049Z'),
};

export const forecastSummary = {
  forecaster: forecastSummaryData.forecaster,
  forecastStatus: forecastSummaryData.forecastStatus,
  nextForecastTimestamp: 1631898000,
  nextForecast: {
    timestamp: 1631898000,
    utcOffset: -7,
  },
  highlights: forecastSummaryData.highlights,
  bestBets: forecastSummaryData.bestBets,
  bets: forecastSummaryData.bets,
  hasHighlights: true,
  hasBets: false,
};
