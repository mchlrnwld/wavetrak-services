const taxonomy = {
  in: [
    {
      type: 'geoname',
      depth: 0,
      associated: {
        links: [
          {
            www: {
              name: 'United States',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
            },
          },
        ],
      },
    },
    {
      type: 'geoname',
      depth: 1,
      associated: {
        links: [
          {
            www: {
              name: 'California',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
            },
          },
        ],
      },
    },
    {
      type: 'geoname',
      depth: 2,
      associated: {
        links: [
          {
            www: {
              name: 'Orange County',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524',
            },
          },
        ],
      },
    },
  ],
};

export default taxonomy;
