const units = {
  temperature: 'F',
  windSpeed: 'KTS',
  tideHeight: 'FT',
  swellHeight: 'FT',
  surfHeight: 'FT',
};

export default units;
