const breadcrumb = [
  {
    href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
    name: 'United States',
  },
  {
    href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
    name: 'California',
  },
  {
    href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524',
    name: 'Orange County',
  },
];

export default breadcrumb;
