export const spot = {
  _id: '584204204e65fad6a77091aa',
  name: 'HB Pier, Northside',
  lat: 33.656781041213,
  lon: -118.0064678192,
  timezone: 'America/Los_Angeles',
  subregion: '58581a836630e24c44878fd6',
};

export const spotReportViews = [
  {
    _id: '584204204e65fad6a77091aa',
    name: 'HB Pier, Northside',
    lat: 33.656781041213,
    lon: -118.0064678192,
    conditions: {
      human: true,
      value: 'VERY_GOOD',
      sortableCondition: 3,
      expired: false,
    },
    waveHeight: [],
    cameras: [],
    rank: 0,
  },
];
