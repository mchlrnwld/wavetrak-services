// TODO: Deprecate
import { slugify } from '@surfline/web-common';
import { getSpotsBySubregion, getTaxonomy } from '../../../external/spots';
import { getSubregionForecasts } from '../../../external/subregions';
import getGravatarURL from '../../../utils/getGravatarURL';
import { subregion as subregionBreadcrumb } from '../../../utils/breadcrumbs';
import utcOffsetFromTimezone from '../../../utils/utcOffsetFromTimezone';
import convertForecastDayUnits from '../convertForecastDayUnits';
import convertTo7Ratings from '../../../utils/convertTo7Ratings';
import { ForecastDay } from '../../types';

const generateBounds = (spots) => {
  if (spots.length > 1) {
    const latitudes = spots.map((spot) => spot.location.coordinates[1]);
    const longitudes = spots.map((spot) => spot.location.coordinates[0]);
    const epsilon = 0.0001 * spots.length;
    const result = {
      north: Math.max(...latitudes) + epsilon,
      south: Math.min(...latitudes) - epsilon,
      east: Math.max(...longitudes) + epsilon,
      west: Math.min(...longitudes) - epsilon,
    };

    return result;
  }
  return null;
};

const getForecastHandler = async (req, res) => {
  const { user, subregion, units } = req;
  const sevenRatings = req.query.sevenRatings === 'true';
  const days = 7;
  const spots = await getSpotsBySubregion(subregion._id);

  if (!spots || !spots.length) {
    return res.status(404).send({
      message: 'No spots listed for subregion',
    });
  }

  const { timezone } = spots[0];
  const utcOffset = utcOffsetFromTimezone(timezone);
  const chartsUrl = `/surf-charts/wave-height/${slugify(subregion.name)}/${subregion._id}`;

  const associated = {
    advertising: {
      spotId: null,
      subregionId: subregion.legacyId.toString(),
    },
    analytics: {
      spotId: null,
      subregionId: subregion.legacyId.toString(),
    },
    utcOffset,
    timezone,
    units,
    chartsUrl,
  };

  const subregionForecasts = await getSubregionForecasts(subregion._id, timezone, days);
  const { summary } = subregionForecasts;

  if (!summary) {
    return res.status(404).send({
      message: 'Forecast summary not found for subregion',
    });
  }

  const reports = subregionForecasts.days.map(convertForecastDayUnits(req.units, utcOffset));

  const taxonomy = await getTaxonomy(spots[0]._id, 'spot');
  const breadcrumb = subregionBreadcrumb(taxonomy.in);
  const subregionSpots = spots.map((spot) => ({
    _id: spot._id,
    name: spot.name,
    status: spot.status,
    location: spot.location,
    forecastLocation: spot.forecastLocation,
  }));
  const forecaster = summary.forecaster.name
    ? {
        name: summary.forecaster.name,
        avatar: getGravatarURL(summary.forecaster.email),
      }
    : null;
  const highlights = summary.highlights.length
    ? summary.highlights.filter((highlight) => highlight.length > 0)
    : null;

  const data = {
    entitlements: user.entitlements,
    reports: sevenRatings
      ? reports.map((forecast: ForecastDay) => ({
          ...forecast,
          am: {
            ...forecast.am,
            rating: convertTo7Ratings(forecast.am.rating),
          },
          pm: {
            ...forecast.pm,
            rating: convertTo7Ratings(forecast.pm.rating),
          },
        }))
      : reports,
    subregionId: subregion._id,
    subregionName: subregion.name,
    primarySpot: subregion.primarySpot || null,
    subregionSpots,
    timestamp: summary.updatedAt,
    forecastStatus: summary.forecastStatus,
    forecaster,
    highlights,
    nextForecastTimestamp: summary.nextForecast.timestamp,
    nextForecast: {
      timestamp: summary.nextForecast.timestamp,
      utcOffset: summary.nextForecast.utcOffset,
    },
    mapBounds: generateBounds(subregionSpots),
    breadcrumb,
  };

  return res.send({
    associated,
    units: req.units,
    data,
  });
};

export default getForecastHandler;
