import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import createParamString from '../../../external/createParamString';
import * as forecastsAPI from '../../../external/forecasts/forecasts';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSubregion from '../../middleware/getSubregion';
import forecasts from './forecasts';
import waveFixture from './fixtures/wave.json';

const getWaveHandlerSpec = () => {
  describe('/wave', () => {
    let clock;
    let request;
    let getWaveForecastsStub;
    const now = new Date('2021-03-15T00:00:00Z').getTime() / 1000; // 03/14/2021@17:00PDT
    const expectedStart = new Date('2021-03-14T08:00:00Z').getTime() / 1000; // 03/14/2021@00:00PST
    const expectedEnd = new Date('2021-03-16T07:00:00Z').getTime() / 1000; // 03/16/2021@00:00PDT
    const params = {
      subregionId: '58581a836630e24c44878fd6',
      days: 2,
      intervalHours: 12,
    };
    const url = `/wave?${createParamString(params)}`;

    before(() => {
      const app = express();
      app.use(userUnits, entitlements, checkForecastParams, getSubregion, forecasts());
      request = chai.request(app).keepOpen();
      clock = sinon.useFakeTimers(now * 1000);
    });

    beforeEach(() => {
      getWaveForecastsStub = sinon.stub(forecastsAPI, 'getWaveForecasts');
    });

    afterEach(() => {
      getWaveForecastsStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns surf and swell forecast data with UTC offsets applied to each timestep', async () => {
      getWaveForecastsStub.resolves(waveFixture);

      const res = await request.get(url).send();

      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.be.ok();
      expect(res.body.associated.utcOffset).to.equal(-7);
      expect(res.body.associated.forecastLocation).to.deep.equal({
        lat: 33.5,
        lon: -118,
      });
      expect(res.body.associated.offshoreLocation).to.deep.equal({
        lat: 21.75,
        lon: -158.25,
      });
      expect(res.body.data).to.be.ok();
      expect(res.body.data.wave).to.be.ok();
      expect(res.body.data.wave).to.have.length(4);
      expect(res.body.data.wave[0].utcOffset).to.equal(-8);
      expect(res.body.data.wave[1].utcOffset).to.equal(-7);
      expect(res.body.data.wave[2].utcOffset).to.equal(-7);
      expect(res.body.data.wave[3].utcOffset).to.equal(-7);

      const getWaveForecastsArgs = getWaveForecastsStub.firstCall.args[0];
      expect(getWaveForecastsArgs.start).to.equal(expectedStart);
      expect(getWaveForecastsArgs.end).to.equal(expectedEnd);
      expect(getWaveForecastsArgs.interval).to.equal(params.intervalHours * 60);
    });
  });
};

export default getWaveHandlerSpec;
