import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import correctedWaveHeights from '../../middleware/correctedWaveHeights';
import getForecastHandler from './getForecastHandler';
import getConditionsHandler from './getConditionsHandler';
import getWaveHandler from './getWaveHandler';

const forecasts = () => {
  const api = Router();

  api.get('/', wrapErrors(getForecastHandler));
  api.get('/conditions', wrapErrors(getConditionsHandler));
  api.get('/wave', correctedWaveHeights, wrapErrors(getWaveHandler));

  return api;
};

export default forecasts;
