import chai, { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import sinon from 'sinon';
import express from 'express';
import moment from 'moment-timezone';
import { authenticateRequest } from '@surfline/services-common';
import createParamString from '../../../external/createParamString';
import * as getConditions from '../../../model/getLOLAConditions/getConditions';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSubregion from '../../middleware/getSubregion';
import * as SubregionsAPI from '../../../external/subregions';
import subregionForecastsFixture from '../fixtures/subregionForecasts.json';
import forecasts from './forecasts';
import { LOLAConditionsLengthErrorHandler } from '../../../errors/LOLAConditionsLengthError';

const getSpotForecastConditionsHandlerSpec = () => {
  describe('/conditions', () => {
    let request;
    let clock;
    let getConditionsStub;
    let getSubregionForecastsStub;
    const start = deepFreeze(moment('2021-09-15 00:00:00').tz('America/Los_Angeles'));
    const spotConditionsFixture = (startsAt = start, days = 3) => ({
      start: startsAt.clone().unix(),
      conditions: [...Array(days)].map((_, i) => ({
        forecastDate: startsAt.clone().add(i, 'days').unix(),
        am: {
          timestamp: startsAt.clone().add(i, 'days').add(6, 'hours').unix(),
          rating: 'VERY_GOOD',
          minHeight: 1,
          maxHeight: 2,
          humanRelation: 'knee to thigh high',
          relation: 'waist to stomach high',
          observation:
            'Small short period wind waves from the N. Use extreme caution. Gale warnign with dangerous seas. Winds from the WNW.',
        },
        pm: {
          timestamp: startsAt.clone().add(i, 'days').add(12, 'hours').unix(),
          rating: 'GOOD_TO_EPIC',
          minHeight: 1,
          maxHeight: 2,
          humanRelation: 'knee to thigh high',
          relation: 'waist to stomach high',
          observation:
            'Small short period wind waves from the N. Use extreme caution. Gale warnign with dangerous seas. Winds from the WNW.',
        },
      })),
    });

    before(() => {
      const app = express();
      app.use(
        authenticateRequest(),
        userUnits,
        entitlements,
        checkForecastParams,
        getSubregion,
        forecasts(),
      );
      app.use(LOLAConditionsLengthErrorHandler);
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      const startDate = moment('2021-09-15').tz('America/Los_Angeles').toDate();
      clock = sinon.useFakeTimers(startDate);
      getConditionsStub = sinon.stub(getConditions, 'default');
      getSubregionForecastsStub = sinon.stub(SubregionsAPI, 'getSubregionForecasts');
    });

    afterEach(() => {
      clock.restore();
      getConditionsStub.restore();
      getSubregionForecastsStub.restore();
    });

    it('should return conditions given a subregionId', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));
      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
    });

    it('should return 17 days for a premium user', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-09-15');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-09-16');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-09-17');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-09-18');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-09-19');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-09-20');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-09-21');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-09-22');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-09-23');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-09-24');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-09-25');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-09-26');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-09-27');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-09-28');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-09-29');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-09-30');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-10-01');
    });

    it('should return correct forecastDay across PST change', async () => {
      const dstStartDate = moment('2021-11-05').tz('America/Los_Angeles').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-11-05' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-11-06' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-11-07' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-11-05');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-11-06');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-11-07');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-11-08');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-11-09');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-11-10');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-11-11');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-11-12');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-11-13');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-11-14');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-11-15');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-11-16');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-11-17');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-11-18');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-11-19');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-11-20');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-11-21');
    });

    it('should return correct forecastDay across PDT change', async () => {
      const dstStartDate = moment('2021-03-11').tz('America/Los_Angeles').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-03-11' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-03-12' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-03-13' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-03-11');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-03-12');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-03-13');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-03-14');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-03-15');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-03-16');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-03-17');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-03-18');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-03-19');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-03-20');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-03-21');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-03-22');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-03-23');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-03-24');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-03-25');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-03-26');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-03-27');
    });

    it('should return correct forecastDay across AEST change', async () => {
      const dstStartDate = moment('2021-10-01').tz('Australia/Sydney').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-10-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-10-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-10-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-10-01');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-10-02');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-10-03');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-10-04');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-10-05');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-10-06');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-10-07');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-10-08');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-10-09');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-10-10');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-10-11');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-10-12');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-10-13');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-10-14');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-10-15');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-10-16');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-10-17');
    });

    it('should return correct forecastDay across AEDT change', async () => {
      const dstStartDate = moment('2021-04-01').tz('Australia/Sydney').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-04-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-04-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-04-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-04-01');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-04-02');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-04-03');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-04-04');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-04-05');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-04-06');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-04-07');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-04-08');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-04-09');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-04-10');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-04-11');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-04-12');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-04-13');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-04-14');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-04-15');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-04-16');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-04-17');
    });

    it('should return correct forecastDay across CLST change', async () => {
      const dstStartDate = moment('2021-09-01').tz('America/Santiago').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-09-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-09-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-09-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-09-01');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-09-02');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-09-03');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-09-04');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-09-05');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-09-06');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-09-07');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-09-08');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-09-09');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-09-10');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-09-11');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-09-12');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-09-13');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-09-14');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-09-15');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-09-16');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-09-17');
    });

    it('should return correct forecastDay across CLDT change', async () => {
      const dstStartDate = moment('2021-04-01').tz('America/Santiago').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-04-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-04-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-04-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);
      expect(res.body.data.conditions[0].forecastDay).to.equal('2021-04-01');
      expect(res.body.data.conditions[1].forecastDay).to.equal('2021-04-02');
      expect(res.body.data.conditions[2].forecastDay).to.equal('2021-04-03');
      expect(res.body.data.conditions[3].forecastDay).to.equal('2021-04-04');
      expect(res.body.data.conditions[4].forecastDay).to.equal('2021-04-05');
      expect(res.body.data.conditions[5].forecastDay).to.equal('2021-04-06');
      expect(res.body.data.conditions[6].forecastDay).to.equal('2021-04-07');
      expect(res.body.data.conditions[7].forecastDay).to.equal('2021-04-08');
      expect(res.body.data.conditions[8].forecastDay).to.equal('2021-04-09');
      expect(res.body.data.conditions[9].forecastDay).to.equal('2021-04-10');
      expect(res.body.data.conditions[10].forecastDay).to.equal('2021-04-11');
      expect(res.body.data.conditions[11].forecastDay).to.equal('2021-04-12');
      expect(res.body.data.conditions[12].forecastDay).to.equal('2021-04-13');
      expect(res.body.data.conditions[13].forecastDay).to.equal('2021-04-14');
      expect(res.body.data.conditions[14].forecastDay).to.equal('2021-04-15');
      expect(res.body.data.conditions[15].forecastDay).to.equal('2021-04-16');
      expect(res.body.data.conditions[16].forecastDay).to.equal('2021-04-17');
    });

    it('should fail when data does not match', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsFixture);
      getConditionsStub.resolves(spotConditionsFixture(start.clone().add(1, 'days'), 1));

      const params = { subregionId: '58581a836630e24c44878fd6' };

      const res = await request.get(`/conditions?${createParamString(params)}`).send();
      expect(res).to.have.status(500);
      expect(res).to.be.json();
      expect(res.body.message).to.equal(
        'Mismatch: Data length. Could not aggregate all required data components.',
      );
    });

    it('should return old ratings scale by default', async () => {
      const dstStartDate = moment('2021-04-01').tz('America/Santiago').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-04-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-04-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-04-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);

      expect(res.body.data.conditions[0].am.rating).to.equal('POOR');
      expect(res.body.data.conditions[0].pm.rating).to.equal('POOR_TO_FAIR');

      expect(res.body.data.conditions[1].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[1].pm.rating).to.equal('FAIR_TO_GOOD');

      expect(res.body.data.conditions[2].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[2].pm.rating).to.equal('VERY_GOOD');
    });

    it('should return old ratings scale if sevenRatings param is false', async () => {
      const dstStartDate = moment('2021-04-01').tz('America/Santiago').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-04-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-04-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-04-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6', sevenRatings: false };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);

      expect(res.body.data.conditions[0].am.rating).to.equal('POOR');
      expect(res.body.data.conditions[0].pm.rating).to.equal('POOR_TO_FAIR');

      expect(res.body.data.conditions[1].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[1].pm.rating).to.equal('FAIR_TO_GOOD');

      expect(res.body.data.conditions[2].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[2].pm.rating).to.equal('VERY_GOOD');
    });

    it('should return new ratings scale if sevenRatings param is true', async () => {
      const dstStartDate = moment('2021-04-01').tz('America/Santiago').toDate();
      clock = sinon.useFakeTimers(dstStartDate);
      getSubregionForecastsStub.resolves({
        ...subregionForecastsFixture,
        days: [
          { ...subregionForecastsFixture.days[0], forecastDay: '2021-04-01' },
          { ...subregionForecastsFixture.days[1], forecastDay: '2021-04-02' },
          { ...subregionForecastsFixture.days[2], forecastDay: '2021-04-03' },
        ],
      });
      getConditionsStub.resolves(spotConditionsFixture(start, 17));

      const params = { subregionId: '58581a836630e24c44878fd6', sevenRatings: true };
      const res = await request
        .get(`/conditions?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();

      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.conditions.length).to.equal(17);

      expect(res.body.data.conditions[0].am.rating).to.equal('POOR');
      expect(res.body.data.conditions[0].pm.rating).to.equal('POOR_TO_FAIR');

      expect(res.body.data.conditions[1].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[1].pm.rating).to.equal('FAIR_TO_GOOD');

      expect(res.body.data.conditions[2].am.rating).to.equal('GOOD');
      expect(res.body.data.conditions[2].pm.rating).to.equal('GOOD');
    });
  });
};

export default getSpotForecastConditionsHandlerSpec;
