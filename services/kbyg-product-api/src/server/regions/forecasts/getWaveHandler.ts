import { getSpot } from '../../../external/spots';
import waveForecasts from '../../../model/waveForecasts';
import limitToMaxWaveHeights from '../../../utils/limitToMaxWaveHeights';

const getWaveHandler = async (req, res) => {
  const {
    units,
    forecast: { days, intervalHours, maxHeights },
    subregion,
    correctedWaveHeights,
  } = req;
  const spot = await getSpot(subregion.primarySpot);

  const { utcOffset, offshoreLocation, data } = await waveForecasts(
    {
      spot,
      days,
      intervalHours,
      units,
    },
    correctedWaveHeights,
  );

  return res.send({
    associated: {
      units,
      utcOffset,
      forecastLocation: {
        lon: subregion.offshoreSwellLocation.coordinates[0],
        lat: subregion.offshoreSwellLocation.coordinates[1],
      },
      offshoreLocation: {
        lon: offshoreLocation.lon,
        lat: offshoreLocation.lat,
      },
    },
    data: {
      wave: maxHeights ? limitToMaxWaveHeights(data) : data,
    },
  });
};

export default getWaveHandler;
