import getForecastHandlerSpec from './getForecastHandler.spec';
import getConditionsHandlerSpec from './getConditionsHandler.spec';
import getWaveHandlerSpec from './getWaveHandler.spec';

const forecastsSpec = () => {
  describe('/forecasts', () => {
    getForecastHandlerSpec();
    getConditionsHandlerSpec();
    getWaveHandlerSpec();
  });
};

export default forecastsSpec;
