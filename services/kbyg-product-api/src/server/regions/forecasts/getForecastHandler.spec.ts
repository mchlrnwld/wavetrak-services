import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import userUnits from '../../middleware/userUnits';
import entitlements from '../../middleware/entitlements';
import checkForecastParams from '../../middleware/checkForecastParams';
import getSubregion from '../../middleware/getSubregion';
import createParamString from '../../../external/createParamString';
import * as SpotsAPI from '../../../external/spots';
import * as SubregionsAPI from '../../../external/subregions';
import forecasts from './forecasts';
import subregionForecastsPremiumFixture from '../fixtures/subregionForecastsPremium.json';
import subregionSpotsFixture from '../fixtures/subregionSpots.json';

const getForecastHandlerSpec = () => {
  describe('/', () => {
    let request;
    let getSubregionForecastsStub;
    let getSpotsBySubregionStub;

    before(() => {
      const app = express();
      app.use(
        authenticateRequest(),
        userUnits,
        entitlements,
        checkForecastParams,
        getSubregion,
        forecasts(),
      );
      request = chai.request(app).keepOpen();
    });

    beforeEach(() => {
      getSubregionForecastsStub = sinon.stub(SubregionsAPI, 'getSubregionForecasts');
      getSpotsBySubregionStub = sinon.stub(SpotsAPI, 'getSpotsBySubregion');
    });

    afterEach(() => {
      getSubregionForecastsStub.restore();
      getSpotsBySubregionStub.restore();
    });

    it('should return an 8 day forecasts for non-premium users given a subregionId', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsPremiumFixture);
      getSpotsBySubregionStub.resolves(subregionSpotsFixture);
      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request.get(`/?${createParamString(params)}`).send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.reports.length).to.equal(8);
    });

    it('should return an 8 day forecasts for premium users given a subregionId', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsPremiumFixture);
      getSpotsBySubregionStub.resolves(subregionSpotsFixture);
      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.reports.length).to.equal(8);
    });

    it('should use old ratings scale by default', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsPremiumFixture);
      getSpotsBySubregionStub.resolves(subregionSpotsFixture);
      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request
        .get(`/?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.reports.length).to.equal(8);

      expect(res.body.data.reports[0].am.rating).to.equal('VERY_GOOD');
      expect(res.body.data.reports[0].pm.rating).to.equal('GOOD');
    });

    it('should use old ratings scale when sevenRatings param is false', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsPremiumFixture);
      getSpotsBySubregionStub.resolves(subregionSpotsFixture);
      const params = { subregionId: '58581a836630e24c44878fd6', sevenRatings: false };
      const res = await request
        .get(`/?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.reports.length).to.equal(8);

      expect(res.body.data.reports[0].am.rating).to.equal('VERY_GOOD');
      expect(res.body.data.reports[0].pm.rating).to.equal('GOOD');
    });

    it('should use new ratings scale when sevenRatings param is true', async () => {
      getSubregionForecastsStub.resolves(subregionForecastsPremiumFixture);
      getSpotsBySubregionStub.resolves(subregionSpotsFixture);
      const params = { subregionId: '58581a836630e24c44878fd6', sevenRatings: true };
      const res = await request
        .get(`/?${createParamString(params)}`)
        .set('x-auth-userid', '582495992d06cd3b71d66229')
        .send();
      expect(res).to.be.ok();
      expect(res.status).to.equal(200);
      expect(res.body.data.reports.length).to.equal(8);

      expect(res.body.data.reports[0].am.rating).to.equal('GOOD');
      expect(res.body.data.reports[0].pm.rating).to.equal('GOOD');
    });
  });
};

export default getForecastHandlerSpec;
