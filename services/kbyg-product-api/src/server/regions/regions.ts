import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import userUnits from '../middleware/userUnits';
import entitlements from '../middleware/entitlements';
import checkScopeParams from '../middleware/checkScopeParams';
import checkForecastParams from '../middleware/checkForecastParams';
import getSubregion from '../middleware/getSubregion';
import trackRegionsRequests from './trackRegionsRequests';
import forecasts from './forecasts';
import getOverviewHandler from './getOverviewHandler';
import getNearbySubregionsHandler from './getNearbySubregionsHandler';

const regions = (log) => {
  const api = Router();
  api.use('*', trackRegionsRequests(log));
  api.use(
    '/forecasts',
    userUnits,
    entitlements,
    checkScopeParams,
    checkForecastParams,
    getSubregion,
    forecasts(),
  );
  api.get(
    '/overview',
    userUnits,
    entitlements,
    checkScopeParams,
    getSubregion,
    wrapErrors(getOverviewHandler),
  );
  api.get('/nearby', userUnits, getSubregion, wrapErrors(getNearbySubregionsHandler));
  return api;
};

export default regions;
