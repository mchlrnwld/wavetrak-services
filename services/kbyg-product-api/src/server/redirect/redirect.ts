import { NextFunction, Response } from 'express';
import { slugify, kbygPaths } from '@surfline/web-common';
import { Spots, Subregions, Regions } from './Model';
import travelRedirectPaths from './travelRedirectPaths.json';
import geonameMapRedirectPaths from './geonameMapRedirectPaths.json';
import subregionRedirectPaths from './subregionRedirectPaths.json';
import spotRedirectPaths from './spotRedirectPaths.json';
import { KBYGRequest } from '../types';

const { spotReportPath, subregionForecastPath } = kbygPaths;

const env = () => {
  switch (process.env.NODE_ENV) {
    case 'production': {
      return 'www.';
    }
    case 'staging': {
      return 'staging.';
    }
    case 'sandbox': {
      return 'sandbox.';
    }
    default: {
      return 'staging.';
    }
  }
};

export const trackRedirectRequests =
  (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
    log.trace({
      action: '/kbyg/redirect',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    return next();
  };

export const redirectToMVPSpotHandler = async (req: KBYGRequest, res: Response) => {
  const { legacySpotId } = req.params;
  const mongoSpot = await Spots.findOne({ legacyId: legacySpotId });
  const customSpotPath = spotRedirectPaths[legacySpotId];

  if (mongoSpot) {
    const spotId = mongoSpot._id;
    const spotName = mongoSpot.name;
    return res.redirect(
      301,
      `https://${env()}surfline.com${spotReportPath(slugify(spotName), spotId)}`,
    );
  }
  if (customSpotPath) {
    return res.redirect(301, `https://${env()}surfline.com${customSpotPath}`);
  }
  return res.status(404).send('Spot not found.');
};

export const redirectToMVPSubregionHandler = async (req: KBYGRequest, res: Response) => {
  const { legacySubregionId } = req.params;
  const mongoSubregion = await Subregions.findOne({ 'imported.subregionId': legacySubregionId });
  const customSubregionPath = subregionRedirectPaths[legacySubregionId];

  if (mongoSubregion) {
    const subregionId = mongoSubregion._id;
    const subregionName = mongoSubregion.name;
    return res.redirect(
      301,
      `https://${env()}surfline.com${subregionForecastPath(slugify(subregionName), subregionId)}`,
    );
  }
  if (customSubregionPath) {
    return res.redirect(301, `https://${env()}surfline.com${customSubregionPath}`);
  }
  return res.status(404).send('Subregion not found.');
};

export const redirectToMVPSubregionChartsHandler = async (req: KBYGRequest, res: Response) => {
  const { legacySubregionId, chartSlug } = req.params;
  const mongoSubregion = await Subregions.findOne({ 'imported.subregionId': legacySubregionId });

  if (mongoSubregion) {
    const subregionId = mongoSubregion._id;
    const subregionName = mongoSubregion.name;
    const chartsPath = `/surf-charts/${chartSlug}/${slugify(subregionName)}/${subregionId}`;
    return res.redirect(301, `https://${env()}surfline.com${chartsPath}`);
  }

  // no custom links resolve for legacy charts
  return res.status(404).send('Subregion not found.');
};

export const redirectToMVPRegionChartsHandler = async (req: KBYGRequest, res: Response) => {
  const { legacySubregionId, chartSlug } = req.params;
  const mongoSubregion = await Subregions.findOne({ 'imported.subregionId': legacySubregionId });

  if (mongoSubregion) {
    const subregionId = mongoSubregion._id;
    const regionId = mongoSubregion.region;
    const region = await Regions.findById(regionId);
    const regionName = region ? region.name : 'region';
    const chartsPath = `/surf-charts/${chartSlug}/${slugify(regionName)}/${subregionId}`;
    return res.redirect(301, `https://${env()}surfline.com${chartsPath}`);
  }

  // no custom links resolve for legacy charts
  return res.status(404).send('Subregion not found.');
};

export const redirectToLegacySubregionHandler = async (req: KBYGRequest, res: Response) => {
  const { subregionId } = req.params;
  const subregion = await Subregions.findById(subregionId);
  if (!subregion) {
    return res.status(404).send('Subregion not found.');
  }
  const legacySubregionId = subregion.imported.subregionId;
  const subregionName = subregion.name;
  const regionId = subregion.region;
  const region = await Regions.findById(regionId);
  const regionName = region ? region.name : 'region';
  const url = `http://www.surfline.com/surf-forecasts/${slugify(regionName)}/${slugify(
    subregionName,
  )}_${legacySubregionId}`;

  return res.redirect(301, url);
};

export const redirectToMVPSpotForecastHandler = async (req: KBYGRequest, res: Response) => {
  const { legacySpotId } = req.params;
  const spot = await Spots.findOne({ legacyId: legacySpotId });
  if (!spot) {
    return res.status(404).send('Spot not found.');
  }
  const spotId = spot._id;
  const spotName = spot.name;
  const url = `https://${env()}surfline.com${spotReportPath(spotName, spotId, true)}`;
  return res.redirect(301, url);
};

export const redirectToMVPTravelHandler = async (req: KBYGRequest, res: Response) => {
  const { legacyTravelId } = req.params;
  const travelPath = travelRedirectPaths[legacyTravelId];
  if (!travelPath) {
    return res.status(404).send('Travel page not found');
  }
  const url = `https://${env()}surfline.com${travelPath}`;
  return res.redirect(301, url);
};

export const redirectToMVPGeonameMapHandler = async (req: KBYGRequest, res: Response) => {
  const { legacyId } = req.params;
  const geonameMapPath = geonameMapRedirectPaths[legacyId];
  if (!geonameMapPath) {
    return res.status(404).send('Geoname Map Page not found');
  }
  const url = `https://${env()}surfline.com${geonameMapPath}`;
  return res.redirect(301, url);
};
