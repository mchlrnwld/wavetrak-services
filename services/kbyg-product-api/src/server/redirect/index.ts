import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import {
  trackRedirectRequests,
  redirectToMVPSpotHandler,
  redirectToMVPSubregionHandler,
  redirectToMVPSubregionChartsHandler,
  redirectToMVPRegionChartsHandler,
  redirectToLegacySubregionHandler,
  redirectToMVPSpotForecastHandler,
  redirectToMVPTravelHandler,
  redirectToMVPGeonameMapHandler,
} from './redirect';

const spots = (log) => {
  const api = Router();

  api.use('*', trackRedirectRequests(log));
  api.get('/mvp/spotforecast/:legacySpotId', wrapErrors(redirectToMVPSpotForecastHandler));
  api.get('/mvp/spot/:legacySpotId', wrapErrors(redirectToMVPSpotHandler));
  api.get('/mvp/subregion/:legacySubregionId', wrapErrors(redirectToMVPSubregionHandler));
  api.get(
    '/mvp/regionCharts/:legacySubregionId/:chartSlug',
    wrapErrors(redirectToMVPRegionChartsHandler),
  );
  api.get(
    '/mvp/subregionCharts/:legacySubregionId/:chartSlug',
    wrapErrors(redirectToMVPSubregionChartsHandler),
  );
  api.get('/mvp/travel/:legacyTravelId', wrapErrors(redirectToMVPTravelHandler));
  api.get('/mvp/geoname-map/:legacyId', wrapErrors(redirectToMVPGeonameMapHandler));
  api.get('/legacy/subregion/:subregionId', wrapErrors(redirectToLegacySubregionHandler));

  return api;
};

export default spots;
