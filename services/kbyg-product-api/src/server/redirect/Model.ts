// TODO: Remove this dependency and make all Spots requests through spots API.
import { model, Schema, Types } from 'mongoose';

interface Spot {
  name: string;
  legacyId: string;
}

interface Subregion {
  name: string;
  imported?: {
    subregionId: number;
  };
  region: Types.ObjectId;
}

interface Region {
  name: string;
}

export const spotSchema = new Schema<Spot>(
  {
    name: { type: String },
    legacyId: { type: String },
  },
  { collection: 'Spots' },
);

export const subregionSchema = new Schema<Subregion>(
  {
    name: { type: String },
    imported: {
      subregionId: { type: Number }, // legacyId
    },
    region: { type: Schema.Types.ObjectId },
  },
  { collection: 'Subregions' },
);

export const regionSchema = new Schema<Region>(
  {
    name: { type: String },
  },
  { collection: 'Regions' },
);

export const Spots = model<Spot>('RedirectSpots', spotSchema);
export const Subregions = model<Subregion>('RedirectSubregions', subregionSchema);
export const Regions = model<Region>('RedirectRegions', regionSchema);
