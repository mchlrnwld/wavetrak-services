const checkForecastParams = (req, res, next) => {
  /* eslint-disable no-param-reassign */
  const { query } = req;
  const { maxHeights } = query;
  const { scopes } = req;
  const premiumPartner = scopes && scopes.includes('forecast:read:premium-partner');
  let maxHeightsValidated = false;
  if (maxHeights) {
    try {
      maxHeightsValidated = JSON.parse(maxHeights.toLowerCase());
    } catch (e) {
      return res.status(500).send({
        message: 'maxHeights parameter must have a value of true or false',
      });
    }
  }
  const maxDays = 17;
  let days = query.days ? parseInt(query.days, 10) : maxDays;
  // override days query param for 3rd Party requests and premium partners
  if (query.thirdParty) days = 3;
  // premium partners always get 16 days of forecast
  if (premiumPartner) days = 17;

  const intervalHours = query.intervalHours ? parseInt(query.intervalHours, 10) : 1;
  // TODO: No longer need offset if we are fetching all data in one go
  const offset = query.offset ? parseInt(query.offset, 10) : 0;

  if (offset > maxDays || days > maxDays || offset + days > maxDays) {
    return res.status(400).send({ message: 'Parameters out of bounds' });
  }

  req.forecast = { days, offset, intervalHours, maxHeights: maxHeightsValidated };
  return next();
  /* eslint-enable no-param-reassign */
};

export default checkForecastParams;
