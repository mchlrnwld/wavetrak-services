import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { errorHandlerMiddleware, authenticateRequest } from '@surfline/services-common';
import * as entitlementsAPI from '../../external/entitlements';
import { KBYGRequest } from '../types';
import entitlements from './entitlements';

describe('middleware / entitlements', () => {
  let request;
  let entitlementsAPIStub;

  before(() => {
    const app = express();
    app.use(authenticateRequest(), entitlements, (req: KBYGRequest & { spot: string }, res) =>
      res.send(req.spot),
    );
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    app.use(errorHandlerMiddleware({ error: sinon.spy() }));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    entitlementsAPI.default.restore();
    entitlementsAPIStub = sinon.stub(entitlementsAPI, 'default');
  });

  afterEach(() => {
    entitlementsAPIStub.restore();
  });

  it('returns 500 if get entitlements errors', async () => {
    entitlementsAPIStub.throws();

    const res = await request.get('/').set('X-Auth-UserId', '56fae6ff19b74318a73a7875').send();
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ message: 'Error encountered' });
  });
});
