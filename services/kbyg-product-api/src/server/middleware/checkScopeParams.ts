import { NextFunction, Response } from 'express';
import { KBYGRequest } from '../types';

const checkScopeParams = (req: KBYGRequest, _res: Response, next: NextFunction) => {
  const {
    scopes,
    query: { days },
  } = req;
  const premiumOverride = scopes && scopes.includes('premium:read:anonymous');
  /* eslint-disable no-param-reassign */

  if (premiumOverride && parseInt(String(days), 10) === 17) {
    req.user.premium = true;
  }
  /* eslint-enable no-param-reassign */
  next();
};

export default checkScopeParams;
