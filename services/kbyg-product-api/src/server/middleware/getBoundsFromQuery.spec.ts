import { APIError } from '@surfline/services-common';
import { expect } from 'chai';
import sinon from 'sinon';
import { describe, it } from 'mocha';

import getBoundsFromQuery from './getBoundsFromQuery';

describe('middleware / getBoundsFromQuery', () => {
  const next = sinon.stub();

  afterEach(() => {
    next.reset();
  });

  it('should throw an API Error if north is missing', () => {
    let err;
    try {
      getBoundsFromQuery({ query: { east: '1', south: '1', west: '1' } }, null, next);
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: Query parameter north is required.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if south is missing', () => {
    let err;
    try {
      getBoundsFromQuery({ query: { east: '1', north: '1', west: '1' } }, null, next);
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: Query parameter south is required.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if east is missing', () => {
    let err;
    try {
      getBoundsFromQuery({ query: { north: '1', south: '1', west: '1' } }, null, next);
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: Query parameter east is required.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if west is missing', () => {
    let err;
    try {
      getBoundsFromQuery({ query: { east: '1', south: '1', north: '1' } }, null, next);
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: Query parameter west is required.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if a bound is not a number/float', () => {
    let err;
    try {
      getBoundsFromQuery(
        { query: { east: '1', south: '1', north: '1', west: 'teststring' } },
        null,
        next,
      );
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: Query parameter west must be a number.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if a north bound is too small', () => {
    let err;
    try {
      getBoundsFromQuery(
        { query: { east: '1', south: '1', north: '-90.01', west: '1' } },
        null,
        next,
      );
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: north must be between -90 and 90.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if a north bound is too large', () => {
    let err;
    try {
      getBoundsFromQuery(
        { query: { east: '1', south: '1', north: '90.001', west: '1' } },
        null,
        next,
      );
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: north must be between -90 and 90.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if a south bound is too small', () => {
    let err;
    try {
      getBoundsFromQuery(
        { query: { east: '1', north: '1', south: '-90.01', west: '1' } },
        null,
        next,
      );
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: south must be between -90 and 90.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should throw an API Error if a south bound is too large', () => {
    let err;
    try {
      getBoundsFromQuery(
        { query: { east: '1', north: '1', south: '90.001', west: '1' } },
        null,
        next,
      );
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err).to.be.instanceOf(APIError);
    expect(err.message).to.be.equal('Invalid Parameters: south must be between -90 and 90.');
    expect(err.statusCode).to.be.equal(400);
    expect(next).not.to.have.been.called();
  });

  it('should allow all boundary coordinates', () => {
    const req = { bounds: null, query: { east: '-180', south: '-90', north: '90', west: '180' } };
    getBoundsFromQuery(req, null, next);

    expect(req.bounds).to.deep.equal({ east: -180, south: -90, north: 90, west: 180 });
    expect(next).to.have.been.called();
  });

  it('should allow all wrapping longitude coordinates', () => {
    const req = { bounds: null, query: { east: '-290', south: '-90', north: '90', west: '220' } };
    getBoundsFromQuery(req, null, next);

    expect(req.bounds).to.deep.equal({ east: -290, south: -90, north: 90, west: 220 });
    expect(next).to.have.been.called();
  });

  it('should return valid bounds translated to numbers ', () => {
    const req = { bounds: null, query: { east: '1', south: '4', north: '3.1', west: '2' } };
    getBoundsFromQuery(req, null, next);

    expect(req.bounds).to.deep.equal({ east: 1, south: 4, north: 3.1, west: 2 });
    expect(next).to.have.been.called();
  });
});
