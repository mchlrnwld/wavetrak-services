import { APIError } from '@surfline/services-common';
import { inRange, isFinite } from 'lodash';

/**
 * @typedef {object} QueryParameters
 * @property {string} north
 * @property {string} south
 * @property {string} east
 * @property {string} west
 */

/**
 * @param {number} start
 * @param {number} end
 * @returns {(value: number) => boolean}
 */
const checkIfValueInRangeInclusive = (start, end) => (value) =>
  inRange(value, start, end) || value === end;

/**
 * @param {import('express').Request<any, any, any, QueryParameters>} req
 * @param {import('express').NextFunction} next
 */
const getBoundsFromQuery = (req, _, next) => {
  const { query } = req;
  const isLatitudeInRange = checkIfValueInRangeInclusive(-90, 90);

  const [north, south, east, west] = ['north', 'south', 'east', 'west'].map((key) => {
    if (!query[key]) {
      throw new APIError(`Invalid Parameters: Query parameter ${key} is required.`);
    }

    const value = parseFloat(query[key]);
    if (!isFinite(value)) {
      throw new APIError(`Invalid Parameters: Query parameter ${key} must be a number.`);
    }

    if (key === 'north' || key === 'south') {
      if (!isLatitudeInRange(value)) {
        throw new APIError(`Invalid Parameters: ${key} must be between -90 and 90.`);
      }
    }

    return value;
  });

  // eslint-disable-next-line no-param-reassign
  req.bounds = {
    north,
    south,
    east,
    west,
  };

  return next();
};

export default getBoundsFromQuery;
