import { NextFunction, Response } from 'express';
import { KBYGRequest } from '../types';

const correctedWaveHeights = (req: KBYGRequest, _res: Response, next: NextFunction) => {
  // eslint-disable-next-line no-param-reassign
  req.correctedWaveHeights = req.query.corrected !== 'false';
  return next();
};

export default correctedWaveHeights;
