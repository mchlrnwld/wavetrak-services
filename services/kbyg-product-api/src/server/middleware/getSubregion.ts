import { wrapErrors } from '@surfline/services-common';
import { Types } from 'mongoose';
import * as spotsAPI from '../../external/spots';

const message = 'Valid spotId or subregionId required';

const getSubregion = async (req, res, next) => {
  /* eslint-disable no-param-reassign */
  let { subregionId } = req.query;
  const { spotId, id } = req.query;

  if (id) {
    subregionId = id;
  }

  if (
    (!subregionId || !Types.ObjectId.isValid(subregionId)) &&
    (!spotId || !Types.ObjectId.isValid(spotId))
  ) {
    return res.status(400).send({ message });
  }

  if (!subregionId) {
    const spot = await spotsAPI.getSpot(spotId);
    if (!spot) {
      return res.status(400).send({ message });
    }
    subregionId = spot.subregion;
  }

  const subregion = subregionId ? await spotsAPI.getSubregion(subregionId) : null;

  if (!subregion) {
    return res.status(400).send({ message });
  }

  req.subregion = subregion;

  return next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(getSubregion);
