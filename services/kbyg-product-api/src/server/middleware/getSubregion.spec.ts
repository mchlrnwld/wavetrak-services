import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { errorHandlerMiddleware } from '@surfline/services-common';
import * as spotsAPI from '../../external/spots';
import getSubregion from './getSubregion';
import { KBYGRequest } from '../types';

describe('middleware / getSubregion', () => {
  const subregionId = '58581a836630e24c44878fd6';
  let request;

  /** @type {sinon.SinonStub} */
  let getSubregionStub;

  before(() => {
    const app = express();
    app.use(getSubregion, (req: KBYGRequest & { subregion: string }, res) =>
      res.send(req.subregion),
    );
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    app.use(errorHandlerMiddleware({ error: sinon.spy() }));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    spotsAPI.getSubregion.restore();
    getSubregionStub = sinon.stub(spotsAPI, 'getSubregion');
  });

  afterEach(() => {
    getSubregionStub.restore();
  });

  it('appends subregion to request object', async () => {
    getSubregionStub.resolves({ _id: subregionId });
    const res = await request.get(`/?subregionId=${subregionId}`).send();
    expect(res).to.have.status(200);
    expect(res.body).to.be.ok();
    expect(res.body._id).to.equal(subregionId);
  });

  it('returns 400 if subregion ID is not found', async () => {
    getSubregionStub.resolves(null);

    const res = await request.get('/').send();
    expect(res).to.have.status(400);
    expect(res.body).to.deep.equal({ message: 'Valid spotId or subregionId required' });
  });

  it('returns 500 if get subregion errors', async () => {
    getSubregionStub.throws();

    const res = await request.get(`/?subregionId=${subregionId}`).send();
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ message: 'Error encountered' });
  });
});
