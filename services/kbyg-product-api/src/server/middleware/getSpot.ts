import newrelic from 'newrelic';
import { wrapErrors } from '@surfline/services-common';
import { Types } from 'mongoose';
import { getSpot as fetchSpot } from '../../external/spots';

const getSpot = async (req, res, next) => {
  /* eslint-disable no-param-reassign */

  const {
    query: { spotId },
  } = req;

  newrelic.addCustomAttribute(spotId, spotId);

  if (!spotId || !Types.ObjectId.isValid(spotId)) {
    return res.status(400).send({
      message: 'Valid spotId required',
    });
  }

  const spot = await fetchSpot(spotId);

  if (!spot) {
    return res.status(400).send({
      message: 'Valid spotId required',
    });
  }
  req.spot = spot;
  return next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(getSpot);
