import { wrapErrors } from '@surfline/services-common';
import { NextFunction, Response } from 'express';
import newrelic from 'newrelic';
import getUnits from '../../utils/getUnits';
import { KBYGRequest } from '../types';

const userUnits = async (req: KBYGRequest, _res: Response, next: NextFunction) => {
  /* eslint-disable no-param-reassign */
  req.units = await getUnits(req.authenticatedUserId, req.geoCountryIso, newrelic);

  const { units } = req.query;

  if (units) {
    const { temperature, tideHeight, swellHeight, waveHeight, windSpeed } = units;

    if (temperature) req.units.temperature = temperature;
    if (tideHeight) req.units.tideHeight = tideHeight;
    if (swellHeight) req.units.swellHeight = swellHeight;
    if (waveHeight) req.units.waveHeight = waveHeight;
    if (windSpeed) req.units.windSpeed = windSpeed;
  }

  return next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(userUnits);
