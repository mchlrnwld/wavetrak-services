import { wrapErrors } from '@surfline/services-common';
import getEntitlements from '../../external/entitlements';
import userTypes from '../../utils/userTypes';

const entitleRequest = async (req, _, next) => {
  /* eslint-disable no-param-reassign */
  req.user = {
    entitlements: [],
    premium: false,
  };
  if (req.authenticatedUserId) {
    const userEntitlements = await getEntitlements(req.authenticatedUserId);
    const premium = userEntitlements.entitlements.includes('sl_premium');
    req.user = {
      id: req.authenticatedUserId,
      entitlements: userEntitlements.entitlements,
      premium,
    };
    const metered = userEntitlements.entitlements.includes('sl_metered');
    if (metered && !premium) {
      const anonymousId = req.get('x-auth-anonymousid');
      const { scopes } = req;
      const premiumOverride = scopes && scopes.includes('premium:read:registered');
      if (anonymousId && premiumOverride) {
        req.user = {
          ...req.user,
          premium: true,
          type: userTypes.REGISTERED,
        };
      }
    }
  }
  const anonymousId = req.get('x-auth-anonymousid');
  const { scopes } = req;
  const premiumOverride = scopes && scopes.includes('premium:read:anonymous');
  if (anonymousId && premiumOverride) {
    req.user = {
      id: anonymousId,
      premium: true,
      entitlements: ['sl_metered'],
      type: userTypes.ANONYMOUS,
    };
  }
  next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(entitleRequest);
