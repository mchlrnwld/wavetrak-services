import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { errorHandlerMiddleware } from '@surfline/services-common';
import * as spotsAPI from '../../external/spots';
import getSpot from './getSpot';
import { KBYGRequest } from '../types';

describe('middleware / getSpot', () => {
  let request;

  before(() => {
    const app = express();
    app.use(getSpot, (req: KBYGRequest & { spot: string }, res) => res.send(req.spot));
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    app.use(errorHandlerMiddleware({ error: sinon.spy() }));
    request = chai.request(app).keepOpen();
  });

  /** @type {sinon.SinonStub} */
  let getSpotStub;

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    spotsAPI.getSpot.restore();
    getSpotStub = sinon.stub(spotsAPI, 'getSpot');
  });

  afterEach(() => {
    getSpotStub.restore();
  });

  it('appends spot to request object', async () => {
    const spotId = '5842041f4e65fad6a77088ed';
    getSpotStub.resolves({ _id: spotId });
    const res = await request.get(`/?spotId=${spotId}`).send();
    expect(res).to.have.status(200);
    expect(res.body).to.be.ok();
    expect(res.body._id).to.equal(spotId);
  });

  it('returns 400 if spot ID is not found', async () => {
    getSpotStub.resolves(null);

    const res = await request.get('/?spotId=1231231').send();
    expect(res).to.have.status(400);
    expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
  });

  it('returns 400 if no spot ID is sent', async () => {
    getSpotStub.resolves(null);

    const res = await request.get('/').send();
    expect(res).to.have.status(400);
    expect(res.body).to.deep.equal({ message: 'Valid spotId required' });
  });

  it('returns 500 if get spot errors', async () => {
    getSpotStub.throws();

    const res = await request.get('/?spotId=6179683113479532ce688f4b').send();
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ message: 'Error encountered' });
  });
});
