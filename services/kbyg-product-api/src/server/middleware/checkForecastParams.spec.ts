import sinon from 'sinon';
import { expect } from 'chai';
import checkForecastParams from './checkForecastParams';

describe('middleware / checkForecastParams', () => {
  const next = sinon.spy();
  const send = sinon.spy();
  const res = { status: sinon.stub().returns({ send }) };

  afterEach(() => {
    next.resetHistory();
    send.resetHistory();
    res.status.resetHistory();
  });

  it('should return 17 days for a premium user', () => {
    const req = { query: {}, user: { premium: true }, forecast: null };
    checkForecastParams(req, res, next);

    expect(req.forecast).to.deep.equal({
      days: 17,
      offset: 0,
      intervalHours: 1,
      maxHeights: false,
    });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should return 17 days for a registered user', () => {
    const req = { query: {}, user: { premium: false }, forecast: null };
    checkForecastParams(req, res, next);

    expect(req.forecast).to.deep.equal({
      days: 17,
      offset: 0,
      intervalHours: 1,
      maxHeights: false,
    });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should return 17 days for a premium partner', () => {
    const req = {
      query: {},
      user: { premium: false },
      scopes: ['forecast:read:premium-partner'],
      forecast: null,
    };
    checkForecastParams(req, res, next);

    expect(req.forecast).to.deep.equal({
      days: 17,
      offset: 0,
      intervalHours: 1,
      maxHeights: false,
    });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should override days query param to 3 days for a 3rd Party request', () => {
    const req = { query: { days: 6, thirdParty: true }, user: { premium: false }, forecast: null };
    checkForecastParams(req, res, next);

    expect(req.forecast).to.deep.equal({ days: 3, offset: 0, intervalHours: 1, maxHeights: false });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should override days query param to 3 days for a non-premium partner 3rd Party request', () => {
    const req = {
      query: { days: 10, thirdParty: true },
      user: { premium: false },
      scopes: ['sessions:write:user'],
      forecast: null,
    };
    checkForecastParams(req, res, next);
    expect(req.forecast).to.deep.equal({ days: 3, offset: 0, intervalHours: 1, maxHeights: false });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should error if more than 17 days are requested for a non-premium user', () => {
    const req = { query: { days: 18 }, user: { premium: false } };
    checkForecastParams(req, res, next);

    expect(res.status).to.have.been.calledOnce();
    expect(res.status).to.have.been.calledWithExactly(400);
    expect(send).to.have.been.calledOnce();
    expect(send).to.have.been.calledWithExactly({ message: 'Parameters out of bounds' });
    expect(next).not.to.have.been.called();
  });

  it('should error if data past 17 days is requested', () => {
    const req = { query: { days: 17, offset: 4 }, user: { premium: true } };
    checkForecastParams(req, res, next);

    expect(res.status).to.have.been.calledOnce();
    expect(res.status).to.have.been.calledWithExactly(400);
    expect(send).to.have.been.calledOnce();
    expect(send).to.have.been.calledWithExactly({ message: 'Parameters out of bounds' });
    expect(next).not.to.have.been.called();
  });

  it('should error if data past 17 days is requested for premium partner', () => {
    const req = {
      query: { days: 17, offset: 4 },
      user: { premium: false },
      scopes: ['forecast:read:premium-partner'],
    };
    checkForecastParams(req, res, next);

    expect(res.status).to.have.been.calledOnce();
    expect(res.status).to.have.been.calledWithExactly(400);
    expect(send).to.have.been.calledOnce();
    expect(send).to.have.been.calledWithExactly({ message: 'Parameters out of bounds' });
    expect(next).not.to.have.been.called();
  });
});
