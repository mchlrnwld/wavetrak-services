import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { errorHandlerMiddleware } from '@surfline/services-common';
import * as getUnits from '../../utils/getUnits';
import userUnits from './userUnits';
import { KBYGRequest } from '../types';

describe('middleware / userUnits', () => {
  let request;
  let getUnitsStub;

  before(() => {
    const app = express();
    app.use(userUnits, (req: KBYGRequest, res) => res.send(req.units));
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    app.use(errorHandlerMiddleware({ error: sinon.spy() }));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    getUnitsStub = sinon.stub(getUnits, 'default');
  });

  afterEach(() => {
    getUnitsStub.restore();
  });

  it('should parse units query params and prioritize over user settings', async () => {
    const unitsFixture = {
      swellHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    };

    getUnitsStub.resolves(unitsFixture);

    const res = await request.get('/?units[waveHeight]=FT').send();

    expect(res.body).to.deep.equal({
      ...unitsFixture,
      waveHeight: 'FT',
    });
  });

  it('returns 500 if get units errors', async () => {
    getUnitsStub.throws();

    const res = await request.get('/').send();
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ message: 'Error encountered' });
  });
});
