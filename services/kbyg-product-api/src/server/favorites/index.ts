import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import userUnits from '../middleware/userUnits';
import {
  getFavoritesHandler,
  trackFavoritesRequests,
  getFavoriteSubregionsHandler,
} from './favorites';
import getStormsHandler from './storms';

const favoritesView = (log) => {
  const api = Router();

  api.use('*', trackFavoritesRequests(log));
  api.get('/', userUnits, wrapErrors(getFavoritesHandler));
  api.get('/storms', userUnits, wrapErrors(getStormsHandler));
  api.get('/subregions', userUnits, wrapErrors(getFavoriteSubregionsHandler));

  return api;
};

export default favoritesView;
