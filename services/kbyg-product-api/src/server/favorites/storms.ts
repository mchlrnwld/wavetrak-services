import getFavorites from '../../external/favorites';
import { getTaxonomyByIds } from '../../external/spots';
import getAllEvents from '../../external/events';

/**
 * @param {Express.Request} req
 * @param {Express.Response} res
 */
const getStormsHandler = async (req, res) => {
  let storms = [];
  if (req.authenticatedUserId) {
    const favorites = await getFavorites(req.authenticatedUserId);
    const spots = favorites.favorites.map(({ spotId }) => spotId);
    const taxonomyIdsResponse = await getTaxonomyByIds({ spots });
    const taxonomyIds = taxonomyIdsResponse.taxonomy.map(({ _id }) => _id);
    const eventsJSON = await getAllEvents();
    storms = eventsJSON.events.filter((elem) =>
      elem.taxonomies.some((item) => taxonomyIds.includes(item.id)),
    );
  }
  return res.send({
    data: {
      storms,
    },
  });
};

export default getStormsHandler;
