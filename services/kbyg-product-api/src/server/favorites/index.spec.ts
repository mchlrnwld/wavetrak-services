import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import favorites from '.';
import * as userfavorites from '../../external/favorites';
import * as taxonomy from '../../external/spots';
import * as swellEvents from '../../external/events';
import * as units from '../../utils/getUnits';
import * as getSpotReportViewsIn from '../../model/SpotReportView/getSpotReportViewsIn';
import spotReportViewsInFixture from './fixtures/spotReportViewsIn.json';

describe('/favorites', () => {
  let log;
  let request;
  let getSpotReportViewsInStub;
  let userFavoritesStub;
  let getTaxonomyByIdsStub;
  let swellEventsStub;
  let unitsStub;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(authenticateRequest(), favorites(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    userFavoritesStub = sinon.stub(userfavorites, 'default').resolves({
      favorites: [
        {
          _id: '590bbb41151cc9000f9ac272',
          spotId: '5842041f4e65fad6a77088e7',
          kind: 'Spot',
          rank: 0,
        },
      ],
    });

    getTaxonomyByIdsStub = sinon.stub(taxonomy, 'getTaxonomyByIds').resolves({
      taxonomy: [
        {
          _id: '58f80848dadb30820bce9644',
          name: 'North Orange County',
          type: 'spot',
          spot: '584204204e65fad6a770998d',
        },
      ],
    });

    unitsStub = sinon.stub(units, 'default').resolves({
      swellHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    });

    swellEventsStub = sinon.stub(swellEvents, 'default').resolves({
      events: [
        {
          id: '1tNeXiRBBGkGcqmGKOAEck',
          createdAt: '2017-10-22T21:33:17.727Z',
          name: 'Test Event by Ajith',
          stormCategory: 'significantSwell',
          summary: 'Test Summary by Ajith',
          banner:
            'https://images.contentful.com/zxy8mg9a81nc/1IarxQjMJSaMCMeq0U4Wai/5dc30d1291ae23cc20d80bf84584647e/matthew_ThuAM.gif',
          taxonomies: [
            {
              id: '58f7ed5ddadb30820bb39651',
              name: 'Orange County',
              type: 'geoname',
              breadCrumbs: ['United States', 'California'],
            },
            {
              id: '58f80848dadb30820bce9644',
              name: 'North Orange County',
              type: 'subregion',
              breadCrumbs: null,
            },
          ],
        },
      ],
    });

    getSpotReportViewsInStub = sinon.stub(getSpotReportViewsIn, 'default');
  });

  afterEach(() => {
    userFavoritesStub.restore();
    getTaxonomyByIdsStub.restore();
    swellEventsStub.restore();
    unitsStub.restore();
    getSpotReportViewsInStub.restore();
  });

  it('should returns storm alerts for a single user', async () => {
    const res = await request.get('/storms').set('X-Auth-UserId', '56faec4119b74318a73d2e9e');
    expect(res).to.have.status(200);
    expect(res.body.data.storms).to.be.an('array');
    expect(res.body.data.storms[0].taxonomies[1].name).to.equal('North Orange County');
  });

  it('should return favorites for a single user', async () => {
    getSpotReportViewsInStub.resolves([]);

    const res = await request.get('/').set('X-Auth-UserId', '56faec4119b74318a73d2e9e');
    expect(res).to.have.status(200);
    expect(res).to.be.json();
  });

  it('should return favorites with conditions using old rating scale by default', async () => {
    getSpotReportViewsInStub.resolves(spotReportViewsInFixture);

    const res = await request.get('/').set('X-Auth-UserId', '56faec4119b74318a73d2e9e');

    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body.data.favorites[0].conditions.value).to.equal('GOOD');
    expect(res.body.data.favorites[1].conditions.value).to.equal('GOOD_TO_EPIC');
    expect(res.body.data.favorites[2].conditions.value).to.equal('FLAT');
    expect(res.body.data.favorites[3].conditions.value).to.equal('VERY_GOOD');
  });

  it('should return favorites with conditions using old rating scale if sevenRatings param is false', async () => {
    getSpotReportViewsInStub.resolves(spotReportViewsInFixture);

    const res = await request
      .get('?sevenRatings=false')
      .set('X-Auth-UserId', '56faec4119b74318a73d2e9e');

    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body.data.favorites[0].conditions.value).to.equal('GOOD');
    expect(res.body.data.favorites[1].conditions.value).to.equal('GOOD_TO_EPIC');
    expect(res.body.data.favorites[2].conditions.value).to.equal('FLAT');
    expect(res.body.data.favorites[3].conditions.value).to.equal('VERY_GOOD');
  });

  it('should return favorites with conditions using new rating scale if sevenRatings param is true', async () => {
    getSpotReportViewsInStub.resolves(spotReportViewsInFixture);

    const res = await request
      .get('?sevenRatings=true')
      .set('X-Auth-UserId', '56faec4119b74318a73d2e9e');

    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body.data.favorites[0].conditions.value).to.equal('GOOD');
    expect(res.body.data.favorites[1].conditions.value).to.equal('GOOD');
    expect(res.body.data.favorites[2].conditions.value).to.equal('VERY_POOR');
    expect(res.body.data.favorites[3].conditions.value).to.equal('GOOD');
  });
});
