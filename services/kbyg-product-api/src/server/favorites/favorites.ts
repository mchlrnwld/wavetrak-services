import _uniq from 'lodash/uniq';
import { getSpotReportViewsIn } from '../../model/SpotReportView';
import getFavorites from '../../external/favorites';
import { getSubregionsBySubregionIds } from '../../external/spots';
import calculateDistance from '../../utils/calculateDistance';
import { METRIC } from '../../common/constants';
import { getAnonymousUserFavorites } from './utils';
import { getLOLAConditionsForSubregion } from '../../model/getLOLAConditions/getLOLAConditionsForSubregion';
import { getWaveHeightUnitsFromQuery } from '../../utils/getUnits';
import { convertConditionsRating } from '../../utils/convertTo7Ratings';

export const trackFavoritesRequests = (log) => (req, _, next) => {
  log.trace({
    action: '/kbyg/favorites',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getFavoritesHandler = async (req, res) => {
  try {
    const sevenRatings = req.query.sevenRatings === 'true';
    const { favorites: favoritesSpots, error } = req.authenticatedUserId
      ? await getFavorites(req.authenticatedUserId)
      : await getAnonymousUserFavorites(req);

    if (error) {
      throw new Error(`Something went wrong fetching favorites: ${error}`);
    }
    const spots = req.authenticatedUserId
      ? favoritesSpots.map(({ spotId }) => spotId)
      : favoritesSpots.map((spot) => spot.spot);

    const units = req.query.units
      ? {
          waveHeight: getWaveHeightUnitsFromQuery(req.query.units),
          windSpeed: 'KTS',
          tideHeight: req.query.units === METRIC ? 'M' : 'FT',
        }
      : req.units;

    let spotReportViews;
    if (!req.authenticatedUserId) {
      spotReportViews = await getSpotReportViewsIn(spots, units, {
        'conditions.sortableCondition': -1,
        'waveHeight.max': -1,
      });

      if (spotReportViews.length > 3) {
        spotReportViews = spotReportViews.filter((report, index) => {
          const distanceBetween = calculateDistance(
            spotReportViews[0].lat,
            spotReportViews[0].lon,
            report.lat,
            report.lon,
          );
          // .8 kilometers from first spot
          return distanceBetween > 0.8 || index === 0;
        });
      }
      spotReportViews = spotReportViews.slice(0, 3);
    } else {
      spotReportViews = await getSpotReportViewsIn(spots, units);
    }

    const spotMap = new Map(favoritesSpots.map((spot, index) => [spot.spotId, spot.rank || index]));

    const favorites = spotReportViews
      .map((spot) => ({
        _id: spot._id,
        name: spot.name,
        cameras: spot.cameras,
        conditions: sevenRatings ? convertConditionsRating(spot.conditions) : spot.conditions,
        wind: spot.wind,
        waveHeight: spot.waveHeight,
        tide: spot.tide,
        subregionId: spot.subregionId,
        thumbnail: spot.thumbnail,
        slug: spot.slug,
        rank: spotMap.get(`${spot._id}`),
      }))
      .sort((prev, next) => prev.rank - next.rank);

    return res.send({
      associated: {
        units,
      },
      data: {
        favorites,
      },
    });
  } catch (error) {
    throw new Error(`Something went wrong fetching favorites: ${error.message}`);
  }
};

/**
 * @description Looks up favorites (based on user ID or geo location for anonymous users)
 * and returns an array of Subregions that contain those favorites.
 *
 * @response
 * ```json
 * {
 *  "subregions": [
 *    {
 *      "_id": "1",
 *      "name": "North Orange County",
 *      "spots": ["ARRAY WITH ALL SUBREGIONS SPOTS"]
 *    },
 *    {
 *      "_id": "2",
 *      "name": "South Orange County",
 *      "spots": ["ARRAY WITH ALL SUBREGIONS SPOTS"]
 *    }
 *  ]
 * }
 * ```
 * @param {Object} req
 * @param {Object} req.query
 * @param {Object} req.units
 * @param {string} req.geoLatitude
 * @param {string} req.geoLongitude
 * @param {string} [req.query.units]
 * @param {string} [req.query.lat]
 * @param {string} [req.query.lon]
 * @param {Object} res
 * @param {Function} res.send
 * @author Jeremy Monson (jmonson)
 */
export const getFavoriteSubregionsHandler = async (req, res) => {
  const { favorites, error } = req.authenticatedUserId
    ? await getFavorites(req.authenticatedUserId)
    : await getAnonymousUserFavorites(req);

  if (error) {
    return false;
  }

  const units = req.query.units
    ? {
        windSpeed: 'KTS',
        waveHeight: req.query.units === METRIC ? 'M' : 'FT',
        tideHeight: req.query.units === METRIC ? 'M' : 'FT',
      }
    : req.units;

  // Get the spot report views for each favorite so we can get their subregion ID's
  const spots = await getSpotReportViewsIn(
    // The favorites API for authenticated users has a different structure than the
    // endpoint for anon users so we have to handle both.
    favorites
      .filter((favorite) => favorite.type === 'spot' || favorite.kind === 'Spot')
      .map((favorite) => favorite.spot || favorite.spotId),
    units,
  );

  // Fetch the subregions
  const subregionIds = spots ? _uniq(spots.map((spot) => spot.subregionId.toString())) : [];
  let { subregions } = subregionIds.length
    ? await getSubregionsBySubregionIds(subregionIds)
    : { subregions: [] };

  try {
    if (req.query.conditions) {
      subregions = await Promise.all(
        subregions.map(async (subregion) => {
          const { conditions } = await getLOLAConditionsForSubregion(subregion, 2, units);
          return {
            ...subregion,
            conditions,
          };
        }),
      );
    }

    // If sortBy=alphaDesc, sort subregions alphabetically by name
    if (req.query.sortBy && req.query.sortBy === 'alphaDesc') {
      subregions = subregions.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
    }
    return res.send({
      associated: {
        units: {
          waveHeight: units.waveHeight,
          windSpeed: units.windSpeed,
          tideHeight: units.tideHeight,
        },
      },
      subregions,
    });
  } catch (err) {
    throw new Error(`Caught Error in getFavoriteSubregionsHandler: ${err.message}`);
  }
};
