import { getNearbyTaxonomy } from '../../external/spots';
import calculateDistance from '../../utils/calculateDistance';

// eslint-disable-next-line import/prefer-default-export
export const getAnonymousUserFavorites = async (req) => {
  const { query, geoLatitude, geoLongitude } = req;
  const lat = query.lat || geoLatitude;
  const lon = query.lon || geoLongitude;
  const coordinates = [parseFloat(lon), parseFloat(lat)];
  const nearbyTaxonomy = await getNearbyTaxonomy('geoname', 'A', 5, 0, coordinates, 5);
  const nearbyCountyWithSpots = nearbyTaxonomy.data
    .filter((taxonomy) => taxonomy.hasSpots === true)
    .map((county) => {
      const newCounty = { ...county };
      newCounty.distance = calculateDistance(
        lat,
        lon,
        county.geonames.lat,
        county.geonames.lng,
        'K',
      );
      return newCounty;
    })
    .sort((a, b) => a.distance - b.distance);

  if (nearbyCountyWithSpots.length === 0) {
    return { favorites: [] };
  }

  const favorites = nearbyCountyWithSpots[0].contains.filter(
    (location) => location.type === 'spot',
  );
  return { favorites };
};
