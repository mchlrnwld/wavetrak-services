import { Router } from 'express';
import { trackSwellsRequests, getSwellsHandler } from './swells';

const swells = (log) => {
  const api = Router();

  api.use('*', trackSwellsRequests(log));
  api.get('/', getSwellsHandler);

  return api;
};

export default swells;
