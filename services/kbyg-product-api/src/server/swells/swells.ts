import type { NextFunction, Response } from 'express';
import { KBYGRequest } from '../types';

export const trackSwellsRequests =
  (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
    log.trace({
      action: '/kbyg/swells',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    return next();
  };

/**
 * @deprecated This endpoint is deprecated. However we are required
 * to keep is as a resolving endpoint since older versions of the Surfline
 * Mobile App are still making requests to this endpoint.
 * @param {Express.Request} req
 * @param {Express.Response} res
 */
export const getSwellsHandler = (_req: KBYGRequest, res: Response) =>
  res.send({ significantSwells: [] });
