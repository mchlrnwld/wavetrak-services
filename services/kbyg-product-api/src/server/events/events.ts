import { NextFunction, Response } from 'express';
import { Types } from 'mongoose';
import getEventsForTaxonomy from './getEventsForTaxonomy';
import { getSpot, getSubregion } from '../../external/spots';
import { KBYGRequest } from '../types';

export const trackEventsRequests =
  (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
    log.trace({
      action: '/kbyg/events/',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    return next();
  };

export const getEventsHandler = async (req: KBYGRequest, res: Response) => {
  const { id, type } = req.query;

  if (!id || !Types.ObjectId.isValid(id.toString()) || !type) {
    return res.status(400).send({
      message: 'Valid spot or subregion ID and type required',
    });
  }

  try {
    // Run validation to check if spot or subregion exists
    let result = null;
    if (type === 'spot') {
      result = await getSpot(id);
    } else if (type === 'subregion') {
      result = await getSubregion(id);
    }
    // End validation
    if (result) {
      const events = await getEventsForTaxonomy(id, type);
      return res.send({ data: events });
    }
    return res.status(400).send({ error: 'No results for ID and type combination' });
  } catch (err) {
    return res.send(err);
  }
};
