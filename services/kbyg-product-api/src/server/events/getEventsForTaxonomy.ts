import getAllEvents from '../../external/events';
import { getTaxonomy } from '../../external/spots';

const getEventsForTaxonomy = async (id, type) => {
  const taxonomy = await getTaxonomy(id, type, 0);
  const inTaxIDs = taxonomy.in.map((tax) => tax._id);
  // If there's an event in this taxonomy, we want to find that as well
  inTaxIDs.push(taxonomy._id);
  const allEvents = await getAllEvents();
  return allEvents.events.filter(
    (event) =>
      event.targetAllLocations ||
      event.taxonomies.some((eventTax) => inTaxIDs.includes(eventTax.id)),
  );
};

export default getEventsForTaxonomy;
