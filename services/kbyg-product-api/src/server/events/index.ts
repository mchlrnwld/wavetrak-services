import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { getEventsHandler, trackEventsRequests } from './events';

const events = (log) => {
  const api = Router();

  api.use('*', trackEventsRequests(log));
  api.get('/taxonomy', wrapErrors(getEventsHandler));

  return api;
};

export default events;
