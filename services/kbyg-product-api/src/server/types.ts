import { SurflineQuery, SurflineRequest } from '@surfline/services-common';

// HACK: SurflineQuery defines all query parameters as type string, which does not account for
// nested parameters like units.
interface Units {
  temperature: string;
  tideHeight: string;
  swellHeight: string;
  waveHeight: string;
  windSpeed: string;
}

export interface KBYGRequest extends SurflineRequest {
  bounds: {
    north: number;
    south: number;
    east: number;
    west: number;
  };
  correctedWaveHeights: boolean;
  query: SurflineQuery & { units: Units };
  user: {
    premium: boolean;
  };
}

export interface KBYGSpotForecastRequest extends SurflineRequest {
  correctedWaveHeights: boolean;
  forecast: {
    days: number;
    intervalHours: number;
    maxHeights: boolean;
    offset: number;
  };
  query: SurflineQuery & {
    corrected: string;
    correctedSurf: string;
    end: number;
    ratingsAlgorithm: string;
    sevenRatings: string;
    start: number;
    thirdParty: string;
  };
  spot: {
    _id: string;
  };
}

interface Condition {
  condition: number;
  humanRelation: string;
  maxHeight: number;
  minHeight: number;
  occasionalHeight: null;
  rating: string;
}

export interface ForecastDay {
  am: Condition;
  pm: Condition;
  observation: string;
}
