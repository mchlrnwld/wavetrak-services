import { setupExpress } from '@surfline/services-common';
import mapview from './mapview';
import swells from './swells';
import favorites from './favorites';
import events from './events';
import cotm from './cotm';
import redirect from './redirect';
import regions from './regions';
import spots from './spots';
import buoys from './buoys';
import config from '../config';
import { LOLAConditionsLengthErrorHandler } from '../errors/LOLAConditionsLengthError';

const setupApp = (log) => {
  setupExpress({
    port: config.EXPRESS_PORT,
    name: 'kbyg-product-api',
    log,
    allowedMethods: ['GET', 'OPTIONS'],
    handlers: [
      ['/buoys', buoys(log)],
      ['/mapview', mapview(log)],
      ['/swells', swells(log)],
      ['/regions', regions(log)],
      ['/spots', spots(log)],
      ['/favorites', favorites(log)],
      ['/cotm', cotm(log)],
      ['/redirect', redirect(log)],
      ['/events', events(log)],
      LOLAConditionsLengthErrorHandler,
    ],
  });
};

export default setupApp;
