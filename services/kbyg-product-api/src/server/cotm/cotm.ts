import { NextFunction, Response } from 'express';
import { getSpotReportViewByCamId } from '../../model/SpotReportView';
import getCOTM from '../../external/cameras';
import { KBYGRequest } from '../types';

export const trackCOTMRequests =
  (log) => (req: KBYGRequest, _res: Response, next: NextFunction) => {
    log.trace({
      action: '/kbyg/cotm',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    return next();
  };

export const getCOTMHandler = async (req: KBYGRequest, res: Response) => {
  const { _id: cameraId } = await getCOTM();
  const { units } = req;

  const spotReportView = await getSpotReportViewByCamId(cameraId, units);

  const advertising = {
    spotId: spotReportView.legacyId?.toString(),
    subregionId: spotReportView.legacyRegionId?.toString(),
  };

  return res.send({
    associated: {
      units,
      advertising,
    },
    data: {
      spot: {
        ...spotReportView,
        cameras: spotReportView.cameras.filter((cam) => cam._id.toString() === cameraId),
      },
    },
  });
};
