import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import userUnits from '../middleware/userUnits';
import { getCOTMHandler, trackCOTMRequests } from './cotm';

const cotmView = (log) => {
  const api = Router();

  api.use('*', trackCOTMRequests(log));
  api.get('/', userUnits, wrapErrors(getCOTMHandler));

  return api;
};

export default cotmView;
