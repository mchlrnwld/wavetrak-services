import productCDN from '@surfline/quiver-assets';

const IMPERIAL = 'e';
const HAWAII = 'h';
const METRIC = 'm';
const WEATHER_ICON_PATH = `${productCDN}/weathericons`;

export { IMPERIAL, METRIC, HAWAII, WEATHER_ICON_PATH };
