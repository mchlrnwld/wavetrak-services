import newrelic from 'newrelic';

class LOLAConditionsLengthError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, LOLAConditionsLengthError);
    this.message = 'Mismatch: Data length. Could not aggregate all required data components.';
  }
}

export const LOLAConditionsLengthErrorHandler = (error, _, res, next) => {
  if (error instanceof LOLAConditionsLengthError) {
    newrelic.noticeError(error);
    return res.status(500).send({ message: error.message });
  }
  return next(error);
};

export default LOLAConditionsLengthError;
