import 'newrelic';
import { createLogger, initCache, setupLogsene } from '@surfline/services-common';
import app from './server';
import * as dbContext from './model/dbContext';
import config from './config';

const start = async () => {
  setupLogsene(config.LOGSENE_KEY);
  const log = createLogger('kbyg-product-api');

  try {
    await initCache({
      enabled: process.env.CACHE_ENABLED === 'true',
      logger: log,
      prefix: 'kbyg-product-api:',
      readRedis: `${process.env.CACHE_READ_HOST}:${process.env.CACHE_READ_PORT}`,
      writeRedis: `${process.env.CACHE_WRITE_HOST}:${process.env.CACHE_WRITE_PORT}`,
    });
  } catch (err) {
    log.error({
      message: 'Cache Initialization failed',
      stack: err,
    });
  }

  try {
    await Promise.all([dbContext.initMongoDB(log)]);
    app(log);
  } catch (err) {
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  }
};

start();
