import { LogLevel } from 'bunyan';

export default {
  CAMERAS_API: process.env.CAMERAS_API,
  ENTITLEMENTS_API: process.env.ENTITLEMENTS_SERVICE,
  FAVORITES_API: process.env.FAVORITES_API,
  FEED_API: process.env.FEED_API,
  FORECASTS_API: process.env.FORECASTS_API,
  GEO_TARGET_API: process.env.GEO_TARGET_SERVICE,
  REPORTS_API: process.env.REPORTS_API,
  SPOTS_API: process.env.SPOTS_API,
  TIDE_API: process.env.TIDE_API,
  SHARED_TIDE_API: process.env.SHARED_TIDE_API,
  USER_API: process.env.USER_SERVICE,
  MONGO_CONNECTION_STRING_KBYG: process.env.MONGO_CONNECTION_STRING_KBYG,
  EXPRESS_PORT: parseInt(process.env.EXPRESS_PORT, 10) || 8081,
  LOGSENE_KEY: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63',
  CONSOLE_LOG_LEVEL: (process.env.CONSOLE_LOG_LEVEL || 'debug') as LogLevel,
  LOGSENE_LEVEL: (process.env.LOGSENE_LEVEL || 'debug') as LogLevel,
  DATA_LAKE_BUCKET: process.env.DATA_LAKE_BUCKET || 'wt-data-lake-dev',
  SCIENCE_DATA_SERVICE: process.env.SCIENCE_DATA_SERVICE,
};
