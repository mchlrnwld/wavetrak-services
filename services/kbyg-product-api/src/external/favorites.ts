import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';

const getFavorites = async (userId) => {
  const params = { type: 'spots' };
  const url = `${config.FAVORITES_API}?${createParamString(params)}`;

  const response = await fetch(url, {
    headers: userId
      ? {
          'x-auth-userid': userId,
        }
      : null,
  });

  if (response.status === 200) return response.json();
  if (response.status === 400) return { favorites: [] };

  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export default getFavorites;
