import { cache } from '@surfline/services-common';
import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';
import { convertTideHeightUnits } from '../utils/convertUnits';

const fetchTides = async ({ locationName, start, end, units }) => {
  const params = {
    port: locationName,
    start,
    end,
  };
  const url = `${config.TIDE_API}/data?${createParamString(params)}`;

  const response = await fetch(url);
  const body = await response.json();

  if (response.status !== 200) throw new Error(`Error making request to ${url}`);

  const tides = body.levels.map((entry) => ({
    timestamp: entry.time,
    type: 'NORMAL',
    height: entry.shift,
  }));

  body.tide.forEach((tide) => {
    tides.push({ timestamp: tide.time, type: tide.state.toUpperCase(), height: tide.shift });
  });

  return {
    tides: tides
      .sort((currentTide, nextTide) => currentTide.timestamp - nextTide.timestamp)
      .map((tide) => ({
        ...tide,
        height: convertTideHeightUnits(units.tideHeight.toUpperCase(), 2, tide.height),
      })),
    associated: {
      units: {
        tideHeight: units.tideHeight,
      },
      tideLocation: {
        name: body.port.name,
        min: convertTideHeightUnits(units.tideHeight.toUpperCase(), 2, body.port.minHeight),
        max: convertTideHeightUnits(units.tideHeight.toUpperCase(), 2, body.port.maxHeight),
        lon: body.port.lon,
        lat: body.port.lat,
        mean: 0.0,
      },
    },
  };
};

export const getTides = async ({ location_name: locationName, start, end, units }) =>
  cache().fetch('getTides', () => fetchTides({ locationName, start, end, units }), {
    params: {
      location_name: locationName,
      start,
      end,
      units: units.tideHeight.toLowerCase(),
    },
    expiration: 60 * 60 * 24,
  });

export default getTides;
