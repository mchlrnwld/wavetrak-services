import fetch from 'node-fetch';

import createParamString from './createParamString';
import config from '../config';

export const getSubregionForecasts = async (
  subregionId: string,
  timezone?: string,
  days?: number,
) => {
  const params = { subregionId, timezone, days };
  const url = `${config.FORECASTS_API}/admin/forecasts?${createParamString(params)}`;

  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};
