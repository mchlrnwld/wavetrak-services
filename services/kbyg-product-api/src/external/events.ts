import fetch from 'node-fetch';
import config from '../config';

const getAllEvents = async () => {
  const url = `${config.FEED_API}/feed/events`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${response.text()}`);
};

export default getAllEvents;
