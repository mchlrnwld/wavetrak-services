import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';

const getBasinsWithinBoundingBox = async ({ north, south, east, west }) => {
  const params = { north, south, east, west };
  const url = `${config.GEO_TARGET_API}/basins?${createParamString(params)}`;

  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export default getBasinsWithinBoundingBox;
