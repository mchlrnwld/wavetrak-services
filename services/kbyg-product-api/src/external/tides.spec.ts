import { expect } from 'chai';
import * as common from '@surfline/services-common';
import sinon from 'sinon';
import * as tidesAPI from './tides';

class Response {
  static json() {
    return {
      port: {
        name: 'Santa Cruz, California',
        lat: 36.9667,
        lon: -122.0167,
        minHeight: -1.1,
        maxHeight: 2.83,
      },
      tide: [
        { time: 1636023021, shift: 0.42, state: 'Low' },
        { time: 1636046076, shift: 1.79, state: 'High' },
      ],
      levels: [
        { time: 1636009200, shift: 1.210763 },
        { time: 1636012800, shift: 0.956633 },
      ],
    };
  }
}

const tidesSpec = () => {
  describe('getTides', () => {
    let cacheStub;
    let fetchStub;

    beforeEach(() => {
      fetchStub = sinon.stub().resolves(new Response());
      cacheStub = sinon.stub(common, 'cache').returns({ fetch: fetchStub, clear: sinon.stub() });
    });

    afterEach(() => {
      cacheStub.restore();
    });

    it('should cache tide data with the correct key', async () => {
      await tidesAPI.getTides({
        location_name: 'Santa Cruz, California',
        start: 1636009200,
        end: 1636531200,
        units: {
          temperature: 'C',
          tideHeight: 'M',
          swellHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
      });
      expect(fetchStub).to.have.been.calledOnce();
      const call = fetchStub.getCall(0);
      expect(call.args[0]).to.deep.equal('getTides');
      // Anonymous function cannot be compared for equality so check
      // that correct amount of arguments are passed into function.
      expect(call.args.length).to.deep.equal(3);
      expect(call.args[2]).to.deep.equal({
        expiration: 86400,
        params: {
          location_name: 'Santa Cruz, California',
          start: 1636009200,
          end: 1636531200,
          units: 'm',
        },
      });
      await tidesAPI.getTides({
        location_name: 'Newport Beach, California',
        start: 1784009400,
        end: 1936531200,
        units: {
          temperature: 'C',
          tideHeight: 'M',
          swellHeight: 'M',
          waveHeight: 'M',
          windSpeed: 'KPH',
        },
      });
      const call2 = fetchStub.getCall(1);
      expect(call2.args[0]).to.deep.equal('getTides');
      // Anonymous function cannot be compared for equality so check
      // that correct amount of arguments are passed into function
      expect(call2.args.length).to.deep.equal(3);
      expect(call2.args[2]).to.deep.equal({
        expiration: 86400,
        params: {
          location_name: 'Newport Beach, California',
          start: 1784009400,
          end: 1936531200,
          units: 'm',
        },
      });
    });
  });
};

export default tidesSpec;
