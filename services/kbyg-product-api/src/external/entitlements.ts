import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';

const getEntitlements = async (userId) => {
  const params = { objectId: userId };
  const url = `${config.ENTITLEMENTS_API}/entitlements?${createParamString(params)}`;

  const response = await fetch(url);

  if (response.status === 200) return response.json();
  if (response.status === 400) return { entitlements: [] };

  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const isPremium = async (userId) => {
  if (!userId) return false;
  const entitlementsResponse = await getEntitlements(userId);
  return entitlementsResponse.entitlements.includes('sl_premium');
};

export default getEntitlements;
