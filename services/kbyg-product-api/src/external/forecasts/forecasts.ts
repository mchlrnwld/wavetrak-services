import { zip } from 'lodash';
import { request } from 'graphql-request';
import config from '../../config';
import {
  ratingForecastsGQL,
  waveForecastsGQL,
  weatherForecastsGQL,
  windForecastsGQL,
} from './gqlQueries';
import translateUnits from './translateUnits';

const GRAPHQL_ENDPOINT = `${config.SCIENCE_DATA_SERVICE}/graphql`;

export interface Associated {
  model: {
    coordinate: {
      lat: number;
      lon: number;
    };
    offshoreCoordinate: {
      lat: number;
      lon: number;
    };
  }[];
}

export interface Model {
  grid: {
    lat: number;
    lon: number;
  };
}

export interface Surf {
  min: number;
  max: number;
}

export interface SwellDirection {
  mean: number;
  min: number;
  max: number;
}

export interface Wave {
  combined: {
    height: number;
    mean: {
      period: number;
      direction: SwellDirection;
    };
    peak: {
      period: number;
      direction: SwellDirection;
    };
  };
  components: {
    height: number;
    period: number;
    spread: number;
    direction: SwellDirection;
  }[];
}

export interface Weather {
  temperature: number;
  conditions: string;
}

export interface Wind {
  speed: number;
  direction: number;
  directionType: string;
  gust: number;
}

const buildAssociated = (models: Model[]): Associated => ({
  model: models.map(({ grid }) => ({
    coordinate: {
      lat: grid.lat,
      lon: grid.lon,
    },
    offshoreCoordinate: {
      lat: grid.lat,
      lon: grid.lon,
    },
  })),
});

// TODO: Remove this HACK once the frontend can display 16 day SDS forecast
const hackAddMissingSDSDataPoints = (data, _start, end, interval) => {
  const intervalSeconds = interval * 60;
  const expectedLastTimestamp = end - intervalSeconds;
  const result = data.filter(({ timestamp }) => timestamp <= expectedLastTimestamp);

  let nextTimestamp = result[result.length - 1].timestamp + intervalSeconds;
  while (nextTimestamp <= expectedLastTimestamp) {
    result.push({
      ...result[result.length - 1],
      timestamp: nextTimestamp,
    });
    nextTimestamp += intervalSeconds;
  }

  return result;
};

export const getRatingForecasts = async ({
  pointOfInterestId,
  start,
  end,
  interval,
  ratingsAlgorithm,
  corrected,
  correctedSurf,
}): Promise<{
  associated: Associated;
  data: { timestamp: number; rating: { key: string; value: number } }[];
}> => {
  const {
    surfSpotForecasts: {
      rating: { models, data: ratingData },
    },
  } = await request(GRAPHQL_ENDPOINT, ratingForecastsGQL, {
    pointOfInterestId,
    start,
    end,
    interval: interval * 60,
    ratingsAlgorithm,
    corrected,
    correctedSurf,
  });

  const data = ratingData
    ? hackAddMissingSDSDataPoints(
        ratingData.map(({ timestamp, rating }) => ({
          timestamp,
          rating,
        })),
        start,
        end,
        interval,
      )
    : null;

  return {
    associated: buildAssociated(models),
    data,
  };
};

export const getWaveForecasts = async (
  { pointOfInterestId, start, end, interval, units },
  corrected = true,
): Promise<{ associated: Associated; data: { timestamp: number; surf: Surf; wave: Wave }[] }> => {
  const {
    surfSpotForecasts: {
      surf: { data: surfData },
      swells: { models, data: swellsData },
    },
  }: {
    surfSpotForecasts: {
      surf: {
        data: {
          timestamp: number;
          surf: {
            breakingWaveHeightMin: number;
            breakingWaveHeightMax: number;
          };
        }[];
      };
      swells: {
        models: Model[];
        data: {
          timestamp: number;
          swells: {
            combined: {
              direction: number;
              height: number;
              period: number;
            };
            components: {
              direction: number;
              height: number;
              period: number;
              spread: number;
              event: number;
            }[];
          };
        }[];
      };
    };
  } = await request(GRAPHQL_ENDPOINT, waveForecastsGQL, {
    pointOfInterestId,
    start,
    end,
    interval: interval * 60,
    waveHeight: translateUnits(units.waveHeight),
    swellHeight: translateUnits(units.swellHeight),
    corrected,
  });

  const data = hackAddMissingSDSDataPoints(
    zip(surfData, swellsData).map(([{ timestamp, surf }, { swells }]) => ({
      timestamp,
      surf: {
        min: surf.breakingWaveHeightMin,
        max: surf.breakingWaveHeightMax,
      },
      wave: {
        combined: {
          height: swells.combined.height,
          mean: {
            period: swells.combined.period,
            direction: {
              min: swells.combined.direction,
              max: swells.combined.direction,
              mean: swells.combined.direction,
            },
          },
          peak: {
            period: swells.combined.period,
            direction: {
              mean: swells.combined.direction,
              min: swells.combined.direction,
              max: swells.combined.direction,
            },
          },
        },
        components: [...Array(6)].map((_, i) => {
          const result = {
            height: 0,
            period: 0,
            spread: 0,
            direction: {
              mean: 0,
              min: 0,
              max: 0,
            },
          };

          const component = swells.components.find(({ event }) => event === i);
          if (component) {
            result.height = component.height;
            result.period = component.period;
            result.spread = component.spread;
            result.direction.mean = component.direction;
            result.direction.min = component.direction - component.spread / 2;
            result.direction.max = component.direction + component.spread / 2;
          }

          return result;
        }),
      },
    })),
    start,
    end,
    interval,
  );

  return {
    associated: buildAssociated(models),
    data,
  };
};

export const getWeatherForecasts = async ({
  pointOfInterestId,
  start,
  end,
  interval,
  units,
}): Promise<{ associated: Associated; data: { timestamp: number; weather: Weather }[] }> => {
  const {
    surfSpotForecasts: {
      weather: { models, data: weatherData },
    },
  } = await request(GRAPHQL_ENDPOINT, weatherForecastsGQL, {
    pointOfInterestId,
    start,
    end,
    interval: interval * 60,
    temperature: translateUnits(units.temperature),
  });

  const data = hackAddMissingSDSDataPoints(
    weatherData.map(({ timestamp, weather: { temperature, conditions } }) => ({
      timestamp,
      weather: {
        temperature,
        conditions,
      },
    })),
    start,
    end,
    interval,
  );

  return {
    associated: buildAssociated(models),
    data,
  };
};

export const getWindForecasts = async ({
  pointOfInterestId,
  start,
  end,
  interval,
  units,
}): Promise<{ associated: Associated; data: { timestamp: number; wind: Wind }[] }> => {
  const {
    surfSpotForecasts: {
      wind: { models, data: windData },
    },
  } = await request(GRAPHQL_ENDPOINT, windForecastsGQL, {
    pointOfInterestId,
    start,
    end,
    interval: interval * 60,
    windSpeed: translateUnits(units.windSpeed),
  });

  const data = hackAddMissingSDSDataPoints(
    windData.map(({ timestamp, wind: { speed, direction, directionType, gust } }) => ({
      timestamp,
      wind: {
        speed,
        direction,
        directionType,
        gust,
      },
    })),
    start,
    end,
    interval,
  );

  return {
    associated: buildAssociated(models),
    data,
  };
};
