import {
  getRatingForecasts,
  getWaveForecasts,
  getWeatherForecasts,
  getWindForecasts,
} from './forecasts';

export { getRatingForecasts, getWaveForecasts, getWeatherForecasts, getWindForecasts };
