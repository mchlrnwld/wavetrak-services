import { gql } from 'graphql-request';

export const ratingForecastsGQL = gql`
  query KBYGRatingForecast(
    $pointOfInterestId: String!
    $start: Int!
    $end: Int!
    $interval: Int!
    $ratingsAlgorithm: String
    $corrected: Boolean
    $correctedSurf: Boolean
  ) {
    surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
      rating(
        start: $start
        end: $end
        interval: $interval
        ratingsAlgorithm: $ratingsAlgorithm
        corrected: $corrected
        correctedSurf: $correctedSurf
      ) {
        models {
          grid {
            lat
            lon
          }
        }
        data {
          timestamp
          rating {
            key
            value
          }
        }
      }
    }
  }
`;

export const waveForecastsGQL = gql`
  query KBYGWaveForecast(
    $pointOfInterestId: String!
    $start: Int!
    $end: Int!
    $interval: Int!
    $waveHeight: String!
    $swellHeight: String!
    $corrected: Boolean!
  ) {
    surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
      surf(
        start: $start
        end: $end
        interval: $interval
        waveHeight: $waveHeight
        corrected: $corrected
      ) {
        models {
          ...modelGrid
        }
        data {
          timestamp
          surf {
            breakingWaveHeightMin
            breakingWaveHeightMax
          }
        }
      }
      swells(start: $start, end: $end, interval: $interval, waveHeight: $swellHeight) {
        models {
          ...modelGrid
        }
        data {
          timestamp
          swells {
            combined {
              height
              period
              direction
            }
            components {
              event
              height
              period
              direction
              spread
            }
          }
        }
      }
    }
  }

  fragment modelGrid on ForecastModel {
    grid {
      lat
      lon
    }
  }
`;

export const weatherForecastsGQL = gql`
  query KBYGWeatherForecast(
    $pointOfInterestId: String!
    $start: Int!
    $end: Int!
    $interval: Int!
    $temperature: String!
  ) {
    surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
      weather(start: $start, end: $end, interval: $interval, temperature: $temperature) {
        models {
          grid {
            lat
            lon
          }
        }
        data {
          timestamp
          weather {
            temperature
            conditions
          }
        }
      }
    }
  }
`;

export const windForecastsGQL = gql`
  query KBYGWindForecast(
    $pointOfInterestId: String!
    $start: Int!
    $end: Int!
    $interval: Int!
    $windSpeed: String!
  ) {
    surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
      wind(start: $start, end: $end, interval: $interval, windSpeed: $windSpeed) {
        models {
          grid {
            lat
            lon
          }
        }
        data {
          timestamp
          wind {
            speed
            direction
            directionType
            gust
          }
        }
      }
    }
  }
`;
