const translateUnits = (units) => {
  switch (units) {
    case 'FT':
      return 'ft';
    case 'KPH':
      return 'km/h';
    case 'KTS':
      return 'knot';
    case 'M':
      return 'm';
    case 'MPH':
      return 'm/h';
    default:
      return units;
  }
};

export default translateUnits;
