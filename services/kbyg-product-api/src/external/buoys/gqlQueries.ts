import { gql } from 'graphql-request';

export const nearbyBuoysGQL = gql`
  query GetNearbyBuoys(
    $lat: Latitude!
    $lon: Longitude!
    $dist: Float!
    $limit: Int
    $waveHeight: String!
  ) {
    stationsRadius(point: { longitude: $lon, latitude: $lat }, distance: $dist, limit: $limit) {
      station {
        id
        name
        sourceId
        latitude
        longitude
        status
      }
      latestData(waveHeight: $waveHeight) {
        timestamp
        height
        period
        direction
        swells {
          height
          period
          direction
        }
      }
    }
  }
`;

export const buoyDetailsGQL = gql`
  query GetBuoyDetails($id: String!) {
    station(id: $id) {
      station {
        id
        source
        sourceId
        name
        status
        latitude
        longitude
        latestTimestamp
      }
    }
  }
`;

export const buoysInBoundsGQL = gql`
  query GetBuoysInBounds($boundingBox: BoundingBoxInput!, $waveHeight: String!) {
    stationsBoundingBox(boundingBox: $boundingBox) {
      station {
        id
        name
        sourceId
        latitude
        longitude
        status
      }
      latestData(waveHeight: $waveHeight) {
        timestamp
        height
        period
        direction
        swells {
          height
          period
          direction
        }
      }
    }
  }
`;

export const buoyReportGQL = gql`
  query GetBuoyReport(
    $id: String!
    $start: Int
    $end: Int
    $waveHeight: String
    $windSpeed: String
    $waterTemperature: String
    $airTemperature: String
    $dewPoint: String
  ) {
    station(id: $id) {
      station {
        latitude
        longitude
        latestTimestamp
      }
      data(
        start: $start
        end: $end
        waveHeight: $waveHeight
        windSpeed: $windSpeed
        waterTemperature: $waterTemperature
        airTemperature: $airTemperature
        dewPoint: $dewPoint
      ) {
        timestamp
        height
        period
        direction
        waterTemperature
        airTemperature
        dewPoint
        pressure
        wind {
          gust
          speed
          direction
        }
        swells {
          height
          period
          direction
        }
      }
    }
  }
`;
