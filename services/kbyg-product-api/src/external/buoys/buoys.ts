/* istanbul ignore file */

import { request } from 'graphql-request';

import { buoyDetailsGQL, buoyReportGQL, nearbyBuoysGQL, buoysInBoundsGQL } from './gqlQueries';
import config from '../../config';
import translateUnits from '../forecasts/translateUnits';

const GRAPHQL_ENDPOINT = `${config.SCIENCE_DATA_SERVICE}/graphql`;

export const getNearbyBuoys = (lat, lon, dist, limit, waveHeight) =>
  request(GRAPHQL_ENDPOINT, nearbyBuoysGQL, {
    lat,
    lon,
    dist,
    limit,
    waveHeight,
  });

/**
 * @param {string} id
 */
export const getBuoyDetails = (id) => request(GRAPHQL_ENDPOINT, buoyDetailsGQL, { id });

export const getBuoyReport = (id, start, end, units) =>
  request(GRAPHQL_ENDPOINT, buoyReportGQL, {
    id,
    start,
    end,
    waveHeight: units.waveHeight.toLowerCase(),
    windSpeed: translateUnits(units.windSpeed),
    waterTemperature: units.temperature,
    airTemperature: units.temperature,
    dewPoint: units.temperature,
  });

/**
 * @param {object} boundingBox
 * @param {number} boundingBox.north
 * @param {number} boundingBox.south
 * @param {number} boundingBox.east
 * @param {number} boundingBox.west
 * @param {'ft' | 'm'} waveHeight
 */
export const getBuoysInBoundingBox = (boundingBox, waveHeight) =>
  request(GRAPHQL_ENDPOINT, buoysInBoundsGQL, {
    boundingBox,
    waveHeight,
  });
