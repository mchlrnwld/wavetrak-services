import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';

const getUserSettings = async (userId, geoCountryIso?) => {
  const params = { geoCountryIso };
  const url = `${config.USER_API}/user/settings?${createParamString(params)}`;
  const response = await fetch(url, {
    headers: userId
      ? {
          'x-auth-userid': userId,
        }
      : null,
  });

  if (response.status > 500) {
    throw new Error(await response.text());
  }

  const body = await response.json();

  if (response.status === 200) {
    return body;
  }

  throw body;
};

export default getUserSettings;
