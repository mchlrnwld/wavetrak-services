import fetch from 'node-fetch';
import config from '../config';

const getCOTM = async () => {
  const url = `${config.CAMERAS_API}/cameras/cotm`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export default getCOTM;
