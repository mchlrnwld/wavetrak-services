import { APIError, cache } from '@surfline/services-common';
import fetch from 'node-fetch';
import newrelic from 'newrelic';
import _chunk from 'lodash/chunk';
import config from '../config';
import createParamString from './createParamString';

const fetchSpot = async (spotId) => {
  if (!spotId) throw new Error('Valid spotId required');

  const url = `${config.SPOTS_API}/admin/spots/${spotId}`;
  const response = await fetch(url);
  if (response.status === 404) return null;
  if (response.status === 200) return response.json();

  const error = response.text();
  newrelic.noticeError(error, {
    url,
  });
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getSpot = async (spotId) =>
  cache().fetch('getSpot', () => fetchSpot(spotId), {
    params: { spotId },
  });

export const getPopularSpots = async () => {
  const url = `${config.SPOTS_API}/admin/spots/?filter=popular&select=name`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  if (response.status === 404) return null;
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getNearbySpots = async (
  lat,
  lon,
  abilityLevels,
  boardTypes,
  distance,
  limit,
  offset,
) => {
  let queryString = createParamString({
    abilityLevels,
    boardTypes,
    distance,
    limit,
    offset,
  });

  // coordinates passed as two seperate qs values, lon then lat for this endpoint.
  // using select=rank for the most minimal json response to get us the ids we need.
  queryString += `&filter=nearby&select=rank&coordinates=${lon}&coordinates=${lat}`;

  const url = `${config.SPOTS_API}/admin/spots/?${queryString}`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getSubregion = async (subregionId) => {
  const url = `${config.SPOTS_API}/admin/subregions/${subregionId}`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getNearbySubregions = async (regionId) => {
  const url = `${config.SPOTS_API}/admin/regions/${regionId}/subregions`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getRegion = async (regionId) => {
  const url = `${config.SPOTS_API}/admin/regions/${regionId}`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getSpotsBySubregion = async (subregionId) => {
  const url = `${config.SPOTS_API}/admin/subregions/${subregionId}/spots`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getTaxonomy = async (id, type, maxDepth = null) => {
  let url = `${config.SPOTS_API}/taxonomy?id=${id}&type=${type}`;
  if (maxDepth) url += `&maxDepth=${maxDepth}`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  if (response.status === 400) throw new APIError((await response.json()).message);
  throw new Error(`Error making request to ${url}`);
};

export const getNearbyTaxonomy = async (
  type,
  fcl,
  count,
  maxDepth,
  coordinates,
  maxContainsDepth,
) => {
  const lat = coordinates[1];
  const lon = coordinates[0];
  const url = `${config.SPOTS_API}/taxonomy/nearby`;
  const queryString = createParamString({
    type,
    fcl,
    count,
    maxDepth,
    maxContainsDepth,
    lat,
    lon,
  });
  try {
    const response = await fetch(`${url}?${queryString}`);
    const body = await response.json();
    if (response.status === 200) return body;
    throw body;
  } catch (error) {
    newrelic.noticeError(error, {
      devMessage: 'taxonomy API error in getNearbyTaxonomy',
      lat,
      lon,
    });
    throw error;
  }
};

export const getTaxonomyByIds = async (items) => {
  const url = `${config.SPOTS_API}/taxonomy/ids`;
  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(items),
    headers: { 'Content-Type': 'application/json' },
  });

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getSubregionsBySubregionIds = async (subregionIds) => {
  const chunkedSubregionIds = _chunk(subregionIds, 25);

  const chunkedResults = await Promise.all(
    chunkedSubregionIds.map(async (chunk) => {
      const url = `${process.env.SPOTS_API}/subregions/batch?subregionIds=${chunk.join(',')}`;
      const response = await fetch(url);
      if (response.status === 200) return response.json();

      throw new Error(`Error making request to ${url}: ${await response.text()}`);
    }),
  );

  const subregions = chunkedResults.reduce(
    (output, currentValue) => [...output, ...currentValue.subregions],
    [],
  );

  return {
    subregions,
  };
};
