import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';

export const getSpotReport = async (spotId) => {
  const params = { spotId, limit: 1 };
  const url = `${config.REPORTS_API}/admin/reports/spot?${createParamString(params)}`;

  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

export const getRegionalReport = async (subregionId) => {
  const params = { subregionId, limit: 1 };
  const url = `${config.REPORTS_API}/admin/reports/regional?${createParamString(params)}`;
  const response = await fetch(url);

  if (response.status === 200) return response.json();
  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};
