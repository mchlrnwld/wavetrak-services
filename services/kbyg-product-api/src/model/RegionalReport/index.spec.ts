import { expect } from 'chai';
import sinon from 'sinon';
import * as RegionalReport from './index';
import RegionalReportModel from './schema';

describe('RegionalReport model', () => {
  describe('getSubregionsInView', () => {
    let aggregateStub;

    beforeEach(() => {
      aggregateStub = sinon.stub(RegionalReportModel, 'aggregate');
    });

    afterEach(() => {
      aggregateStub.restore();
    });

    it('should return subRegionsInView for each spotReportView', async () => {
      const spotReportViews = [
        {
          _id: '5842041f4e65fad6a7708927',
          lat: 33.70877301002487,
          lon: -118.2840112858389,
          name: 'Cabrillo Beach',
          subregionId: '58581a836630e24c4487900b',
        },
      ];
      const subregionsInView = [
        {
          _id: '58581a836630e24c44878fd6',
          subregion: {
            name: 'North Orange County',
            id: '58581a836630e24c44878fd6',
            forecasterEmail: '',
          },
        },
      ];

      aggregateStub.returns({
        allowDiskUse: sinon.stub().resolves(subregionsInView),
      });
      const result = await RegionalReport.default(spotReportViews);
      expect(result).to.deep.equal(subregionsInView);
    });
  });
});
