import { Types } from 'mongoose';
import subregionModel from './schema';

const getSubregionsInSpotReportViews = async (spotReportViews) => {
  const subregionIdSet = new Set();

  const subregionIds = spotReportViews
    .filter(
      ({ subregionId }) =>
        !subregionIdSet.has(subregionId.toString()) && subregionIdSet.add(subregionId.toString()),
    )
    .map(({ subregionId }) => new Types.ObjectId(subregionId));

  const subRegionsInView = await subregionModel
    .aggregate([
      { $match: { _id: { $in: subregionIds } } },
      {
        $project: {
          subregion: {
            id: '$_id',
            name: '$name',
            // Email is deprecated. Here for legacy props only
            forecasterEmail: '',
          },
        },
      },
    ])
    .allowDiskUse(true);

  return subRegionsInView;
};

export default getSubregionsInSpotReportViews;
