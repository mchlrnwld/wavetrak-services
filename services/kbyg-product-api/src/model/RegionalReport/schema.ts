// TODO: Remove this dependency and make all Spots requests through spots API.
import { model, Schema, Types } from 'mongoose';

interface Subregion {
  name: string;
  spots: {
    spot: Types.ObjectId;
  }[];
}

const subregionSchema = new Schema<Subregion>(
  {
    name: { type: String, required: [true, 'Name is required'] },
    spots: [
      {
        spot: {
          type: Schema.Types.ObjectId,
          ref: 'Spot',
        },
      },
    ],
  },
  { collection: 'Subregions' },
);

export default model<Subregion>('Subregions', subregionSchema);
