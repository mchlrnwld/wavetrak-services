export const surfRatings = {
  NONE: 'None',
  EPIC: 'epic',
  GOOD_TO_EPIC: 'good to epic',
  VERY_GOOD: 'very good',
  GOOD: 'good',
  FAIR_TO_GOOD: 'fair to good',
  FAIR: 'fair',
  POOR_TO_FAIR: 'poor to fair',
  POOR: 'poor',
  VERY_POOR: 'very poor',
  FLAT: 'flat',
};

// export default SurfHeightsMap;
