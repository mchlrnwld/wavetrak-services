import mongoose from 'mongoose';
import config from '../config';

mongoose.Promise = global.Promise;

// eslint-disable-next-line
export const initMongoDB = (log) => {
  const connectionString = config.MONGO_CONNECTION_STRING_KBYG;
  const mongoDbConfig = {
    maxPoolSize: 50,
  };
  return new Promise((resolve, reject) => {
    try {
      log.debug(`Mongo ConnectionString: ${connectionString} `);

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        log.info(`MongoDB connected on ${connectionString}`);
        resolve(null);
      });
      mongoose.connection.on('error', (error) => {
        log.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
};
