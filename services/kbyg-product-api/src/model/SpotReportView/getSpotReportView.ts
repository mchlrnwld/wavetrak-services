import convertSpotUnits from './convertSpotUnits';
import SpotReportViewModel from './SpotReportView';
import transformSpot from './transformSpot';

const convertUnits = (spotReportView, units) => convertSpotUnits(units)(spotReportView);

const getSpotReportView = async (spotId, units) => {
  const spotReportView = await SpotReportViewModel.findById(spotId).lean();
  return convertUnits(transformSpot(spotReportView), units);
};

export default getSpotReportView;
