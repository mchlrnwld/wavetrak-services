import { expect } from 'chai';
import sinon from 'sinon';
import SpotReportViewModel from './SpotReportView';
import getNearestSpotReportView from './getNearestSpotReportView';

describe('SpotReportView Model', () => {
  describe('getNearestSpotReportView', () => {
    let findOneStub;

    beforeEach(() => {
      findOneStub = sinon.stub(SpotReportViewModel, 'findOne');
    });

    afterEach(() => {
      findOneStub.restore();
    });

    it('should return the nearest SpotReportView to a point', async () => {
      const spotReportView = {
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        legacyRegionId: 2081,
      };
      const leanStub = sinon.stub().resolves(spotReportView);
      findOneStub.returns({
        lean: leanStub,
      });
      const nearestSpotReportView = await getNearestSpotReportView({
        lon: -118.116072,
        lat: 33.7361865,
      });
      expect(nearestSpotReportView).to.deep.equal(spotReportView);
      expect(findOneStub).to.have.been.calledOnce();
      expect(findOneStub.firstCall.args).to.deep.equal([
        {
          location: {
            $near: {
              $geometry: {
                type: 'Point',
                coordinates: [-118.116072, 33.7361865],
              },
            },
          },
        },
      ]);
      expect(leanStub).to.have.been.calledOnce();
    });
  });
});
