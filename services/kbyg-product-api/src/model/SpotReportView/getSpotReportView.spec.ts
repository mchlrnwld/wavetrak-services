import { expect } from 'chai';
import sinon from 'sinon';
import spotReportViewFixture from './fixtures/spotReportView.json';
import LOLASpotReportViewFixture from './fixtures/LOLASpotReportView.json';
import SpotReportViewModel from './SpotReportView';
import getSpotReportView from './getSpotReportView';
import weatherCondition from '../../utils/weatherCondition';
import rewindClip from '../../utils/rewindClip';

describe('SpotReportView Model', () => {
  describe('getSpotReportView', () => {
    let clock;
    let findByIdStub;

    before(() => {
      clock = sinon.useFakeTimers(1493730000 * 1000);
    });

    beforeEach(() => {
      findByIdStub = sinon.stub(SpotReportViewModel, 'findById');
    });

    afterEach(() => {
      findByIdStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('should find SpotReportView by spot ID', async () => {
      const units = {
        windSpeed: 'KTS',
        waveHeight: 'FT',
        tideHeight: 'FT',
        temperature: 'F',
      };
      findByIdStub.returns({
        lean: sinon.stub().resolves(spotReportViewFixture),
      });
      const spotId = '5842041f4e65fad6a77088ed';
      const spotReportView = await getSpotReportView(spotId, units);
      expect(spotReportView).to.deep.equal({
        _id: '5842041f4e65fad6a77088ed',
        __v: undefined,
        legacyRegionId: 2143,
        location: undefined,
        lat: 33.654213041213,
        lon: -118.0032588192,
        name: 'HB Pier, Southside',
        timezone: 'America/Los_Angeles',
        slug: 'DEPRECATED',
        createdAt: undefined,
        updatedAt: undefined,
        cameras: [
          {
            title: 'HB Pier, Southside',
            streamUrl:
              'http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-hbpiersscam.smil/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg',
            control: 'http://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg',
            rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/hbpiersscam/hbpiersscam',
            alias: 'wc-hbpierss',
            pixelatedStillUrl: undefined,
            _id: '58a1075574fbf700121e9fc1',
            status: {
              isDown: false,
              message: '',
              subMessage: '',
              altMessage: '',
            },
            supportsHighlights: false,
            supportsCrowds: false,
            nighttime: false,
            rewindClip: rewindClip(
              'https://camrewinds.cdn-surfline.com/hbpiersscam/hbpiersscam',
              'America/Los_Angeles',
              10,
            ),
          },
        ],
        conditions: {
          human: true,
          value: 'POOR',
        },
        wind: {
          speed: 2,
          direction: 249.88,
        },
        waveHeight: {
          human: true,
          min: 3,
          max: 4,
          occasional: null,
          plus: true,
          humanRelation: null,
        },
        swells: [
          {
            height: 1.3,
            direction: 265.78,
            directionMin: 264,
            period: 11,
            _id: '5a65c633908c020001169dcb',
          },
          {
            height: 0.5,
            direction: 268.59,
            directionMin: 267,
            period: 8,
            _id: '5a65c633908c020001169dca',
          },
          {
            height: 0,
            direction: 0,
            directionMin: 0,
            period: 0,
            _id: '5a65c633908c020001169dc9',
          },
          {
            height: 0,
            direction: 0,
            directionMin: 0,
            period: 0,
            _id: '5a65c633908c020001169dc8',
          },
          {
            height: 0.3,
            direction: 188.44,
            directionMin: 187,
            period: 14,
            _id: '5a65c633908c020001169dc7',
          },
          {
            height: 0,
            direction: 0,
            directionMin: 0,
            period: 0,
            _id: '5a65c633908c020001169dc6',
          },
        ],
        tide: {
          previous: {
            type: 'LOW',
            height: -0.6,
            timestamp: 1486945280,
            utcOffset: -8,
          },
          current: {
            type: 'NORMAL',
            height: 1.6,
            timestamp: 1486955280,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 4.6,
            timestamp: 1486968170,
            utcOffset: -8,
          },
        },
        waterTemp: {
          min: 60,
          max: 65,
        },
        weather: {
          temperature: 74,
          condition: weatherCondition('CLEAR_NO_RAIN'),
        },
        legacyId: 4874,
        rank: 10,
        subregionId: '58581a836630e24c44878fd6',
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
        abilityLevels: ['BEGINNER'],
        boardTypes: ['SHORTBOARD'],
        relivableRating: 2,
      });
      expect(SpotReportViewModel.findById).to.have.been.calledOnce();
      expect(SpotReportViewModel.findById).to.have.been.calledWithExactly(spotId);
    });

    it('should map waveHeight and humanRelation if no human forecast', async () => {
      const units = {
        windSpeed: 'KTS',
        waveHeight: 'FT',
        tideHeight: 'FT',
        temperature: 'F',
      };
      findByIdStub.returns({
        lean: sinon.stub().resolves(LOLASpotReportViewFixture),
      });
      const spotId = '5842041f4e65fad6a77088ed';
      const spotReportView = await getSpotReportView(spotId, units);
      expect(spotReportView.waveHeight).to.deep.equal({
        human: false,
        min: 7,
        max: 10,
        occasional: null,
        plus: false,
        humanRelation: '2x overhead',
      });
      expect(SpotReportViewModel.findById).to.have.been.calledOnce();
      expect(SpotReportViewModel.findById).to.have.been.calledWithExactly(spotId);
    });
  });
});
