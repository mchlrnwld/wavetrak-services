import newrelic from 'newrelic';
import convertSpotUnits from './convertSpotUnits';
import transformSpot from './transformSpot';
import SpotReportViewModel from './SpotReportView';

const getSpotReportViewsIn = async (spotIds, units, sort = {}, select = null) => {
  const $and = [{ _id: { $in: spotIds } }];

  // allow select to be passed, but enforce certain fields to return as well.
  const selectArray = select ? select.split(',').concat(['_id', 'name', 'rank']) : [];

  const spotReportViews = await SpotReportViewModel.find({ $and }).sort(sort).lean();

  // Since we transform and convert, map/reduce select fields after conversions.
  const spotReportsTransformed = spotReportViews
    .map((report) => {
      try {
        return transformSpot(report);
      } catch (err) {
        newrelic.noticeError(err);
        return null;
      }
    })
    .filter((report) => !!report);
  const spotReportsConverted = spotReportsTransformed.map(convertSpotUnits(units));
  let result = [];
  if (selectArray.length) {
    // TODO: Replace reduce function with lodash/pick
    result = spotReportsConverted.map((spotReport) =>
      Object.keys(spotReport).reduce(
        (acc, key) => (selectArray.indexOf(key) === -1 ? acc : { ...acc, [key]: spotReport[key] }),
        {},
      ),
    );
  } else {
    result = spotReportsConverted;
  }

  return result;
};

export default getSpotReportViewsIn;
