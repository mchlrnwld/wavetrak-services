import convertSpotUnits from './convertSpotUnits';
import SpotReportViewModel, { SpotReportView } from './SpotReportView';
import transformSpot from './transformSpot';

const getSpotReportViews = async (
  { north, south, east, west },
  units,
  filterCamsOnly = false,
): Promise<SpotReportView[]> => {
  const query = {
    location: {
      $geoWithin: {
        $box: [
          [west, south],
          [east, north],
        ],
      },
    },
  };

  if (filterCamsOnly) query['cameras.0'] = { $exists: true };

  const spotReportViews = await SpotReportViewModel.find(query).lean();
  const result = spotReportViews.map(transformSpot);
  return result.map(convertSpotUnits(units));
};

export default getSpotReportViews;
