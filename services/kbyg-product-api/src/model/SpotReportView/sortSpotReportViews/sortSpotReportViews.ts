import _ from 'lodash/fp';
import sortSubregions from './sortSubregions';
import { SpotReportView } from '../SpotReportView';

const groupBySubregion = _.groupBy((spot: SpotReportView) => spot.subregionId);
const sortByRank = _.sortBy((spot: SpotReportView) => spot.rank);

const rankSubregionSpots = (subregionRank, spots) =>
  spots.map((spot, spotRank) => ({
    ...spot,
    rank: [subregionRank, spotRank],
  }));

/**
 * Sorts SpotReportViews by subregion order and then by spot rank.
 * @param  spotReportViews - Array of SpotReportView instances
 * @return - Sorted array of SpotReportView instances
 *                                              with rank property attached
 */
const sortSpotReportViews = (spotReportViews: SpotReportView[]): SpotReportView[] => {
  if (spotReportViews.length === 0) return spotReportViews;

  const sortedSpotsBySubregion = _.mapValues(sortByRank)(groupBySubregion(spotReportViews));
  const sortedSubregions = sortSubregions(sortedSpotsBySubregion);

  const sortedSpots = _.flatten(
    sortedSubregions.map((subregion, subregionRank) =>
      rankSubregionSpots(subregionRank, sortedSpotsBySubregion[subregion]),
    ),
  );

  return sortedSpots;
};

export default sortSpotReportViews;
