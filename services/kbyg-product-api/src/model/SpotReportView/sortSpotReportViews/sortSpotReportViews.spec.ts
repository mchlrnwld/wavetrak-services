import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import sortSpotReportViews from './sortSpotReportViews';

describe('SpotReportView Model', () => {
  describe('sortSpotReportViews', () => {
    it('should sort spots by subregion rank and then by spot rank', () => {
      const spotReportViews = deepFreeze([
        {
          name: 'Seal Beach',
          lat: 33.737902112055,
          lon: -118.1086921691,
          subregionId: '58581a836630e24c44878fd6',
          rank: 0,
        },
        {
          name: 'Trails',
          lat: 33.34509459091381,
          lon: -117.5226651633729,
          subregionId: '58581a836630e24c4487900a',
          rank: 16,
        },
        {
          name: 'Cabrillo Beach',
          lat: 33.70877301002487,
          lon: -118.2840112858389,
          subregionId: '58581a836630e24c4487900b',
          rank: 12,
        },
        {
          name: 'Rockpile',
          lat: 33.54268310353641,
          lon: -117.7915988640743,
          subregionId: '58581a836630e24c4487900a',
          rank: 0,
        },
        {
          name: 'Santa Monica Pier',
          lat: 34.00750300824362,
          lon: -118.4971783496269,
          subregionId: '58581a836630e24c4487900b',
          rank: 0,
        },
        {
          name: 'Crystal Cove',
          lat: 33.56489,
          lon: -117.83585,
          subregionId: '58581a836630e24c44878fd6',
          rank: 24,
        },
      ]);
      expect(sortSpotReportViews(spotReportViews)).to.deep.equal([
        {
          name: 'Santa Monica Pier',
          lat: 34.00750300824362,
          lon: -118.4971783496269,
          subregionId: '58581a836630e24c4487900b',
          rank: [0, 0],
        },
        {
          name: 'Cabrillo Beach',
          lat: 33.70877301002487,
          lon: -118.2840112858389,
          subregionId: '58581a836630e24c4487900b',
          rank: [0, 1],
        },
        {
          name: 'Seal Beach',
          lat: 33.737902112055,
          lon: -118.1086921691,
          subregionId: '58581a836630e24c44878fd6',
          rank: [1, 0],
        },
        {
          name: 'Crystal Cove',
          lat: 33.56489,
          lon: -117.83585,
          subregionId: '58581a836630e24c44878fd6',
          rank: [1, 1],
        },
        {
          name: 'Rockpile',
          lat: 33.54268310353641,
          lon: -117.7915988640743,
          subregionId: '58581a836630e24c4487900a',
          rank: [2, 0],
        },
        {
          name: 'Trails',
          lat: 33.34509459091381,
          lon: -117.5226651633729,
          subregionId: '58581a836630e24c4487900a',
          rank: [2, 1],
        },
      ]);
    });

    it('should return if no spots in array', () => {
      expect(sortSpotReportViews([])).to.deep.equal([]);
    });

    it('should sort spots by rank if no subregionId present', () => {
      const spotReportViews = deepFreeze([
        {
          name: 'Random Spot',
          lat: 33.56489,
          lon: -117.83585,
          rank: 3,
        },
        {
          name: 'Another Random Spot',
          lat: 33.54268310353641,
          lon: -117.7915988640743,
          rank: 1,
        },
        {
          name: 'Last Random Spot',
          lat: 33.34509459091381,
          lon: -117.5226651633729,
          rank: 2,
        },
      ]);
      expect(sortSpotReportViews(spotReportViews)).to.deep.equal([
        {
          name: 'Another Random Spot',
          lat: 33.54268310353641,
          lon: -117.7915988640743,
          rank: [0, 0],
        },
        {
          name: 'Last Random Spot',
          lat: 33.34509459091381,
          lon: -117.5226651633729,
          rank: [0, 1],
        },
        {
          name: 'Random Spot',
          lat: 33.56489,
          lon: -117.83585,
          rank: [0, 2],
        },
      ]);
    });
  });
});
