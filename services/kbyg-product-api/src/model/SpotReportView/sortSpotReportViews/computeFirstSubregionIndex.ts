import computeSortDirectionStart from './computeSortDirectionStart';

/**
 * Object with subregion ID, start position, and end position.
 * @typedef  {Object} SubregionVector
 * @property {string} subregion      - Subregion ID from MongoDB
 * @property {LatLonSpherical} start - Vector start position lat/lon
 * @property {LatLonSpherical} end   - Vector end position lat/lon
 */

/**
 * Finds the index of the first subregion to begin sorting from.
 * @param  {SubregionVector[]} subregionVectors - Subregion vector defined by start and
 *                                                end LatLonSpherical properties
 * @return {number}                             - Index of first subregion to begin sorting from
 */
const computeFirstSubregionIndex = (subregionVectors) => {
  const starts = subregionVectors.map((vector) => vector.start);
  const ends = subregionVectors.map((vector) => vector.end);

  const north = Math.max(...starts.map(({ lat }) => lat));
  const south = Math.min(...ends.map(({ lat }) => lat));
  const east = Math.max(...ends.map(({ lon }) => lon));
  const west = Math.min(...starts.map(({ lon }) => lon));

  const subregionIndex = {
    north: subregionVectors.findIndex(({ start }) => start.lat === north),
    south: subregionVectors.findIndex(({ end }) => end.lat === south),
    east: subregionVectors.findIndex(({ end }) => end.lon === east),
    west: subregionVectors.findIndex(({ start }) => start.lon === west),
  };

  const northSubregion = subregionVectors[subregionIndex.north];
  const southSubregion = subregionVectors[subregionIndex.south];
  const eastSubregion = subregionVectors[subregionIndex.east];
  const westSubregion = subregionVectors[subregionIndex.west];

  const sortDirectionStart = computeSortDirectionStart({
    north: northSubregion.start,
    south: southSubregion.end,
    east: eastSubregion.end,
    west: westSubregion.start,
  });

  return subregionIndex[sortDirectionStart];
};

export default computeFirstSubregionIndex;
