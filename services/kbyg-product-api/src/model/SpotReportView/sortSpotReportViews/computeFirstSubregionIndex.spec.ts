import { expect } from 'chai';
import sinon from 'sinon';
import { LatLonSpherical } from 'geodesy';
import * as computeSortDirectionStart from './computeSortDirectionStart';
import computeFirstSubregionIndex from './computeFirstSubregionIndex';

describe('SpotReportView Model', () => {
  describe('sortSpotReportViews', () => {
    describe('computeFirstSubregionIndex', () => {
      let computeSortDirectionStartStub;

      beforeEach(() => {
        computeSortDirectionStartStub = sinon.stub(computeSortDirectionStart, 'default');
      });

      afterEach(() => {
        computeSortDirectionStartStub.restore();
      });

      it('should compute sort direction starting index for array of subregion vectors', () => {
        const subregionVectors = [
          {
            start: new LatLonSpherical(-1, 1),
            end: new LatLonSpherical(-2, 0),
          },
          {
            start: new LatLonSpherical(1, 1),
            end: new LatLonSpherical(0, 0),
          },
          {
            start: new LatLonSpherical(-1, -1),
            end: new LatLonSpherical(2, 0),
          },
          {
            start: new LatLonSpherical(1, -1),
            end: new LatLonSpherical(2, 2),
          },
        ];

        computeSortDirectionStartStub.returns('north');
        expect(computeFirstSubregionIndex(subregionVectors)).to.equal(1);

        computeSortDirectionStartStub.returns('west');
        expect(computeFirstSubregionIndex(subregionVectors)).to.equal(2);

        expect(computeSortDirectionStartStub).to.have.been.calledTwice();
        expect(computeSortDirectionStartStub).to.have.been.calledWithExactly({
          north: subregionVectors[1].start,
          south: subregionVectors[0].end,
          east: subregionVectors[3].end,
          west: subregionVectors[2].start,
        });
      });
    });
  });
});
