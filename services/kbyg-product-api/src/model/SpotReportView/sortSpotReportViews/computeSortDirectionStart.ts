/**
 * Determines where to begin sorting. Either from the north
 * or the west depending on the general direction.
 * @param  {LatLonSpherical} north - North-most position
 * @param  {LatLonSpherical} south - South-most position
 * @param  {LatLonSpherical} east  - East-most position
 * @param  {LatLonSpherical} west  - West-most position
 * @return {string}                - Direction to begin sorting from
 */
const computeSortDirectionStart = ({ north, south, east, west }) => {
  if (north === west) return 'north';

  const northSouthDistance = north.distanceTo(south);
  const westEastDistance = west.distanceTo(east);

  if (northSouthDistance > westEastDistance) {
    return 'north';
  }

  if (northSouthDistance < westEastDistance) {
    return 'west';
  }

  const bearing = west.bearingTo(north);
  if (bearing > 45) {
    return 'west';
  }

  return 'north';
};

export default computeSortDirectionStart;
