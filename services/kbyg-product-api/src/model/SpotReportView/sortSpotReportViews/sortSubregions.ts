import _ from 'lodash/fp';
import { LatLonSpherical } from 'geodesy';
import computeFirstSubregionIndex from './computeFirstSubregionIndex';
import { SpotReportView } from '../SpotReportView';

/**
 * Sorts subregions based on the position of their start and end spots.
 * @param  sortedSpotsBySubregion - Object with subregion keys and sorted spot array values
 * @return - Array of subregion IDs
 */
const sortSubregions = (sortedSpotsBySubregion: { [key: string]: SpotReportView[] }): string[] => {
  const subregionVectors = _.keys(sortedSpotsBySubregion).map((subregion) => {
    const spots = sortedSpotsBySubregion[subregion];
    const start = _.head(spots);
    const end = _.last(spots);
    return {
      subregion,
      start: new LatLonSpherical(start.lat, start.lon),
      end: new LatLonSpherical(end.lat, end.lon),
    };
  });

  const firstSubregionIndex = computeFirstSubregionIndex(subregionVectors);
  const sortedSubregions = subregionVectors.splice(firstSubregionIndex, 1);

  while (subregionVectors.length > 0) {
    const previousSubregion = _.last(sortedSubregions);

    const distanceToOtherSubregions = subregionVectors.map((subregion) =>
      previousSubregion.end.distanceTo(subregion.start),
    );
    const minDistance = Math.min(...distanceToOtherSubregions);

    const nextSubregionIndex = distanceToOtherSubregions.findIndex(
      (distance) => distance === minDistance,
    );
    const nextSubregion = _.head(subregionVectors.splice(nextSubregionIndex, 1));

    sortedSubregions.push(nextSubregion);
  }

  return sortedSubregions.map(({ subregion }) => subregion);
};

export default sortSubregions;
