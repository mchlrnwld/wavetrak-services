import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import sortSubregions from './sortSubregions';

describe('SpotReportView Model', () => {
  describe('sortSpotReportViews', () => {
    describe('sortSubregions', () => {
      it('should sort subregions by relative distance from one to the next', () => {
        const sortedSpotsBySubregion = deepFreeze({
          '58581a836630e24c4487900a': [
            // South Orange County
            {
              name: 'Rockpile',
              lat: 33.54268310353641,
              lon: -117.7915988640743,
            },
            {
              name: 'Trails',
              lat: 33.34509459091381,
              lon: -117.5226651633729,
            },
          ],
          '58581a836630e24c4487900b': [
            // South Los Angeles
            {
              name: 'Santa Monica Pier',
              lat: 34.00750300824362,
              lon: -118.4971783496269,
            },
            {
              name: 'Cabrillo Beach',
              lat: 33.70877301002487,
              lon: -118.2840112858389,
            },
          ],
          '58581a836630e24c44878fd6': [
            // North Orange County
            {
              name: 'Seal Beach',
              lat: 33.737902112055,
              lon: -118.1086921691,
            },
            {
              name: 'Crystal Cove',
              lat: 33.56489,
              lon: -117.83585,
            },
          ],
        });
        expect(sortSubregions(sortedSpotsBySubregion)).to.deep.equal([
          '58581a836630e24c4487900b', // South Los Angeles
          '58581a836630e24c44878fd6', // North Orange County
          '58581a836630e24c4487900a', // South Orange County
        ]);
      });
    });
  });
});
