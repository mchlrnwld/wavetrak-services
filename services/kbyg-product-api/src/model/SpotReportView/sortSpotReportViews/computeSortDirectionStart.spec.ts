import { expect } from 'chai';
import sinon from 'sinon';
import computeSortDirectionStart from './computeSortDirectionStart';

describe('SpotReportView Model', () => {
  describe('sortSpotReportViews', () => {
    describe('computeSortDirectionStart', () => {
      it('should return north if north and west positions are the same', () => {
        const north = {};
        const west = north;
        expect(computeSortDirectionStart({ north, west, east: {}, south: {} })).to.equal('north');
      });

      it('should return north if north-south distance greater than west-east distance', () => {
        const south = {};
        const east = {};
        const north = {
          distanceTo: sinon.stub().withArgs(south).returns(100),
        };
        const west = {
          distanceTo: sinon.stub().withArgs(east).returns(99),
        };
        expect(computeSortDirectionStart({ north, south, east, west })).to.equal('north');
        expect(north.distanceTo).to.have.been.calledOnce();
        expect(north.distanceTo).to.have.been.calledWithExactly(south);
        expect(west.distanceTo).to.have.been.calledOnce();
        expect(west.distanceTo).to.have.been.calledWithExactly(east);
      });

      it('should return west if north-south distance less than west-east distance', () => {
        const south = {};
        const east = {};
        const north = {
          distanceTo: sinon.stub().withArgs(south).returns(99),
        };
        const west = {
          distanceTo: sinon.stub().withArgs(east).returns(100),
        };
        expect(computeSortDirectionStart({ north, south, east, west })).to.equal('west');
        expect(north.distanceTo).to.have.been.calledOnce();
        expect(north.distanceTo).to.have.been.calledWithExactly(south);
        expect(west.distanceTo).to.have.been.calledOnce();
        expect(west.distanceTo).to.have.been.calledWithExactly(east);
      });

      it('should return west if north-south and west-east distances equal and west-to-north bearing greater than 45', () => {
        const south = {};
        const east = {};
        const north = {
          distanceTo: sinon.stub().withArgs(south).returns(100),
        };
        const west = {
          distanceTo: sinon.stub().withArgs(east).returns(100),
          bearingTo: sinon.stub().withArgs(north).returns(46),
        };
        expect(computeSortDirectionStart({ north, south, east, west })).to.equal('west');
        expect(north.distanceTo).to.have.been.calledOnce();
        expect(north.distanceTo).to.have.been.calledWithExactly(south);
        expect(west.distanceTo).to.have.been.calledOnce();
        expect(west.distanceTo).to.have.been.calledWithExactly(east);
        expect(west.bearingTo).to.have.been.calledOnce();
        expect(west.bearingTo).to.have.been.calledWithExactly(north);
      });

      it('should return north if north-south and west-east distances equal and west-to-north bearing less than or equal to 45', () => {
        const south = {};
        const east = {};
        const north = {
          distanceTo: sinon.stub().withArgs(south).returns(100),
        };
        const west = {
          distanceTo: sinon.stub().withArgs(east).returns(100),
          bearingTo: sinon.stub().withArgs(north).returns(45),
        };
        expect(computeSortDirectionStart({ north, south, east, west })).to.equal('north');
        expect(north.distanceTo).to.have.been.calledOnce();
        expect(north.distanceTo).to.have.been.calledWithExactly(south);
        expect(west.distanceTo).to.have.been.calledOnce();
        expect(west.distanceTo).to.have.been.calledWithExactly(east);
        expect(west.bearingTo).to.have.been.calledOnce();
        expect(west.bearingTo).to.have.been.calledWithExactly(north);
      });
    });
  });
});
