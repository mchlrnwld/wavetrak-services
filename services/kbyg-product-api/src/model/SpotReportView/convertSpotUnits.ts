import { curry } from 'lodash';
import {
  convertWindSpeedUnits,
  convertSwellHeightUnits,
  convertTideHeightUnitsOld,
  convertTemperatureUnits,
  convertStringByUnits,
} from '../../utils/convertUnits';
import convertWaveHeightsUnits from '../../utils/convertWaveHeightsUnits';

const baseConvertSpotUnits = curry(
  (
    convertWindSpeed,
    convertWaveHeights,
    convertSwellHeight,
    convertTideHeight,
    convertTemperature,
    convertString,
    spot,
  ) => {
    const { minHeight, maxHeight } = convertWaveHeights(
      spot.waveHeight.min,
      spot.waveHeight.max,
      true,
    );
    return {
      ...spot,
      wind: {
        ...spot.wind,
        speed: convertWindSpeed(spot.wind.speed),
      },
      waveHeight: {
        ...spot.waveHeight,
        min: minHeight,
        max: maxHeight,
        occasional: null,
        humanRelation: spot.waveHeight.humanRelation
          ? convertString(spot.waveHeight.humanRelation)
          : null,
        plus: spot.waveHeight.plus,
      },
      swells: spot.swells.map((swell) => ({
        ...swell,
        height: convertSwellHeight(swell.height),
      })),
      tide: spot.tide
        ? {
            ...spot.tide,
            previous: {
              ...spot.tide.previous,
              height: convertTideHeight(spot.tide.previous.height),
            },
            current: {
              ...spot.tide.current,
              height: convertTideHeight(spot.tide.current.height),
            },
            next: {
              ...spot.tide.next,
              height: convertTideHeight(spot.tide.next.height),
            },
          }
        : null,
      waterTemp: {
        ...spot.waterTemp,
        min: convertTemperature(spot.waterTemp.min),
        max: convertTemperature(spot.waterTemp.max),
      },
      weather: {
        ...spot.weather,
        temperature: convertTemperature(spot.weather.temperature),
      },
    };
  },
);

const convertSpotUnits = (units) =>
  baseConvertSpotUnits(
    convertWindSpeedUnits(units.windSpeed),
    convertWaveHeightsUnits(units.waveHeight),
    convertSwellHeightUnits(units.swellHeight),
    convertTideHeightUnitsOld(units.tideHeight),
    convertTemperatureUnits(units.temperature),
    convertStringByUnits(units.waveHeight),
  );

export default convertSpotUnits;
