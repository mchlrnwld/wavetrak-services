import { Types } from 'mongoose';
import SpotReportViewModel, { Spots } from './SpotReportView';
import convertSpotUnits from './convertSpotUnits';
import transformSpot from './transformSpot';

const getFilteredSpotIds = async (spotIds, abilityLevels) => {
  if (!abilityLevels) return spotIds;
  const $and: object[] = [{ _id: { $in: spotIds } }];
  $and.push({ abilityLevels: { $in: abilityLevels.split(',') } });

  const filterSpots = await Spots.find({ $and }).lean();

  const filteredSpots = filterSpots.map((filtered) => filtered._id.toString());
  return filteredSpots;
};

const getBatchSpotReportViewsByParentTaxonomy = async (
  taxonomyId,
  units,
  sort = {},
  abilityLevels = null,
  select = null,
) => {
  const $and = [{ parentTaxonomy: new Types.ObjectId(taxonomyId) }];

  // allow select to be passed, but enforce certain fields to return as well.
  const selectArray = select ? select.concat(['_id', 'name', 'rank']) : [];

  const spotReportViewsByParentTaxonomy = await SpotReportViewModel.find({ $and })
    .sort(sort)
    .lean();

  const spotIds = spotReportViewsByParentTaxonomy.length
    ? spotReportViewsByParentTaxonomy.map((spotReport) => spotReport._id.toString())
    : [];

  // find spot ids for abilityLevel filters, if passed
  const filteredSpotIds = await getFilteredSpotIds(spotIds, abilityLevels);

  const spotReportViews =
    spotIds.length !== filteredSpotIds.length
      ? spotReportViewsByParentTaxonomy.filter(
          (spotReportView) => filteredSpotIds.indexOf(spotReportView._id.toString()) > -1,
        )
      : spotReportViewsByParentTaxonomy;

  // Since we transform and convert, map/reduce select fields after conversions.
  const spotReportsTransformed = spotReportViews.map(transformSpot);
  const spotReportsConverted = spotReportsTransformed.map(convertSpotUnits(units));
  let result = [];
  if (selectArray.length) {
    // TODO: Replace reduce function with lodash/pick
    result = spotReportsConverted.map((spotReport) =>
      Object.keys(spotReport).reduce(
        (acc, key) => (selectArray.indexOf(key) === -1 ? acc : { ...acc, [key]: spotReport[key] }),
        {},
      ),
    );
  } else {
    result = spotReportsConverted;
  }

  return { _id: taxonomyId, spots: result };
};

export default getBatchSpotReportViewsByParentTaxonomy;
