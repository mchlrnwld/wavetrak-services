import { expect } from 'chai';
import sinon from 'sinon';
import cameraFixture from './fixtures/cameras.json';
import SpotReportViewModel from './SpotReportView';
import getSpotReportViewByCamId from './getSpotReportViewByCamId';
import weatherCondition from '../../utils/weatherCondition';
import rewindClip from '../../utils/rewindClip';

describe('SpotReportView Model', () => {
  describe('getSpotReportViewByCamId', () => {
    let clock;
    let findOneStub;

    before(() => {
      clock = sinon.useFakeTimers(1493730000 * 1000);
    });

    beforeEach(() => {
      findOneStub = sinon.stub(SpotReportViewModel, 'findOne');
    });

    afterEach(() => {
      findOneStub.restore();
    });

    after(() => {
      clock.restore();
    });

    it('should findOne SpotReportView from a cameraId', async () => {
      const spotReportView = cameraFixture;

      findOneStub.returns({
        lean: sinon.stub().resolves(spotReportView),
      });

      const units = {
        windSpeed: 'KTS',
        waveHeight: 'FT',
        tideHeight: 'FT',
        temperature: 'F',
      };

      const cotmReportView = await getSpotReportViewByCamId('592f17d3ebb85d0012ddcb2a', units);
      expect(cotmReportView).to.deep.equal({
        __v: undefined,
        _id: '5842041f4e65fad6a77088ed',
        name: 'HB Pier, Southside',
        timezone: 'America/Los_Angeles',
        slug: 'DEPRECATED',
        cameras: [
          {
            _id: '592f17d3ebb85d0012ddcb2a',
            title: 'HB Pier, Southside',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253362/05eabbd7/playlist.m3u8',
            stillUrl: 'https://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg',
            control: 'https://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg',
            rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/hbpiersscam/hbpiersscam',
            alias: 'wc-hbpierss',
            pixelatedStillUrl: undefined,
            status: {
              isDown: false,
              message: '',
              subMessage: '',
              altMessage: '',
            },
            nighttime: false,
            rewindClip: rewindClip(
              'https://camrewinds.cdn-surfline.com/hbpiersscam/hbpiersscam',
              'America/Los_Angeles',
              10,
            ),
          },
        ],
        createdAt: undefined,
        updatedAt: undefined,
        lat: 33.654213041213,
        lon: -118.0032588192,
        location: undefined,
        waveHeight: {
          human: true,
          min: 2,
          max: 3,
          occasional: null,
          plus: true,
          humanRelation: 'shoulder high to 1 ft overhead',
        },
        swells: [
          {
            height: 1.3,
            direction: 265.78,
            directionMin: 264,
            period: 11,
            _id: '5a65c633908c020001169dcb',
          },
          {
            height: 0.5,
            direction: 268.59,
            directionMin: 267,
            period: 8,
            _id: '5a65c633908c020001169dca',
          },
          {
            height: 0,
            direction: 0,
            directionMin: 0,
            period: 0,
            _id: '5a65c633908c020001169dc9',
          },
          {
            height: 0,
            direction: 0,
            directionMin: 0,
            period: 0,
            _id: '5a65c633908c020001169dc8',
          },
          {
            height: 0.3,
            direction: 188.44,
            directionMin: 187,
            period: 14,
            _id: '5a65c633908c020001169dc7',
          },
          {
            height: 0,
            direction: 0,
            directionMin: 0,
            period: 0,
            _id: '5a65c633908c020001169dc6',
          },
        ],
        wind: {
          speed: 4,
          direction: 345.23,
        },
        tide: {
          previous: {
            type: 'HIGH',
            height: 19.6,
            timestamp: 1473841815,
            utcOffset: -8,
          },
          current: {
            type: 'NORMAL',
            height: 8.2,
            timestamp: 1473851815,
            utcOffset: -8,
          },
          next: {
            type: 'LOW',
            height: 5.8,
            timestamp: 1473863720,
            utcOffset: -8,
          },
        },
        waterTemp: {
          min: 60,
          max: 65,
        },
        weather: {
          temperature: 74,
          condition: weatherCondition('MOSTLY_CLEAR_NO_RAIN'),
        },
      });
    });
  });
});
