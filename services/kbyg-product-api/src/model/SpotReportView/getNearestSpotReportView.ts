import SpotReportViewModel from './SpotReportView';

const getNearestSpotReportView = async ({ lon, lat }) => {
  const query = {
    location: {
      $near: {
        $geometry: {
          type: 'Point',
          coordinates: [lon, lat],
        },
      },
    },
  };
  const nearestSpotReportView = await SpotReportViewModel.findOne(query).lean();
  return nearestSpotReportView;
};

export default getNearestSpotReportView;
