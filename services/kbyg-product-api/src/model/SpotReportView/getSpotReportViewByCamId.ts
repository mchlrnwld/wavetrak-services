import convertSpotUnits from './convertSpotUnits';
import transformSpot from './transformSpot';
import SpotReportViewModel, { SpotReportView } from './SpotReportView';

const getSpotReportViewByCamId = async (cameraId: string, units): Promise<SpotReportView> => {
  let spotReportViewByCam = await SpotReportViewModel.findOne({ 'cameras.0._id': cameraId }).lean();

  // Applicable to COTM that are non-primary on multicam spots
  if (!spotReportViewByCam) {
    spotReportViewByCam = await SpotReportViewModel.findOne({ 'cameras._id': cameraId }).lean();
  }
  return convertSpotUnits(units)(transformSpot(spotReportViewByCam));
};

export default getSpotReportViewByCamId;
