import weatherCondition from '../../utils/weatherCondition';
import { sunriseSunset, locationNighttime } from '../../utils/datetime';
import mapLOTUSConditions from '../../utils/mapLOTUSConditions';
import rewindClip from '../../utils/rewindClip';

const cleanSpotData = {
  __v: undefined,
  slug: 'DEPRECATED', // TODO: Remove this. DEPRECATED to not break contracts.
  location: undefined,
  createdAt: undefined,
  updatedAt: undefined,
};

const transformSpot = (spotReportView) => {
  const { sunrise, sunset } = sunriseSunset(
    spotReportView.updatedAt,
    spotReportView.lat,
    spotReportView.lon,
  );

  let { waveHeight } = spotReportView;
  if (!waveHeight.human) {
    const mappedLOTUSConditions = mapLOTUSConditions(waveHeight.min, waveHeight.max);
    waveHeight = {
      ...waveHeight,
      humanRelation: mappedLOTUSConditions.humanRelation,
    };
  }

  return {
    ...spotReportView,
    ...cleanSpotData,
    cameras: spotReportView.cameras
      ? spotReportView.cameras.map((camera) => ({
          ...camera,
          stillUrl: camera.stillUrl,
          // TODO: Remove control. Left for backwards compatibility.
          control: camera.stillUrl,
          pixelatedStillUrl: undefined,
          nighttime: locationNighttime(spotReportView.lat, spotReportView.lon),
          rewindClip: rewindClip(camera.rewindBaseUrl, spotReportView.timezone, 10),
        }))
      : [],
    waveHeight,
    weather: {
      temperature: spotReportView.weather.temperature,
      condition: weatherCondition(
        spotReportView.weather.condition,
        spotReportView.updatedAt < sunrise || sunset <= spotReportView.updatedAt,
      ),
    },
  };
};

export default transformSpot;
