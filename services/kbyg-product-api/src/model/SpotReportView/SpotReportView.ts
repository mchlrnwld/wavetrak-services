import { model, Schema, Types } from 'mongoose';
import 'mongoose-geojson-schema';

// TODO: Remove this dependency and make all Spots requests through spots API.
interface Spot {
  _id: Types.ObjectId;
  name: string;
  spotType: string;
  abilityLevels: string[];
  boardTypes: string[];
  relivableRating: number;
}

interface Camera {
  _id: Types.ObjectId;
  title: string;
  status: {
    isDown: boolean;
    message?: string;
    subMessage?: string;
    altMessage?: string;
  };
  streamUrl: string;
  stillUrl: string;
  rewindBaseUrl: string;
  supportsHighlights: boolean;
  supportsCrowds: boolean;
  alias: string;
}

interface Swell {
  height: number;
  direction: number;
  directionMin: number;
  period: number;
}

interface Tide {
  type: string;
  height: number;
  timestamp: number;
  utcOffset: number;
}

export interface SpotReportView {
  _id: Types.ObjectId;
  name: string;
  lat: number;
  lon: number;
  location: {
    type: string;
    coordinates: number[];
  };
  legacyRegionId: number;
  legacyId: number;
  subregionId: Types.ObjectId;
  parentTaxonomy: Types.ObjectId[];
  waveHeight: {
    human: boolean;
    min: number;
    max: number;
    plus: boolean;
    occasional?: null;
    humanRelation: string;
  };
  swells: Swell[];
  weather: {
    temperature: number;
    condition: string;
  };
  wind: {
    speed: number;
    direction: number;
  };
  tide: {
    previous: Tide;
    current: Tide;
    next: Tide;
  };
  note?: string;
  conditions: {
    human: boolean;
    value: string;
    sortableCondition: number;
  };
  cameras: Camera[];
  thumbnail: string;
  timezone: string;
  waterTemp: {
    min: number;
    max: number;
  };
  abilityLevels: string[];
  boardTypes: string[];
  relivableRating?: number;
  offshoreDirection: number;
  rank: number;
}

export const spotSchema = new Schema<Spot>(
  {
    name: { type: String },
    spotType: { type: String },
    abilityLevels: [{ type: String }],
    boardTypes: [{ type: String }],
    relivableRating: { type: Number },
  },
  { collection: 'Spots' },
);

const cameraSchema = new Schema<Camera>({
  title: { type: String, required: true },
  status: {
    isDown: { type: Boolean, required: true },
    message: { type: String },
    subMessage: { type: String, default: '' },
    altMessage: { type: String, default: '' },
  },
  streamUrl: { type: String, required: true },
  stillUrl: { type: String, required: true },
  rewindBaseUrl: { type: String },
  supportsHighlights: {
    type: Boolean,
    default: false,
  },
  supportsCrowds: {
    type: Boolean,
    default: false,
  },
  alias: { type: String },
});

const swellSchema = new Schema<Swell>(
  {
    height: { type: Number, required: true },
    direction: { type: Number, required: true },
    directionMin: { type: Number, required: true },
    period: { type: Number, required: true },
  },
  {
    _id: false,
  },
);

const tideSchema = new Schema<Tide>(
  {
    type: { type: String },
    height: { type: Number },
    timestamp: { type: Number },
    utcOffset: { type: Number },
  },
  {
    _id: false,
  },
);

const spotReportViewSchema = new Schema<SpotReportView>(
  {
    name: { type: String },
    lat: { type: Number },
    lon: { type: Number },
    location: {
      type: Schema.Types.Point,
    },
    legacyRegionId: { type: Number },
    legacyId: { type: Number },
    subregionId: { type: Schema.Types.ObjectId, ref: 'Subregions' },
    parentTaxonomy: [{ type: Schema.Types.ObjectId, ref: 'Taxonomy' }],
    waveHeight: {
      human: { type: Boolean },
      min: { type: Number },
      max: { type: Number },
      plus: { type: Boolean, default: false },
      occasional: { type: Number, default: null },
      humanRelation: { type: String },
    },
    swells: [swellSchema],
    weather: {
      temperature: { type: Number },
      condition: { type: String },
    },
    wind: {
      speed: { type: Number },
      direction: { type: Number },
    },
    tide: {
      previous: tideSchema,
      current: tideSchema,
      next: tideSchema,
    },
    note: { type: String, required: true, default: '' },
    conditions: {
      human: { type: Boolean },
      value: { type: String },
      sortableCondition: { type: Number },
    },
    cameras: [cameraSchema],
    thumbnail: { type: String },
    timezone: { type: String },
    waterTemp: {
      min: { type: Number },
      max: { type: Number },
    },
    abilityLevels: [{ type: String }],
    boardTypes: [{ type: String }],
    relivableRating: { type: Number, required: false, default: 0 },
    offshoreDirection: { type: Number },
    rank: { type: Number },
  },
  {
    collection: 'SpotReportViews',
  },
);

spotReportViewSchema.index({ 'cameras.0._id': 1 }, { sparse: true });
spotReportViewSchema.index({ subregionId: 1 });
spotReportViewSchema.index({ parentTaxonomy: 1 });
spotReportViewSchema.index({ location: '2dsphere' });

export const Spots = model<Spot>('AttributeFilteredSpots', spotSchema);

export default model<SpotReportView>('SpotReportViews', spotReportViewSchema);
