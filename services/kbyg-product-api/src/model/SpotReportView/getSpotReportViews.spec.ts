import { expect } from 'chai';
import sinon from 'sinon';
import spotsFixture from './fixtures/spots.json';
import SpotReportViewModel from './SpotReportView';
import getSpotReportViews from './getSpotReportViews';
import weatherCondition from '../../utils/weatherCondition';

describe('SpotReportView Model', () => {
  describe('getSpotReportViews', () => {
    let findStub;

    beforeEach(() => {
      findStub = sinon.stub(SpotReportViewModel, 'find');
    });

    afterEach(() => {
      findStub.restore();
    });

    it('should find SpotReportViews within coordinate box', async () => {
      findStub.returns({
        lean: sinon.stub().resolves(spotsFixture),
      });
      const units = {
        windSpeed: 'KTS',
        waveHeight: 'FT',
        tideHeight: 'FT',
      };
      const spotReportViews = await getSpotReportViews(
        {
          north: 33.78114884960411,
          south: 33.551699629659424,
          east: -117.86386978111409,
          west: -118.13166152916097,
        },
        units,
      );
      expect(spotReportViews).to.deep.equal([
        {
          ...spotsFixture[0],
          location: undefined,
          createdAt: undefined,
          updatedAt: undefined,
          cameras: [],
          __v: undefined,
          waveHeight: {
            human: true,
            min: 2,
            max: 3,
            occasional: null,
            plus: true,
            humanRelation: 'shoulder high to 1 ft overhead',
          },
          swells: [
            {
              height: 1.3,
              direction: 265.78,
              directionMin: 264,
              period: 11,
              _id: '5a65c633908c020001169dcb',
            },
            {
              height: 0.5,
              direction: 268.59,
              directionMin: 267,
              period: 8,
              _id: '5a65c633908c020001169dca',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc9',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc8',
            },
            {
              height: 0.3,
              direction: 188.44,
              directionMin: 187,
              period: 14,
              _id: '5a65c633908c020001169dc7',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc6',
            },
          ],
          tide: {
            previous: {
              type: 'HIGH',
              height: 19.6,
              timestamp: 1473841815,
              utcOffset: -8,
            },
            current: {
              type: 'NORMAL',
              height: 8.2,
              timestamp: 1473851815,
              utcOffset: -8,
            },
            next: {
              type: 'LOW',
              height: 5.8,
              timestamp: 1473863720,
              utcOffset: -8,
            },
          },
          weather: {
            temperature: 74,
            condition: weatherCondition('MOSTLY_CLEAR_NO_RAIN'),
          },
        },
        {
          ...spotsFixture[1],
          location: undefined,
          createdAt: undefined,
          updatedAt: undefined,
          cameras: [],
          __v: undefined,
          waveHeight: {
            human: true,
            min: 3,
            max: 3,
            occasional: null,
            plus: true,
            humanRelation: 'shoulder high to 1 ft overhead',
          },
          swells: [
            {
              height: 1.3,
              direction: 265.78,
              directionMin: 264,
              period: 11,
              _id: '5a65c633908c020001169dcb',
            },
            {
              height: 0.5,
              direction: 268.59,
              directionMin: 267,
              period: 8,
              _id: '5a65c633908c020001169dca',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc9',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc8',
            },
            {
              height: 0.3,
              direction: 188.44,
              directionMin: 187,
              period: 14,
              _id: '5a65c633908c020001169dc7',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc6',
            },
          ],
          tide: {
            previous: {
              type: 'HIGH',
              height: 19.5,
              timestamp: 1473841815,
              utcOffset: -8,
            },
            current: {
              type: 'HIGH',
              height: 19.5,
              timestamp: 1473841815,
              utcOffset: -8,
            },
            next: {
              type: 'LOW',
              height: 5.5,
              timestamp: 1473863720,
              utcOffset: -8,
            },
          },
          weather: {
            temperature: 74,
            condition: weatherCondition('MOSTLY_CLEAR_NO_RAIN'),
          },
        },
        {
          ...spotsFixture[2],
          location: undefined,
          createdAt: undefined,
          updatedAt: undefined,
          cameras: [],
          __v: undefined,
          waveHeight: {
            human: true,
            min: 2,
            max: 3,
            occasional: null,
            plus: true,
            humanRelation: null,
          },
          swells: [
            {
              height: 1.3,
              direction: 265.78,
              directionMin: 264,
              period: 11,
              _id: '5a65c633908c020001169dcb',
            },
            {
              height: 0.5,
              direction: 268.59,
              directionMin: 267,
              period: 8,
              _id: '5a65c633908c020001169dca',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc9',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc8',
            },
            {
              height: 0.3,
              direction: 188.44,
              directionMin: 187,
              period: 14,
              _id: '5a65c633908c020001169dc7',
            },
            {
              height: 0,
              direction: 0,
              directionMin: 0,
              period: 0,
              _id: '5a65c633908c020001169dc6',
            },
          ],
          tide: null,
          weather: {
            temperature: 73,
            condition: weatherCondition('CLEAR_NO_RAIN'),
          },
        },
      ]);
    });
  });
});
