import { Types } from 'mongoose';
import SpotReportViewModel from './SpotReportView';
import convertSpotUnits from './convertSpotUnits';
import transformSpot from './transformSpot';

const getTopSpotReportViews = async (taxonomyId, units) => {
  const spotReportViews = await SpotReportViewModel.find({
    parentTaxonomy: new Types.ObjectId(taxonomyId),
  })
    .sort({ 'waveHeight.max': -1 })
    .limit(20)
    .lean();

  const result = spotReportViews.map(transformSpot).map(convertSpotUnits(units));
  return result;
};

export default getTopSpotReportViews;
