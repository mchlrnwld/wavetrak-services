import newrelic from 'newrelic';
import convertSpotUnits from './convertSpotUnits';
import SpotReportViewModel, { SpotReportView } from './SpotReportView';
import transformSpot from './transformSpot';

const getSubregionSpotReportViews = async (
  units,
  subregionId: string,
): Promise<SpotReportView[]> => {
  const query = { subregionId };
  const spotReportViews = await SpotReportViewModel.find(query).lean();
  const result = spotReportViews
    .map((report) => {
      try {
        return transformSpot(report);
      } catch (err) {
        newrelic.noticeError(err);
        return null;
      }
    })
    .filter((report) => !!report)
    .map(convertSpotUnits(units)) as SpotReportView[];
  return result;
};

export default getSubregionSpotReportViews;
