import { expect } from 'chai';
import spotsFixture from './fixtures/spots.json';
import convertSpotUnits from './convertSpotUnits';

describe('SpotReportView Model', () => {
  describe('convertSpotUnits', () => {
    it('converts wind speed, wave heights, and tide heights from imperial based on units parameter', () => {
      const spot = spotsFixture[0];

      const metricUnits = {
        swellHeight: 'M',
        windSpeed: 'KPH',
        waveHeight: 'M',
        tideHeight: 'M',
        temperature: 'C',
      };
      expect(convertSpotUnits(metricUnits)(spot)).to.deep.equal({
        ...spot,
        wind: {
          ...spot.wind,
          speed: 7,
        },
        waveHeight: {
          ...spot.waveHeight,
          min: 0.7,
          max: 1,
          occasional: null,
          humanRelation: 'shoulder high to 0.3 m overhead',
        },
        swells: [
          {
            ...spot.swells[0],
            height: 0.4,
          },
          {
            ...spot.swells[1],
            height: 0.2,
          },
          {
            ...spot.swells[2],
            height: 0,
          },
          {
            ...spot.swells[3],
            height: 0,
          },
          {
            ...spot.swells[4],
            height: 0.1,
          },
          {
            ...spot.swells[5],
            height: 0,
          },
        ],
        tide: {
          ...spot.tide,
          previous: {
            ...spot.tide.previous,
            height: 6,
          },
          current: {
            ...spot.tide.current,
            height: 2.5,
          },
          next: {
            ...spot.tide.next,
            height: 1.8,
          },
        },
        waterTemp: {
          min: 16,
          max: 18,
        },
        weather: {
          ...spot.weather,
          temperature: 23,
        },
      });

      const imperialUnits = {
        swellHeight: 'FT',
        windSpeed: 'MPH',
        waveHeight: 'FT',
        tideHeight: 'FT',
        temperature: 'F',
      };
      expect(convertSpotUnits(imperialUnits)(spot)).to.deep.equal({
        ...spot,
        wind: {
          ...spot.wind,
          speed: 5,
        },
        waveHeight: {
          ...spot.waveHeight,
          min: 2,
          max: 3,
          occasional: null,
        },
        swells: [
          {
            ...spot.swells[0],
            height: 1.3,
          },
          {
            ...spot.swells[1],
            height: 0.5,
          },
          {
            ...spot.swells[2],
            height: 0,
          },
          {
            ...spot.swells[3],
            height: 0,
          },
          {
            ...spot.swells[4],
            height: 0.3,
          },
          {
            ...spot.swells[5],
            height: 0,
          },
        ],
        tide: {
          ...spot.tide,
          previous: {
            ...spot.tide.previous,
            height: 19.6,
          },
          current: {
            ...spot.tide.current,
            height: 8.2,
          },
          next: {
            ...spot.tide.next,
            height: 5.8,
          },
        },
        waterTemp: {
          min: 60,
          max: 65,
        },
        weather: {
          ...spot.weather,
          temperature: 74,
        },
      });
    });
  });
});
