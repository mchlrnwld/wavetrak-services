import { getHeightInFeet } from '../../utils/convertUnits';
import { getWaveForecasts } from '../../external/forecasts';
import { dateRange, createUTCOffsetCalculator, fromUnixTimestamp } from '../../utils/datetime';
import utcOffsetFromTimezone from '../../utils/utcOffsetFromTimezone';
import { getOptimalSurfScore, getOptimalSwellScore } from '../../utils/getOptimalScore';
import mapLOTUSConditions from '../../utils/mapLOTUSConditions';

const waveForecasts = async (
  { spot, days, intervalHours, units },
  correctedWaveHeights,
  thirdParty = null,
) => {
  const { start, end } = dateRange(days, spot.timezone);
  // Passing unit of 'HI' to the wave forecast fetch will return a simple rounding as found here
  // https://github.com/Surfline/wavetrak-services/blob/master/services/wave-api/src/utils/convertHeight.js
  const correctedWaveForecastUnits = {
    ...units,
    waveHeight: units.waveHeight === 'HI' ? 'FT' : units.waveHeight,
  };
  const forecast = await getWaveForecasts(
    {
      units: correctedWaveForecastUnits,
      pointOfInterestId: spot.pointOfInterestId,
      start,
      end,
      interval: intervalHours * 60,
    },
    correctedWaveHeights,
  );

  const utcOffsetCalculator = createUTCOffsetCalculator(spot.timezone);

  const data = forecast.data.map(({ timestamp, wave, surf }) => {
    const convertedSurfMin = getHeightInFeet(units.waveHeight, surf.min);
    const convertedSurfMax = getHeightInFeet(units.waveHeight, surf.max);
    const optimalSurfScore = getOptimalSurfScore(convertedSurfMax, spot.best.sizeRange);
    const { humanRelation, hawaiian } = mapLOTUSConditions(convertedSurfMin, convertedSurfMax);
    const surfData = {
      ...surf,
      min: units.waveHeight === 'HI' ? hawaiian.minHeight : surf.min,
      max: units.waveHeight === 'HI' ? hawaiian.maxHeight : surf.max,
      optimalScore: optimalSurfScore,
      humanRelation,
    };
    if (thirdParty) delete surfData.optimalScore;
    return {
      timestamp,
      utcOffset: utcOffsetCalculator(fromUnixTimestamp(timestamp)),
      surf: surfData,
      swells: wave.components.map(({ height, direction, period, spread }) => {
        const convertedSwellHeight = getHeightInFeet(units.waveHeight, height, true);
        if (thirdParty) return { height, period, spread, direction: direction.mean };
        return {
          height,
          period,
          direction: direction.mean,
          directionMin: direction.min,
          optimalScore: getOptimalSwellScore(
            convertedSwellHeight,
            direction.mean,
            period,
            spot.best.swellWindow,
            spot.best.swellPeriod,
            optimalSurfScore,
          ),
        };
      }),
    };
  });

  return {
    // TODO: Deprecate this property.
    utcOffset: utcOffsetFromTimezone(spot.timezone),
    offshoreLocation: forecast.associated.model[0].offshoreCoordinate,
    data,
  };
};

export default waveForecasts;
