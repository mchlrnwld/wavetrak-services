import { secondsPerDay, secondsPerHour } from '../../utils/datetime';
import { getWaveForecasts, getWindForecasts } from '../../external/forecasts';
import sortSwellsByImpact from './helpers/sortSwellsByImpact';
import writtenForecast from './helpers/writtenForecast';
import mapLOTUSConditions from '../../utils/mapLOTUSConditions';

const SEVEN_AM = secondsPerHour * 7;
const THREE_PM = secondsPerHour * 15;

const INTERVAL_MINUTES = 60; // every 1 hours gets us 7am and 3pm in the response

const CONDITION_RATING_UNITS = { swellHeight: 'ft', waveHeight: 'ft', windSpeed: 'm/h' };

const convertForecast = (wave, _, rating = null) => {
  const { humanRelation } = mapLOTUSConditions(wave.surf.min, wave.surf.max);

  return {
    rating,
    minHeight: wave.surf.min,
    maxHeight: wave.surf.max,
    plus: wave.surf.plus,
    relation: humanRelation,
  };
};

const getConditions = async (spot, start, end) => {
  const [waveResponse, windResponse] = await Promise.all([
    getWaveForecasts({
      pointOfInterestId: spot.pointOfInterestId,
      start,
      end,
      interval: INTERVAL_MINUTES,
      units: CONDITION_RATING_UNITS,
    }),
    getWindForecasts({
      pointOfInterestId: spot.pointOfInterestId,
      start,
      end,
      interval: INTERVAL_MINUTES,
      units: CONDITION_RATING_UNITS,
    }),
  ]);

  const windMap = new Map(windResponse.data.map((forecast) => [forecast.timestamp, forecast]));

  const waveMap = new Map(waveResponse.data.map((forecast) => [forecast.timestamp, forecast]));

  const days = Math.round((end - start) / secondsPerDay);

  const forecastConditions = [...Array(days)].map((_, i) => {
    const timestamp = start + secondsPerDay * i; // midnight of current iterated day

    const timestamp7 = timestamp + SEVEN_AM;
    const timestamp15 = timestamp + THREE_PM;

    const wave7 = waveMap.get(timestamp7);
    const wave15 = waveMap.get(timestamp15);

    const wind7 = windMap.get(timestamp7);
    const wind15 = windMap.get(timestamp15);

    const swell7 = sortSwellsByImpact(wave7.wave.components)[0];
    const swell15 = sortSwellsByImpact(wave15.wave.components)[0];

    return {
      forecastDate: timestamp,
      am: {
        timestamp: timestamp7,
        observation: writtenForecast(
          spot.offshoreDirection,
          wave7.surf.max,
          swell7.period,
          swell7.direction.mean,
          0.868976 * wind7.wind.speed, // Convert from mph to kts
          wind7.wind.direction,
        ),
        ...convertForecast(wave7, wind7),
      },
      pm: {
        timestamp: timestamp15,
        observation: writtenForecast(
          spot.offshoreDirection,
          wave15.surf.max,
          swell15.period,
          swell15.direction.mean,
          0.868976 * wind15.wind.speed, // Convert from mph to kts
          wind15.wind.direction,
        ),
        ...convertForecast(wave15, wind15),
      },
    };
  });

  return { start, conditions: forecastConditions, spot };
};

export default getConditions;
