import { getSpot } from '../../external/spots';
import getLOLAConditions from './getLOLAConditions';

export const getLOLAConditionsForSubregion = async (subregion, days, units) => {
  const spot = await getSpot(subregion.primarySpot);

  const conditions = await getLOLAConditions({
    spot,
    days,
    units,
  });
  return {
    conditions,
    timezone: spot.timezone,
  };
};
