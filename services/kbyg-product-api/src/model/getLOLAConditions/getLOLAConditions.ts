import moment from 'moment-timezone';
import { convertStringByUnits } from '../../utils/convertUnits';
import LOLAConditionsLengthError from '../../errors/LOLAConditionsLengthError';
import { getSubregionForecasts } from '../../external/subregions';
import getGravatarURL from '../../utils/getGravatarURL';
import mapPeriod from './helpers/mapPeriod';
import getConditions from './getConditions';
import { secondsPerDay, dateRange } from '../../utils/datetime';
import convertWaveHeightsUnits from '../../utils/convertWaveHeightsUnits';

const getLOLAConditions = async ({ spot, days, units }) => {
  const { start, end } = dateRange(days, spot.timezone);
  const [observed, lola] = await Promise.all([
    getSubregionForecasts(spot.subregion, spot.timezone, days),
    getConditions(spot, start, end),
  ]);

  if (lola.conditions.length !== days) throw new LOLAConditionsLengthError();

  const doConvertWaveHeightsUnits = convertWaveHeightsUnits(units.waveHeight);
  const doConvertStringByUnits = convertStringByUnits(units.waveHeight);
  const forecaster = observed.summary.forecaster.name
    ? {
        name: observed.summary.forecaster.name,
        avatar: getGravatarURL(observed.summary.forecaster.email),
      }
    : null;

  const conditions = lola.conditions.map((dayLola, i) => {
    const dayObserved = observed.days[i];
    const human = !!dayObserved;
    const forecastDay = dayObserved
      ? observed.days[i].forecastDay
      : moment().tz(spot.timezone).clone().add(i, 'days').format('YYYY-MM-DD');

    const observation =
      human && dayObserved.forecast.observation
        ? dayObserved.forecast.observation
        : dayLola.am.observation;

    return {
      timestamp: start + i * secondsPerDay,
      forecastDay,
      forecaster: dayObserved ? forecaster : null,
      human,
      observation,
      am: mapPeriod(
        human,
        dayObserved ? dayObserved.forecast.am : dayLola.am,
        doConvertWaveHeightsUnits,
        doConvertStringByUnits,
      ),
      pm: mapPeriod(
        human,
        dayObserved ? dayObserved.forecast.pm : dayLola.pm,
        doConvertWaveHeightsUnits,
        doConvertStringByUnits,
      ),
    };
  });

  return conditions;
};

export default getLOLAConditions;
