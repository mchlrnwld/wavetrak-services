const swellImpact = ({ height, period }) => height * (0.1 * period);

const sortSwellsByImpact = (swells) =>
  [...swells].sort((prevSwell, nextSwell) => swellImpact(nextSwell) - swellImpact(prevSwell));

export default sortSwellsByImpact;
