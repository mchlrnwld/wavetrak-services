import mapLOTUSConditions from '../../../utils/mapLOTUSConditions';

const mapPeriod = (human, conditions, doConvertWaveHeights, doConvertStringByUnits) => {
  const { maxHeight, minHeight } = doConvertWaveHeights(
    conditions.minHeight,
    conditions.maxHeight,
    true,
  );

  return {
    maxHeight,
    minHeight,
    /* TODO: See kbyg-3336 for a potential refactor.
     *  In this case, plus != null is correct because null is a potential outcome
     *  from doConvertWaveHeights. In that case it should defer to addPlus.
     */
    plus: !!conditions.plus,
    humanRelation: doConvertStringByUnits(
      mapLOTUSConditions(conditions.minHeight, conditions.maxHeight).humanRelation,
    ),
    occasionalHeight: null,
    rating: human ? conditions.rating : null,
  };
};

export default mapPeriod;
