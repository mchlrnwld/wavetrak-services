import { expect } from 'chai';
import { convertStringByUnits } from '../../../utils/convertUnits';
import convertWaveHeightsUnits from '../../../utils/convertWaveHeightsUnits';
import mapPeriod from './mapPeriod';

describe('mapPeriod', () => {
  let condition = {
    maxHeight: 2,
    minHeight: 1,
    occasionalHeight: null,
    rating: 'VERY_GOOD',
  };

  it('should return a formatted forecast condition for a LOTUS mapped condition', () => {
    expect(
      mapPeriod(false, condition, convertWaveHeightsUnits('FT'), convertStringByUnits('FT')),
    ).to.deep.equal({
      humanRelation: 'Knee to thigh',
      maxHeight: 2,
      minHeight: 1,
      occasionalHeight: null,
      plus: false,
      rating: null,
    });

    expect(
      mapPeriod(false, condition, convertWaveHeightsUnits('M'), convertStringByUnits('M')),
    ).to.deep.equal({
      humanRelation: 'Knee to thigh',
      maxHeight: 0.6,
      minHeight: 0.3,
      occasionalHeight: null,
      plus: false,
      rating: null,
    });

    expect(
      mapPeriod(false, condition, convertWaveHeightsUnits('HI'), convertStringByUnits('HI')),
    ).to.deep.equal({
      humanRelation: 'Knee to thigh',
      maxHeight: 1,
      minHeight: 0.5,
      occasionalHeight: null,
      plus: false,
      rating: null,
    });
  });

  it('should return a formatted forecast condition for a human observed forecast', () => {
    condition = {
      maxHeight: 2,
      minHeight: 1,
      occasionalHeight: null,
      rating: 'VERY_GOOD',
    };

    expect(
      mapPeriod(true, condition, convertWaveHeightsUnits('FT'), convertStringByUnits('FT')),
    ).to.deep.equal({
      humanRelation: 'Knee to thigh',
      maxHeight: 2,
      minHeight: 1,
      occasionalHeight: null,
      plus: false,
      rating: 'VERY_GOOD',
    });

    expect(
      mapPeriod(true, condition, convertWaveHeightsUnits('M'), convertStringByUnits('M')),
    ).to.deep.equal({
      humanRelation: 'Knee to thigh',
      maxHeight: 0.6,
      minHeight: 0.3,
      occasionalHeight: null,
      plus: false,
      rating: 'VERY_GOOD',
    });

    expect(
      mapPeriod(true, condition, convertWaveHeightsUnits('HI'), convertStringByUnits('HI')),
    ).to.deep.equal({
      humanRelation: 'Knee to thigh',
      maxHeight: 1,
      minHeight: 0.5,
      occasionalHeight: null,
      plus: false,
      rating: 'VERY_GOOD',
    });
  });

  it('should conditionally return based on human', () => {
    condition = {
      maxHeight: 4,
      minHeight: 3,
      occasionalHeight: null,
      rating: 'VERY_GOOD',
    };

    expect(
      mapPeriod(true, condition, convertWaveHeightsUnits('FT'), convertStringByUnits('FT')),
    ).to.deep.equal({
      humanRelation: 'Waist to chest',
      maxHeight: 4,
      minHeight: 3,
      occasionalHeight: null,
      plus: false,
      rating: 'VERY_GOOD',
    });

    expect(
      mapPeriod(false, condition, convertWaveHeightsUnits('FT'), convertStringByUnits('FT')),
    ).to.deep.equal({
      humanRelation: 'Waist to chest',
      maxHeight: 4,
      minHeight: 3,
      occasionalHeight: null,
      plus: false,
      rating: null,
    });
  });
});
