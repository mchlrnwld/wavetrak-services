import { expect } from 'chai';
import writtenForecast, { getWindDirectionType } from './writtenForecast';

const writtenForecastSpec = () => {
  describe('writtenForecast', () => {
    it('return a written observation given small surf conditions', () => {
      expect(writtenForecast(null, 2.3, 8, 279.84, 4.47, 193.9)).to.equal(
        'Small short period waves from the W. Light and variable winds with clean conditions.',
      );
    });

    describe('getWindDirectionType', () => {
      it('returns correct direction type based on offshore direction and direction', () => {
        expect(getWindDirectionType(90, 35)).to.equal('ONSHORE');
        expect(getWindDirectionType(90, 90)).to.equal('ONSHORE');
        expect(getWindDirectionType(90, 145)).to.equal('ONSHORE');
        expect(getWindDirectionType(90, 155)).to.equal('SIDE_ONSHORE');
        expect(getWindDirectionType(90, 180)).to.equal('SIDESHORE');
        expect(getWindDirectionType(90, 205)).to.equal('SIDE_OFFSHORE');
        expect(getWindDirectionType(90, 215)).to.equal('OFFSHORE');
        expect(getWindDirectionType(90, 270)).to.equal('OFFSHORE');
        expect(getWindDirectionType(90, 325)).to.equal('OFFSHORE');
        expect(getWindDirectionType(90, 335)).to.equal('SIDE_OFFSHORE');
        expect(getWindDirectionType(90, 0)).to.equal('SIDESHORE');
        expect(getWindDirectionType(90, 25)).to.equal('SIDE_ONSHORE');

        expect(getWindDirectionType(0, 305)).to.equal('ONSHORE');
        expect(getWindDirectionType(0, 0)).to.equal('ONSHORE');
        expect(getWindDirectionType(0, 55)).to.equal('ONSHORE');
        expect(getWindDirectionType(0, 65)).to.equal('SIDE_ONSHORE');
        expect(getWindDirectionType(0, 90)).to.equal('SIDESHORE');
        expect(getWindDirectionType(0, 115)).to.equal('SIDE_OFFSHORE');
        expect(getWindDirectionType(0, 125)).to.equal('OFFSHORE');
        expect(getWindDirectionType(0, 180)).to.equal('OFFSHORE');
        expect(getWindDirectionType(0, 235)).to.equal('OFFSHORE');
        expect(getWindDirectionType(0, 245)).to.equal('SIDE_OFFSHORE');
        expect(getWindDirectionType(0, 270)).to.equal('SIDESHORE');
        expect(getWindDirectionType(0, 295)).to.equal('SIDE_ONSHORE');
      });
    });
  });
};

export default writtenForecastSpec;
