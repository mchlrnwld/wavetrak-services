import { Dms } from 'geodesy';

export const getWindDirectionType = (offshoreDirection, direction) => {
  if (!offshoreDirection && offshoreDirection !== 0) return null;

  const angleBetween = Math.abs(offshoreDirection - direction) % 360;

  if (angleBetween < 60) return 'ONSHORE';
  if (angleBetween < 75) return 'SIDE_ONSHORE';
  if (angleBetween < 105) return 'SIDESHORE';
  if (angleBetween < 120) return 'SIDE_OFFSHORE';
  if (angleBetween < 240) return 'OFFSHORE';
  if (angleBetween < 255) return 'SIDE_OFFSHORE';
  if (angleBetween < 285) return 'SIDESHORE';
  if (angleBetween < 300) return 'SIDE_ONSHORE';
  return 'ONSHORE';
};

const getWindDescription = (speed, direction) => {
  const compassDirection = Dms.compassPoint(direction);

  if (speed <= 6) return 'Light and variable winds';
  if (speed < 11) return `Light ${compassDirection} winds`;
  if (speed < 17) return `Moderate ${compassDirection} winds`;
  if (speed < 22) return `Moderate to strong ${compassDirection} winds`;
  if (speed < 28) return `Strong ${compassDirection} winds`;
  if (speed < 35) return `Very strong ${compassDirection} winds`;
  if (speed < 64) return `Tropical storm force ${compassDirection} winds`;
  return `Hurricane force ${compassDirection} winds`;
};

const getWindConditions = (speed, directionType) => {
  if (speed <= 6) return 'clean conditions';
  if (speed < 11) {
    switch (directionType) {
      case 'OFFSHORE':
        return 'clean conditions';
      case 'ONSHORE':
        return 'semi bumpy conditions';
      case 'SIDE_OFFSHORE':
      case 'SIDE_ONSHORE':
      case 'SIDESHORE':
        return 'semi clean to slightly sideshore textured conditions';
      default:
        return null;
    }
  }
  if (speed < 17) {
    switch (directionType) {
      case 'OFFSHORE':
        return 'clean conditions';
      case 'ONSHORE':
        return 'bumpy conditions';
      case 'SIDE_OFFSHORE':
      case 'SIDE_ONSHORE':
      case 'SIDESHORE':
        return 'sideshore textured conditions';
      default:
        return null;
    }
  }
  if (speed < 22) {
    switch (directionType) {
      case 'OFFSHORE':
      case 'SIDE_OFFSHORE':
        return 'semi clean to clean conditions';
      case 'ONSHORE':
      case 'SIDE_ONSHORE':
        return 'choppy conditions';
      case 'SIDESHORE':
        return 'bumpy conditions';
      default:
        return null;
    }
  }
  if (speed < 28) {
    switch (directionType) {
      case 'OFFSHORE':
      case 'SIDE_OFFSHORE':
        return 'semi clean faces but spray and whitecaps becoming problematic';
      case 'ONSHORE':
      case 'SIDE_ONSHORE':
      case 'SIDESHORE':
        return 'choppy conditions';
      default:
        return null;
    }
  }
  switch (directionType) {
    case 'OFFSHORE':
    case 'SIDE_OFFSHORE':
      return 'blown out conditions except for very sheltered spots';
    case 'ONSHORE':
    case 'SIDE_ONSHORE':
    case 'SIDESHORE':
      return 'very choppy, blown out conditions';
    default:
      return null;
  }
};

const getWindObservation = (offshoreDirection, speed, direction) => {
  const directionType = getWindDirectionType(offshoreDirection, direction);
  const windDescription = getWindDescription(speed, direction);
  const windConditions = getWindConditions(speed, directionType);
  return windConditions ? `${windDescription} with ${windConditions}` : windDescription;
};

const getWaveDirection = (direction) => Dms.compassPoint(direction);

const getSwellObservation = (period) => {
  if (period <= 7) return 'very short period waves';
  if (period < 11) return 'short period waves';
  if (period < 15) return 'mid period waves';
  if (period < 20) return 'long period waves';
  return 'very long period waves';
};

const getSurfHeight = (height) => {
  if (height <= 2) return 'Very small';
  if (height <= 3) return 'Small';
  if (height <= 5) return 'Fun sized';
  if (height <= 8) return 'Moderate';
  if (height <= 12) return 'Solid';
  if (height <= 18) return 'Large';
  return 'Very large';
};

/**
 * Given a set of condition values return written commentary for it's condition's.
 *
 * @param {Number} height
 * @param {Number} period
 * @param {Number} direction
 * @param {Number} windSpeed passed in m/s
 * @param {Number} windDirection
 */
const writtenForecast = (
  offshoreDirection,
  surfHeight,
  swellPeriod,
  waveDirection,
  windSpeed,
  windDirection,
) => {
  const waveDirectionWritten = getWaveDirection(waveDirection);

  const windObservation = getWindObservation(offshoreDirection, windSpeed, windDirection);
  const swellObservation = getSwellObservation(swellPeriod);
  const surfHeightWritten = getSurfHeight(surfHeight);

  return `${surfHeightWritten} ${swellObservation} from the ${waveDirectionWritten}. ${windObservation}.`;
};

export default writtenForecast;
