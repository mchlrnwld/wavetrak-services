import { expect } from 'chai';
import sinon from 'sinon';
import * as forecastsAPI from '../../external/forecasts/forecasts';
import { secondsPerHour, secondsPerDay } from '../../utils/datetime';
import getConditions from './getConditions';
import writtenForecastSpec from './helpers/writtenForecast.spec';

describe('models / getLOLAConditions / getConditions', () => {
  let clock;
  let getWaveForecastsStub;
  let getWindForecastsStub;

  const start = 1493708400; // 05/02/2017@00:00PDT
  const end17Days = start + 17 * secondsPerDay; // 05/19/2017/00:00PDT
  const end3Days = start + 3 * secondsPerDay; // 05/19/2017/00:00PDT

  const SEVEN_AM = secondsPerHour * 7;
  const THREE_PM = secondsPerHour * 15;

  const spot = {
    _id: '5842041f4e65fad6a7708827',
    location: { coordinates: [90, 90] },
    forecastLocation: { coordinates: [90, 90] },
    timezone: 'America/Los_Angeles',
  };

  const waveForecast = {
    data: [...Array((end17Days - start) / 3600)].map((_, i) => ({
      timestamp: start + i * 3600,
      surf: { min: 1, max: 2 },
      wave: {
        components: [
          {
            height: 1.5,
            direction: { min: 279.14, max: 288.98, mean: 284.06 },
            period: 6,
          },
        ],
      },
    })),
  };

  const windForecast = {
    data: [...Array((end17Days - start) / 3600)].map((_, i) => ({
      timestamp: start + i * 3600,
      wind: { speed: null, direction: null },
    })),
  };

  before(() => {
    clock = sinon.useFakeTimers(start * 1000);
  });

  beforeEach(() => {
    getWaveForecastsStub = sinon.stub(forecastsAPI, 'getWaveForecasts');
    getWindForecastsStub = sinon.stub(forecastsAPI, 'getWindForecasts');
  });

  afterEach(() => {
    getWaveForecastsStub.restore();
    getWindForecastsStub.restore();
  });

  after(() => {
    clock.restore();
  });

  it('should return conditions given a spot', async () => {
    getWaveForecastsStub.resolves(waveForecast);
    getWindForecastsStub.resolves(windForecast);

    const conditions = await getConditions(spot, start, end3Days);

    expect(conditions).to.be.ok();
    expect(conditions.conditions.length).to.equal(3);
  });

  it('should return am/pm condition forecasts for 7am and 3pm', async () => {
    getWaveForecastsStub.resolves(waveForecast);
    getWindForecastsStub.resolves(windForecast);

    const { conditions } = await getConditions(spot, start, end3Days);

    expect(conditions.length).to.equal(3);
    conditions.forEach((condition, k) => {
      const midnight = start + 86400 * k;
      expect(condition.forecastDate).to.equal(midnight);
      expect(condition.am.timestamp).to.equal(midnight + SEVEN_AM);
      expect(condition.pm.timestamp).to.equal(midnight + THREE_PM);
    });
  });

  it('should return 17 days when passed as days', async () => {
    getWaveForecastsStub.resolves(waveForecast);
    getWindForecastsStub.resolves(windForecast);

    const { conditions } = await getConditions(spot, start, end17Days);

    expect(conditions.length).to.equal(17);
  });

  it('should throw an error if the spot ID is invalid', async () => {
    try {
      await getConditions({ _id: 'badspotobject' }, 0, 0);
    } catch (err) {
      expect(err).to.exist();
    }
  });

  writtenForecastSpec();
});
