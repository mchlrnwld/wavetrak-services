import { initCache } from '@surfline/services-common';
import sinon from 'sinon';
import chai from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import mongoose from 'mongoose';

import * as userApi from './external/user';
import * as spotsApi from './external/spots';
import * as reportsApi from './external/reports';
import * as entitlementsApi from './external/entitlements';

import subregions from './fixtures/subregions';
import spotTaxonomy from './fixtures/spotTaxonomy';
import { premium, free } from './fixtures/entitlements';
import { northOCSpot, southOCSpot } from './fixtures/spots';
import { spotReport, noSpotReport, regionalReport, noRegionalReport } from './fixtures/reports';

chai.use(chaiHttp);
chai.use(dirtyChai);
chai.use(sinonChai);

mongoose.Promise = global.Promise;
initCache({ enabled: false, readRedis: '', writeRedis: '' });

/** @type {sinon.SinonStub} */
let getUserSettingsStub;

/** @type {sinon.SinonStub} */
let fetchSpotStub;

/** @type {sinon.SinonStub} */
let getSubregionStub;

/** @type {sinon.SinonStub} */
let getSpotTaxonomyStub;

/** @type {sinon.SinonStub} */
let getSpotReportStub;

/** @type {sinon.SinonStub} */
let getRegionalReportStub;

/** @type {sinon.SinonStub} */
let getEntitlementsStub;

/** @type {sinon.SinonStub} */
let isPremiumStub;

beforeEach(() => {
  getUserSettingsStub = sinon.stub(userApi, 'default');
  getUserSettingsStub.withArgs('582495992d06cd3b71d66220').resolves({
    units: {
      swellHeight: 'M',
      surfHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      windSpeed: 'KTS',
    },
  });
  getUserSettingsStub.resolves({
    units: {
      swellHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      surfHeight: 'M',
      windSpeed: 'KPH',
    },
  });
  fetchSpotStub = sinon.stub(spotsApi, 'getSpot');
  fetchSpotStub.withArgs(northOCSpot._id).resolves(northOCSpot);
  fetchSpotStub.withArgs(southOCSpot._id).resolves(southOCSpot);

  getSubregionStub = sinon.stub(spotsApi, 'getSubregion');
  getSubregionStub.resolves(subregions);

  getSpotTaxonomyStub = sinon.stub(spotsApi, 'getTaxonomy');
  getSpotTaxonomyStub.resolves(spotTaxonomy);

  getSpotReportStub = sinon.stub(reportsApi, 'getSpotReport');
  getSpotReportStub.withArgs('5842041f4e65fad6a77088ed').resolves(spotReport);
  getSpotReportStub.resolves(noSpotReport);

  getRegionalReportStub = sinon.stub(reportsApi, 'getRegionalReport');
  getRegionalReportStub.withArgs('58581a836630e24c44878fd6').resolves(regionalReport);
  getRegionalReportStub.resolves(noRegionalReport);

  getEntitlementsStub = sinon.stub(entitlementsApi, 'default');
  getEntitlementsStub.withArgs('582495992d06cd3b71d66229').resolves(premium);
  getEntitlementsStub.resolves(free);

  isPremiumStub = sinon.stub(entitlementsApi, 'isPremium');
  isPremiumStub.withArgs('582495992d06cd3b71d66229').resolves(true);
  isPremiumStub.resolves(false);
});

afterEach(() => {
  getUserSettingsStub.restore();
  fetchSpotStub.restore();
  getSubregionStub.restore();
  getSpotTaxonomyStub.restore();
  getSpotReportStub.restore();
  getRegionalReportStub.restore();
  getEntitlementsStub.restore();
  isPremiumStub.restore();
});
