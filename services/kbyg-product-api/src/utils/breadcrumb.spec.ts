import { expect } from 'chai';
import * as breadcrumb from './breadcrumbs';

import spotTaxonomy from './fixtures/spotTaxonomy.json';

describe('breadcrumbs', () => {
  it('should return breadcrumb for a spot', () => {
    const spotBreadcrumb = breadcrumb.spot(spotTaxonomy.in);
    expect(spotBreadcrumb).to.deep.equal([
      {
        name: 'United States',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/6252001',
      },
      {
        name: 'California',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/ca/5332921',
      },
      {
        name: 'Orange County',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/ca/orange-county/5379524',
      },
      {
        name: 'Huntington Beach',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/ca/orange-county/huntington-beach/5358705',
      },
    ]);
  });

  it('should return breadcrumb for a subregion', async () => {
    const subregionBreadcrumb = breadcrumb.subregion(spotTaxonomy.in);
    expect(subregionBreadcrumb).to.deep.equal([
      {
        name: 'United States',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/6252001',
      },
      {
        name: 'California',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/ca/5332921',
      },
      {
        name: 'Orange County',
        href: 'https://www.surfline.com/surf-reports-forecasts-cams/us/ca/orange-county/5379524',
      },
    ]);
  });
});
