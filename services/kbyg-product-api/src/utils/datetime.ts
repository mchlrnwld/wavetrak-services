import suncalc from 'suncalc';
import moment from 'moment';

const secondsPerDay = 60 * 60 * 24;
const secondsPerHour = 60 * 60;

const fromUnixTimestamp = (timestamp: number) => new Date(timestamp * 1000);

const toUnixTimestamp = (date: Date) => Math.floor(date.getTime() / 1000);

const day = (timezone: string, date?: Date) => moment(date).tz(timezone).format('YYYY-MM-DD');

const midnightOfDate = (timezone: string, date?: Date) =>
  moment(date).tz(timezone).startOf('day').toDate();

const sunriseSunset = (date: Date, lat: number, lon: number) => {
  const { dawn, sunrise, sunset, dusk } = suncalc.getTimes(date, lat, lon);
  return {
    dawn,
    sunrise,
    sunset,
    dusk,
  };
};

const locationNighttime = (lat: number, lon: number) => {
  const currentDate = new Date();
  const times = suncalc.getTimes(currentDate, lat, lon);
  const currentTime = toUnixTimestamp(currentDate);
  const nauticalDawn = toUnixTimestamp(new Date(times.nauticalDawn));
  const nauticalDusk = toUnixTimestamp(new Date(times.nauticalDusk));
  return currentTime > nauticalDusk || currentTime < nauticalDawn;
};

const createUTCOffsetCalculator = (timezone: string) => {
  const zone = moment.tz.zone(timezone);
  // zone.utcOffset is inverted to be POSIX compatible.
  // See https://momentjs.com/timezone/docs/#/zone-object/offset/
  return (date) => -zone.utcOffset(date) / 60.0;
};

const dateRange = (days: number, timezone: string, date?: Date) => {
  const start = moment(midnightOfDate(timezone, date));

  // Use noon instead of midnight to ensure correct date
  // Related to the following issue: https://github.com/moment/moment/issues/4743
  const noon = moment(start).tz(timezone).add(12, 'hours');

  const end = moment(noon).add(days, 'days').startOf('day');

  return { start: start.unix(), end: end.unix() };
};

export {
  secondsPerDay,
  secondsPerHour,
  fromUnixTimestamp,
  toUnixTimestamp,
  day,
  midnightOfDate,
  sunriseSunset,
  locationNighttime,
  createUTCOffsetCalculator,
  dateRange,
};
