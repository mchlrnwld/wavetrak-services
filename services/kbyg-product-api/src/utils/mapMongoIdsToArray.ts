import _get from 'lodash/get';

export default (mongoArray) => {
  const mappedArray =
    mongoArray && mongoArray.length
      ? mongoArray.map((arrItem) => {
          const result = arrItem.$oid
            ? _get(arrItem, '$oid', '')
            : { ...arrItem, _id: _get(arrItem, '_id.$oid', '') };
          return result;
        })
      : [];
  return mappedArray;
};
