import { convertWaveHeightsUnits } from '@surfline/services-common';
import { curry } from 'lodash';
import mapLOTUSConditions from './mapLOTUSConditions';

export type waveHeights = {
  minHeight: number;
  maxHeight: number;
};

export default curry(
  (
    units: string,
    minHeightInFeet: number,
    maxHeightInFeet: number,
    roundResult = true,
  ): waveHeights => {
    if (units === 'HI') {
      return mapLOTUSConditions(minHeightInFeet, maxHeightInFeet).hawaiian;
    }
    return convertWaveHeightsUnits(units, minHeightInFeet, maxHeightInFeet, roundResult);
  },
);
