import { intersection } from 'lodash';

const spot = (taxonomy) => {
  const geonames = taxonomy.filter(({ type }) => type === 'geoname');
  const urls = [];
  const ids = geonames.map(({ _id }) => _id);
  let node = geonames.find(({ liesIn }) => intersection(liesIn, ids).length === 0);

  while (node) {
    if (node.associated && node.associated.links) {
      const www = node.associated.links.find((link) => link && link.key === 'www');
      if (www) {
        urls.push({
          name: node.name,
          href: www.href,
        });
      }
    }

    // eslint-disable-next-line no-loop-func
    node = geonames.find(({ liesIn }) => liesIn.includes(node._id));
  }

  return urls;
};

const subregion = (taxonomy) => spot(taxonomy).slice(0, -1);

export { subregion, spot };
