import { expect } from 'chai';
import { getOptimalSurfScore, getOptimalSwellScore, getOptimalWindScore } from './getOptimalScore';

describe('utils / getOptimalScore', () => {
  describe('getOptimalSurfScore', () => {
    it('should return 2 if surfMax is within best wave height range', () => {
      const surfMax = 3;
      const bestSurfHeightRanges = [{ min: 2, max: 4 }];
      expect(getOptimalSurfScore(surfMax, bestSurfHeightRanges)).to.equal(2);
    });

    it('should return 0 if surfMax is not within best wave height range', () => {
      const surfMax = 6;
      const bestSurfHeightRanges = [{ min: 2, max: 4 }];
      expect(getOptimalSurfScore(surfMax, bestSurfHeightRanges)).to.equal(0);
    });
  });

  describe('getOptimalSwellScore', () => {
    // Swell window and period values are for HB Pier, Southside.
    // Roughly a SW-S window, W-NW window.
    const bestSwellDirectionRanges = [
      { min: 165, max: 200 },
      { min: 250, max: 280 },
    ];
    const bestSwellPeriodRanges = [{ min: 9, max: 22 }];
    const optimalSurfScore = 2;
    const suboptimalSurfScore = 0;

    it('should return 2 (optimum) if surf from swell >= 5ft, surf height is within range, swell angle is optimum', () => {
      const swell = { height: 5.2, period: 15, direction: { mean: 200 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          optimalSurfScore,
        ),
      ).to.equal(2);
    });

    it('should return 2 (optimum) if surf from swell >= 5ft, surf height is within range, swell angle is optimum and swell matches the second best swell direction window', () => {
      const swell = { height: 5.2, period: 15, direction: { mean: 260 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          optimalSurfScore,
        ),
      ).to.equal(2);
    });

    // Bad surf from swell.
    it('should return 0 if surf from swell < (3ft), surf height is within range, swell angle is optimum', () => {
      const swell = { height: 0.5, period: 9, direction: { mean: 200 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          optimalSurfScore,
        ),
      ).to.equal(0);
    });

    // Bad direction
    it('should return 0 if surf from swell >= 3ft, surf height is within range, but swell angle is offshore', () => {
      const swell = { height: 2.2, period: 15, direction: { mean: 90 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          optimalSurfScore,
        ),
      ).to.equal(0);
    });

    // Bad period
    it('should return 0 if surf from swell > 3ft, surf height is within range, swell angle is optimum but the period is too low', () => {
      const swell = { height: 20, period: 8, direction: { mean: 200 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          optimalSurfScore,
        ),
      ).to.equal(0);
    });

    // Bad total surf, but good swell.
    it('should return 1 if surf height is not in range, but everything else is optimal', () => {
      const swell = { height: 2, period: 20, direction: { mean: 200 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          suboptimalSurfScore,
        ),
      ).to.equal(1);
    });

    // Surf is good, all swell params aren't.
    it("should return 0 if surf height is good, but swell isn't optimal", () => {
      const swell = { height: 0.2, period: 2, direction: { mean: 90 } };
      expect(
        getOptimalSwellScore(
          swell.height,
          swell.direction.mean,
          swell.period,
          bestSwellDirectionRanges,
          bestSwellPeriodRanges,
          optimalSurfScore,
        ),
      ).to.equal(0);
    });
  });

  describe('getOptimalWindScore', () => {
    const bestWindDirectionRanges = [
      { min: 0, max: 100 },
      { min: 300, max: 360 },
    ];

    it('should return 2 if wind is below windSpeedThreshold', () => {
      const windSpeed = 4;
      const windDirection = 150;
      expect(getOptimalWindScore(windSpeed, windDirection, bestWindDirectionRanges)).to.equal(2);
    });

    it('should return 0 if wind is >= 5 and onshore', () => {
      const windSpeed = 5;
      const windDirection = 150;
      expect(getOptimalWindScore(windSpeed, windDirection, bestWindDirectionRanges)).to.equal(0);
    });

    it('should return 2 if wind is high but offshore', () => {
      const windSpeed = 1000;
      const windDirection = 30;
      expect(getOptimalWindScore(windSpeed, windDirection, bestWindDirectionRanges)).to.equal(2);
    });

    it('should return 0 if wind is high and not offshore', () => {
      const windSpeed = 1000;
      const windDirection = 130;
      expect(getOptimalWindScore(windSpeed, windDirection, bestWindDirectionRanges)).to.equal(0);
    });
  });
});
