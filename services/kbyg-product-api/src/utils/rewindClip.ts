import moment from 'moment-timezone';

// eslint-disable-next-line default-param-last
const rewindClip = (baseUrl, zone, hour, minutes = 0, date?) => {
  if (!zone) return null;
  // Cam recording server uses east coast time
  const eastCoastZone = 'America/New_York';
  const now = moment(date || new Date()).set({
    hour,
    minute: Math.floor(minutes / 10) * 10,
  });
  const eastCoastOffset = moment.tz.zone(eastCoastZone).utcOffset(now.utcOffset());
  const localOffset = moment.tz.zone(zone).utcOffset(now.utcOffset());
  const offsetHours = (localOffset - eastCoastOffset) / 60;
  const zoneAwareDate = moment(now)
    .subtract(offsetHours >= 0 ? 1 : 0, 'd')
    .add(offsetHours, 'hours');
  const fileHour = zoneAwareDate.format('HHmm');
  const fileDate = zoneAwareDate.format('YYYY-MM-DD');
  return `${baseUrl}.${fileHour}.${fileDate}.mp4`;
};

export default rewindClip;
