import { expect } from 'chai';
import sinon from 'sinon';
import * as getUserSettings from '../external/user';
import getUnits from './getUnits';

describe('getUnits', () => {
  let getUserSettingsStub;

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    getUserSettings.default.restore();
    getUserSettingsStub = sinon.stub(getUserSettings, 'default');
  });

  afterEach(() => {
    getUserSettingsStub.restore();
  });

  it('should return units for a user ID', async () => {
    getUserSettingsStub.withArgs('582495992d06cd3b71d66220').resolves({
      units: {
        swellHeight: 'FT',
        surfHeight: 'FT',
        temperature: 'F',
        tideHeight: 'FT',
        windSpeed: 'KTS',
      },
    });
    getUserSettingsStub.resolves({
      swellHeight: 'M',
      surfHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      windSpeed: 'KPH',
    });
    const result = await getUnits('582495992d06cd3b71d66220');
    expect(result).to.deep.equal({
      swellHeight: 'FT',
      waveHeight: 'FT',
      temperature: 'F',
      tideHeight: 'FT',
      windSpeed: 'KTS',
    });
  });

  it('returns default units when get user settings errors', async () => {
    getUserSettingsStub.rejects();
    const result = await getUnits('invaliduserid');
    expect(result).to.deep.equal({
      swellHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    });
  });
});
