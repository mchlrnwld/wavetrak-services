/**
 * @description Utility function to check if an anonymous session
 * or a registered user is eligible for Premium data
 * This function returns premium true if the user type is anonymous or registered
 * and  the entitlements array has sl_metered in it.
 * @param {} user object
 */

export const isMeteredPremium = (user?) => {
  if (!user) return false;
  const { entitlements, type } = user;
  if (!entitlements || !type) return false;
  return (type === 'anonymous' || type === 'registered') && entitlements.includes('sl_metered');
};

export default isMeteredPremium;
