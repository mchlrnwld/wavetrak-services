import { expect } from 'chai';
import md5 from 'md5';
import getGravatarURL from './getGravatarURL';

describe('getGravatarURL', () => {
  it('should return gravatar URL for an email address', () => {
    const katieMd5 = md5('katie@surfline.com');
    const emptyMd5 = md5('');
    expect(getGravatarURL('katie@surfline.com')).to.equal(
      `https://www.gravatar.com/avatar/${katieMd5}?d=mm`,
    );
    expect(getGravatarURL('')).to.equal(`https://www.gravatar.com/avatar/${emptyMd5}?d=mm`);
  });
});
