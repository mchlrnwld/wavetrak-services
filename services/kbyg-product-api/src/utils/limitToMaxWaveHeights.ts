export default (data) =>
  data.map((day) => ({
    ...day,
    surf: {
      max: day.surf.max,
      min: null,
      optimalScore: null,
    },
    swells: [],
  }));
