import { expect } from 'chai';
import sinon from 'sinon';
import {
  createUTCOffsetCalculator,
  dateRange,
  fromUnixTimestamp,
  toUnixTimestamp,
  day,
  midnightOfDate,
  sunriseSunset,
} from './datetime';

describe('utils / datetime', () => {
  let clock;

  before(() => {
    clock = sinon.useFakeTimers(new Date('2021-03-01T21:44:00Z').getTime());
  });

  after(() => {
    clock.restore();
  });

  describe('dateRange', () => {
    const losAngeles = 'America/Los_Angeles';
    const santiago = 'America/Santiago';

    it('returns range of midnight of current day to n days out based on timezone', () => {
      const date = new Date('2021-02-01T22:10:00Z');
      expect(dateRange(16, losAngeles, date)).to.deep.equal({
        start: new Date('2021-02-01T08:00:00Z').getTime() / 1000,
        end: new Date('2021-02-17T08:00:00Z').getTime() / 1000,
      });
    });

    it('defaults to date to now', () => {
      expect(dateRange(16, losAngeles)).to.deep.equal({
        start: new Date('2021-03-01T08:00:00Z').getTime() / 1000,
        end: new Date('2021-03-17T07:00:00Z').getTime() / 1000,
      });
    });

    it('should return range starting and ending at midnight correctly when start date transitions from PST to PDT', () => {
      const date = new Date('2021-03-14T12:00:00Z');
      expect(dateRange(1, losAngeles, date)).to.deep.equal({
        start: new Date('2021-03-14T08:00:00Z').getTime() / 1000,
        end: new Date('2021-03-15T07:00:00Z').getTime() / 1000,
      });
    });

    it('should return range starting and ending at midnight correctly when start date transitions from PDT to PST', () => {
      const date = new Date('2021-11-07T12:00:00Z');
      expect(dateRange(1, losAngeles, date)).to.deep.equal({
        start: new Date('2021-11-07T07:00:00Z').getTime() / 1000,
        end: new Date('2021-11-08T08:00:00Z').getTime() / 1000,
      });
    });

    it('should return range starting and ending at midnight when transitioning to CLST', () => {
      const date = new Date('2021-09-04T12:00:00Z');
      expect(dateRange(3, santiago, date)).to.deep.equal({
        start: new Date('2021-09-04T04:00:00Z').getTime() / 1000,
        end: new Date('2021-09-07T03:00:00Z').getTime() / 1000,
      });
    });

    it('should return range starting and ending at midnight when transitioning from CST', () => {
      const date = new Date('2021-04-03T12:00:00Z');
      expect(dateRange(3, santiago, date)).to.deep.equal({
        start: new Date('2021-04-03T03:00:00Z').getTime() / 1000,
        end: new Date('2021-04-06T04:00:00Z').getTime() / 1000,
      });
    });
  });

  describe('fromUnixTimestamp', () => {
    it('converts seconds from 01/01/1970 to UTC Date object', () => {
      expect(fromUnixTimestamp(1493771258)).to.deep.equal(new Date('2017-05-03T00:27:38Z'));
    });
  });

  describe('toUnixTimestamp', () => {
    it('converts UTC Date object to seconds from 01/01/1970', () => {
      expect(toUnixTimestamp(new Date('2017-05-03T00:27:38Z'))).to.equal(1493771258);
    });

    it('converts local Date object to seconds from 01/01/1970', () => {
      expect(toUnixTimestamp(new Date('2017-05-02T17:27:38-07:00'))).to.equal(1493771258);
    });
  });

  describe('day', () => {
    it('returns day of date local to timezone', () => {
      expect(day('America/Los_Angeles', new Date('2017-05-03T07:27:38Z'))).to.deep.equal(
        '2017-05-03',
      );
      expect(day('America/Los_Angeles', new Date('2017-05-03T21:27:38Z'))).to.deep.equal(
        '2017-05-03',
      );
      expect(day('America/Los_Angeles', new Date('2017-05-03T01:00:00Z'))).to.deep.equal(
        '2017-05-02',
      );
    });
  });

  describe('midnightOfDate', () => {
    it('returns midnight of date local to timezone', () => {
      expect(midnightOfDate('America/Los_Angeles', new Date('2017-05-03T07:27:38Z'))).to.deep.equal(
        new Date('2017-05-03T07:00:00Z'),
      );
      expect(midnightOfDate('America/Los_Angeles', new Date('2017-05-03T21:27:38Z'))).to.deep.equal(
        new Date('2017-05-03T07:00:00Z'),
      );
      expect(midnightOfDate('America/Los_Angeles', new Date('2017-05-03T01:00:00Z'))).to.deep.equal(
        new Date('2017-05-02T07:00:00Z'),
      );
    });
  });

  describe('sunriseSunset', () => {
    it('returns sunrise and sunset times for a date local to a lat/lon', () => {
      expect(
        sunriseSunset(new Date('2017-05-03T00:27:38'), 33.654213041213, -118.0032588192),
      ).to.deep.equal({
        dawn: new Date('2017-05-02T12:36:39.019Z'),
        sunrise: new Date('2017-05-02T13:03:17.675Z'),
        sunset: new Date('2017-05-03T02:37:03.326Z'),
        dusk: new Date('2017-05-03T03:03:41.982Z'),
      });
    });
  });

  describe('createUTCOffsetCalculator', () => {
    it('creates a function from a timezone that calculates UTC offset for any given date timestamp', () => {
      const losAngelesUTCOffset = createUTCOffsetCalculator('America/Los_Angeles');
      expect(losAngelesUTCOffset(new Date('2021-03-14T09:00:00Z'))).to.equal(-8);
      expect(losAngelesUTCOffset(new Date('2021-03-14T10:00:00Z'))).to.equal(-7);

      const londonUTCOffset = createUTCOffsetCalculator('Europe/London');
      expect(londonUTCOffset(new Date('2021-03-28T00:00:00Z'))).to.equal(0);
      expect(londonUTCOffset(new Date('2021-03-28T01:00:00Z'))).to.equal(1);

      const melbourneUTCOffset = createUTCOffsetCalculator('Australia/Melbourne');
      expect(melbourneUTCOffset(new Date('2021-04-03T15:00:00Z'))).to.equal(11);
      expect(melbourneUTCOffset(new Date('2021-04-03T16:00:00Z'))).to.equal(10);
    });
  });
});
