import { round } from 'lodash';

const valueWithinRanges = (value, ranges) =>
  ranges.some((range) => value <= range.max && value >= range.min);

const getOptimalSurfScore = (surfMax, bestSurfHeightRange) => {
  if (valueWithinRanges(surfMax, bestSurfHeightRange)) {
    return 2;
  }
  return 0;
};

const deg2rad = (deg) => (deg * Math.PI) / 180;

const getSurfForSwell = (height, period) => {
  const slopeDeg = 1.7;
  const wavelength = round(1.56 * period ** 2, 4);
  const m = 1 / (1 / Math.tan(deg2rad(slopeDeg)));
  const breakingRatio = (0.34 + 2.47 * m) * (height / wavelength) ** (-0.3 + 0.88 * m);
  return height * breakingRatio;
};

// For a swell to be labeled optimal: total surf height should be within the optimal range,
// the direction of the swell should be within the optimal range and the surf produced from
// that swell over 3ft.
const getOptimalSwellScore = (
  swellHeight,
  swellDirection,
  swellPeriod,
  bestSwellDirectionRanges,
  bestSwellPeriodRanges,
  optimalSurfScore,
) => {
  // Minimum surf height from swell (ft).
  const OPTIMAL_SURF_HEIGHT_THRESHOLD = 5;
  const GOOD_SURF_HEIGHT_THRESHOLD = 3;

  const isOptimalDirection = valueWithinRanges(swellDirection, bestSwellDirectionRanges);
  const isOptimalPeriod = valueWithinRanges(swellPeriod, bestSwellPeriodRanges);
  // FYI: New line is because of the eslint line length rule.
  const isOptimalSurfHeight =
    getSurfForSwell(swellHeight, swellPeriod) >= OPTIMAL_SURF_HEIGHT_THRESHOLD;
  const isGoodSurfHeight = getSurfForSwell(swellHeight, swellPeriod) >= GOOD_SURF_HEIGHT_THRESHOLD;
  const isOptimalSurfHeightForAllSwells = optimalSurfScore === 2;

  if (
    isOptimalDirection &&
    isOptimalPeriod &&
    isOptimalSurfHeight &&
    isOptimalSurfHeightForAllSwells
  ) {
    return 2;
  }
  if (isOptimalDirection && isOptimalPeriod && isGoodSurfHeight) {
    return 1;
  }
  return 0;
};

const getOptimalWindScore = (windSpeed, windDirection, bestWindDirectionRanges) => {
  const WIND_SPEED_THRESHOLD = 5;

  if (windSpeed < WIND_SPEED_THRESHOLD) {
    return 2;
  }
  if (valueWithinRanges(windDirection, bestWindDirectionRanges)) {
    return 2;
  }
  return 0;
};

export { getOptimalSurfScore, getOptimalSwellScore, getOptimalWindScore };
