import { expect } from 'chai';
import weatherCondition from './weatherCondition';

describe('utils / weatherCondition', () => {
  it('returns absolute URL to weather condition icon served by CDN', () => {
    expect(weatherCondition('CLEAR_NO_SKIES')).to.equal('CLEAR_NO_SKIES');
    expect(weatherCondition('MOSTLY_CLOUDY_RAIN')).to.equal('MOSTLY_CLOUDY_RAIN');
  });

  it('can return night version of SVG icon', () => {
    expect(weatherCondition('CLEAR_NO_SKIES', true)).to.equal('NIGHT_CLEAR_NO_SKIES');
    expect(weatherCondition('MOSTLY_CLOUDY_RAIN', true)).to.equal('NIGHT_MOSTLY_CLOUDY_RAIN');
    expect(weatherCondition('PARTLY_CLOUDY_NO_RAIN', false)).to.equal('PARTLY_CLOUDY_NO_RAIN');
  });
});
