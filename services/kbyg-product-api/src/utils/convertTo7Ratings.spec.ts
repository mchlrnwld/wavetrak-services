import { expect } from 'chai';
import convertTo7Ratings, { convertConditionsRating } from './convertTo7Ratings';

describe('convertTo7Ratings', () => {
  describe('convertTo7Ratings', () => {
    it('converts old rating value to new rating value', () => {
      expect(convertTo7Ratings('Flat')).to.equal('VERY_POOR');
      expect(convertTo7Ratings('FLAT')).to.equal('VERY_POOR');
      expect(convertTo7Ratings('VERY_POOR')).to.equal('VERY_POOR');
      expect(convertTo7Ratings('POOR')).to.equal('POOR');
      expect(convertTo7Ratings('POOR_TO_FAIR')).to.equal('POOR_TO_FAIR');
      expect(convertTo7Ratings('FAIR')).to.equal('FAIR');
      expect(convertTo7Ratings('FAIR_TO_GOOD')).to.equal('FAIR_TO_GOOD');
      expect(convertTo7Ratings('GOOD')).to.equal('GOOD');
      expect(convertTo7Ratings('VERY_GOOD')).to.equal('GOOD');
      expect(convertTo7Ratings('GOOD_TO_EPIC')).to.equal('GOOD');
      expect(convertTo7Ratings('EPIC')).to.equal('EPIC');
    });
  });

  describe('convertConditionsRating', () => {
    const conditions = {
      human: true,
      sortableCondition: 1,
      expired: false,
    };
    it('converts conditions.value attribute to new rating value', () => {
      expect(convertConditionsRating({ ...conditions, value: null })).to.deep.equal({
        ...conditions,
        value: null,
      });
      expect(convertConditionsRating({ ...conditions, value: 'Flat' })).to.deep.equal({
        ...conditions,
        value: 'VERY_POOR',
      });
      expect(convertConditionsRating({ ...conditions, value: 'FLAT' })).to.deep.equal({
        ...conditions,
        value: 'VERY_POOR',
      });
      expect(convertConditionsRating({ ...conditions, value: 'VERY_GOOD' })).to.deep.equal({
        ...conditions,
        value: 'GOOD',
      });
      expect(convertConditionsRating({ ...conditions, value: 'GOOD_TO_EPIC' })).to.deep.equal({
        ...conditions,
        value: 'GOOD',
      });
    });
  });
});
