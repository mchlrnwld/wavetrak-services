interface Conditions {
  human: boolean;
  value: string | null;
  sortableCondition?: number;
  expired?: boolean;
}

const NEW_RATINGS_MAPPING = {
  FLAT: 'VERY_POOR',
  VERY_POOR: 'VERY_POOR',
  POOR: 'POOR',
  POOR_TO_FAIR: 'POOR_TO_FAIR',
  FAIR: 'FAIR',
  FAIR_TO_GOOD: 'FAIR_TO_GOOD',
  GOOD: 'GOOD',
  VERY_GOOD: 'GOOD',
  GOOD_TO_EPIC: 'GOOD',
  EPIC: 'EPIC',
};

const convertTo7Ratings = (rating: string) => {
  if (!rating) {
    return null;
  }
  return NEW_RATINGS_MAPPING[rating.toLocaleUpperCase()];
};

export default convertTo7Ratings;

export const convertConditionsRating = (conditions: Conditions) => {
  if (conditions && conditions.value) {
    return { ...conditions, value: convertTo7Ratings(conditions.value) };
  }
  return conditions;
};
