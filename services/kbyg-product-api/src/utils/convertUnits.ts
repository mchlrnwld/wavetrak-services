import { round, curry } from 'lodash';

export const convertFeetToMeters = (length) => length * 0.3048;
export const convertMetersToFeet = (length) => length / 0.3048;
export const convertKnotsToMPH = (speed) => speed * 1.15078;
export const convertMPHToKnots = (speed) => speed / 1.15078;
export const convertKnotsToKPH = (speed) => speed * 1.852;
export const convertKPHToKnots = (speed) => speed / 1.852;
export const convertFahrenheitToCelsius = (temp) => (temp - 32) * (5 / 9);
export const convertCelsiusToFahrenheit = (temp) => (temp * 9) / 5 + 32;

export const convertTemperatureUnits = curry((units, tempInFahrenheit) => {
  if (!tempInFahrenheit) return tempInFahrenheit;
  if (units === 'C') return round(convertFahrenheitToCelsius(tempInFahrenheit));
  return round(tempInFahrenheit);
});

export const convertWindSpeedUnits = curry((units, windSpeedInKnots) => {
  if (!windSpeedInKnots) return windSpeedInKnots;
  switch (units) {
    case 'MPH':
      return round(convertKnotsToMPH(windSpeedInKnots));
    case 'KPH':
      return round(convertKnotsToKPH(windSpeedInKnots));
    default:
      return round(windSpeedInKnots);
  }
});

export const convertSwellHeightUnits = curry((units, swellHeightInFeet) => {
  if (!swellHeightInFeet) return swellHeightInFeet;
  if (units === 'M') return round(convertFeetToMeters(swellHeightInFeet), 1);
  return round(swellHeightInFeet, 1);
});

export const convertTideHeightUnitsOld = curry((units, tideHeightInFeet) => {
  if (!tideHeightInFeet) return tideHeightInFeet;
  if (units === 'M') return round(convertFeetToMeters(tideHeightInFeet), 1);
  return round(tideHeightInFeet, 1);
});

export const convertTideHeightUnits = curry(
  (units: string, precision: number, tideHeightInMeters: number) => {
    if (!tideHeightInMeters) return tideHeightInMeters;
    if (units === 'FT') return round(convertMetersToFeet(tideHeightInMeters), precision);
    return round(tideHeightInMeters, precision);
  },
);

const convertStrArrayToMetric = (arr) => {
  if (arr.length === 1) return arr.join('');
  const arrMod = arr.map((key) => {
    if (parseInt(key, 10).toString() === key.toString()) {
      // convert to meters and round
      return round(convertFeetToMeters(parseInt(key, 10)), 1);
    }
    return key;
  });
  return arrMod.join('');
};

export const convertStringByUnits = curry((units, str) => {
  if (units !== 'M') return str;
  // create array isolating numbers
  let strMetric = str;
  const strByNumsArray = strMetric.split(/(\d+)/);
  // convert imperial nums to metric
  strMetric = convertStrArrayToMetric(strByNumsArray);
  strMetric = strMetric.replace(/ft/gi, 'm').trim();
  return strMetric;
});

export const getHeightInFeet = (unitsOfValue, value, precise = false) => {
  if (unitsOfValue === 'M') {
    const heightInFeet = convertMetersToFeet(value);

    return precise ? heightInFeet : round(heightInFeet);
  }
  return value;
};

export const getSpeedInKnots = (unitsOfValue, value) => {
  if (unitsOfValue === 'MPH') {
    return round(convertMPHToKnots(value));
  }
  if (unitsOfValue === 'KPH') {
    return round(convertKPHToKnots(value));
  }
  return value;
};
