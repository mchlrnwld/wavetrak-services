import md5 from 'md5';

const getGravatarURL = (email) => `https://www.gravatar.com/avatar/${md5(email || '')}?d=mm`;

export default getGravatarURL;
