import { expect } from 'chai';
import {
  convertWindSpeedUnits,
  convertTideHeightUnits,
  convertTideHeightUnitsOld,
  convertStringByUnits,
} from './convertUnits';

describe('convertUnits', () => {
  describe('convertWindSpeedUnits', () => {
    it('converts wind speed from knots', () => {
      expect(convertWindSpeedUnits('MPH', 10)).to.equal(12);
      expect(convertWindSpeedUnits('MPH', 20)).to.equal(23);
      expect(convertWindSpeedUnits('MPH', 30)).to.equal(35);
      expect(convertWindSpeedUnits('KPH', 10)).to.equal(19);
      expect(convertWindSpeedUnits('KPH', 20)).to.equal(37);
      expect(convertWindSpeedUnits('KPH', 30)).to.equal(56);
      expect(convertWindSpeedUnits('KTS', 10)).to.equal(10);
      expect(convertWindSpeedUnits('KTS', 10.1)).to.equal(10);
      expect(convertWindSpeedUnits('KTS', 10.5)).to.equal(11);
    });

    it('is curried', () => {
      expect(convertWindSpeedUnits('MPH')(10)).to.equal(12);
    });

    it('returns wind speed if falsey', () => {
      expect(convertWindSpeedUnits('M', 0)).to.equal(0);
      expect(convertWindSpeedUnits('M', null)).to.be.null();
      expect(convertWindSpeedUnits('M', undefined)).to.be.undefined();
    });
  });

  describe('convertTideHeightUnits', () => {
    it('converts wave height from meters', () => {
      expect(convertTideHeightUnits('FT', 1, 1)).to.equal(3.3);
      expect(convertTideHeightUnits('FT', 1, 2)).to.equal(6.6);
      expect(convertTideHeightUnits('FT', 1, 3)).to.equal(9.8);
      expect(convertTideHeightUnits('M', 1, 2.3)).to.equal(2.3);
      expect(convertTideHeightUnits('M', 1, 3.55)).to.equal(3.6);
      expect(convertTideHeightUnits('M', 1, 5.25)).to.equal(5.3);
    });

    it('converts wave height from meters with given precision', () => {
      expect(convertTideHeightUnits('FT', 1, 1)).to.equal(3.3);
      expect(convertTideHeightUnits('FT', 2, 1)).to.equal(3.28);
      expect(convertTideHeightUnits('M', 1, 2.3)).to.equal(2.3);
      expect(convertTideHeightUnits('M', 2, 3.55)).to.equal(3.55);
    });

    it('is curried', () => {
      expect(convertTideHeightUnits('FT')(1)(1)).to.equal(3.3);
    });

    it('returns tide height if falsey', () => {
      expect(convertTideHeightUnits('FT', 1, 0)).to.equal(0);
      expect(convertTideHeightUnits('FT', 1, null)).to.be.null();
      expect(convertTideHeightUnits('FT', 1, undefined)).to.be.undefined();
    });
  });

  describe('convertTideHeightUnitsOld', () => {
    it('converts wave height from feet', () => {
      expect(convertTideHeightUnitsOld('M', 1)).to.equal(0.3);
      expect(convertTideHeightUnitsOld('M', 2)).to.equal(0.6);
      expect(convertTideHeightUnitsOld('M', 3)).to.equal(0.9);
      expect(convertTideHeightUnitsOld('FT', 1.1)).to.equal(1.1);
      expect(convertTideHeightUnitsOld('FT', 1.11)).to.equal(1.1);
      expect(convertTideHeightUnitsOld('FT', 1.15)).to.equal(1.2);
    });

    it('is curried', () => {
      expect(convertTideHeightUnitsOld('M')(1)).to.equal(0.3);
    });

    it('returns tide height if falsey', () => {
      expect(convertTideHeightUnitsOld('M', 0)).to.equal(0);
      expect(convertTideHeightUnitsOld('M', null)).to.be.null();
      expect(convertTideHeightUnitsOld('M', undefined)).to.be.undefined();
    });
  });

  describe('convertStringByUnits', () => {
    it('should convert feet to meters', () => {
      expect(convertStringByUnits('M')('8 ft overhead')).to.equal('2.4 m overhead');
      expect(convertStringByUnits('FT')('8 ft overhead')).to.equal('8 ft overhead');
      expect(convertStringByUnits('M')('4-5 ft')).to.equal('1.2-1.5 m');
      expect(convertStringByUnits('FT')('4-5 ft')).to.equal('4-5 ft');
    });
  });
});
