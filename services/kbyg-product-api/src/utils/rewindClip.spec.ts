import { expect } from 'chai';
import rewindClip from './rewindClip';

describe('rewindClip', () => {
  it('creates correct rewind url for Grandview', () => {
    const utcStart = '2017-02-05T18:20:00';
    const rewindBase = 'https://camrewinds.cdn-surfline.com/grandviewcam/grandviewcam';
    expect(rewindClip(rewindBase, 'America/Los_Angeles', 10, 0, utcStart)).to.equal(
      `${rewindBase}.1300.2017-02-04.mp4`,
    );
  });
  it('creates correct rewind url for Lincoln Blvd, Long Beach', () => {
    const utcStart = '2017-02-05T18:20:00';
    const rewindBase = 'https://camrewinds.cdn-surfline.com/lincolncam/lincolncam';
    expect(rewindClip(rewindBase, 'America/New_York', 10, 0, utcStart)).to.equal(
      `${rewindBase}.1000.2017-02-04.mp4`,
    );
  });
  it('creates correct rewind url for Padang Padang', () => {
    const utcStart = '2017-02-05T18:20:00';
    const rewindBase = 'https://camrewinds.cdn-surfline.com/padangcam/padangcam';
    expect(rewindClip(rewindBase, 'Asia/Dili', 10, 0, utcStart)).to.equal(
      `${rewindBase}.2000.2017-02-04.mp4`,
    );
  });
});
