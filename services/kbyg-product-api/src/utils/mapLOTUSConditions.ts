export type LOTUSCondition = {
  humanRelation: string;
  hawaiian: { minHeight: number; maxHeight: number; plus: boolean };
};

const mapLOTUSConditions = (minHeight: number, maxHeight: number): LOTUSCondition => {
  switch (true) {
    case maxHeight <= 0.5:
      return {
        humanRelation: 'Flat',
        hawaiian: {
          minHeight: 0,
          maxHeight: 0,
          plus: false,
        },
      };
    case maxHeight <= 1:
      return {
        humanRelation: 'Shin to knee',
        hawaiian: {
          minHeight: 0,
          maxHeight: 0.5,
          plus: false,
        },
      };
    case maxHeight <= 2:
      return {
        humanRelation: 'Knee to thigh',
        hawaiian: {
          minHeight: 0.5,
          maxHeight: 1,
          plus: false,
        },
      };
    case maxHeight <= 3:
      return {
        humanRelation: 'Thigh to waist',
        hawaiian: {
          minHeight: 1,
          maxHeight: 2,
          plus: false,
        },
      };
    case maxHeight <= 3.5:
      return {
        humanRelation: 'Thigh to stomach',
        hawaiian: {
          minHeight: 1,
          maxHeight: 2,
          plus: true,
        },
      };
    case maxHeight <= 4:
      return {
        humanRelation: 'Waist to chest',
        hawaiian: {
          minHeight: 1,
          maxHeight: 3,
          plus: false,
        },
      };
    case maxHeight <= 4.5:
      return {
        humanRelation: 'Waist to shoulder',
        hawaiian: {
          minHeight: 1,
          maxHeight: 3,
          plus: false,
        },
      };
    case maxHeight <= 5 && minHeight <= 3.5:
      return {
        humanRelation: 'Waist to head',
        hawaiian: {
          minHeight: 1,
          maxHeight: 3,
          plus: false,
        },
      };
    case maxHeight <= 5:
      return {
        humanRelation: 'Chest to head',
        hawaiian: {
          minHeight: 2,
          maxHeight: 3,
          plus: false,
        },
      };
    case maxHeight <= 6:
      return {
        humanRelation: 'Chest to overhead',
        hawaiian: {
          minHeight: 2,
          maxHeight: 3,
          plus: false,
        },
      };
    case maxHeight <= 6.5:
      return {
        humanRelation: 'Chest to 1ft overhead',
        hawaiian: {
          minHeight: 2,
          maxHeight: 3,
          plus: true,
        },
      };
    case maxHeight <= 7:
      return {
        humanRelation: 'Head to 2ft overhead',
        hawaiian: {
          minHeight: 2,
          maxHeight: 4,
          plus: false,
        },
      };
    case maxHeight <= 8 && minHeight <= 5.5:
      return {
        humanRelation: 'Head to well overhead',
        hawaiian: {
          minHeight: 3,
          maxHeight: 4,
          plus: false,
        },
      };
    case maxHeight <= 8:
      return {
        humanRelation: 'Overhead to well overhead',
        hawaiian: {
          minHeight: 3,
          maxHeight: 4,
          plus: true,
        },
      };
    case maxHeight <= 10 && minHeight <= 6.5:
      return {
        humanRelation: 'Head to 2x overhead',
        hawaiian: {
          minHeight: 3,
          maxHeight: 5,
          plus: false,
        },
      };
    case maxHeight <= 10:
      return {
        humanRelation: '2x overhead',
        hawaiian: {
          minHeight: 4,
          maxHeight: 5,
          plus: false,
        },
      };
    case maxHeight <= 12:
      return {
        humanRelation: '2x overhead',
        hawaiian: {
          minHeight: 4,
          maxHeight: 6,
          plus: false,
        },
      };
    case maxHeight <= 15:
      return {
        humanRelation: '2-3x overhead',
        hawaiian: {
          minHeight: 6,
          maxHeight: 8,
          plus: false,
        },
      };
    case maxHeight <= 18:
      return {
        humanRelation: '3x overhead',
        hawaiian: {
          minHeight: 6,
          maxHeight: 8,
          plus: true,
        },
      };
    case maxHeight <= 20:
      return {
        humanRelation: '3-4x overhead',
        hawaiian: {
          minHeight: 8,
          maxHeight: 10,
          plus: false,
        },
      };
    case maxHeight <= 25:
      return {
        humanRelation: 'XL',
        hawaiian: {
          minHeight: 10,
          maxHeight: 12,
          plus: false,
        },
      };
    case maxHeight <= 30:
      return {
        humanRelation: 'XL',
        hawaiian: {
          minHeight: 12,
          maxHeight: 15,
          plus: false,
        },
      };
    case maxHeight <= 35:
      return {
        humanRelation: 'XXL',
        hawaiian: {
          minHeight: 15,
          maxHeight: 18,
          plus: false,
        },
      };
    case maxHeight <= 40:
      return {
        humanRelation: 'XXL',
        hawaiian: {
          minHeight: 15,
          maxHeight: 20,
          plus: false,
        },
      };
    default:
      return {
        humanRelation: 'XXL',
        hawaiian: {
          minHeight: 20,
          maxHeight: 25,
          plus: false,
        },
      };
  }
};

export default mapLOTUSConditions;
