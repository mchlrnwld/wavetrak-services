import getUserSettings from '../external/user';
import { METRIC, IMPERIAL, HAWAII } from '../common/constants';

/**
 * @param {string} userId
 * @param {string} geoCountryIso
 * @param {import('newrelic')} [newrelic=undefined]
 */
const getUnits = async (userId, geoCountryIso?, newrelic = null) => {
  try {
    const userSettings = await getUserSettings(userId, geoCountryIso);
    return {
      temperature: userSettings.units.temperature,
      tideHeight: userSettings.units.tideHeight,
      // temp default value for swellHeight
      swellHeight: userSettings.units.swellHeight,
      waveHeight: userSettings.units.surfHeight,
      windSpeed: userSettings.units.windSpeed,
    };
  } catch (err) {
    if (newrelic) {
      newrelic.noticeError(err);
    }
    return {
      swellHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    };
  }
};

export const getWaveHeightUnitsFromQuery = (unitQuery) => {
  switch (unitQuery) {
    case METRIC:
      return 'M';
    case HAWAII:
      return 'HI';
    case IMPERIAL:
    default:
      return 'FT';
  }
};

export default getUnits;
