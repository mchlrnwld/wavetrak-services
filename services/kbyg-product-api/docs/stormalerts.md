# Storm Alerts

Returns all the active Storm alerts for a user based on his Favorites. This endpoint requires an access token to be passed in. The response contains a "data" object with an array of objects where each object is associated to a Storm alert. If there is no access token passed in or if the user do not have any favorites then an empty array is returned.


## Sample Request

```
GET /favorites/storms HTTP/1.1
Host: server.example.com
```
## Sample Authentication Header

| Key           | Value                    | Description      |
| ------------- | ------------------------ | ---------------- |
| X-Auth-UserId | 582495992d06cd3b71d66229 | User's Mongo ID. |

## Sample Response

```JSON
{
  "data": {
    "storms": [
      {
        "id": "1tNeXiRBBGkGcqmGKOAEck",
        "createdAt": "2017-10-22T21:33:17.727Z",
        "name": "Test Event by Ajith",
        "stormCategory": "significantSwell",
        "summary": "Test Summary by Ajith",
        "banner": "https://images.contentful.com/zxy8mg9a81nc/1IarxQjMJSaMCMeq0U4Wai/5dc30d1291ae23cc20d80bf84584647e/matthew_ThuAM.gif",
        "location": {
          "basins": [
            "2"
          ],
          "countries": []
        },
        "taxonomies": [
          {
            "id": "58f7ed5ddadb30820bb39651",
            "name": "Orange County",
            "type": "geoname",
            "breadCrumbs": [
              "United States",
              "California"
            ]
          },
          {
            "id": "58f7ed5ddadb30820bb39689",
            "name": "North Orange County",
            "type": "subregion",
            "breadCrumbs": null
          },
          {
            "id": "5908d78edadb30820b3ba228",
            "name": "England",
            "type": "region",
            "breadCrumbs": null
          }
        ]
      },
      {
        "id": "6sA55S2VwIWcccagOG6QCy",
        "createdAt": "2017-10-18T18:23:46.421Z",
        "name": "Test Event by Drew",
        "stormCategory": "tropicalStorm1",
        "summary": "Test Summary 13 .     ",
        "banner": "https://images.contentful.com/zxy8mg9a81nc/6lxXyiR6hysw20448OmQoU/1fee5617e78b4e7bca2d26d93dd21e93/snow-jonas.jpg",
        "location": {
          "basins": [
            "1"
          ],
          "countries": []
        },
        "taxonomies": [
          {
            "id": "5908d78edadb30820b3ba228",
            "name": "England",
            "type": "region",
            "breadCrumbs": null
          },
          {
            "id": "58f7ed5ddadb30820bb39689",
            "name": "North Orange County",
            "type": "subregion",
            "breadCrumbs": null
          }
        ]
      }
    ]
  }
}
```
