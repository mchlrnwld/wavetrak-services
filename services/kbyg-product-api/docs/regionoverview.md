# Subregion Overview

Forecast overview for a subregion.
Includes summary data for the subregion, including highlights, next forecast date and forecaster info.  Returns breadcrumb data and all the spots that are in the subregion.

## Request Query Parameters

| Parameters | Type   | Description                                                                                           | Example                  | Required |
| ---------- | ------ | ----------------------------------------------------------------------------------------------------- | ------------------------ | -------- |
| subregionId   | string | Subregion's Mongo ID.                                                                                 | 58581a836630e24c44878fd6 | Yes      |
| spotId   | string | A spotId for that region (Mongo ID).                                                                                 | 5842041f4e65fad6a77088ed | No      |

* subregionId IS required to return data, but a spotId can be passed in here to do a lookup on the subregionId if not known (so technically subregionId is NOT required, but one of these values is to return data).  passing a spotID essentially, just finds the subregionId and executes the code as if a subregionId was provided.

## Sample Request

```
HTTP/1.1 GET /regions/overview?subregionId=58581a836630e24c44878fd6
```

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight":"FT",
      "waveHeight":"FT",
      "windSpeed":"KTS"
    },
    "chartsUrl":"/surf-charts/north-america/southern-california/north-san-diego/2144",
    "advertising": {
      "spotId": null,
      "subregionId": 2137
    },
    "abbrTimezone": "PDT",
    "utcOffset": -7
  },
  "data": {
    "name": "North Orange County",
    "primarySpot":"5842041f4e65fad6a77088b7",
    "breadcrumb": [
      {
        "name": "United States",
        "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001"
      },
      {
        "name": "Orange County",
        "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524"
      },
      {
        "name": "California",
        "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921"
      }
    ],
    "timestamp": 1515777540,
    "forecastSummary": {
      "forecastStatus": {
        "status": "active",
        "inactiveMessage": ""
      },
      "nextForecastTimestamp": 1515877200,
      "forecaster": {
        "name": "Super Admin",
        "title": "Development Employee",
        "iconUrl": "https://www.gravatar.com/avatar/212a0bc3f4da04290f3e54bac53f7597?d=mm"
      },
      "highlights": [
        "Newest Highlight for north orange county going for all the characters......",
        "Next highlight here the second of three with a lot of characters as well.",
        "Finally the third and final highlight waves are lookin pretty good out here"
      ]
    },
    "bets": {
      "best": "A good bet",
      "worst": "A less good bet?"
    },
    "hasHighlights": true,
    "hasBets": true,
    "spots": [
      {
        "_id": "5842041f4e65fad6a77088f2",
        "name": "Newport Point",
        "conditions": {
          "human": false,
          "value": null
        },
        "waveHeight": {
          "human": false,
          "min": 0.2,
          "max": 0.5,
          "occasional": null,
          "humanRelation": null,
          "plus": false
        },
        "lat": 33.605165797643664,
        "lon": -117.92500376701355
      },
      {
        "_id": "584204214e65fad6a7709cde",
        "name": "Lower Jetties",
        "conditions": {
          "human": false,
          "value": null
        },
        "waveHeight": {
          "human": false,
          "min": 0.2,
          "max": 0.5,
          "occasional": null,
          "humanRelation": null,
          "plus": false
        },
        "lat": 33.61288593141895,
        "lon": -117.93608665466309
      },
      {
        "_id": "5842041f4e65fad6a770882c",
        "name": "Newport Beach Pier",
        "conditions": {
          "human": false,
          "value": null
        },
        "waveHeight": {
          "human": false,
          "min": 0,
          "max": 0.2,
          "occasional": null,
          "humanRelation": null,
          "plus": false
        },
        "lat": 33.60577342582,
        "lon": -117.9315376281
      }
    ]
  }
}
```

### SPECIAL NOTE ###
When forecastSummary data is NOT available for a particular subregion, sane defaults are provided for the forecastSummary node in the response, as well as the main timestamp value.  Below is an example of how the contract will differ for that node when no forecastSummary data is available.
Things to note:  

#### main timestamp
* timeStamp will be null

#### forecastSummary object
* forecastStatus will be "off"
* forecaster will be null
* nextForecastTimeStamp will be null
* highlights will be a blank array

```

"timestamp": null,
"forecastSummary": {
  "forecastStatus": {
    "status": "off",
    "inactiveMessage": ""
  },
  "nextForecastTimestamp": null,
  "forecaster": null,
  "highlights": []
}

```
