# Buoy Details

Returns buoy station for given buoy id.

## Request Route Parameters

| Endpoint | Type   | Required | Description | Example                                |
| -------- | ------ | -------- | ----------- | -------------------------------------- |
| buoyId   | string | Yes      | Buoy UUID   | "9897640a-cecd-11eb-a2c9-024238d3b313" |

## Sample Request

```
HTTP/1.1 GET /buoys/details/9897640a-cecd-11eb-a2c9-024238d3b313
```

## Sample Response

```JSON
{
  "associated": {
    "abbrTimezone": "PDT"
  },
  "data": {
    "id": "9897640a-cecd-11eb-a2c9-024238d3b313",
    "source": "NDBC",
    "sourceId": "46053",
    "name": "EAST SANTA BARBARA  - 12NM Southwest of Santa Barbara, CA",
    "status": "ONLINE",
    "latitude": 34.241,
    "longitude": -119.839,
    "latestTimestamp": 1626450600,
    "utcOffset": -7
  }
}
```
