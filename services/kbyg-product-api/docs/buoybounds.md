# Buoy In Bounds

Returns list of buoy stations for given lat/lon boundaries.

## Request Query Parameters

| Parameters | Type    | Required | Description                                 | Example |
| ---------- | ------- | -------- | ------------------------------------------- | ------- |
| north      | integer | Yes      | North boundary (must be between -90 and 90) | 60      |
| south      | integer | Yes      | South boundary (must be between -90 and 90) | -2      |
| east       | integer | Yes      | East boundary                               | 10      |
| west       | integer | Yes      | West boundary                               | 20      |

## Sample Request

```
HTTP/1.1 GET /buoys/bounds?north=60&south=10&east=120&west=150
```

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "C",
      "tideHeight": "M",
      "swellHeight": "M",
      "waveHeight": "M",
      "windSpeed": "KPH"
    }
  },
  "data": [
    {
      "id": "7f5b875a-ca13-11eb-a34f-0242015eddd9",
      "name": "MONTEREY - 27NM WNW of Monterey, CA",
      "sourceId": "46042",
      "latitude": 36.785,
      "longitude": -122.398,
      "status": "ONLINE",
      "abbrTimezone": "PDT",
      "latestData": {
        "timestamp": 1626825000,
        "height": 1.9,
        "period": 8,
        "direction": 326,
        "swells": [
          {
            "height": 1.40807,
            "period": 7.69231,
            "direction": 330
          },
          {
            "height": 1.21001,
            "period": 4.7619,
            "direction": 330
          },
          {
            "height": 0.39017,
            "period": 14.70588,
            "direction": 265
          },
          {
            "height": null,
            "period": null,
            "direction": null
          },
          {
            "height": null,
            "period": null,
            "direction": null
          },
          {
            "height": null,
            "period": null,
            "direction": null
          }
        ],
        "utcOffset": -7
      }
    },
    {
      "id": "d96cf568-ca14-11eb-bf7e-0242015eddd9",
      "name": "Point Santa Cruz, CA (254)",
      "latitude": 36.934,
      "longitude": -122.034,
      "status": "ONLINE",
      "abbrTimezone": "PDT",
      "latestData": {
        "timestamp": 1626825600,
        "height": 1,
        "period": 15,
        "direction": 203,
        "swells": [
          {
            "height": 5.00783,
            "period": 8.33333,
            "direction": 250
          },
          {
            "height": 1.00254,
            "period": 5.55556,
            "direction": 270
          },
          {
            "height": 0.43601,
            "period": 15.38462,
            "direction": 200
          },
          {
            "height": 0.2051,
            "period": 2.17391,
            "direction": 265
          },
          {
            "height": null,
            "period": null,
            "direction": null
          },
          {
            "height": null,
            "period": null,
            "direction": null
          }
        ],
        "utcOffset": -7
      }
    }
  ]
}
```
