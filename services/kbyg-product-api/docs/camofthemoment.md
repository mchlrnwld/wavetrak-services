# Cam of the Moment

Spot Report View related to the currently set Cam of the Moment in the CMS.

## Sample Request

```
HTTP/1.1 GET /cotm
```

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "C",
      "tideHeight": "M",
      "waveHeight": "M",
      "windSpeed": "KPH"
    }
  },
  "data": {
    "spot": {
      "_id": "5842041f4e65fad6a7708941",
      "legacyRegionId": 2959,
      "lat": 36.69388179265737,
      "lon": -121.8128918258394,
      "name": "Marina State Beach",
      "cameras": [
        {
          "_id": "58349c5f3421b20545c4b559",
          "title": "Marina State Beach",
          "streamUrl": "https://wowzaprod3-i.akamaihd.net/hls/live/253362/b868b7cc/playlist.m3u8",
          "stillUrl": "https://camstills.cdn-surfline.com/marinasbcam/latest_small_pixelated.png",
          "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/marinasbcam/marinasbcam",
          "status": {
            "isDown": false,
            "message": ""
          }
        }
      ],
      "conditions": {
        "human": false,
        "value": null
      },
      "wind": {
        "speed": 14,
        "direction": 235.91
      },
      "waveHeight": {
        "human": false,
        "min": 0.6,
        "max": 1,
        "occasional": null,
        "humanRelation": null,
        "plus": false
      },
      "tide": {
        "previous": {
          "type": "LOW",
          "height": -0.1,
          "timestamp": 1496251277,
          "utcOffset": -7
        },
        "current": {
          "type": "NORMAL",
          "height": 0.7,
          "timestamp": 1496264857,
          "utcOffset": -7
        },
        "next": {
          "type": "HIGH",
          "height": 1.4,
          "timestamp": 1496276966,
          "utcOffset": -7
        }
      },
      "legacyId": 4957,
      "subregionId": "58581a836630e24c44879012",
      "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/5842041f4e65fad6a7708941/5842041f4e65fad6a7708941_1500.jpg",
      "rank": 1,
      "slug": "DEPRECATED",
      "waterTemp": {
        "min": 14,
        "max": 15
      },
      "weather": {
        "temperature": 19,
        "condition": "CLEAR_NO_RAIN"
      }
    }
  }
}
```
