# Nearby Spots by SpotId

All spots in the same subregion as the spot ID provided.

## Request Query Parameters

| Parameters | Type   | Description     | Example                  |
| ---------- | ------ | --------------- | ------------------------ |
| spotId     | string | Spot's Mongo ID | 5842041f4e65fad6a77088ed |

## Sample Request

```
HTTP/1.1 GET /spots/nearby?spotId=5842041f4e65fad6a77088ed
```

## Response Nullable Fields
| Path                                     | Explanation                                                  |
| ---------------------------------------- | ------------------------------------------------------------ |
| `data.spots[i].conditions.value`         | LOTUS spots do not have human reported conditions             |
| `data.spots[i].waveHeight.occasional`    | LOTUS spots do not have human reported occasional wave height |
| `data.spots[i].waveHeight.humanRelation` | LOTUS spots do not have human reported wave human relation    |
| `data.spots[i].tide`                     | Some spots do not have nearby tide stations                  |

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "windSpeed": "KPH",
      "waveHeight": "M",
      "tideHeight": "M"
    },
  },
  "data": {
    "spots": [
      {
        "_id": "5842041f4e65fad6a7708820",
        "lat": 33.737902112055,
        "lon": -118.1086921691,
        "name": "Seal Beach",
        "cameras": [
          {
            "title": "Seal Beach",
            "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-sealbeachcam.smil/playlist.m3u8",
            "stillUrl": "http://camstills.cdn-surfline.com/sealbeachcam/latest_small.jpg",
            "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/sealbeachcam/sealbeachcam",
            "_id": "589a610034a7e10012595b52",
            "status": {
              "isDown": false,
              "message": ""
            }
          }
        ],
        "conditions": {
          "human": true,
          "value": "POOR"
        },
        "wind": {
          "speed": 5.75,
          "direction": 244.6
        },
        "waveHeight": {
          "human": true,
          "min": 0.3,
          "max": 0.9,
          "occasional": null
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": -0.3,
            "timestamp": 1486502712,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 1.0,
            "timestamp": 1486502612,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 1.2,
            "timestamp": 1486525649,
            "utcOffset": -8
          }
        },
        "legacyId": 4217,
        "rank": 0,
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg"
      },
      {
        "_id": "5842041f4e65fad6a77088e4",
        "lat": 33.7368459504689,
        "lon": -118.1034243408604,
        "name": "South Side/13th Street",
        "cameras": [
          {
            "title": "South Side/13th Street",
            "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-sealbeachsscam.smil/playlist.m3u8",
            "stillUrl": "http://camstills.cdn-surfline.com/sealbeachsscam/latest_small.jpg",
            "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/sealbeachsscam/sealbeachsscam",
            "_id": "589a61db34a7e10012595c10",
            "status": {
              "isDown": false,
              "message": ""
            }
          }
        ],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 5.4,
          "direction": 242.66
        },
        "waveHeight": {
          "human": false,
          "min": 0.3,
          "max": 0.6,
          "occasional": null
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": -0.3,
            "timestamp": 1486502712,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 1.0,
            "timestamp": 1486502612,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 1.2,
            "timestamp": 1486525649,
            "utcOffset": -8
          }
        },
        "legacyId": 4865,
        "rank": 1,
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg"
      },
      ...
      {
        "_id": "5842041f4e65fad6a7708f21",
        "lat": 33.56489,
        "lon": -117.83585,
        "name": "Crystal Cove",
        "cameras": [],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 3.51,
          "direction": 216.45
        },
        "waveHeight": {
          "human": false,
          "min": 0.6,
          "max": 0.9,
          "occasional": null
        },
        "tide": null,
        "rank": 24,
        "subregionId": "58581a836630e24c44878fd6",
        "legacyId": 46525,
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg"
      }
    ]
  }
}
```

# Nearby Spots Geo Based

All spots within a specified distance of provided lat, lon values (with optional abilityLevels filter).

## Request Query Parameters

| Parameters    | Type           | Required | Description                               | Example        |
| ------------- | -------------- | -------- | ----------------------------------------- | -------------- |
| lat           |    decimal     |   yes    | Latitude                                  | 33.55          |
| lon           |    decimal     |   yes    | Latitude                                  | -117.222       |
| distance      |    decimal     |   no     | Distance (miles)                          | 10             |
| limit         |    integer     |   no     | Limit results.                            | 10 (results)   |
| offset        |    integer     |   no     | for pagination (used with limit)          | 0 (start on "page 1") |
| abilityLevels |  string (list) |   no     | filter by ability level (comma sep list)  | BEGINNER       |
| boardTypes    |  string (list) |   no     | filter by board types (comma sep list)    | SHORTBOARD      |
| select        |  string (list) |   no     | filter by board types (comma sep list)    | BEGINNER       |

### abilityLevels

| OPTIONS      |  Notes                                                                       |
| ------------ | ---------------------------------------------------------------------------- |
| BEGINNER     | Beginner level                                                               |
| INTERMEDIATE | Intermediate level                                                           |
| ADVANCED     | Advanced level                                                               |
| PRO          | Pro level (not currently being set via CMS, but available as a filter)       |

Example usage would be `&abilityLevels=BEGINNER,INTERMEDIATE` and would return *any* spots that have been saved with abilityLevels of *either* BEGINNER, INTERMEDIATE (or both).
These values do not return as part of the response, and are simply used as filtering options.

### boardTypes

| OPTIONS     |  Notes                              |
| ------------ | ---------------------------------- |
| SHORTBOARD  | SHORTBOARD board type perference    |
| FISH        | FISH board type perference          |
| FUNBOARD    | FUNBOARD board type perference      |
| LONGBOARD   | LONGBOARD board type perference     |
| GUN         | GUN board type perference           |
| TOW         | TOW board type perference           |
| SUP         | SUP board type perference           |
| FOIL        | FOIL board type perference          |
| SKIMMING    | SKIMMING board type perference      |
| BODYBOARD   | BODYBOARD board type perference     |
| BODYSURFING | BODYSURFING board type perference   |
| KITEBOARD   | KITEBOARD board type perference     |

Example usage would be `&boardTypes=LONGBOARD,FUNBOARD` and would return *any* spots that have been saved with boardTypes of *either* LONGBOARD, FUNBOARD (or both)
These values do not return as part of the response, and are simply used as filtering options.

### select
This applies the the values coming back within the `data.spots` array.
You can specify the values you would like returned and exclude all others in order to reduce the size of the response.  There are 3 values that will always be returned which are `_id`, `name` and `rank`, otherwise you can limit the response for each `data.spots` element to the values you pass as a comma seprated list.  See example below where it uses `&select=tides,cameras,conditions`

## Sample Request

```
HTTP/1.1 GET /spots/nearbygeo?lat=33.66&lon=-117.222&distance=10&abilityLevels=BEGINNER&boardTypes=FISH&select=tides,cameras,conditions
```

## Response Nullable Fields
| Path                                     | Explanation                                                  |
| ---------------------------------------- | ------------------------------------------------------------ |
| `data.spots[i].conditions.value`         | LOTUS spots do not have human reported conditions             |
| `data.spots[i].waveHeight.occasional`    | LOTUS spots do not have human reported occasional wave height |
| `data.spots[i].waveHeight.humanRelation` | LOTUS spots do not have human reported wave human relation    |
| `data.spots[i].tide`                     | Some spots do not have nearby tide stations                  |

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "windSpeed": "KPH",
      "waveHeight": "M",
      "tideHeight": "M"
    },
  },
  "data": {
    "spots": [
      {
        "_id": "5842041f4e65fad6a7708820",
        "lat": 33.737902112055,
        "lon": -118.1086921691,
        "name": "Seal Beach",
        "cameras": [
          {
            "title": "Seal Beach",
            "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-sealbeachcam.smil/playlist.m3u8",
            "stillUrl": "http://camstills.cdn-surfline.com/sealbeachcam/latest_small.jpg",
            "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/sealbeachcam/sealbeachcam",
            "_id": "589a610034a7e10012595b52",
            "status": {
              "isDown": false,
              "message": ""
            }
          }
        ],
        "conditions": {
          "human": true,
          "value": "POOR"
        },
        "wind": {
          "speed": 5.75,
          "direction": 244.6
        },
        "waveHeight": {
          "human": true,
          "min": 0.3,
          "max": 0.9,
          "occasional": null
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": -0.3,
            "timestamp": 1486502712,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 1.0,
            "timestamp": 1486502612,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 1.2,
            "timestamp": 1486525649,
            "utcOffset": -8
          }
        },
        "legacyId": 4217,
        "rank": 0,
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg"
      },
      {
        "_id": "5842041f4e65fad6a77088e4",
        "lat": 33.7368459504689,
        "lon": -118.1034243408604,
        "name": "South Side/13th Street",
        "cameras": [
          {
            "title": "South Side/13th Street",
            "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-sealbeachsscam.smil/playlist.m3u8",
            "stillUrl": "http://camstills.cdn-surfline.com/sealbeachsscam/latest_small.jpg",
            "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/sealbeachsscam/sealbeachsscam",
            "_id": "589a61db34a7e10012595c10",
            "status": {
              "isDown": false,
              "message": ""
            }
          }
        ],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 5.4,
          "direction": 242.66
        },
        "waveHeight": {
          "human": false,
          "min": 0.3,
          "max": 0.6,
          "occasional": null
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": -0.3,
            "timestamp": 1486502712,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 1.0,
            "timestamp": 1486502612,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 1.2,
            "timestamp": 1486525649,
            "utcOffset": -8
          }
        },
        "legacyId": 4865,
        "rank": 1,
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg"
      },
      ...
      {
        "_id": "5842041f4e65fad6a7708f21",
        "lat": 33.56489,
        "lon": -117.83585,
        "name": "Crystal Cove",
        "cameras": [],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 3.51,
          "direction": 216.45
        },
        "waveHeight": {
          "human": false,
          "min": 0.6,
          "max": 0.9,
          "occasional": null
        },
        "tide": null,
        "rank": 24,
        "subregionId": "58581a836630e24c44878fd6",
        "legacyId": 46525,
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg"
      }
    ]
  }
}
```
