# Spot Forecasts (DEPRECATED)

Wave, wind, weather, and tide forecast data for a spot. Premium users can receive up to a 17 day hourly forecast. Non-premium users can receive up to a 3 day 3-hourly forecast.

## Request Query Parameters

| Parameters | Type   | Description                                                                                                                                                                                     | Example                  | Required |
| ---------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ | -------- |
| spotId     | string | Spot's Mongo ID.                                                                                                                                                                                | 5842041f4e65fad6a77088ed | Yes      |
| start      | number | Start date/time for forecast request in Unix timestamp format. Forecast will start at time step nearest requested start time. Defaults to midnight of current day local to the spot's timezone. | 1486172917               | No       |
| end        | number | End date/time for forecast request in Unix timestamp format. Defaults to return as much data as the user can get starting from midnight of the current day local to the spot's timezone.        | 1487638117               | No       |

## Sample Request

```
HTTP/1.1 GET /spots/forecasts?spotId=5842041f4e65fad6a77088ed&start=1486172917&end=1487638117
X-Auth-UserId: 582495992d06cd3b71d66229
```

## Response Nullable Fields
| Path                | Explanation                                 |
| ------------------- | ------------------------------------------- |
| `data.tideLocation` | Some spots do not have nearby tide stations |
| `data.tides`        | Some spots do not have nearby tide stations |

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8,
    "weatherIconPath": "https://wa.cdn-surfline.com/quiver/0.1.0/weathericons"
  },
  "data": {
    "sunriseSunsetTimes": [
      {
        "midnight": 1493708400,
        "sunrise": 1493730197,
        "sunset": 1493779023
      },
      {
        "midnight": 1493794800,
        "sunrise": 1493816540,
        "sunset": 1493865470
      },
      {
        "midnight": 1493881200,
        "sunrise": 1493902883,
        "sunset": 1493951917
      }
    ],
    "tideLocation": {
      "name": "Huntington Cliffs near Newport Bay",
      "min": -3.54,
      "max": 9.06,
      "lon": -117.998,
      "lat": 33.6603,
      "mean": 2.76
    },
    "forecasts": [
      {
        "timestamp": 1493708400,
        "weather": {
          "temperature": 74.1,
          "condition": "CLEAR_NO_RAIN"
        },
        "wind": {
          "speed": 4.01,
          "direction": 151.94
        },
        "surf": {
          "min": 5.58,
          "max": 6.56
        },
        "swells": [
          {
            "height": 2.3,
            "direction": 180,
            "period": 15
          },
          {
            "height": 5.58,
            "direction": 270,
            "period": 11
          },
          {
            "height": 0.66,
            "direction": 188.44,
            "period": 5
          },
          {
            "height": 0,
            "direction": 0,
            "period": 0
          },
          {
            "height": 0.66,
            "direction": 178.59,
            "period": 10
          },
          {
            "height": 0,
            "direction": 0,
            "period": 0
          }
        ]
      },
      {
        "timestamp": 1493719200,
        "weather": {
          "temperature": 74.1,
          "condition": "CLEAR_NO_RAIN"
        },
        "wind": {
          "speed": 3.73,
          "direction": 149.06
        },
        "surf": {
          "min": 5.58,
          "max": 6.56
        },
        "swells": [
          {
          "height": 2.3,
          "direction": 180,
          "period": 15
          },
          {
          "height": 5.58,
          "direction": 270,
          "period": 11
          },
          {
          "height": 0.66,
          "direction": 188.44,
          "period": 5
          },
          {
          "height": 0,
          "direction": 0,
          "period": 0
          },
          {
          "height": 0.66,
          "direction": 178.59,
          "period": 10
          },
          {
          "height": 0,
          "direction": 0,
          "period": 0
          }
        ]
      },
      ...
      {
        "timestamp": 1493956800,
        "weather": {
          "temperature": 75.7,
          "condition": "CLEAR_NO_RAIN"
        },
        "wind": {
          "speed": 7.2,
          "direction": 3.67
        },
        "surf": {
          "min": 3.28,
          "max": 3.61
        },
        "swells": [
          {
            "height": 1.64,
            "direction": 178.59,
            "period": 11
          },
          {
            "height": 3.28,
            "direction": 265.78,
            "period": 8
          },
          {
            "height": 1.97,
            "direction": 185.63,
            "period": 6
          },
          {
            "height": 0.33,
            "direction": 285.47,
            "period": 15
          },
          {
            "height": 0,
            "direction": 0,
            "period": 0
          },
          {
            "height": 0,
            "direction": 0,
            "period": 0
          }
        ]
      }
    ],
    "tides": [
      {
        "timestamp": 1493704800,
        "type": "HIGH",
        "height": 3.08
      },
      {
        "timestamp": 1493708400,
        "type": "NORMAL",
        "height": 2.57
      },
      ...
      {
        "timestamp": 1493967600,
        "type": "LOW",
        "height": -0.39
      }
    ]
  }
}
```
