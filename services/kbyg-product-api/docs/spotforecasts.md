# Spot Forecasts

Base Url: https://services.surfline.com/kbyg/

Endpoints for conditions, tides, wave, weather, and wind forecasts. Premium
users can receive up to 17 days of forecasts. Non-premium users can receive up
to 3 days of forecasts.

For the deprecated Spot Forecasts endpoint:
[Spot Forecasts (DEPRECATED)](spotforecasts_old.md)

## Request Query Parameters

| Parameter       | Type       | Description                                       | Example                    | Default     |
| --------------- | ---------- | ------------------------------------------------- | -------------------------- | ----------- |
| `spotId`        | `ObjectId` | The spotId to fetch forecast data for.            | `5842041f4e65fad6a77088ed` | Required    |
| `days`          | `Number`   | Number of days to get forecast data for.          | `5`                        | `3` or `17` |
| `intervalHours` | `Number`   | Interval between data points returned (in hours). | `2`                        | `1`         |

## Response Associated

All endpoints return an `associated` property with `units` and `utcOffset`.

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8
  }
  ...
}
```

## Endpoints

* [Conditions](#conditions)
* [Tides](#tides)
* [Wave](#wave)
* [Weather](#weather)
* [Wind](#wind)

### Conditions

Returns condition data for the provided number of days.

### Sample Request

```http
GET /spots/forecasts/conditions?spotId=5842041f4e65fad6a77088ed&days=3&offset=1
```

The response will contain an array of conditions under `data.conditions`. Each
day is represented by an index in the array.

#### `data.conditions` Response Properties

* `timestamp` (Number) Returns the unix timstamp of current day in the spots
* `utcOffset` (Number) UTC offset of the timezone of at the at the time of the the forecast.
* `human` (Boolean) `true` if the conditions are observed by a forecaster,
  `false` if conditions are computed from LOTUS.
* `forecaster` (Object, nullable) An object containing forecaster details. This
  object will be null if LOTUS conditions are returned.
* `forecaster.name` (String) The name of the forecaster.
* `forecaster.avatar` (Url) A Url linking to the forecaster image.
* `observation` (String) Prose string containing current conditions for the day.
* `am|pm.maxHeight` (Number) The max surf height.
* `am|pm.minHeight` (Number) The min surf height
* `am|pm.humanRelation` (String) Surf height in relation to a human.
* `am|pm.plus` (Boolean) `false` Show a + sign in the UI next to wave heights.
* `am|pm.occasionalHeight` (Number, nullable) The occasional surf height. This
  value will be `null` if LOTUS conditions are returned.
* `am|pm.rating` (Enum) Current surf conditions. This string is an enum that
  maps to
  [https://github.com/Surfline/surfline-services/blob/master/services/kbyg-product-api/src/model/conditions.js#L220](https://github.com/Surfline/surfline-services/blob/master/services/kbyg-product-api/src/model/conditions.js#L220).
  ### Tides

Tides forecast data. The response returns all data for the `days` provided,
ignoring `intervalHours`. The `associated` property includes a `tideLocation`.

#### Sample Request

```
HTTP/1.1 GET
/spots/forecasts/tides?spotId=5842041f4e65fad6a77088ed&days=1
```

#### Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8,
    "tideLocation": {
      "name": "Huntington Cliffs near Newport Bay",
      "min": -3.54,
      "max": 9.06,
      "lon": -117.998,
      "lat": 33.6603,
      "mean": 2.76
    }
  },
  "data": [
    {
      "timestamp": 1512460800,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 3.58
    },
    {
      "timestamp": 1512464400,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 2.82
    },
    {
      "timestamp": 1512468000,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 2.13
    },
    {
      "timestamp": 1512471600,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 1.77
    },
    {
      "timestamp": 1512472529,
      "utcOffset": -8,
      "type": "LOW",
      "height": 1.77
    },
    {
      "timestamp": 1512475200,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 1.87
    },
    {
      "timestamp": 1512478800,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 2.56
    },
    {
      "timestamp": 1512482400,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 3.64
    },
    {
      "timestamp": 1512486000,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 4.86
    },
    {
      "timestamp": 1512489600,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 5.97
    },
    {
      "timestamp": 1512493200,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 6.63
    },
    {
      "timestamp": 1512495122,
      "utcOffset": -8,
      "type": "HIGH",
      "height": 6.69
    },
    {
      "timestamp": 1512496800,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 6.63
    },
    {
      "timestamp": 1512500400,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 5.97
    },
    {
      "timestamp": 1512504000,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 4.63
    },
    {
      "timestamp": 1512507600,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 2.92
    },
    {
      "timestamp": 1512511200,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 1.18
    },
    {
      "timestamp": 1512514800,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": -0.26
    },
    {
      "timestamp": 1512518400,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": -1.15
    },
    {
      "timestamp": 1512521110,
      "utcOffset": -8,
      "type": "LOW",
      "height": -1.35
    },
    {
      "timestamp": 1512522000,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": -1.31
    },
    {
      "timestamp": 1512525600,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": -0.82
    },
    {
      "timestamp": 1512529200,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 0.2
    },
    {
      "timestamp": 1512532800,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 1.44
    },
    {
      "timestamp": 1512536400,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 2.69
    },
    {
      "timestamp": 1512540000,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 3.61
    },
    {
      "timestamp": 1512543600,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 4.07
    },
    {
      "timestamp": 1512544881,
      "utcOffset": -8,
      "type": "HIGH",
      "height": 4.1
    },
    {
      "timestamp": 1512547200,
      "utcOffset": -8,
      "type": "NORMAL",
      "height": 4
    }
  ]
}
```

#### No Tide Station

If the spot in the above request did not have an assigned tide station, the following response would have been returned instead

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8,
    "tideLocation": {
      "name": "None",
      "min": 0.0,
      "max": 0.0,
      "lon": 0.0,
      "lat": 0.0,
      "mean": 0.0,
    }
  },
  "data": []
}
```

### Wave

Wave forecast data. The `associated` property includes a `location`.

#### Sample Request

```
HTTP/1.1 GET
/spots/forecasts/wave?spotId=5842041f4e65fad6a77088ed&days=1&intervalHours=8
```

#### Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8,
    "location": {
      "lon": -118.0032588192,
      "lat": 33.654213041213
    },
    "forecastLocation": {
      "lon": -118.007,
      "lat": 33.656
    }
  },
  "data": [
    {
      "timestamp": 1512460800,
      "utcOffset": -8,
      "surf": {
        "min": 2.3,
        "max": 3.28,
        "optimalScore": 0,
      },
      "swells": [
        {
          "height": 0.98,
          "direction": 278.44,
          "period": 8,
          "optimalScore": 1,
        },
        {
          "height": 0.66,
          "direction": 189.84,
          "period": 16,
          "optimalScore": 0,
        },
        {
          "height": 0.66,
          "direction": 270,
          "period": 12,
          "optimalScore": 0,
        },
        {
          "height": 0,
          "direction": 0,
          "period": 0,
          "optimalScore": 0,
        }
      ]
    },
    {
      "timestamp": 1512489600,
      "utcOffset": -8,
      "surf": {
        "min": 3.28,
        "max": 3.94,
        "optimalScore": 2,
      },
      "swells": [
        {
          "height": 0.66,
          "direction": 282.66,
          "period": 9,
          "optimalScore": 0,
        },
        {
          "height": 0.98,
          "direction": 188.44,
          "period": 16,
          "optimalScore": 0,
        },
        {
          "height": 0.33,
          "direction": 270,
          "period": 12,
          "optimalScore": 0,
        },
        {
          "height": 0,
          "direction": 0,
          "period": 0,
          "optimalScore": 0,
        }
      ]
    },
    {
      "timestamp": 1512518400,
      "utcOffset": -8,
      "surf": {
        "min": 3.28,
        "max": 3.94,
        "optimalScore": 2,
      },
      "swells": [
        {
          "height": 1.31,
          "direction": 46.41,
          "period": 3,
          "optimalScore": 2,
        },
        {
          "height": 1.31,
          "direction": 188.44,
          "period": 15,
          "optimalScore": 0,
        },
        {
          "height": 0.98,
          "direction": 347.34,
          "period": 3,
          "optimalScore": 0,
        },
        {
          "height": 0,
          "direction": 0,
          "period": 0,
          "optimalScore": 0,
        }
      ]
    }
  ]
}
```

### Weather

Wave forecast data. The `associated` property includes a `weatherIconPath`.

#### Sample Request

```
HTTP/1.1 GET
/spots/forecasts/weather?spotId=5842041f4e65fad6a77088ed&days=1&intervalHours=8
```

#### Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8,
    "weatherIconPath": "https://wa.cdn-surfline.com/quiver/0.1.0/weathericons"
  },
  "data": {
    "sunlightTimes": [
      {
        "midnight": 1512460800,
        "dawn": 1512483374,
        "sunrise": 1512485015,
        "sunset": 1512521098,
        "dusk": 1512522740
      }
    ],
    "weather": [
      {
        "timestamp": 1512460800,
        "utcOffset": -8,
        "temperature": 62.6,
        "condition": "NIGHT_MOSTLY_CLOUDY_NO_RAIN"
      },
      {
        "timestamp": 1512489600,
        "utcOffset": -8,
        "temperature": 59.9,
        "condition": "CLEAR_NO_RAIN"
      },
      {
        "timestamp": 1512518400,
        "utcOffset": -8,
        "temperature": 66.7,
        "condition": "CLEAR_NO_RAIN"
      }
    ]
  }
}
```

### Wind

Wave forecast data.

#### Sample Request

```
HTTP/1.1 GET
/spots/forecasts/wind?spotId=5842041f4e65fad6a77088ed&days=1&intervalHours=8
```

#### Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    },
    "utcOffset": -8,
  },
  "data": [
    {
      "timestamp": 1512460800,
      "utcOffset": -8,
      "speed": 16.49,
      "direction": 53.05,
      "gust": 20,
      "optimalScore": 0
    },
    {
      "timestamp": 1512489600,
      "utcOffset": -8,
      "speed": 14.83,
      "direction": 59,
      "gust": 20,
      "optimalScore": 0
    },
    {
      "timestamp": 1512518400,
      "utcOffset": -8,
      "speed": 10.77,
      "direction": 36.93,
      "gust": 20,
      "optimalScore": 2
    }
  ]
}
```
