
# Popular Spots

Latest spot report for curated group of "popular" spots, along with political locations for the spot to use for bread crumbs.

## Sample Request

```
HTTP/1.1 GET /spots/popular
```

## Response Nullable Fields
| Path                                | Explanation                                                  |
| ----------------------------------- | ------------------------------------------------------------ |
| `forecast.conditions.value`         | LOTUS spots do not have human reported conditions             |
| `forecast.waveHeight.occasional`    | LOTUS spots do not have human reported occasional wave height |
| `forecast.waveHeight.humanRelation` | LOTUS spots do not have human reported wave human relation    |
| `forecast.tide`                     | Some spots do not have nearby tide stations                  |
| `report`                            | Spot may not have a recent report                            |

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    }
  },
  "data": {
    "popularSpots": [
      {
        "_id": "5842041f4e65fad6a7708890",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/5842041f4e65fad6a7708890/5842041f4e65fad6a7708890_1500.jpg",
        "rank": 15,
        "subregionId": "58581a836630e24c44878fcb",
        "lat": 21.66464735075164,
        "lon": -158.0519986152649,
        "name": "Pipeline",
        "tide": {
          "previous": {
            "type": "LOW",
            "height": 0.3,
            "timestamp": 1551370620,
            "utcOffset": -10
          },
          "current": {
            "type": "NORMAL",
            "height": 0.3,
            "timestamp": 1551376713,
            "utcOffset": -10
          },
          "next": {
            "type": "HIGH",
            "height": 0.5,
            "timestamp": 1551390240,
            "utcOffset": -10
          }
        },
        "waterTemp": {
          "min": 73,
          "max": 75
        },
        "cameras": [
          {
            "_id": "58349eed3421b20545c4b56c",
            "title": "HI - Pipeline",
            "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/hi-pipeline/playlist.m3u8",
            "stillUrl": "https://camstills.cdn-surfline.com/hi-pipeline/latest_small.jpg",
            "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/hi-pipeline/hi-pipeline",
            "isPremium": false,
            "alias": "hi-pipeline",
            "status": {
              "isDown": false,
              "message": "",
              "altMessage": ""
            },
            "control": "https://camstills.cdn-surfline.com/hi-pipeline/latest_small.jpg",
            "nighttime": false,
            "rewindClip": "https://camrewinds.cdn-surfline.com/hi-pipeline/hi-pipeline.1500.2019-02-27.mp4"
          },
          {
            "_id": "58349ef6e411dc743a5d52cc",
            "title": "HI - Pipeline Overview",
            "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/hi-pipelinefixed/playlist.m3u8",
            "stillUrl": "https://camstills.cdn-surfline.com/hi-pipelinefixed/latest_small.jpg",
            "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/hi-pipelinefixed/hi-pipelinefixed",
            "isPremium": true,
            "alias": "hi-pipelinefixed",
            "status": {
              "isDown": false,
              "message": "",
              "altMessage": ""
            },
            "control": "https://camstills.cdn-surfline.com/hi-pipelinefixed/latest_small.jpg",
            "nighttime": false,
            "rewindClip": "https://camrewinds.cdn-surfline.com/hi-pipelinefixed/hi-pipelinefixed.1500.2019-02-27.mp4"
          },
          {
            "_id": "58a373b5c9d273fd4f581be2",
            "title": "HI - Backdoor",
            "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/hi-backdoor/playlist.m3u8",
            "stillUrl": "https://camstills.cdn-surfline.com/hi-backdoor/latest_small.jpg",
            "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/hi-backdoor/hi-backdoor",
            "isPremium": false,
            "alias": "hi-backdoor",
            "status": {
              "isDown": false,
              "message": "",
              "altMessage": ""
            },
            "control": "https://camstills.cdn-surfline.com/hi-backdoor/latest_small.jpg",
            "nighttime": false,
            "rewindClip": "https://camrewinds.cdn-surfline.com/hi-backdoor/hi-backdoor.1500.2019-02-27.mp4"
          },
          {
            "_id": "58349581e411dc743a5d526b",
            "title": "HI - Backdoor Overview",
            "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/hi-backdoorfixed/playlist.m3u8",
            "stillUrl": "https://camstills.cdn-surfline.com/hi-backdoorfixed/latest_small.jpg",
            "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/hi-backdoorfixed/hi-backdoorfixed",
            "isPremium": false,
            "alias": "hi-backdoorfixed",
            "status": {
              "isDown": false,
              "message": "",
              "altMessage": ""
            },
            "control": "https://camstills.cdn-surfline.com/hi-backdoorfixed/latest_small.jpg",
            "nighttime": false,
            "rewindClip": "https://camrewinds.cdn-surfline.com/hi-backdoorfixed/hi-backdoorfixed.1500.2019-02-27.mp4"
          }
        ],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 12,
          "direction": 15.56
        },
        "weather": {
          "temperature": 64,
          "condition": "PARTLY_CLOUDY_NO_RAIN"
        },
        "waveHeight": {
          "human": false,
          "min": 10,
          "max": 15,
          "occasional": null,
          "humanRelation": "10-15 ft. – 2-3 times overhead",
          "plus": false
        },
        "legacyId": 4750,
        "legacyRegionId": 2132,
        "timezone": "Pacific/Honolulu",
        "saveFailures": 0,
        "swells": [
          {
            "height": 2.6,
            "direction": 19.69,
            "period": 11,
            "_id": "5c78214c6ca4d45d9b5851e2"
          },
          {
            "height": 8.4,
            "direction": 315,
            "period": 14,
            "_id": "5c78214c6ca4d421e55851e1"
          },
          {
            "height": 7.6,
            "direction": 344.53,
            "period": 11,
            "_id": "5c78214c6ca4d43eee5851e0"
          },
          {
            "height": 5.4,
            "direction": 29.53,
            "period": 9,
            "_id": "5c78214c6ca4d44fe65851df"
          },
          {
            "height": 0,
            "direction": 0,
            "period": 0,
            "_id": "5c78214c6ca4d47e925851de"
          },
          {
            "height": 0,
            "direction": 0,
            "period": 0,
            "_id": "5c78214c6ca4d462c55851dd"
          }
        ],
        "offshoreDirection": 315,
        "parentTaxonomy": [
          "58f7ed51dadb30820bb38782",
          "58f7ed51dadb30820bb38791",
          "58f7ed51dadb30820bb3879c",
          "5908c7e5dadb30820b27dc1a",
          "59a87e22dadb30820b1a4bf3",
          "58f7ed87dadb30820bb3c50d",
          "58f7ed87dadb30820bb3c51d",
          "58f7ed87dadb30820bb3c537"
        ],
        "slug": "DEPRECATED"
      }
    ]
  }
}
```
