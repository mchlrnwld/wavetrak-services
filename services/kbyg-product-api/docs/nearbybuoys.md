# Nearby Buoys

Returns list of nearby buoy stations for given lat/lon coordinates.

## Request Query Parameters

| Parameters | Type  | Required | Description                           | Example |
| ---------- | ----- | -------- | ------------------------------------- | ------- |
| latitude   | float | Yes      | Location Latitude                     | 33.62   |
| longitude  | float | Yes      | Location Longitude                    | -118    |
| distance   | float | Optional | Radius distance in km                 | 160     |
| limit      | float | Optional | Max number of buoy stations to return | 4       |

## Sample Request

```
HTTP/1.1 GET /buoys/nearby?latitude=33.62&longitude=-118
```

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "C",
      "tideHeight": "M",
      "swellHeight": "M",
      "waveHeight": "M",
      "windSpeed": "KPH"
    }
  },
  "data": [
    {
      "id": "b18a5966-cecc-11eb-9d2e-024238d3b313",
      "name": "Del Mar Nearshore, CA (153)",
      "sourceId": "46042",
      "latitude": 32.957,
      "longitude": -117.279,
      "status": "ONLINE",
      "abbrTimezone": "PDT",
      "latestData": {
        "timestamp": 1626364800,
        "height": 0.5,
        "period": 14,
        "direction": 233,
        "swells": [
          {
            "height": 4.50561,
            "period": 10.52632,
            "direction": 230
          },
          {
            "height": 0.7227,
            "period": 6.25,
            "direction": 275
          },
          {
            "height": null,
            "period": null,
            "direction": null
          },
          {
            "height": null,
            "period": null,
            "direction": null
          },
          {
            "height": null,
            "period": null,
            "direction": null
          },
          {
            "height": null,
            "period": null,
            "direction": null
          }
        ],
        "utcOffset": -7
      }
    },
    ...
  ]
}
```
