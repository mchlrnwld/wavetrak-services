# Subregion Forecasts

## Conditions

Condition forecast data. This endpoint is the same as [spot condition forecasts](spotforecasts.md#conditions) except that it takes a `subregionId` and returns conditions based on the offshore swell for the subregion.

## Wave

Wave forecast data. This endpoint is the same as [spot wave forecasts](spotforecasts.md#wave) except that it takes a `subregionId` and returns the offshore swell for the subregion.

## DEPRECATED Forecasts

Forecasts for a subregion. Premium users receive 7 days forecast. Non-premium users receive 3 days forecast.

### *Note this endpoint will be deprecated as soon as mobile releases using overview endpoint and older app adoption rate drops to low levels

## Request Query Parameters

| Parameters | Type   | Description                                                                                           | Example                  | Required |
| ---------- | ------ | ----------------------------------------------------------------------------------------------------- | ------------------------ | -------- |
| subregionId   | string | Subregion's Mongo ID.                                                                                 | 58581a836630e24c44878fd6 | Yes      |
| spotId   | string | A spotId for that region (Mongo ID).                                                                                 | 5842041f4e65fad6a77088ed | No      |

* subregionId IS required to return data, but a spotId can be passed in here to do a lookup on the subregionId if not known (so technically subregionId is NOT required, but one of these values is to return data).  passing a spotID essentially, just finds the subregionId and executes the code as if a subregionId was provided.

## Sample Request

```
HTTP/1.1 GET /regions/forecasts?subregionId=58581a836630e24c44878fd6
X-Auth-UserId: 582495992d06cd3b71d66229
```

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight":"FT",
      "waveHeight":"FT",
      "windSpeed":"KTS"
    },
    "chartsUrl":"/surf-charts/north-america/southern-california/north-san-diego/2144",
    "advertising": {
      "spotId": null,
      "subregionId": 2137
    },
    "analytics": {
      "spotId": null,
      "subregionId": 2137
    },
    "utcOffset": -7
  },
  "units": {
    "waveHeight": "M"
  },
  "data": {
    "entitlements": [],
    "breadcrumb": [
      {
        "name": "United States",
        "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001"
      },
      {
        "name": "Orange County",
        "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524"
      },
      {
        "name": "California",
        "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921"
      }
    ],
  "reports": [
      {
        "forecastDate": 1494442800,
        "pm": null,
        "am": {
          "humanRelation": "waist to shoulder high",
          "maxHeight": 1.2192,
          "minHeight": 0.9144000000000001,
          "condition": "12",
          "occasionalHeight": null,
          "rating": "fair to good"
        },
        "observation": ""
      },
      {
        "forecastDate": 1494442800,
        "pm": null,
        "am": {
          "humanRelation": "knee to shoulder high",
          "maxHeight": 1.2192,
          "minHeight": 0.6096,
          "condition": "10",
          "occasionalHeight": null,
          "rating": "fair"
        },
        "observation": ""
      },
      {
        "forecastDate": 1494442800,
        "pm": null,
        "am": {
          "humanRelation": "ankle to waist high",
          "maxHeight": 0.9144000000000001,
          "minHeight": 0.3048,
          "condition": "4",
          "occasionalHeight": null,
          "rating": "poor to fair"
        },
        "observation": ""
      }
    ],
    "subregionName": "North Orange County",
    "primarySpot":"5842041f4e65fad6a77088b7",
    "subregionSpots": [
      {
        "id": "5842041f4e65fad6a7708820",
        "name": "Seal Beach",
        "status": "PUBLISHED",
        "location": {
          "type": "Point",
          "coordinates": [-118.1086921691, 33.737902112055]
        },
        "forecastLocation": {
          "type": "Point",
          "coordinates": [-118.112, 33.736]
        }
      },
      {
        "id": "5842041f4e65fad6a77088e4",
        "name": "South Side/13th Street",
        "status": "PUBLISHED",
        "location": {
          "type": "Point",
          "coordinates": [-118.1034243408604, 33.7368459504689]
        },
        "forecastLocation": {
          "type": "Point",
          "coordinates": [-118.105, 33.734]
        }
      },
      ...
      {
        "id": "5842041f4e65fad6a7708f21",
        "name": "Crystal Cove",
        "status": "PUBLISHED",
        "location": {
          "type": "Point",
          "coordinates": [-117.83585, 33.56489]
        },
        "forecastLocation": {
          "type": "Point",
          "coordinates": [-117.836, 33.565]
        }
      }
    ],
    "timestamp": 1494442800,
    "utcOffset": -8,
    "forecastStatus": {
      "status": "active",
      "inactiveMessage": "Inactive status message for North Orange County."
    },
    "forecaster": {
      "name": "Johnny Tsunami",
      "iconUrl": "https://www.gravatar.com/avatar/212a0bc3f4da04290f3e54bac53f7597?d=mm",
      "title": "Chief Forecaster",
    },
    "highlights": [
      "WNW swell eases through the day",
      "Better breaks in waist-head high range",
      "Most size in the AM at top HB breaks"
    ],
    "nextForecastTimeStamp": 1494442800,
    "mapBounds": {
      "north": 33.740402112054994,
      "south": 33.56239,
      "east": -117.83335,
      "west": -118.1111921691
    },
  }
}
```
