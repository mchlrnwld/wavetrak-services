# Buoy Report

Returns buoy report(s) for given buoy id and number of days.

## Request Route Parameters

| Endpoint | Type   | Required | Description | Example                                |
| -------- | ------ | -------- | ----------- | -------------------------------------- |
| buoyId   | string | Yes      | Buoy UUID   | "9897640a-cecd-11eb-a2c9-024238d3b313" |

## Request Query Parameters

| Parameters | Type    | Required | Description                                                                          | Example |
| ---------- | ------- | -------- | ------------------------------------------------------------------------------------ | ------- |
| days       | integer | Optional | Number of day's worth of hourly buoy reports to retrieve (between 1 and 3 inclusive) | 3       |

## Sample Request

```
HTTP/1.1 GET /buoys/report/9897640a-cecd-11eb-a2c9-024238d3b313?days=1
```

## Sample Response

```JSON
{
    "associated": {
        "abbrTimezone": "PDT",
        "units": {
            "temperature": "C",
            "tideHeight": "M",
            "swellHeight": "M",
            "waveHeight": "M",
            "windSpeed": "KPH"
        }
    },
    "data": [
        {
            "timestamp": 1626821400,
            "height": 1.1,
            "period": 8,
            "direction": 262,
            "waterTemperature": 18.9,
            "airTemperature": 17.1,
            "dewPoint": 15.6,
            "pressure": 1014,
            "wind": {
                "gust": 32.4,
                "speed": 25.2,
                "direction": 260
            },
            "swells": [
                {
                    "height": 0.86171,
                    "period": 3.7037,
                    "direction": 270
                },
                {
                    "height": 0.63794,
                    "period": 7.69231,
                    "direction": 270
                },
                {
                    "height": 0.17301,
                    "period": 2.35294,
                    "direction": 260
                },
                {
                    "height": null,
                    "period": null,
                    "direction": null
                },
                {
                    "height": null,
                    "period": null,
                    "direction": null
                },
                {
                    "height": null,
                    "period": null,
                    "direction": null
                }
            ],
            "utcOffset": -7
        }
    ]
}
```
