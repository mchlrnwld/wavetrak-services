# Spot Reports

Latest forecaster report for a spot along with political locations for the spot to use for bread crumbs.

## Request Query Parameters

| Parameters | Type   | Description     | Example                  |
| ---------- | ------ | --------------- | ------------------------ |
| spotId     | string | Spot's Mongo ID | 5842041f4e65fad6a77088ed |

## Sample Request

```
HTTP/1.1 GET /spots/reports?spotId=5842041f4e65fad6a77088ed
```

## Response Nullable Fields
| Path                                | Explanation                                                  |
| ----------------------------------- | ------------------------------------------------------------ |
| `forecast.conditions.value`         | LOTUS spots do not have human reported conditions             |
| `forecast.waveHeight.occasional`    | LOTUS spots do not have human reported occasional wave height |
| `forecast.waveHeight.humanRelation` | LOTUS spots do not have human reported wave human relation    |
| `forecast.tide`                     | Some spots do not have nearby tide stations                  |
| `report`                            | Spot may not have a recent report                            |

## Response DEPRECATED Fields
| Path    | Explanation                    |
| ------- | ------------------------------ |
| `units` | Use `associated.units` instead |

## Sample Response

```JSON
{
  "associated": {
    "units": {
      "windSpeed": "KPH",
      "waveHeight": "M",
      "tideHeight": "M"
    },
    "advertising": {
      "spotId": "1234",
      "subregionId": "4321",
      "sst": "66-70",
    },
    "analytics": {
      "spotId": "1234",
      "subregionId": "4321",
    },
    "weatherIconPath": "https://wa.cdn-surfline.com/quiver/0.1.0/weathericons",
    "utcOffset": -8
  },
  "spot": {
    "name": "HB Pier, Southside",
    "breadCrumbs": [
      "United States",
      "California",
      "Orange County"
    ],
    "lat": 33.654213041213,
    "lon": -118.0032588192,
    "cameras": [
      {
        "title": "HB Pier, Southside",
        "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-hbpiersscam.smil/playlist.m3u8",
        "stillUrl": "http://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg",
        "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/hbpiersscam/hbpiersscam",
        "_id": "58a1075574fbf700121e9fc1",
        "status": {
          "isDown": false,
          "message": ""
        }
      }
    ],
    "subregion": {
      "_id": "58581a836630e24c44878fd6",
      "forecastStatus": "off"
    }
  },
  "forecast": {
    "conditions": {
      "human": true,
      "value": "POOR"
    },
    "wind": {
      "speed": 2.09,
      "direction": 249.88
    },
    "waveHeight": {
      "human": true,
      "min": 0.9,
      "max": 1.2,
      "occasional": null,
      "humanRelation": "waist to shoulder high"
    },
    "tide": {
      "previous": {
        "type": "LOW",
        "height": -0.2,
        "timestamp": 1486945280,
        "utcOffset": -8
      },
      "current": {
            "type": "HIGH",
            "height": 1.4,
            "timestamp": 1486968170,
            "utcOffset": -8
          },
      "next": {
        "type": "HIGH",
        "height": 1.4,
        "timestamp": 1486968170,
        "utcOffset": -8
      }
    },
    "waterTemp": {
      "min": 20,
      "max": 21
    },
    "weather": {
      "temperature": 73.8,
      "condition": "CLEAR_NO_RAIN"
    }
  },
  "report": {
    "timestamp": 1486935145,
    "forecaster": {
      "name": "Kurt Korte",
      "iconUrl": "https://www.gravatar.com/avatar/f425192c4a3c092fb603914c0c2d90f0?d=mm",
      "title": "Lead Forecaster"
    },
    "body": "<p><strong>Afternoon Report for North OC: </strong>Winds have switched out of the WNW this afternoon, putting good amount of bump/chop on the water. Old Westerly swell mix fades for mainly 3-4' jumbled waves at top spots. Overall, pretty dismal out there due to this onshore flow. Dropping tide.&nbsp;</p>\n<p><br></p>\n<p><br>\n<strong>Short-Term Forecast for North OC:</strong> Tide bottoms out at a -.6' low at 4:20pm, draining things late afternoon.&nbsp;Winds continue out of the WNW-NW direction, topping out at moderate+ levels. Expect smaller surf by the evening with the combination of drained tide.</p>\n<p>&nbsp;&nbsp;<br>\n<strong>Health Department warns against ocean contact during and within 72 hours after a significant rain event.<br>\n<br>\n</strong>Until further notice we will provide human observed reports for the following spots only:&nbsp;<a href=\"http://www.surfline.com/surf-report/seal-beach-southern-california_4217/\" target=\"_blank\">Seal Beach</a>,&nbsp;<a href=\"http://www.surfline.com/surf-report/bolsa-chica-overview-southern-california_103685/\" target=\"_blank\">Bolsa Chica</a>,&nbsp;<a href=\"http://www.surfline.com/surf-report/goldenwest-southern-california_4870/\" target=\"_blank\">Golden West</a>,&nbsp;<a href=\"http://www.surfline.com/surf-report/hb-pier-southside-southern-california_4874/\" target=\"_blank\">HB Pier</a>,&nbsp;<a href=\"http://www.surfline.com/surf-report/56th-street-newport-southern-california_43103/\" target=\"_blank\">54th-56th St</a>&nbsp;and&nbsp;<a href=\"http://www.surfline.com/surf-report/blackies-southern-california_53412/\" target=\"_blank\">Blac</a><a href=\"http://www.surfline.com/surf-report/blackies-southern-california_53412/\" target=\"_blank\">kies</a>. The surf heights you see for all other spots are generated by our computer swell model.</p>"
  }
}
```
