# Map View

## Map View Bounding Box

Returns SpotReportViews within a bounding box.

### Request Query Parameters

| Parameters | Type   | Description                                                                                                                                        | Example     | Required |
| ---------- | ------ | -------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- | -------- |
| north      | float  | North latitude for map boundary. Precise to 6 decimal points. Values outside of range [-90, 90] will be clamped to -90/90. i.e. 100 → 90           | 33.672556   | Yes        |
| south      | float  | South latitude for map boundary. Precise to 6 decimal points. Values outside of range [-90, 90] will be clamped to -90/90. i.e. -100 → -90         | 33.650731   | Yes        |
| east       | float  | East longitude for map boundary. Precise to 6 decimal points. Values outside of range [-180, 180] will be wrapped to the interval. i.e. -190 → 170 | -117.972804 | Yes        |
| west       | float  | West longitude for map boundary. Precise to 6 decimal points. Values outside of range [-180, 180] will be wrapped to the interval. i.e. 190 → -170 | -118.027435 | Yes        |
| units      | string | Determines whether to sends heights in feet or meters. Valid options are "m" or "e". Defaults to "e".                                              | m           | No         |
| camsOnly | bool      | Determines whether to return only spots with cams. Defaults to false.                                                                             | true        | No       |

### Sample Request

```
HTTP/1.1 GET /mapview?south=33.650731&west=-118.027435&north=33.672556&east=-117.972804&units=e
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Response Nullable Fields
| Path                                     | Explanation                                                  |
| ---------------------------------------- | ------------------------------------------------------------ |
| `data.spots[i].conditions.value`         | LOTUS spots do not have human reported conditions             |
| `data.spots[i].waveHeight.occasional`    | LOTUS spots do not have human reported occasional wave height |
| `data.spots[i].waveHeight.humanRelation` | LOTUS spots do not have human reported wave human relation    |
| `data.spots[i].tide`                     | Some spots do not have nearby tide stations                  |

### Sample Response

```JSON
{
  "associated": {
    "units": {
      "waveHeight": "FT",
      "windSpeed": "KTS",
      "tideHeight": "FT"
    },
  },
  "data": {
    "regionalForecast": {
      "iconUrl": "https://somecdn.surfline.com/region1.png",
      "legacyRegionId": 2143,
      "subregionId": "58581a836630e24c44878fd6"
    },
    "subregions": [
      {
        "_id": "58581a836630e24c44878fd6",
        "subregion": {
          "name": "North Orange County",
          "id": "58581a836630e24c44878fd6",
          "forecasterEmail": "jherndon@surfline.com"
        }
      }
    ],
    "spots": [
      {
        "_id": "5842041f4e65fad6a77088ea",
        "lat": 33.66704400178788,
        "lon": -118.0192012569068,
        "name": "Goldenwest",
        "cameras": [
          {
            "title": "Goldenwest",
            "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-goldenwestcam.smil/playlist.m3u8",
            "stillUrl": "http://camstills.cdn-surfline.com/goldenwestcam/latest_small.jpg",
            "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/goldenwestcam/goldenwestcam",
            "_id": "5894f5fbb6f192001292d87d",
            "status": {
              "isDown": false,
              "message": ""
            }
          }
        ],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 3.1,
          "direction": 168.38
        },
        "waveHeight": {
          "human": false,
          "min": 5,
          "max": 6,
          "occasional": null,
          "humanRelation": "waist to shoulder high"
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": 1.4,
            "timestamp": 1486139669,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 2.2,
            "timestamp": 1486148600,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 3.1,
            "timestamp": 1486160190,
            "utcOffset": -8
          }
        },
        "legacyId": 4870,
        "rank": [0, 0],
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg",
        "abilityLevels": ["BEGINNER"],
        "boardTypes": ["SHORTBOARD"],
        "relivableRating": 3
      },
      {
        "_id": "5842041f4e65fad6a77088eb",
        "lat": 33.66315613358258,
        "lon": -118.0143556189923,
        "name": "17th Street",
        "cameras": [],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 3.08,
          "direction": 169.42
        },
        "waveHeight": {
          "human": false,
          "min": 6,
          "max": 7,
          "occasional": null
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": 1.4,
            "timestamp": 1486139669,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 2.2,
            "timestamp": 1486148600,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 3.1,
            "timestamp": 1486160190,
            "utcOffset": -8
          }
        },
        "legacyId": 4871,
        "rank": [0, 1],
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg",
        "abilityLevels": ["BEGINNER"],
        "boardTypes": ["SHORTBOARD"],
        "relivableRating": 3
      },
      ...
      {
        "_id": "5842041f4e65fad6a77088ed",
        "lat": 33.654213041213,
        "lon": -118.0032588192,
        "name": "HB Pier, Southside",
        "cameras": [
          {
            "title": "HB Pier, Southside",
            "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-hbpiersscam.smil/playlist.m3u8",
            "stillUrl": "http://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg",
            "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/hbpiersscam/hbpiersscam",
            "_id": "5894f5dcb6f192001292d838",
            "status": {
              "isDown": false,
              "message": ""
            }
          }
        ],
        "conditions": {
          "human": false,
          "value": null
        },
        "wind": {
          "speed": 4.17,
          "direction": 157.45
        },
        "waveHeight": {
          "human": false,
          "min": 6,
          "max": 7,
          "occasional": null
        },
        "tide": {
          "previous": {
            "type": "LOW",
            "height": 1.4,
            "timestamp": 1486139669,
            "utcOffset": -8
          },
          "current": {
            "type": "NORMAL",
            "height": 2.2,
            "timestamp": 1486148600,
            "utcOffset": -8
          },
          "next": {
            "type": "HIGH",
            "height": 3.1,
            "timestamp": 1486160190,
            "utcOffset": -8
          }
        },
        "legacyId": 4874,
        "rank": [0, 3],
        "subregionId": "58581a836630e24c44878fd6",
        "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg",
        "abilityLevels": ["BEGINNER"],
        "boardTypes": ["SHORTBOARD"],
        "relivableRating": 3
      }
    ]
  }
}
```

## Nearest SpotReportView

Returns SpotReportView nearest to the lat/lon provided.

### Request Query Parameters

| Parameters | Type  | Description                                    | Example     | Required |
| ---------- | ----- | ---------------------------------------------- | ----------- | -------- |
| lat        | float | Latitude. Defaults to geo targeted latitude.   | -118.001042 | No       |
| lon        | float | Longitude. Defaults to geo targeted longitude. | 33.6604657  | No       |

### Sample Request

```http
GET /mapview/spot?lat=33.6604657&lon=-118.001042
```

### Sample Response

```JSON
{
  "spot": {
    "_id": "5977abb3b38c2300127471ec",
    "updatedAt": "2018-04-18T18:03:49.477Z",
    "createdAt": "2017-08-03T18:56:59.981Z",
    "thumbnail": "https://spot-thumbnails.cdn-surfline.com/spots/default/default_1500.jpg",
    "rank": 11,
    "subregionId": "58581a836630e24c44878fd6",
    "location": {
      "coordinates": [
        -118.00316870212555,
        33.65544147271585
      ],
      "type": "Point"
    },
    "lat": 33.65544147271585,
    "lon": -118.00316870212555,
    "name": "HB Pier, South Closeup",
    "tide": {
      "previous": {
        "type": "LOW",
        "height": -0.56,
        "timestamp": 1524055020,
        "utcOffset": -7
      },
      "current": {
        "type": "NORMAL",
        "height": 3.623878787878788,
        "timestamp": 1524074629,
        "utcOffset": -7
      },
      "next": {
        "type": "HIGH",
        "height": 3.81,
        "timestamp": 1524077700,
        "utcOffset": -7
      }
    },
    "waterTemp": {
      "min": 56,
      "max": 58
    },
    "cameras": [
      {
        "_id": "5978cb2518fd6daf0da062ae",
        "title": "SOCAL - HB Pier Southside Closeup",
        "streamUrl": "https://cams.cdn-surfline.com/wsc-west/wc-hbpiersclosecam.stream/playlist.m3u8",
        "stillUrl": "https://camstills.cdn-surfline.com/hbpiersclosecam/latest_small.jpg",
        "pixelatedStillUrl": "https://camstills.cdn-surfline.com/hbpiersclosecam/latest_small_pixelated.png",
        "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/hbpiersclosecam/hbpiersclosecam",
        "isPremium": false,
        "status": {
          "isDown": false,
          "message": ""
        }
      }
    ],
    "conditions": {
      "human": true,
      "value": "FAIR_TO_GOOD",
      "sortableCondition": 6
    },
    "wind": {
      "speed": 8.84,
      "direction": 168.6
    },
    "weather": {
      "temperature": 63.3,
      "condition": "CLEAR_NO_RAIN"
    },
    "waveHeight": {
      "human": true,
      "min": 3,
      "max": 4,
      "occasional": null,
      "humanRelation": "waist to shoulder high",
      "plus": true
    },
    "legacyId": 148545,
    "__v": 28206,
    "timezone": "America/Los_Angeles",
    "legacyRegionId": 2143,
    "saveFailures": 0,
    "swells": [
      {
        "height": 1.97,
        "direction": 271.41,
        "period": 8,
        "_id": "5ad78885a2dade2ec5d67913"
      },
      {
        "height": 0.98,
        "direction": 272.81,
        "period": 11,
        "_id": "5ad78885a2dadeb1b7d67912"
      },
      {
        "height": 1.64,
        "direction": 174.38,
        "period": 11,
        "_id": "5ad78885a2dade158ad67911"
      },
      {
        "height": 0.66,
        "direction": 192.66,
        "period": 15,
        "_id": "5ad78885a2dade3e5bd67910"
      },
      {
        "height": 0,
        "direction": 0,
        "period": 0,
        "_id": "5ad78885a2dade2adcd6790f"
      },
      {
        "height": 0,
        "direction": 0,
        "period": 0,
        "_id": "5ad78885a2dade058ad6790e"
      }
    ],
    "offshoreDirection": 211
  }
}
```
