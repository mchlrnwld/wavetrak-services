# Favorites Subregions

Returns info on all subregions in a user's favorites

## Sample Request

```
GET /favorites/subregions HTTP/1.1
Host: server.example.com
```
## Sample Authentication Header

| Key           | Value                    | Description      |
| ------------- | ------------------------ | ---------------- |
| X-Auth-UserId | 582495992d06cd3b71d66229 | User's Mongo ID. |

## Query Parameters

| Parameter | Type   | Description     | Example                  | Required |
| ---------- | ------ | --------------- | ------------------------ | -------- |
| conditions | boolean | Conditions added to response | true | No |


## Sample Response

```JSON
{
    "associated": {
        "units": {
            "windSpeed": "KTS",
            "waveHeight": "FT",
            "tideHeight": "FT",
        },
    },
    "subregions": [
        {
            "offshoreSwellLocation": {
                "type": "Point",
                "coordinates": [
                    -118,
                    33.5
                ]
            },
            "status": "PUBLISHED",
            "order": "5",
            "_id": "58581a836630e24c44878fd6",
            "name": "North Orange County",
            "__v": 8,
            "region": "5908c45e6a2e4300134fbe92",
            "geoname": 5332921,
            "primarySpot": "5842041f4e65fad6a7708827",
            "legacyId": 2143,
            "conditions": [
                {
                    "timestamp": 1579161600,
                    "forecaster": null,
                    "human": false,
                    "observation": "Fun sized long period waves from the SSE. Light and variable winds with clean conditions.",
                    "am": {
                        "maxHeight": 3,
                        "minHeight": 2,
                        "plus": false,
                        "humanRelation": "waist to stomach high",
                        "occasionalHeight": null,
                        "rating": null
                    },
                    "pm": {
                        "maxHeight": 3,
                        "minHeight": 2,
                        "plus": false,
                        "humanRelation": "waist to stomach high",
                        "occasionalHeight": null,
                        "rating": null
                    }
                },
                {
                    "timestamp": 1579248000,
                    "forecaster": null,
                    "human": false,
                    "observation": "Fun sized long period waves from the SSE. Light and variable winds with clean conditions.",
                    "am": {
                        "maxHeight": 3,
                        "minHeight": 2,
                        "plus": false,
                        "humanRelation": "waist to stomach high",
                        "occasionalHeight": null,
                        "rating": null
                    },
                    "pm": {
                        "maxHeight": 4,
                        "minHeight": 3,
                        "plus": false,
                        "humanRelation": "waist to chest high",
                        "occasionalHeight": null,
                        "rating": null
                    }
                }
            ]
        },
        {
            "offshoreSwellLocation": {
                "type": "Point",
                "coordinates": [
                    -117.747,
                    33.456
                ]
            },
            "status": "PUBLISHED",
            "order": "6",
            "_id": "58581a836630e24c4487900a",
            "name": "South Orange County",
            "__v": 4,
            "region": "5908c45e6a2e4300134fbe92",
            "geoname": 5332921,
            "primarySpot": "5842041f4e65fad6a7708884",
            "legacyId": 2950,
            "conditions": [
                {
                    "timestamp": 1579161600,
                    "forecaster": null,
                    "human": false,
                    "observation": "Very small mid period waves from the WNW. Light and variable winds with clean conditions.",
                    "am": {
                        "maxHeight": 2,
                        "minHeight": 1,
                        "plus": false,
                        "humanRelation": "ankle to knee high",
                        "occasionalHeight": null,
                        "rating": null
                    },
                    "pm": {
                        "maxHeight": 2,
                        "minHeight": 1,
                        "plus": false,
                        "humanRelation": "ankle to knee high",
                        "occasionalHeight": null,
                        "rating": null
                    }
                },
                {
                    "timestamp": 1579248000,
                    "forecaster": null,
                    "human": false,
                    "observation": "Small long period waves from the SSE. Light and variable winds with clean conditions.",
                    "am": {
                        "maxHeight": 3,
                        "minHeight": 2,
                        "plus": false,
                        "humanRelation": "knee to waist high",
                        "occasionalHeight": null,
                        "rating": null
                    },
                    "pm": {
                        "maxHeight": 3,
                        "minHeight": 2,
                        "plus": false,
                        "humanRelation": "knee to waist high",
                        "occasionalHeight": null,
                        "rating": null
                    }
                }
            ]
        }
    ]
}
```
