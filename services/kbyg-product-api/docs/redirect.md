# Redirect

All endpoints and ids are treated as resources.
Redirects are sent as a 301.

## Pattern

```
kbyg/redirect/:targetPlatform/:sourceIdType/:sourceId
```

## Request Query Parameters

| Endpoint | Use case   |
| ---------- | ------ |
| /mvp/spot/:legacySpotId   | legacy spot -> new spot  |
| /mvp/subregion/:legacySubregionId   | legacy subregion -> new subregion  |
| /legacy/subregion/:subregionId   | new subregion -> legacy regional forecast  |
| /mvp/travel/:legacyTravelId   | legacy id -> new travel page (mapping is managed in `server/redirect/travelRedirectPaths.json`)  |
| /mvp/geoname-map/:legacyId   | legacy id -> new geoname map page (mapping is managed in `server/redirect/geonameMapRedirectPaths.json`)  |
