# Batch Spot Report Views

## By spotIds
Returns the spot report views for a batch of spotIds. You should do a POST to /batch to get the results.
The body should specify an array of mongo spotIds and a units object that contains the user units.

### Body Parameters

| Parameters | Type   | Description                                                                                                                                                                                     | Example                  | Required |
| ---------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ | -------- |
| spotIds     | Array | An array of Mongo Spot IDs.                                                                                                                                                                                | ['584204204e65fad6a77091aa', '5842041f4e65fad6a77088ea'] | Yes      |
| units      | Object | Units object that contains the user units. | {temperature: 'F',tideHeight: 'FT',waveHeight: 'FT',windSpeed: 'KTS'}                 | No       |

### Sample Request

```
POST /spots/batch HTTP/1.1
Host: server.example.com
Content-Type: application/json
{
  "units": {
    "temperature": "F",
    "tideHeight": "FT",
    "waveHeight": "FT",
    "windSpeed": "KTS"
  },
  "spotIds":[
    "584204204e65fad6a77091aa",
    "5842041f4e65fad6a77088ea"
  ]
}
```

### Sample Response

```JSON
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    }
  },
  "data": [
    {
      "_id": "5842041f4e65fad6a77088ea",
      "thumbnail": "https://spot-thumbnails.staging.surfline.com/spots/5842041f4e65fad6a77088ea/5842041f4e65fad6a77088ea_1500.jpg",
      "rank": 6,
      "subregionId": "58581a836630e24c44878fd6",
      "lat": 33.66704400178788,
      "lon": -118.0192012569068,
      "name": "Goldenwest",
      "tide": {
        "previous": {
          "type": "LOW",
          "height": 2.5,
          "timestamp": 1501701781,
          "utcOffset": -7
        },
        "current": {
          "type": "NORMAL",
          "height": 5,
          "timestamp": 1501720628,
          "utcOffset": -7
        },
        "next": {
          "type": "HIGH",
          "height": 5.3,
          "timestamp": 1501724898,
          "utcOffset": -7
        }
      },
      "waterTemp": {
        "min": 63,
        "max": 65
      },
      "cameras": [
        {
          "_id": "58235706302ec90c4108c3e4",
          "title": "Goldenwest",
          "streamUrl": "https://cams.cdn-surfline.com/wsc-west/wc-goldenwestcam.stream/playlist.m3u8",
          "stillUrl": "https://camstills.cdn-surfline.com/goldenwestcam/latest_small_pixelated.png",
          "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/goldenwestcam/goldenwestcam",
          "status": {
            "isDown": false,
            "message": ""
          },
          "control": "https://camstills.cdn-surfline.com/goldenwestcam/latest_small.jpg",
          "nighttime": false,
          "rewindClip": null
        }
      ],
      "conditions": {
        "human": true,
        "value": "FAIR"
      },
      "wind": {
        "speed": 8,
        "direction": 104.29
      },
      "weather": {
        "temperature": 80,
        "condition": "OVERCAST_NO_RAIN"
      },
      "waveHeight": {
        "human": false,
        "min": 5,
        "max": 6,
        "occasional": null,
        "humanRelation": null,
        "plus": false
      },
      "legacyId": 4870,
      "legacyRegionId": 2143,
      "slug": "DEPRECATED"
    },
    {
      "_id": "584204204e65fad6a77091aa",
      "thumbnail": "https://spot-thumbnails.staging.surfline.com/spots/584204204e65fad6a77091aa/584204204e65fad6a77091aa_1500.jpg",
      "rank": 10,
      "subregionId": "58581a836630e24c44878fd6",
      "lat": 33.65516462595945,
      "lon": -118.00669312477112,
      "name": "HB Pier, Northside Overview",
      "tide": {
        "previous": {
          "type": "LOW",
          "height": 2.5,
          "timestamp": 1501701781,
          "utcOffset": -7
        },
        "current": {
          "type": "NORMAL",
          "height": 5,
          "timestamp": 1501720562,
          "utcOffset": -7
        },
        "next": {
          "type": "HIGH",
          "height": 5.3,
          "timestamp": 1501724898,
          "utcOffset": -7
        }
      },
      "waterTemp": {
        "min": 63,
        "max": 65
      },
      "cameras": [
        {
          "_id": "58349a5c3421b20545c4b542",
          "title": "Huntington Beach Overview",
          "streamUrl": "https://cams.cdn-surfline.com/wsc-west/wc-hbpiernsovcam.stream/playlist.m3u8",
          "stillUrl": "https://camstills.cdn-surfline.com/hbpiernsovcam/latest_small_pixelated.png",
          "rewindBaseUrl": "http://live-cam-archive.cdn-surfline.com/hbpiernsovcam/hbpiernsovcam",
          "status": {
            "isDown": false,
            "message": ""
          },
          "control": "https://camstills.cdn-surfline.com/hbpiernsovcam/latest_small.jpg",
          "nighttime": false,
          "rewindClip": null
        }
      ],
      "conditions": {
        "human": true,
        "value": "POOR"
      },
      "wind": {
        "speed": 8,
        "direction": 104.33
      },
      "weather": {
        "temperature": 80,
        "condition": "OVERCAST_NO_RAIN"
      },
      "waveHeight": {
        "human": true,
        "min": 2,
        "max": 3,
        "occasional": null,
        "humanRelation": "knee to waist high",
        "plus": false
      },
      "legacyId": 58218,
      "legacyRegionId": 2143,
      "slug": "DEPRECATED"
    }
  ]
}
```

## By parentTaxonomyIds

Returns the spot report views for a batch of parent taxonomy ids. You should do a POST to /batch to get the results.
This endpoint returns an array of taxonomy `_id` values matching the `parentTaxonomyIds` sent as part of the request body, along with an array of `spots` for each matching `_id`, which contains spot report data for each spot that lies within the provided array of `parentTaxonomyIds`.  Pass a comma-separated list of spot report node names you would like included in the response to limit what is returned.

#### Request Parameters

A set of batch `parentTaxonomyIds` can be sent in the body of a `POST` to the `/spots/batch` endpoint.

| param               | values            | required  | Description                                       |
| ------------------- | ----------------- | --------- | -------------------------------------             |
| `parentTaxonomyIds`       | `Array<ObjectId>` |   yes     | Array of taxonomy ids of type geoname             |
| `select`            | `String`<List>    |   no      | comma sep list of spot report node names (limits response)   |

Example of `select` param could be `thumbnail,lat,lon,conditions` and would return `name`, `_id`, `rank`, `thumbnail`, `lat`, `lon`, `conditions` for each spot report vs the entire result set.

** Note: `_id`, `name` and `rank` will always be part of returned spot report data.

#### Sample Request Body

```json
{
  "parentTaxonomyIds": ["58f7ed5ddadb30820bb39651"],
	"select": "thumbnail,lat,lon,conditions"
}
```

#### Sample Response Body

Full Response, all spot report data for 2 spots (assumes no `select` parameter provided)

Contains example of full spot report view within spots array for two spots for reference.

```json
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    }
  },
  "data": [
    {
      "_id": "58f7ed5ddadb30820bb39651",
      "spots": [
        {
          "_id": "5842041f4e65fad6a770882c",
          "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/5842041f4e65fad6a770882c/5842041f4e65fad6a770882c_1500.jpg",
          "rank": 29,
          "subregionId": "58581a836630e24c44878fd6",
          "lat": 33.60577342582,
          "lon": -117.9315376281,
          "name": "Newport Beach Pier",
          "tide": {
            "previous": {
              "type": "HIGH",
              "height": 3,
              "timestamp": 1551134760,
              "utcOffset": -8
            },
            "current": {
              "type": "NORMAL",
              "height": 2.2,
              "timestamp": 1551147033,
              "utcOffset": -8
            },
            "next": {
              "type": "LOW",
              "height": 2,
              "timestamp": 1551152520,
              "utcOffset": -8
            }
          },
          "waterTemp": {
            "min": 55,
            "max": 57
          },
          "cameras": [
            {
              "_id": "58349dfee411dc743a5d52be",
              "title": "SOCAL - Newport Pier",
              "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/wc-newportpier/playlist.m3u8",
              "stillUrl": "https://camstills.cdn-surfline.com/wc-newportpier/latest_small.jpg",
              "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/wc-newportpier/wc-newportpier",
              "isPremium": false,
              "alias": "wc-newportpier",
              "status": {
                "isDown": false,
                "message": "",
                "altMessage": ""
              },
              "control": "https://camstills.cdn-surfline.com/wc-newportpier/latest_small.jpg",
              "nighttime": false,
              "rewindClip": "https://camrewinds.cdn-surfline.com/wc-newportpier/wc-newportpier.1300.2019-02-25.mp4"
            }
          ],
          "conditions": {
            "human": false,
            "value": null
          },
          "wind": {
            "speed": 4,
            "direction": 218.04
          },
          "weather": {
            "temperature": 59,
            "condition": "NIGHT_CLEAR_NO_RAIN"
          },
          "waveHeight": {
            "human": false,
            "min": 0,
            "max": 0,
            "occasional": null,
            "humanRelation": "Flat",
            "plus": false
          },
          "legacyId": 4227,
          "legacyRegionId": 2143,
          "timezone": "America/Los_Angeles",
          "saveFailures": 0,
          "swells": [
            {
              "height": 0.3,
              "direction": 275.63,
              "period": 12,
              "_id": "5c74a01aa805bc34c7e66977"
            },
            {
              "height": 0,
              "direction": 0,
              "period": 0,
              "_id": "5c74a01aa805bc8339e66976"
            },
            {
              "height": 0.8,
              "direction": 275.63,
              "period": 5,
              "_id": "5c74a01aa805bc011ee66975"
            },
            {
              "height": 1.1,
              "direction": 184.22,
              "period": 12,
              "_id": "5c74a01aa805bce5f5e66974"
            },
            {
              "height": 0,
              "direction": 0,
              "period": 0,
              "_id": "5c74a01aa805bc295fe66973"
            },
            {
              "height": 0,
              "direction": 0,
              "period": 0,
              "_id": "5c74a01aa805bc862ee66972"
            }
          ],
          "offshoreDirection": 209,
          "parentTaxonomy": [
            "58f7ed51dadb30820bb38782",
            "58f7ed51dadb30820bb38791",
            "58f7ed51dadb30820bb3879c",
            "5908c46bdadb30820b23c1f3",
            "58f7ed5ddadb30820bb39651",
            "58f7ed51dadb30820bb387a6",
            "58f7ed5ddadb30820bb39689",
            "58f7ed60dadb30820bb39a11"
          ],
          "slug": "DEPRECATED"
        },
        {
          "_id": "5842041f4e65fad6a770882e",
          "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/5842041f4e65fad6a770882e/5842041f4e65fad6a770882e_1500.jpg",
          "rank": 4,
          "subregionId": "58581a836630e24c4487900a",
          "lat": 33.474,
          "lon": -117.724,
          "name": "Salt Creek",
          "tide": {
            "previous": {
              "type": "HIGH",
              "height": 4.6,
              "timestamp": 1551176460,
              "utcOffset": -8
            },
            "current": {
              "type": "NORMAL",
              "height": 2.2,
              "timestamp": 1551192273,
              "utcOffset": -8
            },
            "next": {
              "type": "LOW",
              "height": 0.7,
              "timestamp": 1551204240,
              "utcOffset": -8
            }
          },
          "waterTemp": {
            "min": 57,
            "max": 59
          },
          "cameras": [
            {
              "_id": "58349fe73421b20545c4b57e",
              "title": "SOCAL - Salt Creek",
              "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/wc-saltcreek/playlist.m3u8",
              "stillUrl": "https://camstills.cdn-surfline.com/wc-saltcreek/latest_small.jpg",
              "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/wc-saltcreek/wc-saltcreek",
              "isPremium": true,
              "alias": "wc-saltcreek",
              "status": {
                "isDown": false,
                "message": "",
                "altMessage": ""
              },
              "control": "https://camstills.cdn-surfline.com/wc-saltcreek/latest_small.jpg",
              "nighttime": false,
              "rewindClip": "https://camrewinds.cdn-surfline.com/wc-saltcreek/wc-saltcreek.1300.2019-02-25.mp4"
            }
          ],
          "conditions": {
            "human": false,
            "value": null
          },
          "wind": {
            "speed": 1,
            "direction": 63.9
          },
          "weather": {
            "temperature": 53,
            "condition": "CLEAR_NO_RAIN"
          },
          "waveHeight": {
            "human": false,
            "min": 1,
            "max": 2,
            "occasional": null,
            "humanRelation": "1-2 ft – ankle to knee high",
            "plus": false
          },
          "legacyId": 4233,
          "legacyRegionId": 2950,
          "timezone": "America/Los_Angeles",
          "saveFailures": 0,
          "swells": [
            {
              "height": 0.2,
              "direction": 282.66,
              "period": 11,
              "_id": "5c7550d3ae40645ca95eb68c"
            },
            {
              "height": 0.2,
              "direction": 278.44,
              "period": 4,
              "_id": "5c7550d3ae406461315eb68b"
            },
            {
              "height": 0.9,
              "direction": 184.22,
              "period": 12,
              "_id": "5c7550d3ae4064578b5eb68a"
            },
            {
              "height": 0,
              "direction": 0,
              "period": 0,
              "_id": "5c7550d3ae4064773b5eb689"
            },
            {
              "height": 0.2,
              "direction": 241.88,
              "period": 11,
              "_id": "5c7550d3ae40643ce05eb688"
            },
            {
              "height": 0,
              "direction": 0,
              "period": 0,
              "_id": "5c7550d3ae4064a8455eb687"
            }
          ],
          "offshoreDirection": 213,
          "parentTaxonomy": [
            "58f7ed51dadb30820bb38782",
            "58f7ed51dadb30820bb38791",
            "58f7ed51dadb30820bb3879c",
            "5908c46bdadb30820b23c1f3",
            "58f7ed5ddadb30820bb39651",
            "58f7ed51dadb30820bb387a6",
            "58f7ed63dadb30820bb39cad",
            "58f7ed63dadb30820bb39cd0"
          ],
          "slug": "DEPRECATED"
        }
      ]
    }
  ]
}
```

Partial Response, selected spot report data for 2 spots

Contains example of spot report view, within spots array for 2 spots with `select` parameters of "thumbnail,lat,lon,conditions" supplied, for reference.

```json
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "KTS"
    }
  },
  "data": [
    {
      "_id": "58f7ed5ddadb30820bb39651",
      "spots": [
        {
          "_id": "5842041f4e65fad6a770882c",
          "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/5842041f4e65fad6a770882c/5842041f4e65fad6a770882c_1500.jpg",
          "rank": 29,
          "lat": 33.60577342582,
          "lon": -117.9315376281,
          "name": "Newport Beach Pier",
          "conditions": {
            "human": false,
            "value": null
          }
        },
        {
          "_id": "5842041f4e65fad6a770882e",
          "thumbnail": "https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/5842041f4e65fad6a770882e/5842041f4e65fad6a770882e_1500.jpg",
          "rank": 4,
          "lat": 33.474,
          "lon": -117.724,
          "name": "Salt Creek",
          "conditions": {
            "human": false,
            "value": null
          }
        }
      ]
    }
  ]
}
```