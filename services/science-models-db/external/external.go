package external

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
)

// Client has methods for making calls to external APIs.
type Client interface {
	GetModelRuns() ([]models.ModelRun, error)
}

type client struct{}

type resError struct {
	Message string
	Errors  []string
}

func getResError(body []byte) (*resError, error) {
	var resErr resError
	if jsonErr := json.Unmarshal(body, &resErr); jsonErr != nil {
		return nil, errors.WrapError(jsonErr)
	}
	return &resErr, nil
}

// TODO: Use GraphQL endpoint
func (client) GetModelRuns() ([]models.ModelRun, error) {
	url := fmt.Sprintf("%s/graphql", os.Getenv("SCIENCE_DATA_SERVICE"))
	query, jsonErr := json.Marshal(map[string]string{
		"query": `
    query {
      models {
        agency
        model
        runs(statuses:ONLINE,types:FULL_GRID) {
          run
        }
      }
    }`,
	})
	if jsonErr != nil {
		return nil, errors.WrapError(jsonErr)
	}

	res, httpErr := http.Post(url, "application/json", bytes.NewBuffer(query))
	if httpErr != nil {
		return nil, errors.WrapError(httpErr)
	}
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode != 200 {
		resErr, jsonErr := getResError(body)
		if jsonErr != nil {
			return nil, errors.WrapError(jsonErr)
		}
		return nil, errors.WrapError(fmt.Errorf("/graphql request to science-data-service return %v: %v: %v", res.Status, resErr.Message, resErr.Errors))
	}

	modelRuns, jsonErr := models.ModelRunsFromGQL(body)
	if jsonErr != nil {
		return nil, errors.WrapError(jsonErr)
	}
	return modelRuns, nil
}

// New returns a client.
func New() Client {
	return new(client)
}
