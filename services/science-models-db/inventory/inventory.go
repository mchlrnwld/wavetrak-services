package inventory

import (
	e "errors"
	"log"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/backups"
	"github.com/Surfline/wavetrak-services/services/science-models-db/database"
	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/Surfline/wavetrak-services/services/science-models-db/remoteinventory"
)

// Inventory manages multiple databases.
type Inventory interface {
	AddDB(models.ModelRun, bool) (*database.Database, error)
	DropDB(models.ModelRun, bool) error
	LoadRemote(chan error, chan error)
	Database(models.ModelRun) (*database.Database, bool)
	Databases() map[string]database.Database
	Restore([]models.ModelRun, backups.Manager) error
}

type inventory struct {
	dir    string
	local  map[string]database.Database
	remote remoteinventory.RemoteInventory
}

// AddDB adds a database to inventory.
func (inv inventory) AddDB(modelRun models.ModelRun, readOnly bool) (*database.Database, error) {
	key := modelRun.Key()
	dbPath := path.Join(inv.dir, key)
	db, err := database.New(dbPath, modelRun, readOnly)
	if err != nil {
		return nil, errors.WrapError(err)
	}

	if inv.remote != nil {
		if err := inv.remote.PutDB(key, readOnly); err != nil {
			db.Close()
			return nil, errors.WrapError(err)
		}
	}
	inv.local[key] = *db
	return db, nil
}

// DropDB removes database from inventory.
func (inv inventory) DropDB(modelRun models.ModelRun, permanent bool) error {
	key := modelRun.Key()
	db, ok := inv.local[key]
	if !ok {
		return nil
	}

	if err := db.Close(); err != nil {
		return errors.WrapError(err)
	}

	if permanent {
		if err := os.RemoveAll(path.Join(inv.dir, key)); err != nil {
			return errors.WrapError(err)
		}
	}

	if inv.remote != nil {
		if err := inv.remote.DeleteDB(key); err != nil {
			return errors.WrapError(err)
		}
	}
	delete(inv.local, key)
	return nil
}

func (inv inventory) addRemoteItems(items []remoteinventory.Item) error {
	for _, item := range items {
		modelRun, err := models.ModelRunFromKey(item.Database)
		if err != nil {
			return errors.WrapError(err)
		}

		if _, ok := inv.Database(modelRun); ok {
			return nil
		}

		if _, err := inv.AddDB(modelRun, item.ReadOnly); err != nil {
			return errors.WrapError(err)
		}
	}

	return nil
}

func (inv inventory) LoadRemote(readOnlyDone chan error, writeableDone chan error) {
	if inv.remote == nil {
		err := errors.WrapError(e.New("Remote inventory undefined"))
		readOnlyDone <- err
		writeableDone <- err
	}

	items, err := inv.remote.GetDBs()
	if err != nil {
		wErr := errors.WrapError(err)
		readOnlyDone <- wErr
		writeableDone <- wErr
	}

	readOnlyItems, writeableItems := make([]remoteinventory.Item, 0, len(items)), make([]remoteinventory.Item, 0, len(items))
	for _, item := range items {
		if item.ReadOnly {
			readOnlyItems = append(readOnlyItems, item)
		} else {
			writeableItems = append(writeableItems, item)
		}
	}

	if err := inv.addRemoteItems(readOnlyItems); err != nil {
		readOnlyDone <- errors.WrapError(err)
	} else {
		readOnlyDone <- nil
	}

	for {
		if err := inv.addRemoteItems(writeableItems); err == nil {
			writeableDone <- nil
			return
		}

		time.Sleep(30 * time.Second)
	}
}

func (inv inventory) Databases() map[string]database.Database {
	return inv.local
}

func (inv inventory) Database(modelRun models.ModelRun) (*database.Database, bool) {
	db, ok := inv.local[modelRun.Key()]
	return &db, ok
}

// Restore database files from S3.
func (inv inventory) Restore(modelRuns []models.ModelRun, backupsManager backups.Manager) error {
	// Setup a worker thread for each model run. This allows for optimal download performance since
	// AWS S3 rate limits based on common prefixes and each model run is a separate prefix.
	// https://docs.aws.amazon.com/AmazonS3/latest/dev/optimizing-performance.html
	errChan := make(chan error)
	downloadQueues := make([]chan string, len(modelRuns))
	modelRunWG := new(sync.WaitGroup)
	for i, modelRun := range modelRuns {
		log.Printf("Setting up a worker for restoring %+v data files...", modelRun)

		downloadQueues[i] = make(chan string, 1000)
		modelRunWG.Add(1)
		go func(modelRun models.ModelRun, downloadQueue chan string) {
			defer modelRunWG.Done()

			downloader := backupsManager.NewDownloader(inv.dir)
			for fileKey := range downloadQueue {
				if err := downloader.Download(fileKey); err != nil {
					log.Printf("Failed to restore %+v data files.", modelRun)
					errChan <- errors.WrapError(err)
					return
				}
			}

			log.Printf("Successfully restored %+v data files.", modelRun)
			log.Printf("Loading %+v to inventory...", modelRun)

			if _, err := inv.AddDB(modelRun, true); err != nil {
				if !strings.Contains(err.Error(), database.NoDecoderFound) {
					errChan <- errors.WrapError(err)
				} else {
					log.Println(err)
				}
				return
			}

			log.Printf("Successfully loaded %+v to inventory.", modelRun)
		}(modelRun, downloadQueues[i])
	}

	// Concurrently list files for download to avoid blocking.
	for i, modelRun := range modelRuns {
		log.Printf("Queueing up data files for %+v...", modelRun)

		go func(modelRun models.ModelRun, downloadQueue chan string) {
			defer close(downloadQueue)

			fileKeys, err := backupsManager.ListFileKeys(modelRun)
			if err != nil {
				errChan <- errors.WrapError(err)
			}

			for _, fileKey := range fileKeys {
				downloadQueue <- fileKey
			}
			log.Printf("Finished queueing up data files for %+v.", modelRun)
		}(modelRun, downloadQueues[i])
	}

	// Asynchronously wait for worker threads to be done before closing the error channel to avoid blocking.
	go func() {
		modelRunWG.Wait()
		close(errChan)
	}()

	// Processes errors from worker threads and wait for error channel to close before returning.
	// This needs to happen in the main thread so that we can return the error. Wait group error
	// handling pattern inspired by
	// https://www.jernejsila.com/2017/12/23/error-handling-concurrent-programs-golang/.
	for err := range errChan {
		if err != nil {
			return errors.WrapError(err)
		}
	}

	log.Println("Successfully restored all model runs to inventory.")
	return nil
}

// New creates a inventory of database from a path.
func New(dir string, remote remoteinventory.RemoteInventory) Inventory {
	inv := inventory{
		dir:    dir,
		local:  make(map[string]database.Database),
		remote: remote,
	}
	return inv
}
