package inventory

import (
	"errors"
	"testing"

	"github.com/Surfline/wavetrak-services/services/science-models-db/database"
	"github.com/Surfline/wavetrak-services/services/science-models-db/mocks"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	th "github.com/Surfline/wavetrak-services/services/science-models-db/testhelpers"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var modelRuns = []models.ModelRun{
	models.ModelRun{
		Agency: "NOAA",
		Model:  "GFS",
		Run:    2019100112,
	},
	models.ModelRun{
		Agency: "Wavetrak",
		Model:  "Lotus-WW3",
		Run:    2019100106,
	},
}
var fileKeys = []string{"file1", "file2"}

func TestRestore(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		var err error
		downloaded := make([]string, 0)
		downloader := new(mocks.BackupsDownloader)
		downloader.On("Download", mock.AnythingOfType("string")).Times(len(modelRuns) * len(fileKeys)).Run(func(args mock.Arguments) {
			downloaded = append(downloaded, args.String(0))
		}).Return(nil)

		backupsManager := new(mocks.BackupsManager)
		backupsManager.On("NewDownloader", dir).Times(len(modelRuns)).Return(downloader)
		backupsManager.On("ListFileKeys", mock.Anything).Times(len(modelRuns)).Return(fileKeys, nil)

		inventory := New(dir, nil)
		for _, modelRun := range modelRuns {
			_, err = inventory.AddDB(modelRun, false)
			assert.NoError(t, err)
			err = inventory.DropDB(modelRun, false)
			assert.NoError(t, err)
		}

		err = inventory.Restore(modelRuns, backupsManager)
		assert.NoError(t, err)
		downloader.AssertExpectations(t)
		backupsManager.AssertExpectations(t)
		assert.Len(t, downloaded, len(modelRuns)*len(fileKeys))
	})
}

func TestRestoreDownloadError(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		var err error
		expectedErr := errors.New("Download error")
		downloaded := make([]string, 0)
		downloader := new(mocks.BackupsDownloader)
		downloader.On("Download", mock.AnythingOfType("string")).Return(expectedErr)

		backupsManager := new(mocks.BackupsManager)
		backupsManager.On("NewDownloader", dir).Return(downloader)
		backupsManager.On("ListFileKeys", mock.Anything).Return(fileKeys, nil)

		inventory := New(dir, nil)
		err = inventory.Restore(modelRuns, backupsManager)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), expectedErr.Error())
		downloader.AssertExpectations(t)
		backupsManager.AssertExpectations(t)
		assert.Empty(t, downloaded)
	})
}

func TestRestoreAddDBError(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		var err error
		downloader := new(mocks.BackupsDownloader)

		backupsManager := new(mocks.BackupsManager)
		backupsManager.On("NewDownloader", dir).Return(downloader)
		backupsManager.On("ListFileKeys", mock.Anything).Return(make([]string, 0), nil)

		inventory := New(dir, nil)
		err = inventory.Restore(modelRuns, backupsManager)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), database.UnableToOpenDatabase)
		downloader.AssertExpectations(t)
		backupsManager.AssertExpectations(t)
	})
}

func TestRestoreIgnoreDecoderNotFoundError(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		var err error
		downloader := new(mocks.BackupsDownloader)

		backupsManager := new(mocks.BackupsManager)
		backupsManager.On("NewDownloader", dir).Return(downloader)
		backupsManager.On("ListFileKeys", mock.Anything).Return(make([]string, 0), nil)

		inventory := New(dir, nil)
		err = inventory.Restore([]models.ModelRun{
			models.ModelRun{
				Agency: "Agency with no decoder",
				Model:  "Model with no decoder",
				Run:    0,
			},
		}, backupsManager)
		assert.NoError(t, err)
		downloader.AssertExpectations(t)
		backupsManager.AssertExpectations(t)
	})
}

func TestRestoreListFileKeysError(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		var err error
		expectedErr := errors.New("ListFileKeys error")
		downloaded := make([]string, 0)
		downloader := new(mocks.BackupsDownloader)

		backupsManager := new(mocks.BackupsManager)
		backupsManager.On("NewDownloader", dir).Return(downloader)
		backupsManager.On("ListFileKeys", mock.Anything).Return(make([]string, 0), expectedErr)

		inventory := New(dir, nil)
		err = inventory.Restore(modelRuns, backupsManager)
		assert.Error(t, err)
		downloader.AssertExpectations(t)
		backupsManager.AssertExpectations(t)
		assert.Empty(t, downloaded)
	})
}
