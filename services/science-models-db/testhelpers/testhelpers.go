package testhelpers

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// WithTempDir sets up a temporary directory to run a test with.
func WithTempDir(t *testing.T, test func(string)) {
	path, err := os.Getwd()
	assert.NoError(t, err)
	dir, err := ioutil.TempDir(path, "test")
	assert.NoError(t, err)
	defer os.RemoveAll(dir)

	test(dir)
}
