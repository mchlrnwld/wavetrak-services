variable "environment" {
}

variable "vpc_id" {
}

variable "availability_zones" {
  type = list(string)
}

variable "instance_subnets" {
  type = list(string)
}

variable "lb_subnets" {
  type = list(string)
}

variable "dns_zone_id" {
}

variable "internal_sg_group" {
}

variable "instance_type" {
}

variable "load_balancer_arn" {
}

variable "alb_listener_arn_http" {
}

variable "key_name" {
}

variable "science_bucket" {
}

variable "min_instance_count" {
}

variable "max_instance_count" {
}

locals {
  company     = "wt"
  application = "science-models-db"
  dns_name    = "${local.application}.${var.environment}.surfline.com"

  health_check_grace_period_seconds = 3600

  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  service_placement_strategy = [
    {
      type  = "spread"
      field = "instanceId"
    },
  ]

  science_prefix = "science-models-db"

  common_tags = {
    Company     = local.company
    Application = local.application
    Environment = var.environment
    Terraform   = "true"
  }

  ecs_tags = merge(
    local.common_tags,
    {
      "Service" = "ecs"
    },
  )
}
