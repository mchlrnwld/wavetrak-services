resource "aws_dynamodb_table" "kinesis_checkpoints" {
  name         = "${local.company}-${local.application}-kinesis-checkpoints-${var.environment}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "namespace"
  range_key    = "shard_id"

  ttl {
    attribute_name = "ttl_timestamp"
    enabled        = true
  }

  attribute {
    name = "namespace"
    type = "S"
  }

  attribute {
    name = "shard_id"
    type = "S"
  }

  tags = local.common_tags
}

resource "aws_dynamodb_table" "database_inventory" {
  name         = "${local.company}-${local.application}-database-inventory-${var.environment}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "instance_id"
  range_key    = "database"

  ttl {
    attribute_name = "ttl_timestamp"
    enabled        = true
  }

  attribute {
    name = "instance_id"
    type = "S"
  }

  attribute {
    name = "database"
    type = "S"
  }

  tags = local.common_tags
}
