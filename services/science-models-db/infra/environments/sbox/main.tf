provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "science-models-db/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "science-models-db" {
  source = "../../"

  environment           = "sandbox"
  vpc_id                = "vpc-981887fd"
  availability_zones    = ["us-west-1b"]
  instance_subnets      = ["subnet-f2d458ab"]
  lb_subnets            = ["subnet-f4d458ad", "subnet-0b09466e"]
  dns_zone_id           = "Z3DM6R3JR1RYXV"
  internal_sg_group     = "sg-90aeaef5"
  instance_type         = "i3.xlarge"
  key_name              = "sturdy-surfline-dev"
  science_bucket        = "surfline-science-s3-dev"
  load_balancer_arn     = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/wt-science-models-db-sandbox/e713eb34ce27f351"
  alb_listener_arn_http = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/wt-science-models-db-sandbox/e713eb34ce27f351/5534f6eee3aa2f51"
  min_instance_count    = 1
  max_instance_count    = 1
}
