provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "science-models-db/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "science-models-db" {
  source = "../../"

  environment           = "prod"
  vpc_id                = "vpc-116fdb74"
  availability_zones    = ["us-west-1b"]
  instance_subnets      = ["subnet-d1bb6988"]
  lb_subnets            = ["subnet-debb6987", "subnet-baab36df"]
  dns_zone_id           = "Z3LLOZIY0ZZQDE"
  internal_sg_group     = "sg-a5a0e5c0"
  instance_type         = "i3.xlarge"
  key_name              = "sturdy_surfline_id_rsa"
  science_bucket        = "surfline-science-s3-prod"
  load_balancer_arn     = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/wt-science-models-db-prod/d9841ab965822b12"
  alb_listener_arn_http = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/wt-science-models-db-prod/d9841ab965822b12/64758d63315fd639"
  min_instance_count    = 1
  max_instance_count    = 1
}
