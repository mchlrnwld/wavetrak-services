resource "aws_cloudwatch_log_group" "science_models_db" {
  name              = "${local.company}-logs-${local.application}-${var.environment}"
  tags              = local.ecs_tags
  retention_in_days = 3
}
