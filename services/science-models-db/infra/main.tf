resource "aws_iam_role" "science_models_db_service_role" {
  name               = "${local.company}-ecs-service-${local.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-role.json")
}

resource "aws_iam_role_policy" "science_models_db_service_role_policy" {
  name   = "${local.company}-ecs-service-${local.application}-${var.environment}"
  policy = file("${path.module}/resources/ecs-service-role-policy.json")
  role   = aws_iam_role.science_models_db_service_role.id
}

module "science_models_db_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = local.company
  application  = local.application
  environment  = var.environment
  service_name = local.application

  service_lb_rules            = local.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "${local.company}-${var.environment}-${local.application}"
  service_td_container_name   = local.application
  service_td_count            = 1
  service_port                = 8080
  service_alb_priority        = 100
  service_placement_strategy  = local.service_placement_strategy

  alb_listener      = var.alb_listener_arn_http
  iam_role_arn      = aws_iam_role.science_models_db_service_role.arn
  dns_name          = local.dns_name
  dns_zone_id       = var.dns_zone_id
  load_balancer_arn = var.load_balancer_arn
  default_vpc       = var.vpc_id
  ecs_cluster       = aws_ecs_cluster.science_models_db.name

  auto_scaling_enabled              = false
  health_check_grace_period_seconds = local.health_check_grace_period_seconds
}

data "template_file" "task_policy" {
  template = templatefile("${path.module}/resources/ecs-task-role-policy.json", {
    science_bucket            = var.science_bucket
    kinesis_stream            = aws_kinesis_stream.science_models_db.arn
    kinesis_checkpoints_table = aws_dynamodb_table.kinesis_checkpoints.arn
    database_inventory_table  = aws_dynamodb_table.database_inventory.arn
  })
}

resource "aws_iam_role_policy" "task_policy" {
  name   = module.science_models_db_service.task_role
  role   = module.science_models_db_service.task_role
  policy = data.template_file.task_policy.rendered
}
