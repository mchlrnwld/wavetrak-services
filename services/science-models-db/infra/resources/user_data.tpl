#cloud-config
repo_update: true
repo_upgrade: all
system_info:
  default_user:
    name: ec2-user
users:
  default
ssh_authorized_keys:
  # Private/public keys stored in
  # "1Password > Surfline > Infrastructure > science-models-db ssh ssh keypair"
  - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCyrt+/+DTf/Ll/rL7WxD6IwzTAo2Rfy9ehLceUKKRIgB3gCfeLbPdoP/DzAhSHUa16wOSXVC/gKqpXnj5JCDwA2MsNU0Yj1JYQF21XwbKx8Wrqacib7Ed0Eu9tBxCilTlec4iBQ2Ja6SSIn59ubHr/UnjPKHx1KDV1Vxfv/fd0VraBJduvbaqIeZXRZpx+B4rfWtWzEHS9wC3b7k1JiCWXsdCX/KVgdaSh5lbBroVBSII7VpFKnRNDk1NlXRtocmE8N4JCjO2VU/qsVNPav8VbXUBrY4vkr5OxqqRMmUVrnRcHaKoO5EHtpUnMvGWFAi1AljrrGkLQy13agtH+xbrd science-models-db
write_files:
  - path: /etc/ecs/ecs.config
    content: |
      ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=5m
      ECS_CLUSTER=${ecs_cluster}
      ECS_AVAILABLE_LOGGING_DRIVERS=["json-file","awslogs"]
      ECS_IMAGE_PULL_BEHAVIOR=prefer-cached
      ECS_ENABLE_CONTAINER_METADATA=true
  - path: /etc/newrelic-infra.yml
    permissions: '0644'
    owner: 'root'
    group: 'root'
    content: |
      license_key: a26ab469b2d2b56766af51d1c8c436e736ba72bf
runcmd:
  - echo "***************************"
  - echo "*** installing packages ***"
  - echo "***************************"
  - yum install -y aws-cli

  - echo "*****************"
  - echo "*** ECR login ***"
  - echo "*****************"
  - amazon-linux-extras enable docker
  - yum install -y amazon-ecr-credential-helper
  - mkdir -p ~/.docker
  - echo '{"credsStore":"ecr-login"}' > ~/.docker/config.json

  - echo "****************************"
  - echo "*** installing SSM agent ***"
  - echo "****************************"
  - yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
  - systemctl enable amazon-ssm-agent
  - systemctl start amazon-ssm-agent

  - echo "*********************************"
  - echo "*** mounting instance storage ***"
  - echo "*********************************"
  - mkdir -p /mnt/storage
  - mkfs.ext4 -F -E nodiscard /dev/nvme0n1
  - mount -t ext4 /dev/nvme0n1 /mnt/storage

  - echo "****************************"
  - echo "*** installing New Relic ***"
  - echo "****************************"
  - wget "https://yum.newrelic.com/pub/newrelic/el5/$(uname -m | sed -e 's/i686/i386/g')/newrelic-repo-5-3.noarch.rpm"
  - rpm -Uvh newrelic-repo-5-3.noarch.rpm
  - yum install -y newrelic-sysmond

  - echo "*****************************"
  - echo "*** configuring New Relic ***"
  - echo "*****************************"
  - /usr/sbin/nrsysmond-config --set license_key=a26ab469b2d2b56766af51d1c8c436e736ba72bf

  - echo "**************************"
  - echo "*** starting New Relic ***"
  - echo "**************************"
  - /sbin/service newrelic-sysmond start

  - echo "*******************************************"
  - echo "*** installing New Relic Infrastructure ***"
  - echo "*******************************************"
  - curl -o /etc/yum.repos.d/newrelic-infra.repo https://download.newrelic.com/infrastructure_agent/linux/yum/el/7/x86_64/newrelic-infra.repo
  - yum -q makecache -y --disablerepo='*' --enablerepo='newrelic-infra'
  - yum install newrelic-infra -y
