resource "aws_kinesis_stream" "science_models_db" {
  name        = "${local.company}-${local.application}-${var.environment}"
  shard_count = 4

  lifecycle {
    ignore_changes = [shard_count]
  }

  tags = local.common_tags
}
