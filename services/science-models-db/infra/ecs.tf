data "aws_ami" "ecs_optimized" {
  most_recent = true
  owners      = [833713747344] # All science-models-db AMIs are owned by surfline-prod
  filter {
    name   = "name"
    values = ["surfline-science-models-db-*"]
  }
}

data "aws_s3_bucket" "science_bucket" {
  bucket = var.science_bucket
}

data "template_file" "ecs_instance_role_policy" {
  template = templatefile("${path.module}/resources/ecs-instance-role-policy.json", {
    science_bucket = data.aws_s3_bucket.science_bucket.arn
    science_prefix = local.science_prefix
  })
}

data "template_file" "science_models_db_user_data" {
  template = templatefile("${path.module}/resources/user_data.tpl", {
    ecs_cluster = aws_ecs_cluster.science_models_db.name
  })
}

resource "aws_ecs_cluster" "science_models_db" {
  name = "${local.company}-${local.application}-${var.environment}"
}

resource "aws_iam_role" "ecs_host_role" {
  name               = "${local.company}-ecs-host-${local.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs_host_role_policy" {
  name   = "${local.company}-ecs-instance-${local.application}-${var.environment}"
  policy = data.template_file.ecs_instance_role_policy.rendered
  role   = aws_iam_role.ecs_host_role.id
}

resource "aws_iam_instance_profile" "science_models_db" {
  name = "${local.company}-ecs-${local.application}-${var.environment}"
  role = aws_iam_role.ecs_host_role.name
}

resource "aws_security_group" "science_models_db_ecs" {
  name        = "${local.company}-sg-ecs-${local.application}-${var.environment}"
  description = "Allows all traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.ecs_tags
}

resource "aws_launch_configuration" "science_models_db" {
  name_prefix                 = "${local.company}-${local.application}-ecs-${var.environment}"
  image_id                    = data.aws_ami.ecs_optimized.id
  instance_type               = var.instance_type
  security_groups             = [var.internal_sg_group, aws_security_group.science_models_db_ecs.id]
  iam_instance_profile        = aws_iam_instance_profile.science_models_db.name
  key_name                    = var.key_name
  user_data                   = data.template_file.science_models_db_user_data.rendered
  associate_public_ip_address = false

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "science_models_db" {
  name                 = "${local.company}-${local.application}-ecs-${var.environment}"
  availability_zones   = var.availability_zones
  min_size             = var.min_instance_count
  max_size             = var.max_instance_count
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.science_models_db.name
  vpc_zone_identifier  = var.instance_subnets
  enabled_metrics      = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances"]

  tag {
    key                 = "Name" # names the launched instances
    value               = "${local.company}-ecs-${local.application}-${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Service"
    value               = "ecs"
    propagate_at_launch = true
  }

  tag {
    key                 = "Company"
    value               = local.common_tags["Company"]
    propagate_at_launch = true
  }

  tag {
    key                 = "Application"
    value               = local.common_tags["Application"]
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = local.common_tags["Environment"]
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = local.common_tags["Terraform"]
    propagate_at_launch = true
  }

  # type is used by the bootstrap script to locate other instances of same type
  tag {
    key                 = "type"
    value               = "${local.application}-ecs-host"
    propagate_at_launch = true
  }
}
