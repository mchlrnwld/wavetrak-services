module github.com/Surfline/wavetrak-services/services/science-models-db

go 1.12

require (
	github.com/Surfline/wavetrak-services/common/smdb v0.0.0-20190814171354-b3f9c6385c47
	github.com/aws/aws-sdk-go v1.29.12
	github.com/awslabs/kinesis-aggregation/go v0.0.0-20191028093912-c3e632da0703
	github.com/dgraph-io/badger v1.6.1-rc1
	github.com/go-errors/errors v1.0.1
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/harlow/kinesis-consumer v0.3.3
	github.com/newrelic/go-agent v2.2.0+incompatible
	github.com/stretchr/testify v1.4.0
	github.com/subosito/gotenv v1.1.1
)

replace gopkg.in/urfave/cli.v1 => github.com/urfave/cli v1.21.0

replace github.com/Surfline/wavetrak-services/common/smdb => ../../common/smdb/
