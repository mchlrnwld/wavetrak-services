package main

import (
	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	inv "github.com/Surfline/wavetrak-services/services/science-models-db/inventory"
	"github.com/Surfline/wavetrak-services/services/science-models-db/remoteinventory"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/newrelic/go-agent"
)

type service struct {
	nrApp     newrelic.Application
	inventory inv.Inventory
	session   *session.Session
}

type serviceOptions struct {
	dataPath        string
	session         *session.Session
	remoteInventory remoteinventory.RemoteInventory
}

func newService(options serviceOptions) (*service, error) {
	svc := &service{
		inventory: inv.New(
			options.dataPath,
			options.remoteInventory,
		),
		session: options.session,
	}

	nrApp, err := createNewRelicApp()
	if err != nil {
		return nil, errors.WrapError(err)
	}

	svc.nrApp = nrApp

	return svc, nil
}
