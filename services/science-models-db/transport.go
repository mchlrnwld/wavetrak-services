package main

import (
	"context"
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/newrelic/go-agent/_integrations/nrgorilla/v1"
)

/*
GET  /health    health check
GET  /inventory gets list of model runs loaded
GET  /records   gets record data
*/
func makeHTTPHandler(svc service) (*mux.Router, error) {
	r := mux.NewRouter()
	e := MakeServerEndpoints(svc)
	options := []httptransport.ServerOption{
		httptransport.ServerErrorEncoder(errors.ErrorEncoder),
	}

	getInventoryHandler := httptransport.NewServer(
		e.GetInventoryEndpoint,
		decodeEmptyRequest,
		encodeResponse,
		options...,
	)
	getRecordsHandler := httptransport.NewServer(
		e.GetRecordsEndpoint,
		decodeGetRecordsRequest,
		encodeResponse,
		options...,
	)

	r.Methods("GET").Path("/inventory").Handler(getInventoryHandler)
	r.Methods("GET").Path("/records").Handler(getRecordsHandler)

	// https://github.com/newrelic/go-agent/blob/master/_integrations/nrgorilla/v1/example/main.go
	r = nrgorilla.InstrumentRoutes(r, svc.nrApp)

	// Do not instrument /health endpoint for New Relic
	getHealthHandler := httptransport.NewServer(
		e.GetHealthEndpoint,
		decodeEmptyRequest,
		encodeResponse,
		options...,
	)
	r.Methods("GET").Path("/health").Handler(getHealthHandler)

	return r, nil
}

func decodeEmptyRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request emptyRequest
	return request, nil
}

func decodeGetRecordsRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	query := r.URL.Query()
	agency := query.Get("agency")
	model := query.Get("model")
	run := query.Get("run")
	grid := query.Get("grid")
	lat := query.Get("lat")
	lon := query.Get("lon")
	start := query.Get("start")
	end := query.Get("end")

	if agency == "" {
		return nil, errors.WrapError(errors.ValidationError{Message: "Agency is required"})
	}
	if model == "" {
		return nil, errors.WrapError(errors.ValidationError{Message: "Model is required"})
	}
	if run == "" {
		return nil, errors.WrapError(errors.ValidationError{Message: "Run is required"})
	}
	if grid == "" {
		return nil, errors.WrapError(errors.ValidationError{Message: "Grid is required"})
	}
	if lat == "" {
		return nil, errors.WrapError(errors.ValidationError{Message: "Latitude is required"})
	}
	if lon == "" {
		return nil, errors.WrapError(errors.ValidationError{Message: "Longitude is required"})
	}

	re := regexp.MustCompile("((19|20)\\d{2})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(00|06|12|18)")
	run64, err := strconv.ParseInt(run, 10, 64)
	if err != nil || !re.MatchString(run) {
		return nil, errors.WrapError(errors.ValidationError{Message: "Run must be a date/time in the format YYYYMMDDHH"})
	}

	lat64, err := strconv.ParseFloat(lat, 64)
	if err != nil || lat64 > 90 || lat64 < -90 {
		return nil, errors.WrapError(errors.ValidationError{Message: "Lat must be within -90 and 90 degrees"})
	}

	lon64, err := strconv.ParseFloat(lon, 64)
	if err != nil {
		return nil, errors.WrapError(errors.ValidationError{Message: "Lon must be in degrees"})
	}

	var startTime *time.Time
	if start != "" {
		start64, err := strconv.ParseInt(start, 10, 64)
		if err != nil {
			return nil, errors.WrapError(errors.ValidationError{Message: "Start must be Unix seconds from epoch"})
		}
		utc := time.Unix(start64, 0).UTC()
		startTime = &utc
	}

	var endTime *time.Time
	if end != "" {
		end64, err := strconv.ParseInt(end, 10, 64)
		if err != nil {
			return nil, errors.WrapError(errors.ValidationError{Message: "End must be Unix seconds from epoch"})
		}
		utc := time.Unix(end64, 0).UTC()
		endTime = &utc
	}

	return getRecordsRequest{
		Agency:    agency,
		Model:     model,
		Grid:      grid,
		Run:       uint32(run64),
		Latitude:  lat64,
		Longitude: lon64,
		Start:     startTime,
		End:       endTime,
	}, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
