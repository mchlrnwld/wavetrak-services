package backups

import (
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// Downloader manages downloading backup files from S3 to file system.
type Downloader interface {
	Download(string) error
}

type downloader struct {
	dir        string
	bucket     string
	prefix     string
	downloader *s3manager.Downloader
}

func (d downloader) Download(fileKey string) error {
	localPath := path.Join(d.dir, strings.Replace(fileKey, d.prefix, "", 1))
	if err := os.MkdirAll(filepath.Dir(localPath), os.ModePerm); err != nil {
		return errors.WrapError(err)
	}
	f, err := os.Create(localPath)
	if err != nil {
		return errors.WrapError(err)
	}
	defer f.Close()

	if _, err := d.downloader.Download(f, &s3.GetObjectInput{
		Bucket: aws.String(d.bucket),
		Key:    aws.String(fileKey),
	}); err != nil {
		return errors.WrapError(err)
	}

	return nil
}

// Manager allows creating new downloaders or listing files for downloading.
type Manager interface {
	NewDownloader(string) Downloader
	ListFileKeys(models.ModelRun) ([]string, error)
}

type manager struct {
	session  *session.Session
	s3Client *s3.S3
	bucket   string
	prefix   string
}

func (m manager) NewDownloader(dir string) Downloader {
	return downloader{
		dir:    dir,
		bucket: m.bucket,
		prefix: m.prefix,
		// Optimal settings for downloading large files.
		// https://docs.aws.amazon.com/AmazonS3/latest/dev/optimizing-performance-design-patterns.html#optimizing-performance-parallelization
		downloader: s3manager.NewDownloader(m.session, func(d *s3manager.Downloader) {
			d.Concurrency = 8
			d.PartSize = 1024 * 1024 * 8
		}),
	}
}

func (m manager) ListFileKeys(modelRun models.ModelRun) ([]string, error) {
	listObjectsOutput, err := m.s3Client.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket: aws.String(m.bucket),
		Prefix: aws.String(path.Join(m.prefix, modelRun.Agency, modelRun.Model, strconv.Itoa(int(modelRun.Run)))),
	})
	if err != nil {
		return nil, errors.WrapError(err)
	}

	fileKeys := make([]string, 0, len(listObjectsOutput.Contents))
	for _, object := range listObjectsOutput.Contents {
		if object.Key != nil {
			fileKeys = append(fileKeys, *object.Key)
		}
	}

	return fileKeys, nil
}

// New creates a new backups Manager.
func New(sess *session.Session, bucket, prefix string) Manager {
	return manager{
		session:  sess,
		s3Client: s3.New(sess),
		bucket:   bucket,
		prefix:   prefix,
	}
}
