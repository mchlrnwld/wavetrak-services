package kinesis

import (
	"context"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/awslabs/kinesis-aggregation/go/deaggregator"
	"github.com/harlow/kinesis-consumer"
	store "github.com/harlow/kinesis-consumer/store/ddb"
)

// Record is an alias for kinesis.Record from the AWS SDK
type Record = kinesis.Record

type logger struct{}

func (l *logger) Log(args ...interface{}) {
	log.Println(args...)
}

// StoreOptions to use for storing checkpoint store.
type StoreOptions struct {
	AppName   string
	TableName string
}

// SubscribeOptions to use to subscribe to a Kinesis stream.
type SubscribeOptions struct {
	Session            *session.Session
	StreamName         string
	AppName            string
	TableName          string
	RecordsCallback    func([]*kinesis.Record) error
	TransactionWrapper func(*kinesis.Record, func(*kinesis.Record) error) error
}

// Subscribe begins polling Kinesis Data Stream for records and executes recordsCallback records.
func Subscribe(options SubscribeOptions) error {
	// use cancel func to signal shutdown
	ctx, cancel := context.WithCancel(context.Background())

	// trap SIGINT, wait to trigger shutdown
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	go func() {
		<-signals
		cancel()
	}()

	// Use DynamoDB to store shard checkpoints. See documentation:
	// - https://github.com/harlow/kinesis-consumer/tree/v0.3.3#store
	// - https://github.com/harlow/kinesis-consumer/tree/v0.3.3#dynamodb
	store, err := store.New(
		options.AppName,
		options.TableName,
		store.WithDynamoClient(dynamodb.New(options.Session)),
	)
	if err != nil {
		return errors.WrapError(err)
	}

	opts := []consumer.Option{
		consumer.WithClient(kinesis.New(options.Session)),
		consumer.WithStore(store),
		consumer.WithTimestamp(time.Now().Add(-time.Hour)),
	}
	if os.Getenv("APP_ENV") == "development" {
		opts = append(opts, consumer.WithLogger(new(logger)))
	}

	cons, err := consumer.New(
		options.StreamName,
		opts...,
	)
	if err != nil {
		return errors.WrapError(err)
	}

	log.Printf("Subscribing to Kinesis Stream: %s", options.StreamName)
	if options.TransactionWrapper == nil {
		options.TransactionWrapper = func(r *kinesis.Record, f func(*kinesis.Record) error) error {
			return f(r)
		}
	}
	err = cons.Scan(ctx, func(r *consumer.Record) error {
		return options.TransactionWrapper(r, func(*consumer.Record) error {
			records, err := deaggregator.DeaggregateRecords([]*consumer.Record{r})
			if err != nil {
				return errors.WrapError(err)
			}

			if err := options.RecordsCallback(records); err != nil {
				return errors.WrapError(err)
			}

			return nil
		})
	})
	if err != nil {
		return errors.WrapError(err)
	}

	return nil
}
