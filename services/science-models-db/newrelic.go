package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	newrelic "github.com/newrelic/go-agent"
)

func createNewRelicApp() (newrelic.Application, error) {
	appName := "Wavetrak - Science Models DB"
	if appEnv := os.Getenv("APP_ENV"); appEnv != "production" {
		appName = fmt.Sprintf("%v %v", appName, appEnv)
	}
	config := newrelic.NewConfig(appName, os.Getenv("NEW_RELIC_LICENSE_KEY"))
	config.Logger = newrelic.NewLogger(os.Stdout)
	config.Enabled = os.Getenv("NEW_RELIC_ENABLED") == "true"
	config.ErrorCollector.IgnoreStatusCodes = []int{
		http.StatusNotFound,
		http.StatusBadRequest,
	}
	config.TransactionTracer.Enabled = true
	app, err := newrelic.NewApplication(config)
	if err != nil {
		return nil, errors.WrapError(err)
	}
	return app, nil
}
