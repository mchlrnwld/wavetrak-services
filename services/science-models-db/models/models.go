package models

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
)

// ModelGQLResponse represents a response from a GraphQL query for models.
type ModelGQLResponse struct {
	Data struct {
		Models []struct {
			Agency string `json:"agency"`
			Model  string `json:"model"`
			Runs   []struct {
				Run uint32 `json:"run"`
			} `json:"runs"`
		} `json:"models"`
	} `json:"data"`
}

// ModelRun represents a single run for a data model.
type ModelRun struct {
	Agency string `json:"agency"`
	Model  string `json:"model"`
	Run    uint32 `json:"run"`
}

// Key returns a string representation of a model run to be used as the database key.
func (m ModelRun) Key() string {
	return fmt.Sprintf("%v/%v/%v", m.Agency, m.Model, m.Run)
}

// ModelRunFromKey creates a ModelRun from a database key.
func ModelRunFromKey(key string) (ModelRun, error) {
	var modelRun ModelRun
	attributes := strings.Split(key, "/")
	if len(attributes) < 3 {
		return modelRun, errors.WrapError(fmt.Errorf("Key %v does not have 3 attributes", key))
	}

	run64, err := strconv.ParseUint(attributes[2], 10, 32)
	if err != nil {
		return modelRun, errors.WrapError(err)
	}

	modelRun.Agency = attributes[0]
	modelRun.Model = attributes[1]
	modelRun.Run = uint32(run64)

	return modelRun, nil
}

// ModelRunFromJSON parses JSON bytes into a ModelRun.
func ModelRunFromJSON(data []byte) (ModelRun, error) {
	var modelRun ModelRun
	if err := json.Unmarshal(data, &modelRun); err != nil {
		return modelRun, errors.WrapError(err)
	}
	return modelRun, nil
}

// ModelRunsFromGQL parses GraphQL response JSON bytes into ModelRuns.
func ModelRunsFromGQL(data []byte) ([]ModelRun, error) {
	var modelGQLResponse ModelGQLResponse
	if err := json.Unmarshal(data, &modelGQLResponse); err != nil {
		return nil, errors.WrapError(err)
	}
	modelRuns := make([]ModelRun, 0)
	for _, model := range modelGQLResponse.Data.Models {
		for _, run := range model.Runs {
			modelRuns = append(modelRuns, ModelRun{
				Agency: model.Agency,
				Model:  model.Model,
				Run:    run.Run,
			})
		}
	}
	return modelRuns, nil
}
