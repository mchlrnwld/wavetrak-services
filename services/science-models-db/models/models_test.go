package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKey(t *testing.T) {
	modelRun := ModelRun{
		Agency: "Wavetrak",
		Model:  "Lotus-WW3",
		Run:    2019090912,
	}
	assert.Equal(t, "Wavetrak/Lotus-WW3/2019090912", modelRun.Key())
}

func TestModelRunFromKey(t *testing.T) {
	key := "Wavetrak/Lotus-WW3/2019090912"
	modelRun, err := ModelRunFromKey(key)
	assert.NoError(t, err)
	assert.Equal(t, ModelRun{
		Agency: "Wavetrak",
		Model:  "Lotus-WW3",
		Run:    2019090912,
	}, modelRun)
}

func TestModelRunFromJSON(t *testing.T) {
	data := []byte("{\"Agency\":\"NOAA\",\"Model\":\"GFS\",\"Run\":2019093012}")
	modelRun, err := ModelRunFromJSON(data)
	assert.NoError(t, err)
	assert.Equal(t, ModelRun{
		Agency: "NOAA",
		Model:  "GFS",
		Run:    2019093012,
	}, modelRun)
}

func TestModelRunsFromGQL(t *testing.T) {
	data := []byte("{\"data\":{\"models\":[{\"agency\":\"NOAA\",\"model\":\"WW3-Ensemble\",\"runs\":[]},{\"agency\":\"NASA\",\"model\":\"MUR-SST\",\"runs\":[]},{\"agency\":\"NOAA\",\"model\":\"GFS\",\"runs\":[{\"run\":2020030512},{\"run\":2020030518}]},{\"agency\":\"NOAA\",\"model\":\"WW3\",\"runs\":[]},{\"agency\":\"Wavetrak\",\"model\":\"LOLA-WW3\",\"runs\":[]},{\"agency\":\"Wavetrak\",\"model\":\"Lotus-WW3\",\"runs\":[{\"run\":2020030700}]},{\"agency\":\"Wavetrak\",\"model\":\"Proteus-WW3\",\"runs\":[]},{\"agency\":\"Wavetrak\",\"model\":\"WRF\",\"runs\":[]}]}}")
	modelRuns, err := ModelRunsFromGQL(data)
	assert.NoError(t, err)
	assert.Equal(t, []ModelRun{
		ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2020030512,
		},
		ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2020030518,
		},
		ModelRun{
			Agency: "Wavetrak",
			Model:  "Lotus-WW3",
			Run:    2020030700,
		},
	}, modelRuns)
}
