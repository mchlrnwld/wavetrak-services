package remoteinventory

import (
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// Item in the inventory.
type Item struct {
	InstanceID   string `json:"instance_id"`
	Database     string `json:"database"`
	ReadOnly     bool   `json:"read_only"`
	TTLTimestamp *int   `json:"ttl_timestamp"`
}

// RemoteInventory interface for managing a remote inventory of databases.
type RemoteInventory interface {
	GetDBs() ([]Item, error)
	PutDB(string, bool) error
	DeleteDB(string) error
}

type ddbInventory struct {
	client     *dynamodb.DynamoDB
	instanceID string
	tableName  string
}

func (dbi ddbInventory) GetDBs() ([]Item, error) {
	queryInput := &dynamodb.QueryInput{
		ConsistentRead:         aws.Bool(true),
		TableName:              aws.String(dbi.tableName),
		KeyConditionExpression: aws.String("instance_id = :instance_id"),
		ProjectionExpression:   aws.String("instance_id, #database, read_only"),
		ExpressionAttributeNames: map[string]*string{
			"#database": aws.String("database"), // Required because "database" is a keyword
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":instance_id": &dynamodb.AttributeValue{
				S: aws.String(dbi.instanceID),
			},
		},
	}
	queryOutput, err := dbi.client.Query(queryInput)
	if err != nil {
		return nil, errors.WrapError(err)
	}

	var items []Item
	if err := dynamodbattribute.UnmarshalListOfMaps(queryOutput.Items, &items); err != nil {
		return nil, errors.WrapError(err)
	}

	return items, nil
}

func (dbi ddbInventory) PutDB(key string, readOnly bool) error {
	item := Item{
		InstanceID:   dbi.instanceID,
		Database:     key,
		ReadOnly:     readOnly,
		TTLTimestamp: aws.Int(int(time.Now().AddDate(0, 0, 7).Unix())),
	}
	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return errors.WrapError(err)
	}

	putItemInput := &dynamodb.PutItemInput{
		TableName: aws.String(dbi.tableName),
		Item:      av,
	}
	if _, err := dbi.client.PutItem(putItemInput); err != nil {
		return errors.WrapError(err)
	}

	return nil
}

func (dbi ddbInventory) DeleteDB(key string) error {
	deleteItemInput := &dynamodb.DeleteItemInput{
		TableName: aws.String(dbi.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"instance_id": &dynamodb.AttributeValue{
				S: aws.String(dbi.instanceID),
			},
			"database": &dynamodb.AttributeValue{
				S: aws.String(key),
			},
		},
	}
	if _, err := dbi.client.DeleteItem(deleteItemInput); err != nil {
		return errors.WrapError(err)
	}
	return nil
}

// New creates remote database inventory stored in DynamoDB.
func New(sess *session.Session, instanceID, tableName string) RemoteInventory {
	return ddbInventory{
		client:     dynamodb.New(sess),
		instanceID: instanceID,
		tableName:  tableName,
	}
}
