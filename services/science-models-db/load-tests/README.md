# Load Tests

Load tests using [k6](https://k6.io/).

## Quickstart

```sh
$ k6 run -e HOST=science-models-db.prod.surfline.com -e MODEL_RUN_TIME=2019050106 --out json=results.json test.js
```
