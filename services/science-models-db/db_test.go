package main

import (
	"testing"
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	th "github.com/Surfline/wavetrak-services/services/science-models-db/testhelpers"
	"github.com/stretchr/testify/assert"
)

func TestTurnDBOnline(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		svc, err := newService(serviceOptions{
			dataPath: dir,
		})
		assert.NoError(t, err)

		modelRun := models.ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2019101700,
		}
		_, err = svc.inventory.AddDB(modelRun, false)
		assert.NoError(t, err)

		err = turnDBOnline(*svc, modelRun, time.Millisecond, time.Second)
		assert.NoError(t, err)

		db, ok := svc.inventory.Database(modelRun)
		assert.True(t, ok)
		assert.True(t, db.ReadOnly)
	})
}

func TestTurnDBOnlineTimeoutError(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		svc, err := newService(serviceOptions{
			dataPath: dir,
		})
		assert.NoError(t, err)

		modelRun := models.ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2019101700,
		}
		_, err = svc.inventory.AddDB(modelRun, false)
		assert.NoError(t, err)

		err = turnDBOnline(*svc, modelRun, time.Second, time.Millisecond)
		assert.Error(t, err)

		db, ok := svc.inventory.Database(modelRun)
		assert.True(t, ok)
		assert.False(t, db.ReadOnly)
	})
}

func TestPruneOlderDBs(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		svc, err := newService(serviceOptions{
			dataPath: dir,
		})
		assert.NoError(t, err)

		modelRuns := []models.ModelRun{
			{
				Agency: "NOAA",
				Model:  "GFS",
				Run:    2019101618,
			},
			{
				Agency: "NOAA",
				Model:  "GFS",
				Run:    2019101700,
			},
			{
				Agency: "NOAA",
				Model:  "GFS",
				Run:    2019101706,
			},
			{
				Agency: "NOAA",
				Model:  "GFS",
				Run:    2019101712,
			},
			{
				Agency: "Wavetrak",
				Model:  "Lotus-WW3",
				Run:    2019101700,
			},
		}

		// Setup ReadOnly databases for each model run
		for _, modelRun := range modelRuns {
			_, err = svc.inventory.AddDB(modelRun, false)
			assert.NoError(t, err)
			// Leave one run as writeable to test it is also removed
			if modelRun.Run != 2019101618 {
				err = turnDBOnline(*svc, modelRun, time.Millisecond, time.Second)
				assert.NoError(t, err)
			}
		}

		err = pruneOlderDBs(*svc, models.ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2019101706,
		})
		assert.NoError(t, err)

		dbMap := svc.inventory.Databases()
		assert.Len(t, dbMap, len(modelRuns)-2)

		var ok bool
		_, ok = svc.inventory.Database(models.ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2019101700,
		})
		assert.False(t, ok)

		_, ok = svc.inventory.Database(models.ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2019101618,
		})
		assert.False(t, ok)
	})
}
