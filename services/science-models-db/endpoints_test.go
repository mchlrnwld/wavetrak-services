package main

import (
	"fmt"
	"path"
	"strconv"
	"testing"

	"github.com/Surfline/wavetrak-services/services/science-models-db/database"
	inv "github.com/Surfline/wavetrak-services/services/science-models-db/inventory"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	th "github.com/Surfline/wavetrak-services/services/science-models-db/testhelpers"
	"github.com/stretchr/testify/assert"
)

func seedDB(dbPath string, inventory *inv.Inventory, modelRun models.ModelRun, readOnly bool) (*database.Database, error) {
	dir := path.Join(dbPath, modelRun.Key())
	db, err := database.New(dir, modelRun, false)
	if err != nil {
		return nil, err
	}
	if err := db.Close(); err != nil {
		return nil, err
	}
	if inventory != nil {
		return (*inventory).AddDB(modelRun, readOnly)
	}
	return nil, nil
}

func TestGetInventory(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		var res getInventoryResponse
		var err error
		var svc service

		inventory := inv.New(dir, nil)
		gfsdb, err := seedDB(dir, &inventory, models.ModelRun{
			Agency: "NOAA",
			Model:  "GFS",
			Run:    2019013106,
		}, true)
		assert.NoError(t, err)
		lotusdb, err := seedDB(dir, &inventory, models.ModelRun{
			Agency: "Wavetrak",
			Model:  "Lotus-WW3",
			Run:    2019013100,
		}, true)
		assert.NoError(t, err)

		// GetInventory returns modelKey
		svc = service{
			inventory: inventory,
		}
		res, err = svc.GetInventory()
		assert.NoError(t, err)
		assert.Len(t, res, 2)
		assert.Contains(t, res, gfsdb.ModelRun)
		assert.Contains(t, res, lotusdb.ModelRun)
	})
}

type testType int

func (ts testType) String() string {
	return fmt.Sprintf("Test Type: %s", strconv.FormatInt(int64(ts), 10))
}

func TestGetJSONType(t *testing.T) {
	var jsonType string
	var transform func(interface{}) (interface{}, error)
	var tValue interface{}
	var err error

	sampleBool := true
	jsonType, transform = getJSONType(sampleBool)
	assert.Equal(t, "boolean", jsonType)
	assert.Nil(t, transform)

	sampleFloat32 := float32(1.5)
	jsonType, transform = getJSONType(sampleFloat32)
	assert.Equal(t, "number", jsonType)
	assert.Nil(t, transform)

	sampleFloat64 := float64(1.5)
	jsonType, transform = getJSONType(sampleFloat64)
	assert.Equal(t, "number", jsonType)
	assert.Nil(t, transform)

	sampleTestType := testType(1)
	jsonType, transform = getJSONType(sampleTestType)
	assert.Equal(t, "string", jsonType)
	tValue, err = transform(sampleTestType)
	assert.NoError(t, err)
	assert.Equal(t, "Test Type: 1", tValue)
}
