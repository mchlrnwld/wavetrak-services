package database

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/Surfline/wavetrak-services/common/smdb/pkg/schemas"
	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/dgraph-io/badger"
	"github.com/dgraph-io/badger/options"
)

// KeyValue represents a raw Badger DB record.
type KeyValue struct {
	Key   []byte
	Value []byte
}

// Database contains metadata for a model run database.
type Database struct {
	ModelRun     models.ModelRun
	DB           *badger.DB
	DecodeRecord func([]byte) (interface{}, error)
	ReadOnly     bool
	LastWrite    time.Time
}

// Record in a database.
type Record struct {
	Timestamp time.Time
	Member    *uint32
	Value     interface{}
}

// Close removes a database connection.
func (db Database) Close() error {
	if err := db.DB.Close(); err != nil {
		return errors.WrapError(err)
	}
	return nil
}

// GetRecords queries a database for records matching the grid, latitude, longitude, and within the start and end times.
func (db Database) GetRecords(grid string, latitude, longitude float64, start, end *time.Time) ([]Record, error) {
	records := []Record{}

	err := db.DB.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		iter := txn.NewIterator(opts)
		defer iter.Close()
		prefix := []byte(fmt.Sprintf("%v,%v,%v,", grid, latitude, longitude))
		for iter.Seek(prefix); iter.ValidForPrefix(prefix); iter.Next() {
			item := iter.Item()
			key := item.Key()
			keyItems := strings.Split(string(key[len(prefix):]), ",")
			timestamp, err := time.Parse(time.RFC3339, keyItems[0])
			if err != nil {
				return errors.WrapError(err)
			}
			var member *uint32
			if len(keyItems) > 1 {
				member64, err := strconv.ParseUint(keyItems[1], 10, 32)
				if err != nil {
					return errors.WrapError(err)
				}
				member32 := uint32(member64)
				member = &member32
			}
			if start != nil && timestamp.Before(*start) {
				continue
			}

			if end != nil && timestamp.After(*end) {
				break
			}

			vErr := item.Value(func(b []byte) error {
				value, pErr := db.DecodeRecord(b)
				if pErr != nil {
					return errors.WrapError(pErr)
				}
				records = append(records, Record{
					Timestamp: timestamp,
					Member:    member,
					Value:     value,
				})
				return nil
			})
			if vErr != nil {
				return errors.WrapError(vErr)
			}
		}
		return nil
	})

	if err != nil {
		return nil, errors.WrapError(err)
	}

	return records, nil
}

// SetRecords writes key value records to the Database.
func (db Database) SetRecords(kvs []KeyValue) error {
	if db.ReadOnly {
		return errors.WrapError(fmt.Errorf("Cannot set records to read only database: %+v", db.ModelRun))
	}

	txn := db.DB.NewTransaction(true)
	defer txn.Discard()
	for _, kv := range kvs {
		if err := txn.Set(kv.Key, kv.Value); err != nil {
			return errors.WrapError(err)
		}
	}
	if err := txn.Commit(); err != nil {
		return errors.WrapError(err)
	}
	db.LastWrite = time.Now()
	return nil
}

// TODO: Move these decoders into common/smdb/pkg/schemas alongside encoders.
func decodeWeatherRecord(b []byte) (interface{}, error) {
	var value schemas.WeatherRecord
	buf := bytes.NewReader(b)
	if rErr := binary.Read(buf, binary.BigEndian, &value); rErr != nil {
		return nil, errors.WrapError(rErr)
	}
	return value, nil
}

func decodeSwellsRecord(b []byte) (interface{}, error) {
	var value schemas.SwellsRecord
	buf := bytes.NewReader(b)
	if rErr := binary.Read(buf, binary.BigEndian, &value); rErr != nil {
		return nil, errors.WrapError(rErr)
	}
	return value, nil
}

var recordDecoders = map[string](func([]byte) (interface{}, error)){
	"NOAA/GFS":           decodeWeatherRecord,
	"Wavetrak/Lotus-WW3": decodeSwellsRecord,
}

func getRecordDecoder(agency, model string) (func([]byte) (interface{}, error), bool) {
	key := fmt.Sprintf("%v/%v", agency, model)
	recordDecoder, ok := recordDecoders[key]
	return recordDecoder, ok
}

type noopLog struct{}

func (l *noopLog) Errorf(f string, v ...interface{})   {}
func (l *noopLog) Warningf(f string, v ...interface{}) {}
func (l *noopLog) Infof(f string, v ...interface{})    {}
func (l *noopLog) Debugf(f string, v ...interface{})   {}

var noopLogger = new(noopLog)

// NoDecoderFound is a constant to specify an error when a decoder is not found.
const NoDecoderFound = "No decoder found"

// UnableToOpenDatabase is a constant to specify an error when unable to open a database.
const UnableToOpenDatabase = "Unable to open database"

// New creates a new database.
func New(dir string, modelRun models.ModelRun, readOnly bool) (*Database, error) {
	decodeRecord, ok := getRecordDecoder(modelRun.Agency, modelRun.Model)
	if !ok {
		return nil, errors.WrapError(fmt.Errorf("%s for %+v", NoDecoderFound, modelRun))
	}

	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return nil, errors.WrapError(err)
	}

	opts := badger.DefaultOptions(dir).WithReadOnly(readOnly).WithValueLogLoadingMode(options.FileIO).WithTableLoadingMode(options.FileIO)

	if !readOnly && os.Getenv("APP_ENV") != "development" {
		opts = opts.WithLogger(noopLogger)
	}

	db, err := badger.Open(opts)
	if err != nil {
		return nil, errors.WrapError(fmt.Errorf("%s %+v: %v", UnableToOpenDatabase, modelRun, err))
	}

	return &Database{
		ModelRun:     modelRun,
		DB:           db,
		DecodeRecord: decodeRecord,
		ReadOnly:     readOnly,
		LastWrite:    time.Now(),
	}, nil
}
