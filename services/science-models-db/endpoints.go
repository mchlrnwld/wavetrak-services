package main

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/database"
	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/go-kit/kit/endpoint"
)

/*
Endpoints collects all the endpoints that compose the
science models db service
*/
type Endpoints struct {
	GetHealthEndpoint    endpoint.Endpoint
	GetInventoryEndpoint endpoint.Endpoint
	GetRecordsEndpoint   endpoint.Endpoint
}

/*
MakeServerEndpoints returns an Endpoints struct
Each endpoint invokes the corresponding method on the service
*/
func MakeServerEndpoints(svc service) Endpoints {
	return Endpoints{
		GetHealthEndpoint:    makeGetHealthEndpoint(svc),
		GetInventoryEndpoint: makeGetInventoryEndpoint(svc),
		GetRecordsEndpoint:   makeGetRecordsEndpoint(svc),
	}
}

func (svc service) GetInventory() (getInventoryResponse, error) {
	dbs := svc.inventory.Databases()
	inventory := make([]models.ModelRun, 0, len(dbs))
	for _, db := range dbs {
		if db.ReadOnly {
			inventory = append(inventory, db.ModelRun)
		}
	}

	sort.Slice(inventory, func(i, j int) bool {
		a := inventory[i]
		b := inventory[j]
		if a.Agency < b.Agency {
			return true
		}

		if a.Agency > b.Agency {
			return false
		}

		if a.Model < b.Model {
			return true
		}

		if a.Model > b.Model {
			return false
		}

		return a.Run < b.Run
	})

	return inventory, nil
}

func (svc service) GetRecords(
	agency string,
	model string,
	grid string,
	run uint32,
	latitude float64,
	longitude float64,
	start *time.Time,
	end *time.Time,
) ([]database.Record, error) {
	db, ok := svc.inventory.Database(models.ModelRun{
		Agency: agency,
		Model:  model,
		Run:    run,
	})
	if !ok {
		return nil, errors.WrapError(errors.ValidationError{Message: "Valid agency, model, run required"})
	}

	records, err := db.GetRecords(grid, latitude, longitude, start, end)
	if err != nil {
		return nil, errors.WrapError(err)
	}
	return records, nil
}

type emptyRequest struct{}

type messageResponse struct {
	Message string `json:"message"`
}

type getInventoryResponse []models.ModelRun

type getRecordsRequest struct {
	Agency    string
	Model     string
	Grid      string
	Run       uint32
	Latitude  float64
	Longitude float64
	Start     *time.Time
	End       *time.Time
}

type getRecordsResponse struct {
	Headers []string      `json:"headers"`
	Types   []string      `json:"types"`
	Values  []interface{} `json:"values"`
}

func makeGetHealthEndpoint(svc service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		return messageResponse{Message: "OK"}, nil
	}
}

func makeGetInventoryEndpoint(svc service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		return svc.GetInventory()
	}
}

const (
	s = "string"
	b = "boolean"
	n = "number"
)

func toString(value interface{}) (interface{}, error) {
	return fmt.Sprint(value), nil
}

func getJSONType(value interface{}) (string, func(interface{}) (interface{}, error)) {
	typeName := reflect.TypeOf(value).Name()
	switch typeName {
	case "bool":
		return b, nil
	case "float32":
		return n, nil
	case "float64":
		return n, nil
	default:
		return s, toString
	}
}
func makeGetRecordsEndpoint(svc service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(getRecordsRequest)
		records, err := svc.GetRecords(
			req.Agency,
			req.Model,
			req.Grid,
			req.Run,
			req.Latitude,
			req.Longitude,
			req.Start,
			req.End,
		)
		if err != nil {
			return nil, errors.WrapError(err)
		}

		headers := []string{}
		types := []string{}
		transforms := [](func(interface{}) (interface{}, error)){}
		values := []interface{}{}

		if len(records) > 0 {
			headers = append(headers, "Timestamp")
			types = append(types, "number")

			if records[0].Member != nil {
				headers = append(headers, "Member")
				types = append(types, "number")
			}

			t := reflect.ValueOf(records[0].Value).Type()
			elem := reflect.Indirect(reflect.ValueOf(&records[0].Value)).Elem()

			for i := 0; i < t.NumField(); i++ {
				h := t.Field(i).Name
				headers = append(headers, h)

				value := elem.Field(i).Interface()
				jsonType, transform := getJSONType(value)
				types = append(types, jsonType)
				transforms = append(transforms, transform)
			}

			for _, record := range records {
				var set []interface{}
				set = append(set, record.Timestamp.Unix())
				if record.Member != nil {
					set = append(set, record.Member)
				}
				elem := reflect.Indirect(reflect.ValueOf(&record.Value)).Elem()
				for i := 0; i < t.NumField(); i++ {
					value := elem.Field(i).Interface()
					transform := transforms[i]
					if transform != nil {
						tValue, err := transform(value)
						if err != nil {
							return nil, errors.WrapError(err)
						}
						set = append(set, tValue)
					} else {
						set = append(set, value)
					}
				}
				values = append(values, set)
			}
		}

		return getRecordsResponse{Headers: headers, Types: types, Values: values}, nil
	}
}
