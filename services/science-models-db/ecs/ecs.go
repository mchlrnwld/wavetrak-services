package ecs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecs"
)

type metaDataJSON struct {
	Cluster              string `json:"Cluster"`
	ContainerInstanceARN string `json:"ContainerInstanceARN"`
}

func getECSContainerMetaData(metaDataFilePath string) (*metaDataJSON, error) {
	metaDataBytes, err := ioutil.ReadFile(metaDataFilePath)
	if err != nil {
		return nil, errors.WrapError(err)
	}

	var metaData metaDataJSON
	if err := json.Unmarshal(metaDataBytes, &metaData); err != nil {
		return nil, errors.WrapError(err)
	}

	return &metaData, nil
}

// GetECSContainerEC2InstanceID looks up and returns the ECS container's host EC2 instance ID.
func GetECSContainerEC2InstanceID(sess *session.Session, metaDataFilePath string) (string, error) {
	metaData, err := getECSContainerMetaData(metaDataFilePath)
	if err != nil {
		return "", errors.WrapError(err)
	}

	client := ecs.New(sess)
	input := &ecs.DescribeContainerInstancesInput{
		Cluster:            &metaData.Cluster,
		ContainerInstances: []*string{&metaData.ContainerInstanceARN},
	}
	containerInstances, err := client.DescribeContainerInstances(input)
	if err != nil {
		return "", errors.WrapError(err)
	}

	if len(containerInstances.ContainerInstances) < 1 || containerInstances.ContainerInstances[0].Ec2InstanceId == nil {
		return "", errors.WrapError(fmt.Errorf("Could not find EC2 instance ID for cluster %v and container instance ARN %v", metaData.Cluster, metaData.ContainerInstanceARN))
	}

	return *containerInstances.ContainerInstances[0].Ec2InstanceId, nil
}
