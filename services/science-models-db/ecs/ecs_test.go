package ecs

import (
	"io/ioutil"
	"os"
	"path"
	"testing"

	th "github.com/Surfline/wavetrak-services/services/science-models-db/testhelpers"
	"github.com/stretchr/testify/assert"
)

func TestGetECSContainerMetaData(t *testing.T) {
	th.WithTempDir(t, func(dir string) {
		metaDataPath := path.Join(dir, "metadata.json")
		metaDataJSON := "{\"Cluster\":\"test-cluster\",\"ContainerInstanceARN\":\"test-container-instance-arn\"}"
		err := ioutil.WriteFile(metaDataPath, []byte(metaDataJSON), os.ModePerm)
		assert.NoError(t, err)

		metaData, err := getECSContainerMetaData(metaDataPath)
		assert.NoError(t, err)
		assert.Equal(t, "test-cluster", metaData.Cluster)
		assert.Equal(t, "test-container-instance-arn", metaData.ContainerInstanceARN)
	})
}
