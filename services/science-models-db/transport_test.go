package main

import (
	"reflect"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestMakeHTTPHandler(t *testing.T) {
	expected := mux.NewRouter()

	var svc service
	handler, err := makeHTTPHandler(svc)

	assert.NoError(t, err)
	assert.Equal(t, reflect.TypeOf(handler), reflect.TypeOf(expected), "makeHTTPHandler should return an item of type *mux.Router")
}
