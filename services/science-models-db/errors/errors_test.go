package errors

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/newrelic/go-agent"
	"github.com/stretchr/testify/mock"
)

type httpResponseWriterMock struct {
	mock.Mock
}

func (w httpResponseWriterMock) Header() http.Header {
	args := w.Called()
	return args.Get(0).(http.Header)
}

func (w httpResponseWriterMock) Write(body []byte) (int, error) {
	args := w.Called(body)
	return args.Int(0), args.Error(1)
}

func (w httpResponseWriterMock) WriteHeader(statusCode int) {
	w.Called(statusCode)
}

type newrelicTransactionMock struct {
	httpResponseWriterMock
}

func (txn newrelicTransactionMock) End() error {
	args := txn.Called()
	return args.Error(0)
}

func (txn newrelicTransactionMock) Ignore() error {
	args := txn.Called()
	return args.Error(0)
}

func (txn newrelicTransactionMock) SetName(name string) error {
	args := txn.Called(name)
	return args.Error(0)
}

func (txn newrelicTransactionMock) NoticeError(err error) error {
	args := txn.Called(err)
	return args.Error(0)
}

func (txn newrelicTransactionMock) AddAttribute(key string, value interface{}) error {
	args := txn.Called(key, value)
	return args.Error(0)
}

func (txn newrelicTransactionMock) SetWebRequest(r newrelic.WebRequest) error {
	args := txn.Called(r)
	return args.Error(0)
}

func (txn newrelicTransactionMock) StartSegmentNow() newrelic.SegmentStartTime {
	args := txn.Called()
	return args.Get(0).(newrelic.SegmentStartTime)
}

func (txn newrelicTransactionMock) CreateDistributedTracePayload() newrelic.DistributedTracePayload {
	args := txn.Called()
	return args.Get(0).(newrelic.DistributedTracePayload)
}

func (txn newrelicTransactionMock) AcceptDistributedTracePayload(t newrelic.TransportType, payload interface{}) error {
	args := txn.Called(t, payload)
	return args.Error(0)
}

func TestErrorEncoder(t *testing.T) {
	err := WrapError(fmt.Errorf("Expected unknown error"))
	txn := newrelicTransactionMock{}
	txn.On("NoticeError", mock.Anything).Return(nil).Once()
	writer := httpResponseWriterMock{}
	writer.On("WriteHeader", 500).Return().Once()
	writer.On("Write", []byte("{\"error\":\"Error encountered\"}\n")).Return(0, nil).Once()
	ErrorEncoder(newrelic.NewContext(context.Background(), txn), err, writer)
	writer.AssertExpectations(t)

	vErr := WrapError(ValidationError{"Expected validation error"})
	vTxn := newrelicTransactionMock{}
	vWriter := httpResponseWriterMock{}
	vWriter.On("WriteHeader", 400).Return().Once()
	vWriter.On("Write", []byte("{\"error\":\"Expected validation error\"}\n")).Return(0, nil).Once()
	ErrorEncoder(newrelic.NewContext(context.Background(), vTxn), vErr, vWriter)
	vWriter.AssertExpectations(t)
}
