package errors

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-errors/errors"
	"github.com/newrelic/go-agent"
)

// ValidationError represents a 400 error.
type ValidationError struct{ Message string }

func (e ValidationError) Error() string { return e.Message }

// WrapError returns an error with stack trace added.
func WrapError(err error) error {
	if err != nil {
		return errors.Wrap(err, 1)
	}

	return nil
}

type errorWrapper struct {
	Error string `json:"error"`
}

// ErrorEncoder handles errors and sets the status code and response body for the error.
func ErrorEncoder(c context.Context, err error, w http.ResponseWriter) {
	statusCode := getErrorCode(err)
	w.WriteHeader(statusCode)

	var errMessage string
	if statusCode == http.StatusInternalServerError {
		wErr := WrapError(err).(*errors.Error)
		stack := wErr.ErrorStack()

		log.Printf("Error encountered: %v", stack)

		txn := newrelic.FromContext(c)
		nrErr := newrelic.Error{
			Message: wErr.Error(),
			Class:   wErr.TypeName(),
			Attributes: map[string]interface{}{
				"stack_trace": stack,
			},
		}
		if txnErr := txn.NoticeError(nrErr); txnErr != nil {
			log.Printf("Error recording error to New Relic: %v", WrapError(txnErr))
		}

		errMessage = "Error encountered"
	} else {
		errMessage = err.Error()
	}

	json.NewEncoder(w).Encode(errorWrapper{Error: errMessage})
}

func getErrorCode(err error) int {
	wErr := WrapError(err).(*errors.Error)
	switch wErr.Err.(type) {
	case ValidationError:
		return http.StatusBadRequest
	}
	return http.StatusInternalServerError
}
