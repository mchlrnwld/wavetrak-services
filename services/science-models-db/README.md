# Science Model Database

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents

- [Development](#development)
    - [Application environment variables](#application-environment-variables)
- [Quickstart](#quickstart)
- [Endpoints](#endpoints)
  - [/inventory](#inventory)
    - [GET](#get)
      - [Sample Request](#sample-request)
      - [Sample Response](#sample-response)
  - [/records](#records)
    - [GET](#get-1)
      - [Query Parameters](#query-parameters)
      - [Sample Request](#sample-request-1)
      - [Sample Response](#sample-response-1)
- [Service Validation](#service-validation)
- [Load Testing](#load-testing)
- [Help](#help)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Development

To run service locally in the dev environment:
```sh
$ cp .env.sample .env
$ go get github.com/codegangsta/gin
$ which gin
$GOPATH/bin/gin
$ make watch
```

When `APP_ENV=development` the application will attempt to load data from the path defined in `DATA_PATH`. Sample data exists in S3.
```sh
$ aws s3 cp --recursive s3://surfline-science-s3-dev/smdb/data/ data/
```

#### Application environment variables
Like other core services, this application's environment variables are defined in the ci.json file at the root level of the service. The one exception is the `DATA_PATH` variable, which is defined in the docker-compose file at `wavetrak-services/docker/science-models-db.ecs-params.yml'.

## Quickstart
```sh
$ cp .env.sample .env
$ docker build -t science-models-db .
$ docker run --env-file .env -p 8081:8081 science-models-db
```
If you'd like to open up a local badger DB, save them in a directory `data` on the root level:
```sh
docker run --env-file .env -p 8081:8081 -v $(pwd)/data:/opt/app/data science-models-db
```

## Endpoints

### /inventory

#### GET

Returns a list of model runs currently loaded in the database.

##### Sample Request

```
HTTP/1.1 GET /inventory
```

##### Sample Response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

[
  {
    "agency": "NOAA",
    "model": "GFS",
    "run": 2019013100
  },
  {
    "agency": "NOAA",
    "model": "GFS",
    "run": 2019013106
  }
]
```

### /records

#### GET

Queries data records from model runs in the database by latitude and longitude.

##### Query Parameters

| Parameter | Required | Description |
| --------- | -------- | ----------- |
| `agency`  | Yes | e.g. `NOAA`, `Wavetrak` |
| `model`   | Yes | e.g. `GFS`, `WW3`, `Lotus-WW3` |
| `run`     | Yes | Run date/time in the format `YYYYMMDDHH` e.g. `2018122400` |
| `grid`    | Yes | Grid name e.g. `0p25`, `GLOB_30m` |
| `lat`     | Yes | Latitude of grid coordinate for model data |
| `lon`     | Yes | Longitude of grid coordinate for model data |

* Note that latitude and longitude query parameters must exist in the database to be queryable. This endpoint will not support snapping to nearest grid point.

##### Sample Request

```
HTTP/1.1 GET /records?agency=noaa&model=gfs&grid=0p25&run=2019012806&lat=0&lon=0
```

##### Sample Response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
  "headers": [
    "Grid",
    "Lat",
    "Lon",
    "Timestamp",
    "Visibility",
    "WindGust",
    "Temperature",
    "Dewpoint",
    "RelativeHumidity",
    "WindU",
    "WindV",
    "TotalPrecipitation",
    "Snow",
    "IcePellets",
    "FreezingRain",
    "SurfaceLiftedIndex",
    "ConvectiveAvailablePotentialEnergy",
    "PrecipitableWater",
    "TotalCloudCover",
    "PressureReducedtoMSL"
  ],
  "types": [
    "string",
    "number",
    "number",
    "number",
    "number",
    "number",
    "number",
    "number",
    "number",
    "number",
    "number",
    "number",
    "boolean",
    "boolean",
    "boolean",
    "number",
    "number",
    "number",
    "number",
    "number"
  ],
  "values": [
    [
      "0p25",
      32.25,
      0,
      1548658800,
      24100,
      4.0131593,
      279.3865,
      276.2,
      79.9,
      1.3530933,
      -2.8974144,
      0,
      false,
      false,
      false,
      8.535779,
      0,
      9.357831,
      0,
      101886.27
    ],
    [
      "0p25",
      32.25,
      0,
      1548662400,
      24100,
      3.4174201,
      280.06406,
      275.80002,
      74.200005,
      1.2834619,
      -2.3464012,
      0,
      false,
      false,
      false,
      9.192673,
      0,
      9.061266,
      0,
      101926.93
    ],
    [
      "0p25",
      32.25,
      0,
      1548669600,
      24100,
      10,
      283.8551,
      273,
      47,
      3.0149193,
      -5.759819,
      0,
      false,
      false,
      false,
      9.531385,
      0,
      7.5,
      0,
      101878.34
    ],
    ...
  ],
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `science-models-db` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TnpNMU16TTFNalkifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE56TTFNek0xTWpZIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Endpoints](#endpoints) section. 

**Ex:**

Test `GET` requests to the `/inventory` endpoint:


```
curl \
    -X GET \
    -i \
    http://science-models-db.sandbox.surfline.com/inventory
```
**Expected response:** See [above](#sample-response) for sample response.

## Load Testing

Load testing has been implemented using k6 https://k6.io/
To re-run the default test and view results in JSON, update `VALID_RUN` and
```sh
$ k6 run load-tests/loadTest.js --out json=load-tests/loadTestResults.json
$ k6 run load-tests/stressTest.js --out json=load-tests/stressTestResults.json
$ k6 run load-tests/randomizedLoadTest.js --out json=load-tests/randomizedLoadTestResults.json
```

## Help
Is this your first time working with Go? You'll need a functioning Goworkspace and GOPATH.
See the language documentation for help: https://golang.org/doc/code.html#Organization
