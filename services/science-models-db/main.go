package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/Surfline/wavetrak-services/services/science-models-db/backups"
	"github.com/Surfline/wavetrak-services/services/science-models-db/ecs"
	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/external"
	"github.com/Surfline/wavetrak-services/services/science-models-db/kinesis"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/Surfline/wavetrak-services/services/science-models-db/remoteinventory"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/subosito/gotenv"
)

type env struct {
	session         *session.Session
	appEnv          string
	dataPath        string
	streamName      string
	instanceID      string
	checkpointTable string
	inventoryTable  string
	devModelRuns    []models.ModelRun
	scienceBucket   string
	backupPrefix    string
}

func environment() (*env, error) {
	gotenv.Load()

	sess, err := session.NewSession()
	if err != nil {
		return nil, errors.WrapError(err)
	}

	appEnv, ok := os.LookupEnv("APP_ENV")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf("Environment variable APP_ENV must be set"))
	}

	dataPath, ok := os.LookupEnv("DATA_PATH")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf("Environment variable DATA_PATH must be set"))
	}

	streamName, ok := os.LookupEnv("SMDB_STREAM_NAME")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf(("Environment variable SMDB_STREAM_NAME must be set")))
	}

	instanceID, ok := os.LookupEnv("DEV_INSTANCE_ID")
	if !ok {
		metaDataFilePath, ok := os.LookupEnv("ECS_CONTAINER_METADATA_FILE")
		if ok {
			ecsInstanceID, err := ecs.GetECSContainerEC2InstanceID(sess, metaDataFilePath)
			if err != nil {
				return nil, errors.WrapError(err)
			}
			instanceID = ecsInstanceID
		}
	}

	devBackups := os.Getenv("DEV_BACKUPS")
	devModelRuns := make([]models.ModelRun, 0)
	if devBackups != "" {
		for _, devBackup := range strings.Split(devBackups, ",") {
			backupVals := strings.Split(devBackup, "/")
			run64, err := strconv.ParseUint(backupVals[2], 10, 32)
			if err != nil {
				log.Fatal(errors.WrapError(err))
			}
			devModelRuns = append(devModelRuns, models.ModelRun{
				Agency: backupVals[0],
				Model:  backupVals[1],
				Run:    uint32(run64),
			})
		}
	}

	checkpointTable, ok := os.LookupEnv("SMDB_CHECKPOINT_TABLE")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf(("Environment variable SMDB_CHECKPOINT_TABLE must be set")))
	}

	inventoryTable, ok := os.LookupEnv("SMDB_INVENTORY_TABLE")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf(("Environment variable SMDB_INVENTORY_TABLE must be set")))
	}

	scienceBucket, ok := os.LookupEnv("SCIENCE_BUCKET")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf(("Environment variable SCIENCE_BUCKET must be set")))
	}

	backupPrefix, ok := os.LookupEnv("SMDB_BACKUP_PREFIX")
	if !ok {
		return nil, errors.WrapError(fmt.Errorf(("Environment variable SMDB_BACKUP_PREFIX must be set")))
	}

	return &env{
		session:         sess,
		appEnv:          appEnv,
		dataPath:        dataPath,
		streamName:      streamName,
		instanceID:      instanceID,
		checkpointTable: checkpointTable,
		inventoryTable:  inventoryTable,
		devModelRuns:    devModelRuns,
		scienceBucket:   scienceBucket,
		backupPrefix:    backupPrefix,
	}, nil
}

func main() {
	env, err := environment()
	if err != nil {
		log.Fatal(errors.WrapError(err))
	}

	svc, err := newService(serviceOptions{
		dataPath:        env.dataPath,
		session:         env.session,
		remoteInventory: remoteinventory.New(env.session, env.instanceID, env.inventoryTable),
	})
	if err != nil {
		log.Fatal(errors.WrapError(err))
	}

	readOnlyDone, writeableDone := make(chan error), make(chan error)
	go svc.inventory.LoadRemote(readOnlyDone, writeableDone)
	// Wait on read only databases to be loaded
	if err := <-readOnlyDone; err != nil {
		log.Fatal(errors.WrapError(err))
	}

	if len(svc.inventory.Databases()) == 0 {
		log.Println("No databases found in inventory. Restoring databases from S3...")

		// Use development model runs if available
		modelRuns := env.devModelRuns
		if len(modelRuns) == 0 {
			extClient := external.New()
			extModelRuns, err := extClient.GetModelRuns()
			if err != nil {
				log.Fatal(errors.WrapError(err))
			}
			modelRuns = extModelRuns
		}

		backupsManager := backups.New(svc.session, env.scienceBucket, env.backupPrefix)
		if err := svc.inventory.Restore(modelRuns, backupsManager); err != nil {
			log.Fatal(errors.WrapError(err))
		}
	}

	go func() {
		// Asynchronously wait on writeable databases to be loaded into inventory before streaming
		if err := <-writeableDone; err != nil {
			log.Fatal(errors.WrapError(err))
		}

		if err := kinesis.Subscribe(kinesis.SubscribeOptions{
			Session:         svc.session,
			StreamName:      env.streamName,
			AppName:         env.instanceID,
			TableName:       env.checkpointTable,
			RecordsCallback: recordsCallbackFactory(*svc),
			TransactionWrapper: func(r *kinesis.Record, f func(*kinesis.Record) error) error {
				txn := svc.nrApp.StartTransaction("Kinesis record", nil, nil)
				defer txn.End()
				err := f(r)
				if err != nil {
					log.Printf("Failed to process record %+v with error: %s", r, err)
					txn.NoticeError(err)
				}
				return nil
			},
		}); err != nil {
			log.Fatal(errors.WrapError(err))
		}
	}()

	httpHandler, err := makeHTTPHandler(*svc)
	if err != nil {
		log.Fatal(errors.WrapError(err))
	}

	port, ok := os.LookupEnv("EXPRESS_PORT")
	if !ok {
		log.Fatal("Environment variable EXPRESS_PORT must be set")
	}
	log.Printf("Listening on port %v", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), httpHandler))
}
