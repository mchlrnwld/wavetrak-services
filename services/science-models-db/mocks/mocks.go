package mocks

import (
	"github.com/Surfline/wavetrak-services/services/science-models-db/backups"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/stretchr/testify/mock"
)

// BackupsDownloader mocks the backups.Downloader interface.
type BackupsDownloader struct {
	mock.Mock
}

// Download mocked method.
func (d *BackupsDownloader) Download(fileKey string) error {
	args := d.Called(fileKey)
	return args.Error(0)
}

// BackupsManager mocks the backups.Manager interface.
type BackupsManager struct {
	mock.Mock
}

// NewDownloader mocked method.
func (m *BackupsManager) NewDownloader(dir string) backups.Downloader {
	args := m.Called(dir)
	return args.Get(0).(backups.Downloader)
}

// ListFileKeys mocked method.
func (m *BackupsManager) ListFileKeys(modelRun models.ModelRun) ([]string, error) {
	args := m.Called(modelRun)
	return args.Get(0).([]string), args.Error(1)
}
