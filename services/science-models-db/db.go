package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/Surfline/wavetrak-services/services/science-models-db/database"
	"github.com/Surfline/wavetrak-services/services/science-models-db/errors"
	"github.com/Surfline/wavetrak-services/services/science-models-db/kinesis"
	"github.com/Surfline/wavetrak-services/services/science-models-db/models"
	"github.com/newrelic/go-agent"
)

func getRecordsDB(svc service, modelRun models.ModelRun) (*database.Database, error) {
	db, ok := svc.inventory.Database(modelRun)
	if ok {
		return db, nil
	}

	db, err := svc.inventory.AddDB(modelRun, false)
	if err != nil {
		return nil, errors.WrapError(err)
	}

	log.Printf("Begin writing to database: %+v", modelRun)

	return db, nil
}

func turnDBOnline(svc service, modelRun models.ModelRun, noWritesDuration time.Duration, timeout time.Duration) error {
	start := time.Now()
	for {
		if time.Now().Sub(start) > timeout {
			return errors.WrapError(fmt.Errorf("Waiting for database %+v to be ready to turn online timed out after %v", modelRun, timeout))
		}

		db, ok := svc.inventory.Database(modelRun)
		if !ok {
			return errors.WrapError(fmt.Errorf("Database not found for %+v", modelRun))
		}

		if db.ReadOnly {
			log.Printf("Database already online: %+v", modelRun)
			return nil
		}

		// Wait to try again if database has been written to within duration
		if time.Now().Sub(db.LastWrite) < noWritesDuration {
			time.Sleep(noWritesDuration)
			continue
		}

		// Reload database as read only
		if err := svc.inventory.DropDB(modelRun, false); err != nil {
			return errors.WrapError(err)
		}

		if _, err := svc.inventory.AddDB(modelRun, true); err != nil {
			return errors.WrapError(err)
		}

		return nil
	}
}

func turnDBOnlineAsync(svc service, modelRun models.ModelRun) error {
	txn := svc.nrApp.StartTransaction(fmt.Sprintf("Turn database online: %+v", modelRun), nil, nil)
	go func(txn newrelic.Transaction) {
		defer txn.End()

		if err := turnDBOnline(svc, modelRun, time.Minute, 3*time.Hour); err != nil {
			txn.NoticeError(errors.WrapError(err))
		} else {
			log.Printf("Database now online: %+v", modelRun)
		}
	}(txn)

	return nil
}

func pruneOlderDBs(svc service, modelRun models.ModelRun) error {
	dbMap := svc.inventory.Databases()
	for _, db := range dbMap {
		if db.ModelRun.Agency == modelRun.Agency && db.ModelRun.Model == modelRun.Model && db.ModelRun.Run < modelRun.Run {
			if err := svc.inventory.DropDB(db.ModelRun, true); err != nil {
				return errors.WrapError(err)
			}
			log.Printf("Removed database: %+v", modelRun)
		}
	}
	return nil
}

func parsePartitionKey(key string) (models.ModelRun, string, error) {
	var modelRun models.ModelRun
	keyItems := strings.Split(key, ",")
	if len(keyItems) < 4 {
		return modelRun, "", errors.WrapError(fmt.Errorf("Invalid partition key: %s", key))
	}

	modelRun.Agency = keyItems[0]
	modelRun.Model = keyItems[1]
	run64, err := strconv.ParseUint(keyItems[2], 10, 32)
	if err != nil {
		return modelRun, "", errors.WrapError(err)
	}
	modelRun.Run = uint32(run64)
	rest := strings.Join(keyItems[3:], ",")

	return modelRun, rest, nil
}

type recordsCallbackMessage struct {
	records []*kinesis.Record
	errCh   chan error
}

func recordsCallback(svc service, records []*kinesis.Record) error {
	if len(records) < 1 {
		return errors.WrapError(fmt.Errorf("No record found: %+v", records))
	}

	sampleRecord := records[0]
	if *sampleRecord.PartitionKey == "Database Complete" {
		modelRun, err := models.ModelRunFromJSON(sampleRecord.Data)
		if err != nil {
			return errors.WrapError(err)
		}

		log.Printf("Turning database online: %+v", modelRun)
		if err := turnDBOnlineAsync(svc, modelRun); err != nil {
			return errors.WrapError(err)
		}

		return nil
	}

	if *sampleRecord.PartitionKey == "Prune Older Databases" {
		modelRun, err := models.ModelRunFromJSON(sampleRecord.Data)
		if err != nil {
			return errors.WrapError(err)
		}

		log.Printf("Pruning databases older than %+v", modelRun)
		if err := pruneOlderDBs(svc, modelRun); err != nil {
			return errors.WrapError(err)
		}

		return nil
	}

	modelRun, _, err := parsePartitionKey(*sampleRecord.PartitionKey)
	if err != nil {
		return errors.WrapError(err)
	}

	db, err := getRecordsDB(svc, modelRun)
	if err != nil {
		return errors.WrapError(err)
	}
	if db.ReadOnly {
		log.Printf("Database already online: %+v", modelRun)
		return nil
	}

	keyValues := make([]database.KeyValue, len(records))
	for i, record := range records {
		_, key, err := parsePartitionKey(*record.PartitionKey)
		if err != nil {
			return errors.WrapError(err)
		}
		kv := database.KeyValue{
			Key:   []byte(key),
			Value: record.Data,
		}
		keyValues[i] = kv
	}
	if err := db.SetRecords(keyValues); err != nil {
		return errors.WrapError(err)
	}

	return nil
}

func recordsCallbackFactory(svc service) func([]*kinesis.Record) error {
	// Treat messageCh as a queue and process record batches in a single goroutine to throttle
	// processing so that streaming data into a database does not degrade the performance of
	// serving data.
	messageCh := make(chan recordsCallbackMessage)
	go func() {
		for message := range messageCh {
			message.errCh <- recordsCallback(svc, message.records)
		}
	}()

	return func(records []*kinesis.Record) error {
		errCh := make(chan error)
		messageCh <- recordsCallbackMessage{
			records: records,
			errCh:   errCh,
		}
		err := <-errCh
		if err != nil {
			return errors.WrapError(err)
		}
		return nil
	}
}
