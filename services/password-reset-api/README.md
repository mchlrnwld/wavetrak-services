# Password Reset Service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Using the Password Reset Service](#using-the-password-reset-service)
- [Internal Workflow](#internal-workflow)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


### Using the Password Reset Service

GET password-reset/?email=pranspach@surfline.com&brand=sl (unauthenticated)
   * Sends password recovery email with a code
   * Success 200 or error (both in {message: ""} JSON format)

POST password-reset/(authenticated with resetCode)
   * JSON body:
   ```
   {
     userId: 3d2d322df2f...,
     password: newPassword,
     resetCode: 1234
   }
   ```
   * Success 200:
   ```
   {
     access_token: access_token,
     refresh_token: refresh_token
   }
   ```

POST password-reset/validate-code (unauthenticated)
   * JSON body:
   ```
   {
     resetCode: 1234,
     brand: 'sl' // 'sl' || 'bw' || 'fs'
   }
   ```
   * Success 200:
   ```
   {
     userId: 1,
     message: 'Valid reset code found!'
   }
   ```


### Internal Workflow
  - Request sent with email
  - Look up email from userService
  - Generate UUID to use as the reset code
  - Save key to Redis with userID
  - Send Analytics event to trigger Email notification

### Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `password-reset-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TmpZMU1EUTNNZyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE5qWTFNRFEzTWciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the section [using the password reset service](#using-the-password-reset-service).

**Ex:**

Test `GET` requests to the `/password-reset` endpoint:

```
curl \
    -X GET \
    -i \
    http://password-reset-service.sandbox.surfline.com/password-reset?email=mmalchak@surfline.com&brand=sl
```

**Sample response:** The response should be:
```
{"message":"Reset code emailed to user"}
```
