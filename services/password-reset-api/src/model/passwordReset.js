import * as dbContext from '../common/dbContext';
import * as logger from '../common/logger';
import * as generateCode from '../common/generateCode';
import { decrypt } from '../common/generateHash';
import * as analytics from '../common/analytics';

const log = logger.logger('password-reset:model');
export const redis = () => dbContext.dbClient();
const redisTTL = 21600; // 6 hours
const longTTL = 15552000; // 6 months

/**
  @setCode takes a mandatory userId and resetCode
  and creates a new redis key with an expiration time of {ttl} minutes.
  --
  New key saved in Redis as "resetCode:A1B2C3D4E5 1800 1234"
* */
export const setCode = (userId, resetCode, ttl) =>
  new Promise((resolve, reject) => {
    if (!resetCode) {
      return reject(new Error('Password Reset: Error return from setCode().'));
    }
    return redis().setex(`resetCode:${userId}`, ttl, resetCode, (err, res) => {
      if (err) {
        log.error({
          action: `setCode(${userId})`,
          error: err,
        });
        reject(err);
      } else {
        resolve(res);
      }
    });
  });

/**
  @getCode must take a userId.  It uses @generateCode and @setCode to
  to return a Promise object to the routeHandler.
* */
export const getCode = (userId, longlived) =>
  new Promise((resolve, reject) => {
    if (!userId) {
      return reject(new Error('Password Reset: Error return from getCode().'));
    }
    const code = generateCode.generateCode();

    return setCode(userId, code, longlived ? longTTL : redisTTL)
      .then((redisResponse) => {
        log.debug(`Response from setCode(): ${redisResponse}:${code}`);
        resolve(code);
      })
      .catch((redisErr) => {
        log.error(redisErr);
        reject(redisErr);
      });
  });

/**
  @validateCode accepts a userId and responds to the route handler with the
  corresponding resetCode, if the user has one in Redis.
* */
export const validateCode = (userId, brand = 'sl') =>
  new Promise((resolve, reject) => {
    if (!userId) {
      return reject(new Error('Password Reset: Error returned from validateCode().'));
    }
    return redis().get(`resetCode:${userId}`, (err, res) => {
      if (err) {
        log.error({
          action: 'Password Reset - Redis Error from validateCode();',
          error: err,
        });
        reject(new Error('Password Reset: Redis Error in validateCode().'));
      } else if (res === null) {
        analytics.trackAnonymous(
          'Invalid Forgot Password Reset Code',
          'InvalidResetCode',
          { code: userId },
          brand,
        );

        reject(new Error('Invalid resetCode', { statusCode: 400 }));
      } else {
        resolve(res);
      }
    });
  });

export const invalidateCode = (userId) =>
  new Promise((resolve, reject) => {
    if (!userId) {
      return reject(new Error('Password Reset: Error from invalidateCode().'));
    }
    return redis().del(`resetCode:${userId}`, (err) => {
      if (err) {
        log.error({
          action: 'Password Reset - Redis Error from invalidateCode();',
          error: err,
        });
        reject(new Error('Password Reset: Redis Error in invalidateCode().'));
      } else {
        resolve();
      }
    });
  });

/**
  @validateHash accepts a hashed query string from the route handler.  It uses
  @dcrypt to decipher the string, and checks if the resetCode and userId are
  maching key/value pairs in Redis.
* */
export const validateHash = (brand, hash) =>
  new Promise((resolve, reject) => {
    try {
      const decryptedString = decrypt(hash);

      // attrs is an array containing: [userId, resetCode];
      const attrs = decryptedString.split('#');
      const userId = attrs[0];
      const resetCode = attrs[1];

      redis().get(`resetCode:${userId}`, (err, res) => {
        if (err) {
          log.err({
            action: 'Password Reset - Redis Error from validateHash();',
            error: err,
          });
          reject(err);
        } else if (res === null || resetCode !== res) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject({ statusCode: 400, message: 'Expired hash.' });
        } else {
          resolve({ resetCode: res, userId });
        }
      });
    } catch (err) {
      log.error({
        action: 'Password Reset - Error returned from decrypt();',
        error: err,
      });
      reject(new Error('Invalid hash format.', { statusCode: 400 }));
    }
  });
