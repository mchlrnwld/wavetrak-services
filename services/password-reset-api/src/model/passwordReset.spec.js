/* eslint-disable no-unused-expressions */
import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import * as analytics from '../common/analytics';
import * as logger from '../common/logger';
import * as passwordReset from './passwordReset';
import * as dbContext from '../common/dbContext';
import * as generateCode from '../common/generateCode';

const { expect } = chai;
chai.should();
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('password reset model', () => {
  let analyticsStub;
  let loggerStub;

  let redisClient;
  let redisClientStub;
  let generateCodeStub;
  let generateCodePromise;

  const theUserId = '1234';
  const theResetCode = '5678';

  beforeEach(() => {
    analyticsStub = sinon.stub(analytics, 'triggerEmail').returns({});
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace() {},
      debug() {},
      info() {},
      error() {},
    });
    redisClient = {
      setex(resetCode, redisTTL, userId, callback) {
        callback(null, 1);
      },
      get(resetCode, callback) {
        callback(null, 1);
      },
      del(resetCode, callback) {
        callback(null);
      },
    };
    redisClientStub = sinon.stub(dbContext, 'dbClient').returns(redisClient);
    generateCodeStub = sinon.stub(generateCode, 'generateCode');
    generateCodePromise = generateCodeStub.returnsPromise();
  });

  afterEach(() => {
    analyticsStub.restore();
    loggerStub.restore();
    generateCodeStub.restore();
    redisClientStub.restore();
  });

  it('should initialize an object', () => {
    expect(passwordReset).to.exist;
  });

  it('should set a reset code with set code when userId and resetCode are passed', (done) => {
    passwordReset.setCode(theUserId, theResetCode).then((res) => {
      expect(res).to.exist;
      expect(res).to.equal(1);
      done();
    });
  });

  it('should not set a reset code with set code when resetCode is not passed', (done) => {
    passwordReset.setCode(theUserId, null).catch((err) => {
      expect(err.message).to.equal('Password Reset: Error return from setCode().');
      done();
    });
  });

  it('should get a reset code with set code when valid userId is passed', (done) => {
    passwordReset.getCode(theUserId).then((res) => {
      expect(res).to.exist;
      expect(res).to.have.deep.property('resetCode', theResetCode);
      done();
    });
    generateCodePromise.resolves({ resetCode: theResetCode });
  });

  it('should validate a valid code', (done) => {
    passwordReset.validateCode('sl', theResetCode).then((res) => {
      expect(res).to.exist;
      expect(res).to.equal(1);
      done();
    });
  });

  it('should invalidate a code', (done) => {
    passwordReset.invalidateCode(theResetCode).then((err) => {
      expect(err).to.not.exist;
      done();
    });
  });
});
