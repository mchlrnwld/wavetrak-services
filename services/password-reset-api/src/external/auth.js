import request from 'request';
import * as logger from '../common/logger';

const log = logger.logger('password-reset-service');

export const invalidateToken = (brand, userId) =>
  new Promise((resolve, reject) => {
    const options = {
      uri: `${process.env.AUTH_SERVICE}/trusted/invalidate-token`,
      method: 'POST',
      form: { userId },
      json: true,
    };
    request(options, (error, response, body) => {
      if (error) {
        reject(error);
        log.error(error);
      } else if (response.statusCode === 200) {
        resolve(body);
      } else {
        log.info(`invalidateToken status: ${response.statusCode}`);
        reject(new Error('Error invalidating tokens', { statusCode: response.statusCode, body }));
      }
    });
  });

export const validateUser = (userEmail, userPassword, authorizationHeaders) =>
  new Promise((resolve, reject) => {
    const options = {
      uri: `${process.env.AUTH_SERVICE}/trusted/token`,
      headers: {
        authorization: `${authorizationHeaders}`,
      },
      method: 'POST',
      form: { username: userEmail.toLowerCase(), password: userPassword, grant_type: 'password' },
      json: true,
    };
    request(options, (error, response, body) => {
      if (error) {
        log.error(error);
        reject(error);
      } else if (response.statusCode === 200) {
        resolve(body);
      } else {
        log.info(`validateUser status: ${response.statusCode}`);
        reject(new Error('Error validating user', { statusCode: response.statusCode, body }));
      }
    });
  });
