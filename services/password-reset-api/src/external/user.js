import request from 'request';

export const validateUserEmail = (userEmail) =>
  new Promise((resolve, reject) => {
    const options = {
      uri: `${process.env.USER_SERVICE}/user`,
      qs: { email: encodeURIComponent(userEmail.toLowerCase()) },
      json: true,
    };
    request(options, (error, response, body) => {
      if (error) {
        reject(error);
      } else if (response.statusCode === 400) {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ statusCode: 400, body });
      } else {
        resolve(body);
      }
    });
  });

export const resetPassword = (userId, password, authorizationHeaders) =>
  new Promise((resolve, reject) => {
    const options = {
      uri: `${process.env.USER_SERVICE}/user/ChangePassword`,
      headers: {
        authorization: `${authorizationHeaders}`,
      },
      method: 'PUT',
      json: { _id: userId, password },
    };
    request(options, (error, response, body) => {
      if (response.statusCode === 200) {
        resolve(body.email);
      } else {
        reject(new Error('Error validating user email', { statusCode: response.statusCode, body }));
      }
    });
  });
