import bunyan from 'bunyan';
import Logsene from '@surfline/bunyan-logsene';
import PrettyStream from 'bunyan-prettystream';

const logseneStream = new Logsene({
  token: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63', // default to sandbox
});
logseneStream.setMaxListeners(50);

const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout); // Using hadfieldn's fork of node-bunyan-prettystream to handle pretty printing with circular reference safety

/**
 * Use example:
 *
 * // In ES6 using "import { x } from 'y'" is analogous to overwriting module.exports with a function in ES5. * as logger makes writing unit tests easier.
 * import * as logger from '../common/logger';
 *
 * export function MyController() {
 *    const log = logger.logger('user-service');
 *    log.trace('MyController created');
 * }
 *
 *
 * Test example:
 *
 * import sinon from 'sinon';
 * import * as logger from '../common/logger';
 * import * as MyController from './MyController';
 *
 * sinon.stub(logger, 'logger').returns({});
 *
 * @param name of service
 */
export function logger(name) {
  const log = bunyan.createLogger({
    name: name || 'default-js-logger',
    serializers: bunyan.stdSerializers,
    streams: [
      {
        level: process.env.CONSOLE_LOG_LEVEL || 'info',
        type: 'raw',
        stream: prettyStdOut,
      },
      {
        level: process.env.LOGSENE_LEVEL || 'warn',
        stream: logseneStream,
        type: 'raw',
        reemitErrorEvents: true,
      },
    ],
  });
  log.on('error', () => {
    console.error('Error logging.');
  });
  log.trace(`${name} logging started.`);
  return log;
}
