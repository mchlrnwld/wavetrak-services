import Redis from 'redis';
import { logger } from './logger';

const log = logger('password-reset-service:dbContext');

let redisClient = {};

export function initCache() {
  // todo add redis username/password for more security
  const connectionString = `redis://${process.env.REDIS_IP}:${process.env.REDIS_PORT}`;
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Redis ConnectionString: ${connectionString} `);
      redisClient = Redis.createClient(connectionString);

      redisClient.once('ready', () => {
        log.info(`Redis connected on ${connectionString} and ready to accept commands`);
        resolve();
      });

      redisClient.on('error', (error) => {
        log.error({
          action: 'Redis:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export const dbClient = () => redisClient;
