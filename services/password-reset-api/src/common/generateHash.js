import crypto from 'crypto';

const algorithm = process.env.ENCRYPTION_ALGORITHM;
const password = process.env.ENCRYPTION_KEY;

export function encrypt(userId, code) {
  const cipher = crypto.createCipher(algorithm, password);
  const text = `${userId}#${code}`;
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

export function decrypt(text) {
  const decipher = crypto.createDecipher(algorithm, password);
  try {
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  } catch (err) {
    throw new Error(err);
  }
}
