import Analytics from 'analytics-node';
import { encrypt } from './generateHash';

const slAnalytics = new Analytics(process.env.SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const bwAnalytics = new Analytics(process.env.BW_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const fsAnalytics = new Analytics(process.env.FS_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });

const getAnalyticsFor = (brand) => {
  let analytics;
  switch (brand) {
    case 'bw':
      analytics = bwAnalytics;
      break;
    case 'fs':
      analytics = fsAnalytics;
      break;
    default:
      analytics = slAnalytics;
  }
  return analytics;
};

const getUrlFor = (brand) => {
  let url;
  const urlPath = 'forgot-password/set-password';
  switch (brand) {
    case 'bw':
      url = `https://www.buoyweather.com/${urlPath}`;
      break;
    case 'fs':
      url = `https://www.fishtrack.com/${urlPath}`;
      break;
    default:
      url = `${process.env.LINK_URL}/${urlPath}`;
  }
  return url;
};

/**
 * @description Triggers a Segment event. Invoked by the password reset flow.
 * @param {string} event required
 * @param {object} user required
 * @param {string} resetCode required
 * @param {string} brand required
 * @param {string} platform required - For tracking purposes
 * @returns {Promise}
 */
export const triggerEmail = (event, user, resetCode, brand, platform) =>
  new Promise((resolve, reject) => {
    if (!event || !user || !resetCode) {
      reject(new Error('Error processing triggerEmail()'));
    } else {
      const url = getUrlFor(brand);
      const hash = encrypt(user._id, resetCode);
      const analytics = getAnalyticsFor(brand);
      const { hasPassword, migratedUser } = user;
      const migratedBrand =
        migratedUser && !hasPassword ? migratedUser.substring(0, migratedUser.indexOf('_')) : null;
      analytics.track({
        event,
        userId: user._id,
        properties: {
          code: resetCode,
          email: user.email.toLowerCase(),
          migratedBrand,
          url: `${url}?hash=${hash}`,
          platform,
        },
      });
      resolve(hash);
    }
  });

export const triggerAdminResetPasswordEmail = (userId, password, brand) => {
  /* eslint-disable no-unused-vars */
  const analytics = getAnalyticsFor(brand);
  const properties = {
    password,
  };
  /* eslint-enable no-unused-vars */

  // TODO - Enable event once Email Campaign is setup in Customer.io
  // analytics.track({ event: 'Admin Reset Password', userId, properties });

  return true;
};

export const track = (event, userId, properties, brand) => {
  const analytics = getAnalyticsFor(brand);
  analytics.track({ event, userId, properties });
};

export const trackAnonymous = (event, anonymousId, properties, brand) => {
  const analytics = getAnalyticsFor(brand);
  analytics.track({ event, anonymousId, properties });
};
