import { Router } from 'express';
import newrelic from 'newrelic';
import { getPlatform, MAX_PASSWORD_LENGTH } from '@surfline/services-common';
import * as logger from '../common/logger';
import * as analytics from '../common/analytics';
import * as passwordResetModel from '../model/passwordReset';
import * as userService from '../external/user';
import * as authService from '../external/auth';

const log = logger.logger('password-reset-service');

export const handleErrorForResponse = (res, action) => (err) => {
  newrelic.addCustomAttributes({
    stack: err.stack,
    message: err.message,
  });
  if (err.statusCode) {
    log.debug({
      message: `Password Reset Service: ${action}`,
      stack: err,
    });
    return res.status(err.statusCode).send(err);
  }
  if (typeof err === 'object') {
    log.error({
      message: `Password Reset Service: ${action}`,
      stack: err,
    });
  } else {
    log.error({
      message: `Password Reset Service: ${action}`,
      errorMessage: err,
    });
  }
  return res.sendStatus(500);
};

export const authenticatePost = (req, res, next) => {
  passwordResetModel
    .validateCode(req.body.userId, req.body.brand)
    .then((resetCode) => {
      if (resetCode !== req.body.resetCode) {
        res.sendStatus(401);
      } else {
        next();
      }
    })
    .catch(handleErrorForResponse(res, 'validateCodeHandler'));
};

export function passwordReset() {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/PasswordReset',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  async function getHandler(req, res) {
    try {
      const user = await userService.validateUserEmail(req.query.email);
      const resetCode = await passwordResetModel.getCode(user._id, !user.hasPassword);
      const platform = getPlatform(req.headers['user-agent']);
      await analytics.triggerEmail(
        'Requested Forgot Password Email',
        user,
        resetCode,
        req.query.brand,
        platform,
      );
      return res.status(200).send({ message: 'Reset code emailed to user' });
    } catch (err) {
      return handleErrorForResponse(res, 'getHandler')(err);
    }
  }

  const validateCodeHandler = (req, res) => {
    passwordResetModel
      .validateCode(req.body.resetCode, req.body.brand)
      .then((resetCode) => {
        res.status(200).json({ resetCode, message: 'Valid reset code found!' });
      })
      .catch(handleErrorForResponse(res, 'validateCodeHandler'));
  };

  const validateHashHandler = (req, res) => {
    passwordResetModel
      .validateHash(req.body.brand, req.body.hash)
      .then((user) => {
        res.status(200).json(user);
      })
      .catch(handleErrorForResponse(res, 'validateHashHandler'));
  };

  async function postHandler(req, res) {
    try {
      const { userId, password, brand } = req.body;
      const authorizationHeader = req.headers.authorization;
      if (password && password.length > MAX_PASSWORD_LENGTH) {
        log.warn(`User requested to set a password longer than ${MAX_PASSWORD_LENGTH} characters.`);
        return res
          .status(400)
          .send({ message: `Password length must not exceed ${MAX_PASSWORD_LENGTH} characters.` });
      }

      const userEmail = await userService.resetPassword(userId, password, authorizationHeader);
      await authService.invalidateToken(brand, userId);

      const credentials = await authService.validateUser(userEmail, password, authorizationHeader);
      await passwordResetModel.invalidateCode(userId);

      return res.status(200).json({
        access_token: credentials.access_token,
        refresh_token: credentials.refresh_token,
      });
    } catch (err) {
      return handleErrorForResponse(res, 'postHandler')(err);
    }
  }

  /**
   * @description This function invalidates a user's password reset code.
   *
   * @param {String} body.userId - the Wavetrak user id.
   * @returns {Object} response - a response indicating success or failure of the request.
   */
  const invalidateCodeHandler = async (req, res) => {
    try {
      const { userId } = req.body;
      await passwordResetModel.invalidateCode(userId);
      return res.status(200).json({
        message: 'Successfully invalidated reset code.',
      });
    } catch (err) {
      return handleErrorForResponse(res, 'deleteHandler')(err);
    }
  };

  api.get('/', trackRequest, getHandler);
  api.post('/', trackRequest, authenticatePost, postHandler);
  api.post('/validate-code', trackRequest, validateCodeHandler);
  api.post('/validate-hash', trackRequest, validateHashHandler);
  api.post('/invalidate-code', trackRequest, invalidateCodeHandler);

  return api;
}
