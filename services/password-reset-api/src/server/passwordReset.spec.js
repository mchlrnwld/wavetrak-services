/* eslint-disable no-unused-expressions */
import express from 'express';
import bodyParser from 'body-parser';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import { MAX_PASSWORD_LENGTH } from '@surfline/services-common';
import * as logger from '../common/logger';
import * as analytics from '../common/analytics';
import * as passwordReset from './passwordReset';
import * as passwordResetModel from '../model/passwordReset';
import * as userService from '../external/user';
import * as authService from '../external/auth';
import * as dbContext from '../common/dbContext';

console.log(passwordReset.authenticatePost);
const { expect } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('password reset routes', () => {
  let loggerStub;
  let analyticsStub;

  let modelValidateUserEmailStub;
  let modelValidateUserEmailPromise;
  let modelValidateCodeStub;
  let modelValidateCodePromise;
  let modelValidateHashStub;
  let modelValidateHashPromise;
  let modelInvalidateTokenStub;
  let modelInvalidateTokenPromise;
  let modelValidateUserStub;
  let modelValidateUserPromise;
  let modelValidateResetPasswordStub;
  let modelValidateResetPasswordPromise;
  let modelInvalidateCodeStub;
  let modelInvalidateCodePromise;
  let authenticatePostSpy;

  const redisClient = {
    setex: (arg1, arg2, arg3, callback) => {
      callback(null, 1);
    },
    del: (arg1, callback) => {
      callback(null);
    },
  };
  let redisClientStub;
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use('/', passwordReset.passwordReset());

  const user = {
    userId: 111,
  };

  const resetCode = '1234';

  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace() {},
      debug() {},
      info() {},
      error() {},
      warn() {},
    });
  });

  after(() => {
    loggerStub.restore();
  });
  beforeEach(() => {
    analyticsStub = sinon.stub(analytics, 'triggerEmail').returns({});
    redisClientStub = sinon.stub(dbContext, 'dbClient').returns(redisClient);
    modelValidateUserEmailStub = sinon.stub(userService, 'validateUserEmail');
    modelValidateUserEmailPromise = modelValidateUserEmailStub.returnsPromise();
    modelInvalidateTokenStub = sinon.stub(authService, 'invalidateToken');
    modelInvalidateTokenPromise = modelInvalidateTokenStub.returnsPromise();
    modelValidateUserStub = sinon.stub(authService, 'validateUser');
    modelValidateUserPromise = modelValidateUserStub.returnsPromise();
    modelValidateResetPasswordStub = sinon.stub(userService, 'resetPassword');
    modelValidateResetPasswordPromise = modelValidateResetPasswordStub.returnsPromise();
    modelValidateCodeStub = sinon.stub(passwordResetModel, 'validateCode');
    modelValidateCodePromise = modelValidateCodeStub.returnsPromise();
    modelInvalidateCodeStub = sinon.stub(passwordResetModel, 'invalidateCode');
    modelInvalidateCodePromise = modelInvalidateCodeStub.returnsPromise();
    modelValidateHashStub = sinon.stub(passwordResetModel, 'validateHash');
    modelValidateHashPromise = modelValidateHashStub.returnsPromise();
    authenticatePostSpy = sinon.stub(passwordReset, 'authenticatePost');
    authenticatePostSpy.callsArg(2);
  });

  afterEach(() => {
    analyticsStub.restore();
    modelValidateUserEmailStub.restore();
    modelValidateCodeStub.restore();
    modelValidateHashStub.restore();
    modelInvalidateTokenStub.restore();
    modelValidateUserStub.restore();
    modelValidateResetPasswordStub.restore();
    modelInvalidateCodeStub.restore();
    redisClientStub.restore();
    authenticatePostSpy.restore();
  });

  it('should be running without any problems', () => {
    expect(passwordReset.passwordReset).to.not.throw();
  });

  it('should send reset code to valid user', (done) => {
    chai
      .request(app)
      .get('/')
      .query({ email: 'john@test.com' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal({ message: 'Reset code emailed to user' });
        done();
      });
    modelValidateUserEmailPromise.resolves({ _id: user.userId });
  });

  it('should not send reset code to invalid user', (done) => {
    chai
      .request(app)
      .get('/')
      .query({ email: 'john@test.com' })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        done();
      });
    modelValidateUserEmailPromise.rejects({ statusCode: 400 });
  });

  it('should return a reset code when a code is validated', (done) => {
    chai
      .request(app)
      .post('/validate-code')
      .send({ userId: '1234' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    modelValidateCodePromise.resolves({ resetCode });
  });

  it('should return a user id and reset code when hash is valid', (done) => {
    chai
      .request(app)
      .post('/validate-hash')
      .send({ hash: '123412341234' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    modelValidateHashPromise.resolves({ userId: user.userId, resetCode });
  });

  it('should return an access token if userId, password and reset code are validated', (done) => {
    chai
      .request(app)
      .post('/')
      .send({ userId: 111, password: 'secretPassword', resetCode: '1234' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    modelValidateCodePromise.resolves('1234');
    modelValidateResetPasswordPromise.resolves({ email: 'john@test.com' });
    modelInvalidateTokenPromise.resolves(200);
    modelValidateUserPromise.resolves({
      accessToken: '123412341234',
      refresh_token: '123412341234',
    });
    modelInvalidateCodePromise.resolves(200);
  });

  it('should not return an access token if userId or password is not validated', (done) => {
    chai
      .request(app)
      .post('/')
      .send({ userId: 111, password: 'secretPassword', resetCode: '1234' })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        done();
      });
    modelValidateCodePromise.resolves('1234');
    modelValidateResetPasswordPromise.rejects({ statusCode: 400 });
    modelInvalidateTokenPromise.rejects({});
    modelValidateUserPromise.resolves({});
    modelInvalidateCodePromise.resolves({});
  });

  it('should not return an access token if reset code is invalid', (done) => {
    chai
      .request(app)
      .post('/')
      .send({ userId: 111, password: 'secretPassword', resetCode: '1234' })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        done();
      });
    modelValidateCodePromise.resolves('1234');
    modelValidateResetPasswordPromise.resolves({ email: 'john@test.com' });
    modelInvalidateTokenPromise.rejects({ statusCode: 400 });
    modelValidateUserPromise.resolves({});
    modelInvalidateCodePromise.resolves({});
  });

  it('should not return an access token if new password exceeds max length ', (done) => {
    const password = '#'.repeat(MAX_PASSWORD_LENGTH + 1);
    chai
      .request(app)
      .post('/')
      .send({ userId: 111, password, resetCode: '1234' })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        done();
      });
    modelValidateCodePromise.resolves('1234');
  });

  it('should invalidate a reset code if a userId is provided', (done) => {
    chai
      .request(app)
      .post('/invalidate-code')
      .send({ userId: 111 })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
    modelInvalidateCodePromise.resolves({});
  });

  it('should return 500 if invalidateCode returns an error', (done) => {
    chai
      .request(app)
      .post('/invalidate-code')
      .send({})
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(500);
        done();
      });
    modelInvalidateCodePromise.rejects(new Error());
  });
});
