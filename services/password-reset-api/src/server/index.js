import { setupExpress } from '@surfline/services-common';
import { passwordReset } from './passwordReset';
import admin from './admin';
import { logger } from '../common/logger';
import * as dbContext from '../common/dbContext';

const log = logger('password-reset-service:server');

const setupApp = () =>
  setupExpress({
    port: process.env.EXPRESS_PORT || 8000,
    log,
    name: 'password-reset-service',
    handlers: [
      ['/password-reset', passwordReset()],
      ['/admin', admin()],
    ],
    allowedMethods: ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'],
  });

dbContext
  .initCache()
  .then(setupApp)
  .catch((err) => {
    log.debug("App Initialization failed. Can't connect to database", err);
    process.exit(1);
  });

export default setupApp;
