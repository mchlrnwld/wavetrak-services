import { Router } from 'express';
import passwordReset from './passwordReset';

export default () => {
  const api = Router();

  api.use('/password-reset', passwordReset());

  return api;
};
