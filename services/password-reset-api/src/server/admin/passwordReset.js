import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { resetPassword as userResetPassword } from '../../external/user';
import { invalidateToken } from '../../external/auth';
import { triggerAdminResetPasswordEmail } from '../../common/analytics';

export default () => {
  const api = Router();

  const randomString = (length, chars) => {
    let result = '';
    for (let i = length; i > 0; i -= 1) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  };

  const resetPassword = async (req, res) => {
    const { body } = req;
    const { userId, brand } = body;
    const temporaryPassword = randomString(
      10,
      '23456789ABCDEFGHJLMNPQRSTUVWXYZabcdefghjmnpqrstuvwxyz',
    );

    await userResetPassword(userId, temporaryPassword);
    await invalidateToken(brand, userId);
    triggerAdminResetPasswordEmail(userId, temporaryPassword, brand);
    res.json({ temporaryPassword });
  };

  api.post(
    '/',
    wrapErrors((req, res) => resetPassword(req, res)),
  );

  return api;
};
