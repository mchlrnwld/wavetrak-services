provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/pw-reset-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "pw_reset_service" {
  source = "../../"

  company     = "sl"
  application = "password-reset-service"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "password-reset-service.sandbox.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"

  dns_name    = "password-reset-service.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled = false
}

