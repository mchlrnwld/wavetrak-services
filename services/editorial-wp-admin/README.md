# Editorial Wordpress Admin service

This module contains _only_ the infrastructure code for the `editorial-wp-admin` service. The application code can be found at https://github.com/Surfline/surfline-web-editorial.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments currently only include `sbox`, `staging` and `prod`.
