provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-editorial-wp-admin/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "sandbox"
  application = "editorial-wp-admin"

  aws_alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b/e78a34116d637d96"
  iam_role_arn         = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn    = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b"

  default_vpc = "vpc-981887fd"
  dns_name    = "editorial-wp-admin.${local.environment}.surfline.com"

  service_td_count = 1
}

module "editorial_wp_admin_service" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_td_count = local.service_td_count
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  aws_alb_listener_arn = local.aws_alb_listener_arn
  iam_role_arn         = local.iam_role_arn
  load_balancer_arn    = local.load_balancer_arn
}
