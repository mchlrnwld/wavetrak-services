provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-editorial-wp-admin/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "prod"
  application = "editorial-wp-admin"

  aws_alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/7d4f5548c2239855"
  load_balancer_arn    = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b"
  iam_role_arn         = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  default_vpc = "vpc-116fdb74"
  dns_name    = "editorial-wp-admin.${local.environment}.surfline.com"

  service_td_count = 2
}

module "editorial_wp_admin_service" {
  source = "../../"

  company      = local.company
  application  = local.application
  environment  = local.environment

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_td_count = local.service_td_count
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  aws_alb_listener_arn = local.aws_alb_listener_arn
  iam_role_arn         = local.iam_role_arn
  load_balancer_arn    = local.load_balancer_arn
}
