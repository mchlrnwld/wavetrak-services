provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-editorial-wp-admin/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "staging"
  application = "editorial-wp-admin"

  aws_alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/a39da1ea44320d7c"
  load_balancer_arn    = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-staging/bca6896a1b354474"
  iam_role_arn         = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"

  default_vpc = "vpc-981887fd"
  dns_name    = "editorial-wp-admin.${local.environment}.surfline.com"

  service_td_count = 2
}

module "editorial_wp_admin_service" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_td_count = local.service_td_count
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  aws_alb_listener_arn = local.aws_alb_listener_arn
  iam_role_arn         = local.iam_role_arn
  load_balancer_arn    = local.load_balancer_arn
}
