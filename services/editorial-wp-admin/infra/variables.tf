variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "aws_alb_listener_arn" {
}

variable "load_balancer_arn" {
}

variable "default_vpc" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_request_count"
}

variable "auto_scaling_min_size" {
  default = ""
}

variable "auto_scaling_max_size" {
  default = ""
}

variable "auto_scaling_up_count" {
  default = ""
}

variable "auto_scaling_down_count" {
  default = ""
}

variable "auto_scaling_target_value" {
  default = ""
}

variable "auto_scaling_alb_arn_suffix" {
  default = ""
}
