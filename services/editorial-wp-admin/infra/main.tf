data "aws_route53_zone" "dns_zone" {
  name = "${var.environment}.surfline.com."
}

data "aws_alb_listener" "listener_port_443" {
  arn = var.aws_alb_listener_arn
}

module "editorial_wp_admin_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = data.aws_route53_zone.dns_zone.zone_id
  ecs_cluster = "sl-core-svc-${var.environment}"

  service_td_name             = "sl-cms-${var.environment}-editorial-wp-admin"
  service_td_container_name   = "wp-admin"
  service_td_count            = var.service_td_count
  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/wp-admin/health/"
  service_alb_priority        = 430
  service_port                = 80

  alb_listener      = data.aws_alb_listener.listener_port_443.arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = false
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = ""
  auto_scaling_min_size       = ""
  auto_scaling_max_size       = ""
  auto_scaling_alb_arn_suffix = ""
}

resource "aws_alb_listener_rule" "wp_admin_rule" {
  listener_arn = data.aws_alb_listener.listener_port_443.arn
  priority     = 110

  action {
    type             = "forward"
    target_group_arn = module.editorial_wp_admin_service.target_group
  }

  condition {
    path_pattern {
      values = ["/wp-admin/*"]
    }
  }
}

resource "aws_alb_listener_rule" "wp_login_rule" {
  listener_arn = data.aws_alb_listener.listener_port_443.arn
  priority     = 120

  action {
    type             = "forward"
    target_group_arn = module.editorial_wp_admin_service.target_group
  }

  condition {
    path_pattern {
      values = ["/wp-login*"]
    }
  }
}
