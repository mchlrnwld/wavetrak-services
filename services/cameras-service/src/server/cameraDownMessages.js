import { Router } from 'express';
import DownMessages from '../stores/CameraDownMessages';

export function cameraDownMessages() {
  const api = Router();

  const getCameraDownMessages = (req, res) => {
    res.status(200).json(DownMessages);
  };

  api.get('/', getCameraDownMessages);

  return api;
}
