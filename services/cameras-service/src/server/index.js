import { setupExpress } from '@surfline/services-common';
import { camera } from './camera';
import { cameraClip } from './cameraClip';
import { cameraOfTheMoment } from './cameraOfTheMoment';
import { cameraDownMessages } from './cameraDownMessages';
import { logger } from '../common/logger.js';
import { cameraRecording } from './cameraRecording';
import { cameraRewind } from './cameraRewind';
import prerecorded from './prerecorded';
import embed from './embed';
import cameraCrowd from './cameraCrowd';

export const log = logger('cameras-service:server');

const syntaxErrorHandler = (error, _, res, next) => {
  if (error instanceof SyntaxError) {
    return res.status(400).send({ message: 'Syntax Error' });
  }
  return next(error);
};

const accessControlMiddleware = (req, res, next) => {
  let allowedOrigin = req.header('Origin') || '*';
  // eslint-disable-next-line no-param-reassign
  res.locals.scopes = req.header('X-Auth-AccessScope')?.split(',') || null;
  if (allowedOrigin === 'null' || allowedOrigin === '*') {
    const referer = req.header('referer');
    if (referer) {
      const refererParts = referer.split('/');
      allowedOrigin = `${refererParts[0]}//${refererParts[2]}`;
    }
  }
  res.header('Access-Control-Allow-Origin', allowedOrigin);
  res.header('Access-Control-Request-Headers', 'Origin');
  res.header(
    'Access-Control-Allow-Headers',
    // eslint-disable-next-line max-len
    'Origin, X-Requested-With, Content-Type, Accept, Credentials, X-Auth-ClientId, X-Auth-AnonymousId'
  );
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT, DELETE');
  next();
};

const routes = [
  ['/cameras/prerecorded', prerecorded(log)],
  ['/cameras/embed', embed(log)],
  ['/cameras/recording', cameraRecording()],
  ['/cameras/rewind', cameraRewind()],
  ['/cameras/messages', cameraDownMessages()],
  ['/cameras/cotm', cameraOfTheMoment()],
  ['/cameras', camera()],
  ['/clips', cameraClip()],
  ['/crowds', cameraCrowd()],
];

export const startApp = () => {
  const { app, server } = setupExpress({
    name: 'cameras-service',
    port: process.env.EXPRESS_PORT || 8081,
    log,
    setAccessControlHeaders: false,
    handlers: [accessControlMiddleware, ...routes, syntaxErrorHandler],
  });
  return { app, server };
};
