import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import moment from 'moment-timezone';
import * as cameraRecordingModel from '../model/CameraRecording';
import * as cameraModel from '../model/Camera';
import * as logger from '../common/logger';
import newrelic from 'newrelic';
import { isPremium } from '../external/entitlements';
import { getSpotsByCams } from '../external/spots';

const log = logger.logger('camera-service:recording');

export function cameraRewind() {
  const api = Router();

  const handleErrorForResponse = (errorCode, res, action) => (err) => {
    if (typeof err === 'object') {
      log.error({
        message: `camerasService:${action}`,
        stack: err,
        trace: err.stack
      });
    } else {
      log.error({
        message: `camerasService:${action}`,
        errorMessage: err,
        trace: err.stack
      });
    }
    res.status(errorCode).send(err);
  };

  /**
   *  Extract header X-Auth-UserId for User MongoDB ObjectId
   *  Use x-auth-userid if it exists, otherwise perform as expected
   *      + openresty still boots people if they are unauthenticated
   *      + More multi-purpose for admin tools, etc... that might not have an authenticated token
   *
   * @param req injects user{id} into request from req.headers['x-auth-userid']
   * @param res
   * @param next
   * @returns {*} Passes req/res to next middleware -- injects user{id} into request
   */
  const authenticateRequest = (req, res, next) => {
    if (req.headers['x-auth-userid']) {
      // eslint-disable-next-line no-param-reassign
      req.authenticatedUserId = req.headers['x-auth-userid'];
      const id = req.authenticatedUserId || null;
      newrelic.addCustomAttribute('userId', id);
    }

    next();
  };

  const getCamRewindByCamId = async (req, res) => {
    const startDate = Number.parseInt(req.query.startDate || Date.now() - 86400000, 10);
    const endDate = Number.parseInt(req.query.endDate || Date.now(), 10);
    const allowPartialMatch = (req.query.allowPartialMatch === 'true');
    const includeExpired = (req.query.includeExpired === 'true');
    const { cameraId } = req.params;

    if (!cameraId) {
      return res.status(400).json({
        message: 'Missing required parameter: cameraId'
      });
    }

    const foundCamera = (await cameraModel.getCameraById(cameraId));
    if (foundCamera.length === 0) {
      return res.status(400).json({
        message: 'No camera for passed cameraId.'
      });
    }

    const timezoneOffsetNumber = (tz) => {
      const tzChunked = tz.split(':');
      const tzHour = parseInt(tzChunked[0], 10);
      const tzMinutes = parseInt(tzChunked[1], 10);
      return tzHour + (tzMinutes / 60);
    };

    let spots = await getSpotsByCams(cameraId);

    spots = await Promise.all(spots.map(async spot => ({
      ...spot,
      timezoneOffset: timezoneOffsetNumber(moment().tz(spot.timezone).format('Z')),
      cameras: await Promise.all(spot.cams.map(async cam => {
        const camDetails = await cameraModel.getCameraById(cam);
        const { _id, title, alias } = camDetails[0];
        return {
          _id,
          title,
          alias,
          stillUrl: `https://camstills.cdn-surfline.com/${alias}/latest_small.jpg`,
          isPremium: camDetails[0].isPremium,
        };
      })),
    })));

    try {
      const cameraRecordings = await cameraRecordingModel.getCameraRecording(startDate, endDate,
        cameraId, allowPartialMatch, includeExpired);
      if (cameraRecordings instanceof Array && cameraRecordings.length === 0) {
        return res.status(200).json({ spots, clips: [] });
      }

      if (req.authenticatedUserId && !(await isPremium(req.authenticatedUserId))) {
        return res.status(200).json({ spots, clips: [] });
      }

      return res.status(200).json({
        spots,
        clips: cameraRecordings
      });
    } catch (err) {
      return handleErrorForResponse(400, res, 'getCamRewindByCamId')(err);
    }
  };

  api.get('/id/:cameraId', authenticateRequest, wrapErrors(getCamRewindByCamId));

  return api;
}
