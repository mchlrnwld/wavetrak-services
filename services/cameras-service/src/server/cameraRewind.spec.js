import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import express from 'express';

import { cameraRecording } from './cameraRecording';
import * as CameraRecording from '../model/CameraRecording';
import * as Camera from '../model/Camera';

import singleCameraModel from './fixtures/recording/singleCameraModel.json';
import singleRecordingModel from './fixtures/recording/singleRecordingModel.json';
import singleRecordingResponse from './fixtures/recording/singleRecordingResponse.json';


chai.use(dirtyChai);
chai.use(chaiHttp);

describe('/cameras/rewind', () => {
  let request;

  before(() => {
    const app = express();
    app.use(cameraRecording());
    request = chai.request(app);
  });

  describe('GET', () => {
    context('camera exists', () => {
      beforeEach(() => {
        sinon.stub(CameraRecording, 'getCameraRecording');
        sinon.stub(Camera, 'getCameraById');

        CameraRecording.getCameraRecording.resolves({
          ...singleRecordingResponse,
          toJSON: sinon.stub().returns(singleRecordingResponse)
        });
      });

      afterEach(() => {
        CameraRecording.getCameraRecording.restore();
        Camera.getCameraById.restore();
      });

      it('gets camera recordings with cameraId', async () => {
        const cameraId = singleRecordingModel[0].cameraId;
        const startDate = new Date(singleRecordingModel[0].startDate);
        const startTimestampInMs = startDate.getTime();
        const endDate = new Date(singleRecordingModel[0].endDate);
        const endTimestampInMs = endDate.getTime();

        Camera.getCameraById.resolves({
          ...singleCameraModel,
          toJSON: sinon.stub().returns(singleCameraModel)
        });

        const res = await request.get(
          `/id/${cameraId}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}`
        ).send();

        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(singleRecordingResponse);
        expect(CameraRecording.getCameraRecording).to.have.been.calledOnce();
        expect(Camera.getCameraById).to.have.been.calledOnce();

        expect(CameraRecording.getCameraRecording).to.have.been.calledWithExactly(
          startTimestampInMs, endTimestampInMs, cameraId, false, false
        );
      });
    });

    context('camera non-existent', () => {
      beforeEach(() => {
        sinon.stub(Camera, 'getCameraById');
      });

      afterEach(() => {
        Camera.getCameraById.restore();
      });

      it('should return 400 w/ non-existent cameraId', async () => {
        const cameraId = singleRecordingModel[0].cameraId;
        const startDate = new Date(singleRecordingModel[0].startDate);
        const startTimestampInMs = startDate.getTime();
        const endDate = new Date(singleRecordingModel[0].endDate);
        const endTimestampInMs = endDate.getTime();

        Camera.getCameraById.resolves([]);

        try {
          const res = await request.get(
            `/id/${cameraId}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}
            &includeExpired=true`
          ).send();
          expect(res).to.have.status(400);
        } catch (err) {
          expect(err.response).to.have.status(400);
          expect(err.response).to.be.json();
          expect(err.response.body).to.deep.equal({
            message: 'No camera for passed cameraId.'
          });
          expect(Camera.getCameraById).to.have.been.calledOnce();
        }
      });
    });
  });
});
