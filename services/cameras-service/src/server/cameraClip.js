import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as logger from '../common/logger';
import CameraClip, { createCameraClip } from '../model/CameraClipModel';
import sendSqsMessage from './sendSqsMessage';
import { Types } from 'mongoose';
import { keyBy } from 'lodash';
import { json } from 'body-parser';

const log = logger.logger('camera-clip');

const NOT_FOUND = { message: 'Camera clip not found' };

export function cameraClip() {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/clips',
      request: {
        method: req.method,
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body
      }
    });
    next();
  };

  const createCameraClipHandler = async (req, res) => {
    let clip;
    try {
      clip = await createCameraClip({
        ...req.body
      });
    } catch (err) {
      log.error({ err, message: 'Error creating clip' });
      return res.status(500).send({ message: 'Error creating clip' });
    }

    try {
      await sendSqsMessage( // eslint-disable-line no-unused-expressions
        process.env.AWS_CLIP_GENERATION_SQS_QUEUE_URL,
        JSON.stringify({
          clipId: clip.clipId,
          cameraId: clip.cameraId,
          startTimestampInMs: clip.startTimestampInMs,
          endTimestampInMs: clip.endTimestampInMs,
        })
      );
    } catch (err) {
      log.error({ err, message: 'Error sending SQS message' });
      return res.status(500).send({ message: 'Error sending SQS message' });
    }

    return res.send(clip);
  };

  const updateCameraClipHandler = async (req, res) => {
    const {
      cameraId,
      startTimestampInMs,
      endTimestampInMs,
      status,
      bucket,
      clip,
      thumbnail
    } = req.body;

    const clipId = req.params.clipId;

    if (!Types.ObjectId.isValid(clipId)) {
      return res.status(404).send(NOT_FOUND);
    }

    let storedClip;
    try {
      storedClip = await CameraClip.findOne({ _id: clipId });
    } catch (err) {
      log.error({ err, message: 'Error retrieving clip' });
      return res.status(500).send({ message: 'Error retrieving clip' });
    }
    if (!storedClip) return res.status(404).send(NOT_FOUND);

    storedClip.cameraId = cameraId;
    storedClip.startTimestampInMs = startTimestampInMs;
    storedClip.endTimestampInMs = endTimestampInMs;
    storedClip.status = status;
    storedClip.bucket = bucket;
    storedClip.clip = clip;
    storedClip.thumbnail = thumbnail;

    try {
      await storedClip.save(); // eslint-disable-line no-unused-expressions
    } catch (err) {
      log.error({ err, message: 'Error saving clip' });
      return res.status(500).send({ message: 'Error saving clip' });
    }

    return res.send(storedClip);
  };

  const getCameraClipsHandler = async (req, res) => {
    const {
      ids,
      fields,
    } = req.query;

    const objIds = [];

    if (ids) {
      // Omit invalid keys from filter
      ids.split(',').forEach(id => {
        if (Types.ObjectId.isValid(id)) {
          objIds.push(new Types.ObjectId(id));
        }
      });
    } else {
      log.warn('Clip requested without IDs.');
      return res.status(400).send(
        { message: 'Please specify one or more clip IDs.' }
      );
    }

    let query = CameraClip.find({ _id: { $in: objIds } });

    // If specific fields were requested, filter for them
    if (fields) {
      query = query.select(fields.replace(/,/g, ' '));
    }

    let clips;
    try {
      clips = await query;
    } catch (err) {
      log.error({ err, message: 'Error retrieving clips' });
      return res.status(500).send({ message: 'Error retrieving clips' });
    }

    log.trace({ clips });

    // Return clips in order of the requested ids
    let hasValidClips = false;
    let hasInvalidClips = false;
    const groupedClips = keyBy(clips, 'clipId');
    const sortedClips = ids.split(',').map(id => {
      let clip;
      if (groupedClips.hasOwnProperty(id)) {
        clip = groupedClips[id];
        hasValidClips = true;
      } else {
        clip = {
          clipId: id,
          status: 'CLIP_DOES_NOT_EXIST'
        };
        hasInvalidClips = true;
      }
      return clip;
    });

    let statusCode = 200;
    if (hasValidClips && hasInvalidClips) statusCode = 207;
    if (!hasValidClips && hasInvalidClips) statusCode = 404;

    return res.status(statusCode).send(sortedClips);
  };

  api.use('*', json());
  api.get('/', trackRequest, wrapErrors(getCameraClipsHandler));
  api.post('/', trackRequest, wrapErrors(createCameraClipHandler));
  api.put('/:clipId', trackRequest, wrapErrors(updateCameraClipHandler));

  return api;
}

export default cameraClip;
