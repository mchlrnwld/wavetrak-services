import { Router } from 'express';
import { APIError, NotFound, wrapErrors } from '@surfline/services-common';
import * as logger from '../common/logger';
import { json } from 'body-parser';
import { checkStatusIsValid, createCameraCrowds, getCameraCrowds } from '../model/CameraCrowds';
import CameraCrowdsModel from '../model/CameraCrowdsModel';
import * as cameraModel from '../model/Camera';
import mongoose from 'mongoose';
import { average, standardDeviation } from '../utils/mathsHelpers';

const log = logger.logger('camera-clip');

const DEFAULT_INTERVAL = 60 * 30;

const checkCam = async (cameraId) => {
  const foundCamera = await cameraModel.getCameraById(cameraId);
  if (foundCamera.length === 0) {
    throw new NotFound('No camera for passed cameraId.');
  }
  return foundCamera;
};

export function cameraCrowd() {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/crowds',
      request: {
        method: req.method,
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body
      }
    });
    next();
  };

  const createCameraCrowdsHandler = async ({ body }, res) => {
    if (!Array.isArray(body)) {
      return res.status(400).send({
        message: 'Crowds must be passed as an array'
      });
    }
    const insertedData = await Promise.all(body.map(async item => (
      (await createCameraCrowds(item)).toObject()
    )));
    return res.json(insertedData);
  };

  const patchCameraCrowdsHandler = async ({ params: { stillObjectKey }, body }, res) => {
    // check the object exists in the DB
    const existingObject = await CameraCrowdsModel.findOne({ stillObjectKey });
    if (!existingObject) {
      throw new APIError(`No crowd count for ${stillObjectKey}.`);
    }
    const { waterCount, beachCount, visibility, status, annotatedObjectKey } = body;
    // update the existing objects values
    if (visibility && !existingObject.visibility) existingObject.visibility = visibility;
    if (visibility && existingObject.visibility) {
      // update visibility variables don't just replace whole object
      for (const [key, value] of Object.entries(visibility)) {
        existingObject.visibility[key] = value;
      }
    }

    for (const [key, value] of Object.entries({ waterCount, beachCount })) {
      if (value === null || value >= 0) {
        existingObject[key] = value;
      } else if (value !== undefined) {
        throw new APIError(`${key} cannot be less than 0.`);
      }
    }
    if (annotatedObjectKey) existingObject.annotatedObjectKey = annotatedObjectKey;
    if (status) existingObject.status = status;
    checkStatusIsValid(existingObject);
    // save the updated object
    try {
      const patchedCameraCrowd = await existingObject.save();
      return res.json(patchedCameraCrowd.toObject());
    } catch (error) {
      if (error instanceof mongoose.Error.ValidationError) {
        throw new APIError(error.message);
      }
      throw error;
    }
  };

  const getCrowdHandler = async ({ params: { crowdId } }, res) => {
    let _id;
    try {
      _id = mongoose.Types.ObjectId(crowdId);
    } catch (error) {
      return res.status(400).json({
        message: 'Crowd Id must a string of 24 hex characters',
      });
    }

    return res.json((await CameraCrowdsModel.findOne({ _id })).toObject());
  };


  const getCameraCrowdsHandler = async ({ params: { cameraId }, query: {
    startDate, endDate
  } }, res) => {
    if (!(startDate && endDate)) {
      throw new APIError('startDate and endDate are both required.');
    }

    // eslint-disable-next-line no-unused-expressions
    await checkCam(cameraId);

    const cameraCrowds = await getCameraCrowds(cameraId, startDate, endDate);

    return res.status(200).json(cameraCrowds);
  };

  const clipAboveSigma = (values) => {
    const mean = average(values);
    const std = standardDeviation(values);
    const valuesAboveSigma = values.filter(val => val > mean - std);
    return (
        valuesAboveSigma.length === 0 ?
        0 :
        average(valuesAboveSigma)
    );
  };


  /**
   * getCameraCrowdsSmoothedHandler returns crowd data aggregated at a configurable interval
   * @param {*} req express request object
   * @param {string} req.params.cameraId the camera id that crowd counts are wanted for
   * @param {string} req.query.startDate the start date for the period that we want crowd counts for
   * @param {string} req.query.endDate the end date for the period that we want crowd counts for
   * @param {string} req.query.interval the interval that we want the crowd counts at
   * (if the end date does not lie exactly on one of these intervals,
   * the interval that is cut short will be excluded from the results)
   * @param {*} res express result object
   * @returns null
   */
  const getCameraCrowdsSmoothedHandler = async ({ params: { cameraId }, query: {
    startDate, endDate, interval = DEFAULT_INTERVAL
  } }, res) => {
    if (!(startDate && endDate)) {
      throw new APIError('startDate and endDate are both required.');
    }

    if (endDate > Date.now() / 1000) {
      throw new APIError('endDate must not be in the future.');
    }

    // eslint-disable-next-line no-param-reassign
    startDate = Number(startDate);
    // eslint-disable-next-line no-param-reassign
    endDate = Number(endDate);
    // eslint-disable-next-line no-param-reassign
    interval = Number(interval);

    // eslint-disable-next-line no-unused-expressions
    await checkCam(cameraId);

    // we can only use data from whole periods otherwise results will be skewed
    const croppedEnd = endDate - (endDate - startDate) % interval;

    const cameraCrowds = await getCameraCrowds(cameraId, startDate, croppedEnd, true);

    const smoothedCrowds = [];

    for (let start = startDate; start < croppedEnd; start += interval) {
      const crowdBatch = cameraCrowds.filter(
        (crowd) => (
          crowd.timestamp >= start && crowd.timestamp < start + interval
        )
      );

      if (crowdBatch.length === 0) {
        smoothedCrowds.push({
          beachCount: null,
          waterCount: null,
          startDate: start,
          endDate: start + interval,
          status: 'CROWDS_UNAVAILABLE',
        });
      } else {
        const beachCounts = crowdBatch.map((crowd) => crowd.beachCount);
        const waterCounts = crowdBatch.map((crowd) => crowd.waterCount);

        smoothedCrowds.push({
          beachCount: clipAboveSigma(beachCounts),
          waterCount: clipAboveSigma(waterCounts),
          startDate: start,
          endDate: start + interval,
          status: 'CROWDS_AVAILABLE',
        });
      }
    }

    return res.status(200).json(smoothedCrowds);
  };


  api.use('*', json());
  api.get('/cam/:cameraId/raw', wrapErrors(getCameraCrowdsHandler));
  api.get('/cam/:cameraId', wrapErrors(getCameraCrowdsSmoothedHandler));
  api.get('/:crowdId', trackRequest, wrapErrors(getCrowdHandler));
  api.post('/', trackRequest, wrapErrors(createCameraCrowdsHandler));
  api.patch('/:stillObjectKey', trackRequest, wrapErrors(patchCameraCrowdsHandler));

  return api;
}

export default cameraCrowd;
