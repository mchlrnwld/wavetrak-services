import AWS from 'aws-sdk';
import snsMessageConfig from '../snsMessageConfig';

const AWS_REGION = 'us-west-1';
const AWS_API_VERSION = '2010-03-31';

const publishCameraTopic = (
  camera,
  topic = 'AWS_CAMERA_DETAILS_UPDATED_TOPIC',
  settings = {},
) => new Promise((resolve, reject) => {
  if (process.env.NODE_ENV === 'test') return resolve();

  AWS.config.update({
    region: AWS_REGION,
  });

  const sns = new AWS.SNS({ apiVersion: AWS_API_VERSION });
  const topicArn = snsMessageConfig[topic].arn;
  const payload = {
    cameraId: `${camera._id}`,
    topic,
    settings,
  };
  const subject = `Cameras Service: ${topic}`;
  const params = {
    TopicArn: topicArn,
    Message: JSON.stringify({
      default: JSON.stringify(payload),
    }),
    MessageStructure: 'json',
    Subject: subject,
  };

  return sns.publish(params, (err, data) => {
    if (err) return reject(err);
    return resolve(data);
  });
});

export default publishCameraTopic;
