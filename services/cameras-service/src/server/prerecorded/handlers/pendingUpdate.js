import moment from 'moment-timezone';
import CameraModel from '../../../model/CameraModel';
import { getSpotsByCams } from '../../../external/spots';

export const getHandler = async (req, res) => {
  const now = new Date();
  const currentUTC = Date.UTC(
    now.getUTCFullYear(),
    now.getUTCMonth(),
    now.getUTCDate(),
    now.getUTCHours(),
    now.getUTCMinutes(),
    now.getUTCSeconds(),
    now.getUTCMilliseconds(),
  );
  // Prerecorded cams with last prerecorded clip end time older than current UTC time.
  const allCamsPendingUpdate = await CameraModel.find({
    $and: [
      { isPrerecorded: true },
      { 'prerecordedClipRecordingTimes.0': { $exists: true } },
      {
        $or: [
          { lastPrerecordedClipEndTimeUTC: { $lt: now.toUTCString() } },
          { lastPrerecordedClipEndTimeUTC: { $exists: false } },
        ],
      },
    ],
  });

  const matchingSpots = await getSpotsByCams(
    allCamsPendingUpdate.map(cam => cam._id).toString()
  );
  const pendingUpdate = allCamsPendingUpdate.map((cam) => {
    const matchSpot = matchingSpots.filter(
      spot => spot.cams[0].toString() === cam._id.toString()
    );
    if (matchSpot.length > 0) {
      const { timezone } = matchSpot[0];
      const zone = moment.tz.zone(timezone);
      const tzOffset = zone.parse(currentUTC);
      const timeRanges = cam.prerecordedClipRecordingTimes.map((timeRange) => {
        const {
          startHours,
          startMinutes,
          endHours,
          endMinutes,
        } = timeRange;
        // CMS recording times are set as local. Convert to UTC here.
        const startDateUTC = Date.UTC(
          now.getUTCFullYear(),
          now.getUTCMonth(),
          now.getUTCDate(),
          startHours,
          startMinutes,
          0,
          0,
        ) + (tzOffset * 60000);
        const endDateUTC = Date.UTC(
          now.getUTCFullYear(),
          now.getUTCMonth(),
          now.getUTCDate(),
          endHours,
          endMinutes,
          0,
          0,
        ) + (tzOffset * 60000);
        return {
          startDateUTC,
          endDateUTC,
        };
      })
      // Time range should not be before current UTC time
        .filter(timeRange => timeRange.endDateUTC <= currentUTC)
      // Time range should not be less than the last prerecorded clip time
        .filter(timeRange =>
          timeRange.endDateUTC > new Date(cam.lastPrerecordedClipEndTimeUTC).getTime() ||
        !cam.lastPrerecordedClipEndTimeUTC
        );
      return {
        id: cam._id,
        alias: cam.alias,
        recordingTimeRange: timeRanges.length > 0 ?
          // Only retrieve the latest time range to record
          timeRanges.reduce((prevRange, currentRange) =>
            (prevRange.endDateUTC > currentRange.endDateUTC) ? prevRange : currentRange
          ) : null,
      };
    }
    return {
      id: cam._id,
      alias: cam.alias,
      recordingTimeRange: null,
    };
  })
    .filter(pending => pending.recordingTimeRange);
  res.send({ pendingUpdate });
};
