import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import { pendingUpdate } from './handlers';

export const trackRequests = log => (req, res, next) => {
  log.trace({
    action: '/prerecorded',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const prerecorded = (log) => {
  const api = Router();
  api.use('*', json(), trackRequests(log));

  api.get('/pendingUpdate', wrapErrors(pendingUpdate.getHandler));

  return api;
};


export default prerecorded;
