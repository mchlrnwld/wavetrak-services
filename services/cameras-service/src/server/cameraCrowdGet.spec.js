/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
import chai, { AssertionError, expect } from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';
import { log, startApp } from './';
import CameraCrowdsModel from '../model/CameraCrowdsModel';
import singleCameraModel from './fixtures/recording/singleCameraModel.json';
import * as cameraModel from '../model/Camera';
chai.use(dirtyChai);
chai.use(chaiHttp);
let mongoServer;
let thisServer;

const CAMERA_ID = singleCameraModel[0]._id;

describe('GET/cam/:camId', () => {
  before(async () => {
    mongoServer = new MongodbMemoryServer();

    const mongoUri = await mongoServer.getUri();
    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);

    const { server } = await startApp(log);
    thisServer = server;

    await cameraModel.createCamera(singleCameraModel[0]);

    await CameraCrowdsModel.insertMany([
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986340,
        waterCount: 11,
        beachCount: 1,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986400,
        waterCount: 11,
        beachCount: 1,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986460,
        waterCount: 12,
        beachCount: 3,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986520,
        waterCount: 16,
        beachCount: 2,
        status: 'FATAL_ERROR',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986580,
        waterCount: 16,
        beachCount: 5,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986640,
        waterCount: 15,
        beachCount: 7,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986700,
        waterCount: 10,
        beachCount: 9,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986760,
        waterCount: 5,
        beachCount: 3,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986820,
        waterCount: 6,
        beachCount: 2,
        status: 'CROWD_AVAILABLE',
      },
      {
        cameraId: new mongoose.Types.ObjectId(CAMERA_ID),
        timestamp: 1634986880,
        waterCount: 3,
        beachCount: 1,
        status: 'CROWD_AVAILABLE',
      },
    ]);
  });

  after(() => {
    thisServer.close();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });

  describe('/raw', () => {
    it('returns data', async () => {
      const result = await chai
        .request(thisServer)
        .get(`/crowds/cam/${CAMERA_ID}/raw?startDate=1634986400&endDate=1634986520`)
        .send();

      const expected = [
        {
          cameraId: CAMERA_ID,
          timestamp: 1634986400,
          waterCount: 11,
          beachCount: 1,
          status: 'CROWD_AVAILABLE',
        },
        {
          cameraId: CAMERA_ID,
          timestamp: 1634986460,
          waterCount: 12,
          beachCount: 3,
          status: 'CROWD_AVAILABLE',
        },
        {
          cameraId: CAMERA_ID,
          timestamp: 1634986520,
          waterCount: 16,
          beachCount: 2,
          status: 'FATAL_ERROR',
        },
      ];

      expect(result).to.have.status(200);
      expect(result).to.be.json();
      expect(result.body.length).to.equal(expected.length);
      expected.forEach((item, index) => {
        expect(result.body[index]).to.eql({
          ...item,
          _id: result.body[index]._id
        });
      });
    });

    it('handles missing data', async () => {
      const result = await chai
        .request(thisServer)
        .get(`/crowds/cam/${CAMERA_ID}/raw?startDate=1634986200&endDate=1634986330`)
        .send();

      const expected = [];

      expect(result).to.have.status(200);
      expect(result).to.be.json();
      expect(result.body).to.eql(expected);
    });

    it('validates missing fields', async () => {
      try {
        await chai
          .request(thisServer)
          .get(`/crowds/cam/${CAMERA_ID}/raw`)
          .send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'Invalid Parameters: startDate and endDate are both required.'
        });
        return;
      }
      throw new AssertionError('Request did not throw error');
    });

    it('checks whether cam exists', async () => {
      try {
        await chai
          .request(thisServer)
          .get('/crowds/cam/41224d776a326fb40f000001/raw?startDate=1635123456&endDate=1635123457')
          .send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'No camera for passed cameraId.'
        });
        return;
      }
      throw new AssertionError('Request did not throw error');
    });
  });

  describe('smoothed', () => {
    it('returns data', async () => {
      const result = await chai
        .request(thisServer)
        .get(`/crowds/cam/${CAMERA_ID}?startDate=1634986400&endDate=1634986820&interval=180`)
        .send();

      const expected = [
        {
          startDate: 1634986400,
          endDate: 1634986580,
          waterCount: 12,
          beachCount: 3,
          status: 'CROWDS_AVAILABLE'
        },
        {
          startDate: 1634986580,
          endDate: 1634986760,
          waterCount: 15.5,
          beachCount: 8,
          status: 'CROWDS_AVAILABLE'
        },
      ];

      expect(result).to.have.status(200);
      expect(result).to.be.json();
      expect(result.body).to.eql(expected);
    });

    it('handles time range smaller than interval', async () => {
      const result = await chai
        .request(thisServer)
        .get(`/crowds/cam/${CAMERA_ID}?startDate=1634986200&endDate=1634986330&interval=180`)
        .send();

      const expected = [];

      expect(result).to.have.status(200);
      expect(result).to.be.json();
      expect(result.body).to.eql(expected);
    });

    it('handles missing data', async () => {
      const result = await chai
        .request(thisServer)
        .get(`/crowds/cam/${CAMERA_ID}?startDate=1634986000&endDate=1634986330&interval=180`)
        .send();

      const expected = [{
        startDate: 1634986000,
        endDate: 1634986180,
        waterCount: null,
        beachCount: null,
        status: 'CROWDS_UNAVAILABLE'
      }];

      expect(result).to.have.status(200);
      expect(result).to.be.json();
      expect(result.body).to.eql(expected);
    });

    it('validates missing fields', async () => {
      try {
        await chai
          .request(thisServer)
          .get(`/crowds/cam/${CAMERA_ID}`)
          .send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'Invalid Parameters: startDate and endDate are both required.'
        });
        return;
      }
      throw new AssertionError('Request did not throw error');
    });

    it('checks whether cam exists', async () => {
      try {
        await chai
          .request(thisServer)
          .get('/crowds/cam/41224d776a326fb40f000001?startDate=1635123456&endDate=1635123457')
          .send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'No camera for passed cameraId.'
        });
        return;
      }
      throw new AssertionError('Request did not throw error');
    });

    it('checks whether endDate is in the past', async () => {
      const futureDate = Date.now() / 1000 + 100;
      try {
        await chai
          .request(thisServer)
          .get(`/crowds/cam/41224d776a326fb40f000001?startDate=1635123456&endDate=${futureDate}`)
          .send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'Invalid Parameters: endDate must not be in the future.'
        });
        return;
      }
      throw new AssertionError('Request did not throw error');
    });
  });
});
