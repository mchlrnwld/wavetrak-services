/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';
import nock from 'nock';

import * as CameraRecording from '../model/CameraRecording';
import * as Camera from '../model/Camera';
import { log, startApp } from './';

import createRecordingModel from './fixtures/recording/createRecordingModel.json';
import createRecordingResponse from './fixtures/recording/createRecordingResponse.json';
import singleCameraModel from './fixtures/recording/singleCameraModel.json';
import singleRecordingModel from './fixtures/recording/singleRecordingModel.json';
import singleRecordingResponse from './fixtures/recording/singleRecordingResponse.json';
import { primeHighlights } from './fixtures/recording/highlightsRecordingsModel.js';
import CameraRecordingModel from '../model/CameraRecordingModel';


chai.use(dirtyChai);
chai.use(chaiHttp);
let mongoServer;
let thisServer;

process.env.ENTITLEMENTS_SERVICE = 'http://entitlements-service.fake.surfline.com';

describe('/cameras/recording', () => {
  beforeEach(async () => {
    sinon.stub(log, 'info'); // silence express app info for integration tests to keep output clean

    mongoServer = new MongodbMemoryServer();

    const mongoUri = await mongoServer.getUri();
    mongoose.connect(
      mongoUri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      err => err,
    );

    const { server } = await startApp(log);
    thisServer = server;

    await Camera.createCamera(singleCameraModel[0]);
  });

  afterEach(() => {
    log.info.restore();
    thisServer.close();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });

  describe('GET', () => {
    beforeEach(async () => {
      await CameraRecording.createCameraRecording(singleRecordingModel[0]);
    });

    it('gets camera recordings with cameraId, for non-premium user with sl_metered entitlement', async () => {
      const cameraId = singleRecordingModel[0].cameraId;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const meteredEntitlementResponse = { entitlements: ['sl_metered'] };
      const userId = '123456';

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, meteredEntitlementResponse);
      const res = await chai.request(thisServer).get(
        `/cameras/recording/id/${cameraId}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}`
        )
        .set('x-auth-userid', userId)
        .set('x-auth-scopes', ['premium:read:registered'])
        .set('x-auth-anonymousid', 654321)
        .send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(singleRecordingResponse);
    });

    it('gets camera recordings with an alias', async () => {
      const cameraAlias = singleRecordingModel[0].alias;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();

      const res = await chai.request(thisServer).get(
        `/cameras/recording/${cameraAlias}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}`
      ).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(singleRecordingResponse);
    });

    it('should return 400 w/ non-existent alias', async () => {
      const cameraAlias = singleRecordingModel[0].alias;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();

      try {
        await chai.request(thisServer).get(
          `/cameras/recording/${cameraAlias}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}
          &includeExpired=true`
        ).send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'No camera for passed alias.'
        });
      }
    });

    it('should return 400 w/ non-existent cameraId', async () => {
      const cameraId = singleRecordingModel[0].cameraId;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();

      try {
        await chai.request(thisServer).get(
          `/cameras/recording/id/${cameraId}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}
          &includeExpired=true`
        ).send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'No camera for passed cameraId.'
        });
      }
    });

    it('should return highlights for a camera, for non-premium user with sl_metered entitlement', async () => {
      const {
        highlightsRecordings,
        expectedTimeRange,
        expectedHighlights,
        highlightsCam,
        expires,
      } = primeHighlights();
      await CameraRecordingModel.deleteMany({});
      await CameraRecordingModel.insertMany(highlightsRecordings);
      const meteredEntitlementResponse = { entitlements: ['sl_metered'] };
      const userId = '123456';

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, meteredEntitlementResponse);
      const res = await chai.request(thisServer)
        .get(`/cameras/recording/highlights/${highlightsCam}`)
        .set('x-auth-userid', userId)
        .set('x-auth-scopes', ['premium:read:registered'])
        .set('x-auth-anonymousid', 123456)
        .query(expectedTimeRange);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(expectedHighlights);
      expect(res.headers.expires).to.equal(expires.toUTCString());
    });

    it('should return default highlights for a camera, for non-premium user with sl_metered entitlement', async () => {
      const {
        highlightsRecordings,
        expectedHighlights,
        highlightsCam,
        expires,
      } = primeHighlights(new Date());
      await CameraRecordingModel.deleteMany({});
      await CameraRecordingModel.insertMany(highlightsRecordings);
      const meteredEntitlementResponse = { entitlements: ['sl_metered'] };
      const userId = '123456';

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, meteredEntitlementResponse);
      const res = await chai.request(thisServer)
        .get(`/cameras/recording/highlights/${highlightsCam}`)
        .set('x-auth-userid', userId)
        .set('x-auth-scopes', ['premium:read:registered'])
        .set('x-auth-anonymousid', 123456);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(expectedHighlights);
      expect(res.headers.expires).to.equal(expires.toUTCString());
    });

    it('should return an empty array when no highlights available', async () => {
      const {
        highlightsRecordings,
        highlightsEndDate,
        highlightsCam
      } = primeHighlights();
      await CameraRecordingModel.deleteMany({});
      await CameraRecordingModel.insertMany(highlightsRecordings);
      const meteredEntitlementResponse = { entitlements: ['sl_metered'] };
      const userId = '123456';

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, meteredEntitlementResponse);
      const res = await chai.request(thisServer)
        .get(`/cameras/recording/highlights/${highlightsCam}`)
        .set('x-auth-userid', userId)
        .set('x-auth-scopes', ['premium:read:registered'])
        .set('x-auth-anonymousid', 123456)
        .query({
          startDate: highlightsEndDate.getTime() + 10 * 60 * 1000,
          endDate: highlightsEndDate.getTime() + 20 * 60 * 1000,
        });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal([]);
    });
  });

  describe('POST', () => {
    it('creates single recording w/ cameraId', async () => {
      const res = await chai.request(thisServer).post('/cameras/recording/id/58349c1fe411dc743a5d52ad').send(createRecordingModel);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...createRecordingResponse,
        _id: res.body._id,
        expireSoft: res.body.expireSoft,
      });
      const returnedDate = new Date(res.body.expireSoft).getTime();
      const expectedDate = new Date(Date.now() + 6 * 86400000).getTime();
      expect(Math.abs(returnedDate - expectedDate)).to.be.within(0, 200, 'expireSoft not within 200ms of expected date');
    });

    it('creates single recording with highlights', async () => {
      const highlights = {
        url: 'http://test.com/file.mp4',
        thumbUrl: 'http://test.com/file.jpg',
        gifUrl: 'http://test.com/file.gif',
      };
      const res = await chai
        .request(thisServer)
        .post('/cameras/recording/id/58349c1fe411dc743a5d52ad')
        .send({
          ...createRecordingModel,
          highlights,
        });
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...createRecordingResponse,
        _id: res.body._id,
        expireSoft: res.body.expireSoft,
        highlights,
      });
      const returnedDate = new Date(res.body.expireSoft).getTime();
      const expectedDate = new Date(Date.now() + 6 * 86400000).getTime();
      expect(Math.abs(returnedDate - expectedDate)).to.be.within(0, 200, 'expireSoft not within 200ms of expected date');
    });

    it('returns 400 for invalid recording', async () => {
      const highlights = {
        url: 'http://test.com/file',
        thumbUrl: 'http://test.com/file.jpg',
        gifUrl: 'http://test.com/file.gif',
      };
      try {
        await chai
          .request(thisServer)
          .post('/cameras/recording/id/58349c1fe411dc743a5d52ad')
          .send({
            ...createRecordingModel,
            highlights,
          });
      } catch (err) {
        expect(err.response).to.have.status(400);
      }
    });

    it('should return 400 w/ non-existent cameraId', async() => {
      try {
        const res = await chai
          .request(thisServer)
          .post('/cameras/recording/id/48349c1fe411dc743a5d52ad')
          .send(createRecordingModel);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'No camera exists for passed cameraId.'
        });
      }
    });

    it('returns existing recording if duplicate entry attempted', async () => {
      const highlights = {
        url: 'http://test.com/file.mp4',
        thumbUrl: 'http://test.com/file.jpg',
        gifUrl: 'http://test.com/file.gif',
      };
      await chai
        .request(thisServer)
        .post('/cameras/recording/id/58349c1fe411dc743a5d52ad')
        .send({
          ...createRecordingModel,
          highlights,
        });
      const res = await chai
        .request(thisServer)
        .post('/cameras/recording/id/58349c1fe411dc743a5d52ad')
        .send({
          ...createRecordingModel,
          highlights,
        });
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...createRecordingResponse,
        _id: res.body._id,
        expireSoft: res.body.expireSoft,
        highlights,
      });
      const returnedDate = new Date(res.body.expireSoft).getTime();
      const expectedDate = new Date(Date.now() + 6 * 86400000).getTime();
      expect(Math.abs(returnedDate - expectedDate)).to.be.within(0, 200, 'expireSoft not within 200ms of expected date');
    });
  });

  describe('Authentication', () => {
    beforeEach(async () => {
      await CameraRecording.createCameraRecording(singleRecordingModel[0]);
    });

    afterEach(() => {
      nock.cleanAll();
    });

    it('gets camera recordings for internal anonymous requests', async () => {
      const cameraAlias = singleRecordingModel[0].alias;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();

      const res = await chai.request(thisServer).get(
        `/cameras/recording/${cameraAlias}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}`);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(singleRecordingResponse);
    });

    it('gets camera recordings for premium user', async () => {
      const accessToken = 'abc123';
      const userId = '123456';
      const cameraAlias = singleRecordingModel[0].alias;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const premiumEntitlementResponse = { entitlements: ['sl_premium'] };

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, premiumEntitlementResponse);

      const res = await chai.request(thisServer).get(
        `/cameras/recording/${cameraAlias}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}&accesstoken=${accessToken}`)
        .set('x-auth-userid', userId);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(singleRecordingResponse);
    });

    it('gets camera recordings for non-premium user with sl_metered entitlement', async () => {
      const accessToken = 'abc123';
      const userId = '123456';
      const cameraAlias = singleRecordingModel[0].alias;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const meteredEntitlementResponse = { entitlements: ['sl_metered'] };

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, meteredEntitlementResponse);
      const res = await chai.request(thisServer).get(
        `/cameras/recording/${cameraAlias}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}&accesstoken=${accessToken}`)
        .set('x-auth-userid', userId)
        .set('x-auth-scopes', ['premium:read:registered'])
        .set('x-auth-anonymousid', 123456);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(singleRecordingResponse);
    });

    it('returns 403 for non-premium user without sl_metered entitlement', async () => {
      const accessToken = 'abc123';
      const userId = '123456';
      const cameraAlias = singleRecordingModel[0].alias;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const noEntitlementResponse = { entitlements: [] };

      nock(`${process.env.ENTITLEMENTS_SERVICE}`)
        .get(`/entitlements?objectId=${userId}`)
        .times(2)
        .reply(200, noEntitlementResponse);
      try {
        const res = await chai.request(thisServer).get(
          `/cameras/recording/${cameraAlias}?startDate=${startTimestampInMs}&endDate=${endTimestampInMs}&accesstoken=${accessToken}`)
          .set('x-auth-userid', userId);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(403);
        expect(err.response.body).to.deep.equal({
          message: 'Missing premium entitlement.',
        });
      }
    });
  });

  describe('PATCH', () => {
    it('validates an invalid camera-recording', async () => {
      const newData = {
        expireSoft: 'invalid date',
        highlights: {
          url: 'invalid clip',
          thumbUrl: 'invalid thumb',
          gifUrl: 'invalid gif',
          duration: 'invalid'
        }
      };

      const { _id } = await CameraRecording.createCameraRecording(singleRecordingModel[0]);

      try {
        await chai.request(thisServer)
          .patch(`/cameras/recording/${_id}`)
          .send(newData);
      } catch ({ response }) {
        expect(response).to.have.status(400);
        expect(response.body.message).to.eql(
          // eslint-disable-next-line no-multi-str
          'Invalid Parameters: CameraRecording validation failed: expireSoft: Cast to Date failed for value "invalid date" at path "expireSoft", \
highlights.duration: Cast to Number failed for value "invalid" at path "highlights.duration", \
highlights.url: Path `highlights.url` is invalid (invalid clip)., \
highlights.thumbUrl: Path `highlights.thumbUrl` is invalid (invalid thumb)., \
highlights.gifUrl: Path `highlights.gifUrl` is invalid (invalid gif).'
        );
      }
    });

    it('rejects non-existent recordings', async () => {
      try {
        await chai.request(thisServer)
          .patch('/cameras/recording/58349c1fe411dc743a5d52ad')
          .send({});
      } catch ({ response }) {
        expect(response).to.have.status(400);
        expect(response.body.message).to.eql(
          'Invalid Parameters: No recording for 58349c1fe411dc743a5d52ad.'
        );
      }
    });

    it('rejects invalid recording ids', async () => {
      try {
        await chai.request(thisServer)
          .patch('/cameras/recording/test')
          .send({});
      } catch ({ response }) {
        expect(response).to.have.status(400);
        expect(response.body.message).to.eql(
          'Cast to ObjectId failed for value "test" at path "_id" for model "CameraRecording"'
        );
      }
    });

    it('patches a camera recording', async () => {
      const newData = {
        expireSoft: new Date('2021-05-03T00:00:00.00Z'),
        highlights: {
          url: 'http://test.com/file.mp4',
          thumbUrl: 'http://test.com/file.jpg',
          gifUrl: 'http://test.com/file.gif',
          duration: 3000.5
        }
      };

      const { _id } = await CameraRecording.createCameraRecording(singleRecordingModel[0]);

      const res = await chai.request(thisServer)
        .patch(`/cameras/recording/${_id}`)
        .send(newData);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...singleRecordingModel[0],
        __v: 0,
        _id: _id.toString(),
        ...newData,
        expireSoft: newData.expireSoft.toJSON()
      });
    });

    it('patches a camera recording without duration', async () => {
      const newData = {
        expireSoft: new Date('2021-05-03T00:00:00.00Z'),
        highlights: {
          url: 'http://test.com/file.mp4',
          thumbUrl: 'http://test.com/file.jpg',
          gifUrl: 'http://test.com/file.gif'
        }
      };

      const { _id } = await CameraRecording.createCameraRecording(singleRecordingModel[0]);

      const res = await chai.request(thisServer)
        .patch(`/cameras/recording/${_id}`)
        .send(newData);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...singleRecordingModel[0],
        __v: 0,
        _id: _id.toString(),
        ...newData,
        expireSoft: newData.expireSoft.toJSON()
      });
    });

    it('patches a camera recording with a partial highlights obj', async () => {
      const newData = {
        highlights: {
          url: 'http://test.com/file2.mp4',
          gifUrl: 'http://test.com/file2.gif',
        },
      };

      const originalData = {
        ...singleRecordingModel[0],
        highlights: {
          url: 'http://test.com/file.mp4',
          thumbUrl: 'http://test.com/file.jpg',
          gifUrl: 'http://test.com/file.gif',
        },
      };

      const { _id, expireSoft } = await CameraRecording.createCameraRecording(originalData);

      const res = await chai.request(thisServer)
        .patch(`/cameras/recording/${_id}`)
        .send(newData);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...originalData,
        __v: 0,
        _id: _id.toString(),
        expireSoft: expireSoft.toJSON(),
        highlights: {
          url: 'http://test.com/file2.mp4',
          thumbUrl: 'http://test.com/file.jpg',
          gifUrl: 'http://test.com/file2.gif',
        },
      });
    });
  });
});
