/* eslint-disable dot-notation */
import { Router } from 'express';
import * as logger from '../common/logger';
import * as cameraModel from '../model/Camera';

import newrelic from 'newrelic';

export function cameraOfTheMoment() {
  const api = Router();
  const log = logger.logger('cameras-service');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/cameras/cotm',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body
      }
    });
    next();
  };

  /**
   *  Extract header X-Auth-UserId for User MongoDB ObjectId
   *  Use x-auth-userid if it exists, otherwise perform as expected
   *      + openresty still boots people if they are unauthenticated
   *      + More multi-purpose for admin tools, etc... that might not have an authenticated token
   *
   * @param req injects user{id} into request from req.headers['x-auth-userid']
   * @param res
   * @param next
   * @returns {*} Passes req/res to next middleware -- injects user{id} into request
   */
  const authenticateRequest = (req, res, next) => {
    if (req.headers['x-auth-userid']) {
      // eslint-disable-next-line no-param-reassign
      req.authenticatedUserId = req.headers['x-auth-userid'];
      const id = req.authenticatedUserId || null;
      newrelic.addCustomAttribute('userId', id);
    }

    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (err) => {
    if (typeof err === 'object') {
      log.error({
        message: `camerasService:${action}`,
        stack: err,
        trace: err.stack
      });
    } else {
      log.error({
        message: `camerasService:${action}`,
        errorMessage: err,
        trace: err.stack
      });
    }
    res.status(errorCode).send(err);
  };

  const getCameraOfTheMomentHandler = (req, res) => {
    const findCamera = cameraModel.getCameraOfTheMoment;
    return findCamera()
      .then((foundCamera) => {
        if (foundCamera instanceof Array && foundCamera.length === 0) {
          log.debug('Camera of the moment not found.');
          res.status(400).json({ message: 'Camera of the moment not found' });
        } else {
          res.status(200).json(foundCamera[0]);
        }
      })
      .catch(handleErrorForResponse(400, res, 'getCameraOfTheMomentHandler'));
  };

  const changeCameraOfTheMomentHandler = (req, res) => {
    const changeCameraOfTheMoment = cameraModel.changeCameraOfTheMoment;
    if (!req.params.id) {
      log.warn('Change camera of the moment request without a _id.');
      return res.status(400).send(
        { message: 'You need to specify an _id in your body.' }
      );
    }
    return cameraModel.getCameraById(req.params.id).then(
      (foundCameraById) => {
        if (foundCameraById.length < 1) {
          log.debug(`Change camera of the moment request with invalid ID: ${req.params.id}.`);
          return res.status(400).send({ message: `No camera with ID: ${req.params.id}` });
        }
        return changeCameraOfTheMoment(req.params.id)
          .then((updatedCamera) => {
            res.status(200).json(updatedCamera);
          })
          .catch(handleErrorForResponse(400, res, 'changeCameraOfTheMomentHandler'));
      });
  };


  api.get('/', trackRequest, authenticateRequest, getCameraOfTheMomentHandler);

  api.put('/:id', trackRequest, authenticateRequest, changeCameraOfTheMomentHandler);

  return api;
}
