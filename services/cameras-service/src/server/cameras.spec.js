/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
import express from 'express';
import bodyParser from 'body-parser';
import chai from 'chai';
import chaihttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import nock from 'nock';
import * as publishTopic from './publishTopic';
import * as logger from '../common/logger';
import { createCamera } from './../model/Camera';
import CameraModel from '../model/CameraModel';
import { log, startApp } from './';

const expect = chai.expect;
const assert = chai.assert;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);
sinonStubPromise(sinon);
chai.use(dirtyChai);
let mongoServer;
let thisServer;
process.env.SPOTS_API = 'http://spots-api.fake.surfline.com';

describe('camera route', () => {
  let loggerStub;
  let modelGetCameraPromise;
  let modelGetCameraStub;
  let modelUpdateCameraPromise;
  let modelUpdateCameraStub;
  let modelChangeAliasStub;
  let model;
  let camera;
  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace: () => {
      },
      debug: () => {
      },
      info: () => {
      },
      error: () => {
      },
      warn: () => {
      }
    });
    model = require('./../model/Camera');
    camera = require('./camera').camera;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use('/cameras', camera());
  });

  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    modelGetCameraStub = sinon.stub(model, 'getCameraByAlias');
    modelGetCameraPromise = modelGetCameraStub.returnsPromise();
    modelUpdateCameraStub = sinon.stub(model, 'updateCamera');
    modelUpdateCameraPromise = modelUpdateCameraStub.returnsPromise();
    modelChangeAliasStub = sinon.stub(model, 'changeCameraAlias');
    sinon.stub(publishTopic, 'default');
  });

  afterEach(() => {
    modelGetCameraStub.restore();
    modelUpdateCameraStub.restore();
    modelChangeAliasStub.restore();
    publishTopic.default.restore();
  });

  it('should initialize as an object', () => {
    expect(camera).to.exist;
  });

  it('should update a camera when requesting with id', (done) => {
    publishTopic.default.resolves({});
    chai.request(app)
      .put('/cameras/111111/UpdateCamera')
      .set('X-Auth-AccessScope', 'cameras_admin,cameras:read:admin')
      .send({ cameraTitle: 'Updated Camera Title' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    modelUpdateCameraPromise.resolves({ cameraTitle: 'Updated Camera Title' });
  });

  it('should not update a camera when requesting without a body', (done) => {
    chai.request(app)
      .put('/cameras/111111/UpdateCamera')
      .set('X-Auth-AccessScope', 'cameras_admin,cameras:read:admin')
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        expect(res).to.be.json;
        done();
      });
    modelUpdateCameraPromise.resolves({ _id: 111 });
  });

  it('should not update a camera alias when requesting without an alias in body', (done) => {
    chai.request(app)
      .put('/cameras/111111/ChangeAlias')
      .set('X-Auth-AccessScope', 'cameras_admin,cameras:read:admin')
      .send({})
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        expect(res).to.be.json;
        done();
      });
  });

  it('should not create a camera when requesting with invalid body params', (done) => {
    chai.request(app)
      .post('/cameras')
      .set('X-Auth-AccessScope', 'cameras_admin,cameras:read:admin')
      .send({ aliaser: 'e' })
      .end((err, res) => {
        expect(err.response.body.errors).to.not.be.null;
        expect(res).to.have.status(400);
        expect(res).to.be.json;
        done();
      });
  });

  it('should eject early when creating a camera without required params', (done) => {
    chai.request(app)
      .post('/cameras/')
      .set('X-Auth-AccessScope', 'cameras_admin,cameras:read:admin')
      .send({ randomParam: '' })
      .end((err, res) => {
        assert(err.response.body.message === 'Title and alias must be specified in the body.');
        expect(res).to.have.status(400);
        expect(res).to.be.json;
        done();
      });
  });

  it('should return a camera when validating an alias', (done) => {
    const alias = 'testcams';
    chai.request(app)
      .get('/cameras/ValidateAlias')
      .query({ alias })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.text).to.equal('{"alias":"testcams","_id":111}');
        done();
      });
    modelGetCameraPromise.resolves([{ _id: 111, alias }]);
  });
});

describe('camera route/validationAlias', () => {
  beforeEach(async () => {
    mongoServer = new MongodbMemoryServer();
    const mongoUri = await mongoServer.getUri();
    const { server } = await startApp(log);
    thisServer = server;
    return mongoose.connect(
        mongoUri,
        { useNewUrlParser: true, useUnifiedTopology: true },
        err => err,
      );
  });

  afterEach(() => {
    mongoServer = null;
    mongoose.disconnect();
    thisServer.close();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
    nock.cleanAll();
  });

  it('inserts a camera supporting crowds and highlights into mongoDB and should return a camera, its location and if it supports highlights or crowds, when validating an alias', async () => {
    // create a camera in mongoDB server
    const createdCamera = await createCamera({
      alias: 'testcams',
      message: 'cam message',
      modifiedDate: Date.now,
      title: 'cam title',
      playlistUrl: 'www.playlist.com',
      lastPrerecordedClipEndTimeUTC: 1486083520,
      lastPrerecordedClipStartTimeUTC: 1483405120,
      supportsHighlights: true,
      supportsCrowds: true,
    });
    const spotsResponse = [{
      location: {
        type: 'Point',
        coordinates: [12, 67],
      },
      timezone: 'America/Los_Angeles',
    }];
    // check that created cam exists
    expect(createdCamera).to.exist();
    const savedCam = await CameraModel.findOne({ _id: createdCamera._id });
    expect(savedCam.toObject()).to.be.eql(createdCamera);

    const result = {
      alias: 'testcams',
      _id: createdCamera._id.toString(),
      location: {
        type: 'Point',
        coordinates: [12, 67],
      },
      timezone: 'America/Los_Angeles',
      supportsCrowds: true,
      supportsHighlights: true,
    };

    // test validateAliasHandler
    nock(`${process.env.SPOTS_API}`)
      .get('/admin/spots/')
      .query(true)
      .reply(200, spotsResponse);
    const res = await chai.request(thisServer)
      .get(`/cameras/ValidateAlias?alias=${createdCamera.alias}`);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(result);
  });

  it('inserts a camera supporting highlights into mongoDB and should return a camera, its location and if it supports highlights or crowds, when validating an alias', async () => {
    // create a camera in mongoDB server
    const createdCamera = await createCamera({
      alias: 'testcams',
      message: 'cam message',
      modifiedDate: Date.now,
      title: 'cam title',
      playlistUrl: 'www.playlist.com',
      lastPrerecordedClipEndTimeUTC: 1486083520,
      lastPrerecordedClipStartTimeUTC: 1483405120,
      supportsHighlights: true,
    });
    const spotsResponse = [{
      location: {
        type: 'Point',
        coordinates: [12, 67],
      },
      timezone: 'America/Los_Angeles'
    }];
    // check that created cam exists
    expect(createdCamera).to.exist();
    const savedCam = await CameraModel.findOne({ _id: createdCamera._id });
    expect(savedCam.toObject()).to.be.eql(createdCamera);

    const result = {
      alias: 'testcams',
      _id: createdCamera._id.toString(),
      location: {
        type: 'Point',
        coordinates: [12, 67],
      },
      timezone: 'America/Los_Angeles',
      supportsCrowds: false,
      supportsHighlights: true,
    };

    // test validateAliasHandler
    nock(`${process.env.SPOTS_API}`)
      .get('/admin/spots/')
      .query(true)
      .reply(200, spotsResponse);
    const res = await chai.request(thisServer)
      .get(`/cameras/ValidateAlias?alias=${createdCamera.alias}`);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(result);
  });

  it('inserts a camera not supporting highlights/crowds into mongoDB and should return a camera, its location and if it supports highlights or crowds, when validating an alias', async () => {
    // create a camera in mongoDB server
    const createdCamera = await createCamera({
      alias: 'testcams',
      message: 'cam message',
      modifiedDate: Date.now,
      title: 'cam title',
      playlistUrl: 'www.playlist.com',
      lastPrerecordedClipEndTimeUTC: 1486083520,
      lastPrerecordedClipStartTimeUTC: 1483405120,
    });
    // check that created cam exists
    expect(createdCamera).to.exist();
    const savedCam = await CameraModel.findOne({ _id: createdCamera._id });
    expect(savedCam.toObject()).to.be.eql(createdCamera);

    const result = {
      alias: 'testcams',
      _id: createdCamera._id.toString(),
      supportsCrowds: false,
      supportsHighlights: false,
    };

    // test validateAliasHandler
    const res = await chai.request(thisServer)
      .get(`/cameras/ValidateAlias?alias=${createdCamera.alias}`);
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(result);
  });
});
