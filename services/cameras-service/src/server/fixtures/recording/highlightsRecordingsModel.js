
export const primeHighlights = date => {
  const recordings = [];
  const expected = [];
  let startDate;
  let endDate = date || new Date('2018-02-28T23:40:00.000Z');
  const highlightsEndDate = endDate;
  const highlightsCam = '58349c1fe411dc743a5d52ad';
  const highlightsCams = [highlightsCam, '12345c1fe411dc743a5d52ad'];
  let expectedStartDate;
  let expectedEndDate;


  highlightsCams.forEach((cameraId) => {
    for (let index = 6; index >= 0; index--) {
      startDate = new Date(endDate.getTime() - 10 * 60 * 1000);
      let highlights;
      if (index <= 2) {
        highlights = {
          url: `http://test.com/${cameraId}_${index}.mp4`,
          thumbUrl: `http://test.com/${cameraId}_${index}.jpg`,
          gifUrl: `http://test.com/${cameraId}_${index}.gif`,
        };
      }
      const recording = {
        cameraId,
        alias: 'a-test',
        startDate,
        endDate,
        recordingUrl: `http://stills.cdn-surfline.com/${cameraId}.${index}.mp4`,
        thumbLargeUrl: `http://stills.cdn-surfline.com/${cameraId}.${index}_large.jpg`,
        thumbSmallUrl: `https://camstills.cdn-surfline.com/${cameraId}.${index}_small.jpg`,
        resolution: '1280x720',
        expireSoft: Date.now() + 99999,
        highlights,
      };
      recordings.push(recording);

      if (index <= 2 && index > 0 && cameraId === highlightsCam) {
        const {
          // eslint-disable-next-line no-unused-vars
          expireSoft,
          // eslint-disable-next-line no-unused-vars
          resolution,
          startDate: start,
          endDate: end,
          ...returnedRecording
        } = recording;
        expected.push({
          ...returnedRecording,
          startDate: start.toJSON(),
          endDate: end.toJSON(),
        });
        expectedStartDate = Math.min(expectedStartDate || start.getTime(), start.getTime());
        expectedEndDate = Math.max(expectedEndDate || end.getTime(), end.getTime());
      }
      endDate = startDate;
    }
  });

  const highlightsStartDate = startDate;

  const expectedTimeRange = {
    startDate: expectedStartDate + 1,
    endDate: expectedEndDate - 1,
  };

  const expires = new Date(expectedEndDate + 60 * 12 * 1000);

  const highlightsRecordings = recordings;

  // requested cam, requested time range, in order from most recent
  const expectedHighlights = expected;

  return {
    highlightsEndDate,
    highlightsCam,
    highlightsStartDate,
    expectedTimeRange,
    highlightsRecordings,
    expectedHighlights,
    expires
  };
};
