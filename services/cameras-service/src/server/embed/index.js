import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import { configs } from './handlers';

export const trackRequests = log => (req, res, next) => {
  log.trace({
    action: '/embed',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const embed = log => {
  const api = Router();
  api.use('*', json(), trackRequests(log));
  api.post('/configs', wrapErrors(configs.createHandler));
  return api;
};

export default embed;
