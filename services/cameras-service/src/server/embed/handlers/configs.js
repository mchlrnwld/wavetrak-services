import hash from 'object-hash';
import publishTopic from '../../publishTopic';
import appConfig from '../../../config';

export const createHandler = async (req, res) => {
  const {
    showPreRollAds,
    showMidRollAds,
    showMidRollCTA,
    enablePremiumCam,
    playerTimeout,
    partner,
    event,
    adTarget,
    customMessageText,
    customMessageLink,
    camId,
    createdAt,
  } = req.body;
  const config = {
    showPreRollAds: showPreRollAds === 'true',
    showMidRollAds: showMidRollAds === 'true',
    showMidRollCTA: showMidRollCTA === 'true',
    enablePremiumCam: enablePremiumCam === 'true',
    playerTimeout: parseInt(playerTimeout, 10),
    partner,
    event,
    adTarget,
    customMessage: {
      text: customMessageText,
      href: customMessageLink,
    },
    camId,
    createdAt,
  };
  const fileHash = hash(config);
  const filename = `${fileHash}.json`;
  const json = {
    filename,
    config,
  };
  // eslint-disable-next-line no-unused-expressions
  await publishTopic({ _id: camId }, 'AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC', json);
  res.status(200).json({
    message: `Embed config created for ${camId}`,
    path: appConfig.EMBED_CDN,
    hash: fileHash,
  });
};
