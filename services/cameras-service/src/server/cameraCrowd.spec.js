/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';
import { log, startApp } from './';
import CameraCrowdsModel from '../model/CameraCrowdsModel';

chai.use(dirtyChai);
chai.use(chaiHttp);
let mongoServer;
let thisServer;

describe('/crowds', () => {
  before(async () => {
    mongoServer = new MongodbMemoryServer();

    const mongoUri = await mongoServer.getUri();
    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);

    const { server } = await startApp(log);
    thisServer = server;
  });

  after(() => {
    thisServer.close();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });

  // eslint-disable-next-line arrow-body-style
  afterEach(() => {
    return CameraCrowdsModel.deleteMany({});
  });

  it('POST/ rejects non array input gracefully', async () =>
    chai
      .request(thisServer)
      .post('/crowds/')
      .send({
        timestamp: 1582996461,
        cameraId: '5a21a180096c27001ac4f18f',
        stillObjectKey: 'testKey',
      })
      .catch(({ response }) => {
        expect(response).to.have.status(400);
        expect(response.body.message).to.equal('Crowds must be passed as an array');
      }));

  it('GET/:id rejects non id input gracefully', async () =>
    chai
      .request(thisServer)
      .get('/crowds/nonId')
      .catch(({ response }) => {
        expect(response).to.have.status(400);
        expect(response.body.message).to.equal('Crowd Id must a string of 24 hex characters');
      }));

  it('POST/ inserts crowd count', async () => {
    const res = await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'testKey',
        },
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18c',
          stillObjectKey: 'testKey2',
        },
      ]);
    expect(res).to.have.status(200);
    const expected = [
      {
        __v: 0,
        _id: res.body[0]._id,
        timestamp: 1582996461,
        cameraId: '5a21a180096c27001ac4f18f',
        stillObjectKey: 'testKey',
        waterCount: null,
        beachCount: null,
        status: 'CROWD_PENDING',
      },
      {
        __v: 0,
        _id: res.body[1]._id,
        timestamp: 1582996461,
        cameraId: '5a21a180096c27001ac4f18c',
        stillObjectKey: 'testKey2',
        waterCount: null,
        beachCount: null,
        status: 'CROWD_PENDING',
      },
    ];
    expect(res.body).to.eql(expected);

    return Promise.all(
      res.body.map(async (item, ind) => {
        const { body: savedCrowd } = await chai.request(thisServer).get(`/crowds/${item._id}`);
        return expect(savedCrowd).to.eql(expected[ind]);
      })
    );
  });

  it('PATCH/ request successfully updates the object that gets returned from the GET request.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      waterCount: 12,
      beachCount: null,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    const res = await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'objectTestKey',
        },
      ]);
    const expected = {
      __v: 0,
      _id: res.body[0]._id,
      timestamp: 1582996461,
      cameraId: '5a21a180096c27001ac4f18f',
      stillObjectKey: 'objectTestKey',
      annotatedObjectKey: 'annotated_object_key',
      waterCount: 12,
      beachCount: null,
      visibility: {
        clear: 1.234,
        fog: 1.234,
        glare: 2.345,
        rainblur: 2.345,
      },
      status: 'CROWD_AVAILABLE',
    };
    const res2 = await chai.request(thisServer).patch('/crowds/objectTestKey').send(payload);
    expect(res2).to.have.status(200);
    expect(res2).to.be.json();
    expect(res2.body).to.deep.eql(expected);
    const { body: savedCrowd } = await chai.request(thisServer).get(`/crowds/${res.body[0]._id}`);
    expect(savedCrowd).to.eql(expected);
  });

  it('PATCH/ request successfully updates the object that gets returned from the GET request, with one count value.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      waterCount: 12,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    const res = await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'objectTestKey',
          beachCount: 15,
        },
      ]);
    const expected = {
      __v: 0,
      _id: res.body[0]._id,
      timestamp: 1582996461,
      cameraId: '5a21a180096c27001ac4f18f',
      stillObjectKey: 'objectTestKey',
      annotatedObjectKey: 'annotated_object_key',
      waterCount: 12,
      beachCount: 15,
      visibility: {
        clear: 1.234,
        fog: 1.234,
        glare: 2.345,
        rainblur: 2.345,
      },
      status: 'CROWD_AVAILABLE',
    };
    const res2 = await chai.request(thisServer).patch('/crowds/objectTestKey').send(payload);
    expect(res2).to.have.status(200);
    expect(res2).to.be.json();
    expect(res2.body).to.deep.eql(expected);
    const { body: savedCrowd } = await chai.request(thisServer).get(`/crowds/${res.body[0]._id}`);
    expect(savedCrowd).to.eql(expected);
  });

  it('PATCH/ request successfully updates the object that gets returned from the GET request, with no count values.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      annotatedObjectKey: 'annotated_object_key',
    };
    const res = await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'objectTestKey',
        },
      ]);
    const expected = {
      __v: 0,
      _id: res.body[0]._id,
      timestamp: 1582996461,
      cameraId: '5a21a180096c27001ac4f18f',
      stillObjectKey: 'objectTestKey',
      annotatedObjectKey: 'annotated_object_key',
      beachCount: null,
      waterCount: null,
      visibility: {
        clear: 1.234,
        fog: 1.234,
        glare: 2.345,
        rainblur: 2.345,
      },
      status: 'CROWD_PENDING',
    };
    const res2 = await chai.request(thisServer).patch('/crowds/objectTestKey').send(payload);
    expect(res2).to.have.status(200);
    expect(res2).to.be.json();
    expect(res2.body).to.deep.eql(expected);
    const { body: savedCrowd } = await chai.request(thisServer).get(`/crowds/${res.body[0]._id}`);
    expect(savedCrowd).to.eql(expected);
  });

  it('PATCH/ request successfully updates the object that gets returned from the GET request, with partial visibility values.', async () => {
    const payload = {
      visibility: { clear: 1.111, fog: 2.222, rainblur: 3.333 },
      waterCount: 12,
      beachCount: null,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    const res = await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          visibility: { clear: 1.123, glare: 2.345, fog: 1.456, rainblur: 2.678 },
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'stillObjectKey',
        },
      ]);
    const expected = {
      __v: 0,
      _id: res.body[0]._id,
      timestamp: 1582996461,
      cameraId: '5a21a180096c27001ac4f18f',
      stillObjectKey: 'stillObjectKey',
      annotatedObjectKey: 'annotated_object_key',
      waterCount: 12,
      beachCount: null,
      visibility: {
        clear: 1.111,
        fog: 2.222,
        glare: 2.345,
        rainblur: 3.333,
      },
      status: 'CROWD_AVAILABLE',
    };
    const res2 = await chai.request(thisServer).patch('/crowds/stillObjectKey').send(payload);
    expect(res2).to.have.status(200);
    expect(res2).to.be.json();
    expect(res2.body).to.eql(expected);
    const { body: savedCrowd } = await chai.request(thisServer).get(`/crowds/${res.body[0]._id}`);
    expect(savedCrowd).to.eql(expected);
  });

  it('PATCH/ request with invalid enum status.', async () => {
    const payload = {
      visibility: { clear: 1.111, fog: 2.222, rainblur: 3.333 },
      waterCount: 12,
      beachCount: null,
      annotatedObjectKey: 'annotated_object_key',
      status: 'INVALID_STATUS',
    };
    await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'testKey',
        },
      ]);
    try {
      await chai.request(thisServer).patch('/crowds/testKey').send(payload);
    } catch (response) {
      expect(response).to.have.status(400);
    }
  });

  it('PATCH/ request returns a 400 when there is not an object with matching stillObjectKey.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      waterCount: 12,
      beachCount: null,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    try {
      await chai.request(thisServer).patch('/crowds/testKey').send(payload);
    } catch ({ response }) {
      expect(response).to.have.status(400);
      expect(response.body.message).to.equal('Invalid Parameters: No crowd count for testKey.');
    }
  });

  it('PATCH/ request returns a 400 when the waterCount < 0.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      waterCount: -1,
      beachCount: null,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'stillObjectKey',
        },
      ]);
    try {
      await chai.request(thisServer).patch('/crowds/stillObjectKey').send(payload);
    } catch ({ response }) {
      expect(response).to.have.status(400);
      // eslint-disable-next-line max-len
      expect(response.body.message).to.equal(
        'Invalid Parameters: waterCount cannot be less than 0.'
      );
    }
  });

  it('PATCH/ request returns a 400 when the beachCount < 0.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      waterCount: 12,
      beachCount: -10,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          visibility: { clear: 1.123, glare: 2.345, fog: 1.456, rainblur: 2.678 },
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'stillObjectKey',
        },
      ]);
    try {
      await chai.request(thisServer).patch('/crowds/stillObjectKey').send(payload);
    } catch ({ response }) {
      expect(response).to.have.status(400);
      expect(response.body.message).to.equal(
        'Invalid Parameters: beachCount cannot be less than 0.'
      );
    }
  });

  it('PATCH/ request returns a 400 when the waterCount is null and give status is CROWD_AVAILABLE.', async () => {
    const payload = {
      visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
      waterCount: null,
      beachCount: 10,
      annotatedObjectKey: 'annotated_object_key',
      status: 'CROWD_AVAILABLE',
    };
    await chai
      .request(thisServer)
      .post('/crowds/')
      .send([
        {
          timestamp: 1582996461,
          cameraId: '5a21a180096c27001ac4f18f',
          stillObjectKey: 'stillObjectKey',
        },
      ]);
    try {
      await chai.request(thisServer).patch('/crowds/stillObjectKey').send(payload);
    } catch ({ response }) {
      expect(response).to.have.status(400);
      expect(response.body.message).to.equal(
        'Invalid Parameters: status cannot be CROWD_AVAILABLE when waterCount is null'
      );
    }
  });
});
