import { Router } from 'express';
import * as cameraRecordingModel from '../model/CameraRecording';
import CameraAlias from '../model/CameraAliasModel';
import * as cameraModel from '../model/Camera';
import * as logger from '../common/logger';
import newrelic from 'newrelic';
import getEntitlements, { isPremium } from '../external/entitlements';
import { isMeteredPremium } from '../utils/isMeteredPremium';
import CameraRecordingModel from '../model/CameraRecordingModel';
import { APIError, wrapErrors } from '@surfline/services-common/dist';
import mongoose from 'mongoose';

const log = logger.logger('camera-service:recording');


export function cameraRecording() {
  const api = Router();

  const handleErrorForResponse = (errorCode, res, action) => (err) => {
    if (typeof err === 'object') {
      log.error({
        message: `camerasService:${action}`,
        stack: err,
        trace: err.stack
      });
    } else {
      log.error({
        message: `camerasService:${action}`,
        errorMessage: err,
        trace: err.stack
      });
    }
    res.status(errorCode).send(err);
  };

  /**
   *  Extract header X-Auth-UserId for User MongoDB ObjectId
   *  Use x-auth-userid if it exists, otherwise perform as expected
   *      + openresty still boots people if they are unauthenticated
   *      + More multi-purpose for admin tools, etc... that might not have an authenticated token
   *
   * @param req injects user{id} into request from req.headers['x-auth-userid']
   * @param res
   * @param next
   * @returns {*} Passes req/res to next middleware -- injects user{id} into request
   */
  const authenticateRequest = (req, res, next) => {
    if (req.headers['x-auth-userid']) {
      // eslint-disable-next-line no-param-reassign
      req.authenticatedUserId = req.headers['x-auth-userid'];
      const id = req.authenticatedUserId || null;
      newrelic.addCustomAttribute('userId', id);
    }

    next();
  };

  /**
   * Checks if the user is a registered metered user with surf checks remaining.
   * If so, we set premium:true and type:registered on the req.user object.
   *
   * @param req
   * @param next
   * @returns {*} Passes req/res to next middleware
   */
  const entitleRequest = async (req, _, next) => {
    /* eslint-disable no-param-reassign */
    req.user = {
      entitlements: [],
      premium: false,
    };
    if (req.authenticatedUserId) {
      const userEntitlements = await getEntitlements(req.authenticatedUserId);
      const premium = userEntitlements.entitlements.includes('sl_premium');
      req.user = {
        id: req.authenticatedUserId,
        entitlements: userEntitlements.entitlements,
        premium,
      };
      const metered = userEntitlements.entitlements.includes('sl_metered');
      if (metered && !premium) {
        const anonymousId = req.get('x-auth-anonymousid');
        const { scopes } = req;
        const premiumOverride = scopes && scopes.includes('premium:read:registered');
        if (anonymousId && premiumOverride) {
          req.user = {
            ...req.user,
            premium: true,
            type: 'registered',
          };
        }
      }
    }

    next();
    /* eslint-enable no-param-reassign */
  };

  /**
   * Optionally enforce premium user access. If a user is attached, it has to be premium or
   * a metered registered user with surf checks remaining.
   * This allows externally facing premium requests and internal non-authenticated requests.
   *
   * @param req rejects request if user id is included and isn't premium
   * @param res
   * @param next
   * @returns {*} Passes req/res to next middleware
   */
  const optionallyAuthorizePremiumRequest = async (req, res, next) => {
    const premium = (await isPremium(req.authenticatedUserId)) || isMeteredPremium(req.user);
    if (req.authenticatedUserId && !premium) {
      return res.status(403).send({ message: 'Missing premium entitlement.' });
    }

    next();
  };

  /**
   *
   * @param req
   *  query:
   *    startDate: defaults to 1 day ago, milliseconds elapsed since the UNIX epoch
   *    endDate: defaults to now, milliseconds elapsed since the UNIX epoch
   *    allowPartialMatch: defaults to false, allow for clips partially matching range
   *    includeExpired: defaults to false, allow for expired clips to be returned
   *  params:
   *    alias: required, no default
   * @param res
   */
  const getRecordingForAlias = async (req, res) => {
    const startDate = Number.parseInt(req.query.startDate || Date.now() - 86400000, 10);
    const endDate = Number.parseInt(req.query.endDate || Date.now(), 10);
    const allowPartialMatch = (req.query.allowPartialMatch === 'true');
    const includeExpired = (req.query.includeExpired === 'true');
    const { alias } = req.params;

    if (!alias) {
      return res.status(400).json({
        message: 'Missing required parameter: alias'
      });
    }

    const foundCamera = (await cameraModel.getCameraByAlias(alias));
    if (foundCamera.length === 0) {
      return res.status(400).json({
        message: 'No camera for passed alias.'
      });
    }
    const cameraId = foundCamera[0]._id;

    return cameraRecordingModel.getCameraRecording(startDate, endDate, cameraId,
      allowPartialMatch, includeExpired)
      .then((cameraRecordings) => {
        if (cameraRecordings instanceof Array && cameraRecordings.length === 0) {
          res.status(200).json([]);
        } else {
          res.status(200).json(cameraRecordings);
        }
      })
      .catch(handleErrorForResponse(400, res, 'getRecordingForAlias'));
  };

  const getRecordingForId = async (req, res) => {
    const startDate = Number.parseInt(req.query.startDate || Date.now() - 86400000, 10);
    const endDate = Number.parseInt(req.query.endDate || Date.now(), 10);
    const allowPartialMatch = (req.query.allowPartialMatch === 'true');
    const includeExpired = (req.query.includeExpired === 'true');
    const { cameraId } = req.params;

    if (!cameraId) {
      throw new APIError('Missing required parameter: cameraId');
    }

    const foundCamera = await cameraModel.getCameraById(cameraId);
    if (foundCamera.length === 0) {
      return res.status(400).json({
        message: 'No camera for passed cameraId.'
      });
    }

    try {
      const cameraRecordings = await cameraRecordingModel.getCameraRecording(startDate, endDate,
        cameraId, allowPartialMatch, includeExpired);
      if (cameraRecordings instanceof Array && cameraRecordings.length === 0) {
        return res.status(200).json([]);
      }

      return res.status(200).json(cameraRecordings);
    } catch (err) {
      return handleErrorForResponse(400, res, 'getRecordingForId')(err);
    }
  };

  const getHighlightsForId = async (
    {
      params: { cameraId },
      query: { startDate, endDate }
    },
    res
  ) => {
    if (!cameraId) {
      throw new APIError('Missing required parameter: cameraId');
    }

    const start = Number.parseInt(startDate || Date.now() - 60 * 60 * 1000, 10);
    const end = Number.parseInt(endDate || Date.now(), 10);

    try {
      const cameraRecordings = await cameraRecordingModel.getCameraRecording(start, end,
        cameraId, true, false, true);
      if (cameraRecordings instanceof Array && cameraRecordings.length === 0) {
        // Don't bother re-checking for 5 mins,
        // this is either not a highlights cam or it's night time
        return res.status(200)
          .header('cache-control', `public, max-age=${60 * 5}`)
          .json([]);
      }

      // 10 min segments, 2 mins processing time (optimistic estimate)
      const nextClipDue = new Date(cameraRecordings[0].endDate.getTime() + 12 * 60 * 1000);
      const maxAge = Math.max(0, (nextClipDue.getTime() - Date.now()) / 1000);

      return res.status(200)
        .header('expires', nextClipDue.toUTCString())
        .header('cache-control', `public, max-age=${maxAge}`)
        .json(cameraRecordings);
    } catch (err) {
      throw new APIError(err.message);
    }
  };

  /**
   *
   * @param req
   *  body: all required
   *    startDate: milliseconds elapsed since the UNIX epoch
   *    endDate: milliseconds elapsed since the UNIX epoch
   *    resolution: 720p, 1080p, Other
   *      Add a new resolution to the Mongoose Enum for a new resolution
   *    thumbLargeUrl: url for thumb, large
   *    thumbSmallUrl: url for thumb, small
   *    recordingUrl: url for recording
   *  params: all required
   *    cameraId: cam cameraId
   * @param res
   */
  const createRecordingForId = async ({ body, params: { cameraId } }, res) => {
    const foundCamera = (await cameraModel.getCameraById(cameraId));
    if (foundCamera.length === 0) {
      return res.status(400).json({
        message: 'No camera exists for passed cameraId.'
      });
    }

    try {
      const createdRecording = await cameraRecordingModel.createCameraRecording(
        { ...body, cameraId }
      );
      return res.status(200).json(createdRecording);
    } catch (error) {
      if (error instanceof mongoose.Error.ValidationError) {
        throw new APIError(error.message);
      }
      const existing = await CameraRecordingModel
        .findOne({ recordingUrl: body.recordingUrl })
        .lean();
      if (existing) return res.status(200).json(existing);
      throw error;
    }
  };

  const patchRecording = async ({ body, params: { recordingId } }, res) => {
    const recording = await CameraRecordingModel.findOne({ _id: recordingId });
    if (!recording) {
      throw new APIError(`No recording for ${recordingId}.`);
    }

    if (body.expireSoft) recording.expireSoft = body.expireSoft;

    if (body.highlights) {
      const highlightsFields = ['url', 'thumbUrl', 'gifUrl', 'duration'];
      highlightsFields.forEach(field => {
        if (body.highlights[field]) recording.highlights[field] = body.highlights[field];
      });
    }

    try {
      const savedRecording = await recording.save();
      res.json(savedRecording.toObject());
    } catch (error) {
      if (error instanceof mongoose.Error.ValidationError) {
        throw new APIError(error.message);
      }
      throw error;
    }
  };


  /**
   * Given a legacy recording name, redirect to recordingUrl for closest match
   *
   * @param req
   *  params:
   *    alias: required, no default, waikikibeachcam
   *    file: required, no default, waikikibeachcam.1930.2018-04-25.mp4
   * @param res
   */
  const getRecordingRedirectForAlias = async (
    { params: { alias, file } }, res) => {
    if (!alias || !file) {
      return res.status(400).json({ message: 'Missing required parameter: alias and file' });
    }

    // Check to see if alias requires translation
    let camAliasMap;
    try {
      camAliasMap = await CameraAlias.findOne({ oldAlias: alias });
    } catch (err) {
      log.error({ err, message: 'Error retrieving camera alias' });
      return res.status(500).send({ message: 'Error retrieving camera alias' });
    }

    const newAlias = camAliasMap?.newAlias || alias;

    return cameraRecordingModel.getCameraRecordingBestMatch(newAlias, file)
      .then((cameraRecordings) => {
        if (cameraRecordings instanceof Array && cameraRecordings.length > 0) {
          res.redirect(301, cameraRecordings[0].recordingUrl);
        } else {
          res.status(404).json({ message: 'Recording file not found.' });
        }
      })
      .catch(handleErrorForResponse(400, res, 'getRecordingForAlias'));
  };

  const getRecordingById = async ({ params: { recordingId } }, res) => {
    const recording = await CameraRecordingModel.findOne({ _id: recordingId }).lean();
    if (!recording) {
      throw new APIError(`No recording for ${recordingId}.`);
    }
    return res.status(200).json(recording);
  };

  api.get(
    '/recordingId/:recordingId',
    getRecordingById
  );
  api.patch(
    '/:recordingId',
    wrapErrors(patchRecording),
  );
  api.get(
    '/id/:cameraId',
    authenticateRequest,
    entitleRequest,
    optionallyAuthorizePremiumRequest,
    getRecordingForId
  );
  api.get(
    '/redirect/:alias/:file',
    getRecordingRedirectForAlias,
  );
  api.get(
    '/:alias',
    authenticateRequest,
    entitleRequest,
    optionallyAuthorizePremiumRequest,
    getRecordingForAlias
  );
  api.get(
    '/highlights/:cameraId',
    authenticateRequest,
    entitleRequest,
    wrapErrors(getHighlightsForId),
  );
  api.post(
    '/id/:cameraId',
    wrapErrors(createRecordingForId),
  );

  return api;
}
