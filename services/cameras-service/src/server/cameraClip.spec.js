import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import express from 'express';
import { Types } from 'mongoose';

import cameraClip from './cameraClip';
import CameraClip, * as cameraClipModel from '../model/CameraClipModel';
import * as sendSqsMessage from './sendSqsMessage';

import singleClipModel from './fixtures/clip/singleClipModel.json';
import singleClipResponse from './fixtures/clip/singleClipResponse.json';
import multipleClipModel from './fixtures/clip/multipleClipModel.json';
import multipleClipResponse from './fixtures/clip/multipleClipResponse.json';
import mixedResponseClipModel from './fixtures/clip/mixedResponseClipModel.json';
import mixedResponseClipResponse from './fixtures/clip/mixedResponseClipResponse.json';
import allBadClipIdsResponse from './fixtures/clip/allBadClipIdsResponse.json';
import createClipModel from './fixtures/clip/createClipModel.json';
import createClipResponse from './fixtures/clip/createClipResponse.json';
import createClipSqsReceipt from './fixtures/clip/createClipSqsReceipt.json';
import updateClipModel from './fixtures/clip/updateClipModel.json';
import updateClipResponse from './fixtures/clip/updateClipResponse.json';

chai.use(dirtyChai);
chai.use(chaiHttp);

describe('/clips', () => {
  let request;

  before(() => {
    const app = express();
    app.use(cameraClip());
    request = chai.request(app);
  });

  beforeEach(() => {
    sinon.stub(CameraClip, 'find');
    sinon.stub(CameraClip, 'findOne');
    sinon.stub(cameraClipModel, 'createCameraClip');
    sinon.stub(sendSqsMessage, 'default');
  });

  afterEach(() => {
    CameraClip.find.restore();
    CameraClip.findOne.restore();
    cameraClipModel.createCameraClip.restore();
    sendSqsMessage.default.restore();
  });

  describe('GET', () => {
    it('should return single clip by clipId', async () => {
      const clipId = singleClipModel[0].clipId;

      CameraClip.find.resolves({
        ...singleClipModel,
        toJSON: sinon.stub().returns(singleClipModel),
      });
      const res = await request.get(`/?ids=${clipId}`).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(singleClipResponse);
      expect(CameraClip.find).to.have.been.calledOnce();
      expect(CameraClip.find).to.have.been.calledWithExactly({
        _id: { $in: [new Types.ObjectId(clipId)] },
      });
    });

    it('should return multiple clips by clipId', async () => {
      const clipIds = multipleClipModel.map(clip => clip.clipId);

      CameraClip.find.resolves({
        ...multipleClipModel,
        toJSON: sinon.stub().returns(multipleClipModel),
      });
      const res = await request.get(`/?ids=${clipIds.join(',')}`).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(multipleClipResponse);
      expect(CameraClip.find).to.have.been.calledOnce();
      expect(CameraClip.find).to.have.been.calledWithExactly({
        _id: { $in: clipIds.map(id => new Types.ObjectId(id)) },
      });
    });

    it('should return mixed-response when not all requested clips exist', async () => {
      const clipIds = mixedResponseClipModel.map(clip => clip.clipId);

      CameraClip.find.resolves({
        ...mixedResponseClipModel,
        toJSON: sinon.stub().returns(mixedResponseClipModel),
      });
      const res = await request.get(`/?ids=${clipIds.join(',')},this-id-doesnt-exist`).send();

      expect(res).to.have.status(207);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(mixedResponseClipResponse);
      expect(CameraClip.find).to.have.been.calledOnce();
      expect(CameraClip.find).to.have.been.calledWithExactly({
        _id: { $in: clipIds.map(id => new Types.ObjectId(id)) },
      });
    });

    it('should return 404 when no requested clips exist', async () => {
      const clipIds = allBadClipIdsResponse.map(clip => clip.clipId);
      try {
        CameraClip.find.resolves(null);
        const res = await request.get(`/?ids=${clipIds.join(',')}`).send();

        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal(allBadClipIdsResponse);
        expect(CameraClip.find).to.have.been.calledOnce();
      }
    });
  });

  describe('POST', () => {
    it('should add a clip', async () => {
      cameraClipModel.createCameraClip.resolves(createClipResponse);
      sendSqsMessage.default.resolves(createClipSqsReceipt);

      const res = await request.post('/').send(createClipModel);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(createClipResponse);
      expect(cameraClipModel.createCameraClip).to.have.been.calledOnce();
      expect(sendSqsMessage.default).to.have.been.calledOnce();
    });
  });

  describe('PUT', () => {
    it('should update a clip', async () => {
      const existingModel = {
        ...updateClipResponse,
        save: sinon.stub().resolves(),
      };
      const clipId = existingModel.clipId;

      CameraClip.findOne.resolves({
        ...existingModel,
        toJSON: sinon.stub().returns(existingModel),
      });

      const res = await request.put(`/${clipId}`).send(updateClipModel);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(updateClipResponse);
      expect(CameraClip.findOne).to.have.been.calledOnce();
      expect(existingModel.save).to.have.been.calledOnce();
    });
  });
});
