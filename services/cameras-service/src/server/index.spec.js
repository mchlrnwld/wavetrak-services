import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { Types } from 'mongoose';
import chaihttp from 'chai-http';
import prerecorded from './prerecorded';
import CameraModel from '../model/CameraModel';
import * as spotsAPI from '../external/spots';

describe('/prerecorded/pendingUpdate', () => {
  const app = express();
  let now;

  before(() => {
    app.use(prerecorded({ trace: sinon.spy() }));
    chai.use(chaihttp);
  });

  beforeEach(() => {
    sinon.stub(CameraModel, 'find');
    sinon.stub(spotsAPI, 'getSpotsByCams');
    now = sinon.useFakeTimers({ now: 1561995401845 });
  });

  afterEach(() => {
    CameraModel.find.restore();
    spotsAPI.getSpotsByCams.restore();
    now.restore();
  });

  it('should GET cams pending update', async () => {
    const model = {
      _id: new Types.ObjectId(),
      alias: 'somealias',
      lastPrerecordedClipEndTimeUTC: '1970-01-19T01:47:49.918Z',
      lastPrerecordedClipStartTimeUTC: '1970-01-19T01:47:49.831Z',
      prerecordedClipRecordingTimes: [
        {
          _id: '5d153b164156e82a8123604d',
          endMinutes: 30,
          endHours: 8,
          startMinutes: 20,
          startHours: 8,
        },
        {
          _id: '5d153b164156e82a8123604c',
          endMinutes: 30,
          endHours: 9,
          startMinutes: 20,
          startHours: 9,
        }
      ],
      isPrerecorded: true,
    };
    const _id = `${model._id}`;
    spotsAPI.getSpotsByCams.resolves([{
      _id: '5a9d8a84b0f634001ada2d1e',
      hasThumbnail: false,
      cams: [
        _id,
      ],
      timezone: 'America/New_York',
      name: ' Lake Worth Pier Southside',
      toJSON: sinon.stub().returns(model),
    }]);
    CameraModel.find.resolves([{
      ...model,
      toJSON: sinon.stub().returns(model),
    }]);
    const res = await chai.request(app).get('/pendingUpdate')
      .set('Content-Type', 'application/json')
      .send();
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      pendingUpdate: [
        {
          id: _id,
          alias: 'somealias',
          recordingTimeRange: {
            startDateUTC: 1561987200000,
            endDateUTC: 1561987800000,
          },
        },
      ]
    });
  });
});
