/* eslint-disable dot-notation */
import { Router } from 'express';
import fetch from 'node-fetch';
import publishTopic from './publishTopic';
import * as logger from '../common/logger';
import * as cameraModel from '../model/Camera';
import DownMessages from '../stores/CameraDownMessages.js';
import {
  getCameraStreamCookies
} from '../model/CameraStreamAuthorization';
import { getSpotsByCams } from '../external/spots';
import mongoose from 'mongoose';
import { APIError, wrapErrors } from '@surfline/services-common/dist';

export function camera() {
  const api = Router();
  const log = logger.logger('cameras-service');

  const trackRequest = (req, _, next) => {
    log.trace({
      action: '/cameras',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body
      }
    });
    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (err) => {
    if (typeof err === 'object') {
      log.error({
        message: `camerasService:${action}`,
        stack: err,
        trace: err.stack
      });
    } else {
      log.error({
        message: `camerasService:${action}`,
        errorMessage: err,
        trace: err.stack
      });
    }
    res.status(errorCode).send(err);
  };

  const transformCamDoc = {
    init(cam) {
      this.camera = cam;
      this.transform();
      return this.camera;
    },
    transform() {
      this.mediaUrls();
      this.camStatus();
      this.prerecorded();
    },
    mediaUrls() {
      const url = this.camera.playlistUrl || this.camera.streamUrl;
      const secureUrl =
        `${process.env.SECURE_CAM_HOST}/cameras/authorization/${this.camera.alias}.m3u8`;
      Object.assign(this.camera, {
        streamUrl: url && url.indexOf('secure') > -1 ? secureUrl : url,
        stillUrl: `https://camstills.cdn-surfline.com/${this.camera.alias}/latest_small.jpg`,
        pixelatedStillUrl: `https://camstills.cdn-surfline.com/${this.camera.alias}/latest_small_pixelated.png`,
        rewindBaseUrl: `https://camrewinds.cdn-surfline.com/${this.camera.alias}/${this.camera.alias}`
      });
    },
    camStatus() {
      this.camera.isDown = DownMessages.filter(message => message.id === this.camera.isDown)[0];
    },
    prerecorded() {
      const date = new Date;
      date.setDate(date.getDate() - 1);
      this.camera.prerecordedClipRecordingTimes = this.camera.prerecordedClipRecordingTimes ?
        this.camera.prerecordedClipRecordingTimes : [];
      this.camera.lastPrerecordedClipStartTimeUTC = this.camera.lastPrerecordedClipStartTimeUTC ?
        this.camera.lastPrerecordedClipStartTimeUTC : date;
      this.camera.lastPrerecordedClipEndTimeUTC = this.camera.lastPrerecordedClipEndTimeUTC ?
        this.camera.lastPrerecordedClipEndTimeUTC : date;
      this.camera.isPrerecorded = !!this.camera.isPrerecorded;
    }
  };

  const getCamerasHandler = (req, res) => {
    const textSearchField = {};
    const metaField = {};
    const scoreField = {};
    let sort = {};

    if (req.query.search) {
      const searchField = {};
      searchField['$search'] = req.query.search.trim();
      textSearchField['$text'] = searchField;
      metaField['$meta'] = 'textScore';
      scoreField['score'] = metaField;
      sort = scoreField;
    }

    if (req.query.orderBy && req.query.orderBy === 'title') {
      sort = 'title';
    }

    return cameraModel.getCameraByTextSearch(textSearchField, scoreField, sort)
      .then((foundCamera) => {
        if (foundCamera instanceof Array && foundCamera.length === 0) {
          res.status(200).json([]);
        } else {
          res.status(200).json(foundCamera.map(cam => transformCamDoc.init(cam)));
        }
      })
      .catch(handleErrorForResponse(400, res, 'getCamerasHandler'));
  };

  const getCamerasDownHandler = (req, res) => {
    const searchParams = { isDown: { $ne: 0 } };

    return cameraModel.getCamera(searchParams)
      .then((foundCameras) => {
        if (foundCameras instanceof Array && foundCameras.length === 0) {
          res.status(200).json([]);
        }
        res.status(200).json(foundCameras.map(cam => transformCamDoc.init(cam)));
      })
      .catch(handleErrorForResponse(400, res, 'getCamerasDownHandler'));
  };

  const getCameraHandler = (req, res) => {
    let findCamera;
    let cameraIdentifier;
    if (req.params.id) {
      findCamera = cameraModel.getCameraById;
      cameraIdentifier = req.params.id;
    } else {
      log.warn('Camera requested without an identifier.');
      return res.status(400).send(
        { message: 'You need to specify a camera ID.' }
      );
    }

    return findCamera(cameraIdentifier)
      .then((foundCamera) => {
        if (foundCamera instanceof Array && foundCamera.length === 0) {
          log.debug(`Camera ${cameraIdentifier} not found.`);
          res.status(400).json({ message: 'Camera not found' });
        } else {
          res.status(200).json(transformCamDoc.init(foundCamera[0]));
        }
      })
      .catch(handleErrorForResponse(400, res, 'getCameraHandler'));
  };

  const deleteCameraHandler = (req, res) => {
    let findCamera;
    let cameraIdentifier;
    if (req.params.id) {
      findCamera = cameraModel.deleteCameraById;
      cameraIdentifier = req.params.id;
    } else {
      log.warn('Camera requested without an identifier.');
      return res.status(400).send(
        { message: 'You need to specify a camera ID.' }
      );
    }

    return findCamera(cameraIdentifier)
      .then(async (foundCamera) => {
        if (foundCamera instanceof Array && foundCamera.length === 0) {
          log.debug(`Camera ${cameraIdentifier} not found.`);
          res.status(400).json({ message: 'Camera not found' });
        } else {
          // eslint-disable-next-line no-unused-expressions
          await publishTopic({ _id: cameraIdentifier });
          res.status(200).json({ message: `Camera deleted: ${cameraIdentifier}` });
        }
      })
      .catch(handleErrorForResponse(400, res, 'deleteCameraHandler'));
  };

  const validateAliasHandler = async (req, res) => {
    let spotLocation;
    let spotTimezone;
    let foundCamera;

    if (!req.query.alias) {
      log.warn('Alias validation requested without an alias.');
      return res.status(400).send(
        { message: 'You need to specify an alias as a query param.' }
      );
    }
    const cameraIdentifier = req.query.alias.toLowerCase();

    try {
      foundCamera = await cameraModel.getCameraByAlias(cameraIdentifier);
    } catch (error) {
      if (error instanceof mongoose.Error.ValidationError) {
        throw APIError(error.message);
      }
      throw error;
    }

    if (foundCamera instanceof Array && foundCamera.length === 0) {
      log.debug(`Camera ${cameraIdentifier} not found.`);
      return res.status(404).json({ message: 'Camera not found' });
    }

    const { supportsHighlights, supportsCrowds } = foundCamera[0];
    if (supportsHighlights || supportsCrowds) {
      // fetch spot's location from spots-api if we have the camId
      const spots = await getSpotsByCams(foundCamera[0]._id.toString());
      if (spots instanceof Array) {
        spotLocation = spots[0].location;
        spotTimezone = spots[0].timezone;
      }
    }

    return res.status(200).json({
      alias: foundCamera[0].alias,
      _id: foundCamera[0]._id,
      location: spotLocation,
      timezone: spotTimezone,
      supportsCrowds,
      supportsHighlights,
    });
  };

  const changeAliasHandler = (req, res) => {
    const body = req.body;
    if (!body.alias) {
      log.warn('Change alias request without a body.');
      return res.status(400).send(
        { message: 'You need to specify an alias in your body.' }
      );
    }
    const changeCameraAlias = cameraModel.changeCameraAlias;
    const alias = body.alias.toLowerCase();
    if (!req.params.id || !alias) {
      log.warn('Change alias request without a _id.');
      return res.status(400).send(
        { message: 'You need to specify an _id and alias in your body.' }
      );
    }
    cameraModel.getCameraById(req.params.id).then(
      (foundCameraById) => {
        if (foundCameraById.length < 1) {
          log.debug(`Change camera alias request with invalid ID: ${req.params.id}.`);
          return res.status(400).send({ message: `No camera with ID: ${req.params.id}` });
        }
        cameraModel.getCameraByAlias(alias).then(
          (foundCameraByAlias) => {
            if (foundCameraByAlias && foundCameraByAlias.length > 0) {
              log.debug(`Change camera alias request with an existing alias, ${alias}.`);
              return res.status(400).send({ message: `Alias already exists: ${alias}` });
            }
            return changeCameraAlias(req.params.id, alias)
              .then((updatedCamera) => {
                res.status(200).json(transformCamDoc.init(updatedCamera));
              })
              .catch(handleErrorForResponse(400, res, 'changeAliasHandler'));
          });
      });
  };

  const createCameraHandler = async (req, res) => {
    const body = req.body;
    const errors = {};

    if (!body || !body.alias) {
      log.warn('Create camera request without an alias.');
      errors.alias = {};
      errors.alias.message = 'You need to specify an alias.';
    }

    if (!body || !body.title) {
      log.warn('Create camera request without a camera title.');
      errors.title = {};
      errors.title.message = 'You need to specify a title.';
    }

    if ('title' in errors || 'alias' in errors) {
      return res.status(400).send({
        message: 'Title and alias must be specified in the body.',
        errors
      });
    }

    try {
      body.alias = body.alias.toLowerCase();
      const foundCamera = await cameraModel.getCameraByAlias(body.alias);
      if (foundCamera && foundCamera.length > 0) {
        log.debug(`Create camera request with an existing alias, ${body.alias}.`);
        errors.alias = {};
        errors.alias.message = `Camera already exists with alias: ${body.alias}`;
        return res.status(400).send({
          message: 'Camera alias already exists',
          errors
        });
      }

      const createdCamera = await cameraModel.createCamera(body);
      await publishTopic(createdCamera); // eslint-disable-line no-unused-expressions
      return res.status(200).json(transformCamDoc.init(createdCamera));
    } catch (err) {
      handleErrorForResponse(400, res, 'createCameraHandler');
    }
  };

  const updateCameraHandler = (req, res) => {
    const body = req.body;
    const alias = req.body.alias || null;
    const errors = {};

    if ('alias' in body && alias === '') {
      log.warn('Create camera request without an alias.');
      errors.alias = {};
      errors.alias.message = 'You need to specify an alias.';
    }

    if ('title' in body && body.title === '') {
      log.warn('Create camera request with blank camera title.');
      errors.title = {};
      errors.title.message = 'You need to specify a title.';
    }

    if ('title' in errors || 'alias' in errors) {
      return res.status(400).send({
        message: 'Title and alias must be specified in the body.',
        errors
      });
    }


    if (!req.params.id) {
      log.warn('Update camera request without an ID.');
      return res.status(400).send({ message: 'You need to specify a camera id.' });
    }

    if (Object.keys(body).length < 1) {
      log.warn('Update camera request without a body.');
      return res.status(400).send({ message: 'You need to specify a body in the POST.' });
    }

    const updateCamera = () => (cameraModel.updateCamera(req.params.id, body)
      .then(async updatedCamera => {
        await publishTopic(updatedCamera); // eslint-disable-line no-unused-expressions
        return res.status(200).json(transformCamDoc.init(updatedCamera));
      }).catch(handleErrorForResponse(400, res, 'updateCameraHandler'))
    );

    if (alias) {
      cameraModel.getCameraByAlias(alias.toLowerCase()).then(
        (foundCamera) => {
          if (foundCamera && foundCamera.length > 0 &&
            foundCamera[0]._id.toString() !== req.params.id) {
            log.debug(`Create camera request with an existing alias, ${alias}.`);
            errors.alias = {};
            errors.alias.message = `Camera already exists with alias: ${alias}`;
            return res.status(400).send({
              message: 'Camera alias already exists',
              errors
            });
          }
          updateCamera();
        });
    } else {
      updateCamera();
    }
  };

  const getPlaylist = async (streamUrl, cookieHash) => {
    let cookie = '';
    Object.entries(cookieHash).forEach(([cookieName, cookieValue]) => {
      cookie += `${cookieName}=${cookieValue}; `;
    });
    const result = await fetch(streamUrl, { headers: { cookie } });
    return result.text();
  };

  /**
   * 1. The cam playlist is `https://cam-cdn.surfline.com/cameras/authorization/{cam-alias}.m3u8`
   * 2. Optionally validate the user’s identity and whether they should access the url specified
   * 3. Generate cloudfront signing cookies and get playlist,
   *    re-writing playlist contents to refer to correct chunklist
   * 4. The user's cam player reads playlist contents, stores the attached cookies,
   *    and requests chunklist for TS segments
   *
   * Notes:
   * The response needs to be a buffer, otherwise express won't correctly set the content-type
   * Safari private mode will not send Origin, but will send referer
   * CloudFront strips referer
   *
   * @param req
   * @param res
   * @returns {Promise.<void>}
   */
  const getStreamAuthorizationHandler = async (req, res) => {
    const { params: { cameraAlias }, authenticatedUserId, ip } = req;
    try {
      if (cameraAlias) {
        const [{ streamUrl, playlistUrl }] = await cameraModel.getCameraByAlias(
          cameraAlias.replace(/(\.m3u8)/g, ''));
        const assetUrl = playlistUrl || streamUrl;
        const cookieHash = await getCameraStreamCookies(authenticatedUserId, ip, assetUrl);
        const path = assetUrl.replace(/(.*\.com)|(playlist\.m3u8)/g, '');
        Object.entries(cookieHash).forEach(([cookieName, cookieValue]) => {
          res.cookie(cookieName, cookieValue, { path });
        });

        const playlist = await getPlaylist(assetUrl, cookieHash);
        const modifiedPlaylist = playlist.replace(/(chunklist.*m3u8)/g, `${path}chunklist.m3u8`);
        res.setHeader('Content-Type', 'application/vnd.apple.mpegurl');
        res.status(200).send(new Buffer(modifiedPlaylist));
      } else {
        res.status(400).json({ message: 'Invalid Arguments' });
      }
    } catch (e) {
      if (e && e.message) {
        res.status(400).json({ message: e.message });
      } else {
        log.error(e);
        res.status(500).json({ message: 'Internal Server Error' });
      }
    }
    return log.trace('cam access requested');
  };

  const createCameraLog = (req, res) => {
    const body = req.body;
    body.ip = body.ip || req.ip;
    body.cameraId = req.params.id;
    log.debug(body);
    return res.status(200).json(body);
  };

  const checkAccess = (_, res, next) => {
    const scopes = res.locals.scopes;
    // Had to add condition for no scopes because tests and other services don't have onelogin roles
    if (!scopes ||
      scopes.includes('cameras_admin') ||
      scopes.includes('cameras:write:admin') ||
      scopes.includes('super_admin')) {
      return next();
    }
    log.info('Correct scopes not present');
    return res.status(403).send({
      errors: {
        scopes: {
          message: 'Authentication failed, missing correct scopes'
        }
      }
    });
  };

  const getCameraIDsHandler = async (_, res) => {
    const allCameras = await cameraModel.getAllCameraIDs();
    return res.status(200).json(allCameras);
  };

  // Middleware to check role authorization on POST, PUT, or DELETE
  api.post('*', checkAccess);
  api.put('*', checkAccess);
  api.delete('*', checkAccess);

  api.post('/:id/log', trackRequest, createCameraLog);

  api.get('/ValidateAlias', trackRequest, wrapErrors(validateAliasHandler));

  api.get('/down', trackRequest, getCamerasDownHandler);
  api.get('/embeds', trackRequest, getCameraIDsHandler);

  api.get('/:id', trackRequest, getCameraHandler);

  api.delete('/:id', trackRequest, deleteCameraHandler);

  api.post('/', trackRequest, createCameraHandler);

  api.get('/', trackRequest, getCamerasHandler);

  api.put('/:id/ChangeAlias', trackRequest, changeAliasHandler);

  api.put('/:id/UpdateCamera', trackRequest, updateCameraHandler);

  api.get('/authorization/:cameraAlias', trackRequest,
    getStreamAuthorizationHandler);

  return api;
}
