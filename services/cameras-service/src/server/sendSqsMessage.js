import AWS from 'aws-sdk';
import * as logger from '../common/logger';

const log = logger.logger('send-sqs-message');

const sendSqsMessage = async (queueUrl, message) => {
  const sqs = new AWS.SQS({
    apiVersion: '2012-11-05',
    region: 'us-west-1',
  });

  const receipt = await sqs.sendMessage({
    QueueUrl: queueUrl,
    MessageBody: message,
  }).promise();

  log.debug({ receipt });

  return receipt;
};

export default sendSqsMessage;
