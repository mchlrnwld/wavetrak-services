import { expect } from 'chai';
import { average, standardDeviation } from './mathsHelpers';

describe('utils/mathsHelpers', () => {
  it('calculates an average from a list of numbers', () => {
    expect(average([3, 5, 7])).to.equal(5);
    expect(average([8])).to.equal(8);
    expect(average([-3, -5, -7])).to.equal(-5);
  });

  it('calculates the standard deviation from a list of numbers', () => {
    expect(standardDeviation([3, 5, 7])).to.equal(1.632993161855452);
    expect(standardDeviation([8])).to.equal(0);
    expect(standardDeviation([-3, -5, -7])).to.equal(1.632993161855452);
  });
});
