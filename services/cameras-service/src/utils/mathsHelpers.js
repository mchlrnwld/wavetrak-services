export const average = values => values.reduce((p, c) => p + c, 0) / values.length;

export const standardDeviation = values => {
  const mean = average(values);
  return Math.sqrt(average(values.map(x => Math.pow(x - mean, 2))));
};
