/**
 * @description Utility function to check if a registered user is eligible for Premium data.
 * This function returns true if the user type is registered and
 * the entitlements array has sl_metered in it.
 * @param {} user object
 */

export const isMeteredPremium = (user) => {
  if (!user) return false;
  const { entitlements, type } = user;
  if (!entitlements || !type) return false;
  return type === 'registered' && entitlements.includes('sl_metered');
};

export default isMeteredPremium;
