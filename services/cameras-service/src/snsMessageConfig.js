export const AWS_CAMERA_DETAILS_UPDATED_TOPIC = 'AWS_CAMERA_DETAILS_UPDATED_TOPIC';
// eslint-disable-next-line
export const AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC = 'AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC';

const base = {
  region: 'us-west-1',
  api: '2010-03-31',
};

const envConfig = {
  development: {
    [AWS_CAMERA_DETAILS_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:camera_details_updated_sandbox',
    },
    [AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:embed_config_created_sandbox',
    },
  },
  sandbox: {
    [AWS_CAMERA_DETAILS_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:camera_details_updated_sandbox',
    },
    [AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:embed_config_created_sandbox',
    },
  },
  staging: {
    [AWS_CAMERA_DETAILS_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:camera_details_updated_staging',
    },
    [AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:embed_config_created_staging',
    },
  },
  production: {
    [AWS_CAMERA_DETAILS_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:833713747344:camera_details_updated_prod',
    },
    [AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:833713747344:embed_config_created_prod',
    },
  },
};

export default {
  ...base,
  ...envConfig[process.env.NODE_ENV || 'development'],
};
