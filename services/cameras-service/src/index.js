import 'newrelic';
import { initMongoDB } from './model/dbContext';
import { startApp, log } from './server';

initMongoDB()
  .then(startApp)
  .catch(err => {
    log.error({
      message: 'App Initialization failed. Can\'t connect to database',
      stack: err,
    });
    process.exit(1);
  });
