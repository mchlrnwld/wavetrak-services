import fetch from 'node-fetch';

export const getSpotsByCams = async (cams) => {
  const urlBase = `${process.env.SPOTS_API}/admin/spots/`;
  const url = `${urlBase}?select=location,timezone,name,cams&camsOnly=true&cams=${cams}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
