import fetch from 'node-fetch';
import createParamString from './createParamString';

const getEntitlements = async (userId) => {
  const params = { objectId: userId };
  const url = `${process.env.ENTITLEMENTS_SERVICE}/entitlements?${createParamString(params)}`;

  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  if (response.status === 400) return { entitlements: [] };
  throw body;
};


export const isPremium = async (userId) => {
  if (!userId) return false;
  try {
    const entitlementsResponse = await getEntitlements(userId);
    return entitlementsResponse.entitlements.includes('sl_premium');
  } catch (err) {
    return false;
  }
};

export default getEntitlements;
