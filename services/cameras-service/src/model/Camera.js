import * as logger from '../common/logger';
import CameraModel from './CameraModel';

const log = logger.logger('cameras-model');


export const getAllCameraIDs = async () =>
  await CameraModel.find().select('_id title').lean();

export const getCameraAlias = id =>
  new Promise((resolve, reject) => {
    CameraModel.findOne({
      $and: [{ _id: id },
        { alias: 1 },
        { status: { $ne: 'deleted' } }]
    },
    (err, res) => {
      if (err) {
        log.debug({
          message: 'getCameraAlias(${id})',
          stack: err
        });
        reject(err);
      } else {
        resolve(res);
      }
    });
  }).lean();


/**
 * @param searchParameters
 * @returns {Promise}
 */
export const getCamera = searchParameters =>
  CameraModel.find({ $and: [searchParameters, { status: { $ne: 'deleted' } }] },
    (err, cameras) => {
      if (err) {
        log.error({
          action: 'getCamera():find',
          stack: err
        });
      }
      return cameras || [];
    }).lean();

/**
 * @param id
 * @returns {Promise}
 */
export const deleteCamera = id =>
  new Promise((resolve, reject) => {
    CameraModel.findOneAndUpdate({ _id: id },
      { $set: { status: 'deleted', modifiedDate: new Date() } }, (err, camera) => {
        if (err) {
          log.error({
            message: `deleteCamera(${id}):findOneAndUpdate`,
            stack: err
          });
          reject(err);
        } else {
          resolve(camera.toObject());
        }
      }
    );
  });


/**
 * @param searchParameters
 * @returns {Promise}
 */
export const getCameraWithOptions = (searchParameters, options, sort) =>
  CameraModel.find({ $and: [searchParameters, { status: { $ne: 'deleted' } }] }, options,
    (err, cameras) => {
      if (err) {
        log.error({
          action: 'getCamera():find',
          stack: err
        });
      }
      return cameras || [];
    }).sort(sort).lean();

/**
 * @param searchParameters K-V for retrieving a camera from Mongo
 * @returns {Promise}
 */
const getCameraByAttribute = (searchParameters) => {
  if (!searchParameters) {
    return new Promise((resolve, reject) => reject('No camera found'));
  }
  return getCamera(searchParameters);
};

/**
 * @param searchParameters K-V for retrieving a camera from Mongo
 * @returns {Promise}
 */
export const getCameraByTextSearch = (searchCriteria, options, sort) => {
  if (!searchCriteria) {
    return new Promise((resolve, reject) => reject('No camera found'));
  }
  return getCameraWithOptions(searchCriteria, options, sort);
};

/**
 * @param id
 * @returns {Promise}
 */
export const getCameraById = (id) => {
  if (!id) {
    return new Promise((resolve, reject) => reject('No id requested'));
  }
  const searchParameters = { _id: id };
  return getCameraByAttribute(searchParameters);
};

/**
 * @returns {Promise}
 */
export const getCameraOfTheMoment = () =>
  CameraModel.find({
    $and: [{ cotm: true }, { status: { $ne: 'deleted' } }]
  },
  (err, cameras) => {
    if (err) {
      log.error({
        action: 'getCameraOfTheMoment():find',
        stack: err
      });
    }
    return cameras;
  })
    .limit(1)
    .lean();

/**
 * @param id
 * @returns {Promise}
 */
export const deleteCameraById = (id) => {
  if (!id) {
    return new Promise((resolve, reject) => reject('No id requested'));
  }
  return deleteCamera(id);
};

/**
 * @param alias
 * @returns {Promise}
 */
export const getCameraByAlias = (alias) => {
  if (!alias) {
    return new Promise((resolve, reject) => reject('No alias requested'));
  }
  const searchParameters = { alias: alias.toLowerCase() };
  return getCameraByAttribute(searchParameters);
};

/**
 * @param id
 * @param alias
 * @returns {Promise}
 */
export const changeCameraAlias = (id, alias) =>
  new Promise((resolve, reject) => {
    CameraModel.findOneAndUpdate({ $and: [{ _id: id }, { status: { $ne: 'deleted' } }] },
      { $set: { alias, modifiedDate: new Date() } }, {
        new: true,
        runValidators: true
      }, (err, camera) => {
        if (err) {
          log.error({
            message: `changeCameraAlias(${id}):findOneAndUpdate`,
            stack: err
          });
          reject(err);
        } else {
          resolve(camera.toObject());
        }
      }
    );
  });


/**
 * @param id
 * @returns {Promise}
 */
export const changeCameraOfTheMoment = id =>
  new Promise((resolve, reject) => {
    CameraModel.findOneAndUpdate({
      $and: [
        { _id: id },
        { status: { $ne: 'deleted' } }
      ]
    },
      {
        $set: {
          cotm: true,
          modifiedDate: new Date()
        }
      },
      {
        new: true,
        runValidators: true
      }, (err, camera) => {
        if (err) {
          log.error({
            message: `changeCameraOfTheMoment(${id}):findOneAndUpdate`,
            stack: err
          });
          reject(err);
        } else {
          CameraModel.findOneAndUpdate({ $and: [{ cotm: true }, { _id: { $ne: id } }] },
          { $set: { cotm: false, modifiedDate: new Date() } },
          { new: true, runValidators: true }, (error) => {
            if (error) {
              log.error({
                message: `changeCameraOfTheMoment(${id}):findOneAndUpdate`,
                stack: error
              });
              reject(error);
            } else {
              resolve(camera.toObject());
            }
          }
        );
        }
      }
    );
  });

/**
 * @param cameraFields
 * @returns {Promise|*}
 */
export function createCamera(cameraFields) {
  return new Promise((resolve, reject) => {
    cameraFields.alias = cameraFields.alias.toLowerCase();
    cameraFields.modifiedDate = new Date();
    if (cameraFields.streamUrl && !cameraFields.playlistUrl) {
      cameraFields.playlistUrl = cameraFields.streamUrl;
    }

    const camera = new CameraModel(cameraFields);
    return camera.save((err) => {
      if (err) {
        log.error({
          message: 'createCamera:CameraModel()',
          stack: err
        });
        reject(err);
      } else {
        resolve(camera.toObject());
      }
    });
  });
}

/**
 *
 * @param cameraFields
 * @returns {MongooseError|undefined}
 */
export function validateCamera(cameraFields) {
  const camera = new CameraModel(cameraFields);
  return camera.validateSync();
}

/**
 * @param id
 * @param changedFields
 * @returns {Promise}
 */
export const updateCamera = (id, changedFields) => {
  changedFields.modifiedDate = new Date();
  if (changedFields.streamUrl && !changedFields.playlistUrl) {
    changedFields.playlistUrl = changedFields.streamUrl;
  }

  delete changedFields.status;
  delete changedFields._id;
  return new Promise((resolve, reject) =>
    CameraModel.findOneAndUpdate({ _id: id },
      { $set: changedFields }, { new: true, runValidators: true }, (err, camera) => {
        if (err) {
          log.error({
            message: `updateCamera(${id}):findOneAndUpdate`,
            stack: err
          });
          reject(err);
        } else {
          resolve(camera.toObject());
        }
      }
    )
  );
};
