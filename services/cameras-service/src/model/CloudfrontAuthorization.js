import AWS from 'aws-sdk';
import { logger } from '../common/logger';

const log = logger('cameras-service:cloudfront-authorization');

// aws ssm put-parameter --type SecureString --name /development/cloudfront/key_private \
//  --region us-west-1 --overwrite --value "<PASTE PEM HERE>"
// aws ssm get-parameter --name /development/cloudfront/key_private --region us-west-1 \
//  --with-decryption

let signer;
const AWS_ENVIRONMENT = process.env.NODE_ENV === 'production' ? 'legacy' : 'development';
const getCloudfrontSigningPolicy = (baseUrl, epochTime) => ({
  Statement: [
    {
      Resource: `${baseUrl}*`,
      Condition: {
        DateLessThan: {
          'AWS:EpochTime': epochTime,
        }
      }
    }
  ]
});

export const initCloudfrontSigner = () => new Promise((resolve, reject) => {
  const ssmReferences = {
    private: `/${AWS_ENVIRONMENT}/cloudfront/key_private`,
    id: `/${AWS_ENVIRONMENT}/cloudfront/key_id`,
  };
  const ssm = new AWS.SSM({
    region: 'us-west-1',
  });
  ssm.getParameters({
    Names: Object.values(ssmReferences),
    WithDecryption: true
  }, (err, { Parameters, InvalidParameters }) => {
    if (err) {
      log.error(err);
      reject(err);
    } else if (InvalidParameters && InvalidParameters[0]) {
      log.error(InvalidParameters);
      reject(InvalidParameters);
    }
    let privateKey = '';
    let keyId = '';
    Parameters.forEach(({ Name, Value }) => {
      if (Name === ssmReferences.private) {
        privateKey = Value;
      } else if (Name === ssmReferences.id) {
        keyId = Value;
      }
    });
    signer = new AWS.CloudFront.Signer(
      keyId,
      privateKey
    );
    log.info(`Initialized cloudfront signer for ${process.env.NODE_ENV}`);
    resolve(true);
  });
});

export const generateSignedCookie = (baseUrl, epochTime) => signer.getSignedCookie({
  policy: JSON.stringify(getCloudfrontSigningPolicy(baseUrl, epochTime))
});

