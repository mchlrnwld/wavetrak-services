import { APIError } from '@surfline/services-common/dist';
import mongoose from 'mongoose';
import CameraCrowdsModel from './CameraCrowdsModel';

/**
 * @param {object} cameraCrowdsObject object with params:
 *      cameraId, timestamp, stillObjectKey, annotatedObjectKey, waterCount, beachCount, visibility
 */
export const checkStatusIsValid = (cameraCrowdsObject) => {
  if (cameraCrowdsObject.waterCount === null && cameraCrowdsObject.status === 'CROWD_AVAILABLE') {
    throw new APIError('status cannot be CROWD_AVAILABLE when waterCount is null');
  }
};

/**
 * @param {object} cameraCrowdsObject object with params:
 *      cameraId, timestamp, stillObjectKey, annotatedObjectKey, waterCount, beachCount, visibility
 */
export const createCameraCrowds = async (cameraCrowdsObject) => {
  const cameraCrowd = new CameraCrowdsModel({
    cameraId: new mongoose.Types.ObjectId(cameraCrowdsObject.cameraId),
    ...cameraCrowdsObject,
  });
  checkStatusIsValid(cameraCrowdsObject);
  return cameraCrowd.save();
};

export const getCameraCrowds = (cameraId, startDate, endDate, filterSuccessful = false) => (
  CameraCrowdsModel
      .find({ $and: [
        { cameraId },
        { timestamp: { $lte: parseInt(endDate, 10) } },
        { timestamp: { $gte: parseInt(startDate, 10) } },
        ...(
          filterSuccessful ?
          [{ status: 'CROWD_AVAILABLE' }] : []
        )
      ] })
      .select('-__v')
      .sort({ timestamp: 1 })
      .lean()
      .exec()
);
