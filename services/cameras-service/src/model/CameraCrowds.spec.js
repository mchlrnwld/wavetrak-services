import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';
import { createCameraCrowds } from './CameraCrowds';
import crowdsModel from '../server/fixtures/crowds/crowdsModel.json';
import CameraCrowdsModel from './CameraCrowdsModel';

chai.use(dirtyChai);
let mongoServer;

describe('model / CameraCrowds', () => {
  describe('model / CameraCrowds / createCameraCrowds', () => {
    before(async () => {
      mongoServer = new MongodbMemoryServer();

      const mongoUri = await mongoServer.getUri();
      return mongoose.connect(
          mongoUri,
          { useNewUrlParser: true, useUnifiedTopology: true },
          err => err,
        );
    });

    after(() => {
      mongoServer = null;
      mongoose.disconnect();
    });

    // eslint-disable-next-line max-len
    it('inserts crowd count data into mongo and retrieves the inserted data using the object ID', async () => {
      const createdCamCount = await createCameraCrowds(crowdsModel[0]);
      expect(createdCamCount).to.exist();

      const savedCrowd = await CameraCrowdsModel.findOne({ _id: createdCamCount._id });
      expect(savedCrowd.toObject()).to.be.eql(createdCamCount.toObject());
      expect(savedCrowd.toObject()).to.eql({
        visibility: { clear: 1.234, glare: 2.345, fog: 1.234, rainblur: 2.345 },
        _id: createdCamCount._id,
        cameraId: new mongoose.Types.ObjectId('41224d776a326fb40f000001'),
        timestamp: 1519860967455,
        waterCount: 12,
        beachCount: null,
        stillObjectKey: 'still_object_key',
        annotatedObjectKey: 'annotated_object_key',
        status: 'CROWD_PENDING',
        __v: 0
      });
    });

    it('inserts a placeholder crowd count', async () => {
      const createdCamCount = await createCameraCrowds({
        cameraId: new mongoose.Types.ObjectId('41224d776a326fb40f000001'),
        timestamp: 1519860967455,
        stillObjectKey: 'still_object_key',
      });

      expect(createdCamCount.toObject()).to.eql({
        _id: createdCamCount._id,
        __v: 0,
        cameraId: new mongoose.Types.ObjectId('41224d776a326fb40f000001'),
        timestamp: 1519860967455,
        stillObjectKey: 'still_object_key',
        waterCount: null,
        beachCount: null,
        status: 'CROWD_PENDING',
      });
    });
  });
});
