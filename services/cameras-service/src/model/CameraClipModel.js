import mongoose from 'mongoose';

const CameraClipSchema = mongoose.Schema({
  cameraId: {
    type: String,
    required: [true, 'Camera ID is required'],
  },
  startTimestampInMs: {
    type: Number,
    required: [true, 'Start Timestamp (in ms) is required'],
  },
  endTimestampInMs: {
    type: Number,
    required: [true, 'End Timestamp (in ms) is required'],
  },
  status: {
    type: String,
    enum: [
      'CLIP_AVAILABLE',
      'CLIP_DOES_NOT_EXIST',
      'CLIP_NOT_AVAILABLE',
      'CLIP_OUT_OF_RANGE',
      'CLIP_PENDING',
      'CLIP_RETRY_TIMEOUT',
      'CLIP_TOO_LONG',
      'FATAL_ERROR',
    ],
    default: 'CLIP_PENDING',
  },
  bucket: { type: String },
  clip: {
    key: { type: String },
    url: { type: String },
  },
  thumbnail: {
    key: { type: String },
    url: { type: String },
    sizes: [Number],
  }
}, {
  id: false,
  toObject: { virtuals: true },
  toJSON: { virtuals: true },
  timestamps: true,
  collection: 'CameraClip',
});

// Alias _id to clipId
CameraClipSchema.virtual('clipId').get(function setClipId() {
  return this._id.toHexString();
});

CameraClipSchema.set('toJSON', {
  transform: (doc, ret) => {
    ret.clipId = ret._id.toHexString(); // eslint-disable-line no-param-reassign
    delete ret._id; // eslint-disable-line no-param-reassign

    delete ret.__v; // eslint-disable-line no-param-reassign

    delete ret.createdAt; // eslint-disable-line no-param-reassign
    delete ret.updatedAt; // eslint-disable-line no-param-reassign

    return ret;
  },
});

const CameraClipModel = mongoose.model('CameraClip', CameraClipSchema);

export const createCameraClip = async (model) => {
  const cameraClip = new CameraClipModel(model);
  await cameraClip.save(); // eslint-disable-line no-unused-expressions
  return cameraClip;
};

export default CameraClipModel;
