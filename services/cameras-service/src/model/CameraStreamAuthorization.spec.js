import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import * as logger from '../common/logger';
import * as CameraStreamAuthorization from './CameraStreamAuthorization';
import * as CloudfrontAuthorization from './CloudfrontAuthorization';

sinonStubPromise(sinon);
const expect = chai.expect;
const assert = chai.assert;
chai.should();
chai.use(sinonChai);

describe('camera stream authorization model', () => {
  let loggerStub;
  let generateSignedCookiePromise;
  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      info: console.log,
      error: console.log,
      debug: console.log,
      trace: console.log
    });
  });
  after(() => {
    loggerStub.restore();
  });
  beforeEach(() => {
    generateSignedCookiePromise = sinon.stub(CloudfrontAuthorization, 'generateSignedCookie')
      .returnsPromise();
    sinon.stub(CameraStreamAuthorization, 'logStreamAuthorization').returns({});
  });
  afterEach(() => {
    generateSignedCookiePromise.restore();
    CameraStreamAuthorization.logStreamAuthorization.restore();
  });

  it('should initialize', () => {
    assert(expect(CameraStreamAuthorization).to.exist);
  });
  it('should return cookie information for camera stream', async () => {
    const cookies = { dummy: 1 };
    generateSignedCookiePromise.resolves(cookies);
    const cookieHash = await CameraStreamAuthorization.getCameraStreamCookies('', '', 'http://');
    expect(cookieHash.dummy).to.equal(cookies.dummy);
  });
});
