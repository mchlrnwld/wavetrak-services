import mongoose from 'mongoose';

const CameraAliasSchema = mongoose.Schema({
  oldAlias: {
    type: String,
    trim: true,
    validate: {
      validator: v => /^[a-zA-Z0-9\-_]{0,40}$/.test(v),
      message: 'Old alias must not have special characters or spaces.'
    }
  },
  newAlias: {
    type: String,
    trim: true,
    validate: {
      validator: v => /^[a-zA-Z0-9\-_]{0,40}$/.test(v),
      message: 'New alias must not have special characters or spaces.'
    }
  },
}, {
  collection: 'CameraAliases'
});

CameraAliasSchema.index(
  { oldAlias: 1 },
  { name: 'oldAlias_1' }
);

const CameraAliasModel = mongoose.model('CameraAliases', CameraAliasSchema);

export default CameraAliasModel;
