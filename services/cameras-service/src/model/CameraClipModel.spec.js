import chai from 'chai';
import CameraClipModel from './CameraClipModel';

const expect = chai.expect;
const assert = chai.assert;

describe('camera clip model', () => {
  it('should require camera ID field', (done) => {
    const cameraClip = new CameraClipModel({});
    cameraClip.save((err) => {
      assert(expect(err).to.be.ok);
      assert(expect(err.name).to.equal('ValidationError'));
      assert(expect(err.errors).to.be.ok);
      assert(expect(err.errors.cameraId).to.be.ok);
      assert(expect(err.errors.cameraId.message)
        .to.equal('Camera ID is required'));
      return done();
    });
  });

  it('should require start timestamp field', (done) => {
    const cameraClip = new CameraClipModel({});
    cameraClip.save((err) => {
      assert(expect(err).to.be.ok);
      assert(expect(err.name).to.equal('ValidationError'));
      assert(expect(err.errors).to.be.ok);
      assert(expect(err.errors.startTimestampInMs).to.be.ok);
      assert(expect(err.errors.startTimestampInMs.message)
        .to.equal('Start Timestamp (in ms) is required'));
      return done();
    });
  });

  it('should require end timestamp field', (done) => {
    const cameraClip = new CameraClipModel({});
    cameraClip.save((err) => {
      assert(expect(err).to.be.ok);
      assert(expect(err.name).to.equal('ValidationError'));
      assert(expect(err.errors).to.be.ok);
      assert(expect(err.errors.endTimestampInMs).to.be.ok);
      assert(expect(err.errors.endTimestampInMs.message)
        .to.equal('End Timestamp (in ms) is required'));
      return done();
    });
  });
});
