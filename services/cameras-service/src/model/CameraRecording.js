import moment from 'moment';
import * as logger from '../common/logger';
import CameraRecordingModel from './CameraRecordingModel';

const log = logger.logger('camera-recording-model');


/**
 *
 * @param file waikikibeachcam.1930.2018-04-25.mp4
 * @returns {number} Milliseconds since Unix Epoch
 */
export const getDateFromLegacyRecording = (file) => {
  // eslint-disable-next-line array-bracket-spacing
  const [, time, date, ] = file.split('.');
  const hour = time.slice(0, time.length - 2);
  const minute = time.slice(time.length - 2);
  const timestamp = `${date}T${hour.padStart(2, '0')}:${minute}-05:00`;
  return Number.parseInt(moment(timestamp).format('X'), 10) * 1000;
};


/**
 * Given a legacy recording name, return recordingUrl for closest match
 *
 * @param alias waikikibeachcam
 * @param file waikikibeachcam.1930.2018-04-25.mp4
 * @returns {Query|*} Promise for lean Mongo find
 */
export const getCameraRecordingBestMatch = (alias, file) => CameraRecordingModel.find({
  $and: [
    { startDate: { $gte: getDateFromLegacyRecording(file) } },
    { alias: alias.toLowerCase() },
    { expireSoft: { $gt: Date.now() } }
  ] },
'recordingUrl -_id',
{ sort: { startDate: 1 } },
(err, cameraRecordings) => {
  if (err) {
    log.error({
      action: 'CameraRecordingModel:getCameraRecordingBestMatch()',
      stack: err
    });
  }
  return cameraRecordings || [];
}).limit(1).lean();

/**
 * Get camera recordings within range for a camera.
 *
 * Recording must fall entirely within the search range.
 *
 * No partials unless `allowPartialMatch` is specified.
 *
 * Example: []=searchWindow {##}=recordingWindow -=timeline
 *
 * allowPartialMatch == false
 *  Match:     --[--{##}---]--
 *  No Match:  --{##[##}---]--
 *
 * allowPartialMatch matches any clip that overlaps at all with the given range.
 *
 * allowPartialMatch == true
 *  Match:     --[--{##}---]--
 *  Match:     --{##[##}---]--
 *
 * includeExpired returns clips that have expired if True
 *
  * Note: Uses a compound MongoDB index. If you add fields to this, update the index.
 *
 * @param startDate $gte
 * @param endDate $lte
 * @param cameraId
 * @param allowPartialMatch
 * @param includeExpired
 *
 * @returns {Promise}
 */
export const getCameraRecording = async (
  startDate, endDate, cameraId, allowPartialMatch, includeExpired, highlightsOnly = false
) => {
  const andExpression = [{ cameraId }];

  if (allowPartialMatch) {
    andExpression.push({ startDate: { $lte: endDate } });
    andExpression.push({ endDate: { $gte: startDate } });
  } else {
    andExpression.push({ startDate: { $gte: startDate } });
    andExpression.push({ endDate: { $lte: endDate } });
  }

  if (!includeExpired) {
    andExpression.push({ expireSoft: { $gt: Date.now() } });
  }

  if (highlightsOnly) {
    andExpression.push({ highlights: { $exists: true } });
  }

  return CameraRecordingModel
    .find({ $and: andExpression })
    .select(
      'cameraId alias startDate endDate recordingUrl thumbLargeUrl thumbSmallUrl highlights -_id'
    )
    .sort({ startDate: -1 })
    .lean()
    .exec();
};

/**
 * @param cameraRecordingFields
 *
 *  Required fields:
 *    cameraId, alias, startDate, endDate,
 *    s3Url, thumbSmallUrl, thumbLargeUrl, resolution
 *
 * @returns {Promise|*}
 */
export function createCameraRecording(cameraRecordingFields) {
  return new Promise((resolve, reject) => {
    const cameraRecording = new CameraRecordingModel(cameraRecordingFields);
    return cameraRecording.save((err) => {
      if (err) {
        log.error({
          message: 'CameraRecordingModel:createCameraRecording()',
          stack: err
        });
        reject(err);
      } else {
        resolve(cameraRecording.toObject());
      }
    });
  });
}
