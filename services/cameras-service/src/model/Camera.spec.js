import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import * as logger from '../common/logger';
import CameraModel from './CameraModel';

const expect = chai.expect;
const assert = chai.assert;
chai.should();
chai.use(sinonChai);

describe('camera model', () => {
  const objectId = '41224d776a326fb40f000001';

  let camera;
  let loggerStub;
  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      info: console.log,
      error: console.log,
      debug: console.log,
      trace: console.log
    });

    camera = require('./Camera');
  });
  after(() => {
    loggerStub.restore();
  });
  beforeEach(() => {
    sinon.stub(CameraModel, 'find');
    sinon.stub(CameraModel, 'findOneAndUpdate');
    CameraModel.find.returns({ lean: sinon.stub.resolves({}) });
    CameraModel.findOneAndUpdate.returns({ lean: sinon.stub.resolves({}) });
  });

  afterEach(() => {
    CameraModel.find.restore();
    CameraModel.findOneAndUpdate.restore();
  });

  it('should initialize as an object', () => {
    assert(expect(camera).to.exist);
  });

  it('should return a camera with getCameraById(id)', () => {
    camera.getCameraById(objectId);
    assert(CameraModel.find.calledOnce);
  });
  it('should return a camera with getCameraByAlias(alias)', () => {
    camera.getCameraByAlias('testcam');
    assert(CameraModel.find.calledOnce);
  });

  it('should not return a camera with getCamera(undefined)', () => {
    camera.getCameraByAlias();
    camera.getCameraById();
    assert(CameraModel.find.notCalled);
  });
  it('should update a camera with updateCamera', () => {
    const spy = sinon.spy();
    camera.updateCamera(objectId, {
      cameraTitle: 'Updated Camera Title',
      playlistUrl: 'Updated Stream URL'
    }, spy);
    assert(CameraModel.findOneAndUpdate.calledOnce);
  });
  it('should create a camera with createCamera', () => {
    const createdCamera = camera.createCamera({
      alias: 'testcam',
      playlistUrl: 'Stream URL'
    });
    assert(expect(createdCamera).to.exist);
  });
});
