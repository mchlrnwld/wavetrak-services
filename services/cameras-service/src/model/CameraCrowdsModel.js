import mongoose from 'mongoose';

const CameraCrowdsSchema = new mongoose.Schema(
  {
    id: { type: mongoose.Schema.Types.ObjectId },
    timestamp: { type: Number, required: [true, 'A Timestamp is required'] },
    // eslint-disable-next-line max-len
    cameraId: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'A camera ID is required']
    },
    stillObjectKey: { type: String },
    annotatedObjectKey: { type: String },
    waterCount: { type: Number, default: null },
    beachCount: { type: Number, default: null },
    visibility: {
      clear: { type: Number },
      glare: { type: Number },
      fog: { type: Number },
      rainblur: { type: Number }
    },
    status: {
      type: String,
      enum: [
        'CROWD_AVAILABLE',
        'CROWD_DOES_NOT_EXIST',
        'CROWD_PENDING',
        'FATAL_ERROR',
      ],
      default: 'CROWD_PENDING',
    }
  });

const CameraCrowdsModel = mongoose.model('CameraCrowds', CameraCrowdsSchema);

export default CameraCrowdsModel;
