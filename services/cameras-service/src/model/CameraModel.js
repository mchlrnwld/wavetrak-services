import mongoose from 'mongoose';

const CameraSchema = mongoose.Schema(
  {
    alias: {
      type: String,
      index: true,
      trim: true,
      validate: {
        validator: (v) => /^[a-zA-Z0-9\-_]{0,40}$/.test(v),
        message: 'Alias must not have special characters or spaces.',
      },
    },
    cotm: {
      type: Boolean,
      default: false,
    },
    isPremium: {
      type: Boolean,
      default: false,
    },
    isDown: {
      type: Number,
      default: 0,
    },
    message: {
      type: String,
      trim: true,
      default: '',
      maxlength: [250, 'Message must not have more than 250 characters.'],
    },
    modifiedDate: {
      type: Date,
      default: Date.now,
    },
    title: {
      type: String,
      trim: true,
      maxlength: [50, 'Title cannot be more than 50 characters.'],
    },
    status: {
      type: String,
      trim: true,
      default: 'active',
    },
    playlistUrl: {
      type: String,
      validate: [/\S+/, 'Playlist URL is a required field'],
      required: [true, 'Playlist URL is a required field'],
    },
    isPrerecorded: {
      type: Boolean,
      default: false,
    },
    prerecordedClipRecordingTimes: [
      {
        startHours: {
          type: Number,
          default: 0,
        },
        startMinutes: {
          type: Number,
          default: 0,
        },
        endHours: {
          type: Number,
          default: 0,
        },
        endMinutes: {
          type: Number,
          default: 0,
        },
      },
    ],
    lastPrerecordedClipEndTimeUTC: {
      type: Date,
    },
    lastPrerecordedClipStartTimeUTC: {
      type: Date,
    },
    supportsHighlights: {
      type: Boolean,
      default: false,
    },
    supportsCrowds: {
      type: Boolean,
      default: false,
    },
  },
  { collection: 'Cameras' }
);

CameraSchema.index({ title: 'text' }, { name: 'Camera Title Text Search Index' });

const CameraModel = mongoose.model('Cameras', CameraSchema);

export default CameraModel;
