import mongoose from 'mongoose';

const CameraRecordingSchema = mongoose.Schema(
  {
    cameraId: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'cameraId is a required field'],
    },
    alias: {
      type: String,
      trim: true,
      lowercase: true,
      validate: {
        validator: (v) => /^[a-zA-Z0-9\-_]{0,40}$/.test(v),
        message: 'Alias must not have special characters or spaces.',
      },
      required: [true, 'camera alias is a required field'],
    },
    startDate: {
      type: Date,
      required: [true, 'startDate is a required field'],
    },
    endDate: {
      type: Date,
      required: [true, 'endDate is a required field'],
    },
    expireSoft: {
      // Converted to Glacier storage
      // User clients should filter on this
      type: Date,
      default: () => new Date(Date.now() + 6 * 86400000), // plus 6 days
    },
    recordingUrl: {
      type: String,
      validate: [/\S+/, 'recording URL is a required field'],
      required: [true, 'recording URL is a required field'],
      unique: true,
    },
    thumbSmallUrl: {
      type: String,
      validate: [/\S+/, 'small thumb URL is a required field'],
      required: [true, 'small thumb URL is a required field'],
    },
    thumbLargeUrl: {
      type: String,
      validate: [/\S+/, 'small thumb URL is a required field'],
      required: [true, 'small thumb URL is a required field'],
    },
    resolution: {
      // Format: 1280x720...
      type: String,
      required: [true, 'resolution is a required field'],
    },
    highlights: {
      url: { type: String, match: /http.*\.mp4/ },
      thumbUrl: { type: String, match: /http.*\.jpg/ },
      gifUrl: { type: String, match: /http.*\.gif/ },
      duration: { type: Number }
    }
  },
  { collection: 'CameraRecording' }
);

CameraRecordingSchema.index(
  { startDate: 1, endDate: 1, cameraId: 1, alias: 1, expireSoft: 1 },
  {
    name: 'Date Range Query for camera',
  }
);

CameraRecordingSchema.index({
  cameraId: 1,
});

CameraRecordingSchema.index({
  alias: 1,
});

CameraRecordingSchema.index(
  {
    recordingUrl: 1,
  },
  { unique: true }
);

CameraRecordingSchema.index({
  cameraId: 1,
  startDate: -1,
});

const CameraRecordingModel = mongoose.model('CameraRecording', CameraRecordingSchema);

export default CameraRecordingModel;
