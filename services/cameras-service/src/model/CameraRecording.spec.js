import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import * as logger from '../common/logger';
import * as CameraRecording from './CameraRecording';

import CameraRecordingModel from './CameraRecordingModel';
import singleRecordingModel from '../server/fixtures/recording/singleRecordingModel.json';

sinonStubPromise(sinon);
chai.should();
chai.use(dirtyChai);
chai.use(sinonChai);

describe('model / CameraRecording', () => {
  it('should initialize', () => {
    expect(CameraRecording).to.exist();
  });

  describe('getDateFromLegacyRecording', () => {
    let loggerStub;
    before(() => {
      loggerStub = sinon.stub(logger, 'logger').returns({
        info: console.log,
        error: console.log,
        debug: console.log,
        trace: console.log
      });
    });
    after(() => {
      loggerStub.restore();
    });

    it('should parse legacy recording date with leading-zero hour', async () => {
      const recordingTs = (
        CameraRecording.getDateFromLegacyRecording('waikikibeachcam.0930.2018-04-25.mp4')
      );
      expect(recordingTs).to.equal(1524666600000);
    });

    it('should parse legacy recording date with single-digit hour', async () => {
      const recordingTs = (
        CameraRecording.getDateFromLegacyRecording('waikikibeachcam.930.2018-04-25.mp4')
      );
      expect(recordingTs).to.equal(1524666600000);
    });

    it('should parse legacy recording date with two-digit hour', async () => {
      const recordingTs = (
        CameraRecording.getDateFromLegacyRecording('waikikibeachcam.1930.2018-04-25.mp4')
      );
      expect(recordingTs).to.equal(1524702600000);
    });
  });

  describe('getCameraRecordingModel', () => {
    beforeEach(() => {
      sinon.stub(CameraRecordingModel, 'find');

      CameraRecordingModel.find.returns({
        select: sinon.stub().returnsThis(),
        sort: sinon.stub().returnsThis(),
        lean: sinon.stub().returnsThis(),
        exec: sinon.stub().resolves({
          ...singleRecordingModel,
          toJSON: sinon.stub().returns(singleRecordingModel),
        }),
      });
    });

    afterEach(() => {
      CameraRecordingModel.find.restore();
    });

    it('finds camera recordings within the start and end date by default', async () => {
      const cameraId = singleRecordingModel[0].cameraId;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const allowPartialMatch = false;
      const includeExpired = false;

      const fetchedRecording = await CameraRecording.getCameraRecording(
        startTimestampInMs, endTimestampInMs, cameraId, allowPartialMatch, includeExpired
      );

      expect(fetchedRecording[0]).to.deep.equal(singleRecordingModel[0]);

      const args = CameraRecordingModel.find.getCall(0).args[0];
      expect(args).to.have.nested.property('$and[0].cameraId', cameraId);
      expect(args).to.have.nested.property('$and[1].startDate.$gte',
        Number.parseInt(startTimestampInMs, 10));
      expect(args).to.have.nested.property('$and[2].endDate.$lte',
        Number.parseInt(endTimestampInMs, 10));
      expect(args).to.have.nested.property('$and[3].expireSoft');
      expect(args.$and).to.have.length(4);
    });

    it('finds camera recordings that partially exists within the start and end date', async () => {
      const cameraId = singleRecordingModel[0].cameraId;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const allowPartialMatch = true;
      const includeExpired = false;

      const fetchedRecording = await CameraRecording.getCameraRecording(
        startTimestampInMs, endTimestampInMs, cameraId, allowPartialMatch, includeExpired
      );

      expect(fetchedRecording[0]).to.deep.equal(singleRecordingModel[0]);

      const args = CameraRecordingModel.find.getCall(0).args[0];
      expect(args).to.have.nested.property('$and[0].cameraId', cameraId);
      expect(args).to.have.nested.property('$and[1].startDate.$lte',
        Number.parseInt(endTimestampInMs, 10));
      expect(args).to.have.nested.property('$and[2].endDate.$gte',
        Number.parseInt(startTimestampInMs, 10));
      expect(args).to.have.nested.property('$and[3].expireSoft');
      expect(args.$and).to.have.length(4);
    });

    it('finds expired camera recordings within the start and end date', async () => {
      const cameraId = singleRecordingModel[0].cameraId;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const allowPartialMatch = false;
      const includeExpired = true;

      const fetchedRecording = await CameraRecording.getCameraRecording(
        startTimestampInMs, endTimestampInMs, cameraId, allowPartialMatch, includeExpired
      );

      expect(fetchedRecording[0]).to.deep.equal(singleRecordingModel[0]);

      const args = CameraRecordingModel.find.getCall(0).args[0];
      expect(args).to.have.nested.property('$and[0].cameraId', cameraId);
      expect(args).to.have.nested.property('$and[1].startDate.$gte',
        Number.parseInt(startTimestampInMs, 10));
      expect(args).to.have.nested.property('$and[2].endDate.$lte',
        Number.parseInt(endTimestampInMs, 10));
      expect(args.$and).to.have.length(3);
    });

    it('finds expired recordings that partially exist within the start and end date', async () => {
      const cameraId = singleRecordingModel[0].cameraId;
      const startDate = new Date(singleRecordingModel[0].startDate);
      const startTimestampInMs = startDate.getTime();
      const endDate = new Date(singleRecordingModel[0].endDate);
      const endTimestampInMs = endDate.getTime();
      const allowPartialMatch = true;
      const includeExpired = true;

      const fetchedRecording = await CameraRecording.getCameraRecording(
        startTimestampInMs, endTimestampInMs, cameraId, allowPartialMatch, includeExpired
      );

      expect(fetchedRecording[0]).to.deep.equal(singleRecordingModel[0]);

      const args = CameraRecordingModel.find.getCall(0).args[0];
      expect(args).to.have.nested.property('$and[0].cameraId', cameraId);
      expect(args).to.have.nested.property('$and[1].startDate.$lte',
        Number.parseInt(endTimestampInMs, 10));
      expect(args).to.have.nested.property('$and[2].endDate.$gte',
        Number.parseInt(startTimestampInMs, 10));
      expect(args.$and).to.have.length(3);
    });
  });
});
