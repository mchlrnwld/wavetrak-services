import mongoose from 'mongoose';
import * as logger from '../common/logger';

const log = logger.logger('cameras-service:DB');
mongoose.Promise = global.Promise;

export function initMongoDB() {
  const connectionString = process.env.MONGO_CONNECTION_STRING_KBYG;
  const mongoDbConfig = {
    poolSize: 50,
    keepAlive: 120,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      log.debug(`Mongo ConnectionString: ${connectionString} `);

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open',
        () => {
          log.info(`MongoDB connected on ${connectionString}`);
          resolve();
        }
      );
      mongoose.connection.on('error',
        (error) => {
          log.error({
            action: 'MongoDB:ConnectionError',
            error
          });
          reject(error);
        }
      );
    } catch (error) {
      log.error({
        message: 'MongoDB:ConnectionError',
        stack: error
      });
      reject(error);
    }
  });
}
