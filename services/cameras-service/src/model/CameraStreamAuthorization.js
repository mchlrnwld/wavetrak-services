import moment from 'moment';
import * as logger from '../common/logger';
import { generateSignedCookie } from './CloudfrontAuthorization';

const log = logger.logger('cameras-service:authorization');

export const logStreamAuthorization = (options) => {
  log.info('Stream Authorized', options);
  // TODO Kinesis logging
};

/**
 * https://www.spacevatican.org/2015/5/1/using-cloudfront-signed-cookies/
 * Returns cookies for cam stream
 *
 * Note: we can lock down by IpAddress in signing policy
 *
 * alias: set_cookies
 */
export const getCameraStreamCookies = async (authenticatedUserId, ip, streamUrl) => {
  // CloudFront cookie expiry must match expires times on cookies
  const baseUrl = streamUrl.replace(/playlist\.m3u8/, '');
  const expires = moment.utc().add(6, 'h');
  const epochTime = parseInt(expires.format('X'), 10);
  const cookieHash = await generateSignedCookie(baseUrl, epochTime);
  logStreamAuthorization({
    cloudFrontSignature: cookieHash['CloudFront-Signature'],
    authenticatedUserId,
    ip,
    streamUrl,
  });
  return cookieHash;
};
