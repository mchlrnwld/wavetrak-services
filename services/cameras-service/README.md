# Camera service

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Quick Start](#quick-start)
  - [Copy Latest Recordings to lower tiers for testing](#copy-latest-recordings-to-lower-tiers-for-testing)
    - [staging](#staging)
    - [sandbox](#sandbox)
- [Resources](#resources)
  - [Cameras](#cameras)
    - [Collection](#collection)
    - [Schema](#schema)
    - [Endpoints](#endpoints)
      - [Get Camera - `GET /cameras/:id`](#get-camera---get-camerasid)
        - [Example](#example)
      - [Get Multiple Cameras - `GET /cameras`](#get-multiple-cameras---get-cameras)
        - [Example](#example-1)
      - [Get Multiple Cameras, Order By Field - `GET /cameras?orderBy=:field`](#get-multiple-cameras-order-by-field---get-camerasorderbyfield)
        - [Example](#example-2)
      - [Get Multiple Cameras, Filter By Title - `GET /cameras?search=:term`](#get-multiple-cameras-filter-by-title---get-camerassearchterm)
        - [Example](#example-3)
      - [Get Down Cameras - `GET /cameras/down`](#get-down-cameras---get-camerasdown)
        - [Example](#example-4)
      - [Get Camera ID From Alias - `GET /cameras/ValidateAlias?alias=:camalias`](#get-camera-id-from-alias---get-camerasvalidatealiasaliascamalias)
        - [Example](#example-5)
      - [Create Camera - `POST /cameras`](#create-camera---post-cameras)
        - [`isDown` Enum](#isdown-enum)
        - [Example](#example-6)
      - [Update Camera - `PUT /cameras/:id/UpdateCamera`](#update-camera---put-camerasidupdatecamera)
        - [Example](#example-7)
      - [Change Alias - `PUT /cameras/:id/ChangeAlias`](#change-alias---put-camerasidchangealias)
        - [Example](#example-8)
      - [Log Camera Event - `POST /cameras/:id/log`](#log-camera-event---post-camerasidlog)
        - [Example](#example-9)
  - [Camera Recordings](#camera-recordings)
    - [Collection](#collection-1)
    - [Schema](#schema-1)
    - [Endpoints](#endpoints-1)
      - [Create a Camera Recording - `POST /cameras/recording/id/:cameraId`](#create-a-camera-recording---post-camerasrecordingidcameraid)
        - [Example](#example-10)
      - [Get Camera Recordings by Alias - `GET /cameras/recording/:alias?startDate=:startTimestampInMs&endDate=:endTimestampInMs`](#get-camera-recordings-by-alias---get-camerasrecordingaliasstartdatestarttimestampinmsenddateendtimestampinms)
        - [Example](#example-11)
      - [Get Camera Recordings by Id - `GET /cameras/recording/id/:cameraId?startDate=:startTimestampInMs&endDate=:endTimestampInMs`](#get-camera-recordings-by-id---get-camerasrecordingidcameraidstartdatestarttimestampinmsenddateendtimestampinms)
        - [Example](#example-12)
      - [Get Camera Recordings with Partial Matches - `GET /cameras/recording/:alias?startDate=:startTimestampInMs&endDate=:endTimestampInMs&allowPartialMatch=true`](#get-camera-recordings-with-partial-matches---get-camerasrecordingaliasstartdatestarttimestampinmsenddateendtimestampinmsallowpartialmatchtrue)
        - [Example](#example-13)
      - [Get Expired Camera Recordings - `GET /cameras/recording/:alias?startDate=:startTimestampInMs&endDate=:endTimestampInMs&includeExpired=true`](#get-expired-camera-recordings---get-camerasrecordingaliasstartdatestarttimestampinmsenddateendtimestampinmsincludeexpiredtrue)
        - [Example](#example-14)
      - [Get Legacy Camera Rewind Redirect - `GET /cameras/recording/redirect/:alias/:legacy_filename`](#get-legacy-camera-rewind-redirect---get-camerasrecordingredirectaliaslegacy_filename)
        - [Example](#example-15)
  - [Camera Clips](#camera-clips)
    - [Collection](#collection-2)
    - [Schema](#schema-2)
    - [Endpoints](#endpoints-2)
      - [Get Camera Clips - `GET /clips?ids=:id1,:id2,:id3,:id4,:id5,:id6,...`](#get-camera-clips---get-clipsidsid1id2id3id4id5id6)
        - [Response Status Codes](#response-status-codes)
        - [Example](#example-16)
        - [Example - Fields](#example---fields)
        - [Example - Bad ID](#example---bad-id)
      - [Create Camera Clip - `POST /clips`](#create-camera-clip---post-clips)
        - [Response Status Codes](#response-status-codes-1)
        - [Example](#example-17)
      - [Update Camera Clip - `PUT /clips/:id`](#update-camera-clip---put-clipsid)
        - [Clip Status Enum](#clip-status-enum)
        - [Response Status Codes](#response-status-codes-2)
        - [Example](#example-18)
  - [Camera Stream Authorization (Not Live)](#camera-stream-authorization-not-live)
    - [Authorization flow](#authorization-flow)
    - [Authorize CloudFront camera streams](#authorize-cloudfront-camera-streams)
      - [Store keys for environment](#store-keys-for-environment)
        - [Put parameter](#put-parameter)
        - [Get parameter](#get-parameter)
    - [Gotchas](#gotchas)
- [Service Validation](#service-validation)
  - [Manual Validation](#manual-validation)
  - [Scripted Validation](#scripted-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Quick Start

```bash
# install the correct version of node
nvm install v8.5.0

# install dependencies
npm install

# configure local environment variables
cp .env.sample .env
vim .env

# run tests
npm run test

# run local service
npm run startlocal
```

### Copy Latest Recordings to lower tiers for testing

Ensure you've whitelisted your IP on both surfline-dev and surfline-prod in Atlas

If you have poor internet connection use a temp EC2 instance ([Confluence: Create a Temporary EC2 Instance](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1107951999/Create+a+Temporary+EC2+Instance)) as these commands download and upload the whole collection over your connection and takes a while even with good internet. If the pipe breaks then you have to start again

If you need to sync clips change `-c CameraRecording` to `-c CameraClips` or any other collection for that matter. don't copy `Spots` as that requires an aditional task to assign the correct POI Ids since they differ across environments

#### staging
```
mongodump "mongodb+srv://[USER]:[PASSWORD]@prod-common-mongodb-atlas-aj1dk.mongodb.net/KBYG?ssl=true&authSource=admin&readPreference=secondaryPreferred" -c CameraRecording --archive --ssl | mongorestore "mongodb+srv://[USER]:[PASSWORD]@dev-common-mongodb-atla.baspr.mongodb.net/KBYG?retryWrites=true&w=majority" --archive --ssl --drop
```

#### sandbox
```
mongodump "mongodb+srv://[USER]:[PASSWORD]@prod-common-mongodb-atlas-aj1dk.mongodb.net/KBYG?ssl=true&authSource=admin&readPreference=secondaryPreferred" -c CameraRecording --archive --ssl | mongorestore "mongodb+srv://[USER]:[PASSWORD]@dev-common-mongodb-atla.baspr.mongodb.net/KBYG_sandbox?retryWrites=true&w=majority" --archive --ssl --drop --nsFrom KBYG.CameraRecording --nsTo KBYG_sandbox.CameraRecording
```


## Resources

### Cameras

#### Collection

```
Cameras
```

#### Schema

```json
{
  "_id": "57c9e77174d8f8d01e3ccd83",
  "alias": "cam01",
  "title": "Test title",
  "modifiedDate": "2016-09-02T20:56:17.462Z",
  "message": "",
  "isPremium": false,
  "isDown": {
    "id": 0,
    "title": "No",
    "message": "",
    "status": false
  },
  "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
  "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
  "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01"
}
```

#### Endpoints

##### Get Camera - `GET /cameras/:id`

###### Example

```bash
GET /cameras/57c9e77174d8f8d01e3ccd83

# =>

{
  "__v": 0,
  "alias": "cam01",
  "title": "Test title",
  "_id": "57c9e77174d8f8d01e3ccd83",
  "modifiedDate": "2016-09-02T20:56:17.462Z",
  "message": "",
  "isPremium": false,
  "isDown": {
    "id": 0,
    "title": "No",
    "message": "",
    "status": false
  },
  "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
  "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
  "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01"
}
```

##### Get Multiple Cameras - `GET /cameras`

###### Example

```bash
GET /cameras

# =>

[
  {
    "__v": 0,
    "_id": "57d866f9df087a2359ae0161",
    "alias": "cam05",
    "isDown": {
      "id": 0,
      "message": "",
      "status": false,
      "title": "No"
    },
    "isPremium": false,
    "message": "",
    "modifiedDate": "2016-09-13T20:52:09.404Z",
    "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam05/cam05",
    "stillUrl": "http://camstills.cdn-surfline.com/cam05/latest_small.jpg",
    "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam05.smil/playlist.m3u8",
    "title": "Cam 5 Title"
  },
  {
    "__v": 0,
    "_id": "57d061bf8497083a1735a833",
    "alias": "cam01",
    "isDown": {
      "id": 0,
      "message": "",
      "status": false,
      "title": "No"
    },
    "isPremium": false,
    "message": "",
    "modifiedDate": "2016-09-07T18:52:28.703Z",
    "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01",
    "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
    "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
    "title": "Test title CHANGRE "
  }
]
```

##### Get Multiple Cameras, Order By Field - `GET /cameras?orderBy=:field`

###### Example

```bash
GET /cameras?orderBy=title

# =>

[
  {
    "__v": 0,
    "_id": "57d866f9df087a2359ae0161",
    "alias": "cam05",
    "isDown": {
      "id": 0,
      "message": "",
      "status": false,
      "title": "No"
    },
    "isPremium": false,
    "message": "",
    "modifiedDate": "2016-09-13T20:52:09.404Z",
    "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam05/cam05",
    "stillUrl": "http://camstills.cdn-surfline.com/cam05/latest_small.jpg",
    "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam05.smil/playlist.m3u8",
    "title": "Cam 5 Title"
  },
  {
    "__v": 0,
    "_id": "57d061bf8497083a1735a833",
    "alias": "cam01",
    "isDown": {
      "id": 0,
      "message": "",
      "status": false,
      "title": "No"
    },
    "isPremium": false,
    "message": "",
    "modifiedDate": "2016-09-07T18:52:28.703Z",
    "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01",
    "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
    "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
    "title": "Test title CHANGRE "
  }
]
```

##### Get Multiple Cameras, Filter By Title - `GET /cameras?search=:term`

###### Example

```bash
GET /cameras?search=Test

# =>

[
  {
    "__v": 0,
    "_id": "57c9e77174d8f8d01e3ccd83",
    "alias": "cam01",
    "isDown": {
      "id": 3,
      "message": "We apologize for the camera downtime that was caused by a Severe Weather Event at this location. We are working to restore service as soon as possible. Thank you for your patience.",
      "status": true,
      "title": "Severe Weather"
    },
    "isPremium": false,
    "message": "Test message",
    "modifiedDate": "2016-09-02T20:56:17.462Z",
    "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01",
    "score": 0.65,
    "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
    "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
    "title": "Test title cam01"
  }
]
```

##### Get Down Cameras - `GET /cameras/down`

###### Example

```bash
GET /cameras/down

# =>

[
  {
    "_id": "5cd2f613e64ac813641c836a",
    "title": "SD - Blacks",
    "alias": "wc-blacks",
    "playlistUrl": "https://cams.cdn-surfline.com/cdn-wc/wc-blacks/playlist.m3u8",
    "status": "active",
    "modifiedDate": "2019-06-24T17:00:21.912Z",
    "message": "",
    "isDown": {
      "id": 1,
      "title": "ISP",
      "message": "We're sorry to report this camera is down. The Surfline team is aware of the issue and is currently working toward a resolution.",
      "subMessage": "Please note: the down time is due to internet issues at this location. The camera will return when these issues are resolved.",
      "altMessage": "Camera Down",
      "status": true
    },
    "isPremium": true,
    "cotm": false,
    "__v": 0,
    "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/wc-blacks/playlist.m3u8",
    "stillUrl": "https://camstills.cdn-surfline.com/wc-blacks/latest_small.jpg",
    "pixelatedStillUrl": "https://camstills.cdn-surfline.com/wc-blacks/latest_small_pixelated.png",
    "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/wc-blacks/wc-blacks",
    "prerecordedClipRecordingTimes": [],
    "lastPrerecordedClipStartTimeUTC": "2019-07-11T21:49:08.454Z",
    "lastPrerecordedClipEndTimeUTC": "2019-07-11T21:49:08.454Z",
    "isPrerecorded": false
  },
  ...
]
```

##### Get Camera ID From Alias - `GET /cameras/ValidateAlias?alias=:camalias`

###### Example

```bash
GET /cameras/ValidateAlias?alias=wc-fiftyfournewport

# =>

{
  "_id": "5834949d3421b20545c4b51c",
  "alias": "wc-fiftyfournewport"
}
```

##### Create Camera - `POST /cameras`

| Property    | Type      | Required | Description                                            |
| :---------- | :-------- | :------- | :----------------------------------------------------- |
| `alias`     | `String`  | Yes      | The camera alias                                       |
| `title`     | `String`  | Yes      | Description of the camera                              |
| `message`   | `String`  | No       | Message to display to users                            |
| `isDown`    | `Enum<Number>`  | No       | Is the camera currently down? Default: `0`. Enum values below. |
| `isPremium` | `Boolean` | No       | Is the camera only for premium users? Default: `false` |

###### `isDown` Enum

| id | title | message | status |
|:--|:--|:--|:--|
| `0` | No |  | false |
| `1` | ISP | We apologize for the camera downtime due to a local Internet Service Provider issue. Their technicians are working to restore service. Our Camera will return as soon as their issue is resolved. Thank you for your patience. | true |
| `2` | Power | We apologize for the camera downtime due to a localized Power Outage issue. We are working to restore service as soon as possible. Thank you for your patience. | true |
| `3` | Severe Weather | We apologize for the camera downtime that was caused by a Severe Weather Event at this location. We are working to restore service as soon as possible. Thank you for your patience. | true |
| `4` | Lightning | We apologize for the camera downtime due to a direct lightning strike at this location. We are working to restore service as soon as possible. Thank you for your patience. | true |
| `5` | Hardware | We apologize for the camera downtime due to hardware failure. We are working to restore service as soon as possible. Thank you for your patience. | true |
| `6` | Building renovations | We apologize for the camera downtime due to ongoing building renovations at this location. Our camera will be back online when those renovations have been completed. Thank you for your patience. | true |
| `7` | Camera coming soon | Camera coming soon. | true |

###### Example

```bash
POST /cameras
{
  "alias": "cam01",
  "title": "Test title ",
  "message": "",
  "streamUrl": "https://cams.cdn-surfline.com/test-url/playlist.m3u8",
  "isPremium": false,
  "isDown": 0
}

# =>

{
  "__v": 0,
  "_id": "57c9e77174d8f8d01e3ccd83",
  "alias": "cam01",
  "isDown": {
    "id": 0,
    "message": "",
    "status": false,
    "title": "No"
  },
  "isPremium": false,
  "message": "",
  "modifiedDate": "2016-09-02T20:56:17.462Z",
  "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01",
  "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
  "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
  "title": "Test title"
}
```

Validation errors are returned as `400` with the validation details in the body.

##### Update Camera - `PUT /cameras/:id/UpdateCamera`

The following properties may be updated:

| Property    | Type      | Required | Description                                            |
| :---------- | :-------- | :------- | :----------------------------------------------------- |
| `title`     | `String`  | No       | Description of the camera                              |
| `message`   | `String`  | No       | Message to display to users                            |
| `isDown`    | `Number`  | No       | Is the camera currently down? Default: `0`             |
| `isPremium` | `Boolean` | No       | Is the camera only for premium users? Default: `false` |

Note: `alias` cannot be changed through a normal update. Use "Change Alias" (documented below) for this.

###### Example

```bash
PUT /cameras/57c9e77174d8f8d01e3ccd83/UpdateCamera
{
  "title": "Test title cam01",
  "message": "Test message",
  "isDown": 3,
  "isPremium": true
}

# =>

{
  "__v": 0,
  "_id": "57c9e77174d8f8d01e3ccd83",
  "alias": "cam01",
  "isDown": {
    "id": 3,
    "message": "We apologize for the camera downtime that was caused by a Severe Weather Event at this location. We are working to restore service as soon as possible. Thank you for your patience.",
    "status": true,
    "title": "Severe Weather"
  },
  "isPremium": false,
  "message": "Test message",
  "modifiedDate": "2016-09-02T20:56:17.462Z",
  "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01",
  "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
  "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
  "title": "Test title cam01"
}
```

##### Change Alias - `PUT /cameras/:id/ChangeAlias`

###### Example

```bash
PUT /cameras/57c9e77174d8f8d01e3ccd83/ChangeAlias
{
  "alias": "cam02"
}

# =>

{
  "__v": 0,
  "_id": "57c9e77174d8f8d01e3ccd83",
  "alias": "cam02",
  "isDown": {
    "id": 0,
    "message": "",
    "status": false,
    "title": "No"
  },
  "isPremium": false,
  "message": "",
  "modifiedDate": "2016-09-02T20:56:17.462Z",
  "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/cam01/cam01",
  "stillUrl": "http://camstills.cdn-surfline.com/cam01/latest_small.jpg",
  "streamUrl": "http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-cam01.smil/playlist.m3u8",
  "title": "Test title"
}
```

##### Log Camera Event - `POST /cameras/:id/log`

Log a camera event. You can specify the following properties to log:

| Property         | Notes                                                                 |
| ---------------- | --------------------------------------------------------------------- |
| `event`          | The name of the event to log.                                         |
| `userId`         | Users mongo id.                                                       |
| `entitlement`    | Users subscription entitlement.                                       |
| `ip`             | IP address of the user (so I can get their location after the fact).  |
| `reachableVia`   | For iOS we can determine what their connectivity is, like wifi or 3G. |
| `streamUrl`      | The URL of the stream (without query params).                         |
| `spotId`         | The spot ID for the cam.                                              |
| `platform`       | web/native                                                            |
| `platformString` | On web send the raw user agent. On iOS platform string.               |
| `carrier`        | Get the carrier name.                                                 |
| `osVersion`      | iOS version. Ignore on web.                                           |
| `appVersion`     | iOS app version ID. Ignore on web.                                    |
| `timestamp`      | Timestamp of the event.                                               |

There is no authentication required and no fields are required.

###### Example

```bash
POST /cameras/57c9e77174d8f8d01e3ccd83/log
{
  "userId": "57c9e77174d8e8d01e3ccd83",
  "entitlement": "free",
  "ip": "127.0.0.1"
}
```
### Camera Recordings

#### Collection

```
CameraRecording
```

#### Schema

```json
{
  "cameraId": "5eda6788f5fc834ed3a00a77",
  "alias": "a-test",
  "endDate": "2018-02-28T23:36:07.455Z",
  "recordingUrl": "http://stills.cdn-surfline.com/a-test/a-test.20180227T201716.mp4",
  "startDate": "2018-02-28T23:26:07.455Z",
  "thumbLargeUrl": "http://stills.cdn-surfline.com/a-test/large.jpg",
  "thumbSmallUrl": "http://stills.cdn-surfline.com/a-test/small.jpg"
}
```

#### Endpoints

##### Create a Camera Recording - `POST /cameras/recording/id/:cameraId`

###### Example
Note: `recordingUrl` must be unique or POST request will fail.

```bash
POST /cameras/recording/a-test
{
    "alias": "a-test",
    "resolution": "1280x720",
    "thumbLargeUrl": "http://stills.cdn-surfline.com/a-test/large.jpg",
    "thumbSmallUrl": "http://stills.cdn-surfline.com/a-test/small.jpg",
    "recordingUrl": "http://stills.cdn-surfline.com/a-test/a-test.20180227T201716.mp4",
    "endDate": 1519860967455,
    "startDate": 1519860367460
}
```

##### Get Camera Recordings by Alias - `GET /cameras/recording/:alias?startDate=:startTimestampInMs&endDate=:endTimestampInMs`

Return an array of recordings for the specified camera and time range.

This returns only recordings that are completely within the time range specified. See `allowPartialMatch` param below to return recordings that partially match the given time range.

Also does not include expired recordings. See `includeExpired` param below to return all recordings that are expired or non-expired within the given time range.

###### Example

```bash
GET /cameras/recording/a-test?startDate=1534305911000&endDate=1534305981000

# =>

[
  {
    "cameraId": "5eda6788f5fc834ed3a00a77",
    "alias": "a-test",
    "endDate": "2018-02-28T23:36:07.455Z",
    "recordingUrl": "http://stills.cdn-surfline.com/a-test/a-test.20180227T201716.mp4",
    "startDate": "2018-02-28T23:26:07.455Z",
    "thumbLargeUrl": "http://stills.cdn-surfline.com/a-test/large.jpg",
    "thumbSmallUrl": "http://stills.cdn-surfline.com/a-test/small.jpg"
  }
]
```

##### Get Camera Recordings by Id - `GET /cameras/recording/id/:cameraId?startDate=:startTimestampInMs&endDate=:endTimestampInMs`

Return an array of recordings for the specified camera and time range.

This returns only recordings that are completely within the time range specified. See `allowPartialMatch` param below to return recordings that partially match the given time range.

Also doesn't include expired recordings. See `includeExpired` param below to return all recordings that are expired or non-expired within the given time range.

###### Example

```bash
GET /cameras/recording/id/5eda6788f5fc834ed3a00a77?startDate=1534305911000&endDate=1534305981000

# =>

[
  {
    "cameraId": "5eda6788f5fc834ed3a00a77",
    "alias": "a-test",
    "endDate": "2018-02-28T23:36:07.455Z",
    "recordingUrl": "http://stills.cdn-surfline.com/a-test/a-test.20180227T201716.mp4",
    "startDate": "2018-02-28T23:26:07.455Z",
    "thumbLargeUrl": "http://stills.cdn-surfline.com/a-test/large.jpg",
    "thumbSmallUrl": "http://stills.cdn-surfline.com/a-test/small.jpg"
  }
]
```

##### Get Camera Recordings with Partial Matches - `GET /cameras/recording/:alias?startDate=:startTimestampInMs&endDate=:endTimestampInMs&allowPartialMatch=true`

Return an array of recordings for the specified camera and time range.

With `allowPartialMatch=true`, this returns recordings that partially match the given time range.

Can also be done with camera id.

###### Example

```bash
GET /cameras/recording/bolsachicacam?startDate=1538696569000&endDate=1538696634000&allowPartialMatch=true
GET /cameras/recording/id/5eda6788f5fc834ed3a00a77?startDate=1538696569000&endDate=1538696634000&allowPartialMatch=true

# =>

[
  {
    "cameraId": "5eda6788f5fc834ed3a00a77",
    "alias": "bolsachicacam",
    "endDate": "2018-10-04T23:50:06.000Z",
    "recordingUrl": "https://camrewinds.cdn-surfline.com/live/bolsachicacam.20181004T234006.mp4",
    "startDate": "2018-10-04T23:40:06.000Z",
    "thumbLargeUrl": "https://camstills.cdn-surfline.com/bolsachicacam/2018/10/04/bolsachicacam.20181004T234006_full.jpg",
    "thumbSmallUrl": "https://camstills.cdn-surfline.com/bolsachicacam/2018/10/04/bolsachicacam.20181004T234006_small.jpg"
  }
]
```

##### Get Expired Camera Recordings - `GET /cameras/recording/:alias?startDate=:startTimestampInMs&endDate=:endTimestampInMs&includeExpired=true`

Return an array of expired and non-expired recordings for the specified camera and time range.

Expired recordings are the recordings that have been moved to Glacier in S3.

Can be used with `allowPartialMatch=true`, this returns recordings that partially match the given time range.

Can also be done with camera id.

###### Example

```bash
GET /cameras/recording/bolsachicacam?startDate=1538696569000&endDate=1538696634000&includeExpired=true
GET /cameras/recording/id/5eda6788f5fc834ed3a00a77?startDate=1538696569000&endDate=1538696634000&includeExpired=true

# =>

[
  {
    "cameraId": "5eda6788f5fc834ed3a00a77",
    "alias": "bolsachicacam",
    "endDate": "2018-10-04T23:50:06.000Z",
    "recordingUrl": "https://camrewinds.cdn-surfline.com/live/bolsachicacam.20181004T234006.mp4",
    "startDate": "2018-10-04T23:40:06.000Z",
    "thumbLargeUrl": "https://camstills.cdn-surfline.com/bolsachicacam/2018/10/04/bolsachicacam.20181004T234006_full.jpg",
    "thumbSmallUrl": "https://camstills.cdn-surfline.com/bolsachicacam/2018/10/04/bolsachicacam.20181004T234006_small.jpg"
  }
]
```

##### Get Legacy Camera Rewind Redirect - `GET /cameras/recording/redirect/:alias/:legacy_filename`

###### Example

```
GET /cameras/recording/redirect/:alias/:legacy_filename

# =>

301 Redirect

Moved Permanently. Redirecting to https://camrewinds.cdn-surfline.com/live/waikikibeachcam.20180425T163051.mp4
```

If the alias isn't found or no recent recordings exist, `404`

```
{
  "message": "Recording file not found."
}
```

### Camera Clips

#### Collection

```
CameraClip
```

#### Schema

```json
{
  "bucket": "sl-cam-clip-archive-prod",
  "cameraId": "583499c4e411dc743a5d5296",
  "clip": {
    "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4",
    "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4"
  },
  "clipId": "507f191e810c19729de860ea",
  "endTimestampInMs": 1534305611000,
  "startTimestampInMs": 1534305591000,
  "status": "CLIP_AVAILABLE",
  "thumbnail": {
    "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
    "sizes": [300, 640, 1500, 3000],
    "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png"
  }
}
```

#### Endpoints

##### Get Camera Clips - `GET /clips?ids=:id1,:id2,:id3,:id4,:id5,:id6,...`

Bulk get an array of edited cam rewind clips.

| Property | Type            | Required | Description                                                                                                                                                            |
| :------- | :-------------- | :------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `ids`    | `Array<String>` | Yes      | An array of clip IDs to retrieve                                                                                                                                       |
| `fields` | `Array<String>` | No       | An array of fields to include in the response. Possible values include all top-level properties in the Clip object below except for `clipId` which is always included. |

###### Response Status Codes

| Status Code        | Description                              |
| :----------------- | :--------------------------------------- |
| `200 OK`           | All clip records exist                   |
| `207 Multi-Status` | Some records exist, at least one doesn't |
| `404 Not Found`    | No records exist                         |

###### Example

```bash
GET /clips?ids=507f191e810c19729de860ea,5b5e2adbf265fd0019190576

# =>

[
  {
    "clipId": "507f191e810c19729de860ea",
    "cameraId": "583499c4e411dc743a5d5296",
    "startTimestampInMs": 1534305591000,
    "endTimestampInMs": 1534305611000,
    "status": "CLIP_AVAILABLE",
    "bucket": "sl-cam-clip-archive-prod",
    "clip": {
      "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4",
      "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4"
    },
    "thumbnail": {
      "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
      "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
      "sizes": [300, 640, 1500, 3000]
    }
  },
  {
    "clipId": "5b5e2adbf265fd0019190576",
    "cameraId": "583499c4e411dc743a5d5296",
    "startTimestampInMs": 1534305911000,
    "endTimestampInMs": 1534305981000,
    "status": "CLIP_AVAILABLE",
    "bucket": "sl-cam-clip-archive-prod",
    "clip": {
      "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/5b5e2adbf265fd0019190576.mp4",
      "key": "/583499c4e411dc743a5d5296/5b5e2adbf265fd0019190576.mp4"
    },
    "thumbnail": {
      "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/5b5e2adbf265fd0019190576_{size}.png",
      "key": "/583499c4e411dc743a5d5296/5b5e2adbf265fd0019190576{size}.png",
      "sizes": [300, 640, 1500, 3000]
    }
  }
]
```

###### Example - Fields

```bash
GET /clips?ids=507f191e810c19729de860ea,5b5e2adbf265fd0019190576&fields=status

# =>

[
  {
    "clipId": "507f191e810c19729de860ea",
    "status": "CLIP_AVAILABLE",
  },
  {
    "clipId": "5b5e2adbf265fd0019190576",
    "status": "CLIP_AVAILABLE",
  }
]
```

###### Example - Bad ID

```bash
GET /clips?ids=507f191e810c19729de860ea,5b5e2adbf265fd0019190576,1234567890&fields=status

# =>

[
  {
    "clipId": "507f191e810c19729de860ea",
    "status": "CLIP_AVAILABLE",
  },
  {
    "clipId": "5b5e2adbf265fd0019190576",
    "status": "CLIP_AVAILABLE",
  },
  {
    "clipId": "1234567890",
    "status": "CLIP_DOES_NOT_EXIST",
  }
]
```

##### Create Camera Clip - `POST /clips`

Creates a job to generate an edited cam rewind clip. This stores a record of the clip with status `CLIP_PENDING` in MongoDB and initiates the clip generation pipeline.

| Property             | Type      | Required | Description                                               |
| :------------------- | :-------- | :------- | :-------------------------------------------------------- |
| `cameraId`           | `String`  | Yes      | Camera ID to generate the clip for                        |
| `startTimestampInMs` | `Integer` | Yes      | Clip start time, UNIX timestamp to millisecond resolution |
| `endTimestampInMs`   | `Integer` | Yes      | Clip end time, UNIX timestamp to millisecond resolution   |

###### Response Status Codes

| Status Code    | Description                                                |
| :------------- | :--------------------------------------------------------- |
| `202 Accepted` | Clip record created and clip generation pipeline initiated |

###### Example

```bash
POST /clips
{
  "cameraId": "583499c4e411dc743a5d5296",
  "startTimestampInMs": 1534305911000,
  "endTimestampInMs": 1534305981000
}

# =>

{
  "cameraId": "583499c4e411dc743a5d5296",
  "clipId": "5ba9bc479a2e26650a31ebed",
  "endTimestampInMs": 1534305981000,
  "startTimestampInMs": 1534305911000,
  "status": "CLIP_PENDING",
  "thumbnail": {
    "sizes": []
  }
}
```

##### Update Camera Clip - `PUT /clips/:id`

Updates cam rewind clip MongoDB record with clip metadata.

| Property             | Type           | Required | Description                                                                                                                                 |
| :------------------- | :------------- | :------- | :------------------------------------------------------------------------------------------------------------------------------------------ |
| `clipId`             | `String`       | Yes      | Clip ID                                                                                                                                     |
| `cameraId`           | `String`       | Yes      | Camera ID to generate the clip for                                                                                                          |
| `startTimestampInMs` | `Integer`      | Yes      | Clip start time, UNIX timestamp to millisecond resolution                                                                                   |
| `endTimestampInMs`   | `Integer`      | Yes      | Clip end time, UNIX timestamp to millisecond resolution                                                                                     |
| `status`             | `Enum<String>` | Yes      | The status of the clip, see possible enum values below                                                                                      |
| `bucket`             | `String`       | No       | The S3 bucket that the clip and thumbnails are stored in                                                                                    |
| `clip`               | `Object`       | No       | Object containing information about the clip                                                                                                |
| `clip.key`           | `String`       | No       | The S3 object key of the clip                                                                                                               |
| `clip.url`           | `String`       | No       | The full URL to the clip                                                                                                                    |
| `thumbnail`          | `Object`       | No       | Object containing information about the thumbnails                                                                                          |
| `thumbnail.key`      | `String`       | No       | S3 object key of thumbnail file with {size} template param included. Replace {size} with one of the sizes in the array to access the image. |
| `thumbnail.url`      | `String`       | No       | URL of thumbnail file with {size} template param included. Replace {size} with one of the sizes in the array to access the image.           |
| `thumbnail.sizes`    | `Array`        | No       | List of sizes the thumbnail has available.                                                                                                  |

###### Clip Status Enum

| Enum Value            | Description                                                                                                                                                |
| :-------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `CLIP_AVAILABLE`      | Rewind available                                                                                                                                           |
| `CLIP_PENDING`        | Clip pending, spot with cam (Note: Since we don't create a resource on the clip generation request, this will almost never be valid here)                  |
| `CLIP_NOT_AVAILABLE`  | Clip not available, spot with cam but cam is down                                                                                                          |
| `CLIP_OUT_OF_RANGE`   | Clip not available, spot with cam but sessions were synced after cam rewind source files expired                                                           |
| `CLIP_DOES_NOT_EXIST` | No record of the clip exists in MongoDB                                                                                                                    |
| `FATAL_ERROR`         | There was an unrecoverable error during clip processing. (Note: This will only occur if the Lambda fails spectacularly and ends up in a dead letter queue) |

###### Response Status Codes

| Status Code | Description         |
| :---------- | :------------------ |
| `200 OK`    | Clip record updated |

###### Example

```bash
PUT /clips/507f191e810c19729de860ea
{
  "status": "CLIP_AVAILABLE",
  "bucket": "sl-cam-clip-archive-prod",
  "clip": {
    "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4",
    "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4"
  },
  "thumbnail": {
    "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
    "sizes": [
      300,
      640,
      1500,
      3000
    ],
    "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png"
  }
}

# =>

{
  "bucket": "sl-cam-clip-archive-prod",
  "cameraId": "583499c4e411dc743a5d5296",
  "clip": {
    "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4",
    "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4"
  },
  "clipId": "507f191e810c19729de860ea",
  "endTimestampInMs": 1534305611000,
  "startTimestampInMs": 1534305591000,
  "status": "CLIP_AVAILABLE",
  "thumbnail": {
    "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
    "sizes": [
      300,
      640,
      1500,
      3000
    ],
    "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png"
  }
}
```

### Camera Stream Authorization (Not Live)

This document the cam stream authorization workflow which exists within the camera service but is not live. This documentation is being maintained for now in case we revisit these authorization flows.

#### Authorization flow

[Inspired by this blog post](https://www.spacevatican.org/2015/5/1/using-cloudfront-signed-cookies/)

1. The cam playlist is `https://cam-cdn.surfline.com/cameras/authorization/{cam-alias}.m3u8`
2. Optionally validate the user’s identity and whether they should access the url specified
3. Generate cloudfront signing cookies and get playlist, re-writing playlist contents to refer to correct chunklist
4. The user's cam player reads playlist contents, stores the attached cookies, and requests chunklist for TS segments

```
+-------+               +-------------+                                       +-----------------+
| User1 |               | CloudFront  |                                       | CamerasService  |
+-------+               +-------------+                                       +-----------------+
    |                          |                                                       |
    | bolsachica.m3u8          |                                                       |
    |--------------------------------------------------------------------------------->|
    |                          |      -----------------------------------------------\ |
    |                          |      | Generates CloudFront signing cookies         |-|
    |                          |      |----------------------------------------------| |
    |                          |                                 Playlist with cookies |
    |                          |<------------------------------------------------------|
    |                          |                                                       |
    |                          | Playlist                                              |
    |                          |------------------------------------------------------>|
    |                          |-----------------------------------------------------\ |
    |                          || Rewrites playlist to use absolute url of chunklist |-|
    |                          ||----------------------------------------------------| |
    |                          |                                                       |
    |                          |                                      Playlist+cookies |
    |<---------------------------------------------------------------------------------|
    |                          |                                                       |
    | Chunklist+cookies        |                                                       |
    |------------------------->|                                                       |
    |  Video Segments Play     |                                                       |
    |<-------------------------|                                                       |
```

#### Authorize CloudFront camera streams

CameraModel's `streamUrl` is a synthetic field pointing to camera-service's authorization endpoint. That URL will redirect through the authorization flow, set cookies for cloudfront access, and redirect to the actual m3u8 URL (with cookies).

##### Store keys for environment

CloudFront cookie signing requires a private key (PEM) and the AWS private key ID. Store both in SSM's parameter store for secure access.

| AWS Account   | Parameter Name                      | Parameter Value                          |
| ------------- | ----------------------------------- | ---------------------------------------- |
| surfline-dev  | /development/cloudfront/key_private | Copy pasted surfline-dev PEM             |
| surfline-dev  | /development/cloudfront/key_id      | Copy pasted AWS surfline-dev keypair ID  |
| surfline-prod | /production/cloudfront/key_private  | Copy pasted surfline-prod PEM            |
| surfline-prod | /production/cloudfront/key_id       | Copy pasted AWS surfline-prod keypair ID |

###### Put parameter

`aws ssm put-parameter --type SecureString --name /development/cloudfront/key_private --region us-west-1 --overwrite --value "<PASTE PEM HERE>"`

###### Get parameter

`aws ssm get-parameter --name /development/cloudfront/key_private --region us-west-1 --with-decryption`

#### Gotchas

- CloudFront key pair modification are only accessible through the AWS root account owner. In this case, all AWS accounts are owned by Tolga (Sturdy). Engage with Sturdy for CloudFront key pair modifications. Current legacy/dev/production CloudFront keypairs+ids are all stored in ParameterStore: `/{development|production}/cloudfront/key_*`.

## Service Validation


### Manual Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `cameras-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd6TVRrNE5EYzJPQSJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ek1UazRORGMyT0EiLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests on the endpoints detailed in the [Endpoints](https://github.com/Surfline/wavetrak-services/tree/INF-429_Add-validation-criteria-to-READMES/services/cameras-service#endpoints-2) section (substituting parameters as needed).

**Ex:**

Test `GET` requests against the `/clips` endpoint:
```
curl \
    -X GET \
    -i \
    http://cameras-service.sandbox.surfline.com/clips/?ids=5c1aa5b77051a038a93acdca
```
**Expected response:** See [above](#example-16) for sample response.

### Scripted Validation

A detailed smoke test is stored in the `test` directory, and should be used to validate `GET`, `POST`, `PUT` and `DELETE` actions on a number of service endpoints. To run the test, you should first set an environment variable called `SERVICE_URL` in your shell, pointing to the appropriate test environment / service endpoint. For example:
```
export SERVICE_URL='http://cameras-service.sandbox.surfline.com'
```

The test can be invoked from the service's root directory via the `test:smoke` target in the `package.json`:
```
npm run-script test:smoke
```
