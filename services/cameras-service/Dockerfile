FROM node:12 as base
WORKDIR /opt/app
COPY .npmrc .
COPY package.json .
# COPY yarn.lock .
RUN yarn install
RUN touch .env

FROM base AS test
WORKDIR /opt/app
COPY . /opt/app
RUN yarn run lint && \
    yarn run test

FROM test AS build
WORKDIR /opt/app
ARG APP_ENV=sandbox
ARG APP_VERSION=master
ENV NODE_ENV=$APP_ENV
ENV APP_VERSION=$APP_VERSION
RUN yarn run dist && \
    yarn install --production --ignore-scripts --prefer-offline

FROM node:12-alpine
WORKDIR /opt/app
COPY --from=build /opt/app/dist dist
COPY --from=build /opt/app/node_modules /opt/app/node_modules

# execute yarn start equivalent. Prevent extra process calls
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#cmd
CMD ["node", "dist/index.js"]
