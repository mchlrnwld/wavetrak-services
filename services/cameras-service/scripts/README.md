# Update Camera Recordings w/ CameraId Script
Script will update all documents in the mongo collection with the correct cameraId.
Once completed script will save off a file named `bad_aliases.txt`, these are the
aliases that don't have an existing camera in the Cameras collection in Mongo.

## Setup
Script uses Python 3 to run the script, and requires you to `pip install pymongo`,
and `pip install requests`

## To Run
+ Add your MongoDB connection string to line 5.
+ Then run: `python update_camera_recordings_with_cameraId.py`
