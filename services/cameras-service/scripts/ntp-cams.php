<?php

// This script updates all SL camera network cams with correct NTP.
// This is a initial version I´m sure it can be fine-tuned as needed and part of CRUD operations in imap_fetchstructure

// Curl PHP library
require_once 'php-rest-curl/rest.inc.php';

// function get NTP info
function AXISApi($userpass, $pubip, $APIcommand, $action)
{
  $url =
    "http://" .
    $pubip .
    "/axis-cgi/param.cgi?action=" .
    $action .
    "&" .
    $APIcommand;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
  curl_setopt($ch, CURLOPT_USERPWD, "$userpass");
  curl_setopt($ch, CURLOPT_TIMEOUT, "10");

  $response = curl_exec($ch);
  //echo $response;
  $err = curl_error($ch);
  curl_close($ch);

  if ($err) {
    echo "cURL Error #:" . $err . "\n";
  } else {
    echo $response . "\n";
  }
}
// end function

// function get NTP info
function DahuaAPi($userpass, $pubip, $APIcommand)
{
  $url =
    "http://" . $pubip . "/cgi-bin/configManager.cgi?action=" . $APIcommand;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
  curl_setopt($ch, CURLOPT_USERPWD, "$userpass");
  curl_setopt($ch, CURLOPT_TIMEOUT, "5");

  $response = curl_exec($ch);
  //echo $response;
  $err = curl_error($ch);

  curl_close($ch);

  if ($err) {
    echo "cURL Error #:" . $err . "\n";
  } else {
    $response = str_replace(array("\n", "\r", "\t"), '', $response);
    $array = explode("table.NTP.", $response);
    echo $array[1] . " " . $array[2] . " " . $array[4] . "\n";
  }
}
// end function

// function get NTP info
function DahuaAPiPost($userpass, $pubip, $APIcommand)
{
  //	$user = "admin";
  //	$pass = "Bolt300$";
  //	$encodeUserPass = base64_encode($user.":".$pass);
  $url =
    "http://" . $pubip . "/cgi-bin/configManager.cgi?action=" . $APIcommand;
  //$dahuaAPIcommand = "getConfig&name=NTP";
  //	$typerequest = "GET";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
  curl_setopt($ch, CURLOPT_USERPWD, "$userpass");
  curl_setopt($ch, CURLOPT_TIMEOUT, "5");
  curl_setopt($ch, CURLOPT_POST, true);

  $response = curl_exec($ch);
  //echo $response;
  $err = curl_error($ch);

  curl_close($ch);

  if ($err) {
    echo "cURL Error #:" . $err . "\n";
  } else {
    echo "OK ";
  }
}

// Call Camera Service - collect all our camera id and alias and title from our CMS - source of true)

$urlCameras = 'http://cameras-service.prod.surfline.com/cameras/';

$res = RestCurl::get($urlCameras);

$cameraservice_alias = array();
$cameraservice_id = array();

// Add all info in a camera-service array
foreach ($res['data'] as $value) {
  array_push($cameraservice_alias, array(
    $value->_id,
    $value->alias,
    $value->title
  ));
  //echo "name: ".$value->_id." alias: ". $value->alias. " title: ".$value->title." \n";
  array_push($cameraservice_id, $value->_id);
}

// Call AIRTABLE - collect all other relevant location data from our Airtable info (camera brand, public IP, user and pass)
$airtableApp = 'appSqnLyp42YxeiD5';
$airtableApiKey = 'keyh615nsHoeAbRt1';

function getAirtableData($airtableOffset)
{
  global $airtableOffset;
  global $airtableArrayNames;
  $url =
    'https://api.airtable.com/v0/appSqnLyp42YxeiD5/Data?api_key=keyh615nsHoeAbRt1&offset=' .
    $airtableOffset .
    '';

  //echo $url."\n";
  $cURL = curl_init();

  curl_setopt($cURL, CURLOPT_URL, $url);
  curl_setopt($cURL, CURLOPT_HTTPGET, true);

  curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Accept: application/json'
  ));
  curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);

  $airtableArray = curl_exec($cURL);
  curl_close($cURL);
  $airtableArray = json_decode($airtableArray, true);
  $airtableOffset = $airtableArray['offset'];
  //echo "offset: ".$airtableOffset. "\n";

  /// STAR UPDATING THE NTPStart
  foreach ($airtableArray['records'] as $values) {
    array_push($airtableArrayNames, array(
      $values['id'],
      $values['fields']['id'],
      $values['fields']['name'],
      $values['fields']['alias'],
      $values['fields']['ownership'],
      $values['fields']['cam_brand'],
      $values['fields']['cam_userpass'],
      $values['fields']['cam_pubIP'],
      $values['fields']['quick_cam']
    ));
    //echo $values['id']." ".$values['fields']['id']." ".$values['fields']['name']." ".$values['fields']['brand']." ".$values['fields']['alias'];
    if (
      $values['fields']['cam_userpass'] == "n/a" ||
      $values['fields']['ownership'] == "shared"
    ) {
      //echo $values['fields']['id']." ".$values['fields']['name']." ".$values['fields']['alias']." - ".$values['fields']['ownership']."\n";
      // For Dahua cameras only in US for now!
    } elseif (
      $values['fields']['cam_pubIP'] !== "" &&
      $values['fields']['cam_userpass'] !== "n/a" &&
      $values['fields']['cam_brand'] == "Dahua"
    ) {
      //echo $values['fields']['id']." ".$values['fields']['name']." ".$values['fields']['alias'].$values['fields']['cam_pubIP']."\n";
      echo $values['fields']['alias'] .
        " " .
        $values['fields']['cam_userpass'] .
        " " .
        $values['fields']['cam_pubIP'] .
        " " .
        $values['fields']['cam_brand'] .
        "\n";

      // update WC, EC, HI

      if (strpos($values['fields']['alias'], 'wc-') !== false) {
        // WC  California (-8 PST (have daylight Savings))
        //https://www.timeanddate.com/time/zone/usa/san-diego
        DahuaAPiPost(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "setConfig&NTP.Enable=true&NTP.Address=us.pool.ntp.org&NTP.TimeZone=28&NTP.UpdatePeriod=10"
        );
        DahuaAPiPost(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "setConfig&Locales.DSTEnable=true&Locales.DSTStart.Week=0&Locales.DSTStart.Day=11&Locales.DSTStart.Month=3&Locales.DSTStart.Hour=2&Locales.DSTEnd.Week=0&Locales.DSTEnd.Day=4&Locales.DSTEnd.Month=11&Locales.DSTEnd.Hour=2"
        );
        DahuaAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "getConfig&name=NTP"
        );
      } elseif (strpos($values['fields']['alias'], 'hi-') !== false) {
        // HI  Hawaii ( -10 HST (no daylight savings))
        //https://www.timeanddate.com/time/zone/usa/honolulu
        DahuaAPiPost(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "setConfig&NTP.Enable=true&NTP.Address=us.pool.ntp.org&NTP.TimeZone=30&NTP.UpdatePeriod=10"
        );
        DahuaAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "getConfig&name=NTP"
        );
      } elseif (strpos($values['fields']['alias'], 'ec-') !== false) {
        // EC  New Your ( -5 HST (no daylight savings))
        //https://www.timeanddate.com/time/zone/usa/new-york
        DahuaAPiPost(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "setConfig&NTP.Enable=true&NTP.Address=us.pool.ntp.org&NTP.TimeZone=25&NTP.UpdatePeriod=10"
        );
        DahuaAPiPost(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "setConfig&Locales.DSTEnable=true&Locales.DSTStart.Week=0&Locales.DSTStart.Day=11&Locales.DSTStart.Month=3&Locales.DSTStart.Hour=2&Locales.DSTEnd.Week=0&Locales.DSTEnd.Day=4&Locales.DSTEnd.Month=11&Locales.DSTEnd.Hour=2"
        );
        DahuaAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "getConfig&name=NTP"
        );
      } else {
        echo " not US!  \n";
        //DahuaAPi ($values['fields']['cam_userpass'],$values['fields']['cam_pubIP'],"getConfig&name=NTP");
      }

      // For Axis cameras only in US for now!
    } elseif (
      $values['fields']['cam_pubIP'] !== "" &&
      $values['fields']['cam_userpass'] !== "n/a" &&
      $values['fields']['cam_brand'] == "Axis"
    ) {
      //echo $values['fields']['id']." ".$values['fields']['name']." ".$values['fields']['alias'].$values['fields']['cam_pubIP']."\n";
      echo $values['fields']['alias'] .
        " " .
        $values['fields']['cam_userpass'] .
        " " .
        $values['fields']['cam_pubIP'] .
        " " .
        $values['fields']['cam_brand'] .
        "\n";

      // update WC, EC, HI

      if (strpos($values['fields']['alias'], 'wc-') !== false) {
        // WC  California (-8 PST (have daylight Savings))
        //https://www.timeanddate.com/time/zone/usa/san-diego
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "Time.ObtainFromDHCP=no&Time.POSIXTimeZone=PST8PDT,M3.2.0,M11.1.0&Time.SyncSource=NTP&Time.DST.Enabled=yes&Time.NTP.Server=us.pool.ntp.org",
          "update"
        );
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "group=root.Time.ServerTime",
          "list"
        );
      } elseif (strpos($values['fields']['alias'], 'hi-') !== false) {
        // HI  Hawaii ( -10 HST (no daylight savings))
        //https://www.timeanddate.com/time/zone/usa/honolulu
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "Time.ObtainFromDHCP=no&Time.POSIXTimeZone=HAST10HADT,M3.2.0,M11.1.0&Time.SyncSource=NTP&Time.DST.Enabled=no&Time.NTP.Server=us.pool.ntp.org",
          "update"
        );
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "group=root.Time.ServerTime",
          "list"
        );
      } elseif (strpos($values['fields']['alias'], 'ec-') !== false) {
        // EC  New Your ( -5 HST (no daylight savings))
        //https://www.timeanddate.com/time/zone/usa/new-york
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "Time.ObtainFromDHCP=no&Time.POSIXTimeZone=EST5EDT,M3.2.0,M11.1.0&Time.SyncSource=NTP&Time.DST.Enabled=yes&Time.NTP.Server=us.pool.ntp.org",
          "update"
        );
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "group=root.Time.ServerTime",
          "list"
        );
      } else {
        echo " not US!  \n";
        AxisAPi(
          $values['fields']['cam_userpass'],
          $values['fields']['cam_pubIP'],
          "group=root.Time.ServerTime",
          "list"
        );
      }
    } else {
      //	echo "pubIP: ".$values['fields']['pubIP']."\n";
      //	echo $values['fields']['id']." ".$values['fields']['name']." ".$values['fields']['alias'];
      //	echo $values['fields']['cam_brand']." ".$values['fields']['cam_userpass']." ".$values['fields']['pubIP']." ".$values['fields']['quick_cam']."\n";
    }
  }
}

$airtableArrayNames = array();
$airtableOffset = '';
getAirtableData($airtableOffset);

while (!empty($airtableOffset)) {
  getAirtableData($airtableOffset);
  //echo $airtableOffset."\n \n";
}

echo "Count Camera Service: " . count($cameraservice_alias);
echo "\n";
echo "Count Airtable Entries: " . count($airtableArrayNames);
echo "\n";
/// End

// extra code for other debug proposes
foreach ($cameraservice_alias as $value) {
  //var_dump($value);
  $keycomponent = array_search($value[0], array_column($airtableArrayNames, 1));
  if (is_bool($keycomponent)) {
    //echo "id: ".$value[0]." alias: ". $value[1]. " title: ".$value[2]." \n";
    //echo $keycomponent;
  } else {
    if (
      strcmp($value[1], substr($airtableArrayNames[$keycomponent][3], 3)) > 0
    ) {
      //echo "Mismatch CS-Airtable - ".$value[1]."-".$value[2];
      //echo "AIRTABLE - ".$airtableArrayNames[$keycomponent][0]." ".$airtableArrayNames[$keycomponent][1]." ".$airtableArrayNames[$keycomponent][2]." ".$airtableArrayNames[$keycomponent][3]."\n";
    }
  }
}

echo "Done!";
?>
