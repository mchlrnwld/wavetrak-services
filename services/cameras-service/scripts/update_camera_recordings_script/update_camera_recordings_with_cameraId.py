from pymongo import MongoClient
import requests
import time

CONNECTION_STR = '' # add mongo string


def get_camera_id_from_alias(alias):
    response = requests.get(f"http://cameras-service.prod.surfline.com/cameras/ValidateAlias?alias={alias}")

    if response.status_code == 200:
        camera_id = response.json()['_id']
        return camera_id

    print(f"Camera doesn't exists for alias: {alias}")
    return None


def map_aliases(rewinds_collection):
    mapped_aliases = []

    distinct_aliases = rewinds_collection.distinct('alias')
    for alias in distinct_aliases:
        alias_dict = {'alias': alias, 'cameraId': get_camera_id_from_alias(alias)}
        mapped_aliases.append(alias_dict)

    return mapped_aliases


def update_documents(rewinds_collection, mapped_aliases):
    for alias_dict in mapped_aliases:
        if alias_dict['cameraId']:
            print(f"Updating documents with alias: {alias_dict['alias']}, and added cameraId: {alias_dict['cameraId']}")
            rewinds_collection.update_many({"alias": alias_dict['alias']}, {"$set": {"cameraId": alias_dict['cameraId']}})
        else:
            print(f"Unable to update documents with alias: {alias_dict['alias']}")
            with open('bad_aliases.txt', 'a') as output_file:
                output_file.write(f"{alias_dict['alias']}\n")


def main():
    client = MongoClient(CONNECTION_STR)
    rewinds_collection = client.KBYG.CameraRecording

    start_time = time.time()

    mapped_aliases = map_aliases(rewinds_collection)
    update_documents(rewinds_collection, mapped_aliases)

    print(f"Run time: {time.time() - start_time}")

    print("Finished updating Mongo Docs.")


if __name__ == '__main__':
    main()
