# -*- coding: utf-8 -*-
""" Root service handler module for AWS Lambda function. 'METHOD_HANDLERS' """

import json
import logging

from lib import utils
from lib.config import CLIP_DOWNLOAD_DEST, EXECUTION_ENVIRONMENT, LOG_LEVEL
from lib.handlers import clip_generator_trigger

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)


def handler(event, context):

    logger.info(
        'Event: {0}'.format(json.dumps(event, indent=4, sort_keys=True))
    )

    # run a resources report before and after processing
    utils.run_resources_report()

    # clear temp disk space before processing
    if EXECUTION_ENVIRONMENT == 'lambda':
        utils.remove_dir_files(CLIP_DOWNLOAD_DEST)

    # catch errors, clear disk space, and rethrow error
    try:
        response = clip_generator_trigger.handler(event, context)
    finally:
        if EXECUTION_ENVIRONMENT == 'lambda':
            utils.remove_dir_files(CLIP_DOWNLOAD_DEST)

    # run a resources report before and after processing
    utils.run_resources_report()

    return response
