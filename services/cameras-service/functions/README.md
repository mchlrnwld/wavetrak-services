# Cameras Service Functions

* **[Rewind Clip Generator](./rewind-clip-generator)**: Add the ability to get cam rewinds by two NTP UTC timestamps.
* **[Cam Embed Generator](./embed-generator)**: Generate camera embed files in S3/Cloudfront. Auto-update files when cam settings change.
