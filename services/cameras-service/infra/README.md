# cameras-service Infrastructure

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Naming Conventions](#naming-conventions)
- [Terraform](#terraform)
  - [Using Terraform](#using-terraform)
  - [Using the Terraform New Relic Provider](#using-the-terraform-new-relic-provider)
    - [New Relic credentials](#new-relic-credentials)
    - [Testing New Relic Alerts in Lower Tiers](#testing-new-relic-alerts-in-lower-tiers)
    - [Configuring Alert Thresholds](#configuring-alert-thresholds)
- [Overview](#overview)
- [Wowza infrastructure](#wowza-infrastructure)
  - [Recorder defaults](#recorder-defaults)
- [Thumbnail generation](#thumbnail-generation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Naming Conventions

See <https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions>.

## Terraform

> :warning: Note: New Relic API credentials are required to invoke the terraform modules in this project. See the [New Relic Credentials](#new-relic-credentials) section below for more details.

_Uses terraform version `0.12.28`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement its respective module. See <https://www.terraform.io/docs/modules/usage.html> for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments are `sbox`, `staging`, `prod`.

### Using the Terraform New Relic Provider

This terraform project uses the New Relic provider to automatically manage alerts as part of the infrastructure code. These alerts can optionally be toggled on and off by the variable `newrelic_{alert-type}_create_alert`.

Each New Relic alert type is managed and configured by a corresponding terraform resource. The three alert types that we configure include apdex, error rate, and NRQL-based alerts. 

#### New Relic credentials

New Relic credentials are passed to terraform via shell environment variables. **You will need to export two variables to invoke the terraform module successfully**: `NEW_RELIC_ACCOUNT_ID` and `NEW_RELIC_API_KEY`. See the [styleguide](https://github.com/Surfline/styleguide/blob/master/terraform/README.md#new-relic-credentials) for information on how to obtain these values.

Once you have obtained these values, you must export them to the shell where you'll invoke terraform:
```
export NEW_RELIC_ACCOUNT_ID=<account ID>
export NEW_RELIC_API_KEY="<your New Relic Personal API key>"
```

#### Testing New Relic Alerts in Lower Tiers

There are two New Relic accounts at Wavetrak: `prod` uses the standard New Relic account, and `sbox` & `staging` use the New Relic Lite account. Authentication for both of these accounts is handled via OneLogin. (Contact a system administrator if you need access to one or both of them).

By default, the alerts are toggled on in `prod`, but off in the lower tiers. If you want to test the alerts in a lower tier, you'll need to do the following:

1. In the environment's `main.tf` file, configure the basic alert values. Most of the configuration options are sanely defaulted in the respective `wavetrak-infrastructure` module. However, there are a few configuration options you'll need to explicitly set. See the [prod terraform configuration](https://github.com/Surfline/wavetrak-services/blob/master/services/cameras-service/infra/environments/prod/main.tf) for an example of which variables you must configure. (The New Relic variables in that file are all prepended with the string `newrelic_`.)
2. 	Export the `NEW_RELIC_ACCOUNT_ID` & `NEW_RELIC_API_KEY` that correspond to the New Relic Lite account. See the [styleguide](https://github.com/Surfline/styleguide/blob/master/terraform/README.md#new-relic-credentials) for more information on how to do this.
3. Ensure that `APM` is set up for the service in New Relic Lite. You can check this by going to the `APM` section in New Relic Lite and searching for the service. (Ex: for the Cameras Service, you should see `Surfline Services - Cameras (sandbox)`). If APM is not configured, set the service's `NEWRELIC_ENABLED` value to `true` in AWS Parameter Store, then redeploy the service in Jenkins. (For more information on service-specific environment variables, see [Application Secret Management with Parameter Store](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1066338448/Application+Secret+Management+with+Parameter+Store).
4. Ensure that the policy you've named in the `newrelic_alert_policy` variable exists. If it doesn't, you can manually create it in the New Relic Lite dashboard by going to `Alerts & AI > Policies > New alert policy`.

#### Configuring Alert Thresholds

Alerting thresholds should be selected carefully for each service / lambda function. Use your best judgement based on past error patterns, the service's fault-tolerance, etc. See the styleguide's [discussion about New Relic](https://github.com/Surfline/styleguide/blob/master/terraform/README.md#configuring-new-relic-alerts) for more information.

## Overview

This infrastructure stack comprises the following architecture (highlighted in blue):

![Camera Service Architecture](docs/images/cam-service-architecture.png 'Camera Service Architecture')

1. Sessions API makes request against Cameras Service to generate a cam clip
1. Cameras Service puts clip request message on an SQS queue and returns a job ID to the Sessions API
1. Lambda function to create cam clip is triggered by SQS queue
1. Lambda gets a list of cam rewind source video files within specified time range from Cameras Service
1. Lambda gets cam rewind source video files from sl-live-cam-archive-prod S3 bucket
1. Lambda uses ffmpeg to trim/merge video files to generate a clip and extract thumbnail images from the middle of the generated clip
1. Lambda pushes generated clip and thumbnails to sl-cam-clip-archive-prod S3 bucket
1. Lambda sends generated clip metadata to Cameras Service
1. Cameras Service stores generated clip metadata in MongoDB
1. Lambda publishes message to SNS topic containing generated clip metadata
1. Sessions API receives message published to SNS topic and updates session data store with generated clip metadata

## Wowza infrastructure

CloudFormation templates

- Encoder `wavetrak-infrastructure/cloudformation/surfline-wowza-encoder.json`
- Recorder `wavetrak-infrastructure/cloudformation/surfline-wowza-recorder.json`

Smallest ec2 instance with ssd: `m3.medium`

EBS can resize live, but higher cost-per-IOPS and higher latency
Instance storage has better IOPS, but fixed size

### Recorder defaults

| Name                          | Value               |
| ----------------------------- | ------------------- |
| AlertSubscriptionEmail        | camera@surfline.com |
| AmiId                         | ami-d1b9f5b1        |
| ApplicationName               | wowza               |
| CompanyName                   | sl                  |
| EnvironmentName               | prod                |
| InstanceType                  | r3.xlarge           |
| InternalServerSecurityGroupId | sg-6d0d070f         |
| KeyName                       | surfline-wowza-wc   |
| PubSubnet1                    | subnet-68a1af0a     |
| PubSubnet2                    | subnet-0e226848     |
| RootVolumeSize                | 60                  |
| ServerCount                   | 1                   |
| ServiceName                   | wowza-recorder      |
| ServiceRegion                 | wc                  |
| VpcId                         | vpc-29f3f241        |

## Thumbnail generation

We have a lambda that periodically generates thumbnails: `wavetrak-infrastructure/terraform/cam-thumbnail-generation/README.md`

This should move to either an s3 bucket triggered lambda or happen on recording's first disk write
