provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/cameras-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}
data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/staging/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/staging/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

locals {
  company     = "sl"
  application = "cameras-service"
  environment = "staging"

  artifacts_bucket  = "sl-artifacts-staging"
  internal_sg_group = "sg-90aeaef5"
  instance_subnets  = ["subnet-0909466c", "subnet-f2d458ab"]
  sg_all_servers    = "sg-91aeaef4"
  web_url           = "https://staging.surfline.com"

  default_vpc = "vpc-981887fd"
  dns_name    = "${local.application}.${local.environment}.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  alb_listener_arn                  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  cam_details_updated_sns_topic_arn = "arn:aws:sns:us-west-1:665294954271:camera_details_updated_staging"
  iam_role_arn                      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn                 = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"

  clips_cdn_acm_domains = ["staging.surfline.com"]
  clips_cdn_fqdn = {
    "staging.surfline.com" = ["camclips.staging.surfline.com"]
  }
  clips_cdn_default_ttl       = 3600
  clips_cdn_max_ttl           = 3600
  clips_cdn_whitelist_headers = ["Origin"]

  embed_cdn_acm_domains = ["staging.surfline.com"]
  embed_cdn_fqdn = {
    "staging.surfline.com" = ["embed.staging.surfline.com"]
  }
  embed_cdn_default_ttl       = 3600
  embed_cdn_max_ttl           = 3600
  embed_cdn_whitelist_headers = ["Origin"]

  prerecorded_clips_cdn_acm_domains = ["staging.surfline.com"]
  prerecorded_clips_cdn_fqdn = {
    "staging.surfline.com" = ["prerecorded-clips.staging.surfline.com"]
  }
  prerecorded_clips_cdn_default_ttl       = 3600
  prerecorded_clips_cdn_max_ttl           = 3600
  prerecorded_clips_cdn_whitelist_headers = ["Origin"]
}

module "cameras_service" {
  source = "../../modules/cameras-service"

  company     = local.company
  environment = local.environment
  application = local.application

  default_vpc = local.default_vpc
  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id
  ecs_cluster = "sl-core-svc-${local.environment}"

  service_td_count = 1
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = local.alb_listener_arn
  iam_role_arn      = local.iam_role_arn
  load_balancer_arn = local.load_balancer_arn

  auto_scaling_enabled = false

  ssm_parameter_name = "arn:aws:ssm:us-west-1:665294954271:parameter/development/cloudfront/*"
  kms_key            = "arn:aws:kms:us-west-1:665294954271:key/69161481-b5fc-4ea0-9a6e-5fc874c38ffb"

  # New Relic apdex alert configuration
  newrelic_apdex_create_alert = false

  # New Relic error rate alert configuration
  newrelic_error_rate_create_alert = false
}

module "rewind_clips" {
  source = "../../modules/rewind-clips"

  application = local.application
  company     = local.company
  environment = local.environment

  function_name = "generate-clip"

  timeout             = 900

  clips_cdn_acm_domains        = local.clips_cdn_acm_domains
  clips_cdn_fqdn               = local.clips_cdn_fqdn
  clips_cdn_default_ttl        = local.clips_cdn_default_ttl
  clips_cdn_max_ttl            = local.clips_cdn_max_ttl
  clips_cdn_whitelist_headers  = local.clips_cdn_whitelist_headers
}

# Allow cameras service to put messages on SQS queue
resource "aws_iam_role_policy_attachment" "cameras_service_rewind_clips_sqs_send" {
  role       = module.cameras_service.camera_service_task_role_name
  policy_arn = module.rewind_clips.queue_policy
}

module "analytics-firehose" {
  source = "../../modules/analytics-firehose"

  environment         = local.environment
  producer_role_name  = "cameras_service_task_role_${local.environment}"
  analytics_retention = 30
}

module "embed_generator" {
  source = "../../modules/embed-generator"

  application = local.application
  company     = local.company
  environment = local.environment

  function_name       = "embed-generator"
  timeout             = 60

  web_url           = local.web_url

  embed_cdn_acm_domains       = local.embed_cdn_acm_domains
  embed_cdn_fqdn              = local.embed_cdn_fqdn
  embed_cdn_default_ttl       = local.embed_cdn_default_ttl
  embed_cdn_max_ttl           = local.embed_cdn_max_ttl
  embed_cdn_whitelist_headers = local.embed_cdn_whitelist_headers
}

module "prerecorded_clips" {
  source = "../../modules/prerecorded-clips"

  application = local.application
  company     = local.company
  environment = local.environment

  prerecorded_clips_cdn_acm_domains       = local.prerecorded_clips_cdn_acm_domains
  prerecorded_clips_cdn_fqdn              = local.prerecorded_clips_cdn_fqdn
  prerecorded_clips_cdn_default_ttl       = local.prerecorded_clips_cdn_default_ttl
  prerecorded_clips_cdn_max_ttl           = local.prerecorded_clips_cdn_max_ttl
  prerecorded_clips_cdn_whitelist_headers = local.prerecorded_clips_cdn_whitelist_headers
}
