provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/cameras-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

locals {
  company     = "sl"
  application = "cameras-service"
  environment = "prod"

  artifacts_bucket  = "sl-artifacts-prod"
  internal_sg_group = "sg-a5a0e5c0"
  instance_subnets  = ["subnet-d1bb6988", "subnet-85ab36e0"]
  sg_all_servers    = "sg-a4a0e5c1"
  web_url           = "https://www.surfline.com"

  default_vpc = "vpc-116fdb74"
  dns_name    = "${local.application}.${local.environment}.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  alb_listener_arn                  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  cam_details_updated_sns_topic_arn = "arn:aws:sns:us-west-1:833713747344:camera_details_updated_prod"
  iam_role_arn                      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn                 = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"

  clips_cdn_acm_domains = ["cdn-surfline.com"]
  clips_cdn_fqdn = {
    "cdn-surfline.com" = ["camclips.cdn-surfline.com"]
  }
  clips_cdn_default_ttl       = 3600
  clips_cdn_max_ttl           = 3600
  clips_cdn_whitelist_headers = ["Origin"]

  rewind_source_s3_bucket_name = "sl-live-cam-archive-prod"
  rewind_source_cross_account_roles = [
    "arn:aws:iam::665294954271:role/cameras_service_task_role_sandbox",
    "arn:aws:iam::665294954271:role/cameras_service_task_role_staging",
  ]

  embed_cdn_acm_domains = ["cdn-surfline.com"]
  embed_cdn_fqdn = {
    "cdn-surfline.com" = ["embed.cdn-surfline.com"]
  }
  embed_cdn_default_ttl       = 3600
  embed_cdn_max_ttl           = 3600
  embed_cdn_whitelist_headers = ["Origin"]

  prerecorded_clips_cdn_acm_domains = ["cdn-surfline.com"]
  prerecorded_clips_cdn_fqdn = {
    "cdn-surfline.com" = ["prerecorded-clips.cdn-surfline.com"]
  }
  prerecorded_clips_cdn_default_ttl       = 3600
  prerecorded_clips_cdn_max_ttl           = 3600
  prerecorded_clips_cdn_whitelist_headers = ["Origin"]
}

module "cameras_service" {
  source = "../../modules/cameras-service"

  company     = local.company
  environment = local.environment
  application = local.application

  default_vpc = local.default_vpc
  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id
  ecs_cluster = "sl-core-svc-${local.environment}"

  service_td_count = 3
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = local.alb_listener_arn
  iam_role_arn      = local.iam_role_arn
  load_balancer_arn = local.load_balancer_arn

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = "617"
  auto_scaling_min_size     = "2"
  auto_scaling_max_size     = "5000"

  ssm_parameter_name = "arn:aws:ssm:us-west-1:833713747344:parameter/legacy/cloudfront/*"
  kms_key            = "arn:aws:kms:us-west-1:833713747344:key/33cf824d-3a14-43b8-9c94-2d46adfcaf31"

  # New Relic apdex alert configuration
  newrelic_apdex_create_alert = true

  # Add custom runbook link via newrelic_lambda_nrql_runbook_url variable if one is available; otherwise use default
  newrelic_apdex_alert_threshold   = ".70"
  newrelic_apdex_warning_threshold = ".85"

  # New Relic error rate alert configuration
  newrelic_error_rate_create_alert = true

  # Add custom runbook link via newrelic_lambda_nrql_runbook_url variable if one is available; otherwise use default
  newrelic_error_rate_alert_threshold   = ".05"
  newrelic_error_rate_warning_threshold = ".03"
}

module "rewind_clips" {
  source = "../../modules/rewind-clips"

  application = local.application
  company     = local.company
  environment = local.environment

  function_name = "generate-clip"

  timeout = 900

  clips_cdn_acm_domains        = local.clips_cdn_acm_domains
  clips_cdn_fqdn               = local.clips_cdn_fqdn
  clips_cdn_default_ttl        = local.clips_cdn_default_ttl
  clips_cdn_max_ttl            = local.clips_cdn_max_ttl
  clips_cdn_whitelist_headers  = local.clips_cdn_whitelist_headers
}

# Allow cameras service to put messages on SQS queue
resource "aws_iam_role_policy_attachment" "cameras_service_rewind_clips_sqs_send" {
  role       = module.cameras_service.camera_service_task_role_name
  policy_arn = module.rewind_clips.queue_policy
}

# Allow cross-account roles to access rewind S3 bucket
resource "aws_s3_bucket_policy" "cross_account_s3_read_policy" {
  bucket = local.rewind_source_s3_bucket_name
  policy = templatefile("${path.module}/resources/cross-account-s3-read-policy.json", {
    cross_account_roles = join(",", formatlist("\"%s\"", local.rewind_source_cross_account_roles))
    s3_bucket_name      = local.rewind_source_s3_bucket_name
  })
}

module "analytics-firehose" {
  source = "../../modules/analytics-firehose"

  environment         = local.environment
  producer_role_name  = "cameras_service_task_role_${local.environment}"
  analytics_retention = 180
}

module "embed_generator" {
  source = "../../modules/embed-generator"

  application = local.application
  company     = local.company
  environment = local.environment

  function_name       = "embed-generator"
  timeout             = 60

  web_url           = local.web_url

  embed_cdn_acm_domains       = local.embed_cdn_acm_domains
  embed_cdn_fqdn              = local.embed_cdn_fqdn
  embed_cdn_default_ttl       = local.embed_cdn_default_ttl
  embed_cdn_max_ttl           = local.embed_cdn_max_ttl
  embed_cdn_whitelist_headers = local.embed_cdn_whitelist_headers
}

module "prerecorded_clips" {
  source = "../../modules/prerecorded-clips"

  application = local.application
  company     = local.company
  environment = local.environment

  prerecorded_clips_cdn_acm_domains       = local.prerecorded_clips_cdn_acm_domains
  prerecorded_clips_cdn_fqdn              = local.prerecorded_clips_cdn_fqdn
  prerecorded_clips_cdn_default_ttl       = local.prerecorded_clips_cdn_default_ttl
  prerecorded_clips_cdn_max_ttl           = local.prerecorded_clips_cdn_max_ttl
  prerecorded_clips_cdn_whitelist_headers = local.prerecorded_clips_cdn_whitelist_headers
}
