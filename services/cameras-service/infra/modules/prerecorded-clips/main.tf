# Add prerecorded clip S3 bucket and cloudfront distribution
module "prerecorded_clips_cdn" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/cloudfront-with-s3"

  company     = var.company
  environment = var.environment
  application = var.application
  service     = "prerecorded-clips-cdn"

  cdn_acm_count      = length(var.prerecorded_clips_cdn_acm_domains)
  cdn_acm_domains    = var.prerecorded_clips_cdn_acm_domains
  comment            = "${var.environment} Prerecorded Cam Clips CDN wth S3"
  cdn_fqdn           = var.prerecorded_clips_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
  default_ttl        = var.prerecorded_clips_cdn_default_ttl
  max_ttl            = var.prerecorded_clips_cdn_max_ttl
  whitelist_headers  = var.prerecorded_clips_cdn_whitelist_headers
}

# Add pretty Route53 records for the CDN
data "aws_route53_zone" "cdn" {
  count = length(var.prerecorded_clips_cdn_acm_domains)
  name  = var.prerecorded_clips_cdn_acm_domains[count.index]
}

resource "aws_route53_record" "cdn" {
  count   = length(var.prerecorded_clips_cdn_acm_domains)
  zone_id = data.aws_route53_zone.cdn.*.zone_id[count.index]
  name = element(
    var.prerecorded_clips_cdn_fqdn[element(var.prerecorded_clips_cdn_acm_domains, count.index)],
    0,
  )
  type    = "CNAME"
  ttl     = 300
  records = module.prerecorded_clips_cdn.domain_name
}
