provider "aws" {
  # Note: We explicitly specify `us-west-2` here because the aws resources in
  # this stack didn't exist in `us-west-1` when we created it. We typically
  # default to `us-west-1` and don't explicitly specify region here.
  region = "us-west-2"
}

# Firehose.
resource "aws_s3_bucket" "bucket" {
  bucket = "sl-camera-analytics-${var.environment}"
  acl    = "private"

  lifecycle_rule {
    prefix  = "events/"
    enabled = true

    expiration {
      days = var.analytics_retention
    }
  }

  tags = {
    Company     = "sl"
    Service     = "s3"
    Application = "camera-analytics"
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_iam_role" "firehose_role" {
  name               = "sl-camera-analytics-firehose-delivery-policy-${var.environment}"
  assume_role_policy = file("${path.module}/resources/firehose-delivery-policy.json")
}

resource "aws_iam_policy" "firehose_write_to_s3_policy" {
  name = "sl-camera-analytics-firehose-dest-write-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/firehose-dest-write-policy.json", {
    s3_bucket = aws_s3_bucket.bucket.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_write_policy" {
  role       = aws_iam_role.firehose_role.name
  policy_arn = aws_iam_policy.firehose_write_to_s3_policy.arn
}

resource "aws_iam_policy" "firehose_producer_policy" {
  name = "sl-camera-analytics-firehose-producer-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/firehose-producer-policy.json", {
    analytics_stream = aws_kinesis_firehose_delivery_stream.analytics_stream.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_producer_policy" {
  role       = var.producer_role_name
  policy_arn = aws_iam_policy.firehose_producer_policy.arn
}

resource "aws_kinesis_firehose_delivery_stream" "analytics_stream" {
  name        = "sl-camera-analytics-${var.environment}"
  destination = "s3"

  s3_configuration {
    role_arn           = aws_iam_role.firehose_role.arn
    bucket_arn         = aws_s3_bucket.bucket.arn
    compression_format = "GZIP"
    buffer_size        = 3
    prefix             = "events/"
  }
}

# Glue.
resource "aws_iam_role" "glue_role" {
  name               = "sl-camera-analytics-glue-etl-policy-${var.environment}"
  assume_role_policy = file("${path.module}/resources/glue-etl-policy.json")
}

resource "aws_iam_policy" "glue_read_policy" {
  name = "sl-camera-analytics-glue-source-read-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/glue-source-read-policy.json", {
    s3_bucket = aws_s3_bucket.bucket.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_glue_policy" {
  role       = aws_iam_role.glue_role.name
  policy_arn = aws_iam_policy.glue_read_policy.arn
}

resource "aws_iam_role_policy_attachment" "attach_glue_managed_policy" {
  role       = aws_iam_role.glue_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
}
