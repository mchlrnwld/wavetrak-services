# Create the sns topic to trigger the lambda
module "sns" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"

  environment                = var.environment
  topic_name                 = "${var.company}-${var.application}-${var.function_name}-sns"
}

resource "aws_kms_key" "wt_function_kms_key" {
  description             = "Function specific KMS key."
  key_usage               = "ENCRYPT_DECRYPT"
  is_enabled              = true
  enable_key_rotation     = false
  deletion_window_in_days = 30

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Function    = var.function_name
    Service     = "lambda"
    Terraform   = true
  }
}

resource "aws_kms_alias" "wt_function_kms_key_alias" {
  name          = "alias/${var.environment}/functions/${var.application}-${var.function_name}"
  target_key_id = aws_kms_key.wt_function_kms_key.key_id
}

# Add multiple S3 buckets and cloudfront distribution
module "embed_cdn" {
  source = "./embed-s3-buckets-with-cloudfront"

  company     = var.company
  environment = var.environment
  application = var.application
  service     = "embed-cdn"

  allowed_origins = ["*"]
  cdn_acm_count   = length(var.embed_cdn_acm_domains)
  cdn_acm_domains = var.embed_cdn_acm_domains
  cdn_fqdn        = var.embed_cdn_fqdn
  web_url         = var.web_url

  comment            = "${var.environment} Embed CDN wth S3"
  versioning_enabled = false
  default_ttl        = var.embed_cdn_default_ttl
  max_ttl            = var.embed_cdn_max_ttl
  whitelist_headers  = var.embed_cdn_whitelist_headers
}

# Add pretty Route53 records for the CDN
data "aws_route53_zone" "cdn" {
  count = length(var.embed_cdn_acm_domains)
  name  = var.embed_cdn_acm_domains[count.index]
}

resource "aws_route53_record" "cdn" {
  count   = length(var.embed_cdn_acm_domains)
  zone_id = data.aws_route53_zone.cdn.*.zone_id[count.index]
  name = element(
    var.embed_cdn_fqdn[element(var.embed_cdn_acm_domains, count.index)],
    0,
  )
  type    = "CNAME"
  ttl     = 300
  records = [module.embed_cdn.domain_name]
}

# Create embed config created SNS topic
module "embed_config_created_sns_topic" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"

  environment = var.environment
  topic_name  = "embed_config_created"
}

# Permit cameras service to publish to SNS topic
resource "aws_iam_role_policy_attachment" "embed_config_created_topic_publish_policy" {
  role       = "cameras_service_task_role_${var.environment}"
  policy_arn = module.embed_config_created_sns_topic.topic_policy
}
