output "embed_cdn_bucket_arn" {
  value = aws_s3_bucket.embed_cdn_bucket.arn
}

output "embed_cdn_bucket_name" {
  value = aws_s3_bucket.embed_cdn_bucket.id
}

output "embed_config_bucket_arn" {
  value = aws_s3_bucket.embed_config_bucket[0].arn
}

output "embed_config_bucket_name" {
  value = aws_s3_bucket.embed_config_bucket[0].id
}

output "cam_details_bucket_arn" {
  value = aws_s3_bucket.cam_details_bucket[0].arn
}

output "cam_details_bucket_name" {
  value = aws_s3_bucket.cam_details_bucket[0].id
}

output "cam_app_bucket_arn" {
  value = aws_s3_bucket.cam_app_bucket.arn
}

output "cam_app_bucket_name" {
  value = aws_s3_bucket.cam_app_bucket.id
}

output "domain_name" {
  value = aws_cloudfront_distribution.cdn[0].domain_name
}

output "domain_alias" {
  value = aws_cloudfront_distribution.cdn[0].aliases
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.cdn[0].hosted_zone_id
}

output "distribution_arn" {
  value = aws_cloudfront_distribution.cdn[0].arn
}
