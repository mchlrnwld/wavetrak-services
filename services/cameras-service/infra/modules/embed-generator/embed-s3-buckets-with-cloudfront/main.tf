provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

data "aws_region" "current" {
}

locals {
  waf_name = "CamEmbed"
}

resource "aws_waf_web_acl" "waf_acl" {
  name        = "${local.waf_name}_waf_acl"
  metric_name = "${local.waf_name}wafacl"

  default_action {
    type = "ALLOW"
  }

  rules {
    priority = 10
    rule_id  = aws_waf_rule.regex_blacklist.id

    action {
      type = "BLOCK"
    }
  }
}

resource "aws_waf_rule" "regex_blacklist" {

  name        = "${local.waf_name}_regex_blacklist_rule"
  metric_name = "${local.waf_name}regexBlacklist"

  predicates {
    data_id = aws_waf_regex_match_set.regex_blacklist_match_set.id
    negated = false
    type    = "RegexMatch"
  }

}
resource "aws_waf_regex_match_set" "regex_blacklist_match_set" {
  name = "${local.waf_name}_match_regex_blacklist"

  regex_match_tuple {
    field_to_match {
      data = "Referer"
      type = "HEADER"
    }

    regex_pattern_set_id = aws_waf_regex_pattern_set.regex_blacklist_pattern_set.id
    text_transformation  = "NONE"
  }
}

resource "aws_waf_regex_pattern_set" "regex_blacklist_pattern_set" {
  name = "${local.waf_name}_waf_regex_pattern_set"
  regex_pattern_strings = [".*iplivecams.*", "^$"]
}


# S3 bucket for content
resource "aws_s3_bucket" "embed_cdn_bucket" {
  bucket = "${var.company}-${var.application}-embed-cdn-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = var.allowed_origins
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  tags = {
    Name        = "${var.company}-${var.application}-embed-cdn-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = "embed-cdn"
  }
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "embed_cdn_bucket_origin_access_identity" {
  comment = var.comment
}

# S3 bucket policy for CloudFront access
resource "aws_s3_bucket_policy" "embed_cdn_bucket_policy" {
  bucket = aws_s3_bucket.embed_cdn_bucket.id
  policy = templatefile("${path.module}/resources/cdn-bucket-policy.json", {
    s3_bucket  = aws_s3_bucket.embed_cdn_bucket.arn
    cloudfront = aws_cloudfront_origin_access_identity.embed_cdn_bucket_origin_access_identity.iam_arn
  })
}

# S3 bucket for content
resource "aws_s3_bucket" "embed_config_bucket" {
  bucket = "${var.company}-${var.application}-embed-config-bucket-${var.environment}"
  acl    = "private"
  count  = length(var.cdn_acm_domains)

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = var.cdn_fqdn[element(var.cdn_acm_domains, count.index)]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  tags = {
    Name        = "${var.company}-${var.application}-embed-config-bucket-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = "embed-config-bucket"
  }
}

# CloudFront access identity for embed config bucket
resource "aws_cloudfront_origin_access_identity" "embed_config_bucket_origin_access_identity" {
  comment = var.comment
}

# S3 bucket policy for CloudFront access
resource "aws_s3_bucket_policy" "embed_config_bucket_policy" {
  bucket = aws_s3_bucket.embed_config_bucket[0].id
  policy = templatefile("${path.module}/resources/cdn-bucket-policy.json", {
    s3_bucket  = aws_s3_bucket.embed_config_bucket[0].arn
    cloudfront = aws_cloudfront_origin_access_identity.embed_config_bucket_origin_access_identity.iam_arn
  })
}

# S3 bucket for content
resource "aws_s3_bucket" "cam_details_bucket" {
  bucket = "${var.company}-${var.application}-cam-details-bucket-${var.environment}"
  acl    = "private"
  count  = length(var.cdn_acm_domains)

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = flatten([
      var.cdn_fqdn[element(var.cdn_acm_domains, count.index)],
      var.web_url
    ])
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  tags = {
    Name        = "${var.company}-${var.application}-cam-details-bucket-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = "cam-details-bucket"
  }
}

# CloudFront access identity for cam details bucket
resource "aws_cloudfront_origin_access_identity" "cam_details_bucket_origin_access_identity" {
  comment = var.comment
}

# S3 bucket policy for CloudFront access
resource "aws_s3_bucket_policy" "cam_details_bucket_policy" {
  bucket = aws_s3_bucket.cam_details_bucket[0].id
  policy = templatefile("${path.module}/resources/cdn-bucket-policy.json", {
    s3_bucket  = aws_s3_bucket.cam_details_bucket[0].arn
    cloudfront = aws_cloudfront_origin_access_identity.cam_details_bucket_origin_access_identity.iam_arn
  })
}

resource "aws_s3_bucket" "cam_app_bucket" {
  bucket = "${var.company}-${var.application}-cam-app-bucket-${var.environment}"
  acl    = "public-read"

  tags = {
    Name        = "${var.company}-${var.application}-cam-app-bucket-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = "cam_app_bucket"
  }

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

resource "aws_s3_bucket_policy" "cam_app_bucket_policy" {
  bucket = aws_s3_bucket.cam_app_bucket.id
  policy = templatefile("${path.module}/resources/s3-read-policy.json", {
    s3_bucket = aws_s3_bucket.cam_app_bucket.arn
  })
}

data "aws_acm_certificate" "cdn_acm" {
  provider    = aws.us-east-1
  domain      = format("*.%s", element(var.cdn_acm_domains, 0))
  statuses    = ["PENDING_VALIDATION", "ISSUED"]
  most_recent = true
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  web_acl_id = aws_waf_web_acl.waf_acl.id

  origin {
    domain_name = aws_s3_bucket.embed_cdn_bucket.bucket_domain_name
    origin_id   = "S3-${aws_s3_bucket.embed_cdn_bucket.bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.embed_cdn_bucket_origin_access_identity.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = aws_s3_bucket.embed_config_bucket[0].bucket_domain_name
    origin_id   = "S3-${aws_s3_bucket.embed_config_bucket[0].bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.embed_config_bucket_origin_access_identity.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = aws_s3_bucket.cam_details_bucket[0].bucket_domain_name
    origin_id   = "S3-${aws_s3_bucket.cam_details_bucket[0].bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cam_details_bucket_origin_access_identity.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = aws_s3_bucket.cam_app_bucket.website_endpoint
    origin_id   = "S3-${aws_s3_bucket.cam_app_bucket.bucket}"

    custom_origin_config {
      origin_protocol_policy = "http-only"
      http_port              = "80"
      https_port             = "443"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  count               = length(var.cdn_acm_domains)
  enabled             = true
  is_ipv6_enabled     = true
  comment             = var.comment
  aliases             = var.cdn_fqdn[element(var.cdn_acm_domains, count.index)]
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["HEAD", "GET"]
    cached_methods   = ["HEAD", "GET"]
    target_origin_id = "S3-${aws_s3_bucket.cam_app_bucket.bucket}"
    compress         = true

    forwarded_values {
      headers      = var.whitelist_headers
      query_string = true

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  ordered_cache_behavior {
    path_pattern     = "cam-details/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["HEAD", "GET"]
    target_origin_id = "S3-${aws_s3_bucket.cam_details_bucket[0].bucket}"
    compress         = true

    forwarded_values {
      headers      = var.whitelist_headers
      query_string = true

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  ordered_cache_behavior {
    path_pattern     = "cam-config/*"
    allowed_methods  = ["HEAD", "GET"]
    cached_methods   = ["HEAD", "GET"]
    target_origin_id = "S3-${aws_s3_bucket.embed_config_bucket[0].bucket}"
    compress         = true

    forwarded_values {
      headers      = var.whitelist_headers
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  ordered_cache_behavior {
    path_pattern     = "cam/*"
    allowed_methods  = ["HEAD", "GET"]
    cached_methods   = ["HEAD", "GET"]
    target_origin_id = "S3-${aws_s3_bucket.embed_cdn_bucket.bucket}"
    compress         = true

    forwarded_values {
      headers      = var.whitelist_headers
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }

  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.cdn_acm.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
