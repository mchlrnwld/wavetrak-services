# General variables
variable "application" {
  description = "The application name"
  default     = ""
}

variable "company" {
  description = "The company (sl, bw, ft, wt, msw)"
  default     = ""
}

variable "environment" {
  description = "The environment (sbox, staging, prod)"
  default     = ""
}

# Function variables
variable "artifacts_bucket" {
  description = "The artifacts S3 bucket"
  default     = ""
}

variable "function_name" {
  description = "The function name"
  default     = ""
}

variable "timeout" {
  description = "The timeout of the Lambda function"
  default     = 30
}

variable "clips_cdn_acm_domains" {
  description = "List of domains associated with ACM certificate on the Cloudfront distribution"
  type        = list(string)
}

variable "clips_cdn_fqdn" {
  description = "List of domains associated with Cloudfront distribution"
  type        = map(list(string))
}

variable "clips_cdn_default_ttl" {
  description = "Clips CDN default TTL"
  default     = 60
}

variable "clips_cdn_max_ttl" {
  description = "Clips CDN max TTL"
  default     = 60
}

variable "clips_cdn_whitelist_headers" {
  description = "Clips CDN whitelisted headers"
  type        = list(string)
}
