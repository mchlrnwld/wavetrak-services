# Create the SQS queue to trigger it and DLQ for failed runs
module "sqs" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs"

  environment                = var.environment
  queue_name                 = "${var.company}-${var.application}-${var.function_name}-sqs"
  visibility_timeout_seconds = var.timeout
}

# Create the SNS topic
module "sns_topic" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"

  environment = var.environment
  topic_name  = "${var.company}-${var.application}-${var.function_name}-sns"
}


# Add rewind clip S3 bucket and cloudfront distribution
module "clips_cdn" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/cloudfront-with-s3"

  company     = var.company
  environment = var.environment
  application = var.application
  service     = "clips-cdn"

  cdn_acm_count      = length(var.clips_cdn_acm_domains)
  cdn_acm_domains    = var.clips_cdn_acm_domains
  comment            = "${var.environment} Cam Clips CDN wth S3"
  cdn_fqdn           = var.clips_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
  default_ttl        = var.clips_cdn_default_ttl
  max_ttl            = var.clips_cdn_max_ttl
  whitelist_headers  = var.clips_cdn_whitelist_headers
}
