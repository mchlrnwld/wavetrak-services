terraform state mv 'module.rewind_clips.module.lambda_with_sqs.module.sqs_queue.aws_sqs_queue.queue' 'module.rewind_clips.module.sqs.aws_sqs_queue.queue'
terraform state mv 'module.rewind_clips.module.lambda_with_sqs.module.sqs_queue.aws_iam_policy.policy' 'module.rewind_clips.module.sqs.aws_iam_policy.policy'
terraform state mv 'module.embed_generator.module.lambda_with_sns.module.lambda.aws_kms_alias.wt_function_kms_key_alias' 'module.embed_generator.aws_kms_alias.wt_function_kms_key_alias'
terraform state mv 'module.embed_generator.module.lambda_with_sns.module.lambda.aws_kms_key.wt_function_kms_key' 'module.embed_generator.aws_kms_key.wt_function_kms_key'
