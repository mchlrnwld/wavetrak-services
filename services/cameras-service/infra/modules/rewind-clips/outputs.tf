output "queue_arn" {
  value = module.sqs.queue_arn
}

output "queue_policy" {
  value = module.sqs.queue_policy
}

output "sns_topic" {
  value = module.sns_topic.topic
}

output "sns_topic_policy" {
  value = module.sns_topic.topic_policy
}

