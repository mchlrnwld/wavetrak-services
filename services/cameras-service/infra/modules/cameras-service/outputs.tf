output "camera_service_task_role_arn" {
  value = data.aws_iam_role.cameras_service_task_role.arn
}

output "camera_service_task_role_name" {
  value = data.aws_iam_role.cameras_service_task_role.name
}

output "target_group" {
  value = module.cameras_service.target_group
}
