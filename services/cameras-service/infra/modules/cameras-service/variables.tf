variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "alb_listener_arn" {
}

variable "default_vpc" {
}

variable "ecs_cluster" {
}

variable "ssm_parameter_name" {
}

variable "kms_key" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_response_latency"
}

variable "auto_scaling_min_size" {
  default = 1
}

variable "auto_scaling_max_size" {
  default = 6
}

variable "auto_scaling_up_metric" {
  default = 50
}

variable "auto_scaling_down_metric" {
  default = 10
}

variable "auto_scaling_up_count" {
  default = 1
}

variable "auto_scaling_down_count" {
  default = -1
}

variable "auto_scaling_target_value" {
  default = 600
}

# New Relic shared alert variables

variable "newrelic_app_name" {
  description = "Name of the APM service being monitored in New Relic"
  default     = "Surfline Services - Cameras (sandbox)"
}

variable "newrelic_policy_name" {
  description = "Name of the New Relic policy"
  default     = "Camera Service - sbox"
}

# New Relic apdex alert variables

variable "newrelic_apdex_create_alert" {
  description = "Create New Relic apdex alert"
  default     = false
}

# variable "newrelic_apdex_alert_name" {
#   description = "Name of the New Relic alert to be created"
#   default     = "Cameras Service - Apdex (Low)"
# }

variable "newrelic_apdex_runbook_url" {
  description = "URL of runbook that explains how to resolve the alert"
  default     = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
}

variable "newrelic_apdex_alert_threshold" {
  description = "Condition violation threshold for the alert"
  default     = ".70"
}

variable "newrelic_apdex_warning_threshold" {
  description = "Alert condition violation threshold for the warning"
  default     = ".85"
}

# New Relic error rate alert variables

variable "newrelic_error_rate_create_alert" {
  description = "Create New Relic error rate alert"
  default     = false
}

# variable "newrelic_error_rate_alert_name" {
#   description = "Name of the New Relic alert to be created"
#   default     = "Cameras Service - Error percentage (High)"
# }

variable "newrelic_error_rate_runbook_url" {
  description = "URL of runbook that explains how to resolve the alert"
  default     = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
}

variable "newrelic_error_rate_alert_threshold" {
  description = "Condition violation threshold for the alert (as a percentage)"
  default     = 5
}

variable "newrelic_error_rate_warning_threshold" {
  description = "Condition violation threshold for the warning (as a percentage)"
  default     = 3
}
