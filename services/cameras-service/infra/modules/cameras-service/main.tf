locals {
  newrelic_app_name              = "Surfline Services - ${title(element(split("-", var.application), 0))}"
  newrelic_policy_name           = "${title(element(split("-", var.application), 0))} ${title(element(split("-", var.application), 1))}"
}

module "cameras_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-cameras-service"
  service_td_count            = var.service_td_count
  service_td_container_name   = var.application
  service_port                = 8080
  service_alb_priority        = 402

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id
  ecs_cluster = var.ecs_cluster

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
  auto_scaling_up_count       = var.auto_scaling_up_count
  auto_scaling_down_count     = var.auto_scaling_down_count

  # New Relic shared alert variables
  newrelic_app_name    = local.newrelic_app_name
  newrelic_policy_name = local.newrelic_policy_name


  # New Relic apdex alert variable
  newrelic_apdex_create_alert      = var.newrelic_apdex_create_alert
  newrelic_apdex_runbook_url       = var.newrelic_apdex_runbook_url
  newrelic_apdex_alert_threshold   = var.newrelic_apdex_alert_threshold
  newrelic_apdex_warning_threshold = var.newrelic_apdex_warning_threshold

  # New Relic error rate alert variable
  newrelic_error_rate_create_alert      = var.newrelic_error_rate_create_alert
  newrelic_error_rate_runbook_url       = var.newrelic_error_rate_runbook_url
  newrelic_error_rate_alert_threshold   = var.newrelic_error_rate_alert_threshold
  newrelic_error_rate_warning_threshold = var.newrelic_error_rate_warning_threshold
}

data "aws_iam_role" "cameras_service_task_role" {
  name = "cameras_service_task_role_${var.environment}"
}

resource "aws_iam_policy" "cameras_service_ssm_policy" {
  name        = "${var.company}-${var.application}-${var.application}-ssm-policy-${var.environment}"
  description = "policy to access parameter store from cameras service"
  policy = templatefile("${path.module}/resources/ssm-policy.json", {
    ssm_parameter_name = var.ssm_parameter_name
    kms_key            = var.kms_key
  })
}

resource "aws_iam_role_policy_attachment" "cameras_service_task_ssm_arn" {
  role       = "cameras_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.cameras_service_ssm_policy.arn
}

resource "aws_sns_topic" "camera_details_updated" {
  name = "camera_details_updated_${var.environment}"
}

resource "aws_iam_policy" "policy" {
  name        = "sns_policy_camera_details_updated_${var.environment}"
  description = "policy to publish to sns topic"
  policy = templatefile("${path.module}/resources/sns-policy.json", {
    sns_topic = aws_sns_topic.camera_details_updated.arn
  })
}

resource "aws_iam_role_policy_attachment" "camera_service_sns_camera_details_updated" {
  role       = "cameras_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.policy.arn
}
