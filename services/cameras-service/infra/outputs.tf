output "camera_service_sns_topic" {
  value = module.rewind_clips.sns_topic
}

output "camera_service_sns_topic_policy" {
  value = module.rewind_clips.sns_topic_policy
}

output "camera_service_task_role_arn" {
  value = module.cameras_service.camera_service_task_role_arn
}

output "camera_service_sqs_queue" {
  value = module.rewind_clips.queue_arn
}

output "camera_service_sqs_queue_policy" {
  value = module.rewind_clips.queue_policy
}
