variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "artifacts_bucket" {
}

variable "internal_sg_group" {
}

variable "instance_subnets" {
  type = list(string)
}

variable "sg_all_servers" {
}

variable "web_url" {
}

variable "load_balancer_arn" {
}

variable "rewind_source_s3_bucket_name" {
  default = "sl-live-cam-archive-prod"
}

variable "cam_details_updated_sns_topic_arn" {
  description = "An SNS topic ARN used to invoke the lambda function"
}

variable "clips_cdn_acm_domains" {
  description = "List of domains associated with ACM certificate on the Cloudfront distribution"
  type        = list(string)
}

variable "clips_cdn_fqdn" {
  description = "List of domains associated with Cloudfront distribution"
  type        = map(list(string))
}

variable "clips_cdn_default_ttl" {
  description = "Clips CDN default TTL"
  default     = 60
}

variable "clips_cdn_max_ttl" {
  description = "Clips CDN max TTL"
  default     = 60
}

variable "clips_cdn_whitelist_headers" {
  description = "Clips CDN whitelisted headers"
  type        = list(string)
}

variable "rewind_source_cross_account_roles" {
  description = "List of cross-account roles to grant access to rewind source bucket"
  type        = list(string)
}

variable "embed_cdn_acm_domains" {
  description = "List of domains associated with ACM certificate on the Cloudfront distribution"
  type        = list(string)
}

variable "embed_cdn_fqdn" {
  description = "List of domains associated with Cloudfront distribution"
  type        = map(string)
}

variable "embed_cdn_default_ttl" {
  description = "Embed CDN default TTL"
  default     = 60
}

variable "embed_cdn_max_ttl" {
  description = "Embed CDN max TTL"
  default     = 60
}

variable "embed_cdn_whitelist_headers" {
  description = "Embed CDN whitelisted headers"
  type        = list(string)
}

variable "prerecorded_clips_cdn_acm_domains" {
  description = "List of domains associated with ACM certificate on the Cloudfront distribution"
  type        = list(string)
}

variable "prerecorded_clips_cdn_fqdn" {
  description = "List of domains associated with Cloudfront distribution"
  type        = map(string)
}

variable "prerecorded_clips_cdn_default_ttl" {
  description = "Embed CDN default TTL"
  default     = 60
}

variable "prerecorded_clips_cdn_max_ttl" {
  description = "Embed CDN max TTL"
  default     = 60
}

variable "prerecorded_clips_cdn_whitelist_headers" {
  description = "Embed CDN whitelisted headers"
  type        = list(string)
}

variable "newrelic_create_alert" {
  description = "Create New Relic alert with lambda function infrastructure"
  default     = true
}

variable "newrelic_policy_name" {
  description = "Name of New Relic policy"
  default     = "Squad - TAG"
}

variable "newrelic_condition_type" {
  description = "Type of the condition"
  default     = "static"
}

variable "newrelic_condition_name" {
  description = "Name of the New Relic condition to be created"
  default     = ""
}

variable "newrelic_condition_description" {
  description = "New Relic condition description"
  default     = ""
}

variable "newrelic_condition_enabled" {
  description = "Enable New Relic condition alert"
  default     = true
}

variable "newrelic_condition_value_function" {
  description = "Value function; possible values are single_value, sum"
  default     = "sum"
}

variable "newrelic_condition_violation_time_limit_seconds" {
  description = "Limit, in seconds, that will automatically force-close a long-lasting violation"
  default     = 86400
}

variable "newrelic_lambda_name" {
  description = "Name of the lambda function on which to alert"
  default     = ""
}

variable "newrelic_condition_evaluation_offset" {
  description = "Offset of evaluation action in minutes"
  default     = 20
}

variable "newrelic_condition_operator" {
  description = "Condition operator"
  default     = "above"
}

variable "newrelic_condition_threshold" {
  description = "Condition violation threshold for the alert"
  default     = 0
}

variable "newrelic_condition_threshold_duration" {
  description = "Duration of time in seconds before a violation is created"
  default     = 600
}

variable "newrelic_condition_threshold_occurrences" {
  description = "Number of data points that must be in violation for the specified threshold duration"
  default     = "all"
}
