# Reprocess Cam Rewind Clips

This migration gets cam rewind clip references from MongoDB
and regenerates cam rewind clips by sending messages to an
SQS queue.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate 2020-10-20-reprocess-cam-rewind-clips
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Environment

This migration requires that the following environment variables are set in the `.env` file:

| Variable | Description |
|:---------|:------------|
| `MONGO_CONNECTION_STRING` | MongoDB Connection String |
| `AWS_SQS_QUEUE_URL` | The SQS queue URL |

## Usage

```bash
env $(xargs < .env) python main.py
```

