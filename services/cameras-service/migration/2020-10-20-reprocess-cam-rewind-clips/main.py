from datetime import datetime
import json
import os
import sys

import boto3
import pymongo

AWS_SQS_QUEUE_URL = os.environ.get("AWS_SQS_QUEUE_URL")
MONGO_CONNECTION_STRING_KBYG = os.environ.get("MONGO_CONNECTION_STRING_KBYG")

sqs_client = boto3.client("sqs")

print("[INFO] Connecting to MongoDB")
mongo_client = pymongo.MongoClient(MONGO_CONNECTION_STRING_KBYG)
mongo_db = mongo_client["KBYG"]
clip_col = mongo_db["CameraClip"]

# MongoDB Shell Query
#
# db.getCollection("CameraClip").find({
#   $and: [
#     { createdAt: { $gte: ISODate("2020-10-16T12:00:00.000+0000") } },
#     { createdAt: { $lte: ISODate("2020-10-20T18:10:00.000+0000") } },
#   ],
#   status: "CLIP_AVAILABLE",
# }, {
#     clipId: 1,
#     cameraId: 1,
#     startTimestampInMs: 1,
#     endTimestampInMs: 1
# });

print("[INFO] Getting clips from MongoDB")
clips = clip_col.find(
    {
        "createdAt": {
            "$gte": datetime(2020, 10, 16, 12, 0, 0),
            "$lte": datetime(2020, 10, 20, 18, 10, 0),
        },
        "status": "CLIP_AVAILABLE",
    },
    {"cameraId": 1, "startTimestampInMs": 1, "endTimestampInMs": 1},
)

print("[INFO] Posting messages to SQS")
for clip in clips:
    try:
        sqs_message = {
            "clipId": str(clip["_id"]),
            "cameraId": clip["cameraId"],
            "endTimestampInMs": int(clip["endTimestampInMs"]),
            "retryTimestamps": [],
            "startTimestampInMs": int(clip["startTimestampInMs"]),
        }

        response = sqs_client.send_message(
            QueueUrl=AWS_SQS_QUEUE_URL, MessageBody=json.dumps(sqs_message)
        )

        print(
            f"[INFO] Posted SQS message {response['MessageId']} "
            f"for clip ID {sqs_message['clipId']}"
        )
    except:
        e = sys.exc_info()
        print(f"[ERROR] Caught error while handling clip: {e}")
