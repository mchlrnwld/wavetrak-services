/* eslint-disable no-undef */

// takes less than a second, only 415 cameras at last count
// https://docs.mongodb.com/manual/reference/operator/update/rename/

// Migration
db.Cameras.updateMany({}, { $rename: { streamUrl: 'playlistUrl' } });

// Rollback
db.Cameras.updateMany({}, { $rename: { playlistUrl: 'streamUrl' } });
