# Cam Aliases Data Migration

These notebooks rename camera aliases in SQL Server and MongoDB.

## Notebooks

### [SQL Server - Migrate Cam Aliases and Stream Urls](migrate-cam-aliases-in-sql-server.ipynb)

This notebook requires that the following environment variables are set:

| Variable | Description |
|:---------|:------------|
| `DB_USER` | SQL Server database username |
| `DB_PASSWORD ` | SQL Server database password |

#### Example usage

```bash
DB_USER={db-user} DB_PASSWORD={db-password} \
  nteract migrate-cam-aliases-in-sql-server.ipynb
```

### [MongoDB - Migrate Cam Aliases In Recordings](migrate-cam-aliases-in-mongodb.ipynb)

This notebook requires that the following environment variables are set:

| Variable | Description |
|:---------|:------------|
| `MONGO_CONNECTION_STRING ` | MongoDB Connection String |

#### Example usage

```bash
MONGO_CONNECTION_STRING={mongo-connection-string} \
  nteract migrate-cam-aliases-in-mongodb.ipynb
```