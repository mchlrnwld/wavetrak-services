import AWS from 'aws-sdk';
import moment from 'moment';

const MongoClient = require('mongodb').MongoClient;

const s3 = new AWS.S3({});
console.log(process.env.MONGO_CONNECTION_STRING_KBYG);

const connectMongo = () => new Promise((resolve, reject) => {
  MongoClient.connect(process.env.MONGO_CONNECTION_STRING_KBYG, (err, db) => {
    if (err) {
      reject(err);
    }
    resolve(db);
  });
});

const importCameraRecordings = async (recordings) => {
  const db = await connectMongo();
  const collection = db.collection('CameraRecording');
  let bulkUpdateOps = [];
  const length = recordings.length;
  let cursor = 0;

  for (let i = 0; i < length; i++) {
    bulkUpdateOps.push({ insertOne: { document: recordings[i] } });
    if (bulkUpdateOps.length === 1000) {
      const res = await collection.bulkWrite(bulkUpdateOps);
      console.log(`Wrote bulk success(${res.isOk()}) progress(${cursor += 1000}/${length})`);
      bulkUpdateOps = [];
    }
  }

  if (bulkUpdateOps.length > 0) {
    const res = await collection.bulkWrite(bulkUpdateOps);
    console.log(
      `Wrote bulk success(${res.isOk()}) progress(${cursor += bulkUpdateOps.length}/${length})`);
  }
  db.close();
};

// Read directory prefixes
const getAliases = () => new Promise((resolve, reject) => {
  const params = {
    Bucket: 'sl-live-cam-archive-prod',
    MaxKeys: 500,
    Delimiter: '/',
  };
  s3.listObjectsV2(params, (err, data) => {
    if (err) {
      console.log(err, err.stack);
      reject(err);
    } else {
      const { CommonPrefixes } = data;
      const prefixes = [];
      for (let i = 0; i < CommonPrefixes.length; i++) {
        const { Prefix } = CommonPrefixes[i];
        prefixes.push(Prefix.replace('/', ''));
      }
      resolve(prefixes);
    }
  });
});


// Transform s3 entries into an array of recording names
const getRecordingNames = (Contents) =>
  Contents.filter((obj) => obj.StorageClass === 'STANDARD')
    .map((obj) => obj.Key.split('/')[1]);


// Get list of recordings for cam alias, recursively
const getRecordingsForAlias = (alias /* required */, continuationToken /* optional */) =>
  new Promise((resolve, reject) => {
    const params = {
      Bucket: process.env.S3_BUCKET,
      MaxKeys: 1000,
      Prefix: alias,
    };
    if (continuationToken) {
      params.ContinuationToken = continuationToken;
    }
    s3.listObjectsV2(params, async (err, data) => {
      if (err) {
        console.log(err, err.stack);
        reject(err);
      } else {
        const { IsTruncated, NextContinuationToken, Contents } = data;
        if (IsTruncated) {
          resolve(getRecordingNames(Contents)
          .concat(await getRecordingsForAlias(alias, NextContinuationToken)));
        } else {
          resolve(getRecordingNames(Contents));
        }
      }
    });
  });


// Build CameraRecording document for recordings
const buildRecordingDocument = (filename) => {
  const [alias, time, date, _] = filename.split('.');
  const [year, month, day] = date.split('-');
  const momentTime = moment(`${year}-${month}-${day} ${time}`,
    'YYYY-MM-DD HHmm').subtract('3', 'h');
  const utcTime = moment(momentTime).utc();
  return {
    resolution: '1280x720',
    thumbLargeUrl: `https://camstills.cdn-surfline.com/${alias}/${year}/${utcTime.format('MM')}/${utcTime.format('DD')}/feed_${alias}_${utcTime.format('HHmm')}.${utcTime.format('YYYY-MM-DD')}_large.jpg`,
    thumbSmallUrl: `https://camstills.cdn-surfline.com/${alias}/${year}/${utcTime.format('MM')}/${utcTime.format('DD')}/feed_${alias}_${utcTime.format('HHmm')}.${utcTime.format('YYYY-MM-DD')}_small.jpg`,
    startDate: moment(momentTime).toDate(),
    endDate: moment(momentTime).add(9, 'm').add(59, 's').add(999, 'ms').toDate(),
    alias,
    expireSoft: moment(momentTime).add(7, 'd').toDate(),
    expireHard: moment(momentTime).add(25, 'd').toDate(),
    recordingUrl: `https://camrewinds.cdn-surfline.com/${alias}/${filename}`
  };
};

const buildRecordingDocuments = (filenames) =>
  filenames.map((filename) => buildRecordingDocument(filename));

const importS3Recordings = async () => {
  const documents = [];

  const prefixes = await getAliases();
  console.log('Number of cams:', prefixes.length);

  for (const alias of prefixes) {
    const recordings = await getRecordingsForAlias(alias);
    console.log(`${recordings.length} recordings from
    ${recordings[0]} to ${recordings[recordings.length - 1]}`);

    const recordingDocuments = buildRecordingDocuments(recordings);
    documents.push.apply(documents, recordingDocuments);
  }

  importCameraRecordings(documents);

  console.log('Done.', documents.length);
};

importS3Recordings();
