# S3 Recording Import

Imports recordings from s3 bucket to camera collection

## Usage

Set `AWS_PROFILE` in console

Copy `.env.sample` to `.env`, configure Mongo credentials and environment

Start: `npm run start`
