/* eslint-disable no-unused-expressions */
import fetch from 'node-fetch';
import publishTopic from '../src/server/publishTopic';
import snsMessageConfig from '../src/snsMessageConfig';

const cameraService = process.env.CAMERAS_API;

const wait = async ms => new Promise(resolve => {
  setTimeout(resolve, ms);
});

const getCameras = async () => {
  const url = `${cameraService}/cameras/`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

const addCamDetailsToS3 = async (errors) => {
  console.log('Node Env: ', process.env.NODE_ENV);
  console.log('SNS Config: ', snsMessageConfig);
  const cameras = await getCameras();
  console.log(`Processing ${cameras.length} cameras:`);
  for (const camera of cameras) {
    try {
      console.log(`${camera.title} - ${camera._id}`);
      await publishTopic(
        { _id: camera._id },
        'AWS_CAMERA_DETAILS_UPDATED_TOPIC',
        { noStreamUrl: camera.isPremium }
      );
      await wait(3000);
    } catch (error) {
      errors.push({ error, cameraId: camera._id });
    }
  }
};

const runScript = async () => {
  try {
    const errors = [];
    await addCamDetailsToS3(errors);
    if (errors) {
      console.log('FINISHED WITH ERRORS: ', errors);
      console.log('ERRORS: ', errors.length);
    } else {
      console.log('FINISHED');
    }
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};


runScript();
