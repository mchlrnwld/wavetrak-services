/* eslint-disable max-len */
import mongoose from 'mongoose';
import { initMongo, setupLogsene } from '@surfline/services-common';
import fs from 'fs/promises';
import path from 'path';
import CameraClipModel from '../src/model/CameraClipModel';

/**
 * README
 *
 * This script is to update incorrect url naming to fix sessions prod issue.
 * To run this script you will need to update "@surfline/services-common" to use "5.1.0" in package.json
 * additionally, it requires the .env file and the appropriate mongo read/write credentials.
 * It may be required to update regex and date-time values for the mongo query.
 *
 * Test data:
 * createdAt: {
    $gte: new Date('2018-10-11T20:11:56.000Z'),
    $lte: new Date('2018-10-11T20:11:57.000Z')
  }
 * _id: 5bbfae8c4be2c87b18b3dee3
 * _id: 5bbfae8c4be2c860a5b3dee4
*/

const query = {
  'clip.url': {
    $regex: 'camclips\.prod\.surfline\.com'
  },
  createdAt: {
    $gte: new Date('2021-11-11T00:00:00.000Z')
  }
};

const DRY_RUN = false;

const UPDATED_URL = 'https://camclips.cdn-surfline.com//';

// eslint-disable-next-line arrow-body-style
const createFile = (fileName, objectArray) => {
  return fs.writeFile(path.resolve(__dirname, fileName), JSON.stringify(objectArray), 'utf8');
};

const runScript = async () => {
  await setupLogsene('fakekey');
  await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_KBYG, 'cameras-service');
  console.log('Fetching Clips');
  const cameraClips = await CameraClipModel.find(query);
  console.log('Fetched clips');
  if (cameraClips.length) {
    await createFile('originalCameraClips.json', cameraClips);
    console.log(`Captured ${cameraClips.length} clips to update. info in log file originalCameraClips.json`);


    const clipsToProccess = [...cameraClips];

    const requests = clipsToProccess.map(async (doc) => {
      try {
        const thumbnailFile = doc.thumbnail.key.split('/').slice(-1)[0];
        const clipFile = doc.clip.key.split('/').slice(-1)[0];
        doc.thumbnail.url = `${UPDATED_URL}${doc.cameraId}/${thumbnailFile}`;
        doc.clip.url = `${UPDATED_URL}${doc.cameraId}/${clipFile}`;

        if (DRY_RUN === false) {
          await doc.save();
        }

        return {
          ...doc._doc,
          success: true
        };
      } catch (err) {
        console.warn('failed to update clip: ', doc._id);

        return {
          ...doc._doc,
          success: false
        };
      }
    });

    const proccessedClips = await Promise.all(requests);
    const successClips = proccessedClips.filter((clip) => !!clip.success);
    const failedClips = proccessedClips.filter((clip) => !clip.success);

    await createFile('amendedClips.json', successClips);
    console.log(`Captured ${successClips.length} successful updates. info in log file amendedClips.json`);


    if (failedClips.length) {
      await createFile('failedClips.json', failedClips);
      console.log(`Captured ${failedClips.length} failures in log file failedClips.json`);
    }

    console.log('Job successful');
    process.exit(0);
  } else {
    throw new Error('No clips found');
  }
};

runScript();
