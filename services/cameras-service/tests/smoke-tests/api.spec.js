import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

describe('camerasService', () => {
  let SERVICE_URL = process.env.SERVICE_URL;
  let existingCameraAlias;
  let existingCameraId;
  let createdCamId;
  let camCount;
  let createdClipId;
  let startDate;
  let endDate;
  let randIndex;

  console.log(`Running tests against endpoint: ${SERVICE_URL}`)

  // Set values to be used in test requests
  before(async () => {
    const camRequest = await chai
      .request(SERVICE_URL)
      .get('/cameras')
      .catch((err) => {
        throw err;
      });

    // Select random integer between 1 and 20 to use as index value for camera selection
    camCount = camRequest.body.length;
    randIndex = Math.floor(Math.random() * camCount);

    // Set test fixtures
    existingCameraAlias = camRequest.body[randIndex].alias;
    existingCameraId = camRequest.body[randIndex]._id;
    startDate = Date.now() - 600000; // Subtract 10 minutes;
    endDate = Date.now();
  });

  // Camera tests
  describe('#postCamera', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .post('/cameras')
        .send({
          alias: `smoketest-cam-alias-${startDate}`,
          title: `smoketest-cam-title-${startDate}`,
          message: `smoketest-cam-message-${startDate}`,
          streamUrl: `https://cams.cdn-surfline.com/test-url-${startDate}-/playlist.m3u8`,
          isPremium: false,
          isDown: 1,
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          createdCamId = res.body._id;
          expect(res.body).to.include.all.keys(
            'cotm',
            'isPremium',
            'isDown',
            'message',
            'status',
            'isPrerecorded',
            '_id',
            'alias',
            'title',
            'modifiedDate',
            'playlistUrl',
            'prerecordedClipRecordingTimes',
            '__v',
            'streamUrl',
            'stillUrl',
            'pixelatedStillUrl',
            'rewindBaseUrl',
            'lastPrerecordedClipStartTimeUTC',
            'lastPrerecordedClipEndTimeUTC'
          );
          expect(res.body.cotm).to.equal(false);
          expect(res.body._id).to.match(/[A-Za-z0-9]/);
          expect(res.body.alias).to.equal(`smoketest-cam-alias-${startDate}`);
          expect(res.body.lastPrerecordedClipEndTimeUTC).to.match(/^\d{4}-\d{2}-\d{2}/);
          done();
        });
    });
  });

  describe('#getSingleCamera', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .get(`/cameras/${existingCameraId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body).to.include.all.keys(
            '_id',
            'title',
            'streamUrl',
            'playlistUrl',
            'lastPrerecordedClipEndTimeUTC'
          );
          expect(res.body._id).to.match(/[0-9]/);
          expect(res.body.title).to.match(/[A-Za-z-',.\/]/);
          expect(res.body.streamUrl).to.match(/^https?:\/\/(\w*.)*m3u8$/);
          expect(res.body.playlistUrl).to.match(/^https?:\/\/(\w*.)*m3u8$/);
          expect(res.body.lastPrerecordedClipEndTimeUTC).to.match(/^(\d{4})-(\d{2})-(\d{2}).*/);
          done();
        });
    });
  });

  describe('#getMultipleCameras', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .get('/cameras')
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body).to.have.lengthOf(camCount + 1);
          expect(res.body[randIndex]).to.include.all.keys(
            '_id',
            'title',
            'streamUrl',
            'playlistUrl',
            'lastPrerecordedClipEndTimeUTC'
          );
          expect(res.body[randIndex]._id).to.match(/[0-9]/);
          expect(res.body[randIndex].title).to.match(/[A-Za-z-',.\/]/);
          expect(res.body[randIndex].streamUrl).to.match(/^https?:\/\/(\w*.)*m3u8$/);
          expect(res.body[randIndex].playlistUrl).to.match(/^https?:\/\/(\w*.)*m3u8$/);
          expect(res.body[randIndex].lastPrerecordedClipEndTimeUTC).to.match(
            /^(\d{4})-(\d{2})-(\d{2}).*$/
          );
          done();
        });
    });
  });

  describe('#getMultipleCamerasOrderByField', () => {
    it('should return 200 and return fields in expected order', (done) => {
      chai
        .request(SERVICE_URL)
        .get('/cameras?orderBy=title')
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body).to.have.lengthOf(camCount + 1);
          const cameraTitleSlice = Array.from(res.body.slice(0, 9), (camera) => camera.title);
          const cameraTitleSliceSorted = Array.from(
            res.body.slice(0, 9),
            (camera) => camera.title
          ).sort();
          expect(cameraTitleSlice).to.deep.equal(cameraTitleSliceSorted);
          done();
        });
    });
  });

  describe('#getCameraAndFilterByTitle', () => {
    it('should return 200 and title should contain huntington', (done) => {
      chai
        .request(SERVICE_URL)
        .get('/cameras?search=Huntington')
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body[0].title).to.match(/[Hh]untington/);
          done();
        });
    });
  });

  describe('#getCameraIdFromAlias', () => {
    it('should return 200 and contain camera alias', (done) => {
      chai
        .request(SERVICE_URL)
        .get(`/cameras/ValidateAlias?alias=${existingCameraAlias}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body._id).to.equal(existingCameraId);
          done();
        });
    });
  });

  describe('#updateCameraAlias', () => {
    it('should return 200 and reflect updated fields', (done) => {
      chai
        .request(SERVICE_URL)
        .put(`/cameras/${createdCamId}/ChangeAlias`)
        .send({
          alias: 'tmp-camera-smoketest-alias-changed',
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body).to.include.all.keys(
            'cotm',
            'isPremium',
            'isDown',
            'message',
            'status',
            'isPrerecorded',
            '_id',
            'alias',
            'title',
            'modifiedDate',
            'playlistUrl',
            'prerecordedClipRecordingTimes',
            '__v',
            'streamUrl',
            'stillUrl',
            'pixelatedStillUrl',
            'rewindBaseUrl',
            'lastPrerecordedClipStartTimeUTC',
            'lastPrerecordedClipEndTimeUTC'
          );
          expect(res.body.cotm).to.equal(false);
          expect(res.body._id).to.match(/[A-Za-z0-9]/);
          expect(res.body.alias).to.equal('tmp-camera-smoketest-alias-changed');
          expect(res.body.lastPrerecordedClipEndTimeUTC).to.match(/^\d{4}-\d{2}-\d{2}/);
          done();
        });
    });
  });

  describe('#logCameraEvent', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .post(`/cameras/${createdCamId}/log`)
        .send({
          event: 'test-event',
          ip: 'xxx.xxx.xxx.xxx',
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body.event).to.equal('test-event');
          expect(res.body.ip).to.equal('xxx.xxx.xxx.xxx');
          done();
        });
    });
  });

  describe('#getCamerasDown', () => {
    it('should return 200 and verify that previously-created cam is down', (done) => {
      chai
        .request(SERVICE_URL)
        .get('/cameras/down')
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          const downCamIds = Array.from(res.body, (camera) => camera._id);
          expect(downCamIds).to.contain(createdCamId);
          done();
        });
    });
  });

  /*
  Note: the below test will perform a delete action on the
  camera record that was created earlier in this script
  */
  describe('#deleteCamera', () => {
    it('should return 200', (done) => {
      chai
        .request(SERVICE_URL)
        .delete(`/cameras/${createdCamId}`)
        .set('Content-Type', 'application/json')
        .send({
          cameraId: createdCamId,
          startTimestampInMs: startDate,
          endTimestampInMs: endDate,
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          done();
        });
    });
  });

  // Clip tests
  describe('#postClip', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .post('/clips')
        .set('Content-Type', 'application/json')
        .send({
          cameraId: existingCameraId,
          startTimestampInMs: startDate,
          endTimestampInMs: endDate,
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          createdClipId = res.body.clipId;
          expect(res.body).to.include.all.keys(
            'thumbnail',
            'status',
            'cameraId',
            'startTimestampInMs',
            'endTimestampInMs',
            'clipId'
          );
          expect(res.body.status).to.match(/[A-Za-z0-9_]/);
          expect(res.body.cameraId).to.equal(existingCameraId);
          expect(res.body.startTimestampInMs).to.equal(startDate);
          expect(res.body.endTimestampInMs).to.equal(endDate);
          done();
        });
    });
  });

  describe('#getClipId', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .get(`/clips/?ids=${createdClipId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body[0]).to.include.all.keys(
            'thumbnail',
            'status',
            'cameraId',
            'startTimestampInMs',
            'endTimestampInMs',
            'clipId'
          );
          expect(res.body[0].status).to.match(/[A-Za-z0-9_]/);
          expect(res.body[0].cameraId).to.equal(existingCameraId);
          expect(res.body[0].startTimestampInMs).to.equal(startDate);
          expect(res.body[0].endTimestampInMs).to.equal(endDate);
          done();
        });
    });
  });

  describe('#updateClip', () => {
    it('should return 200 and reflect updated clip', (done) => {
      chai
        .request(SERVICE_URL)
        .put(`/clips/${createdClipId}`)
        .send({
          clip: {
            url: 'https://clips.cdn-surfline.com/smoketest-dummy-updated-clip.mp4',
            key: '/smoketest-dummy-file.mp4',
          },
          thumbnail: {
            sizes: [300, 640, 1500, 3000],
            url: 'https://clips.cdn-surfline.com/smoketest-dummy-updated-thumbnail.png',
            key: '/smoketest-dummy-file.jpg',
          },
          status: 'CLIP_PENDING',
          cameraId: 'dummy-camera-id',
          startTimestampInMs: 1233456789123,
          endTimestampInMs: 1233456789123,
          bucket: 'smoketest-dummy-bucket',
          clipId: 'dummy-clip-id',
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body.clip.url).to.equal(
            'https://clips.cdn-surfline.com/smoketest-dummy-updated-clip.mp4'
          );
          expect(res.body.clip.key).to.equal('/smoketest-dummy-file.mp4');
          expect(res.body.thumbnail.url).to.equal(
            'https://clips.cdn-surfline.com/smoketest-dummy-updated-thumbnail.png'
          );
          expect(res.body.thumbnail.key).to.equal('/smoketest-dummy-file.jpg');
          expect(res.body.cameraId).to.equal('dummy-camera-id');
          expect(res.body.startTimestampInMs).to.equal(1233456789123);
          expect(res.body.endTimestampInMs).to.equal(1233456789123);
          expect(res.body.bucket).to.equal('smoketest-dummy-bucket');
          done();
        });
    });
  });

  // Recording tests
  describe('#postRecording', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .post(`/cameras/recording/id/${existingCameraId}`)
        .send({
          startDate: `${startDate}`,
          endDate: `${endDate}`,
          thumbLargeUrl: 'http://stills.cdn-surfline.com/a-test/large.jpg',
          alias: `${existingCameraAlias}`,
          // recordingUrl value must be unique
          recordingUrl: `http://stills.cdn-surfline.com/a-test/${startDate}.mp4`,
          thumbSmallUrl: 'http://stills.cdn-surfline.com/a-test/small.jpg',
          resolution: '1280x720',
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body).to.include.all.keys(
            '_id',
            'startDate',
            'endDate',
            'thumbLargeUrl',
            'alias',
            'recordingUrl',
            'thumbSmallUrl',
            'resolution',
            'cameraId',
            'expireSoft',
            '__v'
          );
          expect(res.body.startDate).to.equal(`${new Date(startDate).toISOString()}`);
          expect(res.body.endDate).to.equal(`${new Date(endDate).toISOString()}`);
          expect(res.body.thumbLargeUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body.alias).to.equal(existingCameraAlias);
          expect(res.body.recordingUrl).to.match(/^https?:\/\/(\w*.)*.mp4$/);
          expect(res.body.thumbSmallUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body.cameraId).to.equal(existingCameraId);
          done();
        });
    });
  });

  describe('#getRecordingByAlias', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .get(`/cameras/recording/${existingCameraAlias}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body[0]).to.include.all.keys(
            'startDate',
            'endDate',
            'thumbLargeUrl',
            'alias',
            'recordingUrl',
            'thumbSmallUrl',
            'cameraId'
          );
          expect(res.body[0].startDate).to.equal(`${new Date(startDate).toISOString()}`);
          expect(res.body[0].endDate).to.equal(`${new Date(endDate).toISOString()}`);
          expect(res.body[0].thumbLargeUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body[0].alias).to.equal(existingCameraAlias);
          expect(res.body[0].recordingUrl).to.match(/^https?:\/\/(\w*.)*.mp4$/);
          expect(res.body[0].thumbSmallUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body[0].cameraId).to.equal(existingCameraId);
          done();
        });
    });
  });

  describe('#getRecordingById', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .get(`/cameras/recording/id/${existingCameraId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body[0]).to.include.all.keys(
            'startDate',
            'endDate',
            'thumbLargeUrl',
            'alias',
            'recordingUrl',
            'thumbSmallUrl',
            'cameraId'
          );
          expect(res.body[0].startDate).to.equal(`${new Date(startDate).toISOString()}`);
          expect(res.body[0].endDate).to.equal(`${new Date(endDate).toISOString()}`);
          expect(res.body[0].thumbLargeUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body[0].alias).to.equal(existingCameraAlias);
          expect(res.body[0].recordingUrl).to.match(/^https?:\/\/(\w*.)*.mp4$/);
          expect(res.body[0].thumbSmallUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body[0].cameraId).to.equal(existingCameraId);
          done();
        });
    });
  });

  describe('#getRecordingsWithPartialMatches', () => {
    it('should return 200 and contain expected response', (done) => {
      chai
        .request(SERVICE_URL)
        .get(
          '/cameras' +
            '/recording' +
            '/id' +
            `/${existingCameraId}` +
            `?startDate=${startDate}` +
            `&endDate=${endDate}` +
            '&allowPartialMatch=true'
        )
        .end((err, res) => {
          expect(res).to.have.status(200);
          if (err) {
            throw err;
          }
          expect(res.body[0]).to.include.all.keys(
            'startDate',
            'endDate',
            'thumbLargeUrl',
            'alias',
            'recordingUrl',
            'thumbSmallUrl',
            'cameraId'
          );
          expect(res.body[0].startDate).to.equal(`${new Date(startDate).toISOString()}`);
          expect(res.body[0].endDate).to.equal(`${new Date(endDate).toISOString()}`);
          expect(res.body[0].thumbLargeUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body[0].alias).to.equal(existingCameraAlias);
          expect(res.body[0].recordingUrl).to.match(/^https?:\/\/(\w*.)*.mp4$/);
          expect(res.body[0].thumbSmallUrl).to.match(/^https?:\/\/(\w*.)*.jpg$/);
          expect(res.body[0].cameraId).to.equal(existingCameraId);
          done();
        });
    });
  });
});
