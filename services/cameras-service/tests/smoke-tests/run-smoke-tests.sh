#!/bin/bash
set -euxo pipefail

echo 'Building smoketest container:'
docker build --target test -t cameras-service:test .

echo 'Running smoketest container:'
docker run --env SERVICE_URL="${SERVICE_URL}" cameras-service:test npm run test:smoke
