# Installing S3Upload Plugin

To install this plugin in a recoder instance, follow [this steps](https://www.wowza.com/docs/how-to-upload-recorded-media-to-an-amazon-s3-bucket-modules3upload).

Note: you will need an AWS access key and secret key.
