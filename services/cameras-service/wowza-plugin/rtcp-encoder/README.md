# Module wowza-rtptime-segmenting

### Install Apache Maven:

```
$ sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
$ sudo sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
$ sudo yum install -y apache-maven
$ mvn --version
```

### Build:
```
$ cp  /home/wowza/lib/wms-server.jar  wowza-rtptime-segmenting/lib/
$ mvn clean
$ mvn package
```

### Install:
```
$ cp wowza-rtptime-segmenting-0.9.jar /home/wowza/lib/
```

### Configuration:

Configure Application.xml inserting into element `Application / RTP / Properties`:

```
	<Property>
		<Name>rtpDePacketizerWrapper</Name>
		<Value>net.random.wowza.RTPTimestampRTPDepacketizerWrapper</Value>
	</Property>
```

Insert into `Application / Modules` element, as last module:

```
	<Module>
		<Name>ModuleLiveStreamRecordSplitter</Name>
		<Description>ModuleLiveStreamRecordSplitter</Description>
		<Class>net.random.wowza.ModuleLiveStreamRecordSplitter</Class>
	</Module>
```

To use default settings, the below properties do not have to be set (module uses defaults automatically).

Module supports the following settings, insert into Application / Properties:

```
	<!-- LSRS-streamNameFilter possible values:
		* 		process all incoming streams (default)
		*suffix 	procress all streams with streamname ending with suffix
		prefix* 	process all streams starting with prefix
		stream1.stream 	process all streams matching the fully qualified streamname
		regExp		processing all streamnames matching the regular expression
	-->
	<Property>
		<Name>LSRS-streamNameFilter</Name>
		<Value>*</Value>
		<Type>String</Type>
	</Property>

	<!-- LSRS-adjustSegmentStartTimeToSourceNTP
		if set to false(default), will leave triggering of segmenting to wowza cron
		if set to true, module will trigger segmenting itselves and use setting from LSRS-splitIntervalSeconds for segment length
		Recommended setting: false (less intrusive)
	-->
	<Property>
		<Name>LSRS-adjustSegmentStartTimeToSourceNTP</Name>
		<Value>false</Value>
		<Type>Boolean</Type>
	</Property>
	<Property>
		<Name>LSRS-splitIntervalSeconds</Name>
		<Value>600</Value>
		<Type>Integer</Type>
	</Property>
```


### How does it work?

For all streams matched by the streamNameFilter (default: *), it will:
- intercept RTP and RTCP packets and calculate source NTP time for current RTP packets based on NTP time from RTCP SR events
- intercept created recorders, and set a custom fileVersionDelegate
- if LSRS-adjustSegmentStartTimeToSourceNTP is true, also disable the cron schedule on the recorder and steer triggering of the segmenting by itselves
- it will then use current source NTP time for RTP packets for the file naming of the saved streams

Note: the file name/timestamp for the very first, initial file created for an incoming stream will be based on host system time (not RTP time), because at this time, timestamps from RTP/RTCP packets are not yet available...


