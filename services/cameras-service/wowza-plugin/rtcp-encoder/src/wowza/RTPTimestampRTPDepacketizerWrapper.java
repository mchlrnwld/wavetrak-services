
package net.random.wowza;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.net.SocketAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.application.WMSProperties;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.rtp.depacketizer.IRTPDePacketizer;
import com.wowza.wms.rtp.depacketizer.IRTPDePacketizerWrapper;
import com.wowza.wms.rtp.depacketizer.RTPDePacketizerItem;
import com.wowza.wms.rtp.depacketizer.RTPReceiverReportHandler;
import com.wowza.wms.rtp.model.RTPContext;
import com.wowza.wms.rtp.model.RTPTrack;

/**
 * A IRTPDePacketizerWrapper that will parse RTCP SR and RTP packets to get a mapping of source NTP to RTP time
 * Will write NTP ts of RTP packets into stream property 'rtpNTP'
 * Will also notify ModuleLiveStreamRecordSplitter of rtpNTP timestamps
 * 
 *
 */
public class RTPTimestampRTPDepacketizerWrapper implements IRTPDePacketizerWrapper {

	// RTCP packet types
    public static final int RTCP_SR = 200;
    public static final int RTCP_RR = 201;
    public static final int RTCP_SDES = 202;
    public static final int RTCP_BYE = 203;
    public static final int RTCP_APP = 204;
    public static final int RTCP_COMPOUND = -1;	
	
    // baseline NTP time if bit-0=0 -> 7-Feb-2036 @ 06:28:16 UTC
    private static final long MSB_0_BASE_TIME = 2085978496000L;

    // baseline NTP time if bit-0=1 -> 1-Jan-1900 @ 01:00:00 UTC
    public static final long MSB_1_BASE_TIME = -2208988800000L;
	
	
    private volatile IRTPDePacketizer rtpDePacketizer = null;
	
	private Map<String, Long[]> lastStreamRTPtoNTPTimestamps = Collections.synchronizedMap(new HashMap<String, Long[]>());


	public RTPTimestampRTPDepacketizerWrapper() {
		WMSLoggerFactory.getLogger(this.getClass()).info("[RTPTimestampRTPDepacketizerWrapper] RTPTimestampRTPDepacketizerWrapper() ");
	}

	
	/**
	 * Called by Wowza API, IRTPDePacketizerWrapper
	 */
	public void handleRTCPPacket(SocketAddress socketAddr, RTPTrack rtpTrack, byte[] bytes, int boffset, int len) {
		this.rtpDePacketizer.handleRTCPPacket(socketAddr, rtpTrack, bytes, boffset, len);
		// parse data
        try {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes, boffset, len));
        	int length;
            for (int offset = 0; offset < len; offset += length) {
                // version (2 bits), padding (1 bit), 5 bits of data
                int firstbyte = in.readUnsignedByte();
                // version must be 2.
                if ((firstbyte & 0xc0) != 128) {
                	return;
                }
                int type = in.readUnsignedByte(); // 8 bits
                // length: 16 bits The length of this RTCP packet in 32-bit
                // words minus one, including the header and any padding.
                length = in.readUnsignedShort();
                // packet length in bytes
                length = length + 1 << 2;
                if (offset + length > len) {
                	return;
                }

                // find padding length, if any.
                int padlen = 0;
                if (offset + length == len) {
                    if ((firstbyte & 0x20) != 0) {
                        // packet has padding.
                        padlen = bytes[boffset + len - 1] & 0xff;
                        if (padlen == 0) {
                            return; 
                        }
                    }
                } else if ((firstbyte & 0x20) != 0) {
                    return;
                }
                int inlength = length - padlen;

                firstbyte &= 0x1f;
                switch (type) {
                    case RTCP_SR:
                        if (inlength != 28 + 24 * firstbyte) {
                        	return;
                        }
                        in.readInt(); // ssrc
                        long ntptimestampmsw = in.readInt() & 0xffffffffL;
                        long ntptimestamplsw = in.readInt() & 0xffffffffL;
                        long rtptimestamp = in.readInt() & 0xffffffffL;
                        in.readInt() /*& 0xffffffffL*/; // packetcount
                        in.readInt() /*& 0xffffffffL*/; // octetcount
                        long fullNTPTime = getTimestamp(ntptimestampmsw, ntptimestamplsw);
                        if (WMSLoggerFactory.getLogger(this.getClass()).isDebugEnabled()) {
                        	WMSLoggerFactory.getLogger(this.getClass()).debug("[RTPTimestampRTPDepacketizerWrapper] StreamName "+rtpTrack.getStream().getStreamName()+": RTCP SR - timestamp: "+fullNTPTime+", rtp timestamp: "+rtptimestamp);
                        }
            			String key = rtpTrack.getStream().getAppName()+"/"+rtpTrack.getStream().getStreamName();
            			Long[] lastTS = new Long[]{fullNTPTime, rtptimestamp};
            			lastStreamRTPtoNTPTimestamps.put(key, lastTS);
            			return;
                    case RTCP_RR:
                    	return;
                    case RTCP_SDES:
                    	return;
                    case RTCP_BYE:
                    	return;
                    case RTCP_APP:
                    	return;
                    default:
                        return;
                }
            }
        } catch (Exception ign) {
        	//WMSLoggerFactory.getLogger(this.getClass()).error("ERROR: "+ign.getMessage(), ign);
        }
    }

 
	/**
	 * Called by Wowza API, IRTPDePacketizerWrapper
	 */
	public void handleRTPPacket(SocketAddress socketAddr, RTPTrack rtpTrack, byte[] bytes, int offset, int len) {
		this.rtpDePacketizer.handleRTPPacket(socketAddr, rtpTrack, bytes, offset, len);
		// only interested in video
		if (rtpTrack.getRTPMapType() != RTPTrack.RTPMAPTYPE_H264) {
			return;
		}
		// get RTP timestamp
        try {
            DataInputStream datainputstream = new DataInputStream(new ByteArrayInputStream(bytes, offset, len));
            int firstByte = datainputstream.readUnsignedByte();
            if ((firstByte & 0xc0) != 128) {
                return; //throw new BadFormatException();
            }  
            datainputstream.readUnsignedByte(); // payload type
            datainputstream.readUnsignedShort(); // seq number
            long timestamp = datainputstream.readInt() & 0xffffffffL;
            int scale = rtpTrack.getTimescale();
            Long[] lastTS = lastStreamRTPtoNTPTimestamps.get(rtpTrack.getStream().getAppName()+"/"+rtpTrack.getStream().getStreamName());
            if (lastTS != null) {
            	long rtpDeltaMillis = (timestamp - lastTS[1]) / (scale / 1000);
            	long rtpNTP = lastTS[0] + rtpDeltaMillis;
            	rtpTrack.getStream().getStream().getProperties().put("rtpNTP", rtpNTP);
            	ModuleLiveStreamRecordSplitter splitter = ModuleLiveStreamRecordSplitter.getModuleLiveStreamRecordSplitter(rtpTrack.getStream().getAppName());
            	if (splitter != null) {
            		splitter.notifyRTPAbsoluteTimestamp(rtpTrack.getStream().getStream(), rtpNTP);
            	} else {
            		//WMSLoggerFactory.getLogger(this.getClass()).info("No splitter for app: "+rtpTrack.getStream().getAppName());
            		
            	}
            	//WMSLoggerFactory.getLogger(this.getClass()).info("RTP absolute diff:"+(System.currentTimeMillis()-rtpNTP));
            }
			//WMSLoggerFactory.getLogger(this.getClass()).info("[RTPTimestampRTPDepacketizerWrapper] RTP timestamp: "+timestamp);
        } catch (Exception ign) {
        	//WMSLoggerFactory.getLogger(this.getClass()).error("ERROR: "+ign.getMessage(), ign);
        }		
	}
	
	/**
	 * Generate NTP timestamp from RTCP SR timestamp ms and ls bytes
	 * @param ntpTimestampMSW
	 * @param ntpTimestampLSW
	 * @return timestamp in milliseconds
	 */
    public long getTimestamp(long ntpTimestampMSW, long ntpTimestampLSW) {
        long seconds = ntpTimestampMSW;
        long fraction = ntpTimestampLSW;
        // Use round-off on fractional part to preserve going to lower precision
        fraction = Math.round(1000D * fraction / 0x100000000L);
        long msb = seconds & 0x80000000L;
        if (msb == 0) {
            // use base: 7-Feb-2036 @ 06:28:16 UTC
            return MSB_0_BASE_TIME + (seconds * 1000) + fraction;
        }
        // use base: 1-Jan-1900 @ 01:00:00 UTC
        return MSB_1_BASE_TIME + (seconds * 1000) + fraction;
    }	


	public WMSProperties getProperties() {
		return rtpDePacketizer.getProperties();
	}

	public boolean canHandle(RTPTrack rtpTrack) {
		return this.rtpDePacketizer.canHandle(rtpTrack);
	}

	public void init(RTPContext rtpContext, RTPDePacketizerItem rtpDePacketizerItem) {
		this.rtpDePacketizer.init(rtpContext, rtpDePacketizerItem);
	}

	public void setProperties(WMSProperties properties) {
		this.rtpDePacketizer.setProperties(properties);
	}

	public void shutdown(RTPTrack rtpTrack) {
		this.rtpDePacketizer.shutdown(rtpTrack);
	}

	public void startup(RTPTrack rtpTrack) {
		this.rtpDePacketizer.startup(rtpTrack);
	}

	public void setDePacketizer(IRTPDePacketizer rtpDePacketizer) {
		this.rtpDePacketizer = rtpDePacketizer;
	}

	public RTPReceiverReportHandler getReceiverReportHandler() {
		return rtpDePacketizer.getReceiverReportHandler();
	}


	public void onAppStop(IApplicationInstance arg0) {
		// nothing to do
	}


}