package net.random.wowza;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.wowza.wms.livestreamrecord.manager.IStreamRecorder;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorderFileVersionDelegate;
import com.wowza.wms.logging.WMSLoggerFactory;

/**
 * An IStreamRecorderFileVersionDelegate that will utilize timestamps read from stream property 'rtpNTP'
 * for file naming
 * Depends on RTPTimestampRTPDepacketizerWrapper to write the timestamps into stream properties
 * 
 *
 */
public class SplitFileVersionDelegate implements IStreamRecorderFileVersionDelegate {
	
	// e.g.: 2018-04-25-13.01.05.509-MESZ
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSS-z", Locale.getDefault());
	
	public SplitFileVersionDelegate() {
		
	}
	
	public String getFilename(IStreamRecorder recorder) {
		
		String name;
		try {
			File baseFile = new File(recorder.getBaseFilePath());
			String oldBasePath = baseFile.getParent();
			Long rtpNTP = (Long)recorder.getStream().getProperties().get("rtpNTP");
			if (rtpNTP == null) {
				WMSLoggerFactory.getLogger(this.getClass()).info("[SplitFileVersionDelegate] no rtpNTP in stream-properties, falling back to system time");
				rtpNTP = System.currentTimeMillis(); // might be used for initial segment
			}
			
			String suffix = "";
			if (baseFile.getName().indexOf(".") != -1) {
				suffix = baseFile.getName().substring(baseFile.getName().lastIndexOf("."));
			}
			name = oldBasePath+"/"+recorder.getStreamName()+"_"+sdf.format(new Date(rtpNTP))+suffix;
			File file = new File(name);
			if (file.exists()) {
				file.delete();
			}
			WMSLoggerFactory.getLogger(this.getClass()).info("[SplitFileVersionDelegate] naming file: "+name);
		} catch (Exception e) {
			WMSLoggerFactory.getLogger(this.getClass()).error("[SplitFileVersionDelegate] getFilename(): "+e.getMessage(), e);
			// return a temp filename
			name = "err.tmp";
		}
		return name;
	}
}