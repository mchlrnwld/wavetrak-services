package net.random.wowza;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.wowza.wms.application.WMSProperties;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorder;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorderFileVersionDelegate;
import com.wowza.wms.logging.WMSLoggerFactory;

/**
 * An IStreamRecorderFileVersionDelegate that will utilize timestamps read from stream property 'rtpNTP'
 * for file naming
 * Depends on RTPTimestampRTPDepacketizerWrapper to write the timestamps into stream properties
 *
 *
 */
public class SplitFileVersionDelegate implements IStreamRecorderFileVersionDelegate {

	String defaultDateFormat = "yyyyMMdd'T'HHmmssSSS"; // default
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmssSSS", Locale.getDefault());

	public SplitFileVersionDelegate() {
		this((WMSProperties)null);
	}

	/**
	 * Constructor
	 * @param props application properties to be configured from
	 */
	public SplitFileVersionDelegate(WMSProperties props) {
		String dateFormat = null;
		String timeZoneStr = null;
		if (props != null) {
	     	dateFormat = props.getPropertyStr("LSRS-dateFormat", dateFormat);
	     	timeZoneStr = props.getPropertyStr("LSRS-timeZone", null);
		}
		sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
		TimeZone timeZone = TimeZone.getDefault();
		if (timeZoneStr != null) {
			try {
				timeZone = TimeZone.getTimeZone(timeZoneStr);
			} catch (Exception e) {
				WMSLoggerFactory.getLogger(this.getClass()).debug("[SplitFileVersionDelegate] Error getting timezone for string '"+timeZoneStr+"': "+e.getMessage()+", will use default timezone.");
			}
		}
		sdf.setTimeZone(timeZone);
	}

	/**
	 * IStreamRecorderFileVersionDelegate method used to generate filename for recorder segment
	 */
	public String getFilename(IStreamRecorder recorder) {

		String name;
		try {
			File baseFile = new File(recorder.getBaseFilePath());
			String oldBasePath = baseFile.getParent();
			Long rtpNTP = (Long)recorder.getStream().getProperties().get("rtpNTP");
			if (rtpNTP == null) {
				WMSLoggerFactory.getLogger(this.getClass()).info("[SplitFileVersionDelegate] no rtpNTP in stream-properties, falling back to system time");
				rtpNTP = System.currentTimeMillis(); // might be used for initial segment
			}

			String suffix = "";
			if (baseFile.getName().indexOf(".") != -1) {
				suffix = baseFile.getName().substring(baseFile.getName().lastIndexOf("."));
			}
			Date date = new Date(rtpNTP);

			name = oldBasePath+"/"+recorder.getStreamName()+"."+sdf.format(date)+suffix;

			File file = new File(name);
			if (file.exists()) {
				file.delete();
			}
			WMSLoggerFactory.getLogger(this.getClass()).info("[SplitFileVersionDelegate] naming file: "+name);
		} catch (Exception e) {
			WMSLoggerFactory.getLogger(this.getClass()).error("[SplitFileVersionDelegate] getFilename(): "+e.getMessage(), e);
			// return a temp filename
			name = "err.tmp";
		}
		return name;
	}
}
