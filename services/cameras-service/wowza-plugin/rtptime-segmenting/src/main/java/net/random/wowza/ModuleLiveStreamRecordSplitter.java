package net.random.wowza;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.livestreamrecord.manager.ILiveStreamRecordManagerActionNotify;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorder;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorderActionNotify;
import com.wowza.wms.livestreamrecord.manager.StreamRecorder;
import com.wowza.wms.livestreamrecord.manager.StreamRecorderParameters;
import com.wowza.wms.module.IModuleOnApp;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.stream.IMediaStream;
import com.wowza.wms.vhost.IVHost;

/**
 * Wowza module intercepting created recoders. Used to have segmenting filenames be based on source RTP timestamps
 *
 *
 */
public class ModuleLiveStreamRecordSplitter extends ModuleBase implements IModuleOnApp, IStreamRecorderActionNotify, ILiveStreamRecordManagerActionNotify {


	private IApplicationInstance appInstance;
	private IVHost vhost;

	private int splitIntervalSeconds = 60*10;
	private String streamNameFilter = "*";
	private boolean adjustSegmentStartTimeToSourceNTP = false;

	private Map<String, Long> nextSplitTimes = Collections.synchronizedMap(new HashMap<String, Long>());

	private static Map<String, ModuleLiveStreamRecordSplitter> splitters = new HashMap<String, ModuleLiveStreamRecordSplitter>();


	public void onAppStart(final IApplicationInstance appInstance) {
		getLogger().info("[ModuleLiveStreamRecordSplitter] onAppStart(), app "+appInstance.getApplication().getName());
     	this.appInstance = appInstance;
     	this.vhost = appInstance.getVHost();
     	streamNameFilter = appInstance.getProperties().getPropertyStr("LSRS-streamNameFilter", "*");
     	splitIntervalSeconds = appInstance.getProperties().getPropertyInt("LSRS-splitIntervalSeconds", 60*10);
     	adjustSegmentStartTimeToSourceNTP = appInstance.getProperties().getPropertyBoolean("LSRS-adjustSegmentStartTimeToSourceNTP", false);

     	splitters.put(appInstance.getApplication().getName(), this);

     	// removed, will start recorder
     	//StreamRecorderParameters recordParams = new StreamRecorderParameters(appInstance);
		//recordParams.notifyListener = this;
		//getLogger().info("[ModuleLiveStreamRecordSplitter] application recorder params :"+recordParams);
		// vhost.getLiveStreamRecordManager().startRecording(appInstance, recordParams);

     	// instead, just add us as listener
     	vhost.getLiveStreamRecordManager().addListener(this);
	}

	public void notifyRTPAbsoluteTimestamp(IMediaStream stream, long ts) {
		if (!adjustSegmentStartTimeToSourceNTP) {
			// split not triggered by module
			return;
		}
		// decide on if split to be triggered
		Long nextSplitTime = nextSplitTimes.get(stream.getName());
		//getLogger().info("[ModuleLiveStreamRecordSplitter] ts: "+ts+", next split: "+nextSplitTime);

		if (nextSplitTime != null && ts > nextSplitTime) {
			// split
			getLogger().info("[ModuleLiveStreamRecordSplitter] splitting stream "+stream.getName()+" at "+ts);
			nextSplitTimes.put(stream.getName(), getNextSplitTime(nextSplitTime));
			IStreamRecorder rec = vhost.getLiveStreamRecordManager().getRecorder(appInstance, stream.getName());
			if (rec == null) {
				getLogger().info("[ModuleLiveStreamRecordSplitter] Could not retrieve recorder for stream "+stream.getName());
				return;
			}
			rec.splitRecorder();
		}
	}


	private long getNextSplitTime(long lastSplitTime) {
		if (lastSplitTime >= 0) {
			return lastSplitTime + (splitIntervalSeconds*1000L);
		} else {
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(new Date(System.currentTimeMillis()));
			// roll to next 10-minutes
			cal.add(Calendar.SECOND, 60 - cal.get(Calendar.SECOND));
			cal.add(Calendar.MINUTE, 10 - (cal.get(Calendar.MINUTE) % 10));
			return cal.getTimeInMillis();
		}
	}

	public static ModuleLiveStreamRecordSplitter getModuleLiveStreamRecordSplitter(String appName) {
		return splitters.get(appName);
	}


	// IStreamRecorderActionNotify

	public void onCreateRecorder(IStreamRecorder recorder) {
		String streamName = recorder.getStreamName();
		if (shouldHandleStream(streamName)) {
			getLogger().info("[ModuleLiveStreamRecordSplitter] streamName "+streamName+" matching streamNameFilter "+streamNameFilter);
			StreamRecorderParameters params = recorder.getRecorderParams();
			params.fileVersionDelegate = new SplitFileVersionDelegate();
			// do we have to use startOnKeyFrame=false, or will delayed call to fileVersionTemplate take care of ts offset?
	     	// --> it seems not, as fileVersionTemplate will permanently be updated with TS, and be called on-demand if iframe hit
	     	//recordParams.startOnKeyFrame = false;
			if (adjustSegmentStartTimeToSourceNTP) {
				// unset cron from application config...
		     	params.segmentSchedule = "0 0 1 1 * 2050";
				long nextSplitTime = getNextSplitTime(-1);
				getLogger().info("[ModuleLiveStreamRecordSplitter] Initial split time for stream "+streamName+": "+nextSplitTime);
				nextSplitTimes.put(streamName, getNextSplitTime(-1));
			}
		} else {
			getLogger().info("[ModuleLiveStreamRecordSplitter] streamName "+streamName+" not matching streamNameFilter "+streamNameFilter);
		}
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onCreateRecorder[" + appInstance.getContextStr() + "]: new Recording created:" + recorder.getStreamName());
	}

	private boolean shouldHandleStream(String streamName) {
		boolean recordStream = false;
		if (streamNameFilter == null) {
			return false;
		} else if ("*".equals(streamNameFilter)) {
			recordStream = true;
		} else if (streamNameFilter.equals(streamName)) {
			recordStream = true;
		} else if (streamNameFilter.startsWith("*") && streamName.endsWith(streamNameFilter.substring(1))) {
			recordStream = true;
		} else if (streamNameFilter.endsWith("*") && streamName.startsWith(streamNameFilter.substring(0, streamNameFilter.length()-1))) {
			recordStream = true;
		} else if (streamName.matches(streamNameFilter)) {
			recordStream = true;
		}
		return recordStream;
	}



	public void onStartRecorder(IStreamRecorder recorder) {
	    // log where the recording is going to being written
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onStartRecorder[" + appInstance.getContextStr() + "]: new Recording started:" + recorder.getStreamName() + " " + recorder.getFilePath());
	}

	public void onSplitRecorder(IStreamRecorder recorder) {
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onSplitRecorder[" + appInstance.getContextStr() + "]: Segment recording:" + recorder.getStreamName());
	}

	public void onStopRecorder(IStreamRecorder recorder) {
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onStopRecorder[" + appInstance.getContextStr() + "]: Recording stopped:" + recorder.getStreamName() + " " + recorder.getCurrentFile());
	}

	public void onSwitchRecorder(IStreamRecorder recorder, IMediaStream newStream) {
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onSwitchRecorder[" + appInstance.getContextStr() + "]: switch to new stream, old Stream:" + recorder.getStreamName() +" new Stream:" + newStream.getName());
	}

	public void onSegmentStart(IStreamRecorder recorder) {
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onSegmentStart[" + appInstance.getContextStr() + "]: new segment created:" + recorder.getStreamName());
	}

	public void onSegmentEnd(IStreamRecorder recorder) {
		getLogger().info("[ModuleLiveStreamRecordSplitter] .onSegmentEnd[" + appInstance.getContextStr() + "]: segment closed:" + recorder.getStreamName());
	}


	// ILiveStreamRecordManagerActionNotify

	public void onCreateRecord(IStreamRecorder recorder) {

	}

	public void onSplitRecord(IStreamRecorder recorder) {

	}

	public void onStartRecord(IStreamRecorder recorder) {

	}

	public void onStopRecord(IStreamRecorder recorder) {

	}

	public void onSwitchRecord(IStreamRecorder recorder, IMediaStream stream) {

	}

	public IStreamRecorder recordFactory(String streamName, StreamRecorderParameters params) {
		// add us as listener
		params.notifyListener = this;
		getLogger().info("[ModuleLiveStreamRecordSplitter] application recorder params: "+params);
		StreamRecorder recorder = new StreamRecorder(appInstance, streamName, params);
		return recorder;
	}

	public void onAppStop(IApplicationInstance arg0) {
		// nothing to do
	}




}
