

Module wowza-rtptime-segmenting

Building:
1. Copy wms-server.jar from current Wowza/lib dir to wowza-rtptime-segmenting/lib/wms-server.jar (required as build dependency)
2. Build:
mvn clean
mvn package


Installation:
1. Copy wowza-rtptime-segmenting-$VERSION.jar into wowza /lib folder

2. Configuration
You can also configure all of below using the Wowza Manager webapp.

To configure using the XML directly, in Application.xml for the application to use the feature:
Insert into element Application / RTP / Properties:
	<Property>
		<Name>rtpDePacketizerWrapper</Name>
		<Value>net.random.wowza.RTPTimestampRTPDepacketizerWrapper</Value>
	</Property>

Insert into Application / Modules element, as last module:
	<Module>
		<Name>ModuleLiveStreamRecordSplitter</Name>
		<Description>ModuleLiveStreamRecordSplitter</Description>
		<Class>net.random.wowza.ModuleLiveStreamRecordSplitter</Class>
	</Module>


To use default settings, the below properties do not have to be set (module uses defaults atomatically).
Module supports the following settings, insert into Application / Properties:
	<!-- LSRS-streamNameFilter possible values:
		* 		process all incoming streams (default)
		*suffix 	procress all streams with streamname ending with suffix
		prefix* 	process all streams starting with prefix
		stream1.stream 	process all streams matching the fully qualified streamname
		regExp		processing all streamnames matching the regular expression
	-->
	<Property>
		<Name>LSRS-streamNameFilter</Name>
		<Value>*</Value>
		<Type>String</Type>
	</Property>

	<!-- LSRS-dateFormat
	     Java SimpleDateFormat format of date-part of the filename. See e.g.: https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
	     Default: yyyyMMdd'T'HHmmssSSS
	-->
	<Property>
		<Name>LSRS-dateFormat</Name>
		<Value>yyyyMMdd'T'HHmmssSSS</Value>
		<Type>String</Type>
	</Property>

	<!-- LSRS-timeZone
	     Set the timezone to use to generate the filename date string.
	     Default: use local time
	-->
	<Property>
		<Name>LSRS-timeZone</Name>
		<Value>UTC</Value>
		<Type>String</Type>
	</Property>


	<!-- LSRS-adjustSegmentStartTimeToSourceNTP
		if set to false(default), will leave triggering of segmenting to wowza cron
		if set to true, module will trigger segmenting itselves and use setting from LSRS-splitIntervalSeconds for segment length
		Recommended setting: false (less intrusive)
	-->
	<Property>
		<Name>LSRS-adjustSegmentStartTimeToSourceNTP</Name>
		<Value>false</Value>
		<Type>Boolean</Type>
	</Property>
	<Property>
		<Name>LSRS-splitIntervalSeconds</Name>
		<Value>600</Value>
		<Type>Integer</Type>
	</Property>

3. How does it work:
For all streams matched by the streamNameFilter (default: *), it will:
- intercept RTP and RTCP packets and calculate source NTP time for current RTP packets based on NTP time from RTCP SR events
- intercept created recorders, and set a custom fileVersionDelegate
- if LSRS-adjustSegmentStartTimeToSourceNTP is true, also disable the cron schedule on the recorder and steer triggering of the segmenting by itselves
- it will then use current source NTP time for RTP packets for the file naming of the saved streams

Note: the file name/timestamp for the very first, initial file created for an incoming stream will be based on host system time (not RTP time), because at this time, timestamps from RTP/RTCP packets are not yet available...



