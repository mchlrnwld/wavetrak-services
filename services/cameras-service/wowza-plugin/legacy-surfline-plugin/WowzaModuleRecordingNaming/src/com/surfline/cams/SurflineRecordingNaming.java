package com.surfline.cams;


import com.wowza.wms.amf.AMFPacket;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.application.WMSProperties;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorder;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorderConstants;
import com.wowza.wms.livestreamrecord.manager.IStreamRecorderFileVersionDelegate;
import com.wowza.wms.livestreamrecord.manager.StreamRecorderParameters;
import com.wowza.wms.media.model.MediaCodecInfoAudio;
import com.wowza.wms.media.model.MediaCodecInfoVideo;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.stream.IMediaStream;
import com.wowza.wms.stream.IMediaStreamActionNotify3;
import com.wowza.wms.vhost.IVHost;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class SurflineRecordingNaming extends ModuleBase {

    private IApplicationInstance appInstance = null;
    private IVHost vhost = null;
    private String recordType = IStreamRecorderConstants.FORMAT_MP4;
    private String versioningOption = IStreamRecorderConstants.APPEND_FILE;
    private String segmentationType = IStreamRecorderConstants.SEGMENT_NONE;
    private long segmentSize = 30000L;
    private String schedule = "*/10 * * * *";
    private int duration = 60000 * 10; //10 min
    private boolean startOnKeyFrame = false;
    private SimpleDateFormat sdf = new SimpleDateFormat(".yyyyMMdd'T'HHmmss");


    public void onAppStart(IApplicationInstance appInstance) {

        getLogger().info("APP: module loaded");
        this.appInstance = appInstance;
        this.vhost = appInstance.getVHost();

        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        versioningOption = appInstance.getProperties().getPropertyStr("autoRecordOverwrite", IStreamRecorderConstants.APPEND_FILE);
        if (!versioningOption.equalsIgnoreCase(IStreamRecorderConstants.APPEND_FILE)) {
            versioningOption = IStreamRecorderConstants.OVERWRITE_FILE;
        }

        startOnKeyFrame = appInstance.getProperties().getPropertyBoolean("autoRecordStartOnKeyFrame", this.startOnKeyFrame);

        segmentationType = appInstance.getProperties().getPropertyStr("autoRecordSegment", IStreamRecorderConstants.SEGMENT_NONE);
        if (!segmentationType.equalsIgnoreCase(IStreamRecorderConstants.SEGMENT_BY_DURATION) &&
                !segmentationType.equalsIgnoreCase(IStreamRecorderConstants.SEGMENT_BY_SCHEDULE) &&
                !segmentationType.equalsIgnoreCase(IStreamRecorderConstants.SEGMENT_NONE)
                ) {
            segmentationType = IStreamRecorderConstants.SEGMENT_NONE;
        }

        if (segmentationType.equalsIgnoreCase(IStreamRecorderConstants.SEGMENT_BY_SIZE)) {
            this.segmentSize = appInstance.getProperties().getPropertyLong("autoRecordSegmentSize", this.segmentSize);
            if (this.segmentSize <= 0) {
                this.segmentSize = 30000L;
            }
        } else if (segmentationType.equalsIgnoreCase(IStreamRecorderConstants.SEGMENT_BY_SCHEDULE)) {
            this.schedule = appInstance.getProperties().getPropertyStr("autoRecordSchedule", this.schedule);
        } else if (segmentationType.equalsIgnoreCase(IStreamRecorderConstants.SEGMENT_BY_DURATION)) {
            this.duration = appInstance.getProperties().getPropertyInt("autoRecordDuration", this.duration);
        }
    }

    public void onAppStop(IApplicationInstance appInstance) {
        getLogger().info("SurflineRecordingNaming: onAppStop");
    }

    class StreamListener implements IMediaStreamActionNotify3 {
        IVHost vhost = null;
        IApplicationInstance appInstance = null;

        public StreamListener(IVHost ivhost, IApplicationInstance appins) {
            this.vhost = ivhost;
            this.appInstance = appins;
        }

        public void onMetaData(IMediaStream stream, AMFPacket metaDataPacket) {
        }

        public void onPauseRaw(IMediaStream stream, boolean isPause, double location) {
        }

        public void onPause(IMediaStream stream, boolean isPause, double location) {
        }

        public void onPlay(IMediaStream stream, String streamName, double playStart, double playLen, int playReset) {
        }

        public void onSeek(IMediaStream stream, double location) {
        }

        public void onStop(IMediaStream stream) {
        }

        public void onCodecInfoAudio(IMediaStream stream, MediaCodecInfoAudio codecInfoAudio) {
        }

        public void onCodecInfoVideo(IMediaStream stream, MediaCodecInfoVideo codecInfoVideo) {
        }

        public void onPublish(IMediaStream stream, String streamName, boolean isRecord, boolean isAppend) {
            // record ALL INCOMING STREAMS
            StreamRecorderParameters recordParams = new StreamRecorderParameters(this.appInstance);
            recordParams.fileFormat = recordType;
            recordParams.versioningOption = versioningOption;
            recordParams.segmentationType = segmentationType;
            recordParams.startOnKeyFrame = startOnKeyFrame;
            recordParams.recordData = true;
            recordParams.fileVersionDelegate = new SurflineFileVersionDelegate(); // Segment naming
            recordParams.outputFile = setFilename(streamName); // Initial name
            this.vhost.getLiveStreamRecordManager().startRecording(this.appInstance, streamName, recordParams);
            getLogger().info("SurflineRecordingNaming: onPublish " + streamName);
            getLogger().info("SurflineRecordingNaming: onPublish StreamRecorderParameters " + recordParams.outputFile);
        }

        @Override
        public void onUnPublish(IMediaStream stream, String streamName, boolean isRecord, boolean isAppend) {
            getLogger().info("SurflineRecordingNaming: onUnPublish " + streamName);
            IStreamRecorder recorder = this.vhost.getLiveStreamRecordManager().getRecorder(this.appInstance, streamName);
            if (recorder != null) {
                getLogger().info("SurflineRecordingNaming: onUnPublish: stopRecording " + streamName);
                this.vhost.getLiveStreamRecordManager().stopRecording(this.appInstance, streamName);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void onStreamCreate(IMediaStream stream) {
        IMediaStreamActionNotify3 actionNotify = new StreamListener(this.vhost, this.appInstance);
        WMSProperties props = stream.getProperties();
        synchronized (props) {
            props.put("streamActionNotifier", actionNotify);
        }
        stream.addClientListener(actionNotify);
    }

    public void onStreamDestroy(IMediaStream stream) {
        IMediaStreamActionNotify3 actionNotify = null;
        WMSProperties props = stream.getProperties();
        synchronized (props) {
            actionNotify = (IMediaStreamActionNotify3) stream.getProperties().get("streamActionNotifier");
        }
        if (actionNotify != null) {
            getLogger().info("SurflineRecordingNaming: onStreamDestroy");
            stream.removeClientListener(actionNotify);
        }
    }

    /*
     * Initial name for file, will be modified by SurflineFileVersionDelegate for segmentation
     */
    private String setFilename(String streamName) {
        Date date = new Date();
        String name = streamName + sdf.format(date) + "." + recordType;

        getLogger().info("SurflineRecordingNaming: setFilename " + name);
        return name;

    }

    class SurflineFileVersionDelegate implements IStreamRecorderFileVersionDelegate {

        /*
         * Provides filename for segment, modifies initial name
         */
        public String getFilename(IStreamRecorder recorder) {
            File file = new File(recorder.getBaseFilePath());
            String oldBasePath = file.getParent();
            String name = oldBasePath + "/" + setFilename(recorder.getStreamName());

            getLogger().info("SurflineRecordingNaming: getFilename " + name);
            return name;
        }
    }
}
