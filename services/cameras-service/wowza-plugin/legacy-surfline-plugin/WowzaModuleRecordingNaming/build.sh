#!/usr/bin/env bash
set -x

IMAGE_NAME=wowza-module-builder
ARTIFACTS_PATH=/opt/workspace/WowzaModuleRecordingNaming/bin
TARGET_ARTIFACTS_PATH=tmp_build_artifacts

docker build -t "${IMAGE_NAME}" .

# Copy files from container to Jenkins workspace
# Instantiate the child container, copy assets from it, and remove the child container
ARTIFACTS_SRC="$(docker create "${IMAGE_NAME}:latest")"
mkdir -p "${TARGET_ARTIFACTS_PATH}"
docker cp "${ARTIFACTS_SRC}:${ARTIFACTS_PATH}/." "${TARGET_ARTIFACTS_PATH}"
docker rm "${ARTIFACTS_SRC}"
