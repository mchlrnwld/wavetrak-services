package com.surfline.cams;

import java.util.Map;

import com.wowza.util.HTTPUtils;
import com.wowza.wms.amf.*;
import com.wowza.wms.application.*;
import com.wowza.wms.client.*;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.httpstreamer.model.*;
import com.wowza.wms.httpstreamer.cupertinostreaming.httpstreamer.*;
import com.wowza.wms.httpstreamer.smoothstreaming.httpstreamer.*;
import com.wowza.wms.httpstreamer.util.HTTPStreamerUtils;
import com.wowza.wms.module.*;
import com.wowza.wms.request.*;
import com.wowza.wms.rtp.model.*;
import com.wowza.wms.stream.*;
import com.wowza.wms.vhost.IVHost;

public class AccessControl extends ModuleBase {

    /**
     * Sets allow origin header for cross-domain streaming.
     * It's wild card or if we're passing an origin, use that origin.
     * <p>
     * In the future, this is where we could lock down origin streaming.
     *
     * @param Nullable origin
     * @return NotNull origin for an Access-Control-Allow-Origin header
     */
    private static String getAllowedOrigin(String origin) {
        String allowedOrigin = origin != null ? origin : "*";
        getLogger().info("AccessControl.Origin: " + origin);
        return allowedOrigin;
    }

    public void onHTTPSessionCreate(IHTTPStreamerSession httpSession) {
        getLogger().info("AccessControl.onHTTPSessionCreate: " + httpSession.getSessionId());
        String origin = httpSession.getHTTPHeader("origin");
        httpSession.setUserHTTPHeader("Access-Control-Allow-Origin", getAllowedOrigin(origin));
    }

    public void onAppStart(IApplicationInstance appInstance) {
        getLogger().info("AccessControl.onAppStart[" + appInstance.getContextStr() + "]");

        IVHost vhost = appInstance.getVHost();

        String adapterName = HTTPStreamerUtils.httpSessionProtocolToName(
                IHTTPStreamerSession.SESSIONPROTOCOL_CUPERTINOSTREAMING);

        IHTTPStreamerApplicationContext appContext = appInstance.getHTTPStreamerApplicationContext(adapterName, true);
        IHTTPStreamerAdapter streamerAdapter = vhost.getHTTPStreamerAdapter(adapterName);

        if (appContext != null && streamerAdapter != null) {
            SurflineHTTPOriginSessionIdProvider httpOriginSessionIdProvider = new SurflineHTTPOriginSessionIdProvider(
                    (HTTPStreamerApplicationContextCupertinoStreamer) appContext,
                    (HTTPStreamerAdapterCupertinoStreamer) streamerAdapter);

            appContext.setHTTPOriginSessionIdProvider(httpOriginSessionIdProvider);
        }
    }

    /**
     * https://www.wowza.com/docs/how-to-control-http-origin-session-creation
     * <p>
     * Wowza Streaming Engine combines all sessions with a given stream name into a single session by default.
     * IHTTPStreamerHTTPOriginSessionIdProvider API hook enables you to control the httpOriginId,
     * which is used to combine sessions and determine if a session is unique.
     */
    class SurflineHTTPOriginSessionIdProvider implements IHTTPStreamerHTTPOriginSessionIdProvider {
        HTTPStreamerApplicationContextCupertinoStreamer appContextCupertino = null;
        HTTPStreamerAdapterCupertinoStreamer streamAdapterCupertino = null;

        public SurflineHTTPOriginSessionIdProvider(HTTPStreamerApplicationContextCupertinoStreamer appContextCupertino,
                                                   HTTPStreamerAdapterCupertinoStreamer streamAdapterCupertino) {
            this.appContextCupertino = appContextCupertino;
            this.streamAdapterCupertino = streamAdapterCupertino;
        }

        public String getHTTPOriginId(String httpOriginId, IHTTPRequest req, IHTTPStreamerApplicationContext appContext,
                                      String oldSessionId, String streamDomainStr, boolean isHTTPOrigin,
                                      boolean isDvr, long currTime) {
            String origin = req.getHeader("origin");
            if (origin == null) {
                origin = "*";
            }

            httpOriginId = httpOriginId + "|origin=" + origin;

            getLogger().info("AccessControl#SurflineHTTPOriginSessionIdProvider.getHTTPOriginId:" + httpOriginId);

            return httpOriginId;
        }
    }
}
