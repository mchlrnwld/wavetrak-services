node {
  checkout scm
  GString workingDirectory = "${ pwd() }/services/cameras-service/wowza-plugin"
  def BUILD_ENVIRONMENT = "prod"
  def s3Bucket = "s3://sl-artifacts-${ BUILD_ENVIRONMENT }/wowza/"
  def assetName = "surfline-recording-control*.jar"
  def assetPath = "WowzaModuleRecordingNaming/tmp_build_artifacts"

  stage('Build') {
    dir(workingDirectory) {
      sh(
        script: "cd WowzaModuleRecordingNaming && chmod +x build.sh && ./build.sh"
      )
      stash name: 'deployArtifacts'
    }
  }
  stage('Copy to S3') {
    unstash 'deployArtifacts'
    sh(
      // For portability to Jenkins build slaves,
      // prefer /usr/local/bin/assume-role ${env} over --profile ${env}
      script: "eval \$(/usr/local/bin/assume-role ${ BUILD_ENVIRONMENT }) && " +
        "aws s3 cp ${ assetPath }/${ assetName } ${ s3Bucket }"
    )
  }
}
