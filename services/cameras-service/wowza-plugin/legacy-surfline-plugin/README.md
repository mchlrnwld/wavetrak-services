# Wowza modules

## Build pipeline
* Jenkins builds and copies the module over to s3
* Manual step: copy that artifact from `s3://sl-artifacts-prod/wowza/` to wowza and enable it.

### Building in an IDE
* Use the Wowza Streaming Engine plug-in for Eclipse: https://www.wowza.com/docs/how-to-extend-wowza-streaming-engine-using-the-wowza-ide
* Install Wowza Streaming Engine locally with a R&D key ( request through Wowza Support / #camops Slack channel )
* The project's default ANT build targets the local Wowza plug-in directory

## Recording naming

### Why?
* UTC recording file names
* Every 10 minutes
* The first recording will not be snapped to 10 minute marks, just its `HHmm`

### Building module using Docker
* `./build.sh`

### Including in Wowza
* [Enable your Wowza user's advanced options](https://www.wowza.com/docs/how-to-find-your-way-around-wowza-streaming-engine-manager#advProperties)
* Add com.surfline.cams.SurflineRecordingNaming to enabled modules for an application, order above s3 upload module

## (Deprecated) Dynamic Access-Control-Allow-Origin header

### As of Wowza 4.7.4, functionality is integrated

* [4.7.4 Changelog](https://www.wowza.com/docs/wowza-streaming-engine-4-7-4-release-notes)
* Excerpt: Updated cross-origin resource sharing (CORS) to allow dynamic responses based on CORS request (default is disabled):
    * Application/HTTPStreamer/Properties [httpType]CORSDynamicEnabled (Boolean, default = false)
    * Application/HTTPStreamer/Properties [httpType]CORSUseHostname (Boolean, default = false)


### Why?
* We use cookies to authorize cam stream playback with CloudFront
* XMLHttpRequests.withCredentials = true is required to pass cookies along with redirects 
* In that secure redirect, Access-Control-Allow-Origin can not be "*"
* Passing Origin header back to Wowza Streaming Engine gives us the ability to customize the ACAO header
* We need a Wowza plugin to customize that header

### Building module using Docker
* `./build.sh`

### About CloudFront streaming
* http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-cookies.html
* http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/header-caching.html#header-caching-web-cors

### About JWPlayer streaming with credentials
Use `playlist[index].withCredentials` to allow cookie passing
* https://developer.jwplayer.com/jw-player/docs/developer-guide/customization/configuration-reference/#playlist

## About Wowza Modules
* https://www.wowza.com/blog/building-modules-why-they-matter
* https://www.wowza.com/docs/how-to-extend-wowza-streaming-engine-using-java

