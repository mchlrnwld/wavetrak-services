# Admin Auth Service

This module contains _only_ the **infrastructure code** for the `admin-auth-service`.

The application code can be found at [surfline-admin/auth-service].

## Naming Conventions

See [naming-conventions] for AWS resources.

## Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV={env} make plan
ENV={env} make apply
```

Available environments are:

- sbox
- staging
- prod

[surfline-admin/auth-service]: https://github.com/Surfline/surfline-admin/tree/master/services/auth-service
[naming-conventions]: https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions
