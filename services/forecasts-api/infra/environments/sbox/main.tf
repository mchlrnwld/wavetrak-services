provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/forecasts-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  alb_name         = "sl-int-core-srvs-1-sandbox"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  dns_name         = "forecasts-api.sandbox.surfline.com"
}

data "aws_alb" "main_internal" {
  name = local.alb_name
}

data "aws_alb_listener" "main_internal" {
  arn = local.alb_listener_arn
}

module "forecasts-api" {
  source = "../../"

  company     = "sl"
  application = "forecasts-api"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/forecasts/*"
    },
  ]

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  load_balancer_arn = data.aws_alb.main_internal.arn
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"

  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled = false
}
