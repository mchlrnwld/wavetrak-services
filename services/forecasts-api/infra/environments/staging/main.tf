provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/forecasts-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  alb_name         = "sl-int-core-srvs-1-staging"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  dns_name         = "forecasts-api.staging.surfline.com"
}

data "aws_alb" "main_internal" {
  name = local.alb_name
}

data "aws_alb_listener" "main_internal" {
  arn = local.alb_listener_arn
}

module "forecasts-api" {
  source = "../../"

  company     = "sl"
  application = "forecasts-api"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/forecasts/*"
    },
  ]

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  load_balancer_arn = data.aws_alb.main_internal.arn
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"

  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled = false
}
