provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/forecasts-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  alb_name         = "sl-int-core-srvs-1-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  dns_name         = "forecasts-api.prod.surfline.com"
}

data "aws_alb" "main_internal" {
  name = local.alb_name
}

data "aws_alb_listener" "main_internal" {
  arn = local.alb_listener_arn
}

module "forecasts-api" {
  source = "../../"

  company     = "sl"
  application = "forecasts-api"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 2
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/forecasts/*"
    },
  ]

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  load_balancer_arn = data.aws_alb.main_internal.arn
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 200
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}
