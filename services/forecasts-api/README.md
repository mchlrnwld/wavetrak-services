# Forecasts API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Custom New Relic Instrumentation](#custom-new-relic-instrumentation)
- [Forecast API Endpoints](#forecast-api-endpoints)
- [CREATE or UPDATE forecast summary and days](#create-or-update-forecast-summary-and-days)
- [GET latest forecast summary with forecast days withing a specified date range](#get-latest-forecast-summary-with-forecast-days-withing-a-specified-date-range)
  - [Query Parameters](#query-parameters)
  - [Sample Request 1](#sample-request-1)
  - [Sample Response 1](#sample-response-1)
  - [Sample Request 2](#sample-request-2)
  - [Sample Response 2](#sample-response-2)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Configure global environemnt
`nvm install v4.3.0`
`npm install -g eslint eslint-config-airbnb`

Configure .env appropriately (.env.sample provided)

Configuring the project
`npm install`

Testing the project
`npm run test`

Start logging docker in interactive mode (assuming docker is installed and daemon is running)
`cd ./services/logging-service && ./start-logstash.sh logstash-embedded.conf`

## Custom New Relic Instrumentation

You can add custom attributes with:

```
import * as newrelic from 'newrelic';
newrelic.default.addCustomAttribute('request', req);
```

## Forecast API Endpoints

## CREATE or UPDATE forecast summary and days

This operation will create a forecast summary. The forecast summary and days requires a valid ObjectId reference to a subregion from the Subregions database. The forecast date of the summary and each forecast day is very important. If mongo cannot find a mongo document with a matching subregion ID and forecast date, a new document will be created. If mongo can find a mongo document (summary or forecast day), the document will be updated. The forecast date should be passed as `mm/dd/yyyy`.

```
POST /admin/forecasts  HTTP/1.1
```

```JavaScript
{
  "summary": {
      "forecastDate": "12/20/2016",
      "currentDate": "12/20/2016",
      "nextForecast": {
          "day": "Friday",
          "time": "8 am"
      },
      "forecastStatus": {
          "status": "active",
          "inactiveMessage": "Inactive status message"
      },
      "highlights": ["highlight 1.5", "highlight 2.5", "highlight 3"],
      "bestBets": ["SS to the Cliffs look the biggest but beware of the high tide."],
      "bets": {
        "best": "SS to the Cliffs look the biggest but beware of the high tide.",
        "worst": "Overly high expectations. Spots that don't pull in S/SSE swell.",
      },
      "subregionId": "584204a24e65fad6a7709e46"
  },
  "days": [
    {
      "subregionId": "584204a24e65fad6a7709e46",
      "forecastDate": "12/20/2016",
      "forecast": {
        "am": {
          "condition": 7,
          "occasionalHeight": 12,
          "rating": "NONE"
        },
        "pm": {
          "condition": 1,
          "occasionalHeight": 3,
          "rating": "NONE"
        },
        "observation": "This is the observation"
      }
    }
  ]
}
```

If the forecast is saved successfully, we will get a 200 OK response, with the payload
looking like the following:

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "days": [
    {
      "_id": "585c3589d94f78ded408b115",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T17:56:33.049Z",
      "__v": 0,
      "createdAt": "2016-12-22T20:20:25.471Z",
      "extra": "working",
      "forecast": {
        "pm": {
          "humanRelation": "Flat",
          "maxHeight": 1,
          "minHeight": 0,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "Flat"
        },
        "am": {
          "humanRelation": "knee to chest high",
          "maxHeight": 3,
          "minHeight": 2,
          "rating": "None",
          "occasionalHeight": 12,
          "condition": "2-3 ft + – knee to chest high"
        },
		    "observation": "This is the observation"
      },
      "forecastDate": "2016-12-20T00:00:00.000Z"
    }
  ],
  "summary": {
    "_id": "585c354ed94f78ded408b113",
    "subregionId": "584204a24e65fad6a7709e46",
    "updatedAt": "2016-12-23T17:56:33.039Z",
    "forecasterEmail": "forecaster@surfline.com",
    "__v": 0,
    "createdAt": "2016-12-22T20:19:26.762Z",
    "nextForecast": {
      "time": "8 am",
      "day": "Friday"
    },
    "forecastStatus": {
      "status": "active",
      "inactiveMessage": "Inactive status message"
    },
    "highlights": [
      "highlight 1.5",
      "highlight 2.5",
      "highlight 3"
    ],
    "bestBets": ["SS to the Cliffs look the biggest but beware of the high tide."],
    "bets": {
      "best": "SS to the Cliffs look the biggest but beware of the high tide.",
      "worst": "Overly high expectations. Spots that don't pull in S/SSE swell.",
    },
    "forecastDate": "2016-12-20T00:00:00.000Z",
    "currentDate": "2016-12-20T00:00:00.000Z"
  }
}
```

In the case of an error, the create operation will attempt to process all of the forecast days in the array and at the end, present a list of days that are in error. For example, assume that in our last example we had a typo where the forecast am condition field is:

```
POST /admin/forecasts  HTTP/1.1
```

```JavaScript
{
    "summary": {
        "forecastDate": "adsfasdfasdfasdf",
        "currentDate": "12/20/2016",
        "nextForecast": {
            "day": 1234,
            "time": "8 am"
        },
        "forecastStatus": {
            "status": "active",
            "inactiveMessage": "Inactive status message"
        },
        "highlights": 123,
        "bestBets": ["SS to the Cliffs look the biggest but beware of the high tide."],
        "bets": {
          "best": "SS to the Cliffs look the biggest but beware of the high tide.",
          "worst": "Overly high expectations. Spots that don't pull in S/SSE swell.",
        },
        "subregionId": "584204a24e65fad6a7709e46"
    },
	"days": [
        {
        	"subregionId": "584204a24e65fad6a7709e46",
            "forecastDate": "asfasdfsad",
            "forecast": {
		        "am": {
		          "condition": "apples",
		          "rating": "NONE"
		        },
		        "pm": {
		          "condition": "dogs",
		          "occasionalHeight": 3,
		          "rating": "water"
		        },
				    "observation": "This is the observation"
            }
        },
        {
        	"subregionId": "584204a24e65fad6a7709e46",
            "forecastDate": "12/21/2016",
            "forecast": {
		        "am": {
		          "condition": "apples",
		          "occasionalHeight": 12,
		          "rating": "NONE"
		        },
		        "pm": {
		          "condition": 1,
		          "occasionalHeight": "doggy",
		          "rating": "water"
		        },
				    "observation": "This is the observation"
            }
        }
	  ]
}
```

We would get back the following response:

```
HTTP/1.1 500 Internal Server Error
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "message": "Found errors",
  "days": [
    {
      "errors": {
        "forecast.am.condition": {
          "message": "apples is an invalid condition enum for AM forecast."
        },
        "forecast.am.occasionalHeight": {
          "message": "The occasional height for AM forecast must be a number."
        },
        "forecast.pm.condition": {
          "message": "dogs is an invalid condition enum for PM forecast."
        },
        "forecast.pm.rating": {
          "message": "water is an invalid rating enum for PM forecast."
        },
        "forecast.observation": {
          "message": "The observation must be less than 200 characters."
        },
        "forecastDate": {
          "message": "The forecast date is incorrect."
        }
      },
      "forecastDate": null
    },
    {
      "errors": {
        "forecast.am.condition": {
          "message": "apples is an invalid condition enum for AM forecast."
        },
        "forecast.pm.rating": {
          "message": "water is an invalid rating enum for PM forecast."
        },
        "forecast.pm.occasionalHeight": {
          "message": "The occasional height for PM forecast must be a number."
        }
      },
      "forecastDate": "2016-12-21T05:00:00.000Z"
    }
  ],
  "summary": {
    "errors": {
      "forecastDate": {
        "message": "Please submit a valid forecast date"
      },
      "nextForecast.day": {
        "message": "Invalid forecast day string"
      },
      "highlights": {
        "message": "Highlights must be an array"
      }
    }
  }
}
```

## GET latest forecast summary with forecast days withing a specified date range

This endpoint returns forecasts for the subregion sorted by forecast date descending. If no start and end dates are specified, the current local date of the subregion will be used as the start date and the end date will be 7 days later.

#### Query Parameters

| Parameter     | Required | Description                |
| ------------- | -------- | -------------------------- |
| `subregionId` | yes      | ObjectId for subregion     |
| `startDate`   | no       | Number to limit results to |
| `endDate`     | no       | Number to limit results to |

#### Sample Request 1

```
GET /admin/forecasts?subregionId=58581a836630e24c4487902f
```

#### Sample Response 1

```JavaScript
{
  "summary": {
    "subregionId": "58581a836630e24c4487902f",
    "subregionName": "Australia North Coast",
    "nextForecast": {
      "day": "",
      "time": ""
    },
    "forecastStatus": {
      "status": "active",
      "inactiveMessage": "Inactive status message"
    },
    "highlights": [],
    "bestBets": [],
    "bets": {
      "best": null,
      "worst": null,
    },
    "forecastDate": "2017-01-06T00:00:00.000Z",
    "currentDate": "2017-01-06T00:00:00.000Z"
  },
  "days": []
}
```

#### Sample Request 2

```
GET /admin/forecasts?subregionId=584204a24e65fad6a7709e46&startDate=12/20/2016&endDate=12/25/2016 HTTP/1.1
```

#### Sample Response 2

```JavaScript
{
  "days": [
    {
      "_id": "585d9f0a6ef28ffa78af345b",
      "forecastDate": "2016-12-25T00:00:00.000Z",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.075Z",
      "forecast": {
        "pm": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
        "am": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
		    "observation": "This is the observation"
      },
      "__v": 0,
      "createdAt": "2016-12-23T22:02:50.041Z"
    },
    {
      "_id": "585d9f0a6ef28ffa78af345a",
      "forecastDate": "2016-12-24T00:00:00.000Z",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.071Z",
      "forecast": {
        "pm": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
        "am": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
		    "observation": "This is the observation"
      },
      "__v": 0,
      "createdAt": "2016-12-23T22:02:50.038Z"
    },
    {
      "_id": "585d9f0a6ef28ffa78af3459",
      "forecastDate": "2016-12-23T00:00:00.000Z",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.061Z",
      "forecast": {
        "pm": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
        "am": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
		    "observation": "This is the observation"
      },
      "__v": 0,
      "createdAt": "2016-12-23T22:02:50.037Z"
    },
    {
      "_id": "585d9f0a6ef28ffa78af3458",
      "forecastDate": "2016-12-22T00:00:00.000Z",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.058Z",
      "forecast": {
        "pm": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
        "am": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
		    "observation": "This is the observation"
      },
      "__v": 0,
      "createdAt": "2016-12-23T22:02:50.035Z"
    },
    {
      "_id": "585d9f0a6ef28ffa78af3457",
      "forecastDate": "2016-12-21T00:00:00.000Z",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.053Z",
      "forecast": {
        "pm": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
        "am": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
		    "observation": "This is the observation"
      },
      "__v": 0,
      "createdAt": "2016-12-23T22:02:50.033Z"
    },
    {
      "_id": "585d9f0a6ef28ffa78af3456",
      "forecastDate": "2016-12-20T00:00:00.000Z",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.046Z",
      "forecast": {
        "pm": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
        "am": {
          "humanRelation": "knee to thigh",
          "maxHeight": 2,
          "minHeight": 1,
          "rating": "None",
          "occasionalHeight": 3,
          "condition": "1-2 ft – knee to thigh"
        },
		    "observation": "This is the observation"
      },
      "__v": 0,
      "createdAt": "2016-12-23T22:02:50.026Z"
    }
  ],
  "summary": {
    "_id": "585d9f0a6ef28ffa78af3455",
    "forecastDate": "2016-12-20T00:00:00.000Z",
    "currentDate": "2016-12-20T00:00:00.000Z",
    "subregionId": "584204a24e65fad6a7709e46",
    "updatedAt": "2016-12-23T22:03:03.032Z",
    "forecasterEmail": "forecaster@surfline.com",
    "highlights": [
      "highlight 1",
      "highlight 2",
      "highlight 3"
    ],
    bestBets: ["SS to the Cliffs look the biggest but beware of the high tide."],
    "bets": {
      "best": "SS to the Cliffs look the biggest but beware of the high tide.",
      "worst": "Overly high expectations. Spots that don't pull in S/SSE swell.",
    },
    "nextForecast": {
      "time": "8 am",
      "day": "Friday"
    },
    "forecastStatus": {
      "status": "active",
      "inactiveMessage": "Inactive status message"
    },
    "__v": 0,
    "createdAt": "2016-12-23T22:02:50.017Z"
  }
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `forecast-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TURJNU1qSTJNVEEifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE1ESTVNakkyTVRBIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests on the endpoints detailed in the [Forecast API Endpoints](#forecast-api-endpoints) section.

**Ex:**

Test `GET` requests against the `/admin/forecasts` endpoint:

```
curl \
    -X GET \
    -i \
    http://forecasts-api.sandbox.surfline.com/admin/forecasts?subregionId=58581a836630e24c4487902f
```

**Expected response:** See [above](#sample-response-1) for sample response.
