import setupApp from './server';
import logger from './common/logger';
import * as dbContext from './model/dbContext';

const log = logger('forecasts-api:server');

const start = async () => {
  await dbContext.initMongoDB();
  const port = parseInt(process.env.EXPRESS_PORT, 10) || 8081;
  setupApp(port);
};

start().catch((err) => {
  const error = err as Error;
  log.error({
    message: "App Initialization failed. Can't connect to database",
    stack: error,
  });
});
