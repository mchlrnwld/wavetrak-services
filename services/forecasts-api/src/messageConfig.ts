export const SUBREGION_FORECAST_UPDATED_TOPIC = 'subregion_forecast_updated';
export const CLEAR_CF_REDIS_TOPIC = 'clear_cf_redis';

const base = {
  region: 'us-west-1',
  api: '2010-03-31',
} as const;

type Environment = 'development' | 'sandbox' | 'staging' | 'production';

interface EnvConfig {
  [SUBREGION_FORECAST_UPDATED_TOPIC]: { arn: string };
  [CLEAR_CF_REDIS_TOPIC]: { arn: string };
}

const envConfig: Record<Environment, EnvConfig> = {
  development: {
    [SUBREGION_FORECAST_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:subregion_forecast_updated_staging',
    },
    [CLEAR_CF_REDIS_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:clear_cf_redis_staging',
    },
  },
  sandbox: {
    [SUBREGION_FORECAST_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:subregion_forecast_updated_sandbox',
    },
    [CLEAR_CF_REDIS_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:clear_cf_redis_sandbox',
    },
  },
  staging: {
    [SUBREGION_FORECAST_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:subregion_forecast_updated_staging',
    },
    [CLEAR_CF_REDIS_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:665294954271:clear_cf_redis_staging',
    },
  },
  production: {
    [SUBREGION_FORECAST_UPDATED_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:833713747344:subregion_forecast_updated_prod',
    },
    [CLEAR_CF_REDIS_TOPIC]: {
      arn: 'arn:aws:sns:us-west-1:833713747344:clear_cf_redis_prod',
    },
  },
};

const messageConfig = {
  ...base,
  ...envConfig[process.env.APP_ENV || 'development'],
} as typeof base & EnvConfig;

export default messageConfig;
