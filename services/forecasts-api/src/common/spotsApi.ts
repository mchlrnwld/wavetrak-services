/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import fetch from 'node-fetch';

export const getSubregion = async (subregionId: string) => {
  const result = await fetch(`${process.env.SPOTS_API}/admin/subregions/${subregionId}`);
  const response = await result.json();

  if (result.status === 200) return response;
  throw response;
};

export const getSpotsBySubregion = async (subregionId: string) => {
  const result = await fetch(`${process.env.SPOTS_API}/admin/subregions/${subregionId}/spots`);
  const response = await result.json();

  if (result.status === 200) return response;
  throw response;
};
