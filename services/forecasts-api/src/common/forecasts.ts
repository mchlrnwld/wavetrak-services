/* eslint-disable no-restricted-syntax */
import mongoose from 'mongoose';

import SubregionForecastDayModel from '../model/SubregionForecastDayModel';
import SubregionForecastSummaryModel from '../model/SubregionForecastSummaryModel';
import { SubregionForecastDayInput, SubregionForecastSummaryInput } from '../types/api';
import type { SubregionForecastDay, SubregionForecastSummary } from '../types/models';
import { DateInput, DateOrTimestamp } from '../types/mongoose';

export const updateForecastDay = async (date: DateInput, day: SubregionForecastDayInput) => {
  const dayInput = { ...day };
  delete dayInput.forecastDay;

  const normalizedDayInput: SubregionForecastDay = {
    ...dayInput,
    forecastDate: date,
    forecast: {
      ...dayInput.forecast,
      am: {
        ...dayInput.forecast.am,
        // TODO: remove hardcoded empty string for am/pm observation after mobile contract is updated
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        observation: '',
        occasionalHeight: dayInput.forecast.am.occasionalHeight || 0,
      },
      pm: {
        ...dayInput.forecast.pm,
        // TODO: remove hardcoded empty string for am/pm observation after mobile contract is updated
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        observation: '',
        occasionalHeight: dayInput.forecast.pm.occasionalHeight || 0,
      },
    },
  };

  const forecastDayFound = await SubregionForecastDayModel.findOne({
    $and: [{ subregionId: mongoose.Types.ObjectId(dayInput.subregionId) }, { forecastDate: date }],
  });

  if (forecastDayFound) {
    for (const key in normalizedDayInput) {
      if (key !== 'prototype') {
        forecastDayFound[key] = normalizedDayInput[key as keyof SubregionForecastDay];
      }
    }

    return forecastDayFound.save();
  }

  const forecastDayModel = new SubregionForecastDayModel(normalizedDayInput);
  return forecastDayModel.save();
};

type SubregionForecastSummaryWithoutMongooseProperties = Omit<
  SubregionForecastSummary,
  '_id' | 'updatedAt' | 'createdAt'
>;

export const updateForecastSummary = async (
  date: DateOrTimestamp,
  summaryInput: SubregionForecastSummaryInput,
) => {
  const subregionId = mongoose.Types.ObjectId(summaryInput.subregionId);
  const normalizedSummaryInput: SubregionForecastSummaryWithoutMongooseProperties = {
    forecastDate: date,
    highlights: summaryInput.highlights,
    bestBets: summaryInput.bestBets,
    bets: summaryInput.bets,
    nextForecast: summaryInput.nextForecast,
    nextForecastTimestamp: summaryInput.nextForecastTimestamp,
    forecaster: summaryInput.forecaster,
    subregionId,
    forecastStatus: summaryInput.forecastStatus,
  };

  const existingSummary = await SubregionForecastSummaryModel.findOne({
    $and: [{ subregionId }, { forecastDate: date }],
  });

  if (existingSummary) {
    for (const key in normalizedSummaryInput) {
      if (key !== 'prototype') {
        existingSummary[key] =
          normalizedSummaryInput[key as keyof SubregionForecastSummaryWithoutMongooseProperties];
      }
    }
    return existingSummary.save();
  }

  const summaryModel = new SubregionForecastSummaryModel(normalizedSummaryInput);
  return summaryModel.save();
};
