import mongoose from 'mongoose';
import { Condition } from '../types/forecast';
import { SubregionForecastDay } from '../types/models';
import { surfConditions, surfConditionsKeys, surfRatings } from './conditions';

const forecastSchema = new mongoose.Schema<SubregionForecastDay>(
  {
    forecastDate: {
      type: Date,
      default: Date.now,
    },
    subregionId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Subregion',
    },
    forecast: {
      am: {
        condition: {
          type: Number,
          required: [true, 'Condition is required'],
          enum: surfConditionsKeys,
        },
        minHeight: { type: Number, required: [true, 'Min Height is required'] },
        maxHeight: { type: Number, required: [true, 'Max Height is required'] },
        humanRelation: { type: String, required: [true, 'Human Relation required'] },
        occasionalHeight: { type: Number },
        rating: {
          type: String,
          required: [true, 'Rating is required'],
          enum: Object.keys(surfRatings),
        },
      },
      pm: {
        condition: {
          type: Number,
          required: [true, 'Condition is required'],
          enum: surfConditionsKeys,
        },
        minHeight: { type: Number, required: [true, 'Min Height is required'] },
        maxHeight: { type: Number, required: [true, 'Max Height is required'] },
        humanRelation: { type: String, required: [true, 'Human Relation required'] },
        occasionalHeight: { type: Number },
        rating: {
          type: String,
          required: [true, 'Rating is required'],
          enum: Object.keys(surfRatings),
        },
      },
      observation: {
        type: String,
        maxlength: [200, 'Observation must not have more than 200 characters.'],
      },
    },
  },
  {
    collection: 'SubregionForecastDays',
    timestamps: true,
  },
);

forecastSchema.index({ forecastDate: 1, subregionId: 1 }, { unique: true });

forecastSchema.pre('validate', function parseConditions(next) {
  const amConditions = surfConditions[this.forecast.am.condition] as Condition;
  this.forecast.am.minHeight = amConditions.minHeight;
  this.forecast.am.maxHeight = amConditions.maxHeight;
  this.forecast.am.humanRelation = amConditions.humanRelation;

  const pmConditions = surfConditions[this.forecast.pm.condition] as Condition;
  this.forecast.pm.minHeight = pmConditions.minHeight;
  this.forecast.pm.maxHeight = pmConditions.maxHeight;
  this.forecast.pm.humanRelation = pmConditions.humanRelation;
  return next();
});

const SubregionForecastDayModel = mongoose.model<SubregionForecastDay>(
  'SubregionForecastDay',
  forecastSchema,
);

export default SubregionForecastDayModel;
