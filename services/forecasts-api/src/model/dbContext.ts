import mongoose from 'mongoose';
import logger from '../common/logger';

const log = logger('forecasts-api:MongoDB');

mongoose.Promise = global.Promise;

export const initMongoDB = async () => {
  const connectionString = process.env.MONGO_CONNECTION_STRING_KBYG;
  const mongoDbConfig: mongoose.ConnectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  try {
    log.debug(`Mongo ConnectionString: ${connectionString} `);

    await mongoose.connect(connectionString, mongoDbConfig);
    log.info(`MongoDB connected on ${connectionString}`);
  } catch (e) {
    const error = e as Error;
    log.error({
      action: 'MongoDB:ConnectionError',
      error,
    });
  }
};
