import mongoose from 'mongoose';
import { surfRatings } from './conditions';
import type { SpotForecastModel } from '../types/models';

const forecastSchema = new mongoose.Schema<SpotForecastModel>(
  {
    forecastDate: {
      type: Date,
      default: Date.now,
    },
    spotId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Spot',
    },
    forecast: {
      hours: [
        {
          timestamp: { type: Number, required: [true, 'Forecast hour timestamp is required.'] },
          waveHeight: {
            type: new mongoose.Schema(
              {
                min: { type: Number, required: [true, 'Min wave height is required.'] },
                max: { type: Number, required: [true, 'Max wave height is required.'] },
                plus: { type: Boolean, default: false },
              },
              { _id: false },
            ),
            required: false,
          },
          rating: {
            type: String,
            enum: Object.keys(surfRatings),
            required: [true, 'Rating is required.'],
          },
        },
      ],
      observation: {
        type: String,
        maxlength: [240, 'Observation must not have more than 200 characters.'],
      },
    },
  },
  {
    collection: 'SpotForecastDays',
    timestamps: true,
  },
);

forecastSchema.index({ forecastDate: 1, spotId: 1 }, { unique: true });

export default mongoose.model<SpotForecastModel>('SpotForecastDay', forecastSchema);
