import { expect } from 'chai';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import SpotForecastDay from './SpotForecastDayModel';

describe('SpotForecastDay Model', () => {
  let mongoServer: MongoMemoryServer;

  before(async () => {
    mongoServer = await MongoMemoryServer.create();

    const mongoUri = mongoServer.getUri();

    const mongoDbConfig: mongoose.ConnectOptions = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    await mongoose.connect(mongoUri, mongoDbConfig);
  });

  after(async () => {
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  afterEach(async () => {
    await SpotForecastDay.deleteMany();
  });

  it('should not require wave heights', (done) => {
    const spotForecastDay = new SpotForecastDay({
      forecast: {
        hours: [
          {
            timestamp: 1645668000,
            rating: 'GOOD',
          },
        ],
      },
    });
    spotForecastDay.save((err: mongoose.Error.ValidationError) => {
      expect(err).to.be.null();

      return done();
    });
  });

  it('should require min/max wave height fields for hourly forecast wave heights', (done) => {
    const spotForecastDay = new SpotForecastDay({
      forecast: {
        hours: [
          {
            timestamp: 1645668000,
            waveHeight: {},
            rating: 'GOOD',
          },
        ],
      },
    });
    spotForecastDay.save((err: mongoose.Error.ValidationError) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors['forecast.hours.0.waveHeight.min']).to.be.ok();
      expect(err.errors['forecast.hours.0.waveHeight.max']).to.be.ok();

      expect(err.errors['forecast.hours.0.waveHeight.min'].message).to.equal(
        'Min wave height is required.',
      );
      expect(err.errors['forecast.hours.0.waveHeight.max'].message).to.equal(
        'Max wave height is required.',
      );

      return done();
    });
  });

  it('should require rating field for hourly forecasts', (done) => {
    const spotForecastDay = new SpotForecastDay({ forecast: { hours: [{}] } });
    spotForecastDay.save((err: mongoose.Error.ValidationError) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors['forecast.hours.0.rating']).to.be.ok();
      expect(err.errors['forecast.hours.0.rating'].message).to.equal('Rating is required.');

      return done();
    });
  });
});
