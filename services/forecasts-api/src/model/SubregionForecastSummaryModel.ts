import mongoose from 'mongoose';
import { SubregionForecastSummary } from '../types/models';

const forecastSummarySchema = new mongoose.Schema<SubregionForecastSummary>(
  {
    forecastDate: {
      type: Date,
      default: Date.now,
      required: [true, 'Forecast date is required'],
    },
    highlights: [
      {
        type: String,
        maxlength: [86, 'Highlight must not have more than 86 characters.'],
      },
    ],
    bestBets: [
      {
        type: String,
        maxlength: [86, 'Best Bets must not have more than 86 characters.'],
      },
    ],
    bets: {
      best: {
        type: String,
        maxlength: [86, 'Best bet must not have more than 86 characters.'],
        default: null,
      },
      worst: {
        type: String,
        maxlength: [86, 'Worst bet must not have more than 86 characters.'],
        default: null,
      },
    },
    nextForecast: {
      day: {
        type: String,
        required: [true, 'Next forecast day is required'],
      },
      time: {
        type: String,
        required: [true, 'Next forecast time is required'],
      },
    },
    nextForecastTimestamp: {
      type: Date,
      default: Date.now,
      required: [true, 'Next forecast timestamp is required'],
    },
    forecaster: {
      email: { type: String },
      name: { type: String },
      title: { type: String },
    },
    subregionId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Subregion',
    },
    forecastStatus: {
      status: {
        type: String,
        default: 'active',
      },
      inactiveMessage: {
        type: String,
        maxlength: [150, 'Status message must not have more than 150 characters.'],
        default: 'Inactive status message',
      },
    },
  },
  {
    collection: 'SubregionForecastSummaries',
    timestamps: true,
  },
);

interface Spot {
  name: string;
  timezone: string;
}
const spotSchema = new mongoose.Schema<Spot>(
  {
    name: { type: String },
    timezone: { type: String },
  },
  {
    collection: 'Spots',
  },
);

forecastSummarySchema.index({ subregionId: 1 });
forecastSummarySchema.index({ forecastDate: -1 });
forecastSummarySchema.index({
  subregionId: 1,
  forecastDate: -1,
});

mongoose.model<Spot>('Spot', spotSchema);
export default mongoose.model<SubregionForecastSummary>(
  'SubregionForecastSummary',
  forecastSummarySchema,
);
