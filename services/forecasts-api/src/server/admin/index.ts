import type Logger from 'bunyan';
import { Router } from 'express';
import forecasts from './forecasts';

const admin = (log: Logger) => {
  const api = Router();

  api.use('/forecasts', forecasts(log));

  return api;
};

export default admin;
