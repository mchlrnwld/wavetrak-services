import type { NextFunction, Response } from 'express';
import type Logger from 'bunyan';
import newrelic from 'newrelic';
import moment from 'moment-timezone';
import mongoose from 'mongoose';

import SubregionForecastSummaryModel from '../../../model/SubregionForecastSummaryModel';
import { getSpotsBySubregion, getSubregion } from '../../../common/spotsApi';
import SubregionForecastDayModel from '../../../model/SubregionForecastDayModel';
import { updateForecastDay, updateForecastSummary } from '../../../common/forecasts';
import publishTopic from './publishTopic';
import { SUBREGION_FORECAST_UPDATED_TOPIC, CLEAR_CF_REDIS_TOPIC } from '../../../messageConfig';
import {
  validateForecastSummaryFields,
  validateForecastDayFields,
} from '../../../utils/validateForecastDayFields';
import {
  toUnixTimestamp,
  createUTCOffsetCalculator,
  midnightOfDate,
  getForecastDateRange,
} from '../../../utils/datetime';
import {
  CreateForecastInput,
  GetSubregionForecastDayResponse,
  GetSubregionForecastSummaryResponse,
} from '../../../types/api';
import { WavetrakRequest } from '../../../types/express';
import type { SubregionForecastDay, SubregionForecastSummary } from '../../../types/models';

const logger = console;

export const trackForecastsRequests =
  (log: Logger) => (req: WavetrakRequest, _res: Response, next: NextFunction) => {
    log.trace({
      action: '/admin/forecasts',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body as unknown,
      },
    });
    return next();
  };

export const getEmptyPlaceholderDay = (
  forecastDate: number,
  subregionId: string,
): GetSubregionForecastDayResponse => ({
  forecast: {
    am: {
      condition: 0,
      humanRelation: 'None',
      maxHeight: 0,
      minHeight: 0,
      occasionalHeight: 0,
      rating: 'None',
    },
    pm: {
      condition: 0,
      humanRelation: 'None',
      maxHeight: 0,
      minHeight: 0,
      occasionalHeight: 0,
      rating: 'None',
    },
    observation: '',
  },
  forecastDate,
  subregionId,
  createdAt: null,
  updatedAt: null,
});

export const insertMissingDays = ({
  forecastDays,
  startDate,
  endDate,
  cmsMode,
  subregionId,
}: {
  forecastDays: SubregionForecastDay[];
  startDate: number;
  endDate: number;
  cmsMode: boolean;
  subregionId: string;
}) => {
  const firstDay = moment.tz(startDate, 'UTC');
  const lastDay = moment.tz(endDate, 'UTC');
  const numDaysExpected = lastDay.diff(firstDay, 'days');

  return [...Array<SubregionForecastDay | GetSubregionForecastDayResponse>(numDaysExpected)].map(
    (_, daysToAdd) => {
      const expectedDate = moment(firstDay).clone().add(daysToAdd, 'days');
      const expectedDateFormatted = expectedDate.format('YYYY-MM-DD');
      const expectedDayForecast = forecastDays.find(
        (day) => moment.tz(day.forecastDate, 'UTC').format('YYYY-MM-DD') === expectedDateFormatted,
      );
      if (expectedDayForecast) {
        return expectedDayForecast;
      }
      // return null for missing days except in cms mode then we return an empty placeholder object
      return !cmsMode ? null : getEmptyPlaceholderDay(expectedDate.unix() * 1000, subregionId);
    },
  );
};

export const getForecastHandler = async (req: WavetrakRequest, res: Response) => {
  const { days, subregionId, startDate, endDate, cms } = req.query;

  let spots: [{ timezone: string }];
  let subregion: { name: string };
  let timezone: string;
  ({ timezone } = req.query);
  const daysToReturn = days ? parseInt(days, 10) : 8;
  try {
    subregion = (await getSubregion(subregionId)) as { name: string };
    if (!timezone) {
      spots = (await getSpotsBySubregion(subregionId)) as [{ timezone: string }];
      if (!spots || !spots.length) {
        return res.status(400).send({
          message: 'No spots listed for subregion',
        });
      }
      [{ timezone }] = spots;
    }
  } catch (err) {
    return res.status(400).send({
      message: 'Invalid subregion ID',
    });
  }

  let forecastDate: { $gte?: number; $lte?: number };
  const dateRange: { utcStartDate?: number; utcEndDate?: number } = {};
  const utcOffsetCalculator = createUTCOffsetCalculator(timezone);
  const subregionCurrMoment = moment().tz(timezone);
  const subregionTimezoneOffset = utcOffsetCalculator(subregionCurrMoment.unix());

  if (startDate && endDate) {
    dateRange.utcStartDate = +midnightOfDate('UTC', new Date(startDate), true) * 1000;
    dateRange.utcEndDate = +midnightOfDate('UTC', new Date(endDate), true) * 1000;
    forecastDate = { $gte: dateRange.utcStartDate, $lte: dateRange.utcEndDate };
  } else {
    ({ utcStartDate: dateRange.utcStartDate, utcEndDate: dateRange.utcEndDate } =
      getForecastDateRange(timezone, daysToReturn));
    forecastDate = { $gte: dateRange.utcStartDate, $lte: dateRange.utcEndDate };
  }

  const forecastSummary = await SubregionForecastSummaryModel.findOne({
    subregionId,
  })
    .sort({ forecastDate: -1 })
    .lean();

  const forecastDays: SubregionForecastDay[] =
    !forecastSummary || forecastSummary.forecastStatus.status === 'off'
      ? []
      : await SubregionForecastDayModel.find({
          $and: [{ subregionId: mongoose.Types.ObjectId(subregionId) }, { forecastDate }],
        })
          .limit(daysToReturn)
          .sort({ forecastDate: 1 })
          .lean();

  const forecastedDays = insertMissingDays({
    forecastDays,
    startDate: dateRange.utcStartDate,
    endDate: dateRange.utcEndDate,
    cmsMode: cms === 'true',
    subregionId,
  }).map((day) => {
    if (day) {
      return {
        ...day,
        updatedAt: day.updatedAt ? toUnixTimestamp(day.updatedAt) : null,
        createdAt: day.createdAt ? toUnixTimestamp(day.createdAt) : null,
        forecastDate: toUnixTimestamp(day.forecastDate as Date),
        // force UTC to prevent offset conversion even though day is relative to subregion
        forecastDay: moment.tz(day.forecastDate, 'UTC').format('YYYY-MM-DD'),
      };
    }
    return null;
  });

  let summary: GetSubregionForecastSummaryResponse = {
    subregionId,
    subregionName: subregion.name,
    subregionTimezoneOffset,
    timezone,
    nextForecast: {
      day: '',
      time: '',
      timestamp: null,
      utcOffset: null,
    },
    // TODO FE-639: Remove deprecated nextForecastTimestamp
    nextForecastTimestamp: null,
    forecaster: { name: '', title: '', email: '' },
    forecastStatus: {
      status: !forecastSummary ? 'active' : forecastSummary.forecastStatus.status,
      inactiveMessage: '',
    },
    highlights: [],
    bestBets: [],
    bets: {
      best: null,
      worst: null,
    },
    forecastDate: toUnixTimestamp(forecastDate.$gte),
    currentDate: toUnixTimestamp(forecastDate.$gte),
  };

  if (forecastSummary && forecastSummary.forecastStatus.status !== 'off') {
    summary = {
      ...summary,
      _id: forecastSummary._id,
      forecaster: forecastSummary.forecaster,
      updatedAt: toUnixTimestamp(forecastSummary.updatedAt),
      createdAt: toUnixTimestamp(forecastSummary.createdAt),
      nextForecast: {
        ...forecastSummary.nextForecast,
        timestamp: toUnixTimestamp(forecastSummary.nextForecastTimestamp as Date),
        utcOffset: utcOffsetCalculator(moment(forecastSummary.nextForecastTimestamp).unix()),
      },
      // TODO FE-639: Remove deprecated nextForecastTimestamp
      nextForecastTimestamp: toUnixTimestamp(forecastSummary.nextForecastTimestamp as Date),
      highlights: forecastSummary.highlights,
      bestBets: forecastSummary.bestBets || [],
      bets: forecastSummary.bets || { best: null, worst: null },
      forecastDate: toUnixTimestamp(forecastSummary.forecastDate as Date),
      currentDate: toUnixTimestamp(forecastDate.$gte),
      forecastStatus: forecastSummary.forecastStatus,
    };
  }

  return res.send({ days: forecastedDays, summary });
};

export const createForecastHandler = async (
  req: WavetrakRequest<CreateForecastInput>,
  res: Response,
) => {
  const {
    days,
    summary,
    summary: { subregionId },
  } = req.body;
  summary.forecaster = {
    email: req.get('X-Auth-EmployeeEmail') || '',
    name: req.get('X-Auth-EmployeeName') || 'Johnny Tsunami',
    title: req.get('X-Auth-EmployeeTitle') || '',
  };

  let legacyId: string;
  try {
    ({ legacyId } = (await getSubregion(subregionId)) as { legacyId: string });
  } catch (err) {
    return res.status(400).send({ message: 'Invalid subregion ID' });
  }

  let forecastSummaryModel: SubregionForecastSummary;
  let summaryFieldErrors: Record<string, { message: string }> | null = {};
  let summaryErrors: { errors?: Record<string, { message: string }> } = {};
  try {
    summaryFieldErrors = validateForecastSummaryFields(summary);
    if (summaryFieldErrors) throw Error('Invalid fields');

    const utcForecastSummaryDate =
      +midnightOfDate('UTC', new Date(summary.forecastDate), true) * 1000;
    forecastSummaryModel = await updateForecastSummary(utcForecastSummaryDate, summary);
    if (!forecastSummaryModel) {
      return res.status(404).send({ message: 'Forecast summary not found' });
    }
  } catch (e) {
    const err = e as mongoose.Error.ValidationError;
    summaryErrors = {
      errors: { ...summaryFieldErrors, ...err.errors },
    };
  }

  const dayErrors: {
    errors: { [key: string]: { message: string } };
    forecastDate: string | null;
  }[] = [];
  const savedForecasts = await Promise.all(
    days.map(async (day, index) => {
      let forecastDaySave: SubregionForecastDay;
      let fieldErrors: Record<string, { message: string }> = {};
      try {
        fieldErrors = validateForecastDayFields(days, summary, day, index);
        if (Object.keys(fieldErrors).length > 0) throw Error('Invalid fields');
        if (days.length > 1 && index === days.length - 1) {
          const diff = moment(day.forecastDay).diff(moment(days[index - 1].forecastDay), 'days');
          if (!(diff > 1)) forecastDaySave = await updateForecastDay(day.forecastDay, day);
        } else {
          forecastDaySave = await updateForecastDay(day.forecastDay, day);
        }
      } catch (e) {
        const err = e as mongoose.Error.ValidationError;
        dayErrors.push({
          errors: { ...err.errors, ...fieldErrors },
          forecastDate: fieldErrors.forecastDate ? null : day.forecastDay,
        });
      }
      return forecastDaySave;
    }),
  );

  if (dayErrors.length > 0 || Object.keys(summaryErrors).length > 0) {
    const err = { days: dayErrors, summary: summaryErrors };
    newrelic.noticeError(new Error('Forecast Validation Errors'));
    return res.status(500).send({ message: 'Found errors', ...err });
  }

  if (legacyId) {
    try {
      const publishResponse = await publishTopic(legacyId, CLEAR_CF_REDIS_TOPIC);
      logger.info(publishResponse);
    } catch (err) {
      // ignore caching errors, just try to clear cache based on legacyId
      logger.error(err);
    }
  }
  try {
    const publishResponse = await publishTopic(subregionId, SUBREGION_FORECAST_UPDATED_TOPIC);
    logger.info(publishResponse);
  } catch (err) {
    logger.error(err);
  }
  return res.send({
    days: savedForecasts,
    summary: forecastSummaryModel,
  });
};
