import type { Response } from 'express';
import _ from 'lodash';

import { surfConditions, surfRatings, occasionalHeights } from '../../../model/conditions';
import { WavetrakRequest } from '../../../types/express';

const getConditionOptions = async (_req: WavetrakRequest, res: Response) => {
  const conditions = _.map(surfConditions, (condition, k) => ({
    value: k,
    text: condition.condition,
    minHeight: condition.minHeight,
    maxHeight: condition.maxHeight,
  })).sort((a, b) => a.minHeight + a.maxHeight - (b.minHeight + b.maxHeight));

  const occ = occasionalHeights;
  const ratings = _.map(surfRatings, (rating, k) => ({ value: k, text: rating }));

  return res.send({
    conditions,
    occ,
    ratings,
    surfConditions,
  });
};

export default getConditionOptions;
