import AWS from 'aws-sdk';
import messageConfig, {
  SUBREGION_FORECAST_UPDATED_TOPIC,
  CLEAR_CF_REDIS_TOPIC,
} from '../../../messageConfig';

const publishSubregionUpdateTopic = (
  subregionId: string,
  topic: typeof SUBREGION_FORECAST_UPDATED_TOPIC | typeof CLEAR_CF_REDIS_TOPIC,
) =>
  new Promise((resolve, reject) => {
    AWS.config.update({
      region: messageConfig.region,
    });

    const sns = new AWS.SNS({ apiVersion: messageConfig.api });
    const topicArn = messageConfig[topic].arn;
    const subject = `Forecast API: Subregion Updated - ${topic}`;
    const params = {
      TopicArn: topicArn,
      Message: subregionId.toString(),
      Subject: subject,
    };

    sns.publish(params, (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });

export default publishSubregionUpdateTopic;
