/* eslint-disable @typescript-eslint/no-misused-promises */

import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import type { Request, Response } from 'express';
import type Logger from 'bunyan';

import { trackForecastsRequests, getForecastHandler, createForecastHandler } from './forecasts';
import getConditionOptions from './conditions';

// Note: APP_VERSION is set in the Dockerfile--not from Parameter Store.
const forecasts = (log: Logger) => {
  const api = Router();
  api.get('/health', (_req: Request, res: Response) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );
  api.use('*', json(), trackForecastsRequests(log));
  api.get('/', wrapErrors(getForecastHandler));
  api.post('/', wrapErrors(createForecastHandler));
  api.get('/conditions', wrapErrors(getConditionOptions));

  return api;
};

export default forecasts;
