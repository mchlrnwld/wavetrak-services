import forecastDaysFixture from './forecastDays.json';
import forecastDaysDSTFixture from './forecastDaysDST.json';
import forecastDaysWithGapsFixture from './forecastDaysWithGaps.json';
import forecastDaysNoGapsRespFixture from './forecastDaysNoGapsResponse.json';

export const defaultForecastSummaryFixture = {
  forecaster: {
    name: '',
    title: '',
    email: '',
  },
  subregionId: '58581a836630e24c44878fd6',
  subregionName: 'North Orange County',
  subregionTimezoneOffset: -7, // PDT
  timezone: 'America/Los_Angeles',
  nextForecast: {
    day: '',
    time: '',
    timestamp: null,
    utcOffset: null,
  },
  nextForecastTimestamp: null,
  forecastStatus: {
    status: 'active',
    inactiveMessage: '',
  },
  highlights: [],
  bestBets: [],
  bets: {
    best: null,
    worst: null,
  },
  currentDate: 1627862400, // 2021-08-02T00:00:00Z
  forecastDate: 1627862400, // 2021-08-02T00:00:00Z
};

export const forecastSummaryDBFixture = {
  ...defaultForecastSummaryFixture,
  createdAt: new Date('2021-08-02T23:49:54.093Z'),
  forecastDate: new Date('2021-08-02T00:00:00.000Z'),
  updatedAt: new Date('2021-08-02T23:49:54.093Z'),
  nextForecastTimestamp: new Date('2021-08-06T14:00:00.000Z'),
};

export const forecastSummaryResponseFixture = {
  ...defaultForecastSummaryFixture,
  createdAt: '2021-08-02T23:49:54.093Z',
  forecastDate: '2021-08-02T00:00:00.000Z',
  updatedAt: '2021-08-02T23:49:54.093Z',
  nextForecastTimestamp: '2021-08-06T14:00:00.000Z',
};

export const forecastSummaryDBFixtureDST = {
  ...defaultForecastSummaryFixture,
  createdAt: new Date('2021-11-05T23:49:54.093Z'),
  forecastDate: new Date('2021-11-05T00:00:00.000Z'),
  updatedAt: new Date('2021-11-05T23:49:54.093Z'),
  nextForecastTimestamp: new Date('2021-11-08T14:00:00.000Z'),
};

export const forecastDaysDBFixture = forecastDaysFixture.map((day, index) => ({
  ...forecastDaysFixture[index],
  forecastDate: new Date(forecastDaysFixture[index].forecastDate),
  createdAt: new Date(forecastDaysFixture[index].createdAt),
  updatedAt: new Date(forecastDaysFixture[index].updatedAt),
}));

export const forecastDaysDSTDBFixture = forecastDaysDSTFixture.map((day, index) => ({
  ...forecastDaysDSTFixture[index],
  forecastDate: new Date(forecastDaysDSTFixture[index].forecastDate),
  createdAt: new Date(forecastDaysDSTFixture[index].createdAt),
  updatedAt: new Date(forecastDaysDSTFixture[index].updatedAt),
}));

export const forecastDaysResponseFixture = forecastDaysFixture.map((day, index) => ({
  ...forecastDaysFixture[index],
  forecastDate: forecastDaysFixture[index].forecastDate,
  createdAt: forecastDaysFixture[index].createdAt,
  updatedAt: forecastDaysFixture[index].updatedAt,
}));

export const forecastDaysWithGapsDBFixture = forecastDaysWithGapsFixture.map((day, index) => ({
  ...forecastDaysWithGapsFixture[index],
  forecastDate: new Date(forecastDaysWithGapsFixture[index].forecastDate),
  createdAt: new Date(forecastDaysWithGapsFixture[index].createdAt),
  updatedAt: new Date(forecastDaysWithGapsFixture[index].updatedAt),
}));

export const forecastDaysResponseNoGapsFixture = [...forecastDaysNoGapsRespFixture];
