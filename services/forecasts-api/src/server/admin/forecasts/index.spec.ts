import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import moment from 'moment-timezone';
import deepFreeze from 'deep-freeze';
import Logger from 'bunyan';
import forecastsAPI from '.';
import * as spotsAPI from '../../../common/spotsApi';
import * as forecastsModel from '../../../common/forecasts';
import SubregionForecastDayModel from '../../../model/SubregionForecastDayModel';
import SubregionForecastSummaryModel from '../../../model/SubregionForecastSummaryModel';
import createParamString from '../../../utils/createParamString';
import {
  toUnixTimestamp,
  createUTCOffsetCalculator,
  getMidnightUtcFromLocalTime,
} from '../../../utils/datetime';
import * as publishTopic from './publishTopic';
import forecastPostFixture from './fixtures/forecastPost.json';
import forecastDaysFixture from './fixtures/forecastDays.json';
import {
  defaultForecastSummaryFixture,
  forecastSummaryDBFixture,
  forecastSummaryDBFixtureDST,
  forecastDaysDBFixture,
  forecastDaysDSTDBFixture,
  forecastDaysWithGapsDBFixture,
  forecastDaysResponseNoGapsFixture,
  forecastSummaryResponseFixture,
  forecastDaysResponseFixture,
} from './fixtures/forecasts';

describe('/admin/forecasts', () => {
  let log: Logger;
  let request: ChaiHttp.Agent;
  let clock: sinon.SinonFakeTimers;

  let getSubregionStub: sinon.SinonStub;
  let getSpotsBySubregionStub: sinon.SinonStub;
  let findStub: sinon.SinonStub;
  let findOneStub: sinon.SinonStub;
  let updateForecastSummaryStub: sinon.SinonStub;
  let updateForecastDayStub: sinon.SinonStub;
  let publishTopicStub: sinon.SinonStub;

  before(() => {
    log = { trace: sinon.spy(), info: sinon.spy() } as unknown as Logger;
    const app = express();
    app.use(forecastsAPI(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    clock = sinon.useFakeTimers(1631250000 * 1000); // 2021-09-09 PDT, 2021-09-10 AEST;
    getSubregionStub = sinon.stub(spotsAPI, 'getSubregion');
    getSpotsBySubregionStub = sinon.stub(spotsAPI, 'getSpotsBySubregion');
    findStub = sinon.stub(SubregionForecastDayModel, 'find');
    findOneStub = sinon.stub(SubregionForecastSummaryModel, 'findOne');
    updateForecastSummaryStub = sinon.stub(forecastsModel, 'updateForecastSummary');
    updateForecastDayStub = sinon.stub(forecastsModel, 'updateForecastDay');
    publishTopicStub = sinon.stub(publishTopic, 'default');
  });

  afterEach(() => {
    clock.restore();
    getSubregionStub.restore();
    getSpotsBySubregionStub.restore();
    findStub.restore();
    findOneStub.restore();
    publishTopicStub.restore();
    updateForecastSummaryStub.restore();
    updateForecastDayStub.restore();
  });

  describe('GET /', () => {
    const subregionFixture = {
      _id: '58581a836630e24c44878fd6',
      name: 'North Orange County',
    };
    const timezone = 'America/Los_Angeles';
    const spotsFixture = [{ timezone }];
    const startMoment = deepFreeze(moment('2021-08-02 00:00:00').tz('UTC'));
    const endMoment = deepFreeze(moment('2021-08-10 00:00:00').tz('UTC'));
    const dstStartMoment = deepFreeze(moment('2021-11-05 00:00:00').tz('UTC'));
    const dstEndMoment = deepFreeze(moment('2021-11-08 00:00:00').tz('UTC'));

    const startDate = deepFreeze(startMoment.toISOString());
    const endDate = deepFreeze(endMoment.toISOString());
    const dstStartDate = deepFreeze(dstStartMoment.toISOString());
    const dstEndDate = deepFreeze(dstEndMoment.toISOString());

    const emptyDaysResponseFixture = [...Array<null>(endMoment.diff(startMoment, 'days'))].map(
      () => null,
    );

    it('should return default forecast summary and days for active subregion with no summary', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      findOneStub.returns({
        sort: () => ({ lean: () => {} }),
        lean: () => {},
      });
      const params = { subregionId: '58581a836630e24c44878fd6', startDate, endDate };
      const res = await request.get(`/?${createParamString(params)}`).send();
      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal(defaultForecastSummaryFixture);
      expect(body.days).to.deep.equal(emptyDaysResponseFixture);
    });

    it('should return default forecast summary and days for disabled subregion', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      const lean = () => ({
        ...forecastSummaryDBFixture,
        forecastStatus: {
          status: 'off',
          inactiveMessage: '',
        },
      });
      findOneStub.returns({
        sort: () => ({ lean }),
        lean,
      });
      const params = { subregionId: '58581a836630e24c44878fd6', startDate, endDate };
      const res = await request.get(`/?${createParamString(params)}`).send();
      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal({
        ...defaultForecastSummaryFixture,
        forecastStatus: {
          ...defaultForecastSummaryFixture.forecastStatus,
          status: 'off',
        },
      });
      expect(body.days).to.deep.equal(emptyDaysResponseFixture);
    });

    it('should return forecastDate for current day if no startDate provided', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      const lean = () => {};

      findOneStub.returns({
        sort: () => ({ lean }),
        lean,
      });

      const utcOffsetCalculator = createUTCOffsetCalculator(timezone);
      const subregionCurrMoment = moment().tz(timezone);
      const subregionTimezoneOffset = utcOffsetCalculator(subregionCurrMoment.unix());
      const subregionCurrDay = getMidnightUtcFromLocalTime(timezone, subregionCurrMoment);
      const defaultForecastSummaryFixtureNow = {
        ...defaultForecastSummaryFixture,
        currentDate: subregionCurrDay,
        forecastDate: subregionCurrDay,
        subregionTimezoneOffset,
      };
      const params = { subregionId: '58581a836630e24c44878fd6' };
      const res = await request.get(`/?${createParamString(params)}`).send();
      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal(defaultForecastSummaryFixtureNow);
      expect(body.days).to.deep.equal(emptyDaysResponseFixture);
    });

    it('should return forecast summary and days for a given date range', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      const lean = () => ({
        ...forecastSummaryDBFixture,
        forecastStatus: {
          status: 'active',
          inactiveMessage: '',
        },
      });
      findOneStub.returns({
        sort: () => ({ lean }),
        lean,
      });
      findStub.returns({
        limit: () => ({ sort: () => ({ lean: () => forecastDaysDBFixture }) }),
      });

      const forecastSummaryFixture = {
        ...defaultForecastSummaryFixture,
        createdAt: 1627948194, // 2021-08-02T23:49:54Z
        forecastDate: 1627862400, // 2021-08-02T00:00:00Z
        updatedAt: 1627948194, // 2021-08-02T23:49:54Z
        nextForecast: {
          ...defaultForecastSummaryFixture.nextForecast,
          timestamp: 1628258400, // 2021-08-02T00:00:00Z
          utcOffset: -7, // PDT
        },
        nextForecastTimestamp: 1628258400, // 2021-08-02T00:00:00Z
      };
      const forecastDaysResponse = forecastDaysFixture.map((_, index) => ({
        ...forecastDaysFixture[index],
        forecastDate: toUnixTimestamp(forecastDaysDBFixture[index].forecastDate),
        createdAt: toUnixTimestamp(forecastDaysDBFixture[index].createdAt),
        updatedAt: toUnixTimestamp(forecastDaysDBFixture[index].updatedAt),
        forecastDay: moment
          .tz(forecastDaysDBFixture[index].forecastDate, 'UTC')
          .format('YYYY-MM-DD'),
      }));

      const endDateToMatchFixture = deepFreeze(
        moment('2021-08-04 00:00:00').tz('UTC').toISOString(),
      );
      const params = {
        subregionId: '58581a836630e24c44878fd6',
        startDate,
        endDate: endDateToMatchFixture,
      };
      const res = await request.get(`/?${createParamString(params)}`).send();

      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal(forecastSummaryFixture);
      expect(body.days).to.deep.equal(forecastDaysResponse);
    });

    it('should handle missing days by inserting blank placholder days for cms mode', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      findOneStub.returns({
        sort: () => ({
          lean: () => ({
            ...forecastSummaryDBFixture,
            forecastStatus: {
              status: 'active',
              inactiveMessage: '',
            },
          }),
        }),
      });
      findStub.returns({
        limit: () => ({
          sort: () => ({
            lean: () => forecastDaysWithGapsDBFixture,
          }),
        }),
      });

      const forecastDaysResponse = forecastDaysResponseNoGapsFixture.map((_, index) => ({
        ...forecastDaysResponseNoGapsFixture[index],
        forecastDay: moment
          .tz(forecastDaysResponseNoGapsFixture[index].forecastDate * 1000, 'UTC')
          .format('YYYY-MM-DD'),
      }));

      const params = {
        subregionId: '58581a836630e24c44878fd6',
        startDate,
        endDate,
        cms: true,
      };
      const res = await request.get(`/?${createParamString(params)}`).send();
      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.days).to.deep.equal(forecastDaysResponse);
    });

    it('should handle missing days by using null days when not in cms mode', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      findOneStub.returns({
        sort: () => ({
          lean: () => ({
            ...forecastSummaryDBFixture,
            forecastStatus: {
              status: 'active',
              inactiveMessage: '',
            },
          }),
        }),
      });
      findStub.returns({
        limit: () => ({
          sort: () => ({
            lean: () => forecastDaysDBFixture,
          }),
        }),
      });

      const forecastSummaryFixture = {
        ...defaultForecastSummaryFixture,
        createdAt: 1627948194, // 2021-08-02T23:49:54Z
        forecastDate: 1627862400, // 2021-08-02T00:00:00Z
        updatedAt: 1627948194, // 2021-08-02T23:49:54Z
        nextForecast: {
          ...defaultForecastSummaryFixture.nextForecast,
          timestamp: 1628258400, // 2021-08-02T00:00:00Z
          utcOffset: -7, // PDT
        },
        nextForecastTimestamp: 1628258400, // 2021-08-02T00:00:00Z
      };
      const forecastDaysResponse = [...Array<unknown>(8)].map((_, index) => {
        if (index < forecastDaysFixture.length) {
          return {
            ...forecastDaysFixture[index],
            forecastDate: toUnixTimestamp(forecastDaysDBFixture[index].forecastDate),
            createdAt: toUnixTimestamp(forecastDaysDBFixture[index].createdAt),
            updatedAt: toUnixTimestamp(forecastDaysDBFixture[index].updatedAt),
            forecastDay: moment
              .tz(forecastDaysDBFixture[index].forecastDate, 'UTC')
              .format('YYYY-MM-DD'),
          };
        }

        // Note: the fixture only has 2 days, so 8 days will be null
        return null;
      });

      const params = { subregionId: '58581a836630e24c44878fd6', startDate, endDate };
      const res = await request.get(`/?${createParamString(params)}`).send();

      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal(forecastSummaryFixture);
      expect(body.days).to.deep.equal(forecastDaysResponse);
    });

    it('should return correct next forecast utcOffset across DST change', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      findOneStub.returns({
        sort: () => ({
          lean: () => ({
            ...forecastSummaryDBFixtureDST,
            forecastStatus: {
              status: 'active',
              inactiveMessage: '',
            },
          }),
        }),
      });
      findStub.returns({
        limit: () => ({
          sort: () => ({
            lean: () => forecastDaysDSTDBFixture,
          }),
        }),
      });

      const forecastSummaryFixtureDST = {
        ...defaultForecastSummaryFixture,
        currentDate: 1636070400, // 2021-11-05T00:00:00Z
        createdAt: 1636156194, // 2021-11-05T23:49:54Z
        forecastDate: 1636070400, // 2021-08-02T00:00:00Z
        updatedAt: 1636156194, // 2021-11-05T23:49:54Z
        nextForecast: {
          ...defaultForecastSummaryFixture.nextForecast,
          timestamp: 1636380000, // 2021-11-08T14:00:00Z
          utcOffset: -8, // PST
        },
        nextForecastTimestamp: 1636380000, // 2021-11-08T14:00:00Z
      };
      const forecastDaysResponseFixtureDST = forecastDaysDSTDBFixture.map((_day, index) => ({
        ...forecastDaysDSTDBFixture[index],
        forecastDate: toUnixTimestamp(forecastDaysDSTDBFixture[index].forecastDate),
        createdAt: toUnixTimestamp(forecastDaysDSTDBFixture[index].createdAt),
        updatedAt: toUnixTimestamp(forecastDaysDSTDBFixture[index].updatedAt),
        forecastDay: moment
          .tz(forecastDaysDSTDBFixture[index].forecastDate, 'UTC')
          .format('YYYY-MM-DD'),
      }));
      const params = {
        subregionId: '58581a836630e24c44878fd6',
        startDate: dstStartDate,
        endDate: dstEndDate,
      };
      const res = await request.get(`/?${createParamString(params)}`).send();
      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal(forecastSummaryFixtureDST);
      expect(body.days).to.deep.equal(forecastDaysResponseFixtureDST);
    });

    it('should handle date string format for startDate and endDate params', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns(spotsFixture);
      findOneStub.returns({
        sort: () => ({
          lean: () => ({
            ...forecastSummaryDBFixtureDST,
            forecastStatus: {
              status: 'active',
              inactiveMessage: '',
            },
          }),
        }),
      });
      findStub.returns({
        limit: () => ({
          sort: () => ({
            lean: () => forecastDaysDSTDBFixture,
          }),
        }),
      });

      const forecastSummaryFixtureDST = {
        ...defaultForecastSummaryFixture,
        currentDate: 1636070400, // 2021-11-05T00:00:00Z
        createdAt: 1636156194, // 2021-11-05T23:49:54Z
        forecastDate: 1636070400, // 2021-08-02T00:00:00Z
        updatedAt: 1636156194, // 2021-11-05T23:49:54Z
        nextForecast: {
          ...defaultForecastSummaryFixture.nextForecast,
          timestamp: 1636380000, // 2021-11-08T14:00:00Z
          utcOffset: -8, // PST
        },
        nextForecastTimestamp: 1636380000, // 2021-11-08T14:00:00Z
      };
      const forecastDaysResponseFixtureDST = forecastDaysDSTDBFixture.map((_day, index) => ({
        ...forecastDaysDSTDBFixture[index],
        forecastDate: toUnixTimestamp(forecastDaysDSTDBFixture[index].forecastDate),
        createdAt: toUnixTimestamp(forecastDaysDSTDBFixture[index].createdAt),
        updatedAt: toUnixTimestamp(forecastDaysDSTDBFixture[index].updatedAt),
        forecastDay: moment
          .tz(forecastDaysDSTDBFixture[index].forecastDate, 'UTC')
          .format('YYYY-MM-DD'),
      }));
      const params = {
        subregionId: '58581a836630e24c44878fd6',
        startDate: '2021-11-05',
        endDate: '2021-11-08',
      };
      const res = await request.get(`/?${createParamString(params)}`).send();
      const body = res.body as { summary: Record<string, unknown>; days: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(body.summary).to.deep.equal(forecastSummaryFixtureDST);
      expect(body.days).to.deep.equal(forecastDaysResponseFixtureDST);
    });

    it('should return 400 status code for invalid subregionId', async () => {
      getSubregionStub.throws({ message: 'Subregion not found' });
      const params = { subregionId: 'invalid', startDate, endDate };
      const res = await request.get(`/?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'Invalid subregion ID' });
    });

    it('should return 400 status code for subregion with no spots', async () => {
      getSubregionStub.returns(subregionFixture);
      getSpotsBySubregionStub.returns([]);
      const params = { subregionId: 'invalid', startDate, endDate };
      const res = await request.get(`/?${createParamString(params)}`).send();
      expect(res).to.have.status(400);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ message: 'No spots listed for subregion' });
    });
  });

  describe('GET /conditions', () => {
    it('should return a set of conditions suitable for forms', async () => {
      const res = await request.get('/conditions').send();
      const body = res.body as { conditions: unknown[]; occ: unknown[]; ratings: unknown[] };
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.have.property('conditions');
      expect(res.body).to.have.property('occ');
      expect(res.body).to.have.property('ratings');
      expect(body.conditions).to.be.instanceOf(Array);
      expect(body.occ).to.be.instanceOf(Array);
      expect(body.ratings).to.be.instanceOf(Array);
    });
  });

  describe('POST /', () => {
    const subregionFixture = {
      _id: '58581a836630e24c44878fd6',
      name: 'North Orange County',
    };

    it('should post forecast days', async () => {
      publishTopicStub.resolves({});
      getSubregionStub.returns(subregionFixture);
      updateForecastSummaryStub.resolves(forecastSummaryDBFixture);
      updateForecastDayStub.resolves(forecastDaysDBFixture);

      const res = await request.post('/').send(forecastPostFixture);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        days: [forecastDaysResponseFixture],
        summary: forecastSummaryResponseFixture,
      });
    });

    it('validates forecasted days begin on current day in the subregion', async () => {
      publishTopicStub.resolves({});
      getSubregionStub.returns(subregionFixture);
      updateForecastSummaryStub.resolves(forecastSummaryDBFixture);
      updateForecastDayStub.resolves(forecastDaysDBFixture);

      const auPostFixture = {
        ...forecastPostFixture,
        summary: {
          ...forecastPostFixture.summary,
          timezone: 'Australia/Sydney',
        },
      };

      const res = await request.post('/').send(auPostFixture);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        days: [
          {
            errors: {
              skippedFirstDay: {
                message: 'Day 1 forecast must be completed.',
              },
            },
            forecastDate: '2021-09-09',
          },
        ],
        summary: {},
        message: 'Found errors',
      });
    });

    it('validates no days have been skipped', async () => {
      publishTopicStub.resolves({});
      getSubregionStub.returns(subregionFixture);
      updateForecastSummaryStub.resolves(forecastSummaryDBFixture);
      updateForecastDayStub.resolves(forecastDaysDBFixture);
      const skippedDaysPostFixture = {
        ...forecastPostFixture,
        days: [
          ...forecastPostFixture.days,
          {
            forecastDate: '2021-09-12T00:00:00.000Z',
            forecastDay: '2021-09-12',
            subregionId: '58581a836630e24c44878fd6',
            forecast: {
              am: {
                rating: 'POOR_TO_FAIR',
                occasionalHeight: 0,
                condition: 2,
                observation: '',
              },
              pm: {
                rating: 'FAIR_TO_GOOD',
                occasionalHeight: 0,
                condition: 4,
                observation: '',
              },
              observation: 'Description.',
            },
          },
        ],
      };

      const res = await request.post('/').send(skippedDaysPostFixture);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        days: [
          {
            errors: {
              skipped: {
                message:
                  'Forecasts skipped after 2021-09-09. Please complete forecast for: 2021-09-10, 2021-09-11',
              },
            },
            forecastDate: '2021-09-09',
          },
        ],
        summary: {},
        message: 'Found errors',
      });
    });

    it('validates forecast day fields', async () => {
      publishTopicStub.resolves({});
      getSubregionStub.returns(subregionFixture);
      updateForecastSummaryStub.resolves(forecastSummaryDBFixture);
      updateForecastDayStub.resolves(forecastDaysDBFixture);

      const skippedDaysPostFixture = {
        ...forecastPostFixture,
        days: [
          ...forecastPostFixture.days,
          {
            forecastDate: '2021-09-09T00:00:00.000Z',
            forecastDay: '2021-09-09',
            subregionId: '58581a836630e24c44878fd6',
            forecast: {
              am: {
                rating: 'NONE',
                occasionalHeight: 0,
                condition: 2,
                observation: '',
              },
              pm: {
                rating: 'FAIR_TO_GOOD',
                occasionalHeight: 0,
                condition: 4,
                observation: '',
              },
              observation: 'Description.',
            },
          },
        ],
      };

      const res = await request.post('/').send(skippedDaysPostFixture);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        days: [
          {
            errors: {
              ampmRatingRequired: {
                message: 'AM rating must be completed for 2021-09-09',
              },
            },
            forecastDate: '2021-09-09',
          },
        ],
        summary: {},
        message: 'Found errors',
      });
    });
  });
});
