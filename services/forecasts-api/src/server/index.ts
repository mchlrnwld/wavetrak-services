import { setupExpress } from '@surfline/services-common';
import logger from '../common/logger';
import admin from './admin';

const log = logger('forecasts-api');

const setupApp = (port: number) =>
  setupExpress({
    port,
    name: 'forecasts-api',
    log,
    allowedMethods: 'GET, POST, PUT, DELETE, OPTIONS',
    handlers: [['/admin', admin(log)]],
  });

export default setupApp;
