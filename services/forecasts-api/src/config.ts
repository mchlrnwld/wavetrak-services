export default {
  development: {
    AWS_API_REGION: 'us-west-1',
    AWS_API_VERSION: '2010-03-31',
    AWS_API_SPOT_UPDATED_TOPIC_ARN:
      'arn:aws:sns:us-west-1:665294954271:spot_report_updated_sandbox',
  },
  sandbox: {
    AWS_API_REGION: 'us-west-1',
    AWS_API_VERSION: '2010-03-31',
    AWS_API_SPOT_UPDATED_TOPIC_ARN:
      'arn:aws:sns:us-west-1:665294954271:spot_report_updated_sandbox',
  },
  staging: {
    AWS_API_REGION: 'us-west-1',
    AWS_API_VERSION: '2010-03-31',
    AWS_API_SPOT_UPDATED_TOPIC_ARN:
      'arn:aws:sns:us-west-1:665294954271:spot_report_updated_staging',
  },
  production: {
    AWS_API_REGION: 'us-west-1',
    AWS_API_VERSION: '2010-03-31',
    AWS_API_SPOT_UPDATED_TOPIC_ARN:
      'arn:aws:sns:us-west-1:665294954271:spot_report_updated_staging',
  },
}[process.env.APP_ENV || 'development'];
