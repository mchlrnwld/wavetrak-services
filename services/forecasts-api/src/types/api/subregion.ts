import type { Types } from 'mongoose';
import type { EmptyPlaceholderForecastPoint, Forecaster, ForecastPoint } from '../forecast';
import { DateInput } from '../mongoose';

export interface SubregionForecastSummaryInput {
  subregionId: string;
  subregionName: string;
  subregionTimezoneOffset: number;
  timezone: string;
  nextForecast: {
    timestamp: number;
    day: string;
    time: string;
  };
  nextForecastTimestamp: string;
  forecaster: Forecaster;
  forecastStatus: {
    status: 'active' | 'inactive';
    inactiveMessage: string;
  };
  highlights: string[];
  bestBets: string[];
  bets: {
    best: string;
    worst: string;
  };
  forecastDate: string;
  currentDate: string;
  _id: string;
  updatedAt: number | null;
  createdAt: number | null;
}

export interface SubregionForecastDayInput {
  forecast: {
    am: ForecastPoint;
    pm: ForecastPoint;
    observation: string;
  };
  forecastDate: string;
  subregionId: string;
  updatedAt: number | null;
  createdAt: number | null;
  forecastDay: string;
}

export interface CreateForecastInput {
  days: SubregionForecastDayInput[];
  summary: SubregionForecastSummaryInput;
}

export interface GetSubregionForecastDayResponse {
  _id?: string;
  forecastDate: DateInput;
  subregionId: string;
  forecast: {
    am: ForecastPoint | EmptyPlaceholderForecastPoint;
    pm: ForecastPoint | EmptyPlaceholderForecastPoint;
    observation?: string;
  };
  updatedAt: number | null;
  createdAt: number | null;
}

export interface GetSubregionForecastSummaryResponse {
  _id?: Types.ObjectId;
  updatedAt?: number;
  createdAt?: number;
  subregionId: string;
  subregionName: string;
  subregionTimezoneOffset: number;
  timezone: string;
  nextForecast: {
    day: string;
    time: string;
    timestamp: number | null;
    utcOffset: number | null;
  };
  // TODO FE-639: Remove deprecated nextForecastTimestamp
  nextForecastTimestamp: number | null;
  forecaster: Forecaster;
  forecastStatus: {
    status: 'active' | 'inactive' | 'off';
    inactiveMessage: string;
  };
  highlights: string[];
  bestBets: string[];
  bets: {
    best: string | null;
    worst: string | null;
  };
  forecastDate: number;
  currentDate: number;
}

export interface GetSubregionForecastResponse {
  days: GetSubregionForecastDayResponse[];
  summary: GetSubregionForecastSummaryResponse;
}
