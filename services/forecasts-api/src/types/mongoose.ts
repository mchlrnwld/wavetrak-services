export type DateString = string;
export type UnixMilliseconds = number;
export type DateInput = Date | DateString | UnixMilliseconds;
export type DateOrTimestamp = Date | number;
