import type { HumanRelation, Rating } from './ratings';

export interface Condition {
  condition: number;
  minHeight: number;
  maxHeight: number;
  humanRelation: HumanRelation;
}

export interface Forecaster {
  email: string;
  name: string;
  title: string;
}

export interface ForecastPoint {
  minHeight: number;
  maxHeight: number;
  rating: Rating;
  condition: number;
  occasionalHeight?: number;
  humanRelation: HumanRelation;
}

export interface EmptyPlaceholderForecastPoint {
  minHeight: 0;
  maxHeight: 0;
  rating: 'None';
  condition: 0;
  occasionalHeight: 0;
  humanRelation: 'None';
}

export interface WaveHeight {
  min: number;
  max: number;
  plus: boolean;
}

export interface ForecastHour {
  timestamp: number;
  waveHeight: WaveHeight;
  rating: Rating;
}
