import type { Types } from 'mongoose';
import type { ForecastPoint } from '../forecast';
import { DateInput } from '../mongoose';

export interface SubregionForecastDay {
  _id: Types.ObjectId;
  forecastDate: DateInput;
  subregionId: Types.ObjectId;
  forecast: {
    am: ForecastPoint;
    pm: ForecastPoint;
  };
  observation?: string;
  updatedAt: Date;
  createdAt: Date;
}

export interface SubregionForecastSummary {
  _id: Types.ObjectId;
  forecastDate: DateInput;
  nextForecastTimestamp: DateInput;
  nextForecast: {
    day: string;
    time: string;
  };
  forecastStatus: {
    status: 'active' | 'inactive' | 'off';
    inactiveMessage: string;
  };
  highlights: string[];
  bestBets: string[];
  bets: {
    best: string;
    worst: string;
  };
  subregionId: Types.ObjectId;
  forecaster: {
    email: string;
    name: string;
    title: string;
  };
  updatedAt: Date;
  createdAt: Date;
}
