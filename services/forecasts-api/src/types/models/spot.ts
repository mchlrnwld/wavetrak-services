import type { Types } from 'mongoose';
import type { ForecastHour } from '../forecast';
import { DateInput } from '../mongoose';

export interface SpotForecastModel {
  forecastDate: DateInput;
  spotId: Types.ObjectId;
  forecast: {
    hours: ForecastHour[];
    observation: string;
  };
}
