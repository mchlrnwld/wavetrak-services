import { SurflineQuery, SurflineRequest, SurflineHeaders } from '@surfline/services-common';

// HACK: SurflineQuery defines all query parameters as type string, which does not account for
// nested parameters like units.
interface Units {
  temperature: string;
  tideHeight: string;
  swellHeight: string;
  waveHeight: string;
  windSpeed: string;
}

export interface WavetrakRequest<Body = undefined> extends SurflineRequest {
  body: Body;
  correctedWaveHeights: boolean;
  query: SurflineQuery & {
    units: Units;
    startDate: string;
    endDate: string;
    subregionId: string;
    days: string;
    cms: 'true' | 'false';
  };
  user: {
    premium: boolean;
  };
  headers: SurflineHeaders & {
    'X-Auth-EmployeeEmail'?: string;
    'X-Auth-EmployeeName'?: string;
    'X-Auth-EmployeeTitle'?: string;
  };
}
