import type { SurfConditions, SurfRatings } from '../model/conditions';

export type Rating = keyof SurfRatings;
export type RatingHumanRelation = SurfRatings[keyof SurfRatings];
export type HumanRelation = SurfConditions[keyof SurfConditions]['humanRelation'];
