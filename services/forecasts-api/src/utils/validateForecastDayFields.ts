import moment from 'moment-timezone';
import { surfConditions } from '../model/conditions';
import { SubregionForecastDayInput, SubregionForecastSummaryInput } from '../types/api';

export const validateForecastSummaryFields = (summary: SubregionForecastSummaryInput) => {
  if (!moment(summary.forecastDate).isValid()) {
    return {
      forecastDate: {
        message: 'Please submit a valid summary forecast date',
      },
    };
  }
  return null;
};

export const validateForecastDayFields = (
  days: SubregionForecastDayInput[],
  summary: SubregionForecastSummaryInput,
  day: SubregionForecastDayInput,
  index: number,
) => {
  let fieldErrors = {};
  const currentSubregionDay = moment.tz(summary.timezone).format('YYYY-MM-DD');
  if (index === 0 && !moment(day.forecastDay).isSame(currentSubregionDay, 'day')) {
    fieldErrors = {
      ...fieldErrors,
      skippedFirstDay: { message: 'Day 1 forecast must be completed.' },
    };
  }
  if (index < days.length - 1) {
    const diff = moment(days[index + 1].forecastDay).diff(day.forecastDay, 'days');
    if (diff > 1) {
      const daysSkipped = [...Array<moment.Moment>(diff - 1)].map((_, daysToAdd) =>
        moment(day.forecastDay)
          .clone()
          .add(daysToAdd + 1, 'days')
          .format('YYYY-MM-DD'),
      );
      fieldErrors = {
        ...fieldErrors,
        skipped: {
          message: `Forecasts skipped after ${
            day.forecastDay
          }. Please complete forecast for: ${daysSkipped.join(', ')}`,
        },
      };
    }
  }

  ['am', 'pm'].forEach((ampm: 'am' | 'pm') => {
    if (Number(day.forecast[ampm].condition) === 0) {
      fieldErrors = {
        ...fieldErrors,
        ampmConditionRequired: {
          message: `${ampm.toUpperCase()} condition must be completed for ${day.forecastDay}`,
        },
      };
    }
    if (day.forecast[ampm].rating === 'NONE') {
      fieldErrors = {
        ...fieldErrors,
        ampmRatingRequired: {
          message: `${ampm.toUpperCase()} rating must be completed for ${day.forecastDay}`,
        },
      };
    }
  });

  if (!surfConditions[day.forecast.am.condition]) {
    fieldErrors = {
      ...fieldErrors,
      'forecast.am.condition': {
        message: `${day.forecast.am.condition} is an invalid condition enum for AM forecast.`,
      },
    };
  }
  if (!surfConditions[day.forecast.pm.condition]) {
    fieldErrors = {
      ...fieldErrors,
      'forecast.pm.condition': {
        message: `${day.forecast.pm.condition} is an invalid condition enum for PM forecast.`,
      },
    };
  }

  if (!moment(day.forecastDay).isValid()) {
    fieldErrors = {
      ...fieldErrors,
      forecastDate: {
        message: 'The forecast day is invalid.',
      },
    };
  }
  return fieldErrors;
};

export default validateForecastDayFields;
