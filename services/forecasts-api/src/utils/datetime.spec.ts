import { expect } from 'chai';
import sinon from 'sinon';
import {
  midnightOfDate,
  getMidnightUtcFromLocalTime,
  getForecastDateRange,
  toUnixTimestamp,
} from './datetime';

describe('utils / datetime', () => {
  let clock: sinon.SinonFakeTimers;
  const currentDate = 1629183600; // 2021-08-17T00:00:00 PDT

  beforeEach(() => {
    clock = sinon.useFakeTimers(currentDate * 1000);
  });

  afterEach(() => {
    clock.restore();
  });

  describe('midnightOfDate', () => {
    it('returns midnight of date local to timezone', () => {
      expect(midnightOfDate('America/Los_Angeles', new Date('2017-05-03T07:27:38Z'))).to.deep.equal(
        new Date('2017-05-03T07:00:00Z'),
      );
      expect(midnightOfDate('America/Los_Angeles', new Date('2017-05-03T21:27:38Z'))).to.deep.equal(
        new Date('2017-05-03T07:00:00Z'),
      );
      expect(midnightOfDate('America/Los_Angeles', new Date('2017-05-03T01:00:00Z'))).to.deep.equal(
        new Date('2017-05-02T07:00:00Z'),
      );
    });

    it('returns midnight timestamp local to timezone if convertToTimestamp is true', () => {
      expect(
        midnightOfDate('America/Los_Angeles', new Date('2017-05-03T07:27:38Z'), true),
      ).to.equal(1493794800);
      expect(
        midnightOfDate('America/Los_Angeles', new Date('2017-05-03T21:27:38Z'), true),
      ).to.equal(1493794800);
      expect(
        midnightOfDate('America/Los_Angeles', new Date('2017-05-03T01:00:00Z'), true),
      ).to.equal(1493708400);
    });
  });

  describe('getMidnightUtcFromLocalTime', () => {
    it('converts local date to midnight UTC timestamp', () => {
      expect(
        getMidnightUtcFromLocalTime('America/Los_Angeles', new Date('2017-05-03T07:27:38Z')),
      ).to.equal(1493769600); // 2017-05-03T00:00:00Z
      expect(
        getMidnightUtcFromLocalTime('America/Los_Angeles', new Date('2017-05-03T21:27:38Z')),
      ).to.equal(1493769600); // 2017-05-03T00:00:00Z
      expect(
        getMidnightUtcFromLocalTime('America/Los_Angeles', new Date('2017-05-03T01:00:00Z')),
      ).to.equal(1493683200); // 2017-05-02T00:00:00Z
    });
  });

  describe('getForecastDateRange', () => {
    it('returns range of midnight UTC timestamps in ms from current day to n days out for a given timezone', () => {
      const timezone = 'America/Los_Angeles';
      expect(getForecastDateRange(timezone, 7)).to.deep.equal({
        utcStartDate: toUnixTimestamp(+new Date('2021-08-17T00:00:00Z')) * 1000,
        utcEndDate: toUnixTimestamp(+new Date('2021-08-24T00:00:00Z')) * 1000,
      });

      expect(getForecastDateRange(timezone, 8)).to.deep.equal({
        utcStartDate: toUnixTimestamp(new Date('2021-08-17T00:00:00Z')) * 1000,
        utcEndDate: toUnixTimestamp(new Date('2021-08-25T00:00:00Z')) * 1000,
      });

      const updatedTimezone = 'Australia/Brisbane';
      expect(getForecastDateRange(updatedTimezone, 8)).to.deep.equal({
        utcStartDate: toUnixTimestamp(new Date('2021-08-17T00:00:00Z')) * 1000,
        utcEndDate: toUnixTimestamp(new Date('2021-08-25T00:00:00Z')) * 1000,
      });
    });

    it('returns range of midnight UTC timestamps in ms of current day to n days out across PST change', () => {
      const weekOfDST = 1636182000; // 2021-11-06 PDT, DST change 2021-11-07
      clock = sinon.useFakeTimers(weekOfDST * 1000);
      const timezone = 'America/Los_Angeles';
      expect(getForecastDateRange(timezone, 8)).to.deep.equal({
        utcStartDate: toUnixTimestamp(new Date('2021-11-06T00:00:00Z')) * 1000,
        utcEndDate: toUnixTimestamp(new Date('2021-11-14T00:00:00Z')) * 1000,
      });
    });

    it('returns range of midnight UTC timestamps in ms of current day to n days out across PDT change', () => {
      const weekOfDST = 1615536000; // 2021-03-12 PST, PDT change 2021-03-14
      clock = sinon.useFakeTimers(weekOfDST * 1000);
      const timezone = 'America/Los_Angeles';
      expect(getForecastDateRange(timezone, 8)).to.deep.equal({
        utcStartDate: toUnixTimestamp(new Date('2021-03-12T00:00:00Z')) * 1000,
        utcEndDate: toUnixTimestamp(new Date('2021-03-20T00:00:00Z')) * 1000,
      });
    });
  });
});
