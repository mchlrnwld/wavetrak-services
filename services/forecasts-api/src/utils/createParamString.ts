const createParamString = (params: Record<string, string | number | boolean>) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export default createParamString;
