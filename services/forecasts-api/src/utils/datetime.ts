import type { Moment } from 'moment';
import moment from 'moment-timezone';

export const toUnixTimestamp = (date: Date | number) => Math.floor(+date / 1000);

export const createUTCOffsetCalculator = (timezone: string) => {
  const zone = moment.tz.zone(timezone);
  // zone.utcOffset is inverted to be POSIX compatible.
  // See https://momentjs.com/timezone/docs/#/zone-object/offset/
  return (unixTimestamp: number) => -zone.utcOffset(unixTimestamp * 1000) / 60.0;
};

export const midnightOfDate = (
  timezone: string,
  date: string | Date = new Date(),
  convertToTimestamp = false,
) => {
  const midnightMoment = moment(date).tz(timezone).startOf('day');
  return convertToTimestamp ? midnightMoment.unix() : midnightMoment.toDate();
};

export const getMidnightUtcFromLocalTime = (
  timezone: string,
  date: Moment | Date | number | string,
) => {
  const dateString = moment.tz(date, timezone).startOf('day').format('YYYY-MM-DD');
  return midnightOfDate('UTC', dateString, true);
};

export const getForecastDateRange = (timezone: string, daysToReturn: number) => {
  const subregionStartMoment = moment().tz(timezone);
  const subregionEndMoment = subregionStartMoment.clone().add(daysToReturn, 'days');
  const utcStartDate = +getMidnightUtcFromLocalTime(timezone, subregionStartMoment) * 1000;
  const utcEndDate = +getMidnightUtcFromLocalTime(timezone, subregionEndMoment) * 1000;
  return { utcStartDate, utcEndDate };
};
