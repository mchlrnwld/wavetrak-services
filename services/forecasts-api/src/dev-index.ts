/* istanbul ignore file */
/* eslint-disable import/first */
// eslint-disable-next-line import/no-extraneous-dependencies
import dotenv from 'dotenv';

dotenv.config();

import './index';
