
  # Health returns 200
  describe command('curl -ksv http://localhost:8080/health -H "Host:platform.surfline.com" -H "X-Forwarded-Proto:http" | grep \'200\'') do
    its('stdout') { should match /200/ }
  end

  describe command('curl -ksv http://localhost:8080/crowds/cam/583499c4e411dc743a5d5296?startDate=1644278400&endDate=1644404658 -H "Host:platform.surfline.com" -H "X-Forwarded-Proto:http" | grep \'200\'') do
    its('stdout') { should match /200/ }
  end

  describe command('curl -ksv http://localhost:8080/crowds/cam/583499c4e411dc743a5d5296/raw?startDate=1644278400&endDate=1644404658 -H "Host:platform.surfline.com" -H "X-Forwarded-Proto:http" | grep \'404\'') do
    its('stdout') { should match /404/ }
  end
