# Buoyweather tests

# /auth/authorise - Returns 'invalid_request'
describe command('curl -ksv "http://localhost:8080/auth/authorise" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'invalid_request\'') do
  its('stdout') { should match /invalid_request/ }
end

# /user - returns Authentication Required
describe command('curl -ksv "http://localhost:8080/user" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'Authentication\'') do
  its('stdout') { should match /Authentication/ }
end

# /entitlements - returns Authentication Required
describe command('curl -ksv "http://localhost:8080/entitlements" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'Authentication\'') do
  its('stdout') { should match /Authentication/ }
end

# /favorites - returns You are not authorized to view the resource
describe command('curl -ksv "http://localhost:8080/favorites" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'authorized\'') do
  its('stdout') { should match /authorized/ }
end

# solunar returns both 'solunar' and 'associated' in response
describe command('curl -ksv "http://localhost:8080/solunar?lat=33.65&lon=-118" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'solunar\'') do
  its('stdout') { should match /solunar/ }
end

describe command('curl -ksv "http://localhost:8080/solunar?lat=33.65&lon=-118" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'associated\'') do
  its('stdout') { should match /associated/ }
end

# tides returns both 'tides' and 'associated' in response
describe command('curl -ksv "http://localhost:8080/tides?lat=33.65&lon=-118" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'tides\'') do
  its('stdout') { should match /tides/ }
end

describe command('curl -ksv "http://localhost:8080/tides?lat=33.65&lon=-118" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'associated\'') do
  its('stdout') { should match /associated/ }
end

# wind returns both 'wind' and 'associated' in response
describe command('curl -ksv "http://localhost:8080/forecasts/wind?lat=33.65&lon=-118" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'wind\'') do
  its('stdout') { should match /wind/ }
end

describe command('curl -ksv "http://localhost:8080/forecasts/wind?lat=33.65&lon=-118" -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'associated\'') do
  its('stdout') { should match /associated/ }
end

# Health returns 200
describe command('curl -ksv http://localhost:8080/health -H "Host:services.buoyweather.com" -H "X-Forwarded-Proto:http" | grep \'200\'') do
  its('stdout') { should match /200/ }
end

