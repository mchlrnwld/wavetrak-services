# Surfline tests
# Add more here

# /auth/authorise - Returns 'invalid_request'
describe command('curl -ksv "http://localhost:8080/auth/authorise" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'invalid_request\'') do
  its('stdout') { should match /invalid_request/ }
end

# /user - returns Authentication Required
describe command('curl -ksv "http://localhost:8080/user" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'Authentication\'') do
  its('stdout') { should match /Authentication/ }
end

# /entitlements - returns Authentication Required
describe command('curl -ksv "http://localhost:8080/entitlements" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'Authentication\'') do
  its('stdout') { should match /Authentication/ }
end

# /notications/outlets - returns Authentication Required
describe command('curl -ksv "http://localhost:8080/notifications/outlets" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'Authentication\'') do
  its('stdout') { should match /Authentication/ }
end

# /meter - returns Authentication Required
describe command('curl -ksv "http://localhost:8080/meter" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'Authentication\'') do
  its('stdout') { should match /Authentication/ }
end

# kbyg/spots/forecasts/conditions for HB north side - return both associated and data
# describe command('curl -ksv "http://localhost:8080/kbyg/spots/forecasts/conditions?spotId=5842041f4e65fad6a7708827" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'associated\'') do
#  its('stdout') { should match /associated/ }
# end

# describe command('curl -ksv "http://localhost:8080/kbyg/spots/forecasts/conditions?spotId=5842041f4e65fad6a7708827" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'data\'') do
#   its('stdout') { should match /data/ }
# end

# /search/site for q=huntington return suggest and huntington
describe command('curl -ksv "http://localhost:8080/search/site?q=huntington&querySize=1&suggestionSize=1" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'huntington\'') do
  its('stdout') { should match /huntington/ }
end

describe command('curl -ksv "http://localhost:8080/search/site?q=huntington&querySize=1&suggestionSize=1" -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'suggest\'') do
  its('stdout') { should match /suggest/ }
end

# Health returns 200
describe command('curl -ksv http://localhost:8080/health -H "Host:services.surfline.com" -H "X-Forwarded-Proto:http" | grep \'200\'') do
  its('stdout') { should match /200/ }
end

