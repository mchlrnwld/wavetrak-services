#!/usr/bin/env sh
set -e

if [ "$1" = 'openresty' ]; then
    export DNS_SERVER=$(grep 'nameserver' /etc/resolv.conf | awk '{print $2}' || echo '8.8.8.8')
    /usr/local/bin/confd -onetime -backend env
    exec /usr/local/openresty/bin/openresty -g "daemon off;"
else
    exec "$@"
fi
