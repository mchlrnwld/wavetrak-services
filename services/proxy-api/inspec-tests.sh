#!/bin/bash

ENV="tests"

CONTAINER_BUILD=$(docker build --build-arg APP_ENV=${ENV} -t services-proxy .)
CONTAINER_RUN=$(docker run --env-file .env.test -p 8080:8080 -dt services-proxy openresty)

# run inspec test
TEST_RESPONSE=$(inspec exec test/**/*.rb -t docker://$CONTAINER_RUN)

echo "stopping test container ${CONTAINER_RUN}"
docker stop $CONTAINER_RUN

echo "removing test container ${CONTAINER_RUN}"
docker rm $CONTAINER_RUN

# set -x here to avoid docker build output and duplicate tests response info
# will output to the console as part of the matching below
set -x

# Match a failure and if a match, send non-zero exit code
echo "*** TESTING OUTPUT AGAINST FAILURE SUBSTRING ***"
FAILURE_MATCH="Diff:"
if [[ "$TEST_RESPONSE" == *"$FAILURE_MATCH"* ]]
then
  echo "*** inspec tests failed.  See output above *** "
  exit 1;
else
  echo "*** inspec tests successful.  See output above *** "
fi
