server {
  listen 8080;
  server_name
    services.local.surfline.com
    services.sandbox.surfline.com
    services.staging.surfline.com
    services.surfline.com;
  proxy_connect_timeout 10;
  proxy_read_timeout 10;

  set $upstream_auth_service {{getenv "UPSTREAM_AUTH_SERVICE"}};
  set $upstream_cameras_service {{getenv "UPSTREAM_CAMERAS_SERVICE"}};
  set $upstream_charts_service {{getenv "UPSTREAM_CHARTS_SERVICE"}};
  set $upstream_entitlement_service {{getenv "UPSTREAM_ENTITLEMENTS_SERVICE"}};
  set $upstream_favorites_service {{getenv "UPSTREAM_FAVORITES_SERVICE"}};
  set $upstream_feed_api {{getenv "UPSTREAM_FEED_API"}};
  set $upstream_geo_target_service {{getenv "UPSTREAM_GEO_TARGET_SERVICE"}};
  set $upstream_graphql_gateway {{getenv "UPSTREAM_GRAPHQL_GATEWAY"}};
  set $upstream_kbyg_product_api {{getenv "UPSTREAM_KBYG_PRODUCT_API"}};
  set $upstream_location_view_service {{getenv "UPSTREAM_LOCATION_VIEW_SERVICE"}};
  set $upstream_metering_service {{getenv "UPSTREAM_METERING_SERVICE"}};
  set $upstream_notifications_service {{getenv "UPSTREAM_NOTIFICATIONS_SERVICE"}};
  set $upstream_onboarding_api {{getenv "UPSTREAM_ONBOARDING_API"}};
  set $upstream_password_reset_service {{getenv "UPSTREAM_PASSWORD_RESET_SERVICE"}};
  set $upstream_promotions_service {{getenv "UPSTREAM_PROMOTIONS_SERVICE"}};
  set $upstream_science_data_service {{getenv "UPSTREAM_SCIENCE_DATA_SERVICE"}};
  set $upstream_search_service {{getenv "UPSTREAM_SEARCH_SERVICE"}};
  set $upstream_sessions_api {{getenv "UPSTREAM_SESSIONS_API"}};
  set $upstream_spots_api {{getenv "UPSTREAM_SPOTS_API"}};
  set $upstream_tide_service {{getenv "UPSTREAM_TIDE_SERVICE"}};
  set $upstream_subscription_service {{getenv "UPSTREAM_SUBSCRIPTION_SERVICE"}};
  set $upstream_user_service {{getenv "UPSTREAM_USER_SERVICE"}};
  set $upstream_web_backplane {{getenv "UPSTREAM_WEB_BACKPLANE"}};

  gzip on;
  gzip_types text/html application/json;
  gzip_vary on;

  # shortcut to /auth/token -- includes header/cookie/etc...
  location /login {
    if ($request_method = POST ) {
      content_by_lua_file 'authenticate.lua';
    }
  }

  location /logout {
    content_by_lua_file 'logout.lua';
  }

  # Auth can be exposed publicly
  location /auth {
    # non-gzip response so we can easily read it
    more_clear_input_headers Accept-Encoding;
    proxy_pass  http://$upstream_auth_service;

    proxy_set_header  Host             $upstream_auth_service;
    proxy_set_header  X-Real-IP        $remote_addr;
    proxy_set_header  X-Forwarded-For  $proxy_add_x_forwarded_for;

  }

  location /trusted/token {
      access_by_lua_file  'logger.lua';
      proxy_set_header Authorization $http_authorization;
      proxy_pass_header  Authorization;
      proxy_set_header  Host             $upstream_auth_service;
      proxy_set_header  X-Real-IP        $remote_addr;
      proxy_set_header  X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_pass http://$upstream_auth_service;
  }

  location /trusted {
    # non-gzip response so we can easily read it
    more_clear_input_headers Accept-Encoding;
    proxy_pass  http://$upstream_auth_service;

    proxy_set_header  Host             $upstream_auth_service;
    proxy_set_header  X-Real-IP        $remote_addr;
    proxy_set_header  X-Forwarded-For  $proxy_add_x_forwarded_for;
  }

  location /oauth {
    proxy_set_header Host $upstream_auth_service;
    proxy_pass http://$upstream_auth_service;
  }

  location /user {
    access_by_lua_file  'access.lua';
    proxy_set_header  Host             $upstream_user_service;
    proxy_pass http://$upstream_user_service;
  }

  location /password-reset {
    proxy_set_header  Host            $upstream_password_reset_service;
    proxy_pass http://$upstream_password_reset_service;
  }

  location ~ /cameras/ValidateAlias {
    proxy_set_header Host $upstream_cameras_service;
    proxy_pass http://$upstream_cameras_service;
  }

  location ~ /cameras/([a-z0-9]+)/log {
    proxy_set_header  Host            $upstream_cameras_service;
    proxy_pass http://$upstream_cameras_service;
  }

  # /cameras/authorization/camname
  location ~ /cameras/authorization/(.*)$ {
    access_by_lua_file  'access.lua';
    proxy_set_header  Host            $upstream_cameras_service;
    proxy_pass http://$upstream_cameras_service/cameras/authorization/$1$is_args$args;
  }

  location ~ /cameras/embeds {
    access_by_lua_file  'access.lua';
    proxy_set_header Host $upstream_cameras_service;
    proxy_pass http://$upstream_cameras_service;
  }

  location ~ /cameras/recording/redirect/(.*)$ {
    proxy_set_header  Host            $upstream_cameras_service;
    proxy_pass http://$upstream_cameras_service/cameras/recording/redirect/$1$is_args$args;
  }

  # /cameras/recording/camname
  location ~ /cameras/recording/(.*)$ {
    set $camname $1;
    if ($request_method ~ GET|OPTIONS ){
      access_by_lua_file  'access.lua';
      proxy_pass http://$upstream_cameras_service/cameras/recording/$camname$is_args$args;
      break;
    }

    return 404;
  }

  # /cameras/rewind/id/{camID}
  location ~ /cameras/rewind/(.*)$ {
    if ($request_method = GET ) {
      proxy_pass http://$upstream_cameras_service/cameras/rewind/$1$is_args$args;
      break;
    }

    return 404;
  }

  location ~ /geo-target/(?<path>.*) {
    more_clear_input_headers Accept-Encoding;
    proxy_set_header  Host  $upstream_geo_target_service;
    proxy_pass http://$upstream_geo_target_service/$path?$args;
  }

  # /entitlements?usID=12345&objectId=5a6b789c10d
  location /entitlements {
    access_by_lua_file 'access.lua';
    proxy_set_header  Host  $upstream_entitlement_service;
    proxy_pass http://$upstream_entitlement_service;
  }

  # /subscriptions
  location /subscriptions {
    access_by_lua_file  'access.lua';
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header  X-Forwarded-For  $proxy_add_x_forwarded_for;
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /credit-cards
  location /credit-cards {
    access_by_lua_file 'access.lua';
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /payment/alerts
  location /payment/alerts {
    access_by_lua_file 'access.lua';
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /pricing-plans
  location /pricing-plans {
    access_by_lua_file 'access.lua';
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /iap
  location /iap {
    access_by_lua_file  'access.lua';
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /integrations/v1/stripe
  # V1 Stripe webhooks are validated in-code with stripe key in body
  location /integrations/v1/stripe {
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /integrations/stripe
  # stripe integrations are validated in-code with stripe key in body
  location /integrations/stripe {
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /integrations/itunes
  # itunes server-to-server notifications are validated in-code with itunes key in body
  location /integrations/itunes {
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /integrations/google
  # google real-time developer notifications are validated in-code
  location /integrations/google {
    proxy_set_header Host $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /kbyg
  location ~ ^/kbyg/(.*)$ {
    access_by_lua_file  'access.lua';
    proxy_pass http://$upstream_kbyg_product_api/$1$is_args$args;
  }

  # /favorites
  location /favorites/ {
    access_by_lua_file 'access.lua';
    proxy_pass http://$upstream_favorites_service;
  }

  # /search
  location /search/ {
    access_by_lua_file 'access.lua';
    proxy_pass http://$upstream_search_service;
  }

  # /sessions
  location ~ /sessions/failed {
    deny all;
  }

  location ~ /sessions/users {
    deny all;
  }

  location /sessions {
    access_by_lua_file 'access.lua';
    client_max_body_size 6m;
    proxy_set_header Host $upstream_sessions_api;
    proxy_pass http://$upstream_sessions_api;
  }

  location /surfparks/sessions {
    access_by_lua_file 'access.lua';
    client_max_body_size 6m;
    proxy_set_header Host $upstream_sessions_api;
    proxy_pass http://$upstream_sessions_api;
  }

  # /location-view-service
  location /location/view {
    access_by_lua_file 'access.lua';
    proxy_pass http://$upstream_location_view_service;
  }

  # /onboarding-api
  location /onboarding {
    access_by_lua_file 'access.lua';
    proxy_pass http://$upstream_onboarding_api;
  }

  # /feed-api
  location /feed {
    access_by_lua_file 'access.lua';
    proxy_pass http://$upstream_feed_api;
  }

  # /spots-api
  location /taxonomy {
    access_by_lua_file 'access.lua';
    proxy_pass http://$upstream_spots_api;
  }

  location /subregions {
    proxy_set_header  Host             $upstream_spots_api;
    proxy_pass http://$upstream_spots_api;
  }

  location /admin/spots/ {
    # Only allow requests containing "cams={objectid}" in the query string
    # With no query string params, this is a very heavy request
    if ($query_string ~ "^((?!cams=[a-fA-F\d]{24}).)*$)" {
      return 405;
    }
    # Only allow timezone, name, and cams array to be returned
    if ($query_string ~ "^((?!select=timezone,name,cams).)*$)" {
      return 405;
    }
    # Only allow GET requests, we don't care about HEAD for this route
    if ($request_method != GET) {
      return 405;
    }
    proxy_set_header Host $upstream_spots_api;
    proxy_pass http://$upstream_spots_api;
  }

  # /charts-service
  location ~ ^/charts/(.*)$ {
    access_by_lua_file 'access.lua';
    proxy_set_header  Host  $upstream_charts_service;
    proxy_pass http://$upstream_charts_service/$1$is_args$args;
  }

  # /science-data-service
  location /forecasts {
    if ($query_string ~ ^((?!api_key=temporary_and_insecure_wavetrak_api_key).)*$) {
      return 401;
    }
    proxy_set_header Host $upstream_science_data_service;
    proxy_pass http://$upstream_science_data_service;
  }

  # graphql gateway
  location /graphql {
    # Return 401 if api_key isn't set in query string
    if ($query_string ~ ^((?!api_key=temporary_and_insecure_wavetrak_api_key).)*$) {
      return 401;
    }

    proxy_set_header Host $upstream_graphql_gateway;
    proxy_pass http://$upstream_graphql_gateway;
  }

  # /notifications
  location ~ /notifications/(.*)$ {
    access_by_lua_file 'access.lua';
    proxy_set_header Host $upstream_notifications_service;
    proxy_pass http://$upstream_notifications_service/$1$is_args$args;
  }

  # backplane
  location ~* ^/backplane/(.*)$ {
    access_by_lua_file 'backplane_blacklist.lua';
    proxy_set_header  Host  $upstream_web_backplane;
    proxy_pass http://$upstream_web_backplane/$1$is_args$args;
  }

  # /promotions-service
  location ~ ^/promotions/(.*)$ {
    access_by_lua_file 'access.lua';
    proxy_set_header  Host  $upstream_promotions_service;
    proxy_pass http://$upstream_promotions_service/$1$is_args$args;
  }

  # /gifts/products|purchase (PENDING DEPRECATION)
  location ~ ^/gifts/(products|purchase)$ {
    proxy_set_header  Host  $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /gifts/redeem (PENDING DEPRECATION)
  location ~ /gifts/redeem {
    access_by_lua_file 'access.lua';
    proxy_set_header  Host  $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /gift/payment
  location ~ ^/gift/payment$ {
    proxy_set_header  Host  $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # /gift/redeem
  location ~ /gift/redeem {
    access_by_lua_file 'access.lua';
    proxy_set_header  Host  $upstream_subscription_service;
    proxy_pass http://$upstream_subscription_service;
  }

  # Internal admin endpoints for the metering service.
  # These should not be publicly available on the services proxy
  location ~ /meter/admin {
    deny all;
  }

  # /meter
  location ~ ^\/meter\/?(.*)?$ {
    access_by_lua_file 'access.lua';
    proxy_set_header  Host  $upstream_metering_service;
    proxy_pass http://$upstream_metering_service/$1$is_args$args;
  }

  # Tide service
  location ~ ^\/tide/(.*)?$ {
    proxy_set_header  Host  $upstream_tide_service;
    proxy_pass http://$upstream_tide_service/$1$is_args$args;
  }

  # health endpoint
  location /health {
    add_header Content-Type application/json;
    return 200 '{ "status": 200, "message": "OK" }';
  }
}
