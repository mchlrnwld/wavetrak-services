# proxy-api

# Docker authentication and authorization images

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Goals](#goals)
- [Quick Start](#quick-start)
- [Details](#details)
  - [Nginx reverse proxy](#nginx-reverse-proxy)
      - [Running locally](#running-locally)
      - [Note: You must use a _.surfline.com or a _.buoyweather.com Host header to test the particular routes within the `nginx-conf/envs` .conf files in either curl or postman.](#note-you-must-use-a-_surflinecom-or-a-_buoyweathercom-host-header-to-test-the-particular-routes-within-the-nginx-confenvs-conf-files-in-either-curl-or-postman)
    - [Tests](#tests)
  - [Directory Structure](#directory-structure)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


* [Docker](https://www.docker.com/)
* [Boot2Docker](http://boot2docker.io/)
* [Docker-compose](http://docs.docker.com/compose/)
* [JSON parsing in Nginx](http://www.kyne.com.au/~mark/software/lua-cjson-manual.html)

## Goals

* Create public facing micro-service proxy that will authenticate requests
* Injects authenticated X-Auth-UserId into request header for service use
* Switch front-end services to use this public proxy with authentication for services

## Quick Start

```
$ cp .env.sample .env
$ docker build --build-arg APP_ENV=sandbox -t proxy-api .
$ docker run --rm -p 8080:8080 --env-file=.env proxy-api openresty
```

## Details

The solution is composed of microservices, using [nginx](http://nginx.org/) as a reverse proxy and
[Lua](http://wiki.nginx.org/HttpLuaModule) scripts to control authentication/sessions. Uses [Docker](https://www.docker.com/)
and [Docker Compose](https://docs.docker.com/compose/) to build container images which are deployed onto a Docker host
VM.

### Nginx reverse proxy

Nginx is used as the reverse proxy to access the Frontend microservice and it also wires together the authentication and
session management using Lua scripts. To provision the Nginx container I created a DockerFile which installs nginx with
[OpenResty](http://openresty.org/)

* [Dockerfile](https://github.com/stevenalexander/docker-authentication-authorisation/tree/master/docker/image-nginx-lua/Dockerfile) - defines the Nginx container image, with modules for Lua
  scripting
* [nginx.conf](https://github.com/stevenalexander/docker-authentication-authorisation/tree/master/docker/volume-nginx-conf.d/nginx.conf) - main config for Nginx, defines the endpoints
  available and calls the Lua scripts for access and authentication
* [access.lua](https://github.com/stevenalexander/docker-authentication-authorisation/tree/master/docker/volume-nginx-conf.d/access.lua) - run anytime a request is received, defines a list of
  endpoints which do not require authentication and for other endpoints it checks for accessToken cookie in the request
  header then validates it against the Session microservice
* [authenticate.lua](https://github.com/stevenalexander/docker-authentication-authorisation/tree/master/docker/volume-nginx-conf.d/authenticate.lua) - run when a user posts to /login, calls
  the Authentication microservice to check the credentials, then calls the Session microservice to create an accessToken
  for the new authenticated session and finally returns a 302 response with the accessToken in a cookie for future
  authenticated requests.
* [logout.lua](https://github.com/stevenalexander/docker-authentication-authorisation/tree/master/docker/volume-nginx-conf.d/logout.lua) - run when a user calls /logout, calls the Session
  microservice to delete the users accessToken

##### Running locally

Do a docker build and docker run from the main service folder

docker build

```
$ docker build --build-arg APP_ENV=sandbox -t proxy-api .
```

docker run

To run the container this way, you can simply do this

```
$ docker run --rm -p 8080:8080 --env-file=.env proxy-api openresty
```

This will run nginx as a foreground process (similar to npm start) and you will need to open up a separate terminal to access curl etc on the host to do any testing.

(You can run any of the commands individually in that file or whatever else you want to test the container)

_To login from a seperate terminal window_

```
docker ps | grep proxy-api
```

copy the container id (left side) and login to it

```
docker exec -it {container id from docker ps} /bin/bash
```

You will now be in a terminal session within the container, and can test curl commands directly against localhost:8080

##### Note: You must use a _.surfline.com or a _.buoyweather.com Host header to test the particular routes within the `nginx-conf/envs` .conf files in either curl or postman.

#### Tests

Tests are run using `inspec`, which is the test framework used by Test Kitchen.
There are two directories under `/test` (buoyweather and surfline) that contain nginx_test.rb files which curl the proxy with host headers to test responses at various upstream endpoints.

```
inspec exec test/**/*.rb -t docker://$CONTAINER
```

You can get the $CONTAINER value by running this

```
docker ps | grep proxy-api
```

You can also run these locally if you have inspec installed in the same way, just do a docker ps to get the running container. Make sure nginx is started in the docker instance first by doing the docker build and run from the above examples.

### Directory Structure

```
proxy-api
├── CODEOWNERS
├── Dockerfile
├── Dockerfile-Dev
├── Makefile
├── README.md
├── ci.json
├── confd
│   ├── conf.d
│   │   ├── buoyweather.toml
│   │   ├── nginx.toml
│   │   ├── platform.toml
│   │   └── surfline.toml
│   └── templates
│       ├── buoyweather.conf.tmpl
│       ├── nginx.conf.tmpl
│       ├── platform.conf.tmpl
│       └── surfline.conf.tmpl
├── entrypoint.sh
├── infra
│   ├── Makefile
│   ├── README.md
│   ├── modules
│   ├── prod
│   │   ├── main.tf
│   │   └── prod.tfvars
│   ├── sbox
│   │   ├── main.tf
│   │   └── sbox.tfvars
│   └── staging
│       ├── main.tf
│       └── staging.tfvars
├── inspec-tests.sh
├── nginx-conf
│   └── envs
│       ├── nginx-production.conf
│       ├── nginx-sandbox.conf
│       ├── nginx-staging.conf
│       └── nginx-tests.conf
├── package-lock.json
├── scripts
│   ├── access.lua
│   ├── authenticate.lua
│   ├── backplane_blacklist.lua
│   ├── geo.lua
│   ├── geo_spec.lua
│   ├── logout.lua
│   └── route-authentication.lua
└── test
    ├── buoyweather
    │   └── nginx_test.rb
    ├── platform
    │   └── nginx_test.rb
    └── surfline
        └── nginx_test.rb
```

## Service Validation

To validate that the service is functioning as expected, perform a `curl` request that routes through the [proxy](https://github.com/Surfline/wavetrak-services/blob/master/services/proxy-api/confd/templates/surfline.conf.tmpl):

**Ex:**

Test `GET` requests against the `/location` endpoint of the proxy:

```
curl \
    -X GET \
    -i \
    http://services.sandbox.surfline.com/location/view?type=mapView
```
**Expected response:** The response should be a successful html/json body with a 200 response code, similar to:
```
{
  "url": "/united-states/california/orange-county/newport-beach/5376890",
  "boundingBox": {
    "north": 33.630592124456506,
    "south": 33.55695564746362,
    "east": -117.81935248184205,
    "west": -117.9613911234855
  },
  "breadCrumbs": [
    {
      "name": "United States",
      "url": "/united-states/6252001",
      "geonameId": 6252001,
      "id": "58f7ed51dadb30820bb3879c"
    },
...
```
