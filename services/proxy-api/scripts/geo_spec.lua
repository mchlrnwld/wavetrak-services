-- https://olivinelabs.com/busted/
--
-- Install Busted and OpenResty
-- luarocks-5.1 install busted
-- brew install openresty
--
-- Add OpenResty's CLI (lua interpreter) to your path
-- export PATH=/usr/local/openresty/bin:$PATH
--
-- Run test
-- resty geo_spec.lua

require 'busted.runner'()
local geo = require("geo")

describe('GeoTarget features', function()
  geo.getRemoteAddress = function() return '8.8.8.8' end
  geo.setGeoCountryIsoHeader = function(isoCode) return isoCode end
  geo.setGeoLatLonHeader = function(coordinates) return coordinates end

  describe('should initialize openresty context', function()
    it("should run in ngx_lua context", function()
      assert.equal(0, ngx.OK)
      assert.equal(200, ngx.HTTP_OK)
    end)
  end)

  describe('should have a happy path', function()
    it('should get country from ip', function()
      geo.getGeoLocation = function(remoteAddr) return {
        status = ngx.HTTP_OK,
        body = '{ "country": {"iso_code": "US"}, "location": {"latitude": 1, "longitude": 2} }'
      }
      end
      local countryIsoCode = geo.fillGeo().countryIsoCode
      local coordinates = geo.fillGeo().coordinates
      assert.equal(countryIsoCode, 'US')
      assert.equal(coordinates.latitude, 1)
      assert.equal(coordinates.longitude, 2)
    end)
  end)

  describe('should fail gracefully on negative paths', function()
    it('should fail gracefully on internal server error', function()
      geo.getGeoLocation = function(remoteAddr) return { status = ngx.HTTP_INTERNAL_SERVER_ERROR } end
      local geo = geo.fillGeo()
      assert.equal(geo.countryIsoCode, '')
      assert.equal(geo.coordinates.longitude, nil)
      assert.equal(geo.coordinates.latitude, nil)
    end)

    it('should fail gracefully on bad JSON', function()
      geo.getGeoLocation = function(remoteAddr) return {
        status = ngx.HTTP_OK,
        body = '{ "country": {"iso_code": } }'
      }
      end
      local geo = geo.fillGeo()
      assert.equal(geo.countryIsoCode, '')
      assert.equal(geo.coordinates.longitude, nil)
      assert.equal(geo.coordinates.latitude, nil)
    end)
  end)
end)