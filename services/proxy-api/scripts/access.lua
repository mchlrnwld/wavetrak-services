-- 'route-authentication' manages optionally authenticated and unauthenticated routes
local RouteAuthentication = require "route-authentication"
local RouteAuthorization = require "authorize"
local Metering = require "metering_helper"

-- 'cjson' handles JSON parsing so we can read JSON responses
local cjson = require "cjson"
-- fillGeo adds 'X-Geo-Country-ISO' header for non-local IPs
require("geo").fillGeo()

ngx.log(ngx.NOTICE, 'BEGIN access.lua for ' .. ngx.var.uri)

isRouteUnauthenticated = RouteAuthentication.isRouteUnauthenticated(ngx.var.uri)
if isRouteUnauthenticated then
    -- auth is not required, short-circuit
    ngx.header["X-Auth-Required"] = "false"
    return
end

isRouteOptionallyAuthenticated = RouteAuthentication.isRouteOptionallyAuthenticated(ngx.var.uri)
if isRouteOptionallyAuthenticated then
    -- auth is optional, set header and allow request to pass through without access token
    ngx.header["X-Auth-Required"] = "false"
else
    -- auth is required
    ngx.header["X-Auth-Required"] = "true"
end

-- Check Authorization Header for requests from platform.surfline.com
local authorization = ngx.req.get_headers()['Authorization']

-- Check if the route is included in Optional authorization
local isRouteAuthorized = RouteAuthorization.isRouteAuthorized(ngx.var.uri)

-- Check X-Auth-AccessToken -> query param AccessToken -> cookie AccessToken
local accessToken = ngx.req.get_headers()["X-Auth-AccessToken"] or ngx.var.arg_AccessToken
if accessToken == nil then
  if ngx.var.cookie_AccessToken ~= nil then
    local jsonToken = cjson.decode(ngx.var.cookie_AccessToken);
    accessToken = jsonToken['access_token'];
  end
end

if not (accessToken or authorization) and not isRouteOptionallyAuthenticated then
    -- Reject request if route doesn't have an access token and has mandatory authentication
    ngx.log(ngx.WARN, 'No access token!')

    -- Set Cors headers to avoid failures on front end requests
    ngx.header["Access-Control-Allow-Origin"] = '*'
    ngx.header["X-Content-Type-Options"] = 'nosniff'
    ngx.header["Content-Type"] = 'application/json'

    ngx.status = 401;
    return ngx.say('{ "message": "Authentication Required"}');
elseif authorization ~=nil then
  ngx.req.clear_header('X-Auth-UserId')
  ngx.req.clear_header('X-Auth-Scopes')
  ngx.req.clear_header('-Auth-BearerToken')

  ngx.header['Access-Control-Allow-Origin'] = '*'
  ngx.header['X-Content-Type-Options'] = 'nosniff'
  ngx.header['Content-Type'] = 'application/json'
  local bearerToken = authorization:match('Bearer%s+(.*)')
  if not bearerToken then
      ngx.status = 401
      return ngx.say('{ "message": "Invalid credentials (malformed)"}')
  else
    ngx.log(ngx.INFO, 'bearerToken: ' .. bearerToken)

    local sessionsResult = ngx.location.capture('/trusted/authorise', { method = ngx.HTTP_GET })

    print(ngx.INFO, 'sessionsResult.status = ' .. sessionsResult.status)

    if sessionsResult.status ~= ngx.HTTP_OK then
      ngx.log(ngx.WARN, 'Nginx was unable to authorize '.. bearerToken ..' from Auth microservice')

      ngx.status = 401
      return ngx.say('{ "message": "Invalid Authentication"}')
    end

    -- Adding authenticated Mongo UserId to header so services can access it
    local sessionsResultJson = cjson.decode(sessionsResult.body)
    ngx.req.set_header('X-Auth-UserId', sessionsResultJson['id'])
    ngx.req.set_header('X-Auth-Scopes', table.concat(sessionsResultJson['scopes'], ','))
    ngx.req.set_header('X-Auth-BearerToken', bearerToken)

    -- If requests are authorized, append the client scopes to the request headers
    if isRouteAuthorized ~= nil then
      RouteAuthorization.setScopeHeaders()
    end
    Metering.isRouteAuthorizedForMeteredUser(sessionsResultJson['id'])
    ngx.log(ngx.INFO, 'X-Auth-UserId ' .. sessionsResultJson['id'])
    ngx.log(ngx.INFO, 'X-Auth-Scopes bearerToken ' .. table.concat(sessionsResultJson['scopes'], ','))
    ngx.log(ngx.INFO, 'Successfully verified session for request, bearerToken = ' .. bearerToken)
  end
elseif accessToken ~= nil then
  -- If we have an accessToken value, handle authentication
  ngx.log(ngx.INFO, 'accessToken: ' .. accessToken)

  local acceptEncodingHeaderTemp = ngx.req.get_headers()["Accept-Encoding"]
  ngx.req.clear_header("Accept-Encoding")
  ngx.req.clear_header("X-Auth-UserId")
  ngx.req.clear_header('X-Auth-Scopes')

  local sessionsResult = ngx.location.capture('/trusted/authorise?access_token=' .. accessToken, { method = ngx.HTTP_GET });
  ngx.req.set_header('Accept-Encoding', acceptEncodingHeaderTemp)

  print(ngx.INFO, 'sessionsResult.status = ' .. sessionsResult.status)

  if sessionsResult.status ~= ngx.HTTP_OK then
    ngx.log(ngx.WARN, 'Nginx was unable to authorize '.. accessToken ..' from Auth microservice')

    -- Set Cors headers to avoid failures on front end requests
    ngx.header["Access-Control-Allow-Origin"] = '*'
    ngx.header["X-Content-Type-Options"] = 'nosniff'
    ngx.header["Content-Type"] = 'application/json'

    ngx.status = 401;
    return ngx.say('{ "message": "Invalid Authentication" }');
  end

  -- Adding authenticated Mongo UserId to header so services can access it
  local sessionsResultJson = cjson.decode(sessionsResult.body);
  ngx.req.set_header('X-Auth-UserId', sessionsResultJson['id'])
  ngx.req.set_header('X-Auth-AccessToken', accessToken)
  ngx.req.set_header('X-Auth-Scopes', table.concat(sessionsResultJson['scopes'], ','))

  Metering.isRouteAuthorizedForMeteredUser(sessionsResultJson['id'])
  ngx.log(ngx.INFO, 'Successfully verified session for request, accessToken = ' .. accessToken)
  ngx.log(ngx.INFO, 'X-Auth-Scopes accessToken ' .. table.concat(sessionsResultJson['scopes'], ','))

  -- If requests are authorized, append the client scopes to the request headers
  if isRouteAuthorized ~= nil then
    RouteAuthorization.setScopeHeaders()
  end
else

  -- If requests are authorized, append the client scopes to the request headers
  if isRouteAuthorized ~= nil then
    RouteAuthorization.setScopeHeaders()
  end

  -- If accessToken or bearerToken is not present and we are optionally authenticated then pass through
  Metering.isRouteAuthorizedForAnonymous()
  return
end
