-- Handle pass-through for endpoints that do not require access token credentials
--
-- Note: ngx.re.find with the "i" option makes this case-insensitive
--       Nginx's location directive is still case-sensitive in nginx.conf

-- routes that do not require authentication at all
local unauthenticatedRoutes = {'^/login$','^/logout$','^/401$','^/404$', '^/taxonomy(/?).*$'}

-- routes where authentication is optional
local optionallyAuthenticatedGetRoutes = {
  '^/kbyg(/?).*$',
  '^/onboarding(/?).*$',
  '^/user/ValidateEmail$',
  '^/user/show-terms$',
  '^/user/Settings$',
  '^/user/identify$',
  '^/search(/?).*$',
  '^/feed(/?).*$',
  '^/location(/?).*$',
  '^/cameras/authorization/(.*)$',
  '^/forecasts(/?).*$',
  '^/favorites(/?).*$',
  '^/tides(/?).*$',
  '^/solunar(/?).*$',
  '^/charts(/?).*$',
  '^/promotions(/?).*$',
  '^/promotions(/?).*$',
  '^/sessions/clips/(.*)$',
  '^/sessions/(.*)/details.*$',
  '^/cameras/embeds$',
  '^/oauth1/connect$',
  '^/subscriptions/currency(/?).*$',
  '^/meter/anonymous(/?).*$',
  '^/cameras/recording(/?).*$',
  '^/pricing-plans(/?).*$',
}
local optionallyAuthenticatedPostRoutes = {'^/user$', '^/search/analytics$', '^/kbyg/spots/batch$', '^/user/verify-email$', '^/notifications/webhooks(/?).*$', '^/sessions/webhooks/(.*)$', '^/meter/anonymous(/?).*$',}

local optionallyAuthenticatedPatchRoutes = {'^/meter/anonymous(/?).*$',}

local optionallyAuthenticatedPutRoutes = {'^/meter/anonymous(/?).*$',}


local RouteAuthentication = {}

-- Allow OPTIONS & routes in `unauthenticatedRoutes` to pass through without authentication
function RouteAuthentication.isRouteUnauthenticated (currRoute)

  if (ngx.var.request_method == "OPTIONS") then
    return true
  else
    for idx, val in pairs(unauthenticatedRoutes) do
      if ngx.re.find(currRoute, unauthenticatedRoutes[idx], "i") then
        return true
      end
    end
   end

   return false
end

-- Allow routes in `optionallyAuthenticatedGetRoutes` and `optionallyAuthenticatedPostRoutes` to pass through if no accessToken
function RouteAuthentication.isRouteOptionallyAuthenticated (currRoute)

  if ngx.var.request_method == "GET" then
    for idx, val in pairs(optionallyAuthenticatedGetRoutes) do
      if ngx.re.find(currRoute, optionallyAuthenticatedGetRoutes[idx], "i") then
        return true
      end
    end
   elseif ngx.var.request_method == "POST" then
    for idx, val in pairs(optionallyAuthenticatedPostRoutes) do
      if ngx.re.find(currRoute, optionallyAuthenticatedPostRoutes[idx], "i") then
        return true
      end
    end
  elseif ngx.var.request_method == "PATCH" then
    for idx, val in pairs(optionallyAuthenticatedPatchRoutes) do
      if ngx.re.find(currRoute, optionallyAuthenticatedPatchRoutes[idx], "i") then
        return true
      end
    end
  elseif ngx.var.request_method == "PUT" then
    for idx, val in pairs(optionallyAuthenticatedPutRoutes) do
      if ngx.re.find(currRoute, optionallyAuthenticatedPutRoutes[idx], "i") then
        return true
      end
    end
  end
   return false
end

return RouteAuthentication
