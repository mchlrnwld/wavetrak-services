local cjson = require "cjson"

local apiResult = ngx.location.capture('/oauth/validate', { method = ngx.HTTP_GET })
if apiResult.status ~= ngx.HTTP_OK then
    ngx.status = 401
    ngx.header.content_type = "application/json"  
    return ngx.say(cjson.encode({ message = "Unauthorized" }))  
end
local apiResultJson = cjson.decode(apiResult.body)
local scopes = nil
-- Get the scopes array from the api response
for key, value in pairs(apiResultJson) do
    -- scopes in the response is a nested table
    if key == 'scopes' and type(value) == "table" then
        -- Concatenate the scopes to a string
        scopes = table.concat(value, ",")
    end
end 
if scopes ~= nil then
    ngx.req.set_header('X-Auth-Scopes', scopes)
end
