local cjson = require "cjson"

ngx.log(ngx.NOTICE, '(PLATFORM) BEGIN access.lua for ' .. ngx.var.uri)

ngx.req.clear_header('X-Auth-UserId')
ngx.req.clear_header('X-Auth-Scopes')
ngx.req.clear_header('-Auth-BearerToken')

ngx.header['Access-Control-Allow-Origin'] = '*'
ngx.header['X-Content-Type-Options'] = 'nosniff'
ngx.header['Content-Type'] = 'application/json'

local authorization = ngx.req.get_headers()['Authorization']

if not authorization then
    ngx.status = 401
    return ngx.say('{ "message": "Authentication Required"}')
end

local bearerToken = authorization:match('Bearer%s+(.*)')

if not bearerToken then
    ngx.status = 401
    return ngx.say('{ "message": "Invalid credentials (malformed)"}')
else
  ngx.log(ngx.INFO, 'bearerToken: ' .. bearerToken)

  local sessionsResult = ngx.location.capture('/oauth/authorize', { method = ngx.HTTP_GET })

  print(ngx.INFO, 'sessionsResult.status = ' .. sessionsResult.status)

  if sessionsResult.status ~= ngx.HTTP_OK then
    ngx.log(ngx.WARN, 'Nginx was unable to authorize '.. bearerToken ..' from Auth microservice')

    ngx.status = 401
    return ngx.say('{ "message": "Invalid Authentication"}')
  end

  -- Adding authenticated Mongo UserId to header so services can access it
  local sessionsResultJson = cjson.decode(sessionsResult.body)
  ngx.req.set_header('X-Auth-UserId', sessionsResultJson['userId'])
  ngx.req.set_header('X-Auth-Scopes', sessionsResultJson['scopes'])
  ngx.req.set_header('X-Auth-BearerToken', bearerToken)

  ngx.log(ngx.INFO, 'X-Auth-UserId ' .. sessionsResultJson['userId'])
  ngx.log(ngx.INFO, 'X-Auth-Scopes ' .. sessionsResultJson['scopes'])
  ngx.log(ngx.INFO, 'Successfully verified session for request, bearerToken = ' .. bearerToken)
end

ngx.log(ngx.NOTICE, '(PLATFORM) END access.lua for ' .. ngx.var.uri)
