
ngx.log(ngx.NOTICE, 'BEGIN authenticate.lua')

-- Call Authentication microservice to check users credentials
ngx.req.read_body()
local authenticationResult = ngx.location.capture('/trusted/token', { method = ngx.HTTP_POST, always_forward_body = true  })

if authenticationResult.status ~= ngx.HTTP_OK then
  ngx.log(ngx.WARN, 'Rejected authentication attempt. Status :' .. authenticationResult.status)

  ngx.header["X-Frame-Options"] = 'sameorigin'
  ngx.header["X-XSS-Protection"] = '1'
  ngx.header["X-Content-Type-Options"] = 'nosniff'
  ngx.header["Content-Type"] = 'application/json'

  ngx.status = 400;
  ngx.say('{ "message": "Invalid Authentication"}');
  return ngx.exit(ngx.HTTP_BAD_REQUEST);
end

local sessionToken = authenticationResult.body;
ngx.log(ngx.NOTICE, 'Got session token from Session microservice: ' .. sessionToken)

-- Create a cookie for the session so future requests from this users session can be verified
ngx.header["Set-Cookie"] = {"AccessToken=" .. sessionToken .. "; path=/;"}

ngx.status = 200;
ngx.header["Content-Type"] = 'application/json'
ngx.say(sessionToken);
return ngx.exit(ngx.HTTP_OK);
-- Or we could redirect to redirectUrl
-- return ngx.redirect(ngx.var.arg_RedirectUrl or '/')
