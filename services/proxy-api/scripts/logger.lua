local cjson = require "cjson"

ngx.log(ngx.NOTICE, 'BEGIN logger.lua')
local loggerEnabled = ngx.var.arg_logger
if loggerEnabled ~= nil and ngx.var.request_method == "POST" then
  ngx.req.read_body()
  local bodyData = ngx.req.get_body_data()
  local resultJson = cjson.decode(bodyData)
  local username = tostring(resultJson['username'])
  ngx.log(ngx.NOTICE, 'email ' .. username)
  local allHeaders = ngx.req.get_headers()
  for k, v in pairs(allHeaders) do
    ngx.log(ngx.NOTICE, "Got header "..k..": "..v..";")
  end
end
ngx.log(ngx.NOTICE, 'END logger.lua')
