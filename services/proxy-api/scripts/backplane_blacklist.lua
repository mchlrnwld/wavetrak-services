local args = ngx.req.get_uri_args()
if args['userId'] ~= nil then
    ngx.say('not authorized')
    ngx.exit(401)
end
