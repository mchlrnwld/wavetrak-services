local cjson = require "cjson"

function firstMatch(s, delimiter)
    if s ~= nil then
      for match in (s..delimiter):gmatch("(.-)"..delimiter) do
          return (string.gsub(match, "^%s*(.-)%s*$", "%1"))
      end
    end
    return nil
end

local GeoModule = {}

function GeoModule.getRemoteAddress()
  local ipAddr = ngx.req.get_headers()['x-client-ip'] or ngx.var.http_x_forwarded_for or ngx.var.remote_addr
  return firstMatch(ipAddr,",")
end

function GeoModule.getGeoLocation(remoteAddr)
  local geoLocationResponse = ''
  local status, err = pcall(function()
    geoLocationResponse = ngx.location.capture('/geo-target/Region?ipAddress=' .. remoteAddr, { method = ngx.HTTP_GET })
  end)
  if err ~= nil then
    ngx.log(ngx.ERR, err)
  end
  return geoLocationResponse
end

function GeoModule.setGeoCountryIsoHeader(isoCode)
  ngx.req.set_header('X-Geo-Country-ISO', isoCode)
  return isoCode
end

function GeoModule.setGeoLatLonHeader(coordinates)
  ngx.req.set_header('X-Geo-Latitude', coordinates.latitude)
  ngx.req.set_header('X-Geo-Longitude', coordinates.longitude)
  return coordinates
end

function GeoModule.extractCountryIsoCode(geoLocationResponse)
  local countryIsoCode = ''
  local status, err = pcall(function()
    local geoLocationJson = cjson.decode(geoLocationResponse.body)
    countryIsoCode = geoLocationJson['country']['iso_code']
  end)
  if err ~= nil then
    ngx.log(ngx.ERR, err)
  end
  return countryIsoCode
end

function GeoModule.extractLatLon(geoLocationResponse)
  local coordinates = {}
  local status, err = pcall(function()
    local geoLocationJson = cjson.decode(geoLocationResponse.body)
    if geoLocationJson['location'] ~= nil then
      coordinates.latitude = geoLocationJson['location']['latitude']
      coordinates.longitude = geoLocationJson['location']['longitude']
    end
  end)
  if err ~= nil then
    ngx.log(ngx.ERR, err)
  end
  return coordinates
end

function GeoModule.fillGeo()
  local remoteAddr = GeoModule.getRemoteAddress()
  ngx.log(ngx.INFO, 'remoteAddr is: ' .. remoteAddr)

  local geoLocationResponse = GeoModule.getGeoLocation(remoteAddr)
  local countryIsoCode = ''
  local coordinates = {}
  if geoLocationResponse.status ~= ngx.HTTP_OK then
    ngx.log(ngx.WARN, 'Geo location not found for ip: ' .. remoteAddr)
  else
    countryIsoCode = GeoModule.extractCountryIsoCode(geoLocationResponse)
    if countryIsoCode ~= '' then
      GeoModule.setGeoCountryIsoHeader(countryIsoCode)
    end
    coordinates = GeoModule.extractLatLon(geoLocationResponse)
    if coordinates ~= nil and coordinates['latitude'] ~= nil and coordinates['longitude'] ~= nil then
      GeoModule.setGeoLatLonHeader(coordinates)
    end
  end
  local result = {
    coordinates = coordinates,
    countryIsoCode = countryIsoCode,
  }
  return result
end

return GeoModule