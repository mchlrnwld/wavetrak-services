local cjson = require "cjson"

-- routes where authorization is semi-optional
local authorizedGetRoutes = {'^/kbyg(/).*$'}

local RouteAuthorization = {}

function RouteAuthorization.isRouteAuthorized (currRoute)
  if ngx.var.request_method == "GET" then
    for idx, val in pairs(authorizedGetRoutes) do
      if ngx.re.find(currRoute, authorizedGetRoutes[idx], "i") then
        return true
      end
    end
  end
  return false
end


function RouteAuthorization.setScopeHeaders ()
  local apiKey = ngx.req.get_headers()["X-Api-Key"]
  local scopes = ngx.req.get_headers()["X-Auth-Scopes"]

  if apiKey ~= nil then
    local apiKeyResult = ngx.location.capture('/trusted/validate?apiKey=' .. apiKey, { method = ngx.HTTP_GET })
    if apiKeyResult.status ~= ngx.HTTP_OK then
      ngx.status = 401
      return ngx.say('{ "message": "Invalid Authorization" }')
    end

    local apiKeyResultJson = cjson.decode(apiKeyResult.body)
    local authorizationScopes = apiKeyResultJson['scopes']
    ngx.log(ngx.INFO, 'X-Auth-Scopes authorization ' .. authorizationScopes)
    if scopes ~= nil then
      ngx.req.set_header('X-Auth-Scopes', scopes .. ',' .. authorizationScopes)
    else
      ngx.req.set_header('X-Auth-Scopes', authorizationScopes)
    end
  end
end
return RouteAuthorization
