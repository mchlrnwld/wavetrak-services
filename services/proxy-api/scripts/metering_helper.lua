local cjson = require "cjson"
local decodeBase64 = require "decode"
-- Routes that are whitelisted for premium access for Anonymous Sessions
local authorizedMeteredGetRoutes = {'^/kbyg(/).*$', '^/charts(/?).*$','^/cameras/recording(/?).*$',}
-- Routes that require client authentication via encoded client id
local clientAuthenticatedRoutes = {'^/cameras/recording(/?).*$',}
local Metering = {}

local function isRouteMetered (currRoute)
  -- Allow OPTIONS  to pass through
  if (ngx.var.request_method == "OPTIONS") then
    return false
  else
    for idx, val in pairs(authorizedMeteredGetRoutes) do
      if ngx.re.find(currRoute, authorizedMeteredGetRoutes[idx], "i") and ngx.var.request_method == "GET" then
        return true
      end
    end
   end
   return false
end

local function isClientAuthRequired (currRoute)
  for idx, val in pairs(clientAuthenticatedRoutes) do
    if ngx.re.find(currRoute, clientAuthenticatedRoutes[idx], "i") then
      return true
    end
  end
  return false
end

function Metering.isRouteAuthorizedForAnonymous ()
  local premiumScope = "premium:read:anonymous"
  local anonymousId = ngx.req.get_headers()["X-Auth-AnonymousId"]
  local scopes = ngx.req.get_headers()["X-Auth-Scopes"]
  local rlOverride = ngx.req.get_headers()["X-RateLimit-Override"]
  local rlOverrideSecret = "r6iuG0foe3"
  local meterRemaining = ngx.var.arg_meterRemaining
  if scopes ~=nil then
    -- Check to make sure the premiumScope is not forcefully sent in the header. If so remove it.
    scopes = string.gsub(scopes, premiumScope, "")
  end
  local isRouteMetered = isRouteMetered(ngx.var.uri)
  -- Override Metering logic and grant premium access if the request have X-RateLimit-Override header.
  -- Temporarily added for supporting anonymous user requests from the App
  if rlOverride ~= nil and isRouteMetered and tostring(rlOverride) == rlOverrideSecret then
    if scopes ~= nil then
      scopes = scopes .. ',' .. premiumScope
    else
      scopes = premiumScope
    end
    ngx.req.set_header('X-Auth-Scopes', scopes)
    return
  end

  -- For routes that require client authentication, reject if invalid auth header
  local clientAuthRequired = isClientAuthRequired(ngx.var.uri)
  if clientAuthRequired then
    local authHeader = ngx.req.get_headers()['X-Auth-ClientId']
    if authHeader == nil then
      ngx.status = 401
      return ngx.say('{ "message": "Invalid Authentication" }') 
    else
      local encodedClientId = authHeader:match('Basic%s+(.*)')
      local clientId = decodeBase64(encodedClientId)
      local apiKeyResult = ngx.location.capture('/trusted/validate?apiKey=' .. clientId, { method = ngx.HTTP_GET })
      
      if apiKeyResult.status ~= ngx.HTTP_OK then
        ngx.status = 401
        return ngx.say('{ "message": "Invalid Authentication" }')
      end
    end
  end

  if anonymousId ~= nil and isRouteMetered and meterRemaining ~=nil and tonumber(meterRemaining) >= 0 then
    -- Verify if the anonymous session has surf checks left and is eligible for Premium data
    local result = ngx.location.capture('/meter/anonymous/verify/'..anonymousId..'/', {  method = ngx.HTTP_GET  } )
    if result.status ~= ngx.HTTP_OK then
      --Do nothing, Pass through
    else
      local resultJson = cjson.decode(result.body)
      local premiumEligible = tostring(resultJson['premiumEligible'])
      if premiumEligible == "true" then
        if scopes ~= nil then
          scopes = scopes .. ',' .. premiumScope
        else
          scopes = premiumScope
        end
      end
    end
  else
    --Do nothing, Pass through
  end
  -- Finally set the updated scopes in to the Scopes header
  if scopes ~=nil then
    ngx.req.set_header('X-Auth-Scopes', scopes)
  end
end

function Metering.isRouteAuthorizedForMeteredUser (userId)
  local premiumScope = "premium:read:registered"
  local anonymousId = ngx.req.get_headers()["X-Auth-AnonymousId"]
  local scopes = ngx.req.get_headers()["X-Auth-Scopes"]
  local meterRemaining = ngx.var.arg_meterRemaining
  if scopes ~=nil then
    -- Check to make sure the premiumScope is not forcefully sent in the header. If so remove it.
    scopes = string.gsub(scopes, premiumScope, "")
  end
  local isRouteMetered = isRouteMetered(ngx.var.uri)

  if userId ~=nil and anonymousId ~= nil and isRouteMetered and meterRemaining ~=nil and tostring(meterRemaining) >= "0" then
    -- Is a valid metered user. Call the meter/active-surfcheck GET endpoint
    local result = ngx.location.capture('/meter/surf-check/', {  method = ngx.HTTP_GET  } )
    if result.status ~= ngx.HTTP_OK then
      --Do nothing, Pass through
    else
      local resultJson = cjson.decode(result.body)
      local hasActiveSurfCheck = tostring(resultJson['hasActiveSurfCheck'])
      if hasActiveSurfCheck == "true" then
        if scopes ~= nil then
          scopes = scopes .. ',' .. premiumScope
        else
          scopes = premiumScope
        end
      end
    end
  else
    --Do nothing, Pass through
  end
  -- Finally set the updated scopes in to the Scopes header
  if scopes ~=nil then
    ngx.req.set_header('X-Auth-Scopes', scopes)
  end
end

return Metering
