local cjson = require "cjson"

ngx.log(ngx.NOTICE, 'BEGIN logout.lua')

local accessToken;
if ngx.var.cookie_AccessToken ~= nil then
    local jsonToken = cjson.decode(ngx.var.cookie_AccessToken);
    accessToken = jsonToken['access_token'];
else
    accessToken = ngx.var.arg_AccessToken;
end

if not accessToken then
	return ngx.redirect('/login')
end

ngx.log(ngx.INFO, 'accessToken: ' .. accessToken)

ngx.req.clear_header("Accept-Encoding")

local sessionsDeleteResult = ngx.location.capture('/auth/invalidate-token' .. accessToken, { method = ngx.HTTP_POST });

if sessionsDeleteResult.status ~= ngx.HTTP_OK then
  ngx.log(ngx.WARN, 'Nginx was unable to delete '.. accessToken ..' from auth microservice')

  return ngx.exit(500)
end

return ngx.redirect('/login')
