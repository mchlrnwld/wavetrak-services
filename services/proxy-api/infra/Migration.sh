#!/bin/bash

# This script should take care of the custom renaming, or even tweaking, on any
# of the resources that should be named differently after the module migration.
# You should safely remove this file if all migrations on all environments were
# already applied.

# Add module name to resources without/outside a module.
module="module.services-proxy"

# This script will output the content of the .tfstate file.
if [ "$tfstate" != "" ]; then
  cat $tfstate | \
  jq \
    --arg name $module \
    '.resources=[.resources[] | if (.module == null) then (.module = $name) else (.) end]' \
  > $tfstate.tmp
  mv $tfstate.tmp $tfstate
fi

# This script will update the state file renaming the necessary resources.
if [ "$tfplan" != "" ]; then
  cat $tfplan | \
  jq -r \
    '.resources[] | select(.module == null) | select(.mode != "data") | (.type + "." + .name)' | \
  awk \
    -v module=$module \
    '{ print $1,module"."$1 }' | \
  xargs -r -n 2 \
    terraform state mv
fi
