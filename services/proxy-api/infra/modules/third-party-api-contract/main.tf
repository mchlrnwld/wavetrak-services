resource "newrelic_synthetics_monitor" "third_party_api_contract" {
  name      = "Third Party API Contract"
  type      = "SCRIPT_API"
  frequency = 15
  status    = "ENABLED"
  locations = ["AWS_US_WEST_1"]
}

resource "newrelic_synthetics_monitor_script" "third_party_api_contract" {
  monitor_id = newrelic_synthetics_monitor.third_party_api_contract.id
  text       = file("${path.module}/resources/script.js")
}
