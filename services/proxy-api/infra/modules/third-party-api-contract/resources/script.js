const assert = require('assert');

// $secure vars are set in New Relic Synthetics Secure Credentials
const authUsername = $secure.THIRD_PARTY_API_BASIC_AUTH_USERNAME;
const authPassword = $secure.THIRD_PARTY_API_BASIC_AUTH_PASSWORD;
const encodedAuthString = Buffer.from(`${authUsername}:${authPassword}`).toString('base64');
const options = {
  headers: {
    Authorization: `Basic: ${encodedAuthString}`,
  },
};

const spotId = '5842041f4e65fad6a770888a';
const lat = 33.3814;
const lon = -117;

/* Wave Forecasts */
$http.get(
  `https://platform.surfline.com/spots/forecasts/wave?spotId=${spotId}`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  'http://platform.surfline.com/spots/forecasts/wave?spotId=invalidId',
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 400, 'Expected 400 Valid SpotId Required response');
  }
);

$http.get(
  `https://platform.surfline.com/spots/forecasts/wave?spotId=${spotId}`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');
    
    const {
      associated: {
        units,
        utcOffset,
        location,
        forecastLocation,
        offshoreLocation,
      },
      data: {
        wave,
      },
    } = JSON.parse(body);
    assert.ok(typeof units === 'object', 'Expected associated.units to be type object');
    assert.strictEqual(units.waveHeight, 'M', 'Expected default units.waveHeight to equal "M"');
    assert.strictEqual(units.swellHeight, 'M', 'Expected default units.swellHeight to equal "M"');
    assert.ok(typeof utcOffset === 'number', 'Expected associated.utcOffset to be type number');
    assert.ok(typeof location.lat === 'number', 'Expected associated.location.lat to be type number');
    assert.ok(typeof location.lon === 'number', 'Expected associated.location.lon to be type number');
    assert.ok(typeof location.lat === 'number', 'Expected associated.location.lat to be type number');
    assert.ok(typeof forecastLocation.lon === 'number', 'Expected associated.forecastLocation.lon to be type number');
    assert.ok(typeof forecastLocation.lat === 'number', 'Expected associated.forecastLocation.lat to be type number');
    assert.ok(typeof offshoreLocation.lon === 'number', 'Expected associated.offshoreLocation.lon to be type number');
    assert.ok(typeof offshoreLocation.lon === 'number', 'Expected associated.offshoreLocation.lon to be type number');

    assert.strictEqual(wave.length, 72, 'Expected data.wave to have length 72');
    assert.ok(typeof wave[0].timestamp === 'number', 'Expected data.wave[0].timestamp to be type number');
    assert.ok(typeof wave[0].utcOffset === 'number', 'Expected data.wave[0].utcOffset to be type number');
    assert.ok(typeof wave[0].surf.min === 'number', 'Expected data.wave[0].surf.min to be type number');
    assert.ok(typeof wave[0].surf.max === 'number', 'Expected data.wave[0].surf.max to be type number');
    assert.strictEqual(wave[0].swells.length, 6, 'Expected default data.wave[0].swells to have length 6');
    assert.ok(typeof wave[0].swells[0].height === 'number', 'Expected data.wave[0].swells.height to be type number');
    assert.ok(typeof wave[0].swells[0].period === 'number', 'Expected data.wave[0].swells.height to be type number');
    assert.ok(typeof wave[0].swells[0].direction === 'number', 'Expected data.wave[0].swells.height to be type number');
    assert.ok(typeof wave[0].swells[0].spread === 'number', 'Expected data.wave[0].swells.spread to be type number');
  }
);

/* Weather Forecasts */
$http.get(
  `https://platform.surfline.com/spots/forecasts/weather?spotId=${spotId}`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  'https://platform.surfline.com/spots/forecasts/weather?spotId=invalidId',
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 400, 'Expected 400 Valid SpotId Required response');
  }
);

$http.get(
  `https://platform.surfline.com/spots/forecasts/weather?spotId=${spotId}`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');
    
    const { associated: { units, utcOffset }, data: { sunlightTimes, weather } } = JSON.parse(body);
    assert.ok(typeof units === 'object', 'Expected associated.units to be type object');
    assert.strictEqual(units.temperature, 'C', 'Expected default units.temperature to equal "C"');
    assert.ok(typeof utcOffset === 'number', 'Expected associated.utcOffset to be type number');

    assert.strictEqual(sunlightTimes.length, 3, 'Expected data.sunlightTimes to have length 3');
    assert.ok(typeof sunlightTimes[0].midnight === 'number', 'Expected data.sunlightTimes[0].midnight to be type number');
    assert.ok(typeof sunlightTimes[0].midnightUTCOffset === 'number', 'Expected data.sunlightTimes[0].midnightUTCOffset to be type number');
    assert.ok(typeof sunlightTimes[0].dawn === 'number', 'Expected data.sunlightTimes[0].dawn to be type number');
    assert.ok(typeof sunlightTimes[0].dawnUTCOffset === 'number', 'Expected data.sunlightTimes[0].dawnUTCOffset to be type number');
    assert.ok(typeof sunlightTimes[0].sunrise === 'number', 'Expected data.sunlightTimes[0].sunrise to be type number');
    assert.ok(typeof sunlightTimes[0].sunriseUTCOffset === 'number', 'Expected data.sunlightTimes[0].sunriseUTCOffset to be type number');
    assert.ok(typeof sunlightTimes[0].sunset === 'number', 'Expected data.sunlightTimes[0].sunset to be type number');
    assert.ok(typeof sunlightTimes[0].sunsetUTCOffset === 'number', 'Expected data.sunlightTimes[0].sunsetUTCOffset to be type number');
    assert.ok(typeof sunlightTimes[0].dusk === 'number', 'Expected data.sunlightTimes[0].dusk to be type number');
    assert.ok(typeof sunlightTimes[0].duskUTCOffset === 'number', 'Expected data.sunlightTimes[0].duskUTCOffset to be type number');
    
    assert.strictEqual(weather.length, 72, 'Expected data.weather to have length 72');
    assert.ok(typeof weather[0].temperature === 'number', 'Expected data.weather[0].temperature to be type number');
    assert.ok(typeof weather[0].utcOffset === 'number', 'Expected data.weather[0].utcOffset to be type number');
    assert.ok(typeof weather[0].timestamp === 'number', 'Expected data.weather[0].timestamp to be type number');
    assert.ok(typeof weather[0].condition === 'string', 'Expected data.weather[0].condition to be type string');
  }
);

/* Wind Forecasts */
$http.get(
  `https://platform.surfline.com/spots/forecasts/wind?spotId=${spotId}`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  'https://platform.surfline.com/spots/forecasts/wind?spotId=invalidId',
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 400, 'Expected 400 Valid SpotId Required response');
  }
);

$http.get(
  `https://platform.surfline.com/spots/forecasts/wind?spotId=${spotId}`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');
    
    const { associated: { units, utcOffset, location }, data: { wind } } = JSON.parse(body);
    assert.ok(typeof units === 'object', 'Expected associated.units to be type object');
    assert.strictEqual(units.windSpeed, 'KPH', 'Expected default units.windSpeed to equal "KPH"');
    assert.ok(typeof utcOffset === 'number', 'Expected associated.utcOffset to be type number');
    assert.ok(typeof location.lat === 'number', 'Expected associated.location.lat to be type number');
    assert.ok(typeof location.lon === 'number', 'Expected associated.location.lon to be type number');

    assert.strictEqual(wind.length, 72, 'Expected data.wind to have length 72');
    assert.ok(typeof wind[0].speed === 'number', 'Expected data.wind[0].speed to be type number');
    assert.ok(typeof wind[0].direction === 'number', 'Expected data.wind[0].direction to be type number');
    assert.ok(typeof wind[0].gust === 'number', 'Expected data.wind[0].gust to be type number');
    assert.ok(typeof wind[0].utcOffset === 'number', 'Expected data.wind[0].utcOffset to be type number');
    assert.ok(typeof wind[0].timestamp === 'number', 'Expected data.wind[0].timestamp to be type number');
  }
);

/* Tide Forecasts */
$http.get(
  `https://platform.surfline.com/spots/forecasts/tides?spotId=${spotId}`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  'https://platform.surfline.com/spots/forecasts/tides?spotId=invalidId',
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 400, 'Expected 400 Valid SpotId Required response');
  }
);

$http.get(
  `https://platform.surfline.com/spots/forecasts/tides?spotId=${spotId}`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');
    
    const { associated: { units, utcOffset, tideLocation }, data: { tides } } = JSON.parse(body);
    assert.ok(typeof units === 'object', 'Expected associated.units to be type object');
    assert.strictEqual(units.tideHeight, 'M', 'Expected default units.tideHeight to equal "M"');
    assert.ok(typeof utcOffset === 'number', 'Expected associated.utcOffset to be type number');
    assert.ok(typeof tideLocation.name === 'string', 'Expected associated.tideLocation.name to be type string');
    assert.ok(typeof tideLocation.min === 'number', 'Expected associated.tideLocation.min to be type number');
    assert.ok(typeof tideLocation.max === 'number', 'Expected associated.tideLocation.max to be type number');
    assert.ok(typeof tideLocation.lat === 'number', 'Expected associated.tideLocation.lat to be type number');
    assert.ok(typeof tideLocation.lon === 'number', 'Expected associated.tideLocation.lon to be type number');
    assert.ok(typeof tideLocation.mean === 'number', 'Expected associated.tideLocation.mean to be type number');

    assert.ok(typeof tides[0].timestamp === 'number', 'Expected data.tides[0].timestamp to be type number');
    assert.ok(typeof tides[0].utcOffset === 'number', 'Expected data.tides[0].utcOffset to be type number');
    assert.ok(typeof tides[0].type === 'string', 'Expected data.tides[0].type to be type string');
    assert.ok(typeof tides[0].height === 'number', 'Expected data.tides[0].height to be type number');
  }
);

/* Spot Reports */
$http.get(
  `https://platform.surfline.com/spots/reports?spotId=${spotId}`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  'http://platform.surfline.com/spots/reports?spotId=invalidId',
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 400, 'Expected 400 Valid SpotId Required response');
  }
);

$http.get(
  `https://platform.surfline.com/spots/reports?spotId=${spotId}`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const {
      associated: {
        units,
        utcOffset,
      },
      forecast: {
        conditions,
        swells,
        tide,
        waterTemp,
        waveHeight,
        weather,
        wind,
      },
      spot,
    } = JSON.parse(body);

    assert.ok(typeof units === 'object', 'Expected associated.units to be type object');
    assert.strictEqual(units.temperature, 'C', 'Expected default units.temperature to equal "C"');
    assert.strictEqual(units.tideHeight, 'M', 'Expected default units.tideHeight to equal "M"');
    assert.strictEqual(units.swellHeight, 'M', 'Expected default units.swellHeight to equal "M"');
    assert.strictEqual(units.waveHeight, 'M', 'Expected default units.waveHeight to equal "M"');
    assert.strictEqual(units.windSpeed, 'KPH', 'Expected default units.windSpeed to equal "KPH"');
    assert.ok(typeof utcOffset === 'number', 'Expected associated.utcOffset to be type number');

    assert.ok(typeof conditions.human === 'boolean', 'Expected forecast.conditions.human to be type boolean');
    assert.ok(typeof conditions.value === 'string', 'Expected forecast.conditions.value to be type string');
    assert.ok(typeof conditions.expired === 'boolean', 'Expected forecast.conditions.expired to be type boolean');
    assert.ok(typeof swells[0].height === 'number', 'Expected forecast.swells[0].height to be type number');
    assert.ok(typeof swells[0].period === 'number', 'Expected forecast.swells[0].period to be type number');
    assert.ok(typeof swells[0].direction === 'number', 'Expected forecast.swells[0].direction to be type number');
    assert.ok(typeof tide.previous.type === 'string', 'Expected forecast.tide.previous.type to be type string');
    assert.ok(typeof tide.previous.height === 'number', 'Expected forecast.tide.previous.height to be type number');
    assert.ok(typeof tide.previous.timestamp === 'number', 'Expected forecast.tide.previous.timestamp to be type number');
    assert.ok(typeof tide.previous.utcOffset === 'number', 'Expected forecast.tide.previous.utcOffset to be type number');
    assert.ok(typeof tide.current.type === 'string', 'Expected forecast.tide.current.type to be type string');
    assert.ok(typeof tide.current.height === 'number', 'Expected forecast.tide.current.height to be type number');
    assert.ok(typeof tide.current.timestamp === 'number', 'Expected forecast.tide.current.timestamp to be type number');
    assert.ok(typeof tide.current.utcOffset === 'number', 'Expected forecast.tide.current.utcOffset to be type number');
    assert.ok(typeof tide.next.type === 'string', 'Expected forecast.tide.next.type to be type string');
    assert.ok(typeof tide.next.height === 'number', 'Expected forecast.tide.next.height to be type number');
    assert.ok(typeof tide.next.timestamp === 'number', 'Expected forecast.tide.next.timestamp to be type number');
    assert.ok(typeof tide.next.utcOffset === 'number', 'Expected forecast.tide.next.utcOffset to be type number');
    assert.ok(typeof waterTemp.min === 'number', 'Expected forecast.waterTemp.min to be type number');
    assert.ok(typeof waterTemp.max === 'number', 'Expected forecast.waterTemp.max to be type number');
    assert.ok(typeof waveHeight.min === 'number', 'Expected forecast.waveHeight.min to be type number');
    assert.ok(typeof waveHeight.max === 'number', 'Expected forecast.waveHeight.max to be type number');
    assert.ok(typeof waveHeight.human === 'boolean', 'Expected forecast.waveHeight.human to be type boolean');
    assert.ok(typeof waveHeight.humanRelation === 'string', 'Expected forecast.waveHeight.humanRelation to be type string');
    assert.ok(typeof waveHeight.plus === 'boolean', 'Expected forecast.waveHeight.plus to be type boolean');
    assert.ok(typeof weather.temperature === 'number', 'Expected forecast.weather.temperature to be type number');
    assert.ok(typeof weather.condition === 'string', 'Expected forecast.weather.condition to be type string');
    assert.ok(typeof wind.speed === 'number', 'Expected forecast.wind.speed to be type number');
    assert.ok(typeof wind.direction === 'number', 'Expected forecast.wind.direction to be type number');
    assert.ok(typeof spot.name === 'string', 'Expected spot.name to be type string');
    assert.ok(typeof spot.lat === 'number', 'Expected spot.lat to be type number');
    assert.ok(typeof spot.lon === 'number', 'Expected spot.lon to be type number');
    assert.ok(typeof spot.subregionId === 'string', 'Expected spot.subregionId to be type string');
    assert.ok(typeof spot.travelDetails.best.season.description === 'string', 'Expected spot.travelDetails.best.season.description to be type string');
    assert.ok(typeof spot.travelDetails.best.tide.description === 'string', 'Expected spot.travelDetails.best.tide.description to be type string');
    assert.ok(typeof spot.travelDetails.best.size.description === 'string', 'Expected spot.travelDetails.best.size.description to be type string');
    assert.ok(typeof spot.travelDetails.best.windDirection.description === 'string', 'Expected spot.travelDetails.best.windDirection.description to be type string');
    assert.ok(typeof spot.travelDetails.best.swellDirection.description === 'string', 'Expected spot.travelDetails.best.swellDirection.description to be type string');
  }
);

/* Nearby Spots */
$http.get(
  `https://platform.surfline.com/taxonomy/nearby?lat=${lat}&lon=${lon}`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  `https://platform.surfline.com/taxonomy/nearby?lat=${lat}&lon=${lon}`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const { data } = JSON.parse(body);
    assert.ok(typeof data[0].spot === 'string', 'Expected data[0].spot to be type string');
    assert.ok(typeof data[0].name === 'string', 'Expected data[0].name to be type string');
    assert.ok(typeof data[0].location.coordinates[0] === 'number', 'Expected data[0].coordinates to be type number');
    assert.ok(typeof data[0].location.coordinates[1] === 'number', 'Expected data[0].coordinates to be type number');
  }
);

/* Spots */
$http.get(
  `https://platform.surfline.com/spots`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  `https://platform.surfline.com/spots`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const parsedBody = JSON.parse(body);
    assert.ok(typeof parsedBody[0]._id === 'string', 'Expected body[0]._id to be type string');
    assert.ok(typeof parsedBody[0].name === 'string', 'Expected body[0].name to be type string');
    assert.ok(typeof parsedBody[0].location.coordinates[0] === 'number', 'Expected body[0].coordinates to be type number');
    assert.ok(typeof parsedBody[0].location.coordinates[1] === 'number', 'Expected body[0].coordinates to be type number');
  }
);

/* Subregions */
$http.get(
  `https://platform.surfline.com/subregions`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  `https://platform.surfline.com/subregions`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const parsedBody = JSON.parse(body);
    assert.ok(typeof parsedBody[0]._id === 'string', 'Expected body[0]._id to be type string');
    assert.ok(typeof parsedBody[0].name === 'string', 'Expected body[0].name to be type string');
  }
);

/* Regions */
$http.get(
  `https://platform.surfline.com/regions`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  `https://platform.surfline.com/regions`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const parsedBody = JSON.parse(body);
    assert.ok(typeof parsedBody[0]._id === 'string', 'Expected body[0]._id to be type string');
    assert.ok(typeof parsedBody[0].name === 'string', 'Expected body[0].name to be type string');
  }
);

/* Charts */
$http.get(
  `https://platform.surfline.com/charts/types?subregionId=58581a836630e24c448790ed`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  `https://platform.surfline.com/charts/types?subregionId=58581a836630e24c448790ed`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const { data } = JSON.parse(body);
    assert.strictEqual(data.types.region.length, 12, 'Expected data.types.region to have length 12');
    assert.ok(typeof data.types.region[0].type === 'string', 'Expected data.types.region[0].type to be type string');
    assert.ok(typeof data.types.region[0].name === 'string', 'Expected data.types.region[0].name to be type string');
  }
);

$http.get(
  `https://platform.surfline.com/charts/images?subregionId=58581a836630e24c448790ed&type=japanwave`,
  {},
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 401, 'Expected 401 Unauthorized response');
  }
);

$http.get(
  `https://platform.surfline.com/charts/images?subregionId=58581a836630e24c448790ed&type=japanwave`,
  options,
  (err, response, body) => {
    assert.strictEqual(response.statusCode, 200, 'Expected 200 OK response');

    const { data } = JSON.parse(body);
    assert.strictEqual(data.images.length, 30, 'Expected data.images to have length 30');
    assert.ok(typeof data.images[0].url === 'string', 'Expected data.images[0].url to be type string');
    assert.ok(typeof data.images[0].timestamp === 'number', 'Expected data.images[0].timestamp to be type number');
  }
);
