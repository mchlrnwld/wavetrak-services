provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services-proxy/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  application = "services-proxy"
  environment = "prod"

  iam_role_arn           = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  alb_listener_arn_http  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-ecs-services-proxy-prod/15efa45c16bb4f3d/4ef8eb9113a8a319"
  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-ecs-services-proxy-prod/15efa45c16bb4f3d/6f2a54a1c85cfb69"
  default_vpc            = "vpc-116fdb74"
  dns_name               = "services.surfline.com"
  certificate_arn        = "arn:aws:acm:us-west-1:833713747344:certificate/f6268a81-8d22-4928-b244-fa589b0c3bca"

  service_td_count = 10
  service_lb_rules = [
    {
      field = "path-pattern"
      value = "*"
    },
  ]
}

data "aws_route53_zone" "dns_zone" {
  name = "services.surfline.com"
}

data "aws_route53_zone" "surfline_dns_zone" {
  name = "surfline.com."
}

data "aws_lb" "service_proxy_alb" {
  name = "${local.company}-ecs-services-proxy-${local.environment}"
}

data "aws_ssm_parameter" "services_proxy_integration_key" {
  name = "/${local.environment}/newrelic/services-proxy/integration-key"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEWRELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEWRELIC_API_KEY"
}

module "services-proxy" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  newrelic_integration_key = data.aws_ssm_parameter.services_proxy_integration_key.value

  iam_role_arn    = local.iam_role_arn
  default_vpc     = local.default_vpc
  dns_name        = local.dns_name
  dns_zone_id     = data.aws_route53_zone.dns_zone.zone_id
  certificate_arn = local.certificate_arn

  service_lb_rules = local.service_lb_rules
  service_td_count = local.service_td_count

  load_balancer_arn      = data.aws_lb.service_proxy_alb.arn
  load_balancer_dns_name = data.aws_lb.service_proxy_alb.dns_name
  service_proxy_alb      = "${data.aws_lb.service_proxy_alb.dns_name}"
  alb_listener_arn_http  = local.alb_listener_arn_http
  alb_listener_arn_https = local.alb_listener_arn_https

  auto_scaling_enabled        = false
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = ""
  auto_scaling_min_size       = ""
  auto_scaling_max_size       = ""
  auto_scaling_alb_arn_suffix = ""
  task_deregistration_delay   = 3

  new_relic_error_threshold_values = {
    "3XX" = "250000"
    "4XX" = "4000"
    "5XX" = "550"
  }
}

resource "aws_route53_record" "surfline_platform" {
  zone_id = data.aws_route53_zone.surfline_dns_zone.zone_id
  name    = "platform.surfline.com"
  type    = "CNAME"
  ttl     = 300
  records = ["platform.surfline.com.cdn.cloudflare.net"]
}

module "third_party_api_contract" {
  source = "../../modules/third-party-api-contract"
}
