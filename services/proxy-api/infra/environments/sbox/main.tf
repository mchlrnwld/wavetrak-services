provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services-proxy/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  application = "services-proxy"
  environment = "sandbox"

  iam_role_arn           = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  alb_listener_arn_http  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-ecs-services-proxy-sandbox/29d8933dcf777313/6226b66a9b90564a"
  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-ecs-services-proxy-sandbox/29d8933dcf777313/dfb53f20e3b742ce"
  default_vpc            = "vpc-981887fd"
  dns_name               = "services.sandbox.surfline.com"
  certificate_arn        = "arn:aws:acm:us-west-1:665294954271:certificate/5c0161f4-e15a-4ee8-8de3-bd19053d0e24"

  service_td_count = 2
  service_lb_rules = [
    {
      field = "path-pattern"
      value = "*"
    },
  ]
}

data "aws_route53_zone" "dns_zone" {
  name = "${local.environment}.surfline.com."
}

data "aws_lb" "service_proxy_alb" {
  name = "${local.company}-ecs-services-proxy-${local.environment}"
}

data "aws_ssm_parameter" "services_proxy_integration_key" {
  name = "/${local.environment}/newrelic/services-proxy/integration-key"
}

module "services-proxy" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  newrelic_integration_key = data.aws_ssm_parameter.services_proxy_integration_key.value

  iam_role_arn    = local.iam_role_arn
  default_vpc     = local.default_vpc
  dns_name        = local.dns_name
  dns_zone_id     = data.aws_route53_zone.dns_zone.zone_id
  certificate_arn = local.certificate_arn

  service_lb_rules = local.service_lb_rules
  service_td_count = local.service_td_count

  load_balancer_arn      = data.aws_lb.service_proxy_alb.arn
  load_balancer_dns_name = data.aws_lb.service_proxy_alb.dns_name
  service_proxy_alb      = "${data.aws_lb.service_proxy_alb.dns_name}"
  alb_listener_arn_http  = local.alb_listener_arn_http
  alb_listener_arn_https = local.alb_listener_arn_https

  auto_scaling_enabled        = false
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = ""
  auto_scaling_min_size       = ""
  auto_scaling_max_size       = ""
  auto_scaling_alb_arn_suffix = ""
  task_deregistration_delay   = 3

  new_relic_error_threshold_values = {
    "3XX" = "2800"
    "4XX" = "4000"
    "5XX" = "200"
  }
}

resource "aws_route53_record" "surfline_platform" {
  zone_id = data.aws_route53_zone.dns_zone.zone_id
  name    = "platform.sandbox.surfline.com"
  type    = "CNAME"
  ttl     = 300
  records = [data.aws_lb.service_proxy_alb.dns_name]
}
