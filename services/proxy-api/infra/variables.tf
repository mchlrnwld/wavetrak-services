variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "alb_listener_arn_http" {
}

variable "alb_listener_arn_https" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "default_vpc" {
}

variable "auto_scaling_enabled" {
  default = true
}

variable "auto_scaling_scale_by" {
  default = "cpu_utilization"
}

variable "auto_scaling_target_value" {
  default = ""
}

variable "auto_scaling_min_size" {
  default = ""
}

variable "auto_scaling_max_size" {
  default = ""
}

variable "auto_scaling_alb_arn_suffix" {
}

variable "task_deregistration_delay" {
}

variable "service_proxy_alb" {
}

variable "certificate_arn" {
}

variable "load_balancer_dns_name" {
}

variable "newrelic_integration_key" {
}

variable "new_relic_error_threshold_values" {
  type = map(string)
}
