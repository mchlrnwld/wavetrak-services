module "services-proxy" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = "${var.application}"

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-services-proxy"
  service_td_container_name   = "services-proxy"
  service_td_count            = var.service_td_count
  service_port                = 8080
  service_alb_priority        = 100
  alb_listener                = var.alb_listener_arn_http

  dns_name                   = var.dns_name
  dns_zone_id                = var.dns_zone_id
  default_vpc                = var.default_vpc
  ecs_cluster                = "sl-core-svc-${var.environment}"
  iam_role_arn               = var.iam_role_arn
  load_balancer_arn          = var.load_balancer_arn
  target_group_name_override = "srv-proxy-core-svc"

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_alb_arn_suffix = var.auto_scaling_alb_arn_suffix
  task_deregistration_delay   = var.task_deregistration_delay
}

# Rule for the proxy listener
resource "aws_alb_listener_rule" "services_proxy_routing_rule" {
  listener_arn = var.alb_listener_arn_https
  priority     = "100"

  action {
    type             = "forward"
    target_group_arn = module.services-proxy.target_group
  }
  condition {
    path_pattern {
      values = ["*"]
    }
  }
}

# Create New Relic alert channel
resource "newrelic_alert_channel" "services_proxy_alert_channel" {
  name = var.environment == "prod" ? "Services Proxy" : "Services Proxy (${var.environment})"
  type = "pagerduty"

  config {
    service_key = var.newrelic_integration_key
  }
}

# Create New Relic alert policy
resource "newrelic_alert_policy" "services_proxy_policy" {
  name = var.environment == "prod" ? "Services Proxy" : "Services Proxy (${var.environment})"
  incident_preference = "PER_CONDITION_AND_TARGET"
}

# CPU alert
resource "newrelic_nrql_alert_condition" "services_proxy_cpu" {
  name                         = "Services Proxy ${title(var.environment)}: High CPU"
  policy_id                    = newrelic_alert_policy.services_proxy_policy.id
  type                         = "static"
  description                  = "High CPU usage from the services proxy service"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("./${path.module}/resources/services-proxy-cpu-query.tmpl", { environment = var.environment })
    evaluation_offset = "20"
  }

  critical {
    operator              = "above"
    threshold             = 95
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}

# Memory alert
resource "newrelic_nrql_alert_condition" "services_proxy_memory" {
  name                         = "Services Proxy ${title(var.environment)}: High Memory Usage"
  policy_id                    = newrelic_alert_policy.services_proxy_policy.id
  type                         = "static"
  description                  = "High memory usage from the services proxy service"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("./${path.module}/resources/services-proxy-memory-query.tmpl", { environment = var.environment })
    evaluation_offset = "20"
  }

  critical {
    operator              = "above"
    threshold             = 50
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}

# Error rate alerts
resource "newrelic_nrql_alert_condition" "services_proxy_error_rate_alerts" {
  for_each = var.new_relic_error_threshold_values

  name                         = "Services Proxy ${title(var.environment)}: High ${each.key} Error Rate"
  policy_id                    = newrelic_alert_policy.services_proxy_policy.id
  type                         = "static"
  description                  = "High ${each.key} error rate from the services proxy service"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("./${path.module}/resources/services-proxy-error-rate-query.tmpl", { environment = var.environment, error = each.key })
    evaluation_offset = "20"
  }

  critical {
    operator              = "above"
    threshold             = each.value
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}
