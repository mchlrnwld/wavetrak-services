import chai from 'chai';
import dirtyChai from 'dirty-chai';
import pact from './pact';
import admin from './admin';
import sns from './sns';

chai.use(dirtyChai);

describe('Spots API', () => {
  pact();
  admin();
  sns();
});
