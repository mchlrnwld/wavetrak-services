import { MongoClient } from 'mongodb';

const connectMongo = () => MongoClient.connect('mongodb://mongo:27017/KBYG');

export default connectMongo;
