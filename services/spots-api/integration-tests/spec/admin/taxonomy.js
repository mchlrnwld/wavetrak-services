import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';

const taxonomy = () => {
  describe('/taxonomy', () => {
    let db;
    const geonameTaxonomy = {
      _id: new ObjectID('58f7ed5fdadb30820bb3987c'),
      geonameId: 5358705,
      liesIn: ['58f7ed5ddadb30820bb39651'],
      name: 'Huntington Beach',
      category: 'geonames',
    };

    before(async () => {
      db = await connectMongo();
    });

    afterEach(() => cleanMongo(db));

    after(() => {
      db.close();
    });

    describe('POST', () => {
      it('creates a geoname taxonomy', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/taxonomy`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(geonameTaxonomy),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.be.ok();

        const geoname = await db.collection('Taxonomy').findOne({ _id: new ObjectID(body._id) });
        expect(geoname).to.be.ok();
        expect(geoname.type).to.equal('geoname');
      });
    });

    describe('DELETE', () => {
      before(async () => {
        await db.collection('Taxonomy').insert(geonameTaxonomy);
      });

      it('deletes a geoname taxonomy', async () => {
        const taxonomyId = geonameTaxonomy._id;
        const response = await fetch(`${config.SPOTS_API}/admin/taxonomy/${taxonomyId}`, {
          method: 'DELETE',
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const geoname = await db.collection('Taxonomy').findOne({ _id: taxonomyId });
        expect(geoname).to.be.null();
      });
    });

    describe('PATCH', () => {
      const taxonomyId = new ObjectID('58f7ed5fdadb30820bb3987c');
      const originals = {
        huntingtonBeach: {
          _id: taxonomyId,
          name: 'Huntington Beach',
          category: 'geonames',
          type: 'geoname',
          geonameId: 5358705,
          liesIn: [new ObjectID('58f7ed5ddadb30820bb39651')],
          hasSpots: true,
        },
        orangeCounty: {
          _id: new ObjectID('58f7ed5ddadb30820bb39651'),
          name: 'Orange County',
          type: 'geoname',
          geonameId: 5379524,
          liesIn: [new ObjectID('58f7ed51dadb30820bb387a6')],
          hasSpots: true,
        },
        losAngelesCounty: {
          _id: new ObjectID('58f7ed5bdadb30820bb393cd'),
          name: 'Los Angeles County',
          type: 'geoname',
          geonameId: 5368381,
          liesIn: [new ObjectID('58f7ed51dadb30820bb387a6')],
          hasSpots: false,
        },
        california: {
          _id: new ObjectID('58f7ed51dadb30820bb387a6'),
          name: 'California',
          type: 'geoname',
          geonameId: 5332921,
          liesIn: [null],
          hasSpots: true,
        },
      };

      before(async () => {
        await db.collection('Taxonomy').insertMany(Object.values(originals));
      });

      it('updates a taxonomy and normalizes hasSpots properties', async () => {
        const updates = {
          name: 'Huntington Beach (LA)',
          liesIn: ['58f7ed5bdadb30820bb393cd'],
        };
        const response = await fetch(
          `${config.SPOTS_API}/admin/taxonomy/${originals.huntingtonBeach._id}`,
          {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(updates),
          },
        );
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body.name).to.equal(updates.name);
        expect(body.liesIn).to.deep.equal(updates.liesIn);

        const taxonomyList = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomyList).to.be.ok();
        expect(taxonomyList).to.have.length(4);

        const huntingtonBeach = taxonomyList.find(({ name }) => name === 'Huntington Beach (LA)');
        expect(huntingtonBeach).to.be.ok();
        expect(huntingtonBeach).to.deep.equal({
          ...body,
          _id: new ObjectID(body._id),
          liesIn: body.liesIn.map(id => new ObjectID(id)),
          createdAt: huntingtonBeach.createdAt,
          updatedAt: huntingtonBeach.updatedAt,
        });

        const orangeCounty = taxonomyList.find(({ name }) => name === 'Orange County');
        expect(orangeCounty).to.be.ok();
        expect(orangeCounty).to.deep.equal({
          ...originals.orangeCounty,
          hasSpots: false,
          updatedAt: orangeCounty.updatedAt,
        });

        const losAngelesCounty = taxonomyList.find(({ name }) => name === 'Los Angeles County');
        expect(losAngelesCounty).to.be.ok();
        expect(losAngelesCounty).to.deep.equal({
          ...originals.losAngelesCounty,
          hasSpots: true,
          updatedAt: losAngelesCounty.updatedAt,
        });

        const california = taxonomyList.find(({ name }) => name === 'California');
        expect(california).to.be.ok();
        expect(california).to.deep.equal({
          ...originals.california,
          hasSpots: true,
        });
      });
    });
  });
};

export default taxonomy;
