import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';

const subregions = () => {
  describe('/subregions', () => {
    let db;
    const regionId = new ObjectID('5908c45e6a2e4300134fbe92');
    const regionTaxonomyId = new ObjectID('5908c46bdadb30820b23c1f3');

    before(async () => {
      db = await connectMongo();
    });

    afterEach(() => cleanMongo(db));

    after(() => {
      db.close();
    });

    describe('POST', () => {
      it('creates a region', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/regions`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'Southern California',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.be.ok();
        expect(body.createdAt).to.be.ok();
        expect(body.updatedAt).to.be.ok();

        const region = await db.collection('Regions').findOne({ _id: new ObjectID(body._id) });
        expect(region).to.be.ok();
      });
    });

    describe('DELETE', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Taxonomy').insert({
            _id: regionTaxonomyId,
            region: regionId,
            liesIn: [],
          }),
        ]);
      });

      it('deletes a region', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/regions/${regionId}`, {
          method: 'DELETE',
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.equal(`${regionId}`);

        const [region, taxonomy] = await Promise.all([
          db.collection('Regions').findOne({ _id: regionId }),
          db.collection('Taxonomy').findOne({ _id: regionTaxonomyId }),
        ]);
        expect(region).to.be.null();
        expect(taxonomy).to.be.null();
      });
    });

    describe('PUT', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
        ]);
      });

      it('updates a region', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/regions/${regionId}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'Southern California UPDATED',
            region: regionId,
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const region = await db.collection('Regions').findOne({ _id: regionId });
        expect(region).to.be.ok();
        expect(region.name).to.equal('Southern California UPDATED');
      });
    });
  });
};

export default subregions;
