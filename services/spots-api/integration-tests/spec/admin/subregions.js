import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';
import spotFixture from '../fixtures/spotHbPierSouthside.json';

const subregions = () => {
  describe('/subregions', () => {
    let db;
    const regionId = new ObjectID('5908c45e6a2e4300134fbe92');
    const subregionId = new ObjectID('58581a836630e24c44878fd6');
    const subregionTaxonomyId = new ObjectID('58f7ed5ddadb30820bb39689');
    const regionTaxonomyId = new ObjectID('58fa4b2986fd4b596bc05fc9');
    const countyTaxonomyId = new ObjectID('58f7ed5ddadb30820bb39651');
    const spotId = new ObjectID('5842041f4e65fad6a77088ed');

    before(async () => {
      db = await connectMongo();
    });

    afterEach(() => cleanMongo(db));

    after(() => {
      db.close();
    });

    describe('POST', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Taxonomy').insert({
            _id: countyTaxonomyId,
            name: 'Orange County',
            geonameId: 5379524,
            hasSpots: true,
          }),
        ]);
      });

      it('creates a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'North Orange County',
            region: regionId,
            geoname: 5379524,
            offshoreSwellLocation: {
              type: 'Point',
              coordinates: [-118, 33.5],
            },
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: new ObjectID(body._id) });
        expect(subregion).to.be.ok();

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(3);

        const southernCalifornia = taxonomy.find(({ name }) => name === 'Southern California');
        expect(southernCalifornia).to.be.ok();

        const orangeCounty = taxonomy.find(({ name }) => name === 'Orange County');
        expect(orangeCounty).to.be.ok();

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.liesIn).to.deep.equal([countyTaxonomyId, southernCalifornia._id]);
      });
    });

    describe('DELETE', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            name: 'North Orange County',
            region: regionId,
          }),
          db.collection('Taxonomy').insert({
            _id: subregionTaxonomyId,
            subregion: subregionId,
            liesIn: [],
          }),
        ]);
      });

      it('deletes a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'DELETE',
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.equal(`${subregionId}`);

        const [subregion, taxonomy] = await Promise.all([
          db.collection('Subregions').findOne({ _id: subregionId }),
          db.collection('Taxonomy').findOne({ _id: subregionTaxonomyId }),
        ]);
        expect(subregion).to.be.null();
        expect(taxonomy).to.be.null();
      });
    });

    describe('PUT', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            name: 'North Orange County',
            region: regionId,
            geoname: 5379524,
          }),
          db.collection('Taxonomy').insert([
            {
              _id: subregionTaxonomyId,
              type: 'subregion',
              subregion: subregionId,
              liesIn: [regionTaxonomyId, countyTaxonomyId],
            },
            {
              _id: regionTaxonomyId,
              type: 'region',
              region: regionId,
            },
            {
              _id: countyTaxonomyId,
              name: 'Orange County',
              type: 'geoname',
              geonameId: 5379524,
              hasSpots: true,
            },
            {
              name: 'Los Angeles County',
              type: 'geoname',
              geonameId: 5368381,
              hasSpots: true,
            },
          ]),
        ]);
      });

      it('updates a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'North Orange County UPDATED',
            region: regionId,
            geoname: 5368381,
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion.name).to.equal('North Orange County UPDATED');

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(4);

        const orangeCounty = taxonomy.find(({ name }) => name === 'Orange County');
        expect(orangeCounty).to.be.ok();

        const losAngelesCounty = taxonomy.find(({ name }) => name === 'Los Angeles County');
        expect(losAngelesCounty).to.be.ok();

        const southernCalifornia = taxonomy.find(({ name }) => name === 'Southern California');
        expect(southernCalifornia).to.be.ok();

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County UPDATED');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.liesIn).to.deep.equal([
          regionTaxonomyId,
          losAngelesCounty._id,
        ]);
      });
    });

    describe('PATCH', () => {
      let subregionBeforeUpdate;
      beforeEach(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            name: 'North Orange County',
            region: regionId,
            geoname: 5379524,
            imported: { basins: [] },
            order: '1',
            status: 'DRAFT',
            forecastStatus: { inactiveMessage: 'Inactive status message', status: 'active' },
          }),
          db.collection('Taxonomy').insert([
            {
              _id: subregionTaxonomyId,
              type: 'subregion',
              subregion: subregionId,
              liesIn: [regionTaxonomyId, countyTaxonomyId],
            },
            {
              _id: regionTaxonomyId,
              type: 'region',
              region: regionId,
            },
            {
              _id: countyTaxonomyId,
              name: 'Orange County',
              type: 'geoname',
              geonameId: 5379524,
              hasSpots: true,
            },
            {
              name: 'Los Angeles County',
              type: 'geoname',
              geonameId: 5368381,
              hasSpots: true,
            },
          ]),
          db.collection('Spots').insert({
            ...spotFixture,
            _id: spotId,
            status: 'PUBLISHED',
          }),
        ]);

        subregionBeforeUpdate = await db.collection('Subregions').findOne({ _id: subregionId });
      });

      afterEach(() => {
        subregionBeforeUpdate = null;
      });

      it('updates the name of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'North Orange County UPDATED',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          name: 'North Orange County UPDATED',
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });

        // Make sure the taxonomy didn't change other than the subregion name
        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(4);

        const orangeCounty = taxonomy.find(({ name }) => name === 'Orange County');
        expect(orangeCounty).to.be.ok();

        const losAngelesCounty = taxonomy.find(({ name }) => name === 'Los Angeles County');
        expect(losAngelesCounty).to.be.ok();

        const southernCalifornia = taxonomy.find(({ name }) => name === 'Southern California');
        expect(southernCalifornia).to.be.ok();

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County UPDATED');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.liesIn).to.deep.equal([
          regionTaxonomyId,
          countyTaxonomyId,
        ]);
      });

      it('updates the taxonomy of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'North Orange County UPDATED',
            region: regionId,
            geoname: 5368381,
          }),
        });

        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          name: 'North Orange County UPDATED',
          region: regionId,
          geoname: 5368381,
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(4);

        const orangeCounty = taxonomy.find(({ name }) => name === 'Orange County');
        expect(orangeCounty).to.be.ok();

        const losAngelesCounty = taxonomy.find(({ name }) => name === 'Los Angeles County');
        expect(losAngelesCounty).to.be.ok();

        const southernCalifornia = taxonomy.find(({ name }) => name === 'Southern California');
        expect(southernCalifornia).to.be.ok();

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County UPDATED');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.liesIn).to.deep.equal([
          regionTaxonomyId,
          losAngelesCounty._id,
        ]);
      });

      it('updates the forecast status of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });
      });

      it('updates the basin of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            basin: 'natl',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          imported: { basins: ['natl'] },
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });
      });

      it('updates the order of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            order: '2',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          order: '2',
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });
      });

      it('updates the status of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            status: 'PUBLISHED',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          status: 'PUBLISHED',
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });
      });

      it('updates the primarySpot of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            primarySpot: spotId,
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          primarySpot: spotId,
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });
      });

      it('updates the offshoreSwellLocation of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            offshoreSwellLocation: {
              type: 'Point',
              coordinates: [-63.25, 44.25],
            },
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          offshoreSwellLocation: {
            type: 'Point',
            coordinates: [-63.25, 44.25],
          },
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });
      });

      it('updates all properties of a subregion', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'North Orange County UPDATED',
            region: regionId,
            geoname: 5368381,
            primarySpot: spotId,
            status: 'PUBLISHED',
            order: '2',
            basin: 'natl',
            forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
            offshoreSwellLocation: {
              type: 'Point',
              coordinates: [-63.25, 44.25],
            },
          }),
        });

        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const subregion = await db.collection('Subregions').findOne({ _id: subregionId });
        expect(subregion).to.be.ok();
        expect(subregion).to.deep.equal({
          ...subregionBeforeUpdate,
          name: 'North Orange County UPDATED',
          region: regionId,
          geoname: 5368381,
          primarySpot: spotId,
          status: 'PUBLISHED',
          order: '2',
          imported: { basins: ['natl'] },
          forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
          offshoreSwellLocation: {
            type: 'Point',
            coordinates: [-63.25, 44.25],
          },
          createdAt: subregion.createdAt,
          updatedAt: subregion.updatedAt,
        });


        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(4);

        const orangeCounty = taxonomy.find(({ name }) => name === 'Orange County');
        expect(orangeCounty).to.be.ok();

        const losAngelesCounty = taxonomy.find(({ name }) => name === 'Los Angeles County');
        expect(losAngelesCounty).to.be.ok();

        const southernCalifornia = taxonomy.find(({ name }) => name === 'Southern California');
        expect(southernCalifornia).to.be.ok();

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County UPDATED');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.liesIn).to.deep.equal([
          regionTaxonomyId,
          losAngelesCounty._id,
        ]);
      });

      it('returns 404 for a subregion not found', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/5908c45e6a2e4300134fbe92`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            status: 'DRAFT',
          }),
        });
        expect(response.status).to.equal(404);
      });

      it('returns 404 for an invalid subregion id', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/invalidsubregionid`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            status: 'DRAFT',
          }),
        });
        expect(response.status).to.equal(404);
      });

      it('returns 400 for an invalid geoname', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            geoname: 123,
          }),
        });
        expect(response.status).to.equal(400);
        const body = await response.json();
        expect(body.message).to.equal('Invalid Parameters: Invalid geoname');
      });

      it('returns 400 for an invalid primarySpot', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            primarySpot: '123',
          }),
        });
        expect(response.status).to.equal(400);
      });

      it('returns 500 for an malformed geoname', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/subregions/${subregionId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            geoname: '123a',
          }),
        });
        expect(response.status).to.equal(500);
      });
    });
  });
};

export default subregions;
