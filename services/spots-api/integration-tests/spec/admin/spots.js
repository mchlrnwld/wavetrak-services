import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';
import hbPierSouthsideFixture from '../fixtures/spotHbPierSouthside.json';
import pipelineFixture from '../fixtures/spotPipeline.json';
import lawrencetownRightFixture from '../fixtures/spotLawrencetownRight.json';

const spots = () => {
  describe('/spots', () => {
    let db;
    const regionId = new ObjectID('5908c45e6a2e4300134fbe92');
    const subregionId = new ObjectID('58581a836630e24c44878fd6');
    const spotId = new ObjectID('5842041f4e65fad6a77088ed');
    const spotTaxonomyId = new ObjectID('58f7edaadadb30820bb3e72c');
    const cityTaxonomyId = new ObjectID('58f7ed5fdadb30820bb3987c');

    before(async () => {
      db = await connectMongo();
    });

    afterEach(() => cleanMongo(db));

    after(() => {
      db.close();
    });

    describe('GET', () => {
      beforeEach(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            region: regionId,
            name: 'North Orange County',
          }),
          db.collection('Taxonomy').insert({
            _id: cityTaxonomyId,
            name: 'Huntington Beach',
            geonameId: 5358705,
            hasSpots: false,
          }),
          db.collection('Spots').insert({
            ...hbPierSouthsideFixture,
            status: 'PUBLISHED',
          }),
          db.collection('Spots').insert({
            ...lawrencetownRightFixture,
            status: 'DELETED',
          }),
          db.collection('Spots').insert({
            ...pipelineFixture,
            status: 'DRAFT',
          }),
        ]);
      });

      it('Retrieves spots with published and draft statuses', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/spots`, {
          method: 'GET',
        });
        const body = await response.json();
        expect(response.status).to.equal(200);
        expect(body).to.be.ok();
        // should exclude 1 spot that is 'deleted'
        expect(body.length).to.equal(2);
      });

      it('Retrieves only published spots for third party', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/spots?thirdParty=true`, {
          method: 'GET',
        });
        const body = await response.json();
        expect(response.status).to.equal(200);
        expect(body).to.be.ok();
        // should exclude 2 spots ('deleted', 'draft')
        expect(body.length).to.equal(1);
      });
    });

    describe('POST', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            region: regionId,
            name: 'North Orange County',
          }),
          db.collection('Taxonomy').insert({
            _id: cityTaxonomyId,
            name: 'Huntington Beach',
            geonameId: 5358705,
            hasSpots: false,
          }),
        ]);
      });

      it('creates a spot with its taxonomy and proper defaults, and updates hasSpots on parents', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/spots`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(hbPierSouthsideFixture),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.be.ok();

        const spot = await db.collection('Spots').findOne({ _id: new ObjectID(body._id) });
        expect(spot).to.be.ok();
        expect(spot.tideStation).to.be.null();
        expect(spot.hasThumbnail).to.be.false();
        expect(spot.lolaSurfBiasCoefficients).to.deep.equal({ linear: 1, constant: 0 });
        expect(spot.noraBiasCorrection).to.equal(0);
        expect(spot.swellWindow).to.deep.equal([]);
        expect(spot.spotType).to.deep.equal('SURFBREAK');
        expect(spot.abilityLevels).to.deep.equal(['BEGINNER']);
        expect(spot.boardTypes).to.deep.equal(['SHORTBOARD']);
        expect(spot.relivableRating).to.deep.equal(3);

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(4);

        const hbPierSouthside = taxonomy.find(({ name }) => name === 'HB Pier, Southside');
        expect(hbPierSouthside).to.be.ok();

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.hasSpots).to.be.true();

        const southernCalifornia = taxonomy.find(({ name }) => name === 'Southern California');
        expect(southernCalifornia).to.be.ok();
        expect(southernCalifornia.hasSpots).to.be.true();

        const huntingtonBeach = taxonomy.find(({ name }) => name === 'Huntington Beach');
        expect(huntingtonBeach).to.be.ok();
        expect(huntingtonBeach.hasSpots).to.be.true();
      });

      it('creates a draft spot with proper defaults and no taxonomy', async () => {
        const draftFixture = { ...hbPierSouthsideFixture, status: 'DRAFT' };
        const response = await fetch(`${config.SPOTS_API}/admin/spots`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(draftFixture),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.be.ok();

        const spot = await db.collection('Spots').findOne({ _id: new ObjectID(body._id) });
        expect(spot).to.be.ok();
        expect(spot.tideStation).to.be.null();
        expect(spot.hasThumbnail).to.be.false();
        expect(spot.lolaSurfBiasCoefficients).to.deep.equal({ linear: 1, constant: 0 });
        expect(spot.noraBiasCorrection).to.equal(0);
        expect(spot.swellWindow).to.deep.equal([]);
        expect(spot.spotType).to.deep.equal('SURFBREAK');
        expect(spot.abilityLevels).to.deep.equal(['BEGINNER']);
        expect(spot.boardTypes).to.deep.equal(['SHORTBOARD']);
        expect(spot.relivableRating).to.deep.equal(3);
        const taxonomy = await db
          .collection('Taxonomy')
          .find({ spot: new ObjectID(body._id) })
          .toArray();
        expect(taxonomy).to.have.length(0);
      });
    });

    describe('DELETE', () => {
      before(async () => {
        await Promise.all([
          db.collection('Spots').insert({
            ...hbPierSouthsideFixture,
            _id: spotId,
          }),
          db.collection('Taxonomy').insert([
            {
              _id: spotTaxonomyId,
              spot: spotId,
              liesIn: [cityTaxonomyId],
            },
            {
              _id: cityTaxonomyId,
              name: 'Huntington Beach',
              geonameId: 5358705,
              hasSpots: true,
            },
          ]),
        ]);
      });

      it('deletes a spot', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/spots/${spotId}`, {
          method: 'DELETE',
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body._id).to.equal(`${spotId}`);

        const [spot, taxonomy] = await Promise.all([
          db.collection('Spots').findOne({ _id: spotId }),
          db.collection('Taxonomy').find({}).toArray(),
        ]);
        expect(spot).to.be.ok();
        expect(spot.status).to.equal('DELETED');

        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(1);
        expect(taxonomy[0]._id).to.deep.equal(cityTaxonomyId);
        expect(taxonomy[0].hasSpots).to.be.false();
      });
    });

    describe('PUT', () => {
      before(async () => {
        await Promise.all([
          db.collection('Subregions').insert({
            _id: subregionId,
            name: 'North Orange County',
          }),
          db.collection('Spots').insert({
            ...hbPierSouthsideFixture,
            _id: spotId,
          }),
          db.collection('Taxonomy').insert([
            {
              _id: spotTaxonomyId,
              spot: spotId,
              type: 'spot',
              liesIn: [],
            },
            {
              _id: cityTaxonomyId,
              name: 'Huntington Beach',
              geonameId: 5358705,
              hasSpots: false,
            },
          ]),
        ]);
      });

      it('updates a spot', async () => {
        const response = await fetch(`${config.SPOTS_API}/admin/spots/${spotId}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            ...hbPierSouthsideFixture,
            name: 'HB Pier, Southside UPDATED',
            lolaSurfBiasCoefficients: {
              linear: 1.2,
              constant: 0.1,
            },
            spotType: 'WAVEPOOL',
            abilityLevels: ['ADVANCED'],
            boardTypes: ['LONGBOARD'],
            relivableRating: 2,
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const spot = await db.collection('Spots').findOne({ _id: spotId });
        expect(spot).to.be.ok();
        expect(spot.name).to.equal('HB Pier, Southside UPDATED');
        expect(spot.lolaSurfBiasCoefficients).to.deep.equal({ linear: 1.2, constant: 0.1 });
        expect(spot.spotType).to.deep.equal('WAVEPOOL');
        expect(spot.abilityLevels).to.deep.equal(['ADVANCED']);
        expect(spot.boardTypes).to.deep.equal(['LONGBOARD']);
        expect(spot.relivableRating).to.deep.equal(2);

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(3);

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County');
        expect(northOrangeCounty).to.be.ok();

        const hbPierSouthside = taxonomy.find(({ name }) => name === 'HB Pier, Southside UPDATED');
        expect(hbPierSouthside).to.be.ok();
        expect(hbPierSouthside.liesIn).to.deep.equal([cityTaxonomyId, northOrangeCounty._id]);

        const huntingtonBeach = taxonomy.find(({ name }) => name === 'Huntington Beach');
        expect(huntingtonBeach).to.be.ok();
        expect(huntingtonBeach.hasSpots).to.be.true();
      });
    });

    describe('PATCH', () => {
      afterEach(() => cleanMongo(db));

      it('updates a spot and deletes taxonomy if status changed to draft', async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            region: regionId,
            name: 'North Orange County',
          }),
          db.collection('Spots').insert({
            ...hbPierSouthsideFixture,
            _id: spotId,
          }),
          db.collection('Taxonomy').insert([
            {
              _id: spotTaxonomyId,
              spot: spotId,
              type: 'spot',
              liesIn: [cityTaxonomyId],
            },
            {
              _id: cityTaxonomyId,
              name: 'Huntington Beach',
              geonameId: 5358705,
              hasSpots: true,
            },
          ]),
        ]);
        const response = await fetch(`${config.SPOTS_API}/admin/spots/${spotId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'HB Pier, Southside NEW',
            status: 'DRAFT',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const spot = await db.collection('Spots').findOne({ _id: spotId });
        expect(spot).to.be.ok();
        expect(spot.name).to.equal('HB Pier, Southside NEW');

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(1);

        const huntingtonBeach = taxonomy.find(({ name }) => name === 'Huntington Beach');
        expect(huntingtonBeach).to.be.ok();
        expect(huntingtonBeach.hasSpots).to.be.false();
      });

      it('updates a spot status and creates taxonomy if it did not already exist', async () => {
        await Promise.all([
          db.collection('Subregions').insert({
            _id: subregionId,
            name: 'North Orange County',
          }),
          db.collection('Spots').insert({
            ...hbPierSouthsideFixture,
            status: 'DRAFT',
            _id: spotId,
          }),
          db.collection('Taxonomy').insert([
            {
              _id: cityTaxonomyId,
              name: 'Huntington Beach',
              geonameId: 5358705,
              hasSpots: false,
            },
          ]),
        ]);
        const response = await fetch(`${config.SPOTS_API}/admin/spots/${spotId}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: 'HB Pier, Southside UPDATED',
            status: 'PUBLISHED',
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();

        const spot = await db.collection('Spots').findOne({ _id: spotId });
        expect(spot).to.be.ok();
        expect(spot.name).to.equal('HB Pier, Southside UPDATED');

        const taxonomy = await db.collection('Taxonomy').find({}).toArray();
        expect(taxonomy).to.be.ok();
        expect(taxonomy).to.have.length(3);

        const northOrangeCounty = taxonomy.find(({ name }) => name === 'North Orange County');
        expect(northOrangeCounty).to.be.ok();
        expect(northOrangeCounty.hasSpots).to.be.true();

        const hbPierNorthside = taxonomy.find(({ name }) => name === 'HB Pier, Southside UPDATED');
        expect(hbPierNorthside).to.be.ok();
        expect(hbPierNorthside.liesIn).to.deep.equal([cityTaxonomyId, northOrangeCounty._id]);

        const huntingtonBeach = taxonomy.find(({ name }) => name === 'Huntington Beach');
        expect(huntingtonBeach).to.be.ok();
        expect(huntingtonBeach.hasSpots).to.be.true();
      });
    });

    it('updates a spot forecast status to inactive', async () => {
      await Promise.all([
        db.collection('Subregions').insert({
          _id: subregionId,
          name: 'North Orange County',
        }),
        db.collection('Spots').insert({
          ...hbPierSouthsideFixture,
          status: 'DRAFT',
          _id: spotId,
          forecastStatus: { inactiveMessage: 'Inactive status message', status: 'active' },
        }),
        db.collection('Taxonomy').insert([
          {
            _id: cityTaxonomyId,
            name: 'Huntington Beach',
            geonameId: 5358705,
            hasSpots: false,
          },
        ]),
      ]);
      const response = await fetch(`${config.SPOTS_API}/admin/spots/${spotId}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          forecastStatus: {
            inactiveMessage: 'My test message',
            status: 'inactive',
          },
        }),
      });
      expect(response.status).to.equal(200);
      const body = await response.json();
      expect(body).to.be.ok();

      const spot = await db.collection('Spots').findOne({ _id: spotId });
      expect(spot).to.be.ok();

      expect(spot.forecastStatus.inactiveMessage).to.equal('My test message');
      expect(spot.forecastStatus.status).to.equal('inactive');

      // Spot name and Taxonomy should be un-changed
      expect(spot.name).to.equal('HB Pier, Southside');
      const taxonomy = await db.collection('Taxonomy').find({}).toArray();
      expect(taxonomy).to.be.ok();
      expect(taxonomy).to.have.length(1);
      const huntingtonBeach = taxonomy.find(({ name }) => name === 'Huntington Beach');
      expect(huntingtonBeach).to.be.ok();
      expect(huntingtonBeach.hasSpots).to.be.false();
    });
  });
};

export default spots;
