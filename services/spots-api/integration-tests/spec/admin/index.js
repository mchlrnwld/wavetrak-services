import spots from './spots';
import subregions from './subregions';
import regions from './regions';
import taxonomy from './taxonomy';

const admin = () => {
  describe('/admin', () => {
    spots();
    subregions();
    regions();
    taxonomy();
  });
};

export default admin;
