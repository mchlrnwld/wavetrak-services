import { Verifier } from 'pact';

const pact = () => {
  describe('Pact Provider Verifier', () => {
    it('verifies prod consumer contracts', async function pactProviderVerifier() {
      this.timeout(10000);

      try {
        const output = await Verifier.verifyProvider({
          provider: 'spots-api',
          providerBaseUrl: 'http://application',
          providerStatesSetupUrl: 'http://pact-provider-states/setup',
          pactBrokerUrl: 'http://pact-broker.prod.surfline.com/',
          tags: ['prod'],
          publishVerificationResult: true,
          providerVersion: '1.0.0',
        });
        console.log(output);
      } catch (err) {
        if (err.message.includes('Unable to find pacts for given provider')) {
          console.log(err.message);
        } else {
          throw err;
        }
      }
    });
  });
};

export default pact;
