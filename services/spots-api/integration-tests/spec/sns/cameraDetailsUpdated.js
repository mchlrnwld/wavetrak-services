import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';
import spotHbPierSouthsideFixture from '../fixtures/spotHbPierSouthside.json';
import spotPipelineFixture from '../fixtures/spotPipeline.json';
import spotLawrencetownRightFixture from '../fixtures/spotLawrencetownRight.json';
import snsCameraDetailsUpdatedFixture from '../fixtures/snsCameraDetailsUpdated.json';
import snsAutoConfirmSubscriptionFixture from '../fixtures/snsAutoConfirmSubscription.json';

const cameraDetailsUpdated = () => {
  describe('/camera-details-updated', () => {
    let db;
    const spotIdHbPier = new ObjectID('5842041f4e65fad6a77088ed');
    const cameraIdHbPier1 = new ObjectID('615f128d3578c0244c01396e');
    const cameraIdHbPier2 = new ObjectID('58f7edaadadb30820bb3e72c');

    const spotIdPipeline = new ObjectID('5842041f4e65fad6a7708890');
    const cameraIdPipeline1 = new ObjectID('58349eed3421b20545c4b56c');
    const cameraIdPipeline2 = new ObjectID('58349ef6e411dc743a5d52cc');

    const spotIdLawrencetownRight = new ObjectID('584204204e65fad6a77096b0');

    const regionId = new ObjectID('5908c45e6a2e4300134fbe92');
    const subregionId = new ObjectID('58581a836630e24c44878fd6');
    const cityTaxonomyId = new ObjectID('58f7ed5fdadb30820bb3987c');

    const snsTopicArn = 'arn:aws:sns:us-west-1:665294954271:camera_details_updated_test';

    before(async () => {
      db = await connectMongo();
    });

    after(async () => {
      await cleanMongo(db);
      db.close();
    });

    describe('POST', () => {
      before(async () => {
        await Promise.all([
          db.collection('Regions').insert({
            _id: regionId,
            name: 'Southern California',
          }),
          db.collection('Subregions').insert({
            _id: subregionId,
            region: regionId,
            name: 'North Orange County',
          }),
          db.collection('Taxonomy').insert({
            _id: cityTaxonomyId,
            name: 'Huntington Beach',
            geonameId: 5358705,
            hasSpots: false,
          }),
          db.collection('Spots').insert({
            ...spotHbPierSouthsideFixture,
            _id: spotIdHbPier,
            cams: [cameraIdHbPier1, cameraIdHbPier2],
          }),
          db.collection('Spots').insert({
            ...spotLawrencetownRightFixture,
            _id: spotIdLawrencetownRight,
            cams: [cameraIdHbPier1], // shared cam with HB Pier
          }),
          db.collection('Spots').insert({
            ...spotPipelineFixture,
            _id: spotIdPipeline,
            cams: [cameraIdPipeline1, cameraIdPipeline2],
          }),
        ]);
      });

      it('returns invalid SNS Topic ARN for invalid topics', async () => {
        const response = await fetch(`${config.SPOTS_API}/sns/camera-details-updated`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-amz-sns-message-type': 'Notification',
          },
          body: JSON.stringify({
            TopicArn: 'InvalidArn',
          }),
        });
        const body = await response.json();
        expect(response.status).to.equal(400);
        expect(body).to.be.ok();
        expect(body).to.deep.equal({ message: 'Invalid SNS topic ARN.' });
      });

      it('validates SubscribeURL has correct host and action before subscribing', async () => {
        const response = await fetch(`${config.SPOTS_API}/sns/camera-details-updated`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-amz-sns-message-type': 'SubscriptionConfirmation',
          },
          body: JSON.stringify({
            SubscribeURL: `https://sns.us-west-1.amazonaws.com/?Action=FakeAction&TopicArn=${snsTopicArn}`,
            TopicArn: `${snsTopicArn}`,
          }),
        });
        const body = await response.json();
        expect(response.status).to.equal(400);
        expect(body).to.be.ok();
        expect(body).to.deep.equal({ message: 'Invalid SubscribeURL.' });
      });

      it('validates SubscribeURL has correct SNS topic ARN before subscribing', async () => {
        const response = await fetch(`${config.SPOTS_API}/sns/camera-details-updated`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-amz-sns-message-type': 'SubscriptionConfirmation',
          },
          body: JSON.stringify({
            SubscribeURL:
              'https://sns.us-west-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-1:123456789012:FakeTopic',
            TopicArn: `${snsTopicArn}`,
          }),
        });
        const body = await response.json();
        expect(response.status).to.equal(400);
        expect(body).to.be.ok();
        expect(body).to.deep.equal({ message: 'Invalid SubscribeURL.' });
      });

      it('auto confirms an SNS topic subscription', async () => {
        const response = await fetch(`${config.SPOTS_API}/sns/camera-details-updated`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-amz-sns-message-type': 'SubscriptionConfirmation',
          },
          body: JSON.stringify(snsAutoConfirmSubscriptionFixture),
        });
        const body = await response.json();
        expect(response.status).to.equal(200);
        expect(body).to.be.ok();
        expect(body).to.deep.equal({ message: 'OK' });
      });

      it('Spots still reference camera after camera updated', async () => {
        // use cameraId that maps to a mock endpoint that returns
        // a 200 response to simulate the camera updated scenario
        const snsCameraDetailsUpdatedFixture200 = {
          ...snsCameraDetailsUpdatedFixture,
          Message:
            '{"cameraId":"715f128d3578c0244c01396e","topic":"CAMERA_DETAILS_UPDATED_SNS_TOPIC","settings":{}}',
        };
        const response = await fetch(`${config.SPOTS_API}/sns/camera-details-updated`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-amz-sns-message-type': 'Notification',
          },
          // use cameraId that maps to a mock endpoint that returns
          // a 200 response to simulate the camera updated scenario
          body: JSON.stringify(snsCameraDetailsUpdatedFixture200),
        });
        const body = await response.json();
        expect(response.status).to.equal(200);
        expect(body).to.be.ok();

        // Confirm cameraId was NOT removed from spot.cams[] for HB Pier
        const spotHbPier = await db.collection('Spots').findOne({ _id: spotIdHbPier });
        expect(spotHbPier).to.be.ok();
        expect(spotHbPier.cams).to.deep.equal([cameraIdHbPier1, cameraIdHbPier2]);

        // Confirm Lawrencetown Right still cameras associated
        const spotLtown = await db.collection('Spots').findOne({ _id: spotIdLawrencetownRight });
        expect(spotLtown).to.be.ok();
        expect(spotLtown.cams).to.deep.equal([cameraIdHbPier1]);

        // Confirm Pipeline cameras are still in place and unaffected
        const spotPipeline = await db.collection('Spots').findOne({ _id: spotIdPipeline });
        expect(spotPipeline).to.be.ok();
        expect(spotPipeline.cams).to.deep.equal([cameraIdPipeline1, cameraIdPipeline2]);

        // Confirm other HB Pier spot info is unnaffected
        expect(spotHbPier.spotType).to.deep.equal('SURFBREAK');
        expect(spotHbPier.abilityLevels).to.deep.equal(['BEGINNER']);
        expect(spotHbPier.boardTypes).to.deep.equal(['SHORTBOARD']);
        expect(spotHbPier.relivableRating).to.deep.equal(3);
      });

      it('Spots no longer reference deleted camera after delete', async () => {
        // Note: the cameraId in the snsCameraDetailsUpdatedFixture matches
        // a mock endpoint that returns 400 with "camera not found"
        const snsCameraDetailsUpdatedFixture400 = {
          ...snsAutoConfirmSubscriptionFixture,
          Message:
            '{"cameraId":"615f128d3578c0244c01396e","topic":"CAMERA_DETAILS_UPDATED_SNS_TOPIC","settings":{}}',
        };
        const response = await fetch(`${config.SPOTS_API}/sns/camera-details-updated`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'x-amz-sns-message-type': 'Notification',
          },
          body: JSON.stringify(snsCameraDetailsUpdatedFixture400),
        });
        const body = await response.json();
        expect(response.status).to.equal(200);
        expect(body).to.be.ok();

        // Confirm cameraId was removed from spot.cams[] for HB Pier
        const spotHbPier = await db.collection('Spots').findOne({ _id: spotIdHbPier });
        expect(spotHbPier).to.be.ok();
        expect(spotHbPier.cams).to.deep.equal([cameraIdHbPier2]);

        // Confirm Lawrencetown Right no longer has any cameras associated
        const spotLtown = await db.collection('Spots').findOne({ _id: spotIdLawrencetownRight });
        expect(spotLtown).to.be.ok();
        expect(spotLtown.cams).to.deep.equal([]);

        // Confirm Pipeline cameras are still in place and unaffected
        const spotPipeline = await db.collection('Spots').findOne({ _id: spotIdPipeline });
        expect(spotPipeline).to.be.ok();
        expect(spotPipeline.cams).to.deep.equal([cameraIdPipeline1, cameraIdPipeline2]);

        // Confirm other HB Pier spot info is unnaffected
        expect(spotHbPier.spotType).to.deep.equal('SURFBREAK');
        expect(spotHbPier.abilityLevels).to.deep.equal(['BEGINNER']);
        expect(spotHbPier.boardTypes).to.deep.equal(['SHORTBOARD']);
        expect(spotHbPier.relivableRating).to.deep.equal(3);
      });
    });
  });
};

export default cameraDetailsUpdated;
