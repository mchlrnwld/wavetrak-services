import cameraDetailsUpdated from './cameraDetailsUpdated';

const sns = () => {
  describe('/sns', () => {
    cameraDetailsUpdated();
  });
};

export default sns;
