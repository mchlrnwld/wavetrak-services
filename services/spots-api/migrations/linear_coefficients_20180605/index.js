const fs = require('fs');
const { MongoClient, ObjectID } = require('mongodb');

require('dotenv').config();

const { MONGO_URL, MONGO_DB, COEFFICIENTS_FILE } = process.env;

const readFile = path => new Promise((resolve, reject) => fs.readFile(path, (err, data) => {
  if (err) return reject(err);
  return resolve(data);
}));

const connectDBCollection = async () => {
  const client = await MongoClient.connect(MONGO_URL);
  const db = client.db(MONGO_DB);
  const collection = db.collection('Spots');
  return collection;
};

const loadSpotCoefficients = async () => {
  const rawData = await readFile(COEFFICIENTS_FILE);
  const data = rawData.toString().split('\n').slice(1)
    .map(line => line.split(' '))
    .map(([id, a, b]) => ({
      id,
      a: Number(a),
      b: Number(b),
    }))
    .filter(({ id }) => id);
  return data;
};

const main = async () => {
  const [collection, spotCoefficients] = await Promise.all([
    connectDBCollection(),
    loadSpotCoefficients(),
  ]);
  const results = await Promise.all(spotCoefficients.map(sc => collection.updateOne(
    { _id: new ObjectID(sc.id) },
    {
      $set: {
        'lolaSurfBiasCoefficients.constant': sc.b,
        'lolaSurfBiasCoefficients.linear': sc.a,
      },
    },
  )));
  console.log(results);
};

main().then(() => {
  console.log('Done!');
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(-1);
});
