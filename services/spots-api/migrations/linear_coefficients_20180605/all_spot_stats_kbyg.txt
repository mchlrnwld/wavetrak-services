SPOT_ID COEFF_A COEFF_B SPOT_NAME
5842041f4e65fad6a77087f8 1.0773 0 Ocean Beach Overview   
5842041f4e65fad6a77087f9 0.9610 0 South Ocean Beach   
5842041f4e65fad6a7708805 0.9418 0 Steamer Lane Overview   
5842041f4e65fad6a7708806 0.6119 0 Cowells     
5842041f4e65fad6a7708807 0.9170 0 Pleasure Point    
5842041f4e65fad6a770880a 0.8616 0 Morro Bay    
5842041f4e65fad6a770880b 0.9755 0 Jack's     
5842041f4e65fad6a7708813 1.1339 0 County Line    
5842041f4e65fad6a7708817 1.0803 0 Malibu Second to Third Point 
5842041f4e65fad6a7708819 1.0788 0 Venice Beach    
5842041f4e65fad6a770881e 1.3370 0 Topanga Beach    
5842041f4e65fad6a7708820 1.2612 0 Seal Beach Overview   
5842041f4e65fad6a7708827 1.0980 0 HB Pier, Northside   
5842041f4e65fad6a7708828 1.2887 0 C St.    
5842041f4e65fad6a770882e 1.0552 0 Salt Creek    
5842041f4e65fad6a7708830 1.1765 0 T-Street     
5842041f4e65fad6a7708831 0.9401 0 The Point at San Onofre 
5842041f4e65fad6a7708832 0.9537 0 Oceanside Harbor    
5842041f4e65fad6a7708839 1.1413 0 Scripps     
5842041f4e65fad6a770883a 0.9812 0 Birdrock     
5842041f4e65fad6a770883b 1.0272 0 Blacks     
5842041f4e65fad6a770883c 0.8694 0 Windansea     
5842041f4e65fad6a770883f 0.8684 0 Ocean Beach    
5842041f4e65fad6a7708840 0.8017 0 Sunset Cliffs    
5842041f4e65fad6a7708841 0.9061 0 Pacific Beach    
5842041f4e65fad6a7708842 0.9672 0 Mission Beach    
5842041f4e65fad6a7708844 0.9778 0 Imperial Pier, Southside   
5842041f4e65fad6a7708847 0.8970 0 Imperial Pier, Northside   
5842041f4e65fad6a7708850 0.7164 0 Lincoln Blvd.    
5842041f4e65fad6a7708852 0.7660 0 90th St. Rockaways   
5842041f4e65fad6a7708856 0.8918 0 Manasquan Inlet    
5842041f4e65fad6a7708857 0.6965 0 Casino Pier    
5842041f4e65fad6a7708858 0.7586 0 1st St.    
5842041f4e65fad6a770885b 0.9074 0 Long Beach Island   
5842041f4e65fad6a7708863 0.8506 0 3rd St.    
5842041f4e65fad6a7708864 0.6365 0 1st St. Jetty to Pier 
5842041f4e65fad6a7708867 0.9972 0 S-Turns     
5842041f4e65fad6a770886d 0.8682 0 8th St.    
5842041f4e65fad6a770886f 0.8437 0 A St.    
5842041f4e65fad6a7708871 0.7709 0 New Smyrna Beach North  
5842041f4e65fad6a7708872 0.6019 0 Cocoa Beach Pier   
5842041f4e65fad6a7708873 0.7881 0 Indialantic Boardwalk    
5842041f4e65fad6a770887d 0.4718 0 Deerfield Beach Pier   
5842041f4e65fad6a7708887 1.0677 0 Upper Trestles    
5842041f4e65fad6a770888a 1.0480 0 Lower Trestles    
5842041f4e65fad6a770888d 0.7774 0 Sunset Beach    
5842041f4e65fad6a770888e 0.8032 0 Rocky Point    
5842041f4e65fad6a770888f 0.6983 0 Gas Chambers    
5842041f4e65fad6a7708890 0.7355 0 Pipeline     
5842041f4e65fad6a7708891 0.8117 0 Backdoor     
5842041f4e65fad6a7708892 0.9571 0 Rockpile     
5842041f4e65fad6a7708893 0.8565 0 Log Cabins    
5842041f4e65fad6a7708894 1.0682 0 Off-The-Wall     
5842041f4e65fad6a7708895 0.7720 0 Waimea Bay    
5842041f4e65fad6a7708897 0.7266 0 Jocko's     
5842041f4e65fad6a7708898 0.9261 0 Laniakea     
5842041f4e65fad6a7708899 0.7256 0 Chun's     
5842041f4e65fad6a770889c 1.2528 0 South Shore - Ala Moana Park
5842041f4e65fad6a770889d 1.4324 0 Banyans     
5842041f4e65fad6a77088a2 1.2341 0 Diamond Head    
5842041f4e65fad6a77088a5 0.9856 0 Ponto Jetties    
5842041f4e65fad6a77088af 1.0030 0 15th to 18th St.  
5842041f4e65fad6a77088b1 0.9863 0 Cardiff Reef Overview   
5842041f4e65fad6a77088b4 1.0028 0 Swami's     
5842041f4e65fad6a77088c4 0.8831 0 Old Man's at Tourmaline  
5842041f4e65fad6a77088cc 0.8933 0 La Jolla Shores   
5842041f4e65fad6a77088d7 0.9473 0 Doheny State Beach   
5842041f4e65fad6a77088e8 1.1027 0 Bolsa Chica State Beach  
5842041f4e65fad6a77088ea 1.0342 0 Goldenwest     
5842041f4e65fad6a77088ed 1.0516 0 HB Pier, Southside   
5842041f4e65fad6a7708906 1.0710 0 El Porto    
5842041f4e65fad6a7708907 1.0919 0 Manhattan Beach    
5842041f4e65fad6a7708914 1.1699 0 Sunset Point    
5842041f4e65fad6a7708920 1.1159 0 Torrance Beach to Haggerty's  
5842041f4e65fad6a7708930 0.4488 0 Chart House    
5842041f4e65fad6a770893a 1.3565 0 Zuma     
5842041f4e65fad6a7708976 0.7358 0 Pacifica/Lindamar     
5842041f4e65fad6a77089ac 0.9984 0 Pismo Beach Pier   
5842041f4e65fad6a77089cb 0.6594 0 Point Judith    
5842041f4e65fad6a77089e2 0.7051 0 Lido Beach    
5842041f4e65fad6a77089e3 0.7431 0 Long Sands Beach   
5842041f4e65fad6a77089e9 0.9180 0 The Wall    
5842041f4e65fad6a77089f0 0.8289 0 Lincoln Blvd. Overview   
5842041f4e65fad6a77089f6 1.6991 0 Terrace     
5842041f4e65fad6a7708a01 0.8179 0 16th Ave.    
5842041f4e65fad6a7708a23 0.5275 0 83rd to 86th St., North End
5842041f4e65fad6a7708a26 0.5785 0 Croatan to Pendleton   
5842041f4e65fad6a7708a38 0.9184 0 Cape Hatteras Lighthouse   
5842041f4e65fad6a7708a3b 0.7712 0 Frisco Pier    
5842041f4e65fad6a7708a3d 0.9396 0 Avon Pier    
5842041f4e65fad6a7708a40 1.1618 0 Nags Head Pier   
5842041f4e65fad6a7708a41 0.8609 0 Avalon Pier    
5842041f4e65fad6a7708a42 0.8331 0 1st St.    
5842041f4e65fad6a7708a43 0.8989 0 Old Station/Laundromats    
5842041f4e65fad6a7708a44 0.7783 0 Kitty Hawk Pier   
5842041f4e65fad6a7708a49 0.7453 0 Carolina Beach Pier   
5842041f4e65fad6a7708a85 0.6886 0 North Washout    
5842041f4e65fad6a7708a95 0.8154 0 Bethune Beach    
5842041f4e65fad6a7708a9d 0.8236 0 Ponce Inlet    
5842041f4e65fad6a7708a9f 0.6366 0 Sebastian Inlet    
5842041f4e65fad6a7708aa0 0.8923 0 Jacksonville Beach Pier   
5842041f4e65fad6a7708aa1 0.7520 0 Main Street North   
5842041f4e65fad6a7708aa3 0.6093 0 Ft. Pierce Inlet   
5842041f4e65fad6a7708aa4 0.8288 0 Ormond Beach    
5842041f4e65fad6a7708ac1 1.0064 0 South Beach    
5842041f4e65fad6a7708ac7 0.6278 0 16th St. South   
5842041f4e65fad6a7708ad2 0.8306 0 Delray Beach    
5842041f4e65fad6a7708b15 0.8416 0 Venice Jetties North   
5842041f4e65fad6a7708b16 0.7281 0 Venice Jetties South   
5842041f4e65fad6a7708b42 1.1763 0 Ala Moana Bowls   
5842041f4e65fad6a7708b72 0.7834 0 Lake Worth Pier Northside  
5842041f4e65fad6a7708b78 0.5740 0 Juno Pier    
5842041f4e65fad6a7708c5c 0.9143 0 Maria's Overview    
5842041f4e65fad6a7708df5 1.0008 0 Haleiwa     
5842041f4e65fad6a7708e1a 0.8149 0 Ocean Ave.    
5842041f4e65fad6a7708e40 1.0257 0 Matunuck     
5842041f4e65fad6a7708e54 1.0415 0 56th St.    
584204204e65fad6a7709057 0.8430 0 Pua'ena Point    
584204204e65fad6a7709115 1.2424 0 Blackies     
584204204e65fad6a7709148 0.7455 0 Waikiki Beach    
584204204e65fad6a77091ac 0.8613 0 Sunglow Pier Northside   
584204204e65fad6a7709327 0.8083 0 Flagler Beach Pier   
584204204e65fad6a7709395 0.6514 0 Wrightsville Beach    
584204204e65fad6a7709464 0.7536 0 Bath House    
584204204e65fad6a7709682 0.3854 0 East Hampton Beach   
584204204e65fad6a77096b1 1.4424 0 Ventura Point    
584204204e65fad6a77096b2 0.7326 0 St. Augustine Pier   
584204204e65fad6a770997f 0.5359 0 Bogue Inlet Pier   
584204204e65fad6a7709981 0.9875 0 Jennette's Pier, Southside   
584204204e65fad6a7709990 0.8079 0 Eckner Street    
584204204e65fad6a7709991 0.5468 0 42nd to 46th St., North End
584204204e65fad6a7709992 0.7641 0 Oceanana Pier    
584204204e65fad6a77099d4 0.9128 0 Old Man's at San Onofre 
584204204e65fad6a7709a9e 0.9944 0 Jennette's Pier, Northside   
584204214e65fad6a7709b9e 1.1556 0 Sunset Beach    
584204214e65fad6a7709b9f 1.0175 0 Malibu First Point   
584204214e65fad6a7709c79 0.9283 0 Ponto North    
584204214e65fad6a7709cba 0.9354 0 54th St.    
584204214e65fad6a7709cbd 0.7671 0 Hightower Beach    
584204214e65fad6a7709cdf 0.7336 0 Pipeline Overview    
584204214e65fad6a7709ce1 0.9495 0 Steamer Lane Close Up  
584204214e65fad6a7709ce7 0.6347 0 1st St. Jetty   
584204214e65fad6a7709ce8 1.0897 0 Abalone St.    
584204214e65fad6a7709ce9 0.4467 0 Croatan Jetty    
584204214e65fad6a7709ceb 0.8844 0 Backdoor Overview    
584204214e65fad6a7709cf0 0.7719 0 Waimea Bay Overview   
584204214e65fad6a7709cfc 1.2097 0 County Line Overview   
584204214e65fad6a7709cff 1.0071 0 Malibu Overview    
584204214e65fad6a7709d0a 0.8127 0 77th St. Rockaways   
584204214e65fad6a7709d0b 1.5269 0 Sapphire St.    
584204214e65fad6a7709d0e 0.7297 0 Billy Mitchell    
584204214e65fad6a7709d10 0.3217 0 40th St.    
584204214e65fad6a7709d12 0.7638 0 Pelican Beach Park   
584204214e65fad6a7709d1e 0.5400 0 NE 14th Ct.   
584204214e65fad6a7709d20 0.6266 0 Cowells Overview    
584204214e65fad6a7709d24 1.0243 0 El Porto North   
589a447b904aae00147f95e0 0.6630 0 Point Judith North   
590927576a2e4300134fbed8 1.4756 0 Venice Breakwater    
5943589de98ad90013191e1d 0.8609 0 Davis Park Surf   
