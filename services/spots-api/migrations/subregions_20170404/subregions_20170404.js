/* eslint-disable no-console */
import * as dbContext from '../model/dbContext';

import SpotModel from '../model/SpotModel';
import SubregionModel from '../model/SubregionModel';


const start = async () => {
  await dbContext.initMongoDB();
  const spots = await SpotModel.find({});

  let k = 0;
  for (const spot of spots) {
    console.log(`${k} of ${spots.length}`);
    try {
      const subregion = await SubregionModel.findOne({ spots: { $elemMatch: { spot: spot._id } } });
      if (subregion) {
        const hr = subregion.spots.find(s => `${s.spot}` === `${spot._id}`);
        spot.subregion = subregion._id;
        spot.humanReport.status = (hr && hr.humanReport) ? 'ON' : 'OFF';
        console.log(`${spot._id} \t ${spot.humanReport.status} \t ${spot.name} => ${subregion.name}`);
        await spot.save();
      } else {
        console.log('no subregion');
      }
    } catch (err) {
      console.log(err);
    }
    k += 1;
  }

  return `Processed ${spots.length} spots`;
};

start()
  .then((result) => {
    console.log(result);
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
