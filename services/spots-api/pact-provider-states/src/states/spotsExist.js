import { ObjectID } from 'mongodb';

const spotsExist = db => db.collection('Spots').insertMany([
  {
    _id: new ObjectID('5842041f4e65fad6a77088ed'),
    legacyId: 4874,
    legacyRegionId: 2143,
    name: 'HB Pier, Southside',
    slug: 'hb-pier-southside',
    location: {
      type: 'Point',
      coordinates: [
        -118.0032588192,
        33.654213041213,
      ],
    },
    forecastLocation: {
      type: 'Point',
      coordinates: [
        -118.001,
        33.652,
      ],
    },
    tideStation: 'Huntington Cliffs near Newport Bay',
    cams: [
      '583499cb3421b20545c4b53f',
    ],
    timezone: 'America/Los_Angeles',
    status: 'PUBLISHED',
    swellWindow: [
      {
        max: 200,
        min: 160,
      },
      {
        max: 285,
        min: 250,
      },
    ],
    best: {
      sizeRange: [
        {
          max: 8,
          min: 4,
        },
      ],
      swellWindow: [
        {
          max: 200,
          min: 180,
        },
        {
          max: 280,
          min: 250,
        },
      ],
      swellPeriod: [
        {
          max: 33,
          min: 9,
        },
      ],
      windDirection: [
        {
          max: 95,
          min: 350,
        },
      ],
    },
    hasThumbnail: true,
    politicalLocation: {
      hierarchy: [],
      city: {
        name: null,
        geonameId: null,
      },
    },
    humanReport: {
      status: 'AVERAGE',
      average: {
        spotOneId: new ObjectID('584204204e65fad6a77091aa'),
        spotTwoId: new ObjectID('584204204e65fad6a770998c'),
      },
      clone: {
        spotId: null,
      },
    },
    subregion: new ObjectID('58581a836630e24c44878fd6'),
    geoname: 5358705,
  },
  {
    _id: new ObjectID('5842041f4e65fad6a77088e3'),
    legacyId: 4860,
    legacyRegionId: 2950,
    name: 'Rockpile',
    slug: 'rockpile',
    location: {
      type: 'Point',
      coordinates: [
        -117.7915988640743,
        33.54268310353641,
      ],
    },
    forecastLocation: {
      type: 'Point',
      coordinates: [
        -117.793,
        33.542,
      ],
    },
    tideStation: 'Salt Creek ( San Clemente) based o',
    cams: [

    ],
    timezone: 'America/Los_Angeles',
    status: 'PUBLISHED',
    rank: 0,
    lolaSurfBiasCoefficients: {
      linear: 1.2,
      constant: 0.1,
    },
    noraBiasCorrection: 0.44,
    offshoreDirection: 212,
    swellWindow: [
      {
        min: 160,
        max: 215,
      },
      {
        min: 265,
        max: 285,
      },
    ],
    best: {
      windDirection: [
        {
          min: 344,
          max: 78,
        },
      ],
      swellPeriod: [
        {
          min: 9,
          max: 33,
        },
      ],
      swellWindow: [
        {
          min: 160,
          max: 215,
        },
      ],
      sizeRange: [
        {
          min: 4,
          max: 10,
        },
      ],
    },
    hasThumbnail: true,
    politicalLocation: {
      hierarchy: [],
    },
    humanReport: {
      status: 'OFF',
    },
    subregion: new ObjectID('58581a836630e24c4487900a'),
    geoname: 5364275,
  },
]);

export default spotsExist;
