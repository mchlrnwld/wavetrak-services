import { ObjectID } from 'mongodb';

const taxonomyExists = db => db.collection('Taxonomy').insertMany([
  {
    _id: new ObjectID('58f7edaadadb30820bb3e72c'),
    name: 'HB Pier, Southside',
    type: 'spot',
    category: 'surfline',
    spot: new ObjectID('5842041f4e65fad6a77088ed'),
    liesIn: [new ObjectID('58f7ed5fdadb30820bb3987c'), new ObjectID('58f7ed5ddadb30820bb39689')],
    hasSpots: false,
  },
  {
    _id: new ObjectID('58f7ed5fdadb30820bb3987c'),
    name: 'Huntington Beach',
    type: 'geoname',
    category: 'geonames',
    geonameId: 5358705,
    liesIn: [new ObjectID('58f7ed5ddadb30820bb39651')],
    enumeratedPath: ',Earth,North America,United States,California,Orange County,Huntington Beach',
    hasSpots: true,
  },
]);

export default taxonomyExists;
