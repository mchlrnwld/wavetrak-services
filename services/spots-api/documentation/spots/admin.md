# Admin Spot Management

Base URL: `http://spots-api.[env].surfline.com`

_Note: you must be inside the respective environment's VPN to access these endpoint_

## Spot Properties

| Param                         | Required | Type                  | Description                                                                        |
| ----------------------------- | -------- | --------------------- | ---------------------------------------------------------------------------------- |
| name                          | Yes      | Number                | The name of the spot                                                               |
| legacyId                      | No       | Number                | The legacy cms id of the spot                                                      |
| legacyRegionId                | No       | Number                | The legacy cms subregion id                                                        |
| geoname                       | Yes      | Number                | Parent spot geoname                                                                |
| location                      | Yes      | Point                 | The location of the spot                                                           |
| forecastLocation              | No       | Point                 | The offshore forecast location of the spot. Defaults to the `location`.            |
| subregion                     | Yes      | ObjectId              | The id of the spot's subregion                                                     |
| tideStation                   | Yes      | String                | the name of the spot's tide station                                                |
| cams                          | No       | Array<ObjectId>       | An array of camera object ids associated to the spot                               |
| timezone                      | Yes      | String                | Spot's timezone in string format                                                   |
| status                        | Yes      | String                | Valid options are `PUBLISHED`, `DRAFT`, `DELETED`. These values are case sensitive |
| offshoreDirection             | No       | Number                | A number between 0 and 360 representing the spot's offshore swell direction.       |
| noraBiasCorrection            | No       | Number                | Defaults to 0.                                                                     |
| swellWindow                   | No       | Array<Number, Number> |                                                                                    |
| best                          | No       | Object                | An object of best properties                                                       |
| best.windDirection            | No       | Array<Number, Number> |                                                                                    |
| best.swellPeriod              | No       | Array<Number, Number> |                                                                                    |
| best.swellWindow              | No       | Array<Number, Number> |                                                                                    |
| best.sizeRange                | No       | Array<Number, Number> |                                                                                    |
| humanReport.status            | No       | Enum                  | Valid options are `OFF`, `ON`, `AVERAGE`, `CLONE`                                  |
| humanReport.clone             | No       | ObjectId              |                                                                                    |
| humanReport.average           | No       | Object                |                                                                                    |
| humanReport.average.spotOneId | No       | ObjectId              |                                                                                    |
| humanReport.average.spotTwoId | No       | ObjectId              |                                                                                    |

## Creating a Spot

### Endpoint: `POST /admin/spots`

#### Sample Request

```http
POST /admin/spots HTTP/1.1
Host: spots-api.staging.surfline.com
Content-Type: application/json

{
    "humanReport": {
        "status": "ON",
        "average": {
            "spotOneId": null,
            "spotTwoId": null
        },
        "clone": {
            "spotId": null
        }
    },
    "legacyId": 4248,
    "legacyRegionId": 2953,
    "name": "Windansea",
    "location":  [-117.2822713851, 32.829151647032],
    "forecastLocation": [-117.286, 32.81],
    "tideStation": "La Jolla, Scripps Pier, California",
    "timezone": "America/Los_Angeles",
    "status": "PUBLISHED",
    "subregion": "58581a836630e24c4487900d",
    "geoname": 5363943
}
```

## Updating a Spot

### Endpoint: `PATCH /admin/spots/:spotId`

### Description:

This endpoint can be used to update properties on an existing spot and link it to parent taxonomies.
The body of the request has a `geoname` and a `subregion` which will be used to create the `liesIn`
array on the taxonomy for the spot.

If a taxonomy does not already exist for the given spot, one will be created with the link to the
parent geonames.

Additionally other

### Body

| Param                             | Required | Type                  | Description                                                                        |
| --------------------------------- | -------- | --------------------- | ---------------------------------------------------------------------------------- |
| name                              | no       | Number                | The name of the spot                                                               |
| legacyId                          | No       | Number                | The legacy cms id of the spot                                                      |
| tideStation                       | Yes      | String                | the name of the spot's tide station                                                |
| cams                              | No       | Array<ObjectId>       | An array of camera object ids associated to the spot                               |
| subregion                         | Yes      | String                | Parent subregionID                                                                 |
| timezone                          | No       | String                | Spot's timezone in string format                                                   |
| status                            | No       | String                | Valid options are `PUBLISHED`, `DRAFT`, `DELETED`. These values are case sensitive |
| politicalLocation                 | No       | Object                | The political location of the spot                                                 |
| politicalLocation.city            | No       | Object                |                                                                                    |
| politicalLocation.city.geonameId  | No       | Number                |                                                                                    |
| politicalLocation.city.name       | No       | String                |                                                                                    |
| politicalLocation.city.hierarchy  | No       | Object                | See `SpotModel.js`                                                                 |
| offshoreDirection                 | No       | Number                | A number between 0 and 360 representing the spot's offshore swell direction.       |
| lolaSurfBiasCoefficients          | No       | Object                |                                                                                    |
| lolaSurfBiasCoefficients.linear   | No       | Number                | Defaults to 1                                                                      |
| lolaSurfBiasCoefficients.constant | No       | Number                | Defaults to 0                                                                      |
| noraBiasCorrection                | No       | Number                | Defaults to 0.                                                                     |
| swellWindow                       | No       | Array<Number, Number> |                                                                                    |
| best                              | No       | Object                | An object of best properties                                                       |
| best.windDirection                | No       | Array<Number, Number> |                                                                                    |
| best.swellPeriod                  | No       | Array<Number, Number> |                                                                                    |
| best.swellWindow                  | No       | Array<Number, Number> |                                                                                    |
| best.sizeRange                    | No       | Array<Number, Number> |                                                                                    |
| humanReport                       | No       | Object                |                                                                                    |
| humanReport.status                | No       | Enum                  | Valid options are `OFF`, `ON`, `AVERAGE`, `CLONE`                                  |
| humanReport.clone                 | No       | ObjectId              |                                                                                    |
| humanReport.average               | No       | Object                |                                                                                    |
| humanReport.average.spotOneId     | No       | ObjectId              |                                                                                    |
| humanReport.average.spotTwoId     | No       | ObjectId              |                                                                                    |
| geoname                           | Yes      | Number                | Parent spot geoname                                                                |
| spotType                          | No       | Enum                  | Valid values are 'SURFBREAK', 'WAVEPOOL', 'SKISLOPE'                               |
| abilityLevels                     | No       | Array<Enum>           | Valid values are 'BEGINNER', 'INTERMEDIATE', 'ADVANCED', 'PRO'                     |
| boardTypes                        | No       | Enum                  | Valid values can be found in the spot model                                        |
| relivableRating                   | No       | Number                |                                                                                    |
| pointOfInterestId                 | No       | String                |                                                                                    |
| titleTag                          | No       | String                |                                                                                    |
| metaDescription                   | No       | String                |                                                                                    |
| travelDetails                     | No       | Object                | See `TravelDetailsModel.js`                                                        |
| location                          | No       | Point                 | The location of the spot                                                           |
| forecastLocation                  | No       | Point                 | The offshore forecast location of the spot. Defaults to the `location`.            |
| forecastStatus                    | No       | Object                | The forecast status for the spot.                                                  |
| forecastStatus.status             | No       | Enum                  | The forecast status for the spot. Valid options are 'active' and 'inactive'.       |
| forecastStatus.inactiveMessage    | No       | String                | A message describing the inactive forecast status for the spot.                    |

#### Sample Request

```http
POST /admin/spots/584204204e65fad6a77097be HTTP/1.1
Host: spots-api.staging.surfline.com
Content-Type: application/json

{
    "geoname": 2111704,
    "subregion": "58581a836630e24c448790fc",
    "status": "PUBLISHED",
    "forecastStatus": {
        "status": "inactive",
        "inactiveMessage": "Inactive status message test"
    }
}
```

### Sample Response

```json
{
  "politicalLocation": {
    "city": {
      "geonameId": null,
      "name": null
    },
    "hierarchy": []
  },
  "forecastStatus": {
    "status": "inactive",
    "inactiveMessage": "Inactive status message test"
  },
  "lolaSurfBiasCoefficients": {
    "linear": 1,
    "constant": 0
  },
  "best": {
    "sizeRange": [],
    "swellPeriod": [],
    "swellWindow": [],
    "windDirection": []
  },
  "humanReport": {
    "status": "OFF"
  },
  "travelDetails": {
    "abilityLevels": {
      "description": null,
      "levels": [],
      "summary": ""
    },
    "best": {
      "season": {
        "description": null,
        "value": []
      },
      "tide": {
        "description": null,
        "value": []
      },
      "size": {
        "description": null,
        "value": []
      },
      "windDirection": {
        "description": null,
        "value": []
      },
      "swellDirection": {
        "description": null,
        "value": []
      }
    },
    "bottom": {
      "description": null,
      "value": []
    },
    "crowdFactor": {
      "description": null,
      "rating": 0,
      "summary": "Mellow"
    },
    "localVibe": {
      "description": null,
      "rating": 0,
      "summary": "Welcoming"
    },
    "shoulderBurn": {
      "description": null,
      "rating": 0,
      "summary": "Light"
    },
    "spotRating": {
      "description": null,
      "rating": 0,
      "summary": "Poor"
    },
    "waterQuality": {
      "description": null,
      "rating": 0,
      "summary": "Clean"
    },
    "travelId": null,
    "access": null,
    "breakType": [],
    "description": null,
    "hazards": null,
    "relatedArticleId": null,
    "status": "DRAFT",
    "boardTypes": []
  },
  "pointOfInterestId": "e022fc58-fe30-4098-97ba-94040c201603",
  "tideStation": "Niigata, Niigata, Japan",
  "cams": [],
  "hasThumbnail": false,
  "popular": false,
  "noraBiasCorrection": 0,
  "spotType": "SURFBREAK",
  "abilityLevels": [],
  "boardTypes": [],
  "relivableRating": 0,
  "_id": "584204204e65fad6a77097be",
  "legacyId": 91742,
  "legacyRegionId": 91070,
  "name": "Kobari",
  "slug": "kobari",
  "location": {
    "type": "Point",
    "coordinates": [138.9922, 37.9047]
  },
  "forecastLocation": {
    "type": "Point",
    "coordinates": [138.992, 37.908]
  },
  "timezone": "Asia/Tokyo",
  "status": "PUBLISHED",
  "imported": {
    "spotId": 91742,
    "name": "Kobari",
    "author": "Graeme via Script",
    "publishStartDate": "2013-02-05T16:26:00.000Z",
    "publishEndDate": "2025-01-01T00:00:00.000Z",
    "status": "REVIEW",
    "regionId": "91070",
    "Additional Title": "",
    "Bottom Right Sponsor": "",
    "Cam Link": "",
    "Cam Location Advert": null,
    "Cam Logo": "",
    "Camera Alias": "",
    "Camera ID": "",
    "Children ID": null,
    "Comments": "",
    "Display Tides": "",
    "Enable Alternate Wind": null,
    "External Cams": "",
    "External Link": "",
    "Forecast Region ID": null,
    "Generate Forecast": "",
    "Geocode Lat": "",
    "Geocode Lon": "",
    "GovernorStart": null,
    "HD Camera ID": "",
    "HD Camera Link Free": "",
    "HD Camera Link Text": "",
    "HD Marketing Message": "",
    "Host HTML": "",
    "Language Title": "",
    "Lat": "37.9047",
    "Lat Forecast": "37.908",
    "Lat Forecast Wind": null,
    "Lat FT Cam": null,
    "Lat SST": "",
    "Local Info ID": "",
    "Local Knowledge ID": "",
    "Lola Model Only": "",
    "Lon": "138.9922",
    "Lon Forecast": "138.992",
    "Lon Forecast Wind": null,
    "Lon FT Cam": null,
    "Lon SST": "",
    "Map Anchor": "",
    "Menu Lat": null,
    "Menu Lon": null,
    "Mobile Ad Tag": "",
    "Mobile OAS Keyword": null,
    "Mobile OAS Page Keyword": null,
    "Mobile OAS Position": null,
    "Nearest Tide Location": "Niigata, Niigata, Japan",
    "Nearshore Model Name": "",
    "New Cam Toggle": null,
    "Offshore Direction": "",
    "Order": "",
    "Parent ID": "91466",
    "PWS": "",
    "SEO Description": "",
    "SEO Title": "",
    "Show As New": null,
    "Show In Global Nav": "",
    "SLWS ID": null,
    "Still Image": "",
    "Tide Lat": null,
    "Tide Lon": null,
    "Title": "Kobari",
    "Travel ID": "",
    "Type": "",
    "Use in Aggregate": "",
    "UseGovernor": null,
    "UseLatLonTide": null,
    "Vast Ad Apiaddress": null,
    "Vast Zone": null,
    "Wind Source": null,
    "Zoom": null
  },
  "__v": 2,
  "createdAt": "2021-10-13T20:14:04.995Z",
  "geoname": 2111704,
  "subregion": "58581a836630e24c448790fc",
  "swellWindow": [],
  "updatedAt": "2021-10-13T20:16:07.180Z",
  "thumbnail": {
    "300": "https://spot-thumbnails.staging.surfline.com/spots/default/default_300.jpg",
    "640": "https://spot-thumbnails.staging.surfline.com/spots/default/default_640.jpg",
    "1500": "https://spot-thumbnails.staging.surfline.com/spots/default/default_1500.jpg",
    "3000": "https://spot-thumbnails.staging.surfline.com/spots/default/default_3000.jpg",
    "original": "https://spot-thumbnails.staging.surfline.com/spots/default/default_original.jpg"
  },
  "id": "584204204e65fad6a77097be"
}
```
