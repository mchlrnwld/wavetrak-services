## Nearby Taxonomy

### `GET /taxonomy/nearby`

For a requested `lat`, `lon`, `type` and `fcl` and `fcode` (if `type` is `geoname`), this endpoint returns `count` number of `type`s and all geonames each place lies in, up to `maxDepth` layers up.

#### Request Parameters

A set of batch ids can be sent in the body of a `POST` to the taxonomy ids endpoint.

| param       | values     | description                                       |
| ----------- | ---------- | ------------------------------------------------- |
| `lat`       | `Number`   | Between -90 and 90                                |
| `lon`       | `Number`   | Between -180 and 180                              |
| `type`      | `String`   | geoname, spot, subregion or region                |
| `fcl`       | `String`   | P, L or A; defaults to P                          |
| `fcode`     | `String`   | Additional filtering option. Ex ADM2, ADM1, PPL   |
| `count`     | `Number`   | The number of places included in the result; defaults to 5 |
| `maxDepth`  | `Number`   | The max number of parent geonames included in the result for each place; defaults to 10 |
| `maxContainsDepth`  | `Number`   | Optional. Includes taxonomies contained by parent taxonomy. Will be found in `contains` field. If parameter not set, `contains` will not be available in response. |
| `select`     | `String`   | Optional. Comma separated fields to select.|



## Sample Request

### `GET /taxonomy/nearby?lon=-117&lat=34&type=geoname&fcl=P&fcode=PPL&count=5&maxDepth=2`

```json
{
  "data": [
    {
      "_id": "58f7eda4dadb30820bb3e116",
      "geonameId": 5364275,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.54225",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Laguna Beach",
        "countryCode": "US",
        "population": 23365,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Laguna Beach",
        "geonameId": 5364275,
        "lng": "-117.78311",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.78311,
          33.54225
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Laguna Beach",
      "name": "Laguna Beach",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "//spots-api.sandbox.surfline.com/taxonomy?id=5364275&type=geoname"
          },
          {
            "key": "www",
            "href": "//sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/laguna-beach/5364275"
          },
          {
            "key": "travel",
            "href": "//sandbox.surfline.com/travel/united-states/california/orange-county/laguna-beach-surfing-and-beaches/5364275"
          }
        ]
      }
    },
    {
      "_id": "58f7ed63dadb30820bb39cad",
      "geonameId": 5341483,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.46697",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Dana Point",
        "countryCode": "US",
        "population": 34181,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Dana Point",
        "geonameId": 5341483,
        "lng": "-117.69811",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.69811,
          33.46697
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Dana Point",
      "name": "Dana Point",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "//spots-api.sandbox.surfline.com/taxonomy?id=5341483&type=geoname"
          },
          {
            "key": "www",
            "href": "//sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/dana-point/5341483"
          },
          {
            "key": "travel",
            "href": "//sandbox.surfline.com/travel/united-states/california/orange-county/dana-point-surfing-and-beaches/5341483"
          }
        ]
      }
    },
    {
      "_id": "58f7ed62dadb30820bb39b2b",
      "geonameId": 5392148,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.61169",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "San Joaquin Hills",
        "countryCode": "US",
        "population": 3176,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "San Joaquin Hills",
        "geonameId": 5392148,
        "lng": "-117.83672",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.83672,
          33.61169
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,San Joaquin Hills",
      "name": "San Joaquin Hills",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "//spots-api.sandbox.surfline.com/taxonomy?id=5392148&type=geoname"
          },
          {
            "key": "www",
            "href": "//sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/san-joaquin-hills/5392148"
          },
          {
            "key": "travel",
            "href": "//sandbox.surfline.com/travel/united-states/california/orange-county/san-joaquin-hills-surfing-and-beaches/5392148"
          }
        ]
      }
    },
    ...
  ]
}
```
