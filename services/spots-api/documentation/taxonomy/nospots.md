## No Spots Taxonomy

### `GET /taxonomy/nospots`

Returns an array of all taxonomy objects with value `hasSpots: false` and a type not equal to `spot`.


## Sample Request

### `GET /taxonomy/nospots`

```json
{
  "data": [
    {
      "_id": "58f7ed51dadb30820bb388a4",
      "geonameId": 5341430,
      "type": "geoname",
      "liesIn": [
        "58f7ed51dadb30820bb38896"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "37.70577",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Daly City",
        "countryCode": "US",
        "population": 106562,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Daly City",
        "geonameId": 5341430,
        "lng": "-122.46192",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -122.46192,
          37.70577
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,San Mateo County,Daly City",
      "name": "Daly City",
      "category": "geonames",
      "hasSpots": false,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "//spots-api.sandbox.surfline.com/taxonomy?id=5341430&type=geoname"
          },
          {
            "key": "www",
            "href": "//sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/san-mateo-county/daly-city/5341430"
          },
          {
            "key": "travel",
            "href": "//sandbox.surfline.com/travel/united-states/california/san-mateo-county/daly-city-surfing-and-beaches/5341430"
          }
        ]
      }
    },
    {
      "_id": "58f7ed59dadb30820bb39233",
      "geonameId": 5334619,
      "type": "geoname",
      "liesIn": [
        "58f7ed58dadb30820bb38f8b"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "34.18362",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Casa Conejo",
        "countryCode": "US",
        "population": 3249,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Casa Conejo",
        "geonameId": 5334619,
        "lng": "-118.94343",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -118.94343,
          34.18362
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Ventura County,Casa Conejo",
      "name": "Casa Conejo",
      "category": "geonames",
      "hasSpots": false,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "//spots-api.sandbox.surfline.com/taxonomy?id=5334619&type=geoname"
          },
          {
            "key": "www",
            "href": "//sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/ventura-county/casa-conejo/5334619"
          },
          {
            "key": "travel",
            "href": "//sandbox.surfline.com/travel/united-states/california/ventura-county/casa-conejo-surfing-and-beaches/5334619"
          }
        ]
      }
    },
    ...
  ]
}
```
