## Taxonomy Management

### `POST /admin/taxonomy`

Add a new Geoname taxonomy node.

#### Request Body

| Property    | Type              | Description                                    | Required |
| ----------- | ----------------- | ---------------------------------------------- | -------- |
| `category`  | `String`          | `geonames` is the only allowed value currently | Yes      |
| `geonameId` | `Number`          | Geoname ID to look up properties from Geonames | Yes      |
| `liesIn`    | `Array<ObjectID>` | A list of parent node ObjectIDs                | Yes      |

#### Sample Request Body

```JSON
{
  "geonameId": 5358705,
  "liesIn": ["58f7ed5ddadb30820bb39651"],
  "name": "Huntington Beach",
  "category": "geonames"
}
```

#### Sample Response Body

```JSON
{
  "_id": "58f7ed5fdadb30820bb3987c",
  "geonameId": 5358705,
  "name": "Huntington Beach",
  "category": "geonames",
  "enumeratedPath": ",Earth,North America,United States,California,Orange County,Huntington Beach",
  "geonames": {
    "fcode": "PPL",
    "lat": "33.6603",
    "adminName1": "California",
    "fcodeName": "populated place",
    "countryName": "United States",
    "fclName": "city, village,...",
    "name": "Huntington Beach",
    "countryCode": "US",
    "population": 201899,
    "fcl": "P",
    "countryId": "6252001",
    "toponymName": "Huntington Beach",
    "geonameId": 5358705,
    "lng": "-117.99923",
    "adminCode1": "CA"
  },
  "location": {
    "coordinates": [-117.99923, 33.6603],
    "type": "Point"
  },
  "type": "geoname",
  "hasSpots": false,
  "liesIn": ["58f7ed5ddadb30820bb39651"]
}
```

### `PATCH /admin/taxonomy`

Update Geoname taxonomy.

#### Path Parameters

| Name         | Type       | Description                   | Required |
| ------------ | ---------- | ----------------------------- | -------- |
| `taxonomyId` | `ObjectID` | Taxonomy-to-update's ObjectID | Yes      |

#### Request Body

| Property    | Type              | Description                                    | Required |
| ----------- | ----------------- | ---------------------------------------------- | -------- |
| `category`  | `String`          | `geonames` is the only allowed value currently | Yes      |
| `geonameId` | `Number`          | Geoname ID to look up properties from Geonames | Yes      |
| `liesIn`    | `Array<ObjectID>` | A list of parent node ObjectIDs                | Yes      |

#### Sample Request
`PATCH /admin/taxonomy/58f7edaadadb30820bb3e72c`

```JSON
{
  "name": "Huntington Beach",
  "liesIn": ["58f7ed5ddadb30820bb39651"]
}
```

#### Sample Response Body

```JSON
{
  "_id": "58f7edaadadb30820bb3e72c",
  "name": "Huntington Beach",
  "category": "geonames",
  "type": "geoname",
  "liesIn": ["58f7ed5ddadb30820bb39651"],
  "hasSpots": false
}
```
