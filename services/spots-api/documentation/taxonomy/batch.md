## Batch Taxonomy by Spots

### `POST /taxonomy/ids`

This endpoint returns a set of taxonomy ids for a set of `region`, `subregion`, `spot` or `geoname` ids.

#### Request Parameters

A set of batch ids can be sent in the body of a `POST` to the taxonomy ids endpoint.

| param       | values            | description               |
| ----------- | ----------------- | ------------------------- |
| `spot`      | `Array<ObjectId>` | An array spot ids         |
| `subregion` | `Array<ObjectId>` | An array of subregion ids |
| `region`    | `Array<ObjectId>` | An array of region ids    |
| `geonames`  | `Array<Number>`   | An array of geoname ids   |

#### Sample Response Body

```json
{
  "taxonomy": [
    {
      "_id": "58e95b7786fd4b596bc72767",
      "name": "South Orange County",
      "type": "subregion",
      "subregion": "58581a836630e24c4487900a"
    },
    {
      "_id": "58e5c41986fd4b596b896481",
      "name": "United States",
      "type": "geoname",
      "geonameId": 6252001
    },
    {
      "_id": "58e5c41986fd4b596b896473",
      "name": "Earth",
      "type": "geoname",
      "geonameId": 6295630
    },
    {
      "_id": "58ebd6cd86fd4b596be595d5",
      "name": "Pacific Northwest",
      "type": "region",
      "region": "58dee4f9871eb600127a0a7f"
    },
    {
      "_id": "58e9697386fd4b596bc8fecf",
      "name": "Salt Creek",
      "type": "spot",
      "spot": "5842041f4e65fad6a770882e"
    },
    {
      "_id": "58e95ab686fd4b596bc71919",
      "name": "Huntington Beach",
      "type": "geoname",
      "geonameId": 5358705
    },
    {
      "_id": "58e5c41986fd4b596b89647a",
      "name": "North America",
      "type": "geoname",
      "geonameId": 6255149
    },
    {
      "_id": "58e5c41986fd4b596b8964ac",
      "name": "Southern California",
      "type": "region",
      "region": "58dbfe044bfa680012082307"
    },
    {
      "_id": "58e95ab686fd4b596bc71911",
      "name": "Orange County",
      "type": "geoname",
      "geonameId": 5379524
    },
    {
      "_id": "58e9693e86fd4b596bc8f76b",
      "name": "Dana Point",
      "type": "geoname",
      "geonameId": 5341483
    },
    {
      "_id": "58e5c41986fd4b596b896488",
      "name": "California",
      "type": "geoname",
      "geonameId": 5332921
    },
    {
      "_id": "58e9692586fd4b596bc8f3e4",
      "name": "HB Pier, Northside",
      "type": "spot",
      "spot": "5842041f4e65fad6a7708827"
    },
    {
      "_id": "58e95ab686fd4b596bc7192b",
      "name": "North Orange County",
      "type": "subregion",
      "subregion": "58581a836630e24c44878fd6"
    }
  ]
}
```
