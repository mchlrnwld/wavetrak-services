# Taxonomy API

The Taxonomy API provides primitives for querying relationships between geoname places, spots, subregions and regions.

**Base URL**: `/taxonomy`

- [Taxonomy management](taxonomy/admin.md)
- [Batch lookup](taxonomy/batch.md)
- [Nearby taxonomy](taxonomy/nearby.md)
- [Taxonomy with no spots](taxonomy/nospots.md)

## Query Params

| param | values                                 | description                              |
| ----- | -------------------------------------- | ---------------------------------------- |
| type  | See "Types" for a list of valid types. | Taxonomy type to query.                  |
| id    | `Number`, `ObjectId`                   | The id of the taxonomy item. May reference any valid type. |

## Types

The taxonomy api takes different types of nodes that are denormalized from various data sources. These node types are denoted by the `type: String` property on a `Taxonomy` document.

The taxonomy api manages the following taxonomy types and corresponding properties:

* `type: subregion`
  * `subregion: ObjectId` a reference to a subregion document in the `Subregions` collection.
* `type: region`
  * `region: ObjectId` a reference to a region document in the `Regions` collection.
* `type: spot`
  * `spot: ObjectId` a reference to a spot document in the `Spots` collection.
  * `location: GeoJSONPoint` the location of the spot on a 2dsphere.
* `type: geoname`
  * `geonameId: Number` a valid geoname id number.
  * `geonames: Object` a mixed shape of geoname properties. See http://download.geonames.org/export/dump/readme.txt for a list of valid properties.
  * `enumeratedPath: String`  a comma separated list of geoname ancestors. Eg ``,Earth,North America,US`. The values for each ancestor are determined based of the ancestor's geoname `fcode`. This string can be used to generate breadcrumbs for a geoname node.
  * `location: GeoJSONPoint` the location of the geoname place on a 2dsphere.

All types return the following properties

* `name: String`
* `liesIn: Array<ObjectId>` (nullable)
* `associated.links: Array<Object>`
  * `category: Enum<String>` the category that the node is in. There are currently two categories, `surfline` and `geonames`. This is to make it easy to build different hierarchies.

## Conventions

### `in` and `contains`

Every request to the taxonomy api will return an `in` and `contains` array. These are used to bucket nodes based on their relation to `id` passed in the query param. Nodes that appear in the `in` array are parents of the base node. Nodes that appear in the `contains` property are children of the base node.

### Links

Any node within the taxonmy may contain a set of links that represent node resource. A link object has the following properties.

* `href` a link to the keyed resource. (should this include the host?)
* `key` a key representing the resource. Eg `api` or `www`.

#### Key Values

| Key   | Type                          | Description                              |
| ----- | ----------------------------- | ---------------------------------------- |
| `www` | `spot`, `geoname`             | The default link for the resource. This should be used as the Object's source of truth as it's surfaced on the product. Search results should use this key to build links on the product. |
| `travel` | `geoname`             | The default link for the travel page resource. This should be used as the Object's source of truth as it's surfaced on the product. Search results should use this key to build links on the product. |
| `api` | `subregion`, `region`, `spot` | Object's default API resource link. This link can be used to get more information for the node type. |



### Depth

Every node in the `contains` and `in` arrays will return a `depth` property. The `depth` property denotes the number of edges from the root node. Nodes are sorted by `depth`.

## Sample Requests

### `GET /taxonomy?id=6295630&type=geoname`

Returns a hierarchy for Earth.

```json
{
  "_id": "58e4330986fd4b596b71cbd8",
  "geonameId": 6295630,
  "type": "geoname",
  "geonames": {
    "population": 6814400000,
    "fcode": "AREA",
    "fcl": "L",
    "lat": "0",
    "adminName1": "",
    "fcodeName": "area",
    "toponymName": "Earth",
    "fclName": "parks,area, ...",
    "name": "Earth",
    "geonameId": 6295630,
    "lng": "0"
  },
  "location": {
    "coordinates": [
      0,
      0
    ],
    "type": "Point"
  },
  "name": "Earth",
  "category": "geonames",
  "enumeratedPath": ",Earth",
  "associated": {
    "links": [
      null
    ]
  },
  "in": [],
  "contains": [
    {
      "_id": "58e4330a86fd4b596b71cc04",
      "spot": "5842041f4e65fad6a770883c",
      "type": "spot",
      "liesIn": [
        "58e4330a86fd4b596b71cbfd",
        "58e4330a86fd4b596b71cc14"
      ],
      "location": {
        "coordinates": [
          -117.2822713851,
          32.829151647032
        ],
        "type": "Point"
      },
      "name": "Windansea",
      "category": "surfline",
      "depth": 5,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-report/windansea/5842041f4e65fad6a770883c"
          },
          {
            "key": "api",
            "href": "/api/spots/5842041f4e65fad6a770883c"
          }
        ]
      }
    },
    {
      "_id": "58e4330a86fd4b596b71cbfd",
      "geonameId": 5363943,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbf5"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "32.84727",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "La Jolla",
        "countryCode": "US",
        "population": 42808,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "La Jolla",
        "geonameId": 5363943,
        "lng": "-117.2742",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.2742,
          32.84727
        ],
        "type": "Point"
      },
      "name": "La Jolla",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US,CA,San Diego,La Jolla",
      "depth": 4,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/ca/san-diego/la-jolla/5363943"
          },
          {
            "key": "travel",
            "href": "/travel/us/ca/san-diego/la-jolla-surfing-and-beaches/5363943"
          }
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbe6",
      "geonameId": 6252001,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbdf"
      ],
      "geonames": {
        "fcode": "PCLI",
        "lat": "39.76",
        "adminName1": "",
        "fcodeName": "independent political entity",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "United States",
        "countryCode": "US",
        "population": 310232863,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "United States",
        "geonameId": 6252001,
        "lng": "-98.5",
        "adminCode1": "00"
      },
      "location": {
        "coordinates": [
          -98.5,
          39.76
        ],
        "type": "Point"
      },
      "name": "United States",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US",
      "depth": 1,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/6252001"
          },
          {
            "key": "travel",
            "href": "/travel/us-surfing-and-beaches/6252001"
          }
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbdf",
      "geonameId": 6255149,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbd8"
      ],
      "geonames": {
        "population": 0,
        "fcode": "CONT",
        "fcl": "L",
        "lat": "46.07323",
        "adminName1": "",
        "fcodeName": "continent",
        "toponymName": "North America",
        "fclName": "parks,area, ...",
        "name": "North America",
        "geonameId": 6255149,
        "lng": "-100.54688"
      },
      "location": {
        "coordinates": [
          -100.54688,
          46.07323
        ],
        "type": "Point"
      },
      "name": "North America",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America",
      "depth": 0,
      "associated": {
        "links": [
          null
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbee",
      "geonameId": 5332921,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbe6"
      ],
      "geonames": {
        "fcode": "ADM1",
        "lat": "37.25022",
        "adminName1": "California",
        "fcodeName": "first-order administrative division",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "California",
        "countryCode": "US",
        "population": 37691912,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "California",
        "geonameId": 5332921,
        "lng": "-119.75126",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -119.75126,
          37.25022
        ],
        "type": "Point"
      },
      "name": "California",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US,CA",
      "depth": 2,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/ca/5332921"
          },
          {
            "key": "travel",
            "href": "/travel/us/ca-surfing-and-beaches/5332921"
          }
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbf5",
      "geonameId": 5391832,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbee"
      ],
      "geonames": {
        "fcode": "ADM2",
        "lat": "33.0282",
        "adminName1": "California",
        "fcodeName": "second-order administrative division",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "San Diego",
        "countryCode": "US",
        "population": 3095313,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "San Diego County",
        "geonameId": 5391832,
        "lng": "-116.77021",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -116.77021,
          33.0282
        ],
        "type": "Point"
      },
      "name": "San Diego",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US,CA,San Diego",
      "depth": 3,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/ca/san-diego/5391832"
          },
          {
            "key": "travel",
            "href": "/travel/us/ca/san-diego-surfing-and-beaches/5391832"
          }
        ]
      }
    }
  ]
}
```

### `GET /taxonomy?id=5842041f4e65fad6a770883c&type=spot`

Returns the taxonomy for Windansea.

```json
{
  "_id": "58e4330a86fd4b596b71cc04",
  "spot": "5842041f4e65fad6a770883c",
  "type": "spot",
  "liesIn": [
    "58e4330a86fd4b596b71cbfd",
    "58e4330a86fd4b596b71cc14"
  ],
  "location": {
    "coordinates": [
      -117.2822713851,
      32.829151647032
    ],
    "type": "Point"
  },
  "name": "Windansea",
  "category": "surfline",
  "associated": {
    "links": [
      {
        "key": "www",
        "href": "/surf-report/windansea/5842041f4e65fad6a770883c"
      },
      {
        "key": "api",
        "href": "/api/spots/5842041f4e65fad6a770883c"
      }
    ]
  },
  "in": [
    {
      "_id": "58e4330986fd4b596b71cbd8",
      "geonameId": 6295630,
      "type": "geoname",
      "geonames": {
        "population": 6814400000,
        "fcode": "AREA",
        "fcl": "L",
        "lat": "0",
        "adminName1": "",
        "fcodeName": "area",
        "toponymName": "Earth",
        "fclName": "parks,area, ...",
        "name": "Earth",
        "geonameId": 6295630,
        "lng": "0"
      },
      "location": {
        "coordinates": [
          0,
          0
        ],
        "type": "Point"
      },
      "name": "Earth",
      "category": "geonames",
      "enumeratedPath": ",Earth",
      "depth": 6,
      "associated": {
        "links": [
          null
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbdf",
      "geonameId": 6255149,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbd8"
      ],
      "geonames": {
        "population": 0,
        "fcode": "CONT",
        "fcl": "L",
        "lat": "46.07323",
        "adminName1": "",
        "fcodeName": "continent",
        "toponymName": "North America",
        "fclName": "parks,area, ...",
        "name": "North America",
        "geonameId": 6255149,
        "lng": "-100.54688"
      },
      "location": {
        "coordinates": [
          -100.54688,
          46.07323
        ],
        "type": "Point"
      },
      "name": "North America",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America",
      "depth": 5,
      "associated": {
        "links": [
          null
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbee",
      "geonameId": 5332921,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbe6"
      ],
      "geonames": {
        "fcode": "ADM1",
        "lat": "37.25022",
        "adminName1": "California",
        "fcodeName": "first-order administrative division",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "California",
        "countryCode": "US",
        "population": 37691912,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "California",
        "geonameId": 5332921,
        "lng": "-119.75126",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -119.75126,
          37.25022
        ],
        "type": "Point"
      },
      "name": "California",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US,CA",
      "depth": 3,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/ca/5332921"
          },
          {
            "key": "travel",
            "href": "/travel/us/ca-surfing-and-beaches/5332921"
          }
        ]
      }
    },
    {
      "_id": "58e4330a86fd4b596b71cbfd",
      "geonameId": 5363943,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbf5"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "32.84727",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "La Jolla",
        "countryCode": "US",
        "population": 42808,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "La Jolla",
        "geonameId": 5363943,
        "lng": "-117.2742",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.2742,
          32.84727
        ],
        "type": "Point"
      },
      "name": "La Jolla",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US,CA,San Diego,La Jolla",
      "depth": 1,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/ca/san-diego/la-jolla/5363943"
          },
          {
            "key": "travel",
            "href": "/travel/us/ca/san-diego/la-jolla-surfing-and-beaches/5363943"
          }
        ]
      }
    },
    {
      "_id": "58e4330a86fd4b596b71cc0d",
      "region": "58dbfe044bfa680012082307",
      "type": "region",
      "name": "Southern California",
      "category": "surfline",
      "depth": 2,
      "associated": {
        "links": [
          {
            "key": "api",
            "href": "/api/regions/58dbfe044bfa680012082307"
          }
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbf5",
      "geonameId": 5391832,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbee"
      ],
      "geonames": {
        "fcode": "ADM2",
        "lat": "33.0282",
        "adminName1": "California",
        "fcodeName": "second-order administrative division",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "San Diego",
        "countryCode": "US",
        "population": 3095313,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "San Diego County",
        "geonameId": 5391832,
        "lng": "-116.77021",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -116.77021,
          33.0282
        ],
        "type": "Point"
      },
      "name": "San Diego",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US,CA,San Diego",
      "depth": 2,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/ca/san-diego/5391832"
          },
          {
            "key": "travel",
            "href": "/travel/us/ca/san-diego-surfing-and-beaches/5391832"
          }
        ]
      }
    },
    {
      "_id": "58e4330986fd4b596b71cbe6",
      "geonameId": 6252001,
      "type": "geoname",
      "liesIn": [
        "58e4330986fd4b596b71cbdf"
      ],
      "geonames": {
        "fcode": "PCLI",
        "lat": "39.76",
        "adminName1": "",
        "fcodeName": "independent political entity",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "United States",
        "countryCode": "US",
        "population": 310232863,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "United States",
        "geonameId": 6252001,
        "lng": "-98.5",
        "adminCode1": "00"
      },
      "location": {
        "coordinates": [
          -98.5,
          39.76
        ],
        "type": "Point"
      },
      "name": "United States",
      "category": "geonames",
      "enumeratedPath": ",Earth,North America,US",
      "depth": 4,
      "associated": {
        "links": [
          {
            "key": "www",
            "href": "/surf-reports-forecasts-cams/us/6252001"
          },
          {
            "key": "travel",
            "href": "/travel/us-surfing-and-beaches/6252001"
          }
        ]
      }
    },
    {
      "_id": "58e4330a86fd4b596b71cc14",
      "subregion": "58581a836630e24c4487900d",
      "type": "subregion",
      "liesIn": [
        "58e4330a86fd4b596b71cc0d"
      ],
      "name": "South San Diego",
      "category": "surfline",
      "depth": 1,
      "associated": {
        "links": [
          {
            "key": "api",
            "href": "/api/subregions/58581a836630e24c4487900d"
          }
        ]
      }
    }
  ],
  "contains": []
}
```

## Custom Hierarchy

A spot's taxonomy hierarchy is determined by the geonames API, however, we support injecting a geoname into the hierarchy. The bottom of the hierarchy is determined by `spot.geoname`. `taxonomy/customHierarchy.js` maps a given geoname to a desired geoname.

`insertIsland` injects the desired geoname under its immediate parent into `spot.geoname`'s hierarchy. If the desired geoname's hierarchy does not converge with the spot, it will injected it at the end of the hierarchy.

## Notes

Build a hierarchy for a spot (w/ JSX)

```javascript
// fetch taxonomy?id=5842041f4e65fad6a770883c&type=spot
taxonomy.in
	.filter(node => node.type === 'geoname')
	.map(node => {
  		const www = node.links.find(link => link.key === 'www')
        return www ? { name: node.name, ...www } : null;
	})
	.reduce((p, www) => <a href={www.href}>{www.name}</a>, [])
	.join(' / ');
```

Nodes are already sorted by `depth` so there is no need to run a sort on the response body.
