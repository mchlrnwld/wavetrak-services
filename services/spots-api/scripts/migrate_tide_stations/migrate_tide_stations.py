import os
import csv

from pymongo import MongoClient
from bson.objectid import ObjectId

MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
MONGO_USER = os.environ['MONGO_USER']
MONGO_CONNECTION_STRING = os.environ['MONGO_CONNECTION_STRING']
MONGO_DATABASE = os.environ['MONGO_DATABASE']
TIDE_CSV = os.environ['TIDE_CSV']


def main():
    with open(TIDE_CSV) as f:
        d_reader = csv.DictReader(f)
        tide_stations = [
            (line['_id'], line['tideStation']) for line in d_reader
        ]

    connection_str = f'mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CONNECTION_STRING}'
    client = MongoClient(connection_str)
    spots_collection = client[MONGO_DATABASE].Spots

    spots_updated, spots_not_updated, spots_failed_to_update = ([], [], [])
    for tide_station in tide_stations:
        print(tide_station)
        result = spots_collection.update_one(
            {'_id': ObjectId(tide_station[0])},
            {"$set": {"tideStation": tide_station[1]}},
            upsert=False,
        )
        if result.modified_count == 1 and result.matched_count == 1:
            spots_updated.append(tide_station)
            print(f'Update succeeded for Spot with id: {tide_station[0]}')
        elif result.matched_count == 1:
            spots_not_updated.append(tide_station)
            print(f'Spot with id: {tide_station[0]} not updated')
        else:
            spots_failed_to_update.append(tide_station)
            print(f'No matching Spot with id: {tide_station[0]}')

    print(f'{len(spots_updated)} spots updated out of {len(tide_stations)}')
    print(
        f'{len(spots_not_updated)} spots were not updated out of {len(tide_stations)}'
    )
    print(
        f'{len(spots_failed_to_update)} spots did not have a matching id out of {len(tide_stations)}'
    )


if __name__ == '__main__':
    main()
