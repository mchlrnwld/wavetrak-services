# Migrate Tide Stations to the Shared Tide Service

Update the tide station field in the Spots Collection with stations from the shared tide service.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate migrate-tide-stations
$ cp .env.sample .env
```

## Running

1. Edit your `.env` file with the mongodb connection information and postgres connection string for the desired environment.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env) python migrate_tide_stations.py
    ```
