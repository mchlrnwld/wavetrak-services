#!/bin/bash

# This script should take care of the custom renaming, or even tweaking, on any
# of the resources that should be named differently after the module migration.
# You should safely remove this file if all migrations on all environments were
# already applied.

# Add module name to resources without/outside a module.
# Exclude resources with the types listed.
module="module.spots-api-service"
subject="aws_iam_role_policy_attachment"

# This script will output the content of the .tfstate file.
if [ "$tfstate" != "" ]; then
  cat $tfstate | \
  jq \
    --arg subj $subject \
    --arg name $module \
    '.resources=[.resources[] | if (.module == null and .type == $subj) then (.module = $name) else (.) end]' \
  > $tfstate.tmp
  mv $tfstate.tmp $tfstate
fi

# This script will update the state file renaming the necessary resources.
if [ "$tfplan" != "" ]; then
  cat $tfplan | \
  jq -r \
    --arg subj $subject \
    '.resources[] | select(.module == null) | select(.type == $subj) | select(.mode != "data") | (.type + "." + .name)' | \
  awk \
    -v module=$module \
    '{ print $1,module"."$1 }' | \
  xargs -r -n 2 \
    terraform state mv
fi
