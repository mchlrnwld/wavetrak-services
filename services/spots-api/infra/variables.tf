variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "default_vpc" {
}

variable "ecs_cluster" {
}

variable "alb_listener_arn_http" {
}

output "target_group" {
  value = module.spots_api_service.target_group
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_response_latency"
}

variable "auto_scaling_min_size" {
  default = 1
}

variable "auto_scaling_max_size" {
  default = 1
}

variable "auto_scaling_up_metric" {
  default = 10
}

variable "auto_scaling_down_metric" {
  default = 1
}

variable "auto_scaling_up_count" {
  default = 1
}

variable "auto_scaling_down_count" {
  default = -1
}

variable "auto_scaling_target_value" {
  default = ""
}

variable "task_deregistration_delay" {
  default = 10
}

variable "dns_internal_name" {
  default = ""
}

variable "dns_internal_zone_id" {
  default = ""
}

variable "spots_api_image_service_execute" {
}

variable "spots_api_spot_thumbnail" {
}

variable "sns_spot_details_updated" {
}

variable "sns_camera_details_updated" {
  type        = string
  description = "Camera details updated SNS topic name. The service will subscribe to this topic via HTTPS."
}

variable "platform_host" {
  type        = string
  description = "Platform host name for SNS topic HTTPS subscription."
}
