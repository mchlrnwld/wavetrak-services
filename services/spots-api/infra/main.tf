module "spots_api_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-${var.application}"
  service_td_container_name   = var.application
  service_td_count            = var.service_td_count
  service_port                = 8080
  service_alb_priority        = 100

  ecs_cluster = var.ecs_cluster
  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id

  alb_listener      = var.alb_listener_arn_http
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_up_metric      = var.auto_scaling_up_metric
  auto_scaling_down_metric    = var.auto_scaling_down_metric
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
  auto_scaling_up_count       = var.auto_scaling_up_count
  auto_scaling_down_count     = var.auto_scaling_down_count

  task_deregistration_delay = var.task_deregistration_delay
}

data "aws_sns_topic" "camera_details_updated" {
  name = var.sns_camera_details_updated
}

module "camera_details_updated_dead_letter_queue" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs"

  application = var.application
  environment = var.environment
  queue_name  = "${var.company}-${var.application}-dead-letter-queue"
  company     = var.company
}

locals {
  dead_letter_target_arn = jsonencode({
    deadLetterTargetArn = module.camera_details_updated_dead_letter_queue.queue_arn
  })
}

resource "aws_iam_role_policy_attachment" "spots_api_image_service_execute" {
  role       = "spots_api_task_role_${var.environment}"
  policy_arn = var.spots_api_image_service_execute
}

resource "aws_iam_role_policy_attachment" "spots_api_spot_thumbnail" {
  role       = "spots_api_task_role_${var.environment}"
  policy_arn = var.spots_api_spot_thumbnail
}

resource "aws_iam_role_policy_attachment" "spots_api_sns_spot_details_updated" {
  role       = "spots_api_task_role_${var.environment}"
  policy_arn = var.sns_spot_details_updated
}

resource "aws_sns_topic_subscription" "camera_details_updated" {
  topic_arn              = data.aws_sns_topic.camera_details_updated.arn
  protocol               = "https"
  endpoint               = "https://${var.platform_host}/spots/sns/camera-details-updated"
  endpoint_auto_confirms = true
  delivery_policy        = file("${path.module}/resources/sns-delivery-policy.json")
}

# NOTE: Temporary workaround since the aws_sns_topic_subscription
# resource does not support dead letter queues. The null resource below
# executes the AWS cli to attach a RedrivePolicy to the sns subscription resource.
# This is based on the same configuration as science-data-service
#
# The RedrivePolicy tells the SNS subscription to send failed messages to the
# dead letter queue. There is an open PR that implements dead letter queue support for
# the sns subscription resource. Review is pending by the Hashicorp Team:
# https://github.com/terraform-providers/terraform-provider-aws/issues/10931
resource "null_resource" "camera_details_updated_redrive_policy" {
  provisioner "local-exec" {
    command = <<EOF
      aws sns set-subscription-attributes \
      --subscription-arn ${aws_sns_topic_subscription.camera_details_updated.arn} \
      --attribute-name RedrivePolicy \
      --attribute-value '${local.dead_letter_target_arn}'
    EOF
  }

  triggers = {
    dlq_arn       = module.camera_details_updated_dead_letter_queue.queue_arn
    sns_topic_arn = aws_sns_topic_subscription.camera_details_updated.arn
  }

  depends_on = [
    aws_sns_topic_subscription.camera_details_updated,
    module.camera_details_updated_dead_letter_queue,
  ]
}

resource "aws_sqs_queue_policy" "sqs_read_policy" {
  queue_url = module.camera_details_updated_dead_letter_queue.queue_url
  policy = templatefile("${path.module}/resources/sqs-queue-policy.json", {
    dead_letter_queue      = module.camera_details_updated_dead_letter_queue.queue_arn,
    camera_details_updated = data.aws_sns_topic.camera_details_updated.arn
  })
}
