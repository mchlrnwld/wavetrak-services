provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/staging/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/staging/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/spots-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  alb_listener_name               = "sl-int-core-srvs-5-staging"
  default_vpc                     = "vpc-981887fd"
  dns_internal_name               = "internal-core-svc.staging.surfline.com"
  dns_internal_zone_id            = "Z3JHKQ8ELQG5UE"
  dns_name                        = "spots-api.staging.surfline.com"
  dns_zone_id                     = "Z3JHKQ8ELQG5UE"
  ecs_cluster                     = "sl-core-svc-staging"
  iam_role_arn                    = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  alb_listener_arn_http           = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-5-staging/1d549ffa5b6154ba/ac0aacf66663a7b1"
  sns_camera_details_updated      = "camera_details_updated_staging"
  sns_spot_details_updated        = "arn:aws:iam::665294954271:policy/sns_policy_spot_details_updated_staging"
  spots_api_image_service_execute = "arn:aws:iam::665294954271:policy/image-service-execute-staging"
  spots_api_spot_thumbnail        = "arn:aws:iam::665294954271:policy/spot-thumbnail-policy-staging"
  platform_host                   = "platform.staging.surfline.com"
}

data "aws_alb" "main_internal" {
  name = local.alb_listener_name
}

module "spots-api-service" {
  source = "../../"

  company     = "sl"
  application = "spots-api"
  environment = "staging"

  ecs_cluster          = local.ecs_cluster
  default_vpc          = local.default_vpc
  dns_name             = local.dns_name
  dns_zone_id          = local.dns_zone_id
  dns_internal_name    = local.dns_internal_name
  dns_internal_zone_id = local.dns_internal_zone_id

  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/spots/*"
    },
    {
      field = "path-pattern"
      value = "/admin/subregions/*"
    },
    {
      field = "path-pattern"
      value = "/admin/regions/*"
    },
    {
      field = "path-pattern"
      value = "/taxonomy*"
    },
    {
      field = "path-pattern"
      value = "/admin/taxonomy/*"
    },
    {
      field = "path-pattern"
      value = "/admin/geonames/*"
    },
  ]

  iam_role_arn          = local.iam_role_arn
  load_balancer_arn     = data.aws_alb.main_internal.arn
  alb_listener_arn_http = local.alb_listener_arn_http

  sns_camera_details_updated      = local.sns_camera_details_updated
  sns_spot_details_updated        = local.sns_spot_details_updated
  spots_api_image_service_execute = local.spots_api_image_service_execute
  spots_api_spot_thumbnail        = local.spots_api_spot_thumbnail
  platform_host                   = local.platform_host
}

resource "aws_route53_record" "main_internal" {
  zone_id = local.dns_internal_zone_id
  name    = local.dns_internal_name
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
