provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/spots-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  alb_listener_name               = "sl-int-core-srvs-5-prod"
  default_vpc                     = "vpc-116fdb74"
  dns_internal_name               = "internal-core-svc.surfline.com"
  dns_internal_zone_id            = "ZY7MYOQ65TY5X"
  dns_name                        = "spots-api.prod.surfline.com"
  dns_zone_id                     = "Z3LLOZIY0ZZQDE"
  ecs_cluster                     = "sl-core-svc-prod"
  iam_role_arn                    = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  alb_listener_arn_http           = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-5-prod/defea8a6368ab2a7/8757f09b6eb20222"
  sns_camera_details_updated      = "camera_details_updated_prod"
  sns_spot_details_updated        = "arn:aws:iam::833713747344:policy/sns_policy_spot_details_updated_prod"
  spots_api_image_service_execute = "arn:aws:iam::833713747344:policy/image-service-execute-prod"
  spots_api_spot_thumbnail        = "arn:aws:iam::833713747344:policy/spot-thumbnail-policy-prod"
  platform_host                   = "platform.surfline.com"
}

data "aws_alb" "main_internal" {
  name = local.alb_listener_name
}

module "spots-api-service" {
  source = "../../"

  company     = "sl"
  application = "spots-api"
  environment = "prod"

  ecs_cluster          = local.ecs_cluster
  default_vpc          = local.default_vpc
  dns_name             = local.dns_name
  dns_zone_id          = local.dns_zone_id
  dns_internal_name    = local.dns_internal_name
  dns_internal_zone_id = local.dns_internal_zone_id

  service_td_count = 14
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/spots/*"
    },
    {
      field = "path-pattern"
      value = "/admin/subregions/*"
    },
    {
      field = "path-pattern"
      value = "/admin/regions/*"
    },
    {
      field = "path-pattern"
      value = "/taxonomy*"
    },
    {
      field = "path-pattern"
      value = "/admin/taxonomy/*"
    },
    {
      field = "path-pattern"
      value = "/admin/geonames/*"
    },
  ]

  iam_role_arn          = local.iam_role_arn
  load_balancer_arn     = data.aws_alb.main_internal.arn
  alb_listener_arn_http = local.alb_listener_arn_http

  sns_camera_details_updated      = local.sns_camera_details_updated
  sns_spot_details_updated        = local.sns_spot_details_updated
  spots_api_image_service_execute = local.spots_api_image_service_execute
  spots_api_spot_thumbnail        = local.spots_api_spot_thumbnail
  platform_host                   = local.platform_host

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 3500
  auto_scaling_min_size     = 6
  auto_scaling_max_size     = 5000

  task_deregistration_delay = 60
}

resource "aws_route53_record" "main_internal" {
  zone_id = local.dns_internal_zone_id
  name    = local.dns_internal_name
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "spots_prod" {
  zone_id = local.dns_internal_zone_id
  name    = "spots-api.surfline.com"
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
