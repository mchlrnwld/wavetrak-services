# Spots API Microservice

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [`GET /spots/`](#get-spots)
  - [Parameters](#parameters)
  - [Sample Response](#sample-response)
- [`GET /spots/:spotId`](#get-spotsspotid)
  - [Parameters](#parameters-1)
  - [Sample Response](#sample-response-1)
- [`GET /admin/subregions/:subregionId`](#get-adminsubregionssubregionid)
  - [Parameters](#parameters-2)
  - [Sample Response](#sample-response-2)
- [`PUT /admin/subregions/:subregionId`](#put-adminsubregionssubregionid)
  - [Parameters](#parameters-3)
  - [Request Body](#request-body)
  - [Sample Request Body](#sample-request-body)
  - [Sample Request](#sample-request)
  - [Sample Response](#sample-response-3)
- [`PATCH /admin/subregions/:subregionId`](#patch-adminsubregionssubregionid)
  - [Parameters](#parameters-4)
  - [Request Body](#request-body-1)
  - [Sample Request](#sample-request-1)
  - [Sample Response](#sample-response-4)
- [POST /admin/spots/:spotId/upload](#post-adminspotsspotidupload)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

- [Taxonomy API](documentation/taxonomy.md)

## `GET /spots/`

### Parameters

| Parameter | Required | Description                                                                                                                        |
| --------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| `search`  | No       | If no search parameter is provided, all spots are returned                                                                         |
| `limit`   | No       | How many results to limit                                                                                                          |
| `status`  | No       | Comma separated statues to select. Eg `status=publish,draft`. Default: status != deleted. `status` supercedes the `search` option. |
| `select`  | No       | Comma separated fields to select. Eg `select=legacyRegionId,name`.                                                                 |

### Sample Response

```text
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
```

```json
[
  {
    "_id": "5842041f4e65fad6a7708864",
    "legacyId": 4407,
    "legacyRegionId": 2085,
    "name": "1st Street Jetty",
    "location": {
      "coordinates": [
        -75.963,
        36.831
      ],
      "type": "Point"
    },
    "forecastLocation": {
      "coordinates": [
        -75.947,
        36.847
      ],
      "type": "Point"
    },
    "spotType": "SURFBREAK",
    "abilityLevels": ["BEGINNER"],
    "boardTypes": ["SHORTBOARD"],
    "tideStation": "Virginia",
    "timezone": "America/New_York",
    "status": "PUBLISHED",
    "cams": [
      "583494523421b20545c4b518"
    ]
  },
  ...
]
```

## `GET /spots/:spotId`

### Parameters

| Parameter | Required | Description              |
| --------- | -------- | ------------------------ |
| `spotId`  | Yes      | Spot ID of spot resource |

### Sample Response

```text
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
```

```json
{
  "_id": "5842041f4e65fad6a77088ed",
  "legacyId": 4874,
  "legacyRegionId": 2143,
  "name": "HB Pier, Southside",
  "location": {
    "coordinates": [-118.0032588192, 33.654213041213],
    "type": "Point"
  },
  "forecastLocation": {
    "coordinates": [-118.001, 33.652],
    "type": "Point"
  },
  "tideStation": "Huntington Cliffs near Newport Bay",
  "timezone": "America/Los_Angeles",
  "status": "PUBLISHED",
  "rank": 10,
  "best": {
    "sizeRange": [
      {
        "max": 8,
        "min": 4
      }
    ],
    "swellWindow": [
      {
        "max": 200,
        "min": 180
      },
      {
        "max": 280,
        "min": 250
      }
    ],
    "swellPeriod": [
      {
        "max": 33,
        "min": 9
      }
    ],
    "windDirection": [
      {
        "max": 95,
        "min": 350
      }
    ]
  },
  "swellWindow": [
    {
      "max": 200,
      "min": 160
    },
    {
      "max": 285,
      "min": 250
    }
  ],
  "lolaSurfBiasCoefficients": {
    "linear": 1.2,
    "constant": 0.1
  },
  "noraBiasCorrection": 0.44,
  "offshoreDirection": 211,
  "cams": ["583499cb3421b20545c4b53f"],
  "politicalLocation": {
    "hierarchy": [
      {
        "_id": "589b52ac898f7500127b9529",
        "fcode": "AREA",
        "lat": "0",
        "adminName1": "",
        "fcodeName": "area",
        "countryName": null,
        "fclName": "parks,area, ...",
        "name": "Earth",
        "countryCode": null,
        "population": 6814400000,
        "fcl": "L",
        "countryId": null,
        "toponymName": "Earth",
        "geonameId": 6295630,
        "lng": "0",
        "adminCode1": null
      },
      {
        "_id": "589b52ac898f7500127b9528",
        "fcode": "CONT",
        "lat": "46.07323",
        "adminName1": "",
        "fcodeName": "continent",
        "countryName": null,
        "fclName": "parks,area, ...",
        "name": "North America",
        "countryCode": null,
        "population": 0,
        "fcl": "L",
        "countryId": null,
        "toponymName": "North America",
        "geonameId": 6255149,
        "lng": "-100.54688",
        "adminCode1": null
      },
      {
        "_id": "589b52ac898f7500127b9527",
        "fcode": "PCLI",
        "lat": "39.76",
        "adminName1": "",
        "fcodeName": "independent political entity",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "United States",
        "countryCode": "US",
        "population": 310232863,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "United States",
        "geonameId": 6252001,
        "lng": "-98.5",
        "adminCode1": "00"
      },
      {
        "_id": "589b52ac898f7500127b9526",
        "fcode": "ADM1",
        "lat": "37.25022",
        "adminName1": "California",
        "fcodeName": "first-order administrative division",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "California",
        "countryCode": "US",
        "population": 37691912,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "California",
        "geonameId": 5332921,
        "lng": "-119.75126",
        "adminCode1": "CA"
      },
      {
        "_id": "589b52ac898f7500127b9525",
        "fcode": "ADM2",
        "lat": "33.67691",
        "adminName1": "California",
        "fcodeName": "second-order administrative division",
        "countryName": "United States",
        "fclName": "country, state, region,...",
        "name": "Orange County",
        "countryCode": "US",
        "population": 3010232,
        "fcl": "A",
        "countryId": "6252001",
        "toponymName": "Orange County",
        "geonameId": 5379524,
        "lng": "-117.77617",
        "adminCode1": "CA"
      },
      {
        "_id": "589b52ac898f7500127b9524",
        "fcode": "PPL",
        "lat": "33.6603",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Huntington Beach",
        "countryCode": "US",
        "population": 189992,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Huntington Beach",
        "geonameId": 5358705,
        "lng": "-117.99923",
        "adminCode1": "CA"
      }
    ],
    "city": {
      "name": "Huntington Beach",
      "geonameId": 5358705
    }
  },
  "subregionId": "58581a836630e24c44878fd6"
}
```

## `GET /admin/subregions/:subregionId`

### Parameters

| Parameter     | Required | Description                        |
| ------------- | -------- | ---------------------------------- |
| `subregionId` | Yes      | Subregion ID of subregion resource |

### Sample Response

```text
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
```

```json
{
  "_id": "58581a836630e24c4487900b",
  "name": "South Los Angeles",
  "status": "PUBLISHED",
  "forecastStatus": {
    "status": "active",
    "inactiveMessage": "Inactive status message"
  },
  "region": "5908c45e6a2e4300134fbe92",
  "geoname": 5332921,
  "offshoreSwellLocation": {
    "coordinates": [-118.5, 33.85],
    "type": "Point"
  },
  "order": "4",
  "primarySpot": "5842041f4e65fad6a7708906",
  "basins": ["npac", "spac"],
  "spots": [],
  "legacyId": 2951
}
```

## `PUT /admin/subregions/:subregionId`

### Parameters

| Parameter     | Required | Description                        |
| ------------- | -------- | ---------------------------------- |
| `subregionId` | Yes      | Subregion ID of subregion resource |

### Request Body

| Property                         | Required | Type     | Description                                                                      |
| -------------------------------- | -------- | -------- | -------------------------------------------------------------------------------- |
| `name`                           | Yes      | String   | The name of the subregion                                                        |
| `region`                         | Yes      | ObjectId | Region ID of region resource                                                     |
| `primarySpot`                    | Yes      | ObjectId | Spot ID of the primary spot for the subregion                                    |
| `geoname`                        | Yes      | Number   | Geoname ID of the subregion location                                             |
| `status`                         | Yes      | Enum     | Status of the subregion. Valid options are 'Published' and 'Draft'               |
| `order`                          | Yes      | String   | The order within the region                                                      |
| `offshoreSwellLocation`          | Yes      | Point    | Offshore swell location for the subregion                                        |
| `basin`                          | Yes      | String   | The basin for the subregion                                                      |
| `forecastStatus.status`          | Yes      | Enum     | The forecast status for the subregion. Valid options are 'active' and 'inactive' |
| `forecastStatus.inactiveMessage` | Yes      | String   | A message describing the inactive forecast status.                               |

### Sample Request Body

```json
{
  "offshoreSwellLocation": {
    "type": "Point",
    "coordinates": [-63.25, 44.25]
  },
  "status": "PUBLISHED",
  "order": "5",
  "name": "Nova Scotia",
  "region": "5908c4f1380d980012ba8a4b",
  "geoname": 6251999,
  "primarySpot": "584204204e65fad6a77094cf",
  "forecastStatus": {
    "status": "inactive",
    "inactiveMessage": "Inactive status message Test"
  }
}
```

### Sample Request

```text
curl \
  --location --request PUT 'http://spots-api.staging.surfline.com/admin/subregions/58581a836630e24c44879079' \
  --header 'Content-Type: application/json' \
  --data-raw '{"offshoreSwellLocation":{"type":"Point","coordinates":[-63.25,44.25]},"status":"PUBLISHED","order":"5","name":"Nova Scotia","region":"5908c4f1380d980012ba8a4b","geoname":6251999,"primarySpot":"584204204e65fad6a77094cf"}'
```

### Sample Response

```text
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
```

```json
{
  "offshoreSwellLocation": {
    "type": "Point",
    "coordinates": [-63.25, 44.25]
  },
  "forecastStatus": {
    "status": "active",
    "inactiveMessage": "Inactive status message"
  },
  "status": "PUBLISHED",
  "order": "5",
  "_id": "58581a836630e24c44879079",
  "name": "Nova Scotia",
  "__v": 1,
  "region": "5908c4f1380d980012ba8a4b",
  "geoname": 6251999,
  "primarySpot": "584204204e65fad6a77094cf",
  "legacyId": 8000,
  "createdAt": "2022-01-30T19:57:12.029Z",
  "updatedAt": "2022-01-30T20:26:27.929Z"
}
```

## `PATCH /admin/subregions/:subregionId`

### Parameters

| Parameter     | Required | Description                        |
| ------------- | -------- | ---------------------------------- |
| `subregionId` | Yes      | Subregion ID of subregion resource |

### Request Body

| Property                         | Required | Type     | Description                                                                      |
| -------------------------------- | -------- | -------- | -------------------------------------------------------------------------------- |
| `name`                           | No       | String   | The name of the subregion                                                        |
| `region`                         | No       | ObjectId | Region ID of region resource                                                     |
| `primarySpot`                    | No       | ObjectId | Spot ID of the primary spot for the subregion                                    |
| `geoname`                        | No       | Number   | Geoname ID of the subregion location                                             |
| `status`                         | No       | Enum     | Status of the subregion. Valid options are 'Published' and 'Draft'               |
| `order`                          | No       | String   | The order within the region                                                      |
| `offshoreSwellLocation`          | No       | Point    | Offshore swell location for the subregion                                        |
| `basin`                          | No       | String   | The basin for the subregion                                                      |
| `forecastStatus.status`          | No       | Enum     | The forecast status for the subregion. Valid options are 'active' and 'inactive' |
| `forecastStatus.inactiveMessage` | No       | String   | A message describing the inactive forecast status.                               |

```json
{
  "offshoreSwellLocation": {
    "type": "Point",
    "coordinates": [-63.25, 44.25]
  },
  "status": "PUBLISHED",
  "order": "5",
  "name": "Nova Scotia",
  "region": "5908c4f1380d980012ba8a4b",
  "geoname": 6251999,
  "primarySpot": "584204204e65fad6a77094cf",
  "forecastStatus": {
    "status": "inactive",
    "inactiveMessage": "Inactive status message Test"
  }
}
```

### Sample Request

```text
curl \
  --location --request PATCH 'http://spots-api.staging.surfline.com/admin/subregions/58581a836630e24c44879079' \
  --header 'Content-Type: application/json' \
  --data-raw '{"name": "Nova Scotia"}'
```

### Sample Response

```text
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
```

```json
{
  "offshoreSwellLocation": {
    "type": "Point",
    "coordinates": [-63.25, 44.25]
  },
  "forecastStatus": {
    "status": "active",
    "inactiveMessage": "Inactive status message"
  },
  "status": "PUBLISHED",
  "order": "5",
  "_id": "58581a836630e24c44879079",
  "name": "Nova Scotia",
  "__v": 1,
  "region": "5908c4f1380d980012ba8a4b",
  "geoname": 6251999,
  "primarySpot": "584204204e65fad6a77094cf",
  "legacyId": 8000,
  "createdAt": "2022-01-30T19:57:12.029Z",
  "updatedAt": "2022-01-30T20:26:27.929Z"
}
```

## POST /admin/spots/:spotId/upload

```
POST /admin/spots/5842041f4e65fad6a77088ed/upload HTTP/1.1
Host: localhost:8081
Cache-Control: no-cache
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="thumbnail"; filename=""
Content-Type:


------WebKitFormBoundary7MA4YWxkTrZu0gW--
```

Uploads a thumbnail to the spot thumbnail CDN and marks the spot's `hasThumbnail`
property to `true`.

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `sub-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd5TlRZMU5EVTBOdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eU5UWTFORFUwTnciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Spots API](#spots-api-microservice) and [Taxonomy API](https://github.com/Surfline/wavetrak-services/blob/master/services/spots-api/documentation/taxonomy.md) sections.

**Ex:**

Test `GET` requests to the `/spots` endpoint:

```
curl -i -X GET \
    http://spots-api.sandbox.surfline.com/admin/spots/5842041f4e65fad6a77088ed
```

**Expected response:** See [above](#sample-response-1) for sample response.

And test `GET` requests to the `/taxonomy` endpoint:

```
curl -i -X GET \
    http://spots-api.sandbox.surfline.com/taxonomy?id=6295630&type=geoname
```

**Expected response:** See [documentation](https://github.com/Surfline/wavetrak-services/blob/master/services/spots-api/documentation/taxonomy.md#get-taxonomyid6295630typegeoname) for sample response.
