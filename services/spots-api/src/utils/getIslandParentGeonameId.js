/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
import { getHierarchy } from '../common/geonames';

const islandIds = [1214515];

const start = async () => {
  for (const islandId of islandIds) {
    // eslint-disable-next-line no-await-in-loop
    const hierarchy = await getHierarchy(islandId);
    const parentId = hierarchy[hierarchy.length - 2].geonameId;
    console.log(parentId);
  }
};

start()
  .then(() => {
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
