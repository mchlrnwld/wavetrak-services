/* eslint-disable no-console */
/* eslint-disable no-restricted-syntax */
import * as dbContext from '../model/dbContext';
import { geonameForLegacy } from '../model/taxonomy/spot';

const legacyIds = [];

const start = async () => {
  await dbContext.initMongoDB();
  for (const legacyId of legacyIds) {
    // eslint-disable-next-line no-await-in-loop
    const geonameId = await geonameForLegacy(legacyId);
    console.log(geonameId);
  }
};

start()
  .then(() => {
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
