/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
import { getHierarchy } from '../common/geonames';

const islandIds = [
  1214515,
];

const start = async () => {
  for (const islandId of islandIds) {
    // eslint-disable-next-line no-await-in-loop
    const hierarchy = await getHierarchy(islandId);
    const islandDetails = hierarchy[hierarchy.length - 1];
    console.log(islandDetails);
  }
};

start()
  .then(() => {
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
