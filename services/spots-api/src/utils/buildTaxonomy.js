/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console, import/no-extraneous-dependencies */
import mongoose from 'mongoose';
import { argv } from 'yargs';
import * as dbContext from '../model/dbContext';
import { normalizeSpot } from '../model/taxonomy';
import SpotModel from '../model/SpotModel';

// Dummy function as it's required but doesn't exist...
const markTaxonomy = () => false;

const CityModel = mongoose.model('City', new mongoose.Schema(
  {
    name: { type: String },
    geonameId: { type: Number, unique: true },
    location: {
      index: '2dsphere',
      type: mongoose.Schema.Types.Point,
    },
  },
  { collection: 'Cities' },
));

const nearestCity = (coord) => CityModel.findOne({
  location: {
    $near: {
      $geometry: {
        type: 'Point',
        coordinates: coord,
      },
    },
  },
});

const start = async () => {
  await dbContext.initMongoDB();

  let spots = argv.spot;
  if (spots && !Array.isArray(spots)) {
    spots = [spots];
  } else {
    spots = [];
  }

  if (argv.prune && argv.type) {
    const pruned = await markTaxonomy(argv.prune, argv.type);
    return pruned;
  }

  if (argv.all) {
    spots = await SpotModel
      .find({ subregion: { $exists: true }, geoname: { $exists: false } });
    spots = spots.map((spot) => spot._id);
  }

  if (argv.rebuild) {
    spots = await SpotModel
      .find({ subregion: { $exists: true }, geoname: { $exists: true } });
    spots = spots.map((spot) => spot._id);
  }

  let k = 0;
  for (const spotId of spots) {
    console.log(`${k} of ${spots.length}\t ${spotId}`);
    try {
      const spot = await SpotModel
        .findOne({ _id: new mongoose.mongo.ObjectId(spotId) })
        .populate('subregion');
      if (!argv.rebuild && (!spot.geoname || !argv.all)) {
        const spotCity = await nearestCity(spot.location.coordinates);
        spot.geoname = spotCity.geonameId;
        await spot.save();

        const hierarchy = await normalizeSpot(spot);
        console.log(hierarchy);
      } else if (argv.rebuild) {
        console.log('rebuilding');
        await normalizeSpot(spot);
      }
    } catch (err) {
      console.log(err);
    }
    k += 1;
  }

  return `Processed ${spots.length} spots`;
};

start()
  .then((result) => {
    console.log(result);
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
