/* eslint-disable no-console, import/no-extraneous-dependencies */
import { argv } from 'yargs';
import * as dbContext from '../model/dbContext';
import { spotsForTaxonomy } from '../model/taxonomy/spot';

const start = async () => {
  await dbContext.initMongoDB();
  const spots = await spotsForTaxonomy(argv.geoname);
  return spots;
};

start()
  .then((result) => {
    console.log(result);
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
