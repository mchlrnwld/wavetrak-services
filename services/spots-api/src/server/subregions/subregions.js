import { Types } from 'mongoose';
import { getBatchSubregions } from '../../model/SubregionModel';

// eslint-disable-next-line
export const batchSubregionsHandler = async (req, res) => {
  const { subregionIds } = req.query;
  let formattedSubregionIds;
  try {
    formattedSubregionIds = subregionIds.split(',').map((id) => new Types.ObjectId(id));
  } catch (err) {
    return res.status(400).send({ message: 'Valid subregionId(s) required' });
  }
  const subregions = await getBatchSubregions(formattedSubregionIds);
  return res.send({ subregions });
};
