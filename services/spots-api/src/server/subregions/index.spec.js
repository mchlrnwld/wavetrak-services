import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { Types } from 'mongoose';
import Subregion, * as subregionModel from '../../model/SubregionModel';
import subregions from '.';

describe('/subregions', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(subregions(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(Subregion, 'find');
  });

  afterEach(() => {
    Subregion.find.restore();
  });

  describe('GET', () => {
    it('should return an array of subregions', async () => {
      const model = [
        {
          name: 'testsubregion',
          legacyId: null,
          spots: [],
        },
        {
          name: 'testsubregion2',
          legacyId: null,
          spots: [],
        },
      ];
      const subregionIds = '58581a836630e24c44878fd6,58581a836630e24c44878fcb';
      const formattedSubregionIds = subregionIds.split(',').map((id) => new Types.ObjectId(id));
      sinon.stub(subregionModel, 'getBatchSubregions').returns(model);
      const res = await request.get(`/batch?subregionIds=${subregionIds}`).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ subregions: model });
      expect(subregionModel.getBatchSubregions).to.have.been.calledOnce();
      expect(subregionModel.getBatchSubregions).to.have.been.calledWithExactly(
        formattedSubregionIds,
      );
    });

    it('should return 400 if subregionId param provided', async () => {
      Subregion.find.returns(null);
      try {
        await request.get('/batch').send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Valid subregionId(s) required' });
      }
    });
  });
});
