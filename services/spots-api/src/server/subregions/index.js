import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { batchSubregionsHandler } from './subregions';

export const trackRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/subregions',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const subregions = (log) => {
  const api = Router();

  api.all('*', trackRequests(log));
  api.get('/batch', wrapErrors(batchSubregionsHandler));

  return api;
};

export default subregions;
