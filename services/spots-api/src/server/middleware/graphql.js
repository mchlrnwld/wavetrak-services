import { ApolloServer } from 'apollo-server-express';
import { buildFederatedSchema } from '@apollo/federation';
import spotMeta from '../admin/spotMeta';
import brandAPIs from '../admin/spotMeta/datasources';
import { MSWAPI } from '../admin/spotMeta/datasources/mswAPI';

/**
 * instantiates a federated Apollo Server and applies the express app as middleware
 * @param  {logger} log passes a logger to server, this will log all errors and trace all requests
 * @returns {ApolloServer} a federated ApolloServer instance
 */
export default (log) => async (app = null) => {
  try {
    const mswAPI = new MSWAPI();
    mswAPI.initialize({});
    let mswAccessToken;

    try {
      mswAccessToken = await mswAPI.login();
    } catch (error) {
      log.error(error);
    }

    const mswExtraData = { accessToken: mswAccessToken };
    const server = new ApolloServer({
      schema: buildFederatedSchema([spotMeta]),
      dataSources: brandAPIs(mswExtraData),
      context: ({ req }) => {
        if (req) {
          log.trace({
            action: '/graphql',
            request: {
              headers: req.headers,
              params: req.params,
              query: req.query,
              body: req.body,
            },
          });
        }

        return { log };
      },
      formatError: (err) => {
        log.error(err);
        return err;
      },
    });

    if (app) server.applyMiddleware({ app });

    return server;
  } catch (error) {
    log.error(error);
    throw error;
  }
};
