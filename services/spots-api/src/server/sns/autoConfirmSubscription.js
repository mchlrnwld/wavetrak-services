import fetch from 'node-fetch';

const SNS_HOST_PATTERN = '^https://sns..+.amazonaws.com.*Action=ConfirmSubscription';

/**
 * Creates an Express middleware for autoconfirming an SNS subscription.
 *
 * @param snsTopicARN - SNS topic ARN to subscribe to.
 * @return Express middleware for handling SubscriptionConfirmation message type.
 */
const autoConfirmSubscription = (snsTopicARN) => {
  const snsTopicARNParameter = `TopicArn=${snsTopicARN}`;
  return async function autoConfirmSubscriptionHelper(req, res, next) {
    const messageType = req.headers['x-amz-sns-message-type'];
    if (messageType !== 'SubscriptionConfirmation') {
      return next();
    }

    const message = req.body;

    if (
      !message.SubscribeURL
      || !message.SubscribeURL.match(SNS_HOST_PATTERN)
      || !message.SubscribeURL.includes(snsTopicARNParameter)
    ) {
      return res.status(400).send({ message: 'Invalid SubscribeURL.' });
    }

    try {
      await fetch(message.SubscribeURL);
    } catch (err) {
      return res.status(400).send({
        message: `GET ${message.SubscribeURL} failed with error:\n${err}.`,
      });
    }

    return res.send({ message: 'OK' });
  };
};

export default autoConfirmSubscription;
