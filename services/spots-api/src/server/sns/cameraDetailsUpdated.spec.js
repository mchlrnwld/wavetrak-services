import { expect } from 'chai';
import sinon from 'sinon';
import mockExpressRequests from '../../mockExpressRequests.spec';
import cameraDetailsUpdated from './cameraDetailsUpdated';
import * as cameraExternals from '../../externals/cameras';
import * as publishTopic from '../admin/spots/publishTopic';
import * as spotHandlers from '../admin/spots/spots';

const cameraDetailsUpdatedSpec = () => {
  describe('cameraDetailsUpdated', () => {
    let request;

    before(() => {
      request = mockExpressRequests((app) => {
        app.use('/sns/camera-details-updated', cameraDetailsUpdated);
      });

      sinon.stub(publishTopic, 'default');
      sinon.stub(cameraExternals, 'getCamera');
      sinon.stub(spotHandlers, 'removeCamFromSpots');

      publishTopic.default.resolves({});
      spotHandlers.removeCamFromSpots.resolves({});
    });

    afterEach(() => {
      publishTopic.default.resetHistory();
      spotHandlers.removeCamFromSpots.resetHistory();
      cameraExternals.getCamera.resetHistory();
    });

    after(() => {
      cameraExternals.getCamera.restore();
      publishTopic.default.restore();
      spotHandlers.removeCamFromSpots.restore();
    });

    it('does nothing if a camera is updated', async () => {
      cameraExternals.getCamera.resolves({ camera: 'fakeCamera' });
      const res = await request
        .post('/sns/camera-details-updated')
        .set('x-amz-sns-message-type', 'Notification')
        .send({ Message: JSON.stringify({ cameraId: 'invalidCameraId' }) });
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ message: 'OK' });
      expect(spotHandlers.removeCamFromSpots).not.to.have.been.called();
      expect(publishTopic.default).not.to.have.been.called();
    });

    it('updates spot cameras if a camera is deleted', async () => {
      cameraExternals.getCamera.rejects({ status: 404, message: 'Camera not found' });
      const res = await request
        .post('/sns/camera-details-updated')
        .set('x-amz-sns-message-type', 'Notification')
        .send({ Message: JSON.stringify({ cameraId: 'invalidCameraId' }) });
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ message: 'OK' });
      expect(spotHandlers.removeCamFromSpots).to.have.been.calledOnce();
    });
  });
};

export default cameraDetailsUpdatedSpec;
