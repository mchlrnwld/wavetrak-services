import newrelic from 'newrelic';

import { getCamera } from '../../externals/cameras';
import { removeCamFromSpots } from '../admin/spots/spots';

/**
 * Express middleware handles camera updated notification message.
 * Spots are updated in the database to store an updated cams[] based
 * on the latest camera information
 */
async function cameraDetailsUpdated(req, res) {
  const messageType = req.headers['x-amz-sns-message-type'];
  if (messageType !== 'Notification') {
    return res.status(400).send({ message: `Unable to handle message type ${messageType}.` });
  }

  const { cameraId } = JSON.parse(req.body.Message);
  newrelic.addCustomAttribute('camera', cameraId);

  try {
    await getCamera(cameraId);
  } catch (err) {
    if (err.status === 404) {
      // Note: cameras-service will throw an error for "camera not found"
      // for camera's that have been deleted. Update spot.cams[] to remove from any spots using it
      await removeCamFromSpots(cameraId);
    } else {
      throw err;
    }
  }

  return res.send({ message: 'OK' });
}

export default cameraDetailsUpdated;
