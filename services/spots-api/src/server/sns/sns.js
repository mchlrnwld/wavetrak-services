import { Router } from 'express';
import { text } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import verifyMessage from './verifyMessage';
import autoConfirmSubscription from './autoConfirmSubscription';
import config from '../../config';
import cameraDetailsUpdated from './cameraDetailsUpdated';

export const trackRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/sns',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const sns = (log) => {
  const api = Router();

  api.all('*', trackRequests(log));

  // HACK: SNS messages are JSON but sent with Content-Type: text/plain; charset=UTF-8.
  api.use(text(), (req, res, next) => {
    if (!req.headers['content-type'] || !req.headers['content-type'].includes('application/json')) {
      try {
        req.body = JSON.parse(req.body);
      } catch (err) {
        return res.status(400).send({ message: 'Message payload invalid.' });
      }
    }
    return next();
  });

  api.post(
    '/camera-details-updated',
    wrapErrors(verifyMessage(config.CAMERA_DETAILS_UPDATED_SNS_TOPIC)),
    wrapErrors(autoConfirmSubscription(config.CAMERA_DETAILS_UPDATED_SNS_TOPIC)),
    wrapErrors(cameraDetailsUpdated),
  );

  return api;
};

export default sns;
