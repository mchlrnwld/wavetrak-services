import autoConfirmSubscriptionSpec from './autoConfirmSubscription.spec';
import cameraDetailsUpdatedSpec from './cameraDetailsUpdated.spec';

describe('/sns', () => {
  autoConfirmSubscriptionSpec();
  cameraDetailsUpdatedSpec();
});
