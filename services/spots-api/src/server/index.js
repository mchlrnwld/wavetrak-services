import { setupExpress } from '@surfline/services-common';
import logger from '../common/logger';
import admin from './admin';
import taxonomy from './taxonomy';
import subregions from './subregions';
import graphql from './middleware/graphql';
import sns from './sns';

const log = logger('spots-api');

const adminRoute = ['/admin', admin(log)];
const subregionsRoute = ['/subregions', subregions(log)];
const spotsRoute = ['/taxonomy', taxonomy(log)];
const snsRoute = ['/sns', sns(log)];

const setupApp = () => setupExpress({
  name: 'spots-api',
  port: process.env.EXPRESS_PORT || 8081,
  log,
  callback: graphql(log),
  handlers: [adminRoute, subregionsRoute, spotsRoute, snsRoute],
});

export default setupApp;
