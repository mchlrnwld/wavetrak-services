import FlatToNested from 'flat-to-nested';

const convertFlatTaxonomyToNested = (taxonomyItems, rootId) => {
  const allowedTypes = ['geoname', 'spot'];

  const filteredTaxonomyItems = taxonomyItems.filter(
    (item) => allowedTypes.includes(item.type) && (item.hasSpots || item.type === 'spot'),
  );
  const idMap = new Map(
    filteredTaxonomyItems.map((item) => [
      item._id.toString(),
      {
        id: item.geonameId || item.spot,
        depth: item.depth,
      },
    ]),
  );

  // Build a new flat array with id, parent (instead of liesIn[]) and minimal properties
  const flatItems = filteredTaxonomyItems.map((item) => {
    const parents = item.liesIn
      ? item.liesIn.filter((liesInId) => liesInId && idMap.has(liesInId.toString()))
      : [];

    const parentId = parents.length
      ? parents.sort((a, b) => idMap.get(b.toString()).depth - idMap.get(a.toString()).depth)[0]
      : null;

    return {
      id: `${item.geonameId || item.spot}`,
      name: item.name,
      type: item.type,
      lon: item.location ? item.location.coordinates[0] : null,
      lat: item.location ? item.location.coordinates[1] : null,
      parent: parentId ? idMap.get(parentId.toString()).id : undefined,
    };
  });

  const nestedJSON = new FlatToNested({
    id: 'id',
    parent: 'parent',
    children: 'contains',
  }).convert(flatItems);

  // If the taxonomy data has orphoned elements there will be multiple roots.
  // We only want to return the actual root node and everything below.
  const hasMultipleRoots = nestedJSON.id !== rootId;
  return hasMultipleRoots
    ? nestedJSON.contains.filter((rootNode) => rootNode.id === rootId)[0]
    : nestedJSON;
};

export default convertFlatTaxonomyToNested;
