import newrelic from 'newrelic';
import { pick } from 'lodash/fp';
import {
  fetchTaxonomy,
  batchFetchTaxonomyIds,
  fetchNearbyTaxonomy,
  fetchNoSpotsTaxonomy,
} from '../../model/taxonomy';
import { fetchTaxonomyJSON, buildGeoNode } from '../../common/taxonomy';
import Spot from '../../model/SpotModel';
import calculateDistance from '../../utils/calculateDistance';
import convertFlatTaxonomyToNested from './convertFlatTaxonomyToNested';

const GEONAMEID_EARTH = '6295630';

export const getTaxonomyHandler = async (req, res) => {
  const { type, id, maxDepth: maxDepthQuery } = req.query;

  newrelic.addCustomAttributes({
    type,
    id,
    maxDepth: maxDepthQuery,
  });

  const maxDepth = maxDepthQuery ? parseInt(maxDepthQuery, 10) : undefined;
  try {
    const result = await fetchTaxonomyJSON(type, id, maxDepth);

    if (result) {
      return res.send(result);
    }

    return res.status(400).send({
      status: 400,
      message: 'Could not find a suitable taxonomy for the node',
    });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    newrelic.noticeError(error, {
      type,
      id,
      maxDepth,
    });
    throw error;
  }
};

const loadGlobalTaxonomyData = async (camsOnly) => {
  if (camsOnly) {
    // Determine which spots have cams and load corresponding taxonomy
    const spotsUsingCam = await Spot.find(
      { cams: { $exists: true, $ne: [] }, status: 'PUBLISHED' },
      '_id',
    );
    if (spotsUsingCam && spotsUsingCam.length > 0) {
      return batchFetchTaxonomyIds({ spots: spotsUsingCam.map(({ _id }) => _id) }, [
        'liesIn',
        'location',
        'hasSpots',
      ]);
    }
  } else {
    // Full Global Taxonomy
    const globalTax = await fetchTaxonomy('geoname', GEONAMEID_EARTH);
    if (globalTax && globalTax.length > 0 && globalTax[0].contains) {
      // merge root node and contains[] together into a single array
      return globalTax[0].contains.concat({ ...globalTax[0], contains: null, isRoot: true });
    }
  }

  return null;
};

export const getGlobalTaxonomyHandler = async (req, res) => {
  const taxonomyData = await loadGlobalTaxonomyData(
    req.query.camsOnly && req.query.camsOnly.toLowerCase() === 'true',
  );

  if (taxonomyData) {
    return res.send(convertFlatTaxonomyToNested(taxonomyData, GEONAMEID_EARTH));
  }

  return res.status(400).send({
    status: 400,
    message: 'Could not find global taxonomy',
  });
};

export const batchTaxonomyIdsHandler = async (req, res) => {
  const ids = req.body;
  const taxonomy = await batchFetchTaxonomyIds(ids);

  return res.send({
    taxonomy,
  });
};

export const getNearbyTaxonomyHandler = async (req, res) => {
  const lat = req.query.lat || req.geoLatitude;
  const lon = req.query.lon || req.geoLongitude;
  const {
    type,
    fcl,
    fcode,
    select,
  } = req.query;
  const coordinates = [parseFloat(lon), parseFloat(lat)];
  const selectArray = select ? select.split(',') : [];
  const count = req.query.count ? parseInt(req.query.count, 10) : undefined;
  const maxDepth = req.query.maxDepth
    ? parseInt(req.query.maxDepth, 10)
    : undefined;
  const maxContainsDepth = req.query.maxContainsDepth
    ? parseInt(req.query.maxContainsDepth, 10)
    : undefined;

  newrelic.addCustomAttributes({
    lat,
    lon,
    type,
    fcl,
    fcode,
    count,
    maxDepth,
    maxContainsDepth,
  });

  try {
    const resultsTaxonomy = await fetchNearbyTaxonomy(
      coordinates,
      { type, fcl, fcode },
      count,
      maxDepth,
      maxContainsDepth,
    );

    // results occasionally contain non matching parent taxonomies.
    // reason for filter section
    let results = resultsTaxonomy
      .map((taxonomy) => {
        const taxonomyWithDistance = taxonomy;
        if (!taxonomyWithDistance.location) {
          taxonomyWithDistance.distance = 0;
        } else {
          const latitude = taxonomyWithDistance.location.coordinates[1];
          const longitude = taxonomyWithDistance.location.coordinates[0];
          const distance = calculateDistance(
            coordinates[1],
            coordinates[0],
            latitude,
            longitude,
            'K',
          );
          taxonomyWithDistance.distance = distance;
        }
        return taxonomyWithDistance;
      })
      .sort((a, b) => a.distance - b.distance)
      .filter((result) => {
        let isMatch = true;
        if (fcl && fcl !== result.geonames.fcl) {
          isMatch = false;
        }
        if (fcode && fcode !== result.geonames.fcode) {
          isMatch = false;
        }
        if (type && type !== result.type) {
          isMatch = false;
        }
        return isMatch;
      });

    if (results.length > 0) {
      results = results.map((node) => buildGeoNode(node));
      if (selectArray.length) results = results.map(pick(selectArray));
      return res.send({
        data: results,
      });
    }

    return res.status(400).send({
      status: 400,
      message: 'Could not find a suitable taxonomy for the node',
    });
  } catch (error) {
    newrelic.noticeError(error, {
      lat,
      lon,
      type,
      fcl,
      fcode,
      coordinates,
      count,
      maxDepth,
      maxContainsDepth,
    });
    throw error;
  }
};

export const getNoSpotsTaxonomyHandler = async (req, res) => {
  const results = await fetchNoSpotsTaxonomy();

  if (results.length > 0) {
    return res.send({
      data: results.map((node) => buildGeoNode(node)),
    });
  }

  return res.status(400).send({
    status: 400,
    message: 'Could not find a suitable taxonomy with no spots',
  });
};
