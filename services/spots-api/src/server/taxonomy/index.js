import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  getTaxonomyHandler,
  getGlobalTaxonomyHandler,
  batchTaxonomyIdsHandler,
  getNearbyTaxonomyHandler,
  getNoSpotsTaxonomyHandler,
} from './taxonomy';

export const trackRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/taxonomy',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const taxonomy = (log) => {
  const api = Router();

  api.all('*', trackRequests(log));
  api.get('/', wrapErrors(getTaxonomyHandler));
  api.get('/global', wrapErrors(getGlobalTaxonomyHandler));
  api.post('/ids', json(), wrapErrors(batchTaxonomyIdsHandler));
  api.get('/nearby', wrapErrors(getNearbyTaxonomyHandler));
  api.get('/nospots', wrapErrors(getNoSpotsTaxonomyHandler));

  return api;
};

export default taxonomy;
