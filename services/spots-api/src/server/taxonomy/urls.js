import { slugify } from '@surfline/web-common';
import { types } from '../../model/taxonomy';
import config from '../../config';

const {
  GEONAME, SPOT, SUBREGION, REGION,
} = types;

const links = {
  [REGION]: [
    (node) => ({ key: 'taxonomy', href: `${config.SPOTS_API_HOST}/taxonomy?id=${node.region}&type=region` }),
    (node) => ({ key: 'api', href: `${config.SPOTS_API_HOST}/admin/regions/${node.region}` }),
  ],
  [SUBREGION]: [
    (node) => ({ key: 'taxonomy', href: `${config.SPOTS_API_HOST}/taxonomy?id=${node.subregion}&type=subregion` }),
    (node) => ({ key: 'api', href: `${config.SPOTS_API_HOST}/admin/subregions/${node.subregion}` }),
    (node) => ({ key: 'www', href: `${config.WWW_HOST}/surf-forecasts/${slugify(node.name)}/${node.subregion}` }),
  ],
  [SPOT]: [
    (node) => ({ key: 'taxonomy', href: `${config.SPOTS_API_HOST}/taxonomy?id=${node.spot}&type=spot` }),
    (node) => ({ key: 'api', href: `${config.SPOTS_API_HOST}/admin/spots/${node.spot}` }),
    (node) => ({ key: 'www', href: `${config.WWW_HOST}/surf-report/${slugify(node.name)}/${node.spot}` }),
  ],
  [GEONAME]: [
    (node) => ({ key: 'taxonomy', href: `${config.SPOTS_API_HOST}/taxonomy?id=${node.geonameId}&type=geoname` }),
    (node) => {
      const paths = node.enumeratedPath.split(',');

      // this checks that we aren't on a planet or continent, we don't show
      // urls for those divisions.
      if (paths.length <= 3) return null;

      const geoPaths = paths.slice(3).map((slug) => slugify(slug)).join('/');
      return {
        key: 'www',
        href: `${config.WWW_HOST}/surf-reports-forecasts-cams/${geoPaths}/${node.geonameId}`,
      };
    },
    (node) => {
      // travel specific urls
      const paths = node.enumeratedPath.split(',');

      if (paths.length > 0) {
        paths[paths.length - 1] = `${paths[paths.length - 1]}-surfing-and-beaches`;
      }

      // this checks that we aren't on a planet or continent, we don't show
      // urls for those divisions.
      if (paths.length <= 3) return null;

      const geoPaths = paths.slice(3).map((slug) => slugify(slug)).join('/');
      return {
        key: 'travel',
        href: `${config.WWW_HOST}/travel/${geoPaths}/${node.geonameId}`,
      };
    },
  ],
};

const linksFor = (node) => links[node.type]
  .reduce((matched, link) => matched.concat(link(node)), []);

export default linksFor;
