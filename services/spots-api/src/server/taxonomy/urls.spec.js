import sinon from 'sinon';
import { expect } from 'chai';
import linksFor from './urls';
import * as config from '../../config';

describe('generate urls for nodes', () => {
  before(() => {
    sinon.stub(config, 'default').value({
      SPOTS_API_HOST: 'spots-api',
      WWW_HOST: 'surfline.com',
    });
  });

  after(() => {
    sinon.restore();
  });

  describe('region', () => {
    it('should return urls for a region', async () => {
      const regionFixture = {
        _id: '58e5c41986fd4b596b8964ac',
        region: '58dbfe044bfa680012082307',
        type: 'region',
        name: 'Southern California',
        category: 'surfline',
        __v: 0,
      };
      const urls = linksFor(regionFixture);
      expect(urls).to.deep.equal([
        { key: 'taxonomy', href: 'spots-api/taxonomy?id=58dbfe044bfa680012082307&type=region' },
        { key: 'api', href: 'spots-api/admin/regions/58dbfe044bfa680012082307' },
      ]);
    });
  });

  describe('subregion', () => {
    it('should return urls for a subregion', async () => {
      const subregionFixture = {
        _id: '58e5c41a86fd4b596b8964b4',
        subregion: '58581a836630e24c4487900d',
        type: 'subregion',
        liesIn: ['58e5c41986fd4b596b8964ac'],
        name: 'South San Diego',
        category: 'surfline',
        __v: 0,
      };
      const urls = linksFor(subregionFixture);
      expect(urls).to.deep.equal([
        { key: 'taxonomy', href: 'spots-api/taxonomy?id=58581a836630e24c4487900d&type=subregion' },
        { key: 'api', href: 'spots-api/admin/subregions/58581a836630e24c4487900d' },
        {
          key: 'www',
          href: 'surfline.com/surf-forecasts/south-san-diego/58581a836630e24c4487900d',
        },
      ]);
    });
  });

  describe('spot', () => {
    it('should return urls for a spot', async () => {
      const spotFixture = {
        _id: '58e5c41986fd4b596b8964a0',
        spot: '5842041f4e65fad6a770883c',
        type: 'spot',
        liesIn: ['58e5c41986fd4b596b896499', '58e5c41a86fd4b596b8964b4'],
        location: {
          coordinates: [-117.2822713851, 32.829151647032],
          type: 'Point',
        },
        name: 'Windansea',
        category: 'surfline',
        __v: 0,
      };

      const urls = linksFor(spotFixture);
      expect(urls).to.deep.equal([
        { key: 'taxonomy', href: 'spots-api/taxonomy?id=5842041f4e65fad6a770883c&type=spot' },
        { key: 'api', href: 'spots-api/admin/spots/5842041f4e65fad6a770883c' },
        {
          key: 'www',
          href: 'surfline.com/surf-report/windansea/5842041f4e65fad6a770883c',
        },
      ]);
    });
  });

  describe('geoname', () => {
    it('build urls for a geoname city', async () => {
      const cityGeonameFixture = {
        _id: '58e5c41986fd4b596b896499',
        geonameId: 5363943,
        type: 'geoname',
        liesIn: ['58e5c41986fd4b596b896491'],
        geonames: {
          fcode: 'PPL',
          lat: '32.84727',
          adminName1: 'California',
          fcodeName: 'populated place',
          countryName: 'United States',
          fclName: 'city, village,...',
          name: 'La Jolla',
          countryCode: 'US',
          population: 42808,
          fcl: 'P',
          countryId: '6252001',
          toponymName: 'La Jolla',
          geonameId: 5363943,
          lng: '-117.2742',
          adminCode1: 'CA',
        },
        location: {
          coordinates: [-117.2742, 32.84727],
          type: 'Point',
        },
        enumeratedPath: ',Earth,North America,US,CA,San Diego,La Jolla',
        name: 'La Jolla',
        category: 'geonames',
        __v: 0,
      };

      const urls = linksFor(cityGeonameFixture);
      expect(urls).to.deep.equal([
        { key: 'taxonomy', href: 'spots-api/taxonomy?id=5363943&type=geoname' },
        {
          key: 'www',
          href: 'surfline.com/surf-reports-forecasts-cams/us/ca/san-diego/la-jolla/5363943',
        },
        {
          key: 'travel',
          href: 'surfline.com/travel/us/ca/san-diego/la-jolla-surfing-and-beaches/5363943',
        },
      ]);
    });

    it('should not return urls for a continent', () => {
      const northAmericaGeonameFixture = {
        _id: '58e5c41986fd4b596b89647a',
        geonameId: 6255149,
        type: 'geoname',
        liesIn: ['58e5c41986fd4b596b896473'],
        geonames: {
          population: 0,
          fcode: 'CONT',
          fcl: 'L',
          lat: '46.07323',
          adminName1: '',
          fcodeName: 'continent',
          toponymName: 'North America',
          fclName: 'parks,area, ...',
          name: 'North America',
          geonameId: 6255149,
          lng: '-100.54688',
        },
        location: {
          coordinates: [-100.54688, 46.07323],
          type: 'Point',
        },
        enumeratedPath: ',Earth,North America',
        name: 'North America',
        category: 'geonames',
        __v: 0,
      };

      const urls = linksFor(northAmericaGeonameFixture);
      expect(urls).to.deep.equal([
        {
          href: 'spots-api/taxonomy?id=6255149&type=geoname',
          key: 'taxonomy',
        },
        null,
        null,
      ]);
    });
  });
});
