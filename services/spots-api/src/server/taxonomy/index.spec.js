import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import newrelic from 'newrelic';
import taxonomy from '.';

import * as TaxonomyModel from '../../model/taxonomy/taxonomy';
import * as config from '../../config';

import spotModel from './fixtures/spot/model.json';
import spotResponse from './fixtures/spot/response.json';

import geonameModel from './fixtures/geoname/model.json';
import geonameModelForNested from './fixtures/geoname/modelTaxonomyForNested.json';
import taxonomyModel from './fixtures/geoname/modelTaxonomy.json';
import geonameResponse from './fixtures/geoname/response.json';
import geonameResponseNested from './fixtures/geoname/responseNested.json';

import taxonomyIdsResponse from './fixtures/taxonomy/ids.json';
import nearbyTaxonomyResponse from './fixtures/taxonomy/nearby.json';
import nearbySpotsResponse from './fixtures/taxonomy/nearbySpots.json';
import nearbyWithSelectTaxonomyResponse from './fixtures/taxonomy/nearbyWithSelect.json';
import nearbyWithContainsTaxonomyResponse from './fixtures/taxonomy/nearbyWithContains.json';
import noSpotsTaxonomyResponse from './fixtures/taxonomy/nospots.json';

describe('/taxonomy', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(taxonomy(log));
    request = chai.request(app).keepOpen();
    sinon.stub(config, 'default').value({
      SPOTS_API_HOST: 'spots-api',
      WWW_HOST: 'surfline.com',
    });
    sinon.stub(newrelic, 'addCustomAttributes');
    sinon.stub(newrelic, 'addCustomAttribute');
  });

  beforeEach(() => {
    sinon.stub(TaxonomyModel, 'default');
    sinon.stub(TaxonomyModel, 'batchFetchTaxonomyIds');
    sinon.stub(TaxonomyModel, 'fetchNearbyTaxonomy');
    sinon.stub(TaxonomyModel, 'fetchNoSpotsTaxonomy');
  });

  afterEach(() => {
    TaxonomyModel.default.restore();
    TaxonomyModel.batchFetchTaxonomyIds.restore();
    TaxonomyModel.fetchNearbyTaxonomy.restore();
    TaxonomyModel.fetchNoSpotsTaxonomy.restore();
  });

  after(() => {
    sinon.restore();
  });

  it('should return a taxonomy structure for a spot', async () => {
    const spotId = '5842041f4e65fad6a770883c';
    TaxonomyModel.default.resolves(spotModel);

    const res = await request.get(`/?id=${spotId}&type=spot`).send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(spotResponse);
  });

  it('should return a taxonomy structure for a geoname', async () => {
    const spotId = '5842041f4e65fad6a770883c';
    TaxonomyModel.default.resolves(geonameModel);
    const res = await request.get(`/?id=${spotId}&type=spot`).send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(geonameResponse);
  });

  it('should return a taxonomy structure for a mongoId and type=taxonomy', async () => {
    const taxonomyId = '5842041f4e65fad6a770883c';
    TaxonomyModel.default.resolves(taxonomyModel);
    const res = await request.get(`/?id=${taxonomyId}&type=taxonomy`).send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(geonameResponse);
  });

  it('should return a 400 if a taxonomy cannot be found', async () => {
    const spotId = '5842041f4e65fad6a770883a';
    const emptyFixture = [];
    TaxonomyModel.default.resolves(emptyFixture);
    try {
      await request.get(`/?id=${spotId}&type=spot`).send();
    } catch (err) {
      expect(err.response).to.have.status(400);
      expect(err.response).to.be.json();
    }
  });

  describe('/global', async () => {
    it('should return a nested taxonomy structure', async () => {
      TaxonomyModel.default.resolves(geonameModelForNested);
      const res = await request.get('/global').send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(geonameResponseNested);
    });
  });

  describe('/ids', async () => {
    it('should return a set of taxonomy ids for a set of spots', async () => {
      TaxonomyModel.batchFetchTaxonomyIds.resolves(taxonomyIdsResponse);
      const req = {
        spots: ['5842041f4e65fad6a770882e', '5842041f4e65fad6a7708827'],
      };

      const res = await request.post('/ids').send(req);
      expect(TaxonomyModel.batchFetchTaxonomyIds).to.have.been.calledWith(req);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ taxonomy: taxonomyIdsResponse });
    });
  });

  describe('/nearby', async () => {
    it('returns a set of taxonomies', async () => {
      TaxonomyModel.fetchNearbyTaxonomy.resolves(nearbyTaxonomyResponse);
      const res = await request.get('/nearby?lat=33.65&lon=-117.99&type=geoname').send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ data: nearbyTaxonomyResponse });
    });

    it('filters response by select query param if provided', async () => {
      TaxonomyModel.fetchNearbyTaxonomy.resolves(nearbySpotsResponse);
      const res = await request
        .get('/nearby?lat=34&lon=-117&type=spot&maxDepth=0&count=3&select=location,name,spot')
        .send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ data: nearbyWithSelectTaxonomyResponse });
    });

    it('returns a set of taxonomies with contains', async () => {
      TaxonomyModel.fetchNearbyTaxonomy.resolves(nearbyWithContainsTaxonomyResponse);
      const res = await request
        .get('/nearby?lat=33.65&lon=-117.99&type=geoname&maxContainsDepth=5&maxDepth=0')
        .send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ data: nearbyWithContainsTaxonomyResponse });
    });

    it('should return a 400 if a taxonomy cannot be found', async () => {
      const emptyFixture = [];
      TaxonomyModel.fetchNearbyTaxonomy.resolves(emptyFixture);
      try {
        await request.get('/nearby?lat=100&lon=30&type=spot').send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
      }
    });
  });

  describe('/nospots', async () => {
    it('returns a set of taxonomies', async () => {
      TaxonomyModel.fetchNoSpotsTaxonomy.resolves(noSpotsTaxonomyResponse);
      const res = await request.get('/nospots').send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ data: noSpotsTaxonomyResponse });
    });

    it('should return a 400 if a taxonomy with no spots cannot be found', async () => {
      const emptyFixture = [];
      TaxonomyModel.fetchNoSpotsTaxonomy.resolves(emptyFixture);
      try {
        await request.get('/nospots').send();
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
      }
    });
  });
});
