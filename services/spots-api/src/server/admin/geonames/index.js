import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  trackGeonameRequests,
  getCitiesHandler,
  getHierarchyHandler,
} from './geonames';

const geonames = (log) => {
  const api = Router();

  api.use('*', json(), trackGeonameRequests(log));
  api.get('/hierarchy', wrapErrors(getHierarchyHandler));
  api.get('/cities', wrapErrors(getCitiesHandler));

  return api;
};

export default geonames;
