import fetch from 'node-fetch';

export const trackGeonameRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/geonames',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getCitiesHandler = async (req, res) => {
  let geonamesCitiesUrl = 'http://api.geonames.org/citiesJSON';
  geonamesCitiesUrl += `?north=${req.query.north}`;
  geonamesCitiesUrl += `&south=${req.query.south}`;
  geonamesCitiesUrl += `&east=${req.query.east}`;
  geonamesCitiesUrl += `&west=${req.query.west}`;
  geonamesCitiesUrl += `&username=${req.query.username}`;
  geonamesCitiesUrl += '&maxRows=50';
  const cities = await fetch(geonamesCitiesUrl);
  const citiesJson = await cities.json();

  let geonamesIslandsUrl = 'http://api.geonames.org/searchJSON';
  geonamesIslandsUrl += `?north=${req.query.north}`;
  geonamesIslandsUrl += `&south=${req.query.south}`;
  geonamesIslandsUrl += `&east=${req.query.east}`;
  geonamesIslandsUrl += `&west=${req.query.west}`;
  geonamesIslandsUrl += '&featureCode=ISL';
  geonamesIslandsUrl += '&featureCode=ISLS';
  geonamesIslandsUrl += `&username=${req.query.username}`;
  geonamesIslandsUrl += '&maxRows=50';
  const islands = await fetch(geonamesIslandsUrl);
  const islandsJson = await islands.json();

  if (!citiesJson && !islandsJson) return res.status(404).send({ message: 'Cities/Islands not found' });
  return res.send(
    {
      geonames: [
        ...citiesJson.geonames,
        ...islandsJson.geonames,
      ],
    },
  );
};

export const getHierarchyHandler = async (req, res) => {
  let geonamesUrl = 'http://api.geonames.org/hierarchyJSON';
  geonamesUrl += `?geonameId=${req.query.geonameId}`;
  geonamesUrl += `&username=${req.query.username}`;
  const hierarchy = await fetch(geonamesUrl);
  if (!hierarchy) return res.status(404).send({ message: 'Hierarchy not found' });
  return res.send(await hierarchy.json());
};
