import Region, { createRegion } from '../../../model/RegionModel';
import Subregion from '../../../model/SubregionModel';
import { deleteNode } from '../../../model/taxonomy';

export const trackRegionRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/regions',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getRegionHandler = async (req, res) => {
  const region = await Region.findById(req.params.regionId);
  if (!region) return res.status(404).send({ message: 'Region not found' });
  return res.send(region);
};

export const getSubregionsHandler = async (req, res) => {
  const region = await Region.findById(req.params.regionId);
  if (!region) return res.status(404).send({ message: 'Region not found' });
  const subregions = await Subregion.aggregate([
    {
      $match: { region: region._id },
    },
  ])
    .sort({ order: 1 })
    .collation({
      locale: 'en_US',
      numericOrdering: true,
    });
  return res.send(subregions);
};

export const getRegionsHandler = async (req, res) => {
  if (req.query.search) {
    const regions = await Region
      .find({ name: { $regex: req.query.search, $options: 'i' } })
      .sort({ name: 'asc' })
      .limit(20)
      .exec();
    return res.send(regions);
  }
  const regions = await Region.find();
  return res.send(regions);
};

export const createRegionHandler = async (req, res) => {
  const region = await createRegion(req.body);
  if (!region) return res.status(400).send({ message: 'There was a problem creating this region' });
  return res.send(region);
};

export const updateRegionHandler = async (req, res) => {
  const { name } = req.body;
  const region = await Region.findById(req.params.regionId);
  if (!region) return res.status(404).send({ message: 'Region not found' });

  region.name = name;

  await region.save();
  return res.send(region);
};

export const deleteRegionHandler = async (req, res) => {
  const region = await Region.findById(req.params.regionId);
  if (!region) return res.status(404).send({ message: 'Region not found' });
  const subregions = await Subregion.aggregate([
    {
      $match: { region: region._id },
    },
    // waiting on mongoDB upgrade to 3.4
    // {
    //   $count: 'subregions',
    // },
  ]);
  if (subregions.length > 0) return res.status(409).send({ message: 'Region is not empty' });
  const deletedRegion = await Region.findByIdAndRemove(region._id);
  await deleteNode('region', region._id);
  return res.send(deletedRegion);
};
