import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import mongoose from 'mongoose';
import Region, * as regionModel from '../../../model/RegionModel';
import Subregion from '../../../model/SubregionModel';
import * as treeUtils from '../../../model/taxonomy/treeUtils';
import regions from '.';

describe('/admin/regions', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(regions(log));
    request = chai.request(app).keepOpen();
  });

  describe('GET', () => {
    it('should return region by regionId', async () => {
      try {
        const region = {
          name: 'testregion',
        };
        const regionId = new mongoose.Types.ObjectId();
        sinon.stub(Region, 'findById').resolves(region);
        const res = await request.get(`/${regionId.toString()}`).send();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(region);
        expect(Region.findById).to.have.been.calledOnce();
        expect(Region.findById).to.have.been.calledWithExactly(regionId.toString());
      } finally {
        Region.findById.restore();
      }
    });

    it('should return 404 if region not found', async () => {
      try {
        const regionId = new mongoose.Types.ObjectId();
        sinon.stub(Region, 'findById');
        await request.get(`/${regionId.toString()}`).send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Region not found' });
      } finally {
        Region.findById.restore();
      }
    });
  });

  describe('GET /subregions', () => {
    it('should return subregions by regionId', async () => {
      try {
        const regionId = new mongoose.Types.ObjectId();
        const subregions = [
          {
            name: 'SomeSubregion1',
          },
          {
            name: 'SomeSubregion2',
          },
        ];
        sinon.stub(Region, 'findById').resolves(regionId);
        sinon.stub(Subregion, 'aggregate').returns({
          sort: sinon.stub().returns({
            collation: sinon.stub().resolves(subregions),
          }),
        });
        const res = await request.get(`/${regionId.toString()}/subregions`).send();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(subregions);
        expect(Region.findById).to.have.been.calledOnce();
        expect(Region.findById).to.have.been.calledWithExactly(regionId.toString());
        expect(Subregion.aggregate).to.have.been.calledOnce();
      } finally {
        Region.findById.restore();
        Subregion.aggregate.restore();
      }
    });
  });

  describe('POST', () => {
    it('should add region', async () => {
      try {
        const model = {
          name: 'Test Region',
        };

        sinon.stub(regionModel, 'createRegion').resolves(model);
        const res = await request.post('/').send(model);

        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(model);
        expect(regionModel.createRegion).to.have.been.calledOnce();
        expect(regionModel.createRegion).to.have.been.calledWithExactly(model);
      } finally {
        regionModel.createRegion.restore();
      }
    });
  });

  describe('PUT', () => {
    it('should find and update region by id', async () => {
      try {
        const model = {
          _id: 'malicious attempt to change id',
          name: 'Test Region',
        };

        const existingModel = {
          _id: 'testregionid',
          save: sinon.stub().resolves(),
        };

        const resultModel = {
          _id: 'testregionid',
          name: 'Test Region',
        };

        sinon.stub(Region, 'findById').resolves(existingModel);

        const res = await request.put('/testregionid').send(model);
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(resultModel);
        expect(existingModel).to.deep.equal({
          ...existingModel,
          ...resultModel,
        });
        expect(Region.findById).to.have.been.calledOnce();
        expect(Region.findById).to.have.been.calledWithExactly('testregionid');
        expect(existingModel.save).to.have.been.calledOnce();
      } finally {
        Region.findById.restore();
      }
    });

    it('should return 404 if region not found', async () => {
      try {
        const region = {};
        sinon.stub(Region, 'findById').resolves(null);
        await request.put('/testregionid').send(region);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Region not found' });
      } finally {
        Region.findById.restore();
      }
    });
  });

  describe('DELETE', () => {
    it('should delete region by regionId', async () => {
      try {
        const model = { name: 'some region' };
        const subregions = [];

        sinon.stub(Region, 'findById').resolves(model);
        sinon.stub(Region, 'findByIdAndRemove').resolves(model);
        sinon.stub(Subregion, 'aggregate').resolves(subregions);
        sinon.stub(treeUtils, 'deleteNode').resolves({});
        const res = await request.delete('/testregionid').send();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(model);
        expect(Region.findByIdAndRemove).to.have.been.calledOnce();
      } finally {
        Region.findById.restore();
        Region.findByIdAndRemove.restore();
        Subregion.aggregate.restore();
        treeUtils.deleteNode.restore();
      }
    });

    it('should return 409 if region has subregions', async () => {
      try {
        const model = { name: 'some region' };
        const subregions = [{ name: 'testsubregion' }];

        sinon.stub(Region, 'findById').resolves(model);
        sinon.stub(Subregion, 'aggregate').resolves(subregions);
        sinon.stub(Region, 'findByIdAndRemove').resolves(null);
        sinon.stub(treeUtils, 'deleteNode').resolves({});
        await request.delete('/testregionid').send();
      } catch (err) {
        expect(err.response).to.have.status(409);
        expect(Region.findByIdAndRemove).not.to.have.been.called();
      } finally {
        Region.findById.restore();
        Region.findByIdAndRemove.restore();
        Subregion.aggregate.restore();
        treeUtils.deleteNode.restore();
      }
    });

    it('should return 404 if region not found', async () => {
      try {
        sinon.stub(Region, 'findById').resolves(null);
        sinon.stub(Subregion, 'aggregate').resolves(null);
        sinon.stub(Region, 'findByIdAndRemove').resolves(null);
        await request.delete('/testregionid').send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Region not found' });
      } finally {
        Region.findById.restore();
        Region.findByIdAndRemove.restore();
        Subregion.aggregate.restore();
      }
    });
  });
});
