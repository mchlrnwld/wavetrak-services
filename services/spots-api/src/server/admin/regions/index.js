import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  trackRegionRequests,
  getRegionHandler,
  getRegionsHandler,
  getSubregionsHandler,
  createRegionHandler,
  updateRegionHandler,
  deleteRegionHandler,
} from './regions';

const regions = (log) => {
  const api = Router();
  api.use('*', json(), trackRegionRequests(log));
  api.get('/:regionId', wrapErrors(getRegionHandler));
  api.get('/:regionId/subregions', wrapErrors(getSubregionsHandler));
  api.get('/', wrapErrors(getRegionsHandler));
  api.post('/', wrapErrors(createRegionHandler));
  api.put('/:regionId', wrapErrors(updateRegionHandler));
  api.delete('/:regionId', wrapErrors(deleteRegionHandler));

  return api;
};

export default regions;
