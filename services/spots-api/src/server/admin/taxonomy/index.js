import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  trackTaxonomyRequests,
  createTaxonomyHandler,
  deleteTaxonomyHandler,
  patchTaxonomyHandler,
} from './taxonomy';

const taxonomy = (log) => {
  const api = Router();
  api.get('/health', (req, res) => res.send({
    status: 200,
    message: 'OK',
    version: process.env.APP_VERSION || 'unknown',
  }));

  api.use('*', json(), trackTaxonomyRequests(log));
  api.post('/', wrapErrors(createTaxonomyHandler));
  api.delete('/:taxonomyId', wrapErrors(deleteTaxonomyHandler));
  api.patch('/:taxonomyId', wrapErrors(patchTaxonomyHandler));

  return api;
};

export default taxonomy;
