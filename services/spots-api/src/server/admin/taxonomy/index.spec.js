import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import * as geonames from '../../../common/geonames';
import * as taxonomyModel from '../../../model/taxonomy/taxonomy';
import * as treeUtils from '../../../model/taxonomy/treeUtils';
import * as taxonomyGeoname from '../../../model/taxonomy/geoname';
import * as taxonomySchema from '../../../model/taxonomy/schemas/taxonomy';
import taxonomy from '.';

describe('/admin/taxonomy', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(taxonomy(log));
    request = chai.request(app).keepOpen();
  });

  describe('POST', () => {
    const body = {
      name: 'Huntington Beach',
      category: 'geonames',
      type: 'geoname',
      geonameId: '5358705',
      liesIn: ['58f7ed5ddadb30820bb39651'],
      location: {
        type: 'Point',
        coordinates: [-117.99923, 33.6603],
      },
    };

    beforeEach(() => {
      sinon.stub(treeUtils, 'getEnumeratedPath');
      sinon.stub(geonames, 'getHierarchy');
      sinon.stub(taxonomyGeoname, 'createGeoname');
    });

    afterEach(() => {
      treeUtils.getEnumeratedPath.restore();
      geonames.getHierarchy.restore();
      taxonomyGeoname.createGeoname.restore();
    });

    it('should create geoname taxonomy', async () => {
      const huntingtonBeach = {
        name: 'Huntington Beach',
        lat: '33.6603',
        lng: '-117.99923',
      };
      const hierarchy = [
        { name: 'Earth' },
        { name: 'North America' },
        { name: 'United States' },
        { name: 'California' },
        { name: 'Orange County' },
        huntingtonBeach,
      ];
      const enumeratedPath = ',Earth,North America,United States,California,Orange County';
      geonames.getHierarchy.resolves(hierarchy);
      treeUtils.getEnumeratedPath.resolves(enumeratedPath);
      const model = {
        ...body,
        enumeratedPath: `${enumeratedPath},${huntingtonBeach.name}`,
        geonames: huntingtonBeach,
        location: {
          type: 'Point',
          coordinates: [-117.99923, 33.6603],
        },
        hasSpots: false,
      };
      taxonomyGeoname.createGeoname.resolves(model);
      const res = await request.post('/').send(body);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(model);
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(body.geonameId);
      expect(taxonomyGeoname.createGeoname).to.have.been.calledOnce();
      expect(taxonomyGeoname.createGeoname).to.have.been.calledWithExactly(model);
    });

    it('should return 400 for geoname if geonameId hierarchy is not found', async () => {
      geonames.getHierarchy.resolves([{ name: 'Earth' }]);

      try {
        const res = await request.post('/').send(body);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({ message: 'Invalid geonameId' });
        expect(geonames.getHierarchy).to.have.been.calledOnce();
        expect(geonames.getHierarchy).to.have.been.calledWithExactly(body.geonameId);
        expect(taxonomyGeoname.createGeoname).not.to.have.been.called();
      }
    });

    it('should return 400 for surfline category', async () => {
      try {
        const res = await request.post('/').send({ category: 'surfline' });
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Create surfline taxonomy not allowed',
        });
        expect(geonames.getHierarchy).not.to.have.been.called();
        expect(taxonomyGeoname.createGeoname).not.to.have.been.called();
      }
    });
  });

  describe('DELETE', () => {
    const taxonomyId = '58f7ed5fdadb30820bb3987c';

    beforeEach(() => {
      sinon.stub(taxonomyModel, 'default');
      sinon.stub(taxonomyModel, 'deleteTaxonomyById');
    });

    afterEach(() => {
      taxonomyModel.default.restore();
      taxonomyModel.deleteTaxonomyById.restore();
    });

    it('should remove a geoname taxonomy', async () => {
      const model = {
        _id: taxonomyId,
        category: 'geonames',
        contains: [],
      };
      taxonomyModel.default.resolves([model]);
      const res = await request.delete(`/${taxonomyId}`).send();
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ message: 'OK' });
      expect(taxonomyModel.default).to.have.been.calledOnce();
      expect(taxonomyModel.default).to.have.been.calledWithExactly(
        taxonomySchema.types.TAXONOMY,
        taxonomyId,
        0,
      );
      expect(taxonomyModel.deleteTaxonomyById).to.have.been.calledOnce();
      expect(taxonomyModel.deleteTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
    });

    it('should return 404 if taxonomy is not found', async () => {
      taxonomyModel.default.resolves([]);

      try {
        const res = await request.delete(`/${taxonomyId}`).send();
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err).to.have.status(404);
        expect(err.response.body).to.deep.equal({
          message: 'Taxonomy not found',
        });
        expect(taxonomyModel.default).to.have.been.calledOnce();
        expect(taxonomyModel.default).to.have.been.calledWithExactly(
          taxonomySchema.types.TAXONOMY,
          taxonomyId,
          0,
        );
        expect(taxonomyModel.deleteTaxonomyById).not.to.have.been.called();
      }
    });

    it('should return 400 for surfline category', async () => {
      const model = {
        _id: taxonomyId,
        category: 'surfline',
        contains: [],
      };
      taxonomyModel.default.resolves([model]);

      try {
        const res = await request.delete(`/${taxonomyId}`).send();
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Delete surfline taxonomy not allowed',
        });
        expect(taxonomyModel.default).to.have.been.calledOnce();
        expect(taxonomyModel.default).to.have.been.calledWithExactly(
          taxonomySchema.types.TAXONOMY,
          taxonomyId,
          0,
        );
        expect(taxonomyModel.deleteTaxonomyById).not.to.have.been.called();
      }
    });

    it('should return 400 when trying to delete a taxonomy with children', async () => {
      const model = {
        _id: taxonomyId,
        category: 'geonames',
        contains: [{}],
      };
      taxonomyModel.default.resolves([model]);

      try {
        const res = await request.delete(`/${taxonomyId}`).send();
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Delete taxonomy with children not allowed',
        });
        expect(taxonomyModel.default).to.have.been.calledOnce();
        expect(taxonomyModel.default).to.have.been.calledWithExactly(
          taxonomySchema.types.TAXONOMY,
          taxonomyId,
          0,
        );
        expect(taxonomyModel.deleteTaxonomyById).not.to.have.been.called();
      }
    });
  });

  describe('PATCH', () => {
    const taxonomyId = '58f7ed5fdadb30820bb3987c';
    const updates = {
      _id: 'fake id', // ignored
      geonameId: 12345, // ignored
      name: 'Huntington Beach UPDATED',
      liesIn: ['58f7ed5bdadb30820bb393cd'],
    };

    beforeEach(() => {
      sinon.stub(taxonomyModel, 'findTaxonomyById');
      sinon.stub(taxonomyModel, 'findTaxonomyByIds');
      sinon.stub(treeUtils, 'markTaxonomyHasSpots');
      sinon.stub(treeUtils, 'getEnumeratedPath');
      sinon.stub(geonames, 'getHierarchy');
    });

    afterEach(() => {
      taxonomyModel.findTaxonomyById.restore();
      taxonomyModel.findTaxonomyByIds.restore();
      treeUtils.markTaxonomyHasSpots.restore();
      treeUtils.getEnumeratedPath.restore();
      geonames.getHierarchy.restore();
    });

    it('updates name and/or liesIn property of a taxonomy', async () => {
      const original = {
        category: 'geonames',
        liesIn: ['58f7ed5ddadb30820bb39651'],
        save: sinon.spy(),
      };
      const hierarchy = [
        { name: 'Earth' },
        { name: 'North America' },
        { name: 'United States' },
        { name: 'California' },
        { name: 'Orange County', lat: '33.6603', lng: '-117.99923' },
      ];
      const enumeratedPath = ',Earth,North America,United States,California,Orange County';
      const result = {
        category: 'geonames',
        enumeratedPath: `${enumeratedPath},${updates.name}`,
        geonames: hierarchy[hierarchy.length - 1],
        location: {
          type: 'Point',
          coordinates: [-117.99923, 33.6603],
        },
        name: updates.name,
        liesIn: updates.liesIn,
      };
      taxonomyModel.findTaxonomyById.resolves(original);
      taxonomyModel.findTaxonomyByIds.resolves(updates.liesIn.map((_id) => ({ _id })));
      treeUtils.getEnumeratedPath.resolves(enumeratedPath);
      geonames.getHierarchy.resolves(hierarchy);
      const res = await request.patch(`/${taxonomyId}`).send(updates);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(result);
      expect(taxonomyModel.findTaxonomyById).to.have.been.calledOnce();
      expect(taxonomyModel.findTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
      expect(taxonomyModel.findTaxonomyByIds).to.have.been.calledOnce();
      expect(taxonomyModel.findTaxonomyByIds).to.have.been.calledWithExactly(updates.liesIn);
      expect(original.save).to.have.been.calledOnce();
      expect(treeUtils.markTaxonomyHasSpots).to.have.been.calledTwice();
      expect(treeUtils.markTaxonomyHasSpots).to.have.been.calledWith('58f7ed5ddadb30820bb39651');
      expect(treeUtils.markTaxonomyHasSpots).to.have.been.calledWith('58f7ed5bdadb30820bb393cd');
    });

    it('returns 404 if taxonomy not found', async () => {
      try {
        const res = await request.patch(`/${taxonomyId}`).send(updates);
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err).to.have.status(404);
        expect(err.response.body).to.deep.equal({
          message: 'Taxonomy not found',
        });
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledOnce();
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
        expect(taxonomyModel.findTaxonomyByIds).not.to.have.been.called();
        expect(treeUtils.markTaxonomyHasSpots).not.to.have.been.called();
      }
    });

    it('returns 400 if found taxonomy is not a geoname', async () => {
      const original = {
        category: 'surfline',
        save: sinon.spy(),
      };
      taxonomyModel.findTaxonomyById.resolves(original);

      try {
        const res = await request.patch(`/${taxonomyId}`).send({ liesIn: {} });
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Patch surfline taxonomy not allowed',
        });
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledOnce();
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
        expect(taxonomyModel.findTaxonomyByIds).not.to.have.been.called();
        expect(original.save).not.to.have.been.called();
        expect(treeUtils.markTaxonomyHasSpots).not.to.have.been.called();
      }
    });

    it('returns 400 if liesIn is not an array', async () => {
      const original = {
        category: 'geonames',
        save: sinon.spy(),
      };
      taxonomyModel.findTaxonomyById.resolves(original);

      try {
        const res = await request.patch(`/${taxonomyId}`).send({ liesIn: {} });
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Invalid liesIn IDs',
        });
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledOnce();
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
        expect(taxonomyModel.findTaxonomyByIds).not.to.have.been.called();
        expect(original.save).not.to.have.been.called();
        expect(treeUtils.markTaxonomyHasSpots).not.to.have.been.called();
      }
    });

    it('returns 400 if liesIn taxonomy IDs are invalid ObjectIDs', async () => {
      const original = {
        category: 'geonames',
        save: sinon.spy(),
      };
      taxonomyModel.findTaxonomyById.resolves(original);

      try {
        const res = await request.patch(`/${taxonomyId}`).send({ liesIn: ['invalidobjectid'] });
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Invalid liesIn IDs',
        });
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledOnce();
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
        expect(taxonomyModel.findTaxonomyByIds).not.to.have.been.called();
        expect(original.save).not.to.have.been.called();
        expect(treeUtils.markTaxonomyHasSpots).not.to.have.been.called();
      }
    });

    it('returns 400 if liesIn taxonomy IDs do not exist in database', async () => {
      const original = {
        category: 'geonames',
        save: sinon.spy(),
      };
      taxonomyModel.findTaxonomyById.resolves(original);
      taxonomyModel.findTaxonomyByIds.resolves([]);

      try {
        const res = await request.patch(`/${taxonomyId}`).send(updates);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({
          message: 'Invalid liesIn IDs',
        });
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledOnce();
        expect(taxonomyModel.findTaxonomyById).to.have.been.calledWithExactly(taxonomyId);
        expect(taxonomyModel.findTaxonomyByIds).to.have.been.calledOnce();
        expect(taxonomyModel.findTaxonomyByIds).to.have.been.calledWithExactly(updates.liesIn);
        expect(original.save).not.to.have.been.called();
        expect(treeUtils.markTaxonomyHasSpots).not.to.have.been.called();
      }
    });
  });
});
