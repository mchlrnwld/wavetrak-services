/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { Types } from 'mongoose';
import { difference } from 'lodash/fp';
import { getHierarchy } from '../../../common/geonames';
import { createGeoJsonPoint } from '../../../common/geojson';
import {
  createGeoname,
  fetchTaxonomy,
  findTaxonomyById,
  findTaxonomyByIds,
  deleteTaxonomyById,
  types,
} from '../../../model/taxonomy';
import { markTaxonomyHasSpots, getEnumeratedPath } from '../../../model/taxonomy/treeUtils';

const location = (geonames) => createGeoJsonPoint([
  parseFloat(geonames.lng),
  parseFloat(geonames.lat),
]);

export const trackTaxonomyRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/taxonomy',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const createTaxonomyHandler = async (req, res) => {
  const { body } = req;

  if (body.category === 'geonames') {
    const hierarchy = await getHierarchy(body.geonameId);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geonameId',
      });
    }

    const geonames = hierarchy[hierarchy.length - 1];

    const enumeratedPath = await getEnumeratedPath(body.liesIn);
    const model = {
      ...body,
      enumeratedPath: `${enumeratedPath},${body.name}`,
      geonames,
      location: location(geonames),
      hasSpots: false,
    };
    const geoname = await createGeoname(model);
    return res.send(geoname);
  }

  return res.status(400).send({
    message: 'Create surfline taxonomy not allowed',
  });
};

export const deleteTaxonomyHandler = async (req, res) => {
  const { taxonomyId } = req.params;

  const taxonomyList = await fetchTaxonomy(types.TAXONOMY, taxonomyId, 0);
  if (taxonomyList.length === 0) {
    return res.status(404).send({
      message: 'Taxonomy not found',
    });
  }

  const taxonomy = taxonomyList[0];

  if (taxonomy.category !== 'geonames') {
    return res.status(400).send({
      message: 'Delete surfline taxonomy not allowed',
    });
  }

  if (taxonomy.contains.length > 0) {
    return res.status(400).send({
      message: 'Delete taxonomy with children not allowed',
    });
  }

  await deleteTaxonomyById(taxonomyId);

  return res.send({ message: 'OK' });
};

export const patchTaxonomyHandler = async (req, res) => {
  const { taxonomyId } = req.params;
  const { name, liesIn } = req.body;
  const foundTaxonomy = await findTaxonomyById(taxonomyId);

  if (!foundTaxonomy) {
    return res.status(404).send({
      message: 'Taxonomy not found',
    });
  }

  if (foundTaxonomy.category !== 'geonames') {
    return res.status(400).send({
      message: 'Patch surfline taxonomy not allowed',
    });
  }

  if (liesIn && (
    !Array.isArray(liesIn)
    || liesIn.map(Types.ObjectId.isValid).some((value) => !value)
    || liesIn.length !== (await findTaxonomyByIds(liesIn)).length
  )) {
    return res.status(400).send({
      message: 'Invalid liesIn IDs',
    });
  }

  const originalLiesIn = foundTaxonomy.liesIn;

  foundTaxonomy.name = name || foundTaxonomy.name;
  foundTaxonomy.liesIn = liesIn || foundTaxonomy.liesIn;

  const [hierarchy, enumeratedPath] = await Promise.all([
    getHierarchy(foundTaxonomy.geonameId),
    getEnumeratedPath(liesIn),
  ]);

  const geonames = hierarchy[hierarchy.length - 1];
  foundTaxonomy.geonames = geonames;
  foundTaxonomy.location = location(geonames);

  if (enumeratedPath) {
    foundTaxonomy.enumeratedPath = `${enumeratedPath},${foundTaxonomy.name}`;
  }

  await foundTaxonomy.save();

  const taxonomyToNormalize = [
    ...difference(foundTaxonomy.liesIn, originalLiesIn),
    ...difference(originalLiesIn, foundTaxonomy.liesIn),
  ];

  // Loop through instead of Promise.all to avoid race condition
  for (const id of taxonomyToNormalize) {
    await markTaxonomyHasSpots(id);
  }

  return res.send(foundTaxonomy);
};
