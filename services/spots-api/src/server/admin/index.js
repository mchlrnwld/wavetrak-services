import { Router } from 'express';
import regions from './regions';
import subregions from './subregions';
import spots from './spots';
import geonames from './geonames';
import taxonomy from './taxonomy';

const admin = (log) => {
  const api = Router();

  api.get('/health', (_, res) => res.send(
    {
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    },
  ));

  api.use('/regions', regions(log));
  api.use('/subregions', subregions(log));
  api.use('/spots', spots(log));
  api.use('/geonames', geonames(log));
  api.use('/taxonomy', taxonomy(log));

  return api;
};

export default admin;
