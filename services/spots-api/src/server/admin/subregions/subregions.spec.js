import chai, { expect } from 'chai';
import mongoose from 'mongoose';
import sinon from 'sinon';
import express from 'express';
import { MongoMemoryServer } from 'mongodb-memory-server';
import Subregion from '../../../model/SubregionModel';
import subregions from '.';

describe('/admin/subregions', () => {
  let mongoServer;
  let request;
  let log;

  before(async () => {
    log = { trace: sinon.spy() };

    mongoServer = await MongoMemoryServer.create();

    const mongoUri = await mongoServer.getUri();

    await mongoose.connect(
      mongoUri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err) => err,
    );

    const app = express();
    app.use(subregions(log));
    request = chai.request(app).keepOpen();
  });

  after(async () => {
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  afterEach(async () => {
    await Subregion.deleteMany();
  });

  describe('GET', () => {
    it('should return subregion by subregionId', async () => {
      const model = {
        name: 'Test Subregion',
        spots: [{ _id: new mongoose.Types.ObjectId() }],
        regionId: new mongoose.Types.ObjectId(),
        forecastStatus: {
          status: 'inactive',
        },
      };

      const result = await mongoose.connection.collections.Subregions.insertOne(model);

      const subregionId = result.ops[0]._id;

      const res = await request.get(`/${subregionId}`).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body.name).to.equal('Test Subregion');
      expect(res.body.forecastStatus).to.be.ok();
      expect(res.body.forecastStatus.status).to.equal('inactive');
      expect(res.body.forecastStatus.inactiveMessage).to.equal('Inactive status message');
    });
  });
});
