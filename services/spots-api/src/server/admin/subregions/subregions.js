import mongoose from 'mongoose';
import _ from 'lodash';
import { pick } from 'lodash/fp';
import { NotFound } from '@surfline/services-common';
import { getHierarchy } from '../../../common/geonames';
import { createGeoJsonPoint } from '../../../common/geojson';
import Subregion, { createSubregion } from '../../../model/SubregionModel';
import Spot from '../../../model/SpotModel';
import {
  normalizeSubregion, deleteNode, removeEdge, types,
} from '../../../model/taxonomy';

const NOT_FOUND = { message: 'Subregion not found' };

export const trackSubregionRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/subregions',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const checkSubregionId = (req, res, next) => {
  const { subregionId } = req.params;
  if (!mongoose.Types.ObjectId.isValid(subregionId)) return res.status(404).send(NOT_FOUND);
  return next();
};

export const getSubregionHandler = async (req, res) => {
  const subregion = await Subregion.findById(req.params.subregionId).populate('spots.spot');

  const legacyId = subregion.legacyId ? subregion.legacyId : null;
  const spots = subregion.spots ? subregion.spots : [];

  const subregionResponse = {
    _id: subregion.id,
    name: subregion.name,
    status: subregion.status,
    forecastStatus: subregion.forecastStatus,
    region: subregion.region,
    geoname: subregion.geoname,
    offshoreSwellLocation: subregion.offshoreSwellLocation,
    order: subregion.order,
    primarySpot: subregion.primarySpot,
    basins: subregion.imported.basins,
    spots,
    legacyId,
  };

  if (!subregion) return res.status(404).send(NOT_FOUND);
  return res.send(subregionResponse);
};

export const getNearestSubregionsHandler = async (req, res) => {
  const { maxDistance } = req.query;

  const start = await Spot.findOne({
    subregion: new mongoose.mongo.ObjectID(req.params.subregionId),
  });
  if (!start) return res.status(404).send(NOT_FOUND);

  const nearbySpots = await Spot.find({
    location: {
      $near: {
        $geometry: start.location,
        $maxDistance: maxDistance || 321869, // 200 mi
      },
    },
  });

  const subregionIds = nearbySpots.map(({ subregion }) => new mongoose.mongo.ObjectID(subregion));

  const subregions = await Subregion.find({ _id: { $in: subregionIds } }).select('name');

  return res.send(
    subregions.map((subregion) => ({
      subregionId: subregion.id,
      subregionName: subregion.name,
    })),
  );
};

export const addSubregionHandler = async (req, res) => {
  if (req.body.geoname) {
    const hierarchy = await getHierarchy(req.body.geoname);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geoname',
      });
    }
  }

  const subregion = await createSubregion(req.body);
  await normalizeSubregion(subregion);
  return res.send(subregion);
};

export const updateSubregionHandler = async (req, res) => {
  const {
    name, region, primarySpot, geoname, offshoreSwellLocation, basin, status, order,
  } = req.body;
  const subregion = await Subregion.findById(req.params.subregionId);
  if (!subregion) return res.status(404).send(NOT_FOUND);

  if (geoname) {
    const hierarchy = await getHierarchy(geoname);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geoname',
      });
    }
  }

  if (subregion.geoname && subregion.geoname !== geoname) {
    await removeEdge([types.SUBREGION, subregion._id], [types.GEONAME, subregion.geoname]);
  }

  if (subregion.region && subregion.region.toString() !== region) {
    await removeEdge([types.SUBREGION, subregion._id], [types.REGION, subregion.region]);
  }

  const subregionOffshoreSwellLocation = offshoreSwellLocation
    ? createGeoJsonPoint(offshoreSwellLocation.coordinates)
    : null;

  subregion.name = name;
  subregion.region = region;
  subregion.primarySpot = primarySpot;
  subregion.geoname = geoname;
  subregion.offshoreSwellLocation = subregionOffshoreSwellLocation;
  subregion.status = status;
  subregion.order = order;

  if (basin) {
    const { basins } = subregion.imported;
    if (!basins) {
      subregion.imported.basins = [basin];
    } else if (basins.indexOf(basin) === -1) {
      basins.push(basin);
    }
    subregion.markModified('imported');
  }

  await subregion.save();
  await normalizeSubregion(subregion);
  return res.send(subregion);
};

export const patchSubregionHandler = async (req, res) => {
  const subregion = await Subregion.findById(req.params.subregionId);
  if (!subregion) throw new NotFound('Subregion not found');

  const {
    region, geoname, basin, forecastStatus,
  } = req.body;

  if (geoname) {
    const hierarchy = await getHierarchy(geoname);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geoname',
      });
    }

    if (subregion.geoname && subregion.geoname !== geoname) {
      await removeEdge([types.SUBREGION, subregion._id], [types.GEONAME, subregion.geoname]);
    }
  }

  if (region && subregion.region && subregion.region.toString() !== region) {
    await removeEdge([types.SUBREGION, subregion._id], [types.REGION, subregion.region]);
  }

  ['name', 'region', 'primarySpot', 'geoname', 'status', 'order'].forEach((fieldName) => {
    if (req.body[fieldName] !== undefined) {
      subregion[fieldName] = req.body[fieldName];
    }
  });

  ['offshoreSwellLocation'].forEach((fieldName) => {
    if (req.body[fieldName] === null) {
      subregion[fieldName] = null;
    } else if (req.body[fieldName] !== undefined) {
      subregion[fieldName] = createGeoJsonPoint(req.body[fieldName]);
    }
  });

  if (basin) {
    const { basins } = subregion.imported;
    if (!basins) {
      subregion.imported.basins = [basin];
    } else if (basins.indexOf(basin) === -1) {
      basins.push(basin);
    }
    subregion.markModified('imported');
  }

  if (forecastStatus) {
    ['status', 'inactiveMessage'].forEach((fieldName) => {
      if (forecastStatus[fieldName] !== undefined) {
        subregion.forecastStatus[fieldName] = forecastStatus[fieldName];
      }
    });
  }

  await subregion.save();
  await normalizeSubregion(subregion);
  return res.send(subregion);
};

export const deleteSubregionBasinHandler = async (req, res) => {
  const subregion = await Subregion.findById(req.params.subregionId);
  if (!subregion) return res.status(404).send(NOT_FOUND);

  const { basins } = subregion.imported;
  if (!basins) return res.status(400).send('Subregion has no basins');

  _.remove(basins, (basin) => basin === req.body.basin);

  subregion.markModified('imported');

  await subregion.save();
  await normalizeSubregion(subregion);
  return res.send(subregion);
};

export const deleteSubregionHandler = async (req, res) => {
  const subregion = await Subregion.findByIdAndRemove(req.params.subregionId);
  if (!subregion) return res.status(404).send(NOT_FOUND);
  await deleteNode('subregion', subregion._id);
  return res.send(subregion);
};

export const getSubregionsHandler = async (req, res) => {
  const { search, thirdParty } = req.query;
  if (search) {
    const subregions = await Subregion.find({ name: { $regex: search, $options: 'i' } })
      .sort({ name: 'asc' })
      .limit(20)
      .exec();
    return res.send(subregions);
  }
  let subregions = await Subregion.find({ status: 'PUBLISHED' }).sort({ name: 'asc' });
  // return only name and _id for 3rd party requests
  if (thirdParty) subregions = subregions.map(pick(['name', '_id']));
  return res.send(subregions);
};

export const getSpotsHandler = async (req, res) => {
  const subregion = await Subregion.findById(req.params.subregionId);
  if (!subregion) return res.status(404).send(NOT_FOUND);
  const spots = await Spot.find({ subregion: subregion._id, status: { $ne: 'DELETED' } }).sort({
    rank: 1,
  });
  return res.send(spots);
};
