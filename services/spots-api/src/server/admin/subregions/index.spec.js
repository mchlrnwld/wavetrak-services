import chai, { expect } from 'chai';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import sinon from 'sinon';
import express from 'express';
import * as geonames from '../../../common/geonames';
import Region from '../../../model/RegionModel';
import Subregion, * as subregionModel from '../../../model/SubregionModel';
import Spot from '../../../model/SpotModel';
import subregions from '.';
import * as taxonomy from '../../../model/taxonomy/taxonomy';
import * as normalize from '../../../model/taxonomy/normalize';
import * as treeUtils from '../../../model/taxonomy/treeUtils';

describe('/admin/subregions', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(subregions(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(normalize, 'normalizeSubregion');
    sinon.stub(taxonomy, 'removeEdge');
    sinon.stub(Subregion, 'find');
    sinon.stub(Spot, 'findOne');
    sinon.stub(Spot, 'find');
    sinon.stub(geonames, 'getHierarchy');
  });

  afterEach(() => {
    normalize.normalizeSubregion.restore();
    taxonomy.removeEdge.restore();
    Subregion.find.restore();
    Spot.findOne.restore();
    Spot.find.restore();
    geonames.getHierarchy.restore();
  });

  describe('GET', () => {
    it('should return subregion by subregionId', async () => {
      try {
        const model = {
          name: 'testsubregion',
          legacyId: null,
          spots: [],
          imported: {
            basins: [],
          },
        };

        const response = {
          name: 'testsubregion',
          legacyId: null,
          spots: [],
          basins: [],
        };

        const subregionId = new mongoose.Types.ObjectId();

        const populate = sinon.stub().resolves(model);
        sinon.stub(Subregion, 'findById').returns({ populate });

        const res = await request.get(`/${subregionId.toString()}`).send();

        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal(response);
        expect(Subregion.findById).to.have.been.calledOnce();
        expect(Subregion.findById).to.have.been.calledWithExactly(subregionId.toString());
        expect(populate).to.have.been.calledOnce();
        expect(populate).to.have.been.calledWithExactly('spots.spot');
      } finally {
        Subregion.findById.restore();
      }
    });

    it('should return only name and _id for third party requests', async () => {
      Subregion.find.returns({
        sort: sinon.stub().resolves([
          {
            _id: '5842041f4e65fad6a77088ed',
            name: 'HB Southside',
            status: 'PUBLISHED',
            spots: [],
            imported: {
              basins: [],
            },
          },
        ]),
      });

      const res = await request.get('/?thirdParty=true').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal([
        {
          _id: '5842041f4e65fad6a77088ed',
          name: 'HB Southside',
        },
      ]);
    });

    it('should return nearest subregions', async () => {
      Spot.findOne.resolves({
        location: {
          coordinates: [-117.3502922058, 33.14948100706],
          type: 'Point',
        },
      });

      Spot.find.resolves([
        { subregion: '58581a836630e24c4487900b' },
        { subregion: '58581a836630e24c4487900c' },
      ]);

      Subregion.find.returns({
        select: sinon.stub().resolves([
          { id: '58581a836630e24c4487900b', name: 'South Los Angeles' },
          { id: '58581a836630e24c4487900c', name: 'Ventura' },
        ]),
      });

      const res = await request.get('/nearest/58581a836630e24c44878fd6').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal([
        {
          subregionId: '58581a836630e24c4487900b',
          subregionName: 'South Los Angeles',
        },
        {
          subregionId: '58581a836630e24c4487900c',
          subregionName: 'Ventura',
        },
      ]);
    });

    it('should return 404 if subregion not found', async () => {
      Spot.findOne.returns(null);
      try {
        await request.get('/nearest/58581a836630e24c44878fd6').send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
        expect(Spot.findOne).to.have.been.calledOnce();
      }
    });

    it('should return 404 if subregion ID invalid', async () => {
      try {
        await request.get('/nearest/invalidsubregionid').send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
        expect(Spot.findOne).not.to.have.been.called();
      }
    });
  });

  describe('POST', () => {
    beforeEach(() => {
      sinon.stub(subregionModel, 'createSubregion');
    });

    afterEach(() => {
      subregionModel.createSubregion.restore();
    });

    it('should add subregion', async () => {
      const model = {
        name: 'Test Subregion',
        spots: [
          { spot: 'some spot id', rank: 2 },
          { spot: 'another spot id', rank: 1 },
        ],
        geoname: 12345,
      };

      geonames.getHierarchy.resolves([{}, {}]);
      subregionModel.createSubregion.resolves(model);
      const res = await request.post('/').send(model);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(model);
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(12345);
      expect(subregionModel.createSubregion).to.have.been.calledOnce();
      expect(subregionModel.createSubregion).to.have.been.calledWithExactly(model);
    });

    it('should return 400 if geoname is invalid', async () => {
      const model = { geoname: 12345 };
      geonames.getHierarchy.resolves([{}]);

      try {
        const res = await request.post('/').send(model);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body).to.deep.equal({ message: 'Invalid geoname' });
        expect(geonames.getHierarchy).to.have.been.calledOnce();
        expect(geonames.getHierarchy).to.have.been.calledWithExactly(12345);
        expect(subregionModel.createSubregion).not.to.have.been.called();
      }
    });
  });

  describe('PUT', () => {
    beforeEach(() => {
      sinon.stub(Subregion, 'findById');
    });

    afterEach(() => {
      Subregion.findById.restore();
    });

    it('should find and update subregion by id', async () => {
      taxonomy.removeEdge.resolves({});

      const existingModel = {
        _id: new mongoose.Types.ObjectId(),
        save: sinon.stub().resolves(),
      };
      const model = {
        _id: new mongoose.Types.ObjectId(),
        name: 'Test Subregion',
        region: 'Some Region',
        geoname: 12345,
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [0, 0],
        },
      };
      const resultModel = {
        _id: existingModel._id,
        name: 'Test Subregion',
        region: 'Some Region',
        geoname: 12345,
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [0, 0],
        },
      };
      const _id = `${existingModel._id}`;

      geonames.getHierarchy.resolves([{}, {}]);
      Subregion.findById.resolves(existingModel);

      const res = await request.put(`/${_id}`).send(model);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...resultModel,
        _id,
      });
      expect(existingModel).to.deep.equal({
        ...existingModel,
        ...resultModel,
      });
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(12345);
      expect(Subregion.findById).to.have.been.calledOnce();
      expect(Subregion.findById).to.have.been.calledWithExactly(_id);
      expect(existingModel.save).to.have.been.calledOnce();
    });

    it('should return 404 if subregion not found', async () => {
      const model = {};

      try {
        const res = await request.put(`/${new mongoose.Types.ObjectId()}`).send(model);
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
        expect(Subregion.findById).to.have.been.calledOnce();
      }
    });

    it('should return 404 if subregion ID invalid', async () => {
      const model = {};

      try {
        const res = await request.put('/invalidsubregionid').send(model);
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
        expect(Subregion.findById).not.to.have.been.called();
      }
    });

    it('should return 400 if geoname is invalid', async () => {
      const model = { geoname: 12345 };
      const existingModel = {
        _id: new mongoose.Types.ObjectId(),
        save: sinon.stub().resolves(),
      };
      const _id = `${existingModel._id}`;

      Subregion.findById.resolves(existingModel);
      geonames.getHierarchy.resolves([{}]);

      try {
        const res = await request.put(`/${_id}`).send(model);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response.body).to.deep.equal({ message: 'Invalid geoname' });
        expect(Subregion.findById).to.have.been.calledOnce();
        expect(Subregion.findById).to.have.been.calledWithExactly(_id);
        expect(geonames.getHierarchy).to.have.been.calledOnce();
        expect(geonames.getHierarchy).to.have.been.calledWithExactly(12345);
        expect(existingModel.save).not.to.have.been.called();
      }
    });
  });

  describe('PATCH', () => {
    let mongoServer;
    const subregionId = new mongoose.Types.ObjectId('58581a836630e24c44878fd6');
    const regionId = new mongoose.Types.ObjectId('5908c45e6a2e4300134fbe92');
    const spotId = new mongoose.Types.ObjectId('5842041f4e65fad6a77088ed');
    const subregionFixture = {
      _id: subregionId,
      name: 'North Orange County',
      region: regionId,
      geoname: 5379524,
      imported: { basins: [] },
      order: '1',
      status: 'DRAFT',
      forecastStatus: { inactiveMessage: 'Inactive status message', status: 'active' },
    };
    before(async () => {
      mongoServer = await MongoMemoryServer.create();
      const mongoUri = await mongoServer.getUri();
      await mongoose.connect(
        mongoUri,
        { useNewUrlParser: true, useUnifiedTopology: true },
        (err) => err,
      );

      await new Region({
        _id: regionId,
        name: 'Southern California',
      }).save();
    });

    after(async () => {
      await Region.deleteMany();
      if (mongoServer) {
        await mongoServer.stop();
      }
      if (mongoose.connection) {
        mongoose.connection.close();
      }
    });

    beforeEach(async () => {
      await new Subregion(subregionFixture).save();
    });

    afterEach(async () => {
      await Subregion.deleteMany();
      await Spot.deleteMany();
    });

    it('should find and update subregion name', async () => {
      const res = await request.patch(`/${subregionId}`).send({
        name: 'North Orange County UPDATED',
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        name: 'North Orange County UPDATED',
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update subregion geoname and taxonomy', async () => {
      taxonomy.removeEdge.resolves({});
      geonames.getHierarchy.resolves([{}, {}]);
      const res = await request.patch(`/${subregionId}`).send({
        geoname: 5368381,
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        geoname: 5368381,
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(taxonomy.removeEdge).to.have.been.calledOnce();
      expect(taxonomy.removeEdge).to.have.been.calledWithExactly(['subregion', subregionId], ['geoname', 5379524]);
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(5368381);
    });

    it('should find and update subregion forecast status', async () => {
      const res = await request.patch(`/${subregionId}`).send({
        forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update subregion basin', async () => {
      const res = await request.patch(`/${subregionId}`).send({
        basin: 'natl',
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        imported: { basins: ['natl'] },
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update subregion order', async () => {
      const res = await request.patch(`/${subregionId}`).send({
        order: '2',
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        order: '2',
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update subregion status', async () => {
      const res = await request.patch(`/${subregionId}`).send({
        status: 'PUBLISHED',
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        status: 'PUBLISHED',
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update subregion primary spot', async () => {
      await new Spot({
        _id: spotId,
        name: 'HB Pier, Southside',
        geoname: 5358705,
        status: 'PUBLISHED',
        subregion: subregionId,
        timezone: 'America/Los_Angeles',
        location: {
          coordinates: [-118.0032588192, 33.654213041213],
          type: 'Point',
        },
        pointOfInterestId: '74a2e897-ab5b-47b1-8edb-5b20a88e4b86',
      }).save();

      const res = await request.patch(`/${subregionId}`).send({
        primarySpot: spotId,
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        primarySpot: spotId,
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update subregion offshore swell location', async () => {
      const res = await request.patch(`/${subregionId}`).send({
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [-63.25, 44.25],
        },
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [-63.25, 44.25],
        },
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(geonames.getHierarchy).to.not.have.been.called();
    });

    it('should find and update all properties for subregion ', async () => {
      taxonomy.removeEdge.resolves({});
      geonames.getHierarchy.resolves([{}, {}]);
      const res = await request.patch(`/${subregionId}`).send({
        name: 'North Orange County UPDATED',
        region: regionId,
        geoname: 5368381,
        primarySpot: spotId,
        status: 'PUBLISHED',
        order: '2',
        basin: 'natl',
        forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [-63.25, 44.25],
        },
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();

      const result = mongoose.connection.collections.Subregions.find({
        _id: subregionId,
      });
      const subregionFromDB = await result.next();
      expect(subregionFromDB).to.deep.equal({
        ...subregionFixture,
        name: 'North Orange County UPDATED',
        region: regionId,
        geoname: 5368381,
        primarySpot: spotId,
        status: 'PUBLISHED',
        order: '2',
        imported: { basins: ['natl'] },
        forecastStatus: { inactiveMessage: 'Inactive status message updated!', status: 'inactive' },
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [-63.25, 44.25],
        },
        createdAt: subregionFromDB.createdAt,
        updatedAt: subregionFromDB.updatedAt,
        __v: 0,
      });
      expect(taxonomy.removeEdge).to.have.been.calledOnce();
      expect(taxonomy.removeEdge).to.have.been.calledWithExactly(['subregion', subregionId], ['geoname', 5379524]);
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(5368381);
    });

    it('should return 404 if subregion not found', async () => {
      const res = await request.put(`/${new mongoose.Types.ObjectId()}`).send({});
      expect(res).to.have.status(404);
      expect(res.body).to.deep.equal({ message: 'Subregion not found' });
    });

    it('should return 404 if subregion ID invalid', async () => {
      const res = await request.put('/invalidsubregionid').send({});
      expect(res).to.have.status(404);
      expect(res.body).to.deep.equal({ message: 'Subregion not found' });
    });

    it('should return 400 if geoname is invalid', async () => {
      geonames.getHierarchy.resolves([{}]);
      const res = await request.put(`/${subregionId}`).send({ geoname: 12345 });
      expect(res).to.have.status(400);
      expect(res.body).to.deep.equal({ message: 'Invalid geoname' });
    });
  });

  describe('DELETE', () => {
    it('should delete subregion by subregionId', async () => {
      try {
        const model = { name: 'some subregion', _id: new mongoose.Types.ObjectId() };
        const _id = `${model._id}`;

        sinon.stub(Subregion, 'findByIdAndRemove').resolves(model);
        sinon.stub(treeUtils, 'deleteNode').resolves({});
        const res = await request.delete(`/${_id}`).send();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({ _id, name: model.name });
        expect(Subregion.findByIdAndRemove).to.have.been.calledOnce();
        expect(Subregion.findByIdAndRemove).to.have.been.calledWithExactly(_id);
        expect(treeUtils.deleteNode).to.have.been.calledWithExactly('subregion', model._id);
      } finally {
        Subregion.findByIdAndRemove.restore();
        treeUtils.deleteNode.restore();
      }
    });

    it('should return 404 if subregion not found', async () => {
      try {
        sinon.stub(Subregion, 'findByIdAndRemove').resolves(null);
        sinon.stub(treeUtils, 'deleteNode');
        await request.delete(`/${new mongoose.Types.ObjectId()}`).send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
        expect(Subregion.findByIdAndRemove).to.have.been.calledOnce();
        expect(treeUtils.deleteNode).not.to.have.been.called();
      } finally {
        Subregion.findByIdAndRemove.restore();
        treeUtils.deleteNode.restore();
      }
    });

    it('should return 404 if subregion ID invalid', async () => {
      try {
        sinon.stub(Subregion, 'findByIdAndRemove').resolves(null);
        sinon.stub(treeUtils, 'deleteNode');
        await request.delete('/invalidsubregionid').send();
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
        expect(Subregion.findByIdAndRemove).not.to.have.been.called();
        expect(treeUtils.deleteNode).not.to.have.been.called();
      } finally {
        Subregion.findByIdAndRemove.restore();
        treeUtils.deleteNode.restore();
      }
    });
  });
});
