import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  trackSubregionRequests,
  checkSubregionId,
  getSubregionHandler,
  getNearestSubregionsHandler,
  addSubregionHandler,
  updateSubregionHandler,
  patchSubregionHandler,
  deleteSubregionHandler,
  getSubregionsHandler,
  getSpotsHandler,
  deleteSubregionBasinHandler,
} from './subregions';

const subregions = (log) => {
  const api = Router();
  api.use('*', json(), trackSubregionRequests(log));
  api.get('/:subregionId', checkSubregionId, wrapErrors(getSubregionHandler));
  api.get('/:subregionId/spots', checkSubregionId, wrapErrors(getSpotsHandler));
  api.get('/nearest/:subregionId', checkSubregionId, wrapErrors(getNearestSubregionsHandler));
  api.get('/', wrapErrors(getSubregionsHandler));
  api.post('/', wrapErrors(addSubregionHandler));
  api.put('/:subregionId', checkSubregionId, wrapErrors(updateSubregionHandler));
  api.patch('/:subregionId', checkSubregionId, wrapErrors(patchSubregionHandler));
  api.delete('/:subregionId', checkSubregionId, wrapErrors(deleteSubregionHandler));
  api.delete('/:subregionId/basin', checkSubregionId, wrapErrors(deleteSubregionBasinHandler));

  return api;
};

export default subregions;
