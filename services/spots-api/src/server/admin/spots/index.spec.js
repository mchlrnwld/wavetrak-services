import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { Types } from 'mongoose';
import * as geonames from '../../../common/geonames';
import Spot, * as spotModel from '../../../model/SpotModel';
import Subregion, * as subregionModel from '../../../model/SubregionModel';
import SubregionForecastSummary from '../../../model/SubregionForecastSummaryModel';
import spots from '.';
import * as uploadMiddleware from './upload';
import * as taxonomy from '../../../model/taxonomy';
import * as normalize from '../../../model/taxonomy/normalize';
import * as treeUtils from '../../../model/taxonomy/treeUtils';
import * as publishTopic from './publishTopic';
import {
  resolveTravelDetailsExtraData,
  directionRangesToStringArray,
  generateTravelBestValuesForSpot,
} from './spots';

describe('/admin/spots', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    sinon.stub(uploadMiddleware, 'uploader');
    uploadMiddleware.uploader.returns({
      single: () => (req, res, next) => next(),
    });

    const app = express();
    app.use(spots(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(Spot, 'findOne');
    sinon.stub(Spot, 'findOneAndUpdate');
    sinon.stub(Subregion, 'findOne');
    sinon.stub(SubregionForecastSummary, 'findOne');
    sinon.stub(spotModel, 'createSpot');
    sinon.stub(subregionModel, 'getLegacyRegionId');
    sinon.stub(normalize, 'normalizeSpot');
    sinon.stub(publishTopic, 'default');
    sinon.stub(geonames, 'getHierarchy');
  });

  afterEach(() => {
    Spot.findOne.restore();
    Spot.findOneAndUpdate.restore();
    Subregion.findOne.restore();
    SubregionForecastSummary.findOne.restore();
    spotModel.createSpot.restore();
    subregionModel.getLegacyRegionId.restore();
    normalize.normalizeSpot.restore();
    publishTopic.default.restore();
    geonames.getHierarchy.restore();
  });

  describe('GET', () => {
    it('should return spot by spotId', async () => {
      const model = {
        _id: new Types.ObjectId(),
        name: 'testspot',
        subregion: null,
      };
      const _id = `${model._id}`;

      const thumbnailData = {
        300: 'undefined/spots/default/default_300.jpg',
        640: 'undefined/spots/default/default_640.jpg',
        1500: 'undefined/spots/default/default_1500.jpg',
        3000: 'undefined/spots/default/default_3000.jpg',
        original: 'undefined/spots/default/default_original.jpg',
      };

      Spot.findOne.resolves({
        ...model,
        toObject: sinon.stub().returns(model),
        thumbnail: thumbnailData,
      });
      const res = await request.get(`/${_id}`).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        _id,
        name: 'testspot',
        subregion: null,
        subregionForecastStatus: 'off',
        subregionId: null,
        thumbnail: {
          300: 'undefined/spots/default/default_300.jpg',
          640: 'undefined/spots/default/default_640.jpg',
          1500: 'undefined/spots/default/default_1500.jpg',
          3000: 'undefined/spots/default/default_3000.jpg',
          original: 'undefined/spots/default/default_original.jpg',
        },
      });
      expect(Spot.findOne).to.have.been.calledOnce();
      expect(Spot.findOne).to.have.been.calledWithExactly({
        _id,
        status: { $ne: 'DELETED' },
      });
    });

    it('should return spot with subregionId if one is found', async () => {
      const model = {
        _id: new Types.ObjectId(),
        name: 'testspot',
        subregion: new Types.ObjectId(),
      };
      const _id = `${model._id}`;
      const subregion = `${model.subregion}`;

      const thumbnailData = {
        300: 'undefined/spots/default/default_300.jpg',
        640: 'undefined/spots/default/default_640.jpg',
        1500: 'undefined/spots/default/default_1500.jpg',
        3000: 'undefined/spots/default/default_3000.jpg',
        original: 'undefined/spots/default/default_original.jpg',
      };

      Spot.findOne.resolves({
        ...model,
        toObject: sinon.stub().returns(model),
        thumbnail: thumbnailData,
      });
      SubregionForecastSummary.findOne.resolves({ forecastStatus: { status: 'active' } });
      const res = await request.get(`/${_id}`).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        _id,
        name: 'testspot',
        subregionForecastStatus: 'active',
        subregion,
        subregionId: subregion,
        thumbnail: {
          300: 'undefined/spots/default/default_300.jpg',
          640: 'undefined/spots/default/default_640.jpg',
          1500: 'undefined/spots/default/default_1500.jpg',
          3000: 'undefined/spots/default/default_3000.jpg',
          original: 'undefined/spots/default/default_original.jpg',
        },
      });
      expect(Spot.findOne).to.have.been.calledOnce();
      expect(Spot.findOne).to.have.been.calledWithExactly({
        _id,
        status: { $ne: 'DELETED' },
      });
    });

    it('should return 404 if spot not found', async () => {
      try {
        Spot.findOne.resolves(null);
        const res = await request.get(`/${new Types.ObjectId()}`).send();

        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Spot not found' });
        expect(Spot.findOne).to.have.been.calledOnce();
      }
    });

    it('should return 404 if spot ID invalid', async () => {
      try {
        const res = await request.get('/invalidspotid').send();

        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Spot not found' });
        expect(Spot.findOne).not.to.have.been.called();
      }
    });
  });

  describe('POST', () => {
    it('should add spot', async () => {
      publishTopic.default.resolves({});
      taxonomy.normalizeSpot.resolves({});
      subregionModel.getLegacyRegionId.resolves(1234);
      const model = {
        name: 'Test Spot',
        location: [0, 1],
        forecastLocation: [1, 2],
        geoname: 12345,
      };

      const resultModel = {
        name: 'Test Spot',
        location: { type: 'Point', coordinates: [0, 1] },
        forecastLocation: { type: 'Point', coordinates: [1, 2] },
        legacyRegionId: 1234,
        geoname: 12345,
      };

      geonames.getHierarchy.resolves([{}, {}]);
      spotModel.createSpot.resolves({
        ...resultModel,
        toObject: sinon.stub().returns(resultModel),
      });
      const res = await request.post('/').send(model);

      expect(res).to.be.json();
      expect(res.body).to.deep.equal(resultModel);
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(12345);
      expect(spotModel.createSpot).to.have.been.calledOnce();
      expect(spotModel.createSpot.firstCall.args).to.deep.equal([resultModel]);
    });

    it('should return 400 if geoname is invalid', async () => {
      const model = { geoname: 12345 };
      geonames.getHierarchy.resolves([{}]);

      try {
        const res = await request.post('/').send(model);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Invalid geoname' });
        expect(geonames.getHierarchy).to.have.been.calledOnce();
        expect(geonames.getHierarchy).to.have.been.calledWithExactly(model.geoname);
        expect(spotModel.createSpot).not.to.have.been.called();
      }
    });
  });

  describe('PUT', () => {
    it('should find and update spot by id', async () => {
      const model = {
        _id: new Types.ObjectId(),
        name: 'Test Spot',
        location: [0, 1],
        forecastLocation: [1, 2],
        geoname: 12345,
        status: 'PUBLISHED',
      };

      const existingModel = {
        _id: new Types.ObjectId(),
        save: sinon.stub().resolves(),
      };

      const resultModel = {
        _id: existingModel._id,
        name: 'Test Spot',
        location: { type: 'Point', coordinates: [0, 1] },
        forecastLocation: { type: 'Point', coordinates: [1, 2] },
        geoname: 12345,
      };
      const _id = `${existingModel._id}`;

      Spot.findOne.resolves({
        ...existingModel,
        toObject: sinon.stub().returns({
          ...existingModel,
          ...resultModel,
        }),
      });
      geonames.getHierarchy.resolves([{}, {}]);

      const res = await request.put(`/${_id}`).send(model);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...resultModel,
        _id,
      });
      expect(Spot.findOne).to.have.been.calledOnce();
      expect(Spot.findOne.firstCall.args).to.deep.equal([
        {
          _id,
          status: { $ne: 'DELETED' },
        },
      ]);
      expect(geonames.getHierarchy).to.have.been.calledOnce();
      expect(geonames.getHierarchy).to.have.been.calledWithExactly(12345);
      expect(existingModel.save).to.have.been.calledOnce();
    });

    it('should return 404 if spot not found', async () => {
      try {
        const model = {};

        Spot.findOne.resolves(null);
        const res = await request.put(`/${new Types.ObjectId()}`).send(model);
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Spot not found' });
        expect(Spot.findOne).to.have.been.calledOnce();
      }
    });

    it('should return 404 if spot ID invalid', async () => {
      try {
        const model = {};

        Spot.findOne.resolves(null);
        const res = await request.put('/invalidspotid').send(model);
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Spot not found' });
        expect(Spot.findOne).not.to.have.been.called();
      }
    });

    it('should return 400 if geoname is invalid', async () => {
      const model = { geoname: 12345 };
      Spot.findOne.resolves({ geoname: 23456 });
      geonames.getHierarchy.resolves([{}]);
      const _id = `${new Types.ObjectId()}`;

      try {
        const res = await request.put(`/${_id}`).send(model);
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Invalid geoname' });
        expect(Spot.findOne).to.have.been.calledOnce();
        expect(Spot.findOne).to.have.been.calledWithExactly({
          _id,
          status: {
            $ne: 'DELETED',
          },
        });
        expect(geonames.getHierarchy).to.have.been.calledOnce();
        expect(geonames.getHierarchy).to.have.been.calledWithExactly(model.geoname);
      }
    });
  });

  describe('DELETE', () => {
    beforeEach(() => {
      sinon.stub(treeUtils, 'deleteNode').resolves({});
    });

    afterEach(() => {
      treeUtils.deleteNode.restore();
    });

    it('should delete spot by spotId', async () => {
      const model = { name: 'some spot' };
      const _id = `${new Types.ObjectId()}`;

      Spot.findOneAndUpdate.resolves({
        ...model,
        toObject: sinon.stub().returns(model),
      });
      const res = await request.delete(`/${_id}`).send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(model);
      expect(Spot.findOneAndUpdate).to.have.been.calledOnce();
      expect(Spot.findOneAndUpdate.firstCall.args).to.deep.equal([
        {
          _id,
          status: { $ne: 'DELETED' },
        },
        {
          $set: { status: 'DELETED' },
        },
      ]);
    });

    it('should return 404 if spot not found', async () => {
      try {
        Spot.findOneAndUpdate.resolves(null);
        const res = await request.delete(`/${new Types.ObjectId()}`).send();
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Spot not found' });
        expect(Spot.findOneAndUpdate).to.have.been.calledOnce();
      }
    });

    it('should return 404 if spot ID invalid', async () => {
      try {
        Spot.findOneAndUpdate.resolves(null);
        const res = await request.delete('/invalidspotid').send();
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Spot not found' });
        expect(Spot.findOneAndUpdate).not.to.have.been.called();
      }
    });
  });

  describe('Utils', () => {
    describe('resolveTravelDetailsExtraData', () => {
      it('should add a summary to each travelDetails Item that needs one', () => {
        const spot = {
          anotherField: 'test',
          abilityLevels: ['intermediate', 'advanced'],
          boardTypes: ['fish'],
          best: {
            sizeRange: [
              { min: 1, max: 3 },
              { min: 7, max: 8 },
            ],
          },
          travelDetails: {
            crowdFactor: {
              rating: 3,
            },
            localVibe: {
              rating: 7,
            },
            shoulderBurn: {
              rating: 8,
            },
            spotRating: {
              rating: 4,
            },
            waterQuality: {
              rating: 0,
            },
          },
        };
        expect(resolveTravelDetailsExtraData(spot)).to.eql({
          anotherField: 'test',
          abilityLevels: ['intermediate', 'advanced'],
          boardTypes: ['fish'],
          best: {
            sizeRange: [
              { min: 1, max: 3 },
              { min: 7, max: 8 },
            ],
          },
          travelDetails: {
            abilityLevels: {
              summary: 'intermediate, advanced',
              levels: ['intermediate', 'advanced'],
            },
            best: {
              size: {
                value: ['1-3', '7-8'],
              },
            },
            boardTypes: ['fish'],
            crowdFactor: {
              rating: 3,
              summary: 'Mellow',
            },
            localVibe: {
              rating: 7,
              summary: 'Doable',
            },
            shoulderBurn: {
              rating: 8,
              summary: 'Exhausting',
            },
            spotRating: {
              rating: 4,
              summary: 'Fun',
            },
            waterQuality: {
              rating: 0,
              summary: 'Clean',
            },
          },
        });
      });
    });

    describe('directionRangesToStringArray', () => {
      it('should map a direction number to a string', () => {
        expect(
          directionRangesToStringArray([
            { min: 1, max: 84 },
            { min: 200, max: 275 },
            { min: 175, max: 220 },
          ]),
        ).to.eql(['N', 'NNE', 'NE', 'ENE', 'E', 'S', 'SSW', 'SW', 'WSW', 'W']);

        expect(directionRangesToStringArray([{ min: 318, max: 47 }])).to.eql([
          'NW',
          'NNW',
          'N',
          'NNE',
          'NE',
        ]);

        expect(directionRangesToStringArray([{ min: 318, max: 359 }])).to.eql(['NW', 'NNW', 'N']);
      });
    });

    describe('generateTravelBestValuesForSpot', () => {
      it('should generate the travelDetails values', () => {
        const spot = {
          best: {
            windDirection: [{ min: 318, max: 47 }],
            swellWindow: [{ min: 318, max: 47 }],
          },
          travelDetails: {
            best: {
              windDirection: {
                description: 'wind',
              },
              season: {
                value: ['Winter'],
                description: 'best in winter',
              },
            },
          },
        };

        expect(generateTravelBestValuesForSpot(spot)).to.eql({
          ...spot,
          travelDetails: {
            best: {
              windDirection: {
                value: ['NW', 'NNW', 'N', 'NNE', 'NE'],
                description: 'wind',
              },
              swellDirection: {
                value: ['NW', 'NNW', 'N', 'NNE', 'NE'],
              },
              season: {
                value: ['Winter'],
                description: 'best in winter',
              },
            },
          },
        });
      });
    });
  });
});
