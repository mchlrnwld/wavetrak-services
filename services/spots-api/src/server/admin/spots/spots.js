import AWS from 'aws-sdk';
import _ from 'lodash';
import { pick } from 'lodash/fp';
import { Types } from 'mongoose';
import publishTopic from './publishTopic';
import { getHierarchy } from '../../../common/geonames';
import { fetchTaxonomyJSON } from '../../../common/taxonomy';
import { createGeoJsonPoint } from '../../../common/geojson';
import Spot, { createSpot } from '../../../model/SpotModel';
import { getLegacyRegionId } from '../../../model/SubregionModel';
import SubregionForecastSummary from '../../../model/SubregionForecastSummaryModel';
import {
  normalizeSpot, deleteNode, removeEdge, types,
} from '../../../model/taxonomy';
import { markTaxonomyHasSpotsByType } from '../../../model/taxonomy/treeUtils';

const NOT_FOUND = { message: 'Spot not found' };

const stringifyRating = (strings) => ({ rating }) => {
  if (rating !== null && rating !== undefined) {
    return strings[Math.floor(rating / 4)];
  }
  return null;
};

const travelDetailsSummaryDefs = {
  abilityLevels: ({ levels }) => levels && levels.join(', '),
  crowdFactor: stringifyRating(['Mellow', 'Moderate', 'Heavy']),
  localVibe: stringifyRating(['Welcoming', 'Doable', 'Intimidating']),
  shoulderBurn: stringifyRating(['Light', 'Medium', 'Exhausting']),
  spotRating: stringifyRating(['Poor', 'Fun', 'Perfect']),
  waterQuality: stringifyRating(['Clean', 'Fair', 'Dirty']),
};

export const trackSpotsRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/spots',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const addDefaultLolaCoefficients = (spots) => spots.map((spot) => ({
  ...spot,
  lolaSurfBiasCoefficients: spot.lolaSurfBiasCoefficients || { linear: 1, constant: 0 },
}));

export const checkSpotId = (req, res, next) => {
  const { spotId } = req.params;
  if (!Types.ObjectId.isValid(spotId)) return res.status(404).send(NOT_FOUND);
  return next();
};

/** derives the extra data for the travelDetails object from existing spot data
 * @param  {Object} spot a spot object
 * @returns {Object} a spot object with the new fields (doesn't modify the input though)
 */
export const resolveTravelDetailsExtraData = (spot) => {
  if (spot.travelDetails) {
    const { travelDetails } = spot;
    const newTravelDetails = {};
    newTravelDetails.boardTypes = spot.boardTypes;
    newTravelDetails.abilityLevels = {
      ...travelDetails.abilityLevels,
      levels: spot.abilityLevels,
    };
    newTravelDetails.best = {
      ...travelDetails.best,
      size: {
        ...(travelDetails.best && travelDetails.best.size),
        value:
          spot.best
          && spot.best.sizeRange
          && spot.best.sizeRange.map((range) => `${range.min}-${range.max}`),
      },
    };
    travelDetails.abilityLevels = newTravelDetails.abilityLevels;
    Object.keys(travelDetailsSummaryDefs).forEach((prop) => {
      if (travelDetails[prop]) {
        newTravelDetails[prop] = {
          ...travelDetails[prop],
          summary: travelDetailsSummaryDefs[prop](travelDetails[prop]),
        };
      }
    });

    return { ...spot, travelDetails: { ...travelDetails, ...newTravelDetails } };
  }

  return spot;
};

/** Converts an array of min-max objects to an array of direction strings
 * @param  {[Object]} directionRanges array of { min: Number, max: Number } objects
 * @returns {[String]} an array of direction strings
 */
export const directionRangesToStringArray = (directionRanges) => Array.from(
  new Set(
    directionRanges
      .sort((a, b) => a.min - b.min)
      .map(({ min, max }) => {
        const minInd = Math.floor(min / 22.5 + 0.5) % 16;
        const maxInd = Math.floor(max / 22.5 + 0.5) % 16;
        const dirStrings = [
          'N',
          'NNE',
          'NE',
          'ENE',
          'E',
          'ESE',
          'SE',
          'SSE',
          'S',
          'SSW',
          'SW',
          'WSW',
          'W',
          'WNW',
          'NW',
          'NNW',
          'N',
        ];
        const thisStrings = [];
        for (let index = minInd; index <= (maxInd < minInd ? 16 : maxInd); index += 1) {
          thisStrings.push(dirStrings[index]);
        }
        if (maxInd < minInd) {
          for (let index = 0; index <= maxInd; index += 1) {
            thisStrings.push(dirStrings[index]);
          }
        }
        return thisStrings;
      })
      .flat(),
  ),
);

export const generateTravelBestValuesForSpot = (spot) => {
  if (spot.best && (spot.best.windDirection || spot.best.swellWindow)) {
    // eslint-disable-next-line no-param-reassign
    spot.travelDetails = spot.travelDetails || { best: {} };
    // eslint-disable-next-line no-param-reassign
    spot.travelDetails.best = spot.travelDetails.best || {};

    // eslint-disable-next-line no-param-reassign
    spot.travelDetails.best.windDirection = {
      ...spot.travelDetails.best.windDirection,
      value: directionRangesToStringArray(spot.best.windDirection),
    };
    // eslint-disable-next-line no-param-reassign
    spot.travelDetails.best.swellDirection = {
      ...spot.travelDetails.best.swellDirection,
      value: directionRangesToStringArray(spot.best.swellWindow),
    };
  }

  return spot;
};
/**
 * Saves a spot and processes all the required follow up tasks
 * This is all the shared functionality between the patch and put handlers
 * @param  {} spot the spot object from mongo
 * @param  {} res the express res object for sending the response
 */
const saveSpot = async (
  spot,
  res,
  subregion = null,
  geoname = null,
  updateTaxonomy = true,
) => {
  generateTravelBestValuesForSpot(spot);
  await spot.save();
  if (updateTaxonomy) {
    await normalizeSpot(spot);
    if (geoname && spot.geoname && spot.geoname !== geoname) {
      await removeEdge([types.SPOT, spot._id], [types.GEONAME, spot.geoname]);
      await markTaxonomyHasSpotsByType('geoname', spot.geoname.toString());
    }
    if (subregion && spot.subregion && spot.subregion.toString() !== subregion) {
      await removeEdge([types.SPOT, spot._id], [types.SUBREGION, spot.subregion]);
      await markTaxonomyHasSpotsByType('subregion', spot.subregion.toString());
    }
  } else {
    await deleteNode('spot', spot._id);
    if (spot.geoname) await markTaxonomyHasSpotsByType('geoname', spot.geoname.toString());
    if (spot.subregion) markTaxonomyHasSpotsByType('subregion', spot.subregion.toString());
  }
  await publishTopic(spot);

  return res.send(resolveTravelDetailsExtraData(spot.toObject()));
};

export async function getSpotHandler(
  req,
  res,
) {
  const spot = await Spot.findOne({ _id: req.params.spotId, status: { $ne: 'DELETED' } });
  if (!spot) return res.status(404).send(NOT_FOUND);

  let subregionForecastSummary = {};
  if (spot.subregion) {
    subregionForecastSummary = await SubregionForecastSummary.findOne(
      { subregionId: spot.subregion },
      null,
      { sort: { forecastDate: -1 } },
    );
  }

  const obj = resolveTravelDetailsExtraData(spot.toObject());

  return res.send({
    ...obj,
    thumbnail: spot.thumbnail,
    subregionId: spot.subregion,
    subregionForecastStatus: _.get(subregionForecastSummary, 'forecastStatus.status', 'off'),
  });
}

export const deleteSpotHandler = async (req, res) => {
  const spot = await Spot.findOneAndUpdate(
    {
      _id: req.params.spotId,
      status: { $ne: 'DELETED' },
    },
    {
      $set: { status: 'DELETED' },
    },
  );
  if (!spot) return res.status(404).send(NOT_FOUND);
  await deleteNode('spot', spot._id);
  await publishTopic(spot);
  return res.send(resolveTravelDetailsExtraData(spot.toObject()));
};

export const updateSpotHandler = async (req, res) => {
  const {
    name,
    legacyId,
    location,
    forecastLocation,
    subregion,
    tideStation,
    cams,
    timezone,
    status,
    politicalLocation,
    offshoreDirection,
    lolaSurfBiasCoefficients,
    noraBiasCorrection,
    swellWindow,
    best,
    humanReport,
    geoname,
    spotType,
    abilityLevels,
    boardTypes,
    relivableRating,
    pointOfInterestId,
    titleTag,
    metaDescription,
    travelDetails,
    weatherStations,
  } = req.body;
  const spot = await Spot.findOne({ _id: req.params.spotId, status: { $ne: 'DELETED' } });
  if (!spot) return res.status(404).send(NOT_FOUND);
  const spotStatus = status || spot.status;
  const updateTaxonomy = spotStatus === 'PUBLISHED';

  if (geoname) {
    const hierarchy = await getHierarchy(geoname);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geoname',
      });
    }
  }

  spot.name = name;
  spot.legacyId = legacyId;
  spot.legacyRegionId = await getLegacyRegionId(subregion);
  spot.location = createGeoJsonPoint(location);
  spot.forecastLocation = createGeoJsonPoint(forecastLocation);
  spot.subregion = subregion;
  spot.tideStation = tideStation;
  spot.cams = cams;
  spot.timezone = timezone;
  spot.status = status;
  spot.politicalLocation = politicalLocation;
  spot.offshoreDirection = offshoreDirection;
  spot.noraBiasCorrection = noraBiasCorrection;
  spot.lolaSurfBiasCoefficients = lolaSurfBiasCoefficients;
  spot.swellWindow = swellWindow;
  spot.best = best;
  spot.humanReport = humanReport;
  spot.geoname = geoname;
  spot.spotType = spotType;
  spot.abilityLevels = abilityLevels;
  spot.boardTypes = boardTypes;
  spot.relivableRating = relivableRating;
  spot.pointOfInterestId = pointOfInterestId;
  spot.titleTag = titleTag;
  spot.metaDescription = metaDescription;
  spot.travelDetails = travelDetails;
  spot.weatherStations = weatherStations;

  return saveSpot(spot, res, subregion, geoname, updateTaxonomy);
};

export const patchSpotHandler = async (req, res) => {
  const spot = await Spot.findOne({ _id: req.params.spotId, status: { $ne: 'DELETED' } });
  if (!spot) return res.status(404).send(NOT_FOUND);

  const {
    geoname,
    subregion,
    status,
    forecastStatus,
  } = req.body;
  const spotStatus = status || spot.status;
  const updateTaxonomy = spotStatus === 'PUBLISHED';

  if (geoname) {
    const hierarchy = await getHierarchy(geoname);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geoname',
      });
    }
  }

  [
    'name',
    'legacyId',
    'tideStation',
    'cams',
    'subregion',
    'timezone',
    'status',
    'politicalLocation',
    'offshoreDirection',
    'lolaSurfBiasCoefficients',
    'noraBiasCorrection',
    'swellWindow',
    'best',
    'humanReport',
    'geoname',
    'spotType',
    'abilityLevels',
    'boardTypes',
    'relivableRating',
    'pointOfInterestId',
    'titleTag',
    'metaDescription',
    'travelDetails',
    'weatherStations',
  ].forEach((fieldName) => {
    if (req.body[fieldName] !== undefined) {
      spot[fieldName] = req.body[fieldName];
    }
  });

  ['location', 'forecastLocation'].forEach((fieldName) => {
    if (req.body[fieldName] === null) {
      spot[fieldName] = null;
    } else if (req.body[fieldName] !== undefined) {
      spot[fieldName] = createGeoJsonPoint(req.body[fieldName]);
    }
  });

  if (forecastStatus) {
    ['status', 'inactiveMessage'].forEach((fieldName) => {
      if (forecastStatus[fieldName] !== undefined) {
        spot.forecastStatus[fieldName] = forecastStatus[fieldName];
      }
    });
  }

  return saveSpot(spot, res, subregion, geoname, updateTaxonomy);
};

export const createSpotHandler = async (req, res) => {
  if (req.body.geoname) {
    const hierarchy = await getHierarchy(req.body.geoname);
    if (hierarchy.length < 2) {
      return res.status(400).send({
        message: 'Invalid geoname',
      });
    }
  }

  const spot = await createSpot({
    ...generateTravelBestValuesForSpot(req.body),
    location: createGeoJsonPoint(req.body.location),
    forecastLocation: createGeoJsonPoint(req.body.forecastLocation),
    legacyRegionId: await getLegacyRegionId(req.body.subregion),
  });
  if (spot.status === 'PUBLISHED') await normalizeSpot(spot);
  await publishTopic(spot);
  return res.send(resolveTravelDetailsExtraData(spot.toObject()));
};

export const searchSpotsHandler = async (req, res) => {
  const {
    status,
    search,
    select,
    limit,
    filter,
    distance,
    coordinates,
    offset,
    report,
    camsOnly,
    abilityLevels,
    boardTypes,
    relivableRating,
    cams,
    spotIds,
    pointOfInterestId,
    includeTaxonomy,
    thirdParty,
    weatherStations,
  } = req.query;

  const $and = [{ status: { $ne: 'DELETED' } }];

  if (filter === 'nearby') {
    if (distance) {
      $and.push({
        location: {
          $near: {
            $geometry: {
              type: 'Point',
              coordinates,
            },
            $maxDistance: distance * 1609.34, // Convert miles to meters
          },
        },
      });
    } else {
      $and.push({
        location: {
          $near: {
            $geometry: {
              type: 'Point',
              coordinates,
            },
          },
        },
      });
    }
    $and.push({ status: { $ne: 'DRAFT' } });
  } else if (filter === 'popular') {
    $and.push({ popular: { $eq: true } });
  }

  if (search) {
    $and.push({ name: { $regex: search, $options: 'i' } });
  }

  if (abilityLevels) {
    $and.push({ abilityLevels: { $in: abilityLevels.split(',') } });
  }

  if (boardTypes) {
    $and.push({ boardTypes: { $in: boardTypes.split(',') } });
  }

  if (spotIds) {
    $and.push({ _id: { $in: spotIds.split(',') } });
  }

  if (cams) {
    $and.push({ cams: { $in: cams.split(',').map((cam) => new Types.ObjectId(cam)) } });
  }

  if (relivableRating) {
    $and.push({ relivableRating: { $eq: relivableRating } });
  }

  if (status && !thirdParty) {
    $and.push({
      $or: status.split(',').map((elem) => ({ status: { $eq: elem.toUpperCase() } })),
    });
  }

  if (thirdParty) {
    $and.push({ status: { $eq: 'PUBLISHED' } });
  }

  if (report) {
    $and.push({
      $or: report.split(',').map((elem) => ({ 'humanReport.status': { $eq: elem.toUpperCase() } })),
    });
  }

  if (camsOnly && camsOnly.toUpperCase() === 'TRUE') {
    $and.push({ cams: { $exists: true, $ne: [] } });
  }

  if (pointOfInterestId) {
    $and.push({ pointOfInterestId: { $eq: pointOfInterestId } });
  }

  if (weatherStations) {
    $and.push({ weatherStations: { $in: weatherStations.split(',').map((weatherStation) => new Types.ObjectId(weatherStation)) } });
  }

  const find = { $and };

  let query = Spot.find(find);

  if (select) query = query.select(select.replace(/,/g, ' '));
  if (offset) query = query.skip(parseInt(offset, 10));
  if (limit) query = query.limit(parseInt(limit, 10));

  // Only return thumbnails when explicitly searching for them
  if (select && select.indexOf('thumbnail') > -1) {
    const thumbnailSelectQuery = await query;

    // append thumbnail object to response from 'virtual' defined on Spots model.
    const spotsWithThumbnails = thumbnailSelectQuery.map((spot) => ({
      ...spot.toJSON(),
      thumbnail: spot.thumbnail,
    }));
    return res.send(spotsWithThumbnails);
  }

  let spots = filter ? await query.lean() : await query.sort({ name: 1 }).lean();

  if (select && select.indexOf('lolaSurfBiasCoefficients') > -1) spots = addDefaultLolaCoefficients(spots);

  spots = spots.map((spot) => resolveTravelDetailsExtraData(spot));

  if (includeTaxonomy) {
    const taxonomies = await Promise.all(spots.map(({ _id }) => fetchTaxonomyJSON('spot', _id)));

    return res.send(
      spots.map((spot) => {
        const taxonomy = taxonomies.find(
          ({ spot: spotId }) => spotId?.toString() === spot._id?.toString(),
        );

        return {
          ...spot,
          taxonomy,
        };
      }),
    );
  }

  // return only name and location for 3rd party requests
  if (thirdParty && spots.length) {
    spots = spots.map(pick(['_id', 'name', 'location']));
    return res.send(spots);
  }

  return res.send(spots);
};

export const uploadSpotThumbnailHandler = async (req, res) => {
  const { spotId } = req.params;
  const spot = await Spot.findOne({ _id: spotId });
  const lambda = new AWS.Lambda({
    apiVersion: '2015-03-31',
    region: 'us-west-1',
  });

  await lambda
    .invoke({
      FunctionName: process.env.IMAGE_SERVICE,
      InvocationType: 'Event',
      LogType: 'None',
      Payload: JSON.stringify({
        bucket: process.env.SPOT_THUMBNAIL_BUCKET,
        source: req.file.key,
        spotId,
        pipeline: 'spotThumbnail',
      }),
    })
    .promise();

  spot.hasThumbnail = true;
  await spot.save();

  return res.status(200).send({
    message: 'The thumbnail was uploaded for processing',
    status: 200,
    file: req.file.key,
  });
};

export const removeCamFromSpots = async (cameraId) => {
  const mongoCameraId = new Types.ObjectId(cameraId);
  const spotsUsingCam = await Spot.find({
    cams: {
      $in: [mongoCameraId],
    },
  });

  if (spotsUsingCam && spotsUsingCam.length > 0) {
    await Spot.updateMany(
      { _id: { $in: spotsUsingCam.map(({ _id }) => _id) } },
      {
        $pull: {
          cams: { $in: [mongoCameraId] },
        },
      },
    );

    await Promise.all(spotsUsingCam.map(publishTopic));
  }
};
