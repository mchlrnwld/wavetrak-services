import mime from 'mime';
import AWS from 'aws-sdk';
import multer from 'multer';
import multerS3 from 'multer-s3';

const s3 = new AWS.S3({});
const storage = () => multerS3({
  s3,
  bucket: process.env.SPOT_THUMBNAIL_BUCKET,
  metadata: (req, file, cb) => cb(null, { fieldName: file.fieldname }),
  key: (req, file, cb) => cb(
    null,
    `incoming/${req.params.spotId}.${mime.extension(file.mimetype)}`,
  ),
});

export const uploader = () => multer({ // eslint-disable-line import/prefer-default-export
  storage: storage(),
});
