import { expect } from 'chai';
import sinon from 'sinon';
import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';
import {
  createSpotHandler,
  getSpotHandler,
  searchSpotsHandler,
  updateSpotHandler,
  patchSpotHandler,
  deleteSpotHandler,
} from './spots';
import Spot from '../../../model/SpotModel';
import * as taxonomy from '../../../model/taxonomy';
import * as publishTopic from './publishTopic';
import * as geonames from '../../../common/geonames';
import * as normalize from '../../../model/taxonomy/normalize';
import * as treeUtils from '../../../model/taxonomy/treeUtils';

const body = {
  _id: '5842041f4e65fad6a7708ca7',
  name: 'Fistral Beach',
  location: [-5.1032781601, 50.4156609046],
  forecastLocation: [-5.1032781601, 50.4156609046],
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  popular: true,
  timezone: 'Europe/London',
  tideStation: 'Fistral Beach',
  cams: ['5ef354721fc3caa9193a3e00'],
  weatherStations: ['6ef354721fc3caa9193a3e00'],
  status: 'PUBLISHED',
  spotType: 'SURFBREAK',
  abilityLevels: ['BEGINNER', 'INTERMEDIATE'],
  boardTypes: ['FISH', 'SHORTBOARD'],
  relivableRating: 3,
  subregion: '58581a836630e24c44879188',
  geoname: 2641589,
  lolaSurfBiasCoefficients: {
    constant: 0,
    linear: 1,
  },
  noraBiasCorrection: 0,
  politicalLocation: {
    city: {
      geonameId: null,
      name: null,
    },
    hierarchy: [],
  },
  swellWindow: [
    {
      min: 243,
      max: 284,
    },
  ],
  best: {
    windDirection: [
      {
        min: 107,
        max: 153,
      },
    ],
    swellPeriod: [
      {
        min: 0,
        max: 33,
      },
    ],
    swellWindow: [
      {
        min: 258,
        max: 276,
      },
    ],
    sizeRange: [
      {
        min: 2,
        max: 8,
      },
    ],
  },
  humanReport: {
    status: 'OFF',
    clone: {
      spotId: null,
    },
    average: {
      spotOneId: null,
      spotTwoId: null,
    },
  },
  titleTag: 'test title tag',
  metaDescription: 'test meta description',
  travelDetails: {
    travelId: 'testThing',
    abilityLevels: {
      description: 'testThing',
    },
    access: 'testThing',
    best: {
      season: {
        description: 'testThing',
        value: ['Winter', 'Autumn'],
      },
      size: {
        description: 'testThing',
      },
      swellDirection: {
        description: 'testThing',
      },
      tide: {
        description: 'testThing',
        value: ['Low', 'Medium_Low'],
      },
      windDirection: {
        description: 'testThing',
      },
    },
    bottom: {
      description: 'testThing',
      value: ['Coral'],
    },
    breakType: ['Reef'],
    crowdFactor: {
      description: 'testThing',
      rating: 0,
    },
    description: 'testThing',
    hazards: 'testThing',
    localVibe: {
      description: 'testThing',
      rating: 0,
    },
    shoulderBurn: {
      description: 'testThing',
      rating: 0,
    },
    relatedArticleId: 'testThing',
    spotRating: {
      description: 'testThing',
      rating: 0,
    },
    waterQuality: {
      description: 'testThing',
      rating: 0,
    },
    status: 'DRAFT',
  },
};

const humanReport = {
  status: 'CLONE',
  clone: {
    spotId: '58349eed3421b20545c4b56c',
  },
  average: {
    spotOneId: '58349eed3421b20545c4b56c',
    spotTwoId: '58349eed3421b20545c4b56c',
  },
};

const geoname = 3641589;

const expected = {
  ...body,
  id: body._id,
  __v: 0,
  forecastLocation: {
    coordinates: [-5.1032781601, 50.4156609046],
    type: 'Point',
  },
  location: {
    coordinates: [-5.1032781601, 50.4156609046],
    type: 'Point',
  },
  tideStation: 'Fistral Beach',
  hasThumbnail: false,
  legacyRegionId: null,
  thumbnail: {
    300: 'undefined/spots/default/default_300.jpg',
    640: 'undefined/spots/default/default_640.jpg',
    1500: 'undefined/spots/default/default_1500.jpg',
    3000: 'undefined/spots/default/default_3000.jpg',
    original: 'undefined/spots/default/default_original.jpg',
  },
  travelDetails: {
    ...body.travelDetails,
    abilityLevels: {
      ...body.travelDetails.abilityLevels,

      levels: body.abilityLevels,
      summary: 'BEGINNER, INTERMEDIATE',
    },
    best: {
      ...body.travelDetails.best,
      size: {
        ...body.travelDetails.best.size,
        value: ['2-8'],
      },
      swellDirection: {
        ...body.travelDetails.best.swellDirection,
        value: ['WSW', 'W'],
      },
      windDirection: {
        ...body.travelDetails.best.windDirection,
        value: ['ESE', 'SE', 'SSE'],
      },
    },
    boardTypes: body.boardTypes,
    localVibe: {
      ...body.travelDetails.localVibe,
      summary: 'Welcoming',
    },
    shoulderBurn: {
      ...body.travelDetails.shoulderBurn,
      summary: 'Light',
    },
    spotRating: {
      ...body.travelDetails.spotRating,
      summary: 'Poor',
    },
    waterQuality: {
      ...body.travelDetails.waterQuality,
      summary: 'Clean',
    },
    crowdFactor: {
      ...body.travelDetails.crowdFactor,
      summary: 'Mellow',
    },
  },
  forecastStatus: {
    status: 'active',
    inactiveMessage: 'Inactive status message',
  },
};

const { _id } = body;

describe('/admin/spots', () => {
  let mongoServer;

  before(async () => {
    mongoServer = new MongodbMemoryServer();

    const mongoUri = await mongoServer.getConnectionString();
    await mongoose.connect(mongoUri, {}, (err) => err);

    sinon.stub(publishTopic, 'default');
    sinon.stub(geonames, 'getHierarchy');
    sinon.stub(normalize, 'normalizeSpot');
    sinon.stub(treeUtils, 'deleteNode');

    publishTopic.default.resolves({});
    taxonomy.normalizeSpot.resolves({});
    geonames.getHierarchy.resolves([{}, {}]);
    treeUtils.deleteNode.resolves({});
  });

  after(() => {
    publishTopic.default.restore();
    taxonomy.normalizeSpot.restore();
    geonames.getHierarchy.restore();
    treeUtils.deleteNode.restore();
    mongoose.disconnect();
    mongoServer.stop();
  });

  afterEach(async () => {
    await Spot.deleteMany();
  });

  describe('create action', async () => {
    it('should create a spot with all possible data', async () => {
      const res = { send: sinon.spy() };

      await createSpotHandler({ body }, res);

      const call1 = res.send.getCall(0).args[0];

      const { id } = call1;

      expect({
        ...call1,
        _id: call1._id.toString(),
        subregion: call1.subregion.toString(),
        cams: call1.cams.map((camId) => camId.toString()),
        weatherStations: call1.weatherStations
          .map((weatherStationId) => weatherStationId.toString()),
      }).to.eql({
        ...expected,
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });

      await getSpotHandler({ params: { spotId: id } }, res);

      const storedSpot = res.send.getCall(1).args[0];

      expect({
        ...storedSpot,
        _id: call1._id.toString(),
        subregion: storedSpot.subregion.toString(),
        subregionId: storedSpot.subregionId.toString(),
        cams: storedSpot.cams.map((camId) => camId.toString()),
        weatherStations: storedSpot.weatherStations
          .map((weatherStationId) => weatherStationId.toString()),
      }).to.eql({
        ...expected,
        subregionId: body.subregion,
        subregionForecastStatus: 'off',
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });
    });
  });

  describe('update, patch and get actions', async () => {
    beforeEach(async () => {
      const res = { send: sinon.spy() };
      await createSpotHandler({ body }, res);
    });

    it('should return data for a pointOfInterest query', async () => {
      const res = { send: sinon.spy() };
      await searchSpotsHandler(
        { query: { pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1' } },
        res,
      );

      const call1 = res.send.getCall(0).args[0];

      expect(call1[0].name).to.equal('Fistral Beach');
    });

    it('should return data for a filter query', async () => {
      const res = { send: sinon.spy() };
      await searchSpotsHandler({ query: { filter: 'popular' } }, res);

      const call1 = res.send.getCall(0).args[0];

      expect(call1[0].name).to.equal('Fistral Beach');
    });

    it('should return only _id, name, and location for 3rd party requests', async () => {
      const res = { send: sinon.spy() };
      await searchSpotsHandler({ query: { thirdParty: true } }, res);

      const call1 = res.send.getCall(0).args[0];
      const spot = {
        ...call1[0],
        _id: call1[0]._id.toString(),
      };

      expect(spot).to.deep.equal({
        _id: '5842041f4e65fad6a7708ca7',
        name: 'Fistral Beach',
        location: {
          type: 'Point',
          coordinates: [-5.1032781601, 50.4156609046],
        },
      });
    });

    it('should update a spot with new tide station', async () => {
      const res = { send: sinon.spy() };

      await updateSpotHandler(
        {
          body: {
            ...body,
            tideStation: 'New Tide Station',
          },
          params: { spotId: _id },
        },
        res,
      );

      const call1 = res.send.getCall(0).args[0];

      const { id } = call1;

      expect({
        ...call1,
        _id: call1._id.toString(),
        subregion: call1.subregion.toString(),
        cams: call1.cams.map((camId) => camId.toString()),
        weatherStations: call1.weatherStations.map((stationId) => stationId.toString()),
      }).to.eql({
        ...expected,
        tideStation: 'New Tide Station',
        __v: 1,
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });

      await getSpotHandler({ params: { spotId: id } }, res);

      const storedSpot = res.send.getCall(1).args[0];

      // Expect the tide station, created above as New Tide Station,
      // to be returned.
      expect({
        ...storedSpot,
        subregion: storedSpot.subregion.toString(),
        subregionId: storedSpot.subregionId.toString(),
        cams: storedSpot.cams.map((camId) => camId.toString()),
        weatherStations: storedSpot.weatherStations.map((camId) => camId.toString()),
      }).to.eql({
        ...expected,
        tideStation: 'New Tide Station',
        __v: 1,
        _id: call1._id,
        id,
        subregionId: body.subregion,
        subregionForecastStatus: 'off',
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });

      await getSpotHandler({ params: { spotId: id } }, res);

      const storedSpotOldTideStation = res.send.getCall(2).args[0];

      // Expect the old tide station, created in the beginning,
      // to be returned.
      expect({
        ...storedSpotOldTideStation,
        subregion: storedSpotOldTideStation.subregion.toString(),
        subregionId: storedSpotOldTideStation.subregionId.toString(),
        cams: storedSpotOldTideStation.cams.map((camId) => camId.toString()),
        weatherStations: storedSpotOldTideStation.weatherStations.map((camId) => camId.toString()),
      }).to.eql({
        ...expected,
        tideStation: 'New Tide Station',
        _id: storedSpotOldTideStation._id,
        id,
        __v: 1,
        subregionId: body.subregion,
        subregionForecastStatus: 'off',
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });
    });

    it('should patch a spot with new tide staion', async () => {
      const res = { send: sinon.spy() };

      await patchSpotHandler(
        {
          body: {
            ...body,
            humanReport,
            geoname,
            tideStation: 'New Tide Station',
          },
          params: { spotId: _id },
        },
        res,
      );

      const call1 = res.send.getCall(0).args[0];

      const { id } = call1;

      await getSpotHandler({ params: { spotId: id } }, res);

      const storedSpot = res.send.getCall(1).args[0];

      expect({
        ...storedSpot,
        _id: storedSpot._id.toString(),
        subregion: storedSpot.subregion.toString(),
        subregionId: storedSpot.subregionId.toString(),
        cams: storedSpot.cams.map((camId) => camId.toString()),
        weatherStations: storedSpot.weatherStations.map((camId) => camId.toString()),
        humanReport: {
          status: call1.humanReport.status,
          clone: {
            spotId: call1.humanReport.clone.spotId.toString(),
          },
          average: {
            spotOneId: call1.humanReport.average.spotOneId.toString(),
            spotTwoId: call1.humanReport.average.spotTwoId.toString(),
          },
        },
      }).to.eql({
        ...expected,
        tideStation: 'New Tide Station',
        __v: 1,
        subregionId: body.subregion,
        subregionForecastStatus: 'off',
        humanReport,
        geoname,
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });
    });

    it('should patch a spot with forecast status', async () => {
      const res = { send: sinon.spy() };

      await patchSpotHandler(
        {
          body: {
            ...body,
            forecastStatus: {
              status: 'active',
              inactiveMessage: 'Inactive message test',
            },
          },
          params: { spotId: _id },
        },
        res,
      );

      const call1 = res.send.getCall(0).args[0];

      const { id } = call1;

      await getSpotHandler({ params: { spotId: id } }, res);

      const storedSpot = res.send.getCall(1).args[0];

      expect({
        ...storedSpot,
        _id: storedSpot._id.toString(),
        subregion: storedSpot.subregion.toString(),
        subregionId: storedSpot.subregionId.toString(),
        cams: storedSpot.cams.map((camId) => camId.toString()),
        weatherStations: storedSpot.weatherStations.map((camId) => camId.toString()),
      }).to.eql({
        ...expected,
        forecastStatus: {
          status: 'active',
          inactiveMessage: 'Inactive message test',
        },
        __v: 1,
        subregionId: body.subregion,
        subregionForecastStatus: 'off',
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });
    });

    it('should throw a validation error creating a spot with two of the same weatherStations for one spot', async () => {
      const res = { send: sinon.spy() };

      try {
        await updateSpotHandler(
          {
            body: {
              ...body,
              weatherStations: ['6ef354721fc3caa9193a3e09', '6ef354721fc3caa9193a3e09'],
            },
            params: { spotId: body._id },
          },
          res,
        );

        throw new Error('No validation errors');
      } catch (error) {
        expect(error.name).to.deep.equal('ValidationError');
        expect(error.message).to.equal(
          'Spot validation failed: weatherStations: `6ef354721fc3caa9193a3e09,6ef354721fc3caa9193a3e09` contains duplicates.',
        );
      }
    });

    it('should delete a spot', async () => {
      const res = { send: sinon.spy(), status: sinon.stub().returns({ send: sinon.spy() }) };

      await deleteSpotHandler(
        {
          params: { spotId: _id },
        },
        res,
      );

      const call1 = res.send.getCall(0).args[0];

      const { id } = call1;

      expect({
        ...call1,
        _id: call1._id.toString(),
        subregion: call1.subregion.toString(),
        cams: call1.cams.map((camId) => camId.toString()),
        weatherStations: call1.weatherStations
          .map((weatherStationId) => weatherStationId.toString()),
      }).to.eql({
        ...expected,
        createdAt: call1.createdAt,
        updatedAt: call1.updatedAt,
      });

      await getSpotHandler({ params: { spotId: id } }, res);

      const status = res.status.getCall(0).args[0];

      expect(status).to.eql(404);
    });
  });
});
