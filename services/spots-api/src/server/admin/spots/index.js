import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  trackSpotsRequests,
  checkSpotId,
  getSpotHandler,
  deleteSpotHandler,
  updateSpotHandler,
  patchSpotHandler,
  createSpotHandler,
  searchSpotsHandler,
  uploadSpotThumbnailHandler,
} from './spots';
import * as uploaderMiddleware from './upload';

const spots = (log) => {
  const api = Router();
  api.get('/health', (req, res) => res.send({
    status: 200,
    message: 'OK',
    version: process.env.APP_VERSION || 'unknown',
  }));

  const upload = uploaderMiddleware.uploader();

  api.use('*', json(), trackSpotsRequests(log));
  api.get('/:spotId', checkSpotId, wrapErrors(getSpotHandler));
  api.get('/', wrapErrors(searchSpotsHandler));
  api.delete('/:spotId', checkSpotId, wrapErrors(deleteSpotHandler));
  api.put('/:spotId', checkSpotId, wrapErrors(updateSpotHandler));
  api.patch('/:spotId', checkSpotId, wrapErrors(patchSpotHandler));
  api.post('/:spotId/upload', checkSpotId, upload.single('thumbnail'), wrapErrors(uploadSpotThumbnailHandler));
  api.post('/', wrapErrors(createSpotHandler));

  return api;
};

export default spots;
