import { createTestClient } from 'apollo-server-testing';
import nock from 'nock';
// eslint-disable-next-line import/no-extraneous-dependencies
import sinon from 'sinon';
import { SLAPI } from '../datasources/slAPI';
import { MSWAPI } from '../datasources/mswAPI';
import logger from '../../../../common/logger';
import config from '../../../../config';
import graphql from '../../../middleware/graphql';
import { nockMSWLogin } from './testData/setUpNock';

export default () => {
  const log = logger('spots-api');

  before(() => {
    config.MSW_API = 'http://magicseaweedPretendDomain.com/apikey/';
    config.SL_API = 'http://surflinePretendDomain.com/apikey/';
    config.MSW_API_USER = 'test@user.com:testPass';
    sinon.spy(log, 'error');
  });

  after(() => {
    config.MSW_API = process.env.MSW_API;
    config.SL_API = process.env.SL_API;
    config.MSW_API_USER = process.env.MSW_API_USER;
    log.error.restore();
  });

  beforeEach(() => {
    sinon.spy(MSWAPI.prototype, 'get');
    sinon.spy(MSWAPI.prototype, 'post');
    sinon.spy(MSWAPI.prototype, 'put');
    sinon.spy(SLAPI.prototype, 'get');
    sinon.spy(SLAPI.prototype, 'post');
    sinon.spy(SLAPI.prototype, 'patch');
    sinon.spy(MSWAPI.prototype, 'login');
    nockMSWLogin();
  });

  afterEach(() => {
    MSWAPI.prototype.get.restore();
    MSWAPI.prototype.post.restore();
    MSWAPI.prototype.put.restore();
    SLAPI.prototype.get.restore();
    SLAPI.prototype.post.restore();
    SLAPI.prototype.patch.restore();
    MSWAPI.prototype.login.restore();
    nock.cleanAll();
  });

  return log;
};

export const getTestClient = async (log) => createTestClient(await graphql(log)()).query;
