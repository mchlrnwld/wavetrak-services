import { expect } from 'chai';
import { SLAPI } from '../datasources/slAPI';
import { MSWAPI } from '../datasources/mswAPI';
import {
  GET_SPOTS,
  GET_SPOTS_COMMON_FIELDS,
} from './testData/TestQueries';
import {
  nockSpot,
  nockError,
} from './testData/setUpNock';
import config from '../../../../config';
import {
  expectedMswSpot,
  expectedMswSpotParams,
  expectedMSWSpotCommonFields,
  expectedSlSpotParams,
  expectedSLSpotCommonFields,
  expectedSLSpot,
} from './testData/expectedResults';
import setUpGraphqlTest, { getTestClient } from './testSetup';

describe('/graphql multibrand spot queries', async () => {
  const log = setUpGraphqlTest();

  it('Should return all data for a query on all spots', async () => {
    nockSpot();
    const query = await getTestClient(log);
    const res = await query({
      query: GET_SPOTS,
      variables: {
        pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
      },
    });
    expect(MSWAPI.prototype.get.getCall(1).args).to.eql([`${config.MSW_API}spot/?`, expectedMswSpotParams]);
    expect(SLAPI.prototype.get).to.have.been.calledWith(`${config.SL_API}spots/?`, expectedSlSpotParams);
    expect(res.errors).to.eql(undefined);
    expect(res.data.spot).to.eql(
      [
        expectedMswSpot,
        expectedSLSpot,
      ],
    );
  });

  it('should return common fields for a query on all spots', async () => {
    nockSpot();
    const query = await getTestClient(log);
    const res = await query(
      {
        query: GET_SPOTS_COMMON_FIELDS,
        variables: {
          pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
        },
      },
    );
    expect(MSWAPI.prototype.get).to.have.been.calledWith(`${config.MSW_API}spot/?`, expectedMswSpotParams);
    expect(SLAPI.prototype.get).to.have.been.calledWith(`${config.SL_API}spots/?`, expectedSlSpotParams);
    expect(res.errors).to.eql(undefined);
    expect(res.data.spot).to.eql(
      [
        expectedMSWSpotCommonFields,
        expectedSLSpotCommonFields,
      ],
    );
  });

  it('should filter spots by brand', async () => {
    nockSpot();
    const query = await getTestClient(log);
    const res = await query(
      {
        query: GET_SPOTS_COMMON_FIELDS,
        variables: {
          pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
          brands: ['MSW'],
        },
      },
    );
    expect(MSWAPI.prototype.get).to.have.been.calledWith(`${config.MSW_API}spot/?`, expectedMswSpotParams);
    expect(SLAPI.prototype.get).to.have.not.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.spot).to.eql(
      [
        expectedMSWSpotCommonFields,
      ],
    );
  });

  it('should return an error if the remote API returns error data', async () => {
    nockError();
    const query = await getTestClient(log);
    const res = await query(
      {
        query: GET_SPOTS_COMMON_FIELDS,
        variables: {
          pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
          brands: ['MSW'],
        },
      },
    );
    expect(MSWAPI.prototype.get).to.have.been.calledWith(`${config.MSW_API}spot/?`, expectedMswSpotParams);
    expect(SLAPI.prototype.get).to.have.not.been.called();
    expect(res.errors[0].message).to.eql('Error message returned from MSW API: Test Error');
    expect(log.error).to.have.been.called();
  });
});
