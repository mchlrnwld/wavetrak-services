import { gql } from 'apollo-server-express';

const mswSpotFields = `
... on MSWSpot {
  surflineSpotId
  description
  hidden
  edited
  topLevelNav
  lotusGrid
  ratingType
  country {
    _id
    name
    iso
  }
  surfArea {
    _id
    name
  }
  region {
    _id
    name
  }
  stormrider {
    id
    name
    detail
    lat
    lon
  }
  types {
    _id
    name
  }
}
`;

const slSpotFields = `
... on SLSpot {
  timezone
  tideStation
  cams
  status
  spotType
  abilityLevels
  boardTypes
  relivableRating
  subregion
  geoname
  swellWindow {
    min
    max
  }
  best {
    windDirection {
      min
      max
    }
    swellPeriod {
      min
      max
    }
    swellWindow {
      min
      max
    }
    sizeRange {
      min
      max
    }
  }
  humanReport {
    status
    clone {
      spotId
    }
    average {
      spotOneId
      spotTwoId
    }
  }
  titleTag
  metaDescription
  travelDetails {
    travelId
    abilityLevels {
      description
      levels
      summary
    }
    boardTypes
    access
    best {
      season {
        description
        value
      }
      size {
        description
        value
      }
      tide {
        description
        value
      }
      windDirection {
        description
        value
      }
      swellDirection {
        description
        value
      }
    }
    bottom {
      description
      value
    }
    crowdFactor {
      description
      rating
      summary
    }
    description
    hazards
    localVibe {
      description
      rating
      summary
    }
    shoulderBurn {
      description
      rating
      summary
    }
    relatedArticleId
    spotRating {
      description
      rating
      summary
    }
    waterQuality {
      description
      rating
      summary
    }
    status
  }
}`;

const spotCommonFields = `
... on SpotCommonFields {
  _id
  brand
  pointOfInterestId
  name
  lat
  lon
  timezone
  pointOfInterest {
    pointOfInterestId
  }
}
`;

export const GET_SPOTS = gql`
  query getAllSpotDetails($pointOfInterestId: String!, $brands: [Brand]) {
    spot (pointOfInterestId: $pointOfInterestId, brands: $brands) {
      ${spotCommonFields}
      ${mswSpotFields}
      ${slSpotFields}
    }
  }
`;

export const GET_SPOTS_COMMON_FIELDS = gql`
  query getAllSpotDetails($pointOfInterestId: String!, $brands: [Brand]) {
    spot (pointOfInterestId: $pointOfInterestId, brands: $brands) {
      ${spotCommonFields}
    }
  }
`;

export const GET_COUNTRIES = gql`
  query getAllCountries($id: String) {
    mswCountry (id: $id) {
      _id
      name
    }
  }
`;

export const GET_SURFAREAS = gql`
  query getAllSurfareas($id: String) {
    mswSurfArea (id: $id) {
      _id
      name
      countryId
      timezone
    }
  }
`;

export const GET_REGIONS = gql`
  query getAllRegions($id: String) {
    mswRegion (id: $id) {
      _id
      name
    }
  }
`;

export const GET_SPOTTYPES = gql`
  query getAllSpotTypes($id: String) {
    mswSpotType (id: $id) {
      name
    }
  }
`;

export const CREATE_MSW_SPOT = gql`
  mutation upsertMSWSpot($spot: MSWSpotInput!) {
    upsertMSWSpot (spot: $spot) {
      success
      message
      validationErrors {
        key
        errors
      }
      spot {
        ${spotCommonFields}
        ${mswSpotFields}
      }
    }
  }
`;

export const CREATE_SL_SPOT = gql`
  mutation upsertSLSpot($spot: SLSpotInput!) {
    upsertSLSpot (spot: $spot) {
      success
      message
      validationErrors {
        key
        errors
      }
      spot {
        ${spotCommonFields}
        ${slSpotFields}
      }
    }
  }
`;
