import nock from 'nock';

export const mswSpot = {
  _id: 1,
  _obj: 'Spot',
  _path: 'Spot',
  name: 'Bantham',
  description: 'Bantham, Bigbury and Challaborough are the main focus of the South Devon scene and catch their share of swell',
  lat: 50.2787,
  lon: -3.8885,
  dataLat: 50,
  dataLon: -3.87,
  surfAreaId: 10,
  region: {
    _id: 1,
    _obj: 'Region',
    _path: 'Spot.Region',
  },
  country: {
    _id: 1,
    _obj: 'Country',
    _path: 'Spot.Country',
    iso: 'england',
    region: {
      _id: 1,
      _obj: 'Region',
      _path: 'Spot.Country.Region',
    },
  },
  types: [
    {
      _id: 1,
      _obj: 'SpotType',
      _path: 'SpotSpotTypeLink.SpotType',
      name: 'Surf',
    },
  ],
  dataSpotId: null,
  url: '/Bantham-Surf-Report/10/',
  multiplier: 0.8,
  optimumSwellAngle: 230,
  optimumWindAngle: 65,
  timezone: 'Europe/London',
  offset: 3600,
  modelName: 'glo_30m',
  isBigWave: false,
  ratingType: 'directional',
  timeZoneAbbr: 'BST',
  hasAdvancedForecast: false,
  proteusDataId: 2,
  proteusResolution: 'UK_4m',
  surflineSpotId: '584204204e65fad6a77090c9',
  defaultModelId: 42,
  topLevelNav: true,
  tidalPort: null,
  isDataSpot: true,
  favouriteCount: 0,
  mapImageUrl: 'https://chart-1.msw.ms/maps/spot/358c02090c0c31888fee4794b39d397c.png',
  breakingWaveModelId: null,
  weatherModel: null,
  added: -62169984000,
  hidden: true,
  edited: 1585164814,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  lotusGrid: '30M',
  stormrider: {
    id: 942,
    name: 'Bantham',
    detail: 'Consistent, classy beachbreak with sandbars sculpted by the River Avon. Right into the rivermouth and a peak on the main beach. South Devon’s best beach is always busy. Strong rips at size. Parking in the village and on Bigbury side of the river. ',
    lat: 50.2787,
    lon: -3.8885,
    hazards: 'Rip currents, Localism',
  },
};

const slSpot = {
  name: 'Fistral Beach',
  _id: '5ef354721fc3caa9193a3e00',
  location: {
    coordinates: [-5.1032781601, 50.4156609046],
    type: 'Point',
  },
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  timezone: 'Europe/London',
  tideStation: 'Fistral Beach',
  cams: ['5ef354721fc3caa9193a3e00', '5ef354721fc3caa9193a3e00'],
  weatherStations: ['5ef354721fc3caa9193a3e00', '5ef354721fc3caa9193a3e00'],
  status: 'PUBLISHED',
  spotType: 'SURFBREAK',
  abilityLevels: ['BEGINNER', 'INTERMEDIATE'],
  boardTypes: ['FISH', 'SHORTBOARD'],
  relivableRating: 3,
  subregion: '58581a836630e24c44879188',
  geoname: 2641589,
  swellWindow: [
    {
      min: 243,
      max: 284,
    },
  ],
  best: {
    windDirection: [
      {
        min: 107,
        max: 153,
      },
    ],
    swellPeriod: [
      {
        min: 0,
        max: 33,
      },
    ],
    swellWindow: [
      {
        min: 258,
        max: 276,
      },
    ],
    sizeRange: [
      {
        min: 2,
        max: 8,
      },
    ],
  },
  humanReport: {
    status: 'OFF',
    clone: {
      spotId: null,
    },
    average: {
      spotOneId: null,
      spotTwoId: null,
    },
  },
  titleTag: 'test title tag',
  metaDescription: 'test meta description',
  travelDetails: {
    travelId: 'testThing',
    abilityLevels: {
      description: 'testThing',
      levels: ['BEGINNER', 'INTERMEDIATE'],
      summary: 'BEGINNER, INTERMEDIATE',
    },
    boardTypes: ['fish'],
    access: 'testThing',
    best: {
      season: {
        description: 'testThing',
        value: ['Winter', 'Autumn'],
      },
      size: {
        description: 'testThing',
        value: ['1-2', '4-5'],
      },
      swellDirection: {
        description: 'testThing',
        value: ['NW'],
      },
      tide: {
        description: 'testThing',
        value: ['Low', 'Medium_Low'],
      },
      windDirection: {
        description: 'testThing',
        value: ['NW'],
      },
    },
    bottom: {
      description: 'testThing',
      value: ['Coral'],
    },
    crowdFactor: {
      description: 'testThing',
      rating: 0,
      summary: 'mellow',
    },
    description: 'testThing',
    hazards: 'testThing',
    localVibe: {
      description: 'testThing',
      rating: 0,
      summary: 'welcoming',
    },
    shoulderBurn: {
      description: 'testThing',
      rating: 0,
      summary: 'easy',
    },
    relatedArticleId: 'testThing',
    spotRating: {
      description: 'testThing',
      rating: 0,
      summary: 'poor',
    },
    waterQuality: {
      description: 'testThing',
      rating: 0,
      summary: 'poor',
    },
    status: 'DRAFT',
  },
};

export const nockMSWSpotSubfields = (mswNock) => {
  mswNock.get('/apikey/country/1')
    .query(true)
    .reply(
      200,
      [
        {
          _id: 1,
          _obj: 'Country',
          _path: 'Country',
          url: '/UK-Ireland-Surf-Forecast/1/',
          name: 'England',
          iso: 'england',
        },
      ],
    )
    .get('/apikey/region/1')
    .query(true)
    .reply(
      200,
      [
        {
          _id: 1,
          _obj: 'Region',
          _path: 'Region',
          url: '/UK-Ireland-Surf-Forecast/1/',
          liveUrl: '/UK-Ireland-Wave-Buoys/1/?wind=true&wave=true',
          name: 'UK + Ireland',
        },
      ],
    )
    .get('/apikey/surfarea/10')
    .query(true)
    .reply(
      200,
      [
        {
          _id: 10,
          _obj: 'SurfArea',
          _path: 'SurfArea',
          url: '/Cornwall-Devon-South-Surfing/5/',
          name: 'Cornwall & Devon - South',
          countryId: 1,
          timezone: 'Europe/London',
        },
      ],
    );
};

export const nockMSWLogin = () => {
  nock('http://magicseaweedpretenddomain.com')
    .get('/apikey/login')
    .query(true)
    .reply(200, {
      id: 1277545,
      access_token: 'eb695048cc0ef943704b726b2a20c6741457d1cc',
    });
};

export const nockSpot = () => {
  const mswNock = nock('http://magicseaweedpretenddomain.com')
    .defaultReplyHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache, no-store, max-age=0',
    })
    .get('/apikey/spot/')
    .query(true)
    .reply(
      200,
      [
        mswSpot,
      ],
    );

  nockMSWSpotSubfields(mswNock);

  nock('http://surflinepretenddomain.com')
    .defaultReplyHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache, no-store, max-age=0',
    })
    .get('/apikey/spots/')
    .query(true)
    .reply(
      200,
      [
        slSpot,
      ],
    );
};

export const nockError = () => {
  nock('http://magicseaweedpretenddomain.com')
    .get('/apikey/spot/')
    .query(true)
    .reply(
      200,
      {
        error_response: {
          error_msg: 'Test Error',
        },
      },
    );
};

export const nockCountries = () => {
  nock('http://magicseaweedpretenddomain.com')
    .get('/apikey/country/')
    .query(true)
    .reply(
      200,
      [
        {
          _id: 1,
          _obj: 'Country',
          _path: 'Country',
          url: '/UK-Ireland-Surf-Forecast/1/',
          name: 'Scotland',
          iso: 'scotland',
        },
        {
          _id: 2,
          _obj: 'Country',
          _path: 'Country',
          url: '/UK-Ireland-Surf-Forecast/1/',
          name: 'England',
          iso: 'england',
        },
      ],
    );
};

export const nockSurfAreas = () => {
  nock('http://magicseaweedpretenddomain.com')
    .get('/apikey/surfarea/')
    .query(true)
    .reply(200, [
      {
        _id: 1,
        _obj: 'SurfArea',
        _path: 'SurfArea',
        url: '/East-Anglia-Surfing/1/',
        name: 'East Anglia',
        countryId: 1,
        timezone: 'Europe/London',
      },
      {
        _id: 2,
        _obj: 'SurfArea',
        _path: 'SurfArea',
        url: '/Durham-Northumberland-Surfing/2/',
        name: 'Durham & Northumberland',
        countryId: 1,
        timezone: 'Europe/London',
      },
    ]);
};

export const nockRegions = () => {
  nock('http://magicseaweedpretenddomain.com')
    .get('/apikey/region/')
    .query(true)
    .reply(
      200,
      [
        {
          _id: 1,
          _obj: 'Region',
          _path: 'Region',
          url: '/UK-Ireland-Surf-Forecast/1/',
          liveUrl: '/UK-Ireland-Wave-Buoys/1/?wind=true&wave=true',
          name: 'UK + Ireland',
        },
        {
          _id: 2,
          _obj: 'Region',
          _path: 'Region',
          url: '/France-Surf-Forecast/2/',
          liveUrl: '/France-Wave-Buoys/2/?wind=true&wave=true',
          name: 'France',
        },
      ],
    );
};

export const nockSpotTypes = () => {
  nock('http://magicseaweedpretenddomain.com')
    .get('/apikey/spottype/')
    .query(true)
    .reply(
      200,
      [
        {
          _id: 1,
          _obj: 'SpotType',
          _path: 'SpotType',
          name: 'Surf',
        },
        {
          _id: 2,
          _obj: 'SpotType',
          _path: 'SpotType',
          name: 'Other',
        },
      ],
    );
};

export const nockCreateMSWSpot = () => {
  const mswNock = nock('http://magicseaweedpretenddomain.com')
    .post('/apikey/spot')
    .query(true)
    .reply(
      200,
      {
        valid: true,
        _id: 1,
      },
    )
    .get('/apikey/spot/1')
    .query(true)
    .reply(
      200,
      [
        mswSpot,
      ],
    );

  nockMSWSpotSubfields(mswNock);
};

export const nockUpdateMSWSpot = () => {
  const mswNock = nock('http://magicseaweedpretenddomain.com')
    .put('/apikey/spot/1')
    .query(true)
    .reply(
      200,
      {
        valid: true,
        _id: 1,
      },
    )
    .get('/apikey/spot/1')
    .query(true)
    .reply(
      200,
      [
        mswSpot,
      ],
    );

  nockMSWSpotSubfields(mswNock);
};

export const nockCreateSLSpot = () => {
  nock('http://surflinepretenddomain.com')
    .post('/apikey/spots')
    .query(true)
    .reply(
      200,
      slSpot,
    );
};

export const nockUpdateSLSpot = () => {
  nock('http://surflinepretenddomain.com')
    .patch('/apikey/spots/5ef354721fc3caa9193a3e00')
    .query(true)
    .reply(
      200,
      slSpot,
    );
};
