import { MSW, SL } from '../../constants';

export const expectedMswSpotParams = {
  access_token: 'eb695048cc0ef943704b726b2a20c6741457d1cc',
  bustCache: true,
  bustMemCache: true,
  fields: '*,country,region,stormrider,types',
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  showHidden: true,
};

export const expectedSlSpotParams = {
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
};

export const expectedMswSpot = {
  _id: '1',
  brand: MSW,
  name: 'Bantham',
  description: 'Bantham, Bigbury and Challaborough are the main focus of the South Devon scene and catch their share of swell',
  lat: 50.2787,
  lon: -3.8885,
  ratingType: 'RATING_DIRECTIONAL',
  surfArea: {
    _id: 10,
    name: 'Cornwall & Devon - South',
  },
  region: {
    _id: 1,
    name: 'UK + Ireland',
  },
  country: {
    _id: 1,
    iso: 'england',
    name: 'England',
  },
  types: [
    {
      name: 'Surf',
      _id: 1,
    },
  ],
  stormrider: {
    id: 942,
    name: 'Bantham',
    detail: 'Consistent, classy beachbreak with sandbars sculpted by the River Avon. Right into the rivermouth and a peak on the main beach. South Devon’s best beach is always busy. Strong rips at size. Parking in the village and on Bigbury side of the river. ',
    lat: 50.2787,
    lon: -3.8885,
  },
  timezone: 'Europe/London',
  surflineSpotId: '584204204e65fad6a77090c9',
  topLevelNav: true,
  hidden: true,
  edited: 1585164814,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  lotusGrid: '30M',
  pointOfInterest: {
    pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  },
};

export const expectedSLSpot = {
  brand: SL,
  name: 'Fistral Beach',
  _id: '5ef354721fc3caa9193a3e00',
  lat: 50.4156609046,
  lon: -5.1032781601,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  timezone: 'Europe/London',
  tideStation: 'Fistral Beach',
  cams: ['5ef354721fc3caa9193a3e00', '5ef354721fc3caa9193a3e00'],
  status: 'PUBLISHED',
  spotType: 'SURFBREAK',
  abilityLevels: ['BEGINNER', 'INTERMEDIATE'],
  boardTypes: ['FISH', 'SHORTBOARD'],
  relivableRating: 3,
  subregion: '58581a836630e24c44879188',
  geoname: 2641589,
  swellWindow: [
    {
      min: 243,
      max: 284,
    },
  ],
  best: {
    windDirection: [
      {
        min: 107,
        max: 153,
      },
    ],
    swellPeriod: [
      {
        min: 0,
        max: 33,
      },
    ],
    swellWindow: [
      {
        min: 258,
        max: 276,
      },
    ],
    sizeRange: [
      {
        min: 2,
        max: 8,
      },
    ],
  },
  humanReport: {
    status: 'OFF',
    clone: {
      spotId: null,
    },
    average: {
      spotOneId: null,
      spotTwoId: null,
    },
  },
  titleTag: 'test title tag',
  metaDescription: 'test meta description',
  pointOfInterest: {
    pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  },
  travelDetails: {
    travelId: 'testThing',
    abilityLevels: {
      description: 'testThing',
      levels: ['BEGINNER', 'INTERMEDIATE'],
      summary: 'BEGINNER, INTERMEDIATE',
    },
    boardTypes: ['fish'],
    access: 'testThing',
    best: {
      season: {
        description: 'testThing',
        value: ['Winter', 'Autumn'],
      },
      size: {
        description: 'testThing',
        value: ['1-2', '4-5'],
      },
      swellDirection: {
        description: 'testThing',
        value: ['NW'],
      },
      tide: {
        description: 'testThing',
        value: ['Low', 'Medium_Low'],
      },
      windDirection: {
        description: 'testThing',
        value: ['NW'],
      },
    },
    bottom: {
      description: 'testThing',
      value: ['Coral'],
    },
    crowdFactor: {
      description: 'testThing',
      rating: 0,
      summary: 'mellow',
    },
    description: 'testThing',
    hazards: 'testThing',
    localVibe: {
      description: 'testThing',
      rating: 0,
      summary: 'welcoming',
    },
    shoulderBurn: {
      description: 'testThing',
      rating: 0,
      summary: 'easy',
    },
    relatedArticleId: 'testThing',
    spotRating: {
      description: 'testThing',
      rating: 0,
      summary: 'poor',
    },
    waterQuality: {
      description: 'testThing',
      rating: 0,
      summary: 'poor',
    },
    status: 'DRAFT',
  },
};

export const expectedMSWSpotCommonFields = {
  _id: '1',
  brand: MSW,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  name: 'Bantham',
  lat: 50.2787,
  lon: -3.8885,
  timezone: 'Europe/London',
  pointOfInterest: {
    pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  },
};

export const expectedSLSpotCommonFields = {
  brand: SL,
  name: 'Fistral Beach',
  _id: '5ef354721fc3caa9193a3e00',
  lat: 50.4156609046,
  lon: -5.1032781601,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  timezone: 'Europe/London',
  pointOfInterest: {
    pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  },
};
