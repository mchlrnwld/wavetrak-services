import { expect } from 'chai';
import { MSWAPI } from '../datasources/mswAPI';
import { SLAPI } from '../datasources/slAPI';
import {
  GET_COUNTRIES, GET_REGIONS,
  GET_SURFAREAS,
  GET_SPOTTYPES,
} from './testData/TestQueries';
import {
  nockCountries,
  nockSurfAreas,
  nockRegions,
  nockSpotTypes,
} from './testData/setUpNock';
import setUpGraphqlTest, { getTestClient } from './testSetup';

describe('/graphql supporting msw data', async () => {
  const log = setUpGraphqlTest();

  it('should return a list of countries', async () => {
    const query = await getTestClient(log);
    nockCountries();
    const res = await query({ query: GET_COUNTRIES });
    expect(MSWAPI.prototype.get).to.have.been.calledWith('http://magicseaweedPretendDomain.com/apikey/country/?');
    expect(SLAPI.prototype.get).to.have.not.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.mswCountry).to.eql(
      [
        {
          _id: 2,
          name: 'England',
        },
        {
          _id: 1,
          name: 'Scotland',
        },
      ],
    );
  });

  it('should return a list of surfAreas', async () => {
    nockSurfAreas();
    const query = await getTestClient(log);
    const res = await query({ query: GET_SURFAREAS });
    expect(MSWAPI.prototype.get).to.have.been.calledWith('http://magicseaweedPretendDomain.com/apikey/surfarea/?');
    expect(SLAPI.prototype.get).to.have.not.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.mswSurfArea).to.eql([
      {
        _id: 2,
        name: 'Durham & Northumberland',
        timezone: 'Europe/London',
        countryId: 1,
      },
      {
        _id: 1,
        name: 'East Anglia',
        timezone: 'Europe/London',
        countryId: 1,
      },
    ]);
  });

  it('should return a list of Regions', async () => {
    nockRegions();
    const query = await getTestClient(log);
    const res = await query({ query: GET_REGIONS });
    expect(MSWAPI.prototype.get).to.have.been.calledWith('http://magicseaweedPretendDomain.com/apikey/region/?');
    expect(SLAPI.prototype.get).to.have.not.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.mswRegion).to.eql(
      [
        {
          _id: 2,
          name: 'France',
        },
        {
          _id: 1,
          name: 'UK + Ireland',
        },
      ],
    );
  });

  it('should return a list of SpotTypes', async () => {
    nockSpotTypes();
    const query = await getTestClient(log);
    const res = await query({ query: GET_SPOTTYPES });
    expect(MSWAPI.prototype.get).to.have.been.calledWith('http://magicseaweedPretendDomain.com/apikey/spottype/?');
    expect(SLAPI.prototype.get).to.have.not.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.mswSpotType).to.eql(
      [
        { name: 'Other' },
        { name: 'Surf' },
      ],
    );
  });
});
