import { expect } from 'chai';
import { MSWAPI } from '../datasources/mswAPI';
import {
  CREATE_MSW_SPOT,
} from './testData/TestQueries';
import { expectedMswSpot } from './testData/expectedResults';
import {
  nockCreateMSWSpot,
  nockUpdateMSWSpot,
} from './testData/setUpNock';
import setUpGraphqlTest, { getTestClient } from './testSetup';

const spot = {
  name: 'Bantham',
  description: 'Bantham, Bigbury and Challaborough are the main focus of the South Devon scene and catch their share of swell',
  lat: 50.2787,
  lon: -3.8885,
  surfAreaId: 10,
  countryId: 1,
  types: [
    { name: 'Surf', _id: 1 },
  ],
  stormriderSpotId: 942,
  timezone: 'Europe/London',
  surflineSpotId: '584204204e65fad6a77090c9',
  topLevelNav: true,
  hidden: true,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  ratingType: 'RATING_DIRECTIONAL',
};

describe('/graphql MSW mutations', () => {
  const log = setUpGraphqlTest();

  it('creates a MSW spot', async () => {
    nockCreateMSWSpot();

    const query = await getTestClient(log);

    const res = await query({
      query: CREATE_MSW_SPOT,
      variables: { spot },
    });

    expect(MSWAPI.prototype.post.getCall(0).args[0]).to.eql('http://magicseaweedPretendDomain.com/apikey/spot?access_token=eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(JSON.parse(MSWAPI.prototype.post.getCall(0).args[1])).to.eql({
      ...spot,
      ratingType: 'directional',
    });
    expect(MSWAPI.prototype.login).to.have.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.upsertMSWSpot).to.eql(
      {
        spot: expectedMswSpot,
        message: 'Spot successfully created',
        validationErrors: null,
        success: true,
      },
    );
  });

  it('updates a MSW spot', async () => {
    nockUpdateMSWSpot();
    const query = await getTestClient(log);

    const thisSpot = {
      ...spot,
      _id: '1',
    };

    const res = await query({
      query: CREATE_MSW_SPOT,
      variables: {
        spot: thisSpot,
      },
    });

    thisSpot.ratingType = 'directional';

    expect(MSWAPI.prototype.put.getCall(0).args[0]).to.eql('http://magicseaweedPretendDomain.com/apikey/spot/1?access_token=eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(JSON.parse(MSWAPI.prototype.put.getCall(0).args[1])).to.eql(thisSpot);
    expect(MSWAPI.prototype.login).to.have.been.called();
    expect(res.errors).to.eql(undefined);
    expect(res.data.upsertMSWSpot).to.eql(
      {
        spot: expectedMswSpot,
        message: 'Spot successfully updated',
        validationErrors: null,
        success: true,
      },
    );
  });
});
