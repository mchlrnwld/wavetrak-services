import { expect } from 'chai';
import { SLAPI } from '../datasources/slAPI';
import {
  CREATE_SL_SPOT,
} from './testData/TestQueries';
import {
  nockCreateSLSpot,
  nockUpdateSLSpot,
} from './testData/setUpNock';
import setUpGraphqlTest, { getTestClient } from './testSetup';
import { expectedSLSpot } from './testData/expectedResults';

const spot = {
  name: 'Fistral Beach',
  lat: 50.4156609046,
  lon: 354.9,
  pointOfInterestId: '1684f484-1e02-4fcc-abcb-c8105711c6e1',
  timezone: 'Europe/London',
  tideStation: 'Fistral Beach',
  cams: ['5ef354721fc3caa9193a3e00', '5ef354721fc3caa9193a3e00'],
  weatherStations: ['5ef354721fc3caa9193a3e00', '5ef354721fc3caa9193a3e00'],
  status: 'PUBLISHED',
  spotType: 'SURFBREAK',
  abilityLevels: ['BEGINNER', 'INTERMEDIATE'],
  boardTypes: ['FISH', 'SHORTBOARD'],
  relivableRating: 3,
  subregion: '58581a836630e24c44879188',
  geoname: 2641589,
  swellWindow: [
    {
      min: 243,
      max: 284,
    },
  ],
  best: {
    windDirection: [
      {
        min: 107,
        max: 153,
      },
    ],
    swellPeriod: [
      {
        min: 0,
        max: 33,
      },
    ],
    swellWindow: [
      {
        min: 258,
        max: 276,
      },
    ],
    sizeRange: [
      {
        min: 2,
        max: 8,
      },
    ],
  },
  humanReport: {
    status: 'OFF',
    clone: {
      spotId: null,
    },
    average: {
      spotOneId: null,
      spotTwoId: null,
    },
  },
  titleTag: 'test title tag',
  metaDescription: 'test meta description',
  travelDetails: {
    travelId: 'testThing',
    abilityLevels: {
      description: 'testThing',
    },
    access: 'testThing',
    best: {
      season: {
        description: 'testThing',
        value: ['Winter', 'Autumn'],
      },
      size: {
        description: 'testThing',
      },
      swellDirection: {
        description: 'testThing',
      },
      tide: {
        description: 'testThing',
        value: ['Low', 'Medium_Low'],
      },
      windDirection: {
        description: 'testThing',
      },
    },
    bottom: {
      description: 'testThing',
      value: ['Coral'],
    },
    breakType: ['Reef'],
    crowdFactor: {
      description: 'testThing',
      rating: 0,
    },
    description: 'testThing',
    hazards: 'testThing',
    localVibe: {
      description: 'testThing',
      rating: 0,
    },
    shoulderBurn: {
      description: 'testThing',
      rating: 0,
    },
    relatedArticleId: 'testThing',
    spotRating: {
      description: 'testThing',
      rating: 0,
    },
    waterQuality: {
      description: 'testThing',
      rating: 0,
    },
    status: 'DRAFT',
  },
};

describe('/graphql SL mutations', async () => {
  const log = setUpGraphqlTest();

  it('creates a SL spot', async () => {
    const query = await getTestClient(log);
    nockCreateSLSpot();

    const res = await query({
      query: CREATE_SL_SPOT,
      variables: { spot },
    });

    expect(SLAPI.prototype.post.getCall(0).args[0]).to.eql('http://surflinePretendDomain.com/apikey/spots');
    expect(JSON.parse(SLAPI.prototype.post.getCall(0).args[1])).to.eql({
      ...spot,
      location: [
        -5.1,
        50.4156609046,
      ],
      lon: -5.1,
    });
    expect(res.errors).to.eql(undefined);
    expect(res.data.upsertSLSpot).to.eql(
      {
        spot: expectedSLSpot,
        message: 'Spot successfully created',
        validationErrors: null,
        success: true,
      },
    );
  });

  it('updates a SL spot', async () => {
    nockUpdateSLSpot();
    const query = await getTestClient(log);

    const thisSpot = {
      ...spot,
      _id: '5ef354721fc3caa9193a3e00',
    };

    const res = await query({
      query: CREATE_SL_SPOT,
      variables: {
        spot: thisSpot,
      },
    });

    expect(SLAPI.prototype.patch.getCall(0).args[0]).to.eql('http://surflinePretendDomain.com/apikey/spots/5ef354721fc3caa9193a3e00');
    expect(JSON.parse(SLAPI.prototype.patch.getCall(0).args[1])).to.eql({
      ...thisSpot,
      location: [
        -5.1,
        50.4156609046,
      ],
      lon: -5.1,
    });
    expect(res.errors).to.eql(undefined);
    expect(res.data.upsertSLSpot).to.eql(
      {
        spot: expectedSLSpot,
        message: 'Spot successfully updated',
        validationErrors: null,
        success: true,
      },
    );
  });
});
