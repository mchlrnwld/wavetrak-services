import spotTypeDefs from './typeDefs/spotTypeDefs';
import spotResolver from './resolvers/spotResolver';

const spotMetaSchema = {
  typeDefs: spotTypeDefs,
  resolvers: spotResolver,
};

export default spotMetaSchema;
