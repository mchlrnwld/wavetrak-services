import { RESTDataSource } from 'apollo-datasource-rest';
import { ApolloError, UserInputError } from 'apollo-server-express';
import config from '../../../../config';

export default class BrandAPI extends RESTDataSource {
  /**
   * @constructor
   * @param  {string} brand used to select correct API from config and append brand to the results
   */
  constructor(brand) {
    super();
    this.baseURL = config[`${brand}_API`];
    this.brand = brand;
  }

  /**
   * handles all requests, wraps an async function with error handling
   * @param  {function} func the function to be executed
   * @returns {Promise} promise should resolve to an array of objects
   */
  async handleRequest(func) {
    let data;

    try {
      data = await func();
      if (!data) throw new Error('No Data Received from Server');
    } catch (error) {
      throw new ApolloError(`Invalid response from ${this.brand} server: ${error}`);
    }

    if (typeof data === 'string' || 'error_response' in data) {
      const code = data.error_response && data.error_response.code;
      if (code === 401) {
        throw new Error('Access Token Invalid');
      }
      const message = data.error_response ? data.error_response.error_msg : data;

      throw new ApolloError(`Error message returned from ${this.brand} API: ${message}`);
    }

    if (data.valid === false) {
      const validationErrors = Object.keys(data.errors).map((key) => ({
        key,
        errors: data.errors[key],
      }));
      throw new UserInputError(`Validation Failed on ${this.brand} API`, { validationErrors });
    }

    return data;
  }

  /**
   * handles generic get requests for a spot API where the id forms the end of the url if present
   * @param  {string/int} id id of the spot requested
   * @param  {string} path the path after the base path (expecting trailing slash on base path)
   * @param  {object} queryParams=null object that will be used to create the query string
   * @returns {Promise} promise should resolve to an array of objects
   */
  async handleGetRequest(id, path, queryParams = null) {
    return this.handleRequest(() => (
      this.get(`${this.baseURL}${path}/${id || ''}?`, queryParams)
    ));
  }

  /**
   * adds a brand field to each result returned from a get request
   * @param  {[object]} data an array of objects that need a brand field adding
   * @returns {[object]}
   */
  addBrandToResult(data) {
    return data.map((result) => ({
      ...result,
      brand: this.brand,
    }));
  }
}
