/* eslint-disable camelcase */
import btoa from 'btoa';
import { ApolloError } from 'apollo-server-express';
import BrandAPI from './brandAPI';
import { MSW, MSWRatingTypes } from '../constants';
import config from '../../../../config';

/**
 * sorts the array of objects based on their name prop
 * @param  {Array} collection array of objects with a name prop
 */
const sortNames = (collection) => collection.sort((a, b) => {
  const nameA = a.name.toUpperCase(); // ignore upper and lowercase
  const nameB = b.name.toUpperCase(); // ignore upper and lowercase
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  // names must be equal
  return 0;
});

// eslint-disable-next-line import/prefer-default-export
export class MSWAPI extends BrandAPI {
  constructor(extraData) {
    super(MSW);
    this.extraData = extraData || {};
    this.retryLoginCount = 0;
  }

  /**
   * @param  {int/string} id optional, if not specified then all SurfAreas are returned
   * @returns {[object]} an array of surfarea
   */
  async getSurfAreas(id) {
    return sortNames(await this.handleGetRequest(id, 'surfarea', { limit: -1 }));
  }

  /**
   * @param  {int/string} id optional, if not specified then all Countries are returned
   * @returns {[object]} an array of country
   */
  async getCountries(id) {
    return sortNames(await this.handleGetRequest(id, 'country'));
  }

  /**
   * @param  {int/string} id optional, if not specified then all Regions are returned
   * @returns {[object]} an array of region
   */
  async getRegions(id) {
    return sortNames(await this.handleGetRequest(id, 'region'));
  }

  /**
   * @param  {int/string} id optional, if not specified then all spotTypes are returned
   * @returns {[object]} an array of spottype
   */
  async getSpotTypes(id) {
    return sortNames(await this.handleGetRequest(id, 'spottype'));
  }

  /**
   * creates an accessToken on the class if there isn't one there already
   * @returns {string} the access token too in case you need to use it straight away
   */
  async login() {
    if (this.extraData.accessToken) return this.extraData.accessToken;

    const { access_token, error_response } = await this.get(`${this.baseURL}login`, undefined, {
      headers: {
        'X-MSW-Authorization': btoa(config.MSW_API_USER),
      },
    });

    if (!access_token || error_response) {
      throw new Error(
        `Unable to login to the msw API: ${error_response ? error_response.error_msg : ''}`,
      );
    }

    this.extraData.accessToken = access_token;

    return this.extraData.accessToken;
  }

  /**
   * Gets an array of spots for either a spot id or point of interest id or in theory all spots
   * takes an object so we don't have to pass all the params
   * @param  {} {id=null if this is provided then there should only be one spot
   * @param  {} pointOfInterestId=null the poiID that we're searching by if required
   * @param  {} bustCache=false}
   * @returns {[object]} an array of mswSpots (subfields get resolved separately if requested)
   */
  async getSpots({ id = null, pointOfInterestId = null }) {
    const params = {
      bustCache: true,
      bustMemCache: true,
      access_token: this.extraData.accessToken,
      showHidden: true,
      fields: '*,country,region,stormrider,types',
    };

    if (pointOfInterestId) {
      params.pointOfInterestId = pointOfInterestId;
    }

    const data = await this.handleGetRequest(id, 'spot', params);

    return this.addBrandToResult(data);
  }

  /**
   * Updates an msw spot via the msw API
   * @param  {object} spot the data that will be sent to the API
   * @returns {object} an mswSpot (subfields like surfArea get resolved separately if requested)
   */
  async updateSpot(spot) {
    const ratingType = MSWRatingTypes[spot.ratingType];
    try {
      await this.handleRequest(() => this.put(
        `${this.baseURL}spot/${spot._id}?access_token=${this.extraData.accessToken}`,
        JSON.stringify({ ...spot, ratingType }),
      ));
    } catch (error) {
      return this.handleAuthErrors(error, spot, 'updateSpot');
    }

    return (await this.getSpots({ id: spot._id })).shift();
  }

  /**
   * Creates a new spot via the msw API
   * @param  {object} spot the data that will be sent to the API
   * @returns {object} an mswSpot (subfields like surfArea get resolved separately if requested)
   */
  async createSpot(spot) {
    // convert from the enum to the value expected from the msw API
    const ratingType = MSWRatingTypes[spot.ratingType];
    try {
      const createResult = await this.handleRequest(() => this.post(
        `${this.baseURL}spot?access_token=${this.extraData.accessToken}`,
        JSON.stringify({ ...spot, ratingType }),
      ));

      if (createResult._id) {
        return (await this.getSpots({ id: createResult._id })).shift();
      }

      throw new ApolloError(
        `Invalid response from ${this.brand} server, no _id present in response: ${JSON.stringify(
          createResult,
        )}`,
      );
    } catch (error) {
      return this.handleAuthErrors(error, spot, 'createSpot');
    }
  }

  /** Handles authentications errors for a given method
   * @param  {} error the error object that might contain an invalid access token
   * @param  {} spot the spot that was trying to be updated/created
   * @param  {} methodName the name of the method that is calling this method
   */
  async handleAuthErrors(error, spot, methodName) {
    if (error.message === 'Access Token Invalid') {
      if (this.retryLoginCount < 1) {
        this.retryLoginCount += 1;
        this.extraData.accessToken = null;
        await this.login();
        if (this.extraData.accessToken) return this[methodName](spot);
      } else {
        throw new ApolloError(
          'Maximum Automatic MSW Login Attempts Exceeded, please check credentials for spots-api',
        );
      }
    }

    throw error;
  }
}
