import { MSWAPI } from './mswAPI';
import { SLAPI } from './slAPI';
import { MSW, SL } from '../constants';

export default (mswExtraData) => () => ({
  [MSW]: new MSWAPI(mswExtraData),
  [SL]: new SLAPI(),
});
