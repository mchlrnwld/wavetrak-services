import BrandAPI from './brandAPI';
import { SL } from '../constants';
import { latLonToLocation, locationToLatLon } from '../utils';

// eslint-disable-next-line import/prefer-default-export
export class SLAPI extends BrandAPI {
  constructor() {
    super(SL);
  }

  /**
   * takes an object so that it has the same interface as slAPI.getSpots
   * @param  {string} {pointOfInterestId} gets all spots for a given POI id
   * @returns {[object]} an array of SL spots
   */
  async getSpots({ pointOfInterestId }) {
    const data = await this.handleGetRequest(null, 'spots', { pointOfInterestId });

    return this.addBrandToResult(data.map(locationToLatLon));
  }

  /**
   * Updates an sl spot via the sl API
   * @param  {object} spot the data that will be sent to the API
   * @returns {object} an slSpot (subfields like surfArea get resolved separately if requested)
   */
  async updateSpot(spot) {
    const data = await this.handleRequest(() => (
      this.patch(`${this.baseURL}spots/${spot._id}`, JSON.stringify(latLonToLocation(spot)), {
        headers: {
          'content-type': 'application/json',
        },
      })
    ));

    return this.addBrandToResult([locationToLatLon(data)]).shift();
  }

  /**
   * Creates a new spot via the sl API
   * @param  {object} spot the data that will be sent to the API
   * @returns {object} an slSpot (subfields like surfArea get resolved separately if requested)
   */
  async createSpot(spot) {
    const data = await this.handleRequest(() => (
      this.post(`${this.baseURL}spots`, JSON.stringify(latLonToLocation(spot)), {
        headers: {
          'content-type': 'application/json',
        },
      })
    ));

    return this.addBrandToResult([locationToLatLon(data)]).shift();
  }
}
