import sinon from 'sinon';
import nock from 'nock';
import { expect } from 'chai';
import { MSWAPI } from './mswAPI';
import config from '../../../../config';
import { mswSpot, nockMSWSpotSubfields } from '../integrationTests/testData/setUpNock';

describe('MSWAPI class', async () => {
  const extraData = { accessToken: null };

  beforeEach(() => {
    sinon.stub(MSWAPI.prototype, 'get');
    config.MSW_API_USER = 'test@spots-api.com:testPass';
    config.MSW_API = 'http://magicseaweedpretenddomain.com/apikey/';
  });

  afterEach(() => {
    MSWAPI.prototype.get.restore();
    nock.cleanAll();
    config.MSW_API_USER = process.env.MSW_API_USER;
    config.MSW_API = process.env.MSW_API;
  });

  it('should authenticate with the msw API', async () => {
    MSWAPI.prototype.get.resolves(
      {
        id: 1277545,
        access_token: 'eb695048cc0ef943704b726b2a20c6741457d1cc',
      },
    );

    const testAPI = new MSWAPI(extraData);
    await testAPI.login();

    expect(testAPI.extraData.accessToken).to.equal('eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(extraData.accessToken).to.equal('eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(MSWAPI.prototype.get).to.have.been.calledWith(
      'http://magicseaweedpretenddomain.com/apikey/login',
      undefined,
      {
        headers: {
          'X-MSW-Authorization': 'dGVzdEBzcG90cy1hcGkuY29tOnRlc3RQYXNz',
        },
      },
    );
    const testAPI2 = new MSWAPI(extraData);
    await testAPI2.login();

    expect(testAPI2.extraData.accessToken).to.equal('eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(extraData.accessToken).to.equal('eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(MSWAPI.prototype.get.getCalls().length).to.equal(1);
  });

  it('should log an error message if msw API auth fails', async () => {
    MSWAPI.prototype.get.resolves(
      {
        error_response: {
          code: 401,
          error_msg: 'Login details not recognised',
        },
      },
    );
    const testAPI = new MSWAPI({});
    let testError;

    try {
      await testAPI.login();
    } catch (error) {
      testError = error;
    }

    expect(testAPI.extraData.accessToken).to.equal(undefined);
    expect(MSWAPI.prototype.get).to.have.been.calledWith(
      'http://magicseaweedpretenddomain.com/apikey/login',
      undefined,
      {
        headers: {
          'X-MSW-Authorization': 'dGVzdEBzcG90cy1hcGkuY29tOnRlc3RQYXNz',
        },
      },
    );
    expect(testError.message).to.equal('Unable to login to the msw API: Login details not recognised');
  });

  it('Should retry the login if a call returns a 401 (PUT)', async () => {
    MSWAPI.prototype.get.restore();
    sinon.spy(MSWAPI.prototype, 'get');
    sinon.spy(MSWAPI.prototype, 'put');
    const mswNock = nock('http://magicseaweedpretenddomain.com')
      .put('/apikey/spot/1')
      .query(true)
      .reply(200, {
        error_response: {
          code: 401,
          error_msg: 'Must be an admin',
        },
      })
      .get('/apikey/login')
      .query(true)
      .reply(200, {
        id: 1277545,
        access_token: 'eb695048cc0ef943704b726b2a20c6741457d1cc',
      })
      .put('/apikey/spot/1')
      .query(true)
      .reply(200, {
        valid: true,
        _id: 1,
      })
      .get('/apikey/spot/1')
      .query(true)
      .reply(200, [mswSpot]);

    nockMSWSpotSubfields(mswNock);

    const testAPI = new MSWAPI({ accessToken: 'invalidAccessToken' });
    testAPI.initialize({});
    const data = await testAPI.updateSpot({ _id: 1 });

    expect(testAPI.extraData.accessToken).to.equal('eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(MSWAPI.prototype.get).to.have.been.calledWith(
      'http://magicseaweedpretenddomain.com/apikey/login',
      undefined,
      {
        headers: {
          'X-MSW-Authorization': 'dGVzdEBzcG90cy1hcGkuY29tOnRlc3RQYXNz',
        },
      },
    );
    expect(MSWAPI.prototype.put.getCall(0).args[0]).to.eql(
      'http://magicseaweedpretenddomain.com/apikey/spot/1?access_token=invalidAccessToken',
    );
    expect(MSWAPI.prototype.put.getCall(1).args[0]).to.eql(
      'http://magicseaweedpretenddomain.com/apikey/spot/1?access_token=eb695048cc0ef943704b726b2a20c6741457d1cc',
    );
    expect(data).to.eql({ ...mswSpot, brand: 'MSW' });
    MSWAPI.prototype.put.restore();
  });

  it('Should not retry the login if a second call returns a 401 (POST)', async () => {
    MSWAPI.prototype.get.restore();
    sinon.spy(MSWAPI.prototype, 'get');
    sinon.spy(MSWAPI.prototype, 'post');
    nock('http://magicseaweedpretenddomain.com')
      .post('/apikey/spot')
      .query(true)
      .reply(200, {
        error_response: {
          code: 401,
          error_msg: 'Must be an admin',
        },
      })
      .get('/apikey/login')
      .query(true)
      .reply(200, {
        id: 1277545,
        access_token: 'eb695048cc0ef943704b726b2a20c6741457d1cc',
      })
      .post('/apikey/spot')
      .query(true)
      .reply(200, {
        error_response: {
          code: 401,
          error_msg: 'Must be an admin',
        },
      })
      .post('/apikey/spot')
      .query(true)
      .reply(200, {
        error_response: {
          code: 401,
          error_msg: 'Must be an admin',
        },
      });

    const testAPI = new MSWAPI({ accessToken: 'invalidAccessToken' });
    testAPI.initialize({});

    let testError;

    try {
      await testAPI.createSpot({ name: 'testSpot' });
    } catch (err) {
      testError = err;
    }

    expect(testError.message).to.equal(
      'Maximum Automatic MSW Login Attempts Exceeded, please check credentials for spots-api',
    );

    expect(MSWAPI.prototype.post.getCalls().length).to.equal(2);

    expect(testAPI.extraData.accessToken).to.equal('eb695048cc0ef943704b726b2a20c6741457d1cc');
    expect(MSWAPI.prototype.get).to.have.been.calledWith(
      'http://magicseaweedpretenddomain.com/apikey/login',
      undefined,
      {
        headers: {
          'X-MSW-Authorization': 'dGVzdEBzcG90cy1hcGkuY29tOnRlc3RQYXNz',
        },
      },
    );
    expect(MSWAPI.prototype.post.getCall(0).args[0]).to.eql(
      'http://magicseaweedpretenddomain.com/apikey/spot?access_token=invalidAccessToken',
    );
    expect(MSWAPI.prototype.post.getCall(1).args[0]).to.eql(
      'http://magicseaweedpretenddomain.com/apikey/spot?access_token=eb695048cc0ef943704b726b2a20c6741457d1cc',
    );
    MSWAPI.prototype.post.restore();
  });
});
