export const MSW = 'MSW';
export const SL = 'SL';

export const brands = [
  MSW,
  SL,
];

export const MSWRatingTypes = {
  RATING_DIRECTIONAL: 'directional',
  RATING_DOMINANT: 'dominant',
  RATING_BIGWAVE: 'SRSBigWave',
};
