import { SL } from '../constants';
import { buildTypesAndInputs } from '../utils';

const SLSpotFields = `
  tideStation: String
  cams: [String]
  weatherStations: [String]
  status: ${SL}Status
  spotType: ${SL}SpotType
  abilityLevels: [${SL}AbilityLevel]
  boardTypes: [${SL}BoardTypes]
  relivableRating: Int
  titleTag: String
  metaDescription: String
`;

const SLSpotSubTypes = {
  [`${SL}Range`]: {
    min: 'Float',
    max: 'Float',
  },
  [`${SL}Best`]: {
    windDirection: `[${SL}Range]`,
    swellPeriod: `[${SL}Range]`,
    swellWindow: `[${SL}Range]`,
    sizeRange: `[${SL}Range]`,
  },
  [`${SL}HumanReport`]: {
    status: `${SL}HumanReportStatus`,
    clone: `${SL}HumanReportClone`,
    average: `${SL}HumanReportAverage`,
  },
  [`${SL}HumanReportClone`]: {
    spotId: 'String',
  },
  [`${SL}HumanReportAverage`]: {
    spotOneId: 'String',
    spotTwoId: 'String',
  },
  [`${SL}TravelDetails`]: {
    travelId: 'String',
    abilityLevels: `${SL}AbilityLevels`,
    access: 'String',
    best: `${SL}TravelDetailsBest`,
    bottom: `${SL}TravelDetailsBottom`,
    breakType: `[${SL}BreakTypes]`,
    crowdFactor: `${SL}TravelDetailsItem`,
    description: 'String',
    hazards: 'String',
    localVibe: `${SL}TravelDetailsItem`,
    shoulderBurn: `${SL}TravelDetailsItem`,
    relatedArticleId: 'String',
    spotRating: `${SL}TravelDetailsItem`,
    waterQuality: `${SL}TravelDetailsItem`,
    status: `${SL}TravelStatus`,
  },
  [`${SL}TravelDetailsBestSeason`]: {
    description: 'String',
    value: '[Seasons]',
  },
  [`${SL}TravelDetailsBestTide`]: {
    description: 'String',
    value: '[Tides]',
  },
  [`${SL}TravelDetailsBottom`]: {
    description: 'String',
    value: `[${SL}BottomTypes]`,
  },
  [`${SL}TravelDetailsBest`]: {
    season: `${SL}TravelDetailsBestSeason`,
    size: `${SL}TravelDetailsBestItem`,
    tide: `${SL}TravelDetailsBestTide`,
    swellDirection: `${SL}TravelDetailsBestItem`,
    windDirection: `${SL}TravelDetailsBestItem`,
  },
};

const SLEnums = [
  `${SL}HumanReportStatus`,
  `${SL}SpotType`,
  `${SL}AbilityLevel`,
  `${SL}BoardTypes`,
  `${SL}Status`,
  `${SL}TravelStatus`,
  'Tides',
  'Seasons',
  `${SL}BreakTypes`,
  `${SL}BottomTypes`,
];

export default (spotCommonFields) => `
  ${buildTypesAndInputs(SLSpotSubTypes, SLEnums)}

  input ${SL}SpotInput {
    ${spotCommonFields}
    ${SLSpotFields}
    subregion: String!
    geoname: Int!
    swellWindow: [${SL}RangeInput]
    best: ${SL}BestInput
    humanReport: ${SL}HumanReportInput
    travelDetails: ${SL}TravelDetailsInput
  }

  type ${SL}Spot implements SpotCommonFields {
    ${spotCommonFields}
    ${SLSpotFields}
    subregion: String
    geoname: Int
    swellWindow: [${SL}Range]
    best: ${SL}Best
    humanReport: ${SL}HumanReport
    travelDetails: ${SL}TravelDetails
    pointOfInterest: PointOfInterest
  }

  extend type ${SL}TravelDetails {
    boardTypes: [String]
  }

  input ${SL}TravelDetailsItemInput {
    description: String
    rating: Int
  },

  type ${SL}TravelDetailsItem {
    description: String
    rating: Int
    summary: String
  },

  input ${SL}TravelDetailsBestItemInput {
    description: String
  }

  type ${SL}TravelDetailsBestItem {
    description: String
    value: [String]
  }

  type ${SL}AbilityLevels {
    description: String
    levels: [${SL}AbilityLevel]
    summary: String
  }

  input ${SL}AbilityLevelsInput {
    description: String
  }

  enum ${SL}TravelStatus {
    PUBLISHED
    DRAFT
  }

  enum ${SL}Status {
    PUBLISHED
    DRAFT
    DELETED
  }

  enum ${SL}HumanReportStatus {
    OFF
    ON
    AVERAGE
    CLONE
  }

  enum ${SL}SpotType {
    SURFBREAK
    WAVEPOOL
    SKISLOPE
  }

  enum ${SL}AbilityLevel {
    BEGINNER
    INTERMEDIATE
    ADVANCED
    PRO
  }

  enum ${SL}BoardTypes {
    SHORTBOARD
    FISH
    FUNBOARD
    LONGBOARD
    GUN
    TOW
    SUP
    FOIL
    SKIMMING
    BODYBOARD
    BODYSURFING
    KITEBOARD
  }

  enum Tides {
    Low
    Medium_Low
    Medium
    Medium_High
    High
  }

  enum Seasons {
    Spring
    Summer
    Autumn
    Winter
  }

  enum ${SL}BreakTypes {
    Beach_Break
    Canyon
    Jetty
    Offshore
    Pier
    Point
    Reef
    Slab
    Lefts
    Rights
  }

  enum ${SL}BottomTypes {
    Coral
    Lava
    Rock
    Sand
  }
`;
