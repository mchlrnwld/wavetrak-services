import { gql } from 'apollo-server-express';
import { brands } from '../constants';
import { mswQueries, mswTypeDefs } from './mswSpotTypeDefs';
import slTypeDefs from './slSpotTypeDefs';

const brandEnum = brands.join('\n');

const brandUnion = brands.map((brand) => (`${brand}Spot`)).join(' | ');

const brandMutations = brands.map((brand) => (`upsert${brand}Spot(spot: ${brand}SpotInput!): SpotMutationResponse`)).join('\n');

const spotCommonFields = `
  _id: String
  brand: Brand
  pointOfInterestId: String!
  name: String!
  lat: Float!
  lon: Float!
  timezone: String!
`;

export default gql`
  ${mswTypeDefs(spotCommonFields)}
  ${slTypeDefs(spotCommonFields)}

  interface SpotCommonFields {
    ${spotCommonFields}
    pointOfInterest: PointOfInterest
  }

  union Spot = ${brandUnion}

  extend type PointOfInterest @key(fields: "pointOfInterestId") {
    pointOfInterestId: String! @external
    spots (brands: [Brand]): [Spot]
  }

  enum Brand {
    ${brandEnum}
  }

  type Query {
    spot (
      pointOfInterestId: String!
      brands: [Brand]
    ) : [Spot]
    ${mswQueries}
  }

  type SpotMutationResponse {
    success: Boolean!
    message: String!
    validationErrors: [validationError]
    spot: Spot
  }

  type validationError {
    key: String
    errors: [String]
  }

  type Mutation {
    ${brandMutations}
  }
`;
