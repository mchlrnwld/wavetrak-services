import { MSWRatingTypes, MSW } from '../constants';
import { buildTypesAndInputs } from '../utils';

const MSWRatingTypesEnum = Object.keys(MSWRatingTypes).join('\n');

const MSWSpotSubTypes = {
  [`${MSW}SpotType`]: {
    _id: 'Int',
    name: 'String',
  },
};

const MSWEnums = [
  `${MSW}RatingType`,
];

const MSWSpotFields = `
    surflineSpotId: String
    description: String
    hidden: Boolean
    ratingType: ${MSW}RatingType!
    topLevelNav: Boolean!
    lotusGrid: String
    tidalPort: String
`;

export const mswTypeDefs = (spotCommonFields) => (`
  type ${MSW}Spot implements SpotCommonFields {
    ${spotCommonFields}
    ${MSWSpotFields}
    added: Int
    edited: Int
    surfArea: ${MSW}SurfArea
    country: ${MSW}Country
    region: ${MSW}Region
    stormrider: ${MSW}Stormrider
    types: [${MSW}SpotType]
    pointOfInterest: PointOfInterest
  }

  input ${MSW}SpotInput {
    ${spotCommonFields}
    ${MSWSpotFields}
    surfAreaId: Int!
    countryId: Int!
    stormriderSpotId: Int
    types: [MSWSpotTypeInput]
  }

  type ${MSW}SurfArea {
    _id: Int
    name: String
    countryId: Int
    timezone: String
  }

  type ${MSW}Country {
    _id: Int
    iso: String
    name: String
  }

  type ${MSW}Region {
    _id: Int
    name: String
  }

  type ${MSW}Stormrider {
    id: Int
    name: String
    detail: String
    lat: Float
    lon: Float
  }

  ${buildTypesAndInputs(MSWSpotSubTypes, MSWEnums)}

  enum ${MSW}RatingType {
    ${MSWRatingTypesEnum}
  }
`);

export const mswQueries = `
  mswCountry (id: String): [${MSW}Country]
  mswSurfArea (id: String): [${MSW}SurfArea]
  mswRegion (id: String): [${MSW}Region]
  mswSpotType (id: String): [${MSW}SpotType]
`;
