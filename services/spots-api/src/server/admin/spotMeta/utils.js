/**
 * Builds the typedefs string for an object of types that have matching input types
 * @param {object} typeObj object containing all the types to be made into Types and Inputs
 * @param {[String]} enums array of enum names to ensure we don't append 'Input' to them
 * @returns String
 */
export const buildTypesAndInputs = (typeObj, enums) => {
  const scalarOrEnum = [
    ...enums,
    'String',
    'Float',
    'Int',
    'Boolean',
    'Id',
  ];

  return Object.keys(typeObj).map((typeName) => {
    let input = `input ${typeName}Input {\n`;
    let output = `type ${typeName} {\n`;
    const type = typeObj[typeName];

    Object.keys(type).forEach((field) => {
      const fieldType = type[field];
      const isArray = fieldType.includes(']');
      const isScalarOrEnum = scalarOrEnum.includes(fieldType.replace(/[[\]]/g, ''));
      let inputFieldType = fieldType;

      if (isArray && !isScalarOrEnum) {
        inputFieldType = fieldType.replace(']', 'Input]');
      } else if (!isScalarOrEnum) {
        inputFieldType = `${fieldType}Input`;
      }

      input += `${field}: ${inputFieldType}\n`;
      output += `${field}: ${fieldType}\n`;
    });

    return `${input}}\n${output}}\n`;
  }).join();
};

export const latLonToLocation = (spot) => ({
  ...spot,
  location: [
    spot.lon,
    spot.lat,
  ],
});

export const locationToLatLon = (spot) => ({
  ...spot,
  lat: spot.location && spot.location.coordinates[1],
  lon: spot.location && spot.location.coordinates[0],
});

/**
 * Transforms longitude to within the -180 -> 180 bounds that some applications use as a standard.
 * @param  {number} longitude
 * @returns number
 */
export const transformLon = (longitude) => {
  // eslint-disable-next-line no-param-reassign
  while (longitude <= -180) longitude += 360;
  // eslint-disable-next-line no-param-reassign
  while (longitude > 180) longitude -= 360;
  return Math.round(longitude * 100000) / 100000;
};
