import { MSW, MSWRatingTypes } from '../constants';

// eslint-disable-next-line import/prefer-default-export
export default {
  Query: {
    mswSurfArea: (parent, { id }, { dataSources }) => (
      dataSources[MSW].getSurfAreas(id)
    ),
    mswCountry: async (parent, { id }, { dataSources }) => (
      dataSources[MSW].getCountries(id)
    ),
    mswRegion: async (parent, { id }, { dataSources }) => (
      dataSources[MSW].getRegions(id)
    ),
    mswSpotType: (parent, { id }, { dataSources }) => (
      dataSources[MSW].getSpotTypes(id)
    ),
  },
  [`${MSW}Spot`]: {
    ratingType: ({ ratingType }) => (
      Object.keys(MSWRatingTypes).find((key) => MSWRatingTypes[key] === ratingType)
    ),
    surfArea: async ({ surfAreaId }, args, { dataSources }) => (
      (await dataSources[MSW].getSurfAreas(surfAreaId)).shift()
    ),
    country: async ({ country }, args, { dataSources }) => (
      (await dataSources[MSW].getCountries(country._id)).shift()
    ),
    region: async ({ region }, args, { dataSources }) => (
      (await dataSources[MSW].getRegions(region._id)).shift()
    ),
    pointOfInterest: ({ pointOfInterestId }) => ({
      pointOfInterestId,
      __typename: 'PointOfInterest',
    }),
  },
};
