import { SL } from '../constants';

export default {
  [`${SL}Spot`]: {
    pointOfInterest: ({ pointOfInterestId }) => ({
      pointOfInterestId,
      __typename: 'PointOfInterest',
    }),
  },
};
