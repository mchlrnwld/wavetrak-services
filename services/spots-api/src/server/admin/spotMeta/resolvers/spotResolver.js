import { UserInputError } from 'apollo-server-express';
import { brands as brandConstants } from '../constants';
import { transformLon } from '../utils';
import mswResolver from './mswSpotResolver';
import slSpotResolver from './slSpotResolver';

/**
 * Queries the spot APIs for all brands unless a subset of brands are specified in the args
 * in which case it just queries those brand's APIs
 *
 * @param  {null} p not used, just padding out function params so that it can be used in resolver
 * @param  {string} {pointOfInterestId the POI for which we're searching by
 * @param  {[string]} brands=null} the brands we're searching on
 * @param  {object} {dataSources} get the datasources obj from context
 */
const getSpotsForPoi = async (p,
  { pointOfInterestId, brands },
  { dataSources }) => {
  const brandSpots = await Promise.all(
    (brands || brandConstants).map(
      (brand) => dataSources[brand].getSpots({ pointOfInterestId }),
    ),
  );

  return brandSpots.flat();
};

/**
 * returns a function for a given brand that Updates or Creates a spot
 * @param  {string} brand the brand who's datasource will be used to perform the action
 * @param  {null} parent unused parent param
 * @param  {object} {spot} the spot object that will be passed to the brand's API
 * @param  {object} {dataSources} get the datasources obj from context
 */
const upsertSpot = (brand) => async (parent,
  { spot },
  { dataSources }) => {
  const method = spot._id ? 'update' : 'create';
  // eslint-disable-next-line no-param-reassign
  spot.lon = transformLon(spot.lon);

  const result = {
    success: true,
    message: `Spot successfully ${method}d`,
    validationErrors: null,
    spot: null,
  };

  try {
    result.spot = await dataSources[brand][`${method}Spot`](spot);
  } catch (error) {
    if (error instanceof UserInputError) {
      result.success = false;
      result.message = error.message;
      result.validationErrors = error.validationErrors;
    } else {
      throw error;
    }
  }

  return result;
};

// create mutation functions for each brand (as we cant use union for input type)
const brandMutations = brandConstants.reduce((mutations, brand) => (
  {
    ...mutations,
    [`upsert${brand}Spot`]: upsertSpot(brand),
  }
), {});

export default {
  ...mswResolver,
  ...slSpotResolver,
  Query: {
    ...mswResolver.Query,
    spot: getSpotsForPoi,
  },
  Mutation: brandMutations,
  Spot: {
    __resolveType: ({ brand }) => (
      `${brand}Spot`
    ),
  },
  PointOfInterest: {
    spots: (parent, args, context) => getSpotsForPoi(
      null,
      {
        ...parent,
        ...args,
      },
      context,
    ),
  },
};
