import fetch from 'node-fetch';
import customHierarchy from '../server/taxonomy/customHierarchy';

const base = 'http://api.geonames.org';
const userName = process.env.GEONAME_USERNAME;

const insertIsland = async (hierarchy, islandId) => {
  const doFetch = await fetch(
    `${base}/hierarchyJSON?&username=${userName}&geonameId=${islandId}`,
  );
  const { geonames } = await doFetch.json();
  const islandDetails = geonames[geonames.length - 1];
  const liesIn = geonames[geonames.length - 2].geonameId;

  const insertAfter = hierarchy.map((geoname) => geoname.geonameId).indexOf(liesIn) + 1;

  /*
  If the geoname and corresponding island have hierarchies that diverge,
  island is appended to the end of the hierarchy.
  */
  return insertAfter === 0
    ? [...hierarchy.slice(0), islandDetails]
    : [
      ...hierarchy.slice(0, insertAfter),
      islandDetails,
      ...hierarchy.slice(insertAfter),
    ];
};

const getHierarchy = async (geonameId) => {
  if (!geonameId) return [];
  const doFetch = await fetch(
    `${base}/hierarchyJSON?&username=${userName}&geonameId=${geonameId}`,
  );
  const { geonames } = await doFetch.json();

  return Object.keys(customHierarchy).includes(geonameId.toString())
    ? insertIsland(geonames, customHierarchy[geonameId])
    : geonames;
};

export { getHierarchy }; // eslint-disable-line import/prefer-default-export
