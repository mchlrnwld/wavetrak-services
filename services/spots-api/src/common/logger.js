import bunyan from 'bunyan';
import Logsene from '@surfline/bunyan-logsene';
import PrettyStream from 'bunyan-prettystream';

const logseneStream = new Logsene({
  token: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63', // default to sandbox
});

const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);

const logger = (name) => {
  const log = bunyan.createLogger({
    name: name || 'default-js-logger',
    serializers: bunyan.stdSerializers,
    streams: [
      {
        level: process.env.CONSOLE_LOG_LEVEL || 'warn',
        type: 'raw',
        stream: prettyStdOut,
      },
      {
        level: process.env.LOGSENE_LEVEL || 'warn',
        stream: logseneStream,
        type: 'raw',
      },
    ],
  });
  log.trace(`${name} logging started.`);
  return log;
};

export default logger;
