/* eslint-disable import/prefer-default-export */
import _ from 'lodash';
import linksFor from '../server/taxonomy/urls';
import { fetchTaxonomy } from '../model/taxonomy';

export const buildGeoNode = (node) => ({
  ...node,
  __v: undefined,
  associated: {
    links: linksFor(node),
  },
});

export const fetchTaxonomyJSON = async (type, id, maxDepth = 10) => {
  const results = await fetchTaxonomy(type, id, maxDepth);

  if (results.length > 0) {
    const taxonomy = results[0];
    let root = _.remove(taxonomy.in, (place) => place.depth === 0);
    if (root) {
      [root] = root; // move the root of the tree out of contains
      root.depth = undefined;
      const [start, up, down] = await Promise.all([
        buildGeoNode(root),
        taxonomy.in
          .map((place) => buildGeoNode(place))
          .sort((a, b) => b.depth - a.depth),
        taxonomy.contains
          .map((place) => buildGeoNode(place))
          .sort((a, b) => a.depth - b.depth),
      ]);

      return {
        ...start,
        in: up,
        contains: down,
      };
    }
  }

  return false;
};
