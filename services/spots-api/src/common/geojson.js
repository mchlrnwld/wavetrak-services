/* eslint-disable import/prefer-default-export */
const createGeoJsonPoint = (coordinates) => {
  if (!coordinates || !Array.isArray(coordinates)) return coordinates;
  return {
    type: 'Point',
    coordinates,
  };
};

export { createGeoJsonPoint };
