import fetch from 'node-fetch';
import config from '../config';

/* eslint-disable import/prefer-default-export */
export const getCamera = async (id) => {
  const url = `${config.CAMERAS_API}/cameras/${id}`;
  const response = await fetch(url);
  const body = await response.json();

  const cameraNotFoundMessage = 'camera not found';
  if (response.status === 200) return body;
  if (response.status === 404
    || (body.message && body.message.toLowerCase() === cameraNotFoundMessage)) {
    // Note: Cameras-service returns a 400 for deleted cams with message 'Camera not found'
    const err = {
      status: 404,
      message: cameraNotFoundMessage,
    };
    throw err;
  }
  throw body;
};
