import mongoose from 'mongoose';

const regionSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Name is required'],
    },
  },
  {
    collection: 'Regions',
    timestamps: true,
  },
);

regionSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.imported; // eslint-disable-line no-param-reassign
    return ret;
  },
});

regionSchema.post('save', (error, doc, next) => {
  if (error.name === 'MongoError' && error.code === 11000) {
    const err = new Error('There was a duplicate key error.');
    err.name = 'DuplicateKeyError';
    err.errors = {
      name: {
        message: 'Region name must be unique.',
      },
    };
    next(err);
  } else {
    next(error);
  }
});

regionSchema.index({ name: 1 }, { unique: true });

const RegionModel = mongoose.model('Region', regionSchema);

export const createRegion = async (model) => {
  const region = new RegionModel(model);
  await region.save();
  return region;
};

export default RegionModel;
