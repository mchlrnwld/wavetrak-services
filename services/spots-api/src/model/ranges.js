export const validateRanges = {
  validator: (sortedRanges) => sortedRanges.reduce((prev, cur) => ({
    min: cur.min,
    max: cur.max,
    valid: ((prev.valid) && (cur.min < cur.max) && (cur.min > prev.max)),
  }), { max: -1, valid: true }).valid,
  message: 'This field is not valid',
};

export const rangeSchema = (min, max) => ({
  _id: false,
  min: {
    type: Number,
    min: [min, `This field cannot contain values less than ${min}`],
    max: [max, `This field cannot contain values greater than ${max}`],
    default: null,
  },
  max: {
    type: Number,
    min: [min, `This field cannot contain values less than ${min}`],
    max: [max, `This field cannot contain values greater than ${max}`],
    default: null,
  },
});

export const sortRangesByMin = (ranges) => (
  ranges ? ranges.slice().sort((a, b) => a.min - b.min) : []
);
