import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { sortRangesByMin } from './ranges';

describe('ranges', () => {
  describe('sortRangesByMin', () => {
    it('returns empty array if ranges is not given', () => {
      expect(sortRangesByMin()).to.deep.equal([]);
    });

    it('sorts ranges by min value without mutating original ranges parameter', () => {
      const ranges = deepFreeze([
        { min: 3, max: 5 },
        { min: 1, max: 4 },
        { min: 2, max: 3 },
      ]);
      expect(sortRangesByMin(ranges)).to.deep.equal([ranges[1], ranges[2], ranges[0]]);
    });
  });
});
