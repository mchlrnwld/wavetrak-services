import mongoose from 'mongoose';
import 'mongoose-geojson-schema';
import {
  rangeSchema,
  sortRangesByMin,
  validateRanges,
} from './ranges';
import { validateUniqueList, stripFalsey } from './listHelpers';
import TravelDetailsModel from './TravelDetailsModel';

// Thumbnail virtual helper functions
const thumbnailPath = (spotId, type) => `${process.env.SPOT_THUMBNAIL_CDN}/spots/${spotId}/${spotId}_${type}.jpg`;

const thumbnail = (spotId) => ({
  original: thumbnailPath(spotId, 'original'),
  3000: thumbnailPath(spotId, 3000),
  1500: thumbnailPath(spotId, 1500),
  640: thumbnailPath(spotId, 640),
  300: thumbnailPath(spotId, 300),
});

const spotSchema = new mongoose.Schema(
  {
    name: { type: String, required: [true, 'Name is required'] },
    legacyId: { type: Number },
    legacyRegionId: { type: Number },
    geoname: { type: Number, required: [true, 'Geoname is required'] },
    politicalLocation: {
      city: {
        geonameId: { type: Number, default: null },
        name: {
          type: String,
          default: null,
          // required: [true, 'City must be set'],
        },
      },
      hierarchy: [
        {
          adminCode1: { type: String, default: null },
          lng: { type: String, default: null },
          geonameId: { type: Number, default: null },
          toponymName: { type: String, default: null },
          countryId: { type: String, default: null },
          fcl: { type: String, default: null },
          population: { type: Number, default: null },
          countryCode: { type: String, default: null },
          name: { type: String, default: null },
          fclName: { type: String, default: null },
          countryName: { type: String, default: null },
          fcodeName: { type: String, default: null },
          adminName1: { type: String, default: null },
          lat: { type: String, default: null },
          fcode: { type: String, default: null },
        },
      ],
    },
    location: {
      type: mongoose.Schema.Types.Point,
      required: [true, 'Location is required'],
    },
    forecastLocation: {
      type: mongoose.Schema.Types.Point,
    },
    pointOfInterestId: { type: String, required: [true, 'Point of Interest ID is required'] },
    subregion: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Subregion',
      required: [true, 'Subregion is required'],
    },
    tideStation: { type: String, default: null },
    cams: [
      {
        type: mongoose.Schema.Types.ObjectId,
      },
    ],
    weatherStations: [
      {
        type: mongoose.Schema.Types.ObjectId,
      },
    ],
    timezone: { type: String, required: [true, 'Time zone is required'] },
    hasThumbnail: { type: Boolean, default: false, select: true },
    popular: { type: Boolean, default: false },
    status: {
      type: String,
      enum: ['PUBLISHED', 'DRAFT', 'DELETED'],
    },
    forecastStatus: {
      status: {
        type: String,
        enum: ['active', 'inactive', 'off'],
        default: 'active',
      },
      inactiveMessage: {
        type: String,
        maxlength: [
          150,
          'Status message must not have more than 150 characters.',
        ],
        default: 'Inactive status message',
      },
    },
    offshoreDirection: {
      type: Number,
      min: [0, 'Offshore Direction cannot be less than 0'],
      max: [360, 'Offshore Direction cannot be greater than 360'],
    },
    lolaSurfBiasCoefficients: {
      linear: { type: Number, default: 1 },
      constant: { type: Number, default: 0 },
    },
    noraBiasCorrection: { type: Number, default: 0 },
    swellWindow: {
      type: [rangeSchema(0, 360)],
      default: [],
    },
    best: {
      windDirection: {
        type: [rangeSchema(0, 360)],
        required: [true, 'Best wind direction is required'],
      },
      swellPeriod: {
        type: [rangeSchema(0, 33)],
        validate: validateRanges,
        required: [true, 'Best swell period is required'],
      },
      swellWindow: {
        type: [rangeSchema(0, 360)],
        required: [true, 'Best swell window is required'],
      },
      sizeRange: {
        type: [rangeSchema(0, 150)],
        validate: validateRanges,
        required: [true, 'Best size range is required'],
      },
    },
    humanReport: {
      status: {
        type: String,
        enum: ['OFF', 'ON', 'AVERAGE', 'CLONE'],
      },
      clone: {
        spotId: {
          type: mongoose.Schema.Types.ObjectId,
        },
      },
      average: {
        spotOneId: {
          type: mongoose.Schema.Types.ObjectId,
        },
        spotTwoId: {
          type: mongoose.Schema.Types.ObjectId,
        },
      },
    },
    spotType: {
      type: String,
      enum: ['SURFBREAK', 'WAVEPOOL', 'SKISLOPE'],
      default: 'SURFBREAK',
      required: [true, 'Spot type is required'],
    },
    abilityLevels: [
      {
        type: String,
        enum: ['BEGINNER', 'INTERMEDIATE', 'ADVANCED', 'PRO'],
      },
    ],
    boardTypes: [
      {
        type: String,
        enum: [
          'SHORTBOARD',
          'FISH',
          'FUNBOARD',
          'LONGBOARD',
          'GUN',
          'TOW',
          'SUP',
          'FOIL',
          'SKIMMING',
          'BODYBOARD',
          'BODYSURFING',
          'KITEBOARD',
        ],
      },
    ],
    relivableRating: {
      type: Number,
      min: 0,
      max: 5,
      default: 0,
    },
    titleTag: { type: String },
    metaDescription: { type: String },
    travelDetails: TravelDetailsModel,
  },
  {
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
    collection: 'Spots',
    timestamps: true,
  },
);

// Add virtual "calculated" response based on hasThumbnail property
spotSchema.virtual('thumbnail').get(function setThumbnail() {
  return this.hasThumbnail ? thumbnail(this._id) : thumbnail('default');
});

spotSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.imported; // eslint-disable-line no-param-reassign
    delete ret.importedTravel; // eslint-disable-line no-param-reassign
    return ret;
  },
});

spotSchema.pre('validate', function handleDefaults(next) {
  if (!this.status) {
    this.status = 'DRAFT';
  }

  if (!this.forecastLocation) {
    this.forecastLocation = this.location;
  }

  if (!this.humanReport) {
    this.humanReport = {};
  }

  if (!this.humanReport.status) {
    this.humanReport.status = 'OFF';
  }

  if (!this.spotType) {
    this.spotType = 'SURFBREAK';
  }

  if (!this.abilityLevels) {
    this.abilityLevels = [];
  }

  if (!this.boardTypes) {
    this.boardTypes = [];
  }

  // remove empty elements from cams array
  this.cams = stripFalsey(this.cams);

  // remove empty elements from weatherStations array
  this.weatherStations = stripFalsey(this.weatherStations);

  // sort ranges properties by min
  this.swellWindow = sortRangesByMin(this.swellWindow);
  if (this.best) {
    this.best.windDirection = sortRangesByMin(this.best.windDirection);
    this.best.swellPeriod = sortRangesByMin(this.best.swellPeriod);
    this.best.swellWindow = sortRangesByMin(this.best.swellWindow);
    this.best.sizeRange = sortRangesByMin(this.best.sizeRange);
  }

  return next();
});

spotSchema.index({ legacyId: 1 });
spotSchema.index({ name: 'text' });
spotSchema.index({ location: '2dsphere' });
spotSchema.index({ forecastLocation: '2dsphere' });
spotSchema.index({ subregion: 1 });
spotSchema.index({ status: 1 });
spotSchema.index({ popular: 1 });

const SpotModel = mongoose.model('Spot', spotSchema);

SpotModel.schema.path('humanReport.clone.spotId').validate(
  function validate(value) {
    if (this.humanReport.status === 'CLONE') {
      if (!value) {
        return false;
      }
    }
    return true;
  },
  'Clone spot cannot be `{VALUE}`',
  'Invalid clone spot ID',
);

SpotModel.schema.path('humanReport.average.spotTwoId').validate(
  function validate(value) {
    if (this.humanReport.status === 'AVERAGE') {
      if (!value) {
        return false;
      }
    }
    return true;
  },
  'Average spot two cannot be `{VALUE}`',
  'Invalid average spot two ID',
);

SpotModel.schema.path('humanReport.average.spotOneId').validate(
  function validate(value) {
    if (this.humanReport.status === 'AVERAGE') {
      if (!value) {
        return false;
      }
    }
    return true;
  },
  'Average spot one cannot be `{VALUE}`',
  'Invalid average spot one ID',
);

SpotModel.schema.path('cams').validate(
  validateUniqueList,
  '`{VALUE}` contains duplicates.',
);

SpotModel.schema.path('weatherStations').validate(
  validateUniqueList,
  '`{VALUE}` contains duplicates.',
);

export const createSpot = async (model) => {
  const spot = new SpotModel(model);
  await spot.save();
  return spot;
};

export default SpotModel;
