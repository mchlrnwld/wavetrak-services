import { expect } from 'chai';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import Subregion from './SubregionModel';

describe('Subregion Model', () => {
  let mongoServer;

  before(async () => {
    mongoServer = await MongoMemoryServer.create();

    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(
      mongoUri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err) => err,
    );
  });

  after(async () => {
    if (mongoServer) {
      await mongoServer.stop();
    }
    if (mongoose.connection) {
      mongoose.connection.close();
    }
  });

  afterEach(async () => {
    await Subregion.deleteMany();
  });

  it('should require name field', (done) => {
    const subregion = new Subregion({
      spots: [{ _id: new mongoose.Types.ObjectId() }],
      regionId: new mongoose.Types.ObjectId(),
    });
    subregion.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.name).to.be.ok();
      expect(err.errors.name.message).to.equal('Name is required');
      return done();
    });
  });

  it('should require region field', (done) => {
    const subregion = new Subregion({
      name: 'Test Subregion',
      spots: [{ _id: new mongoose.Types.ObjectId() }],
    });
    subregion.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.region).to.be.ok();
      expect(err.errors.region.message).to.equal('Region is required');
      return done();
    });
  });

  it('should set default forecastStatus fields', async () => {
    const region = new mongoose.Types.ObjectId();
    const subregion = new Subregion({
      name: 'Test Subregion',
      spots: [{ _id: new mongoose.Types.ObjectId() }],
      region,
      geoname: 0,
    });

    await subregion.save();

    const result = mongoose.connection.collections.Subregions.find({
      name: 'Test Subregion',
    });

    const subregionFromDB = await result.next();

    expect(subregionFromDB.forecastStatus).to.be.ok();
    expect(subregionFromDB.forecastStatus.status).to.equal('active');
    expect(subregionFromDB.forecastStatus.inactiveMessage).to.equal('Inactive status message');
  });

  it('should validate length of inactiveMessage and return proper error message', (done) => {
    const subregion = new Subregion({
      name: 'Test Subregion',
      spots: [{ _id: new mongoose.Types.ObjectId() }],
      regionId: new mongoose.Types.ObjectId(),
      forecastStatus: {
        status: 'inactive',
        inactiveMessage:
          'Invalid inactive message that contains more than 150 characters. Invalid inactive message that contains more than 150 characters. Invalid inactive message that contains more than 150 characters.',
      },
    });
    subregion.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors).to.be.ok();
      expect(err.errors['forecastStatus.inactiveMessage'].message).to.equal(
        'Status message must not have more than 150 characters.',
      );
      return done();
    });
  });

  it('should set forecastStatus.status to valid enum value', async () => {
    const region = new mongoose.Types.ObjectId();
    const subregion = new Subregion({
      name: 'Test Subregion',
      spots: [{ _id: new mongoose.Types.ObjectId() }],
      region,
      geoname: 0,
      forecastStatus: {
        status: 'inactive',
      },
    });

    await subregion.save();

    const result = mongoose.connection.collections.Subregions.find({ name: 'Test Subregion' });
    const subregionFromDB = await result.next();

    expect(subregionFromDB.forecastStatus).to.be.ok();
    expect(subregionFromDB.forecastStatus.status).to.equal('inactive');
    expect(subregionFromDB.forecastStatus.inactiveMessage).to.equal('Inactive status message');
  });
});
