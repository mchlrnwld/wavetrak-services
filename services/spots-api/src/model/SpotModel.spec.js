import { expect } from 'chai';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import Spot from './SpotModel';

describe('Spot Model', () => {
  let mongoServer;

  before(async () => {
    mongoServer = await MongoMemoryServer.create();

    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(
      mongoUri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err) => err,
    );
  });

  after(async () => {
    if (mongoServer) {
      await mongoServer.stop();
    }
    if (mongoose.connection) {
      mongoose.connection.close();
    }
  });

  afterEach(async () => {
    await Spot.deleteMany();
  });

  it('should require name field', (done) => {
    const spot = new Spot({});
    spot.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.name).to.be.ok();
      expect(err.errors.name.message).to.equal('Name is required');
      return done();
    });
  });

  it('should require location field', (done) => {
    const spot = new Spot({});
    spot.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.location).to.be.ok();
      expect(err.errors.location.message).to.equal('Location is required');
      return done();
    });
  });

  it('should require time zone field', (done) => {
    const spot = new Spot({});
    spot.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.timezone).to.be.ok();
      expect(err.errors.timezone.message).to.equal('Time zone is required');
      return done();
    });
  });

  it('should default forecast location before validate', () => {
    const spotWithoutForecastLocation = new Spot({
      location: {
        type: 'Point',
        coordinates: [0, 1],
      },
    });
    const spotWithForecastLocation = new Spot({
      location: {
        type: 'Point',
        coordinates: [0, 1],
      },
      forecastLocation: {
        type: 'Point',
        coordinates: [1, 2],
      },
    });

    spotWithoutForecastLocation.validate();
    expect(spotWithoutForecastLocation.location).to.deep.equal({
      type: 'Point',
      coordinates: [0, 1],
    });
    expect(spotWithoutForecastLocation.forecastLocation).to.deep.equal({
      type: 'Point',
      coordinates: [0, 1],
    });

    spotWithForecastLocation.validate();
    expect(spotWithForecastLocation.location).to.deep.equal({
      type: 'Point',
      coordinates: [0, 1],
    });
    expect(spotWithForecastLocation.forecastLocation).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });
  });

  it('should default status to DRAFT before validate', () => {
    const spotWithoutStatus = new Spot();
    const spotWithStatus = new Spot({ status: 'PUBLISHED' });

    spotWithoutStatus.validate();
    expect(spotWithoutStatus.status).to.equal('DRAFT');

    spotWithStatus.validate();
    expect(spotWithStatus.status).to.equal('PUBLISHED');
  });

  it('should default human report status to OFF before validate', () => {
    const spotWithoutHumanReportStatus = new Spot();
    spotWithoutHumanReportStatus.validate();
    expect(spotWithoutHumanReportStatus.humanReport.status).to.equal('OFF');
  });

  it('should default spot type to SURFBREAK before validate', () => {
    const spotWithoutSpotTypeSet = new Spot();
    spotWithoutSpotTypeSet.validate();
    expect(spotWithoutSpotTypeSet.spotType).to.equal('SURFBREAK');
  });

  it('should set default forecastStatus fields', async () => {
    const subregion = new mongoose.Types.ObjectId();
    const location = {
      type: 'Point',
      coordinates: [0, 1],
    };

    const spotDetails = {
      name: 'Test Spot 1',
      geoname: 0,
      location,
      pointOfInterestId: 'id',
      subregion,
      timezone: 'zone',
      spotType: 'SURFBREAK',
    };

    const spot = new Spot(spotDetails);

    await spot.save();

    const result = mongoose.connection.collections.Spots.find({ name: 'Test Spot 1' });

    const spotFromDB = await result.next();

    expect(spotFromDB.forecastStatus).to.be.ok();
    expect(spotFromDB.forecastStatus.status).to.equal('active');
    expect(spotFromDB.forecastStatus.inactiveMessage).to.equal('Inactive status message');
  });

  it('should validate length of inactiveMessage and return proper error message', (done) => {
    const spot = new Spot({
      forecastStatus: {
        status: 'inactive',
        inactiveMessage:
          'Invalid inactive message that contains more than 150 characters. Invalid inactive message that contains more than 150 characters. Invalid inactive message that contains more than 150 characters.',
      },
    });
    spot.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors).to.be.ok();
      expect(err.errors['forecastStatus.inactiveMessage'].message).to.equal(
        'Status message must not have more than 150 characters.',
      );
      return done();
    });
  });

  it('should set forecastStatus.status to valid enum value', async () => {
    const subregion = new mongoose.Types.ObjectId();
    const location = {
      type: 'Point',
      coordinates: [0, 1],
    };

    const spotDetails = {
      name: 'Test Spot 1',
      geoname: 0,
      location,
      pointOfInterestId: 'id',
      subregion,
      timezone: 'zone',
      spotType: 'SURFBREAK',
      forecastStatus: { status: 'inactive' },
    };

    const spot = new Spot(spotDetails);

    await spot.save();

    const result = mongoose.connection.collections.Spots.find({ name: 'Test Spot 1' });

    const spotFromDB = await result.next();

    expect(spotFromDB.forecastStatus).to.be.ok();
    expect(spotFromDB.forecastStatus.status).to.equal('inactive');
    expect(spotFromDB.forecastStatus.inactiveMessage).to.equal('Inactive status message');
  });
});
