import mongoose from 'mongoose';
import 'mongoose-geojson-schema';

const subregionSchema = new mongoose.Schema(
  {
    name: { type: String, required: [true, 'Name is required'] },
    geoname: { type: Number, required: [true, 'Geoname is required'] },
    region: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Region',
      required: [true, 'Region is required'],
    },
    primarySpot: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Spot',
    },
    imported: {
      subregionId: { type: Number, select: true },
      basins: [{ type: String }],
    },
    offshoreSwellLocation: {
      type: { type: String },
      coordinates: [Number],
    },
    status: {
      type: String,
      enum: ['PUBLISHED', 'DRAFT'],
      default: 'DRAFT',
    },
    order: {
      type: String,
      default: '1',
    },
    forecastStatus: {
      status: {
        type: String,
        enum: ['active', 'inactive', 'off'],
        default: 'active',
      },
      inactiveMessage: {
        type: String,
        maxlength: [
          150,
          'Status message must not have more than 150 characters.',
        ],
        default: 'Inactive status message',
      },
    },
  },
  {
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
    collection: 'Subregions',
    timestamps: true,
  },
);

subregionSchema.index({ offshoreSwellLocation: '2dsphere' });

// Add virtual "calculated" response based on hasThumbnail property
subregionSchema.virtual('legacyId').get(function setLegacyId() {
  return this.imported.subregionId ? this.imported.subregionId : null;
});

subregionSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.imported; // eslint-disable-line no-param-reassign
    return ret;
  },
});

const SubregionModel = mongoose.model('Subregion', subregionSchema);

export const createSubregion = async (model) => {
  const subregion = new SubregionModel(model);
  await subregion.save();
  return subregion;
};

export const getLegacyRegionId = async (subregionId) => {
  if (!subregionId) return null;

  const {
    imported: { subregionId: legacyRegionId },
  } = (await SubregionModel.findOne({ _id: mongoose.Types.ObjectId(subregionId) })) || {
    imported: { subregionId: null },
  };

  return legacyRegionId;
};

export const getBatchSubregions = (subregionIds) => {
  const subregions = SubregionModel.find({
    _id: {
      $in: subregionIds,
    },
  });
  return subregions;
};

export default SubregionModel;
