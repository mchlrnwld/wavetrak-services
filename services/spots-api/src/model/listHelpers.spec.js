import { expect } from 'chai';
import { stripFalsey, validateUniqueList } from './listHelpers';

describe('listHelpers', () => {
  describe('stripFalsey', () => {
    it('returns empty array if stations/cams are not provided', () => {
      expect(stripFalsey()).to.deep.equal([]);
    });
    it('just returns truthy values from array', () => {
      expect(stripFalsey(['', undefined, null, 'a'])).to.deep.equal(['a']);
    });
  });
  describe('validateUniqueList', () => {
    it('returns invalid when array contains duplicates', () => {
      const isUnique = validateUniqueList(['5842041f4e65fad6a7708ca7', '5842041f4e65fad6a7708ca7']);
      expect(isUnique).to.be.false();
    });
    it('returns valid when array does not contain duplicates', () => {
      const isUnique = validateUniqueList(['5842041f4e65fad6a7708ca8', '5842041f4e65fad6a7708ca7']);
      expect(isUnique).to.be.true();
    });
  });
});
