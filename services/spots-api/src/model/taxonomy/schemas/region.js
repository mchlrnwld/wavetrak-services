import mongoose from 'mongoose';
import Taxonomy, { options } from './taxonomy';

const regionTaxonomySchema = new mongoose.Schema(
  {
    region: { type: mongoose.Schema.Types.ObjectId },
  },
  options,
);

regionTaxonomySchema.index({ region: 1 });

const RegionTaxonomy = Taxonomy.discriminator('region', regionTaxonomySchema);

export default RegionTaxonomy;
