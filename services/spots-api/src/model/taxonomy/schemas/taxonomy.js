import mongoose from 'mongoose';

/** @type {mongoose.SchemaOptions} */
const options = { discriminatorKey: 'type', timestamps: true };

const types = {
  GEONAME: 'geoname',
  SPOT: 'spot',
  SUBREGION: 'subregion',
  REGION: 'region',
  TAXONOMY: 'taxonomy',
};

const taxonomySchema = new mongoose.Schema(
  {
    name: { type: String, required: [true, 'Name is required'] },
    liesIn: [{ type: mongoose.Schema.Types.ObjectId }],
    hasSpots: {
      type: Boolean,
      default: true,
      required: true,
    },
    type: {
      type: String,
      enum: Object.values(types),
      required: true,
    },
    category: {
      type: String,
      enum: ['surfline', 'geonames'],
      required: true,
    },
  },
  {
    ...options,
    collection: 'Taxonomy',
  },
);

taxonomySchema.index({ type: 1 });
taxonomySchema.index({ hasSpots: 1 });
taxonomySchema.index({ liesIn: 1 });

const Taxonomy = mongoose.model('Taxonomy', taxonomySchema);

export default Taxonomy;
export { options, types };
