import mongoose from 'mongoose';
import {} from 'mongoose-geojson-schema';
import Taxonomy, { options } from './taxonomy';

const geonameTaxonomySchema = new mongoose.Schema(
  {
    location: {
      index: '2dsphere',
      type: mongoose.Schema.Types.Point,
    },
    enumeratedPath: {
      type: String,
      required: true,
    },
    geonameId: {
      type: Number,
      required: true,
    },
    geonames: {
      type: {
        name: {
          type: String,
          required: true,
        },
        toponymName: {
          type: String,
          required: true,
        },
        population: {
          type: Number,
          required: true,
        },
        countryId: {
          type: String,
          required: true,
        },
        adminCode1: {
          type: String,
          required: true,
        },
        adminName1: {
          type: String,
          required: true,
        },
        fcode: {
          type: String,
          required: true,
        },
        fcodeName: {
          type: String,
          required: true,
        },
        countryCode: {
          type: String,
          required: true,
        },
        countryName: {
          type: String,
          required: true,
        },
        fcl: {
          type: String,
          required: true,
        },
        fclName: {
          type: String,
          required: true,
        },
      },
    },
  },
  options,
);

geonameTaxonomySchema.index({ geonameId: 1 });

const GeonameTaxonomy = Taxonomy.discriminator('geoname', geonameTaxonomySchema);

export default GeonameTaxonomy;
