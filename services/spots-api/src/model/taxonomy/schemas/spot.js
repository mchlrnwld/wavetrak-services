import mongoose from 'mongoose';
import {} from 'mongoose-geojson-schema';
import Taxonomy, { options } from './taxonomy';

const spotTaxonomySchema = new mongoose.Schema(
  {
    location: {
      index: '2dsphere',
      type: mongoose.Schema.Types.Point,
    },
    spot: { type: mongoose.Schema.Types.ObjectId },
  },
  options,
);

spotTaxonomySchema.index({ spot: 1 });

const SpotTaxonomy = Taxonomy.discriminator('spot', spotTaxonomySchema);

export default SpotTaxonomy;
