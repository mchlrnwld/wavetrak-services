import mongoose from 'mongoose';
import {} from 'mongoose-geojson-schema';
import Taxonomy, { options } from './taxonomy';

const subregionTaxonomySchema = new mongoose.Schema(
  {
    subregion: { type: mongoose.Schema.Types.ObjectId },
  },
  options,
);

subregionTaxonomySchema.index({ subregion: 1 });

const SubregionTaxonomy = Taxonomy.discriminator('subregion', subregionTaxonomySchema);

export default SubregionTaxonomy;
