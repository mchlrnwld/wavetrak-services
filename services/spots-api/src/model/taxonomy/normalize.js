import { markTaxonomyHasSpots } from './treeUtils';
import SubregionModel from '../SubregionModel';
import RegionModel from '../RegionModel';
import InvalidGeonameError from '../../errors/InvalidGeonameError';
import { types } from './schemas/taxonomy';
import { updateSpot } from './spot';
import { updateSubregion } from './subregion';
import { updateRegion } from './region';
import { findTaxonomyByType } from './taxonomy';

/**
 * Given a `regionId` normalizes region into the taxonomy collection. `type: region`
 * don't get normalized with any parent references.
 *
 * @param {ObjectId} regionId the `regionId` to normalize
 */
const normalizeRegion = async (regionId) => {
  const region = await RegionModel.findOne({ _id: regionId });
  const normalizedRegion = await updateRegion(region);
  return normalizedRegion._id;
};

/**
 * Normalizes a subregion. The parent reference `liesIn` will point to a Region.
 *
 * @param {ObjectId} subregionId
 */
const normalizeSubregion = async (subregion) => {
  if (subregion.geoname) {
    const taxonomy = await findTaxonomyByType(types.GEONAME, subregion.geoname);
    if (!taxonomy) throw new InvalidGeonameError();
    await updateSubregion(subregion, taxonomy._id);
  }

  if (subregion) {
    const normalizedRegion = subregion.region
      ? await normalizeRegion(subregion.region) : null;
    const normalizedSubregion = await updateSubregion(subregion, normalizedRegion);
    if (normalizedSubregion) return normalizedSubregion._id;
  }

  throw new Error('A subregion could not be found');
};

/**
 * Normalizes a geoname's hierarchy into our database. Part of the normalization
 * process is adding a `liesIn` property to the `Place` model. This enables
 * us to use the `$graphLookup` function to build up a list of related `Places`.
 *
 * @param {Number} geonameId the geonameId to get the hierarchy for
 *
 * @returns Array an array of `Places`.
 */
const normalizeSpot = async (spot) => {
  if (spot.geoname) {
    const taxonomy = await findTaxonomyByType(types.GEONAME, spot.geoname);
    if (!taxonomy) throw new InvalidGeonameError();
    await updateSpot(spot, taxonomy._id);
    await markTaxonomyHasSpots(taxonomy._id);
  }

  const subregion = await SubregionModel.findOne({ _id: spot.subregion });
  const normalizedSubregionId = await exports.normalizeSubregion(subregion);
  await updateSpot(spot, normalizedSubregionId);
  await markTaxonomyHasSpots(normalizedSubregionId);

  return normalizedSubregionId;
};

export {
  normalizeSpot,
  normalizeSubregion,
};
