import mongoose from 'mongoose';
import _ from 'lodash';
import Taxonomy, { types } from './schemas/taxonomy';

const indexedTypeFields = {
  [types.GEONAME]: 'geonameId',
  [types.SPOT]: 'spot',
  [types.SUBREGION]: 'subregion',
  [types.REGION]: 'region',
  [types.TAXONOMY]: '_id',
};

const idForType = (type, id) => ({
  [types.GEONAME]: () => parseInt(id, 10),
  [types.SPOT]: () => new mongoose.mongo.ObjectId(id),
  [types.SUBREGION]: () => new mongoose.mongo.ObjectId(id),
  [types.REGION]: () => new mongoose.mongo.ObjectId(id),
  [types.TAXONOMY]: () => new mongoose.mongo.ObjectId(id),
})[type].call();

const match = (type, id) => ({ [indexedTypeFields[type]]: idForType(type, id) });

const matchIn = (type, ids) => ({
  [indexedTypeFields[type]]: { $in: ids.map((id) => idForType(type, id)) },
});

const findTaxonomyById = (id) => Taxonomy.findById(id);

const findTaxonomyByType = (type, id) => Taxonomy.findOne(match(type, id));

const findTaxonomyByIds = (ids) => Taxonomy.find({ _id: { $in: ids } });

const deleteTaxonomyById = (_id) => Taxonomy.deleteOne({ _id });

const updateTaxonomyById = (id, $set = {}) => Taxonomy.findByIdAndUpdate(
  id,
  { $set },
  { new: true, runValidators: true },
);

const fetchTaxonomy = (type, id, maxDepth = 10) => Taxonomy.aggregate([
  { $match: match(type, id) },
  {
    $graphLookup: {
      from: 'Taxonomy',
      startWith: '$_id',
      connectFromField: 'liesIn',
      connectToField: '_id',
      as: 'in',
      depthField: 'depth',
    },
  },
  {
    $graphLookup: {
      from: 'Taxonomy',
      startWith: '$_id',
      connectFromField: '_id',
      connectToField: 'liesIn',
      as: 'contains',
      maxDepth,
      depthField: 'depth',
    },
  },
]);

const batchFetchTaxonomyIds = (terms, extraFields = []) => Taxonomy.aggregate([
  { $match: matchIn('spot', terms.spots) },
  {
    $graphLookup: {
      from: 'Taxonomy',
      startWith: '$_id',
      connectFromField: 'liesIn',
      connectToField: '_id',
      as: 'in',
    },
  },
  { $unwind: '$in' },
  {
    $group: {
      _id: '$in._id',
      doc: { $first: '$$ROOT' },
    },
  },
  { $replaceRoot: { newRoot: '$doc' } },
  {
    $project: {
      _id: '$in._id',
      name: '$in.name',
      type: '$in.type',
      ...extraFields.reduce((acc, k) => ({ ...acc, [k]: `$in.${k}` }), {}),
      ...Object.values(indexedTypeFields).reduce((acc, k) => ({ ...acc, [k]: `$in.${k}` }), {}),
    },
  },
]);

const fetchNearbyTaxonomy = (
  coordinates,
  queryParams,
  count = 5,
  maxDepth = 10,
  maxContainsDepth,
) => {
  let query = {
    type: queryParams.type,
    'geonames.fcl': queryParams.fcl,
    'geonames.fcode': queryParams.fcode,
    hasSpots: queryParams.type !== 'spot',
  };

  query = _.omitBy(query, _.isNil);

  const nearbyAggregate = [
    {
      $geoNear: {
        near: { type: 'Point', coordinates },
        distanceField: 'distance',
        spherical: true,
        query,
      },
    },
    {
      $limit: count,
    },
    {
      $graphLookup: {
        from: 'Taxonomy',
        startWith: '$_id',
        connectFromField: 'liesIn',
        connectToField: '_id',
        as: 'containedBy',
        maxDepth,
        depthField: 'depth',
      },
    },
    { $sort: { distance: -1 } }, // descending sort on distance somehow returns closest city first
    { $unwind: '$containedBy' },
    {
      $group: {
        _id: '$containedBy._id',
        doc: { $first: '$containedBy' },
      },
    },
    { $replaceRoot: { newRoot: '$doc' } },
  ];

  if (maxContainsDepth) {
    nearbyAggregate.push({
      $graphLookup: {
        from: 'Taxonomy',
        startWith: '$_id',
        connectFromField: '_id',
        connectToField: 'liesIn',
        as: 'contains',
        maxDepth: maxContainsDepth,
        depthField: 'depth',
      },
    });
  }

  return Taxonomy.aggregate(nearbyAggregate);
};

const removeEdge = async (start, end) => {
  const [startType, startId] = start;
  const [endType, endId] = end;
  const endDoc = await Taxonomy.findOne(match(endType, endId));
  if (!endDoc) return null;
  return Taxonomy.findOneAndUpdate(
    match(startType, startId),
    { $pull: { liesIn: endDoc._id } },
  );
};

const fetchNoSpotsTaxonomy = () => Taxonomy.find({ hasSpots: false, type: { $ne: 'spot' } }).lean();

export default fetchTaxonomy;
export {
  match,
  batchFetchTaxonomyIds,
  removeEdge,
  findTaxonomyById,
  findTaxonomyByType,
  findTaxonomyByIds,
  deleteTaxonomyById,
  updateTaxonomyById,
  fetchNearbyTaxonomy,
  fetchNoSpotsTaxonomy,
};
