import mongoose from 'mongoose';
import SpotTaxonomy from './schemas/spot';
import Taxonomy from './schemas/taxonomy';
import SpotModel from '../SpotModel';

const updateSpot = async (spot, liesIn) => SpotTaxonomy.findOneAndUpdate(
  { spot: spot._id },
  {
    $set: {
      category: 'surfline',
      name: spot.name,
      location: spot.location,
    },
    $addToSet: { liesIn },
  },
  {
    upsert: true,
    new: true,
    runValidators: true,
    setDefaultsOnInsert: true,
    timestamps: true,
  },
);

const spotsForTaxonomy = async (taxonomyId) => {
  const result = await Taxonomy.aggregate([
    { $match: { _id: new mongoose.mongo.ObjectId(taxonomyId) } },
    {
      $graphLookup: {
        from: 'Taxonomy',
        startWith: '$_id',
        connectFromField: '_id',
        connectToField: 'liesIn',
        as: 'contains',
        depthField: 'depth',
      },
    },
  ]);

  // this is easier to filter than coming out of an unwound aggregaton query
  if (result.length > 0) {
    return result[0].contains.filter((node) => node.type === 'spot');
  }

  return [];
};

const geonameForLegacy = async (legacyId) => {
  const spot = await SpotModel.findOne({ legacyId });
  return spot.geoname;
};

export { updateSpot, spotsForTaxonomy, geonameForLegacy };
