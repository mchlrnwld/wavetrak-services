import { expect } from 'chai';
import sinon from 'sinon';
import TaxonomyModel, { types } from './schemas/taxonomy';
import * as taxonomy from './taxonomy';
import * as spots from './spot';
import * as geoname from './geoname';
import { markTaxonomyHasSpots, deleteNode, getEnumeratedPath } from './treeUtils';

describe('tree utilities', () => {
  beforeEach(() => {
    sinon.stub(spots, 'spotsForTaxonomy');
    sinon.stub(taxonomy, 'default');
    sinon.stub(taxonomy, 'updateTaxonomyById');
    sinon.stub(TaxonomyModel, 'findOne');
    sinon.stub(TaxonomyModel, 'findByIdAndUpdate');
    sinon.stub(TaxonomyModel, 'findOneAndRemove');
  });

  afterEach(() => {
    spots.spotsForTaxonomy.restore();
    taxonomy.default.restore();
    taxonomy.updateTaxonomyById.restore();
    TaxonomyModel.findOne.restore();
    TaxonomyModel.findByIdAndUpdate.restore();
    TaxonomyModel.findOneAndRemove.restore();
  });

  describe('mark taxonomy has spots', () => {
    it('should traverse the tree and update hasSpots until it no longer needs to', async () => {
      const earth = {
        _id: '58f7ed51dadb30820bb38782',
        liesIn: [null],
        hasSpots: true,
      };
      const northAmerica = {
        _id: '58f7ed51dadb30820bb38791',
        liesIn: [earth._id],
        hasSpots: true,
      };
      const unitedStates = {
        _id: '58f7ed51dadb30820bb3879c',
        liesIn: [northAmerica._id],
        hasSpots: true,
      };
      const california = {
        _id: '58f7ed51dadb30820bb387a6',
        liesIn: [unitedStates._id],
        hasSpots: false,
      };
      const orangeCounty = {
        _id: '58f7ed5ddadb30820bb39651',
        liesIn: [california._id],
        hasSpots: false,
      };
      const huntingtonBeach = {
        _id: '58f7ed5fdadb30820bb3987c',
        liesIn: [orangeCounty._id],
        hasSpots: false,
      };
      const hbPierSouthside = {
        _id: '58f7edaadadb30820bb3e72c',
        type: 'spot',
        liesIn: [huntingtonBeach._id],
        hasSpots: false,
      };
      taxonomy.default.withArgs(types.TAXONOMY, huntingtonBeach._id, 0).resolves([{
        ...huntingtonBeach,
        contains: [hbPierSouthside],
      }]);
      taxonomy.default.withArgs(types.TAXONOMY, orangeCounty._id, 0).resolves([{
        ...orangeCounty,
        contains: [{
          ...huntingtonBeach,
          hasSpots: true,
        }],
      }]);
      taxonomy.default.withArgs(types.TAXONOMY, california._id, 0).resolves([{
        ...california,
        contains: [{
          ...orangeCounty,
          hasSpots: true,
        }],
      }]);
      taxonomy.default.withArgs(types.TAXONOMY, unitedStates._id, 0).resolves([{
        ...unitedStates,
        contains: [{
          ...california,
          hasSpots: true,
        }],
      }]);
      await markTaxonomyHasSpots(huntingtonBeach._id);
      expect(taxonomy.default.callCount).to.equal(4);
      expect(taxonomy.default).to.have.been.calledWithExactly(
        types.TAXONOMY,
        huntingtonBeach._id,
        0,
      );
      expect(taxonomy.default).to.have.been.calledWithExactly(
        types.TAXONOMY,
        orangeCounty._id,
        0,
      );
      expect(taxonomy.default).to.have.been.calledWithExactly(
        types.TAXONOMY,
        california._id,
        0,
      );
      expect(taxonomy.default).to.have.been.calledWithExactly(
        types.TAXONOMY,
        unitedStates._id,
        0,
      );
      expect(taxonomy.updateTaxonomyById).to.have.been.calledThrice();
      expect(taxonomy.updateTaxonomyById).to.have.been.calledWithExactly(
        huntingtonBeach._id,
        { hasSpots: true },
      );
      expect(taxonomy.updateTaxonomyById).to.have.been.calledWithExactly(
        orangeCounty._id,
        { hasSpots: true },
      );
      expect(taxonomy.updateTaxonomyById).to.have.been.calledWithExactly(
        california._id,
        { hasSpots: true },
      );
    });
  });

  describe('delete node', () => {
    const spot = {
      _id: '58eac6362fe5857c49d19f80',
      spot: '5842041f4e65fad6a770883c',
      type: 'spot',
      liesIn: ['58eac6362fe5857c49d19f7a'],
    };

    const lajolla = {
      _id: '58eac6362fe5857c49d19f7e',
      geonameId: 5363943,
      type: 'geoname',
      liesIn: ['58eac6362fe5857c49d19f7c'],
    };

    it('deletes a node', async () => {
      TaxonomyModel.findOneAndRemove.resolves(lajolla);
      taxonomy.default.resolves([{
        ...spot,
        hasSpots: false,
        contains: [],
      }]);
      await deleteNode('geoname', lajolla._id);
      expect(TaxonomyModel.findOneAndRemove).to.have.been.calledOnce();
      expect(TaxonomyModel.findOneAndRemove).to.have.been.calledWithExactly({ geonameId: 58 });
      expect(taxonomy.default).to.have.been.calledOnce();
      expect(taxonomy.default).to.have.been.calledWithExactly(types.TAXONOMY, lajolla.liesIn[0], 0);
    });
  });

  describe('get enumerated path', () => {
    const liesIn = ['58f7ed5ddadb30820bb39651'];

    beforeEach(() => {
      sinon.stub(geoname, 'findGeonamesByTaxonomyIds');
    });

    afterEach(() => {
      geoname.findGeonamesByTaxonomyIds.restore();
    });

    it('returns enumerated path for liesIn', async () => {
      geoname.findGeonamesByTaxonomyIds.resolves([{
        geonameId: 5379524,
      }]);
      taxonomy.default.resolves([{
        in: [
          {
            _id: '58f7ed51dadb30820bb387a6',
            name: 'California',
            liesIn: ['58f7ed51dadb30820bb3879c'],
          },
          {
            _id: '58f7ed5ddadb30820bb39651',
            name: 'Orange County',
            liesIn: ['58f7ed51dadb30820bb387a6'],
          },
          {
            _id: '58f7ed51dadb30820bb38791',
            name: 'North America',
            liesIn: ['58f7ed51dadb30820bb38782'],
          },
          {
            _id: '58f7ed51dadb30820bb3879c',
            name: 'United States',
            liesIn: ['58f7ed51dadb30820bb38791'],
          },
          {
            _id: '58f7ed51dadb30820bb38782',
            name: 'Earth',
            liesIn: [null],
          },
        ],
      }]);
      expect(await getEnumeratedPath(liesIn)).to.equal(',Earth,North America,United States,California,Orange County');
    });

    it('returns null if geoname not found from liesIn', async () => {
      geoname.findGeonamesByTaxonomyIds.resolves([]);
      expect(await getEnumeratedPath(liesIn)).to.be.null();
    });
  });
});
