import SubregionTaxonomy from './schemas/subregion';

const updateSubregion = async (subregion, liesIn) => SubregionTaxonomy.findOneAndUpdate(
  { subregion: subregion._id },
  {
    $set: {
      category: 'surfline',
      name: subregion.name,
      hasSpots: true,
    },
    ...(liesIn ? { $addToSet: { liesIn } } : null),
  },
  {
    upsert: true,
    new: true,
    runValidators: true,
    setDefaultsOnInsert: true,
    timestamps: true,
  },
);

export { updateSubregion }; // eslint-disable-line import/prefer-default-export
