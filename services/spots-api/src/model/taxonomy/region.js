import RegionTaxonomy from './schemas/region';

export const updateRegion = async (region, liesIn) => RegionTaxonomy.findOneAndUpdate(
  { region: region._id },
  {
    $set: {
      category: 'surfline',
      name: region.name,
    },
    ...(liesIn ? { $addToSet: { liesIn } } : null),
  },
  {
    upsert: true, new: true, runValidators: true, setDefaultsOnInsert: true,
  },
);

export default RegionTaxonomy;
