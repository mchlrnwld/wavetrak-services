/* eslint-disable no-restricted-syntax */
import Taxonomy, { types } from './schemas/taxonomy';
import { findGeonamesByTaxonomyIds } from './geoname';
import fetchTaxonomy, { match, updateTaxonomyById } from './taxonomy';

/**
 * Updates `hasSpots` property for the given taxonomy and parents.
 * @param  {ObjectId}  taxonomyId the taxonomy to update `hasSpots` for
 * @return {Promise}              resolves when all updates are finished
 */
export const markTaxonomyHasSpots = async (taxonomyId) => {
  const [taxonomy] = await fetchTaxonomy(types.TAXONOMY, taxonomyId, 0);
  const hasSpots = taxonomy.contains.some((node) => node.hasSpots || node.type === 'spot');

  if (hasSpots !== taxonomy.hasSpots) {
    await updateTaxonomyById(taxonomyId, { hasSpots });

    if (taxonomy.liesIn) {
      await Promise.all(taxonomy.liesIn.filter((id) => id).map(markTaxonomyHasSpots));
    }
  }
};

/**
 * Marks and checks that there are valid paths to spots for a given taxonomy
 * node.
 *
 * @param {Mixed} id the type id of the taxonomy to start traversing
 * @param {Enum} type the type of taxonomy node to start traversing
 */
export const markTaxonomyHasSpotsByType = async (type, id) => {
  const root = await Taxonomy.findOne(match(type, id));
  if (!root) return null;
  return markTaxonomyHasSpots(root._id);
};

/**
 * Deletes a node document from the taxonomy collection.
 *
 * @param {Mixed} id the id of the node to delete
 * @param {Enum} type the `type` of taxonomy node to delete
 */
export const deleteNode = async (type, id) => {
  const node = await Taxonomy.findOneAndRemove(match(type, id));

  // Loop through instead of Promise.all to avoid race condition
  if (node && node.liesIn) {
    for (const taxonomyId of node.liesIn) {
      // eslint-disable-next-line no-await-in-loop
      await markTaxonomyHasSpots(taxonomyId);
    }
  }
};

export const getEnumeratedPath = async (liesIn) => {
  const liesInGeonames = await findGeonamesByTaxonomyIds(liesIn);

  if (!liesInGeonames[0]) return null;
  const [taxonomy] = await fetchTaxonomy(types.GEONAME, liesInGeonames[0].geonameId, 0);

  const path = [...Array(taxonomy.in.length)];
  const top = taxonomy.in.find(
    (node) => !node.liesIn || (node.liesIn.length === 1 && node.liesIn[0] === null),
  );
  const liesInContainsNextId = (next) => (node) => node.liesIn.some((id) => `${id}` === `${next._id}`);

  let next = top;
  for (let i = 0; i < path.length; i += 1) {
    path[i] = next;
    next = taxonomy.in.find(liesInContainsNextId(next));
  }

  return `,${path.map(({ name }) => name).join(',')}`;
};
