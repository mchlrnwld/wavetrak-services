import GeonameTaxonomy from './schemas/geoname';

const createGeoname = async (model) => {
  const geoname = new GeonameTaxonomy(model);
  await geoname.save();
  return geoname;
};

const findGeonamesByTaxonomyIds = (taxonomyIds) => GeonameTaxonomy.find({
  _id: {
    $in: taxonomyIds,
  },
});

export default GeonameTaxonomy;
export { createGeoname, findGeonamesByTaxonomyIds };
