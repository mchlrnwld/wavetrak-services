import { expect } from 'chai';
import sinon from 'sinon';
import SubregionModel from '../SubregionModel';
import RegionModel from '../RegionModel';
import * as spots from './spot';
import * as subregion from './subregion';
import * as region from './region';
import * as taxonomy from './taxonomy';
import * as normalize from './normalize';
import * as treeUtils from './treeUtils';

describe('taxonomy model', async () => {
  beforeEach(() => {
    sinon.stub(spots, 'updateSpot');
    sinon.stub(SubregionModel, 'findOne');
    sinon.stub(RegionModel, 'findOne');
    sinon.stub(subregion, 'updateSubregion');
    sinon.stub(region, 'updateRegion');
  });

  afterEach(() => {
    spots.updateSpot.restore();
    SubregionModel.findOne.restore();
    RegionModel.findOne.restore();
    region.updateRegion.restore();
    subregion.updateSubregion.restore();
  });

  describe('normalize spot and geonames', async () => {
    beforeEach(() => {
      sinon.stub(taxonomy, 'findTaxonomyByType');
      sinon.stub(normalize, 'normalizeSubregion');
      sinon.stub(treeUtils, 'markTaxonomyHasSpots');
      sinon.stub(treeUtils, 'markTaxonomyHasSpotsByType');
    });

    afterEach(() => {
      taxonomy.findTaxonomyByType.restore();
      normalize.normalizeSubregion.restore();
      treeUtils.markTaxonomyHasSpots.restore();
      treeUtils.markTaxonomyHasSpotsByType.restore();
    });

    it('should add spots geonames to the taxonomy', async () => {
      const spot = {
        _id: '5842041f4e65fad6a770883c',
        geoname: 5391832,
        subregion: null,
      };

      taxonomy.findTaxonomyByType.resolves({ _id: '58efcdf82fe5857c49d1c05b', contains: [], hasSpots: false });
      normalize.normalizeSubregion.resolves('58f7ed5ddadb30820bb39689');
      spots.updateSpot.resolves({});

      const normalized = await normalize.normalizeSpot(spot);

      expect(spots.updateSpot).to.have.been.calledTwice();
      expect(spots.updateSpot).to.have.been.calledWithExactly(spot, '58efcdf82fe5857c49d1c05b');
      expect(spots.updateSpot).to.have.been.calledWithExactly(spot, '58f7ed5ddadb30820bb39689');
      expect(treeUtils.markTaxonomyHasSpots).to.have.been.calledTwice();
      expect(treeUtils.markTaxonomyHasSpots).to.have.been.calledWithExactly('58efcdf82fe5857c49d1c05b');
      expect(treeUtils.markTaxonomyHasSpots).to.have.been.calledWithExactly('58f7ed5ddadb30820bb39689');
      expect(normalized).to.deep.equal('58f7ed5ddadb30820bb39689');
    });
  });

  describe('subregion and region', async () => {
    beforeEach(() => {
      sinon.stub(taxonomy, 'findTaxonomyByType');
    });

    afterEach(() => {
      taxonomy.findTaxonomyByType.restore();
    });

    it('should normalize a subregion and region', async () => {
      const subregionModelFixture = {
        _id: '58581a836630e24c4487900d',
        name: 'South San Diego',
        region: '58dbfe044bfa680012082307',
        geoname: 5391832,
      };

      const subregionTaxonomyFixture = {
        _id: '58efcdf82fe5857c49d1c065',
        subregion: '58581a836630e24c4487900d',
        type: 'subregion',
        liesIn: ['58efcdf82fe5857c49d1c063'],
        name: 'South San Diego',
        category: 'surfline',
        __v: 0,
        hasSpots: true,
      };

      const geonameTaxonomyFixture = { _id: '58f7ed65dadb30820bb39f6a' };

      taxonomy.findTaxonomyByType.resolves(geonameTaxonomyFixture);
      RegionModel.findOne.resolves(subregionTaxonomyFixture);
      region.updateRegion.resolves({ _id: '58efcdf82fe5857c49d1c063' });
      subregion.updateSubregion.resolves({ _id: '58efcdf82fe5857c49d1c065' });

      const normalized = await normalize.normalizeSubregion(subregionModelFixture);

      expect(taxonomy.findTaxonomyByType).to.have.been.calledOnce();
      expect(taxonomy.findTaxonomyByType).to.have.been.calledWithExactly(
        'geoname',
        subregionModelFixture.geoname,
      );
      expect(subregion.updateSubregion).to.have.been.calledTwice();
      expect(subregion.updateSubregion).to.have.been.calledWith(
        subregionModelFixture,
        '58efcdf82fe5857c49d1c063',
      );
      expect(subregion.updateSubregion).to.have.been.calledWith(
        subregionModelFixture,
        geonameTaxonomyFixture._id,
      );
      expect(normalized).to.deep.equal(subregionTaxonomyFixture._id);
    });

    it('should fail if there is no subregion', async () => {
      SubregionModel.findOne.resolves(null);
      try {
        await normalize.normalizeSubregion('58581a836630e24c4487900d');
      } catch (err) {
        expect(err.message).to.equal('A subregion could not be found');
      }
    });
  });
});
