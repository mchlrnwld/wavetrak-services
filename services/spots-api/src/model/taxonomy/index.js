import Taxonomy, { types } from './schemas/taxonomy';
import fetchTaxonomy, {
  batchFetchTaxonomyIds,
  removeEdge,
  findTaxonomyById,
  findTaxonomyByType,
  findTaxonomyByIds,
  deleteTaxonomyById,
  updateTaxonomyById,
  fetchNearbyTaxonomy,
  fetchNoSpotsTaxonomy,
} from './taxonomy';
import { createGeoname } from './geoname';
import { deleteNode } from './treeUtils';
import { normalizeSpot, normalizeSubregion } from './normalize';

export {
  fetchTaxonomy,
  findTaxonomyById,
  findTaxonomyByType,
  findTaxonomyByIds,
  batchFetchTaxonomyIds,
  deleteTaxonomyById,
  updateTaxonomyById,
  fetchNearbyTaxonomy,
  fetchNoSpotsTaxonomy,
  createGeoname,
  deleteNode,
  removeEdge,
  types,
  normalizeSpot,
  normalizeSubregion,
};

export default Taxonomy;
