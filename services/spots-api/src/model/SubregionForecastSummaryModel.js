import mongoose from 'mongoose';

const subregionForecastSummarySchema = new mongoose.Schema(
  {
    forecastStatus: {
      status: {
        type: String,
        default: 'active',
      },
    },
  },
  {
    collection: 'SubregionForecastSummaries',
    timestamps: true,
  },
);

const SubregionForecastSummaryModel = mongoose.model('SubregionForecastSummary', subregionForecastSummarySchema);

export default SubregionForecastSummaryModel;
