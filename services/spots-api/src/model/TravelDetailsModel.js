const directions = [
  'N',
  'NNE',
  'NE',
  'ENE',
  'E',
  'ESE',
  'SE',
  'SSE',
  'S',
  'SSW',
  'SW',
  'WSW',
  'W',
  'WNW',
  'NW',
  'NNW',
];

const breakTypes = [
  'Beach_Break',
  'Canyon',
  'Jetty',
  'Offshore',
  'Pier',
  'Point',
  'Reef',
  'Slab',
  'Lefts',
  'Rights',
];

const bottomTypes = [
  'Coral',
  'Lava',
  'Rock',
  'Sand',
];

const tides = [
  'Low',
  'Medium_Low',
  'Medium',
  'Medium_High',
  'High',
];

const seasons = [
  'Spring',
  'Summer',
  'Autumn',
  'Winter',
];

export default {
  travelId: { type: String, default: null },
  abilityLevels: {
    description: { type: String, default: null },
  },
  access: { type: String, default: null },
  best: {
    season: {
      description: { type: String, default: null },
      value: [{ type: String, default: null, enum: seasons }],
    },
    tide: {
      description: { type: String, default: null },
      value: [{ type: String, default: null, enum: tides }],
    },
    size: {
      description: { type: String, default: null },
    },
    windDirection: {
      description: { type: String, default: null },
      value: [{ type: String, default: null, enum: directions }],
    },
    swellDirection: {
      description: { type: String, default: null },
      value: [{ type: String, default: null, enum: directions }],
    },
  },
  bottom: {
    description: { type: String, default: null },
    value: [{ type: String, default: null, enum: bottomTypes }],
  },
  breakType: [{ type: String, enum: breakTypes, default: null }],
  crowdFactor: {
    description: { type: String, default: null },
    rating: { type: Number, default: 0 },
  },
  description: { type: String, default: null },
  hazards: { type: String, default: null },
  localVibe: {
    description: { type: String, default: null },
    rating: { type: Number, default: 0 },
  },
  shoulderBurn: {
    description: { type: String, default: null },
    rating: { type: Number, default: 0 },
  },
  relatedArticleId: { type: String, default: null },
  spotRating: {
    description: { type: String, default: null },
    rating: { type: Number, default: 0 },
  },
  waterQuality: {
    description: { type: String, default: null },
    rating: { type: Number, default: 0 },
  },
  status: {
    type: String,
    enum: ['PUBLISHED', 'DRAFT'],
    default: 'DRAFT',
  },
};
