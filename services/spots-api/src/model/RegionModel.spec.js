import { expect } from 'chai';
import Region from './RegionModel';

describe('Region Model', () => {
  it('should require name field', (done) => {
    const region = new Region({});
    region.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.name).to.be.ok();
      expect(err.errors.name.message).to.equal('Name is required');
      return done();
    });
  });
});
