export const validateUniqueList = (v) => new Set(v.map((val) => `${val}`)).size === v.length;

export const stripFalsey = (array) => (
  array && array.length ? array.filter((arr) => !!arr) : []
);
