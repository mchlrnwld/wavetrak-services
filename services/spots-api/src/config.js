const config = {
  WWW_HOST: process.env.WWW_HOST,
  SPOTS_API_HOST: process.env.SPOTS_API_HOST,
  MSW_API: process.env.MSW_API,
  SL_API: `${process.env.SPOTS_API}/admin/`,
  MSW_API_USER: process.env.MSW_API_USER,
  CAMERAS_API: process.env.CAMERAS_API,
  CAMERA_DETAILS_UPDATED_SNS_TOPIC: process.env.CAMERA_DETAILS_UPDATED_SNS_TOPIC,
};

export default config;
