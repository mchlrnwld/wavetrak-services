import { APIError } from '@surfline/services-common';

class InvalidGeonameError extends APIError {
  /**
   * @param {string} [message="Invalid geoname"]
   * @param {number} [code=400]
   */
  constructor(message = 'Invalid geoname', code = 400) {
    super(message, code);
    Error.captureStackTrace(this, InvalidGeonameError);
  }
}

export default InvalidGeonameError;
