openapi: 3.0.3
info:
  version: 1.0.0
  title: Wavetrak / Surfline Spots API
  description: Wavetrak / Surfline Spots API

servers:
  - url: http://spots-api.sandbox.surfline.com
    description: Sandbox
  - url: http://spots-api.staging.surfline.com
    description: Staging
  - url: http://spots-api.surfline.com
    description: Production

tags:
  - name: Spots
  - name: Subregions

components:
  parameters:
    SubregionId:
      name: subregionId
      in: path
      required: true
      description: Unique identifier for Surfline subregion.
      schema:
        type: string
        example: '58581a836630e24c44878fd6'
  responses:
    Unauthorized:
      description: Unauthorized.
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: Error message.
                type: string
                example: 'Unauthorized'
    NotFound:
      description: Not Found.
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: Error message.
                type: string
                example: 'Subregion not found'
    BadRequest:
      description: Bad Request.
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: Error message.
                type: string
                example: 'Valid SubregionId Required'
    InternalServerError:
      description: Internal Server Error.
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: Error message.
                type: string
                example: 'Somethign went wrong.'
  schemas:
    Subregion:
      type: object
      properties:
        name:
          type: string
          description: The name of the subregion
          example: 'Nova Scotia'
        region:
          type: string
          description: Unique identifier for Surfline region.
          example: '5908c4f1380d980012ba8a4b'
        primarySpot:
          type: string
          description: Identifier for primary Surfline spot associated with subregion.
          example: '584204204e65fad6a77094cf'
        geoname:
          type: number
          description: Geoname ID of the subregion location.
          example: 6251999
        order:
          type: string
          description: The order within the region
          example: '5'
        status:
          type: string
          enum:
            - PUBLISHED
            - DRAFT
          description: That status of the subregion.
          example: 'PUBLISHED'
        basin:
          type: string
          description: The basin for the subregions.
          example: 'natl'
        offshoreSwellLocation:
          type: object
          description: Offshore swell location for the subregion
          properties:
            type:
              type: string
              enum:
                - Point
              description: The type of location.
              example: 'Point'
            coordinates:
              type: array
              description: Latitude and Longitute coordinates
              example: [-63.25, 44.25]
        forecastStatus:
          type: object
          description: The forecast status for the subregion
          properties:
            status:
              type: string
              enum:
                - active
                - inactive
              description: The forecast status for the subregion.
              example: 'active'
            inactiveMessage:
              type: string
              description: A message describing the inactive forecast status.
              example: 'Inactive status message'
    Spot:
      type: object
      properties:
        name:
          description: The name of the spot.
          type: string
          example: 'Pipeline'
        legacyId:
          description: The legacy cms id of the spot
          type: number
          example: 4750
        cams:
          description: An array of camera object ids associated to the spot
          type: array
          items:
            type: string
            description: A camera object Id
        subregion:
          description: Parent subregionID
          type: string
          example: '5908c4f1380d980012ba8a4b'
        timezone:
          description: Spot's timezone in string format
          type: string
          example: 'Pacific/Honolulu'
        status:
          type: string
          enum:
            - PUBLISHED
            - DRAFT
            - DELETED
          description: That status of the spot. These values are case sensitive
          example: 'PUBLISHED'
        politicalLocation:
          type: object
          properties:
            city:
              type: object
              properties:
                geonameId:
                  description: The geonameId for the spot's city.
                  type: number
                  example: 6251999
                name:
                  description: The name of the city.
                  type: string
                  example: 'Pupukea'
        offshoreDirection:
          description: A number between 0 and 360 representing the spot's offshore swell direction.
          type: string
          example: 250
        lolaSurfBiasCoefficients:
          type: object
          properties:
            linear:
              description: Defaults to 1
              type: number
              example: 1
            constant:
              description: Defaults to 0
              type: number
              example: 1
        noraBiasCorrection:
          description: Defaults to 0
          type: number
          example: 1
        swellWindow:
          type: array
          items:
            type: object
            properties:
              min:
                type: number
                example: 295
              max:
                type: number
                example: 35
        best:
          description: An object of best properties
          type: object
          properties:
            windDirection:
              type: array
              items:
                type: object
                properties:
                  min:
                    type: number
                    example: 295
                  max:
                    type: number
                    example: 35
            swellPeriod:
              type: array
              items:
                type: object
                properties:
                  min:
                    type: number
                    example: 295
                  max:
                    type: number
                    example: 35
            swellWindow:
              type: array
              items:
                type: object
                properties:
                  min:
                    type: number
                    example: 295
                  max:
                    type: number
                    example: 35
            sizeRange:
              type: array
              items:
                type: object
                properties:
                  min:
                    type: number
                    example: 295
                  max:
                    type: number
                    example: 35
        humanReport:
          description: Indicates if the spot has a human report and associated properties
          type: object
          properties:
            status:
              type: string
              enum:
                - OFF
                - ON
                - AVERAGE
                - CLONE
              description: The status of the human report for the spot.
              example: 'OFF'
            clone:
              description: The objectId for another spot to clone the humanReport from.
              type: string
              example: '5908c4f1380d980012ba8a4b'
            average:
              type: object
              properties:
                spotOneId:
                  description: The objectId for for a spot to use for averaging the humanReport.
                  type: string
                  example: '5908c4f1380d980012ba8a4b'
                spotTwoId:
                  description: The objectId for for a spot to use for averaging the humanReport.
                  type: string
                  example: '5908c4f1380d980012ba8a4b'
        geoname:
          description: Parent spot geoname Id.
          type: number
          example: 6251999
        spotType:
          description: The type of spot.
          type: string
          enum:
            - SURFBREAK
            - WAVEPOOL
            - SKISLOPE
          example: 'SURFBREAK'
        abilityLevels:
          description: The ability levels suitable for surfing the spot.
          type: array
          items:
            type: string
            enum:
              - BEGINNER
              - INTERMEDIATE
              - ADVANCED
              - PRO

        boardTypes:
          description: The ability levels suitable for surfing the spot.
          type: array
          items:
            type: string
            enum:
              - 'SHORTBOARD'
              - 'FISH'
              - 'FUNBOARD'
              - 'LONGBOARD'
              - 'GUN'
              - 'TOW'
              - 'SUP'
              - 'FOIL'
              - 'SKIMMING'
              - 'BODYBOARD'
              - 'BODYSURFING'
              - 'KITEBOARD'
        relivableRating:
          type: number
          example: 5
        pointOfInterestId:
          description: The associated point of interest id
          type: string
          example: '5908c4f1380d980012ba8a4b'
        titleTag:
          description: The title tag for the spot.
          type: string
        slug:
          description: The slug used in the url for the spot on the web.
          type: string
          example: 'the-wedge'
        metaDescription:
          description: The description for the meta tag of the spot page.
          type: string
        location:
          type: object
          description: The location of the spot
          properties:
            type:
              type: string
              enum:
                - Point
              description: The type of location.
              example: 'Point'
            coordinates:
              type: array
              description: Latitude and Longitute coordinates
              example: [-63.25, 44.25]
        forecastLocation:
          type: object
          description: The offshore forecast location of the spot. Defaults to the `location`.
          properties:
            type:
              type: string
              enum:
                - Point
              description: The type of location.
              example: 'Point'
            coordinates:
              type: array
              description: Latitude and Longitute coordinates
              example: [-63.25, 44.25]

        forecastStatus:
          type: object
          description: The forecast status for the subregion
          properties:
            status:
              type: string
              enum:
                - active
                - inactive
              description: The forecast status for the subregion.
              example: 'active'
            inactiveMessage:
              type: string
              description: A message describing the inactive forecast status.
              example: 'Inactive status message'
        travelDetails:
          type: object
          properties:
            abilityLevels:
              type: object
              properties:
                description:
                  description: The ability levels suitable for surfing the spot
                  type: string
                  example: 'advanced to psychotic'
            best:
              type: object
              properties:
                season:
                  type: object
                  properties:
                    description:
                      description: Short overview of best seasons.
                      type: string
                      example: 'January-December, but fall can be epic.'
                    value:
                      type: array
                      example: ['Winter', 'Autumn']
                      description: Best season values. Enum values include "Spring", "Summer", "Autumn", "Winter".
                      items:
                        type: string
                        enum: ['Spring', 'Summer', 'Autumn', 'Winter']
                tide:
                  type: object
                  properties:
                    description:
                      description: Short overview of best tides.
                      type: string
                      example: 'medium'
                    value:
                      type: array
                      description: Best tide values. Enum values include "Low", "Medium_Low", "Medium", "Medium_High", "High".
                      example: ['Low', 'Medium_Low']
                      items:
                        type: string
                        enum: ['Low', 'Medium_Low', 'Medium', 'Medium_High', 'High']
                size:
                  type: object
                  properties:
                    description:
                      description: Short overview of best wave sizes.
                      type: string
                      example: 'shoulder-high to 3 feet overhead'
                    value:
                      type: array
                      example: ['4-8']
                      description: Ranges of best wave sizes in feet.
                      items:
                        type: string
                windDirection:
                  type: object
                  properties:
                    description:
                      description: Short overview of best wind directions.
                      type: string
                      example: 'NE, Santa Anas, Southside of pier offers some protection from WNW wind and Northside from SE wind'
                    value:
                      type: array
                      example: ['NE', 'NNE', 'E']
                      description: Best wind directions.
                      items:
                        type: string
                swellDirection:
                  type: object
                  properties:
                    description:
                      description: Short overview of best swell directions.
                      type: string
                      example: 'Combo of swells SSE, S, SW with WSW, W, WNW'
                    value:
                      type: array
                      example: ['SSE', 'S', 'SSW', 'WSW', 'W']
                      description: Best swell directions.
                      items:
                        type: string
            bottom:
              type: object
              properties:
                description:
                  description: A description of the ocean floor beneath the surf break.
                  example: 'sand'
                  type: string
                value:
                  type: array
                  items:
                    type: string
                    example: 'Sand'
            crowdFactor:
              type: object
              properties:
                description:
                  description: A description of the crowds at the surf break.
                  example: 'moderate'
                  type: string
                rating:
                  description: A rating from 1 to 10 on how crowded it is.
                  type: number
                  example: 5
            localVibe:
              type: object
              properties:
                description:
                  description: A description of the local vibe.
                  example: 'sand'
                  type: string
                rating:
                  description: A rating from 1 to 10 on how localized it is.
                  type: number
                  example: 5
            shoulderBurn:
              type: object
              properties:
                description:
                  description: Indicates how much paddling is required.
                  example: '2'
                  type: string
                rating:
                  description: A rating from 1 to 10 for the shoulder burn.
                  type: number
                  example: 2
            spotRating:
              type: object
              properties:
                description:
                  description: Overall rating of the spot.
                  example: '9'
                  type: string
                rating:
                  description: A rating from 1 to 10 on the spot.
                  type: number
                  example: 9
            waterQuality:
              type: object
              properties:
                description:
                  description: Rating of the water quality.
                  example: 'Water is very clean'
                  type: string
                rating:
                  description: A rating from 1 to 10 on the water quality.
                  type: number
                  example: 10
            travelId:
              type: string
              description: A unique travel ID
            access:
              type: string
              description: Describes the type of access for the spot.
              example: "free street parking, but if it's pumping, plan to walk"
            breakType:
              type: array
              items:
                type: string
                description: A description of the break type
                example: 'Lefts, Jetty'
            description:
              type: string
              description: A description for the spot.
              example: 'The most famous surf spot in the world, Pipe is capable of pulling the most incredible disappearing acts...'
            hazards:
              type: string
              description: A description for the hazards at the spot.
              example: 'Broken body and board, death from impact into the shallow bottom or on someone else'
            relatedArticleId:
              type: string
              description: An id for a related article.
            status:
              description: The status of the travel details for the spot.
              type: string
              enum:
                - DRAFT
                - PUBLISHED
paths:
  /admin/subregions/:subregionid:
    patch:
      summary: Patch subregion
      description: Update one or more properties of a subregion.
      requestBody:
        required: false
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Subregion'
      tags:
        - Subregions
      responses:
        '200':
          description: Successfully returns the updated Surfline subregion.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Subregion'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /admin/spots/:spotId:
    patch:
      summary: Patch spot
      description: Update one or more properties of a spot.
      requestBody:
        required: false
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Spot'
      tags:
        - Spots
      responses:
        '200':
          description: Successfully returns the updated Surfline spot.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Spot'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
