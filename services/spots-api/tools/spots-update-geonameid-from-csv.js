/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import axios from 'axios';
import fs from 'fs';
import { parse } from 'csv-parse/sync';

import config from '../src/config';

const wait = async (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

const updateSpotsFromCSV = async (errors) => {
  let numSpotsPatched = 0;

  const content = fs.readFileSync('/Users/lucasharron/Downloads/NewZealand_Geo.csv');
  const spotsToUpdate = parse(content);

  for (const spot of spotsToUpdate) {
    const [spotId, spotName, newGeonameId] = spot;

    try {
      await axios.patch(`${config.SL_API}spots/${spotId}`, {
        geoname: newGeonameId,
      });

      console.log(`Patched ${spotId} ${spotName} ${newGeonameId}`);

      numSpotsPatched += 1;
      await wait(250);
    } catch (error) {
      console.log('error: ', error);
      errors.push({
        error: error.message,
        spotId,
        spotName,
      });
    }
  }

  console.log(`${numSpotsPatched} spots patched`);
};

const runScript = async () => {
  const errors = [];
  await updateSpotsFromCSV(errors);

  if (errors.length > 0) {
    console.log(errors);
    fs.writeFileSync('./script-errors.json', JSON.stringify(errors, null, 2));
  }

  console.log('DONE');
  process.exit(0);
};

runScript()
  .then(() => {
    console.log('DONE');
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(-1);
  });
