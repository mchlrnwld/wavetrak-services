/* eslint-disable */
import mongoose, { Types } from 'mongoose';
import { initMongoDB } from '../src/model/dbContext';
import Spots from '../src/model/SpotModel';

const wait = async (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

const isNumberRating = rating => {
  const parsedRating = parseInt(rating);
  return isNaN(parsedRating) ? null : parsedRating;
};

const importedTravelToTravelDetails = async (errors) => {
  const spots = await Spots.find({ 'importedTravel.travelId': { $exists: true, $ne: null } }, { importedTravel: 1 }).lean();

  console.log('Spots with imported travel: ', spots.length);

  for (const spot of spots) {
    try {
      const { importedTravel } = spot;
      const travelDetails = {
        travelId: importedTravel.travelId,
        abilityLevels: {
          description: importedTravel['Ability Level'],
        },
        access: importedTravel.Access,
        best: {
          season: {
            description: importedTravel['Best Season'],
            value: [],
          },
          tide: {
            description: importedTravel['Best Tide'],
            value: importedTravel['Best Tide DD'].split('|').map(v => v.replace(/ /g,'_')),
          },
          size: {
            description: importedTravel['Best Size'],
          },
          windDirection: {
            description: importedTravel['Best Wind'],
          },
          swellDirection: {
            description: importedTravel['Best Swell Direction'],
          },
        },
        bottom: {
          description: importedTravel.Bottom,
          value: importedTravel['Bottom DD'].split('|').map(v => v.replace(/ /g,'_')),
        },
        breakType: importedTravel['Break Type'].split('|').map(v => v.replace(/ /g,'_')),
        crowdFactor: {
          description: importedTravel['Crowd Factor'],
          rating: isNumberRating(importedTravel['Crowds DD']),
        },
        description: importedTravel['Spot Description'],
        hazards: importedTravel.Hazards,
        localVibe: {
          description: importedTravel['Local Vibe'],
          rating: isNumberRating(importedTravel['Local Vibe DD']),
        },
        shoulderBurn: {
          description: importedTravel['Bicep Burn'],
          rating: isNumberRating(importedTravel['Paddle Burn DD']),
        },
        relatedArticleId: null,
        spotRating: {
          description: importedTravel['Perfecto Meter'],
          rating: isNumberRating(importedTravel['Spot Quality Rating']),
        },
        waterQuality: {
          description: importedTravel['Poo Patrol'],
          rating: isNumberRating(importedTravel['Water Quality']),
        },
        status: 'DRAFT',
      };

      await Spots.updateOne({ _id: spot._id }, { travelDetails });
      await wait(2000);
      console.log('Processed spot ID: ', spot._id);
    } catch (error) {
      errors.push({ ...error, spotId: spot._id });
    }
  }
};

const runScript = async () => {
  await initMongoDB();
  const errors = [];
  await importedTravelToTravelDetails(errors);
  if (errors.length > 0) console.log(errors);
  console.log('DONE');
  process.exit(0);
};


runScript();
