/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import axios from 'axios';
import fs from 'fs';

import config from '../src/config';

// spots affected are stored in a JSON file (based on prod DB)
import SPOTS_PROD_NO_TRAVEL from './spotsBackup/2021-10-SpotsWithoutTravel.json';

// backups of spots collection have been saved to JSON files
import SPOTS_FROM_BACKUP_2021_10 from './spotsBackup/2021-10-Spots.json';
import SPOTS_FROM_BACKUP_2021_09 from './spotsBackup/2021-09-Spots.json';
import SPOTS_FROM_BACKUP_2021_08 from './spotsBackup/2021-08-Spots.json';
import SPOTS_FROM_BACKUP_2021_07 from './spotsBackup/2021-07-Spots.json';
import SPOTS_FROM_BACKUP_2021_06 from './spotsBackup/2021-06-Spots.json';
import SPOTS_FROM_BACKUP_2021_05 from './spotsBackup/2021-05-Spots.json';
import SPOTS_FROM_BACKUP_2021_04 from './spotsBackup/2021-04-Spots.json';
import SPOTS_FROM_BACKUP_2021_03 from './spotsBackup/2021-03-Spots.json';
import SPOTS_FROM_BACKUP_2021_02 from './spotsBackup/2021-02-Spots.json';
import SPOTS_FROM_BACKUP_2021_01 from './spotsBackup/2021-01-Spots.json';
import SPOTS_FROM_BACKUP_2020_12 from './spotsBackup/2020-12-Spots.json';
import SPOTS_FROM_BACKUP_2020_11 from './spotsBackup/2020-11-Spots.json';
import SPOTS_FROM_BACKUP_2020_10 from './spotsBackup/2020-10-Spots.json';

const allBackups = [
  { snapshotDate: '2021-10', spots: SPOTS_FROM_BACKUP_2021_10 },
  { snapshotDate: '2021-09', spots: SPOTS_FROM_BACKUP_2021_09 },
  { snapshotDate: '2021-08', spots: SPOTS_FROM_BACKUP_2021_08 },
  { snapshotDate: '2021-07', spots: SPOTS_FROM_BACKUP_2021_07 },
  { snapshotDate: '2021-06', spots: SPOTS_FROM_BACKUP_2021_06 },
  { snapshotDate: '2021-05', spots: SPOTS_FROM_BACKUP_2021_05 },
  { snapshotDate: '2021-04', spots: SPOTS_FROM_BACKUP_2021_04 },
  { snapshotDate: '2021-03', spots: SPOTS_FROM_BACKUP_2021_03 },
  { snapshotDate: '2021-02', spots: SPOTS_FROM_BACKUP_2021_02 },
  { snapshotDate: '2021-01', spots: SPOTS_FROM_BACKUP_2021_01 },
  { snapshotDate: '2020-12', spots: SPOTS_FROM_BACKUP_2020_12 },
  { snapshotDate: '2020-11', spots: SPOTS_FROM_BACKUP_2020_11 },
  { snapshotDate: '2020-10', spots: SPOTS_FROM_BACKUP_2020_10 },
];

const wait = async (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

const getSpotFromBackup = (spotId) => {
  for (const backup of allBackups) {
    // check each of the backups to find the most recent version with travelDetails
    const spotFromBackup = backup.spots.find((spotBackup) => spotBackup._id.$oid === spotId);
    if (spotFromBackup && spotFromBackup.travelDetails && spotFromBackup.travelDetails.status) {
      return spotFromBackup;
    }
  }
  return null;
};

const restoreDeletedTravelDetails = async (errors, checkOnly) => {
  let numSpotsPatched = 0;
  for (const spot of SPOTS_PROD_NO_TRAVEL) {
    const spotId = spot._id.$oid;
    const spotName = spot.name;
    try {
      const spotBackup = getSpotFromBackup(spotId, spotName);
      if (spotBackup && spotBackup.travelDetails && spotBackup.travelDetails.status) {
        // backup has travelInfo
        if (!checkOnly) {
          await axios.patch(`${config.SL_API}spots/${spotId}`, {
            travelDetails: spotBackup.travelDetails,
          });
          console.log(`Patched ${spotId} ${spotName}`);
        } else {
          console.log(`Spot to be patched ${spotId} ${spotName}`);
        }
        numSpotsPatched += 1;
      }

      await wait(250);
    } catch (error) {
      console.log('error: ', error);
      errors.push({
        error: error.message,
        spotId,
        spotName,
      });
    }
  }

  console.log(`${numSpotsPatched} spots patched`);
};

const runScript = async () => {
  const errors = [];
  // TODO: Change this to false when ready to hit /spots PATCH endpoint
  const checkOnly = true;
  await restoreDeletedTravelDetails(errors, checkOnly);

  if (errors.length > 0) {
    console.log(errors);
    fs.writeFileSync('./script-errors.json', JSON.stringify(errors, null, 2));
  }

  console.log('DONE');
  process.exit(0);
};

runScript().then(() => {
  console.log('DONE');
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(-1);
});
