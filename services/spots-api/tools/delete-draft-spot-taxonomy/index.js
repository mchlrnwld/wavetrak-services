/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import axios from 'axios';
import fs from 'fs';
import draftSpots from './draftSpots.json';
import config from '../../src/config';

const wait = async (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

const deleteDraftSpots = async (errors, deletedTaxonomies) => {
  for (const spot of draftSpots) {
    try {
      const { data } = await axios.get(`${config.SL_API}spots/${spot}`);
      const pointOfInterestId = data.pointOfInterestId || 'not a valid poi';
      await wait(250);

      await axios.patch(`${config.SL_API}spots/${spot}`, { pointOfInterestId });
      deletedTaxonomies.push(spot);

      await wait(250);
      console.log(`Processed ${spot}`);
    } catch (error) {
      console.log('error: ', error);
      errors.push({
        error: error.message,
        spot,
      });
    }
  }
};

const runScript = async () => {
  const errors = [];
  const deletedTaxonomies = [];
  await deleteDraftSpots(errors, deletedTaxonomies);
  fs.writeFileSync('./deletedSpotTaxonomies.json', JSON.stringify(deletedTaxonomies, null, 2));

  if (errors.length > 0) {
    console.log(errors);
    fs.writeFileSync('./script-errors.json', JSON.stringify(errors, null, 2));
  }

  console.log('DONE');
  process.exit(0);
};

runScript();