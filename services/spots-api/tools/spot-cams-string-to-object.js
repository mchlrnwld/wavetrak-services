import mongoose, { Types } from 'mongoose';
import { initMongoDB } from '../src/model/dbContext';
import Spots from '../src/model/SpotModel';

const wait = async (ms) => new Promise(resolve => {
  setTimeout(resolve, ms);
});

const convertSpotCamsStringsToObjects = async (errors) => {
  const spots = await Spots.find(
    {
      'cams.0': { $exists: true, $ne: null }
    },
    {
      cams: 1,
    },
  ).lean();
  console.log('Spots with cams: ', spots.length);
  for (const spot of spots) {
    try {
      const cams = spot.cams.map(cam => new Types.ObjectId(cam));
      await Spots.updateOne({ _id: spot._id }, { cams });
      await wait(2000);
      console.log('processed: ', spot._id);
    } catch (error) {
      errors.push({ ...error, spotId: spot._id });
    }
  }
};

const runScript = async () => {
  await initMongoDB();
  const errors = [];
  await convertSpotCamsStringsToObjects(errors);
  if (errors.length > 0) console.log(errors);
  console.log('DONE');
  process.exit(0);
};


runScript();
