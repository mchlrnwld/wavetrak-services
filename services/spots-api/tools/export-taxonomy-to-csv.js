/* eslint-disable */
import mongoose, { Types } from 'mongoose';
import { promises as fs } from 'fs';
import { parse } from 'json2csv';
import { initMongoDB } from '../src/model/dbContext';
import Spots from '../src/model/SpotModel';
import { fetchTaxonomyJSON } from '../src/common/taxonomy';

const spotBreadcrumbs = (taxonomy) => {
  const parts = {};
  taxonomy.forEach(tax => {
    if (tax.category === 'geonames' && tax.geonames.name !== 'Earth') parts[tax.geonames.fcodeName] = tax.geonames.name;
    if (tax.category === 'surfline') parts[tax.type] = tax.name;
  });
  return parts;
};

const wait = async (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

const exportTaxonomyToCSV = async (errors) => {
  const spots = await Spots.find({ status: "PUBLISHED" }, { _id: 1, name: 1 }).lean();

  console.log('Spots: ', spots.length);

  let myCSVData = [];
  for (const spot of spots) {
    try {
      const spotTaxonomy = await fetchTaxonomyJSON('spot', spot._id);
      const breadcrumbs = spotBreadcrumbs(spotTaxonomy.in);
      myCSVData.push({ _id: spot._id, 'spot name': spot.name, ...breadcrumbs });
      await wait(800);
      console.log(`Processed ${spot.name}: `, spot._id);
    } catch (error) {
      console.log('error: ', error);
      errors.push({ ...error, spotId: spot._id });
    }
  }
  const csv = parse(myCSVData);
  console.log('Writing file to: ', __dirname + '/spot-taxonomy.csv');
  await fs.writeFile(__dirname + '/spot-taxonomy.csv', csv);
};

const runScript = async () => {
  await initMongoDB();
  const errors = [];
  await exportTaxonomyToCSV(errors);
  if (errors.length > 0) console.log(errors);
  console.log('DONE');
  process.exit(0);
};

runScript();
