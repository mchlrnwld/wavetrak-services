/* eslint-disable */
import csvToJson from 'csvtojson';
import { initMongoDB } from '../../src/model/dbContext';
import Spots from '../../src/model/SpotModel';

const errors = [];

const invalidSpots = [];

const wait = async (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const isNumberRating = (rating) => {
  const parsedRating = parseInt(rating);
  return isNaN(parsedRating) ? null : parsedRating;
};

const abilityLevelTypeDefs = ['BEGINNER', 'INTERMEDIATE', 'ADVANCED', 'PRO'];
const boardTypeDefs = [
  'SHORTBOARD',
  'FISH',
  'FUNBOARD',
  'LONGBOARD',
  'GUN',
  'TOW',
  'SUP',
  'FOIL',
  'SKIMMING',
  'BODYBOARD',
  'BODYSURFING',
  'KITEBOARD',
];
const tidesTypeDefs = ['Low', 'Medium_Low', 'Medium', 'Medium_High', 'High'];
const seasonsTypeDefs = ['Spring', 'Summer', 'Autumn', 'Winter'];
const breakTypeDefs = [
  'Beach_Break',
  'Canyon',
  'Jetty',
  'Offshore',
  'Pier',
  'Point',
  'Reef',
  'Slab',
  'Lefts',
  'Rights',
];
const bottomTypeDefs = ['Coral', 'Lava', 'Rock', 'Sand'];

const getTravelDetailsFromCSV = () =>
  new Promise((resolve, reject) => {
    const travelDetails = [];
    csvToJson({
      noheader: false,
      headers: [
        'Spot (just for reference)',
        'SLSpot ID',
        'Spot Description',
        'Ability Level DD',
        'Ability Level Description',
        'Access Description',
        'Best Season Description',
        'Best Season DD',
        'Best Surf Height Description',
        'Best Swell Direction Description',
        'Best Tides Description',
        'Best Tides DD',
        'Best Wind Direction Description',
        'Board Type DD',
        'Bottom Description',
        'Bottom DD',
        'Break Type DD',
        'Crowd Factor Description',
        'Crowd Factor DD',
        'Hazards Description',
        'Local Vibe Description',
        'Local Vibe DD',
        'Shoulder Burn Description',
        'Shoulder Burn DD',
        'Spot Rating Description',
        'Spot Rating DD',
        'Water Quality Description',
        'Water Quality DD',
      ],
    })
      .fromFile(`${__dirname}/missing-travel-details.csv`)
      .on('data', (details) => {
        travelDetails.push(details.toString('utf8'));
      })
      .on('done', () => {
        resolve(travelDetails);
      })
      .on('error', (err) => {
        console.log(err);
        reject(err);
      });
  });

const findOrNone = (value, typeDefs, spotId, field) => {
  if (typeDefs.indexOf(value) > -1) {
    return true;
  } else {
    console.log(`Invalid spot entry (${spotId}): `, value, field);
    invalidSpots.push({ field, value, spotId });
    return false;
  }
};

const splitAndFilter = (spot, field, typeDefs, uppercase = false) =>
  spot[field]
    .split('|')
    .map((value) => {
      const modValue = value.replace(/ /g, '_');
      if (uppercase) return modValue.toUpperCase();
      return modValue;
    })
    .filter((item) => findOrNone(item, typeDefs, spot['SLSpot ID']), field);

const importedTravelDetailsToMongo = async (errors) => {
  const spotTravelDetails = await getTravelDetailsFromCSV();

  for (const spot of spotTravelDetails) {
    const spotJSON = JSON.parse(spot);

    try {
      const abilityLevels = splitAndFilter(
        spotJSON,
        'Ability Level DD',
        abilityLevelTypeDefs,
        true,
      );
      const bestSeasonEnum = splitAndFilter(spotJSON, 'Best Season DD', seasonsTypeDefs);
      const bestTideEnum = splitAndFilter(spotJSON, 'Best Tides DD', tidesTypeDefs);
      const bottomEnum = splitAndFilter(spotJSON, 'Bottom DD', bottomTypeDefs);
      const breakType = splitAndFilter(spotJSON, 'Break Type DD', breakTypeDefs);
      const boardTypes = splitAndFilter(spotJSON, 'Board Type DD', boardTypeDefs, true);

      const travelDetails = {
        travelId: spotJSON.travelId,
        abilityLevels: {
          description: spotJSON['Ability Level Description'],
        },
        access: spotJSON['Access Description'],
        best: {
          season: {
            description: spotJSON['Best Season Description'],
            value: bestSeasonEnum,
          },
          tide: {
            description: spotJSON['Best Tides Description'],
            value: bestTideEnum,
          },
          size: {
            description: spotJSON['Best Surf Height Description'],
          },
          windDirection: {
            description: spotJSON['Best Wind Direction Description'],
          },
          swellDirection: {
            description: spotJSON['Best Swell Direction Description'],
          },
        },
        bottom: {
          description: spotJSON['Bottom Description'],
          value: bottomEnum,
        },
        breakType: breakType,
        crowdFactor: {
          description: spotJSON['Crowd Factor Description'],
          rating: isNumberRating(spotJSON['Crowd Factor DD']),
        },
        description: spotJSON['Spot Description'],
        hazards: spotJSON['Hazards Description'],
        localVibe: {
          description: spotJSON['Local Vibe Description'],
          rating: isNumberRating(spotJSON['Local Vibe DD']),
        },
        shoulderBurn: {
          description: spotJSON['Shoulder Burn Description'],
          rating: isNumberRating(spotJSON['Shoulder Burn DD']),
        },
        relatedArticleId: null,
        spotRating: {
          description: spotJSON['Spot Rating Description'],
          rating: isNumberRating(spotJSON['Spot Rating DD']),
        },
        waterQuality: {
          description: spotJSON['Water Quality Description'],
          rating: isNumberRating(spotJSON['Water Quality DD']),
        },
        status: 'PUBLISHED',
      };

      await Spots.updateOne(
        { _id: spotJSON['SLSpot ID'] },
        {
          boardTypes,
          abilityLevels,
          travelDetails,
        },
      );
      await wait(500);
      console.log('Processed spot ID: ', spotJSON['SLSpot ID']);
    } catch (error) {
      errors.push({ error, spotId: spotJSON['SLSpot ID'] });
    }
  }
};

const runScript = async () => {
  await initMongoDB();
  await importedTravelDetailsToMongo(errors);
  if (errors.length > 0) console.log('WITH ERRORS: ', errors);
  if (invalidSpots.length > 0) console.log('WITH INVALID ENTRIES: ', invalidSpots);
  console.log('DONE');
  process.exit(0);
};

runScript();
