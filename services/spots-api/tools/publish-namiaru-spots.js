/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import axios from 'axios';
import fs from 'fs';
import parse from 'csv-parse/lib/sync';

import config from '../src/config';

const wait = async (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

const publishNamiaruSpots = async (errors) => {
  const content = fs.readFileSync('/Users/jmonson/Downloads/nami_geoname_subid.csv');
  const spotsToPublish = parse(content);

  for (const spot of spotsToPublish) {
    const [spotId, spotName, geoname, subId] = spot;
    try {
      await axios.patch(`${config.SL_API}spots/${spotId}`, {
        geoname: parseInt(geoname, 10),
        subregion: subId,
        status: 'PUBLISHED',
      });

      await wait(250);
      console.log(`Processed ${spotName}`);
    } catch (error) {
      console.log('error: ', error);
      errors.push({
        error: error.message,
        spotId,
        spotName,
        geoname,
        subId,
      });
    }
  }
};

const runScript = async () => {
  const errors = [];
  await publishNamiaruSpots(errors);

  if (errors.length > 0) {
    console.log(errors);
    fs.writeFileSync('./script-errors.json', JSON.stringify(errors, null, 2));
  }

  console.log('DONE');
  process.exit(0);
};

runScript();
