variable "company" {
  type        = string
  description = "The brand that this service will be used for. Wavetrak can be used for cross-brand services"
  default     = "wt"
}

variable "application" {
  type        = string
  description = "Name of the service"
  default     = "weather-stations-api"
}

variable "environment" {
  type        = string
  description = "The environment in which the infrastructure is being created"
}

variable "default_vpc" {
  type        = string
  description = "The default VPC for the service"
}

variable "ecs_cluster" {
  type        = string
  description = "The ECS cluster that the service will sit on"
}

variable "alb_listener_arn" {
  type        = string
  description = "ARN for the ALB listener"
}

variable "service_lb_healthcheck_path" {
  type        = string
  description = "The path that the service healthcheck responds on"
  default     = "/health"
}

variable "service_td_count" {
  type        = string
  description = "The number of task definitions for the service"
}

variable "service_port" {
  type        = string
  description = "The internal port that the container listens on"
  default     = "8080"
}

variable "service_alb_priority" {
  type        = string
  description = "The priority that the service has on the ALB in case of matching service_lb_rules"
  default     = "770"
}

variable "iam_role_arn" {
  type        = string
  description = "ARN for the IAM role"
}

variable "load_balancer_arn" {
  type        = string
  description = "ARN for the load balancer"
}

variable "auto_scaling_enabled" {
  type        = string
  description = "Boolean determining if auto-scaling is enabled"
  default     = "false"
}

variable "auto_scaling_scale_by" {
  type        = string
  description = "Metric which the service should use to trigger scaling"
  default     = "alb_request_count"
}

variable "auto_scaling_min_size" {
  type        = string
  description = "Minimum number of tasks for an auto-scaling service"
  default     = ""
}

variable "auto_scaling_max_size" {
  type        = string
  description = "Maximum number of tasks for an auto-scaling service"
  default     = ""
}

variable "auto_scaling_target_value" {
  type        = string
  description = "The target value for auto-scaling"
  default     = ""
}

variable "propagate_tags" {
  type        = string
  description = "Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid values are SERVICE and TASK_DEFINITION"
  default     = null
}
