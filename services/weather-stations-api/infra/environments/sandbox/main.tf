provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "weather-stations-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "weather_stations_api" {
  source = "../../"

  environment       = "sandbox"
  default_vpc       = "vpc-981887fd"
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
  service_td_count  = "1"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac"
  ecs_cluster       = "sl-core-svc-sbox"
  propagate_tags    = true

  auto_scaling_enabled = false
}
