locals {
  dns_zone_name = "${var.environment}.surfline.com"
  dns_name      = "${var.application}.${local.dns_zone_name}"

  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

data "aws_route53_zone" "selected" {
  name = "${local.dns_zone_name}."
}

module "weather-stations-api" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  service_name                = var.application
  service_lb_rules            = local.service_lb_rules
  service_lb_healthcheck_path = var.service_lb_healthcheck_path
  service_td_name             = "${var.company}-core-svc-${var.environment}-${var.application}"
  service_td_count            = var.service_td_count
  service_port                = var.service_port
  service_td_container_name   = var.application
  service_alb_priority        = var.service_alb_priority
  alb_listener                = var.alb_listener_arn
  iam_role_arn                = var.iam_role_arn
  dns_name                    = local.dns_name
  dns_zone_id                 = data.aws_route53_zone.selected.zone_id
  load_balancer_arn           = var.load_balancer_arn
  company                     = var.company
  application                 = var.application
  environment                 = var.environment
  default_vpc                 = var.default_vpc
  ecs_cluster                 = "sl-core-svc-${var.environment}"
  propagate_tags              = var.propagate_tags

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
}

