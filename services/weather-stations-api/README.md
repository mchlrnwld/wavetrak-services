# Weather stations API

- [Developing](#developing)
  - [Posting Data](#posting-data)
- [Deploying and Accessing Endpoints](#deploying-and-acessing-endpoints)

The Weather Stations API supports an admin user being able to setup and configure weather stations.

## Developing

To start developing

* copy `.env.sample` to `.env` and adjust environment variables to your local computer
* run `npm run build:dist` to compile the app 
* run `npm run start` to start the development server
* `npm run test` will run unit and pact contract tests
* `npm run lint` will run the linter

### Posting Data

You must have your admin cookie set we suggest logging in via the UI and session an `access_token` cookie to the value created once logged in in sandbox.

`POST /api/weather-stations/`

| Body                 | Description                                                                                                                             | Required | type                                |
| -------------------- | --------------------------------------------------------------------------------------------------------------------------------------- | ---------| ----------------------------------- |
| `station_name`       | The name of the station you wish to create                                                                                              | `true`   | `string`                            | 
| `wx_data_url`        | The url of the device in weatherlink                                                                                                    | `false`  | `string`                            |
| `latitude`           | The latitude of the weather-station (between -90 and 90)                                                                                | `true`   | `number`                            |
| `longitude`          | The longitude of the weather-station                                                                                                    | `true`   | `number`                            |
| `anemometer_height`  | The anemometer height                                                                                                                   | `false`  | `number`                            |
| `stationNameTrimmed` | The trimmed/ shortened station name                                                                                                     | `false`  | `string`                            |
| `data_logger`        | The weather station data logger                                                                                                         | `false`  | `string`                            |
| `internal_ip`        | The internal IP address of the weather station / sensor                                                                                 | `true`   | `string`                            |
| `external_ip`        | The external IP address of the weather station / sensor                                                                                 | `true`   | `string`                            |
| `port`               | The port the sensor is running on                                                                                                       | `true`   |`number`                             |
| `status`             | The status of the weather-station                                                                                                       | `true`   | `string` UP, DOWN, INVALID, DELETED |
| `ownership`          | The status of the weather-station                                                                                                       | `true`   | `string` owned, shared              |
| `sensorModel`        | The sensors model                                                                                                                       | `false`  | `string`                            |
| `notes`              | Any notes to be left about the weather-station                                                                                          | `false`  | `string`                            |
| `macAddress`         | The physical Mac Address of the sensor / station in hexadecimal digits, separated by - e.g. 9F-9F-9F-9F-9F-9F. (duplicates not allowed) | `true`   | `string` or `null`                  |


`PATCH /api/weather-stations/{station-id}` - supports same data as above

`DELETE /api/weather-stations/{station-id}`


Additionally there are get endpoints for retrieving the data.

`GET /api/weather-stations/`
`GET /api/weather-stations/{station-id}`
`GET /api/weather-stations/{station-id}/version/{version-number}`


## Deploying and Accessing Endpoints

The [deploy-service-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/) can be used to deploy the sessions api. Select `weather-stations-api` under the dropdown.

From inside the vpn the service can be accessed via `http://weather-stations-api.[env].surfline.com`.


## Data Sources and External Services

* The weather stations API stores data in the `weatherStations` Mongo collection
* The weather stations API stores a history of versions for auditing who edited the data this is in the `weatherStationsVersions` Mongo collection
