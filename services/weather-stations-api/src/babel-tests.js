require('dotenv').config({ silent: true, path: '.env.test' });
require('@babel/register');

jest.setTimeout(60000);
