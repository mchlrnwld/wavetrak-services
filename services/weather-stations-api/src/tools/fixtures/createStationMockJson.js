import fs from 'fs/promises';
import path from 'path';

// create a buffer to mock response the of S3 getObject()
const createMockJson = async () => {
  const file = path.resolve(__dirname, './mockStationList.csv');
  const csvStationsFile = await fs.readFile(file, () => {});
  const json = {
    AcceptRanges: 'bytes',
    LastModified: '2021-09-21T19:52:06.000Z',
    ContentLength: 8181,
    ETag: '"c93ef0cb3d340fdce69fc7faf118adbd"',
    ContentType: 'text/csv',
    Metadata: {},
    Body: csvStationsFile,
  };
  return json;
};

export default createMockJson;
