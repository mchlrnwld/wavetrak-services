/* eslint-disable max-len */
import csv from 'csvtojson';
import { initMongo, setupLogsene } from '@surfline/services-common';
import AWS from 'aws-sdk';
import mongoose from 'mongoose';
import config from '../../../config';
import WeatherStationModel, { createWeatherStation } from '../../../model/weatherStationsSchema';
import { createWeatherStationTransformer } from '../../../handlers/transformer';

const {
  WEATHER_STATIONS_BUCKET,
  WEATHER_STATIONS_FILE,
  MONGO_CONNECTION_STRING_KBYG,
  USER_NAME,
} = config;
const renameKey = (obj, oldKey, newKey) => {
  const updateObj = { ...obj };
  updateObj[newKey] = updateObj[oldKey];
  delete updateObj[oldKey];
  return updateObj;
};

const processStation = async (obj) => {
  let stationObject = { ...obj };
  stationObject = await renameKey(stationObject, 'spot_name', 'station_name');
  delete stationObject.spotid;

  // check if the station exists
  const exists = await WeatherStationModel.exists({ station_name: stationObject.station_name });
  if (!exists) {
    // setting required fields to a certain value for insertion into the DB (this will need updating/removing if the csv is ever updated to include these values)
    stationObject.status = 'INVALID';
    stationObject.ownership = 'owned';

    const weatherStation = createWeatherStationTransformer(stationObject, USER_NAME);

    await createWeatherStation(weatherStation);
  }
};

const sortAndImportStations = async (stationArray) => {
  await Promise.all(stationArray.map(async (obj) => {
    await processStation(obj);
  }));
};

const fetchWeatherStationData = async () => {
  const s3 = new AWS.S3();
  const s3Params = {
    Bucket: WEATHER_STATIONS_BUCKET,
    Key: WEATHER_STATIONS_FILE,
  };
  let response;
  try {
    response = await s3.getObject(s3Params).promise();
    const res = await csv().fromString(response.Body.toString('utf-8'));
    return res;
  } catch (err) {
    throw new Error('Could not retrieve weather stations file from S3');
  }
};

const importWeatherStationsToMongodb = async () => {
  const weatherStations = await fetchWeatherStationData();
  return sortAndImportStations(weatherStations);
};

const runScript = async () => {
  try {
    setupLogsene('dummykey');
    await initMongo(mongoose, MONGO_CONNECTION_STRING_KBYG, 'weather-stations-api');
    await importWeatherStationsToMongodb();

    console.log('script done');
    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

export default runScript;
