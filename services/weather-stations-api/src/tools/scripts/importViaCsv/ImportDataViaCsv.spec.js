import mongoose from 'mongoose';
import AWS from 'aws-sdk';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from '../../../model/weatherStationsSchema';
import runScript from './ImportDataviaCsv';
import createMockJson from '../../fixtures/createStationMockJson';

jest.mock('aws-sdk');
jest.mock('@surfline/services-common');

let mongoServer;
let MOCK_S3_GET;
let MOCK_S3_GET_FAIL;
let mockExit;

describe('importData, ', () => {
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);
  });
  afterAll(async () => {
    await mongoServer.stop();
    mongoServer = null;
    mongoose.disconnect();
  });

  afterEach(async () => {
    jest.clearAllMocks();
    await WeatherStationModel.deleteMany({});
  });

  test('Fetches the weather stations CSV file from the S3 bucket, storing the data in the db', async () => {
    const s3GetObjectResponse = await createMockJson();
    MOCK_S3_GET = jest.fn(() => ({
      promise: () => Promise.resolve(s3GetObjectResponse),
    }));
    jest.spyOn(AWS, 'S3').mockReturnValue({ getObject: MOCK_S3_GET });
    mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});

    await runScript();
    expect(MOCK_S3_GET).toHaveBeenCalled();
    const dbResponse = await WeatherStationModel.find();
    const dbTrestles = await WeatherStationModel.find({ station_name: 'Lower Trestles' });
    const dbOldMans = await WeatherStationModel.find({ station_name: 'Old Mans at San Onofre' });
    const dbHB = await WeatherStationModel.find({ station_name: 'Huntington State Beach' });
    expect(dbResponse).toHaveLength(3);
    expect(dbTrestles).toHaveLength(1);
    expect(dbOldMans).toHaveLength(1);
    expect(dbHB).toHaveLength(1);
    expect(mockExit).toHaveBeenCalledWith(0);
  });

  test('Fails to fetch the weather stations CSV file from the S3 bucket', async () => {
    MOCK_S3_GET_FAIL = jest.fn(() => ({
      promise: () => Promise.resolve({}),
    }));
    jest.spyOn(AWS, 'S3').mockReturnValue({ getObject: MOCK_S3_GET_FAIL });
    const consoleSpy = jest.spyOn(console, 'log');
    console.log('Error: Could not retrieve weather stations file from S3');

    await runScript();
    const dbResponse2 = await WeatherStationModel.find();
    expect(consoleSpy).toHaveBeenCalledWith('Error: Could not retrieve weather stations file from S3');
    expect(dbResponse2).toHaveLength(0);
    expect(mockExit).toHaveBeenCalledWith(1);
  });
});
