import mongoose from 'mongoose';
import nock from 'nock';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from '../../../model/weatherStationsSchema';
import runScript from './importViaWeatherlink';
import WeatherStationsVersionsModel from '../../../model/weatherStationsVersionsSchema';
import * as scriptOptions from './scriptOptions';

jest.mock('@surfline/services-common');
jest.mock('./validStations.json', () => ({
  __esModule: true,
  default: [119308, 99880, 116061],
}));
let mongoServer;

const MOCK_STATIONS_TO_UPDATE = [
  {
    station_id: 119308,
    station_name: 'Bantham',
    gateway_id: 7427597,
    gateway_id_hex: '001D0A71560D',
    product_number: '6100USB',
    username: 'test',
    user_email: 'test@test.com',
    company_name: '',
    active: true,
    private: false,
    recording_interval: 15,
    firmware_version: null,
    registered_date: 1628248335,
    time_zone: 'Europe/London',
    city: '',
    region: 'England',
    country: 'UK',
    latitude: -1.3433591,
    longitude: -80.74369,
    elevation: 285.793,
  },
  {
    station_id: 99880,
    station_name: 'Huntington State Beach',
    gateway_id: 7409062,
    gateway_id_hex: '001D0A710DA6',
    product_number: '6100',
    username: 'surfline',
    user_email: 'camera@surfline.com',
    company_name: '',
    active: true,
    private: true,
    recording_interval: 15,
    firmware_version: null,
    registered_date: 1603481162,
    time_zone: 'America/Los_Angeles',
    city: 'Huntington Beach',
    region: 'CA',
    country: 'United States of America',
    latitude: -1.3433591,
    longitude: -80.74369,
    elevation: 15.419495,
  },
];

const MOCK_ADDITIONAL_NON_VALID_STATIONS = [
  {
    station_id: 121972,
    station_name: 'San Clemente Pier Southside ',
    gateway_id: 7426321,
    gateway_id_hex: '001D0A715111',
    product_number: '6100',
    username: 'surfline',
    user_email: 'camera@surfline.com',
    company_name: '',
    active: true,
    private: false,
    recording_interval: 15,
    firmware_version: null,
    registered_date: 1632157202,
    time_zone: 'America/Los_Angeles',
    city: 'San Clemente',
    region: 'CA',
    country: 'United States of America',
    latitude: -1.3433591,
    longitude: -80.74369,
    elevation: 31.369,
  },
];

const MOCK_DATE = new Date(1639045937633);

const createExpectedStationQuery = (id, name, type) => ({
  location: {
    coordinates: [-1.3433591, -80.74369],
    type: 'Point',
  },
  _id: id,
  station_name: name,
  modifiedDate: MOCK_DATE,
  installationDate: MOCK_DATE,
  version: 0,
  updatedBy: 'Weatherlink station import job',
  macAddress: null,
  ownership: type,
  status: 'INVALID',
  sensors: [],
  __v: 0,
});


const createExpectedStationVersionQuery = (id, stationId, name, type) => ({
  location: {
    coordinates: [-1.3433591, -80.74369],
    type: 'Point',
  },
  _id: id,
  stationId,
  station_name: name,
  modifiedDate: MOCK_DATE,
  installationDate: MOCK_DATE,
  version: 0,
  updatedBy: 'Weatherlink station import job',
  macAddress: null,
  ownership: type,
  status: 'INVALID',
  __v: 0,
});


describe('importData, ', () => {
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);
  });
  afterAll(async () => {
    await mongoServer.stop();
    mongoServer = null;
    mongoose.disconnect();
  });

  afterEach(async () => {
    jest.clearAllMocks();
    await WeatherStationModel.deleteMany({});
    await WeatherStationsVersionsModel.deleteMany({});
  });

  test('Should only query stations if RUN_MONGO_WRITES are set to true', async () => {
    scriptOptions.RUN_MONGO_WRITES = false;

    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
    jest.spyOn(Date, 'now').mockImplementation(() => MOCK_DATE);

    nock('https://api.weatherlink.com')
      .get(/\/v2\/stations/)
      .reply(200, {
        stations: [...MOCK_STATIONS_TO_UPDATE, ...MOCK_ADDITIONAL_NON_VALID_STATIONS],
      });

    await runScript();

    const dbCreateResponse = await WeatherStationModel.find({});

    expect(dbCreateResponse).toHaveLength(0);
    expect(mockExit).toHaveBeenCalledWith(0);
  });

  test('Fetches weatherlink stations and creates internal weather stations using the data correctly ', async () => {
    scriptOptions.RUN_MONGO_WRITES = true;

    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
    jest.spyOn(Date, 'now').mockImplementation(() => MOCK_DATE);

    nock('https://api.weatherlink.com')
      .get(/\/v2\/stations/)
      .reply(200, {
        stations: [
          ...MOCK_STATIONS_TO_UPDATE,
          ...MOCK_ADDITIONAL_NON_VALID_STATIONS,
        ],
      });

    await runScript();

    const dbCreateResponse = await WeatherStationModel.find({});
    const dbCreateTestDataBantham = await WeatherStationModel.find(createExpectedStationQuery(dbCreateResponse[0]._id, 'Bantham', 'shared')).exec();
    const dbCreateTestDataHb = await WeatherStationModel.find(createExpectedStationQuery(dbCreateResponse[1]._id, 'Huntington State Beach', 'owned')).exec();

    const dbVersionResponse = await WeatherStationsVersionsModel.find({});
    const dbVersionTestDataBantham = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(dbVersionResponse[0]._id, dbCreateResponse[0]._id, 'Bantham', 'shared')).exec();
    const initialDbVersionTestDataHb = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(dbVersionResponse[1]._id, dbCreateResponse[1]._id, 'Huntington State Beach', 'owned')).exec();

    expect(dbCreateResponse).toHaveLength(2);
    expect(dbCreateTestDataBantham).toHaveLength(1);
    expect(dbCreateTestDataHb).toHaveLength(1);

    expect(dbVersionResponse).toHaveLength(2);
    expect(dbVersionTestDataBantham).toHaveLength(1);
    expect(initialDbVersionTestDataHb).toHaveLength(1);

    expect(mockExit).toHaveBeenCalledWith(0);
  });

  test('Should handle only partial inserted / created stations (if duplicates are found)', async () => {
    scriptOptions.RUN_MONGO_WRITES = true;

    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
    jest.spyOn(Date, 'now').mockImplementation(() => MOCK_DATE);

    nock('https://api.weatherlink.com')
      .get(/\/v2\/stations/)
      .reply(200, {
        stations: [...MOCK_STATIONS_TO_UPDATE, ...MOCK_ADDITIONAL_NON_VALID_STATIONS],
      });

    await runScript();

    const initialDbCreateResponse = await WeatherStationModel.find({});
    const initialDbCreateTestDataBantham = await WeatherStationModel.find(createExpectedStationQuery(initialDbCreateResponse[0]._id, 'Bantham', 'shared')).exec();
    const initialDbCreateTestDataHb = await WeatherStationModel.find(createExpectedStationQuery(initialDbCreateResponse[1]._id, 'Huntington State Beach', 'owned')).exec();

    const intiialDbVersionResponse = await WeatherStationsVersionsModel.find({});
    const initialDbVersionTestDataBantham = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(intiialDbVersionResponse[0]._id, initialDbCreateResponse[0]._id, 'Bantham', 'shared')).exec();
    const initialDbVersionTestDataHb = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(intiialDbVersionResponse[1]._id, initialDbCreateResponse[1]._id, 'Huntington State Beach', 'owned')).exec();

    expect(initialDbCreateResponse).toHaveLength(2);
    expect(initialDbCreateTestDataBantham).toHaveLength(1);
    expect(initialDbCreateTestDataHb).toHaveLength(1);

    expect(intiialDbVersionResponse).toHaveLength(2);
    expect(initialDbVersionTestDataBantham).toHaveLength(1);
    expect(initialDbVersionTestDataHb).toHaveLength(1);

    expect(mockExit).toHaveBeenCalledWith(0);

    jest.clearAllMocks();

    // Run script again with additional valid station

    const MOCK_ADDITIONAL_STATION = {
      station_id: 116061,
      station_name: 'Nags head fishing pier',
      gateway_id: 7426784,
      gateway_id_hex: '001D0A7152E0',
      product_number: '6100USB',
      username: 'surfline',
      user_email: 'camera@surfline.com',
      company_name: '',
      active: true,
      private: true,
      recording_interval: 15,
      firmware_version: null,
      registered_date: 1623949990,
      time_zone: 'America/New_York',
      city: 'Nags Head',
      region: 'NC',
      country: 'United States of America',
      latitude: -1.3433591,
      longitude: -80.74369,
      elevation: 0,
    };

    nock('https://api.weatherlink.com')
      .get(/\/v2\/stations/)
      .reply(200, {
        stations: [
          ...MOCK_STATIONS_TO_UPDATE,
          ...MOCK_ADDITIONAL_NON_VALID_STATIONS,
          MOCK_ADDITIONAL_STATION,
        ],
      });

    await runScript();

    const dbCreateResponse = await WeatherStationModel.find({});
    const dbCreateTestDataBantham = await WeatherStationModel.find(createExpectedStationQuery(dbCreateResponse[0]._id, 'Bantham', 'shared')).exec();
    const dbCreateTestDataHb = await WeatherStationModel.find(createExpectedStationQuery(dbCreateResponse[1]._id, 'Huntington State Beach', 'owned')).exec();
    const dbCreateTestDataNags = await WeatherStationModel.find(createExpectedStationQuery(dbCreateResponse[2]._id, 'Nags head fishing pier', 'owned')).exec();

    const dbVersionResponse = await WeatherStationsVersionsModel.find({});
    const dbVersionTestDataBantham = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(dbVersionResponse[0]._id, dbCreateResponse[0]._id, 'Bantham', 'shared')).exec();
    const dbVersionTestDataHb = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(dbVersionResponse[1]._id, dbCreateResponse[1]._id, 'Huntington State Beach', 'owned')).exec();
    const dbVersionTestDataNags = await WeatherStationsVersionsModel.find(createExpectedStationVersionQuery(dbVersionResponse[2]._id, dbCreateResponse[2]._id, 'Nags head fishing pier', 'owned')).exec();

    expect(dbCreateResponse).toHaveLength(3);
    expect(dbCreateTestDataBantham).toHaveLength(1);
    expect(dbCreateTestDataHb).toHaveLength(1);
    expect(dbCreateTestDataNags).toHaveLength(1);

    expect(dbVersionResponse).toHaveLength(3);
    expect(dbVersionTestDataBantham).toHaveLength(1);
    expect(dbVersionTestDataHb).toHaveLength(1);
    expect(dbVersionTestDataNags).toHaveLength(1);

    expect(mockExit).toHaveBeenCalledWith(0);
  });


  test('Should catch error when no stations are found from weatherlink', async () => {
    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
    const consoleErrorSpy = jest.spyOn(console, 'error');
    nock('https://api.weatherlink.com')
      .get(/\/v2\/stations/)
      .reply(200, { stations: [] });

    await runScript();

    expect(consoleErrorSpy).toHaveBeenCalledWith('FAILED: ', new Error('No stations found'));
    expect(mockExit).toHaveBeenCalledWith(1);
  });
});
