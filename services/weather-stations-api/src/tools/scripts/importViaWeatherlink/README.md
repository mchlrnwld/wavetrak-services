# Import stations via weatherlink

This is a script that can be used to add weather stations into our systmes from weatherlink.


## How to use this script:

*Step 1:*

Get a list of weather station IDs you want to populate (In the script we filter out only stations we want given we have many stations hooked up in weatherlink that we dont consider "valid"). To find these IDs you must query the weatherlink GET `/stations` found here https://weatherlink.github.io/v2-api/api-reference.

you can use the `RUN_MONGO_WRITES` const in the script to allow for you to just get the list of stations (which will be logged out in the console) without any writes to the DB taking place. You can then confirm which IDs we consider "valid".


*Step 2:*

Update the list of valid stations found in the  `validStations.json` file to include any new stations you want to add. Note: the script will ignore any stations already present in the database and only add "new" stations so you dont need to worry about removing the old ones.


*Step 3:*

To run this script you can use `npm run tools:ImportDataViaWeatherlink`


*Step 4:*

Double check the console output (which contains a JSON dump of the stations and their relevant versions) to make sure your data is correct and that all relevant stations have been added.


*Step 5:*

Log into mongo UI here and make sure you see the expected stations in the `weatherstations` and `weatherstationversions` collections found in the KBYG group.