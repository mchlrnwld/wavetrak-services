import { initMongo, setupLogsene } from '@surfline/services-common';
import mongoose from 'mongoose';
import { transformAndValidateLocation } from '../../../handlers/transformer';
import fetchStations from '../../../helperFile/fetchWeatherLinkStations';
import WeatherStationModel from '../../../model/weatherStationsSchema';
import WeatherStationsVersionsModel from '../../../model/weatherStationsVersionsSchema';
import config from '../../../config';
import { renameProperty } from '../../../helperFile/weatherStationHelperFunctions';
import validStations from './validStations.json';
import { RUN_MONGO_WRITES } from './scriptOptions';

const filterValidStations = (stations) => stations.filter((station) => (
  validStations.indexOf(station.station_id) >= 0
));

const transformStation = (currentDate) => (station) => {
  const updatedBy = 'Weatherlink station import job';

  return {
    _id: new mongoose.Types.ObjectId(),
    weatherLinkId: station.station_id,
    station_name: station.station_name,
    createDate: currentDate,
    modifiedDate: currentDate,
    installationDate: currentDate,
    version: 0,
    updatedBy,
    location: transformAndValidateLocation(station.latitude, station.longitude),
    ownership: station.username === 'surfline' ? 'owned' : 'shared',
    status: 'INVALID',
  };
};

const transformStationForVersionsCollection = (station) => {
  // eslint-disable-next-line no-underscore-dangle
  let stationVersion = { ...station._doc };

  stationVersion = renameProperty(stationVersion, '_id', 'stationId');
  stationVersion._id = new mongoose.Types.ObjectId();

  // Delete sensors Obj before we save version history as this history is stored elswhere
  delete stationVersion.sensors;

  return stationVersion;
};

const createStations = async (stations, currentDate) => {
  let createdStations;

  if (stations.length) {
    try {
      const transformedStations = stations.map(transformStation(currentDate));
      createdStations = await WeatherStationModel.insertMany(
        transformedStations,
        { ordered: false },
      );
    } catch (err) {
      // If only some stations where created set the createdStations array to the ones we inserted
      if (err.insertedDocs?.length > 0) {
        createdStations = err.insertedDocs;
        console.log('Failed to create some stations:', JSON.stringify(err));
      } else {
        throw err;
      }
    }

    return createdStations;
  }

  throw new Error('No stations found');
};

const createStationVersions = async (stations) => {
  if (stations.length) {
    const stationVersions = stations.map(transformStationForVersionsCollection);

    return WeatherStationsVersionsModel.insertMany(stationVersions);
  }

  throw new Error('No stations found');
};

const runScript = async () => {
  console.info('Running station creation job');

  const currentDate = Date.now();

  try {
    await setupLogsene('dummykey');
    // eslint-disable-next-line no-unused-vars
    const [_, { stations }] = await Promise.all([await initMongo(mongoose, config.MONGO_CONNECTION_STRING_KBYG, 'weather-stations-api'), fetchStations()]);
    if (RUN_MONGO_WRITES) {
      const filteredStations = filterValidStations(stations);
      const createdStations = await createStations(filteredStations, currentDate);

      console.log('\n\n\n\nCREATED STATIONS ------\n\n\n\n');
      console.log(JSON.stringify(createdStations));

      const createdStationsVersions = await createStationVersions(createdStations);

      console.log('\n\n\n\nCREATED VERSIONS ------\n\n\n\n');
      console.log(JSON.stringify(createdStationsVersions));
      console.log('\n\n\n\n ---------------------- \n\n\n\n');
    } else {
      console.log('\n\n\n\nNo stations or versions have been created due to RUN_MONGO_WRITES flag\n\n\n\n');

      console.log('\n\n\n\nFULL WEATHERLINK STATIONS LIST ------ \n\n\n\n');
      console.log(JSON.stringify(stations));
      console.log('\n\n\n\n ---------------------- \n\n\n\n');
    }

    console.log('**** Script completed successfully ****');
    process.exit(0);
  } catch (err) {
    console.error('FAILED: ', err);
    process.exit(1);
  }
};


export default runScript;
