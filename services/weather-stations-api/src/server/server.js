import { setupExpress } from '@surfline/services-common';
import trackRequests from '../common/trackRequests';
import { weatherStations } from '../handlers/weatherStations';
import config from '../config';
import log from '../common/logger';
import weatherLinkStations from '../handlers/weatherLinkStations';

const accessControlMiddleware = (req, res, next) => {
  let allowedOrigin = req.header('Origin') || '*';
  // eslint-disable-next-line no-param-reassign
  res.locals.scopes = req.header('X-Auth-AccessScope')?.split(',') || null;
  if (allowedOrigin === 'null' || allowedOrigin === '*') {
    const referer = req.header('referer');
    if (referer) {
      const refererParts = referer.split('/');
      allowedOrigin = `${refererParts[0]}//${refererParts[2]}`;
    }
  }
  res.header('Access-Control-Allow-Origin', allowedOrigin);
  res.header('Access-Control-Request-Headers', 'Origin');
  res.header(
    'Access-Control-Allow-Headers',
    // eslint-disable-next-line max-len
    'Origin, X-Requested-With, Content-Type, Accept, Credentials, X-Auth-ClientId, X-Auth-AnonymousId',
  );
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT, DELETE');
  next();
};

const startApp = () => setupExpress({
  port: config.EXPRESS_PORT || 8081,
  name: 'weather-stations-api',
  log,
  handlers: [
    accessControlMiddleware,
    ['*', trackRequests(log)],
    ['/weather-stations', weatherStations(log)],
    ['/weather-link-stations', weatherLinkStations(log)],
  ],
  allowedMethods: ['GET', 'POST', 'PATCH', 'POST', 'DELETE'],
  bodySizeLimit: '10mb',
});

export default startApp;
