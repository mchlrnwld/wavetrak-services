/* eslint-disable max-len */
import supertest from 'supertest';
import mongoose from 'mongoose';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from './weatherStationsSchema';
import {
  createStationInput,
  mockVersionOutput,
  updateStationInput,
  updateStationOutput,
} from '../fixtures/weatherStationsExpectedData';
import { expectMongoID } from '../helperFile/matchers';
import startApp from '../server/server';

let mongoServer;
let thisServer;
let request;

describe('/weather-stations', () => {
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();

    const mongoUri = await mongoServer.getUri();

    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);

    const { server } = await startApp();
    thisServer = server;
    request = supertest(thisServer);
  });

  afterAll(async () => {
    await thisServer.close();
    await mongoServer.stop();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });


  describe('PATCH', () => {
    let existingStations = [];
    beforeEach(async () => {
      const SECOND_MOCK_STATION = {
        ...createStationInput,
        macAddress: '8C-45-6B-6F-5C-D8',
        station_name: 'Hb pier south',
      };

      const THIRD_MOCK_STATION = {
        ...createStationInput,
        station_name: 'Hb pier north side',
        macAddress: null,
      };

      const FOURTH_MOCK_STATION = {
        ...createStationInput,
        station_name: 'Hb pier test',
      };

      delete FOURTH_MOCK_STATION.macAddress;

      const res = await request.post('/weather-stations/').send(createStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
      const res2 = await request.post('/weather-stations/').send(SECOND_MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
      const res3 = await request.post('/weather-stations/').send(THIRD_MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
      const res4 = await request.post('/weather-stations/').send(FOURTH_MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');

      existingStations.push(res.body, res2.body, res3.body, res4.body);
    });

    afterEach(async () => {
      existingStations = [];
      await WeatherStationModel.deleteMany({});
    });

    describe('Invalid data', () => {
      test('PATCH/ request returns a 400 when an invalid id is given.', async () => {
        const _id = 'non-id';
        const res = await request.patch(`/weather-stations/${_id}`).send({}).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Id: Weather station Id must a string of 24 hex characters');
        expect(res.error).toBeTruthy();
      });

      test('PATCH/ request returns a 400 when there is no existing object with matching station id.', async () => {
        const id = '587fdc0d3b01e6a94510ca69';
        const res = await request.patch(`/weather-stations/${id}`).send({ station_name: 'test' }).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual(`Invalid Parameters: No station with Id: ${id}.`);
        expect(res.error).toBeTruthy();
      });

      test('PATCH/ rejects non array input gracefully', async () => {
        const emptyBody = {};
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(emptyBody).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(400);
        expect(res.body.message).toBe('Insufficient weather Stations data provided to update station');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid status', async () => {
        const MOCK_PATCH_BODY = { status: '' };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');

        expect(res.status).toBe(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: status: status is required');
        expect(res.body.errorHandler).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid ownership value', async () => {
        const MOCK_PATCH_BODY = { ownership: '' };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: ownership: ownership status is required');
        expect(res.body.errorHandler).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid station_name', async () => {
        const MOCK_PATCH_BODY = { station_name: '' };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');

        expect(res.status).toBe(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: station_name: Name is required');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid max latitude', async () => {
        const MOCK_PATCH_BODY = { latitude: 91 };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Latitude value 91 not in range [-90, +90].');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid min latitude', async () => {
        const MOCK_PATCH_BODY = { latitude: -91 };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Latitude value -91 not in range [-90, +90].');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with an invalid macAddress.', async () => {
        const MOCK_PATCH_BODY = { macAddress: 1234 };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');

        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: macAddress: MAC address should be a unique 12 digit hexadecimal number, separated by -, in 6 groups of 2, e.g. `9C-35-5B-5F-4C-D7`.');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid url as a number.', async () => {
        const MOCK_PATCH_BODY = { wx_data_url: 95 };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid url as a string but not in matching format.', async () => {
        const MOCK_PATCH_BODY = { wx_data_url: 'www.surf.com' };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid url as a empty string', async () => {
        const MOCK_PATCH_BODY = { wx_data_url: '' };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError when inserting a new weather station with invalid weatherLinkId as a string', async () => {
        const MOCK_PATCH_BODY = { weatherLinkId: 'testing' };

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: weatherLinkId: Cast to Number failed for value "testing" (type string) at path "weatherLinkId"');
        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError correctly when sensor is not in correct format', async () => {
        const MOCK_PATCH_BODY = {
          sensors: [
            {
              test: 'test',
            },
          ],
        };
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: sensors.0.type: Type is required, sensors.0.weatherLinkId: Weatherlink ID is required');

        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError correctly when multiple issues were found', async () => {
        const MOCK_PATCH_BODY = { wx_data_url: 'test', macAddress: '1234' };
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/, macAddress: MAC address should be a unique 12 digit hexadecimal number, separated by -, in 6 groups of 2, e.g. `9C-35-5B-5F-4C-D7`.');

        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ should not be able to patch ID', async () => {
        const MOCK_PATCH_BODY = { _id: '615433ce728dc1980bd6213b' };
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(MOCK_PATCH_BODY).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Cannot update a station _id.');

        expect(res.body.name).toEqual('APIError');
      });

      test('PATCH/ returns an APIError correctly when no X-Auth-EmployeeName is provided', async () => {
        const MOCK_PATCH_BODY = { notes: 'Notes for the weather stations.' };
        const res = await request
          .patch(`/weather-stations/${existingStations[0]._id}`)
          .send(MOCK_PATCH_BODY);
        expect(res.body.message).toEqual('Missing employee name from request headers.');
        expect(res.status).toEqual(401);
      });
    });

    describe('duplicate station checks', () => {
      test('PATCH/ returns an APIError when updating a weather station with a station_name that is associated with another station.', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ station_name: existingStations[1].station_name }).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.name).toEqual('APIError');
        expect(res.body.message).toEqual('Invalid Parameters: Duplication error. Either station Name or Mac Address has been duplicated. Please check and try again.');
      });

      test('PATCH/ returns an APIError when updating a weather station with a mac_address that is associated with another station.', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ macAddress: existingStations[1].macAddress }).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.name).toEqual('APIError');
        expect(res.body.message).toEqual('Invalid Parameters: Duplication error. Either station Name or Mac Address has been duplicated. Please check and try again.');
      });
    });

    describe('Successfull patch', () => {
      test('PATCH/ request successfully update all fields on the weather station object', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send(updateStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = updateStationOutput(
          existingStations[0]._id, modifiedDate,
          existingStations[0].installationDate,
        );

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('PATCH/ Should successfully patch a single field on a weather station object', async () => {
        const MOCK_STATION_NAME = 'TEST';
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ station_name: MOCK_STATION_NAME }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = {
          ...existingStations[0],
          modifiedDate,
          version: existingStations[0].version + 1,
          station_name: MOCK_STATION_NAME,
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);

        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('PATCH/ request successfully updates the weather station object to remove a macAddress', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ macAddress: null }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;
        const expectedStation = { ...existingStations[0] };
        delete expectedStation.macAddress;
        const expected = {
          ...expectedStation,
          modifiedDate,
          version: existingStations[0].version + 1,
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('PATCH/ request successfully updates the weather station object with a macAddress', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ macAddress: '9F-9F-9F-9F-9F-9F' }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = {
          ...existingStations[0],
          modifiedDate,
          version: existingStations[0].version + 1,
          macAddress: '9F-9F-9F-9F-9F-9F',
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('PATCH/ should successfully be able to move a macAddress to another station', async () => {
        const UPDATE_MAC_ADDRESS = existingStations[1].macAddress;
        await request.patch(`/weather-stations/${existingStations[1]._id}`).send({ macAddress: null }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ macAddress: UPDATE_MAC_ADDRESS }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = {
          ...existingStations[0],
          modifiedDate,
          version: existingStations[0].version + 1,
          macAddress: UPDATE_MAC_ADDRESS,
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);

        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('PATCH/ should successfully be able to patch just a lat', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ latitude: 0.22 }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = {
          ...existingStations[0],
          modifiedDate,
          version: existingStations[0].version + 1,
          location: {
            type: 'Point',
            coordinates: [0.22, 0.25],
          },
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('PATCH/ should successfully be able to patch just a long', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ longitude: 0.22 }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = {
          ...existingStations[0],
          modifiedDate,
          version: existingStations[0].version + 1,
          location: {
            type: 'Point',
            coordinates: [0.26, 0.22],
          },
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });


      test('PATCH/ should successfully be able to patch lat and long with 0', async () => {
        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ latitude: 0, longitude: 0 }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const { modifiedDate, _id, version } = res.body;

        const expected = {
          ...existingStations[0],
          modifiedDate,
          version: existingStations[0].version + 1,
          location: {
            type: 'Point',
            coordinates: [0, 0],
          },
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });


      test('PATCH/ should successfully be able to patch sensors & another station property correctly (should create a new version)', async () => {
        const MOCK_SENSOR_1 = {
          type: 1,
          weatherLinkId: 2345,
        };

        const MOCK_SENSOR_2 = {
          type: 2,
          score: 61,
          weatherLinkId: 5679,
          status: 'INVALID',
          reason: 'This is sensor is invalid',
        };

        const MOCK_STATION_NAME_UPDATE = 'TEST';

        const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ station_name: MOCK_STATION_NAME_UPDATE, sensors: [MOCK_SENSOR_1, MOCK_SENSOR_2] }).set('X-Auth-EmployeeName', 'Joe Blogs');
        const EXPECTED_SENSORS = [
          {
            ...MOCK_SENSOR_1,
            status: 'NOT_YET_VALIDATED',
            reason: 'Station has not yet been validated',
            modifiedDate: res.body.sensors[0].modifiedDate,
            _id: expectMongoID,
          },
          {
            ...MOCK_SENSOR_2,
            modifiedDate: res.body.sensors[1].modifiedDate,
            _id: expectMongoID,
          },
        ];

        const expected = {
          ...existingStations[0],
          // eslint-disable-next-line no-underscore-dangle
          __v: 1,
          station_name: MOCK_STATION_NAME_UPDATE,
          version: existingStations[0].version + 1,
          modifiedDate: res.body.modifiedDate,
          sensors: EXPECTED_SENSORS,
        };

        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);


        // Expect version 1 (initial version) to exist
        const expectedInitialVersion = mockVersionOutput(existingStations[0]);
        const getInitialVersion = await request.get(`/weather-stations/${existingStations[0]._id}/version/1`);
        expect(getInitialVersion.body[0]).toEqual(expect.objectContaining(expectedInitialVersion));
        expect(getInitialVersion.body).toHaveLength(1);


        // check the versions DB has been updated correctly
        // __v is set to 0 as version history table __v key doesnt get updated as it treats versions as new docs
        const expectedUpdatedVersion = mockVersionOutput({ ...expected, __v: 0 });
        const getNewVersion = await request.get(`/weather-stations/${existingStations[0]._id}/version/2`);
        expect(getNewVersion.body[0]).toEqual(expect.objectContaining(expectedUpdatedVersion));
        expect(getNewVersion.body).toHaveLength(1);
      });
    });

    test('PATCH/ should successfully be able to patch sensors correctly & not create a version when only sensors are passed', async () => {
      const MOCK_SENSOR_1 = {
        type: 1,
        weatherLinkId: 2345,
      };

      const MOCK_SENSOR_2 = {
        type: 2,
        score: 61,
        weatherLinkId: 5679,
        status: 'INVALID',
        reason: 'This is sensor is invalid',
      };

      const res = await request.patch(`/weather-stations/${existingStations[0]._id}`).send({ sensors: [MOCK_SENSOR_1, MOCK_SENSOR_2] }).set('X-Auth-EmployeeName', 'Joe Blogs');

      const EXPECTED_SENSORS = [
        {
          ...MOCK_SENSOR_1,
          status: 'NOT_YET_VALIDATED',
          reason: 'Station has not yet been validated',
          modifiedDate: res.body.sensors[0].modifiedDate,
          _id: expectMongoID,
        },
        {
          ...MOCK_SENSOR_2,
          modifiedDate: res.body.sensors[1].modifiedDate,
          _id: expectMongoID,
        },
      ];

      const expected = {
        ...existingStations[0],
        // eslint-disable-next-line no-underscore-dangle
        __v: 1,
        modifiedDate: res.body.modifiedDate,
        sensors: EXPECTED_SENSORS,
      };

      expect(res.status).toBe(200);
      expect(res.body).toEqual(expected);
      const expectedVersion = mockVersionOutput(existingStations[0]);

      // Expect version 1 (initial version) to exist
      const getInitialVersion = await request.get(`/weather-stations/${existingStations[0]._id}/version/1`);
      expect(getInitialVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      expect(getInitialVersion.body).toHaveLength(1);

      // Expect version 2 to not exist as we dont update version when updating just sensors
      const getNewVersion = await request.get(`/weather-stations/${existingStations[0]._id}/version/2`);
      expect(getNewVersion.body.message).toEqual(`Invalid Parameters: No station with Id: ${existingStations[0]._id}, and version 2.`);
    });
  });
});
