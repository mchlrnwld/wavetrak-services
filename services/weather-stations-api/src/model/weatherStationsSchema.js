import mongoose from 'mongoose';
import { validateMacAddress, validateUrl } from '../helperFile/regex';

const sensorSchema = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId },
  weatherLinkId: { type: Number, required: [true, 'Weatherlink ID is required'] },
  type: { type: Number, required: [true, 'Type is required'] },
  score: { type: Number },
  status: {
    type: String,
    enum: [
      'UP',
      'DOWN',
      'INVALID',
      'NOT_YET_VALIDATED',
    ],
    default: 'NOT_YET_VALIDATED',
  },
  reason: { type: String, default: 'Station has not yet been validated' },
  modifiedDate: {
    type: Date,
  },
});

const WeatherStationsSchema = new mongoose.Schema(
  {
    _id: { type: mongoose.Schema.Types.ObjectId },
    version: { type: Number, required: true },
    updatedBy: { type: String, required: true },
    wx_data_url: {
      type: String,
      validate: [validateUrl, 'wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/'],
    },
    macAddress: {
      type: String,
      index: { unique: true, sparse: true },
      validate: {
        validator(value) {
          return (value === null || validateMacAddress.test(value));
        },
        message: 'MAC address should be a unique 12 digit hexadecimal number, separated by -, in 6 groups of 2, e.g. `9C-35-5B-5F-4C-D7`.',
      },
    },
    location: {
      type: {
        type: String,
        enum: ['Point'],
        required: true,
      },
      coordinates: {
        type: [Number],
        required: [true, 'Lat and lon are required'],
      },
    },
    anemometer_height: { type: Number },
    station_name: {
      type: String,
      required: [true, 'Name is required'],
      unique: true,
    },
    stationNameTrimmed: { type: String },
    data_logger: { type: String },
    station_internal_ip: { type: String },
    station_external_ip: { type: String },
    port: { type: String },
    device_id: { type: String },
    status: {
      type: String,
      enum: [
        'UP',
        'DOWN',
        'INVALID',
        'DELETED',
      ],
      required: [true, 'status is required'],
    },
    ownership: {
      type: String,
      enum: ['owned', 'shared'],
      required: [true, 'ownership status is required'],
    },
    installationDate: { type: Date },
    sensorModel: { type: String },
    notes: { type: String },
    modifiedDate: {
      type: Date,
      required: [true, 'modifiedDate is required'],
    },
    weatherLinkId: {
      type: Number,
    },
    sensors: [sensorSchema],
  },
);

const WeatherStationModel = mongoose.model('WeatherStations', WeatherStationsSchema);

/**
 * @param {object} weatherStationObject object with params
 */
export const createWeatherStation = (weatherStationObject) => {
  const weatherStation = new WeatherStationModel(weatherStationObject);

  return weatherStation.save();
};

export default WeatherStationModel;
