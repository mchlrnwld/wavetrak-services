import mongoose from 'mongoose';
import logger from '../common/logger';
import config from '../config';

const log = logger;
mongoose.Promise = global.Promise;

// eslint-disable-next-line
export const initMongoDB = () => {
  const connectionString = config.MONGO_CONNECTION_STRING_KBYG;
  const mongoDbConfig = {
    poolSize: 25,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      log.debug('Mongo ConnectionString');

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        log.info('MongoDB connected');
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        log.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
};
