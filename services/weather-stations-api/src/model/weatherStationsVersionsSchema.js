import mongoose from 'mongoose';
import { validateMacAddress, validateUrl } from '../helperFile/regex';
import { renameProperty } from '../helperFile/weatherStationHelperFunctions';

const WeatherStationsVersionsSchema = mongoose.Schema(
  {
    _id: { type: mongoose.Schema.Types.ObjectId },
    stationId: { type: mongoose.Schema.Types.ObjectId },
    version: { type: Number, required: true },
    updatedBy: { type: String, required: true },
    wx_data_url: {
      type: String,
      validate: [validateUrl, 'wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/'],
    },
    macAddress: {
      type: String,
      index: { sparse: true },
      validate: {
        validator(value) {
          return (value === null || validateMacAddress.test(value));
        },
        message: 'MAC address should be a unique 12 digit hexadecimal number, separated by -, in 6 groups of 2, e.g. `9C-35-5B-5F-4C-D7`.',
      },
    },
    location: {
      type: {
        type: String,
        enum: ['Point'],
      },
      coordinates: {
        type: [Number],
      },
    },
    anemometer_height: { type: Number },
    station_name: {
      type: String,
    },
    stationNameTrimmed: { type: String },
    data_logger: { type: String },
    station_internal_ip: { type: String },
    station_external_ip: { type: String },
    port: { type: String },
    device_id: { type: String },
    status: {
      type: String,
      enum: [
        'UP',
        'DOWN',
        'INVALID',
        'DELETED',
      ],
    },
    ownership: {
      type: String,
      enum: ['owned', 'shared'],
    },
    installationDate: { type: Date },
    sensorModel: { type: String },
    notes: { type: String },
    modifiedDate: {
      type: Date,
      required: [true, 'modifiedDate is required'],
    },
    weatherLinkId: {
      type: Number,
    },
  },
);

const WeatherStationsVersionsModel = mongoose.model('WeatherStationVersions', WeatherStationsVersionsSchema);

/**
 * @param {object} weatherStationObject object with params
 */
export const createWeatherStationVersion = (weatherStationObject) => {
  let { _doc } = weatherStationObject;
  _doc = renameProperty(_doc, '_id', 'stationId');

  // Delete sensors Obj before we save version history as this history is stored elswhere
  delete _doc.sensors;

  const WeatherStationVersions = new WeatherStationsVersionsModel({
    ..._doc,
    _id: new mongoose.Types.ObjectId(),
  });

  return WeatherStationVersions.save();
};

export default WeatherStationsVersionsModel;
