/* eslint-disable max-len */
import supertest from 'supertest';
import mongoose from 'mongoose';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from './weatherStationsSchema';
import {
  createStationInput,
  createStationOutput,
} from '../fixtures/weatherStationsExpectedData';
import startApp from '../server/server';

let mongoServer;
let thisServer;
let request;

describe('/weather-stations', () => {
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();

    const mongoUri = await mongoServer.getUri();

    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);

    const { server } = await startApp();
    thisServer = server;
    request = supertest(thisServer);
  });

  afterAll(async () => {
    await thisServer.close();
    await mongoServer.stop();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });

  describe('GET /weather-stations', () => {
    describe('empty DB', () => {
      test('GET/ returns 400 error GETting all stations from DB, when DB is empty', async () => {
        const res = await request.get('/weather-stations');
        expect(res.status).toEqual(404);
        expect(res.body.message).toEqual('No weather stations in DB');
      });
    });

    describe('populated DB', () => {
      let existingStation;
      let deletedStation;
      const MOCK_DELETED_STATION_NAME = 'DELETE TEST';
      const MOCK_DELETED_MAC_ADDRESS = '6C-45-6B-6F-5C-D8';
      const MOCK_DELETED_STATUS = 'DELETED';

      beforeAll(async () => {
        const res = await request.post('/weather-stations/').send(createStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
        existingStation = res.body;

        const MOCK_DELETED_STATION = {
          ...createStationInput,
          station_name: MOCK_DELETED_STATION_NAME,
          status: MOCK_DELETED_STATUS,
          macAddress: MOCK_DELETED_MAC_ADDRESS,
        };

        const res2 = await request.post('/weather-stations/').send(MOCK_DELETED_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');

        deletedStation = res2.body;
      });

      afterAll(async () => {
        await WeatherStationModel.deleteMany({});
      });

      describe('all stations', () => {
        test('GET/ all stations from DB where status is not deleted when using the includeDeleted query', async () => {
          const expected = createStationOutput(existingStation._id, existingStation.modifiedDate);
          const includeDeleted = false;
          const { body: saved } = await request.get(`/weather-stations/?includeDeleted=${includeDeleted}`);

          expect(saved).toEqual([expected]);
        });

        test('GET all stations from DB where status is deleted when using the includeDeleted query', async () => {
          const expected = createStationOutput(existingStation._id, existingStation.modifiedDate);
          const expectedDeleted = createStationOutput(
            deletedStation._id,
            deletedStation.modifiedDate,
          );
          expectedDeleted.station_name = MOCK_DELETED_STATION_NAME;
          expectedDeleted.macAddress = MOCK_DELETED_MAC_ADDRESS;
          expectedDeleted.status = MOCK_DELETED_STATUS;

          const includeDeleted = true;
          const { body: saved } = await request.get(`/weather-stations/?includeDeleted=${includeDeleted}`);

          expect(saved).toEqual([expected, expectedDeleted]);
        });

        test('GET/all stations from DB', async () => {
          const expected = createStationOutput(existingStation._id, existingStation.modifiedDate);
          const { body: saved } = await request.get('/weather-stations');
          expect(saved).toEqual([expected]);
        });
      });

      describe('single station by ID', () => {
        test('GET/:id rejects an invalid id input gracefully', async () => {
          const res = await request.get('/weather-stations/nonId');
          expect(res.status).toBe(400);
          expect(res.body.message).toEqual('Invalid Id: Weather station Id must a string of 24 hex characters');
        });

        test('GET/:id successfully GETs the correct station', async () => {
          const expected = createStationOutput(existingStation._id, existingStation.modifiedDate);
          const { body: savedStation } = await request.get(`/weather-stations/${existingStation._id}`);
          expect(savedStation).toEqual(expected);
        });
      });
    });
  });
});
