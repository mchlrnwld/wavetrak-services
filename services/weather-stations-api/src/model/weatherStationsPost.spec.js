/* eslint-disable max-len */
import supertest from 'supertest';
import mongoose from 'mongoose';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from './weatherStationsSchema';
import {
  createStationInput,
  createStationOutput,
  mockVersionOutput,
} from '../fixtures/weatherStationsExpectedData';
import startApp from '../server/server';

let mongoServer;
let thisServer;
let request;

describe('/weather-stations', () => {
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();

    const mongoUri = await mongoServer.getUri();

    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);

    const { server } = await startApp();
    thisServer = server;
    request = supertest(thisServer);
  });

  afterAll(async () => {
    await thisServer.close();
    await mongoServer.stop();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });

  describe('POST', () => {
    afterEach(async () => {
      await WeatherStationModel.deleteMany({});
    });

    describe('Required data', () => {
      test('POST/ returns an APIError when inserting a new weather station with no station_name', async () => {
        const MOCK_STATION = { ...createStationInput };
        delete MOCK_STATION.station_name;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: station_name: Name is required');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with no longitude', async () => {
        const MOCK_STATION = { ...createStationInput };
        delete MOCK_STATION.longitude;
        const res = await request.post('/weather-stations/').send(MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Longitude must exist.');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with no latitude', async () => {
        const MOCK_STATION = { ...createStationInput };
        delete MOCK_STATION.latitude;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Latitude must exist.');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with no status', async () => {
        const MOCK_STATION = { ...createStationInput };
        delete MOCK_STATION.status;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: status: status is required');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with no ownership', async () => {
        const MOCK_STATION = { ...createStationInput };
        delete MOCK_STATION.ownership;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: ownership: ownership status is required');
        expect(res.body.name).toEqual('APIError');
      });


      test('POST/ returns an APIError when inserting a WeatherStations with sensors without required fields', async () => {
        const MOCK_STATION = {
          ...createStationInput,
          sensors: [
            {
              test: 'test',
            },
          ],
        };

        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: sensors.0.type: Type is required, sensors.0.weatherLinkId: Weatherlink ID is required');
        expect(res.body.name).toEqual('APIError');
      });
    });


    describe('Invalid data', () => {
      test('POST/ rejects non array input gracefully', async () => {
        const emptyBody = {};
        const res = await request
          .post('/weather-stations/')
          .send(emptyBody)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(400);
        expect(res.body.message).toBe('Insufficient weather Stations data provided');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid status', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.status = '';
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');

        expect(res.status).toBe(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: status: status is required');
        expect(res.body.errorHandler).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid ownership value', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.ownership = '';
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: ownership: ownership status is required');
        expect(res.body.errorHandler).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid station_name', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.station_name = '';
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: station_name: Name is required');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid min latitude', async () => {
        const MOCK_STATION = { ...createStationInput };

        MOCK_STATION.latitude = 91;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Latitude value 91 not in range [-90, +90].');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid max latitude', async () => {
        const MOCK_STATION = { ...createStationInput };

        MOCK_STATION.latitude = -91;

        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Latitude value -91 not in range [-90, +90].');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with an invalid macAddress.', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.macAddress = 1234;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: macAddress: MAC address should be a unique 12 digit hexadecimal number, separated by -, in 6 groups of 2, e.g. `9C-35-5B-5F-4C-D7`.');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid url as a number.', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.wx_data_url = 95;

        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid url as a string but not in matching format.', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.wx_data_url = 'www.surf.com';

        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/');
        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid url as a empty string', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.wx_data_url = '';

        const res3 = await request.post('/weather-stations/').send(MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res3.status).toEqual(400);
        expect(res3.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/');
        expect(res3.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError when inserting a new weather station with invalid weatherLinkId as a string', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.weatherLinkId = 'testing';

        const res3 = await request.post('/weather-stations/').send(MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res3.status).toEqual(400);
        expect(res3.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: weatherLinkId: Cast to Number failed for value "testing" (type string) at path "weatherLinkId"');
        expect(res3.body.name).toEqual('APIError');
      });

      test('POST/ returns an APIError correctly when multiple issues were found', async () => {
        const MOCK_STATION = { ...createStationInput, wx_data_url: 'test', macAddress: '1234' };

        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: WeatherStations validation failed: wx_data_url: wx_data_url must be in correct format: /http://.<station_IP><stationPort>.v1.current_conditions/, macAddress: MAC address should be a unique 12 digit hexadecimal number, separated by -, in 6 groups of 2, e.g. `9C-35-5B-5F-4C-D7`.');

        expect(res.body.name).toEqual('APIError');
      });

      test('POST/ returns an UnauthorizedError correctly when no X-Auth-EmployeeName is provided', async () => {
        const MOCK_STATION = { ...createStationInput };
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION);
        expect(res.body.message).toEqual('Missing employee name from request headers.');
        expect(res.status).toEqual(401);
      });
    });


    describe('duplicate station checks', () => {
      test('POST/ returns an APIError when inserting a new weather station with repeated station name.', async () => {
        const res = await request
          .post('/weather-stations/')
          .send(createStationInput)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(200);

        const res2 = await request.post('/weather-stations/').send(createStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res2.status).toEqual(400);
        expect(res2.body.name).toEqual('APIError');
        expect(res2.body.message).toEqual('Invalid Parameters: Duplication error. Either station Name or Mac Address has been duplicated. Please check and try again.');
      });

      test('POST/ returns an APIError when inserting a new weather station with a repeated macAddress', async () => {
        const MOCK_STATION = { ...createStationInput };

        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(200);

        MOCK_STATION.station_name = 'TEST';
        const resMac = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(resMac.body.message).toEqual('Invalid Parameters: Duplication error. Either station Name or Mac Address has been duplicated. Please check and try again.');
        expect(resMac.body.name).toEqual('APIError');
      });
    });

    describe('Successful data', () => {
      test('POST/ inserts a new weather station with all fields', async () => {
        const res = await request.post('/weather-stations/').send(createStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(200);
        const { _id, modifiedDate, version } = res.body;
        const expected = createStationOutput(_id, modifiedDate);
        expect(res.body).toEqual(expected);

        // check the versions DB has been updated correctly
        const versionWrongId = await request.get(`/weather-stations/12345/version/${version}`);
        expect(versionWrongId.body.message).toEqual('Invalid Id: Weather station Id must a string of 24 hex characters');

        const versionWrongVersion = await request.get(`/weather-stations/${_id}/version/0`);
        expect(versionWrongVersion.body.message).toEqual(`Invalid Parameters: No station with Id: ${_id}, and version 0.`);
        expect(versionWrongVersion.body.name).toEqual('APIError');

        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('POST/ successful insertion of new weather station without non-required fields', async () => {
        const MOCK_STATION = { ...createStationInput };

        delete MOCK_STATION.wx_data_url;
        delete MOCK_STATION.anemometer_height;
        delete MOCK_STATION.stationNameTrimmed;
        delete MOCK_STATION.data_logger;
        delete MOCK_STATION.port;
        delete MOCK_STATION.device_id;
        delete MOCK_STATION.sensorModel;
        delete MOCK_STATION.notes;
        delete MOCK_STATION.macAddress;

        const res = await request.post('/weather-stations/').send(MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(200);

        const { _id, modifiedDate, version } = res.body;

        const expected = createStationOutput(_id, modifiedDate);

        delete expected.macAddress;
        delete expected.wx_data_url;
        delete expected.anemometer_height;
        delete expected.stationNameTrimmed;
        delete expected.data_logger;
        delete expected.port;
        delete expected.device_id;
        delete expected.sensorModel;
        delete expected.notes;

        expect(res.body).toEqual(expected);

        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/${version}`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));
      });

      test('POST/ a new weather station with a lat and long as 0.', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.latitude = 0;
        MOCK_STATION.longitude = 0;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(200);
        const { _id, modifiedDate } = res.body;

        const createOutput = await createStationOutput(_id, modifiedDate);
        const expected = {
          ...createOutput,
          location: {
            coordinates: [0, 0],
            type: 'Point',
          },
        };
        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
      });

      test('POST/ a new weather station with an empty macAddress.', async () => {
        const MOCK_STATION = { ...createStationInput };
        MOCK_STATION.macAddress = null;
        const res = await request
          .post('/weather-stations/')
          .send(MOCK_STATION)
          .set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(200);
        const { _id, modifiedDate } = res.body;

        const createOutput = await createStationOutput(_id, modifiedDate);
        delete createOutput.macAddress;
        const expected = {
          ...createOutput,
        };
        expect(res.status).toBe(200);
        expect(res.body).toEqual(expected);
      });

      test('POST/ two weather stations with an null macAddress.', async () => {
        const MOCK_STATION_NAME = 'TEST';
        const MOCK_STATION_1 = { ...createStationInput, macAddress: null };
        const MOCK_STATION_2 = {
          ...createStationInput,
          macAddress: null,
          station_name: MOCK_STATION_NAME,
        };
        const res1 = await request
          .post('/weather-stations/')
          .send(MOCK_STATION_1)
          .set('X-Auth-EmployeeName', 'Joe Blogs');

        const res2 = await request
          .post('/weather-stations/')
          .send(MOCK_STATION_2)
          .set('X-Auth-EmployeeName', 'Joe Blogs');

        const expected1 = await createStationOutput(res1.body._id, res1.body.modifiedDate);
        const expected2 = await createStationOutput(res2.body._id, res2.body.modifiedDate);

        delete expected1.macAddress;
        delete expected2.macAddress;

        expected2.station_name = MOCK_STATION_NAME;

        expect(res1.status).toBe(200);
        expect(res1.body).toEqual(expected1);

        expect(res2.status).toBe(200);
        expect(res2.body).toEqual(expected2);
      });

      test('POST/ inserts a new weather station with sensors with default reason and status fields', async () => {
        const MOCK_SENSOR = {
          type: 1,
          score: 61,
          weatherLinkId: 2345,
        };
        const MOCK_SENSORS = [
          MOCK_SENSOR,
          {
            ...MOCK_SENSOR,
            type: 2,
            weatherLinkId: 5679,
          },
        ];
        const MOCK_STATION = {
          ...createStationInput,
          sensors: MOCK_SENSORS,
        };


        const res = await request.post('/weather-stations/').send(MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toBe(200);
        const {
          _id,
          modifiedDate,
          sensors,
        } = res.body;

        const EXPECTED_SENSORS = sensors.map((sensor) => ({ ...sensor, status: 'NOT_YET_VALIDATED', reason: 'Station has not yet been validated' }));

        const expected = createStationOutput(_id, modifiedDate, EXPECTED_SENSORS);
        expect(res.body).toEqual(expected);

        // check the versions DB has been updated correctly
        const expectedVersion = mockVersionOutput(res.body);
        const getVersion = await request.get(`/weather-stations/${_id}/version/1`);
        expect(getVersion.body).toHaveLength(1);
        expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));

        const invalidVersion = await request.get(`/weather-stations/${_id}/version/2`);
        expect(invalidVersion.body.message).toEqual(`Invalid Parameters: No station with Id: ${_id}, and version 2.`);
        expect(invalidVersion.body.name).toEqual('APIError');
      });
    });

    test('POST/ inserts a new weather station with sensors correctly', async () => {
      const MOCK_SENSOR_1 = {
        type: 1,
        weatherLinkId: 2345,
      };

      const MOCK_SENSOR_2 = {
        type: 2,
        score: 61,
        weatherLinkId: 5679,
        status: 'INVALID',
        reason: 'This is sensor is invalid',
      };

      const MOCK_STATION = {
        ...createStationInput,
        sensors: [MOCK_SENSOR_1, MOCK_SENSOR_2],
      };

      const res = await request.post('/weather-stations/').send(MOCK_STATION).set('X-Auth-EmployeeName', 'Joe Blogs');
      expect(res.status).toBe(200);

      const {
        _id,
        modifiedDate,
        sensors,
      } = res.body;
      const expected = createStationOutput(_id, modifiedDate, sensors);
      expect(res.body).toEqual(expected);

      const expectedVersion = mockVersionOutput(res.body);

      const getVersion = await request.get(`/weather-stations/${_id}/version/1`);
      expect(getVersion.body).toHaveLength(1);
      expect(getVersion.body[0]).toEqual(expect.objectContaining(expectedVersion));

      const invalidVersion = await request.get(`/weather-stations/${_id}/version/2`);
      expect(invalidVersion.body.message).toEqual(`Invalid Parameters: No station with Id: ${_id}, and version 2.`);
      expect(invalidVersion.body.name).toEqual('APIError');
    });
  });
});
