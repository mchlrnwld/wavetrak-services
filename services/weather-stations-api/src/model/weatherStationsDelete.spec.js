/* eslint-disable max-len */
import supertest from 'supertest';
import mongoose from 'mongoose';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from './weatherStationsSchema';
import {
  createStationInput,
  deleteStationOutput,
  mockVersionOutput,
} from '../fixtures/weatherStationsExpectedData';
import startApp from '../server/server';

let mongoServer;
let thisServer;
let request;

describe('/weather-stations', () => {
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();

    const mongoUri = await mongoServer.getUri();

    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);

    const { server } = await startApp();
    thisServer = server;
    request = supertest(thisServer);
  });

  afterAll(async () => {
    await thisServer.close();
    await mongoServer.stop();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });
  describe('DELETE /weather-station', () => {
    let existingStation;
    let deletedStation;

    beforeEach(async () => {
      const res = await request.post('/weather-stations/').send(createStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
      existingStation = res.body;

      const res2 = await request.delete(`/weather-stations/${existingStation._id}`).set('X-Auth-EmployeeName', 'Joe Blogs');
      deletedStation = res2.body;
    });

    afterEach(async () => {
      await WeatherStationModel.deleteMany({});
    });
    describe('Invalid', () => {
      test('DELETE/ rejects non id input gracefully', async () => {
        const res = await request.delete('/weather-stations/nonId').set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Weather station Id must a string of 24 hex characters');
        expect(res.error).toBeTruthy();
      });

      test('DELETE/ request returns a 400 when given an invalid id', async () => {
        const stationId = null;
        const res = await request.delete(`/weather-stations/${stationId}`);
        expect(res.body.message).toEqual('Weather station Id must a string of 24 hex characters');
        expect(res.status).toEqual(400);
      });

      test('DELETE/ request returns a 400 when there is no object with matching station id.', async () => {
        const id = '587fdc0d3b01e6a94510ca69';
        const res = await request.delete(`/weather-stations/${id}`).set('X-Auth-EmployeeName', 'Joe Blogs');
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual(`Invalid Parameters: No station with Id: ${id}.`);
        expect(res.error).toBeTruthy();
      });

      test('DELETE/ returns an APIError correctly when no X-Auth-EmployeeName is provided', async () => {
        const createStation = { ...createStationInput };
        createStation.station_name = 'Test no employeeName';
        delete createStation.macAddress;
        const res = await request.post('/weather-stations/').send(createStation).set('X-Auth-EmployeeName', 'Joe Blogs');
        const res2 = await request.delete(`/weather-stations/${res.body._id}`);
        expect(res2.body.message).toEqual('Missing employee name from request headers.');
        createStation.macAddress = '9C-35-5B-5F-4C-D7';
      });
    });

    describe('Success', () => {
      test('DELETE/ request successfully updates station status to DELETED for the weather station object that gets returned from the GET request.', async () => {
        const { installationDate, _id } = existingStation;
        const { modifiedDate } = deletedStation;
        const expected = deleteStationOutput(_id, modifiedDate, installationDate);
        expect(deletedStation).toEqual(expected);

        const expectedCreateVersion = mockVersionOutput(existingStation);
        const expectedDeleteVersion = mockVersionOutput(deletedStation);
        const getCreateVersion = await request.get(`/weather-stations/${_id}/version/1`);

        const getDeleteVersion = await request.get(`/weather-stations/${_id}/version/2`);

        expect(getCreateVersion.body).toHaveLength(1);
        expect(getCreateVersion.body[0]).toEqual(expect.objectContaining(expectedCreateVersion));
        expect(getDeleteVersion.body).toHaveLength(1);
        expect(getDeleteVersion.body[0]).toEqual(expect.objectContaining(expectedDeleteVersion));
      });
    });
  });
});
