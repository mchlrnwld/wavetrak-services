/* eslint-disable max-len */
import supertest from 'supertest';
import mongoose from 'mongoose';
import MongoMemoryServer from 'mongodb-memory-server';
import WeatherStationModel from './weatherStationsSchema';
import { createStationInput } from '../fixtures/weatherStationsExpectedData';
import {
  expectMongoID,
  expectDateSting,
} from '../helperFile/matchers';
import startApp from '../server/server';

describe('/weather-stations/:stationId/sensors', () => {
  let mongoServer;
  let thisServer;
  let request;

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => err);
    const { server } = await startApp();
    thisServer = server;
    request = supertest(thisServer);
  });

  afterAll(async () => {
    await thisServer.close();
    await mongoServer.stop();
    thisServer = null;
    mongoServer = null;
    mongoose.disconnect();
  });

  describe('POST', () => {
    let existingStations = [];

    beforeEach(async () => {
      const res = await request.post('/weather-stations/').send(createStationInput).set('X-Auth-EmployeeName', 'Joe Blogs');
      existingStations.push(res.body);
    });

    afterEach(async () => {
      existingStations = [];
      await WeatherStationModel.deleteMany({});
    });

    test('fails with missing data', async () => {
      const res = await request.post(`/weather-stations/${existingStations[0]._id}/sensors`).send({});
      expect(res.status).toBe(400);
      expect(res.body.errorHandler).toEqual('APIError');
    });

    test('creates new sensor', async () => {
      const data = {
        type: 1,
        weatherLinkId: 999,
      };

      const res = await request.post(`/weather-stations/${existingStations[0]._id}/sensors`).send(data);
      expect(res.status).toBe(200);

      const station = (await request.get(`/weather-stations/${existingStations[0]._id}`)).body;
      expect(station.sensors).toHaveLength(1);
      expect(station.sensors[0]).toEqual({
        ...data,
        _id: expectMongoID,
        modifiedDate: expectDateSting,
        status: 'NOT_YET_VALIDATED',
        reason: 'Station has not yet been validated',
      });
    });
  });
});
