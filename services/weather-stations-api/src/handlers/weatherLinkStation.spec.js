/* eslint-disable max-len */
import supertest from 'supertest';
import startApp from '../server/server';
import fetchStations from '../helperFile/fetchWeatherLinkStations';
import {
  RETURNED_STATIONS1,
  RETURNED_STATIONS2,
  RETURNED_STATIONS3,
  RETURN_ALL_STATIONS,
  MOCK_STATIONS1,
  MOCK_STATIONS2,
  MOCK_STATIONS3,
  MOCK_STATIONS4,
  RETURNED_STATIONS4,
} from '../fixtures/weatherLinkFixtures';

jest.mock('../helperFile/fetchWeatherLinkStations');

let thisServer;
let request;

describe('/weather-link-stations', () => {
  beforeAll(async () => {
    const { server } = await startApp();
    thisServer = server;
    request = supertest(thisServer);
  });

  afterAll(async () => {
    await thisServer.close();
    thisServer = null;
  });

  describe('GET /weather-link-stations', () => {
    describe('successful request', () => {
      test('returns empty array when no weather link stations are within 2km of given lat lon', async () => {
        fetchStations.mockReturnValue(Promise.resolve(MOCK_STATIONS1));
        const lat = 51.60019;
        const lon = -4.27026;
        const res = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res.status).toEqual(200);
        expect(res.body).toEqual([]);
      });
      test('gets the nearest (up to 20) weather link stations (within 2km radius) to a given lat lon from over 20 weather link stations', async () => {
        fetchStations.mockReturnValue(Promise.resolve(MOCK_STATIONS2));
        let lat = 33.3853;
        let lon = -117.5939;
        const res = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res.status).toEqual(200);
        expect(res.body).toEqual(RETURNED_STATIONS2);
        lat = 51.60019;
        lon = -4.27026;
        const res2 = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res2.status).toEqual(200);
        expect(res2.body).toEqual(RETURNED_STATIONS1);

        fetchStations.mockReturnValue(Promise.resolve(MOCK_STATIONS3));
        const res3 = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res3.status).toEqual(200);
        expect(res3.body).toEqual(RETURNED_STATIONS3);

        lat = 89.99999;
        lon = 180;
        fetchStations.mockReturnValue(Promise.resolve(MOCK_STATIONS4));
        const res4 = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res4.status).toEqual(200);
        expect(res4.body).toEqual(RETURNED_STATIONS4);
      });
      test('gets all weather link stations when lat and lon are not provided', async () => {
        fetchStations.mockReturnValue(Promise.resolve(MOCK_STATIONS1));
        const res = await request.get('/weather-link-stations/');
        expect(res.body).toEqual(RETURN_ALL_STATIONS);
      });
    });
    describe('invalid request', () => {
      test('returns APIError where lat is invalid', async () => {
        let lat = 90.1;
        const lon = 0.25;
        const res = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Latitude value 90.1 not in range [-90, +90].');
        expect(res.body.name).toEqual('APIError');
        lat = -90.1;
        const res2 = await request.get(`/weather-link-stations/?lat=${lat}&lon=${lon}`);
        expect(res2.status).toEqual(400);
        expect(res2.body.message).toEqual('Invalid Parameters: Latitude value -90.1 not in range [-90, +90].');
        expect(res2.body.name).toEqual('APIError');
      });
      test('returns 400 where only one of lat or lon provided', async () => {
        const lat = 0.25;
        const lon = 0.25;
        const res = await request.get(`/weather-link-stations/?lat=${lat}`);
        expect(res.status).toEqual(400);
        expect(res.body.message).toEqual('Invalid Parameters: Both Lat and Lon must be provided to get the nearest 20 weather link stations.');

        const res2 = await request.get(`/weather-link-stations/?lon=${lon}`);
        expect(res2.status).toEqual(400);
        expect(res2.body.message).toEqual('Invalid Parameters: Both Lat and Lon must be provided to get the nearest 20 weather link stations.');
      });
    });
  });
});
