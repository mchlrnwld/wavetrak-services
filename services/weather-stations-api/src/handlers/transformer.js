import mongoose from 'mongoose';
import { validateLat, validateLon } from '../helperFile/weatherStationHelperFunctions';


/**
 * @param {object} weatherStationObject object with lat lon params:
 *      latitude, longitude
 * @returns {object} updated weatherStation object get returned with valid lat lon's converted
 * into a point object for insertion into mongoDB
 */
export const transformAndValidateLocation = (latitude_, longitude_) => {
  const latitude = validateLat(latitude_);
  const longitude = validateLon(longitude_);

  const coordinates = [latitude, longitude];

  // convert lat lon into point
  return { type: 'Point', coordinates };
};

/**
 * Transforms the sesnor object into expected mongo structure
 * @param  {object} sensorBody
 * @param  {Date|Number} currentDate. Default now
 * @param  {Number} version. Default 1
 * @returns object
 */
export const transformSensor = (sensorBody, currentDate = Date.now(), version = 1) => {
  const sensor = {
    ...sensorBody,
    _id: new mongoose.Types.ObjectId(sensorBody._id),
    modifiedDate: currentDate,
  };

  if (version === 1) {
    sensor.createDate = currentDate;
  }

  return sensor;
};

/**
 * Transforms the request body of create station into the expected mongo structure
 * @param  {object} body
 * @param  {string} employeeName
 * @returns object
 */
export const createWeatherStationTransformer = (body, employeeName) => {
  const currentDate = Date.now();
  const initialVersion = 1;

  const weatherStationObject = {
    ...body,
    _id: new mongoose.Types.ObjectId(body._id),
    modifiedDate: currentDate,
    version: initialVersion,
    installationDate: currentDate,
    updatedBy: employeeName,
    createDate: currentDate,
  };

  weatherStationObject.location = transformAndValidateLocation(body.latitude, body.longitude);

  if (body.macAddress === null) {
    delete weatherStationObject.macAddress;
  }

  if (body?.sensors?.length) {
    weatherStationObject.sensors = body.sensors.map(
      (sensor) => transformSensor(sensor, currentDate, initialVersion),
    );
  }

  return weatherStationObject;
};


/**
 * Takes the existing weatherStations object and uses the data (along with the new request body)
 * to create a object of values to update.
 *
 * @param  {object} body
 * @param  {object} existingWeatherStation
 * @param  {string} employeeName
 * @param  {boolean} isOnlyUpdatingSensors
 * @returns object
 */
export const updateExistingWeatherStationTransformer = (
  body,
  existingWeatherStation,
  employeeName,
  isOnlyUpdatingSensors,
) => {
  const currentDate = Date.now();

  const bodyHasValidLatitude = Boolean(body.latitude || body.latitude === 0);
  const bodyHasValidLongitude = Boolean(body.longitude || body.longitude === 0);

  const latitude = bodyHasValidLatitude ? body.latitude
    : existingWeatherStation.location.coordinates[0];

  const longitude = bodyHasValidLongitude ? body.longitude
    : existingWeatherStation.location.coordinates[1];

  const newVersion = existingWeatherStation.version + 1;
  const bodyData = { ...body };

  // remove lat, long so we dont spread it into newWeatherStationData.
  delete bodyData.latitude;
  delete bodyData.longitude;

  const newWeatherStationData = {
    ...bodyData,
    updatedBy: employeeName,
    modifiedDate: currentDate,
  };

  /*
    * if we need to update lat or long then run it through the transformer (by default
    * it will be spread into the object)
  */
  if (bodyHasValidLatitude || bodyHasValidLongitude) {
    newWeatherStationData.location = transformAndValidateLocation(latitude, longitude);
  }

  if (body.macAddress === null) {
    newWeatherStationData.macAddress = undefined;
  }

  // If we are only patching sensors dont update the version
  if (!isOnlyUpdatingSensors) {
    newWeatherStationData.version = newVersion;
  }

  if (body?.sensors?.length) {
    newWeatherStationData.sensors = body.sensors.map(
      (sensor) => transformSensor(sensor, currentDate),
    );
  }

  return newWeatherStationData;
};
