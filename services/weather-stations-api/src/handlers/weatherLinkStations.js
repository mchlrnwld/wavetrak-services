import { Router } from 'express';
import { APIError, wrapErrors } from '@surfline/services-common';
import { closestStations, transformStations } from '../helperFile/weatherLinkHelperFunctions';
import fetchStations from '../helperFile/fetchWeatherLinkStations';
import { validateLat } from '../helperFile/weatherStationHelperFunctions';

function weatherLinkStations() {
  const api = Router();
  /**
   * fetches closest 20 weatherLink stations if a lat lon is provided,
   * else returns all weather link stations
   *
   * @param object $query   can contain Latitude and Longitude in decimal degrees
   * @return res            returns array of top 20 closest weather link stations
   * in Lat and Lon provided, else returns all weather link stations
   */
  const getWeatherLinkStationsHandler = async ({ query }, res) => {
    const { lat, lon } = query;
    if (([lat, lon].filter((field) => field === undefined)).length === 1) {
      throw new APIError('Both Lat and Lon must be provided to get the nearest 20 weather link stations.');
    }
    let { stations } = await fetchStations();
    if (lat !== undefined) {
      validateLat(lat);
      // if there is a lat/lon given fetch closest stations to the coordinates
      stations = closestStations(lat, lon, stations);
    }
    return res.json(transformStations(stations));
  };

  api.get('/', wrapErrors(getWeatherLinkStationsHandler));

  return api;
}

export default weatherLinkStations;
