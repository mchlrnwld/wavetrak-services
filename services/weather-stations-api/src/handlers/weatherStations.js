import { Router } from 'express';
import { APIError, wrapErrors } from '@surfline/services-common';
import { json } from 'body-parser';
import mongoose from 'mongoose';
import { isEmpty } from 'lodash';
import WeatherStationModel, { createWeatherStation } from '../model/weatherStationsSchema';
import {
  checkForDuplicateError,
  getEmployeeName,
  createFindMongoQueryBasedOnReqQuery,
} from '../helperFile/weatherStationHelperFunctions';
import WeatherStationsVersionsModel, { createWeatherStationVersion } from '../model/weatherStationsVersionsSchema';
import {
  createWeatherStationTransformer,
  transformSensor,
  updateExistingWeatherStationTransformer,
} from './transformer';

export function weatherStations() {
  const api = Router();

  const createWeatherStationHandler = async ({ body, headers }, res) => {
    try {
      if (isEmpty(Object.keys(body))) {
        return res.status(400).send({
          message: 'Insufficient weather Stations data provided',
        });
      }

      const employeeName = getEmployeeName(headers);
      const weatherStationObject = createWeatherStationTransformer(body, employeeName);

      const insertedData = await createWeatherStation(weatherStationObject);

      // update the versions db
      await createWeatherStationVersion(insertedData);

      return res.json(insertedData);
    } catch (error) {
      await checkForDuplicateError(error.code);
      if (error instanceof mongoose.Error.ValidationError) {
        throw new APIError(error.message);
      }
      throw error;
    }
  };

  // get a specified weather station by the _id
  const getWeatherStationHandler = async ({ params: { stationId } }, res) => {
    let _id;
    try {
      _id = new mongoose.Types.ObjectId(stationId);
    } catch (error) {
      return res.status(400).json({
        message: 'Invalid Id: Weather station Id must a string of 24 hex characters',
      });
    }
    return res.json((await WeatherStationModel.findOne({ _id })).toObject());
  };

  // gets all weatherStations including those are soft deleted if includeDeleted exists
  const getAllWeatherStationsHandler = async ({ query }, res) => {
    const mongoQuery = createFindMongoQueryBasedOnReqQuery(query);
    const result = await WeatherStationModel.find(mongoQuery);

    if (result.length === 0) {
      return res.status(404).json({
        message: 'No weather stations in DB',
      });
    }
    return res.json(result);
  };

  // get a specified weather station by the stationId and its version
  const getWeatherStationVersionHandler = async ({ params: { stationId, version_ } }, res) => {
    let id;
    try {
      id = new mongoose.Types.ObjectId(stationId);
    } catch (error) {
      return res.status(400).json({
        message: 'Invalid Id: Weather station Id must a string of 24 hex characters',
      });
    }
    const weatherStationVersion = await WeatherStationsVersionsModel.find(
      { stationId: id, version: version_ },
    );
    if (weatherStationVersion.length === 0) {
      throw new APIError(`No station with Id: ${stationId}, and version ${version_}.`);
    }
    return res.send(weatherStationVersion);
  };

  const patchWeatherStationHandler = async ({ body, headers, params: { stationId } }, res) => {
    try {
      // check id is valid
      if (!mongoose.Types.ObjectId.isValid(stationId)) {
        return res.status(400).json({
          message: 'Invalid Id: Weather station Id must a string of 24 hex characters',
        });
      }

      if (isEmpty(Object.keys(body))) {
        return res.status(400).send({
          message: 'Insufficient weather Stations data provided to update station',
        });
      }

      if (body._id) {
        throw new APIError('Cannot update a station _id.');
      }

      // check the object exists in the DB
      const existingWeatherStation = await WeatherStationModel.findOne({ _id: stationId });
      const isOnlyUpdatingSensors = !Object.keys(body).some((key) => key !== 'sensors');

      if (!existingWeatherStation) {
        throw new APIError(`No station with Id: ${stationId}.`);
      }

      const employeeName = getEmployeeName(headers);

      // Create an object of values to update
      const updateWeatherStationData = updateExistingWeatherStationTransformer(
        body,
        // eslint-disable-next-line no-underscore-dangle
        existingWeatherStation._doc,
        employeeName,
        isOnlyUpdatingSensors,
      );

      // Loop through the values we want to update and update the existing object respectively
      Object.entries(updateWeatherStationData).forEach(([key, value]) => {
        existingWeatherStation[key] = value;
      });

      // save the updated object
      const patchedWeatherStation = await existingWeatherStation.save();

      if (!isOnlyUpdatingSensors) {
        // update the versions db
        await createWeatherStationVersion(patchedWeatherStation);
      }

      res.status(200);
      res.json(patchedWeatherStation);
      return res;
    } catch (error) {
      await checkForDuplicateError(error.code);
      if (error instanceof mongoose.Error.ValidationError) {
        throw new APIError(error.message);
      }
      throw error;
    }
  };

  const deleteStationHandler = async ({ headers, params: { stationId } }, res) => {
    let _id;
    try {
      _id = mongoose.Types.ObjectId(stationId);
    } catch (error) {
      return res.status(400).json({
        message: 'Weather station Id must a string of 24 hex characters',
      });
    }
    const existingObject = await WeatherStationModel.findOne(_id);
    if (!existingObject) {
      throw new APIError(`No station with Id: ${stationId}.`);
    }
    if (existingObject.status !== 'DELETED') {
      // update status
      existingObject.status = 'DELETED';
    }
    // bump version
    existingObject.version += 1;
    // get the employee name and update the station object
    existingObject.updatedBy = await getEmployeeName(headers);
    // update modified date
    existingObject.modifiedDate = Date.now();
    // save the updated object
    const deletedWeatherStation = await existingObject.save();
    // update the versions db
    await createWeatherStationVersion(existingObject);

    res.json(deletedWeatherStation);
    res.status(200);
    return res;
  };

  const postSensorHandler = async ({ body, params: { stationId } }, res) => {
    try {
      const sensor = transformSensor(body);
      const result = await WeatherStationModel.updateOne({ _id: stationId }, {
        $push: { sensors: sensor },
      }, { runValidators: true });
      if (result.nModified === 0) {
        return res.status(404).json({
          message: 'Station not found',
        });
      }
      return res.json(sensor);
    } catch (error) {
      if (error instanceof mongoose.Error.ValidationError) {
        throw new APIError(error.message);
      }
      throw error;
    }
  };

  api.use('*', json());
  api.get('/', wrapErrors(getAllWeatherStationsHandler));
  api.get('/:stationId', wrapErrors(getWeatherStationHandler));
  api.post('/:stationId/sensors', wrapErrors(postSensorHandler));
  api.get('/:stationId/version/:version_', wrapErrors(getWeatherStationVersionHandler));
  api.post('/', wrapErrors(createWeatherStationHandler));
  api.patch('/:stationId', wrapErrors(patchWeatherStationHandler));
  api.delete('/:stationId', wrapErrors(deleteStationHandler));

  return api;
}

export default weatherStations;
