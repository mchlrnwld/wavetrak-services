export const XYZ_POINTS_A = {
  x: 6377.978570747796,
  y: 27.8293580310894,
  z: 27.829622949042108,
};
export const XYZ_POINTS_B = {
  x: 4888.806195299365,
  y: 2300.4975732770476,
  z: 3389.3132800242847,
};

export const XYZ_POINTS_C = { x: 11.131878850535703, y: 0, z: 6378.0902856006405 };

export const XYZ_POINTS_D = {
  x: -11.131878850535703,
  y: 1.3632619802804665e-15,
  z: 6378.0902856006405,
};
export const UNSORTED_ARRAY = [
  {
    distance: 10,
  },
  {
    distance: 20,
  },
  {
    distance: 3,
  },
  {
    distance: 0,
  },
];
export const SORTED_ARRAY = [
  { distance: 0 },
  { distance: 3 },
  { distance: 10 },
  { distance: 20 },
];
export const MOCK_STATIONS = {
  stations: [
    {
      station_id: 6974,
      station_name: 'Private - BL Orchard Range Test x39',
      gateway_id: 8388665,
      latitude: 37.75104,
      longitude: -121.25094,
      elevation: 19,
    },
    {
      station_id: 7327,
      station_name: 'Hayward Mesh 15min x4F',
      gateway_id: 8388687,
      latitude: 37.635952,
      longitude: -122.124794,
      elevation: 20.0344,
    },
  ],
  generated_at: 1509055056,
};
export const SORTED_STATIONS = [
  {
    stationId: 6974,
    latitude: 37.75104,
    longitude: -121.25094,
    stationName: 'Private - BL Orchard Range Test x39',
  },
  {
    stationId: 7327,
    latitude: 37.635952,
    longitude: -122.124794,
    stationName: 'Hayward Mesh 15min x4F',
  },
];
