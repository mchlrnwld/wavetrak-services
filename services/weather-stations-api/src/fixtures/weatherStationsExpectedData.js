import { renameProperty } from '../helperFile/weatherStationHelperFunctions';

export const updateStationInput = {
  wx_data_url: 'http://98.189.163.153:666/v1/current_conditions_v1/',
  latitude: 0.26,
  longitude: 0.25,
  anemometer_height: 11,
  station_name: 'Hb pier north',
  station_internal_ip: '168.212.226.205',
  station_external_ip: '168.212.226.201',
  port: '5001',
  device_id: '5842041f4e65fad6a7708956',
  status: 'DOWN',
  ownership: 'shared',
  sensorModel: 'model2001',
  notes: 'test notes UPDATED',
  macAddress: '6C-45-6B-6F-5C-D8',
  weatherLinkId: 1234567,
};

export const updateStationOutput = (id, updatedAt, installedAt, sensors) => (
  {
    __v: 0,
    _id: id,
    wx_data_url: 'http://98.189.163.153:666/v1/current_conditions_v1/',
    location: {
      type: 'Point',
      coordinates: [0.26, 0.25],
    },
    anemometer_height: 11,
    station_name: 'Hb pier north',
    station_internal_ip: '168.212.226.205',
    station_external_ip: '168.212.226.201',
    port: '5001',
    device_id: '5842041f4e65fad6a7708956',
    status: 'DOWN',
    ownership: 'shared',
    sensorModel: 'model2001',
    notes: 'test notes UPDATED',
    modifiedDate: updatedAt,
    installationDate: installedAt,
    macAddress: '6C-45-6B-6F-5C-D8',
    updatedBy: 'Joe Blogs',
    version: 2,
    weatherLinkId: 1234567,
    sensors: sensors || [],
  }
);

export const createStationInput = {
  wx_data_url: 'http://98.189.163.153:666/v1/current_conditions/',
  latitude: 0.26,
  longitude: 0.25,
  anemometer_height: 10,
  station_name: 'Trestles',
  station_internal_ip: '168.212.226.204',
  station_external_ip: '168.212.226.200',
  port: '5000',
  device_id: '5842041f4e65fad6a7708955',
  status: 'UP',
  ownership: 'owned',
  sensorModel: 'model2000',
  notes: 'test notes',
  macAddress: '9C-35-5B-5F-4C-D7',
  weatherLinkId: 1234567,
};

export const createStationOutput = (id, createdAt, sensors) => ({
  __v: 0,
  _id: id,
  wx_data_url: 'http://98.189.163.153:666/v1/current_conditions/',
  location: {
    type: 'Point',
    coordinates: [0.26, 0.25],
  },
  anemometer_height: 10,
  station_name: 'Trestles',
  station_internal_ip: '168.212.226.204',
  station_external_ip: '168.212.226.200',
  port: '5000',
  device_id: '5842041f4e65fad6a7708955',
  status: 'UP',
  ownership: 'owned',
  sensorModel: 'model2000',
  notes: 'test notes',
  installationDate: createdAt,
  modifiedDate: createdAt,
  macAddress: '9C-35-5B-5F-4C-D7',
  updatedBy: 'Joe Blogs',
  version: 1,
  weatherLinkId: 1234567,
  sensors: sensors || [],
});

export const deleteStationOutput = (id, updatedAt, installedAt, sensors) => (
  {
    __v: 0,
    _id: id,
    wx_data_url: 'http://98.189.163.153:666/v1/current_conditions/',
    location: {
      type: 'Point',
      coordinates: [0.26, 0.25],
    },
    anemometer_height: 10,
    station_name: 'Trestles',
    station_internal_ip: '168.212.226.204',
    station_external_ip: '168.212.226.200',
    port: '5000',
    device_id: '5842041f4e65fad6a7708955',
    status: 'DELETED',
    ownership: 'owned',
    sensorModel: 'model2000',
    notes: 'test notes',
    modifiedDate: updatedAt,
    installationDate: installedAt,
    updatedBy: 'Joe Blogs',
    version: 2,
    macAddress: '9C-35-5B-5F-4C-D7',
    weatherLinkId: 1234567,
    sensors: sensors || [],
  }
);

export const mockVersionOutput = (versionObject) => {
  const updatedVersionObject = { ...versionObject };
  delete updatedVersionObject.sensors;

  return renameProperty(updatedVersionObject, '_id', 'stationId');
};
