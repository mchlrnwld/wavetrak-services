import { APIError, UnauthorizedError } from '@surfline/services-common/dist';

// taken from services/spots-api/src/server/admin/spotMeta/utils.js
/**
 * Transforms longitude to within the -180 -> 180 bounds that some applications use as a standard.
 * @param  {number} longitude
 * @returns number
 */
export const validateLon = (longitude) => {
  if (!longitude && longitude !== 0) {
    throw new APIError('Longitude must exist.');
  }
  // eslint-disable-next-line no-param-reassign
  while (longitude <= -180) longitude += 360;
  // eslint-disable-next-line no-param-reassign
  while (longitude > 180) longitude -= 360;
  return Math.round(longitude * 100000) / 100000;
};

/**
 * Transforms latitude to within the -90 -> 90 bounds that some applications use as a standard.
 * @param  {number} latitude
 * @returns number
 */
export const validateLat = (latitude) => {
  if (!latitude && latitude !== 0) {
    throw new APIError('Latitude must exist.');
  }
  if (latitude < -90 || latitude > 90) {
    throw new APIError(`Latitude value ${latitude} not in range [-90, +90].`);
  }
  return latitude;
};

const duplicateErrorCode = 11000;
const duplicateErrorMessage = 'Duplication error. Either station Name or Mac Address has been duplicated. Please check and try again.';
export const checkForDuplicateError = (errorCode) => {
  if (errorCode === duplicateErrorCode) {
    throw new APIError(duplicateErrorMessage);
  }
};

export const getEmployeeName = (headers) => {
  const employeeName = headers['x-auth-employeename'];
  if (!employeeName) throw new UnauthorizedError('Missing employee name from request headers.');
  return employeeName;
};

export const renameProperty = (obj, fromKey, toKey) => {
  const object = { ...obj };
  object[toKey] = object[fromKey];
  delete object[fromKey];
  return object;
};

export const createFindMongoQueryBasedOnReqQuery = ({ includeDeleted }) => {
  let query = {};

  if (includeDeleted !== 'true') {
    query = { status: { $ne: 'DELETED' } };
  }

  return query;
};
