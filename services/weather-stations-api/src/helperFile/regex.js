export const validateMacAddress = /([0-9A-F]{2}[-]){5}([0-9A-F]{2})/;
export const validateUrl = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:~+#-]*[\w@?^=%&amp;~+#-])?/;
