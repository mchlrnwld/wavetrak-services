import {
  MOCK_STATIONS,
  SORTED_STATIONS,
  SORTED_ARRAY,
  XYZ_POINTS_A,
  XYZ_POINTS_B,
  UNSORTED_ARRAY,
  XYZ_POINTS_C,
  XYZ_POINTS_D,
} from '../fixtures/weatherLinkHelperFunctionFixtures';
import {
  convertToXYZ, degreesToRadians, findDistance, sortStations, transformStations,
} from './weatherLinkHelperFunctions';

describe('/weatherLinkHelperFunctions', () => {
  describe('/convertToXYZ', () => {
    test('converts longitude and latitude to a 3D point, xyz', async () => {
      const convertedLatLon = convertToXYZ(0.25, 0.25);
      expect(convertedLatLon).toEqual(XYZ_POINTS_A);
      const convertedLatLon1 = convertToXYZ(89.9, 0);
      expect(convertedLatLon1).toEqual(XYZ_POINTS_C);
      const convertedLatLon2 = convertToXYZ(89.9, 180);
      expect(convertedLatLon2).toEqual(XYZ_POINTS_D);
    });
  });
  describe('/findDistance', () => {
    test('finds the distance between two pairs of lat and lon positions', async () => {
      const foundDistance = await findDistance(XYZ_POINTS_A, XYZ_POINTS_B);
      expect(foundDistance).toEqual(4322.294246944778);
    });
  });
  describe('/sortStations', () => {
    test('sorts an array of objects by distance ascending', async () => {
      const sortedarray = await sortStations(UNSORTED_ARRAY);
      expect(sortedarray).toEqual(SORTED_ARRAY);
    });
  });
  describe('/transformStations', () => {
    test('removes unwanted key-value pairs from weather link stations', async () => {
      const transformedStations = await transformStations(MOCK_STATIONS.stations);
      expect(transformedStations).toEqual(SORTED_STATIONS);
    });
  });
  describe('/degreesToRadians', () => {
    test('converts degrees to radians', async () => {
      const converted1 = await degreesToRadians(180);
      expect(converted1).toEqual(3.141592653589793);
      const converted2 = await degreesToRadians(-180);
      expect(converted2).toEqual(-3.141592653589793);
    });
  });
});
