import { validateLat, validateLon } from './weatherStationHelperFunctions';

describe('/helperFunctions', () => {
  describe('/validateLon', () => {
    test('normalizes longitude value with negative less than -180', async () => {
      const newLon = validateLon(-189);
      expect(newLon).toBe(171);
    });
    test('normalizes longitude value with 0', async () => {
      const newLon = validateLon(0);
      expect(newLon).toBe(0);
    });
    test('normalizes longitude value with negative more than -180', async () => {
      const newLon = validateLon(-179);
      expect(newLon).toBe(-179);
    });
    test('normalizes longitude value with positive less than 180', async () => {
      const newLon = validateLon(179);
      expect(newLon).toBe(179);
    });
    test('normalizes longitude value with positive more than 180', async () => {
      const newLon = validateLon(181);
      expect(newLon).toBe(-179);
    });
    test('throws an error if no longitude is supplied', async () => {
      expect(() => { validateLon(); }).toThrow('Longitude must exist.');
    });
  });
  describe('/validateLat', () => {
    test('throws an error if no latitude is supplied', async () => {
      expect(() => { validateLat(); }).toThrow('Latitude must exist.');
    });
    test('throws an error if latitude is less than -90', async () => {
      expect(() => { validateLat(-99); }).toThrow('Latitude value -99 not in range [-90, +90].');
    });
    test('throws an error if latitude is more than 90', async () => {
      expect(() => { validateLat(99); }).toThrow('Latitude value 99 not in range [-90, +90].');
    });
    test('returns a valid latitude', async () => {
      const newLat = validateLat(0.25);
      expect(newLat).toBe(0.25);
    });
    test('returns valid latitude value with 0', async () => {
      const newLat = validateLat(0);
      expect(newLat).toBe(0);
    });
  });
});
