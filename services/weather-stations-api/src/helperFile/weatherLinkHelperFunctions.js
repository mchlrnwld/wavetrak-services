/* eslint-disable max-len */

const EARTH_RADIUS = 6378.1; // In km
const DIST = 2; // in km

/**
 * Reduces the weatherLink stations to contain the desired fields
 *
 * @param array weatherLinkStations   array of objects of Weather Link stations
 * @return sortedWeatherLinkStns      array of sorted weather link station objects
 */
export const transformStations = (weatherLinkStations) => weatherLinkStations.map(({
  // eslint-disable-next-line camelcase
  station_id, latitude, longitude, station_name, distance,
}) => ({
  stationId: station_id, latitude, longitude, stationName: station_name, distance,
}));

/**
 * Converts degrees to radians
 *
 * @param float $degrees    decimal degrees
 * @return radians          radian degree
 */
export const degreesToRadians = (degree) => degree * (Math.PI / 180.0);

/**
 * Converts a lat/lon to a coordinate (x, y, z) in 3d cartesian space
 * assuming the earth is a sphere of radius self::EARTH_RADIUS centered at (0, 0, 0).
 *
 * @param float $lat        Latitude in decimal degrees
 * @param float $lon        Longitude in decimal degrees
 * @return array            Array of length three of x, y, z, position
 */
export const convertToXYZ = (lat, lon) => {
  const newLat = degreesToRadians(lat);
  const newLon = degreesToRadians(lon);

  const pointsXYZ = {};
  pointsXYZ.x = (EARTH_RADIUS * Math.cos(newLat) * Math.cos(newLon));
  pointsXYZ.y = (EARTH_RADIUS * Math.cos(newLat) * Math.sin(newLon));
  pointsXYZ.z = (EARTH_RADIUS * Math.sin(newLat));

  return pointsXYZ;
};

/**
 * finds distance between two xyz points using pythagoras theorem
 *
 * @param object $stationXYZ       station in xyz
 * @param object $weatherLinkXYZ   weather link station in xyz
 * @return number                  distance squared in straight line between two points
 */
export const findDistance = (stationXYZ, weatherLinkXYZ) => {
  const xx = ((stationXYZ.x - weatherLinkXYZ.x) ** 2);
  const yy = ((stationXYZ.y - weatherLinkXYZ.y) ** 2);
  const zz = ((stationXYZ.z - weatherLinkXYZ.z) ** 2);

  return Math.sqrt(xx + yy + zz);
};

/**
 * sorts stations by distance accenting
 *
 * @param array $stationArray    array of station objects
 * @return stationArray          returns array of station object sorted by distance
 */
export const sortStations = (stationArray) => {
  stationArray.sort((a, b) => a.distance - b.distance);
  return stationArray;
};

/**
 * finds the closest stations to given lat lon points
 *
* @param float $currentLat        Latitude in decimal degrees
 * @param float $currentLon      Longitude in decimal degrees
 * @return closestStationsArray       returns array of top 20 closest stations
 */
export const closestStations = (currentLat, currentLon, stationsArray) => {
  const stationXYZ = convertToXYZ(currentLat, currentLon);
  const closestStationsArray = stationsArray.map((weatherLinkStation) => {
    const weatherLinkXYZ = convertToXYZ(weatherLinkStation.latitude, weatherLinkStation.longitude);
    const checkX = weatherLinkXYZ.x <= stationXYZ.x + DIST && weatherLinkXYZ.x >= stationXYZ.x - DIST;
    const checkY = weatherLinkXYZ.y <= stationXYZ.y + DIST && weatherLinkXYZ.y >= stationXYZ.y - DIST;
    const checkZ = weatherLinkXYZ.z <= stationXYZ.z + DIST && weatherLinkXYZ.z >= stationXYZ.z - DIST;
    if (checkX && checkY && checkZ) {
      const distance = findDistance(stationXYZ, weatherLinkXYZ);
      if (distance <= DIST) {
        const linkStation = { ...weatherLinkStation };
        linkStation.distance = distance;
        return linkStation;
      }
    }
    return null;
  }).filter((weatherLinkStation) => !!weatherLinkStation);
  sortStations(closestStationsArray);
  return closestStationsArray.slice(0, 19);
};
