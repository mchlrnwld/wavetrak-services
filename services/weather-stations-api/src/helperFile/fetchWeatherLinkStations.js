import fetch from 'node-fetch';
import crypto from 'crypto';
import { URL } from 'url';
import config from '../config';

const createApiRequestParameters = (apiKey, apiSecret, stationId) => {
  // Taken from weatherlink examples see: https://replit.com/@WeatherLink/v2-API-Signature-Calculation-in-Nodejs

  const parameters = {
    'api-key': apiKey,
    'api-secret': apiSecret,
    t: Math.floor(Date.now() / 1000),
  };

  if (stationId) {
    parameters['station-id'] = stationId;
  }

  const parameterNamesSorted = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const parameterName in parameters) {
    if (parameterName) {
      parameterNamesSorted.push(parameterName);
    }
  }
  parameterNamesSorted.sort();

  const currentApiSecret = parameters['api-secret'];
  delete parameters['api-secret'];
  parameterNamesSorted.splice(parameterNamesSorted.indexOf('api-secret'), 1);

  let data = '';
  // eslint-disable-next-line no-restricted-syntax
  for (const parameterName of parameterNamesSorted) {
    data = data + parameterName + parameters[parameterName];
  }

  const hmac = crypto.createHmac('sha256', currentApiSecret);
  hmac.update(data);
  const apiSignature = hmac.digest('hex');

  return {
    ...parameters,
    apiSignature,
  };
};

const HOST = 'https://api.weatherlink.com';

const fetchStations = async () => {
  const requestParams = createApiRequestParameters(
    config.WEATHER_LINK_API_KEY,
    config.WEATHER_LINK_API_SECRET,
  );
  const requestUrl = new URL(
    `${HOST}/v2/stations?api-key=${requestParams['api-key']}&api-signature=${requestParams.apiSignature}&t=${requestParams.t}`,
  );
  const response = await fetch(requestUrl.href);
  return response.json();
};

export default fetchStations;
