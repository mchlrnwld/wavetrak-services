const trackRequests = (log) => (req, res, next) => {
  log.trace({
    action: req.path,
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default trackRequests;
