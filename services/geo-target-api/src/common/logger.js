import { logger, setupLogsene } from '@surfline/services-common';
import config from '../config';

setupLogsene(config.LOGSENE_KEY);

export default logger('geo-target-service');
