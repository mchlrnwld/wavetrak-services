import 'newrelic';
import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import startApp from './server';
import log from './common/logger';
import config from './config';

const start = async () => {
  try {
    const poolSize = 10;
    await initMongo(mongoose, config.MONGO_CONNECTION_STRING_KBYG, 'geo-target-service', poolSize);
    startApp();
  } catch (err) {
    console.log(err);
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  }
};

start();
