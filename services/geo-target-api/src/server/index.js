import { setupExpress } from '@surfline/services-common';
import { region } from './region';
import { assets } from './assets';
import { basins } from './basins';
import { root } from './root';
import { waterways } from './waterways';
import log from '../common/logger';
import config from '../config';

const startApp = () =>
  setupExpress({
    port: config.EXPRESS_PORT || 8080,
    name: 'geo-target-service',
    log,
    allowedMethods: ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'],
    handlers: [
      ['/Region', region()],
      ['/Assets', assets()],
      ['/Basins', basins()],
      ['/Waterways', waterways()],
      ['/', root()],
    ],
  });

export default startApp;
