import express from 'express';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { waterways } from './waterways';
import * as WaterwayModel from '../models/Waterway';
import waterwayFixture from '../fixtures/waterwayResponse.json';

const { expect } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);

describe('waterways route', () => {
  const app = express();

  beforeEach(async () => {
    app.use('/waterways', waterways());
    sinon.stub(WaterwayModel, 'getWaterwayForBoundingBox');
  });

  afterEach(() => {
    WaterwayModel.getWaterwayForBoundingBox.restore();
  });

  it('should initialize as an object', () => {
    expect(waterways).to.exist();
  });

  it('should reject request with missing longitude', (done) => {
    chai
      .request(app)
      .get('/waterways')
      .query({ lat: 49.22 })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return a Waterway when requesting with Vancouver Sound coordinates', (done) => {
    WaterwayModel.getWaterwayForBoundingBox.resolves(waterwayFixture);
    chai
      .request(app)
      .get('/waterways')
      .query({ lat: 49.22, lon: -123.57 })
      .end((err, res) => {
        expect(err).to.be.equal(null);
        expect(res).to.have.status(200);
        expect(res.body.waterways.name).to.equal('Vancouver - Seattle');
        done();
      });
  }).timeout(8000);

  it('should error when given coordinates on land', (done) => {
    WaterwayModel.getWaterwayForBoundingBox.resolves(null);
    chai
      .request(app)
      .get('/waterways')
      .query({ lat: 31.89, lon: -103.57 })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  }).timeout(8000);
});
