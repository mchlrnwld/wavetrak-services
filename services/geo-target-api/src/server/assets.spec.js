import express from 'express';
import chai, { expect } from 'chai';
import chaihttp from 'chai-http';
import sinonChai from 'sinon-chai';
import { assets } from './assets';

chai.use(chaihttp);
chai.use(sinonChai);
chai.should();

describe('assets route', () => {
  const app = express();

  before(() => {
    app.use('/assets', assets());
  });

  describe('/manifest', () => {
    it('should return a 400 error if the ipAddress is invalid', (done) => {
      chai
        .request(app)
        .get('/assets/manifest')
        .query({ ipAddress: '8.8' })
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it('should return a 200 with json if given an ipAddress', (done) => {
      chai
        .request(app)
        .get('/assets/manifest')
        .query({ ipAddress: '8.8.8.8' })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(Object.keys(res.body).length).to.be.above(0);
          done();
        });
    });
  });

  describe('/resource', () => {
    it('should return a redirect', (done) => {
      chai
        .request(app)
        .get('/assets/resource')
        .query({ key: 'default' })
        .end((err, res) => {
          res.should.redirect();
          done();
        });
    });

    it('should return a 404 when an asset is not found', (done) => {
      chai
        .request(app)
        .get('/assets/resource')
        .query({ key: 'this key wont be found' })
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });

    it('should return a 400 when no "key" is provided', (done) => {
      chai
        .request(app)
        .get('/assets/resource')
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });
});

//
// Test below is used to verify for a given IP address in a given area we're loading the expected resource from the manifest file.
// It's an integration test for each county where we've geo'd images. We're testing, does the right manifest load and return
// the right image for a given ip address. Iterate a set of ip, image pairs to validate our library. We don't need to test every
// image in this code, simply that the expected manifest loads for a given IP.
//
describe('Verify expected manifests are loaded', () => {
  const app = express();

  before(() => {
    app.use('/assets', assets());
  });

  const assetsToVerify = [
    {
      name: 'San Diego',
      ip: '75.85.186.143',
      image: 'sign-in-san-diego-billy-watts.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Orange County',
      ip: '72.67.2.178',
      image: 'sign-in-orange-county-miah.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Los Angeles County',
      ip: '134.201.250.155',
      image: 'sign-in-LA-malibu.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Cornwall, UK',
      ip: '195.195.235.133',
      image: 'sign-in-cornwall.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Devon, UK',
      ip: '79.75.103.112',
      image: 'sign-in-devon.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Miami-Dade County',
      ip: '50.73.157.178',
      image: 'sign-in-miami.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Israel',
      ip: '213.184.119.185',
      image: 'sign-in-israel.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Union County, NC',
      ip: '152.26.69.153',
      image: 'sign-in-NC-obx-pullen.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'New South Wales, AU',
      ip: '110.33.122.75',
      image: 'sign-in-new-south-wales.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'New Jersey',
      ip: '209.222.18.222',
      image: 'sign-in-NJ-pat-nolan.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'New York',
      ip: '72.229.28.185',
      image: 'sign-in-NY-nelson.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Rio De Janeiro, BR',
      ip: '187.67.119.247',
      image: 'sign-in-rio-de-janeiro.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'San Francisco County',
      ip: '136.0.16.217',
      image: 'sign-in-san-franciscom-miah.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Luis Obispo County',
      ip: '204.88.239.128',
      image: 'sign-in-san-luis-obispo-county-miah.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Santa Catarina, Brazil',
      ip: '186.207.215.232',
      image: 'sign-in-santa-catarina.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Santa Cruz',
      ip: '165.227.115.242',
      image: 'sign-in-santa-cruz-nikki-brooks.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Sao Paulo, Brazil',
      ip: '200.158.222.107',
      image: 'sign-in-sao-paulo.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Ventura County',
      ip: '207.157.138.163',
      image: 'sign-in-ventura-county-miah.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Victoria, AU',
      ip: '144.138.54.13',
      image: 'sign-in-victoria.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Western Australia, AU',
      ip: '180.216.31.197',
      image: 'sign-in-western-australia.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Virgina Beach, OBX, VA',
      ip: '68.10.64.115',
      image: 'sign-in-NC-obx-pullen.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Hawaii',
      ip: '72.130.194.78',
      image: 'sign-in-hawaii.jpg',
      key: 'sign-in-background',
    },
    {
      name: 'Bali',
      ip: '103.10.67.172',
      image: 'ios-report-indo-uluwato.jpg',
      key: 'funnel-desktop-ad-free',
    },
    {
      name: 'El Salvador',
      ip: '186.32.105.56',
      image: 'ios-report-las-flores-el-salvador.jpg',
      key: 'funnel-desktop-ad-free',
    },
    {
      name: 'Mexico',
      ip: '187.178.254.54',
      image: 'ios-report-puerto-escondido-mexico.jpg',
      key: 'funnel-desktop-ad-free',
    },
    {
      name: 'Barbados',
      ip: '205.214.200.108',
      image: 'ios-report-soup-bowl-barbados.jpg',
      key: 'funnel-desktop-ad-free',
    },
    {
      name: 'Spain',
      ip: '80.245.1.13',
      image: 'ios-report-spain-mundaka.jpg',
      key: 'funnel-desktop-ad-free',
    },
  ];

  assetsToVerify.forEach((obj) => {
    it(`should return a localized image for ${obj.name} (${obj.ip})`, (done) => {
      chai
        .request(app)
        .get('/assets/manifest')
        .query({ ipAddress: obj.ip })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body[obj.key]).to.be.equal(obj.image);
          done();
        });
    });
  });
});
