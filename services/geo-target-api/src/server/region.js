import { Router } from 'express';
import log from '../common/logger';
import * as geoTarget from '../external/geotarget';

export const region = () => {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: `/Region`,
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
      },
    });
    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (message, err) => {
    if (typeof err === 'object') {
      log.error({
        message: `geoTargetService:Region:${action}`,
        stack: err,
      });
    } else {
      log.error({
        message: `geoTargetService:Region:${action}`,
        errorMessage: err,
      });
    }
    res.status(errorCode).send({ message });
  };

  // eslint-disable-next-line consistent-return
  const getRegionHandler = (req, res) => {
    const ipAddress = req.query.ipAddress || req.ip;
    if (!ipAddress || !geoTarget.validate(ipAddress)) {
      log.debug(`Region requested without ipAddress parameter.`);
      return res
        .status(400)
        .send({ message: 'You need to specify a valid IP Address as query param "ipAddress"' });
    }
    const location = geoTarget.getLocationForIPAddress(ipAddress);
    if (location && location.country) {
      res.status(200).json(location);
    } else {
      handleErrorForResponse(
        400,
        res,
        `getRegionHandler(${ipAddress})`,
      )(`Location not found for ${ipAddress}`);
    }
  };

  /**
   * Query Params:
   * ipAddress: String IP Address
   *
   * Returns:
   * application/json
   {
     "continent": {
       "geoname_id": 6255148,
       "name": "Europe"
     },
     "country": {
       "geoname_id": 2635167,
       "name": "United Kingdom",
       "iso_code": "GB"
     },
     "location": {
       "latitude": 50.8441,
       "longitude": -0.1113,
       "time_zone": "Europe/London"
     },
     "city": {
       "geoname_id": 2654710,
       "name": "Brighton"
     },
     "subdivisions": [
       {
         "geoname_id": 6269131,
         "iso_code": "ENG",
         "name": "England"
       },
       {
         "geoname_id": 3333133,
         "iso_code": "BNH",
         "name": "Brighton and Hove"
       }
     ],
     "postal": {
       "code": "BN2"
     }
   }
   */
  api.get('/', trackRequest, getRegionHandler);

  return api;
};
