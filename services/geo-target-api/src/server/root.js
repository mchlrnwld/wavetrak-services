import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import moment from 'moment-timezone';
import log from '../common/logger';

export const root = () => {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: req.path,
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
      },
    });
    next();
  };

  /**
   * @description
   * In order to support letting users change their timezone via the `edit-settings`
   * page, we need to provide a list of available timezones that they can choose from.
   * We do not want to install moment or moment-timezone on the front-end due to it's
   * size, so we having this endpoint to send back the list of available timezones.
   */
  const getSupportedTimezonesHandler = async (req, res) => {
    res.send(moment.tz.names());
  };

  api.get('/timezones', trackRequest, wrapErrors(getSupportedTimezonesHandler));

  return api;
};
