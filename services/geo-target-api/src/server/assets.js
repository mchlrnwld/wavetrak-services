import { Router } from 'express';
import log from '../common/logger';
import * as geoTarget from '../external/geotarget';
import { getManifest } from '../external/manifestManager';

const cdn = {
  development: 'https://drcb7kk0emt05.cloudfront.net',
  sandbox: 'https://drcb7kk0emt05.cloudfront.net',
  qa: 'https://qa-subs.cdn-surfline.com',
  staging: 'https://dojtv91ijeswu.cloudfront.net',
  production: 'https://prod-subs.cdn-surfline.com',
};

const assetPath = 'geo_assets';

export const assets = () => {
  const api = Router();

  const trackRequest = (req, _, next) => {
    log.trace({
      action: '/auth',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const lookupKeyFromIp = (ip) => {
    const location = geoTarget.getLocationForIPAddress(ip);
    if (location && location.country) {
      const country = location.country.iso_code;
      if (location.subdivisions) {
        return (
          country +
          location.subdivisions.reduce((memo, division) => {
            if (division.iso_code) {
              return `${memo}-${division.iso_code}`;
            }
            if (division.name) {
              return `${memo}-${division.name.replace(/\s/g, '')}`;
            }
            return memo;
          }, '')
        ).toLowerCase();
      }
      return country;
    }
    return '';
  };

  const getManifestHandler = (req, res) => {
    const { ipAddress } = req.query;
    if (!ipAddress || !geoTarget.validate(ipAddress)) {
      log.debug(`Region requested without ipAddress parameter.`);
      return res.status(400).send({
        message: 'You need to specify a valid IP Address as query param "ipAddress"',
      });
    }

    const lookupKey = lookupKeyFromIp(ipAddress);
    log.info(`Found lookupKey ${lookupKey} for ${ipAddress}`);
    const assetManifest = getManifest(lookupKey);
    return res.status(200).send(assetManifest);
  };

  const getResourceHandler = (req, res) => {
    const ipAddress = req.query.ipAddress ? req.query.ipAddress : req.ip;
    const { key } = req.query;

    if (!key) {
      log.debug('Asset requested without a lookup key.');
      return res.status(400).send({ message: 'You need to specify a lookup key.' });
    }

    const lookupKey = lookupKeyFromIp(ipAddress);
    log.info(`Found lookupKey ${lookupKey} for ${ipAddress}`);
    const assetManifest = getManifest(lookupKey);

    if (assetManifest[key]) {
      const env = process.env.NODE_ENV;
      const cdnHost = cdn[env] ? cdn[env] : cdn.production;
      let asset = assetManifest[key];

      if (Array.isArray(asset)) {
        const index = Math.floor(Math.random() * asset.length);
        asset = asset[index];
      }

      return res.redirect(302, `${cdnHost}/${assetPath}/${asset}`);
    }

    return res.status(404).send(`404 - Resource with key ${key} was not found.`);
  };

  api.all('*', trackRequest);
  api.get('/resource', getResourceHandler);
  api.get('/manifest', getManifestHandler);

  return api;
};
