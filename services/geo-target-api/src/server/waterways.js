import { Router } from 'express';
import log from '../common/logger';
import { getWaterwayForBoundingBox } from '../models/Waterway';

export const waterways = () => {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: `/Waterways`,
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
      },
    });
    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (message, err) => {
    if (typeof err === 'object') {
      log.error({
        message: `geoTargetService:Waterways:${action}`,
        stack: err,
      });
    } else {
      log.error({
        message: `geoTargetService:Waterways:${action}`,
        errorMessage: err,
      });
    }
    res.status(errorCode).send({ message });
  };

  const generateBounds = (lat, lon) => {
    let west = lon - 0.01;
    let east = lon + 0.01;
    let south = lat - 0.01;
    let north = lat + 0.01;

    if (lon >= 180 || lon <= -180) {
      east = -179.99;
      west = 179.99;
    }
    if (lat >= 90) {
      north = 89.99;
      south = 89.98;
    } else if (lat <= -90) {
      north = -89.98;
      south = -89.99;
    }

    return [
      [west, north],
      [east, north],
      [east, south],
      [west, south],
      [west, north],
    ];
  };

  // eslint-disable-next-line consistent-return
  const getWaterwaysHandler = async (req, res) => {
    const lat = Number(req.query.lat);
    const lon = Number(req.query.lon);

    if (!lat || !lon) {
      log.debug(`Waterways requested without lat and lon parameters.`);
      return res.status(400).send({ message: 'You need to specify lat and lon.' });
    }

    const boundingBoxPolygon = generateBounds(lat, lon);
    const existingWaterways = await getWaterwayForBoundingBox(boundingBoxPolygon);

    if (existingWaterways) {
      const { name, type, tags } = existingWaterways;
      const waterwaysResp = {
        name,
        type,
        tags,
      };
      res.status(200).json({ waterways: waterwaysResp });
    } else {
      const msgData = `lat,lon [${lat},${lon}]`;
      handleErrorForResponse(
        400,
        res,
        `getWaterwaysHandler(${msgData})`,
      )(`No waterway found for ${msgData}`);
    }
  };

  api.get('/', trackRequest, getWaterwaysHandler);

  return api;
};
