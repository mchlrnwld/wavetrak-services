import { Router } from 'express';
import log from '../common/logger';
import BasinModel from '../models/Basin';

export const basins = () => {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: `/Basins`,
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
      },
    });
    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (message, err) => {
    if (typeof err === 'object') {
      log.error({
        message: `geoTargetService:Basins:${action}`,
        stack: err,
      });
    } else {
      log.error({
        message: `geoTargetService:Basins:${action}`,
        errorMessage: err,
      });
    }
    res.status(errorCode).send({ message });
  };

  const getBasinsHandler = async (req, res) => {
    const north = Number(req.query.north);
    const south = Number(req.query.south);
    const east = Number(req.query.east);
    const west = Number(req.query.west);

    if (!north || !south || !east || !west) {
      log.debug(`Basins requested without all parameters.`);
      return res.status(400).send({ message: 'You need to spefify north, south, east and west.' });
    }

    const boundingBoxPolygon = [
      [west, north],
      [east, north],
      [east, south],
      [west, south],
      [west, north],
    ];

    const basinsResponse = await BasinModel.distinct('name', {
      location: {
        $geoIntersects: {
          $geometry: {
            type: 'Polygon',
            coordinates: [boundingBoxPolygon],
          },
        },
      },
    });
    return basinsResponse
      ? res.status(200).json({ basins: basinsResponse })
      : handleErrorForResponse(
          400,
          res,
          `getBasinsHandler(${boundingBoxPolygon})`,
        )(`No basin found with ${boundingBoxPolygon}`);
  };

  api.get('/', trackRequest, getBasinsHandler);

  return api;
};
