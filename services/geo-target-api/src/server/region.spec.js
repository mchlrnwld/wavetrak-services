import express from 'express';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinonChai from 'sinon-chai';
import { region } from './region';

const { expect } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);

describe('region route', () => {
  const app = express();

  before(() => {
    app.use('/region', region());
  });

  it('should initialize as an object', () => {
    expect(region).to.exist();
  });

  it('should reject request with partial IP', (done) => {
    chai
      .request(app)
      .get('/region')
      .query({ ipAddress: '8.8' })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should reject request with invalid IP', (done) => {
    chai
      .request(app)
      .get('/region')
      .query({ ipAddress: 'a' })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return a Location when requesting with US IP', (done) => {
    chai
      .request(app)
      .get('/region')
      .query({ ipAddress: '8.8.8.8' })
      .end((err, res) => {
        expect(err).to.be.equal(null);
        expect(res).to.have.status(200);
        const { body } = res;
        expect(body.subdivisions[1].name).to.equal('Santa Clara County');
        done();
      });
  });

  it('should return a Location when requesting with Brazilian IP', (done) => {
    chai
      .request(app)
      .get('/region')
      .query({ ipAddress: '201.83.41.11' })
      .end((err, res) => {
        expect(err).to.be.equal(null);
        expect(res).to.have.status(200);
        const { body } = res;
        expect(body.subdivisions[0].iso_code).to.equal('SP');
        done();
      });
  });

  it('should return a Location when requesting with UK IP', (done) => {
    chai
      .request(app)
      .get('/region')
      .query({ ipAddress: '195.195.237.39' })
      .end((err, res) => {
        expect(err).to.be.equal(null);
        expect(res).to.have.status(200);
        const { body } = res;
        expect(body.subdivisions[1].iso_code).to.equal('BNH');
        done();
      });
  });

  it('should return a Location when requesting with Australian IP', (done) => {
    chai
      .request(app)
      .get('/region')
      .query({ ipAddress: '110.33.122.75' })
      .end((err, res) => {
        expect(err).to.be.equal(null);
        expect(res).to.have.status(200);
        const { body } = res;
        expect(body.subdivisions[0].iso_code).to.equal('NSW');
        done();
      });
  });
});
