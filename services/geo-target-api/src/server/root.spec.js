import express from 'express';
import chai, { expect } from 'chai';
import chaihttp from 'chai-http';
import moment from 'moment-timezone';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { root } from './root';

chai.use(chaihttp);
chai.use(sinonChai);
chai.should();

describe('/* route', () => {
  const app = express();

  before(() => {
    app.use('/', root());
  });
  beforeEach(() => {
    sinon.stub(moment.tz, 'names');
  });
  afterEach(() => {
    moment.tz.names.restore();
  });

  describe('/timezones', () => {
    it('should return an array of supported timezones', (done) => {
      const timezones = ['America/Los_Angeles', 'Europe/Paris', 'Australia/Melbourne'];
      moment.tz.names.returns(timezones);
      chai
        .request(app)
        .get('/timezones')
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.deep.equal(timezones);
          done();
        });
    });
  });
});
