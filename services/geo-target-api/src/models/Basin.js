import mongoose from 'mongoose';
import config from '../config';

const BasinSchema = mongoose.Schema(
  {
    name: String,
    location: {
      type: {
        type: String,
        enum: [
          'Point',
          'MultiPoint',
          'LineString',
          'MultiLineString',
          'Polygon',
          'MultiPolygon',
          'GeometryCollection',
        ],
      },
      coordinates: {
        type: [
          {
            lat: {
              type: Number,
              max: 90.0,
              min: -90.0,
            },
            lng: {
              type: Number,
              max: 180.0,
              min: -180.0,
            },
            _id: false,
          },
        ],
        default: [
          {
            lat: 0,
            lng: 0,
          },
        ],
      },
    },
  },
  { collection: config.MONGO_BASINS_COLLECTION },
);

BasinSchema.index({ location: '2dsphere' });

const BasinModel = mongoose.model('Basins', BasinSchema);

export default BasinModel;
