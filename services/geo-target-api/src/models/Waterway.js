import mongoose from 'mongoose';

const WaterwaySchema = mongoose.Schema(
  {
    name: String,
    region: String,
    type: String,
    tags: Array,
    location: {
      type: {
        type: String,
        enum: [
          'Point',
          'MultiPoint',
          'LineString',
          'MultiLineString',
          'Polygon',
          'MultiPolygon',
          'GeometryCollection',
        ],
      },
      coordinates: {
        type: [
          {
            lat: {
              type: Number,
              max: 90.0,
              min: -90.0,
            },
            lng: {
              type: Number,
              max: 180.0,
              min: -180.0,
            },
            _id: false,
          },
        ],
        default: [
          {
            lat: 0,
            lng: 0,
          },
        ],
      },
    },
  },
  { collection: process.env.MONGO_WATERWAYS_COLLECTION },
);

const WaterwayModel = mongoose.model('Waterways', WaterwaySchema);

export const getWaterwayForBoundingBox = (boundingBoxPolygon) =>
  WaterwayModel.findOne({
    location: {
      $geoIntersects: {
        $geometry: {
          type: 'Polygon',
          coordinates: [boundingBoxPolygon],
        },
      },
    },
  });

export default WaterwayModel;
