import chai, { expect, assert } from 'chai';
import sinonChai from 'sinon-chai';
import * as GeoTarget from './geotarget';

chai.should();
chai.use(sinonChai);

describe('geo target service', () => {
  it('should initialize as an object', () => {
    assert(expect(GeoTarget).to.exist);
  });

  it('should validate IP addresses and reject partial IPs', () => {
    assert(!GeoTarget.validate({}));
    assert(!GeoTarget.validate('a'));
    assert(!GeoTarget.validate('8.8.8'));
    assert(GeoTarget.validate('8.8.8.8'));
    assert(!GeoTarget.validate(''));
    assert(!GeoTarget.validate());
  });

  it('should only get partial Location for a partial IP', () => {
    assert(GeoTarget.getLocationForIPAddress('8.8').country.iso_code === 'US');
    assert(!GeoTarget.getLocationForIPAddress('8.8').postal);
  });

  it('should get Location for a US IP, including county', () => {
    const location = GeoTarget.getLocationForIPAddress('8.8.8.8');
    assert(location.postal.code === '94040');
    assert(location.subdivisions[1].name === 'Santa Clara County');
  });

  it('should get Location for a UK IP, including subdivision', () => {
    const location = GeoTarget.getLocationForIPAddress('195.195.237.39');
    assert(location.subdivisions[1].iso_code === 'BNH');
  });

  it('should get Location for an Australian IP, including subdivision', () => {
    const location = GeoTarget.getLocationForIPAddress('110.33.122.75');
    assert(location.subdivisions[0].iso_code === 'NSW');
  });

  it('should get Location for a Brazilian IP, including subdivision', () => {
    const location = GeoTarget.getLocationForIPAddress('201.83.41.11');
    assert(location.subdivisions[0].iso_code === 'SP');
  });

  it("should handle IP addresses which don't return a city", () => {
    const location = GeoTarget.getLocationForIPAddress('74.120.127.220');
    assert(location.subdivisions[0].iso_code === 'TX');
    assert(typeof location.city === 'undefined');
  });
});
