import { expect } from 'chai';
import { getManifest } from './manifestManager';

describe('manifest manager', () => {
  it('should initialize as a function', () => {
    expect(getManifest).to.be.a('function');
  });

  it('should return a object of geotargeted values', () => {
    const manifest = getManifest('us-ca-orangecounty');
    expect(manifest).to.be.an('object');
  });

  it('should normalize casing', () => {
    const manifest = getManifest('IL');
    expect(manifest).to.be.an('object');
  });
});
