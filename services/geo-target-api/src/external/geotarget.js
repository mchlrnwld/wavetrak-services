import { Reader, validate as maxmindValidate } from 'maxmind';
import fs from 'fs';
import log from '../common/logger';

// Setup maxmind connection
const buffer = fs.readFileSync(process.env.GEOIP2_CITY_PATH || './src/external/GeoIP2-City.mmdb');
const reader = new Reader(buffer);

log.debug({
  message: 'Done initializing GeoLite2-City',
  path: process.env.GEOIP2_CITY_PATH || './src/external/GeoIP2-City.mmdb',
});

const usCountyLookupFile = fs.readFileSync('./src/external/us-city-to-county.json');
const usCityGeoNameToCounty = JSON.parse(usCountyLookupFile);

export const validate = (ip) => {
  return maxmindValidate(ip);
};

/**
 * At a minimum, loads continent, country, and location (lat/long/time_zone)
 *
 * Will optionally load city, state, postal, county, and additional subdivisions if available.
 *
 * @param locationRecord
 * @returns {*}
 */
const formatLocationRecord = (locationRecord) => {
  if (
    !locationRecord ||
    !locationRecord.continent ||
    !locationRecord.country ||
    !locationRecord.location
  ) {
    return undefined;
  }

  // Incrementally populating location record from what we have available -- better safe than sorry.
  const formattedLocation = {
    continent: {
      geoname_id: locationRecord.continent.geoname_id,
      name: locationRecord.continent.names.en,
    },
    country: {
      geoname_id: locationRecord.country.geoname_id,
      name: locationRecord.country.names.en,
      iso_code: locationRecord.country.iso_code,
    },
    location: {
      latitude: locationRecord.location.latitude,
      longitude: locationRecord.location.longitude,
      time_zone: locationRecord.location.time_zone,
    },
  };

  if (locationRecord.city) {
    formattedLocation.city = {
      geoname_id: locationRecord.city.geoname_id,
      name: locationRecord.city.names.en,
    };
  }

  if (locationRecord.subdivisions && locationRecord.subdivisions.length > 0) {
    const formattedSubdivisions = locationRecord.subdivisions.map((subdivision) => ({
      geoname_id: subdivision.geoname_id,
      iso_code: subdivision.iso_code,
      name: subdivision.names.en,
    }));
    formattedLocation.subdivisions = formattedSubdivisions;

    if (locationRecord.country.iso_code === 'US' && locationRecord.city) {
      const county = usCityGeoNameToCounty[locationRecord.city.geoname_id.toString()];
      if (county) {
        formattedLocation.subdivisions.push(county);
      }
    }
  }

  if (locationRecord.postal) {
    formattedLocation.postal = locationRecord.postal;
  }

  return formattedLocation;
};

/**
 * The returned location object has the following properties:
 * countryCode, countryName, region, city, latitude, longitude, postalCode, metroCode, dmaCode, areaCode
 *
 * @param ipAddress
 * @return Location || undefined
 */
export const getLocationForIPAddress = (ipAddress) => {
  const locationRecord = reader.get(ipAddress);
  return formatLocationRecord(locationRecord);
};
