import fs from 'fs';
import log from '../common/logger';

const openAndParse = (manifest) => {
  try {
    return JSON.parse(fs.readFileSync(`./manifests/${manifest}.json`, 'utf-8'));
  } catch (err) {
    log.debug(`Manifest file was invalid at: ./manifests/${manifest}.json`);
    return {};
  }
};

export const getManifest = (lookupKey) =>
  lookupKey.split('-').reduce((manifest, cur, index, lookupArr) => {
    const file = lookupArr
      .slice(0, index + 1)
      .join('-')
      .toLowerCase();
    return Object.assign(manifest, openAndParse(`assets-${file}`));
  }, openAndParse('assets'));
