provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/geo-target-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "geo_target_service" {
  source = "../../"

  company     = "sl"
  application = "geo-target-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 6
  service_lb_rules = [
    {
      field = "host-header"
      value = "geo-target-service.prod.surfline.com"
    },
  ]
  iam_role_arn          = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn     = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"
  alb_listener_arn_http = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"

  dns_name    = "geo-target-service.prod.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 625
  auto_scaling_min_size     = 3
  auto_scaling_max_size     = 5000
}
