#!/bin/bash

set -x

trap catch_errors ERR;
function catch_errors() {
  echo "script aborted, because of errors";
  exit 1;
}

# Jenkins job for poll-maxmind-database
# This is part of the Jenkins job here:
# https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/poll-maxmind-database/

# Description:  Pull down maxmind db and saved version in s3
# compare them and if maxmind is different, upload to s3 and trigger geo-target build job.

readonly GEOIP_LICENSE_KEY=JLLNWywpG6TK

# Make sure a TMPDIR exists to save and compare files.
TMPDIR=$(mktemp -d) || exit 1;

# Pull down the maxmind db from the source
curl -o ${TMPDIR}/GeoIP2-City.tar.gz.md5 "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz.md5&license_key=${GEOIP_LICENSE_KEY}"

eval $(assume-role prod)

# Copy most recently saved db hash from S3
aws s3 cp s3://sl-artifacts-prod/maxmind/GeoIP2-City.tar.gz.md5 ${TMPDIR}/GeoIP2-CityS3.tar.gz.md5

# Test if databases differ
md5_from_s3="$(cat ${TMPDIR}/GeoIP2-CityS3.tar.gz.md5)"
md5_from_server="$(cat ${TMPDIR}/GeoIP2-City.tar.gz.md5)"

if [[ "${md5_from_server}" != "${md5_from_s3}" ]]; then
  echo "Download latest maxmind db for upload to s3"
  # Pull down the maxmind db from the source
  curl -o ${TMPDIR}/GeoIP2-City.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz&license_key=${GEOIP_LICENSE_KEY}"

  mkdir ${TMPDIR}/GeoIP2-City
  tar -xvzf ${TMPDIR}/GeoIP2-City.tar.gz --strip-components=1 -C ${TMPDIR}/GeoIP2-City
  mv -f ${TMPDIR}/GeoIP2-City/GeoIP2-City.mmdb ${TMPDIR}/GeoIP2-City.mmdb

  echo "No match, copy updated Maxmind db to s3"
  aws s3 cp ${TMPDIR}/GeoIP2-City.mmdb s3://sl-artifacts-prod/maxmind/GeoIP2-City.mmdb --acl public-read
  echo "No match, copy updated Maxmind db hash s3"
  aws s3 cp ${TMPDIR}/GeoIP2-City.tar.gz.md5 s3://sl-artifacts-prod/maxmind/GeoIP2-City.tar.gz.md5

  echo "Kick off the deploy of geo-target service to pick up new database, using post build action"
  rm -rf ${TMPDIR} 2>/dev/null
  exit 0;

else
  echo "The Maxmind db from source and S3 are the same"
  # exit 1 so that triggered build does not fire off.
  rm -rf ${TMPDIR} 2>/dev/null
  exit 1;
fi
