# Geo target service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Region](#region)
- [Waterways](#waterways)
- [Example request](#example-request)
    - [Example response](#example-response)
  - [Discussion](#discussion)
- [Geo-Targeting Asset Service](#geo-targeting-asset-service)
  - [`GET /assets/resource`](#get-assetsresource)
  - [`GET /assets/manifest`](#get-assetsmanifest)
    - [Arguments](#arguments)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Configure global environment
`nvm use` (honors `.nvmrc`)

Configure `.env` appropriately (.env.sample provided)

Configuring the project
`npm install`

Testing the project
`npm run test`

Download fresh maxmind GeoLite v2 from [Maxmind](http://dev.maxmind.com/geoip/geoip2/geolite2/). This repository contains a copy for testing and dev, but it is **not** for production use.


## Region

To get a location for an IP, you should do a GET to /Region with ipAddress as query parameter, for example:

    GET /Region?ipAddress=8.8.8.8 HTTP/1.1
    Host: server.example.com

The expected minimum response set is Continent, Country, and Location. Some IP addresses don't reference additional information.

And the response will be..

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
      "continent": {
        "geoname_id": 6255148,
        "name": "Europe"
      },
      "country": {
        "geoname_id": 2635167,
        "name": "United Kingdom",
        "iso_code": "GB"
      },
      "location": {
        "latitude": 50.8441,
        "longitude": -0.1113,
        "time_zone": "Europe/London"
      },
      "city": {
        "geoname_id": 2654710,
        "name": "Brighton"
      },
      "subdivisions": [
        {
          "geoname_id": 6269131,
          "iso_code": "ENG",
          "name": "England"
        },
        {
          "geoname_id": 3333133,
          "iso_code": "BNH",
          "name": "Brighton and Hove"
        }
      ],
      "postal": {
        "code": "BN2"
      }
    }


## Waterways

```bash
GET /Waterways
```

| Query Param    | Type      | Required | Description                                            |
| :---------- | :-------- | :------- | :----------------------------------------------------- |
| `lat`     | `Number`  | Yes      | The latitudinal coordinate point                                       |
| `lon`     | `Number`  | Yes      | The longitudinal coordinate point                              |

## Example request

```bash
GET /Waterways?lat=49.41&lon=-123.99
```

#### Example response
```
{
  "waterways": {
    "name": "Vancouver - Seattle",
    "type": "Bay",
    "tags": [
      "Inside Passage",
      "Georgia Strait"
    ]
  }
}
```


### Discussion ###
We are using a GeoIP2 binary file and library to quickly convert an IP to geographic data (<1ms).

We also need county information. Online services cost $$$, so we are trying to avoid that.

In a pinch, we could use FCC's [Census Tract Lookup](https://www.fcc.gov/general/census-block-conversions-api) for the US. But, this API's response time is easily an entire second.

We can use a [GeoNames](http://download.geonames.org/export/dump/) lookup for additional subdivision data.

For right now, since we are only filling in US County data, we can statically load US City -> County mappings. Check out the `data-dump` directory for how we are loading from GeoNames TSV files. We are referencing a generated file in src/external/us-city-to-county.json.

## Geo-Targeting Asset Service ##

### `GET /assets/resource` ###
| Parameter | Required | Description |
| --------- | -------- | ----------- |
| `idAddress` | no | the ip address used to geo target the images |
| `key` | yes | asset lookup key |


    GET /assets/resource?key=[key] HTTP/1.1 301 Moved Permanently
    Location: https://drcb7kk0emt05.cloudfront.net/default-sign-in-background-2.jpg


### `GET /assets/manifest` ###

#### Arguments ####
| Parameter | Required | Description |
| --------- | -------- | ----------- |
| `idAddress` | yes | the ip address used to geo target the images |


    GET /assets/manifest?ipAddress=8.8.8.8 HTTP/1.1
    Host: server.example.com
    {
        "sign-in-background": [
            "default-sign-in-background-1.jpg",
            "folder/default-sign-in-background-2.jpg"
        ],
        "create-account-background": "somesub-folder/create-background.jpg"
    }

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `geo-target-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTVRNMU5EQTFNemMifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME1UTTFOREExTXpjIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Example Request](#waterways) section. 

**Ex:**

Test `GET` requests against the `waterways` endpoint:

```
curl \
    -X GET \
    -i \
    http://geo-target-service.sandbox.surfline.com/Waterways?lat=49.41&lon=-123.99
```
**Expected response:** See [above](#example-response) for sample response.
