# Subregions Import

## Installation

```sh
npm install
```

## Usage

Set the following environment variables:
- `MONGO_HOST`
- `MONGO_PORT`
- `MONGO_DATABASE`
- `MONGO_BASINS_COLLECTION`

```sh
npm start
```
