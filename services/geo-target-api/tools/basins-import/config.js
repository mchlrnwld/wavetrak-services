require('dotenv').config();

export default {
  mongo: {
    connectionString: process.env.MONGO_CONNECTION_STRING_KBYG,
    collection: {
      basins: process.env.MONGO_BASINS_COLLECTION,
    },
  },
  jsonFile: './basins.json',
  geoNamesApi: 'http://www.geonames.org/servlet/geonames'
};
