import fetch from 'node-fetch';
import createThrottle from 'async-throttle';
import mongoose from 'mongoose';
import config from './config';
import basinList from './geonames-basins';

const throttle = createThrottle(4);
const fetchBasins = async () => Promise.all(await basinList.map((basin, index) => throttle(async () => { 
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(`Fetching basin # ${index} ${basin.name}`);
    const theFetch = await fetch(`${config.geoNamesApi}?&srv=780&geonameId=${basin.id}&type=json`);
    const theData = await theFetch.json();
    return {
      location: basin.location || theData.shapes[0].geoJson,
      name: basin.name
    }
  })
));

const importBasinsMongoose = basins => new Promise((resolve, reject) => {
  const mongoDbConfig = {server: { poolSize: 50, socketOptions: { keepAlive: 3000000, connectTimeoutMS: 300000 } }};
  const db = mongoose.connect(config.mongo.connectionString, mongoDbConfig);

  const Basin = mongoose.model(config.mongo.collection.basins, new mongoose.Schema({
    name: String,
    location: {
      type: { 
        type: String, 
        enum: ['Polygon', 'MultiPolygon']
      },
      coordinates: { 
        type: [{
          lng:{
            type:Number,
            max:90.0,
            min:-90.0
          },
          lat:{
            type:Number,
            max:180.0,
            min:-180.0
          },
          _id:false
        }],   
        default: [{
          lat:0,
          lng:0
        }]
      }
    }
  }, { collection: config.mongo.collection.basins}));

  console.log("Removing old basins...");
  Basin.collection.remove({});

  console.log("Applying 2dsphere index...");
  Basin.collection.ensureIndex({ location: '2dsphere'});

  const startImport = new Date();

  Basin.collection.insertMany(basins, function(err,r) {
    const endImport = new Date();
    if (err) {
      console.log("ERROR: ", err);
    } else {
      console.log(`Imported ${basins.length} basins into mongo in ${endImport - startImport}ms`);
    }
    db.connection.close();
  })
});

(async () => {
  console.log('Fetching basins from geonames...');
  const startFetch = new Date();
  const basins = await fetchBasins();
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  const endFetch = new Date();
  process.stdout.write(`Fetched ${basins.length} basins in ${endFetch - startFetch}ms`);
  process.stdout.write("\n");
  console.log('Importing basins to mongo...');
  importBasinsMongoose(basins);

})();