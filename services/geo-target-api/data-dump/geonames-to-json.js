var fs = require('fs'),
  _ = require('underscore');

var cityFilename = process.argv[2];
if (!cityFilename) {
  console.log('City file not found');
  process.exit(1);
}

var admin2Filename = process.argv[3];
if (!admin2Filename) {
  console.log('Admin2 Lookup file not found');
  process.exit(1);
}

// Format : concatenated codes <tab>name <tab> asciiname <tab> geonameId
var admin2 = fs
  .readFileSync(admin2Filename)
  .toString()
  .split('\n');
admin2.pop(); // empty element
var processedAdmin2 = {};
_.each(admin2, function(admin2Entry) {
  var data = admin2Entry.split('\t');
  processedAdmin2[data[0]] = {
    name: data[1],
    geoname_id: data[3],
  };
});

/**
 *
 * The main 'geoname' table has the following fields :
   ---------------------------------------------------
   0  geonameid         : integer id of record in geonames database
   1  name              : name of geographical point (utf8) varchar(200)
   2  asciiname         : name of geographical point in plain ascii characters, varchar(200)
   3  alternatenames    : alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table, varchar(10000)
   4  latitude          : latitude in decimal degrees (wgs84)
   5  longitude         : longitude in decimal degrees (wgs84)
   6  feature class     : see http://www.geonames.org/export/codes.html, char(1)
   7  feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
   8  country code      : ISO-3166 2-letter country code, 2 characters
   9  cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 200 characters
   10 admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
   11 admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80)
   12 admin3 code       : code for third level administrative division, varchar(20)
   13 admin4 code       : code for fourth level administrative division, varchar(20)
   14 population        : bigint (8 byte int)
   15 elevation         : in meters, integer
   16 dem               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
   17 timezone          : the timezone id (see file timeZone.txt) varchar(40)
   18 modification date : date of last modification in yyyy-MM-dd format
 * @type {Array}
 */

var cities = fs
  .readFileSync(cityFilename)
  .toString()
  .split('\n');
cities.pop(); // empty element
var processedCities = {};
_.each(cities, function(city) {
  var data = city.split('\t');
  if (data[8] === 'US') {
    // Mapping geoname_id of US City to its County information
    processedCities[Number(data[0])] = processedAdmin2[data[8] + '.' + data[10] + '.' + data[11]];
  }
});

// Formatted for a mongo insert
// var processedCities = _.map(cities, function(city) {
//   var data = city.split('\t');
//   return {
//     geoname_id: Number(data[0]),
//     name: data[1],
//     // country_code: data[8],
//     // admin1_code: data[10],
//     // admin2_code: data[11],
//     // admin2_lookup: data[8] + '.' + data[10] + '.' + data[11],
//     admin2: processedAdmin2[data[8] + '.' + data[10] + '.' + data[11]],
//     coordinates: [Number(data[4]), Number(data[5])]
//   }
// });

fs.writeFileSync('cities.json', JSON.stringify(processedCities));
