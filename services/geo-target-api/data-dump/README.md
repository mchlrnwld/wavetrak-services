### Geonames cities to JSON ###

Generate cities data

    wget http://download.geonames.org/export/dump/cities15000.zip
    unzip cities15000.zip
    node geonames-to-json.js cities15000.txt

The result is json array of cities in the following format:

    {
        _id: <ID>
        name: <Name>,
        alternate_names: [<alternatename1>,…],
        coordinates: [<Latitude>, <Longitude>]
    }

#### Import to MongoDB ####

    mongoimport -d db_name -c cities cities.json --jsonArray