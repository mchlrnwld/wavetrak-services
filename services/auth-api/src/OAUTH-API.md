# OAuth2 API

## Overview
OAuth2 enabled Surfline APIs use the [OAuth 2.0 protocol](https://tools.ietf.org/html/rfc6749) for Client authentication and authorization.  The common OAuth 2.0 scenarios allow Client applications to make permission based API requests on behalf of the authorizing user.

## Authorization Workflow
The following steps attempt to detail the OAuth 2.0 workflow as it is implemented within the Auth-Service.

### Step 1 - Client Registration
Registered Clients are represented by a corresponding document in the `ClientIds` mongo collection.  Each Client contains the following fields:
 - `_id <ObjectId>`
 - `name <String>`
 - `description <String>`
 - `secret <String>`
 - `redirectURI <String (valid URL)>`
 - `scopes <Array (ObjectId)>`

`Scopes` represent the types of actions a Client is permitted to perform.  Surfline API scopes follow the standarized `Object:Class:Perspective` convention.  For example, a Scope permitting a Client to send session data to Surfline on a user's behalf would be defined as `sessions:write:user`.  Scope authorization is per user, and persists throughout the life of the an authorized access token.  Each Scope contains the following fields:
 - `_id <ObjectId>`
 - `name <String>`
 - `description <String>`


### Step 2 - User Authorization
Clients requesting authorization need to direct Users the `GET /connect/authorize` endpoint.  The following URL parameters should to be included with this request:
 - `redirect_uri` - a valid URL to redirect back after authorization
 - `response_type` - a value of "code" representing the type of authorization requested
 - `client_id` - the `_id` of the requesting client
 - `scope` - a URL encoded space separated list of scope names

Prior to User Authorization, the request params are sent through the Oauth2 Authorization middleware.  Here the `client_id` is validated against the `ClientIds` collection.  If either the `clientId`, or the `redirect_uri` received in the request parameters does not match the respective value on the Client document; and if no authorized `scopes` exist, then the request will be denied.

### Step 3 - Authorized Redirect to Client Server
WIP
### Step 4 - Authorization Code Exchange
WIP
## Denied Authorization
WIP
## Refreshing Tokens
WIP
## Using Access Tokens
WIP
## Resources
[The OAuth 2.0 Authorization Framework](https://tools.ietf.org/html/rfc6749)
