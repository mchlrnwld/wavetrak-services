import request from 'request';
import { authError, serverError } from '../common/customErrors';
import * as logger from '../common/logger';

const log = logger.logger('auth-service:facebool');

const doFetch = (uri, params) =>
  new Promise((resolve, reject) => {
    const options = {
      uri,
      ...params,
    };
    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) return resolve(body);
      if (response && response.statusCode === 400) return reject(authError(body.message));
      log.error({
        message: 'facebook auth-service error',
        stack: error,
      });
      return reject(serverError());
    });
  });

const getFBUser = async (accesstoken) =>
  doFetch(
    'https://graph.facebook.com/v3.1/me?' +
      'fields=id,email,first_name,last_name' +
      `&access_token=${accesstoken}`,
    { method: 'GET', json: true },
  );

const createIntegratedUser = async (details) => {
  const response = await doFetch(`${process.env.USER_SERVICE}/user/CreateIntegratedUser`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: details,
    json: true,
  });

  // The `CreateIntegratedUser` response returns a different contract based on
  // if a new user account was created, or an existing one was used.
  //
  // For new user accounts the response contains
  //    { user: { _id, email ... } }
  //
  // For existing user accounts the response is
  //    { _id, email, ... }
  const user = response.user ? response.user : response;

  return {
    _id: user._id,
    email: user.email,
  };
};

export const lookupOrCreateUserByFBAccessToken = async (at) => {
  const { id, email, first_name: firstName, last_name: lastName } = await getFBUser(at);
  const user = await createIntegratedUser({
    email,
    firstName,
    lastName,
    linkedAccount: { facebook: { id } },
  });

  return user;
};
