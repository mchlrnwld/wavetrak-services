import request from 'request';
import * as logger from '../common/logger';
import { authError, serverError } from '../common/customErrors';

const log = logger.logger('auth-service:user');

/**
 * To find a user by facebook id
 * You should include the facebook id as ("linkedAccount")
 * as query param
 * -
 * GET /user/ HTTP/1.1
 * Host: server.example.com
 * Content-Type: application/json
 * linkedAccount=19806033754720832
 * -
 * And the response will be..
 * -
 * HTTP/1.1 200 OK
 * Content-Type: application/json;charset=UTF-8
 * Cache-Control: no-store
 * Pragma: no-cache
 * {
 * "_id": "582cb3c3d7e44b1074114b66",
 * "firstName": "First",
 * "lastName": "Last",
 * "email": "first@test.com",
 * "usID": 1004388,
 * "__v": 0,
 * "postal": "11111",
 * "linkedAccount": {
 *   "facebook": {
 *     "id": "19806033754720832"
 *   }
 * },
 * "locale": "en-US",
 * "brand": "sl",
 * "modifiedDate": "2016-12-09T19:41:28.574Z",
 * "isActive": true
 * }
 *
 * @param linkedAccount
 * @returns {Promise.<T>}
 */
export const findUserByLinkedAccount = (linkedAccountId) =>
  new Promise((resolve, reject) => {
    const options = {
      /**
       * NOTE: the PROXY_URL value is no longer used,
       * as this function has been deprecated.
       */
      uri: `${process.env.PROXY_URL}/user?linkedAccountFb=${linkedAccountId}`,
      method: 'GET',
      json: true,
    };

    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) {
        resolve(body);
      } else {
        log.error({
          message: `findUserByLinkedAccount(${linkedAccountId})`,
          stack: error,
        });
        reject(response);
      }
    });
  });

/**
 * To update or create a user that does not yet have a facebook linkedAccount
 * You should include userInfo in the body
 * userInfo should be an object {
 *   email,
 *   firstName,
 *   lastName,
 *   linkedAccount {
 *     facebook: {
 *       id
 *     }
 *   },
 *   brand
 * }
 * -
 * POST /user/CreateIntegratedUser HTTP/1.1
 * Host: server.example.com
 * Content-Type: application/json
 * -
 * And the response will be..
 * -
 * HTTP/1.1 200 OK
 * Content-Type: application/json;charset=UTF-8
 * Cache-Control: no-store
 * Pragma: no-cache
 * {
 * "_id": "582cb3c3d7e44b1074114b66",
 * "firstName": "First",
 * "lastName": "Last",
 * "email": "first@test.com",
 * "usID": 1004388,
 * "__v": 0,
 * "postal": "11111",
 * "linkedAccount": {
 *   "facebook": {
 *     "id": "19806033754720832"
 *   }
 * },
 * "locale": "en-US",
 * "brand": "sl",
 * "modifiedDate": "2016-12-09T19:41:28.574Z",
 * "isActive": true
 * }
 *
 * @param userInfo
 * @returns {Promise.<T>}
 */
export const createOrUpdateIntegratedUser = (userInfo) =>
  new Promise((resolve, reject) => {
    const options = {
      /**
       * NOTE: the PROXY_URL value is no longer used,
       * as this function has been deprecated.
       */
      uri: `${process.env.PROXY_URL}/user/CreateIntegratedUser`,
      method: 'POST',
      body: userInfo,
      json: true,
    };
    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) {
        resolve(body);
      } else {
        log.error({
          message: `createOrUpdateIntegratedUser(${userInfo})`,
          stack: error,
        });
        reject(response);
      }
    });
  });

export const createUser = (user) =>
  new Promise(async (resolve, reject) => {
    const { firstName, lastName, password } = user;
    const userBody = {
      firstName,
      lastName,
      password,
      email: user.email.trim().toLowerCase(),
      settings: {
        receivePromotions: false,
      },
    };
    const options = {
      uri: `${process.env.USER_SERVICE}/user`,
      method: 'POST',
      body: userBody,
      json: true,
    };
    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) return resolve(body);
      if (response && response.statusCode === 400) return reject(authError(body.message));
      log.error({
        message: `createUser(${userBody})`,
        stack: error,
      });
      return reject(serverError());
    });
  });
