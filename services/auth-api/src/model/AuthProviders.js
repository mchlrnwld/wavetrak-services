import mongoose from 'mongoose';
import { NotFound, ServerError } from '@surfline/services-common';

const { Schema } = mongoose;

const AuthProvidersSchema = Schema(
  {
    client: { type: Schema.Types.ObjectId, ref: 'ClientId' },
    spec: { type: String, enum: ['oauth1'] },
    requestTokenURL: String,
    accessTokenURL: String,
    userAuthorizationURL: String,
    consumerKey: String,
    consumerSecret: String,
    callbackURL: String,
  },
  { collection: 'AuthProviders' },
);

AuthProvidersSchema.index({ client: 1, spec: 1 });
AuthProvidersSchema.index({ spec: 1 });

const AuthProviders = mongoose.model('AuthProviders', AuthProvidersSchema);

/**
 * @description Given a client ID and a spec (only `oauth1` for now)
 * this function performs a mongo query to get the `AuthProvider` and
 * also populate the matching `ClientIds` document on the `client` property
 * @param {string} clientId
 * @param {'oauth1'} [spec="oauth1"]
 * @returns {Promise}
 */
export const getProviderWithClient = async (clientId, spec = 'oauth1') => {
  const provider = await AuthProviders.findOne({ client: clientId, spec }).populate('client');

  if (!provider) {
    throw new NotFound('Provider does not exist');
  }

  // If the client was not found, we won't have access to the name and _id which we need
  if (provider && !provider.client) {
    throw new ServerError({ message: 'Client document was not found' });
  }

  // We need access to the Client Name is on order to reference the correct passport strategy
  if (provider && !provider.client.name) {
    throw new ServerError({ message: 'Client document was missing a name' });
  }

  return provider;
};

export default AuthProviders;
