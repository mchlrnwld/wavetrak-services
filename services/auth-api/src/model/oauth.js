import newrelic from 'newrelic';
import { difference, keyBy } from 'lodash';
import passport from 'passport';
import OAuth1Strategy from 'passport-oauth1';
import mongoose from 'mongoose';
import oauth2orize from 'oauth2orize';
import util from 'util';

import { ServerError, APIError, NotFound, getPlatform } from '@surfline/services-common';
import { redisPromise, cacheClient } from '../common/dbContext';
import {
  createToken,
  formatTokenDataForRedis,
  calculateTokenExpiry,
  validateBasicAuthRequest,
  tokenKeys,
} from '../common/utils';
import { createUser } from '../external/user';
import { tokenError, authError, serverError } from '../common/customErrors';
import * as logger from '../common/logger';
import * as analytics from '../common/analytics';
import { lookupOrCreateUserByFBAccessToken } from '../external/facebook';
import { checkUserCredentials, saveToken } from './user';
import model from './auth';
import deleteFacebookToken from '../integrations/facebook/deleteFacebookToken';
import UserInfo from './UserInfo';
import AuthProviders, { getProviderWithClient } from './AuthProviders';
import ClientId from './ClientId';

const log = logger.logger('auth-service:model:OAuth');
const { ObjectId } = mongoose.Types;

export async function getClientName(clientId) {
  try {
    const { name, appImageURL, _id } = await ClientId.findOne({ _id: clientId });
    return { name, appImageURL, _id };
  } catch (error) {
    throw new Error('invalid client');
  }
}

async function pruneUserTokens(clientId, userId, bearerToken, refreshToken) {
  try {
    const userToken = `user_tokens:${userId}:${clientId}`;
    const userTokenKeys = await redisPromise().hgetall(userToken);
    const todayInSeconds = new Date() / 1000;

    for (const token in userTokenKeys) {
      if (userTokenKeys[token] < new Date() / 1000) {
        await redisPromise().hdel(userToken, token);
        await redisPromise().del(token);
      }
    }
    const userTokenKeyTtl = todayInSeconds + 2592000; // 30 days from today;
    return await redisPromise().hmset(userToken, [
      `bearer_token:${bearerToken}`,
      userTokenKeyTtl,
      `refresh_token:${refreshToken}`,
      userTokenKeyTtl,
    ]);
  } catch (error) {
    // todo log errors
    return serverError('Error pruning userToken');
  }
}

export async function deserializeUser(id, done) {
  try {
    const user = await UserInfo.findOne({ _id: id });
    if (!user) return done('user not found');
    return done(null, user._id);
  } catch (error) {
    return done(serverError());
  }
}

export async function deserializeClient(deserializedClient, done) {
  try {
    const client = await ClientId.findOne({
      _id: deserializedClient.id,
    });
    if (!client) return done(authError());
    return done(null, deserializedClient);
  } catch (error) {
    return done(serverError());
  }
}

export async function authorizeClient(clientId, redirectURI, scopes, done) {
  try {
    const clientLookup = await ClientId.aggregate([
      { $match: { _id: ObjectId(clientId) } },
      { $project: { secret: 0 } },
      {
        $lookup: {
          from: 'Scopes',
          localField: 'scopes',
          foreignField: 'name',
          as: 'scopes',
        },
      },
    ]);
    const client = clientLookup[0];
    if (!client) return done(authError());
    // if (redirectURI !== client.redirectURI) return done(authError());
    if (!scopes.length) return done(authError('No scopes found'));
    const invalidScopes = difference(
      scopes,
      client.scopes.map(({ name }) => name),
    );
    if (invalidScopes.length > 0) return done(authError('Unauthorized scope'));

    const validScopes = keyBy(client.scopes, (scope) => scope.name);

    const serializedClient = {
      id: client._id,
      name: client.name,
      scopes: scopes.map((scope) => validScopes[scope]),
      appImageURL: client.appImageURL,
    };
    return done(null, serializedClient, redirectURI);
  } catch (error) {
    return done(serverError());
  }
}

export async function validateClient(clientId) {
  try {
    const clientLookup = await ClientId.aggregate([
      { $match: { _id: ObjectId(clientId), trusted: true } },
      { $project: { secret: 0 } },
      {
        $lookup: {
          from: 'Scopes',
          localField: 'scopes',
          foreignField: 'name',
          as: 'scopes',
        },
      },
    ]);
    const client = clientLookup[0];

    if (!client) {
      throw authError('Unauthorized Client.', 403, 403);
    }

    const scopes = client.scopes.map(({ name }) => name).join();
    return scopes;
  } catch (error) {
    log.error({
      model: 'validateClient',
      message: 'Error validating client',
      clientId,
      error: JSON.stringify(error),
    });
    if (error instanceof oauth2orize.AuthorizationError) {
      throw error;
    }
    throw serverError();
  }
}

export async function facebookAuthorization(req, done) {
  const { access_token: accessToken } = req.body;
  try {
    const user = await lookupOrCreateUserByFBAccessToken(accessToken);
    return done(null, user);
  } catch (err) {
    return done('Could not connect facebook account');
  }
}

export async function createAccountAuthorization(req, done) {
  try {
    const { email, password } = req.body;
    const formattedEmail = email.trim().toLowerCase();
    const isValidEmail = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/.test(
      formattedEmail.toUpperCase(),
    );
    if (!isValidEmail) {
      return done(authError('Malformed email provided'));
    }

    if (password.length < 6) {
      return done(authError('Password must contain 6 or more characters.'));
    }

    const { user } = await createUser(req.body);
    return done(null, user);
  } catch (err) {
    log.error({
      model: 'createAccountAuthorization',
      message: 'Error creating account',
      err,
    });
    if (err.status === 400) return done(err);
    return done(serverError());
  }
}

export async function localAuthorization(email, password, done) {
  try {
    const user = await checkUserCredentials(email, password);
    return done(null, user);
  } catch (err) {
    return done(err);
  }
}

export async function basicAuthorization(req, clientId, clientSecret, done) {
  try {
    await validateBasicAuthRequest(req);
    const client = await ClientId.findOne({ _id: clientId });
    if (!client) return done(authError());
    if (clientSecret !== client.secret) {
      return done(authError('Unauthorized Client', 'unauthorized_client'));
    }
    return done(null, client);
  } catch (error) {
    return done(error);
  }
}

export async function bearerAuthorization(accessToken, apiKey, isTrusted = false, done) {
  try {
    if (apiKey) {
      const client = await ClientId.findOne({ _id: apiKey });
      if (!client) return done(authError('Unauthorized Client', 'unauthorized_client'));
    }
    const bearerToken = await redisPromise().hgetall(`bearer_token:${accessToken}`);
    if (bearerToken) {
      const { email, name, title, clientId } = bearerToken;
      const logData = {
        userId: bearerToken?.userId,
        accessToken: accessToken?.substring(6),
      };
      // Specifically checking for 'authclientId' since
      // ObjectId.isValid('authclientId') returns true.
      if (isTrusted && ObjectId.isValid(clientId) && clientId !== 'authclientId') {
        const { trusted = false } = await ClientId.findOne({ _id: clientId });
        if (!trusted) {
          log.error(logData, 'Unauthorized Client - not trusted');
          return done(authError('Unauthorized Client', 'unauthorized_client'));
        }
      }
      newrelic.addCustomAttribute('userId', bearerToken?.userId);
      log.info(logData, 'accessToken validated');
      return done(null, {
        userId: bearerToken.userId,
        scopes: bearerToken.scopes,
        email,
        name,
        title,
      });
    }

    const bearerTokenOld = await redisPromise().hgetall(`tokens:${accessToken}`);
    if (bearerTokenOld) {
      // Delete token in old format
      await model.deleteAccessToken(accessToken);
      const { tokenExpires } = calculateTokenExpiry('bearerToken');
      const data = {
        clientId: bearerTokenOld.clientId,
        userId: bearerTokenOld.userId,
        scopes: bearerTokenOld.scopes || 'all',
        deviceId: bearerTokenOld.deviceId || '',
        deviceType: bearerTokenOld.deviceType || '',
        expires: tokenExpires.toISOString(),
        token: accessToken,
      };
      const tokenData = formatTokenDataForRedis('bearerToken', data);
      // Store token in new format
      const tokenResult = await saveToken('bearerToken', accessToken, tokenExpires, tokenData);
      newrelic.addCustomAttribute('userId', bearerTokenOld?.userId);
      return done(null, {
        ...tokenResult,
      });
    }

    const userId = await redisPromise().get(`fb-token:${accessToken}`);
    if (userId) {
      // Delete token in old format
      await deleteFacebookToken(accessToken);
      const { tokenExpires } = calculateTokenExpiry('bearerToken');
      const data = {
        clientId: 'authclientId',
        userId,
        scopes: 'all',
        expires: tokenExpires.toISOString(),
        token: accessToken,
      };
      const tokenData = formatTokenDataForRedis('bearerToken', data);
      // Store token in new format
      const tokenResultFB = await saveToken('bearerToken', accessToken, tokenExpires, tokenData);
      newrelic.addCustomAttribute('userId', userId);
      return done(null, {
        ...tokenResultFB,
      });
    }
    log.info({ accessToken: accessToken?.substring(6) }, 'accessToken does not exist');
    // If reached here then the token do not exist
    return done(tokenError());
  } catch (error) {
    log.error({
      model: 'bearerAuthorization',
      message: 'Error authorizing Token',
      accessToken: accessToken?.substring(6),
      error: JSON.stringify(error),
    });
    return done(serverError());
  }
}

export async function grantAuthCode(client, redirectURI, userId, done) {
  try {
    const code = await createToken();
    const scopes = client.scopes.map(({ name }) => name).join(',');
    await redisPromise().hmset(`authorization_code:${code}`, [
      'redirectURI',
      redirectURI,
      'clientId',
      client.id.toString(),
      'userId',
      userId.toString(),
      'scopes',
      scopes,
    ]);
    await redisPromise().expire(`authorization_code:${code}`, 1200); // 20 minutes

    await UserInfo.updateOne(
      { _id: userId },
      { $pull: { linkedClients: { clientId: client.id } } },
    );
    await UserInfo.updateOne(
      { _id: userId },
      {
        $push: {
          linkedClients: {
            clientId: client.id,
            scopes: client.scopes,
            createdAt: new Date(),
          },
        },
      },
    );
    analytics.track('Connected Account', userId.toString(), { partner: client.name });
    return done(null, code);
  } catch (error) {
    return done(serverError());
  }
}

export async function exchangeAuthCode(client, code, redirectURI, done) {
  try {
    const authorizedCode = await redisPromise().hgetall(`authorization_code:${code}`);
    if (!authorizedCode) return done(tokenError());
    if (
      client._id.toString() !== authorizedCode.clientId
      // || redirectURI !== authorizedCode.redirectURI
    ) {
      return done(authError());
    }
    const bearerTokenTtl = 2592000; // 30 days
    const bearerToken = await createToken();
    const refreshToken = await createToken();
    const { clientId, userId, scopes } = authorizedCode;
    const hashSet = ['clientId', clientId, 'userId', userId, 'scopes', scopes];

    await redisPromise().hmset(`bearer_token:${bearerToken}`, hashSet);
    await redisPromise().expire(`bearer_token:${bearerToken}`, bearerTokenTtl);
    await redisPromise().hmset(`refresh_token:${refreshToken}`, hashSet);
    await redisPromise().del(`authorization_code:${code}`);

    await pruneUserTokens(clientId, userId, bearerToken, refreshToken);

    return done(null, bearerToken, refreshToken, { expires_in: bearerTokenTtl });
  } catch (error) {
    return done(serverError());
  }
}

export async function exchangeRefreshToken(client, refreshToken, scope, done) {
  try {
    const validRefreshToken = await redisPromise().hgetall(`refresh_token:${refreshToken}`);
    if (!validRefreshToken) return done(tokenError());
    if (client._id.toString() !== validRefreshToken.clientId) return done(authError());
    const bearerToken = await createToken();
    const newRefreshToken = await createToken();

    const { clientId, userId, scopes } = validRefreshToken;
    const hashSet = ['clientId', clientId, 'userId', userId, 'scopes', scopes];
    await redisPromise().hmset(`bearer_token:${bearerToken}`, hashSet);
    await redisPromise().expire(`bearer_token:${bearerToken}`, 2592000); // 30 days
    await redisPromise().hmset(`refresh_token:${newRefreshToken}`, hashSet);
    await redisPromise().del(`refresh_token:${refreshToken}`);

    await pruneUserTokens(clientId, userId, bearerToken, refreshToken);

    return done(null, bearerToken, newRefreshToken, { expires_in: 2592000 });
  } catch (error) {
    return done(serverError());
  }
}

/**
 * Extension of connect-ensure-login
 * https://www.npmjs.com/package/connect-ensure-login
 * version 0.1.1
 *
 * Extending this lib allows us to append a query param
 * with the client name to the redirect url.  This param
 * is used to render the client's name and logo in the
 * OAuth2 login view.
 *
 * @param {Object||String} options
 */
export function ensureLoggedIn(options) {
  let redirectOptions;
  if (typeof options === 'string') {
    redirectOptions = { redirectTo: options };
  }
  redirectOptions = options || {};
  const url = redirectOptions.redirectTo || '/login';
  const setReturnTo =
    redirectOptions.setReturnTo === undefined ? true : redirectOptions.setReturnTo;

  return async (req, res, next) => {
    if (!req.isAuthenticated || !req.isAuthenticated()) {
      if (setReturnTo && req.session) {
        // eslint-disable-next-line no-param-reassign
        req.session.returnTo = req.originalUrl || req.url;
      }
      return res.redirect(`${url}?client=${req.query.client_id}`);
    }
    return next();
  };
}

export async function exchangeClientCredentials(client, scope, reqBody, done) {
  try {
    const { _id: clientId } = client;
    const { id, email, name, title } = reqBody;
    const scopesList = scope.join();
    // const bearerTokenLifeTime = 604800; // 1 week
    const hashSet = [
      'clientId',
      clientId.toString(),
      'userId',
      id,
      'email',
      email,
      'name',
      name,
      'title',
      title,
      'scopes',
      scopesList,
    ];
    const bearerToken = await createToken();
    await redisPromise().hmset(`bearer_token:${bearerToken}`, hashSet);
    // Admin Tokens won't get expired in redis
    // await redisPromise().expire(`bearer_token:${bearerToken}`, bearerTokenLifeTime);
    const userToken = `user_tokens:${id}:${clientId.toString()}`;
    await redisPromise().set(userToken, bearerToken);

    return done(null, bearerToken);
  } catch (err) {
    log.error({
      model: 'exchangeClientCredentials',
      message: 'Error creating admin tokens',
      err,
    });
    return done(err);
  }
}

async function generateTokens(
  type = 'password',
  clientId,
  userId,
  scopes,
  deviceId = null,
  deviceType = null,
  isShortLived = false,
  done,
) {
  try {
    const bearerToken = await createToken();
    const refreshToken = await createToken();
    const {
      tokenExpires: bearerTokenExpires,
      tokenLifetime: bearerTokenLifetime,
    } = calculateTokenExpiry('bearerToken', isShortLived);
    const {
      tokenExpires: refreshTokenExpires,
      tokenLifetime: refreshTokenLifetime,
    } = calculateTokenExpiry('refreshToken', isShortLived);
    const data = {
      clientId,
      userId,
      scopes,
      deviceId,
      deviceType,
    };
    const btData = {
      ...data,
      token: bearerToken,
      expires: bearerTokenExpires ? bearerTokenExpires.toISOString() : null,
    };
    const bearerTokenData = formatTokenDataForRedis('bearerToken', btData);
    const rtData = {
      ...data,
      token: refreshToken,
      expires: refreshTokenExpires ? refreshTokenExpires.toISOString() : null,
    };
    const refreshTokenData = formatTokenDataForRedis('refreshToken', rtData);
    await saveToken('bearerToken', bearerToken, bearerTokenExpires, bearerTokenData);
    await saveToken('refreshToken', refreshToken, refreshTokenExpires, refreshTokenData);
    const logData = {
      userId,
      bearerToken: bearerToken?.substring(6),
      refreshToken: refreshToken?.substring(6),
    };
    log.info(logData, 'Tokens Saved');
    return done(null, bearerToken, refreshToken, {
      expires_in: type === 'password' ? bearerTokenLifetime : refreshTokenLifetime,
    });
  } catch (error) {
    log.error({
      model: 'generateTokens',
      message: 'Error creating tokens',
      userId,
      clientId,
      error: JSON.stringify(error),
    });
    return done(serverError());
  }
}

export async function exchangePassword(client, username, password, scope, reqBody, done) {
  try {
    const user = await checkUserCredentials(username, password);
    const { _id: clientId, scopes } = client;
    let { _id: userId } = user;
    const { device_id: deviceId, device_type: deviceType, isShortLived } = reqBody;
    userId = userId.toString();
    const scopesList = scopes.join();
    newrelic.addCustomAttribute('userId', userId);
    return await generateTokens(
      'password',
      clientId,
      userId,
      scopesList,
      deviceId,
      deviceType,
      isShortLived,
      done,
    );
  } catch (error) {
    log.error({
      model: 'exchangePassword',
      message: 'Error creating tokens',
      email: username,
      client,
      error: JSON.stringify(error),
    });
    return done(error);
  }
}

export async function exchangeTrustedRefreshToken(client, refreshToken, scope, done) {
  try {
    const refreshTokenNew = await redisPromise().hgetall(`refresh_token:${refreshToken}`);
    if (refreshTokenNew) {
      const { clientId, userId, scopes, deviceId, deviceType } = refreshTokenNew;
      const logData = {
        userId,
        refreshToken: refreshToken?.substring(6),
      };
      newrelic.addCustomAttribute('userId', userId);
      if (clientId !== client._id.toString()) {
        log.error(logData, 'Invalid Client for refreshToken');
        return done(tokenError());
      }
      // Delete refreshToken
      await redisPromise().del(`refresh_token:${refreshToken}`);
      log.info(logData, 'Refresh Token exchanged');
      return generateTokens(
        'refreshtoken',
        clientId,
        userId,
        scopes,
        deviceId,
        deviceType,
        false,
        done,
      );
    }

    const refreshTokenOld = await redisPromise().hgetall(`refresh_tokens:${refreshToken}`);
    if (refreshTokenOld) {
      newrelic.addCustomAttribute('userId', refreshTokenOld?.userId);
      // Delete token in old format
      await model.deleteRefreshToken(refreshToken);
      const { tokenExpires } = calculateTokenExpiry('refreshToken');
      const data = {
        clientId: refreshTokenOld.clientId,
        userId: refreshTokenOld.userId,
        scopes: refreshTokenOld.scopes || 'all',
        deviceId: refreshTokenOld.deviceId || '',
        deviceType: refreshTokenOld.deviceType || '',
        expires: tokenExpires.toISOString(),
        token: refreshToken,
      };
      const tokenData = formatTokenDataForRedis('refreshToken', data);
      // Store token in new format
      const tokenResult = await saveToken('refreshToken', refreshToken, tokenExpires, tokenData);
      const { clientId, userId, scopes, deviceId, deviceType } = tokenResult;
      if (clientId !== client._id.toString()) return done(tokenError());
      const logData = {
        userId,
        refreshToken: refreshToken?.substring(6),
      };
      log.info(logData, 'Refresh Token exchanged');
      return generateTokens(
        'refreshtoken',
        clientId,
        userId,
        scopes,
        deviceId,
        deviceType,
        false,
        done,
      );
    }

    const fbToken = await redisPromise().get(`fb-token:${refreshToken}`);
    if (fbToken) {
      // Delete token in old format
      await deleteFacebookToken(refreshToken);
      const { tokenExpires } = calculateTokenExpiry('refreshToken');
      const data = {
        clientId: client._id.toString() || 'authclientId',
        userId: fbToken,
        scopes: client.scopes || 'all',
        expires: tokenExpires.toISOString(),
        token: refreshToken,
      };
      const tokenData = formatTokenDataForRedis('refreshToken', data);
      // Store token in new format
      const tokenResult = await saveToken('refreshToken', refreshToken, tokenExpires, tokenData);
      const { clientId, userId, scopes, deviceId, deviceType } = tokenResult;
      newrelic.addCustomAttribute('userId', userId);
      const logData = {
        userId,
        refreshToken: refreshToken?.substring(6),
      };
      log.info(logData, 'Refresh Token exchanged');
      if (clientId !== client._id.toString()) return done(tokenError());
      return generateTokens(
        'refreshtoken',
        clientId,
        userId,
        scopes,
        deviceId,
        deviceType,
        false,
        done,
      );
    }

    // If it reaches here then the token do not exist
    return done(tokenError());
  } catch (err) {
    log.error({
      model: 'exchangeTrustedRefreshToken',
      message: 'Error refreshing token',
      refreshToken: refreshToken?.substring(6),
      error: JSON.stringify(err),
    });
    return done(serverError());
  }
}

export async function exchangeFbTokenTrusted(client, reqBody, done) {
  const { fbToken } = reqBody;
  try {
    const user = await lookupOrCreateUserByFBAccessToken(fbToken);
    const { _id: clientId, scopes } = client;
    let { _id: userId } = user;
    const { device_id: deviceId, device_type: deviceType, isShortLived } = reqBody;
    userId = userId.toString();
    const scopesList = scopes.join();
    newrelic.addCustomAttribute('userId', userId);
    return await generateTokens(
      'password',
      clientId,
      userId,
      scopesList,
      deviceId,
      deviceType,
      isShortLived,
      done,
    );
  } catch (err) {
    log.error({
      model: 'exchangeFbTokenTrusted',
      message: 'Error creating tokens',
      err,
    });
    return done(tokenError());
  }
}

export async function revokeToken(token, tokenType) {
  try {
    if (!token) return authError();
    if (tokenType && tokenType !== 'access_token' && tokenType !== 'refresh_token') {
      throw authError();
    }

    const tokenKeyPrefix = tokenType && tokenType === 'access_token' ? 'bearer_token' : tokenType;

    if (tokenType) {
      const { userId, clientId } = await redisPromise().hgetall(`${tokenKeyPrefix}:${token}`);
      await UserInfo.updateOne({ _id: userId }, { $pull: { linkedClients: { clientId } } });
      return await redisPromise().del(`${tokenKeyPrefix}:${token}`);
    }

    const bearerTokenDel = await redisPromise().del(`bearer_token:${token}`);
    if (bearerTokenDel) return bearerTokenDel;

    return await redisPromise().del(`refresh_token:${token}`);
  } catch (error) {
    if (error.code === 'invalid_request') throw authError();
    throw serverError();
  }
}

export async function getClientNameByClientId(clientId) {
  try {
    const client = await ClientId.findOne({ _id: clientId });
    if (!client) {
      throw new NotFound('Client not found');
    }
    const { name } = client;
    return name;
  } catch (err) {
    if (err.name === 'NotFound') {
      throw err;
    }
    newrelic.noticeError(err);
    log.error({
      model: 'getClientNameByClientId',
      message: 'Error fetching client name',
      error: JSON.stringify(err),
    });
    throw new ServerError({ message: 'Something went wrong getting the client name' });
  }
}

/**
 * @description Create a closure for given passport strategy with the clients _id, name, and scopes
 * @param {string} _id
 * @param {[string]} scopes
 * @param {string} name
 * @returns {(req: {}, token: string, tokenSecret: string, _: {}, cb: (err, user, info) => void)}
 */
export const createOAuth1StrategyCb = (_id, scopes, name) => (req, token, tokenSecret, _, cb) => {
  // Retrieve the UserId from the cookies that should have been set on the platform proxy
  const userId = req.cookies['x-auth-userid'];

  if (!userId) {
    newrelic.noticeError('User Id was not found in cookies during "/oauth1/connect" callback');
    return cb('User ID was not found in cookies', null);
  }

  return UserInfo.findOneAndUpdate(
    { _id: userId },
    {
      $push: {
        linkedClients: {
          clientId: _id,
          scopes,
          createdAt: new Date(),
          name,
          token,
          tokenSecret,
        },
      },
    },
    (err, doc) => {
      if (err) {
        log.error(err);
        newrelic.noticeError(err);
      }
      const platform = getPlatform(req.headers['user-agent']);
      analytics.track('Connected Account', userId, { partner: name, platform });
      // Call the verify callback with the userId in the `info` param
      // in case special processing is needed eventually
      cb(err, doc, { userId });
    },
  );
};

/**
 * @description Given the required properties for a strategy, creates a passport strategy
 * that can be consumed on the connect endpoint
 * @param {string} requestTokenURL
 * @param {string} accessTokenURL
 * @param {string} userAuthorizationURL
 * @param {string} consumerKey
 * @param {string} consumerSecret
 * @param {string} callbackURL
 * @param {string} name
 * @param {[string]} scopes
 * @param {string} _id
 * @returns {OAuth1Strategy}
 * */
export const createOAuth1Strategy = (
  requestTokenURL,
  accessTokenURL,
  userAuthorizationURL,
  consumerKey,
  consumerSecret,
  callbackURL,
  name,
  scopes,
  _id,
) => {
  const customNotice = (missingProperty, message) => {
    // eslint-disable-next-line max-len
    const msg =
      message ||
      `AuthProvider ${name} is missing ${missingProperty} property on AuthProviders document`;
    newrelic.noticeError(msg);
    log.error(msg);
  };

  if (!requestTokenURL) customNotice('requestTokenURL');
  if (!accessTokenURL) customNotice('accessTokenURL');
  if (!userAuthorizationURL) customNotice('userAuthorizationURL');
  if (!consumerKey) customNotice('consumerKey');
  if (!consumerSecret) customNotice('consumerSecret');
  if (!callbackURL) customNotice('callbackURL');
  if (!name) customNotice('name', 'Name missing from ClientIds document');
  if (!scopes) customNotice('scopes', 'Scopes missing from ClientIds document');
  if (!_id) customNotice('_id', 'Client ID missing from ClientIds document');

  const hasRequiredProperties =
    !!requestTokenURL &&
    !!accessTokenURL &&
    !!userAuthorizationURL &&
    !!consumerSecret &&
    !!consumerKey &&
    !!callbackURL &&
    !!name &&
    !!scopes &&
    !!_id;

  if (!hasRequiredProperties) {
    log.error(`Could not create provider for ${name || 'unknown partner'}`);
    return null;
  }

  const options = {
    requestTokenURL,
    accessTokenURL,
    userAuthorizationURL,
    consumerKey,
    consumerSecret,
    callbackURL,
    passReqToCallback: true,
  };
  return new OAuth1Strategy(options, createOAuth1StrategyCb(_id, scopes, name));
};

/**
 * @description When starting the server we want to set up the passport strategies for our
 * different oauth1 partners. Each OAuth1 Partner should have an entry in the AuthProviders
 * collection with the `spec: 'oauth1'` and be linked to a ClientId document in order to
 * have a passport strategy created for it. The following properties must exist on a document
 * in order for a strategy to be successfully setup:
 *  - requestTokenURL
 *  - accessTokenURL
 *  - userAuthorizationURL
 *  - consumerKey
 *  - consumerSecret
 *  - callbackURL
 * @returns {void}
 * */
export const setupOAuth1Strategies = async () => {
  const clients = await AuthProviders.find({ spec: 'oauth1' }).populate('client');

  clients.forEach(
    ({
      requestTokenURL,
      accessTokenURL,
      userAuthorizationURL,
      consumerKey,
      consumerSecret,
      callbackURL,
      client: { name, _id, scopes },
    }) => {
      const strategy = createOAuth1Strategy(
        requestTokenURL,
        accessTokenURL,
        userAuthorizationURL,
        consumerKey,
        consumerSecret,
        callbackURL,
        name,
        scopes,
        _id,
      );
      if (!strategy) {
        return;
      }
      passport.use(name, strategy);
    },
  );
  log.info('OAuth 1.0 Strategies set up');
};

/**
 * @description This function returns a handler in order to
 * render an OAuth1 success or failure redirect
 * @param {'OAuthSuccess.js'| 'OAuthFailed.js'} view
 * @returns {import('express').RequestHandler}
 */
export const getOAuth1RedirectHandler = (view) => async (req, res) => {
  const { clientId } = req.query;

  if (!clientId) {
    throw new APIError('Client ID is required');
  }

  try {
    const client = await ClientId.findOne({ _id: clientId });

    if (!client) {
      throw new NotFound('Client not found');
    }

    return res.render(view, { name: client.name, appImageURL: client.appImageURL });
  } catch (err) {
    if (err.name === 'NotFound') {
      throw err;
    }
    newrelic.noticeError(err);
    throw new ServerError({ message: 'Something went wrong redirecting the request' });
  }
};

export const getOAuth1Providers = async (req, res) => {
  try {
    const providers = await AuthProviders.find({ spec: 'oauth1' }).populate('client');

    const formattedProviders = providers
      .map(({ client }) => {
        if (!client || !client.name) return null;
        return { clientId: client._id, name: client.name, appImageUrl: client.appImageURL };
      })
      .filter(Boolean);

    res.status(200).send(formattedProviders);
  } catch (err) {
    throw new ServerError({ message: 'Something went wrong retrieving providers' });
  }
};

/**
 * @description Given a `clientId` returns success and failure
 * redirect urls with the given `clientId`
 * @param {string} clientId
 * @returns {{
 *    failureRedirectUrl: string,
 *    successRedirectUrl: string
 * }}
 */
export const getOauth1RedirectUrls = (clientId) => ({
  failureRedirectUrl: `/oauth1/failed?clientId=${clientId}`,
  successRedirectUrl: `/oauth1/success?clientId=${clientId}`,
});

/**
 * @description Given a `clientId` this function will retrieve an `AuthProvider`
 * with its matching `ClientIds` document and return a passport authenticator
 * which can be called like so: `authenticator(req, res, next)`. This authenticator
 * will process the oauth request. This function will setup the proper
 * success/failure redirects and target the proper strategy based on the
 * `clientId` and the matching `ClientIds` document `name` property.
 * @param {string} clientId
 * @returns {Promise<import('express').RequestHandler>}
 */
export const getPassportAuthenticator = async (clientId) => {
  const provider = await getProviderWithClient(clientId, 'oauth1');

  const { failureRedirectUrl, successRedirectUrl } = getOauth1RedirectUrls(clientId);

  const authenticator = passport.authenticate(provider.client.name, {
    failureRedirect: failureRedirectUrl,
    successRedirect: successRedirectUrl,
  });

  return authenticator;
};

/**
 * @description Given a `clientId` this function will check the existence of it in
 * `AuthProvider` collection.
 * @param {string} clientId
 * @returns {{
 *    exists: Boolean,
 * }}
 */
export const validateOAuth1Provider = async (req, res) => {
  try {
    const {
      query: { clientId },
    } = req;
    const exists = await AuthProviders.exists({ client: clientId });
    res.status(200).send({ exists });
  } catch (err) {
    throw new ServerError({ message: 'Something went wrong validating the provider' });
  }
};

/**
 * @description Given a bearer and refresh token value this function will delete the assosciated entries from Redis.
 * Triggers a Segment event for tracking.
 * Invoked by the /logout
 * @param {string} bearerToken required
 * @param {string} refreshToken required
 * @param {string} platform required - For tracking purposes
 * @returns {Boolean} true if succesful
 */

export async function deleteUserTokens(bearerToken, refreshToken, platform) {
  if (!bearerToken || !refreshToken) return authError();
  try {
    const bearerTokenFormatted = util.format(tokenKeys.bearerToken, bearerToken);
    const refreshTokenFormatted = util.format(tokenKeys.refreshToken, refreshToken);

    const { userId, clientId } = await redisPromise().hgetall(bearerTokenFormatted);
    const userAccessAndRefreshToken = util.format(tokenKeys.userAccessAndRefreshToken, userId);

    const tokensToDelete = [
      ['del', bearerTokenFormatted],
      ['del', refreshTokenFormatted],
    ];
    const userEntriesToDelete = [
      ['zrem', userAccessAndRefreshToken, bearerTokenFormatted],
      ['zrem', userAccessAndRefreshToken, refreshTokenFormatted],
    ];
    const commands = [...tokensToDelete, ...userEntriesToDelete];
    cacheClient().multi(commands).exec();
    const { brand } = await ClientId.findOne({ _id: clientId });
    analytics.track('Signed Out', userId, { method: 'user signed out', platform }, brand);
    const logData = {
      userId,
      clientId,
      model: 'deleteUserTokens',
    };
    log.info(logData, 'user tokens deleted');
    newrelic.addCustomAttribute('userId', userId);
    newrelic.addCustomAttribute('clientId', clientId);

    return true;
  } catch (error) {
    log.error({
      model: 'deleteUserTokens',
      message: 'Error deleting user tokens',
      bearerToken: bearerToken?.substring(6),
      refreshToken: refreshToken?.substring(6),
      error: JSON.stringify(error),
    });
    throw serverError(error.message);
  }
}
