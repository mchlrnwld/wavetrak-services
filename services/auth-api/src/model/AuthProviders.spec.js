import { expect } from 'chai';
import sinon from 'sinon';
import AuthProviders, { getProviderWithClient } from './AuthProviders';

describe('AuthProviders', () => {
  beforeEach(() => {
    sinon.stub(AuthProviders, 'findOne');
  });

  afterEach(() => {
    AuthProviders.findOne.restore();
  });

  describe('getProviderWithClient', () => {
    it('should throw an error if the provider does not exist', () => {
      AuthProviders.findOne.returns({ populate: () => null });

      try {
        getProviderWithClient('clientId');
      } catch (err) {
        expect(err.name).to.be.equal('NotFound');
        expect(err.message).to.be.equal('Provider does not exist');
      }
    });

    it('should throw an error if the provider client does not exist', () => {
      AuthProviders.findOne.returns({ populate: () => {} });

      try {
        getProviderWithClient('clientId');
      } catch (err) {
        expect(err.name).to.be.equal('ServerError');
        expect(err.message).to.be.equal('Client document was not found');
      }
    });

    it('should throw an error if the provider client name does not exist', () => {
      AuthProviders.findOne.returns({
        populate: () => {},
      });

      try {
        getProviderWithClient('clientId');
      } catch (err) {
        expect(err.name).to.be.equal('ServerError');
        expect(err.message).to.be.equal('Client document was missing a name');
      }
    });
  });
});
