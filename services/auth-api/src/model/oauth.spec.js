import chai from 'chai';
import OAuth1Strategy from 'passport-oauth1';
import sinon from 'sinon';
import newrelic from 'newrelic';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import passport from 'passport';
import * as dbContext from '../common/dbContext';
import * as user from './user';
import * as oAuthModel from './oauth';
import ClientIdModel from './ClientId';
import * as logger from '../common/logger';
import AuthProviders from './AuthProviders';
import UserInfo from './UserInfo';

const { expect } = chai;
chai.should();
chai.use(sinonChai);
chai.use(dirtyChai);

describe('oAuth model', () => {
  let cacheClientStub;
  let redisPromiseStub;
  let loggerStub;
  let checkUserCredentialsStub;
  let findOneClientIdModelStub;
  const loggerErrorStub = sinon.stub();
  const reqBody = {
    username: 'testuser@test.com',
    password: '123456',
    grant_type: 'password',
    client_id: '5af1ce73b5acf7c6dd2592ee',
    client_secret: 'secret',
    device_id: 'UNQ123',
    device_type: 'Web_Browser',
  };
  const reqWithHeaders = {
    req: {
      headers: {
        'x-real-ip': '0.0.0.0',
      },
    },
  };
  const client = {
    clientId: reqBody.client_id,
    scopes: ['all'],
  };
  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace() {},
      debug() {},
      info() {},
      error: loggerErrorStub,
    });
  });

  after(() => {
    loggerStub.restore();
  });
  beforeEach(() => {
    cacheClientStub = sinon.stub(dbContext, 'cacheClient').returns({
      multi: () => ({ exec: () => {} }),
    });
    redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
      hmset: () => {},
      expire: () => {},
      hgetall: () => {
        const res = {
          refreshToken: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          clientId: '5af1ce73b5acf7c6dd2592ee',
          userId: '59f164c72d787c00115ee1f0',
          scopes: 'all',
          deviceId: 'UNQ123',
          deviceType: 'Web_Browser',
        };
        return res;
      },
      get: () => {
        return 1;
      },
      set: () => {},
      incr: () => {},
    });
    checkUserCredentialsStub = sinon.stub(user, 'checkUserCredentials').resolves();
    findOneClientIdModelStub = sinon.stub(ClientIdModel, 'findOne').resolves();
  });

  afterEach(() => {
    redisPromiseStub.restore();
    checkUserCredentialsStub.restore();
    findOneClientIdModelStub.restore();
    loggerErrorStub.resetHistory();
    cacheClientStub.restore();
  });
  describe('trusted /token', () => {
    it('should return Bearer token for a valid username/password', async () => {
      const params = {
        client,
        username: reqBody.username,
        password: reqBody.password,
        scope: 'all',
        reqBody,
        done: () => ({
          access_token: '5543240f39278347ff43951ff57ae34f659cbd53',
          refresh_token: '0fab332eee5e29149a734c0efeff1d8a502bf52c',
          expires_in: 2592000,
          token_type: 'Bearer',
        }),
      };
      checkUserCredentialsStub.returns({ _id: '5bd0d9cb4a17b4000e6a9cfa' });
      const result = await oAuthModel.exchangePassword(...Object.values(params));
      expect(result).to.have.property('access_token', '5543240f39278347ff43951ff57ae34f659cbd53');
      expect(result).to.have.property('refresh_token', '0fab332eee5e29149a734c0efeff1d8a502bf52c');
      expect(result).to.have.property('expires_in', 2592000);
      expect(result).to.have.property('token_type', 'Bearer');
    });

    it('should return error for an invalid username', async () => {
      const params = {
        client,
        username: 'notavaliduser',
        password: reqBody.password,
        scope: 'all',
        reqBody,
        done: () => ({
          error: 'invalid_request',
          error_description: 'User not found',
        }),
      };

      checkUserCredentialsStub.rejects(new Error('User not found'));
      const result = await oAuthModel.exchangePassword(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'User not found');
    });

    it('should return error for an invalid password', async () => {
      const params = {
        client,
        username: reqBody.username,
        password: 'invalidpassword',
        scope: 'all',
        reqBody,
        done: () => ({
          error: 'invalid_request',
          error_description: 'Invalid email and password combination',
        }),
      };
      checkUserCredentialsStub.rejects(new Error('Invalid email and password combination'));
      const result = await oAuthModel.exchangePassword(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property(
        'error_description',
        'Invalid email and password combination',
      );
    });

    it('should return error for missing username', async () => {
      const params = {
        client,
        username: null,
        password: reqBody.password,
        scope: 'all',
        reqBody,
        done: () => ({
          error: 'invalid_request',
          error_description: 'Missing required parameter: username',
        }),
      };
      // Restore this stub first because we are going to test the actual function checkUserCredentials
      checkUserCredentialsStub.restore();
      const result = await oAuthModel.exchangePassword(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'Missing required parameter: username');
    });

    it('should return error for missing password', async () => {
      const params = {
        client,
        username: reqBody.username,
        password: null,
        scope: 'all',
        reqBody,
        done: () => ({
          error: 'invalid_request',
          error_description: 'Missing required parameter: password',
        }),
      };
      // Restore this stub first because we are going to test the actual function checkUserCredentials
      checkUserCredentialsStub.restore();
      const result = await oAuthModel.exchangePassword(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'Missing required parameter: password');
    });

    it('should return Bearer token for a valid refreshToken', async () => {
      const params = {
        client,
        refresh_token: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7ee',
        scope: 'all',
        done: () => ({
          access_token: '5543240f39278347ff43951ff57ae34f659cbd53',
          refresh_token: '0fab332eee5e29149a734c0efeff1d8a502bf52c',
          expires_in: 2592000,
          token_type: 'Bearer',
        }),
      };
      const result = await oAuthModel.exchangeTrustedRefreshToken(...Object.values(params));
      expect(result).to.have.property('access_token', '5543240f39278347ff43951ff57ae34f659cbd53');
      expect(result).to.have.property('refresh_token', '0fab332eee5e29149a734c0efeff1d8a502bf52c');
      expect(result).to.have.property('expires_in', 2592000);
      expect(result).to.have.property('token_type', 'Bearer');
    });

    it('should return error for missing refreshToken', async () => {
      const params = {
        client,
        refresh_token: null,
        scope: 'all',
        done: () => ({
          error: 'invalid_request',
          error_description: 'Missing required parameter: refresh_token',
        }),
      };
      // Restore the redisPromise stub first because we need
      // to change the default implementation of hgetall
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        hgetall: () => null,
      });
      const result = await oAuthModel.exchangeTrustedRefreshToken(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property(
        'error_description',
        'Missing required parameter: refresh_token',
      );
    });

    it('should return error for invalid Client', async () => {
      const params = {
        ...reqWithHeaders,
        clientId: 'invalidclient',
        clientSecret: 'clientsecret',
        done: () => ({
          error: 'invalid_request',
          error_description: 'Invalid Request',
        }),
      };
      findOneClientIdModelStub.rejects(new Error('Client_Id does not exist'));
      const result = await oAuthModel.basicAuthorization(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'Invalid Request');
    });

    it('should return error if client_Id is not passed', async () => {
      const params = {
        ...reqWithHeaders,
        clientId: null,
        clientSecret: 'clientsecret',
        done: () => ({
          error: 'invalid_request',
          error_description: 'Invalid Request',
        }),
      };
      findOneClientIdModelStub.rejects(new Error('Client_Id does not exist'));
      const result = await oAuthModel.basicAuthorization(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'Invalid Request');
    });

    it('should return error if client_secret is incorrect', async () => {
      const params = {
        ...reqWithHeaders,
        clientId: reqBody.client_id,
        clientSecret: 'incorrectclientsecret',
        done: () => ({
          error: 'invalid_request',
          error_description: 'Invalid Request',
        }),
      };
      findOneClientIdModelStub.returns({
        _id: '5bff1583ffda1e1eb8d35c4b',
        clientId: 'WEB_VTM8QWOFNZ4X',
        name: 'Surfline Web',
        description: 'Surfline Web application',
        secret: 'authclient_secret',
        trusted: true,
        scopes: ['all'],
      });
      const result = await oAuthModel.basicAuthorization(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'Invalid Request');
    });

    it('should return error if client is not trusted', async () => {
      const params = {
        ...reqWithHeaders,
        clientId: reqBody.client_id,
        clientSecret: reqBody.client_secret,
        done: () => ({
          error: 'invalid_request',
          error_description: 'Invalid Request',
        }),
      };
      findOneClientIdModelStub.returns({
        _id: '5bff1583ffda1e1eb8d35c4b',
        clientId: 'WEB_VTM8QWOFNZ4X',
        name: 'Surfline Web',
        description: 'Non Trusted Surfline Web application',
        secret: 'authclient_secret',
        trusted: false,
        scopes: ['all'],
      });
      const result = await oAuthModel.basicAuthorization(...Object.values(params));
      expect(result).to.have.property('error', 'invalid_request');
      expect(result).to.have.property('error_description', 'Invalid Request');
    });
  });

  describe('bearerAuthorization', () => {
    it('should return userId for a valid accessToken', async () => {
      const params = {
        accessToken: '9e6654404671b0682870c7663f0a86b1d2a1184e',
        apiKey: null,
        isTrusted: false,
        done: () => ({
          userId: '59f164c72d787c00115ee1f0',
          scopes: 'all',
        }),
      };
      const result = await oAuthModel.bearerAuthorization(...Object.values(params));
      expect(result).to.have.property('userId', '59f164c72d787c00115ee1f0');
      expect(result).to.have.property('scopes', 'all');
    });

    it('should return error for an invalid accessToken', async () => {
      const params = {
        accessToken: 'invalid',
        apiKey: null,
        isTrusted: false,
        done: () => ({
          message: 'Unauthorized client request',
        }),
      };
      // Restore the redisPromise stub first because we need
      // to change the default implementation of hgetall
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        hgetall: () => null,
      });
      const result = await oAuthModel.bearerAuthorization(...Object.values(params));
      expect(result).to.have.property('message', 'Unauthorized client request');
    });

    it('should return error for an external client when trying to access a trusted route', async () => {
      const params = {
        accessToken: '9e6654404671b0682870c7663f0a86b1d2a1184e',
        apiKey: null,
        isTrusted: true,
        done: () => ({
          message: 'Unauthorized Client',
        }),
      };
      findOneClientIdModelStub.returns({
        _id: '5af1ce73b5acf7c6dd2592ee',
        name: 'Rip Curl',
        scopes: ['sessions:write:user', 'sessions:update:user'],
      });
      const result = await oAuthModel.bearerAuthorization(...Object.values(params));
      expect(result).to.have.property('message', 'Unauthorized Client');
    });
  });

  describe('deleteUserTokens', () => {
    it('should delete the bearer and refresh tokens from Redis', async () => {
      findOneClientIdModelStub.returns({
        _id: '5af1ce73b5acf7c6dd2592ee',
        name: 'Surfline Web',
        secret: 'authclient_secret',
        brand: 'sl',
      });
      const result = await oAuthModel.deleteUserTokens('bearerToken', 'refreshToken', 'web');
      expect(result).to.be.equal(true);
      expect(findOneClientIdModelStub).to.have.been.calledOnceWithExactly({
        _id: '5af1ce73b5acf7c6dd2592ee',
      });
    });
    it('should return an auth error if there are no bearer or refresh tokens passed in', async () => {
      try {
        await oAuthModel.deleteUserTokens(null, null, 'web');
      } catch (err) {
        expect(err.message).to.contain('AuthorizationError');
        expect(err.status).to.be.equal(400);
      }
    });
    it('should return an server error if there are other errors', async () => {
      try {
        findOneClientIdModelStub.rejects(new Error('ClientId not found'));
        await oAuthModel.deleteUserTokens('bearerToken', 'refreshToken', 'web');
      } catch (err) {
        console.log('%coauth.spec.js line:403 err', 'color: #007acc;', err);
        expect(err.code).to.be.equal('server_error');
        expect(err.status).to.be.equal(500);
      }
    });
  });

  describe('OAuth1', () => {
    describe('getOAuth1RedirectHandler', () => {
      it('should return a handler with arity 2', async () => {
        const handler = oAuthModel.getOAuth1RedirectHandler('OAuthFailed.js');

        expect(handler.length).to.be.equal(2);
      });

      it('should render the proper view given an existing client id', async () => {
        const handler = oAuthModel.getOAuth1RedirectHandler('OAuthSuccess.js');

        findOneClientIdModelStub.returns({ name: 'name', appImageURL: 'image' });

        const req = { query: { clientId: '123' } };
        const resStub = { render: sinon.stub() };

        await handler(req, resStub);

        expect(findOneClientIdModelStub).to.have.been.calledOnceWithExactly({ _id: '123' });
        expect(resStub.render).to.have.been.calledOnceWithExactly('OAuthSuccess.js', {
          name: 'name',
          appImageURL: 'image',
        });
      });

      it('should throw an error if the client id is not found', async () => {
        const handler = oAuthModel.getOAuth1RedirectHandler('OAuthSuccess.js');

        findOneClientIdModelStub.returns(null);

        const req = { query: { clientId: '123' } };
        const resStub = { render: sinon.stub() };

        try {
          await handler(req, resStub);
        } catch (err) {
          expect(err.name).to.be.equal('NotFound');
          expect(err.message).to.be.equal('Client not found');
        }

        expect(findOneClientIdModelStub).to.have.been.calledOnceWithExactly({ _id: '123' });
      });

      it('should throw an error if not client ID is present', async () => {
        const handler = oAuthModel.getOAuth1RedirectHandler('OAuthSuccess.js');

        const req = { query: {} };

        try {
          await handler(req);
        } catch (error) {
          expect(error.name).to.be.equal('APIError');
          expect(error.message).to.be.equal('Invalid Parameters: Client ID is required');
        }
      });

      it('should throw a server error if an unexpected error occurs', async () => {
        const handler = oAuthModel.getOAuth1RedirectHandler('OAuthSuccess.js');

        findOneClientIdModelStub.throws();

        const req = { query: { clientId: '123' } };

        try {
          await handler(req);
        } catch (error) {
          expect(error.name).to.be.equal('ServerError');
          expect(error.message).to.be.equal('Something went wrong redirecting the request');
        }
      });
    });

    describe('setupOAuth1Strategies', () => {
      beforeEach(() => {
        sinon.stub(AuthProviders, 'find');
        sinon.stub(passport, 'use');
        sinon.stub(newrelic, 'noticeError');
      });

      afterEach(() => {
        AuthProviders.find.restore();
        passport.use.restore();
        newrelic.noticeError.restore();
      });

      it('should setup a provider for given clients', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledTwice();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey')),
        );
        expect(passport.use.getCall(1)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
      });
      it('should skip setting up provider for client if it is missing requestTokenURL', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'AuthProvider test-client is missing requestTokenURL property on AuthProviders document',
        );
      });

      it('should skip setting up provider for client if it is missing accessTokenURL', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'AuthProvider test-client is missing accessTokenURL property on AuthProviders document',
        );
      });

      it('should skip setting up provider for client if it is missing userAuthorizationURL', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'AuthProvider test-client is missing userAuthorizationURL property on AuthProviders document',
        );
      });

      it('should skip setting up provider for client if it is missing consumerKey', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'AuthProvider test-client is missing consumerKey property on AuthProviders document',
        );
      });

      it('should skip setting up provider for client if it is missing consumerSecret', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'AuthProvider test-client is missing consumerSecret property on AuthProviders document',
        );
      });

      it('should skip setting up provider for client if it is missing callbackURL', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'AuthProvider test-client is missing callbackURL property on AuthProviders document',
        );
      });

      it('should skip setting up provider for client if it is missing client name', async () => {
        const result = [
          {
            client: {
              _id: '123',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'Name missing from ClientIds document',
        );
      });

      it('should skip setting up provider for client if it is missing client scopes', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              _id: '123',
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'Scopes missing from ClientIds document',
        );
      });

      it('should skip setting up provider for client if it is missing client ID', async () => {
        const result = [
          {
            client: {
              name: 'test-client',
              scopes: ['test-scope:read:write'],
            },
            requestTokenURL: 'requestTokenURL',
            accessTokenURL: 'accessTokenURL',
            userAuthorizationURL: 'userAuthorizationURL',
            consumerKey: 'consumerKey',
            consumerSecret: 'consumerSecret',
            callbackURL: 'callbackURL',
          },
          {
            client: {
              name: 'test-client1',
              _id: '456',
              scopes: ['testscope:read:write'],
            },
            requestTokenURL: 'requestTokenURL1',
            accessTokenURL: 'accessTokenURL1',
            userAuthorizationURL: 'userAuthorizationURL1',
            consumerKey: 'consumerKey1',
            consumerSecret: 'consumerSecret1',
            callbackURL: 'callbackURL1',
          },
        ];
        const populateStub = sinon.stub().resolves(result);

        AuthProviders.find.returns({ populate: populateStub });
        await oAuthModel.setupOAuth1Strategies();

        expect(passport.use).to.have.been.calledOnce();
        expect(passport.use.getCall(0)).to.have.been.calledWithExactly(
          'test-client1',
          sinon.match
            .instanceOf(OAuth1Strategy)
            .and(sinon.match.hasNested('_oauth._consumerKey', 'consumerKey1')),
        );
        expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(
          'Client ID missing from ClientIds document',
        );
      });
    });

    describe('createOAuth1StrategyCb', () => {
      let clock;
      const current = 1486258500; // 02/05/2017@01:35

      beforeEach(() => {
        clock = sinon.useFakeTimers(current * 1000);
        sinon.stub(UserInfo, 'findOneAndUpdate');
      });

      afterEach(() => {
        clock.restore();
        UserInfo.findOneAndUpdate.restore();
      });

      it('should update the users linkedClients', () => {
        const cbStub = () => {};
        const cb = oAuthModel.createOAuth1StrategyCb('client123', ['scope1'], 'clientName');
        UserInfo.findOneAndUpdate.returns();
        cb({ cookies: { 'x-auth-userid': '123' } }, 'token', 'tokenSecret', {}, cbStub);
        expect(UserInfo.findOneAndUpdate).to.have.been.calledOnceWith(
          { _id: '123' },
          {
            $push: {
              linkedClients: {
                clientId: 'client123',
                scopes: ['scope1'],
                createdAt: new Date(),
                name: 'clientName',
                token: 'token',
                tokenSecret: 'tokenSecret',
              },
            },
          },
        );
      });
      it('should update the users linkedClients', () => {
        const cbStub = sinon.stub();
        const cb = oAuthModel.createOAuth1StrategyCb('client123', ['scope1'], 'clientName');

        cb({ cookies: {} }, 'token', 'tokenSecret', {}, cbStub);
        expect(UserInfo.findOneAndUpdate).to.not.have.been.called();
        expect(cbStub).to.have.been.calledOnceWithExactly('User ID was not found in cookies', null);
      });
    });

    describe('getOAuth1Providers', () => {
      beforeEach(() => {
        sinon.stub(AuthProviders, 'find');
      });

      afterEach(() => {
        AuthProviders.find.restore();
      });

      it('should return a list of supported providers', async () => {
        const sendStub = sinon.stub();
        AuthProviders.find.returns({
          populate: () => [
            { client: { name: 'clientName', _id: '123', appImageURL: 'image' } },
            { missingClient: true },
          ],
        });
        const res = { status: () => ({ send: sendStub }) };
        await oAuthModel.getOAuth1Providers({}, res);

        expect(sendStub).to.have.been.calledOnceWithExactly([
          { clientId: '123', name: 'clientName', appImageUrl: 'image' },
        ]);
      });

      it('should throw a server error if unsuccessful', async () => {
        AuthProviders.find.throws();
        try {
          await oAuthModel.getOAuth1Providers({}, {});
        } catch (err) {
          expect(err.name).to.be.equal('ServerError');
          expect(err.message).to.be.equal('Something went wrong retrieving providers');
        }
      });
    });

    describe('getOauth1RedirectUrls', () => {
      it('should return the proper redirect urls', () => {
        const urls = oAuthModel.getOauth1RedirectUrls('123');

        expect(urls.failureRedirectUrl).to.be.equal('/oauth1/failed?clientId=123');
        expect(urls.successRedirectUrl).to.be.equal('/oauth1/success?clientId=123');
      });
    });

    describe('getPassportAuthenticator', () => {
      beforeEach(() => {
        sinon.stub(AuthProviders, 'findOne');
        sinon.stub(passport, 'authenticate');
      });

      afterEach(() => {
        AuthProviders.findOne.restore();
        passport.authenticate.restore();
      });

      it('should call authenticate with the client name, and redirect urls', async () => {
        AuthProviders.findOne.returns({
          populate: () => ({
            client: {
              name: 'clientName',
            },
          }),
        });
        const handlerStub = sinon.stub();
        passport.authenticate.returns(handlerStub);

        const result = await oAuthModel.getPassportAuthenticator('123');

        expect(passport.authenticate).to.have.been.calledOnceWithExactly('clientName', {
          failureRedirect: '/oauth1/failed?clientId=123',
          successRedirect: '/oauth1/success?clientId=123',
        });
        expect(result).to.be.equal(handlerStub);
      });
    });

    describe('validateOAuth1Provider', () => {
      beforeEach(() => {
        sinon.stub(AuthProviders, 'exists');
      });

      afterEach(() => {
        AuthProviders.exists.restore();
      });

      it('should return true if the provider exists', async () => {
        const sendStub = sinon.stub().resolves({ exists: true });
        AuthProviders.exists.returns(true);
        const res = { status: () => ({ send: sendStub }) };
        const req = { query: { clientId: '123' } };
        await oAuthModel.validateOAuth1Provider(req, res);

        expect(sendStub).to.have.been.calledOnceWithExactly({ exists: true });
      });

      it('should throw a server error if unsuccessful', async () => {
        AuthProviders.exists.throws();
        try {
          await oAuthModel.validateOAuth1Provider({}, {});
        } catch (err) {
          expect(err.name).to.be.equal('ServerError');
          expect(err.message).to.be.equal('Something went wrong validating the provider');
        }
      });
    });
  });
});
