import mongoose from 'mongoose';

const linkedClientsSchema = mongoose.Schema(
  {
    clientId: mongoose.Schema.Types.ObjectId,
    scopes: [String],
    createdAt: Date,
    token: String,
    tokenSecret: String,
    name: String,
  },
  { _id: false },
);

const UserInfoSchema = mongoose.Schema(
  {
    email: String,
    password: String,
    linkedClients: [linkedClientsSchema],
  },
  { collection: process.env.MONGO_TESTING_COLLECTION || 'UserInfo' },
);

export default mongoose.model('UserInfoModel', UserInfoSchema);
