import mongoose from 'mongoose';

const ClientGroupSchema = mongoose.Schema(
  {
    name: String,
    tokenLimit: Number,
    members: [String],
  },
  { collection: 'ClientGroups' },
);

export default mongoose.model('ClientGroup', ClientGroupSchema);
