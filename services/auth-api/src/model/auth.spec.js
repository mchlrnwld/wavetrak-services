import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import redis from 'redis-js';
import bcrypt from 'bcrypt';
import util from 'util';
import * as logger from '../common/logger';
import * as dbContext from '../common/dbContext';
import UserInfoModel from './UserInfo';
import * as utils from '../common/utils';
import model from './auth';

const { expect } = chai;
chai.should();
chai.use(sinonChai);

describe('auth model', () => {
  let loggerStub;
  let redisClientStub;
  let redisClient;
  const req = {};
  let redisPromiseStub;

  const refreshTokenLifetime = process.env.refreshTokenLifetime
    ? parseInt(process.env.refreshTokenLifetime, 10)
    : 31536000;

  // const keys = {
  //   accessToken: 'tokens:%s',
  //   refreshToken: 'refresh_tokens:%s',
  // };

  // New token format
  const keys = {
    bearerToken: 'bearer_token:%s',
    refreshToken: 'refresh_token:%s',
    userAccessAndRefreshToken: 'users:%s:accessAndRefreshToken',
  };

  const token = {
    bearerToken: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
    clientId: 'WEB_VTM8QWOFNZ4X',
    expires: new Date().toISOString(),
    userId: '59f164c72d787c00115ee1f0',
  };

  const user = {
    email: 'john@email.com',
    password: '123456',
  };

  const members = ['surfline_web', 'surfline_android', 'surfline_ios'];
  const clientgroup = {
    members,
    tokenLimit: 2,
  };

  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace() {},
      debug() {},
      info() {},
      error() {},
    });
  });

  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    redisClient = redis.createClient();
    redisClient.hmset(util.format(keys.accessToken, token.accessToken), token);
    redisClient.hmset(util.format(keys.refreshToken, token.accessToken), token);
    redisClientStub = sinon.stub(dbContext, 'cacheClient').returns(redisClient);
    req.body = { client_id: token.clientId };
    req.user = { id: token.userId };
    req.oauth = {};
    req.oauth.client = {};
    req.oauth.client.clientgroup = clientgroup;
    req.oauth.client.clientId = 'surfline_web';
    sinon.stub(UserInfoModel, 'findOne');
    redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
      hmset: () => {},
      expire: () => {},
      zadd: () => {},
      zremrangebyscore: () => {},
      ttl: () => 1000,
      hget: () => '59f164c72d787c00115ee1f0',
      get: () => '59f164c72d787c00115ee1f0',
      del: () => {},
      zrem: () => 1,
      hgetall: () => {
        const res = {
          bearerToken: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          clientId: 'WEB_VTM8QWOFNZ4X',
          userId: '59f164c72d787c00115ee1f0',
          scopes: 'all',
          deviceId: 'UNQ123',
          deviceType: 'Web_Browser',
        };
        return res;
      },
    });
  });

  afterEach(() => {
    redisClientStub.restore();
    redisClient.flushall();
    UserInfoModel.findOne.restore();
    redisPromiseStub.restore();
  });

  it('should initialize as an object', (done) => {
    expect(model).to.exist();
    done();
  });

  it('should return access token with getAccessToken(bearerToken, callback)', (done) => {
    model.getAccessToken(token.accessToken, (err, result) => {
      expect(err).to.not.exist();
      expect(result).to.exist();
      expect(result).to.have.deep.property('accessToken', token.bearerToken);
      expect(result).to.have.deep.property('clientId', token.clientId);
      expect(result).to.have.deep.property('userId', token.userId);
      done();
    });
  });

  it("should return nothing if accessToken expired or doesn't exists with getAccessToken(bearerToken, callback)", (done) => {
    // Restore the redisPromise stub first because we need to change the default implementation of hgetall
    redisPromiseStub.restore();
    redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
      hgetall: () => {
        return null;
      },
      get: () => {
        return null;
      },
      del: () => {},
    });
    model.getAccessToken('tokennotexists', (err, result) => {
      expect(err).to.not.exist();
      expect(result).to.not.exist();
      done();
    });
  });

  it('should return client id with getClient(clientId, clientSecret, callback)', (done) => {
    const callback = sinon.spy();
    model.getClient('authclientId', 'clientSecret', callback);
    expect(callback).to.have.been.calledWith(null, {
      clientId: 'authclientId',
      clientSecret: 'clientSecret',
    });
    done();
  });

  it('should return refreshToken with getRefreshToken(accessToken, callback))', (done) => {
    model.getRefreshToken(token.accessToken, (err, result) => {
      expect(err).to.not.exist();
      expect(result).to.exist();
      expect(result).to.have.deep.property('refreshToken', token.accessToken);
      expect(result).to.have.deep.property('clientId', token.clientId);
      expect(result).to.have.deep.property('userId', token.userId);
      done();
    });
  });

  it("should return nothing if refreshToken expired or doesn't exists with getRefreshToken(accessToken, callback)", (done) => {
    // Restore the redisPromise stub first because we need to change the default implementation of hgetall
    redisPromiseStub.restore();
    redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
      hgetall: () => {
        return null;
      },
      get: () => {
        return null;
      },
    });
    model.getAccessToken('tokennotexists', (err, result) => {
      expect(err).to.not.exist();
      expect(result).to.not.exist();
      done();
    });
  });

  it('should return true with grantTypeAllowed(clientId, grantType, callback)', (done) => {
    const callback = sinon.spy();
    model.grantTypeAllowed('authClientId', 'password', callback);
    expect(callback).to.have.been.calledWith(null, true);
    done();
  });

  it('should save accessToken with saveAccessToken(accessToken, clientId, expires, user, callback)', (done) => {
    const expires = new Date();
    model.saveAccessToken(
      token.accessToken,
      token.clientId,
      expires,
      { id: token.userId },
      '',
      '',
      (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.exist();
        expect(result).to.equal(1);
        done();
      },
    );
  });

  it('should save accessToken with saveRefreshToken(refreshToken, clientId, expires, user, callback)', (done) => {
    const expires = new Date();
    model.saveRefreshToken(
      token.accessToken,
      token.clientId,
      expires,
      { id: token.userId },
      '',
      '',
      (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.exist();
        expect(result).to.equal(1);
        done();
      },
    );
  });

  it('should invalidateTokens with invalidateToken(userId)', (done) => {
    model.invalidateToken(token.userId).then((result) => {
      expect(result).to.exist();
      expect(result).to.be.instanceof(Array);
      expect(result).to.have.lengthOf(1);
      done();
    });
  });

  it('should return a user with  getUser(email, password, callback)', (done) => {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(user.password, salt);
    UserInfoModel.findOne.returns({
      _id: token.userId,
      password: hash,
    });

    model.getUser(user.email, user.password, (err, result) => {
      expect(err).to.not.exist();
      expect(result).to.exist();
      expect(result).to.have.deep.property('id', token.userId.toString());
      done();
    });
  });

  it('should return nothing if password is incorrect for getUser(email, password, callback)', (done) => {
    const password = 'incorrectpassword';
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);
    UserInfoModel.findOne.returns({
      _id: token.userId,
      password: hash,
    });

    model.getUser(user.email, user.password, (result) => {
      (typeof result).should.be.equal('undefined');
      done();
    });
  });

  it("should return nothing if the user doesn't exists with  getUser(email, password, callback)", (done) => {
    UserInfoModel.findOne.returns(null);
    model.getUser(user.email, user.password, (err, result) => {
      expect(err).to.not.exist();
      expect(result).to.not.exist();
      done();
    });
  });

  describe('generate token', () => {
    it('should generate access token for type=accessToken', (done) => {
      model.generateToken('accessToken', req, (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.exist();
        // const ttl = redisClient.ttl(util.format(keys.accessToken, result.accessToken));
        // const token = redisClient.hgetall(util.format(keys.accessToken, result.accessToken));
        // expect(ttl).to.be.below(accessTokenLifetime);
        // expect(token).to.have.deep.property('userId', token.userId);
        // expect(token).to.have.deep.property('clientId', token.clientId);
        done();
      });
    });

    it('should generate refresh token for type=refreshToken', (done) => {
      model.generateToken('refreshToken', req, (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.exist();
        const ttl = redisClient.ttl(util.format(keys.refreshToken, result.refreshToken));
        expect(ttl).to.be.below(refreshTokenLifetime);
        done();
      });
    });

    it('should generate shortLived accesstoken with query param isShortLived=true', (done) => {
      req.query = { isShortLived: 'true' };
      model.generateToken('accessToken', req, (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.exist();
        expect(result).to.have.property('accessToken');
        done();
      });
    });

    it("shouldn't generate shortLived accesstoken with query param isShortLived=false", (done) => {
      req.query = { isShortLived: 'false' }; //
      model.generateToken('accessToken', req, (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.exist();
        // const ttl = redisClient.ttl(util.format(keys.accessToken, result.accessToken));
        // const token = redisClient.hgetall(util.format(keys.accessToken, result.accessToken));
        // expect(ttl).to.be.below(accessTokenLifetime);
        // expect(token).to.have.deep.property('userId', token.userId);
        // expect(token).to.have.deep.property('clientId', token.clientId);
        done();
      });
    });

    it("should return error if type isn't accessToken or refreshToken", (done) => {
      model.generateToken('access', req, (err, result) => {
        expect(err).to.exist();
        expect(err).to.have.deep.property('message', 'Invalid or missing type parameter');
        expect(result).to.not.exist();
        done();
      });
    });

    it("should return error if token can't be generated", (done) => {
      const createTokenStub = sinon
        .stub(utils, 'createToken')
        .rejects('Server error while generating token');
      const checkTokenLimitStub = sinon.stub(model, 'checkTokenLimit').resolves();

      model.generateToken('accessToken', req, (err, result) => {
        try {
          expect(err.message).to.equal('Server error while generating token');
          expect(result).to.equal(undefined);
          done();
        } catch (error) {
          done(error);
        } finally {
          createTokenStub.restore();
          checkTokenLimitStub.restore();
        }
      });
    });

    it("should return error if req object doesn't have client id", (done) => {
      req.body = null;
      model.generateToken('accessToken', req, (err, result) => {
        expect(err).to.exist();
        expect(result).to.not.exist();
        done();
      });
    });

    it("should return error if req object doesn't have user object", (done) => {
      req.user = null;
      model.generateToken('accessToken', req, (err, result) => {
        expect(err).to.exist();
        expect(result).to.not.exist();
        done();
      });
    });
  });

  describe('check token limit', () => {
    it('should resolve checkTokenLimit if client is authClientId', async () => {
      const result = await model.checkTokenLimit('authclientId', req.user.id, req.oauth.client);
      expect(result).to.exist();
      expect(result).to.equal(1);
    });

    it('should resolve checkTokenLimit if token_count is less than token_limit', async () => {
      const expires = new Date();
      expires.setMinutes(expires.getMinutes() + 1);
      model.saveAccessToken(
        'qqac7e984caee3ab84604f15b1f703f218e9c2e1',
        'surfline_web',
        expires,
        { id: token.userId },
        () => {},
      );

      const result = await model.checkTokenLimit('surfline_web', req.user.id, req.oauth.client);
      expect(result).to.exist();
      expect(result).to.equal('User token count is below the token limit');
    });

    it('should reject checkTokenLimit if token_count is greater than token_limit', (done) => {
      const expires = new Date();
      expires.setMinutes(expires.getMinutes() + 1);
      model.saveAccessToken(
        'ffec7e984caee3ab84604f15b1f703f218e9c2e1',
        'surfline_web',
        expires,
        { id: token.userId },
        () => {},
      );
      expires.setMinutes(expires.getMinutes() + 2);
      model.saveAccessToken(
        'dd43e984caee3ab84604f15b1f703f218e9c2e2',
        'surfline_android',
        expires,
        { id: token.userId },
        () => {},
      );
      expires.setMinutes(expires.getMinutes() + 3);
      model.saveAccessToken(
        'kk23e984caee3ab84604f15b1f703f218e9c2e2',
        'surfline_ios',
        expires,
        { id: token.userId },
        () => {},
      );

      model
        .checkTokenLimit('surfline_web', req.user.id, req.oauth.client)
        .then((result) => {
          expect(result).to.equal('User token count is below the token limit');
          done();
        })
        .catch((e) => {
          expect(e).to.equal('User have exceeded the token limit');
          done();
        });
    });

    it('should call deleteStaleTokens and resolve if forced=true', (done) => {
      const expires = new Date();
      expires.setMinutes(expires.getMinutes() + 1);
      model.saveAccessToken(
        'ffec7e984caee3ab84604f15b1f703f218e9c2e1',
        'surfline_web',
        expires,
        { id: token.userId },
        () => {},
      );
      expires.setMinutes(expires.getMinutes() + 2);
      model.saveAccessToken(
        'dd43e984caee3ab84604f15b1f703f218e9c2e2',
        'surfline_android',
        expires,
        { id: token.userId },
        () => {},
      );
      expires.setMinutes(expires.getMinutes() + 3);
      model.saveAccessToken(
        'kk23e984caee3ab84604f15b1f703f218e9c2e2',
        'surfline_ios',
        expires,
        { id: token.userId },
        () => {},
      );
      req.oauth.client.clientgroup.tokenLimit = 0;
      model
        .checkTokenLimit('surfline_web', req.user.id, req.oauth.client, 'true')
        .then((result) => {
          expect(result).to.equal('Tokens deleted successfully');
          done();
        });
    });

    it('should delete stale access_tokens if forced flag is true', async () => {
      const expires = new Date();
      expires.setMinutes(expires.getMinutes() + 1);
      model.saveAccessToken(
        'ffec7e984caee3ab84604f15b1f703f218e9c2e1',
        'surfline_web',
        expires,
        { id: token.userId },
        () => {},
      );
      expires.setMinutes(expires.getMinutes() + 2);
      model.saveAccessToken(
        'dd43e984caee3ab84604f15b1f703f218e9c2e2',
        'surfline_android',
        expires,
        { id: token.userId },
        () => {},
      );
      expires.setMinutes(expires.getMinutes() + 3);
      model.saveAccessToken(
        'kk23e984caee3ab84604f15b1f703f218e9c2e2',
        'surfline_ios',
        expires,
        { id: token.userId },
        () => {},
      );
      const tokensArray = [
        ['surfline_web', '2017-11-10T01:32:47.107Z', 'ffec7e984caee3ab84604f15b1f703f218e9c2e1'],
        ['surfline_android', '2017-11-10T01:34:47.107Z', 'dd43e984caee3ab84604f15b1f703f218e9c2e2'],
      ];

      await model.deleteStaleTokens(token.userId, req.oauth.client, tokensArray);
      // ffec7e984caee3ab84604fi5b1f703f218e9c2e1 is deleted from Redis by now
      // Restore the redisPromise stub first because
      // we need to change the default implementation of hgetall
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        hgetall: () => null,
        get: () => null,
      });

      await model.getAccessToken('ffec7e984caee3ab84604fi5b1f703f218e9c2e1', (err, result) => {
        expect(err).to.not.exist();
        expect(result).to.not.exist();
      });
    });
  });
});
