import newrelic from 'newrelic';
import bcrypt from 'bcrypt';
import util from 'util';
import { authError } from '../common/customErrors';
import UserInfoModel from './UserInfo';
import { redisPromise, cacheClient } from '../common/dbContext';
import * as logger from '../common/logger';

const log = logger.logger('auth-service:model:User');
const keys = {
  bearerToken: 'bearer_token:%s',
  refreshToken: 'refresh_token:%s',
  userAccessAndRefreshToken: 'users:%s:accessAndRefreshToken',
};

export async function checkUserCredentials(email, password) {
  try {
    if (!email || !password) {
      log.info({ email }, 'No email or password found');
      throw authError('No email or password found');
    }
    const formattedEmail = email.trim().toLowerCase();
    const isValidEmail = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/.test(
      formattedEmail.toUpperCase(),
    );
    if (!isValidEmail) {
      log.info({ email }, 'Malformed email provided');
      throw authError('Malformed email provided');
    }
    const user = await UserInfoModel.findOne({ email: formattedEmail });
    if (!user) {
      log.info({ email }, 'User not found');
      throw authError('User not found');
    }
    const isValidPassword = bcrypt.compareSync(password, user.password);
    if (isValidPassword) {
      return user;
    }
    log.info({ email }, 'Invalid email and password combination');
    throw authError('Invalid email and password combination');
  } catch (error) {
    log.error({
      model: 'checkUserCredentials',
      message: 'Error validating credentials',
      email,
      error: JSON.stringify(error),
    });
    throw error;
  }
}

export async function saveToken(type, token, expires, data) {
  const key = util.format(keys[type], token);
  const now = Math.round(new Date().getTime() / 1000);
  const ttl = Math.round(expires.getTime() / 1000) - now;
  const userAccessAndRefreshToken = util.format(
    keys.userAccessAndRefreshToken,
    data[data.indexOf('userId') + 1],
  );
  try {
    await redisPromise().hmset(key, data);
    // userAccessAndRefreshToken keeps an audit trail of tokens: (key, expiration)
    await redisPromise().zadd(userAccessAndRefreshToken, now + ttl, key);
    // pruning audit trail of expired tokens
    await redisPromise().zremrangebyscore(userAccessAndRefreshToken, -Infinity, now);
    await redisPromise().expire(key, ttl);
    const auditTrailttl = await redisPromise().ttl(userAccessAndRefreshToken);
    // If userAccessAndRefreshToken is set to expire sooner than the token granted, set expire
    if (auditTrailttl < ttl) {
      await redisPromise().expire(userAccessAndRefreshToken, ttl);
    }
    const savedToken = await redisPromise().hgetall(key);
    return savedToken;
  } catch (error) {
    log.error({
      model: 'saveToken',
      message: 'Error saving tokens to Redis',
      token,
      userId: userAccessAndRefreshToken,
      error: JSON.stringify(error),
    });
    return error;
  }
}

export async function invalidateToken(userId) {
  const userAccessAndRefreshToken = util.format(keys.userAccessAndRefreshToken, userId);
  const keysToDelete = [['del', userAccessAndRefreshToken]];
  try {
    const tokensToDelete = await redisPromise().zrevrangebyscore(
      userAccessAndRefreshToken,
      Infinity,
      -Infinity,
    );
    tokensToDelete.forEach((token) => keysToDelete.push(['del', token]));
    const result = cacheClient().multi(keysToDelete).exec();

    const { email } = await UserInfoModel.findOne({ _id: userId });
    await redisPromise().del(`auth:username:${email}`);
    newrelic.addCustomAttribute('userId', userId);
    return result;
  } catch (err) {
    log.error({
      model: 'invalidateToken',
      message: 'Error invalidating token',
      userId,
      error: JSON.stringify(err),
    });
    return err;
  }
}
