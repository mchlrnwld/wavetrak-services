import mongoose from 'mongoose';

const ScopeSchema = mongoose.Schema(
  {
    name: { type: String },
    description: String,
  },
  { collection: 'Scopes' },
);

const ClientIdSchema = mongoose.Schema(
  {
    name: String,
    description: String,
    secret: String,
    redirectURI: String,
    appImageURL: String,
    // Deprecated. _id is now used as the clientId
    clientId: String,
    brand: String,
    trusted: Boolean,
    scopes: [
      {
        type: String,
        ref: 'Scope',
      },
    ],
  },
  { collection: 'ClientIds' },
);

ScopeSchema.index({ name: 1 });

mongoose.model('Scope', ScopeSchema);

export default mongoose.model('ClientId', ClientIdSchema);
