/* eslint-disable no-underscore-dangle */
import util from 'util';
import bcrypt from 'bcrypt';
import { flatten } from 'lodash';
import newrelic from 'newrelic';

import * as dbContext from '../common/dbContext';
import * as logger from '../common/logger';
import * as utils from '../common/utils';

import UserInfoModel from './UserInfo';
import ClientGroupModel from './ClientGroup';
import { redisPromise } from '../common/dbContext';
import { saveToken } from './user';
import deleteFacebookToken from '../integrations/facebook/deleteFacebookToken';

const model = {};
const log = logger.logger('auth-service:model:auth');

// OLD token format
/*
const keys = {
  token: 'tokens:%s',
  client: 'clients:%s',
  refreshToken: 'refresh_tokens:%s',
  grantTypes: 'clients:%s:grant_types',
  userAccessAndRefreshToken: 'users:%s:accessAndRefreshToken',
};
*/

// New token format
const keys = {
  bearerToken: 'bearer_token:%s',
  refreshToken: 'refresh_token:%s',
  userAccessAndRefreshToken: 'users:%s:accessAndRefreshToken',
};

const accessTokenLifetime = process.env.accessTokenLifetime
  ? parseInt(process.env.accessTokenLifetime, 10)
  : 2592000;
const refreshTokenLifetime = process.env.refreshTokenLifetime
  ? parseInt(process.env.refreshTokenLifetime, 10)
  : 31536000;
const accessTokenShortLived = process.env.accessTokenShortLived
  ? parseInt(process.env.accessTokenShortLived, 10)
  : 1800;

model.generateAccessToken = (req) =>
  new Promise(async (resolve, reject) => {
    try {
      const token = await utils.createToken();
      if (!token) return reject(new Error('Error generating access token'));

      const expires = new Date();
      const dateNowInSeconds = new Date().getSeconds();
      const expiresInSeconds =
        req.query && req.query.isShortLived === 'true'
          ? dateNowInSeconds + accessTokenShortLived
          : dateNowInSeconds + accessTokenLifetime;
      expires.setSeconds(expiresInSeconds);
      log.trace(`generateAccessToken expires: ${expires}`);
      const { device_id: deviceId, device_type: deviceType } = req.body;
      return model.saveAccessToken(
        token,
        req.body.client_id,
        expires,
        req.user,
        deviceId,
        deviceType,
        (err, success) => {
          if (err || !success) return reject(err);
          return resolve({ accessToken: token });
        },
      );
    } catch (error) {
      return reject(error);
    }
  });

model.generateRefreshToken = (req) =>
  new Promise(async (resolve, reject) => {
    try {
      const token = await utils.createToken();
      if (!token) return reject(new Error('Error generating refresh token'));

      const expires = new Date();
      const dateNowInSeconds = new Date().getSeconds();
      const expiresInSeconds =
        req.query && req.query.isShortLived === 'true'
          ? dateNowInSeconds + accessTokenShortLived
          : dateNowInSeconds + refreshTokenLifetime;
      expires.setSeconds(expiresInSeconds);
      log.trace(`generateAccessToken expires: ${expires}`);
      const { device_id: deviceId, device_type: deviceType } = req.body;
      return model.saveRefreshToken(
        token,
        req.body.client_id,
        expires,
        req.user,
        deviceId,
        deviceType,
        (err, success) => {
          if (err || !success) return reject(err);
          return resolve({ refreshToken: token });
        },
      );
    } catch (error) {
      return reject(error);
    }
  });

model.invalidateToken = (userId) =>
  new Promise((resolve, reject) => {
    log.trace(`invalidateToken for id: ${userId}`);
    const userAccessAndRefreshToken = util.format(keys.userAccessAndRefreshToken, userId);
    const keysToDelete = [['del', userAccessAndRefreshToken]];

    // Get all access and refresh tokens for the given userid
    log.trace('userAccessAndRefreshToken', userAccessAndRefreshToken);
    dbContext
      .cacheClient()
      .zrevrangebyscore(userAccessAndRefreshToken, Infinity, -Infinity, (err, tokens) => {
        if (err) {
          reject(err);
        } else if (tokens) {
          log.trace('tokens to delete', tokens);
          tokens.forEach((token) => keysToDelete.push(['del', token]));
        }

        log.trace('Keys to delete:', keysToDelete);
        dbContext
          .cacheClient()
          .multi(keysToDelete)
          .exec((error, results) => {
            log.trace(`invalidateToken results after del: ${results}`);
            if (error) {
              reject(error);
            } else {
              resolve(results);
            }
          });
      });
  });

/**
 *  userId <- get refreshTokenHash <- refreshToken
 *  zrem refreshToken from userAccessAndRefreshToken
 *  del refreshToken <- refreshToken
 *
 * @param token
 * @returns Promise that resolves 1 for success or 0 for Token Not Found
 */
model.deleteRefreshToken = (token) =>
  new Promise(async (resolve, reject) => {
    try {
      let refreshToken = util.format(keys.refreshToken, token);
      let userId = await redisPromise().hget(refreshToken, 'userId');
      if (!userId) {
        const oldTokenFormat = 'refresh_tokens:%s';
        refreshToken = util.format(oldTokenFormat, token);
        userId = await redisPromise().hget(refreshToken, 'userId');
      }
      if (userId) {
        await redisPromise().del(refreshToken);
        const results = await redisPromise().zrem(
          util.format(keys.userAccessAndRefreshToken, userId),
          refreshToken,
        );
        newrelic.addCustomAttribute('userId', userId);
        resolve(results);
      } else {
        resolve(0);
      }
    } catch (err) {
      log.error({
        model: 'deleteRefreshToken',
        message: 'Error deleting refreshToken',
        token,
        error: JSON.stringify(err),
      });
      reject(err);
    }
  });

/**
 *  userId <- get accessTokenHash <- accessToken
 *  zrem accessToken from userAccessAndRefreshToken
 *  del token <- accessToken
 *
 * @param token
 * @returns Promise that resolves 1 for success or 0 for Token Not Found
 */
model.deleteAccessToken = (token) =>
  new Promise(async (resolve, reject) => {
    try {
      let accessToken = util.format(keys.bearerToken, token);
      let userId = await redisPromise().hget(accessToken, 'userId');
      if (!userId) {
        const oldTokenFormat = 'tokens:%s';
        accessToken = util.format(oldTokenFormat, token);
        userId = await redisPromise().hget(accessToken, 'userId');
      }
      if (userId) {
        await redisPromise().del(accessToken);
        const results = await redisPromise().zrem(
          util.format(keys.userAccessAndRefreshToken, userId),
          accessToken,
        );
        newrelic.addCustomAttribute('userId', userId);
        resolve(results);
      } else {
        resolve(0);
      }
    } catch (err) {
      log.error({
        model: 'deleteAccessToken',
        message: 'Error deleting accessToken',
        token,
        error: JSON.stringify(err),
      });
      reject(err);
    }
  });

model.generateToken = (type, req, callback) => {
  if (type === 'accessToken' && req.body && req.user && req.oauth) {
    model
      .checkTokenLimit(
        req.body.client_id,
        req.user.id,
        req.oauth.client,
        req.body.forced,
        req.body.username,
      )
      .then(() => model.generateAccessToken(req))
      .then((token) => callback(null, token))
      .catch((error) => {
        callback(new Error(error));
      });
  } else if (type === 'refreshToken') {
    model
      .generateRefreshToken(req)
      .then((token) => {
        callback(null, token);
      })
      .catch((error) => {
        callback(error);
      });
  } else {
    callback(new Error('Invalid or missing type parameter'));
  }
};

model.getAccessToken = async (bearerToken, callback) => {
  try {
    const tokenNew = await redisPromise().hgetall(util.format(keys.bearerToken, bearerToken));
    if (tokenNew) {
      return callback(null, {
        accessToken: tokenNew.bearerToken,
        clientId: tokenNew.clientId,
        expires: tokenNew.expires ? new Date(tokenNew.expires) : null,
        userId: tokenNew.userId,
      });
    }

    const oldTokenFormat = 'tokens:%s';
    const tokenOld = await redisPromise().hgetall(util.format(oldTokenFormat, bearerToken));
    if (tokenOld) {
      // Delete token in old format
      await model.deleteAccessToken(bearerToken);
      const { tokenExpires } = utils.calculateTokenExpiry('bearerToken');
      const data = {
        clientId: tokenOld.clientId,
        userId: tokenOld.userId,
        scopes: tokenOld.scopes || 'all',
        deviceId: tokenOld.deviceId || '',
        deviceType: tokenOld.deviceType || '',
        expires: tokenExpires.toISOString(),
        token: bearerToken,
      };
      const tokenData = utils.formatTokenDataForRedis('bearerToken', data);
      // Store token in new format
      const tokenResult = await saveToken('bearerToken', bearerToken, tokenExpires, tokenData);
      return callback(null, {
        accessToken: tokenResult.bearerToken,
        clientId: tokenResult.clientId,
        expires: tokenResult.expires ? new Date(tokenResult.expires) : null,
        userId: tokenResult.userId,
      });
    }
    const userId = await redisPromise().get(`fb-token:${bearerToken}`);
    if (userId && userId !== 0) {
      // Delete token in old format
      await deleteFacebookToken(bearerToken);
      const { tokenExpires } = utils.calculateTokenExpiry('bearerToken');
      const data = {
        clientId: 'authclientId',
        userId,
        scopes: 'all',
        expires: tokenExpires.toISOString(),
        bearerToken,
      };
      const tokenData = utils.formatTokenDataForRedis('bearerToken', data);
      // Store token in new format
      const tokenResult = await saveToken('bearerToken', bearerToken, tokenExpires, tokenData);
      return callback(null, {
        accessToken: tokenResult.bearerToken,
        clientId: tokenResult.clientId,
        expires: tokenResult.expires ? new Date(tokenResult.expires) : null,
        userId: tokenResult.userId,
      });
    }
    // If reached here then the token do not exist
    log.error({
      model: 'getAccessToken',
      message: 'Token not found',
      bearerToken,
    });
    return callback();
  } catch (err) {
    if (err) {
      log.error({
        model: 'getAccessToken',
        err: err.toString(),
        bearerToken,
      });
      return callback(err);
    }
    return null;
  }
};

model.getClient = (clientId, clientSecret, callback) => {
  if (clientId === 'authclientId') {
    callback(null, { clientId, clientSecret });
  } else {
    return ClientGroupModel.findOne({ members: clientId }, (err, clientgroup) => {
      if (err) {
        log.error(`getClient() ${err}`);
        callback(err);
      } else if (clientgroup) {
        callback(null, { clientId, clientSecret, clientgroup });
      } else {
        log.debug(`getClient() No client found for ${clientId}`);
        callback();
      }
    });
  }
  return null;
};

model.getRefreshToken = async (refreshToken, callback) => {
  try {
    const tokenNew = await redisPromise().hgetall(util.format(keys.refreshToken, refreshToken));
    if (tokenNew) {
      return callback(null, {
        refreshToken: tokenNew.refreshToken,
        clientId: tokenNew.clientId,
        expires: tokenNew.expires ? new Date(tokenNew.expires) : null,
        userId: tokenNew.userId,
      });
    }

    const oldTokenFormat = 'refresh_tokens:%s';
    const tokenOld = await redisPromise().hgetall(util.format(oldTokenFormat, refreshToken));
    if (tokenOld) {
      // Delete token in old format
      await model.deleteRefreshToken(refreshToken);
      const { tokenExpires } = utils.calculateTokenExpiry('refreshToken');
      const data = {
        clientId: tokenOld.clientId,
        userId: tokenOld.userId,
        scopes: tokenOld.scopes || 'all',
        deviceId: tokenOld.deviceId || '',
        deviceType: tokenOld.deviceType || '',
        expires: tokenExpires.toISOString(),
        token: refreshToken,
      };
      const tokenData = utils.formatTokenDataForRedis('refreshToken', data);
      // Store token in new format
      const tokenResult = await saveToken('refreshToken', refreshToken, tokenExpires, tokenData);
      return callback(null, {
        refreshToken: tokenResult.refreshToken,
        clientId: tokenResult.clientId,
        expires: tokenResult.expires ? new Date(tokenResult.expires) : null,
        userId: tokenResult.userId,
      });
    }

    // If reached here then the token do not exist
    log.error({
      model: 'getRefreshToken',
      message: 'Could not find refresh token',
      refreshToken,
    });
    return callback();
  } catch (err) {
    if (err) {
      newrelic.noticeError(err);
      return callback(err);
    }
    return null;
  }
};

model.grantTypeAllowed = (clientId, grantType, callback) => {
  // Commenting the actual implementation to bypass the default oAuth implementation
  // dbContext.cacheClient().sismember(util.format(keys.grantTypes, clientId), grantType, callback);
  callback(null, true);
};

model.saveAccessToken = async (
  accessToken,
  clientId,
  expires,
  user,
  deviceId = '',
  deviceType = '',
  callback,
) => {
  const scopes = 'all';
  const data = {
    clientId,
    userId: user.id,
    expires: expires ? expires.toISOString() : null,
    scopes,
    deviceId,
    deviceType,
    token: accessToken,
  };
  const tokenData = utils.formatTokenDataForRedis('bearerToken', data);
  try {
    await saveToken('bearerToken', accessToken, expires, tokenData);
    return callback(null, 1);
  } catch (err) {
    return callback(err);
  }
};

model.saveRefreshToken = async (
  refreshToken,
  clientId,
  expires,
  user,
  deviceId = '',
  deviceType = '',
  callback,
) => {
  const scopes = 'all';
  const data = {
    clientId,
    userId: user.id,
    expires: expires ? expires.toISOString() : null,
    scopes,
    deviceId,
    deviceType,
    token: refreshToken,
  };
  const tokenData = utils.formatTokenDataForRedis('refreshToken', data);
  try {
    await saveToken('refreshToken', refreshToken, expires, tokenData);
    return callback(null, 1);
  } catch (err) {
    return callback(err);
  }
};

// eslint-disable-next-line consistent-return
model.getUser = async (email, password, callback) => {
  if (!email || !password) {
    log.warn('getUser() No email or password passed in');
    return callback('No email or password passed in');
  }
  try {
    const emailLowerCase = email.toLowerCase();
    const user = await UserInfoModel.findOne({ email: emailLowerCase });
    if (user) {
      bcrypt.compare(password, user.password, (error, same) => {
        if (error || !same) {
          log.debug(`getUser() Incorrect password for ${email}`);
          return callback();
        }
        return callback(null, { id: user._id.toString() });
      });
    } else {
      const errObj = { message: 'No user found' };
      throw errObj;
    }
  } catch (error) {
    log.debug(`getUser() No user found for password for ${email}`);
    return callback();
  }
};

model.checkTokenLimit = (clientId, userId, clientObj, isForced = false, username) =>
  new Promise(async (resolve, reject) => {
    let emailWhiteListed = false;
    try {
      emailWhiteListed = await utils.checkWhitelistedEmails(username);
    } catch (error) {
      reject(error);
    }
    if (
      clientId === 'authclientId' ||
      clientObj.clientgroup.name === 'whitelist' ||
      emailWhiteListed
    ) {
      resolve(1);
      return;
    }
    const userKey = util.format(keys.userAccessAndRefreshToken, userId);
    dbContext.cacheClient().zrange(userKey, '0', '-1', (err, res) => {
      if (err) {
        reject(err);
      } else if (res) {
        const tokensArray = res.filter((item) => item.startsWith('tokens:'));
        const execMulti = dbContext.cacheClient().multi();
        tokensArray.forEach((token) =>
          execMulti.hmget(token, 'clientId', 'expires', 'accessToken'),
        );
        execMulti.exec(async (execErr, replies) => {
          if (execErr) {
            reject(execErr);
          }
          if (replies) {
            /*
            replies is an array of arrays.
            Each array consists of [<clientId>, <expires>, <accessToken>]
            HMGET returns the values in the same order as they are requested.
            arr[0] => clientId, arr[1] => expires, arr[2] => accessToken
            */
            const groupmembers = clientObj.clientgroup.members;
            const filteredTokensArray = flatten(
              groupmembers.map((member) => replies.filter((item) => item[0] === member)),
            );
            const tokenCount = filteredTokensArray.length;
            const forced = clientObj.clientgroup.name === 'legacy' ? 'true' : isForced;
            if (forced === 'true' && tokenCount >= clientObj.clientgroup.tokenLimit) {
              try {
                await model.deleteStaleTokens(userId, clientObj, filteredTokensArray);
                resolve('Tokens deleted successfully');
              } catch (error) {
                reject(new Error('Error deleting tokens'));
              }
            }
            if (tokenCount < clientObj.clientgroup.tokenLimit) {
              log.debug(`checkTokenLimit() - tokenCount less than limit for
              User: ${userId} and Client: ${clientObj.clientId}`);
              resolve('User token count is below the token limit');
            } else {
              log.debug(`checkTokenLimit() - tokenCount exceeded the limit for
              User: ${userId} and Client: ${clientObj.clientId}`);
              reject(new Error('User have exceeded the token limit'));
            }
          }
        });
      } else {
        reject();
      }
    });
  });

model.deleteStaleTokens = async (userId, clientObj, tokensArray) => {
  // Sort tokens based on the expiry date
  tokensArray.sort((obj1, obj2) => new Date(obj1[1]) - new Date(obj2[1]));
  // Remove tokens which are greater than the token limit.
  // +1 to make space for the newly created token.
  const numberofTokensToRemove = tokensArray.length - clientObj.clientgroup.tokenLimit + 1;
  const deleteTokensArray = tokensArray.splice(0, numberofTokensToRemove);
  const lastDeletedToken = await Promise.all(
    deleteTokensArray.map((token) => model.deleteAccessToken(token[2])),
  );
  log.debug(`deleteStaleTokens() - stale tokens deleted for
    User: ${userId}, Client: ${clientObj.clientId} and deleteTokensArray: ${deleteTokensArray}`);
  return lastDeletedToken;
};
export default model;
