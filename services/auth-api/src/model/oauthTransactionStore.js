import { createToken } from '../common/utils';
import { redisPromise } from '../common/dbContext';
import { authError } from '../common/customErrors';

function generateKey(transactionId) {
  return `oauth:transactionId:${transactionId}`;
}

class TransactionStore {
  constructor() {
    this.legacy = true;
  }
}

TransactionStore.prototype.load = async (server, options, req, cb) => {
  const formField = 'transaction_id';
  const query = req.query || {};
  const body = req.body || {};
  const transactionId = query[formField] || body[formField];
  if (!transactionId) {
    return cb(new Error(400, { message: `Missing required parameter: ${formField}` }));
  }

  try {
    const redisKey = generateKey(transactionId);
    const transactionKey = await redisPromise().get(redisKey);
    const txn = JSON.parse(transactionKey);
    return server.deserializeClient(txn.client, (err, client) => {
      if (err) {
        return cb(err);
      }
      if (!client) {
        return cb(authError('Unauthorized client', 'unauthorized_client'));
      }
      txn.transactionID = transactionId;
      txn.client = client;
      return cb(null, txn);
    });
  } catch (error) {
    return cb(error);
  }
};

TransactionStore.prototype.store = function storeKey(server, options, req, txn, cb) {
  return server.serializeClient(txn.client, async (err, client) => {
    try {
      if (err) {
        return cb(err);
      }

      const transactionId = await createToken();
      // eslint-disable-next-line no-param-reassign
      txn.client = client;
      const redisKey = generateKey(transactionId);
      await redisPromise().set(redisKey, JSON.stringify(txn), 'EX', 300);
      return cb(null, transactionId);
    } catch (error) {
      return cb(error);
    }
  });
};

TransactionStore.prototype.update = function updateKey(
  server,
  options,
  req,
  transactionId,
  txn,
  cb,
) {
  try {
    return server.serializeClient(txn.client, async (err, obj) => {
      if (err) {
        return cb(err);
      }

      // eslint-disable-next-line no-param-reassign
      txn.client = obj;
      const redisKey = generateKey(transactionId);
      await redisPromise().set(redisKey, JSON.stringify(txn), 'EX', 300);

      return cb(null, transactionId);
    });
  } catch (error) {
    return cb(error);
  }
};

TransactionStore.prototype.remove = async (transactionId) => {
  const redisKey = generateKey(transactionId);
  await redisPromise().del(redisKey);
};

export default TransactionStore;
