import { redisPromise } from '../../common/dbContext';
import * as logger from '../../common/logger';

const log = logger.logger('auth-service:deletedFacebookTokens');

/**
 * Deletes both a facebook access and refresh token.
 *
 * Since facebook tokens do not follow the standardized token storage
 * in Redis, this function accommodates the unconventional means of
 * deleting them.
 *
 * ** TECH DEBT **
 * Facebook token storage should be conventionalized to follow
 * the auth access and refresh token data structors.  This
 * function should be deleted when those changes have been
 * completed.
 *
 * @param {String | prepended with "fb-token"} token
 *
 */
export default (token) =>
  new Promise(async (resolve, reject) => {
    try {
      await redisPromise().del(`fb-token:${token}`);
      return resolve();
    } catch (error) {
      log.error('facebook token error: cannot delete');
      return reject(error);
    }
  });
