/* eslint-disable no-underscore-dangle,no-param-reassign */
import request from 'request';
import { redisPromise } from '../../common/dbContext';
import * as logger from '../../common/logger';
import deleteFacebookToken from './deleteFacebookToken';
import { formatTokenDataForRedis, calculateTokenExpiry } from '../../common/utils';
import { saveToken } from '../../model/user';

export const validateFBGraphToken = (token) =>
  new Promise((resolve, reject) => {
    const fbAppAccessToken = process.env.FB_APP_ID;
    const fbAppSecret = process.env.FB_APP_SECRET;
    const options = {
      uri: 'https://graph.facebook.com/debug_token',
      qs: {
        input_token: token,
        access_token: `${fbAppAccessToken}|${fbAppSecret}`,
      },
      json: true,
      timeout: 8000,
    };
    request(options, (error, response, body) => {
      const errResponse = {
        code: 401,
        error: 'invalid_token',
        error_description: 'The access token provided is invalid.',
      };
      if (error) return reject(errResponse);
      if (body.data && body.data.error) return reject(errResponse);
      if (body.data && body.data.is_valid && body.data.user_id) {
        return resolve(body.data.user_id);
      }
      return reject(errResponse);
    });
  });

const getFBTokenFromRedis = (token) =>
  new Promise(async (resolve, reject) => {
    try {
      const tokenNew = await redisPromise().hgetall(`bearer_token:${token}`);
      if (tokenNew) {
        return resolve(tokenNew.userId);
      }
      const userId = await redisPromise().get(`fb-token:${token}`);
      if (userId && userId !== 0) {
        // Delete token in old format
        await deleteFacebookToken(token);
        const { tokenExpires } = calculateTokenExpiry('bearerToken');
        const data = {
          clientId: 'authclientId',
          userId,
          scopes: 'all',
          expires: tokenExpires.toISOString(),
          token,
        };
        const tokenData = formatTokenDataForRedis('bearerToken', data);
        // Store token in new format
        const savedToken = await saveToken('bearerToken', token, tokenExpires, tokenData);
        return resolve(savedToken.userId);
      }

      // If reached here then the token do not exist
      const errorResponse = new Error('The access token provided is invalid.', {
        cause: 'invalid_token',
      });
      errorResponse.code = 401;
      return reject(errorResponse);
    } catch (err) {
      const errorResponse = new Error('The access token provided is invalid.', {
        cause: 'invalid_token',
      });
      errorResponse.code = 401;
      return reject(errorResponse);
    }
  });

export default async (req, res, next) => {
  const log = logger.logger('auth-service:authoriseFacebookToken');
  const { access_token: accessToken } = req.query;
  const facebookToken = accessToken.substring(8);
  try {
    const userId = await getFBTokenFromRedis(accessToken);
    await validateFBGraphToken(facebookToken);
    req.user = { id: userId };
    next();
  } catch (error) {
    log.error('auth/authorise: request error', error);
    res.status(401);
    req.user = error;
    next();
  }
};
