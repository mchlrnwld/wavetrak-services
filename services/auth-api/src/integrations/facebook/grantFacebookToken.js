/* eslint-disable no-underscore-dangle,no-param-reassign,no-unused-vars,consistent-return,no-new */
import request from 'request';
import * as logger from '../../common/logger';
import { findUserByLinkedAccount, createOrUpdateIntegratedUser } from '../../external/user';
import { createToken, formatTokenDataForRedis, calculateTokenExpiry } from '../../common/utils';
import { saveToken } from '../../model/user';

const log = logger.logger('auth-service:grantFacebookToken');

const generateTokenResponse = async (req, res, user) => {
  try {
    const clientId = req.body.client_id || 'authclientId';
    const userId = user._id;
    const { scopes = 'all', deviceId, deviceType } = req.body;
    const bearerToken = await createToken();
    const refreshToken = await createToken();
    const {
      tokenExpires: bearerTokenExpires,
      tokenLifetime: bearerTokenLifetime,
    } = calculateTokenExpiry('bearerToken');
    const { tokenExpires: refreshTokenExpires } = calculateTokenExpiry('refreshToken');
    const data = {
      clientId,
      userId,
      scopes,
      deviceId,
      deviceType,
    };
    const btData = {
      ...data,
      token: bearerToken,
      expires: bearerTokenExpires ? bearerTokenExpires.toISOString() : null,
    };
    const bearerTokenData = formatTokenDataForRedis('bearerToken', btData);
    const rtData = {
      ...data,
      token: refreshToken,
      expires: refreshTokenExpires ? refreshTokenExpires.toISOString() : null,
    };
    const refreshTokenData = formatTokenDataForRedis('refreshToken', rtData);
    await saveToken('bearerToken', bearerToken, bearerTokenExpires, bearerTokenData);
    await saveToken('refreshToken', refreshToken, refreshTokenExpires, refreshTokenData);

    const tokenResponse = {
      access_token: bearerToken,
      expires_in: bearerTokenLifetime,
      refresh_token: refreshToken,
      token_type: 'bearer',
    };
    req.body.token_response = tokenResponse;
  } catch (err) {
    res.status(401);
    throw err;
  }
};

const findOrCreateIntegratedUser = async (userInfo) => {
  try {
    const foundUser = await findUserByLinkedAccount(userInfo.linkedAccount.facebook.id, 'facebook');
    return foundUser;
  } catch (err) {
    /* User Not Found --> Create New User */
    if (err.statusCode === 400) {
      try {
        const integratedUser = await createOrUpdateIntegratedUser(userInfo);
        return integratedUser;
      } catch (updateOrCreateUserErr) {
        log.error('auth/token: grantFacebookToken: createOrUpdateIntegratedUser error', err);
        return updateOrCreateUserErr;
      }
    } else {
      log.error('auth/token: grantFacebookToken: findUserByLinkedAccount error', err);
      return err;
    }
  }
};

export default (req, res, next) => {
  /* exchange short lived token for extended token ~60 days */
  const fbAppAccessToken = process.env.FB_APP_ID;
  const fbAppSecret = process.env.FB_APP_SECRET;
  const options = {
    uri: 'https://graph.facebook.com/oauth/access_token',
    qs: {
      grant_type: 'fb_exchange_token',
      client_id: fbAppAccessToken,
      client_secret: fbAppSecret,
      fb_exchange_token: req.body.fbToken,
    },
    json: true,
    timeout: 8000,
  };
  request(options, async (error, response, body) => {
    // matches standard OAuth error structure and response.
    // pass into req.user to expose correct json body to /route/auth.js
    const errResponse = {
      code: 401,
      error: 'invalid_request',
      error_description: 'Invalid request',
    };

    if (error || body.error) {
      let errorToLog;
      if (error) {
        errorToLog = error;
      } else if (body.error) {
        errorToLog = body.error;
      }
      log.error('auth/token: grantFacebookToken: request error', errorToLog);
      res.status(401);
      req.body.token_response = errResponse;
      next();
    } else {
      try {
        const userInfo = {
          email: req.body.username,
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          linkedAccount: {
            facebook: {
              id: req.body.facebookId,
            },
          },
          brand: req.body.brand,
        };
        const user = await findOrCreateIntegratedUser(userInfo);
        await generateTokenResponse(req, res, user);
        next();
      } catch (err) {
        log.error('auth/token: grantFacebookToken: request error', err);
        req.body.token_response = errResponse;
        next();
      }
    }
  });
};
