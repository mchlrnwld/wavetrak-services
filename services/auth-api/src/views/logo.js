/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
import React from 'react';

class Logo extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
        <path
          fill="#FFF"
          fillRule="evenodd"
          d="M8.923 20.817a5.843 5.843 0 0 1-1.457-.204 5.875 5.875 0 0 0 4.454-5.632 1.463 1.463 0 0 1 1.48-1.45c.805.01 1.45.677 1.438 1.487-.044 3.247-2.691 5.842-5.915 5.799zM4.605 16.35a1.465 1.465 0 0 1-1.441-1.489c.043-3.247 2.691-5.842 5.915-5.798.504.005.992.077 1.458.204a5.87 5.87 0 0 0-4.455 5.633 1.463 1.463 0 0 1-1.477 1.45zm12.552-5.242C16.147 8.505 9 0 9 0S1.852 8.505.844 11.109A9.063 9.063 0 0 0 0 14.939C0 19.944 4.03 24 9 24s9-4.056 9-9.061a9.073 9.073 0 0 0-.843-3.83z"
        />
      </svg>
    );
  }
}

export default Logo;
