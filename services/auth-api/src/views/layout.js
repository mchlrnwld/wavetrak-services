/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-unused-vars */
import React from 'react';
import * as styles from './styles';
import Logo from './logo';

class Layout extends React.Component {
  render() {
    return (
      <html lang="en">
        <head>
          <title>Surfline Connect</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <style dangerouslySetInnerHTML={styles.fontFace} />
        </head>
        <body style={{ margin: '0px' }}>
          <header style={styles.header}>
            <Logo />
          </header>
          <div style={{ margin: '0 32px 0 32px' }}>
            <div className="container" style={styles.container}>
              {this.props.children}
            </div>
          </div>
        </body>
      </html>
    );
  }
}

export default Layout;
