/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-unused-vars */
import React from 'react';
import * as styles from './styles';
import Layout from './layout';
import LoginForm from './loginForm';

const fb = `
  document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('do-fb-login').addEventListener('click', function() {
      FB.login(function(response) {
        if (response.status === 'connected' && response.authResponse.accessToken) {
          document.getElementById('access_token').value = response.authResponse.accessToken;
          document.getElementById('login-form-facebook').submit()
        } else {
          alert('Something went wrong, please try again.');
        }
      }, { scope: 'public_profile,email' });
    });
  });

  window.fbAsyncInit = function() {
    FB.init({
      appId: '${process.env.FB_APP_ID}',
      cookie: true,
      xfbml: true,
      version: 'v2.8'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
`;

class Login extends React.Component {
  render() {
    const { name, appImageURL, clientId, error } = this.props;
    return (
      <Layout>
        <script dangerouslySetInnerHTML={{ __html: fb }} />
        {appImageURL && (
          <div style={styles.partnerLogo}>
            <img style={{ width: '48px', height: '48px' }} src={appImageURL} alt={name} />
          </div>
        )}
        <div style={styles.title}>Log into your Surfline account to connect to {name}</div>
        <div style={styles.subtitle}>
          Need an account?
          <a style={styles.link} href={`/create-account?client=${clientId}`}>
            Sign up
          </a>
        </div>
        {error ? <div style={styles.error}>{error}</div> : null}
        <LoginForm clientId={clientId} />

        <p
          style={{
            ...styles.buttonAlt,
            margin: '0px',
            textAlign: 'center',
            height: '41px',
          }}
        >
          or
        </p>
        <form style={styles.form} id="login-form-facebook" method="post" action="/login">
          <input name="auth_strategy" type="hidden" value="facebook" />
          <input name="access_token" type="hidden" id="access_token" />
          <button
            id="do-fb-login"
            type="button"
            style={{ ...styles.button, backgroundColor: '#3b5998', width: '100%' }}
          >
            Sign In with Facebook
          </button>
        </form>
      </Layout>
    );
  }
}

export default Login;
