/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-unused-vars */
import React from 'react';
import * as styles from './styles';
import Layout from './layout';
import CreateForm from './createForm';

class Create extends React.Component {
  render() {
    const { name, appImageURL, clientId, error } = this.props;
    return (
      <Layout>
        <div style={styles.partnerLogo}>
          <img style={{ width: '48px', height: '48px' }} src={appImageURL} alt={name} />
        </div>
        <div style={styles.title}>Create a Surfline account to connect to {name}</div>
        <div style={styles.subtitle}>
          Already have an account?
          <a style={styles.link} href={`/login?client=${clientId}`}>
            Sign In
          </a>
        </div>
        {error ? <div style={styles.error}>{error}</div> : null}
        <CreateForm clientId={clientId} />
      </Layout>
    );
  }
}

export default Create;
