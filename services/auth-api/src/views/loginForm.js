/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import * as styles from './styles';

class LoginForm extends React.Component {
  render() {
    const { clientId } = this.props;
    return (
      <form style={styles.form} action="/login" method="post">
        <div>
          <input name="auth_strategy" type="hidden" value="local" />
          <input
            className="login-input"
            style={styles.input}
            type="email"
            name="email"
            id="email"
            placeholder="EMAIL"
            required
          />
          <input
            className="login-input"
            style={{ ...styles.input, marginBottom: '12px' }}
            type="password"
            name="password"
            id="password"
            placeholder="PASSWORD"
            required
          />
          <input type="hidden" name="clientId" value={clientId} />
          <a
            style={{ ...styles.link, display: 'block', margin: '0 0 24px 0' }}
            href="http://surfline.com/forgot-password"
          >
            Forgot Password?
          </a>
          <input style={styles.button} type="submit" value="Connect Surfline" />
        </div>
      </form>
    );
  }
}

export default LoginForm;
