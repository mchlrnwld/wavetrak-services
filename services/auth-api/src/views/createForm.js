/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import * as styles from './styles';

class CreateForm extends React.Component {
  render() {
    const { clientId } = this.props;
    return (
      <form style={styles.form} action="/create-account" method="post">
        <div>
          <input
            className="login-input"
            style={styles.input}
            type="text"
            name="firstName"
            placeholder="FIRST NAME"
            required
          />
          <input
            className="login-input"
            style={styles.input}
            type="text"
            name="lastName"
            placeholder="LAST NAME"
            required
          />
          <input
            className="login-input"
            style={styles.input}
            type="email"
            name="email"
            placeholder="EMAIL"
            required
          />
          <input
            className="login-input"
            style={{ ...styles.input, marginBottom: '12px' }}
            type="password"
            name="password"
            placeholder="PASSWORD"
            required
          />
          <input type="hidden" name="clientId" value={clientId} />
          <input style={styles.button} type="submit" value="Connect Surfline" />
        </div>
      </form>
    );
  }
}

export default CreateForm;
