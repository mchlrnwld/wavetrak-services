/* eslint-disable react/no-array-index-key */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-unused-vars */
import React from 'react';
import * as styles from './styles';
import Layout from './layout';

class Dialog extends React.Component {
  render() {
    const { scopes, appImageURL, name } = this.props.client;
    return (
      <Layout>
        <div style={styles.partnerLogo}>
          <img style={{ width: '48px', height: '48px' }} src={appImageURL} alt={name} />
        </div>
        <div style={styles.title}>Authorize {name} to connect to Surfline</div>
        <div style={styles.subtitle}>{name} will be able to: </div>
        <div style={styles.authorizations}>
          {scopes.map((scope, index) => (
            <div style={styles.authorization}>
              <input
                type="checkbox"
                style={styles.checkbox}
                checked
                disabled
                id={scope.description}
                name={scope.description}
                value={scope.description}
                key={index}
              />
              <label htmlFor={scope.description} style={styles.label}>
                {scope.description}
              </label>
            </div>
          ))}
        </div>
        <form style={styles.form} action="/connect/authorize/decision" method="post">
          <input name="transaction_id" type="hidden" value={this.props.transactionID} />
          <div>
            <input style={styles.button} type="submit" value="Authorize" id="allow" />
            <input style={styles.buttonAlt} type="submit" value="Cancel" name="cancel" id="deny" />
          </div>
        </form>
        <div>
          <div style={styles.disclaimer}>
            {`You can revoke this connection at any time from the profile screen of the ${name} app.`}
          </div>
          <div style={styles.disclaimer}>
            {"By authorizing this application, you agree to Surfline's "}
            <a
              style={styles.disclaimerLink}
              href="https://www.surfline.com/privacy.cfm"
              target="_blank"
              rel="noreferrer"
            >
              Privacy Policy
            </a>
            {' and '}
            <a
              style={styles.disclaimerLink}
              href="https://www.surfline.com/terms_conditions.cfm"
              target="_blank"
              rel="noreferrer"
            >
              Terms of Service.
            </a>
          </div>
        </div>
      </Layout>
    );
  }
}

export default Dialog;
