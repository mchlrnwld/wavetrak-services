/* eslint-disable no-undef */
/* eslint-disable react/button-has-type */
// eslint-disable-next-line no-unused-vars
import React from 'react';

// eslint-disable-next-line no-unused-vars
import Layout from './layout';
import * as styles from './styles';

export const OAuthSuccess = ({ name, appImageURL }) => (
  <Layout>
    {appImageURL && (
      <div style={styles.partnerLogo}>
        <img style={{ width: '48px', height: '48px' }} src={appImageURL} alt={name} />
      </div>
    )}
    <div style={styles.title}>Successfully connected to {name}</div>
    <button style={{ ...styles.button, marginTop: '8px' }} onClick={() => window.close()}>
      Done
    </button>
  </Layout>
);

export default OAuthSuccess;
