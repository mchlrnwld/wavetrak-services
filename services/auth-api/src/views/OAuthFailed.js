/* eslint-disable react/button-has-type */
/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import React from 'react';

// eslint-disable-next-line no-unused-vars
import Layout from './layout';
import * as styles from './styles';

export const OAuthFailed = ({ name, appImageURL }) => (
  <Layout>
    {appImageURL && (
      <div style={styles.partnerLogo}>
        <img style={{ width: '48px', height: '48px' }} src={appImageURL} alt={name} />
      </div>
    )}
    <h1 style={styles.title}>Connection to {name} failed</h1>
    <h5 style={{ ...styles.subtitle, textAlign: 'center' }}>
      We have been notified of the issue and are working towards a resolution.
    </h5>
    <button style={{ ...styles.button, marginTop: '8px' }} onClick={() => window.close()}>
      Done
    </button>
  </Layout>
);

export default OAuthFailed;
