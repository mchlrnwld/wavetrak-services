export const fontFace = {
  __html: `
    @font-face {
      font-family: 'futura-surfline';
      src: url('https://wa.cdn-surfline.com/quiver/0.10.1/fonts/futura-surfline/futura-surfline-regular.woff2');
    }
    @font-face {
      font-family: 'source-sans-pro';
      font-style: 'normal';
      font-weight: 400;
      src: url('https://fonts.gstatic.com/s/sourcesanspro/v9/ODelI1aHBYDBqgeIAH2zlJbPFduIYtoLzwST68uhz_Y.woff2');
    }
    @font-face {
      font-family: 'source-sans-pro-bold';
      font-style: 'normal';
      font-weight: bold;
      src: url('https://fonts.gstatic.com/s/sourcesanspro/v9/ODelI1aHBYDBqgeIAH2zlJbPFduIYtoLzwST68uhz_Y.woff2');
    }
    *:focus { outline: none }
    .login-input::-webkit-input-placeholder { font-size: 14px; color: #CCCCCC }
    .login-input:-moz-placeholder { font-size: 14px; color: #CCCCCC }
    .login-input::-moz-placeholder { font-size: 14px; color: #CCCCCC }
    .login-input:-ms-input-placeholder { font-size: 14px; color: #CCCCCC }
  `,
};

export const header = {
  width: '100%',
  height: '54px',
  display: 'flex',
  justifyContent: 'center',
  backgroundColor: '#003366',
  alignItems: 'center',
  margin: '0 0 32px 0',
  color: '#ffffff',
  fontFamily: 'futura-surfline',
};

export const container = {
  maxWidth: '500px',
  margin: '0 auto',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column',
};

export const partnerLogo = {
  width: '100%',
  height: '48px',
  margin: '0 0 16px 0',
  textAlign: 'center',
};

export const title = {
  width: '100%',
  fontFamily: 'futura-surfline',
  fontSize: '21px',
  color: '#333333',
  margin: '0 0 24px 0',
  textAlign: 'center',
};

export const subtitle = {
  width: '100%',
  fontFamily: 'source-sans-pro-bold',
  fontSize: '19px',
  letterSpacing: '0px',
  color: '#555555',
  margin: '0 0 8px 0',
  fontWeight: 600,
};

export const authorizations = {
  margin: '0 0 64px 0',
  width: '100%',
};

export const authorization = {
  display: 'flex',
};

export const checkbox = {
  marginRight: '8px',
};

export const label = {
  fontFamily: 'source-sans-pro',
  fontSize: '14px',
  color: '#333333',
};

export const link = {
  margin: '0 0 0 4px',
  color: '#42a5fc',
  textDecoration: 'none',
  fontFamily: 'source-sans-pro',
  fontSize: '19px',
  letterSpacing: '0px',
};

export const form = {
  width: '100%',
};

export const input = {
  display: 'block',
  width: 'calc(100% - 32px)',
  padding: '0 16px',
  height: '55px',
  border: 'solid 1px #d2d6d9',
  margin: '0 0 24px 0',
  fontFamily: 'source-sans-pro',
  fontSize: '16px',
  letterSpacing: '1.1px',
};

export const button = {
  width: '100%',
  height: '59px',
  margin: '0 0 16px 0',
  backgroundColor: '#0058b0',
  textTransform: 'uppercase',
  fontFamily: 'futura-surfline',
  color: '#ffffff',
  fontSize: '16px',
  letterSpacing: '1.5px',
  border: 'solid 1px #0058b0',
  cursor: 'pointer',
};

export const buttonAlt = {
  width: '100%',
  height: '59px',
  backgroundColor: '#ffffff',
  textTransform: 'uppercase',
  fontFamily: 'futura-surfline',
  color: '#0058B0',
  fontSize: '16px',
  letterSpacing: '1.5px',
  border: 'none',
  cursor: 'pointer',
  margin: '0 0 16px 0',
};

export const error = {
  width: '100%',
  padding: '20px 0px',
  marginBottom: '16px',
  textAlign: 'center',
  fontFamily: 'source-sans-pro',
  letterSpacing: '1.1px',
  fontSize: '14px',
  backgroundColor: '#FFF3F4', // red 10
  color: '#FA5065', // red 50
};

export const disclaimer = {
  fontFamily: 'source-sans-pro',
  fontSize: '11px',
  color: '#7A8E97',
  marginBottom: '16px',
};

export const disclaimerLink = {
  textDecoration: 'none',
  color: '#555555',
};
