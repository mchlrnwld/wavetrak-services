/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-unused-vars */
import React from 'react';
import Layout from './layout';

// TODO remove prior to launch
class Index extends React.Component {
  render() {
    return (
      <Layout>
        {this.props.code}
        <h3>
          Link to get token:
          <a href="/connect/authorize?redirect_uri=http://localhost:8002&response_type=code&client_id=5af1ce73b5acf7c6dd2592ee&scope=sessions:write:user">
            'http://localhost:8002/connect/authorize?redirect_uri=http://localhost:8002&response_type=code&client_id=5af1ce73b5acf7c6dd2592ee&scope=sessions:write:user'
          </a>
        </h3>
      </Layout>
    );
  }
}

export default Index;
