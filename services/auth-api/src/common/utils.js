import crypto from 'crypto';
import * as dbContext from './dbContext';
import { redisPromise } from './dbContext';
import { authError, RateLimitError } from './customErrors';

/**
 * Verifies if the email passed is whitelisted in the Redis Set "whitelisted_emails".
 * @param {string} email
 */
export const checkWhitelistedEmails = (email) =>
  new Promise(async (resolve) => {
    await dbContext.cacheClient().sismember('whitelisted_emails', email, (err, res) => {
      if (err) {
        resolve(err);
      }
      resolve(res);
    });
  });

export const createToken = () =>
  new Promise((resolve, reject) => {
    crypto.randomBytes(256, (ex, buffer) => {
      if (ex) reject(new Error('Server error while generating token'));
      const token = crypto.createHash('sha1').update(buffer).digest('hex');
      resolve(token);
    });
  });

export const formatTokenDataForRedis = (type, data) => {
  const { token, clientId, userId, expires, scopes = '', deviceId = '', deviceType = '' } = data;
  const hashSet = [
    'clientId',
    clientId,
    'userId',
    userId,
    'expires',
    expires,
    'scopes',
    scopes,
    'deviceId',
    deviceId,
    'deviceType',
    deviceType,
  ];
  const tokenData = [type, token];
  tokenData.push(...hashSet);
  return tokenData;
};

export const calculateTokenExpiry = (type, isShortLived = false) => {
  const tokenExpires = new Date();
  let tokenLifetime = process.env.accessTokenLifetime
    ? parseInt(process.env.accessTokenLifetime, 10)
    : 2592000;
  if (type === 'refreshToken') {
    tokenLifetime = process.env.refreshTokenLifetime
      ? parseInt(process.env.refreshTokenLifetime, 10)
      : 31536000;
  }
  if (isShortLived === 'true') {
    tokenLifetime = process.env.accessTokenShortLived
      ? parseInt(process.env.accessTokenShortLived, 10)
      : 1800;
    tokenExpires.setSeconds(tokenExpires.getSeconds() + tokenLifetime);
  } else {
    tokenExpires.setSeconds(tokenExpires.getSeconds() + tokenLifetime);
  }
  return { tokenExpires, tokenLifetime };
};

export const validateBasicAuthRequest = async (req) => {
  const { username, grant_type: grantType } = req.body;
  if (grantType !== 'password') return true;

  if (!username) {
    throw authError('Missing parameter: username');
  }
  const numAttempts = await redisPromise().get(`auth:username:${username}`);
  if (!numAttempts) {
    return redisPromise().set(`auth:username:${username}`, 1, 'EX', 300);
  }
  if (numAttempts < 10) {
    return redisPromise().incr(`auth:username:${username}`);
  }
  throw new RateLimitError('Too many failed login attempts, please try again in 5 minutes.', 429);
};

/**
 * @description Helper function for formatting the Token Redis keys
 * @returns {String}
 */
export const tokenKeys = {
  bearerToken: 'bearer_token:%s',
  refreshToken: 'refresh_token:%s',
  userAccessAndRefreshToken: 'users:%s:accessAndRefreshToken',
};
