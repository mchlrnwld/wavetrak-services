/* istanbul ignore file */
import { promisify } from 'util';
import redis from 'redis';
import mongoose from 'mongoose';
import { logger } from './logger';

const log = logger('auth-service:common:dbContext');

let redisClient = {};

export function initCache() {
  const connectionString = `redis://${process.env.REDIS_IP}:${process.env.REDIS_PORT}`;
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Redis ConnectionString: ${connectionString} `);
      redisClient = redis.createClient(connectionString);
      redisClient.once('ready', () => {
        log.info(`Redis connected on ${connectionString} and ready to accept commands`);
        resolve();
      });
      redisClient.on('error', (error) => {
        log.error({
          action: 'Redis:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export function initDB() {
  const connectionString = process.env.MONGO_CONNECTION_STRING;
  const mongoDbConfig = {
    poolSize: 10,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      log.debug(`Mongo ConnectionString: ${connectionString} `);

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        log.info(`MongoDB connected on ${connectionString}`);
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        log.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        action: 'MongoDB:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export function cacheClient() {
  return redisClient;
}

let promisifiedRedis = null;
export const redisPromise = () => {
  if (!promisifiedRedis) {
    promisifiedRedis = {
      hgetall: promisify(cacheClient().hgetall).bind(cacheClient()),
      hget: promisify(cacheClient().hget).bind(cacheClient()),
      hmset: promisify(cacheClient().hmset).bind(cacheClient()),
      expire: promisify(cacheClient().expire).bind(cacheClient()),
      del: promisify(cacheClient().del).bind(cacheClient()),
      hdel: promisify(cacheClient().hdel).bind(cacheClient()),
      set: promisify(cacheClient().set).bind(cacheClient()),
      get: promisify(cacheClient().get).bind(cacheClient()),
      zadd: promisify(cacheClient().zadd).bind(cacheClient()),
      zremrangebyscore: promisify(cacheClient().zremrangebyscore).bind(cacheClient()),
      zrevrangebyscore: promisify(cacheClient().zrevrangebyscore).bind(cacheClient()),
      zrem: promisify(cacheClient().zrem).bind(cacheClient()),
      ttl: promisify(cacheClient().ttl).bind(cacheClient()),
      incr: promisify(cacheClient().incr).bind(cacheClient()),
    };
  }
  return promisifiedRedis;
};
