import oauth2orize from 'oauth2orize';
import { RateLimit, APIError } from '@surfline/services-common';

export const handleCustomErrors = (err) => {
  const errObj = {};
  errObj.error = err.error;
  errObj.message = err.message;
  if (err.name === 'OAuth2Error') {
    // We want to catch the OAuth2Errors and let them bubble up their
    // status code and message
    return new APIError(err.message, err.code);
  }
  switch (err.message) {
    case 'User have exceeded the token limit':
      return new RateLimit('User has exceeded the token limit', 403);
    default:
      return err;
  }
};

export class RateLimitError extends Error {
  constructor(message) {
    super();
    this.code = 'rate_limit_error';
    this.status = 429;
    this.message = message || 'Too many requests hit the API within the allowable period of time.';
  }
}

/**
 * OAuth Errors
 *
 * Token errors are returned from invalid OAuth access or
 * refresh token exchanges, and invalid bearer authorization.
 * https://github.com/jaredhanson/oauth2orize/blob/master/lib/errors/tokenerror.js
 *
 * Auth errors are returned from invalid Client or User authorization.
 * https://github.com/jaredhanson/oauth2orize/blob/master/lib/errors/authorizationerror.js
 *
 * Server errors are returned in all instances where general errors may occur.
 * https://github.com/jaredhanson/oauth2orize/blob/master/lib/errors/oauth2error.js
 *
 * @param {String} message
 * @param {String} code
 *
 * @returns class Error {
 *  message: (String)
 *  code: (String)
 *  uri: (String) default to undefined
 *  status: (Http Status Code |Number) default to...
 *    401 (tokenError),
 *    400 (authError),
 *    500 (severError)
 * }
 */
export const tokenError = (message = 'Unauthorized client request', code = 'invalid_client') =>
  new oauth2orize.TokenError(message, code);
export const authError = (message = 'Invalid Request', code = 'invalid_request', statusCode) =>
  new oauth2orize.AuthorizationError(message, code, undefined, statusCode);
export const serverError = (message = 'Internal Server Error') =>
  new oauth2orize.OAuth2Error(message);
