import Analytics from 'analytics-node';

const slAnalytics = new Analytics(process.env.SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const bwAnalytics = new Analytics(process.env.BW_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const fsAnalytics = new Analytics(process.env.FS_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });

const getAnalyticsFor = (brand) => {
  let analytics;
  switch (brand) {
    case 'bw':
      analytics = bwAnalytics;
      break;
    case 'fs':
      analytics = fsAnalytics;
      break;
    default:
      analytics = slAnalytics;
  }
  return analytics;
};

export const track = (event, userId, properties, brand) => {
  const analytics = getAnalyticsFor(brand);
  analytics.track({
    event,
    userId,
    properties,
  });
};
