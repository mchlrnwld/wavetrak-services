import 'newrelic';
import ReactViews from 'express-react-views';
import { setupExpress } from '@surfline/services-common';
import { logger } from './common/logger';
import { auth } from './route/auth';
import { external } from './route/external';
import { trusted } from './route/trusted';
import * as site from './route/site';
import * as dbContext from './common/dbContext';

const log = logger('auth-service:server');

/**
 * @description This function is executed by `setupExpress` before the app
 * server is started. It is a callback which allows us to pass custom logic
 * to the app from outside of `setupExpress`
 */
const setupAppViews = (app) => {
  app.set('views', `${__dirname}/views`);
  app.set('view engine', 'js');
  app.engine('js', ReactViews.createEngine({ transformViews: false }));
};

const startApp = async () => {
  const express = setupExpress({
    handlers: [
      ['/auth', auth()],
      ['/trusted', trusted()],
      ['/', await external()],
      // TODO remove prior to launch
      ['/', site.index],
    ],
    name: 'auth-service',
    port: process.env.EXPRESS_PORT || 8080,
    log,
    callback: setupAppViews,
  });
  express.app.disable('x-powered-by');
  return express;
};

Promise.all([dbContext.initCache(), dbContext.initDB()])
  .then(startApp)
  .catch((err) => {
    log.error("App Initialization failed. Can't connect to database", err);
    process.exit(1);
  });
