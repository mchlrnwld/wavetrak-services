/* eslint-disable no-param-reassign,import/prefer-default-export */
import { Router } from 'express';
import oauth2orize from 'oauth2orize';
import { UnauthorizedError, APIError, wrapErrors, getPlatform } from '@surfline/services-common';
import passport from 'passport';
import { BasicStrategy } from 'passport-http';
import { Strategy as BearerStrategy } from 'passport-http-bearer';
import * as oauthModel from '../model/oauth';
import { invalidateToken } from '../model/user';
import * as logger from '../common/logger';

const log = logger.logger('auth-service:route:trusted');
const trustedPassport = new passport.Passport();

export const trusted = () => {
  const api = new Router();
  const oauthServer = oauth2orize.createServer();
  api.use(trustedPassport.initialize());

  /**
   * BasicStrategy
   *
   * This strategy is used to authenticate registered OAuth clients.  It is
   * employed to protect the `token` endpoint, which consumers use to obtain
   * access tokens.  The OAuth 2.0 specification suggests that clients use the
   * HTTP Basic scheme to authenticate.
   *
   * @param {String} clientId
   * @param {String} clientSecret
   *
   * @returns {Function}
   */
  trustedPassport.use(
    'basic',
    new BasicStrategy({ passReqToCallback: true }, (req, clientId, clientSecret, done) =>
      oauthModel.basicAuthorization(req, clientId, clientSecret, done),
    ),
  );

  /**
   *  This strategy is used to authenticate HTTP requests using bearer tokens.
   *
   *  The Bearer strategy requires a `verify` function which receives the
   *  credentials (`token`) contained in the request.  The function must invoke
   *  `cb` with a user object, which will be set at `req.user` in route handlers
   *  after authentication.
   *
   *  @param {String} accessToken
   *
   *  @returns {Function}
   */
  trustedPassport.use(
    'accesstoken',
    new BearerStrategy({ passReqToCallback: true }, (req, accessToken, done) => {
      const apiKey = req.get('x-api-key');
      // TODO: Remove this variable. We should store trusted attribute in
      // the token hashMap in Redis instead of hardcoding the value here.
      const isTrusted = true;
      return oauthModel.bearerAuthorization(accessToken, apiKey, isTrusted, done);
    }),
  );

  /**
   *  Exchange password for access and refresh tokens.  The callback authenticates the user,
   *  creates the access and refresh tokens, stores them in Redis and send them back to the client.
   *
   * @param {Object} client (returned from client authoriztion)
   * @param {String} username
   * @param {String} password
   * @param {String} scope
   *
   * @returns {Function}
   */
  oauthServer.exchange(
    oauth2orize.exchange.password(async (client, username, password, scope, reqBody, done) => {
      try {
        if (!!reqBody.facebookId && reqBody.facebookId !== 'undefined') {
          return oauthModel.exchangeFbTokenTrusted(client, reqBody, done);
        }
        return oauthModel.exchangePassword(client, username, password, scope, reqBody, done);
      } catch (error) {
        log.error({
          model: 'oauth2orize.exchange.password',
          message: 'Error when exchanging password for tokens',
          email: username,
          error: JSON.stringify(error),
          client,
        });
        return error;
      }
    }),
  );

  /**
   *  Exchange refresh token for access and refresh tokens.  The callback validates
   *  the refresh token in Redis, creates the access and refresh tokens,
   *  stores them in Redis and send them back to the client.
   *
   * @param {Object} client (returned from client authoriztion)
   * @param {String} refreshToken
   * @param {String} scope
   *
   * @returns {Function}
   */
  oauthServer.exchange(
    oauth2orize.exchange.refreshToken((client, refreshToken, scope, done) =>
      oauthModel.exchangeTrustedRefreshToken(client, refreshToken, scope, done),
    ),
  );

  /**
   *  Exchange client credentials for access token.  The callback creates the access
   *  token, stores them in Redis and send them back to the client.
   *
   * @param {Object} client (returned from client authorization)
   * @param {String} scope
   * @param {Object} reqBody
   *
   * @returns {Function}
   */
  oauthServer.exchange(
    oauth2orize.exchange.clientCredentials(
      { scopeSeparator: ',' },
      (client, scope, reqBody, done) =>
        oauthModel.exchangeClientCredentials(client, scope, reqBody, done),
    ),
  );

  /**
   *  modifyReqBody - Middleware function to add client_id and client_secret to request body.
   *  This is to ensure backward compatibilty for existing trused apps that do not send client_id
   * or client_secret
   */
  const modifyReqBody = (req, res, next) => {
    req.body.client_id = req.body.client_id || 'authclientId';
    req.body.client_secret = req.body.client_secret || 'authclient_secret';
    req.body.isShortLived = req.query && req.query.isShortLived;
    next();
  };
  api.use('*', modifyReqBody);

  /**
   *  Middleware to verify the authorization header in the trusted/token endpoint.
   *  If the header does not exist then verify if the authorizationString is present in the body
   *  and set the authorization header accordingly. This is done to fix sign issues for
   *  requests that do not set the authorization header.
   * @function
   * @param req {Object} The request.
   * @param _res {Object} The response.
   * @param next {Function}
   * @return {undefined}
   */

  const verifyAuthHeader = (req, _res, next) => {
    const authorizationString = req?.body?.authorizationString;
    const authorization = req?.headers?.authorization;
    if (!authorization && authorizationString) {
      req.headers.authorization = authorizationString;
    }
    next();
  };

  /**
   *  Token Endpoint - Authenticates the client and user and issues access and refresh tokens
   *
   *  The `authenticate` middleware is used for validating the client in the
   *  request body. If valid, the token middleware is called and
   *  passes the request parameter to the appropriate exchange callback depending on the grant_type
   *  parameter. Before passing on to the exhange callback the user is authenticated
   * or the refresh token is validated in the token middleware.
   *
   *  (refresh token):
   *  @param {String} grant_type = refresh_token
   *  @param {String} refresh_token
   *  @param {String} client_id
   *  @param {String} client_secret (optional)
   *
   *  (password flow)
   *  @param {String} grant_type = password
   *  @param {String} client_id
   *  @param {String} client_secret (optional)
   *  @param {String} username
   *  @param {String} password
   *
   *  (client_credentials flow)
   *  @param {String} grant_type = client_credentials
   *  @param {String} client_id
   *  @param {String} client_secret
   *  @param {String} scope (comma seperated user roles. This is assigned in OnleLogin )
   *  @param {String} id (OneLogin/AD unique PersonImmutableID)
   *
   *  @returns {JSON}
   *  {
   *   "access_token": {String}
   *   "refresh_token": {String}
   *   "expires_in": {Number},
   *   "token_type": "Bearer"
   *  }
   */
  api.post(
    '/token',
    verifyAuthHeader,
    trustedPassport.authenticate('basic', {
      session: false,
    }),
    oauthServer.token(),
    oauthServer.errorHandler(),
  );

  /**
   *  Authorise Enpoint
   *
   *  Similar to the token endpoint, the `authenticate` middleware is used for validating the
   *  authorization credentials in the request header.
   *
   *  For this case the access_token must first pass Bearer Token Authorization (BearerStrategy).
   *  If valid the api returns the protected resource.
   *
   *  @returns {JSON} {
   *    id: {String},
   *    scopes: {Array}
   * }
   */
  api.get(
    '/authorise',
    trustedPassport.authenticate('accesstoken', { session: false }),
    (req, res) => {
      const { email, name, title } = req.user;
      res.json({
        id: req.user.userId,
        scopes: req.user.scopes.split(','),
        email,
        name,
        title,
      });
    },
  );

  api.get('/validate', async (req, res) => {
    try {
      const {
        query: { apiKey },
      } = req;
      const scopes = await oauthModel.validateClient(apiKey);
      return res.send({ scopes });
    } catch (error) {
      log.error({
        message: 'Error when validating client',
        error: JSON.stringify(error),
      });
      const { status, message } = error;
      return res.status(status).send({ message });
    }
  });

  api.get(
    '/client-name',
    wrapErrors(async (req, res) => {
      const {
        query: { clientId },
      } = req;

      if (!clientId) throw new APIError('Client ID required.');

      try {
        const clientName = await oauthModel.getClientNameByClientId(clientId);

        return res.send({ clientName });
      } catch (error) {
        log.error({
          message: 'Error when fetching client',
          error: JSON.stringify(error),
        });
        const { statusCode, message } = error;
        return res.status(statusCode).send({ message });
      }
    }),
  );

  /**
   *  Token Invalidation Enpoint
   *
   *  This API's intended use is to remove the `bearer_token` and `refresh_token` keys from
   *  Redis that are asscoisated with the user id received in the request body.
   *
   * @param body.userId {string} - the Mongo UserInfo id associated with the token that will
   * be removed from Redis
   *
   *  @returns {JSON} {
   *    message: {String},
   * }
   */
  api.post('/invalidate-token', async (req, res) => {
    const {
      body: { userId },
    } = req;
    if (!userId) throw new APIError('Invalid request parameters');
    try {
      await invalidateToken(userId);
      return res.send({ message: 'Successfully invalidated tokens' });
    } catch (error) {
      log.error({
        message: 'Error when invalidating token',
        error: JSON.stringify(error),
      });
      const { status, message } = error;
      return res.status(status).send({ message });
    }
  });

  /**
   *  Logout Enpoint for Trusted clients
   *
   *  This API's intended use is to logout a user by removing the `bearer_token` and `refresh_token` keys from
   *  Redis and deleting the assosciated cookies from the browser.
   */
  api.get('/logout', async (req, res) => {
    const bearerToken = req?.cookies?.access_token;
    const refreshToken = req?.cookies?.refresh_token;
    try {
      if (!bearerToken || !refreshToken) {
        return res.status(400).send({
          Error: 'Missing Bearer and/or Refresh tokens in the request.',
        });
      }
      const platform = getPlatform(req.headers['user-agent']);
      await oauthModel.deleteUserTokens(bearerToken, refreshToken, platform);
      const logData = {
        bearerToken: bearerToken?.substring(6),
        refreshToken: refreshToken?.substring(6),
      };
      log.info(logData, 'logout - tokens and cookies cleared');
      return res.redirect('/');
    } catch (error) {
      log.error({
        message: 'Error logging out',
        bearerToken: bearerToken?.substring(6),
        refreshToken: refreshToken?.substring(6),
        error: JSON.stringify(error),
      });
      const { status, message } = error;
      return res.status(status).send({ message });
    }
  });

  /**
   * Error handler middleware for catching token errors from Oauthserver
   * and formatting them for the global error handler.
   */
  api.use((err, req, _res, next) => {
    if (err.name === 'TokenError') {
      const accessToken = req?.query?.access_token || req?.headers?.authorization;
      if (accessToken) {
        log.error({
          message: 'TokenError',
          accessToken: accessToken.substring(6),
          error: JSON.stringify(err),
        });
      }
      return next(new UnauthorizedError('Unauthorized client request', 401));
    }
    return next(err);
  });

  return api;
};
