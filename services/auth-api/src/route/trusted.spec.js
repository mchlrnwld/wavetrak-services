/* eslint-disable no-unused-expressions */
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { errorHandlerMiddleware } from '@surfline/services-common';
import * as logger from '../common/logger';
import ClientIdModel from '../model/ClientId';
import { trusted } from './trusted';
import * as user from '../model/user';
import * as dbContext from '../common/dbContext';
import UserInfoModel from '../model/UserInfo';
import * as oauthModel from '../model/oauth';

const { expect } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);

describe('trusted route', () => {
  let loggerStub;
  let redisPromiseStub;
  let ClientIdModelfindOneStub;
  let UserInfoModelfindOneStub;
  let checkUserCredentialsStub;
  let deleteUserTokensStub;

  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(trusted());
  app.use(errorHandlerMiddleware({ error: () => {}, info: () => {} }));

  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace() {},
      debug() {},
      info() {},
      error() {},
    });
  });
  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    ClientIdModelfindOneStub = sinon.stub(ClientIdModel, 'findOne');
    UserInfoModelfindOneStub = sinon.stub(UserInfoModel, 'findOne');
    checkUserCredentialsStub = sinon.stub(user, 'checkUserCredentials');
    deleteUserTokensStub = sinon.stub(oauthModel, 'deleteUserTokens').returns();
    redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
      hmset: () => {},
      expire: () => {},
      hgetall: () => {
        const res = {
          refreshToken: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          clientId: '5bff1583ffda1e1eb8d35c4b',
          userId: '59f164c72d787c00115ee1f0',
          scopes: 'all',
          deviceId: 'UNQ123',
          deviceType: 'Web_Browser',
        };
        return res;
      },
      del: () => {},
      get: () => {
        return 1;
      },
      set: () => {},
      incr: () => {},
      zrevrangebyscore: () => {},
      multi: () => {},
    });
    ClientIdModelfindOneStub.returns({
      _id: '5bff1583ffda1e1eb8d35c4b',
      clientId: 'WEB_VTM8QWOFNZ4X',
      name: 'Surfline Web',
      description: 'Surfline Web application',
      secret: 'authclient_secret',
      trusted: true,
      scopes: ['all'],
    });
  });

  afterEach(() => {
    ClientIdModelfindOneStub.restore();
    UserInfoModelfindOneStub.restore();
    checkUserCredentialsStub.restore();
    redisPromiseStub.restore();
    deleteUserTokensStub.restore();
  });

  describe('/token', () => {
    it('should return a valid token for grant_type ("password")', (done) => {
      checkUserCredentialsStub.returns({ _id: '5bd0d9cb4a17b4000e6a9cfa' });
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'ajith@surfline.com',
          password: '123456',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body.access_token).not.to.be.null;
          done();
        });
    });

    it('should return error for invalid username/password', (done) => {
      checkUserCredentialsStub.restore();
      UserInfoModelfindOneStub.returns(null);
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'invaliduser@test.com',
          password: 'invalid',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'invalid_request');
          expect(res.body).to.have.property('error_description', 'User not found');
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return error for missing username/password', (done) => {
      checkUserCredentialsStub.restore();
      UserInfoModelfindOneStub.returns(null);
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: null,
          password: null,
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'invalid_request');
          expect(res.body).to.have.property('error_description', 'Missing parameter: username');
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return error for invalid client', (done) => {
      ClientIdModelfindOneStub.returns(null);
      chai
        .request(app)
        .post('/token')
        .auth('invalid_client', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'invaliduser@test.com',
          password: 'invalid',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'invalid_request');
          expect(res.body).to.have.property('error_description', 'Invalid Request');
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return 401 status for missing client', (done) => {
      ClientIdModelfindOneStub.returns(null);
      chai
        .request(app)
        .post('/token')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'invaliduser@test.com',
          password: 'invalid',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it('should return error for invalid client password', (done) => {
      ClientIdModelfindOneStub.returns({
        _id: '5bff1583ffda1e1eb8d35c4b',
        clientId: 'WEB_VTM8QWOFNZ4X',
        name: 'Surfline Web',
        description: 'Surfline Web application',
        secret: 'authclient_secret',
        trusted: false,
        scopes: ['all'],
      });
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'invalid_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'invaliduser@test.com',
          password: 'invalid',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'unauthorized_client');
          expect(res.body).to.have.property('error_description', 'Unauthorized Client');
          expect(res).to.have.status(403);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return a valid token for grant_type ("refresh_token")', (done) => {
      checkUserCredentialsStub.returns({ _id: '5bd0d9cb4a17b4000e6a9cfa' });
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          refresh_token: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          password: '123456',
          grant_type: 'refresh_token',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body.access_token).not.to.be.null;
          done();
        });
    });

    it('should return error for an invalid refresh_token - grant_type ("refresh_token")', (done) => {
      // Restore the redisPromise stub first because we need to change the default implementation of hgetall
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        hgetall: () => {
          return null;
        },
        get: () => {
          return null;
        },
      });
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          refresh_token: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          grant_type: 'refresh_token',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'invalid_client');
          expect(res.body).to.have.property('error_description', 'Unauthorized client request');
          expect(res).to.have.status(401);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return error for missing refresh_token - grant_type ("refresh_token")', (done) => {
      // Restore the redisPromise stub first because we need to change the default implementation of hgetall
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        hgetall: () => {
          return null;
        },
      });
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          refresh_token: null,
          grant_type: 'refresh_token',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'invalid_request');
          expect(res.body).to.have.property(
            'error_description',
            'Missing required parameter: refresh_token',
          );
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return error for invalid client - grant_type ("refresh_token")', (done) => {
      ClientIdModelfindOneStub.returns(null);
      chai
        .request(app)
        .post('/token')
        .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          refresh_token: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          grant_type: 'refresh_token',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('error', 'invalid_request');
          expect(res.body).to.have.property('error_description', 'Invalid Request');
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });

    it('should return 401 error if there is no authorization header of authorization string present in the body', (done) => {
      chai
        .request(app)
        .post('/token')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'invaliduser@test.com',
          password: 'password',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          expect(res.text).to.be.equal('Unauthorized');
          done();
        });
    });

    it('should return an error if there is no authorization header and an invalid authorization string present in the body', (done) => {
      ClientIdModelfindOneStub.returns(null);
      chai
        .request(app)
        .post('/token')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'invaliduser@test.com',
          password: 'password',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
          authorizationString: 'Basic Invalid string',
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it('should return a valid token if there is no authorization header but a valid authorization string present in the body', (done) => {
      checkUserCredentialsStub.returns({ _id: '5bff1583ffda1e1eb8d35c4b' });
      ClientIdModelfindOneStub.returns({
        _id: '5bff1583ffda1e1eb8d35c4b',
        clientId: 'WEB_VTM8QWOFNZ4X',
        name: 'Surfline Web',
        description: 'Surfline Web application',
        secret: 'sk_Hmxh7RJEJgJnZeaNF3Kx',
        trusted: true,
        scopes: ['all'],
      });
      chai
        .request(app)
        .post('/token')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
          username: 'user@test.com',
          password: 'password',
          grant_type: 'password',
          device_id: 'UNQ123',
          device_type: 'Web_Browser',
          authorizationString:
            'Basic NWJmZjE1ODNmZmRhMWUxZWI4ZDM1YzRiOnNrX0hteGg3UkpFSmdKblplYU5GM0t4',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body.access_token).not.to.be.null;
          done();
        });
    });
  });

  describe('/authorise', () => {
    it('should return a userid for a valid accessToken', (done) => {
      chai
        .request(app)
        .get('/authorise')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', 'Bearer db023ef85b2a5a5347f038cc0f0d3c4c3361a136')
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body.id).not.to.be.null;
          expect(res.body.scopes).not.to.be.null;
          done();
        });
    });

    it('should return error for an invalid accessToken', (done) => {
      // Restore the redisPromise stub first because we need to change the default implementation of hgetall
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        hgetall: () => {
          return null;
        },
        get: () => {
          return null;
        },
      });
      chai
        .request(app)
        .get('/authorise')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', 'Bearer db023ef85b2a5a5347f038cc0f0d3c4c3361a136')
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res.body).to.have.property('message', 'Unauthorized client request');
          expect(res).to.have.status(401);
          expect(res).to.be.json;
          done();
        });
    });
  });

  describe('/invalidate-token', () => {
    it('should invalidate all token for a user', (done) => {
      redisPromiseStub.restore();
      redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
        zrevrangebyscore: () => {
          return 1;
        },
        multi: () => {
          return 1;
        },
        del: () => {
          return 1;
        },
      });
      UserInfoModelfindOneStub.returns({ email: 'user@test.com' });
      chai
        .request(app)
        .post('/invalidate-token')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', 'Bearer db023ef85b2a5a5347f038cc0f0d3c4c3361a136')
        .send({
          userId: '123test',
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body.message).to.equal('Successfully invalidated tokens');
          done();
        });
    });
  });

  describe('/logout', () => {
    it('should logout a user if the access and refresh token cookies are valid', (done) => {
      deleteUserTokensStub.returns(true);
      chai
        .request(app)
        .get('/logout')
        .set('Cookie', 'access_token=access_token;refresh_token=refresh_token')
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.redirect;
          expect(res).to.not.have.cookie('access_token');
          expect(res).to.not.have.cookie('refresh_token');
          expect(res).to.not.have.cookie('USERINFO');
          expect(res).to.not.have.cookie('USER_ID');
          done();
        });
    });
    it('should throw a validation error if access and refresh tokens are missing from the cookies', (done) => {
      chai
        .request(app)
        .get('/logout')
        .end((err, res) => {
          expect(res.text).to.be.equal(
            '{"Error":"Missing Bearer and/or Refresh tokens in the request."}',
          );
          expect(res.status).to.be.equal(400);
          done();
        });
    });
  });
});
