/* eslint-disable no-param-reassign,import/prefer-default-export */
import oauthserver from 'oauth2-server';
import { getPlatform } from '@surfline/services-common';
import { Router } from 'express';
import cookieParser from 'cookie-parser';
import * as logger from '../common/logger';
import model from '../model/auth';
import * as analytics from '../common/analytics';
import grantFacebookToken from '../integrations/facebook/grantFacebookToken';
import authoriseFacebookToken from '../integrations/facebook/authoriseFacebookToken';
import * as customerrors from '../common/customErrors';
import deleteFacebookToken from '../integrations/facebook/deleteFacebookToken';

export function auth() {
  const log = logger.logger('auth-service:route:auth');
  const router = Router();

  const oauth = oauthserver({
    model,
    grants: ['password', 'refresh_token'],
    debug: true,
    accessTokenLifetime: process.env.accessTokenLifetime || 2592000, // 30 days,
    refreshTokenLifetime: process.env.refreshTokenLifetime || 31536000, // 365 days
    clientIdRegex: /^[a-z0-9-_]{3,40}$/i,
  });

  const invalidateHandler = (req, res) => {
    if (!(req.body && req.body.id)) {
      res.status(400).send({ Error: 'Bad request.Id missing in request body' });
    } else {
      model
        .invalidateToken(req.body.id)
        .then((results) => {
          if (results) {
            const platform = getPlatform(req.headers['user-agent']);
            analytics.track(
              'Signed Out',
              req.body.id,
              { method: 'user signed out', platform },
              req.body.brand,
            );
            res.json({ results: 'Successfully invalidated token' });
          }
        })
        .catch((error) => {
          res.status(500).send({ Error: `Error invalidating token:${error.message}` });
        });
    }
  };

  const deleteRefreshTokenHandler = (req, res) => {
    if (!req.params && !req.params.refreshToken) {
      res.status(400).send({ Error: 'Bad request. Refresh token missing in request params' });
    } else {
      model
        .deleteRefreshToken(req.params.refreshToken)
        .then((results) => {
          if (results) {
            res.json({ results: 'Successfully deleted refresh token.' });
          } else {
            res.status(400).json({ results: 'Bad request. No token with that value found.' });
          }
        })
        .catch((error) => {
          res.status(500).send({ Error: `Error deleting refresh token:${error.message}` });
        });
    }
  };

  const deleteAccessTokenHandler = (req, res) => {
    if (!req.params && !req.params.accessToken) {
      res.status(400).send({ Error: 'Bad request. Access token missing in request params.' });
    } else {
      model
        .deleteAccessToken(req.params.accessToken)
        .then((results) => {
          if (results) {
            res.json({ results: 'Successfully deleted access token.' });
          } else {
            res.status(400).json({ results: 'Bad request. No token with that value found.' });
          }
        })
        .catch((error) => {
          res.status(500).send({ Error: `Error deleting access token:${error.message}` });
        });
    }
  };

  const logoutHandler = async (req, res) => {
    const accessToken = req.cookies.access_token;
    const refreshToken = req.cookies.refresh_token;
    const getDomain = () =>
      ({
        development: 'localhost',
        production: 'www.surfline.com',
        staging: 'staging.surfline.com',
        sandbox: 'sandbox.surfline.com',
      }[process.env.NODE_ENV || 'development']);

    if (!accessToken || !refreshToken) {
      return res.status(400).send({
        Error: 'Missing Access and/or Refresh tokens in the request.',
      });
    }

    try {
      await Promise.all([
        model.deleteAccessToken(accessToken),
        model.deleteRefreshToken(refreshToken),
      ]);
      await deleteFacebookToken(accessToken);
      res.clearCookie('access_token', { path: '/', domain: `.${getDomain()}` });
      res.clearCookie('refresh_token', { path: '/', domain: `.${getDomain()}` });
      res.clearCookie('USERINFO', { path: '/', domain: `.${getDomain()}` });
      res.clearCookie('USER_ID', { path: '/', domain: `.${getDomain()}` });
      return res.redirect('/');
    } catch (error) {
      log.error({
        message: 'Error logging out',
        accessToken,
        refreshToken,
        error: JSON.stringify(error),
      });
      return res.status(500).send({ Error: `Error deleting tokens: ${error.message}` });
    }
  };

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/auth',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  // Adding dummy client_id and secret to bypass the default oAuth implementation
  const addAuthHeader = (req, _, next) => {
    req.body.client_id = req.body.client_id || 'authclientId';
    req.body.client_secret = 'authclient_secret';
    req.body.forced = req.body.forced || 'false';
    next();
  };

  router.use('*', addAuthHeader);
  router.use('*', trackRequest);
  router.use('/logout', cookieParser());

  // Handle token grant requests
  const routeTokenRequest = (req, res, next) => {
    if (!!req.body.facebookId && req.body.facebookId !== 'undefined') {
      grantFacebookToken(req, res, next);
    } else {
      oauth.grant()(req, res, next);
    }
  };

  const routeAuthoriseRequest = (req, res, next) => {
    if (req.query && req.query.access_token && req.query.access_token.indexOf('fb-token') > -1) {
      authoriseFacebookToken(req, res, next);
    } else {
      oauth.authorise()(req, res, next);
    }
  };

  router.route('/token').post(routeTokenRequest, (req, res) => {
    res.json(req.body.token_response);
  });

  router.route('/authorise').get(routeAuthoriseRequest, (req, res) => {
    res.json(req.user);
  });

  router.route('/invalidate-token').post((req, res) => invalidateHandler(req, res));

  router
    .route('/refreshToken/:refreshToken')
    .delete((req, res) => deleteRefreshTokenHandler(req, res));
  router
    .route('/accessToken/:accessToken')
    .delete((req, res) => deleteAccessTokenHandler(req, res));

  router.route('/logout').get(logoutHandler);

  // This handler is deprecated. We should rely on the `errorHandlerMiddleware`
  // from `@surfline/services-common` in order to centralize our error logic.
  // This handler is meant to catch a specific error and set a status code.
  router.use((err, _, _res, next) => {
    const errObj = customerrors.handleCustomErrors(err);
    // Pass the error to the top level error handler
    next(errObj);
  });
  return router;
}
