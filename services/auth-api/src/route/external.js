import { wrapErrors, authenticateRequest, APIError } from '@surfline/services-common';
import cookieParser from 'cookie-parser';
import { Router } from 'express';
import oauth2orize from 'oauth2orize';
import passport from 'passport';
import session from 'express-session';
import { Strategy as BearerStrategy } from 'passport-http-bearer';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as CustomStrategy } from 'passport-custom';
import { BasicStrategy } from 'passport-http';
import * as oauthModel from '../model/oauth';
import TransactionStore from '../model/oauthTransactionStore';

const RedisStore = require('connect-redis')(session);

export const external = async () => {
  const api = new Router();

  await oauthModel.setupOAuth1Strategies();

  const oauthServer = oauth2orize.createServer({
    store: new TransactionStore(),
  });

  // hack so that we can support both `scope` and `scopes` (plural) query params
  // choose `scope` when possible. AWS Alexa only supports `scopes`.
  api.use((req, res, next) => {
    if (req.query.scopes) {
      // eslint-disable-next-line no-param-reassign
      req.query = {
        ...req.query,
        scope: req.query.scopes,
      };
    }

    return next();
  });

  api.use(
    session({
      store: new RedisStore({
        url: `redis://${process.env.REDIS_IP}:${process.env.REDIS_PORT}`,
      }),
      saveUninitialized: true,
      secret: 'DrnGTL9Lmm3tvGztsKxW',
      resave: false,
    }),
  );

  api.use(passport.initialize());
  /**
   * CreateAccountStrategy
   *
   * This strategy is used to authenticate recently created users.
   * Anytime a request is made to authorize an application, we must ensure that
   * a user has been created before asking them to approve the request.
   *
   * @param {String} firstName
   * @param {String} lastName
   * @param {String} email
   * @param {String} password
   *
   * @returns {Function}
   */
  passport.use('create-account', new CustomStrategy(oauthModel.createAccountAuthorization));

  /**
   * LocalStrategy
   *
   * This strategy is used to authenticate users based on a username and password.
   * Anytime a request is made to authorize an application, we must ensure that
   * a user is logged in before asking them to approve the request.
   *
   * @param {String} email
   * @param {String} password
   *
   * @returns {Function}
   */
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      (email, password, done) => oauthModel.localAuthorization(email, password, done),
    ),
  );

  passport.serializeUser((user, done) => done(null, user._id));
  passport.deserializeUser((id, done) => oauthModel.deserializeUser(id, done));

  /**
   * BasicStrategy
   *
   * This strategy is used to authenticate registered OAuth clients.  It is
   * employed to protect the `token` endpoint, which consumers use to obtain
   * access tokens.  The OAuth 2.0 specification suggests that clients use the
   * HTTP Basic scheme to authenticate.
   *
   * @param {String} clientId
   * @param {String} clientSecret
   *
   * @returns {Function}
   */
  passport.use(
    new BasicStrategy({ passReqToCallback: true }, (req, clientId, clientSecret, done) =>
      oauthModel.basicAuthorization(req, clientId, clientSecret, done),
    ),
  );

  /**
   * FacebookAuth
   *
   * TODO better docs
   */
  passport.use('facebook', new CustomStrategy(oauthModel.facebookAuthorization));

  /**
   *  Configure the Bearer strategy for use by Passport.
   *
   *  The Bearer strategy requires a `verify` function which receives the
   *  credentials (`token`) contained in the request.  The function must invoke
   *  `cb` with a user object, which will be set at `req.user` in route handlers
   *  after authentication.
   *
   *  @param {String} accessToken
   *
   *  @returns {Function}
   */
  passport.use(
    new BearerStrategy({ passReqToCallback: true }, (req, accessToken, done) => {
      const apiKey = req.get('x-api-key');
      // TODO: Remove this variable. We should store trusted attribute in the
      // token hashMap in Redis instead of hardcoding the value here.
      const isTrusted = false;
      oauthModel.bearerAuthorization(accessToken, apiKey, isTrusted, done);
    }),
  );

  /**
   *  Register serialialization and deserialization functions.
   *
   *  When a client redirects a user to user authorization endpoint, an
   *  authorization transaction is initiated.  To complete the transaction, the
   *  user must authenticate and approve the authorization request.  Because this
   *  may involve multiple HTTP request/response exchanges, the transaction is
   *  stored in the session.
   *
   *  An application must supply serialization functions, which determine how the
   *  client object is serialized into the session.  Typically this will be a
   *  simple matter of serializing the client's ID, and deserializing by finding
   *  the client by ID from the database.
   */
  oauthServer.serializeClient((client, done) => done(null, client));
  oauthServer.deserializeClient((deserializedClient, done) =>
    oauthModel.deserializeClient(deserializedClient, done),
  );

  /**
   *  Register supported grant types.
   *
   *  OAuth 2.0 specifies a framework that allows users to grant client
   *  applications limited access to their protected resources.  It does this
   *  through a process of the user granting access, and the client exchanging
   *  the grant for an access token.
   *
   *  Grant authorization codes.  The callback takes the `client` requesting
   *  authorization, the `redirectURI` (which is used as a verifier in the
   *  subsequent exchange), the authenticated `user` granting access, and
   *  their response, which contains approved scope, duration, etc. as parsed by
   *  the application.  The application issues a code, which is bound to these
   *  values, and will be exchanged for an access token.
   */

  /**
   * Grant Authorization Code
   *
   * @param {Object} client (deserialized client)
   * @param {String} redirectURI
   * @param {String} userId
   * @param {Object} ares (the authorization decision)
   *
   * @returns {Function}
   */
  oauthServer.grant(
    oauth2orize.grant.code((client, redirectURI, userId, ares, done) =>
      oauthModel.grantAuthCode(client, redirectURI, userId, done),
    ),
  );

  /**
   *  Exchange authorization codes for access tokens.  The callback accepts the
   *  client`, which is exchanging `code` and any `redirectURI` from the
   *  authorization request for verification.  If these values are validated, the
   *  application issues an access token on behalf of the user who authorized the
   *  code.
   *
   * @param {Object} client (returned from basic authoriztion)
   * @param {String} code
   * @param {String} redirectURI
   *
   * @returns {Function}
   */
  oauthServer.exchange(
    oauth2orize.exchange.code((client, code, redirectURI, done) =>
      oauthModel.exchangeAuthCode(client, code, redirectURI, done),
    ),
  );

  /**
   *  Exchange refresh token for access token.  The callback accepts the
   *  client, which is exchanging `refresh_token` and client id and secret from the
   *  authorization request for verification.  If these values are validated, the
   *  application issues an access token and new refresh token on behalf of the
   *  user who authorized the code.
   *
   * @param {Object} client (returned from basic authoriztion)
   * @param {String} refreshToken
   * @param {String} scope (the joined scopes array)
   *
   * @returns {Function}
   */
  oauthServer.exchange(
    oauth2orize.exchange.refreshToken((client, refreshToken, scope, done) =>
      oauthModel.exchangeRefreshToken(client, refreshToken, scope, done),
    ),
  );

  /**
   *  Revocation of the bearer token or refresh token.  The function
   *  accepts the token that the client wants to have revoked, along
   *  with an optional value describing the type of token sent.  If
   *  these values are validated, the application invalidates the
   *  token.
   *
   * https://tools.ietf.org/html/rfc7009 Token Revocation Standard
   *
   * @param {Object} client (returned from basic authoriztion)
   * @param {String} token
   * @param {String} token_type_hint ("bearer_token" || "refresh_token")
   *
   * @returns {res}
   */
  async function revoke(req, res) {
    try {
      const { token, token_type_hint: tokenType } = req.body;
      await oauthModel.revokeToken(token, tokenType);
      res.send({ message: 'Successfully processed request.' });
    } catch (error) {
      res.status(error.status).json({ message: error.message });
    }
  }

  api.get('/login', async (req, res) => {
    const clientId = req.query.client;
    try {
      const { name, appImageURL, _id } = await oauthModel.getClientName(clientId);
      return res.render('login.js', { name, appImageURL, clientId: _id });
    } catch (error) {
      return res.status(400).send({ message: 'invalid client request' });
    }
  });

  api.get('/create-account', async (req, res) => {
    const clientId = req.query.client;
    try {
      const { name, appImageURL, _id } = await oauthModel.getClientName(clientId);
      return res.render('create.js', { name, appImageURL, clientId: _id });
    } catch (error) {
      return res.status(400).send({ message: 'invalid client request' });
    }
  });

  api.use(passport.session());
  /**
   *  User authorization endpoints
   *
   *  `authorization` middleware accepts a `validate` callback which is
   *  responsible for validating the client making the authorization request.  In
   *  doing so, is recommended that the `redirectURI` be checked against a
   *  registered value, although security requirements may vary accross
   *  implementations.  Once validated, the `done` callback must be invoked with
   *  a `client` instance, as well as the `redirectURI` to which the user will be
   *  redirected after an authorization decision is obtained.
   *
   *  This middleware simply initializes a new authorization transaction.  It is
   *  the application's responsibility to authenticate the user and render a dialog
   *  to obtain their approval (displaying details about the client requesting
   *  authorization).  We accomplish that here by routing through `ensureLoggedIn()`
   *  first, and rendering the `dialog` view.
   *
   *  Example Request URL
   *  - /connect/authorize?redirect_uri=http://localhost:8002&response_type=code&client_id=5af1ce73b5acf7c6dd2592ee
   */
  api.get(
    '/connect/authorize',
    oauthModel.ensureLoggedIn('/login'),
    oauthServer.authorization((clientId, redirectURI, scope, done) =>
      oauthModel.authorizeClient(clientId, redirectURI, scope, done),
    ),
    (req, res) => {
      res.render('dialog.js', {
        transactionID: req.oauth2.transactionID,
        user: req.user,
        client: req.oauth2.client,
      });
    },
  );

  api.post(
    '/connect/authorize/decision',
    oauthModel.ensureLoggedIn('/login'),
    oauthServer.decision(),
  );

  /**
   *  Token Endpoint
   *
   *  The `authenticate` middleware is used for validating the authorization credentials
   *  in the request header.
   *
   *  For this case, the authorization_code and refresh_token exchanges must first pass
   *  Basic Authorization (BasicStrategy).  If valid, the token middleware is called and
   *  passes the request parameter to the appropriate exchange callback depending on the grant_type
   *  parameter
   *
   *  (refresh token):
   *  @param {String} grant_type
   *  @param {String} refresh_token
   *
   *  (authorization code)
   *  @param {String} grant_type
   *  @param {String} code
   *  @param {String} redirect_uri
   *
   *  @returns {JSON}
   *  {
   *   "access_token": {String}
   *   "refresh_token": {String}
   *   "expires_in": {Number},
   *   "token_type": "Bearer"
   *  }
   */
  api.post(
    '/oauth/token',
    passport.authenticate('basic', {
      session: false,
    }),
    oauthServer.token(),
    oauthServer.errorHandler(),
  );

  /**
   *  Authorize Enpoint
   *
   *  Similar to the token endpoint, the `authenticate` middleware is used for validating the
   *  authorization credentials in the request header.
   *
   *  For this case the access_token must first pass Bearer Token Authorization (BearerStrategy).
   *  If valid the api returns the protected resource.
   *
   *  @returns {JSON} {
   *    userId: {String},
   *    scopes: {String}
   * }
   */
  api.get('/oauth/authorize', passport.authenticate('bearer', { session: false }), (req, res) => {
    res.json({
      userId: req.user.userId.toString(),
      scopes: req.user.scopes,
    });
  });

  /**
   *  Revocation Enpoint
   *
   *  Similar to the token endpoint, the `authenticate` middleware is used for validating the
   *  authorization credentials in the request header.
   *
   *  For this case the access_token must first pass Basic Authorization.
   *  If valid the request is passed through to the revoke middlewar function.
   *  Invalid requests will return an Authorization Error (400) or Internal Server Error (400)
   *
   *  @returns {JSON} {
   *   message: {String}
   * }
   */
  api.post(
    '/oauth/revoke',
    passport.authenticate('basic', {
      session: false,
    }),
    revoke,
  );

  api.post('/login', (req, res, next) => {
    const strategy = req.body.auth_strategy || 'local';

    passport.authenticate(strategy, async (err, user) => {
      if (err || !user) {
        const { clientId } = req.body;
        const { name, appImageURL, _id } = await oauthModel.getClientName(clientId);
        return res.render('login.js', {
          name,
          appImageURL,
          clientId: _id,
          error: err.message,
        });
      }
      return req.logIn(user, (error) => {
        if (error) return next(error);
        if (req.session && req.session.returnTo) {
          return res.redirect(req.session.returnTo);
        }
        return res.redirect('/');
      });
    })(req, res, next);
  });

  api.post('/create-account', (req, res, next) => {
    passport.authenticate('create-account', async (err, user) => {
      if (err || !user) {
        const { clientId } = req.body;
        const { name, appImageURL, _id } = await oauthModel.getClientName(clientId);
        return res.render('create.js', {
          name,
          appImageURL,
          clientId: _id,
          error: err.message,
        });
      }
      return req.logIn(user, (error) => {
        if (error) return next(error);
        if (req.session && req.session.returnTo) {
          return res.redirect(req.session.returnTo);
        }
        return res.redirect('/');
      });
    })(req, res, next);
  });

  api.get(
    '/oauth1/connect',
    cookieParser(),
    authenticateRequest(),
    wrapErrors(async (req, res, next) => {
      const { clientId } = req.query;

      if (!clientId) {
        throw new APIError('Client ID is required.');
      }

      const userId = req.authenticatedUserId;

      if (!userId) {
        throw new APIError('User ID is required.');
      }

      const authenticator = await oauthModel.getPassportAuthenticator(clientId);

      // we need to set a cookie on the platform proxy (or auth-service for testing)
      // in order to track the user that is connecting with the provider.
      // The cookie expires in 30 minutes which is a generous amount of time to complete
      // the flow.
      res.cookie('x-auth-userid', userId, {
        maxAge: 1800000,
      });

      return authenticator(req, res, next);
    }),
  );

  api.get(
    '/oauth1/callback',
    cookieParser(),
    wrapErrors(async (req, res, next) => {
      const { clientId, oauth_token: oauthToken, oauth_verifier: oauthVerifier } = req.query;

      if (!clientId) {
        throw new APIError('Client ID is required.');
      }

      if (!req.cookies['x-auth-userid']) {
        throw new APIError('User ID Cookie is required.');
      }

      if (!oauthToken) {
        throw new APIError('OAuth Token is required.');
      }

      // If the user denied the connection request on the partners side
      // they will send back `oauth_verifier=null` as a query parameter
      // We can catch this and redirect them to the failed page for a
      // smoother experience
      if (!oauthVerifier || oauthVerifier === 'null') {
        const { failureRedirectUrl } = oauthModel.getOauth1RedirectUrls(clientId);
        return res.redirect(failureRedirectUrl);
      }

      const authenticator = await oauthModel.getPassportAuthenticator(clientId);

      // Clear the cookie once they've reached the callback stage since
      // it will exist on the request object and we no longer need it
      // after this request is processed
      res.clearCookie('x-auth-userid');

      return authenticator(req, res, next);
    }),
  );

  api.get('/oauth1/failed', wrapErrors(oauthModel.getOAuth1RedirectHandler('OAuthFailed.js')));
  api.get('/oauth1/success', wrapErrors(oauthModel.getOAuth1RedirectHandler('OAuthSuccess.js')));

  api.get('/oauth1/providers', wrapErrors(oauthModel.getOAuth1Providers));
  api.get('/oauth1/validate', wrapErrors(oauthModel.validateOAuth1Provider));

  /**
   *  @description
   *  Validate Enpoint for External OAuth Clients
   *  Performs a basic authorization check for the ClientId/Client Secret combination by looking up in the ClientId collection
   *
   *  @returns {String[]} scopes
   */
  api.get(
    '/oauth/validate',
    passport.authenticate('basic', {
      session: false,
    }),
    (req, res) => {
      // Client details are in req.user
      const {
        user: { scopes },
      } = req;
      res.send({ scopes });
    },
  );

  return api;
};
