/* eslint-disable no-unused-expressions */
import express from 'express';
import bodyParser from 'body-parser';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import * as logger from '../common/logger';
import * as dbContext from '../common/dbContext';
import model from '../model/auth';
import { auth } from './auth';

const { expect } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);

describe('auth route', () => {
  let loggerStub;
  let modelGetUserStub;
  let modelGetRefreshTokenStub;
  let modelGetAccessTokenStub;
  let modelInvalidateTokenStub;
  let modelGenerateTokenStub;
  let modelDeleteAccessToken;
  let modelDeleteRefreshToken;
  let redisPromiseStub;
  const app = express();
  const user = { id: 12345678 };
  const now = new Date();
  now.setMinutes(now.getMinutes() + 10);

  const accessToken = {
    accessToken: 'cecc7e984caee3ab84604f15b1f703f218e9c2e1',
    clientId: 'authclientId',
    expires: now,
    userId: 12345678,
  };

  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace() {},
      debug() {},
      info() {},
      error() {},
    });
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use('/auth', auth());
  });

  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    modelGetUserStub = sinon.stub(model, 'getUser');
    modelGenerateTokenStub = sinon.stub(model, 'generateToken');
    modelGetRefreshTokenStub = sinon.stub(model, 'getRefreshToken');
    modelGetAccessTokenStub = sinon.stub(model, 'getAccessToken');
    modelInvalidateTokenStub = sinon.stub(model, 'invalidateToken');
    modelDeleteAccessToken = sinon.stub(model, 'deleteAccessToken');
    modelDeleteRefreshToken = sinon.stub(model, 'deleteRefreshToken');
    redisPromiseStub = sinon.stub(dbContext, 'redisPromise').returns({
      hmset: () => {},
      expire: () => {},
      zadd: () => {},
      zremrangebyscore: () => {},
      ttl: () => {
        return 1000;
      },
      hget: () => {
        return '59f164c72d787c00115ee1f0';
      },
      del: () => {},
      zrem: () => {
        return 1;
      },
      hgetall: () => {
        const res = {
          bearerToken: 'c190d2832947ddc2bc168d1b64680ac13dbcfb7e',
          clientId: 'WEB_VTM8QWOFNZ4X',
          userId: '59f164c72d787c00115ee1f0',
          scopes: 'all',
          deviceId: 'UNQ123',
          deviceType: 'Web_Browser',
        };
        return res;
      },
    });
  });

  afterEach(() => {
    modelGetUserStub.restore();
    modelGenerateTokenStub.restore();
    modelGetRefreshTokenStub.restore();
    modelGetAccessTokenStub.restore();
    modelInvalidateTokenStub.restore();
    modelDeleteAccessToken.restore();
    modelDeleteRefreshToken.restore();
    redisPromiseStub.restore();
  });

  it('should initialize as an object', () => {
    expect(auth).to.exist;
  });

  it('should return a valid token for grant_type ("password")', (done) => {
    chai
      .request(app)
      .post('/auth/token')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ username: 'email@email.com', password: 'passwordhash', grant_type: 'password' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body.access_token).not.to.be.null;
        done();
      });
    modelGetUserStub.callsArgWith(2, null, user);
    modelGenerateTokenStub
      .onCall(0)
      .callsArgWith(2, null, { accessToken: accessToken.accessToken });
    modelGenerateTokenStub
      .onCall(1)
      .callsArgWith(2, null, { refreshToken: accessToken.accessToken });
  });

  it('should return a valid token for grant_type ("refresh_token") ', (done) => {
    chai
      .request(app)
      .post('/auth/token')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ refresh_token: 'tGzv3JOkF0XG5Qx2TlKWIA', grant_type: 'refresh_token' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body.access_token).not.to.be.null;
        done();
      });
    modelGetRefreshTokenStub.callsArgWith(1, null, accessToken);
    modelGetUserStub.callsArgWith(2, null, user);
    modelGenerateTokenStub
      .onCall(0)
      .callsArgWith(2, null, { accessToken: accessToken.accessToken });
    modelGenerateTokenStub
      .onCall(1)
      .callsArgWith(2, null, { refreshToken: accessToken.accessToken });
  });

  it('should authorise the request and return a userid', (done) => {
    chai
      .request(app)
      .get('/auth/authorise')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .query({ access_token: 'tGzv3JOkF0XG5Qx2TlKWIA' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body.id).to.equal(12345678);
        done();
      });
    modelGetAccessTokenStub.callsArgWith(1, null, accessToken);
  });

  it('should invalidate the token', (done) => {
    modelInvalidateTokenStub.resolves('1,1');

    chai
      .request(app)
      .post('/auth/invalidate-token')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ id: accessToken.userId })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
  });

  it('should return error with empty id', (done) => {
    modelInvalidateTokenStub.resolves('1,1');

    chai
      .request(app)
      .post('/auth/invalidate-token')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({})
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(400);
        expect(res).to.be.json;
        expect(res.body).to.not.be.null;
        expect(res.body.Error).to.be.equal('Bad request.Id missing in request body');
        done();
      });
  });

  it('should return error with model error', (done) => {
    modelInvalidateTokenStub.rejects(new Error('server error'));

    chai
      .request(app)
      .post('/auth/invalidate-token')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ id: accessToken.userId })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        expect(res).to.be.json;
        done();
      });
  });

  it('should delete tokens and return a 302 redirect', (done) => {
    modelDeleteAccessToken.resolves(1, 0);
    modelDeleteRefreshToken.resolves(1, 0);

    chai
      .request(app)
      .get('/auth/logout')
      .set('Cookie', 'access_token=123;refresh_token=321')
      .end((err, res) => {
        expect(res).to.redirect;
        done();
      });
  });

  it('should return a status of 400 when tokens are not passed', (done) => {
    chai
      .request(app)
      .get('/auth/logout')
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return a status of 500 with model error', (done) => {
    modelDeleteAccessToken.rejects(new Error('server error'));

    chai
      .request(app)
      .get('/auth/logout')
      .set('Cookie', 'access_token=123;refresh_token=321')
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        expect(res).to.be.json;
        done();
      });
  });
});
