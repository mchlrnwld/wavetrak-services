/* eslint-disable no-unused-expressions */
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import passport from 'passport';
import { errorHandlerMiddleware } from '@surfline/services-common';
import ClientIdModel from '../model/ClientId';
import * as oAuthModel from '../model/oauth';
import AuthProviders from '../model/AuthProviders';
import { external } from './external';

const { expect } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);

describe('/external', () => {
  let ClientIdModelfindOneStub;
  const clientId = '123';
  const userId = '1234';
  let setupOAuth1StrategiesStub;
  let getOAuth1RedirectHandlerStub;

  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(bodyParser.json());

  before((done) => {
    try {
      external().then((res) => {
        app.use(res);
        app.use(errorHandlerMiddleware({ error: () => {}, info: () => {} }));
        done();
      });
    } catch (err) {
      done();
    }
    done();
  });

  beforeEach(() => {
    sinon.stub(passport, 'authenticate');
    sinon.stub(AuthProviders, 'findOne');
    ClientIdModelfindOneStub = sinon.stub(ClientIdModel, 'findOne');
    setupOAuth1StrategiesStub = sinon.stub(oAuthModel, 'setupOAuth1Strategies').resolves();
    getOAuth1RedirectHandlerStub = sinon.stub(oAuthModel, 'getOAuth1RedirectHandler');
    getOAuth1RedirectHandlerStub
      .withArgs('OAuthFailed.js')
      .returns(async (_, res) => res.send({ failed: true }));
    getOAuth1RedirectHandlerStub
      .withArgs('OAuthSuccess.js')
      .returns(async (_, res) => res.send({ success: true }));
  });

  afterEach(() => {
    passport.authenticate.restore();
    AuthProviders.findOne.restore();
    ClientIdModelfindOneStub.restore();
    setupOAuth1StrategiesStub.restore();
    getOAuth1RedirectHandlerStub.restore();
  });

  describe('/oauth1/connect', () => {
    it('should process a valid request', (done) => {
      try {
        const clientName = 'clientName';
        AuthProviders.findOne.returns({
          populate: () => ({
            client: { name: clientName },
          }),
        });
        passport.authenticate.returns((req, res) => res.send({ success: true }));
        chai
          .request(app)
          .get('/oauth1/connect')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .set('x-auth-userid', userId)
          .query({ clientId })
          .then((res) => {
            expect(res).to.have.status(200);
            done();
          })
          .catch((err) => {
            console.log('%cexternal.spec.js line:84 err', 'color: #007acc;', err);
            done();
          });
      } catch (err) {
        done();
      }
    });
  });

  describe('/oauth1/callback', () => {
    it('should process a valid request', (done) => {
      try {
        const clientName = 'name';
        const oauthToken = 'oauthToken';
        const oauthVerifier = 'oauthVerifier';
        const cookieValue = `x-auth-userid=${userId}`;
        AuthProviders.findOne.returns({
          populate: () => ({
            client: { name: clientName },
          }),
        });

        passport.authenticate.returns((req, res) => res.send({ success: true }));
        chai
          .request(app)
          .get('/oauth1/callback')
          .set('Cookie', cookieValue)
          .query({ clientId, oauth_token: oauthToken, oauth_verifier: oauthVerifier })
          .then((res) => {
            expect(res).to.have.status(200);
            done();
          })
          .catch(() => {
            done();
          });
      } catch (err) {
        done();
      }
    });
  });

  describe('/oauth/validate', () => {
    it('should validate a client and return the scopes assosciated', (done) => {
      try {
        ClientIdModelfindOneStub.returns({
          _id: '5bff1583ffda1e1eb8d35c4b',
          secret: 'authclient_secret',
          scopes: ['premium:read:anonymous'],
        });
        chai
          .request(app)
          .get('/oauth/validate')
          .auth('5bff1583ffda1e1eb8d35c4b', 'authclient_secret')
          .then((res) => {
            expect(res).to.have.status(200);
            done();
          })
          .catch(() => {
            done();
          });
      } catch (err) {
        done();
      }
    });
  });
});
