# Auth API

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Custom New Relic Instrumentation](#custom-new-relic-instrumentation)
- [Auth API Trusted Endpoints](#auth-api-trusted-endpoints)
- [Auth API Endpoints](#auth-api-endpoints)
- [Tracking access tokens / refresh tokens](#tracking-access-tokens--refresh-tokens)
- [Surfline Admin Auth endpoints](#surfline-admin-auth-endpoints)
- [API Validation](#API-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Configure global environemnt
`nvm install v4.3.0`
`npm install -g eslint eslint-config-airbnb`

Configure .env appropriately (.env.sample provided)

Configuring the project
`npm install`

Testing the project
`npm run test`

## Custom New Relic Instrumentation

You can add custom attributes with:

```
import * as newrelic from 'newrelic';
newrelic.default.addCustomAttribute('request', req);
```

## Auth API Trusted Endpoints

To obtain an accessToken you should POST to /trusted/token/ with the below parameters in the body. The client_id and secret must be sent as Basic Authorization headers in this request.

POST /trusted/token
Authorization: 'Basic NWM3NmViNjlmMGI2Y2I3YmU2NmY2ZTYzOmFhNDY2MGEwLTIzNTQtNDE4Ny04MDMxLTgwYzllOWFmN2RhNw==',
Request Body (`password` grant):

{

     "username": "test@surfline.com",
     "password": "123456",
     "grant_type": "password",
     "device_id": "", // Optional
     "device_type": "" // Optional

}

Response:

```
{
    "access_token": "ae3a160fa188276cf50b758906c5415004de59b6",
    "refresh_token": "5cca3fa18053bf942927934d13aad5bef5ef7f5f",
    "expires_in": 2592000,
    "token_type": "Bearer"
}
```

Note:

1. Make sure to add the client_id to the `ClientIds` collection in UserDB. If it is a trusted client set `trusted=true`.
1. To obtain a shortLived token you should POST to /trusted/token/ with query parameter isShortLived=true
1. `device_id` - Unique identifier for the device from which the user signs in. - Optional.
1. `device_type` - Brief description of the type of device used for sign in. - Optional.

Example:

```
{
    deviceId: Chrome-71.0.3578.98,
    deviceType: Chrome 71.0.3578.98 on OS X 10.14.1 64-bit
}
```

5. `refresh_token` can be used to get a new pair of tokens by doing a POST to `/trusted/token` as below:

```
{
     "refresh_token": "9984521dc3469704129d0df27c1402ca695a38e2",
     "grant_type": "refresh_token",
     "client_id": "WEB_VTM8QWOFNZ4X"
}
```

To authorise a token, you should do a GET to `/trusted/authorise` with access_token as query parameter, for example:

```
curl -X GET \
  '/trusted/authorise?access_token=51080aa5a0e530b3114427e719a57ac2b7d47250' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
```

Response would be user's mongo userId and an array of scopes assosciated to the client with which user logged in. By default trusted clients have scopes set as `all`.

Response:

```
{
    "id": "5aabfbc0ffd66500117e554d",
    "scopes": [
        "all"
    ]
}
```

If the token is invalid then a `401 Unauthorized` status code is sent back to client.
Response:

```
{
    "message": "Unauthorized client request"
}
```

## External Endpoints

### GET /oauth/validate

Performs a basic authorization check for the ClientId/Client Secret combination by looking up in the ClientId collection. The ClientId/secret should be sent as a Authorization header.

```
curl --location --request GET 'http://localhost:8002/oauth/validate' \
--header 'Authorization: Basic NjA2YjM2NWRhOGUwYjk1NmY3NjJmMGE5OnNrX2xUWGdSakI0YXBDU2FLNWYwVVVK'
```

Response: An array of scopes assosciated with the client.

```json
{
  "scopes": ["premium:read:anonymous"]
}
```

## Auth API Endpoints

NOTE: /auth/token/ and /auth/authorise are deprecated but would still be backward compatible. Use `trusted` endpoints instead.

To obtain a token you should POST to /auth/token/. You should include grant_type ("password"), username, password and client_id in the request body, for example:

    POST /auth/token/ HTTP/1.1
    Host: server.example.com
    Content-Type: application/x-www-form-urlencoded
    grant_type=password&username=johndoe&password=A3ddj3w&client_id=surfline_web

And the response will be..

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
        "access_token":"2YotnFZFEjr1zCsicMWpAA",
        "token_type":"bearer",
        "expires_in":3600,
        "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA"
    }

To obtain a shortLived token you should POST to /auth/token/ with query parameter isShortLived=true. You should include grant_type ("password"), username, password and client_id in the request body, for example:

    POST /auth/token/?isShortLived=true HTTP/1.1
    Host: server.example.com
    Content-Type: application/x-www-form-urlencoded
    grant_type=password&username=johndoe&password=A3ddj3w&client_id=surfline_web

And the response will be..

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
        "access_token":"2YotnFZFEjr1zCsicMWpAA",
        "token_type":"bearer",
        "expires_in":3600,
        "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA"
    }

To obtain a token from a refresh token, you should POST to /auth/token/. You should include grant_type ("refresh_token") and refresh_token and client_id in the request body, for example:

    POST /auth/token/ HTTP/1.1
    Host: server.example.com
    Content-Type: application/x-www-form-urlencoded
    grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA&client_id=surfline_web

And the response will be..

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
        "access_token":"2YotnFZFEjr1zCsicMWpAA",
        "token_type":"bearer",
        "expires_in":3600,
        "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA"
    }

Each client comes under a ClientGroup. There is a Mongo collection “ClientGroups” in UserDB which will contain the client_ids that are members of a group and the token_limit for each group.
If a client_id is specified in the request then we will check if the client_id exists in the ClientGroups Mongo collection. If it matches then check if the number of access_tokens already issued for this ClientGroup + user combination is greater than the limit set for each ClientGroup. If it is greater than the limit then respond with a 403 status saying the "User have exceeded the token limit”. The response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
        "code": 403,
        "error": "server_error",
        "error_description": "User have exceeded the token limit"
    }

Note: If there is no client_id specified in the body then we bypass this token check and will issue an access_token.

To authorise a token, you should do a GET to /auth/authorise with access_token as query parameter, for example:

    GET /auth/authorise/ HTTP/1.1
    Host: server.example.com
    access_token=tGzv3JOkF0XG5Qx2TlKWIA

And the response will be..

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
        "id": 12345
    }

To invalidate a token pair by userId, you should do a POST to /auth/invalidate-token. You should include mongo user id and brand in the request body, for example:

    GET /auth/invalidate-token/ HTTP/1.1
    Host: server.example.com
    id=12345
    brand='bw'

And the response will be..

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
       "results":"Successfully invalidated token"
    }

To delete a refresh or access token, DELETE /accessToken/{token} or /refreshToken/{token}

    DELETE /auth/accessToken/322943229432294 HTTP/1.1
    Host: server.example.com

And the response will be...

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {"results":"Successfully deleted refresh token."}

or if the token was not found:

    HTTP/1.1 400 Bad Request
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {"results":"Bad request. No token with that value found."}

To delete a refresh and access token, POST /logout?accessToken={token}&refreshToken={token}

    POST /auth/logout?accessToken={token}&refreshToken={token}

And the response will redirect to the hosts route at '/'

### GET /trusted/logout

This API's intended use is to logout a user by removing the `bearer_token` and `refresh_token` keys from Redis and deleting the assosciated cookies from the browser.

```
curl --location --request GET 'http://localhost:8002/trusted/logout' \
--header 'Cookie: access_token=access_token; connect.sid=s%3AqCjo9FAl_bY8F54H9HZ_7VHTOcNMtVgQ.K8J6f%2FZcrG7RXWatW%2F%2FG%2FhmpUkEay4N9ykayqdXxmtg; refresh_token=refresh_token'
```

## Tracking access tokens / refresh tokens

Audit key for user:
`userAndRefreshToken : 'users:${userId}:accessAndRefreshToken'`

Adding to user's audit:
`ZADD ${userAndRefreshToken} ${longest expiring token for user} ${accessToken}:${refreshToken}`

Maintenance when new tokens added:
`ZREMRANGEBYSCORE ${userAndRefreshToken} -inf ${timestamp}`

## Surfline Admin Auth endpoints

To obtain an admin accessToken you should POST to /trusted/token/ with the below parameters in the body. The client_id and secret must be sent as Basic Authorization headers in this request.

`POST /trusted/token`
Authorization: 'Basic NWM3NmViNjlmMGI2Y2I3YmU2NmY2ZTYzOmFhNDY2MGEwLTIzNTQtNDE4Ny04MDMxLTgwYzllOWFmN2RhNw==',
Request Body (client_credentials grant):

```
{
     grant_type: 'client_credentials',
     client_id: <Client Unique ID>,
     client_secret: <Client Secret>,
     scope: <Comma separated user roles. This is assigned in OneLogin>,
     id: <OneLogin/AD unique PersonImmutableID>,
}
```

Response:

```
{
    "access_token": "188429be93201ff3f30c752f5decb56ee74ca7d9",
    "token_type": "Bearer"
}
```

The client details are stored in `ClientIds` Mongo collection for clientId `admin_tools_web`.

To authorise an admin accessToken, you should do a GET to `/trusted/authorise` with access_token as a Bearer Authorization

`GET /trusted/authorise`

Request:

```
curl -X GET  /trusted/authorise \
  -H 'Authorization: Bearer 188429be93201ff3f30c752f5decb56ee74ca7d9'
```

Response:

```
{
    "id": "7mN+rUaDZ0u7sMvTFleD8w==",
    "scopes": [
        "super_admin",
        "Artifactory",
        "Jenkins",
        "New Relic"
    ]
}
```

## API Validation

To validate that the API is functioning as expected (for example, after a deployment), start with the following:

1. Check the `auth-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TmpreE5UUTNOdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE5qa3hOVFEzTnciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Auth API Endpoints](#auth-api-endpoints) section.

**Note:** to make requests against these endpoints, you will need an `access_token`. You can obtain this by logging into `surfline.com` in a browser, opening the developer tools, navigating to Application > Cookies > `surfline.com`, and copying the `access_token` value.

**Ex:**

Test `GET` requests against the `/authorize` endpoint:

```
curl -X GET \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  'http://auth-service.sandbox.surfline.com/trusted/authorise?access_token=cbdde09aaba24f2d629773a769bd216b79da1cc4'

```

**Expected response:** Results should be something like:

```
{
  "id": "58ff79ecb016012247d2cfb1",
  "scopes": [
    "all"
  ]
}
```

