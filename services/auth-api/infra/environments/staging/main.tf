provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/auth-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "auth_service" {
  source = "../../"

  company     = "sl"
  application = "auth-service"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-staging/4042d775e313f612/e1f601835f792a75"
  service_td_count = "1"
  service_lb_rules = [
    {
      field = "host-header"
      value = "auth-service.staging.surfline.com"
    },
  ]
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  dns_name          = "auth-service.staging.surfline.com"
  dns_zone_id       = "Z3JHKQ8ELQG5UE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-2-staging/4042d775e313f612"

  auto_scaling_enabled = false
}
