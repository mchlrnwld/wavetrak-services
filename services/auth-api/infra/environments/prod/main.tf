provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/auth-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "auth_service" {
  source = "../../"

  company     = "sl"
  application = "auth-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd/b668bfe144c94994"
  service_td_count = 10
  service_lb_rules = [
    {
      field = "host-header"
      value = "auth-service.prod.surfline.com"
    },
  ]
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  dns_name          = "auth-service.prod.surfline.com"
  dns_zone_id       = "Z3LLOZIY0ZZQDE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = "750"
  auto_scaling_min_size     = "8"
  auto_scaling_max_size     = "5000"
}
