# tide-service

## Endpoints

Base domain, as usual, is `tide-service.[env].surfline.com`. There are three end points, one for ports, one for data, and one for images.

All timestamps returned are UTC.

### Tide Ports

Tide ports are available at `/ports`.

#### Request Parameters

Without any parameters, _all_ tide ports will be returned.

Name | Type | Description
-----|------|-----------
`port` | String | Name of the tide port.
`lat` | Number | Search for a tide port near this lat/lon.
`lon` | Number | Search for a tide port near this lat/lon.
`distance' | Number | If using lat/lon search for ports upto this many km from the passed lat/lon. Default value 100.
`limit` | Number | Return at most this many. If using lat lon it will return the closest `N` ports.


#### Response fields

The response is a (possibly empty) array of tide port objects that have the following members:

Name | Type | Description
-----|------|-----------
`name` | String | Tide Port name.
`lat` | Number | latitude.
`lon` | Number | longitude.
`minHeight` | Number | Minimum attainable tide height at the location in meteres.
`maxHeight` | Number | Maximum attainable tide height at the location in meteres.
`distance` | Number | If lat/lon used in the request, this is the distance from the supplied lat/lon to the port.
`distance_unit` | String | Unit for the `distance` field.

### Tide Data

Tide data is available at `/data`.

#### Request Parameters

Name | Type | Description
-----|------|-----------
`port` | String | Name of the tide port. User either port or lat/lon
`lat` | Number | Search for a tide port near this lat/lon.  User either port or lat/lon
`lon` | Number | Search for a tide port near this lat/lon. User either port or lat/lon
`distance' | Number | If using lat/lon consider ports upto this many km from the passed lat/lon. Default value 100.
`start` | Integer | Unix timestamp for start. Default value of current time if not provided.
`end` | Integer | Unix timestamp for end. Default value of `start` plus 24 hours.

If you do not pass one or other of `port` or `lat` and`lon` you will get an HTTP 400 Bad Request response. If there is no port of the given name or near the given lat/lon you'll get an HTTP 404 Not Found response.

#### Response fields

Name | Type | Description
-----|------|-----------
`port` | Object | Data about the tide port in use. Structure matches objects in `/ports` endpoint.
`port.name` | String | Tide Port name.
`port.lat` | Number | latitude.
`port.lon` | Number | longitude.
`port.minHeight` | Number | Minimum attainable tide height at the location in meters.
`port.maxHeight` | Number | Maximum attainable tide height at the location in meters.
`port.distance` | Number | If lat/lon used in the request, this is the distance from the supplied lat/lon to the port.
`port.distance_unit` | String | Unit for the `distance` field.
`tide` | Array | Array of tide high/low times between `start` and `end`.
`tide[].time` | Integer | Unix timestamp of tide state.
`tide[].shift` | Number | Tide height in meters.
`tide[].state` | String | The tide state "High" or "Low".
`levels` | Array | Array of tide levels at hourly intervals between `start` and `end`
`levels[].time` | Integer | Unix timestamp that level is valid for.
`levels[].shift` | Number | Tide height at `time` in meters.

#### Example

E.g. `http://tide-service.prod.surfline.com/data?lat=50&lon=-4&start=1620392400&end=1620460800`

```json
{
  "port": {
    "name": "Thurlestone",
    "lat": 50.26,
    "lon": -3.86,
    "minHeight": -0.02,
    "maxHeight": 6.35,
    "distance": 30.62,
    "distance_unit": "km"
  },
  "tide": [
    {
      "time": 1620400539,
      "shift": 4.27,
      "state": "High"
    },
    {
      "time": 1620422561,
      "shift": 1.81,
      "state": "Low"
    },
    {
      "time": 1620444551,
      "shift": 4.49,
      "state": "High"
    }
  ],
  "levels": [
    {
      "time": 1620392400,
      "shift": 3.693811
    },
    {
      "time": 1620396000,
      "shift": 4.092889
    },
    ...
    {
      "time": 1620460800,
      "shift": 2.244141
    }
  ]
}
```


### Tide Images

TBC

## Local development

To build and run the docker environment, use:

```bash
cp .env.sample .env
```

and update the `.env` file accordingly, then

```bash
make run
```

The PHP application inside [src](./src) is now being served from [localhost](http://localhost:8080/).

## Adding New Tide Ports

This process allows the creation of new tide ports, generating the tide-port harmonics data from the TPXO8 atlas. All steps are to be completed on the command line, from the root directy of this service, i.e. `services/tide-service`.

1. Download the TPXO8 atlas data files.

These files are large and not stored in the repo. This only needs to be done once.

```bash
aws s3 sync s3://wt-artifacts-prod/tide-service/tpxo8/DATA ./tpxo8/DATA
```

2. Create a new branch in the repo for adding the new port (or ports).

Beware, this will update the repo to the lastest version and remove any local changes you have made.

Replace `add-some-port` with an nice branch name for what you wnt to do.

```bash
git fetch && git checkout -b add-some-port && git reset --hard origin/master
```

3. Create the new tide port

Modify the following as you need. It can take a minute or two to run.

Using LAT as Datum
```bash
make tpxo8-port LAT=0.0 LON=0.0 NAME='Some port, somewhere' TZ='Europe/London'
```

Using MLLW as Datum
```bash
make mllw-port LAT=0.0 LON=0.0 NAME='Some port, somewhere' TZ='Europe/London'
```

If the lat/lon values are on land, or too close to land and not in TPXO8 atlas you will get an error, otherwise it should append some lines to the file `./harmonics/wavetrak.txt`

You can see the newly addded lines using `git diff`. They should look something like the other ports in the file, e.g.

```
# 
# source TPXO8 07.14.2014
# BEGIN HOT COMMENTS
# country: Unknown
# restriction: Non-commercial use only
# date_imported: 20190816
# datum: Lowest Astronomical Tide
# confidence: 5
# !units: meters
# !longitude: -149.2776
# !latitude: -17.8619
Teahupoo
+00:00 :Pacific/Tahiti
0.2190 meters
K1              0.0060   58.20
K2              0.0230  310.00
M2              0.0900  298.00
x 0 0
MF              0.0080   31.50
MM              0.0050   19.40
MN4             0.0010   66.80
MS4             0.0010  168.10
N2              0.0160  275.20
O1              0.0180   85.90
P1              0.0020   38.50
Q1              0.0040   78.60
S2              0.0790  315.60
```

Beware, the tooling that reads these files is very picky about the format. Avoid making any other alterations and don't add more comments.

4. Check the tides for your new port

This will print out times of high and low water for the next few days.

```bash
make tides NAME='Your port'
```

5. Finish up

If you're *not happy* with the result, you cna remove your changes to the `./harmonics/wavetrak.txt` using

```bash
git checkout -- ./harmonics/wavetrak.txt
```

If you *are happy* then commit this change with appropriate message and push your branch to GitHub:

```bash
git add ./harmonics/wavetrak.txt && git commit -m 'Add tideport Some Tide Port' && git push --set-upstream origin HEAD
```

then navigate to [wavetrak-services](https://github.com/Surfline/wavetrak-services) and open a pull-request to merge your branch to `master`. Since you're just pushed up some changes, GitHub will likely show a banner inviting you to open a PR.

Once the PR is approved, merged, and deployed to `prod` the new tide port wil be live.

### Adding multiple ports

It's fine to add multiple tide ports as part of single pull request. Just repeat step 3/4 above multiple times. In this case, if a one tide port gives bad results and you want to ditch the harmonic data for that port you'll need to manually edit the file `harmonics/wavetrak.txt` to remove the relevant lines as the `git` command ould revert _all_ changes to the file.
