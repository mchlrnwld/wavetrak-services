// Speed:  angular units over time units.
// The Angle class is not used here since normalization is not what we want.

// Once again we have unnecessary roundoff if rad_per_second is the wrong
// storage representation.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

double
Speed::rad (units per) const {
  switch (per) {
  case SECOND:
    return rad_per_second;
  case HOUR:
    return rad_per_second * 3600.0;
  case JULIAN_CENTURY:
    return rad_per_second * 3155760000.0;
  default:
    assert (0);
  }
  // Silence bogus SGI compiler warning
  return 0.0;
}

double
Speed::deg (units per) const {
  double deg_per_second = rad_per_second * 180.0 / M_PI;
  switch (per) {
  case SECOND:
    return deg_per_second;
  case HOUR:
    return deg_per_second * 3600.0;
  case JULIAN_CENTURY:
    return deg_per_second * 3155760000.0;
  default:
    assert (0);
  }
  // Silence bogus SGI compiler warning
  return 0.0;
}

void
Speed::rad (double inval, units per) {
  switch (per) {
  case SECOND:
    rad_per_second = inval;
    break;
  case HOUR:
    rad_per_second = inval / 3600.0;
    break;
  case JULIAN_CENTURY:
    rad_per_second = inval / 3155760000.0;
    break;
  default:
    assert (0);
  }
}

void
Speed::deg (double inval, units per) {
  double rad_per = inval * M_PI / 180.0;
  switch (per) {
  case SECOND:
    rad_per_second = rad_per;
    break;
  case HOUR:
    rad_per_second = rad_per / 3600.0;
    break;
  case JULIAN_CENTURY:
    rad_per_second = rad_per / 3155760000.0;
    break;
  default:
    assert (0);
  }
}

Speed &
Speed::operator+= (Speed a) {
  rad_per_second += a.rad_per_second;
  return (*this);
}

Speed &
Speed::operator*= (double a) {
  rad_per_second *= a;
  return (*this);
}

Speed operator+ (Speed a, Speed b) {
  Speed t;
  t.rad (a.rad(Speed::SECOND) + b.rad(Speed::SECOND), Speed::SECOND);
  return t;
}

Speed operator* (double a, Speed b) {
  Speed t;
  t.rad (b.rad(Speed::SECOND) * a, Speed::SECOND);
  return t;
}

DegreesPerJulianCentury::DegreesPerJulianCentury (double inval) {
  deg (inval, JULIAN_CENTURY);
}

DegreesPerHour::DegreesPerHour (double inval) {
  deg (inval, HOUR);
}
