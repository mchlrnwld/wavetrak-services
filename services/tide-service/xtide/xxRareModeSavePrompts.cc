// $Id: xxRareModeSavePrompts.cc,v 1.3 2004/04/19 20:16:56 flaterco Exp $
/*  xxRareModeSavePrompts  Get a file name and two timestamps from the user.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxRareModeSavePromptsCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxRareModeSavePrompts *e = (xxRareModeSavePrompts *)client_data;

  Dstr filename (e->filenamediag->val());

  if (filename.strchr ('\n') != -1 ||
      filename.strchr ('\r') != -1 ||
      filename.strchr (' ') != -1 ||
      filename[0] == '-') {
    Dstr details ("Well, it's not that I can't do it, it's that you probably\n\
don't want me to.  The filename that you entered is '");
    details += filename;
    details += "'.\n";
    if (filename[0] == '-')
      details += "Filenames that begin with a dash are considered harmful.";
    else
      details += "Whitespace in filenames is considered harmful.";
    barf (CANT_OPEN_FILE, details, 0);
  } else {
    Dstr start_tm_string, end_tm_string;
    e->btimediag->val (start_tm_string);
    Timestamp start_tm (start_tm_string, e->timezone, e->xtidecontext->settings);
    e->etimediag->val (end_tm_string);
    Timestamp end_tm (end_tm_string, e->timezone, e->xtidecontext->settings);
    if (start_tm.isNull()) {
      Dstr details ("Bad start time.  The offending input was ");
      details += start_tm_string;
      details += "\nin the time zone ";
      if ((*(e->xtidecontext->settings))["z"].c == 'n')
	details += e->timezone;
      else
	details += "UTC0";
      barf (MKTIME_FAILED, details, 0);
    } else if (end_tm.isNull()) {
      Dstr details ("Bad end time.  The offending input was ");
      details += end_tm_string;
      details += "\nin the time zone ";
      if ((*(e->xtidecontext->settings))["z"].c == 'n')
	details += e->timezone;
      else
	details += "UTC0";
      barf (MKTIME_FAILED, details, 0);
    } else {
      if (start_tm <= end_tm)
        (*(e->callback)) (filename, start_tm, end_tm, e->ptr);
      else
        (*(e->callback)) (filename, end_tm, start_tm, e->ptr);
    }
  }
  delete e;
}

static void
xxRareModeSavePromptsCancelCallback (Widget w, XtPointer client_data, XtPointer
call_data) {
  xxRareModeSavePrompts *e = (xxRareModeSavePrompts *)client_data;
  e->dismiss();
}

void
xxRareModeSavePrompts::dismiss() {
  delete this;
}

xxRareModeSavePrompts::~xxRareModeSavePrompts() {
  mypopup->unrealize();
  delete filenamediag;
  delete helplabel1;
  delete gobutton;
  delete cancelbutton;
  delete btimediag;
  delete etimediag;
  delete spacelabel1;
}

xxRareModeSavePrompts::xxRareModeSavePrompts (xxTideContext *in_xtidecontext,
xxContext *context, void (*in_callback) (Dstr &filename, Timestamp start_tm,
Timestamp end_tm, void *in_ptr), void *in_ptr, char *initname,
Timestamp in_start_tm, Timestamp in_end_tm, const Dstr &in_timezone):
xxWindow (in_xtidecontext, context, 1, XtGrabExclusive) {
  ptr = in_ptr;
  callback = in_callback;
  timezone = in_timezone;
  mypopup->setTitle ("Save prompts");

  Arg labelargs[3] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNborderWidth, (XtArgVal)0}
  };
  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)mypopup->visual},
    {XtNcolormap, (XtArgVal)mypopup->colormap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };

  {
    Dstr helptext ("\
Please enter the name of the file in which to save output.\n\
You can also change the begin and end times for the listing.\n\
The active time zone is ");
    if ((*(in_xtidecontext->settings))["z"].c == 'n')
      helptext += timezone;
    else
      helptext += "UTC0";
    helptext += ".";
    Widget labelwidget = XtCreateManagedWidget (helptext.aschar(),
      labelWidgetClass, container->manager, labelargs, 3);
    helplabel1 = new xxContext (mypopup, labelwidget);
  }

  filenamediag = new xxHorizDialog (container, "Filename:", initname);
  btimediag = new xxTimestampDialog (xtidecontext, container, "Start at:",
    in_start_tm, timezone);
  etimediag = new xxTimestampDialog (xtidecontext, container, "End at:",
    in_end_tm, timezone);

  {
    Widget labelwidget = XtCreateManagedWidget ("  ", labelWidgetClass,
      container->manager, labelargs, 3);
    spacelabel1 = new xxContext (mypopup, labelwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Go", commandWidgetClass,
      container->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxRareModeSavePromptsCallback,
     (XtPointer)this);
    gobutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Cancel", commandWidgetClass,
      container->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxRareModeSavePromptsCancelCallback,
      (XtPointer)this);
    cancelbutton = new xxContext (mypopup, buttonwidget);
  }

  mypopup->realize();
}
