// $Id: xxUnsignedChooser.hh,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxUnsignedChooser  Let user choose an unsigned value.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxUnsignedChooser {

  friend void xxUnsignedChooserUpCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxUnsignedChooserDownCallback (Widget w, XtPointer client_data,
    XtPointer call_data);

public:
  xxUnsignedChooser (xxContext *in_xxcontext, char *caption,
    unsigned init, int init_is_null, unsigned in_minimum);
  ~xxUnsignedChooser();

  unsigned choice (); // Returns 1000000 for null.
  void global_redraw();
  void update_value();

protected:
  unsigned minimum, current_choice;
  xxContext *box, *label, *numlabel, *upbutton, *downbutton;
};
