/*  XTide  Harmonic tide clock and tide predictor
    Last modified 1997-11-16

    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

int main (int argc, char **argv) {

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "SUPER_ULTRA_VERBOSE_DEBUGGING is enabled.  (This is a compile-time option.)" << endl;
#ifdef HAVE_PNG
  cerr << "png.h header file reports version " << PNG_LIBPNG_VER_STRING << endl;
  cerr << "libpng reports version " << png_libpng_ver << endl;
  if (!strcmp (png_libpng_ver, "0.95"))
    cerr << "(He just forgot to change the version string; it's okay.)" << endl;
#endif
#endif

  // So much for ANSI.
  // sync_with_stdio();

  srand (time (NULL));

  xxXTideRoot root (argc, argv);

  root.mainloop();

  exit (0);
}
