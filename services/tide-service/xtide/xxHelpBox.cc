// $Id: xxHelpBox.cc,v 1.3 2004/09/07 21:23:30 flaterco Exp $
/*  xxHelpBox  Response to pressing a '?' button.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

static void
dismissCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxHelpBox *)client_data)->dismiss();
}

void
xxHelpBox::dismiss() {
  delete this;
}

xxHelpBox::xxHelpBox (xxTideContext *in_tidecontext,
		      xxContext *in_xxcontext, const Dstr &help):
xxWindow (in_tidecontext, in_xxcontext, 1) {
  mypopup->setTitle ("About...");
  Arg bgargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  {
    Widget labelwidget = XtCreateManagedWidget (help.aschar(),
      labelWidgetClass, container->manager, bgargs, 2);
    label = new xxContext (mypopup, labelwidget);
  }

  {
    Arg buttonargs[2] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
    };
    Widget buttonwidget = XtCreateManagedWidget ("Dismiss", commandWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, dismissCallback,
     (XtPointer)this);
    dismissbutton = new xxContext (mypopup, buttonwidget);
  }

  mypopup->realize();
  mypopup->fixSize();
}

xxHelpBox::~xxHelpBox() {
  mypopup->unrealize();
  delete label;
  delete dismissbutton;
}

void xxHelpBox::global_redraw() {
  xxWindow::global_redraw();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (dismissbutton)
    XtSetValues (dismissbutton->manager, buttonargs, 2);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (label)
    XtSetValues (label->manager, args, 2);
}
