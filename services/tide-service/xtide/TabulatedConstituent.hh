// $Id: TabulatedConstituent.hh,v 1.6 2004/10/04 13:57:50 flaterco Exp $
// TabulatedConstituent:  definition of a constituent that uses tabulated
// equilibrium arguments and node factors.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class TabulatedConstituent {
public:
  TabulatedConstituent();
  ~TabulatedConstituent();

  TabulatedConstituent &operator = (TabulatedConstituent &in_con);

  void speed (Speed in_speed);
  Speed speed ();
  // This should be redundant -- workaround for bug in
  // Sun WorkShop Compiler C++ SPARC Version 5.000
  // Error: Could not find a match for TabulatedConstituent::speed(DegreesPerHour).
  void speed (DegreesPerHour in_speed);

  Angle arg (Year in_year);   // Equilibrium arguments
  double nod (Year in_year);  // Node factors

  Year firstvalidyear();
  Year lastvalidyear();

  // Install equilibrium arguments and node factors.  N.B. single-precision
  // floats on input to match libtcd; otherwise never used in XTide.
  // args are in degrees.
  void installargsandnodes (Year first_year, Year last_year,
			    float *args, float *nodes);

  // This is useless outside of this class, but the Sparc compiler
  // demands that it be declared public.
  class Argfac {
  public:
    Argfac();  // Sets arg = 0, nod = 1
    Argfac (Angle in_arg, double in_nod);
    Angle arg;
    double nod;
  };

protected:
  Speed myspeed;
  Argfac *myargfacs;
  Year myfirstvalidyear, mylastvalidyear;
  unsigned nargs;

  void check_valid (Year in_year);
};
