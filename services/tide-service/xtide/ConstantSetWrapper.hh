// $Id: ConstantSetWrapper.hh,v 1.3 2004/10/21 19:58:13 flaterco Exp $
/*  ConstantSetWrapper

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class ConstantSetWrapper {
public:
  ConstantSetWrapper (ConstituentSet *in_constituents,
                      ConstantSet *in_constants);
  ~ConstantSetWrapper();

  // This will never have a value of type KnotsSquared.
  // This changes if offsets change.
  Amplitude maxAmplitude;

  void setSimpleOffsets (SimpleOffsets in_offsets);
  void setUnits (PredictionValue::Unit in_units);

  // This is the back end to the tide prediction functions that will be
  // finished out in class Station.  predictHeight does not have the
  // datum added in and will not be converted from KnotsSquared.
  // predictHeight wraps time2dt_tide.
  PredictionValue predictHeight (Timestamp in_timestamp, unsigned deriv);
  PredictionValue datum() const;
 /* dt_tide_max (int n)
  *   Returns the maximum that the absolute value of the nth derivative
  * of the tide can ever attain.
  */
  Amplitude dt_tide_max (unsigned deriv);

  // Tell me what units predictHeight will return.
  PredictionValue::Unit predictUnits ();

protected:

#define TIDE_MAX_DERIV (2)      /* Maximum derivative supported by
                                 * time2dt_tide() and family. */

  ConstituentSet *constituents;  // Node facs, eq. args.
  ConstantSet *origConstants;    // Already adjusted for meridian.
  ConstantSet adjConstants;      // origConstants with offsets and unit
                                 // conversions applied.
  ConstantSet yearConstants;     // adjConstants with node factors and
                                 // eq. args. applied

  // Updates adjConstants, maxAmplitude, and maxdt, then calls
  // refresh_yearConstants().
  void refresh_adjConstants_yearConstants ();
  // Updates yearConstants, amplitudes, phases, epoch, and next_epoch.
  void refresh_yearConstants ();

  Amplitude origMaxAmplitude;

  // Behold, shameless hand-optimization of the innermost loop.  These
  // vectors are "pre-fetched" from yearConstants and constituents and
  // are updated as needed in refresh_yearConstants.
  unsigned length;
  double *speeds;        // in radians per second.
  double *amplitudes;    // in myUnits.
  double *phases;        // in radians.

  // G. Dairiki code, slightly revised.
  double _time2dt_tide (long t, unsigned deriv);
  double blend_weight (double x, unsigned deriv);
  double blend_tide (Timestamp in_timestamp, unsigned deriv, Year first_year,
    double blend);
  double time2dt_tide (Timestamp in_timestamp, unsigned deriv);
  // This is now initialized in the constructor.
  Amplitude orig_maxdt[TIDE_MAX_DERIV+2];
  Amplitude maxdt[TIDE_MAX_DERIV+2];

  Year currentYear;
  Timestamp epoch;
  Timestamp next_epoch;
  SimpleOffsets mySimpleOffsets;
  PredictionValue::Unit myUnits;  // Never KnotsSquared
};
