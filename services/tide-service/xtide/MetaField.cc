// $Id: MetaField.cc,v 1.2 2004/10/15 13:58:07 flaterco Exp $

/*
    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

MetaField::MetaField (const char *name_in, const char *value_in) {
  name = name_in;
  value = value_in;
  value.trim();
}

MetaField::MetaField (const char *name_in, const Dstr &value_in) {
  name = name_in;
  value = value_in;
  value.trim();
}

MetaField::MetaField (const Dstr &name_in, const Dstr &value_in) {
  name = name_in;
  value = value_in;
  value.trim();
}
