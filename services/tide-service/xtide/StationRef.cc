// $Id: StationRef.cc,v 1.6 2004/10/26 15:54:52 flaterco Exp $
/*  StationRef  Index information for a station in a harmonics file.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Station *StationRef::load (TideContext *in_context) {
  HarmonicsFile h (*harmonicsFileName, in_context->settings);
  return h.getStation (TCDRecordNumber, in_context);
}

StationRef::StationRef () {
  harmonicsFileName = NULL;
}

void StationRef::shortHarmonicsFileName (Dstr &text_out) {
  assert (harmonicsFileName);
  text_out = *harmonicsFileName;
  int index = text_out.strrchr ('/');
  if (index != -1)
    text_out /= index+1;
}
