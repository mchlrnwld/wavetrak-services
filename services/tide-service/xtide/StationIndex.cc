// $Id: StationIndex.cc,v 1.3 2004/10/26 15:54:52 flaterco Exp $
/*  StationIndex  Collection of StationRefs.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

StationIndex::StationIndex () {
  used = 0;
  max = 1000;
  assert (stationrefs = (StationRef**) malloc (max * sizeof (StationRef*)));
}

StationIndex::~StationIndex() {
  // unsigned long i;
  // Don't do this; share stationrefs.
  // for (i=0; i<used; i++)
  //   delete stationrefs[i];
  free (stationrefs);
}

StationIndex *StationIndex::clone () {
  StationIndex *temp = new StationIndex();
  unsigned long a;
  for (a=0; a<used; a++)
    temp->add (stationrefs[a]);
  return temp;
}

char **StationIndex::makeStringList (unsigned long startat,
unsigned long maxlength) {
  if (!used) {
    char **temp = (char **) malloc (sizeof (char *));
    temp[0] = NULL;
    return temp;
  }

  assert (startat < used);
  if (used - startat < maxlength)
    maxlength = used - startat;
  char **temp = (char **) malloc ((maxlength+1) * sizeof (char *));
  unsigned long i;
  for (i=startat; i<startat+maxlength; i++) {
    Dstr styp;

    if (stationrefs[i]->isReferenceStation)
      styp = "Ref";
    else
      styp = "Sub";

    char cbuf[20];
    char tempbuf[121];
    if (stationrefs[i]->coordinates.isNull())
      sprintf (cbuf, "       NULL       ");
    else
      sprintf (cbuf, "%8.4f %9.4f",
                             stationrefs[i]->coordinates.lat(),
                             stationrefs[i]->coordinates.lng());
    sprintf (tempbuf, "%-80.80s %-4.4s %18.18s",
                             stationrefs[i]->name.aschar(),
                             styp.aschar(),
                             cbuf);
    temp[i-startat] = strdup (tempbuf);
  }
  temp[maxlength] = NULL;
  return temp;
}

double
StationIndex::bestCenterLongitude() {
  // -180 -150 -120 -90 -60 -30 0 30 60 90 120 150
  unsigned long counters[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
  unsigned long i;
  for (i=0; i<used; i++) {
    Coordinates c = stationrefs[i]->coordinates;
    if (!(c.isNull())) {
      int j = (int)((c.lng()+180.0)/30.0+0.5);
      if (j == 12)
        j = 0;
      counters[j]++;
    }
  }
  int best = 0;
  for (i=1; i<12; i++) {
    if (counters[i] > counters[best])
      best = i;
  }
  return (double)best * 30.0 - 180.0;
}

void
StationIndex::add (StationRef *addthis) {
  if (used == max) {
    max *= 2;
    assert (stationrefs = (StationRef**) realloc (stationrefs,
      max * sizeof (StationRef*)));
  }
  stationrefs[used++] = addthis;
}

void StationIndex::addHfileId (Dstr &hid) {
  if (!(hfile_ids.isNull()))
    hfile_ids += "<br>";
  hfile_ids += hid;
}

void
StationIndex::qsort (sortfield sortby) {
  switch (sortby) {
  case sf_name:
    ::qsort (stationrefs, used, sizeof(StationRef*), &compare_stationrefs_name);
    break;
  case sf_latitude:
    ::qsort (stationrefs, used, sizeof(StationRef*), &compare_stationrefs_lat);
    break;
  case sf_longitude:
    ::qsort (stationrefs, used, sizeof(StationRef*), &compare_stationrefs_lng);
    break;
  default:
    assert (0);
  }
}

unsigned long
StationIndex::length() {
  return used;
}

StationRef *
StationIndex::operator[] (unsigned long index) {
  assert (index < used);
  return stationrefs[index];
}

StationRef *
StationIndex::operator[] (const Dstr &name) {
  unsigned long i;
  for (i=0; i<used; i++)
    if (stationrefs[i]->name %= name)
      return stationrefs[i];
  return NULL;
}
