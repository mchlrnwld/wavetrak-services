// $Id: xxErrorBox.hh,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxErrorBox  Show error message.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxErrorBox: public xxWindow {
public:
  xxErrorBox (xxTideContext *in_xtidecontext, xxContext *context, const Dstr &errmsg, int fatal = 1);
  ~xxErrorBox();
  void dismiss();

protected:
  int is_fatal;
  Pixmap errorboxpixmap, errorbgpixmap;
  xxContext *picture, *label, *button;
};
