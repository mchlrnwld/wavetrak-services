// $Id: skycal.hh,v 1.5 2004/04/05 16:49:21 flaterco Exp $
// skycal.hh -- Functions for sun and moon events.
// Please see skycal.cc for verbose commentary.

void find_next_moon_phase (Timestamp &t, TideEvent::EventType &etype_out,
Direction d);

void find_next_rise_or_set (Timestamp &t, Coordinates c,
TideEvent::EventType &etype_out, Direction d, int lunar);

int sun_is_up (Timestamp &t, Coordinates c);
