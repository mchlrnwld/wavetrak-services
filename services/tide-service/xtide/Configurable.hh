// $Id: Configurable.hh,v 1.3 2004/09/08 19:01:47 flaterco Exp $
/*  Configurable  A setting or a switch.

    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

typedef std::vector<Dstr> Dvector;
typedef std::vector<Dstr>::iterator DvectorIterator;

struct Configurable {

  // Tokens mangled to avoid stepping on reserved words.

  // Settings are those configurables enumerated at
  // http://www.flaterco.com/xtide/settings.html, which have
  // representations as preprocessor defines, X resources, xtide.xml
  // attributes, and command line switches.

  // Switches are those configurables enumerated at
  // http://www.flaterco.com/xtide/tty.html, which are not available
  // in any form except command line switches.

  // X switches are those enumerated in the man page for X11.  Their
  // use is generally unsupported and discouraged, but they are
  // "allowed" by the command line parser.

  enum Kind {Ksetting, Kswitch, KXswitch};

  // Representation tells you which of the data types is used.
  // Rvoid means none of them -- the value isn't retained at all.

  enum Representation {Runsigned, Rdouble, Rchar, RDstr,
                       RPredictionValue, RDvector, Rvoid};

  // Interpretation constrains the allowable values.  No value is
  // constrained to be not null.  All constraint checking is done
  // in Settings.cc.  For Rvoids this is moot.
  //   Iboolean -- constrains Rchar to 'y' or 'n'
  //   Iposint  -- constrains Runsigned to be > 0
  //   Iposdouble -- constrains Rdouble to be > 0.0
  //   Igldouble -- constrains Rdouble to have one of the gl values
  //   Imode -- constrains Rchar to be one of the mode characters
  //   Iformat -- constrains Rchar to be one of the format characters
  //   Icolor -- constrains RDstr to contain a color spec
  //   Iunit -- constrains RDstr to be "ft", "m", or "x"
  //   Istrftime -- constrains RDstr to be a strftime format string
  //   Inumber -- a scanf %f must succeed on the input
  //   Itext -- the input cannot be a null pointer or empty string

  enum Interpretation {Iboolean, Iposint, Iposdouble, Igldouble, Imode,
		       Iformat, Icolor, Iunit, Istrftime, Inumber, Itext};

  // For settings and switches, switchname is 2-charater ID used on
  // command line and XML, and resourcename is the X resource name
  // without the XTide* prefix.  For Xswitches, switchname is the
  // possibly long command line switch and resourcename is null.
  Dstr switchname;
  Dstr resourcename;

  // For use in control panel.
  Dstr caption;

  Kind kind;
  Representation representation;
  Interpretation interpretation;

  // This glob of attributes is collectively "value."  The space saved
  // by a union would not be worth the inconvenience.

  int isnull;

  unsigned        u;
  double          d;
  char            c;
  Dstr            s;
  PredictionValue p;
  Dvector         v;

  // Only unsigned configurables can have minimum values, so why make
  // it complicated.
  unsigned min_value;

  // No need for default_value; see ConfiguredDefaults below.
};

// Keyed by switchname.
typedef std::map<std::string, Configurable>            ConfigurablesMap;
typedef std::map<std::string, Configurable>::iterator  ConfigurablesIterator;
typedef std::map<std::string, Configurable>::const_iterator  ConfigurablesConstIterator;

// I am not sure how to make an initializer for a map or if it is
// possible at all, so let's make this an array and initialize the map
// at run time.

// "X" is where the X geometry string ends up.

#define ConfiguredDefaults { \
{"bg", "background", "Background color for text windows and location chooser.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,bgdefcolor,PredictionValue(),Dvector(), 0}, \
{"fg", "foreground", "Color of text and other notations.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,fgdefcolor,PredictionValue(),Dvector(), 0}, \
{"bc", "buttoncolor", "Background color of buttons.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,buttondefcolor,PredictionValue(),Dvector(), 0}, \
{"dc", "daycolor", "Daytime background color in tide graphs.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,daydefcolor,PredictionValue(),Dvector(), 0}, \
{"Dc", "datumcolor", "Color of datum line in tide graphs.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,datumdefcolor,PredictionValue(),Dvector(), 0}, \
{"ec", "ebbcolor", "Foreground in tide graphs during outgoing tide.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,ebbdefcolor,PredictionValue(),Dvector(), 0}, \
{"fc", "floodcolor", "Foreground in tide graphs during incoming tide.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,flooddefcolor,PredictionValue(),Dvector(), 0}, \
{"mc", "markcolor", "Color of mark line in graphs and of stations in the location chooser.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,markdefcolor,PredictionValue(),Dvector(), 0}, \
{"Mc", "mslcolor", "Color of Mean Astronomical Tide line in tide graphs.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,msldefcolor,PredictionValue(),Dvector(), 0}, \
{"nc", "nightcolor", "Nighttime background color in tide graphs.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,nightdefcolor,PredictionValue(),Dvector(), 0}, \
{"tc", "twilightcolor", "Twilight background color in tide graphs.", Configurable::Ksetting, Configurable::RDstr, Configurable::Icolor, 0, 0,0,0,twidefcolor,PredictionValue(),Dvector(), 0}, \
{"el", "extralines", "Draw datum and Mean Astronomical Tide lines in tide graphs?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,extralines,Dstr(),PredictionValue(),Dvector(), 0}, \
{"fe", "flatearth", "Prefer flat map to round globe location chooser?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,flatearth,Dstr(),PredictionValue(),Dvector(), 0}, \
{"cb", "cbuttons", "Create tide clocks with buttons?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,cbuttons,Dstr(),PredictionValue(),Dvector(), 0}, \
{"in", "infer", "Use inferred constituents (expert only)?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,infer,Dstr(),PredictionValue(),Dvector(), 0}, \
{"nl", "noleftmargin", "Suppress Left Margin?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,noleftmargin,Dstr(),PredictionValue(),Dvector(), 0}, \
{"nf", "nofill", "Draw tide graphs as line graphs?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,nofill,Dstr(),PredictionValue(),Dvector(), 0}, \
{"ns", "nosunmoon", "Suppress sun and moon events in output?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,nosunmoon,Dstr(),PredictionValue(),Dvector(), 0}, \
{"tl", "toplines", "Draw depth lines on top of tide graph?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,toplines,Dstr(),PredictionValue(),Dvector(), 0}, \
{"st", "showtitle", "Show title on graph?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,showtitle,Dstr(),PredictionValue(),Dvector(), 0}, \
{"nm", "nomoon", "Suppress moon events?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,nomoon,Dstr(),PredictionValue(),Dvector(), 0}, \
{"tt", "timetext", "Show time text?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,timetext,Dstr(),PredictionValue(),Dvector(), 0}, \
{"ta", "timeaxis", "Show time axis?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,timeaxis,Dstr(),PredictionValue(),Dvector(), 0}, \
{"nx", "nox", "Suppress X?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,nox,Dstr(),PredictionValue(),Dvector(), 0}, \
{"z", "zulu", "Coerce all time zones to UTC?", Configurable::Ksetting, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,zulu,Dstr(),PredictionValue(),Dvector(), 0}, \
{"cw", "cwidth", "Default width for tide clocks (pixels):", Configurable::Ksetting, Configurable::Runsigned, Configurable::Iposint, 0, max(mingwidth,defcwidth),0,0,Dstr(),PredictionValue(),Dvector(), mingwidth}, \
{"gw", "gwidth", "Default width for tide graphs (pixels):", Configurable::Ksetting, Configurable::Runsigned, Configurable::Iposint, 0, max(mingwidth,defgwidth),0,0,Dstr(),PredictionValue(),Dvector(), mingwidth}, \
{"gh", "gheight", "Default height for tide graphs and clocks (pixels):", Configurable::Ksetting, Configurable::Runsigned, Configurable::Iposint, 0, max(mingheight,defgheight),0,0,Dstr(),PredictionValue(),Dvector(), mingheight}, \
{"tw", "ttywidth", "Default width of ASCII graphs and banners (characters):", Configurable::Ksetting, Configurable::Runsigned, Configurable::Iposint, 0, max(minttywidth,defttywidth),0,0,Dstr(),PredictionValue(),Dvector(), minttywidth}, \
{"th", "ttyheight", "Default height of ASCII graphs (characters):", Configurable::Ksetting, Configurable::Runsigned, Configurable::Iposint, 0, max(minttyheight,defttyheight),0,0,Dstr(),PredictionValue(),Dvector(), minttyheight}, \
{"ga", "gaspect", "Default aspect for tide graphs.", Configurable::Ksetting, Configurable::Rdouble, Configurable::Iposdouble, 0, 0,defgaspect,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"lw", "lwidth", "Width for lines in line graphs (pixels, pos. real number).", Configurable::Ksetting, Configurable::Rdouble, Configurable::Iposdouble, 0, 0,deflwidth,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"gl", "globelongitude", "Default center longitude for globe:", Configurable::Ksetting, Configurable::Rdouble, Configurable::Igldouble, 0, 0,defgl,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"df", "datefmt", "Strftime style format string for printing dates.", Configurable::Ksetting, Configurable::RDstr, Configurable::Istrftime, 0, 0,0,0,datefmt,PredictionValue(),Dvector(), 0}, \
{"hf", "hourfmt", "Strftime style format string for printing hour labels on time axis.", Configurable::Ksetting, Configurable::RDstr, Configurable::Istrftime, 0, 0,0,0,hourfmt,PredictionValue(),Dvector(), 0}, \
{"tf", "timefmt", "Strftime style format string for printing times.", Configurable::Ksetting, Configurable::RDstr, Configurable::Istrftime, 0, 0,0,0,timefmt,PredictionValue(),Dvector(), 0}, \
{"u", "units", "Preferred units of length:", Configurable::Ksetting, Configurable::RDstr, Configurable::Iunit, 0, 0,0,0,prefunits,PredictionValue(),Dvector(), 0}, \
\
{"v", Dstr(), Dstr(), Configurable::Kswitch, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,'n',Dstr(),PredictionValue(),Dvector(), 0}, \
{"suck", Dstr(), Dstr(), Configurable::Kswitch, Configurable::Rchar, Configurable::Iboolean, 0, 0,0,'n',Dstr(),PredictionValue(),Dvector(), 0}, \
{"b", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RDstr, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"e", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RDstr, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"s", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RDstr, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"f", Dstr(), Dstr(), Configurable::Kswitch, Configurable::Rchar, Configurable::Iformat, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"m", Dstr(), Dstr(), Configurable::Kswitch, Configurable::Rchar, Configurable::Imode, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"l", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RDvector, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"ml", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RPredictionValue, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"o", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RDstr, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
\
{"X", Dstr(), Dstr(), Configurable::Kswitch, Configurable::RDstr, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
\
{"foreground", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Icolor, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"background", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Icolor, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"display", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"fn", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"font", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"bd", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Icolor, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"bordercolor", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Icolor, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"borderwidth", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"bw", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"iconic", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Iboolean, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"name", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"reverse", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Iboolean, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"rv", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Iboolean, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"selectionTimeout", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Inumber, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"synchronous", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Iboolean, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"title", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"xnllanguage", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
{"xrm", Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0}, \
\
{Dstr(), Dstr(), Dstr(), Configurable::KXswitch, Configurable::Rvoid, Configurable::Itext, 1, 0,0,0,Dstr(),PredictionValue(),Dvector(), 0} \
}
