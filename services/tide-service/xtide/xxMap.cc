// $Id: xxMap.cc,v 1.8 2005/01/07 02:36:32 flaterco Exp $
/*  xxMap   Location chooser using Cylindrical Equidistant projection.

    There is some duplicated code between xxGlobe and xxMap.  However,
    they are sufficiently different that I think complete
    encapsulation is the cleanest approach.  -- DWF

    Copyright (C) 2002  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


    Modified 2002-02-17
    Jan C. Depner
    Naval Oceanographic Office
    depnerj@navo.navy.mil

    Switched from globe to world map using cylindrical equidistant projection
    (that makes it sound like I actually know what I'm doing - it just
    means X/Y).  Added coastlines using the public domain WVS coastline files 
    from the NGDC Global Relief Data CD-ROM (I built those in 1989).  For 
    more information on the files see wvsrtv.cc.

*/

#include "xtide.hh"

void xxMapPointerMotionEventHandler (Widget w, XtPointer client_data,
XEvent *event, Boolean *continue_dispatch) 
{
    xxMap *map = (xxMap *) client_data;
    XMotionEvent *xme = (XMotionEvent *) event;

    // Coordinates relative to upper left corner of map pixmap.
    // Need to subtract the internal margin of the label widget.

    map->last_x = xme->x - map->internalWidth;
    map->last_y = xme->y - map->internalHeight;
    map->update_position (map->last_x, map->last_y);
}

int xxMap::translateCoordinates (double lat, double lon, int *x, int *y)
{
    double lng = lon;

    if (bounds[zoom_level].elon > 180.0 && lng < bounds[zoom_level].wlon)
        lng += 360.0;
    if (bounds[zoom_level].wlon < -180.0 && lng > bounds[zoom_level].elon)
        lng -= 360.0;

    // Allow a cushy margin so that lines won't get chopped.
    if (lat < bounds[zoom_level].slat - antichopmargin ||
        lat > bounds[zoom_level].nlat + antichopmargin ||
        lng < bounds[zoom_level].wlon - antichopmargin ||
        lng > bounds[zoom_level].elon + antichopmargin)
        return (0);

    *x = internalWidth + (int) (((lng - bounds[zoom_level].wlon) / 
        (bounds[zoom_level].elon - bounds[zoom_level].wlon)) * x_map_size);
    *y = internalHeight + (int) (((bounds[zoom_level].nlat - 
        lat) / (bounds[zoom_level].nlat - bounds[zoom_level].slat)) * 
        y_map_size);

    return (1);
}



void xxMap::untranslateCoordinates (double *lat, double *lon, int x, int y)
{
    *lon = bounds[zoom_level].wlon + ((double) (x - internalWidth) / 
        (double) (x_map_size - internalWidth)) * (bounds[zoom_level].elon - 
        bounds[zoom_level].wlon);
    *lat = bounds[zoom_level].nlat - ((double) (y - internalHeight) / 
        (double) (y_map_size - internalHeight)) * (bounds[zoom_level].nlat - 
        bounds[zoom_level].slat);
}



void xxMap::update_position (int rx, int ry)
{
    char   string[50];
    double lat, lon;


    untranslateCoordinates (&lat, &lon, rx, ry);


    if (lat < -90.0) lat = -90.0;
    if (lat > 90.0) lat = 90.0;

    if (lon < bounds[zoom_level].wlon) 
        lon = bounds[zoom_level].wlon;
    if (lon > bounds[zoom_level].elon) 
        lon = bounds[zoom_level].elon;

    if (lon < -180.0) lon += 360.0;
    if (lon > 180.0) lon -= 360.0;

    sprintf (string, "Lat %8.4f", lat);

    Arg latargs[1] = 
      {
        {XtNlabel, (XtArgVal)string}
      };
    XtSetValues (lattext->manager, latargs, 1);


    sprintf (string, "Lon %9.4f", lon);

    Arg lonargs[1] = 
      {
        {XtNlabel, (XtArgVal)string}
      };
    XtSetValues (lontext->manager, lonargs, 1);
}



void xxMap::redrawMap () 
{
    float          *latray, *lonray;
    int            *segray, coast;
    static char    files[6][12] = {"wvsfull.dat", "wvs250k.dat", "wvs1.dat", 
                                   "wvs3.dat", "wvs12.dat", "wvs43.dat"};
    static Pixmap  savemap;
    Cursor         cursor;


    int wvsrtv (char *, int, int, float **, float **, int **);


    cursor = XCreateFontCursor (picture->display, XC_watch);
    XDefineCursor (picture->display, XtWindow (picture->manager), cursor);


    XFillRectangle (picture->display, mappixmap, picture->background_gc, 
        0, 0, x_map_size + 1, y_map_size + 1);


    int startlat = (int) (bounds[zoom_level].slat - 1.0);
    int endlat = (int) (bounds[zoom_level].nlat + 1.0);
    int startlon = (int) (bounds[zoom_level].wlon - 1.0);
    int endlon = (int) (bounds[zoom_level].elon + 1.0);


    latray = (float *) NULL;
    lonray = (float *) NULL;
    segray = (int *) NULL;


    int spot, halfspot, lat, lon, segx[2], segy[2];


    /*  Get the proper coastline data set and spot size based on the zoom 
        level.  */

    switch (zoom_level)
      {
        case 0:
        default:
            coast = 5;
            spot = 3;
            halfspot = 1;
            break;

        case 1:
            coast = 4;
            spot = 5;
            halfspot = 2;
            break;

        case 2:
            coast = 4;
            spot = 6;
            halfspot = 3;
            break;

        case 3:
            coast = 2;
            spot = 6;
            halfspot = 3;
            break;

        case 4:
            coast = 2;
            spot = 6;
            halfspot = 3;
            break;

        case 5:
            coast = 1;
            spot = 7;
            halfspot = 3;
            break;

        case 6:
            coast = 1;
            spot = 7;
            halfspot = 3;
            break;

        case 7:
            coast = 0;
            spot = 8;
            halfspot = 4;
            break;

        case 8:
            coast = 0;
            spot = 8;
            halfspot = 4;
            break;
    }


    /*  Read and plot the coastlines.  */

    if (redraw_zero_level || zoom_level)
      {
        for (lat = startlat ; lat < endlat ; lat++)
          {
            for (lon = startlon ; lon < endlon ; lon++)
              {
                int k, nseg, offset;

                offset = 0;

                nseg = wvsrtv (files[coast], lat, lon, &latray, &lonray, 
                    &segray);

                if (nseg) 
                  {
                    /*  Get rid of single point islands that were required for 
                        NAVO.  These were created during the decimation
                        process.  */

                    if (nseg > 2 || latray[0] != latray[1] || 
                        lonray[0] != lonray[1])
                      {
                        for (k = 0 ; k < nseg ; k++)
                          {
                            int cnt, m;

                            m = 0;
                            for (cnt = 0 ; cnt < segray[k] ; cnt++)
                              {
                                if (translateCoordinates (
                                    (double) latray[offset + cnt], 
                                    (double) lonray[offset + cnt], &segx[m], 
                                    &segy[m]))
                                  {
                                    /*  Check for the weird situation when
                                        west and east are at 0.0 and 360.0.  */

                                    if (m && segx[1] - segx[0] < 50 && 
                                        segx[0] - segx[1] < 50)
                                      {
                                        XDrawLine (picture->display, 
                                            mappixmap, picture->text_gc, 
                                            segx[0], segy[0], segx[1], 
                                            segy[1]);


                                        segx[0] = segx[1];
                                        segy[0] = segy[1];
                                      }
                                    m = 1;
                                  }
                                else
                                  {
                                    m = 0;
                                  }
                              }
                            offset += segray[k];
                          }
                      }
                  }
              }
          }

        wvsrtv ("clean", 0, 0, &latray, &lonray, &segray);
      }
    else
      {
        XCopyArea (picture->display, savemap, mappixmap, picture->mark_gc, 
            0, 0, x_map_size, y_map_size, 0, 0);
      }



    /*  Draw the grid lines.  */

    for (lon = startlon ; lon < endlon ; lon++)
      {
        if (!(lon % 30) && lon > bounds[zoom_level].wlon)
          {
            if (translateCoordinates (bounds[zoom_level].slat, (double) lon, 
                &segx[0], &segy[0]))
              {
                translateCoordinates (bounds[zoom_level].nlat, (double) lon, 
                    &segx[1], &segy[1]);

                XDrawLine (picture->display, mappixmap, picture->text_gc, 
                    segx[0], segy[0], segx[1], segy[1]);
              }
          }
      }
       

    for (lat = startlat ; lat < endlat ; lat++)
      {
        if (!(lat % 30) && lat > -90 && lat < 90)
          {
            if (translateCoordinates ((double) lat, bounds[zoom_level].wlon,
                &segx[0], &segy[0]))
              {
                translateCoordinates ((double) lat, bounds[zoom_level].elon,
                    &segx[1], &segy[1]);

                XDrawLine (picture->display, mappixmap, picture->text_gc, 
                    segx[0], segy[0], segx[1], segy[1]);
              }
          }
      }


    /*  The first time through we want to save the map so we don't have to
        read the file to regen it.  */

    if (redraw_zero_level)
      {
        savemap = mypopup->makePixmap (x_map_size + 1, y_map_size + 1);
        XCopyArea (picture->display, mappixmap, savemap, picture->mark_gc, 
            0, 0, x_map_size, y_map_size, 0, 0);
        redraw_zero_level = 0;
      }


    /*  Plot the stations.  */

    unsigned long i;
    StationIndex *si = new StationIndex();
    for (i = 0 ; i < stationIndex->length() ; i++) 
      {
        int x, y;
        if (translateCoordinates ((*stationIndex)[i]->coordinates.lat(),
            (*stationIndex)[i]->coordinates.lng(), &x, &y))
          {
            // Here, we *do* want to eliminate everything not strictly in
            // the window, modulo the size of the dots.
            if (x >= -halfspot && y >= -halfspot && x <= x_map_size+halfspot && y <= y_map_size+halfspot) {
              XFillArc (picture->display, mappixmap, picture->mark_gc,
                 x - halfspot, y - halfspot, spot, spot, 0, 360*64);
              si->add ((*stationIndex)[i]);
            }
          }
      }


    update_position (last_x, last_y);


    Arg args[1] = 
      {
        {"bitmap", (XtArgVal)mappixmap}
      };
    XtSetValues (picture->manager, args, 1);
    picture->refresh();
    blastflag = 0;
    locationlist->changeList (si);


    XUndefineCursor (picture->display, XtWindow (picture->manager));
    XFreeCursor (picture->display, cursor);
}


static void dismissCallback (Widget w, XtPointer client_data, 
XtPointer call_data) 
{
    xxMap *map = (xxMap *)client_data;
    map->dismiss();
}


void
xxMapRoundCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxMap *map = (xxMap *)client_data;
  map->xtidecontext->root->newGlobe ();
  map->dismiss();
}


void xxMapHelpCallback (Widget w, XtPointer client_data, 
XtPointer call_data) 
{
  xxMap *map = (xxMap *)client_data;
  Dstr helpstring ("\
XTide Location Chooser -- Map Window\n\
\n\
The map window initially shows the entire world (cylindrical equidistant\n\
projection).  Tide stations are projected onto the map as red dots and\n\
enumerated in the location list window.\n\
\n\
Mouse buttons:\n\
\n\
  Left:  zoom in on the clicked region and narrow the location list.\n\
  You can zoom in 8 times for a maximum 512X magnification (first zoom\n\
  is times 4), after which the left mouse button behaves just like the\n\
  right mouse button.\n\
\n\
  Right:  narrow the location list to the clicked area.  A circle will\n\
  be drawn on the map showing the radius included, but no zooming\n\
  will occur.\n\
\n\
  Middle:  select a tide station.  (You can also select a tide station\n\
  by left-clicking on it in the location list.)\n\
\n\
Buttons in the map window:\n\
\n\
  List All:  include all available tide stations in the location list,\n\
  even those whose latitude and longitude are unknown (null).\n\
\n\
  Zoom Out:  self-explanatory.  Sufficient usage will return to the\n\
  world map view.\n\
\n\
  Round:  change to a spherical globe projection.\n\
\n\
  Dismiss:  remove the location chooser.  Any windows containing tide\n\
  predictions will remain.\n\
\n\
Keyboard:\n\
\n\
  Arrow keys:  pan up/down/left/right.");
  (void) map->xtidecontext->root->newHelpBox (helpstring);
}



void xxMapListAllCallback (Widget w, XtPointer client_data, XtPointer call_data) 
{
    xxMap *map = (xxMap *)client_data;
    map->locationlist->changeList (map->stationIndex->clone());
}



void xxMapZoomOutCallback (Widget w, XtPointer client_data, XtPointer call_data) 
{
    xxMap *map = (xxMap *)client_data;

    if (!map->zoom_level) return;

    map->zoom_level--;

    map->redrawMap();
}



// This is done by the flat window image and not by spherical projection.
// It's simpler this way.

void xxMap::blast (int x, int y) 
{
    if (blastflag)
        XDrawArc (picture->display, mappixmap, picture->invert_gc, blastx,
        blasty, blastradius*2, blastradius*2, 0, 360*64);

    blastflag = 1;
    blastx = x - blastradius;
    blasty = y - blastradius;
    XDrawArc (picture->display, mappixmap, picture->invert_gc, blastx,
        blasty, blastradius*2, blastradius*2, 0, 360*64);

    Arg args[1] = 
      {
        {"bitmap", (XtArgVal)mappixmap}
      };
    XtSetValues (picture->manager, args, 1);
    picture->refresh();


    // Make a list of all locations within the blast radius.

    unsigned long a;
    StationIndex *si = new StationIndex();
    for (a = 0 ; a < stationIndex->length() ; a++) 
      {
        int bx, by;
        if (translateCoordinates ((*(stationIndex))[a]->coordinates.lat(),
            (*(stationIndex))[a]->coordinates.lng(), &bx, &by)) 
          {
            double dx = (double) bx - (double) x;
            double dy = (double) by - (double) y;
            if (sqrt (dx * dx + dy * dy) <= blastradius)
                si->add ((*(stationIndex))[a]);
          }
      }
    locationlist->changeList (si);
}



void xxMapKeyboardEventHandler (Widget w, XtPointer client_data,
XEvent *event, Boolean *continue_dispatch) 
{
    xxMap *map = (xxMap *) client_data;
    XKeyEvent *xke = (XKeyEvent *) event;
    KeySym foo = XLookupKeysym (xke, 0); // Index 0 = no shift/ctrl/meta/etc.


    double xsize = (map->bounds[map->zoom_level].elon - 
        map->bounds[map->zoom_level].wlon);
    double ysize = (map->bounds[map->zoom_level].nlat - 
        map->bounds[map->zoom_level].slat);


    /*  Notice the 5% overlap.  */

    switch (foo) 
      {
        case XK_Left:
        case XK_KP_Left:
            if (map->zoom_level)
              {
                map->bounds[map->zoom_level].wlon = 
                    map->bounds[map->zoom_level].wlon - xsize * 0.95;
                map->bounds[map->zoom_level].elon = 
                    map->bounds[map->zoom_level].elon - xsize * 0.95;

                if (map->bounds[map->zoom_level].wlon < -180.0)
                  {
                    map->bounds[map->zoom_level].wlon += 360.0;
                    map->bounds[map->zoom_level].elon = 
                        map->bounds[map->zoom_level].wlon + xsize;
                  }
              }
            else
              {
                map->center_longitude -= 30.0;
                if (map->center_longitude < -180.0)
                    map->center_longitude += 360.0;
                map->bounds[0].wlon = map->center_longitude - 180.0;
                map->bounds[0].elon = map->center_longitude + 180.0;

                if (map->bounds[0].wlon < -180.0) 
                    map->bounds[0].wlon += 360.0;
                if (map->bounds[0].elon > 180.0) 
                    map->bounds[0].elon -= 360.0;

                if (map->bounds[0].wlon >= map->bounds[0].elon) 
                    map->bounds[0].elon += 360.0;

                map->redraw_zero_level = 1;
              }
            break;

        case XK_Up:
        case XK_KP_Up:
            if (map->zoom_level)
              {
                map->bounds[map->zoom_level].slat = 
                    map->bounds[map->zoom_level].slat + ysize * 0.95;
                map->bounds[map->zoom_level].nlat = 
                    map->bounds[map->zoom_level].nlat + ysize * 0.95;

                if (map->bounds[map->zoom_level].nlat > 90.0)
                  {
                    map->bounds[map->zoom_level].nlat = 90.0;
                    map->bounds[map->zoom_level].slat = 
                        map->bounds[map->zoom_level].nlat - ysize;
                  }
              }
            break;

        case XK_Right:
        case XK_KP_Right:
            if (map->zoom_level)
              {
                map->bounds[map->zoom_level].wlon = 
                    map->bounds[map->zoom_level].wlon + xsize * 0.95;
                map->bounds[map->zoom_level].elon = 
                    map->bounds[map->zoom_level].elon + xsize * 0.95;

                if (map->bounds[map->zoom_level].elon > 180.0)
                  {
                    map->bounds[map->zoom_level].elon -= 360.0;
                    map->bounds[map->zoom_level].wlon = 
                        map->bounds[map->zoom_level].elon - xsize;
                  }
              }
            else
              {
                map->center_longitude += 30.0;
                if (map->center_longitude > 180.0)
                    map->center_longitude -= 360.0;
                map->bounds[0].wlon = map->center_longitude - 180.0;
                map->bounds[0].elon = map->center_longitude + 180.0;

                if (map->bounds[0].wlon < -180.0) 
                    map->bounds[0].wlon += 360.0;
                if (map->bounds[0].elon > 180.0) 
                    map->bounds[0].elon -= 360.0;

                if (map->bounds[0].wlon >= map->bounds[0].elon) 
                    map->bounds[0].elon += 360.0;

                map->redraw_zero_level = 1;
              }
            break;

        case XK_Down:
        case XK_KP_Down:
            if (map->zoom_level)
              {
                map->bounds[map->zoom_level].slat = 
                    map->bounds[map->zoom_level].slat - ysize * 0.95;
                map->bounds[map->zoom_level].nlat = 
                    map->bounds[map->zoom_level].nlat - ysize * 0.95;

                if (map->bounds[map->zoom_level].slat < -90.0)
                  {
                    map->bounds[map->zoom_level].slat = -90.0;
                    map->bounds[map->zoom_level].nlat = 
                        map->bounds[map->zoom_level].slat + ysize;
                  }
              }
            break;

        default:
            return;
      }

    map->redrawMap();
}



void xxMapButtonPressEventHandler (Widget w, XtPointer client_data,
XEvent *event, Boolean *continue_dispatch) 
{
    xxMap *map = (xxMap *) client_data;
    XButtonEvent *xbe = (XButtonEvent *) event;


    /*  Only perform the action on ButtonPress not ButtonRelease.  */

    if (xbe->type == ButtonRelease) return;


    // Coordinates relative to upper left corner of map pixmap.
    // Need to subtract the internal margin of the label widget.

    int rx = xbe->x - map->internalWidth;
    int ry = xbe->y - map->internalHeight;


    if (xbe->button == Button3 || (xbe->button == Button1 && 
        map->zoom_level == max_zoom_level)) 
      {
        map->blast (rx, ry);
        return;
      }


    // Zoom

    if (xbe->button == Button1) 
      {
        if (map->zoom_level < max_zoom_level)
          {
            double lat, lon;

            map->untranslateCoordinates (&lat, &lon, xbe->x, xbe->y);

            double xsize = (map->bounds[map->zoom_level].elon - 
                map->bounds[map->zoom_level].wlon) / 2.0;
            double ysize = (map->bounds[map->zoom_level].nlat - 
                map->bounds[map->zoom_level].slat) / 2.0;


            /*  From level zero to level one I want to zoom in four times
                instead of two.  */

            if (!map->zoom_level)
              {
                xsize /= 2.0;
                ysize /= 2.0;
              }


            /*  Increment the zoom level and compute the new bounds.  */

            map->zoom_level++;


            map->bounds[map->zoom_level].wlon = lon - xsize / 2.0;
            map->bounds[map->zoom_level].elon = lon + xsize / 2.0;

            if (map->bounds[map->zoom_level].wlon < -180.0)
                map->bounds[map->zoom_level].wlon += 360.0;

            if (map->bounds[map->zoom_level].elon > 180.0)
                map->bounds[map->zoom_level].elon -= 360;

            if (map->bounds[map->zoom_level].wlon >= 
                map->bounds[map->zoom_level].elon) 
                map->bounds[map->zoom_level].elon += 360.0;


            map->bounds[map->zoom_level].slat = lat - ysize / 2.0;
            map->bounds[map->zoom_level].nlat = lat + ysize / 2.0;

            if (map->bounds[map->zoom_level].slat < -90.0)
              {
                map->bounds[map->zoom_level].slat = -90.0;
                map->bounds[map->zoom_level].nlat = 
                    map->bounds[map->zoom_level].slat + ysize;
              }

            if (map->bounds[map->zoom_level].nlat > 90.0)
              {
                map->bounds[map->zoom_level].nlat = 90.0;
                map->bounds[map->zoom_level].slat = 
                    map->bounds[map->zoom_level].nlat - ysize;
              }
          }

        map->redrawMap();
        return;
      }


    // Load location

    if (xbe->button == Button2) 
      {
        // Find nearest location that is close enough (4 pixels).

        unsigned long a;
        StationRef *closestsr = NULL;
        double d = 17.0;
        for (a = 0 ; a < map->stationIndex->length() ; a++) 
          {
            int x, y;
            if (map->translateCoordinates (
                (*(map->stationIndex))[a]->coordinates.lat(),
                (*(map->stationIndex))[a]->coordinates.lng(), &x, &y))
              {
                double dx = (double) x - (double) rx;
                double dy = (double) y - (double) ry;
                double dd = dx * dx + dy * dy;
                if (dd < d) 
                  {
                    d = dd;
                    closestsr = (*(map->stationIndex))[a];
                  }
              }
          }
        if (closestsr) map->xtidecontext->root->newGraph (closestsr);
      }

    // X11 has Button4 and Button5... go figure.
}



void xxMap::dismiss () 
{
  delete this;
}



xxMap::xxMap (xxContext *context, StationIndex &in_stationIndex,
xxTideContext *in_tidecontext): xxWindow (in_tidecontext, context, 1) 
{
    mypopup->setTitle ("Map");

    blastflag = 0;
    stationIndex = &in_stationIndex;

    mappixmap = mypopup->makePixmap (x_map_size + 1, y_map_size + 1);
    Arg args[3] = 
      {
        {XtNbitmap, (XtArgVal)mappixmap},
        {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
        {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
      };

    Widget picturewidget = XtCreateManagedWidget ("", labelWidgetClass,
        container->manager, args, 3);
    picture = new xxContext (container, picturewidget);
    XtAddEventHandler (picturewidget, PointerMotionMask, False,
        xxMapPointerMotionEventHandler, (XtPointer) this);
    XtAddEventHandler (picturewidget, ButtonPressMask, False,
        xxMapButtonPressEventHandler, (XtPointer) this);
    XtAddEventHandler (picturewidget, KeyPressMask, False,
        xxMapKeyboardEventHandler, (XtPointer) this);
      {
        Arg args[2] = 
          {
            {XtNinternalHeight, (XtArgVal)(&internalHeight)},
            {XtNinternalWidth, (XtArgVal)(&internalWidth)}
          };
        XtGetValues (picture->manager, args, 2);

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Map internal height " << internalHeight <<
      " internal width " << internalWidth << endl;
#endif
      }

    Arg buttonargs[2] =  
      {
        {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
        {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
      };
      {
        Widget buttonwidget = XtCreateManagedWidget ("List All",
            commandWidgetClass, container->manager, buttonargs, 2);
        XtAddCallback (buttonwidget, XtNcallback, xxMapListAllCallback,
            (XtPointer)this);
        listAllButton = new xxContext (mypopup, buttonwidget);
      }
      {
        Widget buttonwidget = XtCreateManagedWidget ("Zoom Out",
            commandWidgetClass, container->manager, buttonargs, 2);
        XtAddCallback (buttonwidget, XtNcallback, xxMapZoomOutCallback,
            (XtPointer)this);
        zoomoutbutton = new xxContext (mypopup, buttonwidget);
      }
      {
        Widget buttonwidget = XtCreateManagedWidget ("Round",
            commandWidgetClass, container->manager, buttonargs, 2);
        XtAddCallback (buttonwidget, XtNcallback, xxMapRoundCallback,
            (XtPointer)this);
        roundbutton = new xxContext (mypopup, buttonwidget);
      }
      {
        Widget buttonwidget = XtCreateManagedWidget ("Dismiss", 
            commandWidgetClass, container->manager, buttonargs, 2);
        XtAddCallback (buttonwidget, XtNcallback, dismissCallback,
            (XtPointer)this);
        dismissbutton = new xxContext (mypopup, buttonwidget);
      }
      {
        Widget buttonwidget = XtCreateManagedWidget ("?", commandWidgetClass,
            container->manager, buttonargs, 2);
        XtAddCallback (buttonwidget, XtNcallback, xxMapHelpCallback,
            (XtPointer)this);
        helpbutton = new xxContext (mypopup, buttonwidget);
      }
      {
        Arg latargs[2] =
          {
            {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
            {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
          };
        Widget lattextwidget = XtCreateManagedWidget ("Lat -00.0000",
          labelWidgetClass, container->manager, latargs, 2);
        lattext = new xxContext (mypopup, lattextwidget);
      }
      {
        Arg lonargs[2] =
          {
            {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
            {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
          };
        Widget lontextwidget = XtCreateManagedWidget ("Lon -000.0000",
          labelWidgetClass, container->manager, lonargs, 2);
        lontext = new xxContext (mypopup, lontextwidget);
      }

    mypopup->realize();
    mypopup->fixSize();


    zoom_level = 0;
    bounds[0].slat = -90.0;
    bounds[0].nlat = 90.0;

    Configurable &gl = (*(in_tidecontext->settings))["gl"];
    if (gl.isnull || gl.d == 360.0)
      center_longitude = stationIndex->bestCenterLongitude();
    else
      center_longitude = gl.d;

    bounds[0].wlon = center_longitude - 180.0;
    bounds[0].elon = center_longitude + 180.0;

    if (bounds[0].wlon < -180.0) bounds[0].wlon += 360.0;
    if (bounds[0].elon > 180.0) bounds[0].elon -= 360.0;

    if (bounds[0].wlon >= bounds[0].elon) bounds[0].elon += 360.0;

    redraw_zero_level = 1;


    // Start with an empty list since it's going to get clobbered in
    // redrawMap anyway.

    locationlist = new xxLocationList (mypopup, new StationIndex(),
        xtidecontext, this);
    redrawMap();
}

xxMap::~xxMap () {
  mypopup->unrealize();
  delete locationlist;
  delete roundbutton;
  delete dismissbutton;
  delete helpbutton;
  delete listAllButton;
  delete picture;
  delete lattext;
  delete lontext;
  XFreePixmap (mypopup->display, mappixmap);
}

void xxMap::global_redraw() {
  xxWindow::global_redraw();
  redrawMap();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (roundbutton)
    XtSetValues (roundbutton->manager, buttonargs, 2);
  if (dismissbutton)
    XtSetValues (dismissbutton->manager, buttonargs, 2);
  if (listAllButton)
    XtSetValues (listAllButton->manager, buttonargs, 2);
  if (helpbutton)
    XtSetValues (helpbutton->manager, buttonargs, 2);
  if (zoomoutbutton)
    XtSetValues (zoomoutbutton->manager, buttonargs, 2);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (lattext)
    XtSetValues (lattext->manager, args, 2);
  if (lontext)
    XtSetValues (lontext->manager, args, 2);
  if (picture)
    XtSetValues (picture->manager, args, 2);
}
