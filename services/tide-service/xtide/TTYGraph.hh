// $Id: TTYGraph.hh,v 1.2 2004/04/07 19:56:31 flaterco Exp $
/*  TTYGraph  Graph implemented on dumb terminal

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class TTYGraph: public Graph {
public:
  TTYGraph (unsigned xsize, unsigned ysize);
  virtual ~TTYGraph();

  void writeAsText (Dstr &text_out);

protected:
  unsigned fontWidth();
  unsigned fontHeight();
  unsigned fontMargin();
  unsigned hourTickLen();
  unsigned depthLabelLeftMargin();
  unsigned depthLabelRightMargin();
  unsigned depthLineVerticalMargin();
  void drawString (int x, int y, const Dstr &s);
  virtual double aspectfudge();
  virtual unsigned startpos();

  // Override only where we don't want asterisks.
  virtual void drawHorizontalLine (int xlo, int xhi, int y,
				   Colors::colorchoice c);
  virtual void drawHourTick (int x, Colors::colorchoice c);

  void clearGraph (Timestamp start, Timestamp endt, Interval increment,
    Station *station, TideEventsOrganizer &organizer);

  void drawX (int x, double y);

  char *tty;

  // Asterisks away.
  void setPixel (int x, int y, Colors::colorchoice c);
  // For the asterisk deprived.
  void setPixel (int x, int y, char c);
  // Just to shut up the stupid compiler warning.
  void setPixel (int x, int y, Colors::colorchoice c, double saturation);
};
