/*  xml.l  Flex file for limited XML parser (just tags, no text).
    Last modified 1997-12-23

    This is *NOT* a general-purpose XML parser.

    All things beginning with <! are treated as comments which are
    terminated by the next > character.  This means that you can't do
    the following (shown in the XML FAQ):

      <!DOCTYPE foo [
        <!ENTITY alephhb cdata "&#224;">
      ]>

    However, it should be possible to add a simple DOCTYPE declaration
    at such a time that I have a DTD:

      <!DOCTYPE xtide SYSTEM "xtide.dtd">

    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

%{

#include "common.hh"

#include "xml_y.hh"

int lineno = 1;

%}

%%

\n	lineno++;

[ \t\r]+	;

"<?"[^\n]*"?>"	{ return XMLSTART; }

"<!"[^>]*">"	{ // Count lines consumed
                  for (unsigned a=0; a<strlen(xmltext); a++)
                    if (xmltext[a] == '\n')
                      lineno++; }

\"[^"\n]*\"	{ // Remove delimiting quotes
                  xmllval.value = new Dstr (xmltext+1);
                  (*xmllval.value) -= (*xmllval.value).length() - 1;
                  return ATTVALUE; }

"="	{ return '='; }

"/"	{ return '/'; }

">"	{ return '>'; }

"<"	{ return '<'; }

[a-zA-Z0-9:_]*	{ xmllval.value = new Dstr (xmltext);
                  return NAME; }

%%

int
xmlwrap() {
  return 1;
}

void
xmlerror (const char *s) {
  Dstr details ("Syntax error in XML file ");
  details += xmlfilename;
  details += " at line ";
  details += lineno;
  barf (XMLPARSE, details);
}
