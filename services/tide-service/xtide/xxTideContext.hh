// $Id: xxTideContext.hh,v 1.2 2004/04/16 21:45:32 flaterco Exp $
/*  xxTideContext  Specialization of TideContext.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxXTideRoot;

class xxTideContext: public TideContext {
public:
  xxTideContext (xxContext *context, xxXTideRoot *in_root, Colors *in_colors,
    Settings *in_settings);
  // The grabkind here is for the title screen if one is needed.
  StationIndex *stationIndex(XtGrabKind in_grabkind = XtGrabExclusive);
  xxXTideRoot *root;
protected:
  xxContext *xxcontext;
  void doHarmonicsFile (Dstr &fname, xxTitleScreen &titleScreen);
};
