// $Id: xxContext.cc,v 1.8 2004/11/07 22:23:01 flaterco Exp $
/*  xxContext    X-Toolkit-on-a-steek.  Packaging of assorted XCrap.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"
#include "crab48.xpm.hh"

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING

// This is stupid -- setbase(2) doesn't work on streams and there's no
// print format for binary!

static Dstr &
base2 (unsigned long a) {
  static Dstr temp;
  temp = "";
  int numbits = sizeof(unsigned long) * 8;
  unsigned long highbit = 1 << numbits-1;
  while (highbit) {
    if (a & highbit)
      temp += '1';
    else
      temp += '0';
    highbit >>= 1;
  }
  return temp;
}

static Dstr &
base2 (unsigned short a) {
  static Dstr temp;
  temp = "";
  int numbits = sizeof(unsigned short) * 8;
  unsigned short highbit = 1 << numbits-1;
  while (highbit) {
    if (a & highbit)
      temp += '1';
    else
      temp += '0';
    highbit >>= 1;
  }
  return temp;
}
#endif

// This converts the XVisualInfo masks to what we want.
void xxContext::convert_mask (unsigned long vinfomask, unsigned short &mask,
			      int &shift) {

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Called xxContext::convert_mask" << endl;
  cerr << "  Vinfo mask in: " << base2(vinfomask) << endl;
#endif

  assert (vinfomask);

  // It's probably wasted effort to make this work with 64-bit longs and
  // 32-bit shorts since XColor clearly defines color values as 16 bits.
  // However....
  int numbits = sizeof(unsigned short) * 8;
  unsigned short highbit = 1 << numbits-1;
  unsigned short allbits = 0;
  unsigned short abit = highbit;
  while (abit) {
    allbits |= abit;
    abit >>= 1;
  }

  shift = 0;
  if (vinfomask > allbits) {
    // Will need to shift left.
    while (vinfomask > allbits) {
      shift++;
      vinfomask >>= 1;
    }
  } else if (!(vinfomask & highbit)) {
    // Will need to shift right.
    while (!(vinfomask & highbit)) {
      shift--;
      vinfomask <<= 1;
    }
  }

  assert (vinfomask);
  assert (vinfomask <= allbits);
  mask = (unsigned short)vinfomask;

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "  Mask out: " << base2(mask) << endl;
  cerr << "  Shift value: " << shift << endl;
#endif
}

// This function and static variable are used in the constructor below
// to pass to Settings::applyXResources.

static XrmDatabase database;
int getResource (Dstr &name_in, Dstr &val_out) {
  Dstr n1 ("xtide*"), n2 ("XTide*");
  n1 += name_in;
  n2 += name_in;
  XrmValue xv;
  char *str_type[20]; // Waste.
  if (XrmGetResource (database, n1.aschar(), n2.aschar(), str_type, &xv)) {
    val_out = (char *)xv.addr;
    return 1;
  }
  return 0;
}

#ifdef DUALHEADDEBUG
// Straight out of O'Reilly volume 1, p. 202.
static char *visual_class[] = {
  "StaticGray", "GrayScale", "StaticColor", "PseudoColor",
  "TrueColor", "DirectColor"};
#endif

xxContext::xxContext (int &argc, char **argv, Colors *&colors, Settings *&settings) {
  XVisualInfo vinfo;
  is_realized = 0;
  is_popup = 0;

  // See if we can get a nice true color visual (code fragment 11-5,
  // X Toolkit Cookbook).  Note that some idiots will set the default
  // to 8 plane PseudoColor even when 24 bit color is supported, so
  // you have to explicitly ask for a TrueColor visual to get it.
  {
    XtToolkitInitialize();
    context = XtCreateApplicationContext();

    // Special handling of -geometry.
    // Concatenation of the argument is not supported here.
    // It's not supported by standard X11 programs either.
    {
      for (int a=0; a<argc; a++)
        if (!strncmp (argv[a], "-ge", 3) || !strcmp (argv[a], "-g"))
          strcpy (argv[a], "-X");
    }

    display = XtOpenDisplay (context, NULL, "XTide", "XTide",
      NULL, 0, &argc, argv);
    if (!display)
      barf (CANTOPENDISPLAY);

#ifdef DUALHEADDEBUG
    fprintf (stderr, "DefaultScreen: %d\n", DefaultScreen(display));
#endif

    // Find best available TrueColor visual
    depth = 0;
    {
      XVisualInfo vinfo_template;
      vinfo_template.c_class = 4;
      vinfo_template.screen = DefaultScreen(display);
      int nitems;
      XVisualInfo *vinfo_out = XGetVisualInfo (display,
        VisualClassMask|VisualScreenMask, &vinfo_template, &nitems);
#ifdef DUALHEADDEBUG
      fprintf (stderr, "TrueColor visuals returned by XGetVisualInfo:\n");
#endif
      if (nitems <= 0)
#ifdef DUALHEADDEBUG
        fprintf (stderr, "  None!\n");
#else
        ;
#endif
      else {
        for (int l = 0; l < nitems; l++) {
#ifdef DUALHEADDEBUG
          fprintf (stderr, "  l = %d\n", l);
          fprintf (stderr, "    visualid = %lu\n", vinfo_out[l].visualid);
          fprintf (stderr, "    screen = %d\n", vinfo_out[l].screen);
          fprintf (stderr, "    depth = %u\n", vinfo_out[l].depth);
          fprintf (stderr, "    class = %s\n", visual_class[vinfo_out[l].c_class]);
          fprintf (stderr, "      redmask = %6lx\n", vinfo_out[l].red_mask);
          fprintf (stderr, "    greenmask = %6lx\n", vinfo_out[l].green_mask);
          fprintf (stderr, "     bluemask = %6lx\n", vinfo_out[l].blue_mask);
          fprintf (stderr, "    colormap size = %d\n", vinfo_out[l].colormap_size);
          fprintf (stderr, "    bits per rgb = %d\n", vinfo_out[l].bits_per_rgb);
#endif
          if (vinfo_out[l].depth < 15) {
#ifdef DUALHEADDEBUG
            fprintf (stderr, "    ^^^ Depth too low\n");
#else
            ;
#endif
          } else if ((unsigned)vinfo_out[l].depth > depth) {
#ifdef DUALHEADDEBUG
            fprintf (stderr, "    ^^^ Selected\n");
#endif
            depth = vinfo_out[l].depth;
            vinfo = vinfo_out[l];
          }
        }
        XFree (vinfo_out);
      }
    }

    // Undocumented switch for testing default visual
    int forcedefault = 0;
    if (argc == 2)
      if (!strcmp (argv[1], "-suck"))
        forcedefault = 1;

    if ((depth) && (forcedefault == 0)) {
      convert_mask (vinfo.red_mask, red_mask, red_shift);
      convert_mask (vinfo.green_mask, green_mask, green_shift);
      convert_mask (vinfo.blue_mask, blue_mask, blue_shift);

      display_sucks = 0;
      visual = vinfo.visual;
      assert (colormap = XCreateColormap (display,
  	DefaultRootWindow (display), visual, AllocNone));
      Arg args[5] = {
	{XtNdepth, (XtArgVal)depth},
	{XtNvisual, (XtArgVal)visual},
	{XtNcolormap, (XtArgVal)colormap},
	{XtNborderColor, (XtArgVal)0},
	{XtNbackground, (XtArgVal)0}
      };
      manager = XtAppCreateShell ("XTide", "XTide",
	applicationShellWidgetClass, display, args, 5);
    } else {
      display_sucks = 1;
      manager = XtAppCreateShell ("XTide", "XTide",
        applicationShellWidgetClass, display, NULL, 0);
      Arg args[3] = {
        {XtNcolormap, (XtArgVal)(&colormap)},
        {XtNvisual, (XtArgVal)(&visual)},
        {XtNdepth, (XtArgVal)(&depth)}
      };
      XtGetValues (manager, args, 3);
    }
  }

  if (!visual)
    visual = DefaultVisual (display, XScreenNumberOfScreen (XtScreen (manager)));

#ifdef DUALHEADDEBUG
  fprintf (stderr, "XTide display:  %d-bit %s\n", depth,
    visual_class[visual->c_class]);
#endif

  // Get settings.
  settings = new Settings();
  database = XtScreenDatabase (XtScreen (manager));
  settings->applyXResources (getResource);
  settings->applyUserDefaults();
  settings->applyCommandLine (argc, argv);

  // Get colors.
  colors = new Colors (*settings);
  pixels = new Pixel[numcolors];
  iconpixels = new Pixel[2]; // bg, fg
  {
    int a;
    for (a=0; a<numcolors; a++)
      pixels[a] = getColor ((*settings)[colorarg[a]].s.aschar());
  }

  // Get GCs.
  XGCValues gcv;
  gcv.foreground = pixels[Colors::foreground];
  text_gc = XtGetGC (manager, GCForeground, &gcv);
  gcv.foreground = pixels[Colors::mark];
  mark_gc = XtGetGC (manager, GCForeground, &gcv);
  gcv.foreground = pixels[Colors::background];
  background_gc = XtGetGC (manager, GCForeground, &gcv);

  gcv.function = GXinvert;
  invert_gc = XtGetGC (manager, GCFunction, &gcv);

  // This needs to be a non-shared GC so we can mess with it.  Kludge!
  {
    char temp[80];
    sprintf (temp, "rgb:%02x/%02x/%02x", rand()%256, rand()%256, rand()%256);
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Trying " << temp << " for spare_gc" << endl;
#endif
    gcv.foreground = getColor (temp);
    spare_gc = XtGetGC (manager, GCForeground, &gcv);
    while (spare_gc == text_gc ||
           spare_gc == mark_gc ||
           spare_gc == background_gc) {
      XtReleaseGC (manager, spare_gc);
      if (display_sucks)
        XFreeColors (display, colormap, &(gcv.foreground), 1, 0);
      sprintf (temp, "rgb:%02x/%02x/%02x", rand()%256, rand()%256, rand()%256);
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
      cerr << "Trying " << temp << " for spare_gc" << endl;
#endif
      gcv.foreground = getColor (temp);
      spare_gc = XtGetGC (manager, GCForeground, &gcv);
    }
  }

  // Get fonts.
  //
  // smallfont is critical, and is the same as has been used several years
  // in XTide 1 with no problems.  (It's also identical to the compiled-in
  // font used to generate PNGs.)  The others only need to be vaguely the
  // right size and shape.  Multiple problems have occurred related to
  // tightly-specified fonts not loading on different platforms.
  //
  // The original bigfont:
  // "-adobe-times-bold-r-normal--24-0-75-75-p-130-iso8859-1"
  // The original tinyfont:
  // "-schumacher-clean-medium-r-normal--10-0-100-100-c-57-iso646.1991-irv"
  //
  // The relaxed specs are (were) courtesy of Dean Pentcheff.
  //
  // --------------------
  //
  // Alan J. Wylie still had problems with tinyfont:
  //    "-schumacher-clean-medium-r-normal-*-10-*-*-*-*-*-iso646.1991-irv"
  // so I relaxed it even more.
  bigfont = XLoadFont (display,
    "-adobe-times-bold-r-normal-*-24-*-*-*-*-*-iso8859-1");
  smallfont = XLoadFont (display,
    "-misc-fixed-medium-r-normal--13-100-100-100-c-70-iso8859-1");
  tinyfont = XLoadFont (display,
    "-schumacher-clean-medium-r-normal-*-10-*-*-*-*-*-*-*");
  assert (bigfontstruct = XQueryFont (display, bigfont));
  assert (smallfontstruct = XQueryFont (display, smallfont));
  assert (tinyfontstruct = XQueryFont (display, tinyfont));
  {
    // The shell has no font attribute.  There is probably a better
    // way to do this.
    Widget labelwidget = XtCreateManagedWidget ("", labelWidgetClass,
      manager, NULL, 0);
    Arg fontargs[1] = {
      {XtNfont, (XtArgVal)(&defaultfontstruct)}
    };
    XtGetValues (labelwidget, fontargs, 1);
    assert (defaultfontstruct);
    XtDestroyWidget (labelwidget);
  }

  // Get screen size
  screen_width = DisplayWidth (display, DefaultScreen (display));
  screen_height = DisplayHeight (display, DefaultScreen (display));

  // Window manager close protocol.
  protocol_atom = XInternAtom (display, "WM_PROTOCOLS", False);
  kill_atom = XInternAtom (display, "WM_DELETE_WINDOW", False);

  // Initialize font.
  textSmallFont();

  // Icon sizes?
  // Try standardizing on 48x48 for now (this is what the CDE demanded).
  icon_size = 48;
#if 0
  XIconSize *szlist;
  int szcount;
  if (XGetIconSizes (display, RootWindow (display, DefaultScreen(display)),
  &szlist, &szcount)) {
    int a;
    unsigned s = 0;
    for (a=0; a<szcount; a++) {
      unsigned ns = min (szlist[a].max_width, szlist[a].max_height);
      if (ns > s && ns <= 64)
        s = ns;
    }
    XFree (szlist);
  }
#endif

  // Make icon pixmap.
  // Must use default visual and default colormap.
  dvisual = DefaultVisual (display, DefaultScreen (display));
  dcolormap = DefaultColormap (display, DefaultScreen (display));
  ddepth =  DefaultDepth (display, DefaultScreen (display));
  crab_pixmap = makePixmap (crab48, dvisual, dcolormap, ddepth);
  set_icon();
  // Get colors for clock icon.
  iconpixels[0] = getColor ((*settings)["dc"].s.aschar(),dvisual,dcolormap,1);
  iconpixels[1] = getColor ((*settings)["fg"].s.aschar(),dvisual,dcolormap,1);
  // Make GC for icon (must match visual and colormap).
  gcv.foreground = iconpixels[0];
  icon_gc = XCreateGC (display, icon_window, GCForeground, &gcv);
  XSetFont (display, icon_gc, tinyfont);
}

void xxContext::setTitle (const Dstr &title) {
  Dstr temp ("XTide: ");
  temp += title;
  Arg args[2] = {
    {XtNtitle, (XtArgVal)temp.aschar()},
    {XtNiconName, (XtArgVal)temp.aschar()}
  };
  XtSetValues (manager, args, 2);
}

void
xxContext::set_icon() {
  XSetWindowAttributes xswa;
  xswa.background_pixmap = crab_pixmap;
  int screen = DefaultScreen(display);
  icon_window = XCreateWindow (display, RootWindow (display, screen),
    0, 0, crab48w, crab48h, 0, DefaultDepth (display, screen),
    InputOutput, DefaultVisual (display, screen),
    CWBackPixmap, &xswa);
  Arg args[1] = {
    {XtNiconWindow, (XtArgVal)icon_window}
  };
  XtSetValues (manager, args, 1);
}

unsigned
xxContext::stringWidth (XFontStruct *fs, const char *s) {
  assert (fs);
  assert (s);
  int dir_return, asc_return, desc_return;
  XCharStruct ov_return;
  XTextExtents (fs, s, strlen(s), &dir_return, &asc_return, &desc_return,
    &ov_return);
  return (unsigned)(ov_return.width);
}

void xxContext::textBigFont() {
  XSetFont (display, text_gc, bigfont);
}

void xxContext::textSmallFont() {
  XSetFont (display, text_gc, smallfont);
}

Pixmap xxContext::makePixmap (char **data, Visual *mpvisual, Colormap
mpcolormap, unsigned mpdepth) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Making a Pixmap from an Xpm." << endl;
#endif
  Pixmap temp;
  Dstr details;
  XpmAttributes xpma;
  xpma.valuemask = XpmColormap | XpmDepth | XpmCloseness | XpmExactColors;
  if (mpvisual)
    xpma.valuemask |= XpmVisual;
  xpma.visual = mpvisual;
  xpma.colormap = mpcolormap;
  xpma.depth = mpdepth;
  xpma.closeness = 1000000;  // Anything worth doing is worth overdoing.
  xpma.exactColors = 0;
  int retval = XCreatePixmapFromData (display,
    XRootWindowOfScreen (XtScreen (manager)),
    data, &temp, NULL, &xpma);
  switch (retval) {
  case XpmColorError:
    fprintf (stderr, "Warning:  Non-fatal Xpm color error reported.\n");
  case XpmSuccess:
    return temp;
  case XpmOpenFailed:
    details = "Xpm: Open Failed.";
    barf (XPM_ERROR, details);
  case XpmFileInvalid:
    details = "Xpm: File Invalid.";
    barf (XPM_ERROR, details);
  case XpmNoMemory:
    details = "Xpm: No Memory.";
    barf (XPM_ERROR, details);
  case XpmColorFailed:
    // details = "Xpm: Color Failed.";
    // barf (XPM_ERROR, details);
    return 0;
  }
  if (retval < 0) {
    details = "Fatal Xpm error of unrecognized type reported.";
    barf (XPM_ERROR, details);
  }
  fprintf (stderr, "Warning:  Non-fatal Xpm error of unrecognized type reported.\n");
  return temp;
}

Pixmap
xxContext::makePixmap (char **data) {
  return makePixmap (data, visual, colormap, depth);
}

// This is the xbm version.
// Not currently used, not currently maintained.
#if 0
Pixmap
xxContext::makePixmap (unsigned width, unsigned height, char *data) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Making a Pixmap from an Xbm (BITMAP)." << endl;
#endif
  Pixmap temp = makePixmap (width, height);
  unsigned x, y;
  int chars_per_row = (int) ceil (((double)width) / 8.0);
  for (y=0; y<height; y++) {
    for (x=0; x<width; x++) {
      unsigned char *v = (unsigned char *) (data + y * chars_per_row + x/8);
      unsigned char bit = (*v) & (1 << ((x%8)));
      if (bit)
        XSetForeground (display, spare_gc, pixels[Colors::foreground]);
      else
        XSetForeground (display, spare_gc, pixels[Colors::background]);
      XDrawPoint (display, temp, spare_gc, x, y);
    }
  }
  return temp;
}
#endif

Pixmap
xxContext::makePixmap (unsigned width, unsigned height, unsigned mpdepth) {
  Pixmap temp;
  assert (temp = XCreatePixmap (display,
    XRootWindowOfScreen (XtScreen (manager)), width, height,
    mpdepth));
  return temp;
}

Pixmap
xxContext::makePixmap (unsigned width, unsigned height) {
  Pixmap temp = makePixmap (width, height, depth);
  XFillRectangle (display, temp, background_gc, 0, 0,
    width, height);
  return temp;
}

// The man page clearly states that colors range from 0 to 65535; this
// code will break if that ever changes.
unsigned short xxContext::scalecolor (unsigned char a) {
  // unsigned long temp = ((unsigned long)a) * 65535 / 255;
  // return (unsigned short)temp;

  // Faster way to get the same answer.
  return ((unsigned short)a << 8) | a;
}

// Create a Pixmap from rawbits PPM data (24-bit color).
// Not currently used, not currently maintained.
#if 0
Pixmap xxContext::makePixmap (unsigned width, unsigned height,
unsigned char *data) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Making a Pixmap from a PPM." << endl;
#endif
  assert (!display_sucks);
  Pixmap temp = makePixmap (width, height);
  unsigned x, y;
  unsigned char *dp = data;
  for (y=0; y<height; y++) {
    for (x=0; x<width; x++) {
      unsigned short r = scalecolor (*(dp++));
      unsigned short g = scalecolor (*(dp++));
      unsigned short b = scalecolor (*(dp++));
      XSetForeground (display, spare_gc, getColor (r, g, b));
      XDrawPoint (display, temp, spare_gc, x, y);
    }
  }
  return temp;
}
#endif

// makePixmap from PNG is not currently used and not currently maintained.

#if 0

static unsigned char *png_fp;
static void read_PNG_data (png_structp png_ptr, png_bytep data,
#if PNG_LIBPNG_VER < 90
		    unsigned long length) {
#else
		    unsigned length) {
#endif
  memcpy (data, png_fp, length);
  png_fp += length;
}

// This is a severely hacked-down replica of example.c in the libpng
// distribution version 0.96.  I tried to make it compatible with the
// libpng 0.89 included in Slackware 3.3, but I got link errors that
// made no sense and gave up.
Pixmap xxContext::makePixmap (unsigned char *data) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Uncompressing a PNG into a PPM." << endl;
#endif

  png_fp = data;

  png_structp png_ptr;
  png_infop info_ptr;
  png_uint_32 width, height;

  assert (png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
    NULL, NULL, NULL));

  assert (info_ptr = png_create_info_struct(NULL));

  // These are alternates; the first is for a real file.
  // png_init_io(png_ptr, fp);
  png_set_read_fn (png_ptr, NULL, &read_PNG_data);

  png_read_info(png_ptr, info_ptr);

  // png_get_IHDR didn't exist in 0.89c
  png_get_IHDR(png_ptr, info_ptr, &width, &height,
    NULL, NULL, NULL, NULL, NULL);

  // This works just as well but is less likely to remain upward compatible.
  // width = info_ptr->width;
  // height = info_ptr->height;

  // Just going to assume that it's a 24-bit image with no weirdness.
  // Add code to convert image types, if I ever need it.

  // I want this to be a contiguous block of memory even though libpng
  // doesn't want it to be.
  unsigned char *ppm;
  assert (ppm = (unsigned char *) malloc (width * height * 3));
  png_bytep *row_pointers;
  assert (row_pointers = (png_bytep*) malloc (height * sizeof (png_bytep)));
  for (png_uint_32 row = 0; row < height; row++)
    row_pointers[row] = (png_bytep) (ppm + row * width * 3);

  png_read_image(png_ptr, row_pointers);

  png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
  free (row_pointers);

  // Now I have my PPM and everything else is hopefully cleaned up.
  Pixmap temp = makePixmap (width, height, ppm);
  free (ppm);
  return temp;
}

#endif

// This is common code from two of the constructors.
void
xxContext::copyParent (xxContext *parent) {
  is_realized = 0;
  display_sucks = parent->display_sucks;
  colormap = parent->colormap;
  dcolormap = parent->dcolormap;
  visual = parent->visual;
  dvisual = parent->dvisual;
  display = parent->display;
  depth = parent->depth;
  ddepth = parent->ddepth;
  pixels = parent->pixels;
  context = parent->context;
  text_gc = parent->text_gc;
  background_gc = parent->background_gc;
  mark_gc = parent->mark_gc;
  invert_gc = parent->invert_gc;
  spare_gc = parent->spare_gc;
  bigfont = parent->bigfont;
  smallfont = parent->smallfont;
  tinyfont = parent->tinyfont;
  red_mask = parent->red_mask;
  green_mask = parent->green_mask;
  blue_mask = parent->blue_mask;
  red_shift = parent->red_shift;
  green_shift = parent->green_shift;
  blue_shift = parent->blue_shift;
  bigfontstruct = parent->bigfontstruct;
  smallfontstruct = parent->smallfontstruct;
  tinyfontstruct = parent->tinyfontstruct;
  defaultfontstruct = parent->defaultfontstruct;
  screen_width = parent->screen_width;
  screen_height = parent->screen_height;
  protocol_atom = parent->protocol_atom;
  kill_atom = parent->kill_atom;
  crab_pixmap = parent->crab_pixmap;
  icon_window = 0;
  iconpixels = parent->iconpixels;
  icon_gc = parent->icon_gc;
  icon_size = parent->icon_size;
}

// Make an xxContext for a new widget, inheriting context from parent.
xxContext::xxContext (xxContext *parent, Widget widget) {
  copyParent (parent);
  is_popup = 0;
  manager = widget;
}

// Make a popup shell.
// unsigned long serialnum = 0;
xxContext::xxContext (xxContext *parent, XtGrabKind in_grabkind) {
  copyParent (parent);
  is_popup = 1;
  grabkind = in_grabkind;

  // Oddly, I have to specify this stuff explicitly under Openwin.
  // Probably has to do with using non-default colormap.
  Arg args[5] = {
    {XtNdepth, (XtArgVal)depth},
    {XtNvisual, (XtArgVal)visual},
    {XtNcolormap, (XtArgVal)colormap},
    {XtNborderColor, (XtArgVal)0},
    {XtNbackground, (XtArgVal)0}
  };
  // char tempname[80];
  // sprintf (tempname, "XTidePopup%lu", serialnum++);
  // manager = XtCreatePopupShell (tempname, transientShellWidgetClass,
  manager = XtCreatePopupShell ("XTide", topLevelShellWidgetClass,
    parent->manager, args, 5);
  set_icon();
}

xxContext::~xxContext() {
  XtDestroyWidget (manager);
  if (icon_window)
    XDestroyWindow(display, icon_window);
}

void
xxContext::realize() {
  if (!is_realized) {
    if (is_popup) {
      XtPopup (manager, grabkind);
      XSetWMProtocols(display, XtWindow(manager), &kill_atom, 1);
    } else {
      XtRealizeWidget (manager);
      XSetWMProtocols(display, XtWindow(manager), &kill_atom, 1);
    }
    is_realized = 1;
  }
}

void
xxContext::unrealize() {
  if (is_realized) {
    if (is_popup)
      XtPopdown (manager);
    else
      XtUnrealizeWidget (manager);
    is_realized = 0;
  }
}

/* Used by GetColor. */
double
xxContext::cdelta (unsigned short a, unsigned short b) {
  return ((double)a-(double)b)*((double)a-(double)b);
}

/* Used by GetColor. */
Pixel
xxContext::remap (unsigned short color, unsigned short mask, int shift) {
  Pixel temp = color & mask;
  // Experiments indicated that shifting by negative values does not work.
  if (shift > 0)
    temp <<= shift;
  else
    temp >>= -shift;
  return temp;
}

Pixel xxContext::getColor (unsigned short red, unsigned short green,
unsigned short blue) {
  return getColor (red, green, blue, visual, colormap, display_sucks);
}

Pixel xxContext::getColor (unsigned short red, unsigned short green,
unsigned short blue, Visual *gcvisual, Colormap gccolormap, int sucks) {

  // Doesn't suck.
  if (!sucks) {
    Pixel temp = 0;
    temp |= remap (red, red_mask, red_shift);
    temp |= remap (green, green_mask, green_shift);
    temp |= remap (blue, blue_mask, blue_shift);
    return temp;
  }

  // Sucks!
  XColor cdef;
  cdef.red = red;
  cdef.green = green;
  cdef.blue = blue;
  cdef.flags = DoRed | DoGreen | DoBlue;
  if (!(XAllocColor (display, gccolormap, &cdef))) {
    int looper;
    XColor tryit, closest;

    /* closest_distance must be initialized to avoid floating */
    /* point exception on DEC Alpha OSF1 V3.0 */
    double distance, closest_distance = 0.0;

    for (looper=0; looper<gcvisual->map_entries; looper++) {
      tryit.pixel = (Pixel) looper;
      tryit.flags = DoRed | DoGreen | DoBlue;
      XQueryColor (display, gccolormap, &tryit);
      distance = cdelta (tryit.red, cdef.red)
               + cdelta (tryit.green, cdef.green)
               + cdelta (tryit.blue, cdef.blue);
      assert (distance >= 0.0);
      if (distance < closest_distance || (!looper)) {
        closest_distance = distance;
        closest = tryit;
      }
    }
    fprintf (stderr, "XTide:  Can't allocate color; using substitute (badness %lu)\n", (unsigned long)(sqrt (closest_distance)));
    if (!(XAllocColor (display, gccolormap, &closest)))
      fprintf (stderr, "XTide:  ACK!  Can't allocate that either!  Expect color shifting.\n");
    return (closest.pixel);
  }
  return (cdef.pixel);
}

Pixel
xxContext::getColor (char *color) {
  return getColor (color, visual, colormap, display_sucks);
}

Pixel
xxContext::getColor (char *color, Visual *gcvisual, Colormap gccolormap,
int sucks) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Colors::parseColor asked to parse " << color << endl;
#endif
  XColor cdef;
  if (!(XParseColor (display, gccolormap, color, &cdef))) {
    Dstr details ("The offending color spec was ");
    details += color;
    details += ".";
    barf (BADCOLORSPEC, details);
  }
  return getColor (cdef.red, cdef.green, cdef.blue, gcvisual, gccolormap,
    sucks);
}

void xxContext::extractColor (Pixel pixel, unsigned short &red,
			      unsigned short &green, unsigned short &blue) {
  XColor temp;
  temp.pixel = pixel;
  temp.flags = DoRed | DoGreen | DoBlue;
  XQueryColor (display, colormap, &temp);
  red = temp.red;
  green = temp.green;
  blue = temp.blue;
}

// There ought to be an easier way to do this.  Is there?
void xxContext::refresh(int window, int width, int height) {
  XExposeEvent synth_event;
  synth_event.type = Expose;
  synth_event.send_event = 1;
  synth_event.display = display;
  synth_event.window = window;
  synth_event.serial = 0;
  synth_event.count = 0;
  synth_event.x = 0;
  synth_event.y = 0;
  synth_event.width = width;
  synth_event.height = height;
  XSendEvent (display, window, True, NoEventMask,
    (XEvent *)(&synth_event));
}

void
xxContext::refreshIcon() {
  refresh (icon_window, 64, 64);
}

void
xxContext::refresh() {
  if (!is_realized)
    return;
  Dimension width, height;
  Arg args[2] = {
    {XtNwidth, (XtArgVal)(&width)},
    {XtNheight, (XtArgVal)(&height)}
  };
  XtGetValues (manager, args, 2);
  refresh (XtWindow (manager), width, height);
}

// This doesn't work.
#if 0
void
xxContext::reset() {
  // if (!is_realized)
  //   return;
  XLeaveWindowEvent synth_event;
  synth_event.type = LeaveNotify;
  synth_event.send_event = 1;
  synth_event.display = display;
  synth_event.window = XtWindow (manager);
  synth_event.root = XtWindow (manager);
  synth_event.subwindow = XtWindow (manager);
  synth_event.serial = 0;
  synth_event.x = -10;
  synth_event.y = -10;
  synth_event.x_root = 0;
  synth_event.y_root = 0;
  synth_event.state = 0;
  XSendEvent (display, XtWindow (manager), True, LeaveWindowMask,
    (XEvent *)(&synth_event));
}
#endif

void
xxContext::adjust_attitude() {
  unrealize();
  // You have to be extra-rude with popups.
  if (is_popup)
    XtUnrealizeWidget (manager);
  Arg args[2] = {
    {XtNwidth, (XtArgVal)0},
    {XtNheight, (XtArgVal)0}
  };
  XtSetValues (manager, args, 2);
}

void
xxContext::fixSize() {
  Dimension width, height;
  Arg args[2] = {
    {XtNwidth, (XtArgVal)&width},
    {XtNheight, (XtArgVal)&height}
  };
  XtGetValues (manager, args, 2);
  XSizeHints sizehint;
  sizehint.min_width = sizehint.max_width = width;
  sizehint.min_height = sizehint.max_height = height;
  sizehint.flags = PMinSize | PMaxSize;
  XSetWMNormalHints (display, XtWindow(manager), &sizehint);
}

void xxContext::setMinSize (Dimension width, Dimension height) {
  XSizeHints sizehint;
  sizehint.min_width = width;
  sizehint.min_height = height;
  sizehint.flags = PMinSize;
  XSetWMNormalHints (display, XtWindow(manager), &sizehint);
}

void
xxContext::widthNoSmaller() {
  Dimension width;
  Arg args[1] = {
    {XtNwidth, (XtArgVal)&width}
  };
  XtGetValues (manager, args, 1);
  XSizeHints sizehint;
  sizehint.min_width = width;
  sizehint.min_height = 0;
  sizehint.flags = PMinSize;
  XSetWMNormalHints (display, XtWindow(manager), &sizehint);
}

static void xxDoEvent (xxContext *context) {
  XEvent foo;
  XtAppNextEvent (context->context, &foo);
  XtDispatchEvent (&foo);
}

void xxMainLoop (xxContext *context) {
  while (XtAppPending (context->context))
    xxDoEvent (context);
}

void xxMainLoopForever (xxContext *context) {
  while (1)
    xxDoEvent (context);
}

void
xxContext::change_settings (xxTideContext *xtidecontext, const Settings &ud) {
  delete xtidecontext->settings;
  xtidecontext->settings = new Settings();
  Settings &settings = *(xtidecontext->settings);
  settings.applyXResources();
  settings.apply (ud);
  settings.applyCommandLine();

  // Get colors.
  delete xtidecontext->colors;
  xtidecontext->colors = new Colors (*(xtidecontext->settings));
  if (display_sucks) {
    XFreeColors (display, colormap, pixels, numcolors, 0);
    XFreeColors (display, dcolormap, iconpixels, 2, 0);
  }
  {
    int a;
    for (a=0; a<numcolors; a++)
      pixels[a] = getColor (settings[colorarg[a]].s.aschar());
  }
  iconpixels[0] = getColor (settings["dc"].s.aschar(),dvisual,dcolormap,1);
  iconpixels[1] = getColor (settings["fg"].s.aschar(),dvisual,dcolormap,1);

  // Update GCs.
  XSetForeground (display, text_gc, pixels[Colors::foreground]);
  XSetForeground (display, mark_gc, pixels[Colors::mark]);
  XSetForeground (display, background_gc, pixels[Colors::background]);
}
