// $Id: xxHorizDialog.hh,v 1.3 2004/04/19 20:16:56 flaterco Exp $
/*  xxHorizDialog  More compact version of Dialog widget.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxHorizDialog {

public:
  xxHorizDialog (xxContext *in_xxcontext, const char *caption, const char *init);
  ~xxHorizDialog();

  char *val();
  void global_redraw();

protected:
  xxContext *box, *label, *text;
  char buf[80];
};
