// $Id: xxDrawable.cc,v 1.8 2004/10/22 17:43:51 flaterco Exp $
/*  xxDrawable  Abstract class for all tide-predicting windows.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

xxDrawable::xxDrawable (xxTideContext *in_tidecontext, xxContext
*in_xxcontext, Station *in_station, int needcontainer):
xxWindow (in_tidecontext, in_xxcontext, needcontainer) {
  assert (station = in_station);
  savebutton = markbutton = helpbutton = dismissbutton =
    optionsbutton = optionsmenu = graphbutton = plainbutton =
    rawbutton = mediumrarebutton = aboutbutton =
    aspectbutton = clockbutton = chooserbutton = timestampbutton =
    unitsbutton = rootbutton = stepbutton = NULL;
}

xxDrawable::~xxDrawable() {
  // Station might have been "stolen"
  if (station) {
    if (station->is_reference_station()) {
      ReferenceStation *r = (ReferenceStation *)station;
      delete r;
    } else {
      SubordinateStation *r = (SubordinateStation *)station;
      delete r;
    }
  }
  mypopup->unrealize();
  if (savebutton)
    delete savebutton;
  if (markbutton)
    delete markbutton;
  if (unitsbutton)
    delete unitsbutton;
  if (timestampbutton)
    delete timestampbutton;
  if (helpbutton)
    delete helpbutton;
  if (dismissbutton)
    delete dismissbutton;
  if (graphbutton)
    delete graphbutton;
  if (plainbutton)
    delete plainbutton;
  if (rawbutton)
    delete rawbutton;
  if (mediumrarebutton)
    delete mediumrarebutton;
  if (aspectbutton)
    delete aspectbutton;
  if (stepbutton)
    delete stepbutton;
  if (clockbutton)
    delete clockbutton;
  if (aboutbutton)
    delete aboutbutton;
  if (chooserbutton)
    delete chooserbutton;
  if (rootbutton)
    delete rootbutton;
  if (optionsbutton)
    delete optionsbutton;
  if (optionsmenu)
    delete optionsmenu;
}

static void
dismissCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->dismiss();
}

static void
helpCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->help();
}

static void
saveCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->save();
}

static void
markCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->mark();
}

static void
unitsCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station) {
    PredictionValue::Unit u = d->station->myUnits;
    switch (u.mytype) {
    case PredictionValue::Unit::Feet:
      u.mytype = PredictionValue::Unit::Meters;
      break;
    case PredictionValue::Unit::Meters:
      u.mytype = PredictionValue::Unit::Feet;
      break;
    default:
      assert (0);
    }
    d->station->setUnits (u);
  }
  d->redraw();
}

static void
timestampCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->timestamp();
}

static void
aspectCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->aspect();
}

static void
stepCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxDrawable*)client_data)->step();
}

void
xxDrawableGraphCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station)
    d->xtidecontext->root->newGraph (d->station->clone(), d->t);
}

void
xxDrawablePlainCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station)
    d->xtidecontext->root->newPlain (d->station->clone(), d->t);
}

void
xxDrawableRawCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station)
    d->xtidecontext->root->newRaw (d->station->clone(), d->t);
}

void
xxDrawableMediumRareCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station)
    d->xtidecontext->root->newMediumRare (d->station->clone(), d->t);
}

void
xxDrawableClockCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station)
    d->xtidecontext->root->newClock (d->station->clone());
}

void
xxDrawableAboutCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  if (d->station)
    d->xtidecontext->root->newAbout (d->station);
}

void
xxDrawableChooserCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  // If it has to index, this could take a while, and somebody might close
  // the window before this callback returns, resulting in a seggie.  So
  // stifle such attempts.
  d->noclose = 1;
  d->xtidecontext->root->newChooser ();
  d->noclose = 0;
}

void
xxDrawableControlPanelCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxDrawable *d = ((xxDrawable*)client_data);
  d->xtidecontext->root->show ();
}

void
xxDrawableMarkCallback (PredictionValue *marklevel, void *in_ptr) {
  xxDrawable *d = (xxDrawable *)in_ptr;
  if (d->station->markLevel)
    delete d->station->markLevel;
  d->station->markLevel = marklevel;
  d->redraw();
}

void xxDrawableTimestampCallback (Timestamp t, void *in_ptr) {
  xxDrawable *d = (xxDrawable *)in_ptr;
  d->t = t;
  d->redraw();
}

void
xxDrawableAspectCallback (double aspect, void *in_ptr) {
  assert (aspect > 0.0);
  xxDrawable *d = (xxDrawable *)in_ptr;
  d->station->aspect = aspect;
  d->redraw();
}

void
xxDrawableStepCallback (Interval step, void *in_ptr) {
  assert (step > Interval (0));
  xxDrawable *d = (xxDrawable *)in_ptr;
  d->station->step = step;
  d->redraw();
}

void xxDrawable::mark() {
  char temp[80];
  if (station->markLevel)
    sprintf (temp, "%f", station->markLevel->val());
  else
    strcpy (temp, "-0.25");
  (void) new xxMarkLevel (xtidecontext, mypopup, xxDrawableMarkCallback, this,
    station->constants->datum().Units(), temp);
}

void xxDrawable::timestamp() {
  assert (!is_clock());
  assert (station);
  (void) new xxTimestamp (xtidecontext, mypopup, xxDrawableTimestampCallback, this, t,
    station->timeZone);
}

void xxDrawable::aspect() {
  char temp[80];
  sprintf (temp, "%f", station->aspect);
  (void) new xxAspect (xtidecontext, mypopup, xxDrawableAspectCallback, this, temp);
}

void xxDrawable::step() {
  (void) new xxStep (xtidecontext, mypopup, xxDrawableStepCallback, this,
    station->step);
}

void xxDrawable::addNormalButtons (Widget below_this, Widget beside_this) {
  assert (mypopup);
  assert (container);

  Arg boxbuttonargs[5] =  {
    {XtNvisual, (XtArgVal)mypopup->visual},
    {XtNcolormap, (XtArgVal)mypopup->colormap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {"menuName", (XtArgVal)"optionsmenu"}
  };
  Arg formbuttonargs[11] =  {
    {XtNfromHoriz, (XtArgVal)beside_this},
    {XtNfromVert, (XtArgVal)below_this},
    {XtNvisual, (XtArgVal)mypopup->visual},
    {XtNcolormap, (XtArgVal)mypopup->colormap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNleft, (XtArgVal)XawChainLeft},
    {XtNright, (XtArgVal)XawChainLeft},
    {XtNtop, (XtArgVal)XawChainBottom},
    {XtNbottom, (XtArgVal)XawChainBottom},
    {"menuName", (XtArgVal)"optionsmenu"}
  };

  // To avoid having to repeat all those args several times,
  // XtNfromHoriz gets overwritten and the last one is only used for
  // the options button (by diddling the length passed to
  // XtCreateManagedWidget).

  Arg *buttonargs;
  int numbuttonargs;
  if (containertype == 1) {
    buttonargs = boxbuttonargs;
    numbuttonargs = 4;
  } else {
    buttonargs = formbuttonargs;
    numbuttonargs = 10;
  }

  // The options menu
  {
    Widget optionsbuttonwidget = XtCreateManagedWidget ("Options",
      menuButtonWidgetClass, container->manager, buttonargs, numbuttonargs+1);
    optionsbutton = new xxContext (mypopup, optionsbuttonwidget);
    formbuttonargs[0].value = (XtArgVal)optionsbuttonwidget;
  }
  {
    Widget menushell = XtCreatePopupShell ("optionsmenu",
      simpleMenuWidgetClass, optionsbutton->manager, buttonargs, numbuttonargs);
    optionsmenu = new xxContext (mypopup, menushell);
  }
  // Buttons on the options menu
  {
    Widget buttonwidget = XtCreateManagedWidget ("Save", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, saveCallback, (XtPointer)this);
    savebutton = new xxContext (mypopup, buttonwidget);
  }
  if (!(is_rare())) {
    Widget buttonwidget = XtCreateManagedWidget ("Set Mark", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, markCallback, (XtPointer)this);
    markbutton = new xxContext (mypopup, buttonwidget);
  }
  if (station) {
    if (!(station->isCurrent)) {
      Widget buttonwidget = XtCreateManagedWidget ("Convert ft<->m", smeBSBObjectClass,
	optionsmenu->manager, boxbuttonargs, 4);
      XtAddCallback (buttonwidget, XtNcallback, unitsCallback, (XtPointer)this);
      unitsbutton = new xxContext (mypopup, buttonwidget);
    }
  }
  if (!(is_clock())) {
    Widget buttonwidget = XtCreateManagedWidget ("Set Time", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, timestampCallback, (XtPointer)this);
    timestampbutton = new xxContext (mypopup, buttonwidget);
  }
  if (is_graph()) {
    Widget buttonwidget = XtCreateManagedWidget ("Set Aspect", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, aspectCallback, (XtPointer)this);
    aspectbutton = new xxContext (mypopup, buttonwidget);
  }
  if (is_rare()) {
    Widget buttonwidget = XtCreateManagedWidget ("Set Step", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, stepCallback, (XtPointer)this);
    stepbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("New Graph Window", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableGraphCallback, (XtPointer)this);
    graphbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("New Plain Mode Window", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawablePlainCallback, (XtPointer)this);
    plainbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("New Raw Mode Window", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableRawCallback, (XtPointer)this);
    rawbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("New Medium Rare Mode Window", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableMediumRareCallback, (XtPointer)this);
    mediumrarebutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("New Clock Window", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableClockCallback, (XtPointer)this);
    clockbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("About This Station", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableAboutCallback, (XtPointer)this);
    aboutbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("New Location Chooser", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableChooserCallback, (XtPointer)this);
    chooserbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Control Panel", smeBSBObjectClass,
      optionsmenu->manager, boxbuttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxDrawableControlPanelCallback, (XtPointer)this);
    rootbutton = new xxContext (mypopup, buttonwidget);
  }
  // Top level buttons
  {
    Widget buttonwidget = XtCreateManagedWidget ("Dismiss", commandWidgetClass,
      container->manager, buttonargs, numbuttonargs);
    XtAddCallback (buttonwidget, XtNcallback, dismissCallback,
     (XtPointer)this);
    dismissbutton = new xxContext (mypopup, buttonwidget);
    formbuttonargs[0].value = (XtArgVal)buttonwidget;
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("?", commandWidgetClass,
      container->manager, buttonargs, numbuttonargs);
    XtAddCallback (buttonwidget, XtNcallback, helpCallback,
      (XtPointer)this);
    helpbutton = new xxContext (mypopup, buttonwidget);
  }
}

int
xxDrawable::is_graph() {
  return 0;
}

int
xxDrawable::is_clock() {
  return 0;
}

int
xxDrawable::is_rare() {
  return 0;
}

void xxDrawable::global_redraw() {
  xxWindow::global_redraw();
  if (station) {

    // 2003-02-11
    // Applying and unapplying the Infer setting necessitates
    // reloading of stations.  Here we are relying on the fact that
    // Station::clone does a reload rather than just copy memory.
    // Danger...
    Station *delme = station;
    station = delme->clone();
    delete delme;

    Configurable &u = (*(xtidecontext->settings))["u"];
    if (!u.isnull) {
      if (u.s != "x") {
        PredictionValue::Unit p_u (u.s);
        station->setUnits (p_u);
      }
    }
  }
  redraw();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (optionsbutton)
    XtSetValues (optionsbutton->manager, buttonargs, 2);
  if (optionsmenu)
    XtSetValues (optionsmenu->manager, buttonargs, 2);
  if (savebutton)
    XtSetValues (savebutton->manager, buttonargs, 2);
  if (markbutton)
    XtSetValues (markbutton->manager, buttonargs, 2);
  if (unitsbutton)
    XtSetValues (unitsbutton->manager, buttonargs, 2);
  if (timestampbutton)
    XtSetValues (timestampbutton->manager, buttonargs, 2);
  if (aspectbutton)
    XtSetValues (aspectbutton->manager, buttonargs, 2);
  if (stepbutton)
    XtSetValues (stepbutton->manager, buttonargs, 2);
  if (graphbutton)
    XtSetValues (graphbutton->manager, buttonargs, 2);
  if (plainbutton)
    XtSetValues (plainbutton->manager, buttonargs, 2);
  if (rawbutton)
    XtSetValues (rawbutton->manager, buttonargs, 2);
  if (mediumrarebutton)
    XtSetValues (mediumrarebutton->manager, buttonargs, 2);
  if (clockbutton)
    XtSetValues (clockbutton->manager, buttonargs, 2);
  if (aboutbutton)
    XtSetValues (aboutbutton->manager, buttonargs, 2);
  if (chooserbutton)
    XtSetValues (chooserbutton->manager, buttonargs, 2);
  if (rootbutton)
    XtSetValues (rootbutton->manager, buttonargs, 2);
  if (dismissbutton)
    XtSetValues (dismissbutton->manager, buttonargs, 2);
  if (helpbutton)
    XtSetValues (helpbutton->manager, buttonargs, 2);
}
