/*  xxContext    X-Toolkit-on-a-steek.  Packaging of assorted XCrap.
    Last modified 1998-05-11

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxContext {
public:
  // Create a new top-level shell widget (do this only once).
  // out colors, out settings.
  xxContext (int &argc, char **argv, Colors *&colors, Settings *&settings);

  // Make an xxContext for a new widget, inheriting context from parent.
  xxContext (xxContext *parent, Widget widget);
  // Make a popup shell.
  xxContext (xxContext *parent, XtGrabKind in_grabkind);
  // Make a regular icon window.
  xxContext (xxContext *parent);
  // Destroy this widget.
  ~xxContext();

  // Changes to user defaults from Control Panel.
  void change_settings (xxTideContext *xtidecontext, const Settings &ud);

  // Show or un-show the window.
  // (These are automatically translated into popup/popdown for popups.)
  void realize();
  void unrealize();

  // Force a button back to its un-highlighted state
  // This doesn't work.
  // void reset();

  // If a color cannot be allocated (read only), the following
  // functions search for the closest match, slowly.
  Pixel getColor (unsigned short red, unsigned short green,
    unsigned short blue);
  Pixel getColor (unsigned short red, unsigned short green,
    unsigned short blue, Visual *gcvisual, Colormap gccolormap, int sucks);
  Pixel getColor (char *color);
  Pixel getColor (char *color, Visual *gcvisual, Colormap gccolormap,
    int sucks);
  unsigned short scalecolor (unsigned char a);

  // Translate a Pixel back to a color.
  void extractColor (Pixel pixel, unsigned short &red, unsigned short &green,
    unsigned short &blue);

  // Switch fonts in text_gc
  void textBigFont();
  void textSmallFont();

  // Change window title and icon name.
  void setTitle (const Dstr &title);

  // Make the window redraw.
  void refresh();
  void refreshIcon();
  void refresh(int window, int width, int height);

  // Make the window more agreeable to resizing to fit its contents.
  // Note that this will leave the window in an unrealized state.
  void adjust_attitude();

  // Stop lusers from resizing this window.  The "curse" can only be
  // applied after the window is realized and dissipates on its own
  // when the window is un-realized.
  void fixSize();
  void widthNoSmaller();
  void setMinSize (Dimension width, Dimension height);

  // These makePixmaps will work on any display.
    // Create a blank Pixmap.
    Pixmap makePixmap (unsigned width, unsigned height);
    Pixmap makePixmap (unsigned width, unsigned height, unsigned mpdepth);
    // Create a Pixmap from an Xpm (limit 256 colors).
    Pixmap makePixmap (char **data);
    Pixmap makePixmap (char **data, Visual *mpvisual, Colormap mpcolormap,
      unsigned mpdepth);
    // Create a Pixmap from an Xbm (limit 2 colors).
    // Not currently used, so not maintained.
    // Pixmap makePixmap (unsigned width, unsigned height, char *data);

  // These are for TrueColor displays ONLY.
    // Create a Pixmap from rawbits PPM data (24-bit color).
    // Not currently used, so not maintained.
    // Pixmap makePixmap (unsigned width, unsigned height, unsigned char *data);
    // Create a Pixmap from a PNG stored in memory.
    // Not currently used, so not maintained.
    // Pixmap makePixmap (unsigned char *data);

  // Return width of string in pixels.
  unsigned stringWidth (XFontStruct *fs, const char *s);

  // Public attributes, for inspection only -- do not modify.
  int display_sucks, screen_width, screen_height;
  XtAppContext context;
  Widget manager;
  Display *display;
  // Pixels is indexed by colors declared in class Colors.
  // Iconpixels gets only two elements
  // [0] is icon background
  // [1] is icon foreground
  // These are used only by xxClock.
  Pixel *pixels, *iconpixels;
  GC text_gc, background_gc, mark_gc, invert_gc;
  Font bigfont, smallfont, tinyfont;
  XFontStruct *bigfontstruct, *smallfontstruct, *defaultfontstruct,
    *tinyfontstruct;
  Colormap colormap, dcolormap;
  Visual *visual, *dvisual;
  unsigned depth, ddepth;
  Atom kill_atom, protocol_atom;
  int is_realized, is_popup;
  unsigned icon_size;

  // You can modify the foreground color or font of this one if you want.
  GC spare_gc;
  // This matches the icon window (default visual, colormap).
  GC icon_gc;

  Window icon_window;

protected:
  Pixmap crab_pixmap;
  void set_icon();
  double cdelta (unsigned short a, unsigned short b);
  Pixel remap (unsigned short color, unsigned short mask, int shift);
  XtGrabKind grabkind;

  // These are only valid if !display_sucks.  They do NOT correspond
  // directly to the masks in XVisualInfo.  The way that you get from
  // an unsigned short to bits for the Pixel is to mask the unsigned
  // short by color_mask and then shift left by color_shift (or right
  // if color_shift is negative).  Note that the bits per color may
  // NOT be the same, contrary to what is implied by the bits_per_RGB
  // field of XVisualInfo.  In 16-bit mode, my PC gives 5 bits red, 6
  // bits green, 5 bits blue.
  unsigned short red_mask, green_mask, blue_mask;
  int red_shift, green_shift, blue_shift;

  // This converts the XVisualInfo masks to what we want.
  void convert_mask (unsigned long vinfomask, unsigned short &mask,
    int &shift);

  // This is common code from two of the constructors.
  void copyParent (xxContext *parent);
};

// This replaces XtAppMainLoop.  It returns when there is nothing to do.
void xxMainLoop (xxContext *context);

// This is the same as xxMainLoop except that it does not return.
void xxMainLoopForever (xxContext *context);
