// 	$Id: ZoneIndex.cc,v 1.4 2004/10/26 15:54:53 flaterco Exp $	
/*  ZoneIndex  Index stations by zone for xttpd.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

ZoneIndex::ZoneIndex() {
  top = NULL;
}

ZoneIndex::dnode::dnode() {
  next = NULL;
  subzones = NULL;
  il = NULL;
}

void
ZoneIndex::dnode::rmit (struct indexlist *i) {
  if (i) {
    rmit (i->next);
    delete i;
  }
}

ZoneIndex::dnode::~dnode() {
  if (next)
    delete next;
  if (subzones)
    delete subzones;
  rmit (il);
}

ZoneIndex::~ZoneIndex() {
  delete top;
}

ZoneIndex::dnode *ZoneIndex::dnode::operator[] (const Dstr &zone) {
  if (zone == name)
    return this;
  // ":America/New_York" %= ":America/"
  else if ((zone %= name) && (subzones != NULL))
    return (*subzones)[zone];
  else if (next)
    return (*next)[zone];
  return NULL;
}

void
ZoneIndex::dnode::add (unsigned long i) {
  // Stations are already in alphabetical order.
  struct indexlist *newil = new indexlist;
  newil->i = i;
  newil->next = NULL;
  if (il) {
    struct indexlist *lastil = il;
    while (lastil->next)
      lastil = lastil->next;
    lastil->next = newil;
  } else {
    il = newil;
  }
}

void
ZoneIndex::add (dnode *&thislevel, const Dstr &zone) {
  // Keep these in alphabetical order.
  dnode *newd = new dnode();
  newd->name = zone;
  if (thislevel) {
    if (dstrcasecmp (zone, thislevel->name) < 0) {
      newd->next = thislevel;
      thislevel = newd;
    } else {
      dnode *last = thislevel;
      while (last->next) {
        if (dstrcasecmp (zone, last->next->name) < 0)
          break;
        last = last->next;
      }
      newd->next = last->next;
      last->next = newd;
    }
  } else {
    thislevel = newd;
  }
}

ZoneIndex::dnode *ZoneIndex::operator[] (const Dstr &zone) {
  assert (zone.length());
  if (top)
    return (*top)[zone];
  return NULL;
}

// I made a mess of this.
ZoneIndex::dnode *ZoneIndex::makezone (const Dstr &zone) {
  assert (zone[zone.length()-1] != '/');
  dnode *tryit = (*this)[zone];
  if (tryit)
    return tryit;
  else {
    Dstr smallzone (zone), addzone (zone);
    int i;
    // Find the level that already exists.
    while ((i = smallzone.strrchr ('/')) != -1) {
      if (i == (int)smallzone.length() - 1) {
        addzone = smallzone;
        smallzone -= i;
      } else {
        smallzone -= i+1;
        if ((tryit = (*this)[smallzone]))
          break;
      }
    }
    // Add one more level.
    if (tryit)
      add (tryit->subzones, addzone);
    else
      add (top, addzone);
    // Be lazy, recurse.
    return makezone (zone);
  }
}

void ZoneIndex::add (StationIndex *si, unsigned long i) {
  assert (si);
  StationRef *sr = (*si)[i];
  Dstr &zone = sr->timezone;
  assert (!zone.isNull());
  dnode *tryit = (*this)[zone];
  if (!tryit)
    tryit = makezone (zone);
  assert (tryit);
  tryit->add (i);
}

void ZoneIndex::add (StationIndex *si) {
  log ("Building zone index...", LOG_NOTICE);
  unsigned long i;
  for (i=0; i<si->length(); i++)
    add (si, i);
}

void
ZoneIndex::dnode::dump (int indent) {
  int a;
  for (a=0; a<indent; a++)
    printf (" ");
  printf ("%s\n", name.aschar());
  if (subzones)
    subzones->dump (indent+2);
  if (next)
    next->dump (indent);
}

void
ZoneIndex::dump () {
  if (top)
    top->dump (0);
  else
    printf ("NULL\n");
}
