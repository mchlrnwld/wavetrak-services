// $Id: Angle.hh,v 1.3 2004/09/29 16:10:15 flaterco Exp $
// Angle:  abstraction to partially alleviate degrees versus radians debate.
// All angles are "normalized" to the 0-2pi rad (0-360 degree) range.

/*
    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Angle {
public:
  enum units {DEGREES, RADIANS};
  // FIXME:  Alternatives to "degrees true?"
  enum qualifier {NONE, TRU};
  // "TRUE" avoided due to likely reserved word conflicts.

  Angle ();
  Angle (units u, double v);
  Angle (units u, qualifier q, double v);

  // As of now, the qualifier does nothing for Angle but is used in
  // output routines to decide whether to print degrees true or just
  // plain old degrees.

  // Return value in whichever units.
  double deg();
  double rad();
  qualifier qual();

  // Pretty-print degrees with specified number of digits after
  // decimal.  Field width is precision plus 4 if precision is greater
  // than 0; if precision is 0, width is 3.  This does not append a
  // degrees symbol or qualifier.
  void ppdeg (unsigned precision, Dstr &buf);
  // This appends a degrees symbol and possibly a qualifier, and does not
  // have fixed field width.
  void ppqdeg (unsigned precision, Dstr &buf);

  // Operations.
  Angle &operator+= (Angle a);
  Angle &operator-= (Angle a);
  Angle &operator*= (double a);

protected:

  // The preferred unit is radians since that is what is supported
  // most efficiently by libm.
  double radians;
  qualifier myqual;

  void normalize();
  void setup(units u, qualifier q, double v);
};

class Degrees: public Angle {
public:
  Degrees (double v);
};

class Radians: public Angle {
public:
  Radians (double v);
};

// Operations.
Angle operator+ (Angle a, Angle b);
Angle operator- (Angle a, Angle b);
Angle operator* (double a, Angle b);
Angle operator* (Angle a, double b);
int operator> (Angle a, Angle b);
int operator< (Angle a, Angle b);
int operator>= (Angle a, Angle b);
int operator<= (Angle a, Angle b);
int operator== (Angle a, Angle b);
int operator!= (Angle a, Angle b);

// Trig functions.
double sin (Angle a);
double cos (Angle a);
double tan (Angle a);
double cot (Angle a);
