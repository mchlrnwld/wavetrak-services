// $Id: Offsets.cc,v 1.5 2004/11/16 22:27:29 flaterco Exp $
// Offsets:  storage for tide offsets.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Offsets::~Offsets() {
  // This space intentionally left blank.
}

void LevelOffset::levelMultiply (double in_mult) {
  assert (in_mult > 0.0);
  mylevelMultiply = in_mult;
}

double LevelOffset::levelMultiply () const {
  return mylevelMultiply;
}

LevelOffset::LevelOffset () {
  mylevelMultiply = 1.0;
}

int
SimpleOffsets::is_simple() {
  return 1;
}

int
HairyOffsets::is_simple() {
  return 0;
}

SimpleOffsets::SimpleOffsets () {
  mylevelMultiply = 1.0;
}

SimpleOffsets::SimpleOffsets (const CombinedOffset &co_in) {
  levelAdd = co_in.levelAdd;
  mylevelMultiply = co_in.levelMultiply();
  timeAdd = co_in.timeAdd;
}

int operator== (CombinedOffset a, CombinedOffset b) {
  if (a.timeAdd == b.timeAdd &&
      a.levelMultiply() == b.levelMultiply() &&
      a.levelAdd == b.levelAdd)
    return 1;
  return 0;
}

HairyOffsets::HairyOffsets () {
  // This space intentionally left blank.
}

HairyOffsets::HairyOffsets (const SimpleOffsets &s_in) {
  max = min = s_in;
  floodbegins = ebbbegins = s_in.timeAdd;
}
