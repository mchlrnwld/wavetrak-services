// $Id: PredictionValue.cc,v 1.6 2004/11/15 16:30:22 flaterco Exp $
/*  PredictionValue  feet, meters, knots, et cetera.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

#define numunits 4
static char *longnames[numunits] = {"feet", "meters", "knots", "knots^2"};
static char *shortnames[numunits] = {"ft", "m", "kt", "kt^2"};

PredictionValue::Unit::Unit () {
  mytype = Meters;
}

PredictionValue::Unit::Unit (const Dstr &name_in) {
  unsigned i;
  for (i=0; i<numunits; i++)
    if (name_in == longnames[i] || name_in == shortnames[i]) {
      mytype = (Units)i;
      return;
    }
  Dstr details ("The offending units were ");
  details += name_in;
  details += ".";
  barf (UNRECOGNIZED_UNITS, details);
}

PredictionValue::Unit::Unit (Units units_in) {
  mytype = units_in;
}

void PredictionValue::Unit::shortname (Dstr &name_out) const {
  name_out = shortnames[mytype];
}

void PredictionValue::Unit::longname (Dstr &name_out) const {
  name_out = longnames[mytype];
}

PredictionValue::PredictionValue () {
  myunits = Unit (PredictionValue::Unit::Meters);
  myvalue = 0.0;
}

PredictionValue::PredictionValue (Unit inunits, double inval) {
  myunits = inunits;
  myvalue = inval;
}

double PredictionValue::val () const {
  return myvalue;
}

void PredictionValue::val (double newval) {
  myvalue = newval;
}

PredictionValue::Unit PredictionValue::Units () const {
  return myunits;
}

#define convbarf {\
  Dstr temp;\
  Dstr details ("From ");\
  myunits.longname (temp);\
  details += temp;\
  details += " to ";\
  tounits.longname (temp);\
  details += temp;\
  barf (IMPOSSIBLE_CONVERSION, details);}

void PredictionValue::Units (PredictionValue::Unit::Units tounits) {
  Unit u (tounits);
  PredictionValue::Units (u);
}

void PredictionValue::Units (Unit tounits) {
  if (myunits == tounits)
    barf (NO_CONVERSION, 0);
  else {
    // You are allowed to convert nulls from meters to knots because
    // otherwise it just complicates initialization.
    if (myvalue != 0.0) {
      switch (myunits.mytype) {
      case Unit::Feet:
	if (tounits.mytype == Unit::Meters)
	  myvalue *= 0.3048;
	else
	  convbarf;
	break;
      case Unit::Meters:
	if (tounits.mytype == Unit::Feet)
	  myvalue /= 0.3048;
	else
	  convbarf;
	break;
      case Unit::KnotsSquared:
	if (tounits.mytype == Unit::Knots) {
	  // This is not mathematically correct, but it is tidematically correct.
	  if (myvalue < 0)
	    myvalue = -sqrt(fabs(myvalue));
	  else
	    myvalue = sqrt(myvalue);
	} else
	  convbarf;
	break;
      case Unit::Knots:
	if (tounits.mytype == Unit::KnotsSquared) {
	  // This is used only in Station::predictTideEvents to set up
	  // the mark level.
	  // This is not mathematically correct, but it is tidematically correct.
	  if (myvalue < 0)
	    myvalue = -(myvalue*myvalue);
	  else
	    myvalue *= myvalue;
	} else
	  convbarf;
	break;
      default:
	convbarf;
      }
    }
    myunits = tounits;
  }
}

// This insists that both values must have exactly the same units.
// When it is time to add the datum for a hydraulic station, you must
// first explicitly convert the amplitude from KnotsSquared to Knots.
// This should prevent any unexpected conversions.
// Exception:  If value is 0 (uninitialized), adopt units of non-zero value.
PredictionValue &PredictionValue::operator+= (PredictionValue in_val) {
  if (in_val.val() == 0.0)
    ;
  else if (myvalue == 0.0)
    (*this) = in_val;  // Adopt units of in_val
  else {
    assert (myunits.mytype == in_val.myunits.mytype);
    this->val (myvalue + in_val.val());
  }
  return (*this);
}
PredictionValue &PredictionValue::operator-= (PredictionValue in_val) {
  (*this) += -in_val;
  return (*this);
}

// This insists that both values must have exactly the same units.
// When it is time to add the datum for a hydraulic station, you must
// first explicitly convert the amplitude from KnotsSquared to Knots.
// This should prevent any unexpected conversions.
PredictionValue operator+ (PredictionValue a, PredictionValue b) {
  assert (a.myunits.mytype == b.myunits.mytype);
  return PredictionValue (a.myunits, a.val()+b.val());
}
PredictionValue operator- (PredictionValue a, PredictionValue b) {
  assert (a.myunits.mytype == b.myunits.mytype);
  return PredictionValue (a.myunits, a.val()-b.val());
}

PredictionValue &
PredictionValue::operator*= (double levelMultiply) {
  // assert (levelMultiply > 0.0);
  myvalue *= levelMultiply;
  return (*this);
}

PredictionValue operator* (double a, PredictionValue b) {
  b *= a;
  return b;
}

PredictionValue operator* (PredictionValue b, double a) {
  b *= a;
  return b;
}

PredictionValue operator/ (PredictionValue b, double a) {
  b *= 1.0/a;
  return b;
}

PredictionValue operator/ (PredictionValue b, long a) {
  return b / (double)a;
}

int operator> (PredictionValue a, PredictionValue b) {
  if (a.Units() != b.Units())
    b.Units (a.Units());
  if (a.val() > b.val())
    return 1;
  return 0;
}

int operator< (PredictionValue a, PredictionValue b) {
  if (a.Units() != b.Units())
    b.Units (a.Units());
  if (a.val() < b.val())
    return 1;
  return 0;
}

int operator<= (PredictionValue a, PredictionValue b) {
  if (a.Units() != b.Units())
    b.Units (a.Units());
  if (a.val() <= b.val())
    return 1;
  return 0;
}

int operator>= (PredictionValue a, PredictionValue b) {
  if (a.Units() != b.Units())
    b.Units (a.Units());
  if (a.val() >= b.val())
    return 1;
  return 0;
}

int operator== (PredictionValue a, PredictionValue b) {
  if (a.Units() != b.Units())
    b.Units (a.Units());
  if (a.val() == b.val())
    return 1;
  return 0;
}

int operator!= (PredictionValue a, PredictionValue b) {
  if (a.Units() != b.Units())
    b.Units (a.Units());
  if (a.val() != b.val())
    return 1;
  return 0;
}

int operator!= (PredictionValue::Unit a, PredictionValue::Unit b) {
  if (a.mytype != b.mytype)
    return 1;
  return 0;
}

#if 0
int operator== (PredictionValue::Unit a, PredictionValue::Unit::Units b) {
  if (a.mytype == b)
    return 1;
  return 0;
}
#endif

#if 0
ostream &operator<< (ostream &out, const PredictionValue &a) {
  Dstr tu;
  a.Units().longname(tu);
  out << a.val() << " " << tu;
  return out;
}
#endif

PredictionValue operator- (PredictionValue a) {
  return PredictionValue (a.Units(), -a.val());
}

PredictionValue abs (PredictionValue a) {
  return PredictionValue (a.Units(), fabs(a.val()));
}

double operator/ (PredictionValue a, PredictionValue b) {
  assert (a.Units() == b.Units());
  return a.val()/b.val();
}

int operator== (PredictionValue::Unit a, PredictionValue::Unit b) {
  if (a.mytype == b.mytype)
    return 1;
  return 0;
}

// Print in the form -XX.YY units (padding as needed)
void PredictionValue::print (Dstr &print_out) const {
  char temp[80];
  Dstr unitname;
  myunits.longname (unitname);
  sprintf (temp, "% 6.2f", myvalue);
  print_out = temp;
  print_out += " ";
  print_out += unitname;
}

void PredictionValue::printnp (Dstr &print_out) const {
  char temp[80];
  Dstr unitname;
  myunits.shortname (unitname);
  sprintf (temp, "%2.2f", myvalue);
  print_out = temp;
  print_out += " ";
  print_out += unitname;
}
