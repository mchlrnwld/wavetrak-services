// $Id: xxErrorBox.cc,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxErrorBox  Show error message.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"
#include "errorbox.xpm.hh"
#include "errorbg.xpm.hh"

static void
dieCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxErrorBox *e = (xxErrorBox *)client_data;
  e->dismiss();
}

void
xxErrorBox::dismiss() {
  delete this;
}

xxErrorBox::~xxErrorBox() {
  if (is_fatal) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    assert (0);  // This is just to get a core dump.
#else
    exit (-1);
#endif
  }
  mypopup->unrealize();
  delete label;
  if (picture) {
    XFreePixmap (mypopup->display, errorboxpixmap);
    delete picture;
  }
  delete button;
  XFreePixmap (mypopup->display, errorbgpixmap);
}

xxErrorBox::xxErrorBox (xxTideContext *in_xtidecontext, xxContext *context,
const Dstr &errmsg, int fatal): xxWindow (in_xtidecontext, context, 1,
XtGrabExclusive) {
  is_fatal = fatal;
  mypopup->setTitle ("Error Message");

  Arg bgargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };

  if (!(errorbgpixmap = mypopup->makePixmap (errorbg_xpm)))
    errorbgpixmap = mypopup->makePixmap (64, 64);
  Arg boxargs[1] = {
    {XtNbackgroundPixmap, (XtArgVal)errorbgpixmap}
  };
  XtSetValues (container->manager, boxargs, 1);

  if (is_fatal) {
    if (!(errorboxpixmap = mypopup->makePixmap (errorbox_xpm)))
      errorboxpixmap = mypopup->makePixmap (64, 64);
    Arg args[5] = {
      {XtNbitmap, (XtArgVal)errorboxpixmap},
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNinternalHeight, (XtArgVal)0},
      {XtNinternalWidth, (XtArgVal)0}
    };
    Widget picturewidget = XtCreateManagedWidget ("", labelWidgetClass,
      container->manager, args, 5);
    picture = new xxContext (mypopup, picturewidget);
  } else
    picture = NULL;

  Widget labelwidget = XtCreateManagedWidget (errmsg.aschar(),
    labelWidgetClass, container->manager, bgargs, 2);
  label = new xxContext (container, labelwidget);

  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::mark]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::background]}
  };

  {
    char *bn;
    if (is_fatal)
      bn = "Exit";
    else
      bn = "Oops";
    Widget buttonwidget = XtCreateManagedWidget (bn,
      commandWidgetClass, container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, dieCallback, (XtPointer)this);
    button = new xxContext (mypopup, buttonwidget);
  }

  mypopup->realize();
  mypopup->fixSize();
}
