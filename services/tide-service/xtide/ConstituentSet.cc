/*  ConstituentSet
    Last modified 1997-09-28

    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

ConstituentSet::ConstituentSet (unsigned in_len) {
  assert (in_len > 0);
  length = in_len;
  constituents = new TabulatedConstituent [length];
}

ConstituentSet::~ConstituentSet () {
  delete [] constituents;
}

TabulatedConstituent &
ConstituentSet::operator[] (unsigned index) const {
  assert (index < length);
  return constituents[index];
}
