// 	$Id: xxTideContext.cc,v 1.4 2004/07/01 14:51:28 flaterco Exp $	
/*  xxTideContext  Specialization of TideContext.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

xxTideContext::xxTideContext (xxContext *context, xxXTideRoot *in_root,
Colors *in_colors, Settings *in_settings): TideContext (in_colors,
in_settings) {
  xxcontext = context;
  root = in_root;
}

// THIS CODE IS DUPLICATED IN TIDECONTEXT!  The duplication is needed
// to embed XEvent polling in xxTideContext.
void
xxTideContext::doHarmonicsFile (Dstr &fname, xxTitleScreen &titleScreen) {
  titleScreen.updateProgress (fname);
  HarmonicsFile h (fname, settings);
  myStationIndex->addHfileId (h.version_string);
  StationRef *s;
  while ((s = h.getNextStationRef())) {
    myStationIndex->add (s);
    xxMainLoop (xxcontext);
  }
}

// THIS CODE IS DUPLICATED IN TIDECONTEXT!  The duplication is needed
// to embed XEvent polling in xxTideContext.
StationIndex *xxTideContext::stationIndex (XtGrabKind in_grabkind) {
  if (!myStationIndex) {
    xxTitleScreen titleScreen (this, xxcontext, in_grabkind);
    myStationIndex = new StationIndex();
    for (unsigned i=0; i<harmonicsPath->length(); i++) {
      struct stat s;
      if (stat ((*harmonicsPath)[i].aschar(), &s) == 0) {
        if (S_ISDIR (s.st_mode)) {
          Dstr dname ((*harmonicsPath)[i]);
          dname += '/';
          DIR *dirp = opendir (dname.aschar());
          if (!dirp)
            xperror (dname.aschar());
          else {
            struct dirent *dp;
	    for (dp = readdir(dirp); dp != NULL; dp = readdir(dirp)) {
              Dstr fname (dp->d_name);
	      if (fname[0] == '.') // Skip all hidden files
		continue;
	      else {
                fname *= dname;
                // DO NOT FREE THIS.  It gets pointed to by every station.
                Dstr *qfname = new Dstr(fname);
                doHarmonicsFile (*qfname, titleScreen);
              }
	    }
	    closedir(dirp);
          }
        } else
          doHarmonicsFile ((*harmonicsPath)[i], titleScreen);
      } else
        xperror ((*harmonicsPath)[i].aschar());
    }
    if (myStationIndex->length() == 0)
      barf (NO_HFILE_PATH);
    myStationIndex->qsort();
  }
  return myStationIndex;
}
