// $Id: Angle.cc,v 1.4 2004/10/19 18:02:57 flaterco Exp $
// Angle:  abstraction to partially alleviate degrees versus radians debate.
// All angles are "normalized" to the 0-2pi rad (0-360 degree) range.

/*
    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

void
Angle::normalize() {
  radians = fmod (radians, 2.0 * M_PI);
  if (radians < 0.0)
    radians += 2.0 * M_PI;
  // This can happen because of floating point roundoff if you add a very
  // small negative number to 2 PI.
  if (radians == 2.0 * M_PI)
    radians = 0.0;
  assert (radians >= 0.0 && radians < 2.0 * M_PI);
}

void Angle::setup (units u, qualifier q, double v) {
  myqual = q;
  switch (u) {
  case DEGREES:
    // Convert to radians and fall through.
    v = v * M_PI / 180.0;
  case RADIANS:
    radians = v;
    normalize();
    break;
  default:
    assert (0);
  }
}

Angle::Angle (units u, qualifier q, double v) {
  setup (u, q, v);
}

Angle::Angle (units u, double v) {
  setup (u, Angle::NONE, v);
}

Angle::Angle () {
  radians = 0.0;
}

Angle::qualifier Angle::qual() {
  return myqual;
}

double Angle::deg () {
  return radians * 180.0 / M_PI;
}

double Angle::rad () {
  return radians;
}

void Angle::ppdeg (unsigned precision, Dstr &buf) {
  char retbuf[80];
  int width = precision + 4;
  assert (width < 80);
  if (precision == 0)
    width = 3;
  sprintf (retbuf, "%*.*f", width, (int)(precision), deg());

  // The rounding can result in us seeing 360.00 -- but we don't want that.
  if (retbuf[0] == '3' && retbuf[1] == '6')
    retbuf[0] = retbuf[1] = ' ';

  buf = retbuf;
}

void Angle::ppqdeg (unsigned precision, Dstr &buf) {
  ppdeg (precision, buf);
  buf += '\260';
  switch (myqual) {
  case Angle::TRU:
    buf += " True";
    break;
  default:
    ;
  }
  buf.trim();
}

Angle &
Angle::operator+= (Angle a) {
  radians += a.rad();
  normalize();
  return (*this);
}

Angle &
Angle::operator-= (Angle a) {
  radians -= a.rad();
  normalize();
  return (*this);
}

Angle &
Angle::operator*= (double a) {
  radians *= a;
  normalize();
  return (*this);
}

Degrees::Degrees (double v): Angle (Angle::DEGREES, v) {}
Radians::Radians (double v): Angle (Angle::RADIANS, v) {}

// Save for a rainy day
// This gives the whole nine yards:  degrees, minutes, and seconds.
// ostream &operator<< (ostream &out, Angle &v) {
//   double degrees, minutes, seconds;
//   degrees = v.deg();
//   minutes = fmod (degrees, 1.0);
//   degrees -= minutes;
//   minutes = fabs (minutes) * 60.0;
//   seconds = fmod (minutes, 1.0);
//   minutes -= seconds;
//   seconds *= 60.0;
//   out << (int)degrees << '\260' << (int)minutes << "'" << seconds << "''";
//   return out;
// }

Angle operator+ (Angle a, Angle b) {
  return Radians (a.rad() + b.rad());
}

Angle operator- (Angle a, Angle b) {
  return Radians (a.rad() - b.rad());
}

Angle operator* (double a, Angle b) {
  return Radians (a * b.rad());
}

Angle operator* (Angle a, double b) {
  return Radians (b * a.rad());
}

int operator> (Angle a, Angle b) {
  if (a.rad() > b.rad())
    return 1;
  return 0;
}

int operator< (Angle a, Angle b) {
  if (a.rad() < b.rad())
    return 1;
  return 0;
}

int operator>= (Angle a, Angle b) {
  if (a.rad() >= b.rad())
    return 1;
  return 0;
}

int operator<= (Angle a, Angle b) {
  if (a.rad() <= b.rad())
    return 1;
  return 0;
}

int operator== (Angle a, Angle b) {
  if (a.rad() == b.rad())
    return 1;
  return 0;
}

int operator!= (Angle a, Angle b) {
  if (a.rad() != b.rad())
    return 1;
  return 0;
}

double sin (Angle a) {
  return sin (a.rad());
}

double cos (Angle a) {
  return cos (a.rad());
}

double tan (Angle a) {
  return tan (a.rad());
}

double cot (Angle a) {
  return 1.0 / tan (a);
}
