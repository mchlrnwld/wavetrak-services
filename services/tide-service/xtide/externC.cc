/*  externC.cc  Functions that need to be declared extern "C".
    Last modified 1999-10-24

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

// The user_io_ptr feature of libpng doesn't seem to work.
FILE *png_file_ptr;
int png_socket;

void
file_write_data_fn (png_structp png_ptr, png_bytep b_ptr, png_size_t sz) {
  fwrite (b_ptr, 1, sz, png_file_ptr);
}

void
socket_write_data_fn (png_structp png_ptr, png_bytep b_ptr, png_size_t sz) {
  write (png_socket, b_ptr, sz);
}

int
compare_stationrefs_name (const void *a, const void *b) {
  StationRef *left = *(StationRef **)(a);
  StationRef *right = *(StationRef **)(b);
  return dstrcasecmp (left->name, right->name);
}

int
compare_stationrefs_lat (const void *a, const void *b) {
  StationRef *left = *(StationRef **)(a);
  StationRef *right = *(StationRef **)(b);
  if (left->coordinates.isNull() && right->coordinates.isNull())
    return 0;
  if (left->coordinates.isNull())
    return 1;
  if (right->coordinates.isNull())
    return -1;
  if (left->coordinates.lat() < right->coordinates.lat())
    return 1;
  if (left->coordinates.lat() > right->coordinates.lat())
    return -1;
  return 0;
}

int
compare_stationrefs_lng (const void *a, const void *b) {
  StationRef *left = *(StationRef **)(a);
  StationRef *right = *(StationRef **)(b);
  if (left->coordinates.isNull() && right->coordinates.isNull())
    return 0;
  if (left->coordinates.isNull())
    return -1;
  if (right->coordinates.isNull())
    return 1;
  if (left->coordinates.lng() < right->coordinates.lng())
    return -1;
  if (left->coordinates.lng() > right->coordinates.lng())
    return 1;
  return 0;
}
