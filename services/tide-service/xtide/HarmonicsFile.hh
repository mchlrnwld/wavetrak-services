// $Id: HarmonicsFile.hh,v 1.7 2004/10/26 15:54:51 flaterco Exp $
/*  HarmonicsFile  Operations for reading harmonics files.

    Copyright (C) 1998  David Flater.

    Vastly altered by Jan Depner 2002-09 to remove old .txt and .xml
    based code and replace with libtcd.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class TideContext;
class StationReference;
class ConstituentSet;
class Settings;

// If the specified file does not exist, the results will be as if
// it existed with zero length: getNextStationRef will return NULL.

class HarmonicsFile {
public:

  // HarmonicsFile has inherited the statefulness of libtcd.  Do not
  // instantiate more than one HarmonicsFile at a time or bad things
  // will happen.

  // libtcd's open_tide_db is called each time a HarmonicsFile is
  // created.  Intentionally, there is no matching call to
  // close_tide_db (consquently, no need for a destructor).  See
  // http://www.flaterco.com/xtide/tcd_notes.html and XTide changelog
  // for version 2.6.3.

  HarmonicsFile (Dstr &filename, Settings *in_settings);
  StationRef *getNextStationRef();  // Allocates a new StationRef, returns
                                    // NULL if end of file.
  Station *getStation (long TCDRecordNumber, TideContext *in_context);
  Dstr version_string;

protected:
  Settings *settings;
  Dstr *myfilename;
  ConstituentSet *get_constituents ();
};
