/*  ZoneIndex  Index stations by zone for xttpd.
    Last modified 1998-02-01

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class ZoneIndex {
public:
  ZoneIndex();
  ~ZoneIndex();
  struct indexlist {
    unsigned long i;
    struct indexlist *next;
  };
  class dnode {
  public:
    dnode();
    ~dnode();
    Dstr name; // For leaves this is the entire time zone string;
               // for others, it is like ":America/"
    struct indexlist *il;
    dnode *subzones;
    dnode *next;
    dnode *operator[] (const Dstr &zone);
    void add (unsigned long i); // Add a station.
    void dump (int indent);
  protected:
    void rmit (struct indexlist *i);
  };
  dnode *operator[] (const Dstr &zone);
  void add (StationIndex *si);
  void dump();
  dnode *top;
protected:
  void add (StationIndex *si, unsigned long i);
  void add (dnode *&thislevel, const Dstr &zone);
  dnode *makezone (const Dstr &zone);
};
