// $Id: Calendar.cc,v 1.9 2004/11/15 16:30:22 flaterco Exp $
/*  Calendar  Manage construction, organization, and printing of calendars.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Calendar::Calendar (Station *station_in, Timestamp start_tm, Timestamp end_tm) {
  station = station_in;
  timezone = station->timeZone;
  settings = station->context->settings;
  isCurrent = station->isCurrent;
  start_day_start = start_tm;
  start_day_start.prev_day (timezone, settings);
  end_day_start = end_tm;
  end_day_start.prev_day (timezone, settings);

  // If end_tm is midnight, the real last day is one earlier.
  if (end_day_start == end_tm) {
    end_day_start -= Interval(1);
    end_day_start.prev_day (timezone, settings);
    assert (end_day_start < end_tm);
  }

  station->predictTideEvents (start_tm, end_tm, organizer);
}

void Calendar::add_month_banner (Dstr &text_out, Timestamp t, char form) {
  Dstr heading;
  t.printcalheading (heading, timezone, settings);
  if (form == 'h') {
    text_out += "<h2>";
    text_out += heading;
    text_out += "</h2>\n";
    text_out += "<table border>\n";
  } else {
    int numspaces = ((int)((*settings)["tw"].u) - (int)(heading.length())) / 2;
    for (int a=0; a<numspaces; a++)
      text_out += ' ';
    text_out += heading;
    text_out += "\n\n";
  }
}

void Calendar::flush_buf (Dstr &text_out, Dstr *buf, int buflen,
char form, int oldcal, int headers) {
  int a, isdone=1;

  // If it's totally null, don't write a blank line.
  for (a=0; a<buflen; a++)
    if (buf[a].length()) {
      isdone = 0;
      break;
    }
  if (isdone)
    return;

  if (form == 'h') {
    if (oldcal) {
      // Strip out the day headers into a separate row
      text_out += "<tr>";
      for (a=0; a<buflen; a++) {
        Dstr strip;
        text_out += "<th>";
        buf[a].getline (strip);
        text_out += strip;
        text_out += "</th>";
      }
      text_out += "</tr>\n";
    }

    // Do the rest
    text_out += "<tr>";
    for (a=0; a<buflen; a++) {
      text_out += (headers ? "<th>" : "<td>");
      text_out += "<small>";
      text_out += buf[a];
      buf[a] = (char *)NULL;
      text_out += "</small>";
      text_out += (headers ? "</th>" : "</td>");
    }
    text_out += "</tr>\n";

  } else {
    char fmt[80];
    int colwid = (*settings)["tw"].u / buflen;
    if (colwid < 2)
      return;
    char *tbuf = (char *) malloc (colwid+1);
    sprintf (fmt, "%%-%d.%ds ", colwid-1, colwid-1);
    isdone = 0;
    while (!isdone) {
      Dstr blankquash;
      isdone = 1;
      for (a=0; a<buflen; a++) {
	if (buf[a].length())
	  isdone = 0;
	Dstr strip;
	buf[a].getline (strip);
	sprintf (tbuf, fmt, strip.aschar());
	blankquash += tbuf;
      }
      blankquash += '\n';
      if (oldcal || headers || !isdone)
        text_out += blankquash;
    }
    free (tbuf);
  }
}

void Calendar::print_oldcal_ht (Dstr &text_out, char form) {
  Dstr weekbuf[7];
  int month = -1;
  Timestamp tm = start_day_start;
  while (tm <= end_day_start) {
    struct tm *tmtm = tm.get_tm (timezone, settings);
    int wday = tmtm->tm_wday;
    if (tmtm->tm_mon != month) {
      if (month != -1) {
	flush_buf (text_out, weekbuf, 7, form, 1);
	if (form == 'h')
	  text_out += "</table>\n";
      }
      month = tmtm->tm_mon;
      add_month_banner (text_out, tm, form);
    } else if (wday == 0) {
      flush_buf (text_out, weekbuf, 7, form, 1);
    }

    Dstr temp;
    tm.printdayheading (temp, timezone, settings);
    weekbuf[wday] += temp;
    weekbuf[wday] += '\n';

    Timestamp next_day_start (tm);
    next_day_start.inc_day (timezone, settings);

    TideEventsIterator it = organizer.lower_bound (tm.timet());
    if (it != organizer.end()) {
      TideEventsIterator stop = organizer.lower_bound (next_day_start.timet());
      while (it != stop) {
        TideEvent &te = it->second;
	weekbuf[wday] += te.longdesc ();
	if (!te.isSunMoonEvent()) {
	  te.pv.printnp (temp);
	  weekbuf[wday] += ' ';
	  weekbuf[wday] += temp;
	}
	if (form == 'h')
	  weekbuf[wday] += "<br>";
	weekbuf[wday] += '\n';
	te.tm.printtime (temp, timezone, settings);
	weekbuf[wday] += temp;
	if (form == 'h')
	  weekbuf[wday] += "<br>";
	weekbuf[wday] += '\n';
        it++;
      }
    }
    tm = next_day_start;
  }
  flush_buf (text_out, weekbuf, 7, form, 1);
}

void Calendar::print_newcal_ht (Dstr &text_out, char form) {
  int a;

  // nns = NOT nosunmoon
  // Value is number of columns needed
  int nns = 5;
  if ((*settings)["ns"].c != 'n')
    nns = 0;

  // Day ... [Mark transitions]? Surises Sunsets Moonphases
  // Tides: ... = High [Low High]+
  // Currents: ... = Slack Flood Slack [Ebb Slack Flood Slack]+
  // For Tides, X = max number of low tides in a day
  // For Currents, X = max number of max ebbs in a day
  // Exception:  ebbs with no intervening slack count as one

  // Pathological behaviors tests:

  // Aransas Pass, Texas Current (diurnal) routinely has Min Floods
  // and Min Ebbs.

  // On 2004-01-28, you get a double helping:

  // 2004-01-28  9:48 AM CST   0.44 knots  Max Flood
  // 2004-01-28 11:26 AM CST   0.41 knots  Min Flood
  // 2004-01-28 11:38 AM CST   Moonrise
  // 2004-01-28  2:01 PM CST   0.50 knots  Max Flood
  // 2004-01-28  6:05 PM CST   Sunset
  // 2004-01-28  6:18 PM CST   0.24 knots  Min Flood
  // 2004-01-28  7:24 PM CST   0.25 knots  Max Flood

  // South Point, Washington Current
  // 2004-03-01 has a min ebb.
  // 2004-03-02 has the slack preceeding a flood happening AFTER
  // max flood, thanks to (A) flood being near 0 kt and (B) bad
  // offsets.

  // 2004-03-02  5:45 AM PST  -0.55 knots  Max Ebb
  // 2004-03-02  6:48 AM PST   Sunrise
  // 2004-03-02 10:28 AM PST   0.01 knots  Max Flood
  // 2004-03-02 10:50 AM PST   0.00 knots  Slack, Flood Begins
  // 2004-03-02 11:33 AM PST  -0.00 knots  Slack, Ebb Begins
  // 2004-03-02  1:02 PM PST   Moonrise
  // 2004-03-02  4:50 PM PST  -1.04 knots  Max Ebb

  // <subordinatestation name="South Point, Washington Current"
  // 	pedigree="August Hahn (West Marine tide book for Puget Sound)"
  // 	latitude="47.8500"
  // 	longitude="-122.5833"
  // 	timezone=":America/Los_Angeles"
  // 	reference="Admiralty Inlet, Washington Current">
  // <offsets>
  // 	<max>
  // 		<timeadd value="-0:44"/>
  // 		<levelmultiply value="0.4"/>
  // 	</max><min>
  // 		<timeadd value="-0:29"/>
  // 		<levelmultiply value="0.4"/>
  // 	</min>
  // 	<floodbegins value="0:00"/>
  // 	<ebbbegins value="0:00"/>
  // </offsets>
  // </subordinatestation>

  // Find the value of "X" and check for mark transitions.
  int X = 1;
  int havemarks = 0;

  // Sufficiently perverse orderings of tide events may break this
  // logic.  It works for the South Point test case.

  Timestamp tm = start_day_start;
  while (tm <= end_day_start) {
    Timestamp next_day_start (tm);
    next_day_start.inc_day (timezone, settings);
    int tx = 0;
    int givemeslack = 0;
    TideEventsIterator it = organizer.lower_bound (tm.timet());
    if (it != organizer.end()) {
      TideEventsIterator stop = organizer.lower_bound (next_day_start.timet());
      while (it != stop) {
        TideEvent &te = it->second;
	switch (te.etype) {
	case TideEvent::min:
	  if (!te.isMinCurrentEvent() && !givemeslack) {
	    tx++;
	    if (isCurrent)
	      givemeslack = 1;
	  }
	  break;
	case TideEvent::max:
	  if (te.isMinCurrentEvent() && !givemeslack) {
	    tx++;
            assert (isCurrent);
            givemeslack = 1;
	  }
	  break;
	case TideEvent::slackrise:
	  givemeslack = 0;
	  break;
	case TideEvent::markrise:
	case TideEvent::markfall:
	  havemarks = 1;
	default:
	  ;
	}
        it++;
      }
      if (tx > X)
	X = tx;
    }
    tm = next_day_start;
  }

  int numcol = (isCurrent ? 3+X*4 : 1+X*2);
  // Col. 0 is day
  // Cols. 1 .. numcol are tides/currents
  // Col. numcol+1 is optionally mark 
  // numcol + havemarks + 1 is moon phase
  // numcol + havemarks + 2 is sunrise
  // numcol + havemarks + 3 is sunset
  // numcol + havemarks + 4 is moonrise
  // numcol + havemarks + 5 is moonset
  // The usually blank phase column makes a natural separator between
  // the tide/current times and the sunrise/sunset times.
  Dstr *colbuf = new Dstr[numcol+7];

  int month = -1;
  tm = start_day_start;
  while (tm <= end_day_start) {
    struct tm *tmtm = tm.get_tm (timezone, settings);
    if (tmtm->tm_mon != month) {
      if (month != -1)
	text_out += (form == 'h' ? "</table>\n" : "\n");
      month = tmtm->tm_mon;
      add_month_banner (text_out, tm, form);

      {
	int i = 0;
	colbuf[i++] = "Day";
	if (isCurrent) {
	  colbuf[i] = (form == 'h' ? "<br>\n" : "\n");
	  colbuf[i++] += "Slack";
	  colbuf[i++] = "Flood";
	  colbuf[i] = (form == 'h' ? "<br>\n" : "\n");
	  colbuf[i++] += "Slack";
	} else {
	  colbuf[i++] = "High";
	}
	for (a=0; a<X; a++) {
	  if (isCurrent) {
	    colbuf[i] = (form == 'h' ? "<br>\n<br>\n" : "\n\n");
	    colbuf[i++] += "Ebb";
	    colbuf[i] = (form == 'h' ? "<br>\n" : "\n");
	    colbuf[i++] += "Slack";
	    colbuf[i++] = "Flood";
	    colbuf[i] = (form == 'h' ? "<br>\n" : "\n");
	    colbuf[i++] += "Slack";
	  } else {
	    colbuf[i] = (form == 'h' ? "<br>\n" : "\n");
	    colbuf[i++] += "Low";
	    colbuf[i++] = "High";
	  }
	}
	if (havemarks)
	  colbuf[i++] = "Mark";
	if (nns) {
	  colbuf[i++] = "Phase";
	  colbuf[i++] = "Sunrise";
	  colbuf[i++] = "Sunset";
	  colbuf[i++] = "Moonrise";
	  colbuf[i++] = "Moonset";
	}
      }
      flush_buf (text_out, colbuf, numcol+havemarks+nns+1, form, 0, 1);
    }

    Dstr temp;
    tm.printdayheading (temp, timezone, settings);
    if (form == 'h')
      colbuf[0] += "<b>";
    colbuf[0] += temp;
    if (form == 'h')
      colbuf[0] += "</b>";

    // Tidecol X maps to colbuf element X+1
    int tidecol = 0;

    Timestamp next_day_start (tm);
    next_day_start.inc_day (timezone, settings);

    TideEventsIterator it = organizer.lower_bound (tm.timet());
    if (it != organizer.end()) {
      TideEventsIterator stop = organizer.lower_bound (next_day_start.timet());
      while (it != stop) {
        TideEvent &te = it->second;
	switch (te.etype) {

	// For currents, we have the exception case of Min Floods and
	// Min Ebbs to deal with.  The combination Max, Min, Max is
	// crammed into one table cell.

        // Also, if we get "Slack, Flood Begins" right after Max
        // Flood, or "Slack, Ebb Begins" right after Max Ebb, the
        // station is screwed up, yes, but the correct behavior here
        // is to back up to the preceding slack, not go forward.

        // I hope that is as perverse as it gets, but I'm not betting
        // on it.

	// There is a lot of duplicated code here, but I find it more
	// understandable when it's all spelled out like this.

	case TideEvent::max:
	  if (isCurrent) {
	    if (te.isMinCurrentEvent()) {
	      while ((tidecol+1) % 4)
		tidecol++;
	    } else {
	      while ((tidecol+3) % 4)
		tidecol++;
	    }
	    assert (tidecol < numcol);
	    if (colbuf[tidecol+1].length())
	      colbuf[tidecol+1] += (form == 'h' ? "<br>\n" : "\n");
	  } else {
	    while (tidecol % 2)
	      tidecol++;
	    assert (tidecol < numcol);
	    assert (!(colbuf[tidecol+1].length()));
	  }
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[tidecol+1] += temp;
	  colbuf[tidecol+1] += " / ";
	  te.pv.printnp (temp);
	  colbuf[tidecol+1] += temp;
	  break;

	case TideEvent::min:
	  if (isCurrent) {
	    if (te.isMinCurrentEvent()) {
	      while ((tidecol+3) % 4)
		tidecol++;
	    } else {
	      while ((tidecol+1) % 4)
		tidecol++;
	    }
	    assert (tidecol < numcol);
	    if (colbuf[tidecol+1].length())
	      colbuf[tidecol+1] += (form == 'h' ? "<br>\n" : "\n");
	  } else {
	    while ((tidecol+1) % 2)
	      tidecol++;
	    assert (tidecol < numcol);
	    assert (!(colbuf[tidecol+1].length()));
	  }
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[tidecol+1] += temp;
	  colbuf[tidecol+1] += " / ";
	  te.pv.printnp (temp);
	  colbuf[tidecol+1] += temp;
	  break;

	case TideEvent::slackrise:
	  assert (isCurrent);
          // Special case: if slackrise follows max, back up.
          if ((tidecol+3) % 4 == 0)
            tidecol--;
          else
  	    while (tidecol % 4)
	      tidecol++;
	  assert (tidecol < numcol);
	  assert (!(colbuf[tidecol+1].length()));
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[tidecol+1] += temp;
	  break;

	case TideEvent::slackfall:
	  assert (isCurrent);
          // Special case: if slackfall follows min, back up.
          if ((tidecol+1) % 4 == 0)
            tidecol--;
          else
  	    while ((tidecol+2) % 4)
	      tidecol++;
	  assert (tidecol < numcol);
	  assert (!(colbuf[tidecol+1].length()));
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[tidecol+1] += temp;
	  break;

	case TideEvent::markrise:
	case TideEvent::markfall:
	  assert (havemarks);
	  if (colbuf[numcol+1].length())
	    colbuf[numcol+1] += (form == 'h' ? "<br>\n" : "\n");
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[numcol+1] += temp;
	  break;

	case TideEvent::sunrise:
	  assert (nns);
	  if (colbuf[numcol+havemarks+2].length())
	    colbuf[numcol+havemarks+2] += (form == 'h' ? "<br>\n" : "\n");
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[numcol+havemarks+2] += temp;
	  break;

	case TideEvent::sunset:
	  assert (nns);
	  if (colbuf[numcol+havemarks+3].length())
	    colbuf[numcol+havemarks+3] += (form == 'h' ? "<br>\n" : "\n");
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[numcol+havemarks+3] += temp;
	  break;

	case TideEvent::moonrise:
	  assert (nns);
	  if (colbuf[numcol+havemarks+4].length())
	    colbuf[numcol+havemarks+4] += (form == 'h' ? "<br>\n" : "\n");
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[numcol+havemarks+4] += temp;
	  break;

	case TideEvent::moonset:
	  assert (nns);
	  if (colbuf[numcol+havemarks+5].length())
	    colbuf[numcol+havemarks+5] += (form == 'h' ? "<br>\n" : "\n");
	  te.tm.printtime (temp, timezone, settings);
	  colbuf[numcol+havemarks+5] += temp;
	  break;

	case TideEvent::newmoon:
	case TideEvent::firstquarter:
	case TideEvent::fullmoon:
	case TideEvent::lastquarter:
	  assert (nns);
	  assert (!(colbuf[numcol+havemarks+1].length()));
	  colbuf[numcol+havemarks+1] += te.longdesc();
	  break;

	//default:
	  //assert (0);
	}
        it++;
      }
      // Now print the day.
      flush_buf (text_out, colbuf, numcol+havemarks+nns+1, form, 0);
    }
    tm = next_day_start;
    for (a=0; a<numcol+5; a++)
      colbuf[a] = (char *)NULL;
  }
  delete [] colbuf;
}

void Calendar::add_csv_event (TideEvent *events, unsigned &i, unsigned limit,
TideEvent b, Dstr &date, char *desc) {
  if (i == limit)
    fprintf (stderr, "Warning:  too many %s events on %s; increase calcsv_nummaxmin or calcsv_numriseset in config.hh\n", desc, date.aschar());
  else
    events[i++] = b;
}

void Calendar::print_csv_maxmin (Dstr &text_out, TideEvent *events, unsigned n,
				 unsigned limit) {
  unsigned i;
  Dstr t;
  for (i=0; i<limit; i++) {
    text_out += ',';
    if (i < n) {
      events[i].tm.printtime (t, timezone, settings);
      text_out += t;
      text_out += ',';
      events[i].pv.printnp (t);
      text_out += t;
    } else
      text_out += ',';
  }
}

void Calendar::print_csv_other (Dstr &text_out, TideEvent *events, unsigned n,
  			        unsigned limit) {
  unsigned i;
  Dstr t;
  for (i=0; i<limit; i++) {
    text_out += ',';
    if (i < n) {
      events[i].tm.printtime (t, timezone, settings);
      text_out += t;
    }
  }
}

void Calendar::print_newcal_c  (Dstr &text_out) {
  Dstr locname (station->name);
  locname.repchar (',', csv_repchar);
  Timestamp tm = start_day_start;
  while (tm <= end_day_start) {
    Timestamp next_day_start (tm);
    next_day_start.inc_day (timezone, settings);

    Dstr date;
    tm.printdate (date, timezone, settings);
    TideEvent maxes[calcsv_nummaxmin], mins[calcsv_nummaxmin],
      slacks[2*calcsv_nummaxmin], sunrises[calcsv_numriseset],
      sunsets[calcsv_numriseset], moonrises[calcsv_numriseset],
      moonsets[calcsv_numriseset];
    unsigned nummax=0,nummin=0,numslack=0,numsunrise=0,numsunset=0,
      nummoonrise=0,nummoonset=0;

    TideEventsIterator it = organizer.lower_bound (tm.timet());
    if (it != organizer.end()) {
      TideEventsIterator stop = organizer.lower_bound (next_day_start.timet());
      while (it != stop) {
        TideEvent &te = it->second;
	switch (te.etype) {
	case TideEvent::max:
	  add_csv_event (maxes, nummax, calcsv_nummaxmin, te, date, "max");
	  break;
	case TideEvent::min:
	  add_csv_event (mins, nummin, calcsv_nummaxmin, te, date, "min");
	  break;
	case TideEvent::slackrise:
	case TideEvent::slackfall:
	  add_csv_event (slacks, numslack, 2*calcsv_nummaxmin, te, date, "slack");
	  break;
	case TideEvent::sunrise:
	  add_csv_event (sunrises, numsunrise, calcsv_numriseset, te, date, "sunrise");
	  break;
	case TideEvent::sunset:
	  add_csv_event (sunsets, numsunset, calcsv_numriseset, te, date, "sunset");
	  break;
	case TideEvent::moonrise:
	  add_csv_event (moonrises, nummoonrise, calcsv_numriseset, te, date, "moonrise");
	  break;
	case TideEvent::moonset:
	  add_csv_event (moonsets, nummoonset, calcsv_numriseset, te, date, "moonset");
	  break;
	default:
	  ;
	}
	it++;
      }
      text_out += locname;
      text_out += ',';
      text_out += date;
      print_csv_maxmin (text_out, maxes, nummax, calcsv_nummaxmin);
      print_csv_maxmin (text_out, mins, nummin, calcsv_nummaxmin);
      print_csv_other (text_out, slacks, numslack, 2*calcsv_nummaxmin);
      print_csv_other (text_out, sunrises, numsunrise, calcsv_numriseset);
      print_csv_other (text_out, sunsets, numsunset, calcsv_numriseset);
      print_csv_other (text_out, moonrises, nummoonrise, calcsv_numriseset);
      print_csv_other (text_out, moonsets, nummoonset, calcsv_numriseset);
      text_out += '\n';
    }
    tm = next_day_start;
  }
}

void Calendar::print (Dstr &text_out, char form, int oldcal) {
  text_out = (char *)NULL;
  if (form == 'h')
    text_out += "<center>\n";
  if (oldcal) {
    print_oldcal_ht (text_out, form);
  } else {
    if (form == 'c')
      print_newcal_c (text_out);
    else 
      print_newcal_ht (text_out, form);
  }
  if (form == 'h')
    text_out += "</table></center>\n";
}
