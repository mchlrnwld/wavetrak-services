// $Id: NullableAngle.cc,v 1.2 2004/10/19 18:08:06 flaterco Exp $
// NullableAngle:  Angle extended to have a null indicator.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

NullableAngle::NullableAngle(): Angle() {
  isnull = 1;
}

int NullableAngle::isNull() const {
  return isnull;
}

NullableAngle::NullableAngle (units u, double v): Angle (u,v)
{
  isnull = 0;
}

NullableAngle::NullableAngle (units u, qualifier q, double v): Angle (u,q,v)
{
  isnull = 0;
}
