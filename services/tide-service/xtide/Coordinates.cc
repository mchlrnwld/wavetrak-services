// $Id: Coordinates.cc,v 1.3 2004/04/06 14:02:54 flaterco Exp $
/*  Coordinates   Degrees latitude and longitude.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Coordinates::Coordinates () {
  latitude = longitude = 0.0;
}

Coordinates::Coordinates (double lat_in, double lng_in) {
  if (lat_in < -90.0 || lat_in > 90.0 || lng_in < -180.0 || lng_in > 180.0) {
    Dstr details ("The offending coordinates were (");
    details += lat_in;
    details += ",";
    details += lng_in;
    details += ").";
    barf (BOGUS_COORDINATES, details);
  }
  latitude = lat_in;
  longitude = lng_in;
}

void Coordinates::lat (double lat_in) {
  if (lat_in < -90.0 || lat_in > 90.0) {
    Dstr details ("The offending latitude was ");
    details += lat_in;
    details += ".";
    barf (BOGUS_COORDINATES, details);
  }
  latitude = lat_in;
}

void Coordinates::lng (double lng_in) {
  if (lng_in < -180.0 || lng_in > 180.0) {
    Dstr details ("The offending longitude was ");
    details += lng_in;
    details += ".";
    barf (BOGUS_COORDINATES, details);
  }
  longitude = lng_in;
}

double Coordinates::lat() const {
  return latitude;
}

double Coordinates::lng() const {
  return longitude;
}

int Coordinates::isNull() const {
  if (latitude == 0.0 && longitude == 0.0)
    return 1;
  return 0;
}

void Coordinates::print (Dstr &out, int pad) const {
  if (isNull())
    out = "NULL";
  else {
    double latn = latitude, lngn = longitude;
    char latc, lngc;
    if (latn < 0.0) {
      latn = -latn;
      latc = 'S';
    } else
      latc = 'N';
    if (lngn < 0.0) {
      lngn = -lngn;
      lngc = 'W';
    } else
      lngc = 'E';
    char temp[80];
    // \260 is ISO Latin 1 degrees symbol
    if (pad)
      sprintf (temp, "%7.4f\260 %c, %8.4f\260 %c", latn, latc, lngn, lngc);
    else
      sprintf (temp, "%6.4f\260 %c, %6.4f\260 %c", latn, latc, lngn, lngc);
    out = temp;
  }
}

void Coordinates::printlat (Dstr &out) const {
  if (isNull())
    out = "NULL";
  else {
    double latn = latitude;
    char latc;
    if (latn < 0.0) {
      latn = -latn;
      latc = 'S';
    } else
      latc = 'N';
    char temp[80];
    // \260 is ISO Latin 1 degrees symbol
    sprintf (temp, "%3.1f\260 %c", latn, latc);
    out = temp;
  }
}

void Coordinates::printlng (Dstr &out) const {
  if (isNull())
    out = "NULL";
  else {
    double lngn = longitude;
    char lngc;
    if (lngn < 0.0) {
      lngn = -lngn;
      lngc = 'W';
    } else
      lngc = 'E';
    char temp[80];
    // \260 is ISO Latin 1 degrees symbol
    sprintf (temp, "%3.1f\260 %c", lngn, lngc);
    out = temp;
  }
}
