// $Id: Interval.hh,v 1.4 2004/10/21 19:58:13 flaterco Exp $
// Interval:  what you get if you subtract two timestamps.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// Since XTide now roams outside of the minimal 1970 to 2037 epoch,
// 32 bits no longer suffice.
#define interval_rep_t long long

class Interval {
protected:

  interval_rep_t seconds;

public:
  Interval ();
  Interval (interval_rep_t in_seconds);

  // Takes meridian string of the form [-]HH:MM
  Interval (const Dstr &in_meridian);

  // Output value in various units.
  interval_rep_t in_seconds () const;
  // Convert seconds to Julian centuries (36525 days)
  double in_Julian_centuries () const;

  // Return in the form [-]HH:MM:SS
  // void as_hhmmss (Dstr &hhmmss_out);

  void operator -= (Interval a);
  void operator *= (unsigned a);
};

Angle operator* (Interval a, Speed b);
Angle operator* (Speed b, Interval a);
Interval operator* (Interval a, unsigned b);
Interval operator* (Interval a, double b);
Interval operator+ (Interval a, Interval b);
Interval operator- (Interval a, Interval b);
Interval abs (Interval a);
Interval operator- (Interval a);
int operator> (Interval a, Interval b);
int operator< (Interval a, Interval b);
int operator<= (Interval a, Interval b);
int operator== (Interval a, Interval b);
Interval operator/ (Interval a, int b);
double operator/ (Interval a, Interval b);
