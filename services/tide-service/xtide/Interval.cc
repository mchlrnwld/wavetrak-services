// $Id: Interval.cc,v 1.4 2004/10/21 19:58:13 flaterco Exp $
// Interval:  what you get if you subtract two timestamps.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Interval::Interval (interval_rep_t in_seconds) {
  seconds = in_seconds;
}

Interval::Interval () {
  seconds = 0;
}

// Takes meridian string of the form [-]HH:MM
Interval::Interval (const Dstr &in_meridian) {
  int h, m;
  char s;
  char *hhmm = in_meridian.aschar();
  if (sscanf (hhmm, "%d:%d", &h, &m) != 2) {
    Dstr details ("The offending string was ");
    details += in_meridian;
    barf (BADHHMM, details);
  }
  if (sscanf (hhmm, "%c", &s) != 1) {
    Dstr details ("The offending string was ");
    details += in_meridian;
    barf (BADHHMM, details);
  }
  if (h < 0 || s == '-')
    m = -m;
  seconds = h*HOURSECONDS + m*60;
}

interval_rep_t Interval::in_seconds () const {
  return seconds;
}

// Convert seconds to Julian centuries (36525 days)
double Interval::in_Julian_centuries () const {
  return ((double)seconds) / 3155760000.0;
}

Interval operator * (Interval a, unsigned b) {
  return Interval (a.in_seconds() * b);
}

Interval operator * (Interval a, double b) {
  return Interval ((interval_rep_t)(a.in_seconds() * b));
}

Angle operator* (Interval a, Speed b) {
  return Radians (a.in_seconds() * b.rad (Speed::SECOND));
}

Angle operator* (Speed b, Interval a) {
  return a * b;
}

Interval abs (Interval a) {
  return Interval (labs (a.in_seconds()));
}

Interval operator- (Interval a) {
  return Interval (- (a.in_seconds()));
}

int operator> (Interval a, Interval b) {
  if (a.in_seconds() > b.in_seconds())
    return 1;
  return 0;
}

int operator< (Interval a, Interval b) {
  if (a.in_seconds() < b.in_seconds())
    return 1;
  return 0;
}

int operator<= (Interval a, Interval b) {
  if (a.in_seconds() <= b.in_seconds())
    return 1;
  return 0;
}

int operator== (Interval a, Interval b) {
  if (a.in_seconds() == b.in_seconds())
    return 1;
  return 0;
}

Interval operator/ (Interval a, int b) {
  return Interval (a.in_seconds() / b);
}

double operator/ (Interval a, Interval b) {
  return ((double)(a.in_seconds()) / (double)(b.in_seconds()));
}

Interval operator+ (Interval a, Interval b) {
  return Interval (a.in_seconds() + b.in_seconds());
}

Interval operator- (Interval a, Interval b) {
  // Potential overflow/underflow
  return Interval (a.in_seconds() - b.in_seconds());
}

void Interval::operator-= (Interval a) {
  // Potential overflow/underflow
  seconds -= a.in_seconds();
}

void Interval::operator*= (unsigned a) {
  // Potential overflow
  seconds *= a;
}

#if 0
// This is not used anymore so it's not worth fixing the overflow.
void
Interval::as_hhmmss (Dstr &hhmmss_out) {
  interval_rep_t temp = seconds;
  hhmmss_out = (char *)NULL;
  if (temp < 0) {
    hhmmss_out += '-';
    temp = -temp;
  }
  long hours = temp / HOURSECONDS;
  temp %= HOURSECONDS;
  long minutes = temp / 60;
  long secs = temp % 60;
  if (hours < 10)
    hhmmss_out += '0';
  hhmmss_out += hours;
  hhmmss_out += ':';
  if (minutes < 10)
    hhmmss_out += '0';
  hhmmss_out += minutes;
  hhmmss_out += ':';
  if (secs < 10)
    hhmmss_out += '0';
  hhmmss_out += secs;
}

ostream &operator<< (ostream &out, Interval a) {
  Dstr temp;
  a.as_hhmmss (temp);
  out << temp;
  return out;
}
#endif
