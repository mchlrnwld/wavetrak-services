// $Id: Year.cc,v 1.2 2002/12/19 18:40:45 flaterco Exp $
// Year

// The original rationale for this class had to do with enforcing the
// epoch limitation (1970-2037).  Now it's pretty pointless except
// maybe for overload resolution.

/*
    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Year::Year () {
  value = 0;
}

Year::Year (unsigned year) {
  // If they tried to specify a negative year it would show up as a big
  // positive number, so don't confuse them with details.
  if (year < 1 || year > 4000)
    barf (YEAR_OUT_OF_RANGE);
  value = year;
}

unsigned
Year::val () {
  if (value == 0)
    barf (NULL_YEAR);
  return value;
}


Year operator+ (Year y, int n) {
  return Year (y.val() + n);
}

Year operator- (Year y, int n) {
  return Year (y.val() - n);
}

int operator < (Year y1, Year y2) {
  if (y1.val() < y2.val())
    return 1;
  return 0;
}

int operator > (Year y1, Year y2) {
  if (y1.val() > y2.val())
    return 1;
  return 0;
}

int operator >= (Year y1, Year y2) {
  if (y1.val() >= y2.val())
    return 1;
  return 0;
}

int operator <= (Year y1, Year y2) {
  if (y1.val() <= y2.val())
    return 1;
  return 0;
}

int operator == (Year y1, Year y2) {
  if (y1.value == y2.value)
    return 1;
  return 0;
}

int operator != (Year y1, Year y2) {
  return (!(y1 == y2));
}

//Year &Year::operator++ () {
//  (*this) = Year (value+1);  // Use integrity checks in constructor
//  return (*this);
//}

int Year::isNull() {
  if (!value)
    return 1;
  return 0;
}
