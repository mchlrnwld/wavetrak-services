// $Id: xmlstruct.hh,v 1.2 2002/10/04 13:44:01 flaterco Exp $
/*  xmlstruct  Structures for storing parse tree produced by
    limited XML parser (just tags, no text).

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

struct xmlattribute {
  Dstr *name;
  Dstr *value;
  struct xmlattribute *next;
};

struct xmltag {
  Dstr *name;
  struct xmlattribute *attributes;
  struct xmltag *contents;
  struct xmltag *next;
};

void freexml (struct xmltag *v);
void freexml (struct xmlattribute *v);
