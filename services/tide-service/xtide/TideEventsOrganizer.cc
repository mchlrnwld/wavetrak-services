// $Id: TideEventsOrganizer.cc,v 1.3 2004/04/06 21:20:07 flaterco Exp $

// TideEventsOrganizer  Collect, sort, subset, and iterate over tide events.
// Refactored from similar notions previously appearing in
// TideContext, Calendar, Station, xxTextMode, Graph, ...

/*
    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "common.hh"

// Add a TideEvent, making moderate effort to suppress duplicates.
// An event is considered a duplicate if difference in times is < 60
// seconds and event type is the same.
void TideEventsOrganizer::add (const TideEvent &te) {
  time_t t = te.tm.timet();
  TideEventsIterator it = lower_bound (t - 59);
  if (it != end()) {
    TideEventsIterator stop = upper_bound (t + 59); // or end()
    while (it != stop) {
      if (te.etype == it->second.etype)
        return;
      it++;
    }
  }
  insert (TideEventsPair(t, te));
}
