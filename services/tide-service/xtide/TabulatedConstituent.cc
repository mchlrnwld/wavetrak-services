// $Id: TabulatedConstituent.cc,v 1.7 2004/10/04 13:57:50 flaterco Exp $
// TabulatedConstituent:  definition of a constituent that uses tabulated
// equilibrium arguments and node factors.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

TabulatedConstituent::Argfac::Argfac () {
  arg = Radians (0.0);
  nod = 1.0;
}

TabulatedConstituent::Argfac::Argfac (Angle in_arg, double in_nod) {
  arg = in_arg;
  nod = in_nod;
  assert (in_nod > 0.0);
}

TabulatedConstituent::TabulatedConstituent () {
  myspeed = DegreesPerHour (0.0);
  myargfacs = NULL;
}

TabulatedConstituent &
TabulatedConstituent::operator= (TabulatedConstituent &in_con) {
  if (myargfacs)
    delete [] myargfacs;
  myspeed = in_con.speed();
  myfirstvalidyear = in_con.firstvalidyear();
  mylastvalidyear = in_con.lastvalidyear();
  nargs = mylastvalidyear.val() - myfirstvalidyear.val() + 1;
  assert (myargfacs = new Argfac [nargs]);
  for (unsigned looper=0; looper<nargs; looper++) {
    Year ty (myfirstvalidyear + looper);
    myargfacs[looper].arg = in_con.arg(ty);
    myargfacs[looper].nod = in_con.nod(ty);
  }
  return (*this);
}

void
TabulatedConstituent::speed (Speed in_speed) {
  myspeed = in_speed;
}

// This should be redundant -- workaround for bug in
// Sun WorkShop Compiler C++ SPARC Version 5.000
// Error: Could not find a match for TabulatedConstituent::speed(DegreesPerHour).
void
TabulatedConstituent::speed (DegreesPerHour in_speed) {
  myspeed = in_speed;
}

Speed TabulatedConstituent::speed () {
  return myspeed;
}

Year
TabulatedConstituent::firstvalidyear() {
  return myfirstvalidyear;
}

Year
TabulatedConstituent::lastvalidyear() {
  return mylastvalidyear;
}

void
TabulatedConstituent::check_valid (Year in_year) {
  if (in_year < myfirstvalidyear ||
      in_year > mylastvalidyear) {
    Dstr details ("The years supported by the harmonics file are ");
    details += (int)myfirstvalidyear.val();
    details += " through ";
    details += (int)mylastvalidyear.val();
    details += ".\n";
    details += "The offending year was ";
    details += (int)in_year.val();
    details += ".\n";
    barf (YEAR_NOT_IN_TABLE, details);
  }
}

Angle
TabulatedConstituent::arg (Year in_year) {
  check_valid (in_year);
  assert (myargfacs);
  return myargfacs[in_year.val()-myfirstvalidyear.val()].arg;
}

double
TabulatedConstituent::nod (Year in_year) {
  check_valid (in_year);
  assert (myargfacs);
  return myargfacs[in_year.val()-myfirstvalidyear.val()].nod;
}

// Install equilibrium arguments and node factors.  N.B. single-precision
// floats on input to match libtcd; otherwise never used in XTide.
// args are in degrees.
void TabulatedConstituent::installargsandnodes (Year first_year,
Year last_year, float *args, float *nodes) {
  assert (!myargfacs);
  assert (last_year >= first_year);
  nargs = last_year.val() - first_year.val() + 1;
  assert (myargfacs = new Argfac [nargs]);
  myfirstvalidyear = first_year;
  mylastvalidyear = last_year;
  for (unsigned looper=0; looper<nargs; looper++) {
    myargfacs[looper].arg = Degrees(args[looper]);
    myargfacs[looper].nod = nodes[looper];
  }
}

TabulatedConstituent::~TabulatedConstituent() {
  if (myargfacs)
    delete [] myargfacs;
}
