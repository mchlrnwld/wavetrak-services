// $Id: xxWindow.cc,v 1.5 2004/04/08 14:25:04 flaterco Exp $
/*  xxWindow  Abstract class for all XTide windows implementing dismiss().

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

xxWindow::xxWindow (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
int needcontainer, XtGrabKind in_grabkind) {
  containertype = needcontainer;
  xtidecontext = in_tidecontext;
  xtidecontext->root->dup (this);
  mypopup = new xxContext (in_xxcontext, in_grabkind);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (mypopup->manager, args, 2);
  XtAddEventHandler (mypopup->manager, NoEventMask, True,
    xxWindowCloseHandler, (XtPointer)this);
  switch (needcontainer) {
  case 0:
    container = NULL;
    break;
  case 1:
    {
      Widget boxwidget = XtCreateManagedWidget ("", boxWidgetClass,
        mypopup->manager, args, 2);
      container = new xxContext (mypopup, boxwidget);
    }
    break;
  case 2:
    {
      Widget formwidget = XtCreateManagedWidget ("", formWidgetClass,
        mypopup->manager, args, 2);
      container = new xxContext (mypopup, formwidget);
    }
    break;
  default:
    assert (0);
  }
  is_title_screen = noclose = 0;
}

void xxWindowCloseHandler (Widget w, XtPointer client_data,
			   XEvent *event, Boolean *continue_dispatch) {
  xxWindow *f = (xxWindow *)client_data;
  if (f->noclose)
    return;
  switch (event->type) {
  case ClientMessage:
    {
      XClientMessageEvent *ev = (XClientMessageEvent *) event;
      // Window manager close.
      if (ev->message_type == f->mypopup->protocol_atom &&
	  ev->data.l[0] == (int)(f->mypopup->kill_atom))
        f->dismiss();
    }
    break;
  default:
    ;
  }
}

xxWindow::~xxWindow() {
  mypopup->unrealize();
  xtidecontext->root->release (this);
  if (container)
    delete container;
  delete mypopup;
}

void xxWindow::move (Dstr &geometry) {
  int xoff, yoff, negx=0, negy=0;
  if (sscanf (geometry.aschar(), "%d%d", &xoff, &yoff) == 2) {
    if (xoff < 0 || (xoff == 0 && geometry[0] == '-'))
      negx = 1;
    if (yoff < 0 || (yoff == 0 && geometry.strrchr('-') > 0))
      negy = 1;
    if (negx || negy) {
      Dimension width, height;
      Arg args[2] = {
	{XtNwidth, (XtArgVal)(&width)},
	{XtNheight, (XtArgVal)(&height)}
      };
      XtGetValues (mypopup->manager, args, 2);
      if (negx)
	xoff += (int)(mypopup->screen_width) - width;
      if (negy)
	yoff += (int)(mypopup->screen_height) - height;
    }
    XMoveWindow (mypopup->display, XtWindow(mypopup->manager), xoff, yoff);
  }
}

void xxWindow::global_redraw() {
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (mypopup->manager, args, 2);
  if (container)
    XtSetValues (container->manager, args, 2);
}
