// $Id: xttpd.cc,v 1.22 2004/10/26 15:54:53 flaterco Exp $
/*  xttpd  XTide web server.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

// These browsers nowadays can get pretty verbose.
#define bufsize 10000

// Nasty global variables.
TideContext *context = NULL;
Dstr webmaster;
ZoneIndex zi;
int zoneinfo_doesnt_suck;

// time control modes

enum TC {	TC_DAY,		// year, month, day
		TC_YEAR,	// year
		TC_MONTH	// year, month
};

#include "purec++.hh"

static int
parse_addr (unsigned short &port_number, unsigned long &addr, char *arg) {
  assert (arg);
  // The : character is reserved for future use to distinguish
  // IPv4 from IPv6 addresses.
  if (strchr (arg, '/') || strchr (arg, '.')) {
    char *c = strrchr (arg, '/');
    if (c) {
      *c = '\0';
      if (sscanf (c+1, "%hu", &port_number) != 1)
        port_number = 80;
    }
    if (!inet_pton (AF_INET, arg, &addr)) {
      Dstr details ("The offending address was ");
      details += arg;
      barf (BAD_IP_ADDRESS, details);
    }
    return 1;
  } else {
    addr = INADDR_ANY;
    if (sscanf (arg, "%hu", &port_number) != 1) {
      port_number = 80;
      return 0;
    }
    return 1;
  }
}

static void
setup_socket (unsigned short port_number, unsigned long addr, int &s)
{
  struct sockaddr_in sin;
  struct hostent *hp;
  struct utsname foo;

  uname (&foo);
  if (!(hp = (struct hostent *) gethostbyname (foo.nodename))) {
    Dstr details ("Gethostbyname fails on own nodename!");
    barf (CANT_GET_SOCKET, details);
  }
  memset ((char *) &sin, '\0', sizeof sin);
  memcpy ((char *) &sin.sin_addr, hp->h_addr, hp->h_length);
  sin.sin_port = htons (port_number);
  sin.sin_family = hp->h_addrtype;
  sin.sin_addr.s_addr = addr;
  if ((s = socket (hp->h_addrtype, SOCK_STREAM, 0)) == -1) {
    Dstr details ("socket: ");
    details += strerror (errno);
    details += ".";
    barf (CANT_GET_SOCKET, details);
  }
  {
    int tmp = 1;
    if (setsockopt (s, SOL_SOCKET, SO_REUSEADDR,
	  	    (char *)&tmp, sizeof(tmp)) < 0) {
      Dstr details ("setsockopt: ");
      details += strerror (errno);
      details += ".";
      barf (CANT_GET_SOCKET, details);
    }
  }{
    struct linger li;
    li.l_onoff = 1;
    li.l_linger = 6000; // Hundredths of seconds (1 min)
    if (setsockopt (s, SOL_SOCKET, SO_LINGER,
                   (char *)&li, sizeof(struct linger)) < 0) {
      xperror ("setsockopt");
    }
  }
  if (bind (s, (struct sockaddr *)(&sin), sizeof sin) == -1) {
    if (errno == EADDRINUSE) {
      Dstr details ("Socket already in use.");
      barf (CANT_GET_SOCKET, details);
    } else {
      Dstr details ("bind: ");
      details += strerror (errno);
      details += ".";
      barf (CANT_GET_SOCKET, details);
    }
  }
  // backlog was 0, but this hung Digital Unix.  5 is the limit for BSD.
  if (listen (s, 5) == -1) {
    Dstr details ("listen: ");
    details += strerror (errno);
    details += ".";
    barf (CANT_GET_SOCKET, details);
  }

  webmaster = getenv ("XTTPD_FEEDBACK");
#ifdef webmasteraddr
  if (webmaster.isNull())
    webmaster = webmasteraddr;
#endif
  if (webmaster == "software@flaterco.com")
    webmaster = (char *)NULL;  // I can't handle the AOLers all by myself.
  if (webmaster.isNull()) {
    // Guess the local webmaster.  (Guessing the FQDN is harder!)
    webmaster = hp->h_name;
    if (webmaster.strchr('.') == -1) {
      // They deleted getdomainname()...  Now what am I supposed to do?
      Dstr domain;
      FILE *fp;
      if ((fp = fopen ("/etc/defaultdomain", "r"))) {
	domain.scan (fp);
	fclose (fp);
	webmaster += ".";
	webmaster += domain;
      } else if ((fp = fopen ("/etc/domain", "r"))) {
	domain.scan (fp);
	fclose (fp);
	webmaster += ".";
	webmaster += domain;
      } else if ((fp = fopen ("/etc/resolv.conf", "r"))) {
	Dstr buf;
	while (!(buf.getline(fp)).isNull()) {
	  buf /= domain;
	  if (domain == "domain") {
	    buf /= domain;
	    webmaster += ".";
	    webmaster += domain;
	    break;
	  }
	}
	fclose (fp);
      }
    }
    if (webmaster %= "www.")
      webmaster /= 4;
    webmaster *= "webmaster@";
  }
  webmaster *= "<a href=\"mailto:";
  webmaster += "\">";
}

static void
add_disclaimer (Dstr &foo) {
  foo += "<center><h2> NOT FOR NAVIGATION </h2></center>\n\
<blockquote>\n\
<p>This program is distributed in the hope that it will be useful, but\n\
WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&nbsp; The author\n\
assumes no liability for damages arising from use of these predictions.&nbsp;\n\
They are not certified to be correct, and they\n\
do not incorporate the effects of tropical storms, El Ni&ntilde;o,\n\
seismic events, continental drift, or changes in global sea level.\n\
</p></blockquote>\n";
}

static void
end_page (int s, int pure = 0) {
  Dstr end_mess ("<hr>");
  if (pure)
    end_mess += "<p><img src=\"/purec++.png\" align=left hspace=25></p>";
  end_mess += "<p> <a href=\"/\">Start over</a> <br>\n";
  end_mess += "<a href=\"/index.html\">Master index</a> <br>\n";
  end_mess += "<a href=\"/zones/\">Zone index</a> <br>\n";
  end_mess += "<a href=\"http://www.flaterco.com/xtide/\">XTide software</a> <br>\n";
  end_mess += webmaster;
  end_mess += "Feedback</a> <br>\n";
  end_mess += "<a href=\"/tricks.html\">Hints and tricks</a></p>\n\
</body>\n\
</html>\n";
  write (s, end_mess.aschar(), end_mess.length());
  close (s);
  exit (0);
}

static void
unimp_barf (int s) {
  char *barf_message = "\
HTTP/1.0 501 Not Implemented\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> HTTP Error </title></head>\n\
<body bgcolor=\"FFFFFF\"><h1> HTTP Error </h1>\n\
<p>So sorry!&nbsp; This server cannot satisfy your request.</p>\n";

  write (s, barf_message, strlen (barf_message));
  end_page (s);
}

static void
notfound_barf (int s) {
  char *barf_message = "\
HTTP/1.0 404 Not Found\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> Not Found </title></head>\n\
<body bgcolor=\"FFFFFF\"><h1> Not Found </h1>\n\
<p>So sorry!&nbsp; This server cannot satisfy your request.</p>\n";

  write (s, barf_message, strlen (barf_message));
  end_page (s);
}

static void
toolong_barf (int s) {
  char *barf_message = "\
HTTP/1.0 413 Request Entity Too Large\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> HTTP Error </title></head>\n\
<body bgcolor=\"FFFFFF\"><h1> HTTP Error </h1>\n\
<p>So sorry!&nbsp; This server cannot satisfy your request.</p>\n";

  write (s, barf_message, strlen (barf_message));
  end_page (s);
}

static void
root_page (int s) {
  Dstr root_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> XTide Tide Prediction Server </title></head>\n\
<body bgcolor=\"FFFFFF\"><center><h1> XTide Tide Prediction Server </h1></center>\n\
\n\
<center>\n\
<p>Copyright (C) 1998\n\
David Flater.</p>\n\
</center>\n");
  add_disclaimer (root_p);
  root_p += "<blockquote>\n\
<p>\n\
While this program is maintained by David Flater, the web site\n\
itself is not.&nbsp; Please direct feedback about problems with the web site to\n\
the local ";
  root_p += webmaster;
  root_p += "webmaster</a>.</p></blockquote>\n\
<p> XTide can provide tide predictions for thousands of places around the\n\
world, but first you have to select a location.&nbsp;\n\
There are three ways to do this.&nbsp; One\n\
way is just to enter its name here, and click on Search: </p>\n\
<p><form method=get action=\"/query\">\n\
<input name=\"location\" type=text size=\"48\">\n\
<input type=submit value=\"Search\"> <input type=reset value=\"Clear\">\n\
</form></p>\n\
\n\
<p> Your other choices are the <a href=\"/zones/\">zone index</a>, which\n\
indexes locations by time zone, and \n\
the <a href=\"index.html\">master index</a>, which is just one big list\n\
of every location supported by this server.&nbsp; The master index could be\n\
very large, so if you are on a slow connection, don't use it.</p>\n\
\n\
<p> All images shipped by this web server are in PNG format.&nbsp; Versions of\n\
Netscape or Internet Explorer older than version 4 will not be able to\n\
display them. </p>\n";

  if (!(context->stationIndex()->hfile_ids.isNull())) {
    root_p += "<p>This server is using the following versions of the XTide data files:<br>";
    root_p += context->stationIndex()->hfile_ids;
    root_p += "</p>\n";
  }

  write (s, root_p.aschar(), root_p.length());
  end_page (s);
}

static void
start_loclist (Dstr &d) {
  context->startLocListHTML (d);
}

// This differs from TideContext::listLocationHTML because we need to
// add hyperlinks.
static void
list_location (Dstr &d, StationIndex *si, unsigned long i) {
  assert (si);
  StationRef *sr = (*si)[i];
  assert (sr);
  d += "<tr><td><a href=\"/locations/";
  d += i;
  d += ".html\">";
  d += sr->name;
  d += "</a></td><td>";
  if (sr->isReferenceStation)
    d += "Ref";
  else
    d += "Sub";
  d += "</td><td>";
  Dstr tempc;
  sr->coordinates.print (tempc);
  d += tempc;
  d += "</td></tr>\n";
}

static void
end_loclist (Dstr &d) {
  context->endLocListHTML (d);
}

static void
index_page (int s) {
  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> XTide Master Index </title></head>\n\
<body bgcolor=\"FFFFFF\"><center><h1> XTide Master Index </h1></center>\n\
\n\
<p>Click on any location to get tide predictions.</p>\n");
  start_loclist (top_p);
  unsigned long i;
  StationIndex *si = context->stationIndex();
  for (i=0; i<si->length(); i++) {
    list_location (top_p, si, i);
    if ((i % maxhtmltablen == 0) && (i != 0)) {
      end_loclist (top_p);
      start_loclist (top_p);
    }
  }
  end_loclist (top_p);
  write (s, top_p.aschar(), top_p.length());
  end_page (s);
}

static void
tricks (int s) {
  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> XTide hints and tricks </title></head>\n\
<body bgcolor=\"FFFFFF\"><center><h1> XTide hints and tricks </h1></center>\n\
\n\
<p> This web server uses numerically indexed URLs to load prediction\n\
pages.&nbsp; Those URLs look like this:</p>\n\
\n\
<pre>http://whatever/locations/nnn.html</pre>\n\
\n\
<p>For convenience, you can also load a prediction page with a\n\
URL of the following form: </p>\n\
\n\
<pre>http://whatever/locations/name</pre>\n\
\n\
<p>You will receive an error if the name-based lookup gives ambiguous results,\n\
but these URLs have the advantage that they do not need to change every time\n\
the tide data are updated.</p>");

  write (s, top_p.aschar(), top_p.length());
  end_page (s);
}

static void
add_time_control (enum TC type, Dstr &text, Dstr &url, unsigned y, unsigned m, unsigned d, int allow_forms) {
  text += "<p> <form method=get action=\"";
  text += url;
  text += "\">\nYear: <select name=\"y\">\n";
  unsigned i;
  for (i=dialogfirstyear; i<=dialoglastyear; i++) {
    if (i == y)
      text += "<option selected>";
    else
      text += "<option>";
    text += i;
    text += "\n";
  }
  text += "</select>\n";

  if(type != TC_YEAR) {
    text += "Month: <select name=\"m\">\n";
    for (i=1; i<=12; i++) {
      if (i == m)
        text += "<option selected>";
      else
        text += "<option>";
      text += i;
      text += "\n";
    }
    text += "</select>\n";
  }

  if(type == TC_DAY)
  {
    text += "Day: <select name=\"d\">\n";
    for (i=1; i<=31; i++) {
      if (i == d)
        text += "<option selected>";
      else
        text += "<option>";
      text += i;
      text += "\n";
    }
    text += "</select>\n";
  }

  if (allow_forms) {
    text += "Output: <select name=\"f\">\n";
    text += "<option selected>Web page</option>\n";
    text += "<option>iCalendar</option>\n";
    text += "</select>\n";
  }

  text += "<input type=submit value=\"Go\"></form></p>";
}

Timestamp
parse_time_control (Dstr &filename, Dstr timezone, Settings *settings,
Dstr &tc_out, char &form_out, int s) {
  tc_out = (char *)NULL;
  Timestamp ret ((time_t)(time(NULL)));
  form_out = 'h';
  int i;
  if ((i = filename.strrchr ('?')) != -1) {
    unsigned y, m=1, d=1;
    if ((sscanf (filename.ascharfrom(i), "?y=%u&m=%u&d=%u", &y, &m, &d)) >= 1) {
      char temp[80];
      sprintf (temp, "%4u-%02u-%02u 00:00", y, m, d);
      Dstr in_iso (temp);
      ret = Timestamp (in_iso, timezone, settings);
      tc_out = filename.ascharfrom(i);
    }
    if ((i = filename.strstr ("&f=")) != -1) {
      sscanf (filename.ascharfrom(i), "&f=%c", &form_out);
      if (form_out != 'W' && form_out != 'i')
        unimp_barf (s);
      if (form_out == 'W')
        form_out = 'h';
    }
  }
  return ret;
}

static void
load_location_page(int s, Dstr &filename, unsigned long i)
{
  StationIndex *si = context->stationIndex();
  Station *st = (*si)[i]->load (context);

  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> ");

  top_p += st->name;
  top_p += "</title></head>\n<body bgcolor=\"FFFFFF\">\n<center><h1> ";
  top_p += st->name;
  top_p += " <br>\n";

  Dstr text_out, tc_out;
  char form;
  Timestamp start_tm = parse_time_control (filename, st->timeZone,
    context->settings, tc_out, form, s);
  Dstr myurl ("/locations/");
  myurl += i;
  myurl += ".html";
  struct tm *starttm = start_tm.get_tm (st->timeZone, context->settings);
  unsigned y = starttm->tm_year + 1900;
  unsigned m = starttm->tm_mon + 1;
  unsigned d = starttm->tm_mday;

  if (tc_out.isNull())
    top_p += "Local time:  ";
  else
    top_p += "Requested time:  ";
#if __alpha
  Dstr mungebuf;
  start_tm.print (mungebuf, st->timeZone, context->settings);
#endif
  Dstr nowbuf;
  start_tm.print (nowbuf, st->timeZone, context->settings);
  top_p += nowbuf;
  top_p += " </h1></center>\n";

  if (!zoneinfo_doesnt_suck) {
    top_p += "<p> Warning:  this host machine is using an obsolete time zone\n\
database.&nbsp; Summer Time (Daylight Savings Time) adjustments will not be done\n\
for some locations. </p>\n";
  }

  top_p += "<p><img src=\"/graphs/";
  top_p += i;
  top_p += ".png";
  top_p += tc_out;
  top_p += "\" width=";
  top_p += (*(context->settings))["gw"].u;
  top_p += " height=";
  top_p += (*(context->settings))["gh"].u;
  top_p += "></p><p><pre>\n";

  context->plainMode (st, start_tm, text_out, 't');
  top_p += text_out;
  top_p += "</pre></p>\n";

  top_p += "<h2> Time Control </h2>\n";
  add_time_control (TC_DAY, top_p, myurl, y, m, d, 0);
  top_p += "<p> Note:&nbsp; If your browser returns a blank page or a \"no data\"\n\
error, then the predictions that you requested are not available.</p>\n";

  top_p += "<h2>Monthly Tide Calendars</h2>\n";
  Dstr monthurl ("/calendar/month/");
  monthurl += i;
  monthurl += ".ics";
  add_time_control (TC_MONTH, top_p, monthurl, y, m, d, 1);

  top_p += "<h2>Yearly Tide Calendars</h2>\n";
  Dstr yearurl ("/calendar/year/");
  yearurl += i;
  yearurl += ".ics";
  add_time_control (TC_YEAR, top_p, yearurl, y, m, d, 1);

  top_p += "<p>Selecting iCalendar output will cause supporting browsers to add events to your calendar.&nbsp; Before adding an entire month or year of events, please try this <a href=\"/calendar/day/";
  top_p += i;
  top_p += ".ics?y=";
  top_p += y;
  top_p += "&m=";
  top_p += m;
  top_p += "&d=";
  top_p += d;
  top_p += "&f=i\">one-day iCalendar test</a> to see how your calendar will react.&nbsp; The events that you get should have no duration and should not block out any time as \"busy\" time.</p>\n";

  top_p += "<h2>About this station</h2>\n";
  Dstr aboutbuf;
  st->about (aboutbuf, 'h');
  top_p += aboutbuf;

  add_disclaimer (top_p);
  write (s, top_p.aschar(), top_p.length());
  end_page (s, 1);
}

static void
exact_location (int s, Dstr &filename, Dstr &loc)
{
  unsigned long i, count = 0;
  long match = -1;
  StationIndex *si = context->stationIndex();
  for (i=0; i<si->length(); i++)
  {
    	if ((*si)[i]->name %= loc)
	{
		if(match == -1)
			match = i;
		else
			match = -2;
      	}
      	count++;
  }

  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> Exact Query Error </title></head>\n<body bgcolor=\"FFFFFF\">\n<center>\n\
<h1> Exact Query Error </h1></center>\n");

  switch(match)
  {
	case -1:	// no match
		top_p += "<p>No locations matched your query.</p>";
		break;

	case -2:	// more than one match
		top_p += "<p>More than one location matched your query.</p>\n";
		break;

	default:	// clear hit
  		load_location_page(s, filename, (unsigned long)match);
		return;
  }
  write (s, top_p.aschar(), top_p.length());
  end_page (s);
}

static void
badclientlocation (Dstr &filename) {
  log ("Bad client location: ", filename, LOG_NOTICE);
}

static void
rangeerror (Dstr &filename) {
  log ("Bad client location (range error): ", filename, LOG_NOTICE);
}

static void
location_page (int s, Dstr &filename) {
  unsigned long i;
  Dstr loc(filename);
  loc /= strlen("/locations/");
  if(!isdigit(loc[0]))
  {
	exact_location (s, filename, loc);
	return;
  }

  if (sscanf (loc.aschar(), "%lu.html", &i) != 1) {
    badclientlocation (filename);
    notfound_barf (s);
  }

  StationIndex *si = context->stationIndex();
  if (i >= si->length()) {
    rangeerror (filename);
    notfound_barf (s);
  }
  load_location_page(s, filename, i);
}

static void
calendar (int s, Dstr &filename) {
  unsigned long i;
  TC whichcal = TC_YEAR;
  if (sscanf (filename.aschar(), "/calendar/year/%lu.ics", &i) != 1) {
    whichcal = TC_MONTH;
    if (sscanf (filename.aschar(), "/calendar/month/%lu.ics", &i) != 1) {
      whichcal = TC_DAY;
      if (sscanf (filename.aschar(), "/calendar/day/%lu.ics", &i) != 1) {
        badclientlocation (filename);
        notfound_barf (s);
      }
    }
  }

  StationIndex *si = context->stationIndex();
  if (i >= si->length()) {
    rangeerror (filename);
    notfound_barf (s);
  }
  Station *st = (*si)[i]->load (context);

  Dstr tc_out;
  char form;
  Timestamp start_tm = parse_time_control (filename, st->timeZone,
    context->settings, tc_out, form, s);
  struct tm *starttm = start_tm.get_tm (st->timeZone, context->settings);
  unsigned y = starttm->tm_year + 1900;
  unsigned m = starttm->tm_mon + 1;
  unsigned d = starttm->tm_mday;

  switch (whichcal) {
  case TC_YEAR:
    assert (m == 1);
  case TC_MONTH:
    assert (d == 1);
  case TC_DAY:
    ;
  }

  Timestamp end_tm;
  {
    char temp[80];
    sprintf (temp, "%4u-%02u-%02u 00:00", y, m, d);
    start_tm = Timestamp (temp, st->timeZone, context->settings);
    switch (whichcal) {
    case TC_YEAR:
      sprintf (temp, "%4u-01-01 00:00", y+1);
      break;
    case TC_MONTH:
      sprintf (temp, "%4u-%02u-01 00:00", y, m+1);
      break;
    case TC_DAY:
      sprintf (temp, "%4u-%02u-%02u 00:00", y, m, d+1);
      break;
    }
    end_tm = Timestamp (temp, st->timeZone, context->settings) - Interval(1);
  }

  Dstr top_p ("HTTP/1.0 200 OK\nMIME-version: 1.0\n");
  if (form == 'h') {
    top_p += "Content-type: text/html\n\n<html>\n<head><title>";
    top_p += st->name;
    top_p += "</title></head>\n<body bgcolor=\"FFFFFF\">\n";
    if (!zoneinfo_doesnt_suck) {
      top_p += "<p> Warning:  this host machine is using an obsolete time zone\n\
  database.&nbsp; Summer Time (Daylight Savings Time) adjustments will not be done\n\
  for some locations. </p>\n";
    }
    top_p += "<p>\n";
  } else {
    // There is no RFC defining the rules for HTML transport.
    // Extrapolating from RFCs 2445, 2446, 2447.
    top_p += "Content-Type:text/calendar; method=publish; charset=iso-8859-1; \n component=vevent\nContent-Transfer-Encoding: 8bit\n\n";
  }

  Dstr text_out;
  context->calendarMode (st, start_tm, end_tm, text_out, form, 0);
  top_p += text_out;
  if (form == 'h') {
    top_p += "</P>\n";
    add_disclaimer (top_p);
  }

  write (s, top_p.aschar(), top_p.length());
  if (form == 'h')
    end_page (s, 1);
  else {
    close (s);
    exit (0);
  }
}

static void
purecppimage (int s) {
  Dstr top_p
    ("HTTP/1.0 200 OK\nMIME-version: 1.0\nContent-type: image/png\n\n");
  write (s, top_p.aschar(), top_p.length());
  write (s, pcpp, pcpplen);
  close (s);
  exit (0);
}

static void
graphimage (int s, Dstr &filename) {
  unsigned long i;
  if (sscanf (filename.aschar(), "/graphs/%lu.png", &i) != 1) {
    badclientlocation (filename);
    notfound_barf (s);
  }

  StationIndex *si = context->stationIndex();
  if (i >= si->length()) {
    rangeerror (filename);
    notfound_barf (s);
  }
  Station *st = (*si)[i]->load (context);

  Dstr tc_out;
  char form;
  Timestamp start_tm = parse_time_control (filename, st->timeZone,
    context->settings, tc_out, form, s);

  Dstr top_p
    ("HTTP/1.0 200 OK\nMIME-version: 1.0\nPragma: no-cache\nContent-type: image/png\n\n");
  write (s, top_p.aschar(), top_p.length());

  RGBGraph g ((*(context->settings))["gw"].u, (*(context->settings))["gh"].u, context->colors);
  g.drawTides (st, start_tm);
  png_socket = s;  // See externC.cc
  g.writeAsPNG (socket_write_data_fn);
  close (s);
  exit (0);
}

static void
zones (int s, Dstr &filename) {
  Dstr zone(filename);
  zone /= strlen("/zones/");
  Dstr title;
  ZoneIndex::dnode *dn;
  if (zone.length()) {
    dn = zi[zone];
    if (!dn) {
      log ("Bad zone: ", filename, LOG_NOTICE);
      notfound_barf (s);
    }
    title = "Zone ";
    title += zone;
  } else {
    title = "Zone Index";
    dn = zi.top;
  }

  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> ");
  top_p += title;
  top_p += "</title></head>\n<body bgcolor=\"FFFFFF\">\n<center><h1> ";
  top_p += title;
  top_p += " </h1></center>\n";

  if (dn->subzones) {
    ZoneIndex::dnode *sn;
    if (zone.length())
      sn = dn->subzones;
    else
      sn = dn;
    top_p += "\
<p> Most zones consist of a region followed by a city name.&nbsp; Choose the\n\
zone that matches the country and time zone of the location that you want.&nbsp;\n\
For example:  places on the east coast of the U.S. are in zone\n\
:America/New_York; places in New Brunswick, Canada are in zone\n\
:America/Halifax.</p><ul>\n";
    while (sn) {
      top_p += "<li><a href=\"/zones/";
      top_p += sn->name;
      top_p += "\">";
      top_p += sn->name;
      top_p += "</a></li>\n";
      sn = sn->next;
    }
    top_p += "</ul>\n";
  }
  if (dn->il) {
    top_p += "<p> Click on any location to get tide predictions. </p>\n";
    struct ZoneIndex::indexlist *ix = dn->il;
    unsigned long i = 0;
    start_loclist (top_p);
    StationIndex *si = context->stationIndex();
    while (ix) {
      list_location (top_p, si, ix->i);
      if ((i % maxhtmltablen == 0) && (i != 0)) {
        end_loclist (top_p);
        start_loclist (top_p);
      }
      ix = ix->next;
      i++;
    }
    end_loclist (top_p);
  }
  if (zone.length()) {
    Dstr upzone(filename);
    upzone -= upzone.length()-1;
    upzone -= upzone.strrchr('/')+1;
    top_p += "<p> <a href=\"";
    top_p += upzone;
    top_p += "\">Back up</a></p>\n";
  }
  write (s, top_p.aschar(), top_p.length());
  end_page (s);
}

// Remove HTTP mangling from query string.
static void
demangle (Dstr &s, int sock) {
  Dstr buf;
  unsigned i = 0;
  while (i<s.length()) {
    if (s[i] == '+') {
      buf += ' ';
      i++;
    } else if (s[i] == '%') {
      char tiny[3];
      unsigned temp;
      i++;
      tiny[0] = s[i++];
      tiny[1] = s[i++];
      tiny[2] = '\0';
      if (sscanf (tiny, "%x", &temp) != 1) {
        log ("Really nasty URL caught in demangle....", LOG_NOTICE);
        notfound_barf (sock);
      }
      buf += (char)temp;
    } else
      buf += s[i++];
  }
  s = buf;
}

static void
query (int s, Dstr &filename) {
  Dstr locq (filename);
  locq /= strlen("/query?location=");
  demangle (locq, s);

  // Strip leading whitespace
  while (locq.length())
    if (isspace (locq[0]))
      locq /= 1;
    else
      break;

  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/html\n\
\n\
<html>\n\
<head><title> Query Results </title></head>\n<body bgcolor=\"FFFFFF\">\n<center>\n\
<h1> Query Results </h1></center>\n");

  unsigned long i, count = 0;
  StationIndex *si = context->stationIndex();
  for (i=0; i<si->length(); i++) {
    if ((*si)[i]->name %= locq) {
      if (count == 0) {
        top_p += "<p>Locations matching \"";
        top_p += locq;
        top_p += "\":</p><p>\n";
        start_loclist (top_p);
      }
      list_location (top_p, si, i);
      if ((count % maxhtmltablen == 0) && (count != 0)) {
        end_loclist (top_p);
        start_loclist (top_p);
      }
      count++;
    }
  }
  if (count)
    end_loclist (top_p);
  else {
    // Try harder.
    for (i=0; i<si->length(); i++) {
      if ((*si)[i]->name.strcasestr (locq) != -1) {
	if (count == 0) {
          top_p += "<p> Regular query found no matches; using more aggressive (substring) query. </p>\n";
	  top_p += "<p>Locations matching \"";
	  top_p += locq;
	  top_p += "\":</p><p>\n";
	  start_loclist (top_p);
	}
	list_location (top_p, si, i);
	if ((count % maxhtmltablen == 0) && (count != 0)) {
	  end_loclist (top_p);
	  start_loclist (top_p);
	}
	count++;
      }
    }
    if (count)
      end_loclist (top_p);
    else
      top_p += "<p> No locations matched your query.&nbsp; However, that might\n\
just mean that something is spelled strangely in the XTide database.&nbsp; To\n\
make sure that you aren't missing a location that is actually in there,\n\
you should check the indexes linked below.</p>\n";
  }
  write (s, top_p.aschar(), top_p.length());
  end_page (s);
}

static void
log_hits (struct sockaddr *addr) {
  Dstr msg;
  switch (addr->sa_family) {
  case AF_INET:
    {
      struct sockaddr_in *u = (struct sockaddr_in *)addr;
      unsigned long a = u->sin_addr.s_addr;
      char *c =
        // Archaic SunOS syntax:
        // inet_ntoa (&a);
        inet_ntoa (u->sin_addr);
      msg = c;
      msg += "  ";
      struct hostent *hp;
      if ((hp = gethostbyaddr ((char *)(&a), sizeof a, AF_INET)) == NULL)
        msg += "(?)";
      else
        msg += hp->h_name;
    }
    break;
  default:
    msg = "Non-Internet address (?)";
  }
  log (msg, LOG_INFO);
}

static void 
robots (int s) {
  Dstr top_p ("\
HTTP/1.0 200 OK\n\
MIME-version: 1.0\n\
Content-type: text/plain\n\
\n\
User-agent: *\n\
Disallow: /\n");

  write (s, top_p.aschar(), top_p.length());
  close (s);
  exit (0);
}

static void
handle_request (struct sockaddr *addr, int s) {
  // It would have been kind of lame to do this in main and make every
  // request get the same random sequence.
  srand (time (NULL));

  char request[bufsize+1];
  Dstr buf, command, filename;

  log_hits (addr);

  // This will truncate long requests, but we generally won't lose
  // anything that matters.
  int len = read (s, request, bufsize); // Blah, blah, blah...

  if (len >= bufsize) {
    log ("Request too long", LOG_WARNING);
    toolong_barf (s);
  }
  request[len] = '\0';
  buf = request;

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  log ("-------------------------\n", request, LOG_DEBUG);
#endif

  buf /= command;
  if (command != "GET") {
    log ("Bad client command: ", command, LOG_NOTICE);
    unimp_barf (s);
  }

  buf /= filename;

  if (filename == "/")
    root_page (s);
  else if (filename == "/robots.txt")
    robots (s);
  else if (filename == "/index.html")
    index_page (s);
  else if (filename == "/tricks.html")
    tricks (s);
  else if (filename == "/purec++.png")
    purecppimage (s);
  else if (filename %= "/locations/")
    location_page (s, filename);
  else if (filename %= "/graphs/")
    graphimage (s, filename);
  else if (filename %= "/zones/")
    zones (s, filename);
  else if (filename %= "/calendar/")
    calendar (s, filename);
  else if (filename %= "/query?location=")
    query (s, filename);
  else {
    log ("Client tried to get ", filename, LOG_NOTICE);
    notfound_barf (s);
  }

  exit (0);
}

static void
dont_be_root() {
  struct passwd *nb = getpwnam ("nobody");
  if (nb) {
    if (setuid (nb->pw_uid) == 0)
      return;
    else
      xperror ("setuid");
  } else
    xperror ("getpwnam");
  log ("Can't set uid to 'nobody.'  Hope that's OK....", LOG_WARNING);
}

int main (int argc, char **argv)
{
  set_daemon_mode();
  pid_t fpid = fork();
  if (fpid == -1) {
    xperror ("fork");
    exit (-1);
  }
  if (fpid)
    exit (0);

  int listen_socket, new_socket, ts;
  fd_set rdset;
  FD_ZERO (&rdset);

  // The port number given to xttpd is not a "proper" command line
  // setting recognized by XTide and doesn't deserve to be, so
  // if we get one we hide it.

  int portnum_evade = 0;

  {
    unsigned short portnum = 80;
    unsigned long addr = INADDR_ANY;
    if (argc >= 2)
      portnum_evade = parse_addr (portnum, addr, argv[1]);
    setup_socket (portnum, addr, listen_socket);
  }

  dont_be_root();
  FD_SET (listen_socket, &rdset);

  Settings settings;
  settings.applyUserDefaults();
  settings.applyCommandLine (
      portnum_evade ? argc-1 : argc,
      portnum_evade ? &(argv[1]) : argv);

  context = new TideContext (new Colors (settings), &settings);
  // Build the indices.
  zi.add (context->stationIndex());
  // Avoid issuing the zoneinfo warning a million times.
  {
    Timestamp nowarn((time_t)(time(NULL)));
    zoneinfo_doesnt_suck = nowarn.zoneinfo_doesnt_suck(&settings);
  }
  log ("Accepting connections.", LOG_NOTICE);

  ts = -1;
  struct sockaddr addr;
  while (1) {
    fd_set trdset, twrset, texset;
    trdset = rdset;
    // I seem to remember that some platforms barf if you provide
    // null pointers for wrset and exset; best to be safe.
    FD_ZERO (&twrset);
    FD_ZERO (&texset);
    ts = select (listen_socket+1, &trdset, &twrset, &texset, NULL);
    if (ts > 0) {
      // Type of length is set by configure.
      acceptarg3_t length = sizeof (addr);
      if ((new_socket = accept (listen_socket, &addr, &length)) != -1) {
        if (!fork())
          handle_request (&addr, new_socket);
        close (new_socket);
      }
    }
    // Clean up zombies.
    while (waitpid (-1, NULL, WNOHANG|WUNTRACED) > 0);
  }

  exit (0);
}
