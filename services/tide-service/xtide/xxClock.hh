/*  xxClock  Tide clock.
    Last modified 1998-05-04

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// This is almost similar enough to xxGraphMode to inherit from it,
// but not quite.  Beware of duplicated code.

class xxClock: public xxDrawable {
  friend void xxClockSaveCallback (Dstr &filename, void *in_ptr);
  friend void xxClockResizeHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch);
  friend void xxClockTimerCallback (XtPointer client_data,
    XtIntervalId *timerid);
  friend void xxClockButtonPressEventHandler (Widget w, XtPointer client_data,
			 XEvent *event, Boolean *continue_dispatch);

public:
  xxClock (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    Station *in_station, int nobuttonsflag = 1);
  ~xxClock();

  void redraw();
  void global_redraw();
  void dismiss();
  void help();
  void save();

  int is_graph();
  int is_clock();

protected:
  int nobuttons;
  XtIntervalId timer;
  Pixmap clockicon;
  Window iconwindow2;
  int toggle_icon_window;
  void redrawIcon();
  Angle analogangle;

  xxPixmapGraph *graph;
  Dimension origwindowheight, origwindowwidth, origgraphheight,
    origgraphwidth, curgraphheight, curgraphwidth, curwindowheight,
    curwindowwidth;
  void draw();
  void clearbuf();
  xxContext *label;
};
