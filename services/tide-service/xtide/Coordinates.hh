// $Id: Coordinates.hh,v 1.3 2004/04/06 14:02:54 flaterco Exp $
/*  Coordinates   Degrees latitude and longitude.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Coordinates {
protected:
  // A value of 0, 0 is considered null.
  double latitude;
  double longitude;
public:
  Coordinates ();
  Coordinates (double lat_in, double lng_in);
  void lat (double lat_in);
  double lat() const;
  void lng (double lng_in);
  double lng() const;
  int isNull() const;
  void print (Dstr &out, int pad = 0) const;
  void printlat (Dstr &out) const;
  void printlng (Dstr &out) const;
};
