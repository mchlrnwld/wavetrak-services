// $Id: config.hh,v 1.41 2005/01/07 02:36:32 flaterco Exp $

/********************************************************/
/************ STUFF YOU MIGHT WANT TO CHANGE ************/
/********************************************************/

// Set this to the e-mail address of your local webmaster.  Xttpd
// will use this for the feedback address.  If left unset, xttpd
// will guess webmaster@your.local.site.
// #define webmasteraddr "you@your.site.here"

// Defaults.  The order of precedence, from least significant to
// most significant, is:
//    1.  config.hh
//    2.  Xdefaults (X resources)
//    3.  ~/.xtide.xml (Control Panel)
//    4.  Command line
// Note that only xtide (not xttpd or tide) reads Xdefaults.

// Default colors.
#define bgdefcolor "white"
#define fgdefcolor "black"
#define markdefcolor "red"
#define buttondefcolor "gray80"
#define daydefcolor "SkyBlue"
#define nightdefcolor "DeepSkyBlue"
#define flooddefcolor "Blue"
#define ebbdefcolor "SeaGreen"
#define datumdefcolor "white"
#define msldefcolor "yellow"
#define twidefcolor "green"

// Default graph width and height (pixels), and aspect
#define defgwidth 960
#define defgheight 312
#define defgaspect 1.0

// Draw datum and Mean Astronomical Tide lines?
#define extralines 'n'

// Prefer flat map to round globe location chooser?
#define flatearth 'n'

// Create tide clocks with buttons?
#define cbuttons 'n'

// Draw depth lines on top of graph?
#define toplines 'n'

// Draw tide graph as a line graph?
#define nofill 'n'

// Suppress sunrise, sunset, moon phases?
#define nosunmoon 'n'

// Infer constituents?  (Expert use only)
#define infer 'n'

// Default width of lines in line graphs
#define deflwidth 2.5

// Default clock width
#define defcwidth 84

// Default preferred units:  ft, m, or x (no preference).
#define prefunits "x"

// Force UTC?
#define zulu 'n'

// Nick 03/12/05

// Suppress title
#define showtitle 'y'

// Suppress Moon Events
#define nomoon 'n'

// Suppress title
#define timeaxis 'y'

// Suppress Moon Events
#define timetext 'y'

// No X 
#define nox 'n'

// Suppress left margin
#define noleftmargin 'n'

// Date, time, hour formats.

// For US-style AM/PM
#define datefmt "%Y-%m-%d"
#ifdef __CYGWIN__
// %l doesn't work under Cygwin (snapshot 2002-05-20), and %Z is null
#define hourfmt "%I"
#define timefmt "%I:%M %p"
#else
#ifdef sgi
// %l doesn't work under Irix but %Z does
#define hourfmt "%I"
#define timefmt "%I:%M %p %Z"
#else
#define hourfmt "%l"
#define timefmt "%l:%M %p %Z"
#endif
#endif

// For 24-hour time with no AM/PM
// #define datefmt "%Y-%m-%d"
// #define hourfmt "%H"
// #define timefmt "%H:%M %Z"

// See the man page for strftime to learn how to alter the formats
// to do other things.

// Default center longitude for location chooser.
// Valid values:  -180 -150 -120 -90 -60 -30 0 30 60 90 120 150 360
// 360 will pick the longitude with the most tide stations.
#define defgl 360

// Default width of ASCII graphs and banners (characters).
#define defttywidth 79

// Default height of ASCII graphs (characters).
#define defttyheight 24

// Define this to enable the time workaround.  This workaround is to
// enable the use of years before 1970 and after 2037 irrespective of
// platform limitations on time_t.  Time zones and daylight savings
// time are sacrificed.  Everything becomes UTC.
// #define TIME_WORKAROUND

// Limits for time control dialogs.
#define dialogfirstyear 1970
#define dialoglastyear  2037

/***********************************************************/
/*********** STUFF YOU PROBABLY SHOULDN'T CHANGE ***********/
/***********************************************************/

// Minimum graph width and height
#define mingwidth 64
#define mingheight 64

// Minimum TTY width and height.  It is actually a very good thing
// for these to be the same to avoid assertion failures in banner
// mode where everything gets sideways.
#define minttywidth 10
#define minttyheight 10

// Fudge factor to correct aspect ratio on TTY
// Correct 80x24 of VT100 to match 4/3 aspect
#define TTYaspectfudge 2.5

// Fudge factor to correct aspect ratio on line printer
// Correct 10x6 of Pica type to be 1:1
#define LPaspectfudge 1.66666667

// Separator between tide stations in text client output
// \f is form feed
#define stationsep "\f"

// If begin time is provided but end time is not, set the end time
// to the begin time plus this number of seconds.
// 345600 = 4 days
#define defpredictinterval 345600

// Margin left at top and bottom of tide graphs when scaling tides;
// how much "water" at lowest tide; how much "sky" at highest tide.
// This is a scaling factor for the graph height.
#define margin 0.0673

// Length of tick marks on time axis of graphs
#define hourticklen 8

// Number of pixels from left hand side of graph to place "now"
#define nowposition 42
// Ditto, for TTY graph mode.
#define TTYnposition 7

// This is used to help calculate the minimum width of graph and clock
// windows.  The minimum width is the string width of the command
// button captions plus minwidthfudge times the number of buttons.
// (All of the buttons must fit on one line to avoid catastrophe.)
#define minwidthfudge 15

// How many lines by default in text, raw, and medium rare mode windows.
#define origtextmodelines 10

// Configuration of location choosers

// Globe
// The viewing window will be min_globe_size pixels squared.
#define min_globe_size 500
// Zoom factor of left mouse click.
#define zoomfactor 2.0
// This controls the selection of which resolution WVS file to use at a
// given zoom factor.  Smaller numbers result in more aggressive use of
// higher resolution files and therefore more CPU loading on average.
// Useful range is vaguely 1e+12 down to 1e+10.
#define zoomresmagicnumber 9.0e+10

// Map
// The viewing window will be x_map_size pixels by y_map_size pixels.
#define y_map_size 500
#define x_map_size 1000

// Both
// Radius (in pixels) affected by right mouse click.
#define blastradius 15
// How far outside the window do we draw to avoid chopping off lines.
// (Pixels)
#define antichopmargin 50

// Maximum length of HTML tables (Netscape has trouble with really big
// tables).
#define maxhtmltablen 100

// Controls for how many columns you get in calendar mode, CSV format.
// If you get more events than there is room for, they are discarded.
//
// A value of x for calcsv_nummaxmin means that you get x columns for
// the max times, x columns for the max heights, x columns for the min
// times, x columns for the min heights, and 2x columns for slacks.
#define calcsv_nummaxmin 5
// A value of x for calcsv_numriseset means that you get x columns for
// sunrise, x columns for sunset, x columns for moonrise, and x columns
// for moonset.
//
// Yes, you can have two sunsets in one day, and you don't even need
// Daylight Savings Time to do it:
//
// Isla Neny, Antarctica
// 68.2000� S, 67.0000� W
//
// 2001-01-24 12:03 AM ARST   Sunset
// 2001-01-24  3:17 AM ARST   Sunrise
// 2001-01-24 11:57 PM ARST   Sunset
#define calcsv_numriseset 1
// Moon phases are discarded.

// Replacement character for commas in CSV format
#define csv_repchar '|'

/*******************************************************************/
/************ STUFF YOU DEFINITELY SHOULDN'T MESS WITH *************/
/*******************************************************************/

#define VERSION "2.8"
#define PATCHLEVEL 2
// #define VSTATUS "BETA 2"

// Warning:  this will enable code that significantly slows down
// tide predictions and should only be used by developers and/or
// debuggers.  It will also generate a core dump on EVERY error.
// FIXME:
// This does not even work anymore since it uses obsoleted stream
// operations that I expunged.
// #define SUPER_ULTRA_VERBOSE_DEBUGGING

// The beginning of time (1970-01-01 00:00:00Z) as a Julian date.
#define beginning_of_time 2440587.5

// Aspect adjustment.  56160 matches with XTide 1.
#define aspmagnum 56160.0

// Configuration of location choosers

// Globe
#define max_globe_size 512000

// Map
#define max_zoom_level 8

// Both
// List windows can't exceed 32K pixels in height.
#define ll_maxlen 2000
#define ll_viewportwidth 750
#define ll_viewportheight 100

// Configuration of disclaimer window
#define d_textwidth 550
#define d_textheight 500

// Must match the size of the title screen image.
#define titlescreenwidth 398
#define titlescreenheight 332

// General.
#define DAYSECONDS 86400
#define HOURSECONDS 3600

// Skycal.
#define DEG_IN_RADIAN     57.2957795130823
#define HRS_IN_RADIAN     3.819718634
#define SEC_IN_DAY        86400.
#define FLATTEN           0.003352813   /* flattening of earth, 1/298.257 */
#define EQUAT_RAD         6378137.    /* equatorial radius of earth, meters */
#define J2000             2451545.        /* Julian date at standard epoch */

// This affects the rise/set predictions.  Normally you would
// need to adjust it for the evelation of the location, but since this
// is a tide prediction program, we can assume that we are always at
// sea level, and actually be RIGHT :-)
#define rise_altitude -0.83
#define twi_altitude -6  /* nautical twilight */

// Stuff for mathematical code in ConstantSetWrapper and Station

/* TIDE_TIME_BLEND
 *   Half the number of seconds over which to blend the tides from
 *   one epoch to the next.
 */
#define TIDE_BLEND_SECONDS (3600)

// Precision (in seconds) to which we will find roots.
#define def_TIDE_TIME_PREC 15

// In drawing of line graphs, slope at which to abandon the thick line
// drawing algorithm.
#define slopelimit 5.0
