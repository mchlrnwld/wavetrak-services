// $Id: xxTimestampDialog.cc,v 1.2 2002/12/19 18:40:45 flaterco Exp $
/*  xxTimestampDialog  Embeddable timestamp chooser

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void xxTimestampDialog::val (Dstr &iso_string_out) {
  char buf[80];
  sprintf (buf, "%04u-%02u-%02u %02u:%02u", year, month, day, hour, minute);
  iso_string_out = buf;
}

void timestampdialogyearcallback (void *cbdata) {
  xxTimestampDialog *mydialog = (xxTimestampDialog *)cbdata;
  mydialog->year = mydialog->yearchoice->choice() + dialogfirstyear;
}

void timestampdialogmonthcallback (void *cbdata) {
  xxTimestampDialog *mydialog = (xxTimestampDialog *)cbdata;
  mydialog->month = mydialog->monthchoice->choice() + 1;
}

void timestampdialogdaycallback (void *cbdata) {
  xxTimestampDialog *mydialog = (xxTimestampDialog *)cbdata;
  mydialog->day = mydialog->daychoice->choice() + 1;
}

void timestampdialoghourcallback (void *cbdata) {
  xxTimestampDialog *mydialog = (xxTimestampDialog *)cbdata;
  mydialog->hour = mydialog->hourchoice->choice();
}

void timestampdialogminutecallback (void *cbdata) {
  xxTimestampDialog *mydialog = (xxTimestampDialog *)cbdata;
  mydialog->minute = mydialog->minutechoice->choice();
}

xxTimestampDialog::xxTimestampDialog (xxTideContext *in_xtidecontext,
xxContext *in_xxcontext, char *caption, Timestamp init,
const Dstr &in_timezone) {
  timezone = in_timezone;
  xtidecontext = in_xtidecontext;

  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNorientation, (XtArgVal)XtorientHorizontal}
    };
    Widget boxwidget = XtCreateManagedWidget ("", boxWidgetClass,
      in_xxcontext->manager, args, 3);
    box = new xxContext (in_xxcontext, boxwidget);
  }
  Arg labelargs[3] =  {
    {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
    {XtNborderWidth, (XtArgVal)0}
  };
  {
    Dstr padded_caption (caption);
    padded_caption += "  ";
    Widget labelwidget = XtCreateManagedWidget (padded_caption.aschar(),
      labelWidgetClass, box->manager, labelargs, 3);
    label = new xxContext (box, labelwidget);
  }

  struct tm *init_tm = init.get_tm (timezone, xtidecontext->settings);

  static char *yearchoices[dialoglastyear-dialogfirstyear+2];
  static int firsttime = 0;
  if (!firsttime) {
    firsttime = 1;
    for (unsigned looper=dialogfirstyear; looper<=dialoglastyear; looper++) {
      char buf[5];
      sprintf (buf, "%04u", looper);
      yearchoices[looper-dialogfirstyear] = strdup (buf);
    }
    yearchoices[dialoglastyear-dialogfirstyear+1] = NULL;
  }

  static char *monthchoices[] = {"01", "02", "03", "04", "05", "06", "07",
				 "08", "09", "10", "11", "12", NULL};
  static char *daychoices[] = {
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
    "31", NULL};
  static char *hourchoices[] = {"00",
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", NULL};
  static char *minutechoices[] = {"00",
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
    "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
    "41", "42", "43", "44", "45", "46", "47", "48", "49", "50",
    "51", "52", "53", "54", "55", "56", "57", "58", "59", NULL};

  year = init_tm->tm_year + 1900;
  month = init_tm->tm_mon + 1;
  day = init_tm->tm_mday;
  hour = init_tm->tm_hour;
  minute = init_tm->tm_min;

  if (year < dialogfirstyear)
    year = dialogfirstyear;
  else if (year > dialoglastyear)
    year = dialoglastyear;

  yearchoice = new xxMultiChoice (box, &timestampdialogyearcallback,
   (void*)this, yearchoices, year-dialogfirstyear);
  monthchoice = new xxMultiChoice (box, &timestampdialogmonthcallback,
   (void*)this, monthchoices, month-1);
  daychoice = new xxMultiChoice (box, &timestampdialogdaycallback,
   (void*)this, daychoices, day-1);
  {
    Widget labelwidget = XtCreateManagedWidget (" ",
      labelWidgetClass, box->manager, labelargs, 3);
    spacelabel1 = new xxContext (box, labelwidget);
  }
  hourchoice = new xxMultiChoice (box, &timestampdialoghourcallback,
   (void*)this, hourchoices, hour);
  {
    Widget labelwidget = XtCreateManagedWidget (":",
      labelWidgetClass, box->manager, labelargs, 3);
    spacelabel2 = new xxContext (box, labelwidget);
  }
  minutechoice = new xxMultiChoice (box, &timestampdialogminutecallback,
   (void*)this, minutechoices, minute);
}

xxTimestampDialog::~xxTimestampDialog () {
  delete label;
  delete box;
  delete yearchoice;
  delete monthchoice;
  delete daychoice;
  delete hourchoice;
  delete minutechoice;
  delete spacelabel1;
  delete spacelabel2;
}

void xxTimestampDialog::global_redraw() {
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)box->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };
  XtSetValues (label->manager, args, 2);
  XtSetValues (box->manager, args, 2);
  yearchoice->global_redraw();
  monthchoice->global_redraw();
  daychoice->global_redraw();
  hourchoice->global_redraw();
  minutechoice->global_redraw();
}
