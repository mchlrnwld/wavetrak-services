// $Id: Banner.cc,v 1.3 2003/02/20 15:19:56 flaterco Exp $
/*  Banner  Graph printed sideways on tractor feed dot matrix printer

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Banner::Banner (unsigned xsize, unsigned ysize): TTYGraph (ysize, xsize) {
  //                                         turn sideways ^^^^^^^^^^^^
}

// The constructor that I fabricated to do all this in one step wouldn't
// compile.
Banner *bannerFactory (Station *station, unsigned xsize, Timestamp start_tm,
Timestamp end_tm) {
  // Everything is sideways.
  Interval increment = (max (1, (long)(aspmagnum / (double)xsize /
    (station->aspect / LPaspectfudge) + 0.5)));
  unsigned ysize = max ((int)((end_tm - start_tm) / increment) + TTYnposition,
    minttywidth);
  return new Banner (xsize, ysize);
}

void
Banner::drawHorizontalLine (int xlo, int xhi, int y,
			      Colors::colorchoice c) {
  int i;
  for (i=xlo; i<=xhi; i++)
    setPixel (i, y, '|');
}

void Banner::drawHourTick (int x, Colors::colorchoice c) {
  setPixel (x, ysize()-1, '-');
}

void Banner::writeAsText (Dstr &text_out) {
  // Everything is sideways.
  text_out = (char *)NULL;
  char *buf = (char *)malloc (ysize()+2);
  buf[ysize()] = '\n';
  buf[ysize()+1] = '\0';
  for (unsigned x=0; x<xsize(); x++) {
    for (unsigned y=0; y<ysize(); y++)
      buf[y] = tty[(ysize()-1-y)*xsize()+x];
    text_out += buf;
  }
  free (buf);
}

double Banner::aspectfudge() {
  return 1.0 / LPaspectfudge;
}

void
Banner::drawStringSideways (int x, int y, const Dstr &s) {
  for (unsigned a=0; a<s.length(); a++)
    setPixel (x, y-a, s[a]);
}


void
Banner::labelHourTick (int x, Dstr &ts) {
  drawStringSidewaysOnLine (x, -1, ts);
}

// A bit of kludging here makes the timestamps look good without massive
// changes to the Graph class.
void Banner::drawStringSidewaysOnLine (int x, int line, const Dstr &s) {
  int y;
  if (s.length() < 1)
    return;
  if (line >= 0) {
    y = s.length() - 1;
    x += (line - 2);
  } else {
    if (line < -1) {
      y = myysize-4*(fontHeight()+fontMargin())-hourTickLen();
      x += (line + 2);
    } else
      y = myysize-(fontHeight()+fontMargin())-hourTickLen();
  }
  drawStringSideways (x, y, s);
}


void Banner::drawTimestampLabel (int x, int line, Dstr &ts) {
  drawStringSidewaysOnLine (x, line, ts);
}

void Banner::drawTitleLine (Dstr &title) {
  // Lose it.
}

int Banner::is_banner () {
  return 1;
}
