// $Id: xxWindow.hh,v 1.4 2004/02/17 19:32:12 flaterco Exp $
/*  xxWindow  Abstract class for all XTide windows implementing dismiss().

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxTideContext;

class xxWindow {
  friend class xxXTideRoot;
  friend void xxWindowCloseHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch);

public:
  // needcontainer:
  //   0  No thanks
  //   1  Make me a box
  //   2  Make me a form
  xxWindow (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    int needcontainer, XtGrabKind in_grabkind = XtGrabNone);
  virtual ~xxWindow();

  virtual void dismiss() = 0;

  // Global_redraw signals a change in global parameters (like colors).
  virtual void global_redraw();

  // Need to identify title screen to avoid exiting when it vanishes.
  // (This was originally a virtual function, but for some reason
  // it wouldn't override it in TitleScreen.)
  int is_title_screen;

  // Use to prevent user from closing window at inopportune times
  int noclose;

  void move (Dstr &geometry);

protected:
  //   0  None
  //   1  Box
  //   2  Form
  int containertype;
  xxContext *mypopup, *container;
  xxTideContext *xtidecontext;
};
