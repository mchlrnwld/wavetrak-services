// $Id: Station.cc,v 1.30 2004/11/17 22:03:56 flaterco Exp $
/*  Station  Abstract superclass of ReferenceStation and SubordinateStation.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Station::Station (TideContext *in_context) {
  // Other fields are filled in in HarmonicsFile::getStation.
  assert (in_context);
  context = in_context;
  markLevel = NULL;
  aspect = (*(context->settings))["ga"].d;
  step = Interval (HOURSECONDS);
}

PredictionValue Station::minLevel() const {
  return constants->datum() - constants->maxAmplitude;
}

PredictionValue Station::maxLevel() const {
  return constants->datum() + constants->maxAmplitude;
}

// The following block of functions is slightly revised from the code
// delivered by Geoffrey T. Dairiki for XTide 1.  I have refrained
// from renaming the functions (much) so that Jeff's comments might
// still make (some) sense.

/*************************************************************************
 *
 * Geoffrey T. Dairiki Fri Jul 19 15:44:21 PDT 1996
 *
 ************************************************************************/

/* TIDE_TIME_PREC
 *   Precision (in seconds) to which we will find roots
 *   See config.hh for def_TIDE_TIME_PREC.
 */
// This initialization fails on AIX 4.2.1, IBM RS6000, 43P-140,
// yielding a zero interval (Alan J. Wylie).  See predictTideEvents
// for workaround.
static Interval TIDE_TIME_PREC (def_TIDE_TIME_PREC);

/* TIDE_TIME_STEP
 *   We are guaranteed to find all high and low tides as long as their
 * spacing is greater than this value (in seconds).
 */
#define TIDE_TIME_STEP (TIDE_TIME_PREC)

// Functions to zero out

// Option #1 -- find maxima and minima
PredictionValue
Station::f_hiorlo (Timestamp t, unsigned deriv) {
  return constants->predictHeight (t, deriv+1);
}

// Option #2 -- find mark crossings or slack water
// Marklev is a class-level variable that is set by the higher-level
// find_mark_crossing.
// ** Marklev must be made compatible with the tide as returned by
// predictHeight, i.e., no datum, no conversion from KnotsSquared.
PredictionValue
Station::f_mark (Timestamp t, unsigned deriv)
{
  PredictionValue pv_out = constants->predictHeight (t, deriv);
  if (deriv == 0)
    pv_out -= marklev;
  return pv_out;
}

/* find_zero (time_t t1, time_t t2, double (*f)(time_t t, int deriv))
 *   Find a zero of the function f, which is bracketed by t1 and t2.
 *   Returns a value which is either an exact zero of f, or slightly
 *   past the zero of f.
 */

/*
 * Here's a root finder based upon a modified Newton-Raphson method.
 */

// The direction parameter just controls which side of the bracket
// we return.

// We are no longer guaranteed to get valid brackets.

Timestamp
Station::find_zero (Timestamp tl, Timestamp tr,
PredictionValue (Station::*f)(Timestamp, unsigned deriv), Direction d) {
  PredictionValue fl = (this->*f)(tl, 0);
  PredictionValue fr = (this->*f)(tr, 0);
  double scale = 1.0;
  Interval dt;
  Timestamp t;
  PredictionValue fp, ft, f_thresh;
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  long itctr = 0;
#endif

  assert(fl.val() != 0.0 && fr.val() != 0.0);
  assert(tl < tr);
  if (fl.val() > 0) {
    scale = -1.0;
    fl = -fl;
    fr = -fr;
  }
  assert(fl.val() < 0.0 && fr.val() > 0.0);

  while (tr - tl > TIDE_TIME_PREC) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    itctr++;
    cout << tr << " - " << tl << " is > " << TIDE_TIME_PREC << endl;
    cout << tr.timet() << " " << tl.timet() << endl;
#endif

    if (t.isNull())
      dt = Interval(0); // Force bisection on first step
    else if (abs(ft) > f_thresh   /* not decreasing fast enough */
        || (ft.val() > 0.0 ?    /* newton step would go outside bracket */
            (fp <= ft / (long)((t - tl).in_seconds())) :
            (fp <= -ft / (long)((tr - t).in_seconds()))))
      dt = Interval(0); /* Force bisection */
    else {
      /* Attempt a newton step */
      assert (fp.val() != 0.0);
      dt = Interval((long)floor(-ft/fp + 0.5));

      /* Since our goal specifically is to reduce our bracket size
         as quickly as possible (rather than getting as close to
         the zero as possible) we should ensure that we don't take
         steps which are too small.  (We'd much rather step over
         the root than take a series of steps which approach the
         root rapidly but from only one side.) */
      if (abs(dt) < TIDE_TIME_PREC)
          dt = ft.val() < 0.0 ? TIDE_TIME_PREC : -TIDE_TIME_PREC;

      if ((t += dt) >= tr || t <= tl)
          dt = Interval(0);   /* Force bisection if outside bracket */
      f_thresh = abs(ft) / 2.0;
    }

    if (dt.in_seconds() == 0) {
      /* Newton step failed, do bisection */
      t = tl + (tr - tl) / 2;
      f_thresh = fr > -fl ? fr : -fl;
    }

    if ((ft = scale * (this->*f)(t,0)).val() == 0.0) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
        cerr << "find_zero took " << itctr << " iterations" << endl;
#endif
        return t;             /* Exact zero */
    } else if (ft.val() > 0.0)
        tr = t, fr = ft;
    else
        tl = t, fl = ft;

    fp = scale * (this->*f)(t,1);
  }

#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "find_zero took " << itctr << " iterations" << endl;
#endif
  if (d == forward)
    return tr;
  return tl;
}

/* next_zero(time_t t, double (*f)(), double max_fp, double max_fpp)
 *   Find the next zero of the function f which occurs after time t.
 *   The arguments max_fp and max_fpp give the maximum possible magnitudes
 *   that the first and second derivative of f can achieve.
 *
 *   Algorithm:  Our goal here is to bracket the next zero of f ---
 *     then we can use find_zero() to quickly refine the root.
 *     So, we will step forward in time until the sign of f changes,
 *     at which point we know we have bracketed a root.
 *     The trick is to use large steps in our search, making
 *     sure the steps are not so large that we inadvertently
 *     step over more than one root.
 *
 *     The big trick, is that since the tides (and derivatives of
 *     the tides) are all just harmonic series', it is easy to place
 *     absolute bounds on their values.
 */

// I've munged this, via clone and hack, to search backwards and forwards.

// This function is ONLY used for finding maxima and minima.  Since by
// definition the tide cannot change direction between maxima and
// minima, there is at most one crossing of a given mark level between
// min/max points.  Therefore, we already have a bracket of the mark
// level to give to find_zero.

Timestamp
Station::next_zero (Timestamp t,
PredictionValue (Station::*f)(Timestamp, unsigned deriv),
int &risingflag_out, Amplitude max_fp, Amplitude max_fpp, Direction d) {
  Timestamp t_left, t_right;
  Interval step, step1, step2;
  PredictionValue f_left, df_left, f_right, df_right;
  double scale = 1.0;
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  long itctr = 0;
#endif

  if (d == forward) {
    // This is the original forwards version.

    t_left = t;

    /* If we start at a zero, step forward until we're past it. */
    while ((f_left = (this->*f)(t_left,0)).val() == 0.0)
      t_left += TIDE_TIME_PREC;

    if (!(risingflag_out = f_left.val() < 0.0)) {
      scale = -1.0;
      f_left = -f_left;
    }

    while (1) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
      itctr++;
#endif

      /* Minimum time to next zero: */
      step1 = Interval((long)(abs(f_left) / max_fp));

      /* Minimum time to next turning point: */
      df_left = scale * (this->*f)(t_left,1);
      step2 = Interval((long)(abs(df_left) / max_fpp));

      if (df_left.val() < 0.0)
        /* Derivative is in the wrong direction. */
        step = step1 + step2;
      else
        step = step1 > step2 ? step1 : step2;

      if (step < TIDE_TIME_STEP)
          step = TIDE_TIME_STEP; /* No ridiculously small steps */

      t_right = t_left + step;
      /*
       * If we hit upon an exact zero, step right until we're off
       * the zero.  If the sign has changed, we are bracketing a desired
       * root, if the sign hasn't changed, then the zero was at
       * an inflection point (i.e. a double-zero to within TIDE_TIME_PREC)
       * and we want to ignore it.
       */
      while ((f_right = scale * (this->*f)(t_right, 0)).val() == 0.0)
        t_right += TIDE_TIME_PREC;
      if (f_right.val() > 0.0) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
        cerr << "next_zero took " << itctr << " iterations" << endl;
#endif
        return find_zero(t_left, t_right, f, d); /* Found a bracket */
      }

      t_left = t_right, f_left = f_right;
    }

  } else {
    // This is the mirror image of the above.

    t_right = t;

    /* If we start at a zero, step backward until we're past it. */
    while ((f_right = (this->*f)(t_right,0)).val() == 0.0)
      t_right -= TIDE_TIME_PREC;

    if ((risingflag_out = f_right.val() > 0.0)) {
      scale = -1.0;
      f_right = -f_right;
    }

    while (1) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
      itctr++;
#endif

      /* Minimum time to next zero: */
      step1 = Interval((long)(abs(f_right) / max_fp));

      /* Minimum time to next turning point: */
      df_right = scale * (this->*f)(t_right,1);
      step2 = Interval((long)(abs(df_right) / max_fpp));

      if (df_right.val() > 0.0)
        /* Derivative is in the wrong direction. */
        step = step1 + step2;
      else
        step = step1 > step2 ? step1 : step2;

      if (step < TIDE_TIME_STEP)
          step = TIDE_TIME_STEP; /* No ridiculously small steps */

      t_left = t_right - step;
      /*
       * If we hit upon an exact zero, step left until we're off
       * the zero.  If the sign has changed, we are bracketing a desired
       * root, if the sign hasn't changed, then the zero was at
       * an inflection point (i.e. a double-zero to within TIDE_TIME_PREC)
       * and we want to ignore it.
       */
      while ((f_left = scale * (this->*f)(t_left, 0)).val() == 0.0)
        t_left -= TIDE_TIME_PREC;
      if (f_left.val() > 0.0) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
        cerr << "next_zero took " << itctr << " iterations" << endl;
#endif
        return find_zero(t_left, t_right, f, d); /* Found a bracket */
      }

      t_right = t_left, f_right = f_left;
    }
  }
}

Timestamp
Station::next_high_or_low_tide (Timestamp t, int &hiflag_out, Direction d) {
  int rising;
  Timestamp thilo = next_zero (t, &Station::f_hiorlo, rising,
    constants->dt_tide_max(2), constants->dt_tide_max(3), d);
  hiflag_out = !rising;
  return thilo;
}

// Find the mark crossing in this bracket, or return NULL if there is
// none.
Timestamp
Station::find_mark_crossing (Timestamp t1, Timestamp t2, int &risingflag_out,
Direction d)
{
  if (t1 > t2) {
    Timestamp t = t1;
    t1 = t2;
    t2 = t;
  }

  PredictionValue f1 = f_mark(t1,0);
  PredictionValue f2 = f_mark(t2,0);

  // Fail gently on rotten brackets.  (This used to be an assertion.)
  if (f1 == f2)
    return Timestamp(); // return null timestamp

  if (!(risingflag_out = f1.val() < 0.0 || f2.val() > 0.0)) {
     f1 = -f1;
     f2 = -f2;
  }

  // Since f1 != f2, we can't get two zeros, so it doesn't matter which
  // one we check first.
  if (f1.val() == 0.0)
    return t1;
  else if (f2.val() == 0.0)
    return t2;

  if (f1.val() < 0.0 && f2.val() > 0.0)
    return find_zero (t1, t2, &Station::f_mark, d);

  return Timestamp(); // Don't have a bracket, return null timestamp
}

// Slightly higher level version of find_mark_crossing that does the
// needed compensation for datum, KnotsSquared, and units.
Timestamp Station::find_mark_crossing (Timestamp t1, Timestamp t2,
PredictionValue marklev_in, int &risingflag_out, Direction d) {
  // marklev must compensate for datum and KnotsSquared.  See f_mark.
  marklev = marklev_in;
  // Correct meters / feet
  if (marklev.Units() != constants->datum().Units())
    marklev.Units (constants->datum().Units());
  marklev -= constants->datum();
  // Correct knots / knots squared
  if (constants->predictUnits() != marklev.Units())
    marklev.Units (constants->predictUnits());
  return find_mark_crossing (t1, t2, risingflag_out, d);
}

// Deal with interpolated sub station mark crossings and slacks.
// FIXME Station needs refactoring.
TideEvent Station::find_mark_crossing (TideEvent left_te, TideEvent right_te,
PredictionValue marklev_in, int &risingflag_out) {
  TideEvent new_te;
  new_te.isCurrent = isCurrent;
  // Toss any brackets in which reverse interpolation will blow up.
  if (right_te.pv != left_te.pv) {
    // Work backwards to uncorrected level corresponding to mark
    // level in this bracket.
    new_te.tm = find_mark_crossing (left_te.uncorrected_tm,
      right_te.uncorrected_tm, left_te.uncorrected_pv +
      (right_te.uncorrected_pv - left_te.uncorrected_pv) *
      ((marklev_in - left_te.pv) / (right_te.pv - left_te.pv)),
      risingflag_out, forward);
    if (!(new_te.tm.isNull())) {
      // Correct tm.
      new_te.tm = left_te.tm + (right_te.tm - left_te.tm) *
	((new_te.tm - left_te.uncorrected_tm) / (right_te.uncorrected_tm - left_te.uncorrected_tm));
    }
  }
  return new_te;
}


// Get all tide events within a range of timestamps and add them to
// the organizer.  The range is >= start_tm and < end_tm.  Because
// predictions are done to plus or minus one minute, invoking this
// multiple times with adjoining ranges may duplicate or delete tide
// events falling right on the boundary.  TideEventsOrganizer should
// suppress the duplicates, but omissions will not be detected.
//
// Either settings or the filter arg can suppress sun and moon events.
void Station::predictTideEvents (Timestamp start_tm, Timestamp end_tm,
TideEventsOrganizer &organizer, TideEventsFilter filter) {
  int risingflag;

  // Workaround for failed initialization of TIDE_TIME_PREC on
  // AIX 4.2.1, IBM RS6000, 43P-140 (Alan J. Wylie)
  TIDE_TIME_PREC = Interval (def_TIDE_TIME_PREC);

  if (start_tm >= end_tm)
    return;

  // Tide events, pass 1:  "known" tide events and mark crossings.
  {
    TideEvent te;
    te.isCurrent = isCurrent;

    // tm is the "internal" timestamp for scanning the reference station.
    // The timestamps of each event get mangled for sub stations.
    Timestamp tm = start_tm - maximumTimeOffset;
    Timestamp etm = end_tm - minimumTimeOffset;

    while (tm <= etm) { // Patience... range is correctly enforced below.
      Timestamp prev_tm (tm);

      // Get next max or min.
      int hiflag;
      tm = te.tm = next_high_or_low_tide (prev_tm, hiflag, forward);
      te.etype = (hiflag ? TideEvent::max : TideEvent::min);
      finishTideEvent (te);
      if (te.tm >= start_tm && te.tm < end_tm)
        organizer.add (te);

      // Check for slacks, if applicable.  Skip the ones that need
      // interpolation; those are done in second pass.
      // (N.B., hiflag pertains to right side of bracket)
      if (filter != MAXMIN && isCurrent && ((hiflag && have_floodbegins()) ||
      ((!hiflag) && have_ebbbegins()))) {
        te.tm = find_mark_crossing (prev_tm, tm,
	  PredictionValue(constants->datum().Units(), 0.0), risingflag,
          forward);
        if (!(te.tm.isNull())) {
          te.etype = (risingflag ? TideEvent::slackrise
                                 : TideEvent::slackfall);
          finishTideEvent (te);
          if (te.tm >= start_tm && te.tm < end_tm)
            organizer.add (te);
        }
      }

      // Check for mark, if applicable.
      if ((!have_residuals()) && markLevel && (filter == NOFILTER)) {
        te.tm = find_mark_crossing (prev_tm, tm, *markLevel, risingflag,
          forward);
        if (!(te.tm.isNull())) {
          te.etype = (risingflag ? TideEvent::markrise
                                 : TideEvent::markfall);
          finishTideEvent (te);
          if (te.tm >= start_tm && te.tm < end_tm)
            organizer.add (te);
        }
      }
    }
  }

  // Tide events, pass 2:  ugliness to support mark crossings and
  // slacks on sub stations with residuals.
  if (have_residuals() && filter == NOFILTER &&
       (markLevel || !have_floodbegins() || !have_ebbbegins())) {
    TideEvent new_te;
    new_te.isCurrent = isCurrent;

    // Problem #1.  The organizer above might be full of other events
    // from previous calls that will confuse us -- both future and
    // past ranges and non-tide events.  Get rid of them.
    TideEventsOrganizer temporg;
    TideEventsIterator it = organizer.lower_bound(start_tm.timet());
    TideEventsIterator stop = organizer.lower_bound(end_tm.timet());
    while (it != stop) {
      TideEvent &te = it->second;
      switch (te.etype) {
      case TideEvent::max:
      case TideEvent::min:
        temporg.add (te);
        break;
      case TideEvent::slackrise:
        if (have_floodbegins())
          temporg.add (te);
        break;
      case TideEvent::slackfall:
        if (have_ebbbegins())
          temporg.add (te);
        break;
      default:
        ;
      }
      it++;
    }

    // Problem #2.  Need to initialize it if empty.
    Interval delta;
    for (delta = Interval(DAYSECONDS); temporg.empty(); delta *= 2U)
      predictTideEvents (start_tm - delta, end_tm + delta, temporg, KNOWNTIDEEVENTS);

    // Problem #3.  We need to extend the range to be sure of getting
    // the first and last mark crossing and slack.
    // (Brace yourself for recursion.)
    for (delta = Interval(DAYSECONDS); temporg.begin()->second.tm >= start_tm; delta *= 2U)
      extendRange (temporg, backward, delta, KNOWNTIDEEVENTS);
    for (delta = Interval(DAYSECONDS); temporg.rbegin()->second.tm < end_tm; delta *= 2U)
      extendRange (temporg, forward, delta, KNOWNTIDEEVENTS);

    // OK great.
    it = temporg.begin();
    TideEvent left_te = it->second;
    it++;
    while (it != temporg.end()) {
      TideEvent right_te = it->second;

      assert (!left_te.uncorrected_tm.isNull());
      assert (!right_te.uncorrected_tm.isNull());

      // We have a bracket as used in
      // SubordinateStation::predictTideLevel.  However, it isn't
      // necessarily a nice bracket for find_mark_crossing.  Do it
      // anyway and hope that find_mark_crossing will have the sense
      // to return null when there's no good answer.

      // Check for slacks, if applicable.
      if (isCurrent && (
        (left_te.etype == TideEvent::max && !have_ebbbegins()) ||
	(left_te.etype == TideEvent::min && !have_floodbegins()))) {
	new_te = find_mark_crossing (left_te, right_te,
          PredictionValue(constants->datum().Units(), 0.0), risingflag);
	if (!(new_te.tm.isNull())) {
	  new_te.etype = (risingflag ? TideEvent::slackrise
				     : TideEvent::slackfall);
	  finishTideEvent (new_te); // This'll get the corrected pv.
	  if (new_te.tm >= start_tm && new_te.tm < end_tm)
	    organizer.add (new_te);
	}
      }

      // Check for mark, if applicable.
      if (markLevel) {
	new_te = find_mark_crossing (left_te, right_te, *markLevel,
				     risingflag);
	if (!(new_te.tm.isNull())) {
	  new_te.etype = (risingflag ? TideEvent::markrise
				     : TideEvent::markfall);
	  finishTideEvent (new_te); // This'll get the corrected pv.
	  if (new_te.tm >= start_tm && new_te.tm < end_tm)
	    organizer.add (new_te);
	}
      }

      left_te = right_te;
      it++;
    }
  }

  // If sun and moon events not disabled...
  if (((*(context->settings))["ns"].c == 'n') && (filter == NOFILTER)) {
    TideEvent te;
    te.isCurrent = isCurrent;

    if (!(coordinates.isNull())) {
      // Add sunrises and sunsets.
      te.tm = start_tm;
      find_next_rise_or_set (te.tm, coordinates, te.etype, forward, 0);
      while (te.tm < end_tm) {
        organizer.add (te);
        find_next_rise_or_set (te.tm, coordinates, te.etype, forward, 0);
      }

      // Add twilight
      te.tm = start_tm;
      find_next_rise_or_set (te.tm, coordinates, te.etype, forward, 2);
      while (te.tm < end_tm) {
        organizer.add (te);
        find_next_rise_or_set (te.tm, coordinates, te.etype, forward, 2);
      }


      // Add moonrises and moonsets.
      if (start_tm.moonriseset_inrange()) {
        te.tm = start_tm;
        find_next_rise_or_set (te.tm, coordinates, te.etype, forward, 1);
        while (te.tm < end_tm) {
          organizer.add (te);
          if (!(te.tm.moonriseset_inrange()))
            break;
          find_next_rise_or_set (te.tm, coordinates, te.etype, forward, 1);
        }
      }
    }

    // Add moon phases.
    te.tm = start_tm;
    find_next_moon_phase (te.tm, te.etype, forward);
    while (te.tm < end_tm) {
      organizer.add (te);
      find_next_moon_phase (te.tm, te.etype, forward);
    }
  }
}

// Analogous, for raw readings.
void Station::predictRawEvents (Timestamp start_tm, Timestamp end_tm,
TideEventsOrganizer &organizer) {
  assert (step > Interval(0));
  assert (start_tm <= end_tm);
  TideEvent te;
  te.isCurrent = isCurrent;
  te.etype = TideEvent::rawreading;
  for (Timestamp t = start_tm; t < end_tm; t += step) {
    te.tm = t;
    finishTideEvent (te);
    organizer.add (te);
  }
}

// Add events to an organizer to extend its range in the specified
// direction by the specified interval.  (Number of events is
// indeterminate.)  A safety margin is used to attempt to prevent tide
// events from falling through the cracks as discussed above
// predictTideEvents.
//
// Either settings or the filter arg can suppress sun and moon events.
void Station::extendRange (TideEventsOrganizer &organizer, Direction d,
                           Interval howmuch, TideEventsFilter filter) {
  Timestamp start_tm, end_tm;
  howmuch = abs(howmuch);
  if (d == forward) {
    TideEventsRiterator it = organizer.rbegin();
    assert (it != organizer.rend());
    start_tm = it->second.tm;
    end_tm = start_tm + howmuch;
    start_tm -= Interval(60);
  } else {
    TideEventsIterator it = organizer.begin();
    assert (it != organizer.end());
    end_tm = it->second.tm;
    start_tm = end_tm - howmuch;
    end_tm += Interval(60);
  }
  predictTideEvents (start_tm, end_tm, organizer, filter);
}

// Analogous, for raw readings.  Specify number of events in howmany.
void Station::extendRange (TideEventsOrganizer &organizer, Direction d,
unsigned howmany) {
  Timestamp start_tm, end_tm;
  assert (step > Interval(0));
  if (d == forward) {
    TideEventsRiterator it = organizer.rbegin();
    assert (it != organizer.rend());
    start_tm = it->second.tm + step;
    end_tm = start_tm + step * howmany;
  } else {
    TideEventsIterator it = organizer.begin();
    assert (it != organizer.end());
    end_tm = it->second.tm;
    start_tm = end_tm - step * howmany;
  }
  predictRawEvents (start_tm, end_tm, organizer);
}

PredictionValue Station::predictTideLevel (Timestamp tm) {
  PredictionValue pv_out;
  pv_out = constants->predictHeight (tm, 0);
  if (pv_out.Units().mytype == PredictionValue::Unit::KnotsSquared)
    pv_out.Units (PredictionValue::Unit::Knots);
  pv_out += constants->datum();
  return pv_out;
}

// N.B., see the .cc of xxDrawable::global_redraw() regarding an
// important dependency on how this is implemented.
Station *Station::clone() {
  HarmonicsFile h (*harmonicsFileName, context->settings);
  Station *s = h.getStation (TCDRecordNumber, context);
  if (markLevel)
    s->markLevel = new PredictionValue(*markLevel);
  else
    s->markLevel = NULL;
  s->aspect = aspect;
  s->step = step;
  // markLevel is copied verbatim above, so the units will already be correct.
  if (s->myUnits != myUnits)
    s->setUnits (myUnits);
  return s;
}

void Station::setUnits (PredictionValue::Unit in_units) {
  // FIXME if there are ever units of velocity other than knots
  assert (!isCurrent);
  if (myUnits != in_units) {
    myUnits = in_units;
    constants->setUnits (in_units);
  }
  if (markLevel)
    if (markLevel->Units() != in_units)
      markLevel->Units (in_units);
}

Station::~Station() {
  // I put everything in the subclasses because of some compiler
  // problem...
}

void Station::about (Dstr &text_out, char mode) const {
  unsigned maxnamelen = 0;
  assert (mode == 't' || mode == 'h');
  if (mode == 'h')
    text_out = "<table>\n";
  else {
    MetaFieldIterator it = metadata.begin();
    while (it != metadata.end()) {
      if (it->name.length() > maxnamelen)
        maxnamelen = it->name.length();
      it++;
    }
  }
  MetaFieldIterator it = metadata.begin();
  while (it != metadata.end()) {
    if (mode == 'h') {
      text_out += "<tr><td valign=top>";
      text_out += it->name;
      text_out += "</td><td valign=top><pre>";
      text_out += it->value;
      text_out += "</pre></td></td>\n";
    } else {
      Dstr tmp1 (it->name), tmp2 (it->value), tmp3;
      tmp1.pad (maxnamelen+2);
      tmp2.getline (tmp3);
      tmp1 += tmp3;
      tmp1 += '\n';
      while (tmp2.length()) {
        tmp3 = "";
        tmp3.pad (maxnamelen+2);
        tmp1 += tmp3;
        tmp2.getline (tmp3);
        tmp1 += tmp3;
        tmp1 += '\n';
      }
      text_out += tmp1;
    }
    it++;
  }
  if (mode == 'h')
    text_out += "</table>\n";
}
