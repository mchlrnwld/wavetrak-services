// $Id: xxHorizDialog.cc,v 1.3 2004/04/19 20:16:56 flaterco Exp $
/*  xxHorizDialog  More compact version of Dialog widget.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

char *xxHorizDialog::val() {
  return buf;
}

xxHorizDialog::xxHorizDialog (xxContext *in_xxcontext, const char *caption,
const char *init) {
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNorientation, (XtArgVal)XtorientHorizontal}
    };
    Widget boxwidget = XtCreateManagedWidget ("", boxWidgetClass,
      in_xxcontext->manager, args, 3);
    box = new xxContext (in_xxcontext, boxwidget);
  }
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNborderWidth, (XtArgVal)0}
    };
    Widget labelwidget = XtCreateManagedWidget (caption,
      labelWidgetClass, box->manager, args, 3);
    label = new xxContext (box, labelwidget);
  }
  assert (strlen (init) < 80);
  strcpy (buf, init);
  {
    Arg args[6] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNstring, (XtArgVal)buf},
      {XtNuseStringInPlace, (XtArgVal)1},
      {XtNlength, (XtArgVal)79},
      {XtNeditType, (XtArgVal)XawtextEdit}
    };
    Widget textwidget = XtCreateManagedWidget ("", asciiTextWidgetClass,
      box->manager, args, 6);
    text = new xxContext (box, textwidget);
  }
}

xxHorizDialog::~xxHorizDialog () {
  delete label;
  delete text;
  delete box;
}

void xxHorizDialog::global_redraw() {
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)box->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };
  XtSetValues (label->manager, args, 2);
  XtSetValues (text->manager, args, 2);
  XtSetValues (box->manager, args, 2);
}
