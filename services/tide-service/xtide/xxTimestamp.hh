// $Id: xxTimestamp.hh,v 1.1 2002/04/29 15:10:44 flaterco Exp $
/*  xxTimestamp  Get a Timestamp from the user.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxTimestamp: public xxWindow {
  friend void xxTimestampCallback (Widget w, XtPointer client_data,
    XtPointer call_data);

public:
  xxTimestamp (xxTideContext *in_xtidecontext, xxContext *context,
     void (*in_timestampcallback) (Timestamp t, void *in_ptr),
     void *in_ptr, Timestamp init, const Dstr &in_timezone);
  ~xxTimestamp();
  void dismiss();

protected:
  void *ptr;
  void (*timestampcallback) (Timestamp t, void *in_ptr);
  Dstr timezone;
  xxContext *gobutton, *cancelbutton, *helplabel, *spacelabel1;
  xxTimestampDialog *mydialog;
};
