// Speed:  angular units over time units.
// The Angle class is not used here since normalization is not what we want.

// Once again we have unnecessary roundoff if rad_per_second is the wrong
// storage representation.

/*
    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Speed {
protected:
  double rad_per_second;

public:
  enum units {SECOND, HOUR, JULIAN_CENTURY};

  // Get.
  double rad (units per) const;
  double deg (units per) const;

  // Set.
  void rad (double inval, units per);
  void deg (double inval, units per);

  Speed &operator+= (Speed a);
  Speed &operator*= (double a);
};

// Operations.
Speed operator+ (Speed a, Speed b);
Speed operator* (double a, Speed b);

class DegreesPerJulianCentury: public Speed {
public:
  DegreesPerJulianCentury(double inval);
};

class DegreesPerHour: public Speed {
public:
  DegreesPerHour(double inval);
};
