// $Id: xxTimestamp.cc,v 1.3 2004/04/19 20:16:56 flaterco Exp $
/*  xxTimestamp  Get a Timestamp from the user.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxTimestampCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxTimestamp *e = (xxTimestamp *)client_data;
  Dstr time_string;
  e->mydialog->val (time_string);
  Timestamp t (time_string, e->timezone, e->xtidecontext->settings);
  if (t.isNull()) {
    Dstr details ("The offending input was ");
    details += time_string;
    details += "\nin the time zone ";
    if ((*(e->xtidecontext->settings))["z"].c == 'n')
      details += e->timezone;
    else
      details += "UTC0";
    barf (MKTIME_FAILED, details, 0);
  } else {
    (*(e->timestampcallback)) (t, e->ptr);
  }
  delete e;
}

static void
xxTimestampCancelCallback (Widget w, XtPointer client_data, XtPointer
call_data) {
  xxTimestamp *e = (xxTimestamp *)client_data;
  e->dismiss();
}

void
xxTimestamp::dismiss() {
  delete this;
}

xxTimestamp::~xxTimestamp() {
  mypopup->unrealize();
  delete helplabel;
  delete gobutton;
  delete cancelbutton;
  delete mydialog;
  delete spacelabel1;
}

xxTimestamp::xxTimestamp (xxTideContext *in_xtidecontext, xxContext *context,
     void (*in_timestampcallback) (Timestamp t, void *in_ptr),
     void *in_ptr, Timestamp init, const Dstr &in_timezone):
xxWindow (in_xtidecontext, context, 1, XtGrabExclusive) {
  assert (!(init.isNull()));

  ptr = in_ptr;
  timestampcallback = in_timestampcallback;
  timezone = in_timezone;

  mypopup->setTitle ("Select Time");

  Arg labelargs[3] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNborderWidth, (XtArgVal)0}
  };
  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)mypopup->visual},
    {XtNcolormap, (XtArgVal)mypopup->colormap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
#if 0
  Arg smallbuttonargs[6] =  {
    {XtNvisual, (XtArgVal)mypopup->visual},
    {XtNcolormap, (XtArgVal)mypopup->colormap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNfont, (XtArgVal)mypopup->tinyfontstruct},
    {XtNvertSpace, (XtArgVal)0}
  };
#endif

  {
    Dstr f;
    f = "Adjust year, month, day, hours, and minutes to desired time in time\n\
zone ";
    if ((*(in_xtidecontext->settings))["z"].c == 'n')
      f += in_timezone;
    else
      f += "UTC0";
    f += " using 24-hour notation instead of AM/PM.";
    Widget labelwidget = XtCreateManagedWidget (f.aschar(), labelWidgetClass,
      container->manager, labelargs, 3);
    helplabel = new xxContext (mypopup, labelwidget);
  }

  mydialog = new xxTimestampDialog (xtidecontext, container, "Set:",
    init, timezone);

  {
    Widget labelwidget = XtCreateManagedWidget ("  ", labelWidgetClass,
      container->manager, labelargs, 3);
    spacelabel1 = new xxContext (mypopup, labelwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Go", commandWidgetClass,
      container->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxTimestampCallback,
     (XtPointer)this);
    gobutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Cancel", commandWidgetClass,
      container->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxTimestampCancelCallback,
      (XtPointer)this);
    cancelbutton = new xxContext (mypopup, buttonwidget);
  }

  mypopup->realize();
}
