// $Id: tide.cc,v 1.12 2004/09/08 17:00:43 flaterco Exp $
/*  tide  Command-line client for dusty old TTYs and line printers.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

#define fmtbarf { \
        Dstr details ("Can't do format "); \
        details += format; \
        details += " in mode "; \
        details += mode; \
        barf (BAD_FORMAT, details); \
      }

void
do_location (const Dstr &name, TideContext *context, Interval step,
FILE *outfp, char mode, char format) {
  StationRef *sr;
  Settings &settings = *(context->settings);
  sr = (*(context->stationIndex()))[name];
  if (sr) {
    Station *s = sr->load(context);
    s->step = step;

    // Install mark level, if applicable.
    {
      Configurable &cfbl = settings["ml"];
      if (!cfbl.isnull) {
        s->markLevel = new PredictionValue (cfbl.p);
        if (cfbl.p.Units() != s->myUnits)
          s->markLevel->Units (s->myUnits);
      }
    }

    // Find start and end times.
    // This needs to be done here due to the timezones.
    Timestamp startt, endt;
    {
      Configurable &cfbl = settings["b"];
      if (cfbl.isnull)
	startt = Timestamp ((time_t)(time(NULL)));
      else {
	startt = Timestamp (cfbl.s, sr->timezone, context->settings);
	if (startt.isNull()) {
	  Dstr details ("The offending input was ");
	  details += cfbl.s;
	  details += "\nin the time zone ";
	  if (settings["z"].c == 'n')
	    details += sr->timezone;
	  else
	    details += "UTC0";
	  barf (MKTIME_FAILED, details);
	}
      }
    }
    {
      Configurable &cfbl = settings["e"];
      if (cfbl.isnull)
	endt = startt + Interval(defpredictinterval);
      else {
	endt = Timestamp (cfbl.s, sr->timezone, context->settings);
	if (endt.isNull()) {
	  Dstr details ("The offending input was ");
	  details += cfbl.s;
	  details += "\nin the time zone ";
	  if (settings["z"].c == 'n')
	    details += sr->timezone;
	  else
	    details += "UTC0";
	  barf (MKTIME_FAILED, details);
	}
      }
    }
    if (startt > endt) {
      Timestamp tt = startt;
      startt = endt;
      endt = tt;
    }

    switch (mode) {
    case 'a':
      if (format != 't' && format != 'h')
        fmtbarf;
      {
        Dstr text_out;
        s->about (text_out, format);
        fprintf (outfp, "%s", text_out.aschar());
      }
      break;

    case 'p':
      if (format != 't' && format != 'c')
        fmtbarf;
      {
        Dstr text_out;
        context->plainMode (s, startt, endt, text_out, format);
        fprintf (outfp, "%s", text_out.aschar());
      }
      break;

    case 's':
      if (format != 't')
        fmtbarf;
      {
        Dstr text_out;
        context->statsMode (s, startt, endt, text_out);
        fprintf (outfp, "%s", text_out.aschar());
      }
      break;

    case 'c':
      switch (format) {
      case 'c':
      case 'h':
      case 'i':
      case 't':
        {
          Dstr text_out;
          context->calendarMode (s, startt, endt, text_out, format, 0);
          fprintf (outfp, "%s", text_out.aschar());
        }
        break;
      default:
        fmtbarf;
      }
      break;

    case 'C':
      switch (format) {
      case 'h':
      case 'i':
      case 't':
        {
          Dstr text_out;
          context->calendarMode (s, startt, endt, text_out, format, 1);
          fprintf (outfp, "%s", text_out.aschar());
        }
        break;
      default:
        fmtbarf;
      }
      break;

    case 'r':
    case 'm':
      if (format != 't' && format != 'c')
        fmtbarf;
      {
        Dstr text_out;
        context->rareModes (s, startt, endt, text_out, mode, format);
        fprintf (outfp, "%s", text_out.aschar());
      }
      break;

    case 'b':
      if (format != 't')
        fmtbarf;
      {
        Dstr text_out;
        context->bannerMode (s, startt, endt, text_out);
        fprintf (outfp, "%s", text_out.aschar());
      }
      break;

    case 'g':
      switch (format) {
      case 'p':
        {
          RGBGraph g (settings["gw"].u, settings["gh"].u, context->colors);
          g.drawTides (s, startt);
          png_file_ptr = outfp;  // See externC.cc
	  g.writeAsPNG (file_write_data_fn);
        }
        break;
      case 't':
        {
          Dstr text_out;
          TTYGraph g (settings["tw"].u, settings["th"].u);
          g.drawTides (s, startt);
	  g.writeAsText (text_out);
          fprintf (outfp, "%s", text_out.aschar());
        }
        break;
      default:
        fmtbarf;
      }
      break;

    default:
      {
        Dstr details ("Unsupported mode: ");
        details += mode;
        barf (BAD_MODE, details);
      }
    }

    delete s;

  } else {
    Dstr details ("Could not find: ");
    details += name;
    barf (STATION_NOT_FOUND, details, 0);
  }
}

void
loop_locations (TideContext *context, FILE *outfp) {
  Settings &settings = *(context->settings);
  int is_first = 1;

  Interval step (HOURSECONDS);
  {
    Configurable &cfbl = settings["s"];
    if (!cfbl.isnull) {
      step = Interval (cfbl.s);
      if (step <= Interval (0)) {
	Dstr details ("You must specify a positive step.  You tried ");
	details += cfbl.s;
	barf (NUMBER_RANGE_ERROR, details);
      }
    }
  }

  char mode, format;
  {
    Configurable &cfbl = settings["m"];
    if (cfbl.isnull)
      mode = 'p';
    else
      mode = cfbl.c;
  }
  {
    Configurable &cfbl = settings["f"];
    if (cfbl.isnull)
      format = 't';
    else
      format = cfbl.c;
  }

  if (mode != 'l') {
    Configurable &cfbl = settings["l"];
    DvectorIterator it = cfbl.v.begin();
    DvectorIterator stop = cfbl.v.end();
    if (it == stop)
      fprintf (stderr, "Warning:  No locations specified with -l; hence, no output.\n");
    while (it != stop) {
      if (is_first)
	is_first = 0;
      else
	fprintf (outfp, "%s", stationsep);
      do_location (*it, context, step, outfp, mode, format);
      it++;
    }

  } else { // List mode
    switch (format) {
    case 't':
      {
	Dstr text_out;
	context->listModePlain (text_out);
	fprintf (outfp, "%s", text_out.aschar());
      }
      break;
    case 'h':
      {
	Dstr text_out;
	context->listModeHTML (text_out);
	fprintf (outfp, "%s", text_out.aschar());
      }
      break;
    default:
      fmtbarf;
    }
  }
}

int main (int argc, char **argv)
{
  // So much for ANSI.
  // sync_with_stdio();

  srand (time (NULL));

  Settings settings;
  settings.applyUserDefaults();
  settings.applyCommandLine (argc, argv);

  TideContext *context = new TideContext (new Colors (settings), &settings);

  if (argc < 2 && settings["l"].v.empty()) {
    fprintf (stderr, "\
Minimal usage:  tide -l \"Location name\" (or set the environment variable\n\
  XTIDE_DEFAULT_LOCATION to \"Location name\")\n\
Other switches:\n\
  -b \"YYYY-MM-DD HH:MM\"\n\
      Specify the begin (start) time for predictions.\n\
  -e \"YYYY-MM-DD HH:MM\"\n\
      Specify the end (stop) time for predictions.\n\
  -f c|h|i|p|t\n\
      Specify the output format as CSV, HTML, iCalendar, PNG, or text.  The\n\
      default is text.  Only calendar and list modes can do HTML; only graph\n\
      mode can do PNG.\n\
  -m a|b|c|C|g|l|m|p|r|s\n\
      Specify mode to be about, banner, calendar, alt. calendar, graph, list,\n\
      medium rare, plain, raw, or stats.  The default is plain.\n\
  -o \"filename\"\n\
      Redirect output to the specified file (appends).\n\
  -s \"HH:MM\"\n\
      Specify the step interval, in hours and minutes, for raw\n\
      mode predictions.  The default is one hour.\n\
  -v\n\
      Print version string and exit.\n\
\n\
NOTE:  These are only the most important switches.  For information on\n\
all of the switches, please read the verbose documentation at:\n\
    http://www.flaterco.com/xtide/\n");
    exit (-1);
  }

  FILE *outfp = stdout;
  {
    Configurable &cfbl = settings["o"];
    if (!cfbl.isnull) {
      if (!(outfp = fopen (cfbl.s.aschar(), "a"))) {
	Dstr details (cfbl.s);
	details += ": ";
	details += strerror (errno);
	details += ".";
	barf (CANT_OPEN_FILE, details);
      }
    }
  }

  loop_locations (context, outfp);

  exit (0);
}
