// $Id: xxRareModeSavePrompts.hh,v 1.1 2002/04/29 15:10:42 flaterco Exp $
/*  xxRareModeSavePrompts  Get a file name and two timestamps from the user.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxRareModeSavePrompts: public xxWindow {
  friend void xxRareModeSavePromptsCallback (Widget w, XtPointer client_data,
    XtPointer call_data);

public:
  xxRareModeSavePrompts (xxTideContext *in_xtidecontext, xxContext *context,
     void (*in_callback) (Dstr &filename, Timestamp start_tm, Timestamp end_tm, void *in_ptr), void *in_ptr,
     char *initname, Timestamp in_start_tm, Timestamp in_end_tm,
     const Dstr &in_timezone);
  ~xxRareModeSavePrompts();
  void dismiss();

protected:
  Dstr timezone;
  xxHorizDialog *filenamediag;
  xxTimestampDialog *btimediag, *etimediag;
  xxContext *helplabel1, *gobutton, *cancelbutton, *spacelabel1;
  void (*callback) (Dstr &filename, Timestamp start_tm, Timestamp end_tm, void *in_ptr);
  void *ptr;
};
