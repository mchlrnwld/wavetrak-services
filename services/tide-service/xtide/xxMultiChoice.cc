// $Id: xxMultiChoice.cc,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxMultiChoice  Multiple-choice button widget.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

unsigned xxMultiChoice::choice () {
  return current_choice;
}

void xxMultiChoiceButtonCallback (Widget w, XtPointer client_data,
			   XtPointer call_data) {
  struct xxMultiChoice::numberedbutton *e =
    (struct xxMultiChoice::numberedbutton *)client_data;
  e->parent->current_choice = e->number;
  Arg args[1] = {
    {XtNlabel, (XtArgVal)(e->parent->choices[e->number])}
  };
  XtSetValues (e->parent->button->manager, args, 1);
  e->parent->button->refresh();
  if (e->parent->cb)
    (*(e->parent->cb))(e->parent->cbdata);
}

// Code common to both constructors.
void
xxMultiChoice::construct() {
  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)box->visual},
    {XtNcolormap, (XtArgVal)box->colormap},
    {XtNbackground, (XtArgVal)box->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };

  // The menu
  {
    Arg menuargs[5] = {
      {XtNvisual, (XtArgVal)box->visual},
      {XtNcolormap, (XtArgVal)box->colormap},
      {"menuName", (XtArgVal)"menu"},
      {XtNbackground, (XtArgVal)box->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
    };
    Widget buttonwidget = XtCreateManagedWidget (choices[current_choice],
      menuButtonWidgetClass, box->manager, menuargs, 5);
    button = new xxContext (box, buttonwidget);
  }
  {
    Widget menushell = XtCreatePopupShell ("menu",
      simpleMenuWidgetClass, button->manager, buttonargs, 4);
    menu = new xxContext (box, menushell);
  }

  num_choices = 0;
  while (choices[num_choices])
    num_choices++;
  assert (num_choices);
  choicebuttons = new struct numberedbutton [num_choices];

  // Buttons on the menu
  unsigned y;
  for (y=0; y<num_choices; y++) {
    Widget buttonwidget = XtCreateManagedWidget (choices[y], smeBSBObjectClass,
      menu->manager, buttonargs, 4);
    choicebuttons[y].button = new xxContext (box, buttonwidget);
    choicebuttons[y].parent = this;
    choicebuttons[y].number = y;
    XtAddCallback (buttonwidget, XtNcallback, xxMultiChoiceButtonCallback, (XtPointer)(&(choicebuttons[y])));
  }
}

xxMultiChoice::xxMultiChoice (xxContext *in_xxcontext, char *caption,
			      char **in_choices, unsigned first_choice) {
  nobox = 0;
  cb = NULL;
  choices = in_choices;
  current_choice = first_choice;
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNorientation, (XtArgVal)XtorientHorizontal}
    };
    Widget boxwidget = XtCreateManagedWidget ("", boxWidgetClass,
      in_xxcontext->manager, args, 3);
    box = new xxContext (in_xxcontext, boxwidget);
  }
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNborderWidth, (XtArgVal)0}
    };
    Widget labelwidget = XtCreateManagedWidget (caption,
      labelWidgetClass, box->manager, args, 3);
    label = new xxContext (box, labelwidget);
  }
  construct();
}

xxMultiChoice::xxMultiChoice (xxContext *in_boxcontext, void (*cb_in)(void*),
void *cbdata_in, char **in_choices, unsigned first_choice) {
  nobox = 1;
  cb = cb_in;
  cbdata = cbdata_in;
  choices = in_choices;
  current_choice = first_choice;
  box = in_boxcontext;
  construct();
}

xxMultiChoice::~xxMultiChoice () {
  for (unsigned a=0; a<num_choices; a++)
    delete choicebuttons[a].button;
  delete [] choicebuttons;
  delete menu;
  delete button;
  if (!nobox) {
    delete label;
    delete box;
  }
}

void xxMultiChoice::global_redraw() {
  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)box->visual},
    {XtNcolormap, (XtArgVal)box->colormap},
    {XtNbackground, (XtArgVal)box->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };
  XtSetValues (button->manager, buttonargs, 4);
  XtSetValues (menu->manager, buttonargs, 4);
  for (unsigned a=0; a<num_choices; a++)
    XtSetValues (choicebuttons[a].button->manager, buttonargs, 4);
  if (!nobox) {
    Arg args[2] =  {
      {XtNbackground, (XtArgVal)box->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
    };
    XtSetValues (label->manager, args, 2);
    XtSetValues (box->manager, args, 2);
  }
}
