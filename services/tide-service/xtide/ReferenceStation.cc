// $Id: ReferenceStation.cc,v 1.9 2004/11/17 16:27:18 flaterco Exp $
/*  ReferenceStation  Implementation for regular tide station.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

ReferenceStation::ReferenceStation (TideContext *in_tidecontext,
ConstantSetWrapper *in_constants): Station (in_tidecontext) {
  // Other fields are filled in in HarmonicsFile::getStation.
  assert (in_constants);
  constants = in_constants;
}

ReferenceStation::~ReferenceStation() {
  if (constants)
    delete constants;
  if (markLevel)
    delete markLevel;
}

int ReferenceStation::is_reference_station() {
  return 1;
}

int ReferenceStation::have_residuals() {
  return 0;
}

int ReferenceStation::have_floodbegins() {
  return 1;
}

int ReferenceStation::have_ebbbegins() {
  return 1;
}

void ReferenceStation::finishTideEvent (TideEvent &te) {
  if (!te.isSunMoonEvent())
    te.pv = predictTideLevel (te.tm);
}
