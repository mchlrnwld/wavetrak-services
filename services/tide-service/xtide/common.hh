// 	$Id: common.hh,v 1.14 2004/11/16 21:26:16 flaterco Exp $	
// Common includes for xtide, tide, and xttpd

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

// XTide uses side-effects from asserts.  Disable "optimization" that
// eliminates these.
#undef NDEBUG
#include <assert.h>

#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <ctype.h>
#include <syslog.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <pwd.h>
#include <png.h>
#include <limits.h>
#include <map>
#include <vector>
#include <string>

#if PNG_LIBPNG_VER < 96
#error Sorry, you must have libpng version 0.96 or newer.  See README for details.
#endif

/* Compatibility by Dale DePriest */

#ifdef OS2
#include <float.h>
#endif /* OS2 */

#include "config.hh"

// This must be done before including the XTide headers.
#ifdef TIME_WORKAROUND
// We want time_t to be a signed 64 bit integer.
#if LONG_MAX == 2147483647L
#define time_t long long
#elif LONG_MAX == 9223372036854775807L
#define time_t long
#else
#error Cannot guess C name of integer*8; fix me in common.hh
#endif
#endif

#include "Dstr.hh"
#include "MetaField.hh"
#include "PredictionValue.hh"
#include "Configurable.hh"
#include "Colors.hh"
#include "Settings.hh"
#include "errors.hh"
#include "HarmonicsPath.hh"
#include "Amplitude.hh"
#include "Year.hh"
#include "Angle.hh"
#include "NullableAngle.hh"
#include "Speed.hh"
#include "Interval.hh"
#include "NullableInterval.hh"
#include "Offsets.hh"
#include "Timestamp.hh"
#include "TabulatedConstituent.hh"
#include "Coordinates.hh"
#include "StationRef.hh"
#include "HarmonicsFile.hh"
#include "StationIndex.hh"
#include "ConstituentSet.hh"
#include "ConstantSet.hh"
#include "ConstantSetWrapper.hh"
#include "TideEvent.hh"
#include "TideEventsOrganizer.hh"
#include "Station.hh"
#include "TideContext.hh"
#include "ReferenceStation.hh"
#include "SubordinateStation.hh"
#include "xmlstruct.hh"
#include "skycal.hh"
#include "ZoneIndex.hh"
#include "Graph.hh"
#include "RGBGraph.hh"
#include "TTYGraph.hh"
#include "Calendar.hh"
#include "Banner.hh"

// Stuff in externC.cc
extern FILE *png_file_ptr;
extern int png_socket;
extern "C" void
  file_write_data_fn (png_structp png_ptr, png_bytep b_ptr, png_size_t sz);
extern "C" void
  socket_write_data_fn (png_structp png_ptr, png_bytep b_ptr, png_size_t sz);
extern "C" int compare_stationrefs_name (const void *a, const void *b);
extern "C" int compare_stationrefs_lat (const void *a, const void *b);
extern "C" int compare_stationrefs_lng (const void *a, const void *b);

// Stuff for access to XML parser.
extern struct xmltag *xmlparsetree;
extern Dstr xmlfilename;
extern FILE *xmlin;
int xmlparse();
// ECHO is defined by /usr/include/asm/termbits.h; we don't want that.
#undef ECHO
#define ECHO  /* Don't echo error text */

#ifndef max
#define max(a,b) (a < b ? b : a)
#endif
#ifndef min
#define min(a,b) (a > b ? b : a)
#endif
