/* $Id: Settings.cc,v 1.8 2004/09/07 21:23:30 flaterco Exp $ */
/*  Settings  XTide global settings

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"


int Settings::checkboolean (char *culprit, char val) {
  if (val != 'y' && val != 'n') {
    if (culprit) {
      Dstr details ("The offending input in ");
      details += culprit;
      details += " was '";
      details += val;
      details += "' (expecting 'y' or 'n').";
      barf (BAD_BOOL, details);
    }
    return 1;
  }
  return 0;
}

int Settings::checkboolean (char *culprit, char *val) {
  if (val)
    if (strlen (val) == 1)
      return checkboolean (culprit, *val);
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting 'y' or 'n').";
    barf (BAD_BOOL, details);
  }
  return 1;
}

int Settings::checkposint (char *culprit, int val) {
  if (val <= 0) {
    if (culprit) {
      Dstr details ("The offending input in ");
      details += culprit;
      details += " was '";
      details += val;
      details += "' (expecting a positive int).";
      barf (NUMBER_RANGE_ERROR, details);
    }
    return 1;
  }
  return 0;
}

int Settings::checkposdouble (char *culprit, double val) {
  if (val <= 0.0) {
    if (culprit) {
      Dstr details ("The offending input in ");
      details += culprit;
      details += " was '";
      details += val;
      details += "' (expecting a positive double).";
      barf (NUMBER_RANGE_ERROR, details);
    }
    return 1;
  }
  return 0;
}

int Settings::checkgldouble (char *culprit, double val) {
  if (val != -180.0 &&
      val != -150.0 &&
      val != -120.0 &&
      val != -90.0 &&
      val != -60.0 &&
      val != -30.0 &&
      val !=  0.0 &&
      val !=  30.0 &&
      val !=  60.0 &&
      val !=  90.0 &&
      val !=  120.0 &&
      val !=  150.0 &&
      val !=  360.0) {
    if (culprit) {
      Dstr details ("The offending input in ");
      details += culprit;
      details += " was '";
      details += val;
      details += "' (expecting one of the following:\n";
      details += "-180 -150 -120 -90 -60 -30 0 30 60 90 120 150 360).";
      barf (NUMBER_RANGE_ERROR, details);
    }
    return 1;
  }
  return 0;
}

#define legalmodes "abcCglmprs"

int Settings::checkmode (char *culprit, char val) {
  if (strchr (legalmodes, val))
    return 0;
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting one of ";
    details += legalmodes;
    details += ".";
    barf (BAD_MODE, details);
  }
  return 1;
}

int Settings::checkmode (char *culprit, char *val) {
  if (val)
    if (strlen (val) == 1)
      return checkmode (culprit, *val);
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting one of ";
    details += legalmodes;
    details += ".";
    barf (BAD_MODE, details);
  }
  return 1;
}

#define legalformats "chipt"

int Settings::checkformat (char *culprit, char val) {
  if (strchr (legalformats, val))
    return 0;
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting one of ";
    details += legalformats;
    details += ".";
    barf (BAD_FORMAT, details);
  }
  return 1;
}

int Settings::checkformat (char *culprit, char *val) {
  if (val)
    if (strlen (val) == 1)
      return checkformat (culprit, *val);
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting one of ";
    details += legalformats;
    details += ".";
    barf (BAD_FORMAT, details);
  }
  return 1;
}

int Settings::checkcolor (char *culprit, char *val) {
  // This is not very rigorous checking, but Parsecolor wants to print
  // warnings.  More rigorous version in xxXTideRoot.
  if (val)
    if (isalpha(*val) || *val == '#')
      return 0;
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "'.";
    barf (BADCOLORSPEC, details);
  }
  return 1;
}

int Settings::checkunit (char *culprit, char *val) {
  if (val)
    if (!strcmp (val, "ft") || !strcmp (val, "m") || !strcmp (val, "x"))
      return 0;
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "'.";
    barf (UNRECOGNIZED_UNITS, details);
  }
  return 1;
}

int Settings::checknumber (char *culprit, char *val) {
  float f;
  if (val)
    if (sscanf (val, "%f", &f) == 1)
      return 0;
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "'.";
    barf (NOT_A_NUMBER, details);
  }
  return 1;
}

int Settings::checktext (char *culprit, char *val) {
  if (val)
    if (*val != '\0')
      return 0;
  if (culprit) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was ";
    if (val)
      details += "an empty string.";
    else
      details += "NULL.";
    barf (BAD_TEXT, details);
  }
  return 1;
}

int Settings::checkstrftime (char *culprit, char *val) {
  // FIXME?
  return checktext (culprit, val);
}

int Settings::checkConfigurable (char *culprit, Configurable &val) {
  if (val.representation == Configurable::Rvoid || val.isnull)
    return 0;
  switch (val.interpretation) {
  case Configurable::Iboolean:
    assert (val.representation == Configurable::Rchar);
    return checkboolean (culprit, val.c);
  case Configurable::Iposint:
    assert (val.representation == Configurable::Runsigned);
    return checkposint (culprit, val.u);
  case Configurable::Iposdouble:
    assert (val.representation == Configurable::Rdouble);
    return checkposdouble (culprit, val.d);
  case Configurable::Igldouble:
    assert (val.representation == Configurable::Rdouble);
    return checkgldouble (culprit, val.d);
  case Configurable::Imode:
    assert (val.representation == Configurable::Rchar);
    return checkmode (culprit, val.c);
  case Configurable::Iformat:
    assert (val.representation == Configurable::Rchar);
    return checkformat (culprit, val.c);
  case Configurable::Icolor:
    assert (val.representation == Configurable::RDstr);
    return checkcolor (culprit, val.s);
  case Configurable::Iunit:
    assert (val.representation == Configurable::RDstr);
    return checkunit (culprit, val.s);
  case Configurable::Inumber:
    // If you made it here, it must be OK.
    return 0;
  case Configurable::Istrftime:
    assert (val.representation == Configurable::RDstr);
    return checkstrftime (culprit, val.s);
  case Configurable::Itext:
    if (val.representation == Configurable::RDstr)
      return checktext (culprit, val.s);
    else {
      assert (val.representation == Configurable::RDvector);
      DvectorIterator it = val.v.begin();
      while (it != val.v.end()) {
        if (checktext (culprit, *it))
          return 1;
        it++;
      }
      return 0;
    }
  default:
    assert (0);
  }
  return 0; // Silence compiler warning
}

int Settings::checkcolor (char *culprit, const Dstr &val) {
  return checkcolor (culprit, val.aschar());
}

int Settings::checkunit (char *culprit, const Dstr &val) {
  return checkunit (culprit, val.aschar());
}

int Settings::checktext (char *culprit, const Dstr &val) {
  return checktext (culprit, val.aschar());
}

int Settings::checkstrftime (char *culprit, const Dstr &val) {
  return checkstrftime (culprit, val.aschar());
}


// Default constructor initializes map to config.hh defaults.  This
// is highly desirable even if you intend to call nullify()
// immediately.  An empty map tells you nothing about what settings
// are even available.  A nulled-out map gives you all the metadata
// (just no data).
Settings::Settings () {
  // ConfiguredDefaults is a #define in Configurable.hh.
  Configurable cd[] = ConfiguredDefaults;
  for (unsigned i=0; !cd[i].switchname.isNull(); i++) {
    Dstr culprit ("the config.hh define for ");
    culprit += cd[i].switchname;
    culprit += '/';
    culprit += cd[i].resourcename;
    checkConfigurable (culprit.aschar(), cd[i]);
    (*this)[cd[i].switchname.aschar()] = cd[i];
  }
}

// Null out all settings.
void Settings::nullify() {
  ConfigurablesIterator it = this->begin();
  while (it != this->end()) {
    it->second.isnull = 1;
    it++;
  }
}

// Supersede by whatever.
void Settings::apply (const Settings &settings_in) {
  ConfigurablesConstIterator it = settings_in.begin();
  while (it != settings_in.end()) {
    const Configurable &cfbl = it->second;
    if (!cfbl.isnull)
      (*this)[it->first] = cfbl;
    it++;
  }
}

// Supersede by ~/.xtide.xml contents.
void Settings::applyUserDefaults () {
  // (homedir is later set in TideContext)
  xmlfilename = getenv ("HOME");
  if (xmlfilename.isNull())
    barf (NOHOMEDIR);
  xmlfilename += "/.xtide.xml";

  xmlparsetree = NULL;
  if ((xmlin = fopen (xmlfilename.aschar(), "r"))) {
    xmlparse();
    fclose (xmlin);
    struct xmltag *tag = xmlparsetree;
    while (tag) {
      if ((*(tag->name)) == "xtideoptions") {
        struct xmlattribute *a = tag->attributes;
        while (a) {
          // Ignore any settings that we don't already have
          ConfigurablesIterator it = this->find (a->name->aschar());
          if (it != this->end()) {
            Configurable &cfbl = it->second;
            if (cfbl.kind == Configurable::Ksetting) {
              Dstr culprit ("the ~/.xtide.xml attribute for ");
              culprit += cfbl.switchname;
              cfbl.isnull = 0;
              switch (cfbl.representation) {
	      case Configurable::Runsigned:
                cfbl.u = max (cfbl.min_value, getposint (culprit.aschar(), *(a->value)));
		break;
	      case Configurable::Rdouble:
		cfbl.d = getdouble (culprit.aschar(), *(a->value));
		break;
	      case Configurable::Rchar:
		cfbl.c = (*(a->value))[0];
		break;
	      case Configurable::RDstr:
                cfbl.s = *(a->value);
		break;
	      default:
		assert(0);
	      }
              checkConfigurable (culprit.aschar(), cfbl);
            }
          }
          a = a->next;
        }
      }
      tag = tag->next;
    }
  }
  freexml (xmlparsetree);
  xmlparsetree = NULL;
}

// Supersede by X resources.  We avoid linkage difficulties in the
// X-less case by taking a function as argument.
static int (*getResource) (Dstr &name_in, Dstr &val_out) = NULL;
void Settings::applyXResources () {
  assert (getResource);
  ConfigurablesIterator it = this->begin();
  while (it != this->end()) {
    Configurable &cfbl = it->second;
    if (cfbl.kind == Configurable::Ksetting) {
      Dstr val;
      if ((*getResource) (cfbl.resourcename, val)) {
        Dstr culprit ("the X resource for ");
        culprit += cfbl.resourcename;
        cfbl.isnull = 0;
	switch (cfbl.representation) {
	case Configurable::Runsigned:
	  cfbl.u = max (cfbl.min_value, getposint (culprit.aschar(), val));;
	  break;
	case Configurable::Rdouble:
	  cfbl.d = getdouble (culprit.aschar(), val);
	  break;
	case Configurable::Rchar:
	  cfbl.c = val[0];
	  break;
	case Configurable::RDstr:
	  cfbl.s = val;
	  break;
	default:
	  assert(0);
	}
	checkConfigurable (culprit.aschar(), cfbl);
      }
    }
    it++;
  }
}
void Settings::applyXResources (int (&getResource_in) (Dstr &name_in, Dstr &val_out)) {
  getResource = &getResource_in;
  applyXResources();
}

double Settings::getdouble (char *culprit, const Dstr &val) {
  return getdouble (culprit, val.aschar());
}

double Settings::getdouble (char *culprit, char *val) {
  double temp;
  if (!val) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was NULL (expecting a double).";
    barf (NOT_A_NUMBER, details);
  }
  if (sscanf (val, "%lf", &temp) != 1) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting a double).";
    barf (NOT_A_NUMBER, details);
  }
  return temp;
}

int Settings::getint (char *culprit, char *val) {
  int temp;
  if (!val) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was NULL (expecting an integer).";
    barf (NOT_A_NUMBER, details);
  }
  if (sscanf (val, "%d", &temp) != 1) {
    Dstr details ("The offending input in ");
    details += culprit;
    details += " was '";
    details += val;
    details += "' (expecting an integer).";
    barf (NOT_A_NUMBER, details);
  }
  return temp;
}

double Settings::getposdouble (char *culprit, char *val) {
  double temp = getdouble (culprit, val);
  checkposdouble (culprit, temp);
  return temp;
}

double Settings::getgldouble (char *culprit, char *val) {
  double temp = getdouble (culprit, val);
  checkgldouble (culprit, temp);
  return temp;
}

unsigned Settings::getposint (char *culprit, char *val) {
  int temp = getint (culprit, val);
  checkposint (culprit, temp);
  return temp;
}

double
Settings::getposdouble (char *culprit, const Dstr &val) {
  return getposdouble (culprit, val.aschar());
}

double
Settings::getgldouble (char *culprit, const Dstr &val) {
  return getgldouble (culprit, val.aschar());
}

unsigned
Settings::getposint (char *culprit, const Dstr &val) {
  return getposint (culprit, val.aschar());
}

void Settings::save() {
  xmlfilename = getenv ("HOME");
  if (xmlfilename.isNull())
    barf (NOHOMEDIR);
  xmlfilename += "/.xtide.xml";
  FILE *fp = fopen (xmlfilename.aschar(), "w");
  if (!fp) {
    Dstr details (xmlfilename);
    details += ": ";
    details += strerror (errno);
    details += ".";
    barf (CANT_OPEN_FILE, details, 0);
    return;
  }
  fprintf (fp, "<?xml version=\"1.0\"?>\n<xtideoptions ");

  ConfigurablesIterator it = this->begin();
  while (it != this->end()) {
    Configurable &cfbl = it->second;
    if (cfbl.kind == Configurable::Ksetting && !cfbl.isnull) {
      fprintf (fp, "%s=\"", cfbl.switchname.aschar());
      switch (cfbl.representation) {
      case Configurable::Runsigned:
        fprintf (fp, "%u", cfbl.u);
        break;
      case Configurable::Rdouble:
        fprintf (fp, "%f", cfbl.d);
        break;
      case Configurable::Rchar:
        fprintf (fp, "%c", cfbl.c);
        break;
      case Configurable::RDstr:
        fprintf (fp, "%s", cfbl.s.aschar());
        break;
      default:
        assert(0);
      }
      fprintf (fp, "\"\n");
    }
    it++;
  }
  fprintf (fp, "/>\n");
  fclose (fp);
}

#define duplicated_code_block {        \
      if (temprest) {                  \
	if (rest1) {                   \
          while (rest1) {              \
            Settings::arglist *t = rest1->next;  \
            delete rest1;              \
            rest1 = t;                 \
          }                            \
          while (temprest) {           \
            Settings::arglist *t = temprest->next;  \
            delete temprest;           \
            temprest = t;              \
          }                            \
	  return NULL;                 \
	} else {                       \
	  rest1 = temprest;            \
	  mycookedswitch = name;       \
	}                              \
      }                                \
}

// The goal is to disambiguate the argument string.
// Inputs:
//   int argc, char **argv   The usual
//   int argi                Index to the argument now being looked at
// Returns:
//   arglist*  Disambiguated argument list if valid.  Null if not.

Settings::arglist *Settings::disambiguate (int argc, char **argv, int argi) {

  char *argii = argv[argi];
  if (*argii != '-' && *argii != '+')
    return NULL;
  int isplus = (*argii == '+');
  argii++;

  Dstr mycookedswitch;
  Settings::arglist *rest1=NULL, *temprest;
  ConfigurablesIterator it = this->begin();
  while (it != this->end()) {
    Configurable &cfbl = it->second;
    if (!isplus || cfbl.interpretation == Configurable::Iboolean) {
      char *name = cfbl.switchname.aschar();
      if (!strncmp (argii, name, strlen(name))) {
	// Try 1:  concatenated argument
	temprest = check_arg (argc, argv, argi, argii + strlen(name),
			      cfbl.interpretation);
	duplicated_code_block;
	// Try 2:  separate argument
	if ((*(argii + strlen(name)) == '\0') && (argi + 1 < argc)) {
	  temprest = check_arg (argc, argv, argi+1, argv[argi+1],
				cfbl.interpretation);
	  duplicated_code_block;
	}
      }
    }
    it++;
  }

  if (rest1) {
    // check_arg is supposed to allocate one for us with a null
    // switch.
    assert (rest1->svitch.isNull());
    // if (isplus)
    //   mycookedswitch *= '+';
    // else
    //   mycookedswitch *= '-';
    rest1->svitch = mycookedswitch;
    if (isplus) {
      if (rest1->arg == "y")
	rest1->arg = "n";
      else if (rest1->arg == "n")
	rest1->arg = "y";
      else
	assert (0);
    }
  }
  return rest1;
}

// The goal is still to disambiguate the argument string.
// char *argii       Index to the current character position in argv[argi].
// Interpretation t  The "type" of argument we expect to find there.

Settings::arglist *Settings::check_arg (int argc, char **argv, int argi,
char *argii, Configurable::Interpretation t)
{
  Dstr mycookedarg;
  switch (t) {

  // Don't use checkboolean here; cut some slack.
  case Configurable::Iboolean:
    assert (argii);
    switch (*argii) {
    case '\0':
    case 'y':
    case 'Y':
      mycookedarg = "y";
      break;
    case 'n':
    case 'N':
      mycookedarg = "n";
      break;
    default:
      return NULL;
    }
    break;

  case Configurable::Iposint:
  case Configurable::Iposdouble:
  case Configurable::Igldouble:
  case Configurable::Inumber:
    // Better checking done later.
    if (checknumber (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  case Configurable::Imode:
    if (checkmode (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  case Configurable::Iformat:
    if (checkformat (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  case Configurable::Icolor:
    if (checkcolor (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  case Configurable::Iunit:
    if (checkunit (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  case Configurable::Itext:
    if (checktext (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  case Configurable::Istrftime:
    if (checkstrftime (NULL, argii))
      return NULL;
    mycookedarg = argii;
    break;

  default:
    assert (0);
  }

  Settings::arglist *rest = NULL;
  if (++argi < argc) {
    rest = disambiguate (argc, argv, argi);
    if (!rest)
      return NULL;
  }
  Settings::arglist *myarg = new Settings::arglist;
  // myarg->svitch intentionally left blank
  myarg->arg = mycookedarg;
  myarg->next = rest;
  return myarg; // Easy.
}

// Command line is disambiguated once and retained here.
static Settings::arglist *args = NULL;
void Settings::applyCommandLine () {
  Settings::arglist *arglooper;
  for (arglooper=args; arglooper; arglooper=arglooper->next) {
    ConfigurablesIterator it = this->find (arglooper->svitch.aschar());
    if (it == this->end()) {
      Dstr details ("The offending switch was '");
      details += arglooper->svitch;
      details += "'.";
      barf (BAD_OR_AMBIGUOUS_COMMAND_LINE, details);
    }
    Configurable &cfbl = it->second;
    Dstr culprit ("the command line argument for ");
    culprit += cfbl.switchname;
    cfbl.isnull = 0;
    switch (cfbl.representation) {
    case Configurable::Runsigned:
      cfbl.u = max (cfbl.min_value, getposint (culprit.aschar(), arglooper->arg));
      break;
    case Configurable::Rdouble:
      cfbl.d = getdouble (culprit.aschar(), arglooper->arg);
      break;
    case Configurable::Rchar:
      cfbl.c = arglooper->arg[0];
      break;
    case Configurable::RDstr:
      cfbl.s = arglooper->arg;
      break;
    case Configurable::RPredictionValue:
      {
	unsigned i;
	for (i=0; i<arglooper->arg.length(); i++) {
	  char c = arglooper->arg[i];
	  if (!((c >= '0' && c <= '9') || c == '+' || c == '-' || c == '.'
	  || c == 'e' || c == 'E'))
	    break;
	}
	Dstr uts = arglooper->arg.ascharfrom(i);
        double v = getdouble (culprit.aschar(), arglooper->arg);
	cfbl.p = PredictionValue (PredictionValue::Unit (uts), v);
      }
      break;
    case Configurable::RDvector:
      cfbl.v.push_back (arglooper->arg);
      break;
    case Configurable::Rvoid:
      break;
    default:
      assert(0);
    }
    checkConfigurable (culprit.aschar(), cfbl);
  }

  Configurable &loc = (*this)["l"];
  if (loc.isnull || loc.v.empty()) {
    loc.v.clear();
    char *defloc = getenv("XTIDE_DEFAULT_LOCATION");
    if (defloc) {
      Dstr temp (defloc);
      loc.isnull = 0;
      loc.v.push_back (defloc);
      checkConfigurable ("XTIDE_DEFAULT_LOCATION", loc);
    }
  }
}
void Settings::applyCommandLine (int argc, char **argv) {
  if (!args)
    if (argc > 1) {
      args = disambiguate (argc, argv, 1);
      if (!args)
        barf (BAD_OR_AMBIGUOUS_COMMAND_LINE);
    }

  char *v = findarg ("v", args);
  if (v)
    if (!strcmp (v, "y")) {
      Dstr line;
      version_string (line);
      fprintf (stderr, "%s\n", line.aschar());
      exit (0);
    }

  applyCommandLine();
}

char *Settings::findarg (char *arg, Settings::arglist *args) {
  while (args) {
    if (args->svitch == arg)
      return args->arg.aschar();
    args = args->next;
  }
  return NULL;
}
