// $Id: Offsets.hh,v 1.5 2004/11/16 22:27:29 flaterco Exp $
// Offsets:  storage for tide offsets.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// An extreme example of inheritance overkill.

class TimeOffset {
public:
  Interval timeAdd;
};

class LevelOffset {
protected:
  double mylevelMultiply;
public:
  LevelOffset();
  PredictionValue levelAdd;
  double levelMultiply() const;
  void levelMultiply (double in_mult); // > 0.0
};

class CombinedOffset: public TimeOffset, public LevelOffset {
};

// The only purpose of this superclass is to permit SimpleOffsets and
// HairyOffsets to be interchangeable.
class Offsets {
public:
  virtual ~Offsets();
  virtual int is_simple() = 0;
};

class SimpleOffsets: public CombinedOffset, public Offsets {
public:
  SimpleOffsets ();
  SimpleOffsets (const CombinedOffset &co_in);
  int is_simple();
};

// floodbegins and ebbbegins are not used unless it's a current location.
class HairyOffsets: public Offsets {
public:
  int is_simple();
  HairyOffsets();
  HairyOffsets (const SimpleOffsets &s_in);
  CombinedOffset max;
  CombinedOffset min;
  NullableInterval floodbegins;
  NullableInterval ebbbegins;
};

int operator== (CombinedOffset a, CombinedOffset b);
