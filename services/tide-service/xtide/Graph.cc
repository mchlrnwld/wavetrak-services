// $Id: Graph.cc,v 1.14 2004/11/15 16:30:22 flaterco Exp $
/*  Graph  Abstract superclass for all graphs.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Graph::Graph (unsigned xsize, unsigned ysize) {
  myxsize = xsize;
  myysize = ysize;
  clockmode = 0;
}

unsigned Graph::xsize() {
  return myxsize;
}

unsigned Graph::ysize() {
  return myysize;
}

void Graph::findnextsunevent (TideEventsIterator &it,
TideEventsOrganizer &organizer, Timestamp now, Timestamp endt,
Timestamp &nextsunevent) {
  while (it != organizer.end()) {
    TideEvent &te = it->second;
    if (te.tm > now &&
    (te.etype == TideEvent::sunrise || te.etype == TideEvent::sunset || te.etype == TideEvent::eveningTwilight || te.etype == TideEvent::morningTwilight)) {
      nextsunevent = te.tm;
      return;
    }
    it++;
  }
  nextsunevent = endt + Interval(DAYSECONDS);
}

double Graph::linterp (double lo, double hi, double saturation) {
  return lo + saturation * (hi - lo);
}

unsigned char Graph::linterp (unsigned char lo, unsigned char hi,
		       double saturation) {
  return (unsigned char) linterp ((double)lo, (double)hi, saturation);
}

unsigned short Graph::linterp (unsigned short lo, unsigned short hi,
		       double saturation) {
  return (unsigned short) linterp ((double)lo, (double)hi, saturation);
}

  // How to translate a tide depth to a y-coordinate
#define xlate(y) linterp (ymax, ymin, (((double)y) - valmin) / (valmax - valmin))

void Graph::drawDepth (double ymax, double ymin, double valmax, double valmin,
unsigned linestep, int &mindepth, int &maxdepth, unsigned labelwidth) {
  int depth;
  double ytide;
  for (depth = 0; depth <= valmax; depth += linestep) {
    ytide = xlate(depth);
    // Leave room for 3 lines of text at top, 3 lines of text plus
    // tick marks at bottom.
    if (ytide - fontHeight()/2 - depthLineVerticalMargin() <= (fontHeight()+fontMargin()) * 3)
      break;
    if (ytide + fontHeight()/2 + depthLineVerticalMargin() >= myysize - (fontHeight()+fontMargin()) * 3 - hourTickLen())
      continue;
    maxdepth = depth;
    if (depth < mindepth) // In case one loop is never executed.
      mindepth = depth;
    drawHorizontalLine (labelwidth, myxsize-1, ytide, Colors::foreground);
  }
  for (depth = -linestep; depth >= valmin; depth -= linestep) {
    ytide = xlate(depth);
    // Leave room for 3 lines of text at top, 3 lines of text plus
    // tick marks at bottom.
    if (ytide - fontHeight()/2 - depthLineVerticalMargin() <= (fontHeight()+fontMargin()) * 3)
      continue;
    if (ytide + fontHeight()/2 + depthLineVerticalMargin() >= myysize - (fontHeight()+fontMargin()) * 3 - hourTickLen())
      break;
    mindepth = depth;
    if (depth > maxdepth) // In case one loop is never executed.
      maxdepth = depth;
    drawHorizontalLine (labelwidth, myxsize-1, ytide, Colors::foreground);
  }
}

void Graph::drawFunkyLine (double prevytide, double ytide, double nextytide,
Settings *settings, int x, Colors::colorchoice c) {
  double dy, yleft, yright;
  double slw = (*settings)["lw"].d;

  // The fix for line slope breaks down when the slope gets nasty,
  // so switch to a more conservative strategy when that happens.
  // Line width becomes 1 no matter what.

#define dohalfline(yy) {                                          \
  double lw;                                                      \
  if (fabs(dy) < slopelimit)                                      \
    lw = (1.0 + (M_SQRT2 - 1.0) * fabs(dy)) * slw / 2.0; \
  else                                                            \
    lw = (fabs(dy) + slw) / 2.0;                         \
  if (dy < 0.0)                                                   \
    lw = -lw;                                                     \
  yy = ytide - lw;                                                \
}

  dy = ytide - prevytide;
  dohalfline (yleft);
  dy = ytide - nextytide;
  dohalfline (yright);

  // Fix degenerate cases.
  if (ytide > yleft && ytide > yright) {
    if (yleft > yright)
      yleft = ytide + slw / 2.0;
    else
      yright = ytide + slw / 2.0;
  } else if (ytide < yleft && ytide < yright) {
    if (yleft < yright)
      yleft = ytide - slw / 2.0;
    else
      yright = ytide - slw / 2.0;
  }
  drawVerticalLine (x, yleft, yright, c);
}

void Graph::clearGraph (Timestamp start, Timestamp endt, Interval increment,
Station *sr, TideEventsOrganizer &organizer) {
  assert (sr);
  char ns = (*(sr->context->settings))["ns"].c;
  // Clear the graph by laying down a background of days and nights.
  int sunisup = 1;
  if (!(sr->coordinates.isNull()) && ns == 'n')
    sunisup = sun_is_up (start, sr->coordinates);
  unsigned x;
  Timestamp loopt (start);
  Timestamp nextsunevent;
  TideEventsIterator it = organizer.begin();
  findnextsunevent (it, organizer, loopt, endt, nextsunevent);
  for (x=0; x<myxsize; x++, loopt += increment) {
    if (loopt >= nextsunevent) {
      findnextsunevent (it, organizer, loopt, endt, nextsunevent);
      assert (loopt < nextsunevent);
      if (it != organizer.end()) {
        switch (it->second.etype) {
        case TideEvent::sunrise:
          sunisup = 0;
          break;
        case TideEvent::sunset:
          sunisup = 1;
          break;
    	case TideEvent::morningTwilight:
	  sunisup = 2;
	  break;
	case TideEvent::eveningTwilight:
	  sunisup = 3;
	break;
        default:
          assert (0);
	}
      } else {
        switch(sunisup) {
    	  case 0:
	    sunisup = 1;
	  break;
	  case 1:
	    sunisup = 3;
	  break;
	  case 2:
   	    sunisup = 0;
	  break;
	  case 3:
	    sunisup = 2;
	  break;
	}
      }
    }
    
    Colors::colorchoice c;
    switch (sunisup) {
      case 1:
	c = Colors::daytime;
      break;
      case 2:
	c = Colors::nighttime;
      break;
      case 0:
      case 3:
	c = Colors::twilight;
      break;
    }
    drawVerticalLine (x, 0, myysize-1, c);
  }
}

void Graph::drawX (int x, double y) {
  drawVerticalLine (x, y-4, y+4, Colors::foreground);
  drawHorizontalLine (x-4, x+4, y, Colors::foreground);
}

void Graph::drawTides (Station *sr, Timestamp now, Angle *angle) {
  assert (sr);
  Settings *settings = sr->context->settings;
  assert (sr->aspect > 0.0);
  char tl = (*settings)["tl"].c;
  char nf = (*settings)["nf"].c;
  char el = (*settings)["el"].c;
  char nm = (*settings)["nm"].c;
  char st = (*settings)["st"].c;
  char tt = (*settings)["tt"].c;
  char ta = (*settings)["ta"].c;
  char nx = (*settings)["nx"].c;
  char nl = (*settings)["nl"].c;

  Interval increment = (max (1, (long)(aspmagnum / (double)myysize /
    (aspectfudge() * sr->aspect) + 0.5)));
  Timestamp start;
  if(nl == 'n'){
    start = now - increment * startpos();
  } else {
    start = now - increment;
  }
  Timestamp endt = start + increment * xsize(); // end is a keyword

  // now is the "now" of the graph (the time to show).
  // rightnow is the real "now" (the actual wallclock time).
  // On tide clocks they ought to be identical.
  Timestamp rightnow ((time_t)(time(NULL)));

  // First get a list of the relevant tide events.  Need some extra on
  // either side since text pertaining to events occurring beyond the
  // margins can still be visible.  We also need to make sure
  // *something* shows up so that extendRange can work below.

  TideEventsOrganizer organizer;
  Interval delta;
  for (delta = Interval(DAYSECONDS); organizer.empty(); delta *= 2U)
    sr->predictTideEvents (start - delta, endt + delta, organizer);

  // Need at least one following max and min for clock mode.
  TideEvent nextmax, nextmin;
  if (clockmode) {
    int donemax = 0, donemin=0;
    delta = Interval(DAYSECONDS);
    while (!(donemax && donemin)) {
      TideEventsIterator it = organizer.upper_bound(rightnow.timet());
      while (it != organizer.end()) {
        TideEvent &te = it->second;
        if (!donemax && te.etype == TideEvent::max) {
          donemax = 1;
          nextmax = te;
        } else if (!donemin && te.etype == TideEvent::min) {
          donemin = 1;
          nextmin = te;
        }
        it++;
      }
      if (!(donemax && donemin)) {
        sr->extendRange (organizer, forward, delta);
        delta *= 2U;
      }
    }

    // Need a max/min pair bracketing current time for tide clock icon
    // angle kludge.
    if (angle) {
      TideEvent nextmaxormin;
      if (nextmax.tm < nextmin.tm)
	nextmaxormin = nextmax;
      else
	nextmaxormin = nextmin;
      TideEvent prevmaxormin;
      {
	int done = 0;
        delta = Interval(DAYSECONDS);
	while (!done) {
	  TideEventsIterator it = organizer.upper_bound(rightnow.timet());
	  assert (it != organizer.end());
	  while (it != organizer.begin())
	    if ((--it)->second.isMaxMinEvent()) {
	      done = 1;
	      prevmaxormin = it->second;
	      break;
	    }
	  if (!done) {
	    sr->extendRange (organizer, backward, delta);
            delta *= 2U;
          }
	}
      }
      assert (prevmaxormin.etype != nextmaxormin.etype);
      assert (prevmaxormin.tm <= rightnow && nextmaxormin.tm > rightnow);
      assert (prevmaxormin.isMaxMinEvent());
      assert (nextmaxormin.isMaxMinEvent());
      double temp = (rightnow - prevmaxormin.tm) /
                    (nextmaxormin.tm - prevmaxormin.tm);
      temp *= 180.0;
      if (prevmaxormin.etype == TideEvent::min)
        temp += 180.0;
      (*angle) = Angle (Angle::DEGREES, temp);
    }
  }

  clearGraph (start, endt, increment, sr, organizer);

  int x;
  double ytide;
  Timestamp loopt;

  // Figure constants.
  double ymin = margin * (double)myysize;
  double ymax = (double)myysize - ymin;
  double valmin = sr->minLevel().val();
  double valmax = sr->maxLevel().val();
  if (valmin >= valmax)
    barf (ABSURD_OFFSETS, sr->name);
  double midwl = xlate(0);

  // Figure increment for depth lines.  Unlike XTide 1, this will decrease
  // the increment if more vertical space becomes available.
  unsigned linestep = 1, prevstep = 1, prevmult = 10;
  while (xlate(0.0) - xlate((double)linestep) < fontHeight() + fontMargin()) {
    switch (prevmult) {
    case 10:
      prevmult = 2;
      linestep = prevstep * prevmult;
      break;
    case 2:
      prevmult = 5;
      linestep = prevstep * prevmult;
      break;
    case 5:
      prevmult = 10;
      prevstep = linestep = prevstep * prevmult;
      break;
    default:
      assert (0);
    }
  }

  // More figuring.
  Dstr u;
  sr->minLevel().Units().shortname(u);
  int mindepth=10000000, maxdepth=-10000000;
  unsigned labellen = 2 + u.length() + (unsigned) (floor (max (max (log10
    (fabs (valmin)), log10 (fabs (valmax))), 0)));
  unsigned labelwidth = fontWidth() * labellen + depthLabelLeftMargin() +
    depthLabelRightMargin();

  // Draw depth lines now?
  if (tl == 'n')
    drawDepth (ymax, ymin, valmax, valmin, linestep, mindepth, maxdepth,
    labelwidth);

  // Draw the actual tides.
  double prevval, prevytide;
  double val = sr->predictTideLevel(start-increment).val();
  ytide = xlate(val);
  double nextval = sr->predictTideLevel(start).val();
  double nextytide = xlate (nextval);
  // loopt is actually 1 step ahead of x
  for (x=0, loopt=start+increment; x<(int)myxsize; x++, loopt += increment) {
    prevval = val;
    prevytide = ytide;
    val = nextval;
    ytide = nextytide;
    nextval = sr->predictTideLevel(loopt).val();
    nextytide = xlate(nextval);

    if (sr->isCurrent) {
      Colors::colorchoice c = (val > 0.0 ?
        Colors::flood : Colors::ebb);
      if (nf == 'n')
        drawVerticalLine (x, midwl, ytide, c);
      else
        drawFunkyLine (prevytide, ytide, nextytide, settings, x, c);
    } else {
      Colors::colorchoice c = (prevval < val ?
        Colors::flood : Colors::ebb);
      if (nf == 'n')
        drawVerticalLine (x, myysize, ytide, c);
      else
        drawFunkyLine (prevytide, ytide, nextytide, settings, x, c);
    }
  }

  // Draw depth lines later?
  if (tl != 'n')
    drawDepth (ymax, ymin, valmax, valmin, linestep, mindepth, maxdepth,
    labelwidth);

  // Height axis.
  int depth;
  for (depth = mindepth; depth <= maxdepth; depth += linestep) {
    Dstr dlabel;
    dlabel += depth;
    dlabel += " ";
    dlabel += u;
    while (dlabel.length() < labellen)
      dlabel *= " ";
    double adj = 0.0;
    if (fontHeight() > 1)
      adj = (double)(fontHeight()) / 2.0;
    drawString (depthLabelLeftMargin(), xlate(depth)-adj, dlabel);
  }

  // Figure increment for time axis.
  unsigned timestep = 1;
  if (HOURSECONDS * timestep / increment < fontWidth() * 2)
    timestep = 2;
  if (HOURSECONDS * timestep / increment < fontWidth() * 2)
    timestep = 3;
  if (HOURSECONDS * timestep / increment < fontWidth() * 2)
    timestep = 4;
  if (HOURSECONDS * timestep / increment < fontWidth() * 2)
    timestep = 6;
  if (HOURSECONDS * timestep / increment < fontWidth() * 2)
    timestep = 12;
  if (HOURSECONDS * timestep / increment < fontWidth() * 2)
    timestep = 24;

  // Time axis.
  loopt = start;
  if(ta == 'y' ){
  for (loopt.prev_hour(sr->timeZone,settings); loopt < endt + Interval(HOURSECONDS);
  loopt.inc_hour(sr->timeZone,settings)) {
    if (loopt.gethour(sr->timeZone,settings) % timestep == 0) {
      x = (int)((loopt - start) / increment + 0.5);
      drawHourTick (x, Colors::foreground);
      Dstr ts;
      loopt.printhour (ts, sr->timeZone, settings);
      if(tt == 'y'){
        labelHourTick (x, ts);
      }
    }
  }
  }
  /* Make tick marks for day boundaries thicker */
  /* This is not guaranteed to coincide with an hour transition! */
  if(ta == 'y' ){
  loopt = start;
  for (loopt.prev_day(sr->timeZone,settings); loopt < endt + Interval(HOURSECONDS);
  loopt.inc_day(sr->timeZone,settings)) {
    x = (int)((loopt - start) / increment + 0.5);
    drawHourTick (x-1, Colors::foreground);
    drawHourTick (x, Colors::foreground);
    drawHourTick (x+1, Colors::foreground);
  }
  }
  if (clockmode) {

    // Write current time
    Dstr ts;
    rightnow.printtime (ts, sr->timeZone, settings);
    centerStringOnLine (myxsize/2, 0, ts);

    // Write next max
    Dstr desc (nextmax.longdesc());
    centerStringOnLine (myxsize/2, 1, desc);
    nextmax.tm.printtime (ts, sr->timeZone, settings);
    centerStringOnLine (myxsize/2, 2, ts);

    // Write next min
    desc = nextmin.longdesc();
    centerStringOnLine (myxsize/2, -3, desc);
    nextmin.tm.printtime (ts, sr->timeZone, settings);
    centerStringOnLine (myxsize/2, -2, ts);

  } else {

    if(st == 'y') {
      drawTitleLine (sr->name);
    }

    // Put timestamps for timestampable events.
    TideEventsIterator it = organizer.begin();
    while (it != organizer.end()) {
      TideEvent &te = it->second;
      Dstr ts;
      x = (int)((te.tm - start) / increment + 0.5);
      switch (te.etype) {
      case TideEvent::max:
      case TideEvent::min:
	te.tm.printdate (ts, sr->timeZone, settings);
	drawTimestampLabel (x, 1, ts);
	te.tm.printtime (ts, sr->timeZone, settings);
	drawTimestampLabel (x, 2, ts);
	break;

      case TideEvent::moonrise:
      case TideEvent::moonset:
      case TideEvent::newmoon:
      case TideEvent::firstquarter:
      case TideEvent::fullmoon:
      case TideEvent::lastquarter:
	if (nm == 'y') {
	  break;
	}

      case TideEvent::slackrise:
      case TideEvent::slackfall:
      case TideEvent::markrise:
      case TideEvent::markfall:

        drawHourTick (x, Colors::mark);
	te.tm.printtime (ts, sr->timeZone, settings);
	drawTimestampLabel (x, -2, ts);

        // Pre-2.6:  only draw mark/slack crossings at the bottom;
        // include date.
	// te.tm.printdate (ts, sr->timeZone, settings);
	// drawTimestampLabel (x, -3, ts);

        // 2.6:  replace the date with the description and draw more
        // sun/moon events.  Try to be smart about when to use long
        // descriptions or short.
        {
          Dstr desc;
          if (!is_banner() && (sr->isCurrent || sr->markLevel))
            desc = te.shortdesc();
          else
	    desc = te.longdesc();
	  drawTimestampLabel (x, -3, desc);
        }
	break;
      default:
	;
      }
      it++;
    }
  }

  // Extra lines.
  if (sr->markLevel) {
    ytide = xlate(sr->markLevel->val());
    drawHorizontalLine (labelwidth, myxsize-1, ytide, Colors::mark);
  }
  if (el != 'n') {
    drawHorizontalLine (labelwidth, myxsize-1, midwl, Colors::datum);
    ytide = (ymax + ymin) / 2.0;
    drawHorizontalLine (labelwidth, myxsize-1, ytide, Colors::msl);
  }

  // X marks the current time.
  if(nx == 'n') {
  if (rightnow >= start && rightnow < endt) {
    x = (int)((rightnow - start) / increment + 0.5);
    ytide = xlate(sr->predictTideLevel(rightnow).val());
    drawX (x, ytide);
  }
  }
}

void Graph::drawHorizontalLine (int xlo, int xhi, double y,
				Colors::colorchoice c) {
  drawHorizontalLine (xlo, xhi, (int)(y+0.5), c);
}

void Graph::drawString (int x, double y, const Dstr &s) {
  drawString (x, (int)(y+0.5), s);
}

void Graph::centerString (int x, int y, const Dstr &s) {
  drawString (x-(int)s.length()*fontWidth()/2, y, s);
}

void Graph::centerString (int x, double y, const Dstr &s) {
  drawString (x-(int)s.length()*fontWidth()/2, y, s);
}

void Graph::centerStringOnLine (int x, int line, const Dstr &s) {
  int y;
  if (line >= 0)
    y = line * (fontHeight()+fontMargin());
  else
    y = myysize+(fontHeight()+fontMargin())*line-hourTickLen();
  centerString (x, y, s);
}

unsigned Graph::fontMargin() {
  // This used to be 1 until fontHeight was increased to accommodate
  // Latin1 characters.
  return 0;
}

unsigned Graph::depthLabelLeftMargin() {
  return 2;
}

unsigned Graph::depthLabelRightMargin() {
  return 2;
}

unsigned Graph::depthLineVerticalMargin() {
  return 2;
}

unsigned Graph::hourTickLen() {
  return hourticklen;
}

double Graph::aspectfudge() {
  return 1.0;
}

void Graph::setPixel (int x, int y, Colors::colorchoice c,
		       double saturation) {
  if (saturation >= 0.5)
    setPixel (x, y, c);
}

void
Graph::drawHourTick (int x, Colors::colorchoice c) {
  drawVerticalLine (x, (int)myysize-1, (int)myysize-hourTickLen(), c);
}

void
Graph::drawVerticalLine (int x, int y1, int y2, Colors::colorchoice c) {
  int ylo, yhi;
  if (y1 < y2) {
    ylo = y1; yhi = y2;
  } else {
    ylo = y2; yhi = y1;
  }
  int i;
  for (i=ylo; i<=yhi; i++)
    setPixel (x, i, c);
}

void Graph::drawVerticalLine (int x, double y1, double y2,
                                 Colors::colorchoice c) {
  double ylo, yhi;
  if (y1 < y2) {
    ylo = y1; yhi = y2;
  } else {
    ylo = y2; yhi = y1;
  }
  int ylo2, yhi2;
  ylo2 = (int) ceil (ylo);
  yhi2 = (int) floor (yhi);
  if (ylo2 < yhi2)
    drawVerticalLine (x, ylo2, yhi2-1, c);
  // What if they both fall within the same pixel:  ylo2 > yhi2
  if (ylo2 > yhi2) {
    assert (yhi2 == ylo2 - 1);
    double saturation = yhi - ylo;
    setPixel (x, yhi2, c, saturation);
  } else {
    // The normal case.
    if (ylo < (double)ylo2) {
      double saturation = (double)ylo2 - ylo;
      setPixel (x, ylo2-1, c, saturation);
    }
    if (yhi > (double)yhi2) {
      double saturation = yhi - (double)yhi2;
      setPixel (x, yhi2, c, saturation);
    }
  }
}

void
Graph::drawHorizontalLine (int xlo, int xhi, int y,
			      Colors::colorchoice c) {
  int i;
  for (i=xlo; i<=xhi; i++)
    setPixel (i, y, c);
}

unsigned
Graph::startpos() {
  return nowposition;
}

void
Graph::labelHourTick (int x, Dstr &ts) {
  centerStringOnLine (x, -1, ts);
}

void Graph::drawTimestampLabel (int x, int line, Dstr &ts) {
  centerStringOnLine (x, line, ts);
}

void Graph::drawTitleLine (Dstr &title) {
  centerStringOnLine (myxsize/2, 0, title);
}

int Graph::is_banner () {
  return 0;
}
