/*  ConstantSet
    Last modified 1998-01-04

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

ConstantSet::ConstantSet () {
  length = 0;
  amplitudes = NULL;
  phases = NULL;
}

ConstantSet::ConstantSet (unsigned in_len) {
  assert (in_len > 0);
  length = in_len;
  amplitudes = new Amplitude [length];
  phases = new Angle [length];
}

ConstantSet &ConstantSet::operator= (const ConstantSet &in_val) {
  if (length != in_val.length) {
    delete [] amplitudes;
    delete [] phases;
    length = in_val.length;
    amplitudes = new Amplitude [length];
    phases = new Angle [length];
  }
  for (unsigned a=0; a<length; a++) {
    amplitudes[a] = in_val.amplitudes[a];
    phases[a] = in_val.phases[a];
  }
  datum = in_val.datum;
  return (*this);
}

ConstantSet::~ConstantSet () {
  if (length) {
    delete [] amplitudes;
    delete [] phases;
  }
}

void ConstantSet::setUnits (PredictionValue::Unit in_units) {
  datum.Units (in_units);
  for (unsigned a=0; a<length; a++)
    amplitudes[a].Units (in_units);
}

void ConstantSet::adjust (SimpleOffsets in_offsets,
const ConstituentSet &constituents) {
  unsigned a;

  // Do levelMultiply
  datum *= in_offsets.levelMultiply();
  for (a=0; a<length; a++)
    amplitudes[a] *= in_offsets.levelMultiply();

  // Do levelAdd
  if (in_offsets.levelAdd.Units() != datum.Units())
    in_offsets.levelAdd.Units (datum.Units());
  datum += in_offsets.levelAdd;

  // Do timeAdd
  // Need to subtract offset from constants to effect the change
  // (to move tides one hour later, you need to turn BACK the phases).
  assert (length == constituents.length);
  for (a=0; a<length; a++)
    phases[a] -= in_offsets.timeAdd * constituents[a].speed();
}
