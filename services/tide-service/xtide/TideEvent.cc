// $Id: TideEvent.cc,v 1.7 2004/04/08 18:52:37 flaterco Exp $

// TideEvent  Generic representation for tide events.  Refactored from
// similar notions previously appearing in TideContext, Calendar,
// Station, xxTextMode, Graph, ...

/*
    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "common.hh"

int TideEvent::isSunMoonEvent () const {
  switch (etype) {
  case TideEvent::sunrise:
  case TideEvent::sunset:
  case TideEvent::moonrise:
  case TideEvent::moonset:
  case TideEvent::newmoon:
  case TideEvent::firstquarter:
  case TideEvent::fullmoon:
  case TideEvent::lastquarter:
  case TideEvent::eveningTwilight:
  case TideEvent::morningTwilight:
    return 1;
  default:
    return 0;
  }
}

int TideEvent::isMaxMinEvent () const {
  switch (etype) {
  case TideEvent::max:
  case TideEvent::min:
    return 1;
  default:
    return 0;
  }
}

// This returns true if the description of the event would be Min Flood or
// Min Ebb.
int TideEvent::isMinCurrentEvent () const {
  switch (etype) {
  case TideEvent::max:
    return (isCurrent ? (pv.val() >= 0.0 ? 0 : 1) : 0);
  case TideEvent::min:
    return (isCurrent ? (pv.val() <= 0.0 ? 0 : 1) : 0);
  default:
    return 0;
  }
}

const char *TideEvent::longdesc () const {
  switch (etype) {
  case TideEvent::max:
    return (isCurrent ? (pv.val() >= 0.0 ? "Max Flood"
			       : "Min Ebb") : "High Tide");
  case TideEvent::min:
    return (isCurrent ? (pv.val() <= 0.0 ? "Max Ebb"
                               : "Min Flood") : "Low Tide");
  case TideEvent::slackrise:
    return "Slack, Flood Begins";
  case TideEvent::slackfall:
    return "Slack, Ebb Begins";
  case TideEvent::markrise:
    return (isCurrent ? (pv.val() <= 0.0 ? "Mark, Ebb Decreasing"
			       : "Mark, Flood Increasing")
		  : "Mark Rising");
  case TideEvent::markfall:
    return (isCurrent ? (pv.val() >= 0.0 ? "Mark, Flood Decreasing"
			       : "Mark, Ebb Increasing")
		  : "Mark Falling");
  case TideEvent::sunrise:
    return "Sunrise";
  case TideEvent::sunset:
    return "Sunset";
  case TideEvent::moonrise:
    return "Moonrise";
  case TideEvent::moonset:
    return "Moonset";
  case TideEvent::newmoon:
    return "New Moon";
  case TideEvent::firstquarter:
    return "First Quarter";
  case TideEvent::fullmoon:
    return "Full Moon";
  case TideEvent::lastquarter:
    return "Last Quarter";
  case TideEvent::eveningTwilight:
    return "Evening Twilight";
  case TideEvent::morningTwilight:
    return "Morning Twilight";
  case TideEvent::rawreading:
  default:
    assert (0);
  }
  return NULL; // Silence bogus "control reaches end of non-void function"
}

const char *TideEvent::shortdesc () const {
  switch (etype) {
  case TideEvent::max:
    return (isCurrent ? (pv.val() >= 0.0 ? "MaxFl" : "MinEb") : "High");
  case TideEvent::min:
    return (isCurrent ? (pv.val() <= 0.0 ? "MaxEb" : "MinFl") : "Low");
  case TideEvent::slackrise:
  case TideEvent::slackfall:
    return "Slack";
  case TideEvent::markrise:
  case TideEvent::markfall:
    return "Mark";
  case TideEvent::sunrise:
    return "Srise";
  case TideEvent::sunset:
    return "Sset";
  case TideEvent::moonrise:
    return "Mrise";
  case TideEvent::moonset:
    return "Mset";
  case TideEvent::newmoon:
    return "NewM";
  case TideEvent::firstquarter:
    return "1stQ";
  case TideEvent::fullmoon:
    return "FullM";
  case TideEvent::lastquarter:
    return "LastQ";
  case TideEvent::eveningTwilight:
    return "EvTwi";
  case TideEvent::morningTwilight:
    return "MgTwi";
  case TideEvent::rawreading:
  default:
    assert (0);
  }
  return NULL; // Silence bogus "control reaches end of non-void function"
}

// Generate one line of text output, applying global formatting
// rules and so on.
// Legal forms are c (CSV), t (text) or i (iCalendar).
// Legal modes are p (plain), r (raw), or m (medium rare).
// Line is not newline terminated.
void TideEvent::print (Dstr &line_out, char form, char mode,
const Station &station) const {
  Dstr etype_desc_long, time_print, pv_print;
  Settings *settings = station.context->settings;

  switch (mode) {
  case 'r':
    switch (form) {
    case 'c':
      line_out = station.name;
      line_out.repchar (',', csv_repchar);
      line_out += ',';
      line_out += tm.timet();
      line_out += ',';
      line_out += pv.val();
      return;
    case 't':
      line_out = tm.timet();
      line_out += ' ';
      line_out += pv.val();
      return;
    default:
      assert (0);
    }

  case 'm':
    switch (form) {
    case 'c':
      line_out = station.name;
      line_out.repchar (',', csv_repchar);
      line_out += ',';
      tm.printdate (time_print, station.timeZone, settings);
      line_out += time_print;
      line_out += ',';
      tm.printtime (time_print, station.timeZone, settings);
      line_out += time_print;
      line_out += ',';
      line_out += pv.val();
      return;
    case 't':
      tm.print (line_out, station.timeZone, settings);
      line_out += ' ';
      line_out += pv.val();
      return;
    default:
      assert (0);
    }

  case 'p':
    switch (form) {
    case 't':
      if (!isSunMoonEvent())
	pv.print (pv_print);
      tm.print (time_print, station.timeZone, settings);
      line_out = time_print;
      line_out += " ";
      line_out += pv_print;
      line_out += "  ";
      line_out += longdesc ();
      break;

    case 'c':
      if (!isSunMoonEvent())
	pv.printnp (pv_print);
      line_out = station.name;
      line_out.repchar (',', csv_repchar);
      line_out += ',';
      tm.printdate (time_print, station.timeZone, settings);
      line_out += time_print;
      line_out += ',';
      tm.printtime (time_print, station.timeZone, settings);
      line_out += time_print;
      line_out += ',';
      line_out += pv_print;
      line_out += ',';
      etype_desc_long = longdesc ();
      etype_desc_long.repchar (',', csv_repchar);
      line_out += etype_desc_long;
      break;

    case 'i':
      {
	if (!isSunMoonEvent())
	  pv.print (pv_print);

	static unsigned long snum = 1;
	char temp[80];

	// Fields required by RFC 2446 for PUBLISH method on VEVENT:
	//   DTSTAMP
	//   DTSTART
	//   ORGANIZER
	//   SUMMARY (can be null)
	//   UID
	// Optional fields of interest:
	//   COMMENT
	//   DESCRIPTION
	//   GEO
	//   LOCATION
	//   TRANSP

	Timestamp now_ts ((time_t)(time(NULL)));
	Dstr now_ds;
	now_ts.print_iCalendar (now_ds, 0);

	// RFC 2445 p. 52:
	//
	// For cases where a "VEVENT" calendar component specifies a
	// "DTSTART" property with a DATE-TIME data type but no "DTEND"
	// property, the event ends on the same calendar date and time of
	// day specified by the "DTSTART" property.

	// TRANSP should be redundant for an event that takes up no time,
	// but being explicit may help less enlightened clients to do the
	// right thing.

	line_out = "BEGIN:VEVENT\r\nDTSTAMP:";
	line_out += now_ds;
	line_out += "\r\nDTSTART:";
	tm.print_iCalendar (time_print, 1);
	line_out += time_print;
	line_out += "\r\nTRANSP:TRANSPARENT\r\nORGANIZER;CN=XTide:MAILTO:nobody@localhost\r\nSUMMARY:";
	line_out += longdesc ();
	if (!pv_print.isNull()) {
	  line_out += ' ';
	  line_out += pv_print;
	}
	line_out += "\r\nUID:XTide-";
	if (!station.coordinates.isNull()) {
	  sprintf (temp, "%ld-%ld-", (long)(station.coordinates.lat()*10000),
		   (long)(station.coordinates.lng()*10000));
	  line_out += temp;
	}
	tm.print_iCalendar (time_print, 0);
	line_out += time_print;

	// Latitude + longitude + event time should be nearly unique, but
	// "you can never have too much overkill."

	line_out += "-\r\n ";
	line_out += now_ds;
	line_out += "-";
	line_out += getpid();
	line_out += "-";
	line_out += snum++;
	line_out += "-";
	line_out += rand();
	line_out += "@localhost\r\n";
	if (!station.coordinates.isNull()) {
	  char temp[80];
	  sprintf (temp, "GEO:%6.4f;%6.4f\r\n", station.coordinates.lat(),
		   station.coordinates.lng());
	  line_out += temp;
	}
	line_out += "LOCATION:";
	{
	  Dstr mangle (station.name);
	  mangle.rfc2445_mangle();
	  line_out += mangle;
	}
	line_out += "\r\nEND:VEVENT\r";
      }
      break;

    default:
      assert (0);
    }
  }
}
