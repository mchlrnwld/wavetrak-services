/*  xxAspect  Get an aspect from the user.
    Last modified 1998-05-03

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxAspectCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxAspect *e = (xxAspect *)client_data;

  Dstr num (XawDialogGetValueString (e->mydialog->manager));

  if (num.length() > 0) {
    if (num.strchr ('\n') != -1 ||
           num.strchr ('\r') != -1 ||
           num.strchr (' ') != -1) {
      Dstr details ("Numbers aren't supposed to contain whitespace.  You entered '");
      details += num;
      details += "'.";
      barf (NOT_A_NUMBER, details, 0);
    } else {
      double temp;
      if (sscanf (num.aschar(), "%lf", &temp) != 1) {
	Dstr details ("The offending input was '");
	details += num;
	details += "'.";
	barf (NOT_A_NUMBER, details, 0);
      } else {
        if (temp <= 0.0) {
  	  Dstr details ("The offending input was '");
	  details += num;
	  details += "'.";
	  barf (NUMBER_RANGE_ERROR, details, 0);
        } else
  	  (*(e->aspectcallback)) (temp, e->ptr);
      }
    }
  }

  delete e;
}

static void
xxAspectCancelCallback (Widget w, XtPointer client_data, XtPointer
call_data) {
  xxAspect *e = (xxAspect *)client_data;
  e->dismiss();
}

void
xxAspect::dismiss() {
  delete this;
}

xxAspect::~xxAspect() {
  mypopup->unrealize();
  delete mydialog;
}

xxAspect::xxAspect (xxTideContext *in_xtidecontext, xxContext *context,
void (*in_aspectcallback) (double aspect, void *in_ptr),
void *in_ptr, char *init): xxWindow (in_xtidecontext, context, 0,
XtGrabExclusive) {
  assert (init);
  ptr = in_ptr;
  aspectcallback = in_aspectcallback;
  mypopup->setTitle ("Enter Aspect");

  {
    // Sparcworks barfed on long string constant in the constructor.
    Dstr temp;
    temp = "The normal graph aspect is 1.0.  A value of 2.0 will stretch\n\
the graph to be only half as crowded as normal.  A value of 0.5\n\
will compress the graph to be twice as crowded as normal.  You must\n\
enter a value greater than 0.\n\
\n\
Please enter new aspect.";
    Arg dargs[4] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNlabel, (XtArgVal)temp.aschar()},
      {XtNvalue, (XtArgVal)init}
    };
    Widget dialogwidget = XtCreateManagedWidget ("",
      dialogWidgetClass, mypopup->manager, dargs, 4);
    mydialog = new xxContext (mypopup, dialogwidget);
  }

  XawDialogAddButton (mydialog->manager, "Go", xxAspectCallback,
   (XtPointer)this);
  XawDialogAddButton (mydialog->manager, "Cancel", xxAspectCancelCallback,
   (XtPointer)this);

  // Need to force the colors.
  Arg largs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (XtNameToWidget (mydialog->manager, "label"), largs, 2);
  XtSetValues (XtNameToWidget (mydialog->manager, "value"), largs, 2);
  Arg bargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (XtNameToWidget (mydialog->manager, "Go"), bargs, 2);
  XtSetValues (XtNameToWidget (mydialog->manager, "Cancel"), bargs, 2);

  mypopup->realize();
}
