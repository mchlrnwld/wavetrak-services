// $Id: Timestamp.cc,v 1.13 2004/09/16 15:31:47 flaterco Exp $
// Timestamp:  encapsulation for all manner of time-related services needed
// by XTide.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

// It is important to know one's limitations.
// If time_t is a signed 32-bit int, you get:
//    time_t 0x00000000 = 1970-01-01 00:00:00Z
//    time_t 0x7FFFFFFF = 2038-01-19 03:14:07Z
//    time_t 0x80000000 = 1901-12-13 20:45:52Z
// However, Posix does not require it to be signed, so the safe range
// is only 1970 to 2037.


#ifdef TIME_WORKAROUND

// ************************************
//     Begin derivative of GNU code
// ************************************

// When TIME_WORKAROUND is engaged, time_t is redefined as a signed
// 64-bit integer in common.hh.  It is used below anytime a signed
// 64-bit integer is required, whether or not it's actually a
// timestamp.

// This function was lifted from the file time/offtime.c in
// glibc-2.3.1, made standalone, munged in a futile effort to avoid
// overflows, and renamed to avoid conflicting with any such function
// that is present on the compiling system.

// Modifications are flagged with DWF.

/* Copyright (C) 1991, 1993, 1997, 1998, 2002 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

// DWF: commented out
// #include <errno.h>
// #include <time.h>

// DWF: incorporated from glibc-2.3.1 time/mktime.c
/* Nonzero if YEAR is a leap year (every 4 years,
   except every 100th isn't, and every 400th is).  */
#define __isleap(year) \
  ((year) % 4 == 0 && ((year) % 100 != 0 || (year) % 400 == 0))

// DWF: incorporated from glibc-2.3.1 time/mktime.c
/* How many days come before each month (0-12).  */
const unsigned short int __mon_yday[2][13] =
  {
    /* Normal years.  */
    { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 },
    /* Leap years.  */
    { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 }
  };

#define	SECS_PER_HOUR	(60 * 60)
#define	SECS_PER_DAY	(SECS_PER_HOUR * 24)

/* Compute the `struct tm' representation of *T,
   offset OFFSET seconds east of UTC,
   and store year, yday, mon, mday, wday, hour, min, sec into *TP.
   Return nonzero if successful.  */
// DWF: renamed, converted to C++ declaration syntax
int xtide_offtime (const time_t *t, long int offset, struct tm *tp)
{
  time_t days, y; // DWF: 32 bits doesn't cut it
  long int rem;
  const unsigned short int *ip;

  days = *t / SECS_PER_DAY;  // DWF: here's why
  rem = *t % SECS_PER_DAY;
  rem += offset;
  while (rem < 0)
    {
      rem += SECS_PER_DAY;
      --days;
    }
  while (rem >= SECS_PER_DAY)
    {
      rem -= SECS_PER_DAY;
      ++days;
    }
  tp->tm_hour = rem / SECS_PER_HOUR;
  rem %= SECS_PER_HOUR;
  tp->tm_min = rem / 60;
  tp->tm_sec = rem % 60;
  /* January 1, 1970 was a Thursday.  */
  tp->tm_wday = (4 + days) % 7;
  if (tp->tm_wday < 0)
    tp->tm_wday += 7;
  y = 1970;

#define DIV(a, b) ((a) / (b) - ((a) % (b) < 0))
#define LEAPS_THRU_END_OF(y) (DIV (y, 4) - DIV (y, 100) + DIV (y, 400))

  while (days < 0 || days >= (__isleap (y) ? 366 : 365))
    {
      /* Guess a corrected year, assuming 365 days per year.  */
      // DWF: 64 bits
      time_t yg = y + days / 365 - (days % 365 < 0);

      /* Adjust DAYS and Y to match the guessed year.  */
      days -= ((yg - y) * 365
	       + LEAPS_THRU_END_OF (yg - 1)
	       - LEAPS_THRU_END_OF (y - 1));
      y = yg;
    }
  tp->tm_year = y - 1900;
  if (tp->tm_year != y - 1900)
    {
      /* The year cannot be represented due to overflow.  */
      // DWF: Don't know where __set_errno came from but I don't have it
      // __set_errno (EOVERFLOW);
      errno = EOVERFLOW;
      return 0;
    }
  tp->tm_yday = days;
  ip = __mon_yday[__isleap(y)];
  for (y = 11; days < (long int) ip[y]; --y)
    continue;
  days -= ip[y];
  tp->tm_mon = y;
  tp->tm_mday = days + 1;
  return 1;
}

// **********************************
//     End derivative of GNU code
// **********************************

// Local version of gmtime, complete with internal static buffer.
// Returns null in case of trouble.
struct tm *xtide_gmtime (const time_t *t) {
  static struct tm sstm;
  if (!(xtide_offtime (t, 0, &sstm)))
    return NULL;
  return &sstm;
}

// Redirection
#define gmtime xtide_gmtime
#define localtime xtide_gmtime

// Mktime is only used once and is redirected at the invocation.

#endif

Timestamp::Timestamp () {
  is_null = 1;
}

// Create a Timestamp from a Posix timestamp.
Timestamp::Timestamp (time_t in_posixtime) {
  is_null = 0;
  posixtime = in_posixtime;
}

// Create a Timestamp for the specified Julian date.
Timestamp::Timestamp (double jd) {
  is_null = 0;
  posixtime = (time_t)((jd - beginning_of_time) * DAYSECONDS);
}

// Convert to Julian date.
double Timestamp::jd() const {
  return ((double)posixtime / (double)DAYSECONDS) + beginning_of_time;
}

// Create a Timestamp for the beginning of the specified year in UTC
// (YEAR-01-01 00:00:00Z)
Timestamp::Timestamp (Year in_year) {
  is_null = 0;
  posixtime = year2utc (in_year);
}

Timestamp::Timestamp (struct tm in_utctime) {
  is_null = 0;
  posixtime = tm2utc (in_utctime);
}

Timestamp::Timestamp (Year in_year, double fraction) {
  is_null = 0;

  assert (fraction >= 0.0 && fraction <= 1.0);

  time_t y1 = year2utc (in_year);
  time_t y2 = year2utc (in_year + 1);
  posixtime = y1 + (time_t) (fraction * (y2 - y1));
}

int Timestamp::isNull() const {
  return is_null;
}

// A simple experiment revealed that the call to gmtime to determine
// the year of a timestamp adds 50% to the time that the innermost
// tide-predicting function takes to complete.  At some point I
// optimized this to use a lookup table for the years 1970 to 2037,
// but that became a liability when we started going outside of that
// epoch.  So we are back to the slow but simple way.

Year Timestamp::year() const {
  assert (!is_null);
  return Year (((gmtime (&posixtime))->tm_year) + 1900);
}

// Likewise, year2utc was using the lookup table.
time_t Timestamp::year2utc (Year in_year) {
  struct tm ht;
  ht.tm_year = in_year.val() - 1900;
  ht.tm_sec = ht.tm_min = ht.tm_hour = ht.tm_mon = 0;
  ht.tm_mday = 1;
  return tm2utc (ht);
}

// Compare two struct tm's according to the fields tm_year, tm_mon,
// tm_mday, tm_hour, tm_min, and tm_sec.  Returns:
//      > 0  if a > b
//        0  if a == b
//      < 0  if a < b
#define compare_int(a,b) (((int)(a))-((int)(b)))
int
Timestamp::compare_tm (struct tm a, struct tm b) {
  int temp;
  if ((temp = compare_int (a.tm_year, b.tm_year)))
    return temp;
  if ((temp = compare_int (a.tm_mon, b.tm_mon)))
    return temp;
  if ((temp = compare_int (a.tm_mday, b.tm_mday)))
    return temp;
  if ((temp = compare_int (a.tm_hour, b.tm_hour)))
    return temp;
  if ((temp = compare_int (a.tm_min, b.tm_min)))
    return temp;
  return compare_int (a.tm_sec, b.tm_sec);
}

// Convert a struct tm expressed in UTC to a time_t.  The fields
// used are tm_year, tm_mon, tm_mday, tm_hour, tm_min, and tm_sec.
// All others are ignored.
time_t
Timestamp::tm2utc (struct tm ht)
{
  time_t guess = 0;
  int loopcounter = sizeof(time_t) * 8;
  time_t thebit = ((time_t)1) << (loopcounter-1);

  // Are we signed?
  if (thebit < (time_t)0) {
    switch (loopcounter) {
    case 32:
      // Get this sign thing out of the way.
      if (ht.tm_year + 1900 < 1970)
	guess |= 0x80000000;
      loopcounter = 31;
      break;
    case 64:
      // Had a lot of trouble with overflows.  40 bits should suffice for
      // all reasonable requests.
      if (ht.tm_year + 1900 < 1970)
	guess |= 0xFFFFFF0000000000LL;
      loopcounter = 40;
      break;
    default:
      // Wha?!?
      // Skip the sign bit and hope for the best.
      // You can't just shift thebit right because it propagates the sign bit.
      loopcounter--;
    }
    thebit = ((time_t)1) << (loopcounter-1);
    assert (thebit > (time_t)0);
  } else {
    // We are unsigned.
    // Assuming no overflows, no action required here.
    ;
  }

  int compare;
  struct tm* gm;
  for (; loopcounter; loopcounter--) {
    time_t newguess = guess | thebit;
    // If gmtime fails, we assume that we are too far in the future.
    gm = gmtime(&newguess);
    if (!(compare = (gm ? compare_tm (*gm, ht) : 1)))
      return newguess;
    if (compare < 0)
      guess = newguess;
    assert (thebit > (time_t)0);
    thebit >>= 1;
  }

  assert (!thebit);
  gm = gmtime(&guess);
  if (gm ? compare_tm (*gm, ht) : 1) {
    Dstr details ("The offending time specification was ");
    char tempbuf[80];
    sprintf (tempbuf, "%04d-%02d-%02d %02d:%02d:%02d",
      ht.tm_year+1900,
      ht.tm_mon+1, ht.tm_mday, ht.tm_hour, ht.tm_min, ht.tm_sec);
    details += tempbuf;
    details += "Z.";
    barf (TM2UTC_FAILED, details);
  }

  return guess;
}

// Subtract b from a.
Interval operator- (Timestamp a, Timestamp b) {
  assert (!a.is_null);
  assert (!b.is_null);
  if (sizeof(interval_rep_t) > sizeof(time_t))
    return Interval ((interval_rep_t)(a.posixtime) - (interval_rep_t)(b.posixtime));
  return Interval (a.posixtime - b.posixtime);
}

int operator> (Timestamp a, Timestamp b) {
  assert (!(a.is_null || b.is_null));
  if (a.posixtime > b.posixtime)
    return 1;
  return 0;
}

int operator>= (Timestamp a, Timestamp b) {
  assert (!(a.is_null || b.is_null));
  if (a.posixtime >= b.posixtime)
    return 1;
  return 0;
}

int operator< (Timestamp a, Timestamp b) {
  assert (!(a.is_null || b.is_null));
  if (a.posixtime < b.posixtime)
    return 1;
  return 0;
}

int operator<= (Timestamp a, Timestamp b) {
  assert (!(a.is_null || b.is_null));
  if (a.posixtime <= b.posixtime)
    return 1;
  return 0;
}

Timestamp &Timestamp::operator+= (Interval b) {
  assert (!(is_null));
  posixtime += b.in_seconds();
  return (*this);
}

Timestamp &Timestamp::operator-= (Interval b) {
  assert (!(is_null));
  posixtime -= b.in_seconds();
  return (*this);
}

Timestamp operator+ (Timestamp a, Interval b) {
  a += b;
  return a;
}

Timestamp operator- (Timestamp a, Interval b) {
  a -= b;
  return a;
}

// This allows nulls to equal nulls, and nothing else.
int operator== (Timestamp a, Timestamp b) {
  if (a.is_null != b.is_null)
    return 0;
  if (a.is_null)
    return 1;
  if (a.posixtime != b.posixtime)
    return 0;
  return 1;
}

int operator!= (Timestamp a, Timestamp b) {
  return !(a == b);
}

void Timestamp::make_null() {
  is_null = 1;
}

// Output timestamp in format complying with RFC 2445 (iCalendar)
// if zero_secs <> 0:  YYYYMMDDTHHMM00Z
//              else:  YYYYMMDDTHHMMSSZ
//
// RFC 2445 p. 140, Recommended Practices:
// 7.  If seconds of the minute are not supported by an implementation,
//     then a value of "00" SHOULD be specified for the seconds
//     component in a time value.
// We have seconds, but they aren't significant.
void Timestamp::print_iCalendar (Dstr &out_time, int zero_secs) const {
  char tempbuf[80];
  if (zero_secs)
    assert (strftime (tempbuf, 79, "%Y%m%dT%H%M00Z", gmtime(&posixtime)) < 79);
  else
    assert (strftime (tempbuf, 79, "%Y%m%dT%H%M%SZ", gmtime(&posixtime)) < 79);
  tempbuf[79] = '\0';
  out_time = tempbuf;
}

// Output timestamp in local time zone, no seconds
void Timestamp::print (Dstr &out_time, const Dstr &timezone, Settings *settings) const {
  char tempbuf[80];
  Dstr fmt ((*settings)["df"].s);
  fmt += " ";
  fmt += (*settings)["tf"].s;
  assert (strftime (tempbuf, 79, fmt.aschar(), get_tm(timezone,settings)) < 79);
  tempbuf[79] = '\0';
  out_time = tempbuf;
}

void Timestamp::printhour (Dstr &out_time, const Dstr &timezone, Settings *settings) const {
  char tempbuf[80];
  assert (strftime (tempbuf, 79, (*settings)["hf"].s.aschar(), get_tm(timezone,settings)) < 79);
  tempbuf[79] = '\0';
  out_time = tempbuf;
  // Kludge to get rid of leading space on hours
  if (out_time[0] == ' ')
    out_time /= 1;
}

struct tm *Timestamp::get_tm (const Dstr &timezone, Settings *settings) const {
  assert (!is_null);
  install_time_zone (timezone, settings);
  return localtime(&posixtime);
}

unsigned
Timestamp::gethour (const Dstr &timezone, Settings *settings) const {
  return (unsigned)((get_tm(timezone,settings))->tm_hour);
}

void Timestamp::printdate (Dstr &out_time, const Dstr &timezone, Settings *settings) const {
  char tempbuf[80];
  assert (strftime (tempbuf, 79, (*settings)["df"].s.aschar(), get_tm(timezone,settings)) < 79);
  tempbuf[79] = '\0';
  out_time = tempbuf;
}

void Timestamp::printtime (Dstr &out_time, const Dstr &timezone, Settings *settings) const {
  char tempbuf[80];
  assert (strftime (tempbuf, 79, (*settings)["tf"].s.aschar(), get_tm(timezone,settings)) < 79);
  tempbuf[79] = '\0';
  out_time = tempbuf;
  // Kludge to get rid of leading space on hours
  if (out_time[0] == ' ')
    out_time /= 1;
}

void Timestamp::printcalheading (Dstr &out_heading, const Dstr &timezone,
				 Settings *settings) const {
  char tempbuf[80];
  assert (strftime (tempbuf, 79, "%B %Y", get_tm(timezone,settings)) < 79);
  tempbuf[79] = '\0';
  out_heading = tempbuf;
}

void Timestamp::printdayheading (Dstr &out_heading, const Dstr &timezone,
				 Settings *settings) const {
  char tempbuf[80];
  assert (strftime (tempbuf, 79, "%a %d", get_tm(timezone,settings)) < 79);
  tempbuf[79] = '\0';
  out_heading = tempbuf;
}

Timestamp::Timestamp (const Dstr &in_iso, const Dstr &timezone,
Settings *settings) {
  is_null = 1;
  struct tm temp_tm;
  temp_tm.tm_sec = 0;
  if (sscanf (in_iso.aschar(), "%u-%u-%u %u:%u",
    &(temp_tm.tm_year),
    &(temp_tm.tm_mon),
    &(temp_tm.tm_mday),
    &(temp_tm.tm_hour),
    &(temp_tm.tm_min)) != 5) {
    Dstr details ("The offending time specification was ");
    details += in_iso;
    barf (BAD_TIMESTAMP, details);
  }
  temp_tm.tm_year -= 1900;
  temp_tm.tm_mon -= 1;
  temp_tm.tm_isdst = -1;
  install_time_zone (timezone, settings);
  errno = 0;
#ifdef TIME_WORKAROUND
  posixtime = tm2utc (temp_tm);
#else
  posixtime = mktime (&temp_tm);
#endif
  if (errno == 0)
    is_null = 0;
}


/**************************************************************************/

#ifdef TIME_WORKAROUND

void Timestamp::install_time_zone (const Dstr &timeZone_in, Settings *settings) const
{
  // The only use of time zones is by strftime for %Z
  static char env_string[80];
  static int installed = 0;
  if (!installed) {
    installed = 1;
    strcpy (env_string, "TZ=UTC0");
    assert (putenv (env_string) == 0);
    tzset ();
  }
}

int Timestamp::zoneinfo_doesnt_suck(Settings *settings) {
  // This function is unnecessary when TIME_WORKAROUND is engaged.
  return 1;
}

#else

/* Declarations for zoneinfo compatibility hack */

/* It's not worth detecting or supporting POSIX because all the platforms
   I know of that support full POSIX with floating day rules do so by
   means of adopting Olson's code, in which case we just use the zoneinfo
   extensions anyway.  POSIX without floating day rules is useless. */

typedef enum {NEWZONEINFO=0, OLDZONEINFO=1, HP=2, BRAINDEAD=3, TZNULL=4}
  tz_support;

static tz_support tzlevel = TZNULL;

/* The following were originally based on tzdata96i.  Since then the
   following churn has occurred:

   1999-12-12
   Added Antarctic stations from tzdata1999j
     The Antarctic time zones had previously been guesswork.

   2000-07-16
   Added America/Iqaluit and America/Hermosillo from tzdata2000d
     Sonora and Nunavut each made changes that forced new zones to
     become necessary.

   2000-08-30
   Added America/Goose_Bay from tzdata2000f.

   2000-10-01
   Added :America/Belem, :America/Fortaleza, :America/Maceio from
   tzdata2000d; revised abbreviation for Sao_Paulo.

   It's unfortunate that these zones will be broken on platforms
   having intermediate versions of the "new" zoneinfo.

   2004-09-16
   Added :America/Juneau, :America/Nome, :America/Yakutat and
   :Pacific/Johnston.
*/

/* In many cases, these substitutions will either break DST adjustments
   or will be incorrect for historical dates.  It's not worth documenting
   every single quirk when the whole thing is an obscene hack. */

static char *subs[][4] = {
{":Africa/Abidjan",             "WAT0", "WAT0", "WAT0"},
{":Africa/Accra",               "WAT0", "WAT0", "WAT0"},
{":Africa/Addis_Ababa",         "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Algiers",             "MET-1", "MET-1", "MET-1"},
{":Africa/Asmera",              "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Bamako",              "WAT0", "WAT0", "WAT0"},
{":Africa/Bangui",              "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Banjul",              "WAT0", "WAT0", "WAT0"},
{":Africa/Bissau",              "WAT0", "WAT0", "WAT0"},
{":Africa/Blantyre",            "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Brazzaville",         "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Bujumbura",           "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Cairo",               ":Egypt", "EET-2", "EET-2"},
{":Africa/Casablanca",          "WET0", "WET0", "WET0"},
{":Africa/Conakry",             "WAT0", "WAT0", "WAT0"},
{":Africa/Dakar",               "WAT0", "WAT0", "WAT0"},
{":Africa/Dar_es_Salaam",       "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Djibouti",            "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Douala",              "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Freetown",            "WAT0", "WAT0", "WAT0"},
{":Africa/Gaborone",            "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Harare",              "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Johannesburg",        "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Kampala",             "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Khartoum",            "EET-2", "EET-2", "EET-2"},
{":Africa/Kigali",              "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Kinshasa",            "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Lagos",               "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Libreville",          "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Lome",                "WAT0", "WAT0", "WAT0"},
{":Africa/Luanda",              "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Lumumbashi",          "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Lusaka",              "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Malabo",              "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Maputo",              "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Maseru",              "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Mbabane",             "SAT-2", "SAT-2", "SAT-2"},
{":Africa/Mogadishu",           "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Monrovia",            "WAT0", "WAT0", "WAT0"},
{":Africa/Nairobi",             "EAT-3", "EAT-3", "EAT-3"},
{":Africa/Ndjamena",            "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Niamey",              "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Nouakchott",          "WAT0", "WAT0", "WAT0"},
{":Africa/Ouagadougou",         "WAT0", "WAT0", "WAT0"},
{":Africa/Porto-Novo",          "CAT-1", "CAT-1", "CAT-1"},
{":Africa/Sao_Tome",            "WAT0", "WAT0", "WAT0"},
{":Africa/Timbuktu",            "WAT0", "WAT0", "WAT0"},
{":Africa/Tripoli",             ":Libya", "EET-2", "EET-2"},
{":Africa/Tunis",               "MET-1", "MET-1", "MET-1"},
{":Africa/Windhoek",            "SAT-2", "SAT-2", "SAT-2"},

{":America/Adak",               ":US/Aleutian", "HAST10HADT",  "AST10ADT"},
{":America/Anchorage",          ":US/Alaska", "AKST9AKDT", "YST9YDT"},
{":America/Anguilla",           "AST4", "AST4", "AST4"},
{":America/Antigua",            "AST4", "AST4", "AST4"},
{":America/Aruba",              "AST4", "AST4", "AST4"},
{":America/Asuncion",           "AST4", "AST4", "AST4"},
{":America/Atka",               ":US/Aleutian", "HAST10HADT",  "AST10ADT"},
{":America/Barbados",           "AST4", "AST4", "AST4"},
{":America/Belem",              ":Brazil/East", "BRT3", "BRT3"},
{":America/Belize",             "CST6", "CST6", "CST6"},
{":America/Bogota",             "EST5", "EST5", "EST5"},
{":America/Buenos_Aires",       "ARST3", "ARST3", "AST3"},
{":America/Caracas",            "AST4", "AST4", "AST4"},
{":America/Cayenne",            "EST3", "EST3", "EST3"},
{":America/Cayman",             "EST5", "EST5", "EST5"},
{":America/Chicago",            ":US/Central", "CST6CDT", "CST6CDT"},
{":America/Costa_Rica",         "CST6", "CST6", "CST6"},
{":America/Curacao",            "AST4", "AST4", "AST4"},
{":America/Denver",             ":US/Mountain", "MST7MDT", "MST7MDT"},
{":America/Detroit",            ":US/Eastern", "EST5EDT", "EST5EDT"},
{":America/Dominica",           "AST4", "AST4", "AST4"},
{":America/Edmonton",           ":Canada/Mountain", "MST7MDT", "MST7MDT"},
{":America/El_Salvador",        "CST6", "CST6", "CST6"},
{":America/Ensenada",           ":Mexico/BajaNorte", "PST8PDT", "PST8PDT"},
{":America/Fortaleza",          ":Brazil/East", "BRT3", "BRT3"},
{":America/Godthab",            "WGT3", "WGT3", "WGT3"},
{":America/Goose_Bay",          ":Canada/Atlantic", "AST4ADT", "AST4ADT"},
{":America/Grand_Turk",         ":US/Eastern", "EST5EDT", "EST5EDT"},
{":America/Grenada",            "AST4", "AST4", "AST4"},
{":America/Guadeloupe",         "AST4", "AST4", "AST4"},
{":America/Guatemala",          "CST6", "CST6", "CST6"},
{":America/Guayaquil",          "EST5", "EST5", "EST5"},
{":America/Guyana",             "EST3", "EST3", "EST3"},
{":America/Halifax",            ":Canada/Atlantic", "AST4ADT", "AST4ADT"},
{":America/Havana",             ":Cuba", "CST5", "CST5"},
{":America/Hermosillo",         "MST7", "MST7", "MST7"},
{":America/Iqaluit",            ":Canada/Eastern", "EST5EDT", "EST5EDT"},
{":America/Jamaica",            ":Jamaica", "EST5EDT", "EST5EDT"},
{":America/Juneau",             ":US/Alaska", "AKST9AKDT", "YST9YDT"},
{":America/La_Paz",             "AST4", "AST4", "AST4"},
{":America/Lima",               "EST5", "EST5", "EST5"},
{":America/Los_Angeles",        ":US/Pacific", "PST8PDT", "PST8PDT"},
{":America/Maceio",             ":Brazil/East", "BRT3", "BRT3"},
{":America/Managua",            "CST6", "CST6", "CST6"},
{":America/Manaus",             ":Brazil/West", "WST4", "WST4"},
{":America/Martinique",         "AST4", "AST4", "AST4"},
{":America/Mazatlan",           ":Mexico/BajaSur", "MST7MDT", "MST7MDT"},
{":America/Mexico_City",        "CST6", "CST6", "CST6"},
{":America/Miquelon",           "SPST3SPDT", "SPST3SPDT", "SST3SDT"},
{":America/Montevideo",         "EST3", "EST3", "EST3"},
{":America/Montreal",           ":Canada/Eastern", "EST5EDT", "EST5EDT"},
{":America/Montserrat",         "AST4", "AST4", "AST4"},
{":America/Nassau",             ":US/Eastern", "EST5EDT", "EST5EDT"},
{":America/New_York",           ":US/Eastern", "EST5EDT", "EST5EDT"},
{":America/Nome",               ":US/Alaska", "AKST9AKDT", "YST9YDT"},
{":America/Noronha",            ":Brazil/DeNoronha", "FST2", "FST2"},
{":America/Panama",             "EST5", "EST5", "EST5"},
{":America/Paramaribo",         "EST3", "EST3", "EST3"},
{":America/Port-au-Prince",     ":US/Eastern", "EST5EDT", "EST5EDT"},
{":America/Port_of_Spain",      "AST4", "AST4", "AST4"},
{":America/Porto_Acre",         ":Brazil/Acre", "AST5", "AST5"},
{":America/Puerto_Rico",        "AST4", "AST4", "AST4"},
{":America/Regina",             ":Canada/Saskatchewan", "CST6", "CST6"},
{":America/Santiago",           ":Chile/Continental", "CST4", "CST4"},
{":America/Santo_Domingo",      "AST4", "AST4", "AST4"},
{":America/Sao_Paulo",          ":Brazil/East", "BRT3", "BRT3"},
{":America/Scoresbysund",       "EGT1", "EGT1", "EGT1"},
{":America/St_Johns",      ":Canada/Newfoundland", "NST3:30NDT", "NST3:30NDT"},
{":America/St_Kitts",           "AST4", "AST4", "AST4"},
{":America/St_Lucia",           "AST4", "AST4", "AST4"},
{":America/St_Thomas",          "AST4", "AST4", "AST4"},
{":America/St_Vincent",         "AST4", "AST4", "AST4"},
{":America/Tegucigalpa",        "CST6", "CST6", "CST6"},
{":America/Thule",              "AST4", "AST4", "AST4"},
{":America/Tijuana",            ":Mexico/BajaNorte", "PST8PDT", "PST8PDT"},
{":America/Tortola",            "AST4", "AST4", "AST4"},
{":America/Vancouver",          ":Canada/Pacific", "PST8PDT", "PST8PDT"},
{":America/Whitehorse",         ":Canada/Yukon", "PST8PDT", "PST8PDT"},
{":America/Winnipeg",           ":Canada/Central", "CST6CDT", "CST6CDT"},
{":America/Yakutat",            ":US/Alaska", "AKST9AKDT", "YST9YDT"},

{":Antarctica/Casey",           "WST-8", "WST-8", "WST-8"},
{":Antarctica/Davis",           "DAVT-7", "DAVT-7", "DAVT-7"},
{":Antarctica/DumontDUrville",  "DDUT-10", "DDUT-10", "DDUT-10"},
{":Antarctica/Mawson",          "MAWT-6", "MAWT-6", "MAWT-6"},
{":Antarctica/McMurdo",         ":NZ", "NZST-12NZDT", "NST-12"},
{":Antarctica/Palmer",          ":Chile/Continental", "CST4", "CST4"},
{":Antarctica/South_Pole",      ":NZ", "NZST-12NZDT", "NST-12"},
{":Antarctica/Syowa",           "SYOT-3", "SYOT-3", "SYOT-3"},

{":Asia/Aden",                  "AST-3", "AST-3", "AST-3"},
{":Asia/Aktau",                 "ASK-5", "ASK-5", "ASK-5"},
{":Asia/Alma-Ata",              "AASK-6", "AASK-6", "ASK-6"},
{":Asia/Amman",                 "EET-2", "EET-2", "EET-2"},
{":Asia/Anadyr",                "ASK-13", "ASK-13", "ASK-13"},
{":Asia/Ashkhabad",             "ASK-5", "ASK-5", "ASK-5"},
{":Asia/Baghdad",               "AST-3", "AST-3", "AST-3"},
{":Asia/Bahrain",               "AST-3", "AST-3", "AST-3"},
{":Asia/Baku",                  "BSK-3", "BSK-3", "BSK-3"},
{":Asia/Bangkok",               "ICT-7", "ICT-7", "ICT-7"},
{":Asia/Beirut",                "EET-2", "EET-2", "EET-2"},
{":Asia/Bishkek",               "BSK-5", "BSK-5", "BSK-5"},
{":Asia/Brunei",                "BNT-8", "BNT-8", "BNT-8"},
{":Asia/Calcutta",              "IST-5:30", "IST-5:30", "IST-5:30"},
{":Asia/Colombo",               "IST-5:30", "IST-5:30", "IST-5:30"},
{":Asia/Dacca",                 "BGT-6", "BGT-6", "BGT-6"},
{":Asia/Damascus",              "EET-2", "EET-2", "EET-2"},
{":Asia/Dubai",                 "GST-4", "GST-4", "GST-4"},
{":Asia/Dushanbe",              "DSK-6", "DSK-6", "DSK-6"},
{":Asia/Gaza",                  "IST-2", "IST-2", "IST-2"},
{":Asia/Hong_Kong",             ":Hongkong", "HKT-8", "HKT-8"},
{":Asia/Irkutsk",               "ISK-8", "ISK-8", "ISK-8"},
{":Asia/Istanbul",              ":Turkey", "EET-2EETDST", "EET-2"},
{":Asia/Jakarta",               "JVT-7", "JVT-7", "JVT-7"},
{":Asia/Jayapura",              "MLT-9", "MLT-9", "MLT-9"},
{":Asia/Jerusalem",             ":Israel", "IST-2", "IST-2"},
{":Asia/Kabul",                 "AFT-4:30", "AFT-4:30", "AFT-4:30"},
{":Asia/Kamchatka",             "PSK-12", "PSK-12", "PSK-12"},
{":Asia/Karachi",               "PKT-5", "PKT-5", "PKT-5"},
{":Asia/Katmandu",              "NPT-5:45", "NPT-5:45", "NPT-5:45"},
{":Asia/Kuala_Lumpur",          "SGT-8", "SGT-8", "SGT-8"},
{":Asia/Kuwait",                "AST-3", "AST-3", "AST-3"},
{":Asia/Macao",                 "CST-8", "CST-8", "CST-8"},
{":Asia/Magadan",               "MSK-11", "MSK-11", "MSK-11"},
{":Asia/Manila",                "PST-8", "PST-8", "PST-8"},
{":Asia/Muscat",                "GST-4", "GST-4", "GST-4"},
{":Asia/Nicosia",               "EET-2", "EET-2", "EET-2"},
{":Asia/Novosibirsk",           "NSK-6", "NSK-6", "NSK-6"},
{":Asia/Omsk",                  "OSK-6", "OSK-6", "OSK-6"},
{":Asia/Phnom_Penh",            "ICT-7", "ICT-7", "ICT-7"},
{":Asia/Pyongyang",             "KST-9", "KST-9", "KST-9"},
{":Asia/Qatar",                 "AST-3", "AST-3", "AST-3"},
{":Asia/Rangoon",               "BMT-6:30", "BMT-6:30", "BMT-6:30"},
{":Asia/Riyadh",                "AST-3", "AST-3", "AST-3"},
{":Asia/Saigon",                "ICT-7", "ICT-7", "ICT-7"},
{":Asia/Seoul",                 ":ROK", "KST-9", "KST-9"},
{":Asia/Shanghai",              ":PRC", "CST-8", "CST-8"},
{":Asia/Singapore",             ":Singapore", "SGT-8", "SGT-8"},
{":Asia/Taipei",                ":ROC", "CST-8", "CST-8"},
{":Asia/Tashkent",              "TSK-5", "TSK-5", "TSK-5"},
{":Asia/Tbilisi",               "TBSK-4", "TBSK-4", "TSK-4"},
{":Asia/Tehran",                ":Iran", "IST-3:30", "IST-3:30"},
{":Asia/Tel_Aviv",              ":Israel", "IST-2", "IST-2"},
{":Asia/Thimbu",                "BGT-6", "BGT-6", "BGT-6"},
{":Asia/Tokyo",                 ":Japan", "JST-9", "JST-9"},
{":Asia/Ujung_Pandang",         "BNT-8", "BNT-8", "BNT-8"},
{":Asia/Ulan_Bator",            "UST-8", "UST-8", "UST-8"},
{":Asia/Vientiane",             "ICT-7", "ICT-7", "ICT-7"},
{":Asia/Vladivostok",           "VSK-10", "VSK-10", "VSK-10"},
{":Asia/Yakutsk",               "YSK-9", "YSK-9", "YSK-9"},
{":Asia/Yekaterinburg",         "ESK-5", "ESK-5", "ESK-5"},
{":Asia/Yerevan",               "AMST-4", "AMST-4", "AST-4"},

{":Atlantic/Azores",            "ACT1", "ACT1", "ACT1"},
{":Atlantic/Bermuda",           "AST4ADT", "AST4ADT", "AST4ADT"},
{":Atlantic/Canary",            ":WET", "WET0WETDST", "WET0"},
{":Atlantic/Cape_Verde",        "AAT1", "AAT1", "AAT1"},
{":Atlantic/Faeroe",            ":WET", "WET0WETDST", "WET0"},
{":Atlantic/Jan_Mayen",         "EGT1", "EGT1", "EGT1"},
{":Atlantic/Madeira",           ":WET", "WET0WETDST", "WET0"},
{":Atlantic/Reykjavik",         ":Iceland", "GMT0", "GMT0"},
{":Atlantic/South_Georgia",     "FST2", "FST2", "FST2"},
{":Atlantic/St_Helena",         "GMT0", "GMT0", "GMT0"},
{":Atlantic/Stanley",           "AST4", "AST4", "AST4"},

{":Australia/Adelaide",         ":Australia/South", "CST-9:30CDT", "CST-9:30"},
{":Australia/Brisbane",         ":Australia/Queensland", "EST-10", "EST-10"},
{":Australia/Broken_Hill", ":Australia/Yancowinna", "CST-9:30CDT", "CST-9:30"},
{":Australia/Canberra",         ":Australia/ACT", "EST-10EDT", "EST-10"},
{":Australia/Darwin",           ":Australia/North", "CST-9:30CDT", "CST-9:30"},
{":Australia/Hobart",           ":Australia/Tasmania", "EST-10EDT", "EST-10"},
{":Australia/Lord_Howe",        ":Australia/LHI", "LST-10:30", "LST-10:30"},
{":Australia/Melbourne",        ":Australia/Victoria", "EST-10EDT", "EST-10"},
{":Australia/Perth",            ":Australia/West", "WST-8", "WST-8"},
{":Australia/Sydney",           ":Australia/NSW", "EST-10EDT", "EST-10"},

/* No, it's not a screwup; they really did invert the signs on the
   GMT-offset files from one version to the next in order to "agree"
   with POSIX. */

{":Etc/GMT",                    ":GMT", "GMT0", "GMT0"},
{":Etc/GMT+0",                  ":GMT-0", "GMT0", "GMT0"},
{":Etc/GMT+1",                  ":GMT-1", "LST1", "LST1"},
{":Etc/GMT+10",                 ":GMT-10", "LST10", "LST10"},
{":Etc/GMT+11",                 ":GMT-11", "LST11", "LST11"},
{":Etc/GMT+12",                 ":GMT-12", "LST12", "LST12"},
{":Etc/GMT+2",                  ":GMT-2", "LST2", "LST2"},
{":Etc/GMT+3",                  ":GMT-3", "LST3", "LST3"},
{":Etc/GMT+4",                  ":GMT-4", "LST4", "LST4"},
{":Etc/GMT+5",                  ":GMT-5", "LST5", "LST5"},
{":Etc/GMT+6",                  ":GMT-6", "LST6", "LST6"},
{":Etc/GMT+7",                  ":GMT-7", "LST7", "LST7"},
{":Etc/GMT+8",                  ":GMT-8", "LST8", "LST8"},
{":Etc/GMT+9",                  ":GMT-9", "LST9", "LST9"},
{":Etc/GMT-0",                  ":GMT+0", "GMT0", "GMT0"},
{":Etc/GMT-1",                  ":GMT+1", "LST-1", "LST-1"},
{":Etc/GMT-10",                 ":GMT+10", "LST-10", "LST-10"},
{":Etc/GMT-11",                 ":GMT+11", "LST-11", "LST-11"},
{":Etc/GMT-12",                 ":GMT+12", "LST-12", "LST-12"},
{":Etc/GMT-13",                 ":GMT+13", "LST-13", "LST-13"},
{":Etc/GMT-2",                  ":GMT+2", "LST-2", "LST-2"},
{":Etc/GMT-3",                  ":GMT+3", "LST-3", "LST-3"},
{":Etc/GMT-4",                  ":GMT+4", "LST-4", "LST-4"},
{":Etc/GMT-5",                  ":GMT+5", "LST-5", "LST-5"},
{":Etc/GMT-6",                  ":GMT+6", "LST-6", "LST-6"},
{":Etc/GMT-7",                  ":GMT+7", "LST-7", "LST-7"},
{":Etc/GMT-8",                  ":GMT+8", "LST-8", "LST-8"},
{":Etc/GMT-9",                  ":GMT+9", "LST-9", "LST-9"},
{":Etc/GMT0",                   ":GMT+0", "GMT0", "GMT0"},
{":Etc/Greenwich",              ":Greenwich", "GMT0", "GMT0"},
{":Etc/UCT",                    ":UCT", "GMT0", "GMT0"},
{":Etc/UTC",                    ":UTC", "GMT0", "GMT0"},
{":Etc/Universal",              ":Universal", "GMT0", "GMT0"},
{":Etc/Zulu",                   ":Zulu", "GMT0", "GMT0"},

/* Although the tztab file from an old HP that I have doesn't contain
   EET-2EETDST, I'm going to gamble that they have added it by now.
   It's an obvious extension. */

{":Europe/Amsterdam",           ":MET", "MET-1METDST", "MET-1"},
{":Europe/Andorra",             ":MET", "MET-1METDST", "MET-1"},
{":Europe/Athens",              ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Belfast",             ":GB-Eire", "GMT0BST", "GMT0"},
{":Europe/Belgrade",            ":MET", "MET-1METDST", "MET-1"},
{":Europe/Berlin",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Bratislava",          ":MET", "MET-1METDST", "MET-1"},
{":Europe/Brussels",            ":MET", "MET-1METDST", "MET-1"},
{":Europe/Bucharest",           ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Budapest",            ":MET", "MET-1METDST", "MET-1"},
{":Europe/Chisinau",            ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Copenhagen",          ":MET", "MET-1METDST", "MET-1"},
{":Europe/Dublin",              ":GB-Eire", "GMT0BST", "GMT0"},
{":Europe/Gibraltar",           ":MET", "MET-1METDST", "MET-1"},
{":Europe/Helsinki",            ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Istanbul",            ":Turkey", "EET-2EETDST", "EET-2"},
{":Europe/Kiev",                ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Kuybyshev",           "KSK-4", "KSK-4", "KSK-4"},
{":Europe/Lisbon",              ":WET", "WET0WETDST", "WET0"},
{":Europe/Ljubljana",           ":MET", "MET-1METDST", "MET-1"},
{":Europe/London",              ":GB-Eire", "GMT0BST", "GMT0"},
{":Europe/Luxembourg",          ":MET", "MET-1METDST", "MET-1"},
{":Europe/Madrid",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Malta",               ":MET", "MET-1METDST", "MET-1"},
{":Europe/Minsk",               ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Monaco",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Moscow",              ":W-SU", "MSK-3", "MSK-3"},
{":Europe/Oslo",                ":MET", "MET-1METDST", "MET-1"},
{":Europe/Paris",               ":MET", "MET-1METDST", "MET-1"},
{":Europe/Prague",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Riga",                ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Rome",                ":MET", "MET-1METDST", "MET-1"},
{":Europe/San_Marino",          ":MET", "MET-1METDST", "MET-1"},
{":Europe/Sarajevo",            ":MET", "MET-1METDST", "MET-1"},
{":Europe/Simferopol",          ":W-SU", "MSK-3", "MSK-3"},
{":Europe/Skopje",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Sofia",               ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Stockholm",           ":MET", "MET-1METDST", "MET-1"},
{":Europe/Tallinn",             ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Tirane",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Vaduz",               ":MET", "MET-1METDST", "MET-1"},
{":Europe/Vatican",             ":MET", "MET-1METDST", "MET-1"},
{":Europe/Vienna",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Vilnius",             ":EET", "EET-2EETDST", "EET-2"},
{":Europe/Warsaw",              ":Poland", "MET-1METDST", "MET-1"},
{":Europe/Zagreb",              ":MET", "MET-1METDST", "MET-1"},
{":Europe/Zurich",              ":MET", "MET-1METDST", "MET-1"},

{":Indian/Antananarivo",        "EAT-3", "EAT-3", "EAT-3"},
{":Indian/Chagos",              "PKT-5", "PKT-5", "PKT-5"},
{":Indian/Christmas",           "JVT-7", "JVT-7", "JVT-7"},
{":Indian/Cocos",               "CCT-6:30", "CCT-6:30", "CCT-6:30"},
{":Indian/Comoro",              "EAT-3", "EAT-3", "EAT-3"},
{":Indian/Kerguelen",           "TFT-5", "TFT-5", "TFT-5"},
{":Indian/Mahe",                "SMT-4", "SMT-4", "SMT-4"},
{":Indian/Maldives",            "PKT-5", "PKT-5", "PKT-5"},
{":Indian/Mauritius",           "SMT-4", "SMT-4", "SMT-4"},
{":Indian/Mayotte",             "EAT-3", "EAT-3", "EAT-3"},
{":Indian/Reunion",             "SMT-4", "SMT-4", "SMT-4"},

{":Pacific/Apia",               "SST11", "SST11", "SST11"},
{":Pacific/Auckland",           ":NZ", "NZST-12NZDT", "NST-12"},
{":Pacific/Chatham",            "CST-12:45", "CST-12:45", "CST-12:45"},
{":Pacific/Easter",             ":Chile/EasterIsland", "CST6", "CST6"},
{":Pacific/Efate",              "NCST-11", "NCST-11", "NST-11"},
{":Pacific/Enderbury",          "TGT-13", "TGT-13", "TGT-13"},
{":Pacific/Fakaofo",            "THT10", "THT10", "THT10"},
{":Pacific/Fiji",               "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Funafuti",           "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Galapagos",          "CST6", "CST6", "CST6"},
{":Pacific/Gambier",            "GBT9", "GBT9", "GBT9"},
{":Pacific/Guadalcanal",        "NCST-11", "NCST-11", "NST-11"},
{":Pacific/Guam",               "GST-10", "GST-10", "GST-10"},
{":Pacific/Honolulu",           ":US/Hawaii", "HST10", "HST10"},
{":Pacific/Johnston",           ":US/Hawaii", "HST10", "HST10"},
{":Pacific/Kiritimati",         "KRT-14", "KRT-14", "KRT-14"},
{":Pacific/Kosrae",             "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Kwajalein",          "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Majuro",             "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Marquesas",          "MQT9:30", "MQT9:30", "MQT9:30"},
{":Pacific/Midway",             "SST11", "SST11", "SST11"},
{":Pacific/Nauru",              "NZST-12", "NZST-12", "NST-12"}, 
{":Pacific/Niue",               "SST11", "SST11", "SST11"},
{":Pacific/Norfolk",            "NRFT-11:30", "NRFT-11:30", "NFT-11:30"},
{":Pacific/Noumea",             "NCST-11", "NCST-11", "NST-11"},
{":Pacific/Pago_Pago",          "SST11", "SST11", "SST11"},
{":Pacific/Palau",              "PLT-9", "PLT-9", "PLT-9"},
{":Pacific/Pitcairn",           "PIT8:30", "PIT8:30", "PIT8:30"},
{":Pacific/Ponape",             "NCST-11", "NCST-11", "NST-11"},
{":Pacific/Port_Moresby",       "EST-10", "EST-10", "EST-10"},
{":Pacific/Rarotonga",          "THT10", "THT10", "THT10"},
{":Pacific/Saipan",             "GST-10", "GST-10", "GST-10"},
{":Pacific/Samoa",              "SST11", "SST11", "SST11"},
{":Pacific/Tahiti",             "THT10", "THT10", "THT10"},
{":Pacific/Tarawa",             "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Tongatapu",          "TGT-13", "TGT-13", "TGT-13"},
{":Pacific/Truk",               "GST-10", "GST-10", "GST-10"},
{":Pacific/Wake",               "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Wallis",             "NZST-12", "NZST-12", "NST-12"},
{":Pacific/Yap",                "GST-10", "GST-10", "GST-10"},

/* Compatibility with outdated TZ specs */

{":US/Hawaii",                  ":US/Hawaii", "HST10",  "HST10"},
{":US/Aleutian",                ":US/Aleutian", "HAST10HADT",  "AST10ADT"},
{":US/Alaska",                  ":US/Alaska", "AKST9AKDT", "YST9YDT"},
{":US/Pacific",                 ":US/Pacific", "PST8PDT", "PST8PDT"},
{":US/Mountain",                ":US/Mountain", "MST7MDT", "MST7MDT"},
{":US/Central",                 ":US/Central", "CST6CDT", "CST6CDT"},
{":US/Eastern",                 ":US/Eastern", "EST5EDT", "EST5EDT"},

{":Canada/Pacific",             ":Canada/Pacific", "PST8PDT", "PST8PDT"},
{":Canada/Yukon",               ":Canada/Yukon", "PST8PDT", "PST8PDT"},
{":Canada/Mountain",            ":Canada/Mountain", "MST7MDT", "MST7MDT"},
{":Canada/Central",             ":Canada/Central", "CST6CDT", "CST6CDT"},
{":Canada/Saskatchewan",        ":Canada/Saskatchewan", "CST6", "CST6"},
{":Canada/East-Saskatchewan",   ":Canada/East-Saskatchewan", "CST6", "CST6"},
{":Canada/Eastern",             ":Canada/Eastern", "EST5EDT", "EST5EDT"},
{":Canada/Atlantic",            ":Canada/Atlantic", "AST4ADT", "AST4ADT"},
{":Canada/Newfoundland",   ":Canada/Newfoundland", "NST3:30NDT", "NST3:30NDT"},

{":Mexico/BajaNorte",           ":Mexico/BajaNorte", "PST8PDT", "PST8PDT"},
{":Mexico/BajaSur",             ":Mexico/BajaSur", "MST7MDT", "MST7MDT"},
{":Mexico/General",             "CST6", "CST6", "CST6"},

{":Brazil/West",                ":Brazil/West", "WST4", "WST4"},
{":Brazil/DeNoronha",           ":Brazil/DeNoronha", "FST2", "FST2"},
{":Brazil/East",                ":Brazil/East", "EST3", "EST3"},

{":Chile/Continental",          ":Chile/Continental", "CST4", "CST4"},
{":Chile/EasterIsland",         ":Chile/EasterIsland", "CST6", "CST6"},

{":Australia/ACT",              ":Australia/ACT", "EST-10EDT", "EST-10"},
{":Australia/LHI",              ":Australia/LHI", "LST-10:30", "LST-10:30"},
{":Australia/North",            ":Australia/North", "CST-9:30CDT", "CST-9:30"},
{":Australia/NSW",              ":Australia/NSW", "EST-10EDT", "EST-10"},
{":Australia/Queensland",       ":Australia/Queensland", "EST-10", "EST-10"},
{":Australia/South",            ":Australia/South", "CST-9:30CDT", "CST-9:30"},
{":Australia/Tasmania",         ":Australia/Tasmania", "EST-10EDT", "EST-10"},
{":Australia/Victoria",         ":Australia/Victoria", "EST-10EDT", "EST-10"},
{":Australia/West",             ":Australia/West", "WST-8", "WST-8"},
{":Australia/Yancowinna",  ":Australia/Yancowinna", "CST-9:30CDT", "CST-9:30"},

{":WET",                        ":WET", "WET0WETDST", "WET0"},
{":MET",                        ":MET", "MET-1METDST", "MET-1"},
{":EET",                        ":EET", "EET-2EETDST", "EET-2"},
{":GB-Eire",                    ":GB-Eire", "GMT0BST", "GMT0"},
{":PRC",                        ":PRC", "CST-8", "CST-8"},
{":ROC",                        ":ROC", "CST-8", "CST-8"},
{":ROK",                        ":ROK", "KST-9", "KST-9"},
{":W-SU",                       ":W-SU", "MSK-3", "MSK-3"},

{":Cuba",                       ":Cuba", "CST5", "CST5"},
{":Egypt",                      ":Egypt", "EET-2", "EET-2"},
{":Hongkong",                   ":Hongkong", "HKT-8", "HKT-8"},
{":Iceland",                    ":Iceland", "GMT0", "GMT0"},
{":Iran",                       ":Iran", "IST-3:30", "IST-3:30"},
{":Israel",                     ":Israel", "IST-2", "IST-2"},
{":Jamaica",                    ":Jamaica", "EST5EDT", "EST5EDT"},
{":Japan",                      ":Japan", "JST-9", "JST-9"},
{":Libya",                      ":Libya", "EET-2", "EET-2"},
{":Poland",                     ":Poland", "MET-1METDST", "MET-1"},
{":Singapore",                  ":Singapore", "SGT-8", "SGT-8"},
{":Turkey",                     ":Turkey", "EET-2EETDST", "EET-2"},

/* Terminator */
{NULL, NULL, NULL, NULL}};

/**************************************************************************/

static Dstr current_time_zone;

// Unfortunately, time zones are a session default.
void Timestamp::install_time_zone (const Dstr &timeZone_in, Settings *settings) const
{
  Dstr timeZone ("UTC0");
  if ((*settings)["z"].c == 'n')
    timeZone = timeZone_in;
  if (current_time_zone == timeZone)
    return;
  current_time_zone = timeZone;

  static char env_string[80];
  char junk[80];
  Dstr tz (timeZone);

  if (tzlevel == TZNULL) {
    // This should only be needed once.
    /* Probe to determine the tzlevel -- hopefully this won't hose anybody */

    time_t testtime = time(NULL);

    /* This is needed on some Solaris and IRIX machines to avoid a false
       positive on NEWZONEINFO that happens when -gstart is used.  Don't
       know exactly what's up with that, but this fixes it. */
    strcpy (env_string, "TZ=GMT0");
    assert (putenv (env_string) == 0);
    tzset ();

    /* New zoneinfo? */
    strcpy (env_string, "TZ=:America/New_York");
    assert (putenv (env_string) == 0);
    tzset();
    strftime (junk, 79, "%Z", localtime (&testtime));
    if (junk[0] == 'E') {
      tzlevel = NEWZONEINFO;
    } else {

      log ("\
XTide Warning:  Using obsolete time zone database.  Summer Time (Daylight\n\
Savings Time) adjustments will not be done for some locations.", LOG_WARNING);

      /* Old zoneinfo? */
      strcpy (env_string, "TZ=:US/Eastern");
      assert (putenv (env_string) == 0);
      tzset();
      strftime (junk, 79, "%Z", localtime (&testtime));
      if (junk[0] == 'E') {
	tzlevel = OLDZONEINFO;
      } else {

	/* HP-UX? */
  #ifdef __hpux
	tzlevel = HP;
  #else
	tzlevel = BRAINDEAD;
  #endif
      }
    }
  }

  assert (tzlevel != TZNULL);
  /* If our level of support is less than NEWZONEINFO, find the
     translation for the timezone string */
  if (tzlevel != NEWZONEINFO) {
    int index = 0;
    while (1) {
      if (subs[index][0] == NULL) {
        /* Not found.  Probably best not to issue a warning, since somebody
           might use something that will work. */
        break;
      }
      if (timeZone == subs[index][0]) {
        tz = subs[index][tzlevel];
        break;
      }
      index++;
    }
  }

  /* According to the SYSV man page, I can't alter env_string ever again. */
  sprintf (env_string, "TZ=%s", tz.aschar());
  assert (putenv (env_string) == 0);
  tzset ();
}

int Timestamp::zoneinfo_doesnt_suck(Settings *settings) {
  if (tzlevel == TZNULL) {
    Dstr temptz ("UTC0");
    install_time_zone (temptz, settings);
  }
  return (tzlevel == NEWZONEINFO);
}

#endif


/* Find the previous hour-transition from a specified time.  If a DST
   change skips over the transition, then we skip it too. */
void
Timestamp::prev_hour (const Dstr &timezone, Settings *settings) {
  install_time_zone (timezone, settings);
  struct tm ttm;
  time_t temp, temp2;

  /* The easy case will work unless we are hosed by DST. */
  ttm = *(localtime (&posixtime));
  temp = posixtime - (ttm.tm_min * 60 + ttm.tm_sec);
  assert (temp > posixtime - HOURSECONDS && temp <= posixtime);
  ttm = *(localtime (&temp));
  if (ttm.tm_sec == 0 && ttm.tm_min == 0) {
    posixtime = temp;
    return;
  }

  /* See if we went back too far. */
  temp2 = temp + HOURSECONDS - (ttm.tm_min * 60 + ttm.tm_sec);
  if (temp2 > posixtime - HOURSECONDS && temp2 <= posixtime) {
    ttm = *(localtime (&temp2));
    if (ttm.tm_sec == 0 && ttm.tm_min == 0) {
      /* fprintf (stderr, "prev_hour:  Too far\n"); */
      posixtime = temp2;
      return;
    }
  }

  /* Go back farther. */
  /* fprintf (stderr, "prev_hour:  Not far enough; recursing\n"); */
  posixtime = temp;
  prev_hour(timezone,settings);
}

/* Given that we are on an hour transition, find the next one.  If a DST
   change skips over the transition, then we skip it too. */
void
Timestamp::inc_hour (const Dstr &timezone, Settings *settings) {
  install_time_zone (timezone, settings);
  struct tm ttm;
  time_t temp, temp2;

  /* The easy case will work unless we are hosed by DST. */
  temp = posixtime + HOURSECONDS;
  ttm = *(localtime (&temp));
  if (ttm.tm_sec == 0 && ttm.tm_min == 0) {
    posixtime = temp;
    return;
  }

  /* See if we went forward too far. */
  temp2 = temp - (ttm.tm_min * 60 + ttm.tm_sec);
  if (temp2 > posixtime && temp2 <= posixtime + HOURSECONDS) {
    ttm = *(localtime (&temp2));
    if (ttm.tm_sec == 0 && ttm.tm_min == 0) {
      /* fprintf (stderr, "increment_hour:  Too far\n"); */
      posixtime = temp2;
      return;
    }
  }

  /* Go forward some more. */
  /* fprintf (stderr, "increment_hour:  Not far enough; recursing\n"); */
  posixtime = temp;
  inc_hour(timezone,settings);
  /* That will work, even though we are not starting on an hour transition. */
}

/* Find the previous day-transition from a specified time.  Returns a
   reasonable guess even if there is no midnight due to DST adjustment. */
void
Timestamp::prev_day (const Dstr &timezone, Settings *settings) {
  install_time_zone (timezone, settings);
  struct tm ttm;
  time_t temp, temp2;
  int today;

  /* The easy case will work unless we are hosed by DST. */
  ttm = *(localtime (&posixtime));
  today = ttm.tm_mday;
  temp = posixtime - (ttm.tm_hour * HOURSECONDS + ttm.tm_min * 60 + ttm.tm_sec);
  assert (temp > posixtime - DAYSECONDS && temp <= posixtime);
  ttm = *(localtime (&temp));
  if (ttm.tm_hour == 0 && ttm.tm_sec == 0 && ttm.tm_min == 0) {
    posixtime = temp;
    return;
  }

  /* See if we went back too far. */
  if (ttm.tm_mday != today) {
    temp2 = temp + DAYSECONDS - (ttm.tm_hour * HOURSECONDS +
    ttm.tm_min * 60 + ttm.tm_sec);
    /* fprintf (stderr, "prev_day:  Too far\n"); */
    /* This is always our best guess (approach from left) due to DST
       changes being applied exactly at midnight. */
    posixtime = temp2;
    return;
  }

  /* Go back farther. */
  /* fprintf (stderr, "prev_day:  Not far enough; recursing\n"); */
  posixtime = temp;
  prev_day (timezone,settings);
}

/* Given that we are on a day transition, find the next one.  Returns a
   reasonable guess even if there is no midnight due to DST adjustment. */
void
Timestamp::inc_day (const Dstr &timezone, Settings *settings) {
  install_time_zone (timezone, settings);
  struct tm ttm;
  int today;

  /* The easy case will work unless we are hosed by DST. */
  ttm = *(localtime (&posixtime));
  today = ttm.tm_mday;
  posixtime += DAYSECONDS;
  ttm = *(localtime (&posixtime));
  if (ttm.tm_hour == 0 && ttm.tm_sec == 0 && ttm.tm_min == 0) {
    return;
  }

  /* See if we went forward too far. */
  if (ttm.tm_mday != today) {
    /* fprintf (stderr, "increment_day:  Too far; calling prev_day\n"); */
    prev_day (timezone,settings);
    return;
  }

  /* Go forward farther. */
  /* fprintf (stderr, "increment_day:  Not far enough; recursing\n"); */
  inc_day (timezone,settings);
  /* That will work, even though we are not starting on a day transition. */
}

time_t Timestamp::timet() const {
  return posixtime;
}

// The moonrise and moonset logic blows up if you go before 1900 or
// after 2099.  This is just a range check for that.
int Timestamp::moonriseset_inrange() const {
  double danger_y = 1900. + (jd() - 2415019.5) / 365.25;
  if (danger_y > 1900.1 && danger_y < 2099.9)
    return 1;
  return 0;
}
