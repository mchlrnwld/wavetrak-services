// 	$Id: HarmonicsFile.cc,v 1.27 2004/11/17 16:27:18 flaterco Exp $	
/*  HarmonicsFile  Operations for reading harmonics files.

    Copyright (C) 1998  David Flater.

    Vastly altered by Jan Depner 2002-09 to remove old .txt and .xml
    based code and replace with libtcd.

    Additional crufting up added subsequently by David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// HarmonicsFile has inherited the statefulness of libtcd.  Do not
// instantiate more than one HarmonicsFile at a time or bad things
// will happen.

#include "common.hh"
#include "tcd.h"

// Abandon all hope.

// Herein lies the interface between the overdesigned object-oriented
// spaghetti of XTide and the overoptimized bit-bashing C-programmer
// disease of libtcd.

// HarmonicsFile has inherited the statefulness of libtcd.  Do not
// instantiate more than one HarmonicsFile at a time or bad things
// will happen.

// libtcd's open_tide_db is called each time a HarmonicsFile is
// created.  Intentionally, there is no matching call to close_tide_db
// (consquently, no need for a destructor).  See
// http://www.flaterco.com/xtide/tcd_notes.html and XTide changelog
// for version 2.6.3.

HarmonicsFile::HarmonicsFile (Dstr &filename, Settings *in_settings) {
  settings = in_settings;
  myfilename = &filename;

  // Do some sanity checks before invoking libtcd.  (open_tide_db just
  // returns false regardless of what the problem was.)
  {
    FILE *fp = fopen (filename.aschar(), "r");
    if (fp) {
      char c = fgetc (fp);
      if (c != '[') {
        Dstr details (filename.aschar());
        details += " is apparently not a TCD file.\n\
We do not use harmonics.txt or offsets.xml anymore.  Please see\n\
http://www.flaterco.com/xtide/files.html for a link to the current data.";
        barf (CORRUPT_HARMONICS_FILE, details);
      }
      fclose (fp);
    } else {
      // This should never happen.  We statted this file in
      // [xx]TideContext.cc.
      Dstr details (filename.aschar());
      details += ": ";
      details += strerror (errno);
      details += ".";
      barf (CANT_OPEN_FILE, details);
    }
  }

  if (!open_tide_db ((*myfilename).aschar())) {
    Dstr details (*myfilename);
    details += ": libtcd returned generic failure.";
    barf (CORRUPT_HARMONICS_FILE, details);
  }
  DB_HEADER_PUBLIC db = get_tide_db_header ();
  version_string = *myfilename;
  {
    int i;
    if ((i = version_string.strrchr ('/')) != -1)
      version_string /= (i+1);
  }
  version_string += " ";
  version_string += (char *) db.last_modified;
  version_string += " / ";
  version_string += (char *) db.version;
}

StationRef *HarmonicsFile::getNextStationRef () {
  TIDE_STATION_HEADER rec;
  long i;
  if ((i = get_next_partial_tide_record (&rec)) == -1) return NULL;
  assert (i >= 0);
  StationRef *sr = new StationRef();
  sr->isReferenceStation = (rec.record_type == 1 ? 1 : 0);
  sr->harmonicsFileName = myfilename;
  sr->TCDRecordNumber = i;
  sr->name = (char *) rec.name;
  sr->coordinates.lat (rec.latitude);
  sr->coordinates.lng (rec.longitude);
  sr->timezone = (char *) get_tzfile (rec.tzfile);
  return sr;
}

// See add_metadata.
static void parse_xfields (MetaFieldVector &metadata, const char *xfields) {
  assert (xfields);
  Dstr x (xfields);
  Dstr linebuf, name, value;
  x.getline (linebuf);
  while (!(linebuf.isNull())) {
    if (linebuf[0] == ' ') {
      if (!(name.isNull())) {
        linebuf /= 1;
        value += '\n';
        value += linebuf;
      }
    } else {
      if (!(name.isNull())) {
        metadata.push_back (MetaField (name, value));
        name = (char *)NULL;
        value = (char *)NULL;
      }
      unsigned i = linebuf.strchr (':');
      if (i > 0) {
        name = linebuf;
        name -= i;
        value = linebuf.ascharfrom(i+1);
      }
    }
    x.getline (linebuf);
  }
  if (!(name.isNull()))
    metadata.push_back (MetaField (name, value));
}

// This fn is static to avoid having to include tcd.h anywhere but here.
static void add_metadata (Dstr *myfilename, Station &s, TIDE_RECORD &rec) {
  Dstr tmpbuf;
  char tmp[80];

  s.metadata.push_back (MetaField ("Name", s.name));
  s.metadata.push_back (MetaField ("In file", *myfilename));
  if (rec.legalese)
    s.metadata.push_back (MetaField ("Legalese", get_legalese(rec.legalese)));
  if (rec.station_id_context[0])
    s.metadata.push_back (MetaField ("Station ID context",
				     rec.station_id_context));
  if (rec.station_id[0])
    s.metadata.push_back (MetaField ("Station ID", rec.station_id));
  if (rec.date_imported)
    s.metadata.push_back (MetaField ("Date imported",
				    ret_date(rec.date_imported)));
  s.coordinates.print (tmpbuf);
  s.metadata.push_back (MetaField ("Coordinates", tmpbuf));
  s.metadata.push_back (MetaField ("Country", get_country(rec.country)));
  s.metadata.push_back (MetaField ("Time zone", s.timeZone));
  if (!(s.maxCurrentAngle.isNull())) {
    s.maxCurrentAngle.ppqdeg (0, tmpbuf);
    s.metadata.push_back (MetaField ("Flood direction", tmpbuf.aschar()));
  }
  if (!(s.minCurrentAngle.isNull())) {
    s.minCurrentAngle.ppqdeg (0, tmpbuf);
    s.metadata.push_back (MetaField ("Ebb direction", tmpbuf.aschar()));
  }
  if (rec.source[0])
    s.metadata.push_back (MetaField ("Source", rec.source));
  s.metadata.push_back (MetaField ("Restriction", get_restriction(rec.restriction)));
  if (rec.comments[0])
    s.metadata.push_back (MetaField ("Comments", rec.comments));
  if (rec.notes[0])
    s.metadata.push_back (MetaField ("Notes", rec.notes));
  parse_xfields (s.metadata, rec.xfields);

  switch (rec.header.record_type) {
  case 1:
    s.metadata.push_back (MetaField ("Type",
      s.isCurrent ? (s.isHydraulicCurrent ?
	"Reference station, hydraulic current" :
	"Reference station, current") : "Reference station, tide"));
    s.metadata.push_back (MetaField ("Meridian", ret_time_neat(rec.zone_offset)));
    if (!s.isCurrent)
      s.metadata.push_back (MetaField ("Datum", get_datum(rec.datum)));
    s.metadata.push_back (MetaField ("Native units", get_level_units (rec.level_units)));
    sprintf (tmp, "%d", rec.months_on_station);
    if (rec.months_on_station)
      s.metadata.push_back (MetaField ("Months on station", tmp));
    if (rec.last_date_on_station)
      s.metadata.push_back (MetaField ("Last date on station",
				      ret_date(rec.last_date_on_station)));
    if (rec.expiration_date)
      s.metadata.push_back (MetaField ("Expiration",
				      ret_date(rec.expiration_date)));
    sprintf (tmp, "%d", rec.confidence);
    s.metadata.push_back (MetaField ("Confidence", tmp));
    break;

  case 2:
    {
      // It seems like I should just be able to cast this...
      SubordinateStation &sub = *((SubordinateStation*)(&s));
      s.metadata.push_back (MetaField ("Type",
	s.isCurrent ? (s.isHydraulicCurrent ?
	  "Subordinate station, hydraulic current" :
	  "Subordinate station, current") : "Subordinate station, tide"));
      s.metadata.push_back (MetaField ("Reference", get_station(rec.header.reference_station)));
      s.metadata.push_back (MetaField ("Min time add",
	rec.min_time_add ? ret_time_neat (rec.min_time_add) : "NULL"));
      sub.origOffsets->min.levelAdd.printnp (tmpbuf);
      if (tmpbuf[0] != '-')
	tmpbuf *= '+';
      s.metadata.push_back (MetaField ("Min level add",
	rec.min_level_add ? tmpbuf : "NULL"));
      sprintf (tmp, "%0.3f", rec.min_level_multiply);
      s.metadata.push_back (MetaField ("Min level mult",
	rec.min_level_multiply > 0.0 ? tmp : "NULL"));
      s.metadata.push_back (MetaField ("Max time add",
	rec.max_time_add ? ret_time_neat (rec.max_time_add) : "NULL"));
      sub.origOffsets->max.levelAdd.printnp (tmpbuf);
      if (tmpbuf[0] != '-')
	tmpbuf *= '+';
      s.metadata.push_back (MetaField ("Max level add",
	rec.max_level_add ? tmpbuf : "NULL"));
      sprintf (tmp, "%0.3f", rec.max_level_multiply);
      s.metadata.push_back (MetaField ("Max level mult",
	rec.max_level_multiply > 0.0 ? tmp : "NULL"));
      if (s.isCurrent) {
        s.metadata.push_back (MetaField ("Flood begins",
	  rec.flood_begins == NULLSLACKOFFSET ? "NULL"
            : ret_time_neat (rec.flood_begins)));
        s.metadata.push_back (MetaField ("Ebb begins",
	  rec.ebb_begins == NULLSLACKOFFSET ? "NULL"
            : ret_time_neat (rec.ebb_begins)));
      }
      break;
    }

  default:
    assert (0);
  }
}

ConstituentSet *HarmonicsFile::get_constituents () {
  DB_HEADER_PUBLIC db = get_tide_db_header();
  unsigned mynumconst = db.constituents;
  ConstituentSet *c = new ConstituentSet (mynumconst);
  unsigned i;
  for (i=0; i<mynumconst; i++) {
    (*c)[i].speed (DegreesPerHour (get_speed (i)));
  }
  Year startyear (db.start_year);
  Year endyear (startyear + (db.number_of_years - 1));
  for (i=0; i<mynumconst; i++)
    (*c)[i].installargsandnodes (startyear, endyear,
      get_equilibriums(i), get_node_factors(i));
  assert (c->constituents);
  assert (!(c->constituents[0].firstvalidyear().isNull()));
  return c;
}

Station *HarmonicsFile::getStation (long TCDRecordNumber,
				    TideContext *in_context) {
  assert (TCDRecordNumber >= 0);
  assert (in_context);
  TIDE_RECORD rec;
  Station *s = NULL;
  assert (read_tide_record (TCDRecordNumber, &rec) == TCDRecordNumber);

  switch (rec.header.record_type) {
  case 1:
    {
      Dstr meridian, units;
      double d_datum;
      unsigned i;

      if ((*(in_context->settings))["in"].c == 'y') infer_constituents (&rec);

      meridian = (char *) ret_time_neat (rec.zone_offset);
      units = (char *) get_level_units (rec.level_units);
      PredictionValue::Unit d_units (units);
      d_datum = rec.datum_offset;
      PredictionValue::Unit a_units;
      if (d_units.mytype == PredictionValue::Unit::KnotsSquared) {
	a_units = PredictionValue::Unit (PredictionValue::Unit::KnotsSquared);
	d_units = PredictionValue::Unit (PredictionValue::Unit::Knots);
      } else
	a_units = d_units;

      ConstituentSet *constituents = get_constituents();
      unsigned mynumconst = constituents->length;
      ConstantSet *constants = new ConstantSet (mynumconst);
      constants->datum = PredictionValue (d_units, d_datum);

      for (i=0; i<mynumconst; i++) {
	constants->amplitudes[i] = Amplitude (a_units, (double) rec.amplitude[i]);
	// Note negation of phase
	constants->phases[i] = Degrees (-rec.epoch[i]);
      }

      // Normalize the meridian to UTC
      // To compensate for a negative meridian requires a positive offset.
      SimpleOffsets fixMeridianOffsets;
      fixMeridianOffsets.timeAdd = -Interval (meridian);
      constants->adjust (fixMeridianOffsets, *constituents);
      ConstantSetWrapper *csw = new ConstantSetWrapper (constituents, constants);
      s = new ReferenceStation (in_context, csw);

      s->myUnits = d_units;
      // FIXME if there are ever units of velocity other than knots
      if (d_units.mytype == PredictionValue::Unit::Knots) {
	s->isCurrent = 1;
	if (a_units.mytype == PredictionValue::Unit::KnotsSquared)
	  s->isHydraulicCurrent = 1;
	else
	  s->isHydraulicCurrent = 0;
      } else
	s->isCurrent = s->isHydraulicCurrent = 0;

      // FIXME if there are ever units of velocity other than knots
      Dstr u = (*(in_context->settings))["u"].s;
      if (u != "x" && (!(s->isCurrent)))
	s->setUnits (PredictionValue::Unit (u));
    }
    break;

  case 2:
    {
      Dstr value;
      PredictionValue::Unit tu;
      HairyOffsets *so = new HairyOffsets ();

      value = (char *) ret_time_neat (rec.min_time_add);
      so->min.timeAdd = Interval (value);
      value = (char *) get_level_units (rec.level_units);
      tu = PredictionValue::Unit (value);
      so->min.levelAdd = PredictionValue (tu, rec.min_level_add);
      if (rec.min_level_multiply != 0.0) 
	so->min.levelMultiply (rec.min_level_multiply);

      value = (char *) ret_time_neat (rec.max_time_add);
      so->max.timeAdd = Interval (value);
      value = (char *) get_level_units (rec.level_units);
      tu = PredictionValue::Unit (value);
      so->max.levelAdd = PredictionValue (tu, rec.max_level_add);
      if (rec.max_level_multiply != 0.0) 
	so->max.levelMultiply (rec.max_level_multiply);

      // For a discussion of why zero is not the same as null here,
      // see http://www.flaterco.com/xtide/tcd_notes.html

      if (rec.flood_begins != NULLSLACKOFFSET) {
	value = (char *) ret_time_neat (rec.flood_begins);
	so->floodbegins = NullableInterval (value);
      }
      // Else let it stay null

      if (rec.ebb_begins != NULLSLACKOFFSET) {
	value = (char *) ret_time_neat (rec.ebb_begins);
	so->ebbbegins = NullableInterval (value);
      }
      // Else let it stay null

      ReferenceStation *rs = (ReferenceStation *) getStation (rec.header.reference_station, in_context);
      s = new SubordinateStation (rs, so);
    }
    break;

  default:
    assert (0);
  }

  s->harmonicsFileName = myfilename;
  s->TCDRecordNumber = TCDRecordNumber;
  s->name = rec.header.name;
  s->timeZone = get_tzfile (rec.header.tzfile);
  s->coordinates.lat (rec.header.latitude);
  s->coordinates.lng (rec.header.longitude);
  if (rec.notes[0])
    s->note = rec.notes;
  if (rec.min_direction != 361) {
    Angle::qualifier tq = Angle::NONE;
    double tv = (double) rec.min_direction;
    Dstr value = (char *) get_dir_units (rec.direction_units);
    if (value == "degrees true") tq = Angle::TRU;
    s->minCurrentAngle = NullableAngle (Angle::DEGREES, tq, tv);
  }
  if (rec.max_direction != 361) {
    Angle::qualifier tq = Angle::NONE;
    double tv = (double) rec.max_direction;
    Dstr value = (char *) get_dir_units (rec.direction_units);
    if (value == "degrees true") tq = Angle::TRU;
    s->maxCurrentAngle = NullableAngle (Angle::DEGREES, tq, tv);
  }
  add_metadata (myfilename, *s, rec);

  // Append legalese to name.
  if (rec.legalese) {
    s->name += " - ";
    s->name += get_legalese(rec.legalese);
  }

  return s;
}
