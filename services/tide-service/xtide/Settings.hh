// $Id: Settings.hh,v 1.6 2004/04/19 20:16:55 flaterco Exp $
/*  Settings  XTide global settings

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Settings: public ConfigurablesMap {
public:

  // Default constructor initializes map to config.hh defaults.  This
  // is highly desirable even if you intend to call nullify()
  // immediately.  An empty map tells you nothing about what settings
  // are even available.  A nulled-out map gives you all the metadata
  // (just no data).
  Settings();

  // Null out all settings.
  void nullify();

  // Supersede by ~/.xtide.xml contents.
  void applyUserDefaults ();

  // Supersede by command line.
  void applyCommandLine (int argc, char **argv);
  void applyCommandLine (); // Must call other one first.

  // Supersede by X resources.  We avoid linkage difficulties in the
  // X-less case by taking a function as argument.
  void applyXResources (int (&getResource_in) (Dstr &name_in, Dstr &val_out));
  void applyXResources (); // Must call other one first.

  // Supersede by whatever.  This is used by the control panel to apply
  // settings before they are saved.
  void apply (const Settings &settings_in);

  // Write to ~/.xtide.xml
  void save();

  // Ignore this.
  struct arglist {Dstr svitch; Dstr arg; arglist *next;};

protected:

  // Constraint checking.
  // Culprit -- who to blame in error message if value is bogus.
  // If null, no error is printed; instead, function returns true.
  int checkboolean   (char *culprit, char val);
  int checkposint    (char *culprit, int val);
  int checkposdouble (char *culprit, double val);
  int checkgldouble  (char *culprit, double val);
  int checkmode      (char *culprit, char val);
  int checkformat    (char *culprit, char val);
  int checkcolor     (char *culprit, char *val);
  int checkunit      (char *culprit, char *val);
  int checktext      (char *culprit, char *val);
  int checkstrftime  (char *culprit, char *val);
  int checkcolor     (char *culprit, const Dstr &val);
  int checkunit      (char *culprit, const Dstr &val);
  int checktext      (char *culprit, const Dstr &val);
  int checkstrftime  (char *culprit, const Dstr &val);
  int checkConfigurable (char *culprit, Configurable &val);

  // Conversions.
  int checknumber    (char *culprit, char *val);
  // These furthermore enforce that the string should be of length
  // one.
  int checkboolean   (char *culprit, char *val);
  int checkmode      (char *culprit, char *val);
  int checkformat    (char *culprit, char *val);

  // Parsing with constraint checking
  double getdouble (char *culprit, char *val);
  double getdouble (char *culprit, const Dstr &val);
  int getint (char *culprit, char *val);
  double getposdouble (char *culprit, char *val);
  double getgldouble (char *culprit, char *val);
  unsigned getposint (char *culprit, char *val);
  double getposdouble (char *culprit, const Dstr &val);
  double getgldouble (char *culprit, const Dstr &val);
  unsigned getposint (char *culprit, const Dstr &val);

  // Crufty stuff for command line parsing

  // The goal is to disambiguate the argument string.
  // Inputs:
  //   int argc, char **argv   The usual
  //   int argi                Index to the argument now being looked at
  // Returns:
  //   arglist*  Disambiguated argument list if valid.  Null if not.
  arglist *disambiguate (int argc, char **argv, int argi);

  // The goal is still to disambiguate the argument string.
  // char *argii       Index to the current character position in argv[argi].
  // Interpretation t  The "type" of argument we expect to find there.
  arglist *check_arg (int argc, char **argv, int argi, char *argii,
    Configurable::Interpretation t);

  // Search command line.  Returns NULL if not found.
  char *findarg (char *arg, arglist *args);
};
