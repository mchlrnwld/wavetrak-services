// $Id: xxStep.hh,v 1.1 2002/04/29 15:10:43 flaterco Exp $
/*  xxStep  Get a step from the user.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxStep: public xxWindow {
  friend void xxStepCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxStepCancelCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void stephourcallback (void *cbdata);
  friend void stepminutecallback (void *cbdata);

public:
  xxStep (xxTideContext *in_xtidecontext, xxContext *context,
     void (*in_stepback) (Interval step,
     void *in_ptr), void *in_ptr, Interval init);
  ~xxStep();
  void dismiss();

protected:
  void (*stepcallback) (Interval step, void *in_ptr);
  void *ptr;
  xxContext *gobutton, *cancelbutton, *helplabel1, *helplabel2,
    *spacelabel1;
  xxMultiChoice *hourchoice, *minutechoice;
  unsigned hours, minutes;
};
