/*  xxMarkLevel  Get a PV from the user.
    Last modified 1998-05-03

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxMarkLevelCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxMarkLevel *e = (xxMarkLevel *)client_data;

  Dstr num (XawDialogGetValueString (e->mydialog->manager));

  if (num.length() == 0)
    (*(e->marklevelcallback)) (NULL, e->ptr);
  else if (num.strchr ('\n') != -1 ||
           num.strchr ('\r') != -1 ||
           num.strchr (' ') != -1) {
    Dstr details ("Numbers aren't supposed to contain whitespace.  You entered '");
    details += num;
    details += "'.";
    barf (NOT_A_NUMBER, details, 0);
  } else {
    double temp;
    if (sscanf (num.aschar(), "%lf", &temp) != 1) {
      Dstr details ("The offending input was '");
      details += num;
      details += "'.";
      barf (NOT_A_NUMBER, details, 0);
    } else
      (*(e->marklevelcallback)) (new PredictionValue(e->u, temp), e->ptr);
  }

  delete e;
}

static void
xxMarkLevelCancelCallback (Widget w, XtPointer client_data, XtPointer
call_data) {
  xxMarkLevel *e = (xxMarkLevel *)client_data;
  e->dismiss();
}

void
xxMarkLevel::dismiss() {
  delete this;
}

xxMarkLevel::~xxMarkLevel() {
  mypopup->unrealize();
  delete mydialog;
}

xxMarkLevel::xxMarkLevel (xxTideContext *in_xtidecontext, xxContext *context,
void (*in_marklevelcallback) (PredictionValue *marklevel, void *in_ptr),
void *in_ptr, PredictionValue::Unit in_u, char *init): xxWindow (
in_xtidecontext, context, 0, XtGrabExclusive) {
  assert (init);
  ptr = in_ptr;
  u = in_u;
  marklevelcallback = in_marklevelcallback;
  mypopup->setTitle ("Enter Mark Level");

  {
    Dstr temp;
    u.longname (temp);
    temp *= "Enter mark level in ";
    Arg dargs[4] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNlabel, (XtArgVal)temp.aschar()},
      {XtNvalue, (XtArgVal)init}
    };
    Widget dialogwidget = XtCreateManagedWidget ("",
      dialogWidgetClass, mypopup->manager, dargs, 4);
    mydialog = new xxContext (mypopup, dialogwidget);
  }

  XawDialogAddButton (mydialog->manager, "Go", xxMarkLevelCallback,
   (XtPointer)this);
  XawDialogAddButton (mydialog->manager, "Cancel", xxMarkLevelCancelCallback,
   (XtPointer)this);

  // Need to force the colors.
  Arg largs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (XtNameToWidget (mydialog->manager, "label"), largs, 2);
  XtSetValues (XtNameToWidget (mydialog->manager, "value"), largs, 2);
  Arg bargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (XtNameToWidget (mydialog->manager, "Go"), bargs, 2);
  XtSetValues (XtNameToWidget (mydialog->manager, "Cancel"), bargs, 2);

  mypopup->realize();
}
