// $Id: SubordinateStation.cc,v 1.21 2004/11/17 16:27:18 flaterco Exp $
/*  SubordinateStation  Implementation for offset station.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

SubordinateStation::SubordinateStation (ReferenceStation *rs,
HairyOffsets *offsets): Station (rs->context) {
  // Other fields are filled in in HarmonicsFile::getStation.
  constants = rs->constants;
  isCurrent = rs->isCurrent;
  isHydraulicCurrent = rs->isHydraulicCurrent;
  myUnits = rs->myUnits;
  origOffsets = offsets;
  residualOffsets = NULL;
  reduce_offsets();
  rs->constants = NULL;
  delete rs;
}

// If offsets are really messed up, it's possible that these could get
// reversed.  Too bad.

PredictionValue SubordinateStation::minLevel() const {
  PredictionValue pv = Station::minLevel();
  if (residualOffsets) {
    // Yet again...
    if (residualOffsets->min.levelAdd.Units() != pv.Units())
      residualOffsets->min.levelAdd.Units (pv.Units());
    pv = pv * residualOffsets->min.levelMultiply() +
              residualOffsets->min.levelAdd;
  }
  return pv;
}

PredictionValue SubordinateStation::maxLevel() const {
  PredictionValue pv = Station::maxLevel();
  if (residualOffsets) {
    // And again.
    if (residualOffsets->max.levelAdd.Units() != pv.Units())
      residualOffsets->max.levelAdd.Units (pv.Units());
    pv = pv * residualOffsets->max.levelMultiply() +
              residualOffsets->max.levelAdd;
  }
  return pv;
}

void SubordinateStation::reduce_offsets () {
  if (residualOffsets)
    delete residualOffsets;
  residualOffsets = NULL;
  assert (origOffsets);

  // origOffsets is never simple anymore.

  // Wherever possible, reduce hairy offsets to simple and incorporate
  // them into the constants.

  if (origOffsets->max == origOffsets->min && ((!isCurrent) || (
    (origOffsets->max.timeAdd == origOffsets->floodbegins ||
     origOffsets->floodbegins.isNull()) &&
    (origOffsets->max.timeAdd == origOffsets->ebbbegins ||
     origOffsets->ebbbegins.isNull()) &&
    // In the case of hydraulic currents, level multipliers cannot be
    // treated as simple and applied to the constants because of the
    // square root operation.
    (!isHydraulicCurrent || origOffsets->max.levelMultiply() == 1.0))))
    constants->setSimpleOffsets (origOffsets->max);
  else
    // There isn't any point in factoring out individual fields since
    // hairy offsets need to be processed in the prediction routine in
    // both cases.

    // If you were to factor out levelAdd and levelMultiply, you would
    // still need to avoid the pitfall of messing up the order of
    // operations.  For example, factoring out a levelAdd but not a
    // levelMultiply would yield incorrect results.

    residualOffsets = new HairyOffsets (*origOffsets);

  // Set minimumTimeOffset and maximumTimeOffset.
  if (residualOffsets) {
    minimumTimeOffset = maximumTimeOffset = residualOffsets->max.timeAdd;
    if (residualOffsets->min.timeAdd < minimumTimeOffset)
      minimumTimeOffset = residualOffsets->min.timeAdd;
    if (residualOffsets->min.timeAdd > maximumTimeOffset)
      maximumTimeOffset = residualOffsets->min.timeAdd;
    if (isCurrent) {
      if (!residualOffsets->floodbegins.isNull()) {
        if (residualOffsets->floodbegins < minimumTimeOffset)
	  minimumTimeOffset = residualOffsets->floodbegins;
        if (residualOffsets->floodbegins > maximumTimeOffset)
	  maximumTimeOffset = residualOffsets->floodbegins;
      }
      if (!residualOffsets->ebbbegins.isNull()) {
        if (residualOffsets->ebbbegins < minimumTimeOffset)
	  minimumTimeOffset = residualOffsets->ebbbegins;
        if (residualOffsets->ebbbegins > maximumTimeOffset)
	  maximumTimeOffset = residualOffsets->ebbbegins;
      }
    }
  }
}

SubordinateStation::~SubordinateStation() {
  if (constants)
    delete constants;
  if (markLevel)
    delete markLevel;
  if (residualOffsets)
    delete residualOffsets;
  if (origOffsets)
    delete origOffsets;
}

// New and improved, 2004-10-21.
PredictionValue SubordinateStation::predictTideLevel (Timestamp tm) {
  // Get rid of the trivial cases.
  if (!residualOffsets)
    return Station::predictTideLevel (tm);

  // If units changed, trigger a refresh.
  if (cache_units != myUnits) {
    subleftt.make_null();
    cache_units = myUnits;
  }

  // If we are outside the cached bracket, trigger a refresh.
  if (!subleftt.isNull())
    if (tm < subleftt || tm >= subrightt)
      subleftt.make_null();

  // Refresh cached bracket?
  if (subleftt.isNull()) {

    // Since we have no way of knowing whether tm will follow a
    // contiguous range or jump around, there's no point preserving
    // the organizer beyond a single bracket.  This puts some noise in
    // the results since everything will come back slightly different
    // even when tm just walks out of the bracket.
    TideEventsOrganizer teo;

    // Initialize organizer with a starting range.  (Yes, this is
    // necessary.)  Unlikely that a real station could go 48 hours
    // with no tide events, but detect and correct if it does.
    Interval delta;
    for (delta = Interval(DAYSECONDS); teo.empty(); delta *= 2U)
      predictTideEvents (tm - delta, tm + delta, teo, KNOWNTIDEEVENTS);

    while (subleftt.isNull()) {

      // If there are multiple events with the same tm, it doesn't
      // matter which one we pick, because they will all be different
      // next time anyway.

      // upper_bound: first element whose key is greater than tm.
      TideEventsIterator right = teo.upper_bound (tm.timet());
      delta = Interval(DAYSECONDS);
      while (right == teo.end()) {
	// Need more future
	extendRange (teo, forward, delta, KNOWNTIDEEVENTS);
        delta *= 2U;
	right = teo.upper_bound (tm.timet());
      }

      // lower_bound: first element whose key is not less than tm.
      TideEventsIterator left = teo.lower_bound (tm.timet());
      // If upper bound existed, this must also exist.
      assert (left != teo.end());
      // But what we usually want is the previous one.
      if (left->second.tm > tm) {
        int recycle = 0;
        delta = Interval(DAYSECONDS);
	while (left == teo.begin()) {
	  // Need more past
	  extendRange (teo, backward, delta, KNOWNTIDEEVENTS);
          delta *= 2U;
          left = teo.lower_bound (tm.timet());
          recycle = 1;
	}

        // It's possible, if the bracket was skewed way off, that
        // extending the range backward could change the result for
        // right.
        if (recycle)
          continue;

	left--;
      }

      // Populate the cached bracket.
      subleftt = left->second.tm;
      subleftp = left->second.pv;
      subrightt = right->second.tm;
      subrightp = right->second.pv;
      uncleftt = left->second.uncorrected_tm;
      uncleftp = left->second.uncorrected_pv;
      uncrightt = right->second.uncorrected_tm;
      uncrightp = right->second.uncorrected_pv;
    }
  }
  assert (subleftt <= tm && tm < subrightt);

  // All manner of pathologies are possible.  We might have skipped
  // over some conflicting events with the same tm.  uncleftt might be
  // later than uncrightt.  The left and right events might be the
  // same type.  It doesn't matter.  Just do it.

  // No, wait, there is one exception.
  // If this happens, fake it.
  if (uncrightp == uncleftp)
    return subleftp + (subrightp - subleftp) *
      ((tm - subleftt) / (subrightt - subleftt));

  // Otherwise, do it.
  return subleftp+(subrightp-subleftp)*((Station::predictTideLevel(uncleftt+
    (uncrightt-uncleftt)*((tm-subleftt)/(subrightt-subleftt)))-uncleftp)/
    (uncrightp-uncleftp));
}

int SubordinateStation::is_reference_station() {
  return 0;
}

int SubordinateStation::have_residuals() {
  return (residualOffsets != NULL);
}

int SubordinateStation::have_floodbegins() {
  if (residualOffsets)
    if (residualOffsets->floodbegins.isNull())
      return 0;
  return 1;
}

int SubordinateStation::have_ebbbegins() {
  if (residualOffsets)
    if (residualOffsets->ebbbegins.isNull())
      return 0;
  return 1;
}

// Fill in PredictionValue and possibly apply corrections.
void SubordinateStation::finishTideEvent (TideEvent &te) {
  if (!te.isSunMoonEvent()) {
    if (te.etype == TideEvent::rawreading)
      te.pv = predictTideLevel (te.tm);
    else {
      te.pv = Station::predictTideLevel (te.tm);

      if (residualOffsets) {

        // Needed in SubordinateStation::predictTideLevel and in
        // Station::predictTideEvents.
        te.uncorrected_tm = te.tm;
        te.uncorrected_pv = te.pv;

	// It may be inefficient to do this here, but it's a lot more robust.
	if (residualOffsets->max.levelAdd.Units() != te.pv.Units())
	  residualOffsets->max.levelAdd.Units (te.pv.Units());
	if (residualOffsets->min.levelAdd.Units() != te.pv.Units())
	  residualOffsets->min.levelAdd.Units (te.pv.Units());

	switch (te.etype) {
	case TideEvent::max:
          if (te.isMinCurrentEvent()) {
            // Handling of min currents is questionable; see
            // http://www.flaterco.com/xtide/mincurrents.html
            if (residualOffsets->ebbbegins.isNull())
  	      te.tm += residualOffsets->min.timeAdd;
            else
  	      te.tm += residualOffsets->ebbbegins;
	    te.pv = te.pv * residualOffsets->min.levelMultiply()
			  + residualOffsets->min.levelAdd;
          } else {
  	    te.tm += residualOffsets->max.timeAdd;
	    te.pv = te.pv * residualOffsets->max.levelMultiply()
			  + residualOffsets->max.levelAdd;
          }
	  break;
	case TideEvent::min:
          if (te.isMinCurrentEvent()) {
            // Handling of min currents is questionable; see
            // http://www.flaterco.com/xtide/mincurrents.html
            if (residualOffsets->floodbegins.isNull())
    	      te.tm += residualOffsets->max.timeAdd;
            else
  	      te.tm += residualOffsets->floodbegins;
	    te.pv = te.pv * residualOffsets->max.levelMultiply()
			  + residualOffsets->max.levelAdd;
          } else {
	    te.tm += residualOffsets->min.timeAdd;
	    te.pv = te.pv * residualOffsets->min.levelMultiply()
			  + residualOffsets->min.levelAdd;
          }
	  break;

	// uncorrected_tm is bogus for mark and slack crossings but
	// won't be used.

	case TideEvent::slackrise:
          if (!residualOffsets->floodbegins.isNull())
  	    te.tm += residualOffsets->floodbegins;
          else
            // tm was fixed in Station::find_mark_crossing
            // (So hopefully this will be zero)
            te.pv = predictTideLevel (te.tm);
	  break;
	case TideEvent::slackfall:
          if (!residualOffsets->ebbbegins.isNull())
	    te.tm += residualOffsets->ebbbegins;
          else
            // tm was fixed in Station::find_mark_crossing
            // (So hopefully this will be zero)
            te.pv = predictTideLevel (te.tm);
	  break;
	case TideEvent::markrise:
	case TideEvent::markfall:
          // tm was fixed in Station::find_mark_crossing..
          te.pv = predictTideLevel (te.tm);
          break;
	default:
	  assert (0);
	}
      }
    }
  }
}
