// $Id: xxGlobe.hh,v 1.3 2004/10/26 18:40:52 flaterco Exp $
/*  xxGlobe   Location chooser using Orthographic Projection.

    There is some duplicated code between xxGlobe and xxMap.  However,
    they are sufficiently different that I think complete
    encapsulation is the cleanest approach.  -- DWF

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

class xxGlobe: public xxWindow {
public:
  xxGlobe (xxContext *context, StationIndex &in_stationIndex,
    xxTideContext *in_tidecontext);
  ~xxGlobe ();
  void global_redraw();
  void dismiss();

protected:
  friend void xxGlobeButtonPressEventHandler (Widget w, XtPointer client_data,
			 XEvent *event, Boolean *continue_dispatch);
  friend void xxGlobeKeyboardEventHandler (Widget w, XtPointer client_data,
			 XEvent *event, Boolean *continue_dispatch);
  friend void xxGlobePointerMotionEventHandler (Widget w, 
                                                XtPointer client_data,
                                                XEvent *event, 
                                                Boolean *continue_dispatch);
  friend void xxGlobeListAllCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxGlobeZoomOutCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxGlobeFlatCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxGlobeHelpCallback (Widget w, XtPointer client_data,
    XtPointer call_data);

  Pixmap globepixmap;
  StationIndex *stationIndex;
  xxContext *picture, *dismissbutton, *listAllButton, *helpbutton,
    *zoomoutbutton, *lattext, *lontext, *flatbutton;
  xxLocationList *locationlist;
  unsigned long size;
  long xorigin, yorigin, blastx, blasty, blastflag, last_x, last_y;
  Dimension internalHeight, internalWidth;
  double center_longitude, center_latitude;

  // projectCoordinates does an orthographic projection of size size
  // (declared below).  The version accepting doubles differs in that
  // (0.0,0.0) is not treated as a null.
  // Returns:
  //   1 = the point is on this side; results valid.
  //   0 = the point is on the other side; ignore results.
  int projectCoordinates (Coordinates c_in, long *x, long *y);
  int projectCoordinates (double lat, double lng, long *x, long *y);

  // translateCoordinates calls projectCoordinates then further cooks the
  // result to map it into the current viewport.  The version accepting
  // doubles differs in that (0.0,0.0) is not treated as a null.
  // Returns:
  //   1 = the point may be visible; draw it.
  //   0 = the point is invisible; ignore results.
  int translateCoordinates (Coordinates c_in, long *x, long *y);
  int translateCoordinates (double lat, double lng, long *x, long *y);

  // Returns:
  //   1 = the point is on the globe somewhere; lat and lng valid.
  //   0 = you missed; ignore lat and lng.
  int untranslateCoordinates (long x, long y, double &lat, double &lng);

  void update_position (long x, long y);
  void blast (long x, long y);
  void redrawGlobe ();
  void draw_coastlines();
  void draw_gridlines();

  // Test for coordinates within reasonable boundaries to avoid
  // overflowing X11 (16-bit ints).
  int inrange (long x, long y);
};
