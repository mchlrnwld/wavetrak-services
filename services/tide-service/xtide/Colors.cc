// $Id: Colors.cc,v 1.4 2004/04/19 20:16:55 flaterco Exp $
/*  Colors  Manage XTide colors without X.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"
#include "rgb.hh"

char *colorarg[numcolors] = {
  "bg", "fg", "mc", "bc", "dc", "nc", "fc", "ec", "Dc", "Mc", "tc"
};

Colors::Colors (const Settings &settings) {
  int a;
  for (a=0; a<numcolors; a++)
    // This is not const: settings[colorarg[a]].s
    parseColor (settings.find(colorarg[a])->second.s, cmap[a][0], cmap[a][1], cmap[a][2]);
}

int
Colors::parseColor (const Dstr &colorname, unsigned char &r, unsigned char &g,
			 unsigned char &b, int fatal) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
  cerr << "Colors::parseColor asked to parse " << colorname << endl;
#endif
  unsigned int rr=0, gg=0, bb=0;
  if (sscanf (colorname.aschar(), "rgb:%x/%x/%x", &rr, &gg, &bb) == 3) {
    r = (unsigned char)rr;
    g = (unsigned char)gg;
    b = (unsigned char)bb;
  // Kludge for default fg and bg colors under the CDE
  } else if (colorname[0] == '#' && colorname.length() == 13) {
    char temp[3];
    temp[2] = '\0';
    temp[0] = colorname[1];
    temp[1] = colorname[2];
    sscanf (temp, "%x", &rr);
    temp[0] = colorname[5];
    temp[1] = colorname[6];
    sscanf (temp, "%x", &gg);
    temp[0] = colorname[9];
    temp[1] = colorname[10];
    sscanf (temp, "%x", &bb);
    r = (unsigned char)rr;
    g = (unsigned char)gg;
    b = (unsigned char)bb;
  // Kludge for default fg and bg colors under Debian
  } else if (colorname[0] == '#' && colorname.length() == 7) {
    char temp[3];
    temp[2] = '\0';
    temp[0] = colorname[1];
    temp[1] = colorname[2];
    sscanf (temp, "%x", &rr);
    temp[0] = colorname[3];
    temp[1] = colorname[4];
    sscanf (temp, "%x", &gg);
    temp[0] = colorname[5];
    temp[1] = colorname[6];
    sscanf (temp, "%x", &bb);
    r = (unsigned char)rr;
    g = (unsigned char)gg;
    b = (unsigned char)bb;
  } else {
    rr = 0;
    while (rgbtxt[rr].name) {
      if (dstrcasecmp (colorname, rgbtxt[rr].name) == 0) {
        r = rgbtxt[rr].r;
        g = rgbtxt[rr].g;
        b = rgbtxt[rr].b;
        return 1;
      }
      rr++;
    }
    Dstr details ("The offending color spec was ");
    details += colorname;
    details += ".";
    barf (BADCOLORSPEC, details, fatal);
    return 0;
  }
  return 1;
}

int
Colors::parseColor (char *colorname, unsigned char &r, unsigned char &g,
		    unsigned char &b, int fatal) {
  assert (colorname);
  Dstr temp (colorname);
  return parseColor (temp, r, g, b, fatal);
}
