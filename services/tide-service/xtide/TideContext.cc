// $Id: TideContext.cc,v 1.31 2004/11/17 16:27:18 flaterco Exp $
/*  TideContext  In lieu of global variables and functions.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

void version_string (Dstr &text_out) {
  text_out = "XTide ";
  text_out += VERSION;
  if (PATCHLEVEL) {
    text_out += ".";
    text_out += PATCHLEVEL;
  }
#ifdef VSTATUS
  text_out += " ";
  text_out += VSTATUS;
#endif
}

TideContext::TideContext (Colors *in_colors, Settings *in_settings) {
  colors = in_colors;
  settings = in_settings;

  // Get HFILE_PATH from environment or /etc/xtide.conf and create
  // persistent HarmonicsPath
  Dstr hfile_path (getenv ("HFILE_PATH"));
  if (hfile_path.isNull()) {
    FILE *configfile;
    if ((configfile = fopen ("/etc/xtide.conf", "r"))) {
      hfile_path.getline (configfile);
      fclose (configfile);
    }
  }
  if (hfile_path.isNull())
    hfile_path = "harmonics.tcd";
  harmonicsPath = new HarmonicsPath (hfile_path);

  homedir = getenv ("HOME");
  if (homedir.isNull())
    barf (NOHOMEDIR);

  disabledisclaimerfile = homedir;
  disabledisclaimerfile += "/.disableXTidedisclaimer";

  // Get this only when needed.
  myStationIndex = NULL;
}

TideContext::~TideContext () {
  if (myStationIndex)
    delete myStationIndex;
  delete harmonicsPath;
  delete colors;
  delete settings;
}

// THIS CODE IS DUPLICATED IN XXTIDECONTEXT!  The duplication is needed
// to embed XEvent polling in xxTideContext.
void TideContext::doHarmonicsFile (Dstr &fname) {
  Dstr msg ("Indexing ");
  msg += fname;
  msg += "...";
  log (msg, LOG_NOTICE);
  HarmonicsFile h (fname, settings);
  myStationIndex->addHfileId (h.version_string);
  StationRef *s;
  while ((s = h.getNextStationRef()))
    myStationIndex->add (s);
}

// THIS CODE IS DUPLICATED IN XXTIDECONTEXT!  The duplication is needed
// to embed XEvent polling in xxTideContext.
StationIndex *TideContext::stationIndex () {
  if (!myStationIndex) {
    myStationIndex = new StationIndex();
    for (unsigned i=0; i<harmonicsPath->length(); i++) {
      struct stat s;
      if (stat ((*harmonicsPath)[i].aschar(), &s) == 0) {
        if (S_ISDIR (s.st_mode)) {
          Dstr dname ((*harmonicsPath)[i]);
          dname += '/';
          DIR *dirp = opendir (dname.aschar());
          if (!dirp)
            xperror (dname.aschar());
          else {
            struct dirent *dp;
	    for (dp = readdir(dirp); dp != NULL; dp = readdir(dirp)) {
              Dstr fname (dp->d_name);
	      if (fname[0] == '.') // Skip all hidden files
		continue;
	      else {
                fname *= dname;
                // DO NOT FREE THIS.  It gets pointed to by every station.
                Dstr *qfname = new Dstr(fname);
                doHarmonicsFile (*qfname);
              }
	    }
	    closedir(dirp);
          }
        } else
          doHarmonicsFile ((*harmonicsPath)[i]);
      } else
        xperror ((*harmonicsPath)[i].aschar());
    }
    if (myStationIndex->length() == 0)
      barf (NO_HFILE_PATH);
    myStationIndex->qsort();
  }
  return myStationIndex;
}

void TideContext::plainMode (Station *station, Timestamp start_tm,
Dstr &text_out, char form) {
  Timestamp end_tm = start_tm + Interval(defpredictinterval);
  plainMode (station, start_tm, end_tm, text_out, form);
}

// Legal forms are c, h, i, or t (although c does nothing)
void TideContext::text_boilerplate (Station *station, Dstr &text_out,
char form) {
  text_out = (char *)NULL;
  if (form == 'c')
    return;
  assert (form == 'h' || form == 'i' || form == 't');

  if (form == 'i') {

    // RFC2445 doesn't allow putting very much outside of the VEVENTs.
    // This makes sense considering that a calendaring tool is only
    // equipped to display metadata corresponding to specific events.

    // RFC2445 does clearly specify CRLF (CRNL) line discipline.

    text_out += "BEGIN:VCALENDAR\r\n\
VERSION:2.0\r\n\
PRODID:";
    // ISO 9070 compliance not mandatory.
    Dstr ver;
    version_string (ver);
    text_out += ver;
    text_out += "\r\n\
CALSCALE:GREGORIAN\r\n\
METHOD:PUBLISH\r\n";
  } else {

    if (form == 'h')
      text_out += "<h3>";
    text_out += station->name;
    if (form == 'h')
      text_out += "<br>";
    text_out += '\n';
    if (station->coordinates.isNull())
      text_out += "Coordinates unknown\n";
    else {
      Dstr t;
      station->coordinates.print (t);
      text_out += t;
      text_out += '\n';
    }

    // When known, append the direction of currents.  (The offending attributes
    // should be null if it's not a current station.)
    if (!(station->maxCurrentAngle.isNull())) {
      if (form == 'h')
	text_out += "<br>";
      text_out += "Flood direction ";
      Dstr tmpbuf;
      station->maxCurrentAngle.ppqdeg (0, tmpbuf);
      text_out += tmpbuf;
      text_out += '\n';
    }
    if (!(station->minCurrentAngle.isNull())) {
      if (form == 'h')
	text_out += "<br>";
      text_out += "Ebb direction ";
      Dstr tmpbuf;
      station->minCurrentAngle.ppqdeg (0, tmpbuf);
      text_out += tmpbuf;
      text_out += '\n';
    }

    // Similarly for notes
    if (!(station->note.isNull())) {
      if (form == 'h')
	text_out += "<br>Note:&nbsp; ";
      else
	text_out += "Note:  ";
      text_out += station->note;
      text_out += '\n';
    }

    if (form == 'h')
      text_out += "</h3>";
    text_out += '\n';
  }
}

// Generate plain mode output.
// Legal forms are c (CSV), t (text) or i (iCalendar).
//
// iCalendar format output is actually produced by plainMode.  From
// an engineering perspective this makes perfect sense.  But from a
// usability perspective, iCalendar output is a calendar and ought
// to appear in calendar mode.  So calendarMode falls through to
// plainMode when i format is chosen.
void TideContext::plainMode (Station *station, Timestamp start_tm,
Timestamp end_tm, Dstr &text_out, char form) {
  text_boilerplate (station, text_out, form);
  TideEventsOrganizer organizer;
  station->predictTideEvents (start_tm, end_tm, organizer);
  TideEventsIterator it = organizer.begin();
  while (it != organizer.end()) {
    Dstr line;
    it->second.print (line, form, 'p', *station);
    text_out += line;
    text_out += '\n';
    it++;
  }
  if (form == 'i')
    text_out += "END:VCALENDAR\r\n";
}

// Legal forms are c, h, i, or t
void TideContext::calendarMode (Station *station, Timestamp start_tm,
Timestamp end_tm, Dstr &text_out, char form, int oldcal) {
  assert ((form == 'c' && !oldcal) || form == 'h' || form == 'i' || form == 't');

  // iCalendar format output is actually produced by plainMode.  From
  // an engineering perspective this makes perfect sense.  But from a
  // usability perspective, iCalendar output is a calendar and ought
  // to appear in calendar mode.  So calendarMode falls through to
  // plainMode when i format is chosen.
  if (form == 'i') {
    plainMode (station, start_tm, end_tm, text_out, form);

  } else {
    text_boilerplate (station, text_out, form);
    Calendar cal (station, start_tm, end_tm);
    Dstr temp;
    cal.print (temp, form, oldcal);
    text_out += temp;
  }
}

void TideContext::bannerMode (Station *station, Timestamp start_tm,
Timestamp end_tm, Dstr &text_out) {
  text_boilerplate (station, text_out, 't');
  Banner *banner = bannerFactory (station, (*settings)["tw"].u, start_tm, end_tm);
  Dstr temp;
  banner->drawTides (station, start_tm);
  banner->writeAsText (temp);
  text_out += temp;
  delete banner;
}

void TideContext::statsMode (Station *station, Timestamp start_tm,
Timestamp end_tm, Dstr &text_out) {
  text_boilerplate (station, text_out, 't');
  PredictionValue maxl = station->maxLevel();
  PredictionValue minl = station->minLevel();
  if (minl >= maxl)
    barf (ABSURD_OFFSETS, station->name);
  PredictionValue meanl = (maxl + minl) / 2.0;
  Dstr temp;
  text_out += "Mathematical upper bound: ";
  maxl.print (temp);
  text_out += temp;
  text_out += "\nMathematical lower bound: ";
  minl.print (temp);
  text_out += temp;
  text_out += "\n";
  if (station->isCurrent)
    text_out += "Permanent current: ";
  else
    text_out += "Mean Astronomical Tide: ";
  meanl.print (temp);
  text_out += temp;
  text_out += "\n\n";

  int first = 1;
  TideEventsOrganizer organizer;
  station->predictTideEvents (start_tm, end_tm, organizer, MAXMIN);
  TideEventsIterator it = organizer.begin();
  Timestamp maxt, mint;
  while (it != organizer.end()) {
    TideEvent &te = it->second;
    assert (!te.isSunMoonEvent());
    if (first || (te.pv < minl)) {
      mint = te.tm;
      minl = te.pv;
    }
    if (first || (te.pv > maxl)) {
      maxt = te.tm;
      maxl = te.pv;
    }
    first = 0;
    it++;
  }

  Dstr timezone = station->timeZone;
  text_out += "Searched interval from ";
  start_tm.print (temp, timezone, settings);
  text_out += temp;
  text_out += " to ";
  end_tm.print (temp, timezone, settings);
  text_out += temp;
  text_out += "\n";
  if (!first) {
    text_out += "Max was ";
    maxl.print (temp);
    text_out += temp;
    text_out += " at ";
    maxt.print (temp, timezone, settings);
    text_out += temp;
    text_out += "\n";
    text_out += "Min was ";
    minl.print (temp);
    text_out += temp;
    text_out += " at ";
    mint.print (temp, timezone, settings);
    text_out += temp;
    text_out += "\n";
  } else {
    text_out += "Found no tide events.\n";
  }
}

// Legal modes are m and r.
// Legal forms are c and t.
// Step should be set on station.
void
TideContext::rareModes (Station *station, Timestamp start_tm, Timestamp end_tm,
Dstr &text_out, char mode, char form) {
  assert (form == 't' || form == 'c');
  assert (mode == 'r' || mode == 'm');
  text_out = (char *)NULL;

  TideEventsOrganizer organizer;
  station->predictRawEvents (start_tm, end_tm, organizer);
  TideEventsIterator it = organizer.begin();
  while (it != organizer.end()) {
    Dstr line;
    it->second.print (line, form, mode, *station);
    text_out += line;
    text_out += '\n';
    it++;
  }
}

void TideContext::listModePlain (Dstr &text_out) {
  unsigned tw = (*settings)["tw"].u;
  text_out = "Location list generated ";
  Timestamp now ((time_t)(time(NULL)));
  Dstr tempnow, tz;
  tz = "UTC0";
  now.print (tempnow, tz, settings);
  text_out += tempnow;
  text_out += "\n\n";
  int namewidth = (int)tw - 28;
  if (namewidth < 10)
    namewidth = 10;
  StationIndex *si = stationIndex();
  char fmt[80];
  sprintf (fmt, "%%-%d.%ds %%-3.3s %%-23.23s\n", namewidth, namewidth);
  char *buf = (char *) malloc (tw + 30);
  for (unsigned i=0; i<si->length(); i++) {
    Dstr styp, c;
    if ((*si)[i]->isReferenceStation)
      styp = "Ref";
    else
      styp = "Sub";
    (*si)[i]->coordinates.print (c, 1);
    sprintf (buf, fmt, (*si)[i]->name.aschar(), styp.aschar(), c.aschar());
    text_out += buf;
  }
  free (buf);
}

void
TideContext::startLocListHTML (Dstr &d) {
  d += "<p><table>\n<tr><th>Location</th><th>Type</th>\n\
<th>Coordinates</th></tr>";
}

void
TideContext::endLocListHTML (Dstr &d) {
  d += "</table></p>\n";
}

void
TideContext::listLocationHTML (Dstr &d, StationRef *sr) {
  assert (sr);
  d += "<tr><td>";
  d += sr->name;
  d += "</td><td>";
  if (sr->isReferenceStation)
    d += "Ref";
  else
    d += "Sub";
  d += "</td><td>";
  Dstr tempc;
  sr->coordinates.print (tempc);
  d += tempc;
  d += "</td></tr>\n";
}

void TideContext::listModeHTML (Dstr &text_out) {
  text_out = "<p> Location list generated ";
  Timestamp now ((time_t)(time(NULL)));
  Dstr tempnow, tz;
  tz = "UTC0";
  now.print (tempnow, tz, settings);
  text_out += tempnow;
  text_out += "</p>\n\n";
  StationIndex *si = stationIndex();
  startLocListHTML (text_out);
  for (unsigned i=0; i<si->length(); i++) {
    listLocationHTML (text_out, (*si)[i]);
    if ((i % maxhtmltablen == 0) && (i != 0)) {
      endLocListHTML (text_out);
      startLocListHTML (text_out);
    }
  }
  endLocListHTML (text_out);
}
