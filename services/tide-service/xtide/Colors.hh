// $Id: Colors.hh,v 1.2 2004/04/19 20:16:55 flaterco Exp $
/*  Colors  Manage XTide colors without X.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Settings;

class Colors {
public:
  Colors (const Settings &settings);

#define numcolors 11
  enum colorchoice {background=0, foreground=1, mark=2, button=3,
    daytime=4, nighttime=5, flood=6, ebb=7, datum=8, msl=9, twilight=10};

  // These will be set to the parsed equivalents of the selected colors.
  // unsigned char cmap[enum colorchoice, 3];
  unsigned char cmap[numcolors][3];

  // This is an X-less implementation of XParseColor.  It understands
  // the standard color names from rgb.txt and colors of the form
  // rgb:hh/hh/hh (24-bit color specs only).
  // Set fatal to determine whether an unparsed color is a fatal error
  // or not.  Returns true on success.
  int parseColor (const Dstr &colorname, unsigned char &r, unsigned char &g,
    unsigned char &b, int fatal = 1);
  int parseColor (char *colorname, unsigned char &r, unsigned char &g,
    unsigned char &b, int fatal = 1);
};

extern char *colorarg[numcolors];
