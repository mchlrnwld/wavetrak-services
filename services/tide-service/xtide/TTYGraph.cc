// $Id: TTYGraph.cc,v 1.3 2004/04/08 14:25:04 flaterco Exp $
/*  TTYGraph  Graph implemented on dumb terminal

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

TTYGraph::TTYGraph (unsigned xsize, unsigned ysize): Graph (xsize, ysize) {
  assert (xsize >= minttywidth && ysize >= minttyheight);
  tty = (char *) malloc (xsize * ysize);
}

TTYGraph::~TTYGraph() {
  if (tty)
    free (tty);
}

unsigned TTYGraph::fontWidth() {
  return 1;
}

unsigned TTYGraph::fontHeight() {
  return 1;
}

unsigned TTYGraph::fontMargin() {
  return 0;
}

unsigned TTYGraph::depthLabelLeftMargin() {
  return 0;
}

unsigned TTYGraph::depthLabelRightMargin() {
  return 1;
}

unsigned TTYGraph::depthLineVerticalMargin() {
  return 0;
}

unsigned TTYGraph::hourTickLen() {
  return 1;
}

void TTYGraph::setPixel (int x, int y, Colors::colorchoice c) {
  setPixel (x, y, '*');
}

void TTYGraph::setPixel (int x, int y, char c) {
  if (x < 0 || x >= (int)xsize() || y < 0 || y >= (int)ysize())
    return;
  tty[y * xsize() + x] = c;
}

void TTYGraph::setPixel (int x, int y, Colors::colorchoice c, double saturation) {
  Graph::setPixel (x, y, c, saturation);
}

void
TTYGraph::drawHorizontalLine (int xlo, int xhi, int y,
			      Colors::colorchoice c) {
  int i;
  for (i=xlo; i<=xhi; i++)
    setPixel (i, y, '-');
}

void TTYGraph::drawHourTick (int x, Colors::colorchoice c) {
  setPixel (x, ysize()-1, '|');
}

void
TTYGraph::drawString (int x, int y, const Dstr &s) {
  for (unsigned a=0; a<s.length(); a++)
    setPixel (x+a, y, s[a]);
}

void TTYGraph::writeAsText (Dstr &text_out) {
  text_out = (char *)NULL;
  char *buf = (char *)malloc (xsize()+2);
  buf[xsize()] = '\n';
  buf[xsize()+1] = '\0';
  for (unsigned y=0; y<ysize(); y++) {
    strncpy (buf, &(tty[y*xsize()]), xsize());
    text_out += buf;
  }
  free (buf);
}

void TTYGraph::clearGraph (Timestamp start, Timestamp endt,
Interval increment, Station *station, TideEventsOrganizer &organizer) {
  memset (tty, ' ', xsize()*ysize());
}

void TTYGraph::drawX (int x, double y) {
  setPixel (x, (int)(y+0.5), '+');
}

double TTYGraph::aspectfudge() {
  return TTYaspectfudge;
}

unsigned
TTYGraph::startpos() {
  return TTYnposition;
}
