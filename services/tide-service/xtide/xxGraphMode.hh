/*  xxGraphMode  Tide graphs in a window.
    Last modified 1998-04-05

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxGraphMode: public xxDrawable {
  friend void xxGraphModeforwardCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxGraphModebackwardCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxGraphModeSaveCallback (Dstr &filename, void *in_ptr);
  friend void xxGraphModeResizeHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch);

public:
  xxGraphMode (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    Station *in_station);
  xxGraphMode (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    Station *in_station, Timestamp t_in);
  ~xxGraphMode();

  void redraw();
  void global_redraw();
  void dismiss();
  void help();
  void save();

  int is_graph();

protected:
  // Common code from multiple constructors.
  void construct ();

  xxPixmapGraph *graph;
  Dimension origwindowheight, origwindowwidth, origgraphheight,
    origgraphwidth, curgraphheight, curgraphwidth, curwindowheight,
    curwindowwidth;
  void draw();
  void clearbuf();
  xxContext *label, *forwardbutton, *backwardbutton;
};
