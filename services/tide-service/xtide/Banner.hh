// $Id: Banner.hh,v 1.3 2003/02/20 15:19:56 flaterco Exp $
/*  Banner  Graph printed sideways on tractor feed dot matrix printer

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Banner: public TTYGraph {
public:
  // This constructor wouldn't compile.
  // Banner (Station *station, unsigned xsize, Timestamp start_tm,
  //   Timestamp end_tm);

  // Don't use this directly, use bannerFactory.
  Banner (unsigned xsize, unsigned ysize);

  void writeAsText (Dstr &text_out);

protected:
  int is_banner();

  double aspectfudge();
  void drawHorizontalLine (int xlo, int xhi, int y,
				   Colors::colorchoice c);
  void drawHourTick (int x, Colors::colorchoice c);
  void labelHourTick (int x, Dstr &ts);
  void drawTimestampLabel (int x, int line, Dstr &ts);
  void drawStringSideways (int x, int y, const Dstr &s);
  void drawStringSidewaysOnLine (int x, int line, const Dstr &s);
  void drawTitleLine (Dstr &title);
};

Banner *bannerFactory (Station *station, unsigned xsize, Timestamp start_tm,
Timestamp end_tm);
