/*  xxAspect  Get an aspect from the user.
    Last modified 1998-03-29

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxAspect: public xxWindow {
  friend void xxAspectCallback (Widget w, XtPointer client_data,
    XtPointer call_data);

public:
  xxAspect (xxTideContext *in_xtidecontext, xxContext *context,
     void (*in_aspectback) (double aspect,
     void *in_ptr), void *in_ptr, char *init);
  ~xxAspect();
  void dismiss();

protected:
  xxContext *mydialog;
  void (*aspectcallback) (double aspect, void *in_ptr);
  void *ptr;
};
