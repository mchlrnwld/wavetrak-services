// $Id: RGBGraph.cc,v 1.2 2004/04/08 14:25:04 flaterco Exp $
/*  RGBGraph  Graph implemented as raw RGB image.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"
#include "glyphs.hh"

RGBGraph::RGBGraph (unsigned xsize, unsigned ysize, Colors *in_colors):
Graph (xsize, ysize) {
  assert (xsize >= mingwidth && ysize >= mingheight);
  rgb = (unsigned char *) malloc (xsize * ysize * 3);
  colors = in_colors;
}

RGBGraph::~RGBGraph() {
  if (rgb)
    free (rgb);
}

unsigned RGBGraph::fontWidth() {
  return 7;
}

unsigned RGBGraph::fontHeight() {
  // This used to be 11, then was increased to make room for Latin1
  // characters.
  return 12;
}

void RGBGraph::setPixel (int x, int y, Colors::colorchoice c) {
  if (x < 0 || x >= (int)xsize() || y < 0 || y >= (int)ysize())
    return;
  unsigned char *a = rgb + (y * xsize() + x) * 3;
  *(a++) = colors->cmap[c][0];
  *(a++) = colors->cmap[c][1];
  *(a) = colors->cmap[c][2];
}

void RGBGraph::setPixel (int x, int y, Colors::colorchoice c,
double saturation) {
  if (x < 0 || x >= (int)xsize() || y < 0 || y >= (int)ysize())
    return;
  unsigned char *a = rgb + (y * xsize() + x) * 3;
  *a = linterp (*a, colors->cmap[c][0], saturation);
  a++;
  *a = linterp (*a, colors->cmap[c][1], saturation);
  a++;
  *a = linterp (*a, colors->cmap[c][2], saturation);
}

void
RGBGraph::drawString (int x, int y, const Dstr &s) {
  for (unsigned a=0; a<s.length(); a++) {
    // It is interesting that casting s[a] to an unsigned int created
    // an off-by-one error.
    int base = s[a];
    if (base < 0)
      base += 256;
    assert (base >= 0 && base < 256);
    base *= fontWidth();
    for (unsigned int yl=0; yl<fontHeight(); yl++)
      for (unsigned int xl=0; xl<fontWidth(); xl++)
        if (glyphs[yl][base+xl])
	  setPixel (x+xl, y+yl, Colors::foreground);
    x += fontWidth();
  }
}

// This is a hacked-down version of write_png in example.c in libpng-0.99c
void
RGBGraph::writeAsPNG (png_rw_ptr write_data_fn) {

#if 0
  {
    FILE *fp = fopen ("testing.ppm", "w");
    fprintf (fp, "P6\n%u %u\n255\n", myxsize, myysize);
    fwrite (rgb, 1, myxsize*myysize*3, fp);
    fclose (fp);
  }
#endif

   png_structp png_ptr;
   png_infop info_ptr;

   /* Create and initialize the png_struct with the desired error handler
    * functions.  If you want to use the default stderr and longjump method,
    * you can supply NULL for the last three parameters.  We also check that
    * the library version is compatible with the one used at compile time,
    * in case we are using dynamically linked libraries.  REQUIRED.
    */
   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
     NULL, NULL, NULL);

   if (png_ptr == NULL)
     barf (PNG_WRITE_FAILURE);

   /* Allocate/initialize the image information data.  REQUIRED */
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL)
     barf (PNG_WRITE_FAILURE);

   /* Set error handling.  REQUIRED if you aren't supplying your own
    * error hadnling functions in the png_create_write_struct() call.
    */
   if (setjmp(png_ptr->jmpbuf))
     barf (PNG_WRITE_FAILURE);

   /* set up the output control if you are using standard C streams */
   // The user_io_ptr feature of libpng doesn't seem to work.
   png_set_write_fn(png_ptr, NULL, write_data_fn, NULL);

   /* Set the image information here.  Width and height are up to 2^31,
    * bit_depth is one of 1, 2, 4, 8, or 16, but valid values also depend on
    * the color_type selected. color_type is one of PNG_COLOR_TYPE_GRAY,
    * PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_PALETTE, PNG_COLOR_TYPE_RGB,
    * or PNG_COLOR_TYPE_RGB_ALPHA.  interlace is either PNG_INTERLACE_NONE or
    * PNG_INTERLACE_ADAM7, and the compression_type and filter_type MUST
    * currently be PNG_COMPRESSION_TYPE_BASE and PNG_FILTER_TYPE_BASE. REQUIRED
    */
   png_set_IHDR(png_ptr, info_ptr, myxsize, myysize, 8, PNG_COLOR_TYPE_RGB,
      PNG_INTERLACE_ADAM7, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

   /* Write the file header information.  REQUIRED */
   png_write_info(png_ptr, info_ptr);

   // Convert contiguous memory to what libpng wants
   // Sparcworks won't do this
   // png_byte *row_pointers[myysize];
   png_byte **row_pointers = (png_byte **) malloc (sizeof(png_byte*)*myysize);
   for (unsigned i=0; i<myysize; i++)
     row_pointers[i] = rgb + i * myxsize * 3;

   /**** write out the entire image data in one call ***/
   png_write_image(png_ptr, row_pointers);

   /* It is REQUIRED to call this to finish writing the rest of the file */
   png_write_end(png_ptr, info_ptr);

   /* clean up after the write, and free any memory allocated */
   png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

   /* that's it */
   free (row_pointers);
   return;
}
