// $Id: xxUnsignedChooser.cc,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxUnsignedChooser  Let user choose an unsigned value.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

unsigned xxUnsignedChooser::choice () {
  return current_choice;
}

void xxUnsignedChooserUpCallback (Widget w, XtPointer client_data,
				  XtPointer call_data) {
  xxUnsignedChooser *u = (xxUnsignedChooser *)client_data;
  if (u->current_choice == 1000000)
    u->current_choice = u->minimum;
  else
    u->current_choice++;
  u->update_value();
}

void xxUnsignedChooserDownCallback (Widget w, XtPointer client_data,
				  XtPointer call_data) {
  xxUnsignedChooser *u = (xxUnsignedChooser *)client_data;
  if (u->current_choice == 1000000)
    ;
  else if (u->current_choice > u->minimum) {
    u->current_choice--;
    u->update_value();
  } else {
    u->current_choice = 1000000;
    u->update_value();
  }
}

void
xxUnsignedChooser::update_value() {
  char temp[80];
  // The buffer size must remain constant to keep the button from moving
  // out from under the mouse pointer.  With variable width fonts, it
  // might still happen.
  if (current_choice < 1000000)
    sprintf (temp, "%7u", current_choice);
  else
    strcpy (temp, "(blank)");
  Arg args[1] = {
    {XtNlabel, (XtArgVal)temp}
  };
  XtSetValues (numlabel->manager, args, 1);
  numlabel->refresh();
}

xxUnsignedChooser::xxUnsignedChooser (xxContext *in_xxcontext, char *caption,
unsigned init, int init_is_null, unsigned in_minimum) {
  if (init_is_null)
    init = 1000000;
  assert (init >= in_minimum);
  minimum = in_minimum;
  current_choice = init;
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNorientation, (XtArgVal)XtorientHorizontal}
    };
    Widget boxwidget = XtCreateManagedWidget ("", boxWidgetClass,
      in_xxcontext->manager, args, 3);
    box = new xxContext (in_xxcontext, boxwidget);
  }
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNborderWidth, (XtArgVal)0}
    };
    Widget labelwidget = XtCreateManagedWidget (caption,
      labelWidgetClass, box->manager, args, 3);
    label = new xxContext (box, labelwidget);
  }
  {
    Arg args[3] =  {
      {XtNbackground, (XtArgVal)in_xxcontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)in_xxcontext->pixels[Colors::foreground]},
      {XtNborderWidth, (XtArgVal)0}
    };
    Widget labelwidget = XtCreateManagedWidget ("",
      labelWidgetClass, box->manager, args, 3);
    numlabel = new xxContext (box, labelwidget);
  }
  update_value();

  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)box->visual},
    {XtNcolormap, (XtArgVal)box->colormap},
    {XtNbackground, (XtArgVal)box->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };

  {
    Widget buttonwidget = XtCreateManagedWidget ("+", repeaterWidgetClass,
      box->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxUnsignedChooserUpCallback,
     (XtPointer)this);
    upbutton = new xxContext (box, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("-", repeaterWidgetClass,
      box->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxUnsignedChooserDownCallback,
     (XtPointer)this);
    downbutton = new xxContext (box, buttonwidget);
  }
}

xxUnsignedChooser::~xxUnsignedChooser () {
  delete upbutton;
  delete downbutton;
  delete label;
  delete numlabel;
  delete box;
}

void xxUnsignedChooser::global_redraw() {
  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)box->visual},
    {XtNcolormap, (XtArgVal)box->colormap},
    {XtNbackground, (XtArgVal)box->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };
  XtSetValues (upbutton->manager, buttonargs, 4);
  XtSetValues (downbutton->manager, buttonargs, 4);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)box->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)box->pixels[Colors::foreground]}
  };
  XtSetValues (label->manager, args, 2);
  XtSetValues (numlabel->manager, args, 2);
  XtSetValues (box->manager, args, 2);
}
