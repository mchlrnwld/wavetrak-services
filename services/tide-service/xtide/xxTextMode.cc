// $Id: xxTextMode.cc,v 1.13 2004/11/15 16:30:22 flaterco Exp $
/*  xxTextMode  Raw/medium/plain modes, in a window.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxTextMode::dismiss () {
  delete this; // Is this safe?
}

void
xxTextMode::help() {
  Dstr helpstring;

  if (mode == 'p')
    helpstring = "\
XTide Plain Mode Window\n\
\n\
Use the forward and backward buttons to scroll the tide predictions\n\
forward or backward in time.  You can resize the window vertically\n\
to see more predictions at once.\n\
\n\
The options menu supplies the following commands:\n\
\n\
  Save:  Use this to write the tide predictions to a text file.\n\
\n\
  Set Mark:  This is used to set the 'mark level' for tide predictions.\n\
  The prediction window will show the times when the tide level crosses\n\
  the mark.\n\
\n\
  Convert ft<->m:  Convert feet to meters or vice-versa.\n\
\n\
  Set Time:  Go to a different point in time.  Major adjustments that\n\
  cannot be made with the forward and backward buttons can be made with\n\
  Set Time.\n\
\n\
  New Graph Window:  Open a window with a tide graph for the current\n\
  location and time.\n\
\n\
  New Plain Mode Window:  Duplicates the existing text window.\n\
\n\
  New Raw Mode Window:  Open a window with a text listing of unprocessed\n\
  numerical time stamps and tide levels for the current location and time.\n\
\n\
  New Medium Rare Mode Window:  Open a window with a text listing of\n\
  processed timestamps and unprocessed numerical tide levels for the\n\
  current location and time.\n\
\n\
  New Clock Window:  Open a window with a tide clock for the current\n\
  location.\n\
\n\
  New Location Chooser:  Open new globe and location list windows to allow\n\
  selecting a new location.";
  else {

  if (mode == 'm')
    helpstring = "XTide Medium Rare Mode Window\n";
  else
    helpstring = "XTide Raw Mode Window\n";
  helpstring += "\n\
Use the forward and backward buttons to scroll the tide predictions\n\
forward or backward in time.  You can resize the window vertically\n\
to see more predictions at once.\n\
\n\
The options menu supplies the following commands:\n\
\n\
  Save:  Use this to send output to a text file.  In this mode you are\n\
  given the opportunity to generate output for a different time interval\n\
  than is displayed on the screen.\n\
\n\
  Convert ft<->m:  Convert feet to meters or vice-versa.\n\
\n\
  Set Time:  Go to a different point in time.  Major adjustments that\n\
  cannot be made with the forward and backward buttons can be made with\n\
  Set Time.\n\
\n\
  Set Step:  Change the time increment of the output.\n\
\n\
  New Graph Window:  Open a window with a tide graph for the current\n\
  location and time.\n\
\n\
  New Plain Mode Window:  Open a window with a text listing of tide\n\
  predictions for the current location and time.\n\
\n";

  if (mode == 'm')
    helpstring += "\
  New Raw Mode Window:  Open a window with a text listing of unprocessed\n\
  numerical time stamps and tide levels for the current location and time.\n\
\n\
  New Medium Rare Mode Window:  Duplicates the existing medium rare mode\n\
  window.\n";
  else
    helpstring += "\
  New Raw Mode Window:  Duplicates the existing raw mode window.\n\
\n\
  New Medium Rare Mode Window:  Open a window with a text listing of\n\
  processed timestamps and unprocessed numerical tide levels for the\n\
  current location and time.\n";

  helpstring += "\n\
  New Clock Window:  Open a window with a tide clock for the current\n\
  location.\n\
\n\
  About This Station:  Show station metadata.\n\
\n\
  New Location Chooser:  Open new globe and location list windows to allow\n\
  selecting a new location.";
  }
  (void) xtidecontext->root->newHelpBox (helpstring);
}

void
xxTextModeforwardCallback (Widget w, XtPointer client_data,
XtPointer call_data) {
  xxTextMode *txt = (xxTextMode *)client_data;
  TideEventsIterator it = txt->organizer.upper_bound (txt->t.timet());
  if (it == txt->organizer.end()) {
    // This should only happen here if lines == 1; see draw().
    if (txt->mode == 'p')
      txt->station->extendRange (txt->organizer, forward, Interval(max(txt->lines,10)*DAYSECONDS/2));
    else
      txt->station->extendRange (txt->organizer, forward, 2*max(txt->lines,10));
    xxTextModeforwardCallback (w, client_data, call_data);
    return;
  }
  txt->t = it->second.tm;
  txt->draw();
}

void
xxTextModebackwardCallback (Widget w, XtPointer client_data,
XtPointer call_data) {
  xxTextMode *txt = (xxTextMode *)client_data;
  TideEventsIterator it = txt->organizer.lower_bound (txt->t.timet());
  assert (it != txt->organizer.end());
  if (it == txt->organizer.begin()) {
    if (txt->mode == 'p')
      txt->station->extendRange (txt->organizer, backward, Interval(max(txt->lines,10)*DAYSECONDS/2));
    else
      txt->station->extendRange (txt->organizer, backward, 2*max(txt->lines,10));
    xxTextModebackwardCallback (w, client_data, call_data);
    return;
  }
  it--;
  txt->t = it->second.tm;
  txt->draw();
}

void
xxTextModeResizeHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch) {
  xxTextMode *txt = (xxTextMode *)client_data;
  switch (event->type) {
  case ConfigureNotify:
    {
      XConfigureEvent *ev = (XConfigureEvent *)event;
      Dimension newheight = ev->height;
      if (newheight != txt->height) {
	txt->height = newheight;
	// Extremely kludgy guesstimate of how many lines we should have now.
	int t = (int)((double)(newheight - (txt->origheight -
	  txt->origlabelheight)) * (double)origtextmodelines /
	  (double)(txt->origlabelheight));
	if (t < 1)
	  txt->lines = 1;
	else
	  txt->lines = t;
	txt->redraw();
      }
    }
    break;
  default:
    ;
  }
}

void
xxRareModeSaveCallback (Dstr &filename, Timestamp start_tm, Timestamp end_tm,
void *in_ptr) {
  xxTextMode *txt = (xxTextMode *)in_ptr;
  FILE *fp;
  if ((fp = fopen (filename.aschar(), "w"))) {
    Dstr text;
    txt->xtidecontext->rareModes (txt->station, start_tm, end_tm, text,
      txt->mode, 't');
    fprintf (fp, "%s", text.aschar());
    fclose (fp);
  } else {
    Dstr details (filename);
    details += ": ";
    details += strerror (errno);
    details += ".";
    barf (CANT_OPEN_FILE, details, 0);
  }
}

void
xxTextModeSaveCallback (Dstr &filename, void *in_ptr) {
  xxTextMode *txt = (xxTextMode *)in_ptr;
  FILE *fp;
  if ((fp = fopen (filename.aschar(), "w"))) {
    Dstr text;
    txt->xtidecontext->plainMode (txt->station, txt->t-Interval(60),
      txt->last_tm+Interval(60), text, 't');
    fprintf (fp, "%s", text.aschar());
    fclose (fp);
  } else {
    Dstr details (filename);
    details += ": ";
    details += strerror (errno);
    details += ".";
    barf (CANT_OPEN_FILE, details, 0);
  }
}

void
xxTextMode::save () {
  if (mode == 'p')
    (void) new xxFilename (xtidecontext, mypopup, xxTextModeSaveCallback, this, "tides.txt");
  else
    (void) new xxRareModeSavePrompts (xtidecontext, mypopup, xxRareModeSaveCallback, this, "tides.txt", t, last_tm+station->step, station->timeZone);
}

xxTextMode::xxTextMode (xxTideContext *in_tidecontext,
xxContext *in_xxcontext, Station *in_station, char mode_in): xxDrawable (in_tidecontext,
in_xxcontext, in_station, 2) {
  mode = mode_in;
  t = Timestamp((time_t)(time(NULL)));
  construct();
}

xxTextMode::xxTextMode (xxTideContext *in_tidecontext,
xxContext *in_xxcontext, Station *in_station, char mode_in, Timestamp t_in):
xxDrawable (in_tidecontext, in_xxcontext, in_station, 2) {
  mode = mode_in;
  t = t_in;
  construct();
}

void xxTextMode::construct () {
  lines = origtextmodelines;
  Dstr title (station->name);
  switch (mode) {
  case 'r':
    title += " (Raw)";
    break;
  case 'm':
    title += " (Medium Rare)";
    break;
  case 'p':
    title += " (Text)";
    break;
  default:
    assert (0);
  }
  mypopup->setTitle (title);
  XtAddEventHandler (mypopup->manager, StructureNotifyMask, False,
		     xxTextModeResizeHandler, (XtPointer)this);

  {
    Arg labelargs[6] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainTop},
      {XtNbottom, (XtArgVal)XawChainTop}
    };
    Widget labelwidget = XtCreateManagedWidget (station->name.aschar(),
      labelWidgetClass, container->manager, labelargs, 6);
    namelabel = new xxContext (mypopup, labelwidget);
  }
  {
    Arg labelargs[7] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)namelabel->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainRight},
      {XtNtop, (XtArgVal)XawChainTop},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget labelwidget = XtCreateManagedWidget ("",
      labelWidgetClass, container->manager, labelargs, 7);
    label = new xxContext (mypopup, labelwidget);
  }
  {
    Arg buttonargs[7] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)label->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainBottom},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget buttonwidget = XtCreateManagedWidget ("Backward", repeaterWidgetClass,
      container->manager, buttonargs, 7);
    XtAddCallback (buttonwidget, XtNcallback, xxTextModebackwardCallback,
     (XtPointer)this);
    backwardbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Arg buttonargs[8] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)label->manager},
      {XtNfromHoriz, (XtArgVal)backwardbutton->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainBottom},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget buttonwidget = XtCreateManagedWidget ("Forward", repeaterWidgetClass,
      container->manager, buttonargs, 8);
    XtAddCallback (buttonwidget, XtNcallback, xxTextModeforwardCallback,
     (XtPointer)this);
    forwardbutton = new xxContext (mypopup, buttonwidget);
  }

  addNormalButtons (label->manager, forwardbutton->manager);

  redraw();
  mypopup->realize();
  mypopup->widthNoSmaller();

  {
    Arg args[1] = {
      {XtNheight, (XtArgVal)(&origheight)}
    };
    XtGetValues (container->manager, args, 1);
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Text window original height " << origheight << endl;
#endif
    height = origheight;
  }
  {
    Dimension internalheight;
    Arg args[2] = {
      {XtNheight, (XtArgVal)(&origlabelheight)},
      {XtNinternalHeight, (XtArgVal)(&internalheight)}
    };
    XtGetValues (label->manager, args, 2);
    origlabelheight -= internalheight * 2;
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Text label original height " << origlabelheight << endl;
#endif
  }
}

void xxTextMode::redraw() {
  organizer.clear();
  if (mode == 'p')
    station->predictTideEvents (t-Interval(60), t+Interval(max(lines,10)*DAYSECONDS/2),
				     organizer);
  else
    station->predictRawEvents (t, t+((station->step)*(max(lines,10)*2)), organizer);

  // Was having major trouble getting the label widget to resize itself
  // reliably.  This seems to help.
  {
    int heightguess = height - (origheight - origlabelheight);
    if (heightguess < 1)
      heightguess = 1;
    Arg labelargs[1] =  {
      {XtNheight, (XtArgVal)heightguess}
    };
    XtSetValues (label->manager, labelargs, 1);
  }

  draw();
}

void xxTextMode::draw () {
  TideEventsIterator it = organizer.lower_bound (t.timet());
  Dstr buf;
  if (it != organizer.end())
    t = it->second.tm;
  for (unsigned a=0; a<lines; a++) {
    if (it == organizer.end()) {
      // You may think it's the end, but it's not.
      if (mode == 'p')
        station->extendRange (organizer, forward, Interval(max(lines,10)*DAYSECONDS/2));
      else
        station->extendRange (organizer, forward, 2*max(lines,10));
      draw();
      return;
    } else {
      Dstr line;
      it->second.print (line, 't', mode, *station);
      if (!(buf.isNull()))
        buf += '\n';
      buf += line;
      last_tm = it->second.tm;
      it++;
    }
  }
  {
    Arg labelargs[1] =  {
      {XtNlabel, (XtArgVal)buf.aschar()}
    };
    XtSetValues (label->manager, labelargs, 1);
  }
}

xxTextMode::~xxTextMode() {
  mypopup->unrealize();
  delete label;
  delete namelabel;
  delete backwardbutton;
  delete forwardbutton;
}

void xxTextMode::global_redraw() {
  xxDrawable::global_redraw();
  redraw();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (backwardbutton)
    XtSetValues (backwardbutton->manager, buttonargs, 2);
  if (forwardbutton)
    XtSetValues (forwardbutton->manager, buttonargs, 2);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (label)
    XtSetValues (label->manager, args, 2);
  if (namelabel)
    XtSetValues (namelabel->manager, args, 2);
}

int xxTextMode::is_rare() {
  if (mode == 'p')
    return 0;
  return 1;
}
