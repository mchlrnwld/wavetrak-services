// $Id: xxDrawable.hh,v 1.4 2004/09/07 21:23:30 flaterco Exp $
/*  xxDrawable  Abstract class for all tide-predicting windows.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxDrawable: public xxWindow {
  friend void xxDrawableMarkCallback (PredictionValue *marklevel,
    void *in_ptr);
  friend void xxDrawableTimestampCallback (Timestamp t,
    void *in_ptr);
  friend void xxDrawableAspectCallback (double aspect, void *in_ptr);
  friend void xxDrawableStepCallback (Interval step, void *in_ptr);
  friend void xxDrawableGraphCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawablePlainCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawableRawCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawableMediumRareCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawableClockCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawableAboutCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawableChooserCallback (Widget w, XtPointer client_data, XtPointer call_data);
  friend void xxDrawableControlPanelCallback (Widget w, XtPointer client_data, XtPointer call_data);

public:
  // needcontainer:
  //   0  No thanks
  //   1  Make me a box
  //   2  Make me a form
  xxDrawable (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    Station *in_station, int needcontainer = 1);
  virtual ~xxDrawable();

  virtual void help() = 0;
  virtual void save() = 0;
  virtual void redraw() = 0;

  // Global_redraw signals a change in global parameters (like colors).
  virtual void global_redraw();

  // These are actually the same for all drawables.
  void mark();
  void timestamp();
  void aspect();
  void step();

  // This is called by subclasses to hook up the dismiss button, the help
  // button, and the options menu.  Requires needcontainer > 0.
  // Forms must pass the widgets above and to the left for layout purposes.
  void addNormalButtons (Widget below_this = NULL, Widget beside_this = NULL);

  // Set this to NULL if station is "stolen" for another window.
  Station *station;

  virtual int is_graph();
  virtual int is_clock();
  virtual int is_rare();

protected:

  // This is the "starting time" or "now" that is used to calibrate
  // any given drawable in time.  This needs to be here so that it
  // is possible for one drawable to create another one with the
  // same start time.
  Timestamp t;

  xxContext *savebutton, *markbutton, *helpbutton, *dismissbutton,
    *optionsbutton, *optionsmenu, *graphbutton, *plainbutton,
    *rawbutton, *mediumrarebutton, *aboutbutton,
    *aspectbutton, *clockbutton, *chooserbutton, *timestampbutton,
    *unitsbutton, *rootbutton, *stepbutton;
};
