// 	$Id: errors.hh,v 1.7 2004/10/22 17:43:51 flaterco Exp $	
// errors:  Global functions for bailing out -- non-X capable version.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

enum tideerr {YEAR_OUT_OF_RANGE,
              TM2UTC_FAILED,
              MKTIME_FAILED,
              INVALID_CONSTITUENT,
              YEAR_NOT_IN_TABLE,
              DOUBLE_READ,
              NULL_YEAR,
              END_OF_FILE,
              NO_HFILE_PATH,
              IMPOSSIBLE_CONVERSION,
              NO_CONVERSION,
              UNRECOGNIZED_UNITS,
              BOGUS_COORDINATES,
              CANT_OPEN_FILE,
              CORRUPT_HARMONICS_FILE,
              BADCOLORSPEC,
              XPM_ERROR,
              NOHOMEDIR,
              BADHHMM,
              XMLPARSE,
              STATION_NOT_FOUND,
              SUB_SUBORDINATE,
              NOOFFSETSFOUND,
              CANTOPENDISPLAY,
              NOT_A_NUMBER,
              PNG_WRITE_FAILURE,
              CANT_GET_SOCKET,
              ABSURD_OFFSETS,
              NUMBER_RANGE_ERROR,
              BAD_MODE,
              BAD_FORMAT,
              BAD_TIMESTAMP,
              BAD_IP_ADDRESS,
              BAD_BOOL,
              BAD_TEXT,
              BAD_OR_AMBIGUOUS_COMMAND_LINE};

// This function should be called once at the start of the program to
// divert messages to syslog.
void set_daemon_mode();

// Output error message and die.
void barf (enum tideerr err, Dstr &details, int fatal = 1);
void barf (enum tideerr err, int fatal = 1);

// Other messages to stderr / syslog.
void log (Dstr &message, int priority);
void log (const char *message, int priority);
// Convenience -- details are just appended to message
void log (const char *message, Dstr &details, int priority);

// Like perror, but messages are diverted through syslog if in daemon mode.
void xperror (const char *s);
