/*  xxPixmapGraph  Graph implemented as Pixmap.
    Last modified 1998-04-26

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxPixmapGraph: public Graph {
public:
  xxPixmapGraph (unsigned xsize, unsigned ysize, xxContext *in_context);
  virtual ~xxPixmapGraph();

  Pixmap pixmap;

protected:
  xxContext *context;

  unsigned fontWidth();
  unsigned fontHeight();
  void drawString (int x, int y, const Dstr &s);

  // These override perfectly good versions in Graph in order to use
  // the available X11 drawing functions.
  void drawVerticalLine (int x, int ylo, int yhi,
                                 Colors::colorchoice c);
  void drawHorizontalLine (int xlo, int xhi, int y,
				   Colors::colorchoice c);

  void setPixel (int x, int y, Colors::colorchoice c);
  void setPixel (int x, int y, Colors::colorchoice c,
    double saturation);  // Saturation ranges from 0 to 1
};
