// $Id: StationIndex.hh,v 1.2 2004/10/26 15:54:52 flaterco Exp $
/*  StationIndex  Collection of StationRefs.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class StationIndex {
public:
  StationIndex();
  ~StationIndex();
  StationIndex *clone ();
  char **makeStringList (unsigned long startat, unsigned long maxlength);
  void add (StationRef *addthis);
  void addHfileId (Dstr &hid);
  Dstr hfile_ids;

  enum sortfield {sf_name, sf_latitude, sf_longitude};
  void qsort (sortfield sortby = sf_name);

  unsigned long length();
  StationRef *operator[] (unsigned long index);
  StationRef *operator[] (const Dstr &name);

  double bestCenterLongitude();
protected:
  StationRef **stationrefs;
  unsigned long used, max;
};
