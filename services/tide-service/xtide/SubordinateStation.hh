// $Id: SubordinateStation.hh,v 1.10 2004/11/17 16:27:18 flaterco Exp $
/*  SubordinateStation  Implementation for offset station.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class SubordinateStation: public Station {
public:
  SubordinateStation (ReferenceStation *rs, HairyOffsets *offsets);
  ~SubordinateStation();
  int is_reference_station();
  int have_residuals();
  int have_floodbegins();
  int have_ebbbegins();
  HairyOffsets *origOffsets;
  HairyOffsets *residualOffsets;

  PredictionValue minLevel() const;
  PredictionValue maxLevel() const;

  PredictionValue predictTideLevel (Timestamp t);

  // Fill in PredictionValue and possibly apply corrections.
  void finishTideEvent (TideEvent &te);

protected:
  void reduce_offsets();

  // Stuff for predictTideLevel
  Timestamp uncleftt, uncrightt, subleftt, subrightt;
  PredictionValue uncleftp, uncrightp, subleftp, subrightp;
  PredictionValue::Unit cache_units;
};
