// $Id: xxGlobe.cc,v 1.13 2004/12/10 20:34:30 flaterco Exp $
/*  xxGlobe   Location chooser using Orthographic Projection.

    There is some duplicated code between xxGlobe and xxMap.  However,
    they are sufficiently different that I think complete
    encapsulation is the cleanest approach.  -- DWF

    Copyright (C) 1998  David Flater.
    (Portions by Jan Depner copied from xxMap.)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void xxGlobePointerMotionEventHandler (Widget w, XtPointer client_data,
XEvent *event, Boolean *continue_dispatch) 
{
    xxGlobe *globe = (xxGlobe *) client_data;
    XMotionEvent *xme = (XMotionEvent *) event;

    // Coordinates relative to upper left corner of globe pixmap.
    // Need to subtract the internal margin of the label widget.

    globe->last_x = xme->x - globe->internalWidth;
    globe->last_y = xme->y - globe->internalHeight;
    globe->update_position (globe->last_x, globe->last_y);
}

// Test for coordinates within reasonable boundaries to avoid
// overflowing X11 (16-bit ints).
int xxGlobe::inrange (long x, long y) {
  return (x >= -32768 && x < 32768 &&
          y >= -32768 && y < 32768);
}

// This draw_coastlines is based on Jan's redrawGlobe
void xxGlobe::draw_coastlines () {
  float          *latray, *lonray;
  int            *segray, coast;
  static char    files[6][12] = {"wvsfull.dat", "wvs250k.dat", "wvs1.dat", 
				 "wvs3.dat", "wvs12.dat", "wvs43.dat"};
  int wvsrtv (char *, int, int, float **, float **, int **);
  // Scales / resolutions as described in wvsrtv.cc (1:X)
  // wvsfull is unknown but doesn't matter
  static unsigned long scales[6] = {0, 250000, 1000000, 3000000, 12000000,
				    43000000};

  // First figure out the worst case bounds.
  int startlat, endlat, startlon, endlon;
  if (center_latitude == 0.0) {
    startlat = -90;
    endlat = 90;
    // Add 1 because of double-to-int roundoff.
    startlon = (int)center_longitude - 91;
    if (startlon < -180)
      startlon += 360;
    endlon = (int)center_longitude + 91;
    if (endlon > 180)
      endlon -= 360;
  } else {
    // Any time that the globe is tilted, a pole could be showing
    // and we could need the entire range of longitudes.
    startlon = -180;
    endlon = 180;
    if (center_latitude > 0.0) {
      // Add 1 because of double-to-int roundoff.
      startlat = -91 + (int)center_latitude;
      if (startlat < -90)
        startlat = -90;
      endlat = 90;
    } else {
      startlat = -90;
      endlat = 91 + (int)center_latitude;
      if (endlat > 90)
        endlat = 90;
    }
  }

  // OK, here we go.
  // The following code will break if the viewport is not centered.
  // There were too many special cases to do it otherwise.
  // (What is left still contains some overkill.)

  // If any of the corners is out in space, stop right there.  We are
  // probably fully zoomed out and can't improve the worst case anyhow.
  double ullat, ullon, urlat, urlon, lllat, lllon, lrlat, lrlon;
  double ulat, llat, junklon;
  int forgetit = 0;
  if (!untranslateCoordinates (0, 0, ullat, ullon))
    forgetit = 1;
  else if (!untranslateCoordinates (0, min_globe_size, lllat, lllon))
    forgetit = 1;
  else if (!untranslateCoordinates (min_globe_size, 0, urlat, urlon))
    forgetit = 1;
  else if (!untranslateCoordinates (min_globe_size, min_globe_size, lrlat, lrlon))
    forgetit = 1;
  // The extrema for latitude can be in the middle.
  else if (!untranslateCoordinates (min_globe_size/2, 0, ulat, junklon))
    forgetit = 1;
  else if (!untranslateCoordinates (min_globe_size/2, min_globe_size, llat, junklon))
    forgetit = 1;

  if (!forgetit) {

#define boundmacro(lbound,ubound,x) \
  if (x<lbound) lbound=x; else if (x>ubound) ubound=x;

    double dminlat = lllat;
    double dmaxlat = lllat;
    boundmacro (dminlat, dmaxlat, ullat);
    boundmacro (dminlat, dmaxlat, lrlat);
    boundmacro (dminlat, dmaxlat, urlat);
    boundmacro (dminlat, dmaxlat, ulat);
    boundmacro (dminlat, dmaxlat, llat);
    double dminlon = lllon;
    double dmaxlon = lllon;
    boundmacro (dminlon, dmaxlon, ullon);
    boundmacro (dminlon, dmaxlon, lrlon);
    boundmacro (dminlon, dmaxlon, urlon);

    int minlat = max (-90, (int)(floor(dminlat)));
    int maxlat = min (90, (int)(ceil(dmaxlat)));
    int minlon = max (-180, (int)(floor(dminlon)));
    int maxlon = min (180, (int)(ceil(dmaxlon)));

    // If a pole is showing, then forget longitude and just work on
    // latitude.  The most expedient way to find if a pole is showing is
    // to translate its coordinates.
    long junkx, junky;
    if (translateCoordinates (90.0, 0.0, &junkx, &junky)) // N
      startlat = max (startlat, minlat);
    else if (translateCoordinates (-90.0, 0.0, &junkx, &junky)) // S
      endlat = min (endlat, maxlat);
    else {
      // Otherwise, we can narrow the bounds every which way, but watch out
      // for that accursed special case (x3).
      startlat = max (startlat, minlat);
      endlat = min (endlat, maxlat);
      if (ullon > urlon && lllon > lrlon) {
	startlon = max (startlon, (int)(floor(min(lllon, ullon))));
	endlon = min (endlon, (int)(ceil(max(lrlon, urlon))));
      } else if (ullon > urlon) {
	startlon = max (startlon, (int)(floor(ullon)));
	endlon = min (endlon, (int)(ceil(urlon)));
      } else if (lllon > lrlon) {
	startlon = max (startlon, (int)(floor(lllon)));
	endlon = min (endlon, (int)(ceil(lrlon)));
      } else {
	startlon = max (startlon, minlon);
	endlon = min (endlon, maxlon);
      }
    }
  }

  // printf ("%d %d %d %d\n", startlat, endlat, startlon, endlon);

  // In Jan's code where he sets bounds (xxGlobeKeyboardEventHandler),
  // the special case where we cross 180 and west is numerically higher
  // than east is addressed by adding 360 to east.  So, we'll do that here.
  if (startlon > endlon)
    endlon += 360;

  latray = (float *) NULL;
  lonray = (float *) NULL;
  segray = (int *) NULL;
  int lat, lon;
  long segx[2], segy[2];

  /* Get the proper coastline data set based on the zoom level. */
  // Your propriety may vary.
  // Originally, 3 was skipped because Jan didn't like having it
  // around because the file is physically larger than 2.
  for (coast=5; coast > 0 &&
	 (double)size * (double)scales[coast] > zoomresmagicnumber; coast--);

  // fprintf (stderr, "%d %6lu %le\n", coast, size, (double)size * (double)scales[coast]);

  /*  Read and plot the coastlines. */

  for (lat = startlat ; lat < endlat ; lat++) {
    for (lon = startlon ; lon < endlon ; lon++) {
      int k, nseg, offset;
      offset = 0;

      nseg = wvsrtv (files[coast], lat, lon, &latray, &lonray, 
	  &segray);

      if (nseg) {
	for (k = 0 ; k < nseg ; k++) {
	  int cnt, m;
	  m = 0;
	  for (cnt = 0 ; cnt < segray[k] ; cnt++) {
	    // Need to fix out-of-range longitudes here.  We get them
	    // because of the workaround discussed above.
	    double fixedlon = (double)lonray[offset+cnt];
	    if (fixedlon < -180.0)
	      fixedlon += 360.0;
	    else if (fixedlon > 180.0)
	      fixedlon -= 360.0;
	    if (translateCoordinates (
	      (double)latray[offset+cnt], fixedlon, &segx[m], &segy[m])) {
	      // In xxMap, Jan did this:
	      /*  Check for the weird situation when
		  west and east are at 0.0 and 360.0.  */
	      //		if (m && segx[1] - segx[0] < 50 && 
	      //		    segx[0] - segx[1] < 50) {
	      // Whatever that was supposed to do in xxMap, it wreaks
	      // havoc here.  (Lines vanish when zoomed in.)
	      if (m) {
		// On sphere, this should be an arc.  (Nit)
		XDrawLine (picture->display, globepixmap, picture->text_gc, 
		  segx[0], segy[0], segx[1], segy[1]);
		segx[0] = segx[1];
		segy[0] = segy[1];
	      }
	      m = 1;
	    } else {
	      m = 0;
	    }
	  }
	  offset += segray[k];
	}
      }
    }
  }
  wvsrtv ("clean", 0, 0, &latray, &lonray, &segray);
}

// Based on Jan's
void xxGlobe::update_position (long rx, long ry)
{
  char   string[50];
  double lat, lon;

  if (untranslateCoordinates (rx, ry, lat, lon)) {
    sprintf (string, "Lat %8.4f", lat);
    Arg args[1] = 
      {
	{XtNlabel, (XtArgVal)string}
      };
    XtSetValues (lattext->manager, args, 1);
    sprintf (string, "Lon %9.4f", lon);
    XtSetValues (lontext->manager, args, 1);
  }
}

void xxGlobe::draw_gridlines () {

  // Draw a circle around everything just for looks.
  if (inrange (-xorigin, -yorigin) && inrange (size - xorigin, size - yorigin))
    XDrawArc (picture->display, globepixmap, picture->text_gc,
      -xorigin, -yorigin, size, size, 0, 360*64);

  // I was hoping to determine the arcs analytically but gave myself a
  // headache trying to figure out the longitude arcs.  The following
  // implementation uses brute force and ignorance to draw a faceted
  // version.  Surprisingly, it seems to be good enough and fast
  // enough.

  // Longitude
  long lng, lat, lastx=0, lasty=0, newx, newy, lastvalid;
  for (lng = -180; lng <= 150; lng += 30) {
    lastvalid = 0;
    for (lat = -90; lat <= 90; lat += 5) {
      if (projectCoordinates (lat, lng, &newx, &newy)) {
        // This draws farther outside the realm than TranslateCoordinates
        // would allow.
        if (lastvalid)
          if (inrange (lastx-xorigin, lasty-yorigin) && inrange (newx-xorigin, newy-yorigin))
            XDrawLine (picture->display, globepixmap, picture->text_gc,
              lastx-xorigin, lasty-yorigin, newx-xorigin, newy-yorigin);
        lastx = newx;
        lasty = newy;
        lastvalid = 1;
      } else
        lastvalid = 0;
    }
  }

  // Latitude
  for (lat = -60; lat <= 60; lat += 30) {
    lastvalid = 0;
    for (lng = -180; lng <= 180; lng += 5) {
      if (projectCoordinates (lat, lng, &newx, &newy)) {
        // This draws farther outside the realm than TranslateCoordinates
        // would allow.
        if (lastvalid)
          if (inrange (lastx-xorigin, lasty-yorigin) && inrange (newx-xorigin, newy-yorigin))
            XDrawLine (picture->display, globepixmap, picture->text_gc,
              lastx-xorigin, lasty-yorigin, newx-xorigin, newy-yorigin);
        lastx = newx;
        lasty = newy;
        lastvalid = 1;
      } else
        lastvalid = 0;
    }
  }
}

// projectCoordinates does an orthographic projection of size size
// (declared below).  The version accepting doubles differs in that
// (0.0,0.0) is not treated as a null.
// Returns:
//   1 = the point is on this side; results valid.
//   0 = the point is on the other side; ignore results.

int xxGlobe::projectCoordinates (double lat, double lng, long *x, long *y) {
  // wvsrtv sent longitude -180.002060
  assert (lat >= -90 && lat <= 90 && lng >= -181 && lng <= 180);

  // Calibrate longitude.
  lng -= center_longitude;
  // Don't know how to calibrate latitude with spherical coordinates so
  // I'll do that later.

  // Using formulas from:
  // Daniel Zwillinger, ed., Standard Mathematical Tables and Formulae,
  // 30th ed., CRC Press.

  // Section 4.9.4.
  // Convert spherical coordinates to Cartesian coordinates.
  // (The foo_ prefix is to remind me that the x and y axes in the
  // formulas are NOT the same as my x and y axes.)
  double zenith   = (90.0 - lat) * M_PI / 180.0;
  double azimuth = lng * M_PI / 180.0;
  double foo_x = cos (azimuth) * sin (zenith);
  double foo_y = sin (azimuth) * sin (zenith);
  double foo_z = cos (zenith);

  // Section 4.10.1, Formulas for Symmetries: Cartesian Coordinates.
  // Calibrate latitude by rotation around (0,1,0).
  // Collapsing all the zeroes and ones out of the formula we get:
  {
    double sinalpha = sin (center_latitude * M_PI / 180.0);
    double cosalpha = cos (center_latitude * M_PI / 180.0);
    double bar_x = cosalpha * foo_x + sinalpha * foo_z;
    double bar_z = -sinalpha * foo_x + cosalpha * foo_z;
    foo_x = bar_x;
    foo_z = bar_z;
  }

  if (foo_x < 0.0)
    return 0; // It's around the other side
  double s2 = (double)size/2.0;
  *x = (int) (foo_y * s2 + s2);
  *y = (int) (-foo_z * s2 + s2);
  return 1;
}

int xxGlobe::projectCoordinates (Coordinates c_in, long *x, long *y) {
  if (c_in.isNull())
    return 0;
  return projectCoordinates (c_in.lat(), c_in.lng(), x, y);
}

// translateCoordinates calls projectCoordinates then further cooks the
// result to map it into the current viewport.  The version accepting
// doubles differs in that (0.0,0.0) is not treated as a null.
// Returns:
//   1 = the point may be visible; draw it.
//   0 = the point is invisible; ignore results.

int xxGlobe::translateCoordinates (double lat, double lng, long *x, long *y) {
  assert (lat >= -90 && lat <= 90 && lng >= -180 && lng <= 180);
  if (!projectCoordinates (lat, lng, x, y))
    return 0;
  *x -= xorigin;
  *y -= yorigin;
  // Allow a cushy margin so that lines won't get chopped.
  if (*x >= -antichopmargin && *x <= min_globe_size+antichopmargin &&
      *y >= -antichopmargin && *y <= min_globe_size+antichopmargin)
    return 1;
  return 0;
}

int xxGlobe::translateCoordinates (Coordinates c_in, long *x, long *y) {
  if (c_in.isNull())
    return 0;
  return translateCoordinates (c_in.lat(), c_in.lng(), x, y);
}

// Returns:
//   1 = the point is on the globe somewhere; lat and lng valid.
//   0 = you missed; ignore lat and lng.
int xxGlobe::untranslateCoordinates (long x, long y, double &lat, double &lng) {

  if (x < 0 || x > min_globe_size || y < 0 || y > min_globe_size)
    return 0;

  // Undo translation to window coordinates.
  x += xorigin;
  y += yorigin;

  // See comments in projectCoordinates about "foo coordinates" and the
  // formulas below.

  // Convert back to foo coordinates, inferring depth axis (foo_x).
  double s2 = (double)size/2.0;
  double foo_y = ((double)x-s2)/s2;
  double foo_z = -((double)y-s2)/s2;
  double foo_x = 1.0 - foo_y * foo_y - foo_z * foo_z; // Temporarily squared
  if (foo_x < 0.0)
    return 0; // Outside the circle = miss.
  foo_x = sqrt (foo_x);

  // Undo latitude rotation (same formula, but negate the angle).
  {
    double sinalpha = sin (-center_latitude * M_PI / 180.0);
    double cosalpha = cos (-center_latitude * M_PI / 180.0);
    double bar_x = cosalpha * foo_x + sinalpha * foo_z;
    double bar_z = -sinalpha * foo_x + cosalpha * foo_z;
    foo_x = bar_x;
    foo_z = bar_z;
  }

  // Convert Cartesian coordinates back to spherical coordinates.
  // (Also in Section 4.10.1)
  // Denominator in second zenith formula is always 1 for us.
  double azimuth = atan2 (foo_y, foo_x);  // Range -pi to pi
  double zenith = acos (foo_z);           // Range 0 to pi

  // Convert spherical coordinates back to lat and lng.
  lat = 90.0 - zenith * 180.0 / M_PI;     // Range -90 to 90
  lng = azimuth * 180.0 / M_PI;           // Range -180 to 180

  // Undo center longitude adjustment.
  lng += center_longitude;
  if (lng > 180.0)
    lng -= 360.0;
  else if (lng < -180.0)
    lng += 360.0;
  assert (lng >= -180.0 && lng <= 180.0);

  return 1;
}

// If you support zooming and panning by drawing the globe bigger and
// parenting it with a viewport, you run out of memory VERY fast.
// Zoom 10x = 5000x5000x3 bytes = 75 MEGS

void
xxGlobe::redrawGlobe () {
  Cursor cursor = XCreateFontCursor (picture->display, XC_watch);
  XDefineCursor (picture->display, XtWindow (picture->manager), cursor);

  if (xorigin < 0)
    xorigin = 0;
  else if (xorigin > (int)size - min_globe_size)
    xorigin = (int)size - min_globe_size;

  if (yorigin < 0)
    yorigin = 0;
  else if (yorigin > (int)size - min_globe_size)
    yorigin = (int)size - min_globe_size;

  XFillRectangle (picture->display, globepixmap,
    picture->background_gc, 0, 0, min_globe_size+1, min_globe_size+1);

  draw_coastlines();
  draw_gridlines();

  // Draw station blobs
  unsigned long i;
  StationIndex *si = new StationIndex();
  int spot, halfspot;
  if (size < max_globe_size / 4) {
    spot = 3;
    halfspot = 1;
  } else {
    spot = 5;
    halfspot = 2;
  }
  for (i=0; i<stationIndex->length(); i++) {
    long x, y;
    if (translateCoordinates ((*stationIndex)[i]->coordinates, &x, &y)) {
      // Here, we *do* want to eliminate everything not strictly in
      // the window, modulo the size of the dots.
      if (x >= -halfspot && y >= -halfspot && x <= min_globe_size+halfspot && y <= min_globe_size+halfspot) {
        XFillArc (picture->display, globepixmap, picture->mark_gc,
          x-halfspot, y-halfspot, spot, spot, 0, 360*64);
        si->add ((*stationIndex)[i]);
      }
    }
  }

  // Update lat/lng readouts
  update_position (last_x, last_y);

  // Install new pixmap and station list
  Arg args[1] = {
    {"bitmap", (XtArgVal)globepixmap}
  };
  XtSetValues (picture->manager, args, 1);
  picture->refresh();
  blastflag = 0;
  locationlist->changeList (si);

  XUndefineCursor (picture->display, XtWindow (picture->manager));
  XFreeCursor (picture->display, cursor);
}

static void
dismissCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxGlobe *globe = (xxGlobe *)client_data;
  globe->dismiss();
}

void
xxGlobeFlatCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxGlobe *globe = (xxGlobe *)client_data;
  globe->xtidecontext->root->newMap ();
  globe->dismiss();
}

void
xxGlobeHelpCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxGlobe *globe = (xxGlobe *)client_data;
  Dstr helpstring ("\
XTide Location Chooser -- Globe Window\n\
\n\
The globe window initially shows an entire hemisphere.  Tide stations\n\
in that hemisphere are projected onto the globe as red dots and\n\
enumerated in the location list window.  Other tide stations can be\n\
accessed by rotating the globe.\n\
\n\
Mouse buttons:\n\
\n\
  Left:  zoom in on the clicked region and narrow the location list.\n\
  You can zoom in 6 times for a maximum 64x magnification, after which\n\
  the left mouse button behaves just like the right mouse button.\n\
\n\
  Right:  narrow the location list to the clicked area.  A circle will\n\
  be drawn on the globe showing the radius included, but no zooming\n\
  will occur.\n\
\n\
  Middle:  select a tide station.  (You can also select a tide station\n\
  by left-clicking on it in the location list.)\n\
\n\
Buttons in the globe window:\n\
\n\
  List All:  include all available tide stations in the location list,\n\
  even those whose latitude and longitude are unknown (null).\n\
\n\
  Zoom Out:  self-explanatory.  Sufficient usage will return to the\n\
  hemisphere view.\n\
\n\
  Flat:  change to a flat map projection.\n\
\n\
  Dismiss:  remove the location chooser.  Any windows containing tide\n\
  predictions will remain.\n\
\n\
Keyboard:\n\
\n\
  Arrow keys:  rotate North/South/East/West.");
  (void) globe->xtidecontext->root->newHelpBox (helpstring);
}

void
xxGlobeListAllCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxGlobe *globe = (xxGlobe *)client_data;
  globe->locationlist->changeList (globe->stationIndex->clone());
}

void
xxGlobeZoomOutCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxGlobe *globe = (xxGlobe *)client_data;
  if (globe->size == min_globe_size)
    return;
  if (globe->size / zoomfactor > min_globe_size) {
    double cx = (double)(globe->xorigin + min_globe_size/2) / (double)globe->size;
    double cy = (double)(globe->yorigin + min_globe_size/2) / (double)globe->size;
    globe->size = (unsigned)(globe->size / zoomfactor);
    globe->xorigin = (int)(cx * globe->size - min_globe_size / 2);
    globe->yorigin = (int)(cy * globe->size - min_globe_size / 2);
  } else {
    globe->size = min_globe_size;
    globe->xorigin = globe->yorigin = 0;
  }
  globe->redrawGlobe();
}

// This is done by the flat window image and not by spherical projection.
// It's simpler this way.
void
xxGlobe::blast (long x, long y) {
  if (blastflag)
    if (inrange (blastx, blasty) && inrange (blastx+blastradius*2, blasty+blastradius*2))
      XDrawArc (picture->display, globepixmap, picture->invert_gc, blastx,
        blasty, blastradius*2, blastradius*2, 0, 360*64);

  blastflag = 1;
  blastx = x - blastradius;
  blasty = y - blastradius;
  if (inrange (blastx, blasty) && inrange (blastx+blastradius*2, blasty+blastradius*2))
    XDrawArc (picture->display, globepixmap, picture->invert_gc, blastx,
      blasty, blastradius*2, blastradius*2, 0, 360*64);

  Arg args[1] = {
    {"bitmap", (XtArgVal)globepixmap}
  };
  XtSetValues (picture->manager, args, 1);
  picture->refresh();

  // Make a list of all locations within the blast radius.
  long bx = x + xorigin;
  long by = y + yorigin;
  unsigned long a;
  StationIndex *si = new StationIndex();
  for (a=0; a<stationIndex->length(); a++) {
    long x, y;
    if (projectCoordinates ((*(stationIndex))[a]->coordinates, &x, &y)) {
      double dx = (double)x - (double)bx;
      double dy = (double)y - (double)by;
      if (sqrt (dx*dx + dy*dy) <= blastradius)
	si->add ((*(stationIndex))[a]);
    }
  }
  locationlist->changeList (si);
}

void xxGlobeKeyboardEventHandler (Widget w, XtPointer client_data,
				  XEvent *event, Boolean *continue_dispatch) {
  xxGlobe *globe = (xxGlobe *)client_data;
  XKeyEvent *xke = (XKeyEvent *)event;
  KeySym foo = XLookupKeysym (xke, 0); // Index 0 = no shift/ctrl/meta/etc.

  double lat, lng;

  switch (foo) {
  case XK_Left:
  case XK_KP_Left:
    assert (globe->untranslateCoordinates (min_globe_size/4, min_globe_size/2, lat, lng));
    globe->center_longitude = lng;
    break;
  case XK_Up:
  case XK_KP_Up:
    assert (globe->untranslateCoordinates (min_globe_size/2, min_globe_size/4, lat, lng));
    if (lat > globe->center_latitude)
      globe->center_latitude = lat;
    else
      globe->center_latitude = 90;
    break;
  case XK_Right:
  case XK_KP_Right:
    assert (globe->untranslateCoordinates (3*min_globe_size/4, min_globe_size/2, lat, lng));
    globe->center_longitude = lng;
    break;
  case XK_Down:
  case XK_KP_Down:
    assert (globe->untranslateCoordinates (min_globe_size/2, 3*min_globe_size/4, lat, lng));
    if (lat < globe->center_latitude)
      globe->center_latitude = lat;
    else
      globe->center_latitude = -90;
    break;
  default:
    return;
  }
  globe->redrawGlobe();
}

void
xxGlobeButtonPressEventHandler (Widget w, XtPointer client_data,
			 XEvent *event, Boolean *continue_dispatch) {
  xxGlobe *globe = (xxGlobe *)client_data;
  XButtonEvent *xbe = (XButtonEvent *)event;

  // Coordinates relative to upper left corner of globe pixmap.
  // Need to subtract the internal margin of the label widget.
  long rx = xbe->x - globe->internalWidth;
  long ry = xbe->y - globe->internalHeight;

  // Relative to entire globe.
  long bx = rx + globe->xorigin;
  long by = ry + globe->yorigin;

  if (xbe->button == Button3 ||
      (xbe->button == Button1 && globe->size == max_globe_size)) {
    globe->blast (rx, ry);
    return;
  }

  // Zoom
  if (xbe->button == Button1) {

#if 0
    // This is the old code that pans instead of rotates.
    double cx = (double)bx / (double)globe->size;
    double cy = (double)by / (double)globe->size;
    globe->xorigin = (int)(cx * globe->size - min_globe_size / 2);
    globe->yorigin = (int)(cy * globe->size - min_globe_size / 2);
#endif

    double lat, lng;
    if (globe->untranslateCoordinates (rx, ry, lat, lng)) {
      globe->center_latitude = lat;
      globe->center_longitude = lng;
      if (globe->size * zoomfactor < max_globe_size)
        globe->size = (unsigned)(zoomfactor * globe->size);
      else
        globe->size = max_globe_size;
      globe->xorigin = globe->yorigin = (int)(globe->size - min_globe_size)/2;
      globe->redrawGlobe();
    }
    return;
  }

  // Load location
  if (xbe->button == Button2) {
    // Find nearest location that is close enough (4 pixels).
    unsigned long a;
    StationRef *closestsr = NULL;
    double d = 17.0;
    for (a=0; a<globe->stationIndex->length(); a++) {
      long x, y;
      if (globe->projectCoordinates ((*(globe->stationIndex))[a]->coordinates,
	&x, &y)) {
	double dx = (double)x - (double)bx;
	double dy = (double)y - (double)by;
        double dd = dx*dx + dy*dy;
        if (dd < d) {
          d = dd;
          closestsr = (*(globe->stationIndex))[a];
        }
      }
    }
    if (closestsr)
      globe->xtidecontext->root->newGraph (closestsr);
  }

  // X11 has Button4 and Button5... go figure.
}

void
xxGlobe::dismiss () {
  delete this;
}

xxGlobe::xxGlobe (xxContext *context, StationIndex &in_stationIndex,
xxTideContext *in_tidecontext): xxWindow (in_tidecontext, context, 1) {
  mypopup->setTitle ("Globe");
  size = min_globe_size;
  xorigin = yorigin = blastflag = 0;
  stationIndex = &in_stationIndex;

  globepixmap = mypopup->makePixmap (min_globe_size+1, min_globe_size+1);
  Arg args[3] = {
    {XtNbitmap, (XtArgVal)globepixmap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  Widget picturewidget = XtCreateManagedWidget ("", labelWidgetClass,
    container->manager, args, 3);
  picture = new xxContext (container, picturewidget);
  XtAddEventHandler (picturewidget, PointerMotionMask, False,
    xxGlobePointerMotionEventHandler, (XtPointer)this);
  XtAddEventHandler (picturewidget, ButtonPressMask, False,
    xxGlobeButtonPressEventHandler, (XtPointer)this);
  XtAddEventHandler (picturewidget, KeyPressMask, False,
    xxGlobeKeyboardEventHandler, (XtPointer)this);

  {
    Arg args[2] = {
      {XtNinternalHeight, (XtArgVal)(&internalHeight)},
      {XtNinternalWidth, (XtArgVal)(&internalWidth)}
    };
    XtGetValues (picture->manager, args, 2);
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Globe internal height " << internalHeight <<
      " internal width " << internalWidth << endl;
#endif
  }

  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };

  {
    Widget buttonwidget = XtCreateManagedWidget ("List All",
      commandWidgetClass, container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxGlobeListAllCallback,
      (XtPointer)this);
    listAllButton = new xxContext (mypopup, buttonwidget);
  }{
    Widget buttonwidget = XtCreateManagedWidget ("Zoom Out",
      commandWidgetClass, container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxGlobeZoomOutCallback,
      (XtPointer)this);
    zoomoutbutton = new xxContext (mypopup, buttonwidget);
  }{
    Widget buttonwidget = XtCreateManagedWidget ("Flat", commandWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxGlobeFlatCallback,
      (XtPointer)this);
    flatbutton = new xxContext (mypopup, buttonwidget);
  }{
    Widget buttonwidget = XtCreateManagedWidget ("Dismiss", commandWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, dismissCallback,
      (XtPointer)this);
    dismissbutton = new xxContext (mypopup, buttonwidget);
  }{
    Widget buttonwidget = XtCreateManagedWidget ("?", commandWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxGlobeHelpCallback,
      (XtPointer)this);
    helpbutton = new xxContext (mypopup, buttonwidget);
  }{
    Arg latargs[2] =
      {
	{XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
	{XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
      };
    Widget lattextwidget = XtCreateManagedWidget ("Lat -00.0000",
      labelWidgetClass, container->manager, latargs, 2);
    lattext = new xxContext (mypopup, lattextwidget);
  }{
    Arg lonargs[2] =
      {
	{XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
	{XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
      };
    Widget lontextwidget = XtCreateManagedWidget ("Lon -000.0000",
      labelWidgetClass, container->manager, lonargs, 2);
    lontext = new xxContext (mypopup, lontextwidget);
  }

  mypopup->realize();
  mypopup->fixSize();

  Configurable &gl = (*(in_tidecontext->settings))["gl"];
  if (gl.isnull || gl.d == 360.0)
    center_longitude = stationIndex->bestCenterLongitude();
  else
    center_longitude = gl.d;
  center_latitude = 0.0;

  // Start with an empty list since it's going to get clobbered in
  // redrawGlobe anyway.
  locationlist = new xxLocationList (mypopup, new StationIndex(),
    xtidecontext, this);
  redrawGlobe();
}

xxGlobe::~xxGlobe () {
  mypopup->unrealize();
  delete locationlist;
  delete flatbutton;
  delete dismissbutton;
  delete helpbutton;
  delete listAllButton;
  delete picture;
  delete lattext;
  delete lontext;
  XFreePixmap (mypopup->display, globepixmap);
}

void xxGlobe::global_redraw() {
  xxWindow::global_redraw();
  redrawGlobe();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (flatbutton)
    XtSetValues (flatbutton->manager, buttonargs, 2);
  if (dismissbutton)
    XtSetValues (dismissbutton->manager, buttonargs, 2);
  if (listAllButton)
    XtSetValues (listAllButton->manager, buttonargs, 2);
  if (helpbutton)
    XtSetValues (helpbutton->manager, buttonargs, 2);
  if (zoomoutbutton)
    XtSetValues (zoomoutbutton->manager, buttonargs, 2);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (lattext)
    XtSetValues (lattext->manager, args, 2);
  if (lontext)
    XtSetValues (lontext->manager, args, 2);
  if (picture)
    XtSetValues (picture->manager, args, 2);
}
