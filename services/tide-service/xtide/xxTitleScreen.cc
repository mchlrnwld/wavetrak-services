// $Id: xxTitleScreen.cc,v 1.2 2002/10/04 13:44:01 flaterco Exp $
/*  xxTitleScreen  XTide title screen.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"
#include "titlescreen.xpm.hh"

// The PNG title screen has too much overhead.
// #include "titlescreen.png.hh"

void
xxTitleScreen::updateProgress (Dstr &in_fname) {
  static unsigned hindex = 0;
  Dstr fname (in_fname), temp;
  int index = fname.strrchr ('/');
  if (index != -1) {
    temp = fname.ascharfrom (index+1);
    fname = temp;
  };
  temp = "Indexing ";
  temp += fname;
  temp += "...";
  XDrawString (mypopup->display,
    titlescreenpixmap, mypopup->text_gc,
    5, 80+hindex*12, temp.aschar(), temp.length());

  // You really have to pound on it to get it to refresh.
  Arg args1[1] = {
    {XtNbackgroundPixmap, (XtArgVal)0}
  };
  XtSetValues (mypopup->manager, args1, 1);
  Arg args2[1] = {
    {XtNbackgroundPixmap, (XtArgVal)titlescreenpixmap}
  };
  XtSetValues (mypopup->manager, args2, 1);
  mypopup->refresh();
  hindex++;
}

void
xxTitleScreen::dismiss() {
  // Ya jerk....
  exit (0);
}

xxTitleScreen::xxTitleScreen (xxTideContext *in_xtidecontext, xxContext
*context, XtGrabKind in_grabkind):
xxWindow (in_xtidecontext, context, 0, in_grabkind) {
  is_title_screen = 1;
  mypopup->setTitle ("Title Screen");

// The PNG title screen has too much overhead.
//   if (mypopup->display_sucks) {
    if (!(titlescreenpixmap = mypopup->makePixmap (titlescreen_xpm))) {
      fprintf (stderr, "XTide Warning: can't create pixmap for title screen (can't get enough colors).\n");
      // Substitute a blank pixmap
      titlescreenpixmap = mypopup->makePixmap (titlescreenwidth, titlescreenheight);
    }
//   } else {
//     titlescreenpixmap = mypopup->makePixmap (titlescreen_png);
//   }

  Dstr title;
  version_string (title);
  mypopup->textBigFont();
  XDrawString (mypopup->display,
               titlescreenpixmap,
               mypopup->text_gc,
               25, 20, title.aschar(), title.length());

  mypopup->textSmallFont();
  title = "(C) 1998 David Flater.  NO WARRANTY;";
  XDrawString (mypopup->display,
               titlescreenpixmap,
               mypopup->text_gc,
               5, 35, title.aschar(), title.length());
  title = "see the GNU General Public License";
  XDrawString (mypopup->display,
               titlescreenpixmap,
               mypopup->text_gc,
               5, 47, title.aschar(), title.length());
  title = "for details.";
  XDrawString (mypopup->display,
               titlescreenpixmap,
               mypopup->text_gc,
               5, 59, title.aschar(), title.length());

  Arg args[3] = {
    {XtNbackgroundPixmap, (XtArgVal)titlescreenpixmap},
    {XtNwidth, (XtArgVal)titlescreenwidth},
    {XtNheight, (XtArgVal)titlescreenheight}
  };
  XtSetValues (mypopup->manager, args, 3);

  mypopup->realize();
  mypopup->fixSize();
}

xxTitleScreen::~xxTitleScreen () {
  mypopup->unrealize();
  XFreePixmap (mypopup->display, titlescreenpixmap);
}
