// $Id: xxXTideRoot.hh,v 1.10 2004/09/09 13:33:02 flaterco Exp $
/*  xxXTideRoot  XTide "root" window (control panel, top-level logic)

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxXTideRoot {

  friend void RootCloseHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch);

public:
  xxXTideRoot (int in_argc, char **in_argv);
  void mainloop();
  ~xxXTideRoot ();

  xxWindow *newHelpBox (const Dstr &help);
  xxWindow *newAbout (const Station *s);

  // For the rest, you'd better clone the station.
  xxDrawable *newGraph (Station *s, Timestamp t);
  xxDrawable *newGraph (StationRef *sr); // Default to now
  xxDrawable *newPlain (Station *s, Timestamp t);
  xxDrawable *newRaw (Station *s, Timestamp t);
  xxDrawable *newMediumRare (Station *s, Timestamp t);
  xxDrawable *newClock (Station *s, int nobuttonsflag);
  xxDrawable *newClock (Station *s); // Default to settings

  // newChooser follows user preference, while newMap and newGlobe
  // are specific.
  // The grabkind here is for the title screen if one is needed.
  void newChooser (XtGrabKind in_grabkind = XtGrabExclusive);
  void newMap (XtGrabKind in_grabkind = XtGrabExclusive);
  void newGlobe (XtGrabKind in_grabkind = XtGrabExclusive);

  // Start clock, graph, or plain windows as requested on command line.
  void commandLineWindows ();

  // These are only used in xxWindow.
  // Increment count of pop-ups.
  void dup(xxWindow *child);
  // Decrement count of pop-ups, and exit when zero.
  void release (xxWindow *child);

  struct cnode {
    xxWindow *child;
    struct cnode *next;
  };

  void show();          // Make control panel visible.
  void hide();          // Make control panel invisible.
  void global_redraw(); // Redraw all windows.
  void apply (int saveflag = 0);

protected:
  void dup();
  void release(int is_title_screen);
  void release (xxContext *child, int is_title_screen);
  void addTwoButtons();
  xxXTideRoot::cnode *children;
  unsigned numpopups;
  xxContext *mycontext, *form, *viewport, *viewbox, *dismissbutton,
    *helpbutton, *label, *applybutton, *savebutton;

  // Map from switchname to a pointer to an xxMultiChoice, xxHorizDialog,
  // or xxUnsignedChooser, as appropriate.
  std::map<std::string, void*> dialogs;

  xxTideContext *myTideContext;
  int sancheckdouble (Dstr &num);
};
