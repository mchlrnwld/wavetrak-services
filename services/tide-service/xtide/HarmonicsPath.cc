// $Id: HarmonicsPath.cc,v 1.2 2002/10/04 13:44:00 flaterco Exp $
/*  HarmonicsPath  Storage of harmonics file names

    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

HarmonicsPath::HarmonicsPath (Dstr &hfile_path) {
  pathlen = hfile_path.repchar (':', ' ') + 1;
  storage = new Dstr[pathlen];
  for (unsigned i=0; i<pathlen; i++)
    hfile_path /= storage[i];
}

HarmonicsPath::~HarmonicsPath() {
  delete [] storage;
}

unsigned HarmonicsPath::length() {
  return pathlen;
}

Dstr &HarmonicsPath::operator[] (unsigned index) {
  assert (index < pathlen);
  return storage[index];
}

#if 0
ostream &operator<< (ostream &out, HarmonicsPath h) {
  out << "Contents of HarmonicsPath:" << endl;
  unsigned i;
  for (i=0; i<h.length(); i++)
    out << "  " << h[i] << endl;
  return out;
}
#endif
