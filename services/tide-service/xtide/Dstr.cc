// $Id: Dstr.cc,v 1.7 2004/04/08 14:25:04 flaterco Exp $
// Dstr:  Dave's String class.
// This source is shared among several of my projects and is public domain.
// Operations are added as needed and there is not necessarily a canonical
// version.

#include "common.hh"
#include "collation.hh"

Dstr::Dstr () {
  theBuffer = NULL;
}

Dstr::Dstr (const char *val) {
  if (val) {
    theBuffer = strdup (val);
    used = strlen (val);
    max = used + 1;
  } else
    theBuffer = NULL;
}

Dstr::Dstr (char val) {
  char t[2];
  t[0] = val;
  t[1] = '\0';
  theBuffer = strdup (t);
  used = 1;
  max = 2;
}

Dstr::Dstr (const Dstr &val) {
  if (!(val.isNull())) {
    theBuffer = val.asdupchar();
    used = val.length();
    max = used + 1;
  } else
    theBuffer = NULL;
}

Dstr::~Dstr () {
  if (theBuffer)
    free (theBuffer);
}

// This sped up the indexing step on my PC from 14 seconds to 12 seconds,
// so it's worth keeping, but the primary time waster is all the effort
// required to extract coordinates and build the StationRefs.
Dstr&
Dstr::getline (FILE *fp) {
  char buf[82], *ret;
  if (!(ret = fgets (buf, 82, fp)))
    (*this) = (char *)NULL;
  else {
    (*this) = "";
    while (ret) {
      (*this) += buf;
      if (used > 0)
        if (theBuffer[used-1] == '\n') {
          (*this) -= used-1;
          break;
        }
      ret = fgets (buf, 82, fp);
    }
  }
  return (*this);
}

void Dstr::getline (Dstr &line_out) {
  line_out = (char *)NULL;
  while (((*this).length() > 0) && (*this)[0] != '\n') {
    line_out += (*this)[0];
    (*this) /= 1;
  }
  if ((*this)[0] == '\n')
    (*this) /= 1;
}

// Scan a string like fscanf (fp, "%s")
Dstr&
Dstr::scan (FILE *fp) {
  int c;
  (*this) = (char *)NULL;
  // Skip whitespace
  do {
    c = getc(fp);
    if (c == EOF)
      return (*this);
  } while (isspace (c));
  // Get the string
  (*this) = (char)c;
  while (1) {
    c = getc(fp);
    if (c == EOF)
      return (*this);
    if (isspace (c))
      return (*this);
    (*this) += (char)c;
  }
}

Dstr &
Dstr::pad (unsigned to_length) {
  while (length() < to_length)
    (*this) += " ";
  return (*this);
}

Dstr &Dstr::trim () {
  while (isspace ((*this)[0]))
    (*this) /= 1;
  while (isspace ((*this)[this->length()-1]))
    (*this) -= (this->length()-1);
  return (*this);
}

Dstr&
Dstr::operator-= (unsigned at_index) {
  if (theBuffer) {
    if (at_index < used) {
      theBuffer[at_index] = '\0';
      used = at_index;
    }
  }
  return (*this);
}

int
Dstr::strchr (char val) const {
  if (!theBuffer)
    return -1;
  char *c = ::strchr (theBuffer, val);
  if (!c)
    return -1;
  return (c - theBuffer);
}

int
Dstr::strrchr (char val) const {
  if (!theBuffer)
    return -1;
  char *c = ::strrchr (theBuffer, val);
  if (!c)
    return -1;
  return (c - theBuffer);
}

int
Dstr::strstr (const Dstr &val) const {
  if (!theBuffer)
    return -1;
  if (!val.theBuffer)
    return -1;
  char *c = ::strstr (theBuffer, val.theBuffer);
  if (!c)
    return -1;
  return (c - theBuffer);
}

int Dstr::strcasestr (const Dstr &val) const {
  if (!theBuffer)
    return -1;
  if (!val.theBuffer)
    return -1;
  if (val.length() == 0)
    return 0;
  int i;
  for (i=0; i<(int)used-(int)(val.length())+1; i++)
    if (!slackcmp (theBuffer+i, val.aschar()))
      return i;
  return -1;
}

int
Dstr::strstr (const char *val) const {
  Dstr temp (val);
  return strstr (temp);
}

Dstr &
Dstr::operator= (const char *val) {
  if (theBuffer)
    free (theBuffer);
  if (val) {
    theBuffer = strdup (val);
    used = strlen (val);
    max = used + 1;
  } else
    theBuffer = NULL;
  return (*this);
}

Dstr&
Dstr::operator= (char val) {
  char t[2];
  t[0] = val;
  t[1] = '\0';
  (*this) = t;
  return (*this);
}

Dstr &
Dstr::operator= (const Dstr &val) {
  (*this) = val.theBuffer;
  return (*this);
}

Dstr&
Dstr::operator= (int val) {
  char t[80];
  sprintf (t, "%d", val);
  (*this) = t;
  return (*this);
}

Dstr&
Dstr::operator= (unsigned int val) {
  char t[80];
  sprintf (t, "%u", val);
  (*this) = t;
  return (*this);
}

Dstr&
Dstr::operator= (long int val) {
  char t[80];
  sprintf (t, "%ld", val);
  (*this) = t;
  return (*this);
}

Dstr&
Dstr::operator= (long unsigned int val) {
  char t[80];
  sprintf (t, "%lu", val);
  (*this) = t;
  return (*this);
}

Dstr&
Dstr::operator= (long long int val) {
  char t[80];
  sprintf (t, "%lld", val);
  (*this) = t;
  return (*this);
}

Dstr&
Dstr::operator= (double val) {
  char t[80];
  sprintf (t, "%f", val);
  (*this) = t;
  return (*this);
}

Dstr&
Dstr::operator+= (const char *val) {
  if (val) {
    if (!theBuffer)
      (*this) = val;
    else {
      unsigned l;
      if ((l = strlen (val))) {
        while (l + used >= max) {  // Leave room for terminator
          // Expand
          max *= 2;
          assert (theBuffer = (char *) realloc (theBuffer, max*sizeof(char)));
        }
        strcpy (theBuffer+used, val);
        used += l;
      }
    }
  }
  return (*this);
}

Dstr&
Dstr::operator+= (char val) {
  char t[2];
  t[0] = val;
  t[1] = '\0';
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator+= (const Dstr &val) {
  (*this) += val.theBuffer;
  return (*this);
}

Dstr&
Dstr::operator+= (int val) {
  char t[80];
  sprintf (t, "%d", val);
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator+= (unsigned int val) {
  char t[80];
  sprintf (t, "%u", val);
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator+= (long int val) {
  char t[80];
  sprintf (t, "%ld", val);
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator+= (long unsigned int val) {
  char t[80];
  sprintf (t, "%lu", val);
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator+= (long long int val) {
  char t[80];
  sprintf (t, "%lld", val);
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator+= (double val) {
  char t[80];
  sprintf (t, "%f", val);
  (*this) += t;
  return (*this);
}

Dstr&
Dstr::operator*= (const char *val) {
  Dstr temp (*this);
  (*this) = val;
  (*this) += temp;
  return (*this);
}

Dstr&
Dstr::operator*= (char val) {
  Dstr temp (*this);
  (*this) = val;
  (*this) += temp;
  return (*this);
}

Dstr&
Dstr::operator*= (const Dstr &val) {
  Dstr temp (*this);
  (*this) = val;
  (*this) += temp;
  return (*this);
}

Dstr&
Dstr::operator/= (unsigned at_index) {
  if (theBuffer) {
    Dstr temp ((*this).ascharfrom(at_index));
    (*this) = temp;
  }
  return (*this);
}

Dstr&
Dstr::operator/= (Dstr &val) {
  val = (char *)NULL;
  if (theBuffer) {
    // Eat whitespace
    while (used > 0 && isspace ((*this)[0]))
      (*this) /= 1;
    // Anything left?
    if (used == 0) {
      // Nothing left.
      (*this) = (char *)NULL;
    } else {
      if ((*this)[0] == '"') {
        // Delimited argument
        val += (*this)[0];
        (*this) /= 1;
        while (used > 0 && (*this)[0] != '"') {
          val += (*this)[0];
          (*this) /= 1;
        }
        // Grab the matching quote, if any.
        if (used > 0) {
          val += (*this)[0];
          (*this) /= 1;
        }
      } else {
        // Undelimited argument
        while (used > 0 && (!(isspace ((*this)[0])))) {
          val += (*this)[0];
          (*this) /= 1;
        }
      }
    }
  }
  return (*this);
}

char
Dstr::operator[] (unsigned at_index) const {
  if (!theBuffer)
    return '\0';
  if (at_index >= used)
    return '\0';
  return theBuffer[at_index];
}

unsigned
Dstr::repchar (char X, char Y) {
  unsigned i, repcount=0;
  for (i=0; i<length(); i++)
    if (theBuffer[i] == X) {
      theBuffer[i] = Y;
      repcount++;
    }
  return repcount;
}

unsigned
Dstr::length () const {
  if (!theBuffer)
    return 0;
  return used;
}

int
Dstr::isNull () const {
  if (theBuffer)
    return 0;
  return 1;
}

double
Dstr::asdouble() const {
  double t;
  if (sscanf (aschar(), "%lf", &t) != 1)
    fprintf (stderr, "Warning:  illegal conversion of Dstr to double\n");
  return t;
}

char *
Dstr::aschar () const {
  if (theBuffer)
    return theBuffer;
  return "";
}

char *
Dstr::asdupchar() const {
  return strdup (aschar());
}

char *
Dstr::ascharfrom(unsigned from_index) const {
  if (!theBuffer)
    return "";
  if (from_index >= used)
    return "";
  return theBuffer + from_index;
}

char *
Dstr::asrawchar () const {
  return theBuffer;
}

int operator== (const Dstr &val1, const char *val2) {
  if ((!val2) && (val1.isNull()))
    return 1;
  if ((!val2) || (val1.isNull()))
    return 0;
  return (!(strcmp (val1.aschar(), val2)));
}

int operator== (const char *val1, const Dstr &val2) {
  return (val2 == val1);
}

int operator== (const Dstr &val1, const Dstr &val2) {
  return (val1 == val2.asrawchar());
}

int operator!= (const char *val1, const Dstr &val2) {
  return (!(val1 == val2));
}

int operator!= (const Dstr &val1, const char *val2) {
  return (!(val1 == val2));
}

int operator!= (const Dstr &val1, const Dstr &val2) {
  return (!(val1 == val2));
}

Dstr &
Dstr::lowercase() {
  unsigned i;
  for (i=0; i<length(); i++)
    theBuffer[i] = tolower(theBuffer[i]);
  return (*this);
}

Dstr &
Dstr::rfc2445_mangle() {
  Dstr temp;
  unsigned i;
  for (i=0; i<length(); i++) {
    switch (theBuffer[i]) {
    case ';':
    case '\\':
    case ',':
      temp += '\\';
      temp += theBuffer[i];
      break;
    case '\n':
      temp += "\\n";
      break;
    default:
      temp += theBuffer[i];
    }
  }
  (*this) = temp;
  return (*this);
}

int dstrcasecmp (const Dstr &val1, const Dstr &val2) {
  unsigned n = min (val1.length(), val2.length());
  unsigned i;
  int c, ii;
  for (i=0; i<n; i++) {
    ii = (int)(val1[i]);
    if (ii < 0)
      ii += 256;
    c = collation[ii];
    ii = (int)(val2[i]);
    if (ii < 0)
      ii += 256;
    c -= collation[ii];
    if (c)
      return c;
  }
  c = (int)(val1.length()) - (int)(val2.length());
  return c;
}

/* Slackful strcmp; 0 = match.  It's case-insensitive and accepts a
   prefix instead of the entire string.  The second argument is the
   one that can be shorter. */
int
slackcmp (char *a, char *b)
{
  int c, n, ii, i;
  n = strlen (b);
  if ((int)(strlen (a)) < n)
    return 1;
  for (i=0;i<n;i++) {
    ii = (int)(a[i]);
    if (ii < 0)
      ii += 256;
    c = collation[ii];
    ii = (int)(b[i]);
    if (ii < 0)
      ii += 256;
    c -= collation[ii];
    if (c)
      return c;
  }
  return 0;
}

int operator%= (const Dstr &a, const Dstr &b) {
  return !(slackcmp (a.aschar(), b.aschar()));
}

int dstrcasecmp (const Dstr &val1, char *val2) {
  Dstr temp (val2);
  return dstrcasecmp (val1, temp);
}
