// $Id: Year.hh,v 1.2 2002/12/19 18:40:45 flaterco Exp $
// Year

// The original rationale for this class had to do with enforcing the
// epoch limitation (1970-2037).  Now it's pretty pointless except
// maybe for overload resolution.

/*
    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Year {
protected:
  unsigned value;
  friend int operator == (Year y1, Year y2);
public:
  Year ();
  Year (unsigned year);
  unsigned val();
  // Year &operator++ ();
  int isNull();
};

Year operator+ (Year y, int n);
Year operator- (Year y, int n);

// These will barf if either year is NULL.
int operator < (Year y1, Year y2);
int operator > (Year y1, Year y2);
int operator >= (Year y1, Year y2);
int operator <= (Year y1, Year y2);

// This allows nulls to equal nulls, and nothing else.
int operator == (Year y1, Year y2);
int operator != (Year y1, Year y2);
