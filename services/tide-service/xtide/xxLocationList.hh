// $Id: xxLocationList.hh,v 1.1 2002/04/29 15:10:41 flaterco Exp $
/*  xxLocationList   Scrolling location chooser.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxTideContext;
class xxMultiChoice;

class xxLocationList: public xxWindow {
  friend void sortbycallback (void *cbdata);
public:
  xxLocationList (xxContext *context, StationIndex *in_stationIndex,
    xxTideContext *in_tidecontext, xxWindow *globe_or_map);
  ~xxLocationList ();
  void changeList (StationIndex *in_stationIndex);
  void global_redraw();
  void dismiss();
protected:
  friend void xxLocationListButtonCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxLocationListChoiceCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxLocationListHelpCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  void refreshList();
  StationIndex *stationIndex;
  xxContext *viewport, *list, *label, **contbuttons, *helpbutton;
  xxMultiChoice *sortchoice;
  xxWindow *myglobe;
  int numcontbuttons;
  char **stringlist;
  unsigned long indexlen, startat;
  void freestringlist();
  void do_contbuttons();
  void listchanged();
  StationIndex::sortfield sortby;
};
