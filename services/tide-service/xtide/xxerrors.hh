// errors:  Global functions for bailing out -- X-capable version.
// Last modified by DWF 1998-01-27

class xxContext;
class xxTideContext;

// Set context for creating error boxes.
void set_error_context (xxContext *in_context, xxTideContext *in_xtidecontext);
