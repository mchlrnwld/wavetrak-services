// $Id: TideContext.hh,v 1.8 2004/04/16 21:45:32 flaterco Exp $
/*  TideContext  In lieu of global variables and functions.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class TideContext {
public:
  TideContext (Colors *in_colors, Settings *in_settings);
  virtual ~TideContext();

  Settings *settings;
  Colors *colors;
  HarmonicsPath *harmonicsPath;
  Dstr homedir, disabledisclaimerfile;

  // If there is no index yet, this function will build one.
  // This is overridden in xxTideContext but it's NOT VIRTUAL.
  // (Different signatures.)
  // It only matters on the first invocation, since that is the
  // only one where there might be a title screen popped up.
  StationIndex *stationIndex();

  // Generate plain mode output.
  // Legal forms are c (CSV), t (text) or i (iCalendar).
  //
  // iCalendar format output is actually produced by plainMode.  From
  // an engineering perspective this makes perfect sense.  But from a
  // usability perspective, iCalendar output is a calendar and ought
  // to appear in calendar mode.  So calendarMode falls through to
  // plainMode when i format is chosen.
  void plainMode (Station *station, Timestamp start_tm, Timestamp end_tm,
    Dstr &text_out, char form);
  void plainMode (Station *station, Timestamp start_tm, Dstr &text_out,
    char form);

  // Other modes.

  // Legal forms are c, h, i, or t.
  void calendarMode (Station *station, Timestamp start_tm, Timestamp end_tm,
    Dstr &text_out, char form, int oldcal);

  // Legal modes are m and r.
  // Legal forms are c and t.
  // Step should be set on station.
  void rareModes (Station *station, Timestamp start_tm, Timestamp end_tm,
		  Dstr &text_out, char mode, char form);

  void listModePlain (Dstr &text_out);
  void listModeHTML (Dstr &text_out);
  void bannerMode (Station *station, Timestamp start_tm, Timestamp end_tm,
    Dstr &text_out);
  void statsMode (Station *station, Timestamp start_tm, Timestamp end_tm,
    Dstr &text_out);

  void startLocListHTML (Dstr &d);
  void listLocationHTML (Dstr &d, StationRef *sr);
  void endLocListHTML (Dstr &d);

protected:
  StationIndex *myStationIndex;

  // Legal forms are h, i, or t
  void text_boilerplate (Station *station, Dstr &text_out, char form);

  // This is overridden in xxTideContext but it's NOT VIRTUAL.
  // (Different signatures.)
  void doHarmonicsFile (Dstr &fname);
  StationRef *doFastloadHarmonicsFile (Dstr &fname, const Dstr &stationname);
};

void version_string (Dstr &text_out);
