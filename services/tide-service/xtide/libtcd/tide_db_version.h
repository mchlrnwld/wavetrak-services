/* $Id: tide_db_version.h,v 1.24 2004/10/15 13:58:07 flaterco Exp $ */

/*****************************************************************************\

                            DISTRIBUTION STATEMENT

    This source file is unclassified, distribution unlimited, public
    domain.  It is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

\*****************************************************************************/



#ifndef VERSION

#define VERSION "PFM Software - libtcd V2.0 - 2004-10-15"
#define MAJOR_REV 2
#define MINOR_REV 0

#endif

/* Changelog moved to libtcd.html */
