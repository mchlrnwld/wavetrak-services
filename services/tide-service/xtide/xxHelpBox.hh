// $Id: xxHelpBox.hh,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxHelpBox  Response to pressing a '?' button.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxHelpBox: public xxWindow {
public:
  xxHelpBox (xxTideContext *in_tidecontext,
    xxContext *in_xxcontext, const Dstr &help);
  ~xxHelpBox();
  void global_redraw();
  void dismiss();

protected:
  xxContext *label, *dismissbutton;
};
