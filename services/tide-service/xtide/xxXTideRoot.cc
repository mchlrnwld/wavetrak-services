// $Id: xxXTideRoot.cc,v 1.17 2004/09/10 15:16:31 flaterco Exp $
/*  xxXTideRoot  XTide "root" window

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

static void
dismissCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxXTideRoot*)client_data)->hide();
}

static void
applyCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  ((xxXTideRoot*)client_data)->apply();
}

// This code is pretty much duplicated from xxAspect
int xxXTideRoot::sancheckdouble (Dstr &num) {
  if (num.length() > 0) {
    if (num.strchr ('\n') != -1 ||
           num.strchr ('\r') != -1 ||
           num.strchr (' ') != -1) {
      Dstr details ("Numbers aren't supposed to contain whitespace.  You entered '");
      details += num;
      details += "'.";
      barf (NOT_A_NUMBER, details, 0);
      return 0;
    } else {
      double temp;
      if (sscanf (num.aschar(), "%lf", &temp) != 1) {
	Dstr details ("The offending input was '");
	details += num;
	details += "'.";
	barf (NOT_A_NUMBER, details, 0);
        return 0;
      } else {
        if (temp <= 0.0) {
  	  Dstr details ("The offending input was '");
	  details += num;
	  details += "'.";
	  barf (NUMBER_RANGE_ERROR, details, 0);
          return 0;
        } else
  	  return 1;
      }
    }
  }
  return 1;
}

void xxXTideRoot::apply (int saveflag) {

  // Build new settings for ~/.xtide.xml
  Settings ud;
  ud.nullify();

  ConfigurablesIterator it = ud.begin();
  while (it != ud.end()) {
    Configurable &cfbl = it->second;
    if (cfbl.kind == Configurable::Ksetting) {
      void *vd = dialogs[it->first];
      assert (vd);
      switch (cfbl.interpretation) {
      case Configurable::Iboolean:
        {
          xxMultiChoice &mc = *(xxMultiChoice*)vd;
          switch (mc.choice()) {
          case 0:
            cfbl.isnull = 0;
            cfbl.c = 'y';
            break;
          case 1:
            cfbl.isnull = 0;
            cfbl.c = 'n';
            break;
          case 2:
            break;
          default:
            assert (0);
          }
        }
	break;
      case Configurable::Igldouble:
        {
          xxMultiChoice &mc = *(xxMultiChoice*)vd;
	  switch (mc.choice()) {
	  case 12:
            cfbl.isnull = 0;
            cfbl.d = 360.0;
	    break;
	  case 13:
	    break;
	  default:
	    cfbl.isnull = 0;
	    cfbl.d = (double)(mc.choice()) * 30.0 - 180.0;
	    break;
	  }
        }
	break;
      case Configurable::Iunit:
        {
          xxMultiChoice &mc = *(xxMultiChoice*)vd;
	  switch (mc.choice()) {
	  case 0:
            cfbl.isnull = 0;
	    cfbl.s = "ft";
	    break;
	  case 1:
            cfbl.isnull = 0;
	    cfbl.s = "m";
	    break;
	  case 2:
            cfbl.isnull = 0;
	    cfbl.s = "x";
	    break;
	  case 3:
	    break;
	  default:
	    assert (0);
	  }
        }
	break;
      case Configurable::Iposint:
        {
          xxUnsignedChooser &uc = *(xxUnsignedChooser*)vd;
          if (uc.choice() != 1000000) {
            cfbl.isnull = 0;
            cfbl.u = uc.choice();
          }
        }
	break;
      case Configurable::Iposdouble:
        {
          Dstr dbl (((xxHorizDialog*)vd)->val());
          // Sanity check double.  (A little more user-friendly than
          // the checking in Settings)
          if (!sancheckdouble (dbl))
            return;
          if (dbl.length()) {
            cfbl.isnull = 0;
            assert (sscanf (dbl.aschar(), "%lf", &cfbl.d) == 1);
          }
        }
	break;
      case Configurable::Icolor:
        {
          Dstr color (((xxHorizDialog*)vd)->val());
          if (color.length()) {
            // Sanity check color
            unsigned char r, g, b;
            if (!(myTideContext->colors->parseColor (color, r, g, b, 0)))
              return;
            cfbl.isnull = 0;
            cfbl.s = color;
          }
        }
        break;
      case Configurable::Istrftime:
        {
          Dstr sft (((xxHorizDialog*)vd)->val());
          if (sft.length()) {
            // Sanity check strftime format string
            if (sft.strchr('"') != -1 || sft.strchr('>') != -1 ||
                sft.strchr('<') != -1) {
                  Dstr details ("Please don't use nasty characters like \", >, and < in your date/time formats.");
                  barf (XMLPARSE, details, 0);
                  return;
	    }
            cfbl.isnull = 0;
            cfbl.s = sft;
          }
        }
	break;
      default:
	assert (0);
      }
    }
    it++;
  }

  mycontext->change_settings (myTideContext, ud);
  global_redraw();

  // Provided that the world did not explode as a result of applying those
  // settings, only then might they be saved.
  if (saveflag)
    ud.save();
}

static void
saveCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxXTideRoot *f = (xxXTideRoot *)client_data;
  f->apply (1);
}

static void
helpCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  Dstr helpstring ("\
XTide Control Panel\n\
\n\
The Control Panel is used to change global XTide settings.  These settings\n\
take precedence over compiled-in defaults and X application defaults, but\n\
they are overridden by settings made on the command line.  Therefore, any\n\
settings that you make in the Control Panel will not have any visible effect\n\
if you have also made these settings on the command line.\n\
\n\
Some settings have dialog boxes for you to type in; others have pull-down\n\
menus or '+' and '-' controls.  In each case, there is a way to leave the\n\
field blank if you want to keep the inherited settings.  Either delete all\n\
text in the dialog box, or choose the \"(blank)\" option on the pull-down\n\
menu, or lay on the '-' button until the field reads \"(blank)\".\n\
\n\
The settings identified as \"default\" settings (widths, heights, and\n\
aspect ratio) will not affect existing windows.  Widths and heights will\n\
affect all new windows; aspect ratio will only affect new windows that\n\
are created from the location chooser.  (This is because the aspect is\n\
preserved from one tide window to any new windows that it spawns.)\n\
\n\
Use the Apply button to apply the new settings to the current XTide session\n\
without making them permanent.  Use Save to make them permanent.\n\
\n\
If necessary, resize the Control Panel to keep all of the settings visible.\n\
\n\
About colors:  When entering colors, use either standard X11 color names\n\
or 24-bit hex specs of the form rgb:hh/hh/hh.  Do not use more or less bits;\n\
it will not work.\n\
\n\
About date/time formats:  Please refer to the man page for strftime to see\n\
how these formats work.  Here is how to get XTide to use 24-hour time instead\n\
of AM/PM:\n\
   Hour format  %H\n\
   Time format  %H:%M %Z");
  xxXTideRoot *f = (xxXTideRoot *)client_data;
  (void) f->newHelpBox (helpstring);
}

void RootCloseHandler (Widget w, XtPointer client_data,
			   XEvent *event, Boolean *continue_dispatch) {
  xxXTideRoot *f = (xxXTideRoot *)client_data;
  switch (event->type) {
  case ClientMessage:
    {
      XClientMessageEvent *ev = (XClientMessageEvent *) event;
      // Window manager close.
      if (ev->message_type == f->mycontext->protocol_atom &&
	  ev->data.l[0] == (int)(f->mycontext->kill_atom))
        f->hide();
    }
    break;
  default:
    ;
  }
}

// This is called by the constructor and also by hide()
void
xxXTideRoot::addTwoButtons() {
  {
    Arg buttonargs[10] =  {
      {XtNvisual, (XtArgVal)mycontext->visual},
      {XtNcolormap, (XtArgVal)mycontext->colormap},
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)viewport->manager},
      {XtNfromHoriz, (XtArgVal)savebutton->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainBottom},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget buttonwidget = XtCreateManagedWidget ("Dismiss", commandWidgetClass,
      form->manager, buttonargs, 10);
    XtAddCallback (buttonwidget, XtNcallback, dismissCallback,
     (XtPointer)this);
    dismissbutton = new xxContext (form, buttonwidget);
  }
  {
    Arg buttonargs[10] =  {
      {XtNvisual, (XtArgVal)mycontext->visual},
      {XtNcolormap, (XtArgVal)mycontext->colormap},
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)viewport->manager},
      {XtNfromHoriz, (XtArgVal)dismissbutton->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainBottom},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget buttonwidget = XtCreateManagedWidget ("?", commandWidgetClass,
      form->manager, buttonargs, 10);
    XtAddCallback (buttonwidget, XtNcallback, helpCallback,
      (XtPointer)this);
    helpbutton = new xxContext (form, buttonwidget);
  }
}

xxXTideRoot::xxXTideRoot (int in_argc, char **in_argv) {
  numpopups = 0;
  children = NULL;
  Colors *colors;
  Settings *settings;
  mycontext = new xxContext (in_argc, in_argv, colors, settings);
  mycontext->setTitle ("Control Panel");
  myTideContext = new xxTideContext (mycontext, this, colors, settings);
  set_error_context (mycontext, myTideContext);

  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]}
  };
  XtSetValues (mycontext->manager, args, 2);
  XtAddEventHandler (mycontext->manager, NoEventMask, True,
    RootCloseHandler, (XtPointer)this);

  {
    Arg formargs[3] =  {
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNvSpace, (XtArgVal)0}
    };
    Widget formwidget = XtCreateManagedWidget ("", formWidgetClass,
      mycontext->manager, formargs, 3);
    form = new xxContext (mycontext, formwidget);
  }

  {
    Arg labelargs[6] =  {
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainTop},
      {XtNbottom, (XtArgVal)XawChainTop}
    };
    Widget labelwidget = XtCreateManagedWidget (
      "-------------- XTide Control Panel --------------",
      labelWidgetClass, form->manager, labelargs, 6);
    label = new xxContext (form, labelwidget);
  }

  Arg vpargs[9] =  {
    {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
    {XtNallowVert, (XtArgVal)1},
    {XtNforceBars, (XtArgVal)1},
    {XtNfromVert, (XtArgVal)label->manager},
    {XtNleft, (XtArgVal)XawChainLeft},
    {XtNright, (XtArgVal)XawChainRight},
    {XtNtop, (XtArgVal)XawChainTop},
    {XtNbottom, (XtArgVal)XawChainBottom}
  };

  Widget viewportwidget = XtCreateManagedWidget ("", viewportWidgetClass,
    form->manager, vpargs, 9);
  viewport = new xxContext (form, viewportwidget);

  Widget scrollbarwidget;
  assert (scrollbarwidget = XtNameToWidget (viewportwidget, "vertical"));
  Arg sbargs[2] = {
    {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]}
  };
  XtSetValues (scrollbarwidget, sbargs, 2);

  {
    Arg boxargs[4] =  {
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNvSpace, (XtArgVal)0},
      {XtNorientation, (XtArgVal)XtorientVertical}
    };
    Widget boxwidget = XtCreateManagedWidget ("", boxWidgetClass,
      viewport->manager, boxargs, 4);
    viewbox = new xxContext (viewport, boxwidget);
  }

  // Get current settings.
  Settings ud;
  ud.nullify();
  ud.applyUserDefaults ();

  static char *togglechoices[] = {"Yes", "No", "(blank)", NULL};
  static char *unitschoices[] = {"Feet", "Meters", "No preference",
    "(blank)", NULL};
  static char *glchoices[] = {"180", "150 W", "120 W", "90 W", "60 W",
    "30 W", "0", "30 E", "60 E", "90 E", "120 E", "150 E",
    "Max stations", "(blank)", NULL};

  // Create all of the dialogs.
  ConfigurablesIterator it = ud.begin();
  while (it != ud.end()) {
    Configurable &cfbl = it->second;
    if (cfbl.kind == Configurable::Ksetting) {
      switch (cfbl.interpretation) {
      case Configurable::Iboolean:
        {
          unsigned firstchoice;
          if (cfbl.isnull)
            firstchoice = 2;
          else if (cfbl.c == 'n')
            firstchoice = 1;
          else
            firstchoice = 0;
          dialogs[it->first] = new xxMultiChoice (viewbox,
	    cfbl.caption.aschar(), togglechoices, firstchoice);
        }
        break;
      case Configurable::Iposint:
        dialogs[it->first] = new xxUnsignedChooser (viewbox,
          cfbl.caption.aschar(), cfbl.u, cfbl.isnull, cfbl.min_value);
        break;
      case Configurable::Iposdouble:
        {
          char temp[80];
	  if (cfbl.isnull)
	    strcpy (temp, "");
	  else
	    sprintf (temp, "%f", cfbl.d);
          dialogs[it->first] = new xxHorizDialog (viewbox,
	    cfbl.caption.aschar(), temp);
        }
        break;
      case Configurable::Igldouble:
        {
          unsigned firstchoice = 13;
          if (!cfbl.isnull) {
            if (cfbl.d == 360.0)
              firstchoice = 12;
            else
              firstchoice = (unsigned) ((cfbl.d + 180.0) / 30.0);
          }
          dialogs[it->first] = new xxMultiChoice (viewbox,
	    cfbl.caption.aschar(), glchoices, firstchoice);
        }
        break;
      case Configurable::Icolor: // deliberate fall through
      case Configurable::Istrftime:
        dialogs[it->first] = new xxHorizDialog (viewbox,
  	  cfbl.caption.aschar(), cfbl.isnull ? "" : cfbl.s.aschar());
        break;
      case Configurable::Iunit:
        {
          unsigned firstchoice;
          if (cfbl.isnull)
            firstchoice = 3;
          else if (cfbl.s == "ft")
            firstchoice = 0;
          else if (cfbl.s == "m")
            firstchoice = 1;
          else
            firstchoice = 2;
          dialogs[it->first] = new xxMultiChoice (viewbox,
            cfbl.caption.aschar(), unitschoices, firstchoice);
        }
        break;
      default:
        assert (0);
      }
    }
    it++;
  }

  {
    Arg applyargs[9] =  {
      {XtNvisual, (XtArgVal)mycontext->visual},
      {XtNcolormap, (XtArgVal)mycontext->colormap},
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)viewport->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainBottom},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget buttonwidget = XtCreateManagedWidget ("Apply", commandWidgetClass,
      form->manager, applyargs, 9);
    XtAddCallback (buttonwidget, XtNcallback, applyCallback,
     (XtPointer)this);
    applybutton = new xxContext (form, buttonwidget);
  }
  {
    Arg saveargs[10] =  {
      {XtNvisual, (XtArgVal)mycontext->visual},
      {XtNcolormap, (XtArgVal)mycontext->colormap},
      {XtNbackground, (XtArgVal)mycontext->pixels[Colors::button]},
      {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]},
      {XtNfromVert, (XtArgVal)viewport->manager},
      {XtNfromHoriz, (XtArgVal)applybutton->manager},
      {XtNleft, (XtArgVal)XawChainLeft},
      {XtNright, (XtArgVal)XawChainLeft},
      {XtNtop, (XtArgVal)XawChainBottom},
      {XtNbottom, (XtArgVal)XawChainBottom}
    };
    Widget buttonwidget = XtCreateManagedWidget ("Save", commandWidgetClass,
      form->manager, saveargs, 10);
    XtAddCallback (buttonwidget, XtNcallback, saveCallback,
     (XtPointer)this);
    savebutton = new xxContext (form, buttonwidget);
  }

  addTwoButtons();
}

void
xxXTideRoot::dup() {
  numpopups++;
}

void
xxXTideRoot::dup(xxWindow *child) {
  dup();
  struct cnode *tempc = new cnode;
  tempc->child = child;
  tempc->next = children;
  children = tempc;
}

void xxXTideRoot::release(int is_title_screen) {
  assert (numpopups > 0);
  numpopups--;
  if ((!numpopups) && (!is_title_screen))
    exit (0);
}

void xxXTideRoot::release(xxContext *child, int is_title_screen) {
  XtUnmanageChild (child->manager);
  release(is_title_screen);
}

void xxXTideRoot::release(xxWindow *child) {
  release (child->mypopup, child->is_title_screen);
  struct cnode *tempc = NULL;
  while (children) {
    struct cnode *t2c = children;
    children = children->next;
    if (t2c->child != child) {
      t2c->next = tempc;
      tempc = t2c;
    } else {
      delete t2c;
    }
  }
  children = tempc;
}

void xxXTideRoot::global_redraw() {
  struct cnode *tempc = children;
  while (tempc) {
    tempc->child->global_redraw();
    tempc = tempc->next;
  }

  Settings &settings = *(myTideContext->settings);
  std::map<std::string, void*>::iterator it = dialogs.begin();
  while (it != dialogs.end()) {
    switch (settings[it->first].interpretation) {
    case Configurable::Iboolean:
    case Configurable::Igldouble:
    case Configurable::Iunit:
      ((xxMultiChoice*)(it->second))->global_redraw();
      break;
    case Configurable::Iposint:
      ((xxUnsignedChooser*)(it->second))->global_redraw();
      break;
    case Configurable::Iposdouble:
    case Configurable::Icolor:
    case Configurable::Istrftime:
      ((xxHorizDialog*)(it->second))->global_redraw();
      break;
    default:
      assert (0);
    }
    it++;
  }

  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mycontext->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]}
  };
  if (applybutton)
    XtSetValues (applybutton->manager, buttonargs, 2);
  if (savebutton)
    XtSetValues (savebutton->manager, buttonargs, 2);
  if (helpbutton)
    XtSetValues (helpbutton->manager, buttonargs, 2);
  if (dismissbutton)
    XtSetValues (dismissbutton->manager, buttonargs, 2);

  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mycontext->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mycontext->pixels[Colors::foreground]}
  };
  if (label)
    XtSetValues (label->manager, args, 2);
  if (viewbox)
    XtSetValues (viewbox->manager, args, 2);
  if (viewport)
    XtSetValues (viewport->manager, args, 2);
  if (form)
    XtSetValues (form->manager, args, 2);
  if (mycontext)
    XtSetValues (mycontext->manager, args, 2);
}

xxXTideRoot::~xxXTideRoot () {
  mycontext->unrealize();
  delete applybutton;
  delete savebutton;
  delete helpbutton;
  delete dismissbutton;
  delete label;

  Settings &settings = *(myTideContext->settings);
  std::map<std::string, void*>::iterator it = dialogs.begin();
  while (it != dialogs.end()) {
    switch (settings[it->first].interpretation) {
    case Configurable::Iboolean:
    case Configurable::Igldouble:
    case Configurable::Iunit:
      delete (xxMultiChoice*)(it->second);
      break;
    case Configurable::Iposint:
      delete (xxUnsignedChooser*)(it->second);
      break;
    case Configurable::Iposdouble:
    case Configurable::Icolor:
    case Configurable::Istrftime:
      delete (xxHorizDialog*)(it->second);
      break;
    default:
      assert (0);
    }
    it++;
  }

  delete viewbox;
  delete viewport;
  delete form;
  delete mycontext;
  delete myTideContext;
}

void xxXTideRoot::mainloop() {
  struct stat buf;
  if (stat (myTideContext->disabledisclaimerfile.aschar(), &buf)) {
    (void) new xxDisclaimer (mycontext, myTideContext);
    xxMainLoop (mycontext);
  }

  if ((*(myTideContext->settings))["l"].isnull)
    newChooser (XtGrabNone); // Let the disclaimer scroll.
  else
    commandLineWindows ();

  xxMainLoopForever (mycontext);
}

xxWindow *xxXTideRoot::newHelpBox (const Dstr &help) {
  return new xxHelpBox (myTideContext, mycontext, help);
}

xxDrawable *xxXTideRoot::newGraph (Station *s, Timestamp t) {
  return new xxGraphMode (myTideContext, mycontext, s, t);
}

xxDrawable *xxXTideRoot::newGraph (StationRef *sr) {
  Timestamp t ((time_t)(time(NULL)));
  return newGraph (sr->load (myTideContext), t);
}

xxDrawable *xxXTideRoot::newPlain (Station *s, Timestamp t) {
  return new xxTextMode (myTideContext, mycontext, s, 'p', t);
}

xxDrawable *xxXTideRoot::newRaw (Station *s, Timestamp t) {
  return new xxTextMode (myTideContext, mycontext, s, 'r', t);
}

xxDrawable *xxXTideRoot::newMediumRare (Station *s, Timestamp t) {
  return new xxTextMode (myTideContext, mycontext, s, 'm', t);
}

xxDrawable *xxXTideRoot::newClock (Station *s, int nobuttonsflag) {
  return new xxClock (myTideContext, mycontext, s, nobuttonsflag);
}

xxDrawable *xxXTideRoot::newClock (Station *s) {
  return newClock (s, ((*(s->context->settings))["cb"].c == 'n'));
}

void xxXTideRoot::commandLineWindows () {
  Settings &settings = *(myTideContext->settings);
  Timestamp t ((time_t)(time(NULL)));
  char mode;
  {
    Configurable &cfbl = settings["m"];
    if (cfbl.isnull)
      mode = 'x';
    else
      mode = cfbl.c;
  }{
    Configurable &l = settings["l"];
    DvectorIterator it = l.v.begin();
    DvectorIterator stop = l.v.end();
    while (it != stop) {
      StationRef *sr;
      sr = (*(myTideContext->stationIndex()))[*it];
      if (sr) {
        Configurable &b = settings["b"];
	if (!b.isnull) {
	  t = Timestamp (b.s, sr->timezone, myTideContext->settings);
	  if (t.isNull()) {
	    Dstr details ("The offending input was ");
	    details += b.s;
	    details += "\nin the time zone ";
	    if (settings["z"].c == 'n')
	      details += sr->timezone;
	    else
	      details += "UTC0";
	    barf (MKTIME_FAILED, details);
	  }
	}
	Station *s = sr->load (myTideContext);
	xxWindow *d = NULL;
	if (mode == 'g')
	  d = newGraph (s, t);
	else if (mode == 'p')
	  d = newPlain (s, t);
	else if (mode == 'a') {
          d = newAbout (s);
          delete s;
	} else
	  d = newClock (s);
        assert (d);

	// Apply -geometry to first one only.
	if (it == l.v.begin())
	  d->move (settings["X"].s);

      } else {
	Dstr details ("Could not find: ");
	details += *it;
	barf (STATION_NOT_FOUND, details);
      }
      it++;
    }
  }
}

void xxXTideRoot::newChooser (XtGrabKind in_grabkind) {
  if ((*(myTideContext->settings))["fe"].c == 'n')
    (void) new xxGlobe (mycontext, *(myTideContext->stationIndex(in_grabkind)),
      myTideContext);
  else
    (void) new xxMap (mycontext, *(myTideContext->stationIndex(in_grabkind)),
      myTideContext);
}

void xxXTideRoot::newMap (XtGrabKind in_grabkind) {
  (void) new xxMap (mycontext, *(myTideContext->stationIndex(in_grabkind)),
    myTideContext);
}

void xxXTideRoot::newGlobe (XtGrabKind in_grabkind) {
  (void) new xxGlobe (mycontext, *(myTideContext->stationIndex(in_grabkind)),
    myTideContext);
}

xxWindow *xxXTideRoot::newAbout (const Station *s) {
  Dstr verbiage;
  s->about (verbiage, 't');
  return newHelpBox (verbiage);
}

void xxXTideRoot::show() {
  if (!(mycontext->is_realized)) {
    mycontext->realize();
    dup();
  }
}

void xxXTideRoot::hide() {
  if (mycontext->is_realized) {
    mycontext->unrealize();
    release(0);

    // Since the window vanishes, the Dismiss button misses the LeaveWindow
    // event.  The next time the window is realized, Dismiss is still stuck
    // on.  I tried to synthesize a LeaveWindow event, but it didn't work.
    // I don't know a good way to reset the state, so extreme measures are
    // required.  To keep the buttons in order, the help button must also
    // be deleted.
    delete dismissbutton;
    delete helpbutton;
    addTwoButtons();
  }
}
