/*  xxFilename  Get a file name from the user.
    Last modified 1998-05-03

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxFilenameCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxFilename *e = (xxFilename *)client_data;

  Dstr filename (XawDialogGetValueString (e->mydialog->manager));

  if (filename.strchr ('\n') != -1 ||
      filename.strchr ('\r') != -1 ||
      filename.strchr (' ') != -1 ||
      filename[0] == '-') {
    Dstr details ("Well, it's not that I can't do it, it's that you probably\n\
don't want me to.  The filename that you entered is '");
    details += filename;
    details += "'.\n";
    if (filename[0] == '-')
      details += "Filenames that begin with a dash are considered harmful.";
    else
      details += "Whitespace in filenames is considered harmful.";
    barf (CANT_OPEN_FILE, details, 0);
  } else
    (*(e->filenamecallback)) (filename, e->ptr);

  delete e;
}

static void
xxFilenameCancelCallback (Widget w, XtPointer client_data, XtPointer
call_data) {
  xxFilename *e = (xxFilename *)client_data;
  e->dismiss();
}

void
xxFilename::dismiss() {
  delete this;
}

xxFilename::~xxFilename() {
  mypopup->unrealize();
  delete mydialog;
}

xxFilename::xxFilename (xxTideContext *in_xtidecontext, xxContext *context,
void (*in_filenamecallback) (Dstr &filename, void *in_ptr), void *in_ptr,
char *initname): xxWindow (in_xtidecontext, context, 0, XtGrabExclusive) {
  ptr = in_ptr;
  filenamecallback = in_filenamecallback;
  mypopup->setTitle ("Enter Filename");

  {
    Arg dargs[4] =  {
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNlabel, (XtArgVal)"Enter filename."},
      {XtNvalue, (XtArgVal)initname}
    };
    Widget dialogwidget = XtCreateManagedWidget ("",
      dialogWidgetClass, mypopup->manager, dargs, 4);
    mydialog = new xxContext (mypopup, dialogwidget);
  }

  XawDialogAddButton (mydialog->manager, "Go", xxFilenameCallback,
   (XtPointer)this);
  XawDialogAddButton (mydialog->manager, "Cancel", xxFilenameCancelCallback,
   (XtPointer)this);

  // Need to force the colors.
  Arg largs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (XtNameToWidget (mydialog->manager, "label"), largs, 2);
  XtSetValues (XtNameToWidget (mydialog->manager, "value"), largs, 2);
  Arg bargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (XtNameToWidget (mydialog->manager, "Go"), bargs, 2);
  XtSetValues (XtNameToWidget (mydialog->manager, "Cancel"), bargs, 2);

  mypopup->realize();
}
