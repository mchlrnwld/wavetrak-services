// $Id: xxMap.hh,v 1.3 2002/10/03 14:35:48 flaterco Exp $
/*  xxMap   Location chooser using Cylindrical Equidistant projection.

    There is some duplicated code between xxGlobe and xxMap.  However,
    they are sufficiently different that I think complete
    encapsulation is the cleanest approach.  -- DWF

    Copyright (C) 2002  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxMap: public xxWindow 
{
public:
  xxMap (xxContext *context, StationIndex &in_stationIndex,
           xxTideContext *in_tidecontext);
  ~xxMap ();
  void global_redraw();
  void dismiss();

protected:
  friend void xxMapButtonPressEventHandler (Widget w, 
                                              XtPointer client_data, 
                                              XEvent *event, 
                                              Boolean *continue_dispatch);
  friend void xxMapKeyboardEventHandler (Widget w, XtPointer client_data,
                                           XEvent *event, 
                                           Boolean *continue_dispatch);
  friend void xxMapPointerMotionEventHandler (Widget w, 
                                                XtPointer client_data,
                                                XEvent *event, 
                                                Boolean *continue_dispatch);
  friend void xxMapListAllCallback (Widget w, XtPointer client_data,
                               XtPointer call_data);
  friend void xxMapZoomOutCallback (Widget w, XtPointer client_data,
                               XtPointer call_data);
  friend void xxMapRoundCallback (Widget w, XtPointer client_data,
                               XtPointer call_data);
  friend void xxMapHelpCallback (Widget w, XtPointer client_data,
                                   XtPointer call_data);

  Pixmap mappixmap;
  StationIndex *stationIndex;
  xxContext *picture, *dismissbutton, *listAllButton, *helpbutton, 
    *zoomoutbutton, *lattext, *lontext, *roundbutton;
  xxLocationList *locationlist;
  int zoom_level, redraw_zero_level;
  int blastx, blasty, blastflag, last_x, last_y;
  Dimension internalHeight, internalWidth;
  double center_longitude;
  typedef struct
  {
    double slat;
    double nlat;
    double wlon;
    double elon;
  } BOUNDS;
  BOUNDS bounds[max_zoom_level + 1];

  int translateCoordinates (double lat, double lon, int *x, int *y);
  void untranslateCoordinates (double *lat, double *lon, int x, int y);
  void update_position (int x, int y);
  void blast (int x, int y);
  void redrawMap ();
};
