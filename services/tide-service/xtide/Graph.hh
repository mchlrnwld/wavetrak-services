// $Id: Graph.hh,v 1.7 2004/04/08 15:35:50 flaterco Exp $
/*  Graph  Abstract superclass for all graphs.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Graph {
public:
  Graph (unsigned xsize, unsigned ysize);

  unsigned xsize();
  unsigned ysize();

  // angle is a kludge to help out the tide clock icon
  void drawTides (Station *sr, Timestamp now, Angle *angle = NULL);

  // Set this to 1 to engage clock mode style.
  int clockmode;

protected:

  unsigned myxsize, myysize;

  // The complex tangle of functions here is to minimize the amount
  // of duplicated code between RGBGraph, xxPixmapGraph, TTYGraph,
  // and Banner.  However, sometimes it is simpler just to ask.
  virtual int is_banner();

  // Draw depth lines.
  void drawDepth (double ymax, double ymin, double valmax, double valmin,
    unsigned linestep, int &mindepth, int &maxdepth, unsigned labelwidth);

  // By default, this does sunrise/sunset.
  virtual void clearGraph (Timestamp start, Timestamp endt, Interval increment,
    Station *station, TideEventsOrganizer &organizer);

  // Mark current time.
  virtual void drawX (int x, double y);

  // Horrible logic for line graphs.
  void drawFunkyLine (double prevytide, double ytide, double nextytide,
    Settings *settings, int x, Colors::colorchoice c);

  // Obviously, variable-width fonts are right out.
  virtual unsigned fontWidth() = 0;
  virtual unsigned fontHeight() = 0;
  virtual unsigned fontMargin();
  virtual unsigned depthLabelLeftMargin();
  virtual unsigned depthLabelRightMargin();
  // This is extra space between depth lines and timestamps or hour ticks
  virtual unsigned depthLineVerticalMargin();
  virtual unsigned hourTickLen();
  virtual double aspectfudge();
  virtual unsigned startpos();

  // All coordinates use northwest gravity.  Integer coordinates range
  // between 0 and size-1; double coordinates range between 0 and size.
  // A double coordinate with integer value x refers to the extreme top
  // of a pixel; the extreme bottom would effectively be x+1.

  // int coordinates are used instead of unsigned so that things
  // can be drawn correctly even when they are only partially visible.

  // Doubles are used for anti-aliasing in truecolor pixmaps; others will
  // just round off.
  void drawVerticalLine (int x, double y1, double y2,
                                 Colors::colorchoice c);
  virtual void drawHorizontalLine (int xlo, int xhi, int y,
				   Colors::colorchoice c);
  virtual void drawHourTick (int x, Colors::colorchoice c);
  virtual void labelHourTick (int x, Dstr &ts);
  virtual void drawTimestampLabel (int x, int line, Dstr &ts);
  virtual void drawTitleLine (Dstr &title);

  // This is the "simple" low level line drawer, where y-coordinates
  // behave in the usual way.
  virtual void drawVerticalLine (int x, int y1, int y2,
                                 Colors::colorchoice c);
  // Likewise, the simple low level pixel setter.
  virtual void setPixel (int x, int y, Colors::colorchoice c) = 0;
  // By default, this just calls the regular setPixel if saturation >= 0.5.
  // Override to do anti-aliasing.
  virtual void setPixel (int x, int y, Colors::colorchoice c,
    double saturation);  // Saturation ranges from 0 to 1

  // Strings should be drawn downwards from the y coordinate provided.
  virtual void drawString (int x, int y, const Dstr &s) = 0;
  void centerString (int x, int y, const Dstr &s);

  // This one uses lines of text instead of pixels.  From the top it's
  // line 0, 1; from the bottom it's -1, -2.
  void centerStringOnLine (int x, int line, const Dstr &s);

  // These won't anti-alias, they will round.
  void drawHorizontalLine (int xlo, int xhi, double y,
				   Colors::colorchoice c);
  void drawString (int x, double y, const Dstr &s);
  void centerString (int x, double y, const Dstr &s);

  void findnextsunevent (TideEventsIterator &it, TideEventsOrganizer
    &organizer, Timestamp now, Timestamp endt, Timestamp &nextsunevent);

  // "lo" is what you get with saturation 0.
  // "hi" is what you get with saturation 1.
  // "lo" does not need to be less than "hi".
  double linterp (double lo, double hi, double saturation);
  unsigned char linterp (unsigned char lo, unsigned char hi,
    double saturation);
  unsigned short linterp (unsigned short lo, unsigned short hi,
    double saturation);
};
