// $Id: NullableInterval.hh,v 1.2 2004/11/17 16:27:18 flaterco Exp $
// NullableInterval:  Interval extended to have a null indicator.

/*
    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class NullableInterval: public Interval {
public:
  int isNull() const;

  NullableInterval();                             // Makes a null Interval.
  NullableInterval (const NullableInterval &in_interval);
  NullableInterval (const Interval &in_interval); // Not null.
  NullableInterval (interval_rep_t in_seconds);   // Not null.
  NullableInterval (const Dstr &in_meridian);     // Not null.

protected:
  int isnull;
};
