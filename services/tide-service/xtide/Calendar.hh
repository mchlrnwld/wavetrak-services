// $Id: Calendar.hh,v 1.6 2004/10/19 17:55:42 flaterco Exp $
/*  Calendar  Manage construction, organization, and printing of calendars.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class Calendar {
public:
  Calendar (Station *station_in, Timestamp start_tm, Timestamp end_tm);

  void print (Dstr &text_out, char form, int oldcal);

protected:

  TideEventsOrganizer organizer;
  Station *station;
  Settings *settings;
  Dstr timezone;
  int isCurrent;
  Timestamp start_day_start, end_day_start;

  void add_csv_event (TideEvent *events, unsigned &i, unsigned limit,
		      TideEvent b, Dstr &date, char *desc);

  void add_month_banner (Dstr &text_out, Timestamp t, char form);
  void flush_buf (Dstr &text_out, Dstr *buf, int buflen,
			    char form, int oldcal, int headers=0);

  // These are broken out from the print method, which got too big.
  void print_oldcal_ht (Dstr &text_out, char form);
  void print_newcal_ht (Dstr &text_out, char form);
  void print_newcal_c  (Dstr &text_out);

  void print_csv_maxmin (Dstr &text_out, TideEvent *events, unsigned n,
			 unsigned limit);
  void print_csv_other  (Dstr &text_out, TideEvent *events, unsigned n,
			 unsigned limit);
};
