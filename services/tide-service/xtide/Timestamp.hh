// $Id: Timestamp.hh,v 1.7 2004/04/06 14:02:54 flaterco Exp $
// Timestamp:  encapsulation for all manner of time-related services needed
// by XTide.

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// All of XTide's time-related functions are only intended to be used
// within the minimal Unix epoch of years 1970 through 2037.


// The "Zulu" setting is enforced all the way down in install_time_zone,
// so Settings get passed down through everything that gets a time zone.

class Timestamp {
public:

  // Create a null Timestamp.
  Timestamp ();

  // Create a Timestamp from a Posix timestamp.
  Timestamp (time_t in_posixtime);

  // Create a Timestamp for the beginning of the specified year in UTC
  // (YEAR-01-01 00:00:00Z)
  Timestamp (Year in_year);

  // Create a Timestamp corresponding to a string in ISO 8601 format,
  // but with separate time zone.  Return null on error.
  Timestamp (const Dstr &in_iso, const Dstr &timezone, Settings *settings);

  // This is a weird one -- create a Timestamp for "year and a half"
  // or year plus some fraction of the year.  This is also UTC.  While
  // normal people would use 07-01 as the canonical mid-year point
  // and construct timestamps based on year and month, I actually use
  // the exact mid-year point (which, after all the math is done, turns
  // out to be 07-02 12:00 on non-leap years and 07-02 00:00 on leap
  // years).
  // fraction should be between 0.0 and 1.0
  Timestamp (Year in_year, double fraction);

  // Create a Timestamp for the specified time in UTC.
  Timestamp (struct tm in_utctime);

  // Create a Timestamp for the specified Julian date.
  Timestamp (double jd);

  // Find hour and day transitions.  (This is non-trivial.)
  void prev_hour(const Dstr &timezone, Settings *settings);
  void prev_day(const Dstr &timezone, Settings *settings);
  void inc_hour(const Dstr &timezone, Settings *settings);
  void inc_day(const Dstr &timezone, Settings *settings);

  // Convert to Julian date.
  double jd() const;

  // Convert to time_t.
  time_t timet() const;

  // Return the year that this timestamp falls in (UTC).
  Year year() const;

  int isNull() const;
  void make_null();

  // The moonrise and moonset logic blows up if you go before 1900 or
  // after 2099.  This is just a range check for that.
  int moonriseset_inrange() const;

  // Output timestamp in local time zone, no seconds
  void print (Dstr &out_time, const Dstr &timezone, Settings *settings) const;
  void printhour (Dstr &out_time, const Dstr &timezone, Settings *settings) const;
  unsigned gethour (const Dstr &timezone, Settings *settings) const;
  void printdate (Dstr &out_time, const Dstr &timezone, Settings *settings) const;
  void printtime (Dstr &out_time, const Dstr &timezone, Settings *settings) const;
  struct tm *get_tm (const Dstr &timezone, Settings *settings) const;
  void printcalheading (Dstr &out_heading, const Dstr &timezone, Settings
    *settings) const;
  void printdayheading (Dstr &out_heading, const Dstr &timezone, Settings
    *settings) const;

  // Output timestamp in format complying with RFC 2445 (iCalendar)
  // if zero_secs <> 0:  YYYYMMDDTHHMM00Z
  //              else:  YYYYMMDDTHHMMSSZ
  void print_iCalendar (Dstr &out_time, int zero_secs) const;

  // Subtract b from a.
  friend Interval operator- (Timestamp a, Timestamp b);

  // Can't do this when null.
  Timestamp &operator+= (Interval b);
  Timestamp &operator-= (Interval b);

  // This forces initialization of the time zone mapping and returns
  // true if it can do Rarotonga correctly.
  int zoneinfo_doesnt_suck(Settings *settings);

  // Deleted astronomical functions from congen.

protected:

  friend int operator> (Timestamp a, Timestamp b);
  friend int operator>= (Timestamp a, Timestamp b);
  friend int operator< (Timestamp a, Timestamp b);
  friend int operator<= (Timestamp a, Timestamp b);
  friend int operator== (Timestamp a, Timestamp b);
  friend int operator!= (Timestamp a, Timestamp b);

  void install_time_zone (const Dstr &timeZone_in, Settings *settings) const;

  int is_null;
  time_t posixtime;

  // Convert a struct tm expressed in UTC to a time_t.  The fields
  // used are tm_year, tm_mon, tm_mday, tm_hour, tm_min, and tm_sec.
  // All others are ignored.
  time_t tm2utc (struct tm ht);

  // Get YEAR-01-01 00:00:00Z in time_t format
  time_t year2utc (Year in_year);

  // Compare two struct tm's according to the fields tm_year, tm_mon,
  // tm_mday, tm_hour, tm_min, and tm_sec.  Returns:
  //      > 0  if a > b
  //        0  if a == b
  //      < 0  if a < b
  int compare_tm (struct tm a, struct tm b);
};

// You can't compare nulls like this.
int operator> (Timestamp a, Timestamp b);
int operator>= (Timestamp a, Timestamp b);
int operator< (Timestamp a, Timestamp b);
int operator<= (Timestamp a, Timestamp b);

// You can't add or subtract from a null.
Timestamp operator+ (Timestamp a, Interval b);
Timestamp operator- (Timestamp a, Interval b);

// This allows nulls to equal nulls, and nothing else.
int operator== (Timestamp a, Timestamp b);
int operator!= (Timestamp a, Timestamp b);
