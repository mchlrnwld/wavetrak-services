// 	$Id: errors.cc,v 1.11 2004/11/17 16:27:18 flaterco Exp $	
// errors:  Global functions for bailing out

/*
    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifdef XTIDE_X11
#include "xtide.hh"
#else
#include "common.hh"
#endif

static int daemon_mode = 0;

void set_daemon_mode() {
  daemon_mode = 1;
  openlog ("xttpd", LOG_CONS|LOG_PID, LOG_DAEMON);
}

void xperror (const char *s) {
  if (daemon_mode)
    syslog (LOG_ERR, "%s: %s", s, strerror (errno));
  else
    perror (s);
}

void log (Dstr &message, int priority) {
  log (message.aschar(), priority);
}

void log (const char *message, int priority) {
  if (message) {
    if (daemon_mode)
      syslog (priority, "%s", message);
    else
      fprintf (stderr, "%s\n", message);
  }
}

void log (const char *message, Dstr &details, int priority) {
  Dstr temp (message);
  temp += details;
  log (temp, priority);
}

static void errmsg (enum tideerr err, const Dstr &details, Dstr &errout,
int fatal = 1) {
  if (fatal)
    errout = "XTide Fatal Error:  ";
  else
    errout = "XTide Error:  ";
  switch (err) {
  case YEAR_OUT_OF_RANGE:
    errout += "YEAR_OUT_OF_RANGE\n\
Some operation has attempted to access time before 1 A.D. or after\n\
4000 A.D.  There's no point even trying to predict tides over that\n\
span of time.";
    break;
  case NULL_YEAR:
    errout += "NULL_YEAR\n\
An attempt was made to access the value of an instance of class Year\n\
before its value was set.  This should never happen.";
    break;
  case TM2UTC_FAILED:
    errout += "TM2UTC_FAILED\n\
XTide was unable to convert an informal time specification into a\n\
legal Unix timestamp.  This probably means that there is something\n\
wrong with the informal time specification, such as referring to a\n\
point in time that does not exist (like February 30th).  It may also\n\
mean that the requested time is outside of the Unix epoch, which only\n\
reliably includes the years from 1970 through 2037.  For the workaround,\n\
see http://www.flaterco.com/xtide/time_t.html.";
    break;
  case MKTIME_FAILED:
    errout += "MKTIME_FAILED\n\
XTide was unable to convert an informal time specification into a\n\
legal Unix timestamp.  This probably means that there is something\n\
wrong with the informal time specification, such as referring to a\n\
point in time that does not exist (like February 30th).  It may also\n\
mean that the requested time is outside of the Unix epoch, which only\n\
reliably includes the years from 1970 through 2037.  For the workaround,\n\
see http://www.flaterco.com/xtide/time_t.html.";
    break;
  case INVALID_CONSTITUENT:
    errout += "INVALID_CONSTITUENT\n\
The program attempted to access a tabulated constituent before it was\n\
fully defined.  This should never happen.";
    break;
  case YEAR_NOT_IN_TABLE:
    errout += "YEAR_NOT_IN_TABLE\n\
Some operation has been initiated that needs data for a year that is\n\
not supported by the harmonics file being used.";
    break;
  case DOUBLE_READ:
    errout += "DOUBLE_READ\n\
An attempt was made to read in a tabulated constituent that has\n\
already been read.  This should never happen.";
    break;
  case END_OF_FILE:
    errout += "END_OF_FILE\n\
An input file ended where it should not.  This can be caused by:\n\
   --  updates to the harmonics file while xtide is running;\n\
   --  a corrupt harmonics file;\n\
   --  libtcd errors (which would have been reported to stderr).\n\
If the harmonics file has been updated, just restart xtide and it will be fine.";
    break;
  case NO_HFILE_PATH:
    errout += "NO_HFILE_PATH\n\
If /etc/xtide.conf is not provided, you must set the environment variable\n\
HFILE_PATH to point to your harmonics files.  Example:\n\
  export HFILE_PATH=/usr/local/share/xtide/harmonics.tcd\n\
Please refer to the documentation for usage of /etc/xtide.conf.";
    break;
  case IMPOSSIBLE_CONVERSION:
    errout += "IMPOSSIBLE_CONVERSION\n\
An attempt was made to convert between units of fundamentally different types,\n\
e.g., to convert from units of velocity to units of length.";
    break;
  case NO_CONVERSION:
    errout += "NO_CONVERSION\n\
An attempt was made to convert a measure with units to the same units.  This\n\
may indicate a fault in the program logic.";
    break;
  case UNRECOGNIZED_UNITS:
    errout += "UNRECOGNIZED_UNITS\n\
The units of a prediction value (e.g., feet, meters, knots) were not one of\n\
the recognized alternatives.";
    break;
  case BOGUS_COORDINATES:
    errout += "BOGUS_COORDINATES\n\
A latitude and longitude pair was found to be out of range.";
    break;
  case CANT_OPEN_FILE:
    errout += "CANT_OPEN_FILE\n\
Unable to open a file.";
    break;
  case CORRUPT_HARMONICS_FILE:
    errout += "CORRUPT_HARMONICS_FILE\n\
Your harmonics file does not conform to the required format.  This can be\n\
caused by:\n\
   --  updates to the harmonics file while xtide is running;\n\
   --  a corrupt harmonics file;\n\
   --  libtcd errors (which would have been reported to stderr).\n\
If the harmonics file has been updated, just restart xtide and it will be fine.";
    break;
  case BADCOLORSPEC:
    errout += "BADCOLORSPEC\n\
A color specification could not be parsed.";
    break;
  case XPM_ERROR:
    errout += "XPM_ERROR\n\
An error condition was reported by an Xpm library function.";
    break;
  case NOHOMEDIR:
    errout += "NOHOMEDIR\n\
The environment variable HOME is not set.";
    break;
  case XMLPARSE:
    errout += "XMLPARSE\n\
The XML file is ill-formed or exceeds the limitations of XTide's parser.";
    break;
  case STATION_NOT_FOUND:
    errout += "STATION_NOT_FOUND\n\
The specified station was not found in any harmonics file.";
    break;
  case SUB_SUBORDINATE:
    errout += "SUB_SUBORDINATE\n\
The subordinate station references another subordinate station.  You\n\
can't do that.";
    break;
  case NOOFFSETSFOUND:
    errout += "NOOFFSETSFOUND\n\
The requested subordinate station was parsed successfully, but no\n\
usable offsets were found.  Possibly this station uses a different\n\
kind of offsets that are not supported in this version of XTide.";
    break;
  case BADHHMM:
    errout += "BADHHMM\n\
XTide was expecting an interval specification of the form [-]HH:MM where HH\n\
is hours and MM is minutes.  What it got did not parse.  This indicates a\n\
problem with an offset, meridian, or step value specification.";
    break;
  case CANTOPENDISPLAY:
    errout += "CANTOPENDISPLAY\n\
XTide cannot open your X11 display.  Check the setting of the\n\
DISPLAY environment variable, and check your permissions (xhost,\n\
xauth, kerberos, firewall, etc.).";
    break;
  case NOT_A_NUMBER:
    errout += "NOT_A_NUMBER\n\
Couldn't convert a text string to a number.";
    break;
  case PNG_WRITE_FAILURE:
    errout += "PNG_WRITE_FAILURE\n\
A general fatal error occurred in libpng while producing a PNG.";
    break;
  case CANT_GET_SOCKET:
    errout += "CANT_GET_SOCKET\n\
Xttpd was unable to bind its socket.  Common causes are (1) you tried to\n\
use the default port 80 without having root privileges; fix this by\n\
providing a usable port number (e.g., 8080) as the first command-line\n\
argument, or (2) there is already something running on the specified port,\n\
such as another web server.";
    break;
  case ABSURD_OFFSETS:
    errout += "ABSURD_OFFSETS\n\
A subordinate station's offsets were so nonsensical as to cause\n\
operational failures in XTide.";
    break;
  case NUMBER_RANGE_ERROR:
    errout += "NUMBER_RANGE_ERROR\n\
A number was parsed OK, but it is not in the range of acceptable values.";
    break;
  case BAD_MODE:
    errout += "BAD_MODE\n\
A mode specified with the -m command line switch is not supported.";
    break;
  case BAD_FORMAT:
    errout += "BAD_FORMAT\n\
Either the default format or the format specified with the -f command line\n\
switch is not supported for the selected mode.";
    break;
  case BAD_TIMESTAMP:
    errout += "BAD_TIMESTAMP\n\
The -b and -e command line switches expect timestamps to be in the format\n\
\"YYYY-MM-DD HH:MM\".  Example:  tide -b \"1998-01-01 13:00\"";
    break;
  case BAD_IP_ADDRESS:
    errout += "BAD_IP_ADDRESS\n\
The IP address given to xttpd was not a valid IPv4 dotted-decimal string.";
    break;
  case BAD_BOOL:
    errout += "BAD_BOOL\n\
A boolean parameter was set to some value other than 'y' or 'n'.";
    break;
  case BAD_TEXT:
    errout += "BAD_TEXT\n\
A text parameter had something not right about it.";
    break;
  case BAD_OR_AMBIGUOUS_COMMAND_LINE:
    errout += "BAD_OR_AMBIGUOUS_COMMAND_LINE\n\
The command line could not be rationalized.  Probably you have provided an\n\
unrecognized switch or invalid argument.  However, you can also get this\n\
error by using ambiguous syntax.  For example, the shorthand -lw5 could mean\n\
\"set the line width to 5\" (-lw 5) or it could mean \"load the location\n\
named w5\" (-l w5).";
    break;
  default:
    assert (0);
  }
  errout += "\n";
  if (!(details.isNull())) {
    errout += "\nError details:\n";
    errout += details;
    errout += "\n";
  }
}

#ifdef XTIDE_X11
static xxContext *mycontext = NULL;
static xxTideContext *mytidecontext = NULL;
void set_error_context (xxContext *in_context, xxTideContext *in_xtidecontext) {
  mycontext = in_context;
  mytidecontext = in_xtidecontext;
}
#endif

void
barf (enum tideerr err, Dstr &details, int fatal)
{
  Dstr errout;
  errmsg (err, details, errout, fatal);
  if (daemon_mode)
    log (errout, LOG_ERR);
  else
    fprintf (stderr, "%s", errout.aschar());

#ifdef XTIDE_X11
  if (mycontext) {
    (void) new xxErrorBox (mytidecontext, mycontext, errout, fatal);
    if (fatal)
      xxMainLoopForever (mycontext);
  }
#endif

  if (fatal) {
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    assert (0);  // This is just to get a core dump.
#else
    exit (-1);
#endif
  }
}

void
barf (enum tideerr err, int fatal) {
  Dstr nulldstr;
  barf (err, nulldstr, fatal);
}
