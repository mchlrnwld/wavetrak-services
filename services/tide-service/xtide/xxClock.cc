// $Id: xxClock.cc,v 1.8 2004/10/22 17:43:51 flaterco Exp $
/*  xxClock  Tide clock.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxClock::dismiss () {
  delete this; // Is this safe?
}

void
xxClock::help() {
  Dstr helpstring ("\
XTide Tide Clock Window\n\
\n\
The tide clock shows the current time and tide level.  It updates once per\n\
minute.  You can resize the window to see more or less detail.\n\
\n\
To remove the buttons and just have a tide clock, click on the graph.  To\n\
bring back the buttons, click on the graph again.\n\
\n\
The options menu supplies the following commands:\n\
\n\
  Save:  Use this to write the tide predictions to a PNG image file.\n\
\n\
  Set Mark:  This is used to set the 'mark level' for tide predictions.\n\
  In clock mode, this only causes a line to be drawn at the mark level.\n\
  To see the times of mark transitions you must switch to graph or text\n\
  mode.\n\
\n\
  Convert ft<->m:  Convert feet to meters or vice-versa.\n\
\n\
  Set Aspect:  This is used to change the aspect ratio for tide graphing.\n\
  The aspect ratio is a measure of how scrunched up or stretched out\n\
  the graph is.  If tide events are too close together, increase the\n\
  aspect; if you want to fit more information onto one graph, decrease\n\
  the aspect.\n\
\n\
  New Graph Window:  Open a graph window for the current location and time.\n\
\n\
  New Plain Mode Window:  Open a window with a text listing of tide\n\
  predictions for the current location and time.\n\
\n\
  New Raw Mode Window:  Open a window with a text listing of unprocessed\n\
  numerical time stamps and tide levels for the current location and time.\n\
\n\
  New Medium Rare Mode Window:  Open a window with a text listing of\n\
  processed timestamps and unprocessed numerical tide levels for the\n\
  current location and time.\n\
\n\
  New Clock Window:  Duplicates the existing clock window.\n\
\n\
  About This Station:  Show station metadata.\n\
\n\
  New Location Chooser:  Open new globe and location list windows to allow\n\
  selecting a new location.");
  (void) xtidecontext->root->newHelpBox (helpstring);
}

void
xxClockResizeHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch) {
  xxClock *g = (xxClock *)client_data;
  switch (event->type) {
  case ConfigureNotify:
    {
      XConfigureEvent *ev = (XConfigureEvent *)event;
      Dimension newheight = ev->height;
      Dimension newwidth = ev->width;
      if (newheight != g->curwindowheight ||
	  newwidth != g->curwindowwidth) {
	g->curwindowheight = newheight;
	g->curwindowwidth = newwidth;
	g->curgraphheight = newheight - (g->origwindowheight - g->origgraphheight);
	g->curgraphwidth = newwidth - (g->origwindowwidth - g->origgraphwidth);
	g->redraw();
      }
    }
    break;
  default:
    ;
  }
}

void
xxClockSaveCallback (Dstr &filename, void *in_ptr) {
  xxClock *gm = (xxClock *)in_ptr;
  // See externC.cc
  if ((png_file_ptr = fopen (filename.aschar(), "w"))) {
    RGBGraph g (gm->curgraphwidth, gm->curgraphheight, gm->xtidecontext->colors);
    g.clockmode = 1;
    g.drawTides (gm->station, gm->t);
    g.writeAsPNG (file_write_data_fn);
    fclose (png_file_ptr);
  } else {
    Dstr details (filename);
    details += ": ";
    details += strerror (errno);
    details += ".";
    barf (CANT_OPEN_FILE, details, 0);
  }
}

void
xxClock::save () {
  (void) new xxFilename (xtidecontext, mypopup, xxClockSaveCallback, this, "tides.png");
}

void
xxClockButtonPressEventHandler (Widget w, XtPointer client_data,
			 XEvent *event, Boolean *continue_dispatch) {
  xxClock *c = (xxClock *)client_data;
  // This is rather crude, but it's hard to do better.
  if (c->station)
    c->xtidecontext->root->newClock (c->station, !(c->nobuttons));
  c->station = NULL;
  delete c;
}

xxClock::xxClock (xxTideContext *in_tidecontext,
xxContext *in_xxcontext, Station *in_station, int nobuttonsflag):
xxDrawable (in_tidecontext, in_xxcontext, in_station, !nobuttonsflag)
{
  label = NULL;
  t = Timestamp((time_t)(time(NULL)));
  nobuttons = nobuttonsflag;
  Dstr title (station->name);
  title += " (Clock)";
  mypopup->setTitle (title);
  XtAddEventHandler (mypopup->manager, StructureNotifyMask, False,
		     xxClockResizeHandler, (XtPointer)this);

  Dimension minxcwidth = minwidthfudge * 3 + mypopup->stringWidth
    (mypopup->defaultfontstruct, "OptionsDismiss?");

  origgraphheight = curgraphheight = (*(xtidecontext->settings))["gh"].u;
  if (nobuttons)
    origgraphwidth = curgraphwidth = (*(xtidecontext->settings))["cw"].u;
  else
    origgraphwidth = curgraphwidth = max (minxcwidth, (*(xtidecontext->settings))["cw"].u);
  graph = new xxPixmapGraph (origgraphwidth, origgraphheight, mypopup);
  graph->clockmode = 1;
  graph->drawTides (station, t, &analogangle);

  if (nobuttons) {
    Arg args[3] = {
      {XtNbackgroundPixmap, (XtArgVal)graph->pixmap},
      {XtNwidth, (XtArgVal)curgraphwidth},
      {XtNheight, (XtArgVal)curgraphheight}
    };
    XtSetValues (mypopup->manager, args, 3);
    XtAddEventHandler (mypopup->manager, ButtonPressMask, False,
      xxClockButtonPressEventHandler, (XtPointer)this);
  } else {
    Arg labelargs[5] =  {
      {XtNbitmap, (XtArgVal)graph->pixmap},
      {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
      {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
      {XtNinternalHeight, (XtArgVal)0},
      {XtNinternalWidth, (XtArgVal)0}
    };
    {
      Widget labelwidget = XtCreateManagedWidget ("",
	labelWidgetClass, container->manager, labelargs, 5);
      label = new xxContext (mypopup, labelwidget);
    }
    addNormalButtons();
    XtAddEventHandler (label->manager, ButtonPressMask, False,
      xxClockButtonPressEventHandler, (XtPointer)this);
  }

  // No call to redraw this time.
  mypopup->realize();

  {
    Arg args[2] = {
      {XtNwidth, (XtArgVal)(&origwindowwidth)},
      {XtNheight, (XtArgVal)(&origwindowheight)}
    };
    if (nobuttons)
      XtGetValues (mypopup->manager, args, 2);
    else
      XtGetValues (container->manager, args, 2);
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Clock window original height " << origwindowheight << endl;
    cerr << "Clock window original width " << origwindowwidth << endl;
#endif
  }
  curwindowheight = origwindowheight;
  curwindowwidth = origwindowwidth;

  if (nobuttons)
    mypopup->setMinSize(mingwidth + (origwindowwidth - origgraphwidth),
                      mingheight + (origwindowheight - origgraphheight));
  else
    mypopup->setMinSize(minxcwidth + (origwindowwidth - origgraphwidth),
                      mingheight + (origwindowheight - origgraphheight));

  clockicon = mypopup->makePixmap (mypopup->icon_size, mypopup->icon_size, mypopup->ddepth);
  XResizeWindow (mypopup->display, mypopup->icon_window, mypopup->icon_size, mypopup->icon_size);

  // Need two icon windows to minimize "flashing" on updates
  int screen = DefaultScreen(mypopup->display);
  iconwindow2 = XCreateWindow (mypopup->display,
    RootWindow (mypopup->display, screen), 0, 0, mypopup->icon_size,
     mypopup->icon_size, 0, DefaultDepth (mypopup->display, screen),
    InputOutput, DefaultVisual (mypopup->display, screen), 0, NULL);
  toggle_icon_window = 1;
  redrawIcon();

  timer = XtAppAddTimeOut (mypopup->context, 1000*(60-(time(NULL)%60)),
    xxClockTimerCallback, this);
}

#ifdef clocktest
int ctserial = 0;
#endif
void
xxClock::redrawIcon() {
  XSetForeground (mypopup->display, mypopup->icon_gc, mypopup->iconpixels[0]);
  XFillRectangle (mypopup->display, clockicon, mypopup->icon_gc, 0, 0,
    mypopup->icon_size, mypopup->icon_size);

  XSetForeground (mypopup->display, mypopup->icon_gc, mypopup->iconpixels[1]);

  char *hicap, *locap;
  if (station->isCurrent) {
    hicap = "Flood"; locap = "Ebb";
  } else {
    hicap = "High"; locap = "Low";
  }
  int c = mypopup->icon_size / 2; // Center
  int d = mypopup->icon_size - 17; // Diameter w/ fixed size "Hi" and "Lo"
  int hl = d / 2 - 4; // Hand length
  XDrawArc (mypopup->display, clockicon, mypopup->icon_gc,
    8, 8, d, d, 0, 360*64);
  XDrawArc (mypopup->display, clockicon, mypopup->icon_gc,
    c-1, c-1, 2, 2, 0, 360*64);
  XDrawString (mypopup->display, clockicon, mypopup->icon_gc,
    c-(mypopup->stringWidth(mypopup->tinyfontstruct, hicap)/2), 7,
    hicap, strlen(hicap));
  XDrawString (mypopup->display, clockicon, mypopup->icon_gc,
    c-(mypopup->stringWidth(mypopup->tinyfontstruct, locap)/2),
    mypopup->icon_size, locap, strlen(locap));
  int handx = (int)(c + hl * sin (analogangle) + 0.5);
  int handy = (int)(c - hl * cos (analogangle) + 0.5);
  XDrawLine (mypopup->display, clockicon, mypopup->icon_gc,
    c, c, handx, handy);

#ifdef clocktest
  char temp[80];
  sprintf (temp, "%d", ctserial++);
  XDrawString (mypopup->display, clockicon, mypopup->icon_gc,
    0, 7, temp, strlen(temp));
#endif

  // Use two windows to minimize "flashing" (can't get rid of it totally)
  Window window;
  if (toggle_icon_window == 1) {
    window = mypopup->icon_window;
    toggle_icon_window = 2;
  } else {
    window = iconwindow2;
    toggle_icon_window = 1;
  }

  // Lower level version of the same old song and dance to force it to
  // update its pixmap.
  XSetWindowAttributes xswa;
  xswa.background_pixmap = None;
  // xswa.event_mask = ExposureMask;
  XChangeWindowAttributes (mypopup->display, window, CWBackPixmap, &xswa);
  xswa.background_pixmap = clockicon;
  XChangeWindowAttributes (mypopup->display, window, CWBackPixmap, &xswa);
  // mypopup->refreshIcon();
#if 0
  Arg args[1] = {
    {XtNiconWindow, (XtArgVal)0}
  };
  XtSetValues (mypopup->manager, args, 1);
#endif
  Arg args2[1] = {
    {XtNiconWindow, (XtArgVal)(window)}
  };
  XtSetValues (mypopup->manager, args2, 1);
}

void
xxClockTimerCallback (XtPointer client_data, XtIntervalId *timerid) {
  xxClock *g = (xxClock *)client_data;
  g->t = Timestamp((time_t)(time(NULL)));
  // Lightweight redraw
  g->graph->drawTides (g->station, g->t, &(g->analogangle));
  g->draw();
  g->redrawIcon();
  g->timer = XtAppAddTimeOut (g->mypopup->context, 1000*(60-(time(NULL)%60)),
    xxClockTimerCallback, g);
}

void xxClock::redraw() {
  // Save this until the window is updated to avoid a BadDrawable error
  xxPixmapGraph *oldgraph = graph;
  graph = new xxPixmapGraph (curgraphwidth, curgraphheight, mypopup);
  graph->clockmode = 1;
  graph->drawTides (station, t, &analogangle);

  // Don't seem to have problems resizing this time.  See xxTextMode
  // for the kludge if needed.

  draw();
  delete oldgraph;
}

// You have to call drawTides on the graph before calling this.
void xxClock::draw () {
  if (nobuttons) {
    // (Kludge from xxTitleScreen)
    // You really have to pound on it to get it to refresh.
    Arg args1[1] = {
      {XtNbackgroundPixmap, (XtArgVal)0}
    };
    XtSetValues (mypopup->manager, args1, 1);
    Arg args2[1] = {
      {XtNbackgroundPixmap, (XtArgVal)graph->pixmap}
    };
    XtSetValues (mypopup->manager, args2, 1);
    mypopup->refresh();
  } else {
    Arg args[1] = {
      {"bitmap", (XtArgVal)graph->pixmap}
    };
    XtSetValues (label->manager, args, 1);
    label->refresh();
  }
}

xxClock::~xxClock() {
  XtRemoveTimeOut (timer);
  mypopup->unrealize();
  delete graph;
  if (!nobuttons)
    delete label;
  XDestroyWindow(mypopup->display, iconwindow2);
  XFreePixmap (mypopup->display, clockicon);
}

int
xxClock::is_graph() {
  return 1;
}

int
xxClock::is_clock() {
  return 1;
}

void xxClock::global_redraw() {
  xxDrawable::global_redraw();
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (label)
    XtSetValues (label->manager, args, 2);
  redrawIcon();
}
