// $Id: TideEvent.hh,v 1.7 2004/10/21 19:58:13 flaterco Exp $

// TideEvent  Generic representation for tide events.  Refactored from
// similar notions previously appearing in TideContext, Calendar,
// Station, xxTextMode, Graph, ...

/*
    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class TideEvent {
public:
  enum EventType {max, min, slackrise, slackfall, markrise, markfall,
    sunrise, sunset, moonrise, moonset, newmoon, firstquarter, fullmoon,
    lastquarter, eveningTwilight, morningTwilight, rawreading};

  Timestamp tm;
  EventType etype;
  PredictionValue pv;
  int isCurrent;

  // For sub stations with residual offsets, these record the time and
  // level of the corresponding event before corrections are applied.
  // This is not necessarily the same as the reference station:  the
  // harmonic constants may still have been adjusted.  When not
  // applicable, these variables remain null.
  Timestamp       uncorrected_tm;
  PredictionValue uncorrected_pv;

  // Generate one line of text output, applying global formatting
  // rules and so on.
  // Legal forms are c (CSV), t (text) or i (iCalendar).
  // Legal modes are p (plain), r (raw), or m (medium rare).
  // Line is not newline terminated.
  void print (Dstr &line_out, char form, char mode, const Station &station) const;

  const char *longdesc () const;
  const char *shortdesc () const;
  int isSunMoonEvent () const;
  int isMaxMinEvent () const;

  // This returns true if the description of the event would be Min Flood or
  // Min Ebb.
  int isMinCurrentEvent () const;
};

// Specify direction of search for tide events.
enum Direction {forward, backward};
