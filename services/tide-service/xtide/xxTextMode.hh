// $Id: xxTextMode.hh,v 1.4 2004/04/06 19:53:29 flaterco Exp $
/*  xxTextMode  Raw/medium/plain modes, in a window.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxTextMode: public xxDrawable {
  friend void xxTextModeforwardCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxTextModebackwardCallback (Widget w, XtPointer client_data,
    XtPointer call_data);
  friend void xxTextModeSaveCallback (Dstr &filename, void *in_ptr);
  friend void xxRareModeSaveCallback (Dstr &filename, Timestamp start_tm,
    Timestamp end_tm, void *in_ptr);
  friend void xxTextModeResizeHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch);

public:
  // Legal modes are p (plain), m (medium rare), or r (raw).
  xxTextMode (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    Station *in_station, char mode_in);
  xxTextMode (xxTideContext *in_tidecontext, xxContext *in_xxcontext,
    Station *in_station, char mode_in, Timestamp t_in);
  ~xxTextMode();

  void redraw();
  void global_redraw();
  void dismiss();
  void help();
  void save();

  int is_rare();

protected:
  // Common code from multiple constructors.
  void construct ();

  char mode;
  Dimension origheight, origlabelheight, height;
  unsigned lines;
  void draw();
  xxContext *label, *namelabel, *forwardbutton, *backwardbutton;
  Timestamp last_tm;

  TideEventsOrganizer organizer;
};
