// $Id: Station.hh,v 1.19 2004/11/17 16:27:18 flaterco Exp $
/*  Station  Abstract superclass of ReferenceStation and SubordinateStation.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// Filters for predictTideEvents.
// NOFILTER = maxes, mins, slacks, mark crossings, sun and moon
// KNOWNTIDEEVENTS = tide events that can be determined without interpolation
//                   (maxes, mins, and sometimes slacks)
// MAXMIN = maxes and mins
enum TideEventsFilter {NOFILTER, KNOWNTIDEEVENTS, MAXMIN};

class Station {
public:
  Station (TideContext *in_context);
  virtual ~Station();

  TideContext *context;
  virtual int is_reference_station() = 0;

  // We end up needing all this in Station::predictTideEvents.  Gakk.
  virtual int have_residuals() = 0;
  // Returns true if floodbegins offset is known OR is not needed
  // (no offsets or offsets are simple).
  virtual int have_floodbegins() = 0;
  // Analogous.
  virtual int have_ebbbegins() = 0;
  int isCurrent;
  int isHydraulicCurrent;

  // This preserves settings like marklevel and units.
  // N.B., see the .cc of xxDrawable::global_redraw() regarding an
  // important dependency on how this is implemented.
  Station *clone();

  // Info needed to reload the station.
  Dstr *harmonicsFileName;
  long TCDRecordNumber;

  // TCD fields of direct interest.
  Dstr name;
  Dstr timeZone;
  Dstr note;
  Coordinates coordinates;
  ConstantSetWrapper *constants;
  NullableAngle minCurrentAngle; // Null if N/A
  NullableAngle maxCurrentAngle; // Null if N/A

  // Other etceteras from the TCD record.
  MetaFieldVector metadata;

  // Stettings that don't necessarily relate directly to the station,
  // but which must transfer with it anyway.  NOTE:  Graph and possibly
  // other code assumes that markLevel will be in the correct units
  // for the station.
  PredictionValue *markLevel; // This may be null.
  double aspect;              // Aspect for graphing.
  Interval step;              // Step size for rare modes.

  // Get and set units.
  PredictionValue::Unit myUnits;  // Never KnotsSquared
  void setUnits (PredictionValue::Unit in_units);

  // To get all tide events falling between t1 and t2, you have to
  // scan the interval from t1 - maximumTimeOffset to t2 - minimumTimeOffset.
  // These will remain zero for reference stations.
  Interval minimumTimeOffset; // Most negative, or least positive.
  Interval maximumTimeOffset; // Most positive, or least negative.

  // The implementations given in Station are usable as-is for a
  // Reference Station but are overridden by SubordinateStation.
  virtual PredictionValue minLevel() const;
  virtual PredictionValue maxLevel() const;

  // Generate description from metadata.
  void about (Dstr &text_out, char mode) const;

  // predictTideLevel is used for drawing tide graphs and estimating
  // the heights in between tide events.  The implementation given in
  // Station is usable as-is for a Reference Station but is overridden
  // by SubordinateStation.
  virtual PredictionValue predictTideLevel (Timestamp tm);

  // Get all tide events within a range of timestamps and add them to
  // the organizer.  The range is >= start_tm and < end_tm.  Because
  // predictions are done to plus or minus one minute, invoking this
  // multiple times with adjoining ranges may duplicate or delete tide
  // events falling right on the boundary.  TideEventsOrganizer should
  // suppress the duplicates, but omissions will not be detected.
  //
  // Either settings or the filter arg can suppress sun and moon events.
  void predictTideEvents (Timestamp start_tm, Timestamp end_tm,
    TideEventsOrganizer &organizer, TideEventsFilter filter = NOFILTER);

  // Analogous, for raw readings.
  void predictRawEvents (Timestamp start_tm, Timestamp end_tm,
    TideEventsOrganizer &organizer);

  // Add events to an organizer to extend its range in the specified
  // direction by the specified interval.  (Number of events is
  // indeterminate.)  A safety margin is used to attempt to prevent
  // tide events from falling through the cracks as discussed above
  // predictTideEvents.
  //
  // Either settings or the filter arg can suppress sun and moon events.
  void extendRange (TideEventsOrganizer &organizer, Direction d,
		    Interval howmuch, TideEventsFilter filter = NOFILTER);

  // Analogous, for raw readings.  Specify number of events in howmany.
  void extendRange (TideEventsOrganizer &organizer, Direction d,
    unsigned howmany);

protected:

  // Fill in PredictionValue and possibly apply corrections.
  virtual void finishTideEvent (TideEvent &te) = 0;

  // G. Dairiki code, revised to use new data types and to reverse
  // direction.
  // marklev is a hidden channel to get information into f_mark
  // (f_hiorlo and f_mark must have identical signatures).  It is
  // set by the higher level find_mark_crossing at bottom.
  PredictionValue marklev;
  PredictionValue f_hiorlo (Timestamp t, unsigned deriv);
  PredictionValue f_mark (Timestamp t, unsigned deriv);
  Timestamp find_zero (Timestamp tl, Timestamp tr,
    PredictionValue (Station::*f)(Timestamp, unsigned deriv),
    Direction d);
  Timestamp find_mark_crossing (Timestamp t1, Timestamp t2,
    int &risingflag_out, Direction d);
  Timestamp next_zero (Timestamp t,
    PredictionValue (Station::*f)(Timestamp, unsigned deriv),
    int &risingflag_out, Amplitude max_fp, Amplitude max_fpp,
    Direction d);
  Timestamp next_high_or_low_tide (Timestamp t, int &hiflag_out, Direction d);

  // Slightly higher level version of find_mark_crossing that does the
  // needed compensation for datum, KnotsSquared, and units and sets
  // marklev.
  Timestamp find_mark_crossing (Timestamp t1, Timestamp t2,
    PredictionValue marklev_in, int &risingflag_out, Direction d);

  // Deal with interpolated sub station mark crossings and slacks.
  // FIXME Station needs refactoring.
  TideEvent find_mark_crossing (TideEvent left_te, TideEvent right_te,
    PredictionValue marklev_in, int &risingflag_out);
};
