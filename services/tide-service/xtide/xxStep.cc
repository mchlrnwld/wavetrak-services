// $Id: xxStep.cc,v 1.3 2003/01/17 17:31:00 flaterco Exp $
/*  xxStep  Get a step from the user.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxStepCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxStep *e = (xxStep *)client_data;

  unsigned seconds = e->hours * HOURSECONDS + e->minutes * 60;
  if (seconds == 0) {
    Dstr details ("The offending input was 0:00.");
    barf (NUMBER_RANGE_ERROR, details, 0);
  } else
    (*(e->stepcallback)) (Interval(seconds), e->ptr);

  delete e;
}

void
xxStepCancelCallback (Widget w, XtPointer client_data, XtPointer
call_data) {
  xxStep *e = (xxStep *)client_data;
  e->dismiss();
}

void stephourcallback (void *cbdata) {
  xxStep *mydialog = (xxStep *)cbdata;
  mydialog->hours = mydialog->hourchoice->choice();
}

void stepminutecallback (void *cbdata) {
  xxStep *mydialog = (xxStep *)cbdata;
  mydialog->minutes = mydialog->minutechoice->choice();
}

void
xxStep::dismiss() {
  delete this;
}

xxStep::~xxStep() {
  mypopup->unrealize();
  delete gobutton;
  delete cancelbutton;
  delete helplabel1;
  delete helplabel2;
  delete hourchoice;
  delete minutechoice;
  delete spacelabel1;
}

xxStep::xxStep (xxTideContext *in_xtidecontext, xxContext *context,
void (*in_stepcallback) (Interval step, void *in_ptr),
void *in_ptr, Interval init): xxWindow (in_xtidecontext, context, 1,
XtGrabExclusive) {
  // Let's be reasonable here...
  assert (init.in_seconds() < 0x7FFFFFFF);
  long init_sec = max (init.in_seconds(), 60);
  hours = init_sec / HOURSECONDS;
  init_sec %= HOURSECONDS;
  minutes = init_sec / 60;
  assert (hours > 0 || minutes > 0);

  ptr = in_ptr;
  stepcallback = in_stepcallback;
  mypopup->setTitle ("Enter Step");

  Arg labelargs[3] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNborderWidth, (XtArgVal)0}
  };
  Arg buttonargs[4] =  {
    {XtNvisual, (XtArgVal)mypopup->visual},
    {XtNcolormap, (XtArgVal)mypopup->colormap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };

  {
    Dstr f = "Please enter the new step size in hours and minutes.";
    Widget labelwidget = XtCreateManagedWidget (f.aschar(), labelWidgetClass,
      container->manager, labelargs, 3);
    helplabel1 = new xxContext (mypopup, labelwidget);
  }

  static char *hourchoices[] = {"00",
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", NULL};
  static char *minutechoices[] = {"00",
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
    "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
    "41", "42", "43", "44", "45", "46", "47", "48", "49", "50",
    "51", "52", "53", "54", "55", "56", "57", "58", "59", NULL};

  hourchoice = new xxMultiChoice (container, &stephourcallback,
   (void*)this, hourchoices, hours);
  {
    Widget labelwidget = XtCreateManagedWidget (":", labelWidgetClass,
      container->manager, labelargs, 3);
    helplabel2 = new xxContext (mypopup, labelwidget);
  }
  minutechoice = new xxMultiChoice (container, &stepminutecallback,
   (void*)this, minutechoices, minutes);

  {
    Widget labelwidget = XtCreateManagedWidget ("  ", labelWidgetClass,
      container->manager, labelargs, 3);
    spacelabel1 = new xxContext (mypopup, labelwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Go", commandWidgetClass,
      container->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxStepCallback,
     (XtPointer)this);
    gobutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Cancel", commandWidgetClass,
      container->manager, buttonargs, 4);
    XtAddCallback (buttonwidget, XtNcallback, xxStepCancelCallback,
      (XtPointer)this);
    cancelbutton = new xxContext (mypopup, buttonwidget);
  }

  mypopup->realize();
}
