// $Id: TideEventsOrganizer.hh,v 1.4 2004/04/06 21:20:07 flaterco Exp $

// TideEventsOrganizer  Collect, sort, subset, and iterate over tide events.
// Refactored from similar notions previously appearing in
// TideContext, Calendar, Station, xxTextMode, Graph, ...

/*
    Copyright (C) 2004  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

typedef std::multimap<time_t, TideEvent>                   TideEventsMap;
typedef std::multimap<time_t, TideEvent>::iterator         TideEventsIterator;
typedef std::multimap<time_t, TideEvent>::reverse_iterator TideEventsRiterator;
typedef std::pair<const time_t, TideEvent>                 TideEventsPair;

class TideEventsOrganizer: public TideEventsMap {
public:
  // Add a TideEvent, making moderate effort to suppress duplicates.
  // An event is considered a duplicate if difference in times is < 60
  // seconds and event type is the same.
  void add (const TideEvent &te);
};
