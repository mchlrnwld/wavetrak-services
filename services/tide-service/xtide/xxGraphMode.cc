// $Id: xxGraphMode.cc,v 1.8 2004/10/22 17:43:51 flaterco Exp $
/*  xxGraphMode  Tide graphs in a window.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxGraphMode::dismiss () {
  delete this; // Is this safe?
}

void
xxGraphMode::help() {
  Dstr helpstring ("\
XTide Graph Mode Window\n\
\n\
Use the forward and backward buttons to scroll the graph forward\n\
or backward in time.  You can resize the window to see more or less\n\
detail.\n\
\n\
The options menu supplies the following commands:\n\
\n\
  Save:  Use this to write the tide predictions to a PNG image file.\n\
\n\
  Set Mark:  This is used to set the 'mark level' for tide predictions.\n\
  The prediction window will show the times when the tide level crosses\n\
  the mark.\n\
\n\
  Convert ft<->m:  Convert feet to meters or vice-versa.\n\
\n\
  Set Time:  Go to a different point in time.  Major adjustments that\n\
  cannot be made with the forward and backward buttons can be made with\n\
  Set Time.\n\
\n\
  Set Aspect:  This is used to change the aspect ratio for tide graphing.\n\
  The aspect ratio is a measure of how scrunched up or stretched out\n\
  the graph is.  If tide events are too close together, increase the\n\
  aspect; if you want to fit more information onto one graph, decrease\n\
  the aspect.\n\
\n\
  New Graph Window:  Duplicates the existing graph window.\n\
\n\
  New Plain Mode Window:  Open a window with a text listing of tide\n\
  predictions for the current location and time.\n\
\n\
  New Raw Mode Window:  Open a window with a text listing of unprocessed\n\
  numerical time stamps and tide levels for the current location and time.\n\
\n\
  New Medium Rare Mode Window:  Open a window with a text listing of\n\
  processed timestamps and unprocessed numerical tide levels for the\n\
  current location and time.\n\
\n\
  New Clock Window:  Open a window with a tide clock for the current\n\
  location.\n\
\n\
  About This Station:  Show station metadata.\n\
\n\
  New Location Chooser:  Open new globe and location list windows to allow\n\
  selecting a new location.");
  (void) xtidecontext->root->newHelpBox (helpstring);
}

void
xxGraphModeforwardCallback (Widget w, XtPointer client_data,
XtPointer call_data) {
  xxGraphMode *gm = (xxGraphMode *)client_data;
  gm->t += Interval(DAYSECONDS);
  gm->graph->drawTides (gm->station, gm->t);
  gm->draw();
}

void
xxGraphModebackwardCallback (Widget w, XtPointer client_data,
XtPointer call_data) {
  xxGraphMode *gm = (xxGraphMode *)client_data;
  gm->t -= Interval(DAYSECONDS);
  gm->graph->drawTides (gm->station, gm->t);
  gm->draw();
}

void
xxGraphModeResizeHandler (Widget w, XtPointer client_data,
    XEvent *event, Boolean *continue_dispatch) {
  xxGraphMode *g = (xxGraphMode *)client_data;
  switch (event->type) {
  case ConfigureNotify:
    {
      XConfigureEvent *ev = (XConfigureEvent *)event;
      Dimension newheight = ev->height;
      Dimension newwidth = ev->width;
      if (newheight != g->curwindowheight ||
	  newwidth != g->curwindowwidth) {
	g->curwindowheight = newheight;
	g->curwindowwidth = newwidth;
	g->curgraphheight = newheight - (g->origwindowheight - g->origgraphheight);
	g->curgraphwidth = newwidth - (g->origwindowwidth - g->origgraphwidth);
	g->redraw();
      }
    }
    break;
  default:
    ;
  }
}

void
xxGraphModeSaveCallback (Dstr &filename, void *in_ptr) {
  xxGraphMode *gm = (xxGraphMode *)in_ptr;
  // See externC.cc
  if ((png_file_ptr = fopen (filename.aschar(), "w"))) {
    RGBGraph g (gm->curgraphwidth, gm->curgraphheight, gm->xtidecontext->colors);
    g.drawTides (gm->station, gm->t);
    g.writeAsPNG (file_write_data_fn);
    fclose (png_file_ptr);
  } else {
    Dstr details (filename);
    details += ": ";
    details += strerror (errno);
    details += ".";
    barf (CANT_OPEN_FILE, details, 0);
  }
}

void
xxGraphMode::save () {
  (void) new xxFilename (xtidecontext, mypopup, xxGraphModeSaveCallback, this, "tides.png");
}

xxGraphMode::xxGraphMode (xxTideContext *in_tidecontext,
xxContext *in_xxcontext, Station *in_station, Timestamp t_in):
xxDrawable (in_tidecontext, in_xxcontext, in_station)
{
  t = t_in;
  construct ();
}

xxGraphMode::xxGraphMode (xxTideContext *in_tidecontext,
xxContext *in_xxcontext, Station *in_station): xxDrawable (in_tidecontext,
in_xxcontext, in_station)
{
  t = Timestamp((time_t)(time(NULL)));
  construct ();
}

void
xxGraphMode::construct () {
  Dstr title (station->name);
  title += " (Graph)";
  mypopup->setTitle (title);
  XtAddEventHandler (mypopup->manager, StructureNotifyMask, False,
 		       xxGraphModeResizeHandler, (XtPointer)this);

  Dimension minxgwidth = minwidthfudge * 5 + mypopup->stringWidth
    (mypopup->defaultfontstruct, "BackwardForwardOptionsDismiss?");

  origgraphheight = curgraphheight = (*(xtidecontext->settings))["gh"].u;
  origgraphwidth = curgraphwidth = max ((*(xtidecontext->settings))["gw"].u,
    minxgwidth);
  graph = new xxPixmapGraph (origgraphwidth, origgraphheight, mypopup);
  graph->drawTides (station, t);
  Arg labelargs[5] =  {
    {XtNbitmap, (XtArgVal)graph->pixmap},
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNinternalHeight, (XtArgVal)0},
    {XtNinternalWidth, (XtArgVal)0}
  };
  {
    Widget labelwidget = XtCreateManagedWidget ("",
      labelWidgetClass, container->manager, labelargs, 5);
    label = new xxContext (mypopup, labelwidget);
  }
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  {
    Widget buttonwidget = XtCreateManagedWidget ("Backward", repeaterWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxGraphModebackwardCallback,
     (XtPointer)this);
    backwardbutton = new xxContext (mypopup, buttonwidget);
  }
  {
    Widget buttonwidget = XtCreateManagedWidget ("Forward", repeaterWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxGraphModeforwardCallback,
     (XtPointer)this);
    forwardbutton = new xxContext (mypopup, buttonwidget);
  }

  addNormalButtons();
  // No call to redraw this time.
  mypopup->realize();

  {
    Arg args[2] = {
      {XtNwidth, (XtArgVal)(&origwindowwidth)},
      {XtNheight, (XtArgVal)(&origwindowheight)}
    };
    XtGetValues (container->manager, args, 2);
#ifdef SUPER_ULTRA_VERBOSE_DEBUGGING
    cerr << "Graph window original height " << origwindowheight << endl;
    cerr << "Graph window original width " << origwindowwidth << endl;
#endif
    curwindowheight = origwindowheight;
    curwindowwidth = origwindowwidth;
  }

  mypopup->setMinSize(minxgwidth + (origwindowwidth - origgraphwidth),
                      mingheight + (origwindowheight - origgraphheight));
}

void xxGraphMode::redraw() {
  // Save this until the window is updated to avoid a BadDrawable error
  xxPixmapGraph *oldgraph = graph;
  graph = new xxPixmapGraph (curgraphwidth, curgraphheight, mypopup);
  graph->drawTides (station, t);

  // Don't seem to have problems resizing this time.  See xxTextMode
  // for the kludge if needed.

  draw();
  delete oldgraph;
}

// You have to call drawTides on the graph before calling this.
void xxGraphMode::draw () {
  Arg args[1] = {
    {"bitmap", (XtArgVal)graph->pixmap}
  };
  XtSetValues (label->manager, args, 1);
  label->refresh();
}

xxGraphMode::~xxGraphMode() {
  mypopup->unrealize();
  delete graph;
  delete label;
  delete backwardbutton;
  delete forwardbutton;
}

int
xxGraphMode::is_graph() {
  return 1;
}

void xxGraphMode::global_redraw() {
  xxDrawable::global_redraw();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (backwardbutton)
    XtSetValues (backwardbutton->manager, buttonargs, 2);
  if (forwardbutton)
    XtSetValues (forwardbutton->manager, buttonargs, 2);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (label)
    XtSetValues (label->manager, args, 2);
}
