// $Id: xxLocationList.cc,v 1.4 2004/09/09 13:33:02 flaterco Exp $
/*  xxLocationList   Scrolling location chooser.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "xtide.hh"

void
xxLocationListButtonCallback (Widget w, XtPointer client_data, XtPointer call_data) {
  xxLocationList *ll = (xxLocationList *)client_data;
  int a;
  for (a=0; a<ll->numcontbuttons; a++)
    if (w == ll->contbuttons[a]->manager) {
      ll->freestringlist();
      ll->startat = a * ll_maxlen;
      ll->stringlist = ll->stationIndex->makeStringList(ll->startat, ll_maxlen);
      ll->listchanged();
      return;
    }
  assert (0);
}

void
xxLocationListHelpCallback (Widget w, XtPointer client_data,
XtPointer call_data) {
  xxLocationList *ll = (xxLocationList *)client_data;
  Dstr helpstring ("\
XTide Location Chooser -- Location List\n\
\n\
The location list is subordinate to the globe/map window.  When a region\n\
is shown in the globe/map window, all tide stations in that region are\n\
enumerated in the location list.  You can then use the scrollbar to\n\
view the entire list or left-click on locations in the list to obtain\n\
tide predictions.\n\
\n\
If the list is too long to display at one time, it will be broken into\n\
several smaller lists, and the location list window will sprout\n\
buttons to let you switch between them.\n\
\n\
You can change the sort field by pulling down the Sort button.\n\
\n\
The location list is dismissed when the globe/map window is dismissed.");
  (void) ll->xtidecontext->root->newHelpBox (helpstring);
}

void
xxLocationListChoiceCallback (Widget w, XtPointer client_data,
XtPointer call_data) {
  xxLocationList *ll = (xxLocationList *)client_data;
  XawListReturnStruct *xrs = (XawListReturnStruct*)call_data;
  StationRef *sr = (*(ll->stationIndex))[(xrs->list_index)+(ll->startat)];
  ll->xtidecontext->root->newGraph (sr);
}

void
xxLocationList::listchanged() {
  XawListChange (list->manager, stringlist, 0, 0, 1);
  // Un-do some damage
  Arg listargs[2] =  {
    {XtNwidth, (XtArgVal)ll_viewportwidth},
    {XtNheight, (XtArgVal)ll_viewportheight}
  };
  XtSetValues (viewport->manager, listargs, 2);
}

void
xxLocationList::dismiss () {
  delete myglobe; // What goes around, comes around.
}

void xxLocationList::do_contbuttons() {
  // Get rid of any existing buttons
  for (int a=0; a<numcontbuttons; a++) {
    XtUnmanageChild (contbuttons[a]->manager);
    delete contbuttons[a];
  }
  if (contbuttons) {
    free (contbuttons);
    contbuttons = NULL;
  }

  numcontbuttons = (int) ceil ((double)indexlen / (double)ll_maxlen);
  if (numcontbuttons == 1)
    numcontbuttons = 0;
  if (numcontbuttons) {
    assert (contbuttons = (xxContext **) malloc (numcontbuttons *
      sizeof (xxContext*)));
    for (int b=0; b<numcontbuttons; b++) {
      Arg buttonargs[2] =  {
        {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
        {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
      };
      char tbuf[80];
      unsigned long last = (b+1)*ll_maxlen-1;
      if (last >= stationIndex->length())
        last = stationIndex->length() - 1;
      // sprintf (tbuf, "%d-%d", b*ll_maxlen, (b+1)*ll_maxlen-1);
      switch (sortby) {
      case StationIndex::sf_name:
        sprintf (tbuf, "%c-%c", (*stationIndex)[b*ll_maxlen]->name[0],
                                (*stationIndex)[last]->name[0]);
        break;
      case StationIndex::sf_latitude:
        {
          Dstr lat1, lat2;
          (*stationIndex)[b*ll_maxlen]->coordinates.printlat (lat1);
          (*stationIndex)[last]->coordinates.printlat (lat2);
          sprintf (tbuf, "(%s)-(%s)", lat1.aschar(), lat2.aschar());
        }
        break;
      case StationIndex::sf_longitude:
        {
          Dstr lng1, lng2;
          (*stationIndex)[b*ll_maxlen]->coordinates.printlng (lng1);
          (*stationIndex)[last]->coordinates.printlng (lng2);
          sprintf (tbuf, "(%s)-(%s)", lng1.aschar(), lng2.aschar());
        }
        break;
      default:
        assert (0);
      }
      Widget buttonwidget = XtCreateManagedWidget (tbuf,
        commandWidgetClass, container->manager, buttonargs, 2);
      XtAddCallback (buttonwidget, XtNcallback, xxLocationListButtonCallback,
       (XtPointer)this);
      contbuttons[b] = new xxContext (container, buttonwidget);
    }
  }
}

void
sortbycallback (void *cbdata) {
  xxLocationList *ll = (xxLocationList *)cbdata;
  switch (ll->sortchoice->choice()) {
  case 0:
    ll->sortby = StationIndex::sf_name;
    break;
  case 1:
    ll->sortby = StationIndex::sf_latitude;
    break;
  case 2:
    ll->sortby = StationIndex::sf_longitude;
    break;
  default:
    assert (0);
  }
  ll->stationIndex->qsort (ll->sortby);
  ll->refreshList();
}

xxLocationList::xxLocationList (xxContext *context,
				StationIndex *in_stationIndex,
				xxTideContext *in_tidecontext,
                                xxWindow *globe_or_map): xxWindow (in_tidecontext, context, 1) {
  mypopup->setTitle ("Location List");
  myglobe = globe_or_map;
  contbuttons = NULL;
  numcontbuttons = 0;
  sortby = StationIndex::sf_name;
  stationIndex = in_stationIndex;
  startat = 0;
  indexlen = stationIndex->length();
  stringlist = stationIndex->makeStringList(startat, ll_maxlen);

  Arg labelargs[3] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNfont, (XtArgVal)mypopup->smallfontstruct}
  };
  Widget labelwidget = XtCreateManagedWidget (
"  Location Name                                                           \
         Type Latitude Longitude",
    labelWidgetClass, container->manager, labelargs, 3);
  label = new xxContext (container, labelwidget);

  Arg vpargs[6] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNallowVert, (XtArgVal)1},
    {XtNheight, (XtArgVal)ll_viewportheight},
    {XtNwidth, (XtArgVal)ll_viewportwidth},
    {XtNforceBars, (XtArgVal)1}
  };

  Widget viewportwidget = XtCreateManagedWidget ("", viewportWidgetClass,
    container->manager, vpargs, 6);
  viewport = new xxContext (container, viewportwidget);

  Widget scrollbarwidget;
  assert (scrollbarwidget = XtNameToWidget (viewportwidget, "vertical"));
  Arg sbargs[2] = {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  XtSetValues (scrollbarwidget, sbargs, 2);

  Arg listargs[7] = {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]},
    {XtNfont, (XtArgVal)mypopup->smallfontstruct},
    {XtNverticalList, (XtArgVal)1},
    {XtNdefaultColumns, (XtArgVal)1},
    {XtNforceColumns, (XtArgVal)1},
    {XtNlist, (XtArgVal)stringlist}
  };

  Widget listwidget = XtCreateManagedWidget ("", listWidgetClass,
    viewport->manager, listargs, 7);
  list = new xxContext (viewport, listwidget);
  XtAddCallback (listwidget, XtNcallback, xxLocationListChoiceCallback,
    (XtPointer)this);

  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };

  static char *sortchoices[] = {"Sort by Name", "Sort by Latitude",
    "Sort by Longitude", NULL};
  sortchoice = new xxMultiChoice (container, &sortbycallback, (void*)this,
    sortchoices, 0);

  {
    Widget buttonwidget = XtCreateManagedWidget ("?", commandWidgetClass,
      container->manager, buttonargs, 2);
    XtAddCallback (buttonwidget, XtNcallback, xxLocationListHelpCallback,
      (XtPointer)this);
    helpbutton = new xxContext (mypopup, buttonwidget);
  }

  do_contbuttons();

  mypopup->realize();
  mypopup->fixSize();
}

void
xxLocationList::freestringlist() {
  unsigned long i=0;
  while (stringlist[i])
    free (stringlist[i++]);
  free (stringlist);
}

void
xxLocationList::changeList (StationIndex *in_stationIndex) {
  delete stationIndex;
  stationIndex = in_stationIndex;
  // These always arrive in alphabetical order because the master
  // list stays in alphabetical order.
  if (sortby != StationIndex::sf_name)
    stationIndex->qsort (sortby);
  refreshList();
}

void
xxLocationList::refreshList() {
  freestringlist();
  startat = 0;
  indexlen = stationIndex->length();
  stringlist = stationIndex->makeStringList(startat, ll_maxlen);
  listchanged();
  do_contbuttons();
}

xxLocationList::~xxLocationList () {
  mypopup->unrealize();
  delete list;
  delete viewport;
  delete stationIndex;
  delete label;
  for (int a=0; a<numcontbuttons; a++)
    delete contbuttons[a];
  if (contbuttons)
    free (contbuttons);
  freestringlist();
  delete sortchoice;
  delete helpbutton;
}

void xxLocationList::global_redraw() {
  xxWindow::global_redraw();
  Arg buttonargs[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::button]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (helpbutton)
    XtSetValues (helpbutton->manager, buttonargs, 2);
  sortchoice->global_redraw();
  for (int a=0; a<numcontbuttons; a++)
    XtSetValues (contbuttons[a]->manager, buttonargs, 2);
  Arg args[2] =  {
    {XtNbackground, (XtArgVal)mypopup->pixels[Colors::background]},
    {XtNforeground, (XtArgVal)mypopup->pixels[Colors::foreground]}
  };
  if (label)
    XtSetValues (label->manager, args, 2);
  if (list)
    XtSetValues (list->manager, args, 2);
  assert (viewport);
  XtSetValues (viewport->manager, args, 2);
  Widget scrollbarwidget;
  assert (scrollbarwidget = XtNameToWidget (viewport->manager, "vertical"));
  XtSetValues (scrollbarwidget, args, 2);
}
