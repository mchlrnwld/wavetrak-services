// $Id: xxMultiChoice.hh,v 1.2 2003/01/17 17:31:00 flaterco Exp $
/*  xxMultiChoice  Multiple-choice button widget.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxMultiChoice {

  friend void xxMultiChoiceButtonCallback (Widget w, XtPointer client_data,
    XtPointer call_data);

public:
  // Create box with caption and everything.
  xxMultiChoice (xxContext *in_xxcontext, char *caption,
    char **in_choices, unsigned first_choice);
  // Put chooser in existing box, no caption, invoke callback after
  // changes if not NULL.
  xxMultiChoice (xxContext *in_boxcontext, void (*cb_in)(void*),
    void *cbdata_in, char **in_choices, unsigned first_choice);
  ~xxMultiChoice();

  unsigned choice ();
  void global_redraw();

  struct numberedbutton {
    xxContext *button;
    xxMultiChoice *parent;
    unsigned number;
  };

protected:
  void construct(); // Common code.
  int nobox; // 1 if box is from parent, 0 if box is created here.
  xxContext *box, *label, *button, *menu;
  void (*cb)(void*);
  void *cbdata;
  char **choices;
  xxMultiChoice::numberedbutton *choicebuttons;
  unsigned num_choices, current_choice;
};
