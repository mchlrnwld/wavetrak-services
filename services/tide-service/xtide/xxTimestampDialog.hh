// $Id: xxTimestampDialog.hh,v 1.1 2002/04/29 15:10:44 flaterco Exp $
/*  xxTimestampDialog  Embeddable timestamp chooser

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class xxTimestampDialog {

public:
  xxTimestampDialog (xxTideContext *in_xtidecontext, xxContext *in_xxcontext,
    char *caption, Timestamp init, const Dstr &in_timezone);
  ~xxTimestampDialog();

  // Returns an ISO 8601 string.  It's not a Timestamp because then you
  // would just get a null timestamp in case of error.
  void val (Dstr &iso_string_out);
  void global_redraw();

  friend void timestampdialogyearcallback (void *cbdata);
  friend void timestampdialogmonthcallback (void *cbdata);
  friend void timestampdialogdaycallback (void *cbdata);
  friend void timestampdialoghourcallback (void *cbdata);
  friend void timestampdialogminutecallback (void *cbdata);

protected:
  unsigned year, month, day, hour, minute;
  Dstr timezone;
  xxTideContext *xtidecontext;

  xxContext *box, *label, *spacelabel1, *spacelabel2;
  xxMultiChoice *yearchoice, *monthchoice, *daychoice, *hourchoice,
    *minutechoice;
};
