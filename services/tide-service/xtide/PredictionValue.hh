// $Id: PredictionValue.hh,v 1.4 2004/09/16 15:07:23 flaterco Exp $
/*  PredictionValue  feet, meters, knots, et cetera.

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

class PredictionValue {
  friend PredictionValue operator+ (PredictionValue a, PredictionValue b);
  friend PredictionValue operator- (PredictionValue a, PredictionValue b);
public:
  class Unit {
    friend PredictionValue operator+ (PredictionValue a, PredictionValue b);
  public:
    enum Units {Feet=0, Meters=1, Knots=2, KnotsSquared=3};
    Units mytype;
    Unit ();
    Unit (const Dstr &name_in);
    Unit (Units units_in);
    void shortname (Dstr &name_out) const;
    void longname (Dstr &name_out) const;
  };
  PredictionValue ();
  PredictionValue (Unit inunits, double inval);
  double val() const;
  virtual void val(double newval);

  // G++ had an undiagnosed problem with naming this setUnits.
  // Any conversion is possible if value is 0.0; otherwise,
  // conversions are enforced.
  void Units (Unit tounits);
  void Units (PredictionValue::Unit::Units tounits);
  Unit Units () const;

  // This insists that both values must have exactly the same units.
  // When it is time to add the datum for a hydraulic station, you must
  // first explicitly convert the amplitude from KnotsSquared to Knots.
  // This should prevent any unexpected conversions.
  // Exception:  If value is 0 (uninitialized), adopt units of non-zero value.
  PredictionValue &operator+= (PredictionValue in_val);
  PredictionValue &operator-= (PredictionValue in_val);

  // Print in the form -XX.YY units (padding as needed)
  void print (Dstr &print_out) const;
  // Same thing without padding, with abbreviated units.
  void printnp (Dstr &print_out) const;

  virtual PredictionValue &operator*= (double levelMultiply);

protected:
  double myvalue;
  Unit myunits;
};

PredictionValue operator* (double a, PredictionValue b);
PredictionValue operator* (PredictionValue b, double a);
int operator> (PredictionValue a, PredictionValue b);
int operator< (PredictionValue a, PredictionValue b);
int operator>= (PredictionValue a, PredictionValue b);
int operator<= (PredictionValue a, PredictionValue b);
int operator== (PredictionValue a, PredictionValue b);
int operator!= (PredictionValue a, PredictionValue b);
int operator== (PredictionValue::Unit a, PredictionValue::Unit b);
int operator!= (PredictionValue::Unit a, PredictionValue::Unit b);
// int operator== (PredictionValue::Unit a, PredictionValue::Unit::Units b);
double operator/ (PredictionValue a, PredictionValue b);
PredictionValue operator/ (PredictionValue b, double a);
PredictionValue operator/ (PredictionValue b, long a);

// This insists that both values must have exactly the same units.
// When it is time to add the datum for a hydraulic station, you must
// first explicitly convert the amplitude from KnotsSquared to Knots.
// This should prevent any unexpected conversions.
PredictionValue operator+ (PredictionValue a, PredictionValue b);
PredictionValue operator- (PredictionValue a, PredictionValue b);

PredictionValue operator- (PredictionValue a);
PredictionValue abs (PredictionValue a);
