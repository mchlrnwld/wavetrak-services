// $Id: xmlstruct.cc,v 1.2 2002/10/04 13:44:01 flaterco Exp $
/*  xmlstruct  Structures for storing parse tree produced by
    limited XML parser (just tags, no text).

    Copyright (C) 1998  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

#if 0
ostream &operator<< (ostream &out, const struct xmltag *v) {
  if (!v)
    return out;
  out << "<" << (*(v->name));
  if (v->attributes) {
    out << "\n";
    out << v->attributes;
  }
  if (v->contents) {
    out << ">" << endl;
    out << v->contents;
    out << "</" << (*(v->name)) << ">";
  } else {
    out << "/>";
  }
  out << "\n";
  out << v->next;
  return out;
}

ostream &operator<< (ostream &out, const struct xmlattribute *v) {
  if (!v)
    return out;
  out << (*(v->name)) << "=\"" << (*(v->value)) << "\"\n";
  out << v->next;
  return out;
}
#endif

void freexml (struct xmltag *v) {
  if (!v)
    return;
  freexml (v->next);
  freexml (v->contents);
  freexml (v->attributes);
  delete v->name;
  delete v;
}

void freexml (struct xmlattribute *v) {
  if (!v)
    return;
  freexml (v->next);
  delete v->value;
  delete v->name;
  delete v;
}
