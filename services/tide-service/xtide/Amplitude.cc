/*  $Id: Amplitude.cc,v 1.2 2004/10/19 18:01:37 flaterco Exp $
    Amplitude  A non-negative PredictionValue.

    Copyright (C) 1997  David Flater.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "common.hh"

Amplitude::Amplitude (Unit inunits, double inval):
PredictionValue (inunits, inval) {
  assert (inval >= 0.0);
}

Amplitude::Amplitude (): PredictionValue () {}

void Amplitude::val (double newval) {
  assert (newval >= 0.0);
  myvalue = newval;
}

double Amplitude::val () {
  return myvalue;
}

Amplitude &Amplitude::operator= (PredictionValue a) {
  val (a.val());  // Checks that it is >= 0.0
  myunits = a.Units();
  return (*this);
}

PredictionValue &
Amplitude::operator*= (double levelMultiply) {
  assert (levelMultiply > 0.0);
  myvalue *= levelMultiply;
  return (*this);
}
