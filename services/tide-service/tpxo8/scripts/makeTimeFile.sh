#! /bin/sh

LAT=$1
LON=$2
INC=$3		#seconds
STEPS=$4

if [ -z "$6" ]
then
	TS=$5
else
	TS=`date -u +%s -s"$5-$6-$7 $8:$9:${10}"`
fi

COUNT=0
while [ $COUNT -lt $STEPS ]
do

	echo -n "$LAT $LON "
	date -u +"%Y %m %d %H %M %S" -d @$TS

	TS=`expr $TS + $INC`

	COUNT=`expr $COUNT + 1`
done
