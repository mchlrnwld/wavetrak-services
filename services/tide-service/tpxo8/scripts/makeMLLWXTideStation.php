<?php

date_default_timezone_set("UTC");

if($argc < 6) {
	error_log("Usage: ".$argv[0]." lat lon name gmtoffset timezone\n");
	die(1);
}

$lat = $argv[1];
$lon = $argv[2];
$name = $argv[3];
$offset = $argv[4];
$timezone = $argv[5];
$HCOUT=trim(shell_exec('mktemp'));

shell_exec($_SERVER['PWD']."/getHC.sh $lat $lon $HCOUT");
$ALWOUT=shell_exec($_SERVER['PWD']."/getMLLW.sh $lat $lon");

$ala = explode("\n",$ALWOUT);

if (count($ala) < 2) {
    error_log("\n\nERROR\n\nNo average low water level. Something is awry. Probably lat/lon point is out of model grid OR on land\n\n");
    die(1);
}

$alb = explode(":",$ala[0]);

if (count($alb) < 2) {
    error_log("\n\nERROR\n\nNo average low water level. Something is awry. Probably lat/lon point is out of model grid OR on land\n\n");
    die(1);
}
$alw = (float)$alb[1];

// This code will deal with multiple points in the output of getHC, but the above code won't. So don't.
$stations = array();

$f = file($HCOUT);
$count=0;
$cols = array();
foreach ($f as $line) {
    if (preg_match("/Site is out of model grid OR land/", $line) === 1) {
        error_log("\n\nERROR\n\nLat/lon point is out of model grid OR on land\n\n");
        die(1);
    }

	// skip first two lines
	if($count++<2) {
		continue;
	}

	$bits = preg_split("/ +/", trim($line));
	if($bits[0] == 'Lat') {
		$colCount = 2;
		foreach($bits as $piece) {
			if($piece != 'Lat' && $piece != 'Lon') {
				$chunks = explode('_', $piece);
				$cols[$chunks[0]][$chunks[1]] = $colCount++;
			}
		}
		continue;
	}
	if($bits[0] == 'HC') {
		continue; //rubbish
	}


	$station = array();
	$station['Lat'] = $bits[0];
	$station['Lon'] = $bits[1];
	foreach($cols as $constituent => $cons){
		$station[$constituent]['amp'] = $bits[$cons['amp']];
		$station[$constituent]['ph'] = $bits[$cons['ph']];
	}

	$stations[] = $station;

}

foreach($stations as $station) {

	print "#\n";
	print "# source TPXO8 07.14.2014\n";
	print "# date_imported: ".date('Ymd')."\n";
	print "# datum: Mean Lower Low Tide MLLW\n";
	print "# confidence: 5\n";
	print "# !units: meters\n";
	print "# !latitude: ".$station['Lat']."\n";
	print "# !longitude: ".$station['Lon']."\n";

	print "$name\n";
	print "$offset :$timezone\n";
	print abs($alw)." meters\n";

	// constituents HAVE to go the correct order
	ksort($station);
	foreach($station as $key=>$con) {
		if($key != 'Lat' && $key != 'Lon') {
			print strtoupper($key)." ".$con['amp']." ".$con['ph']."\n";
		}
	}

}

?>
