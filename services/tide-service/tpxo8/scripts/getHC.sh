#! /bin/sh
LAT=$1
LON=$2
TEMPLATE=./setup.template

if [ -z $3 ]
then
	SO=1
	OUTFILE=`mktemp -p /dev/shm`
else
	SO=0
	OUTFILE=$3
fi
SETUPFILE=`mktemp -p /dev/shm`
LATLONFILE=`mktemp -p /dev/shm`
TMPF=`mktemp -p /dev/shm`

cp $TEMPLATE $SETUPFILE

sed -i "s|INFILE|$LATLONFILE|g" $SETUPFILE
sed -i "s|OUTFILE|$OUTFILE|g" $SETUPFILE


ETY=583416000 # 18.5 years in seconds (approx) 972360
FTY=291708000	# 9 years in seconds (for brevity)
NOW=`date +%s`
THEN=`expr $NOW - $FTY`



# The step and start here are fixed to the US National Tidal Datum Epoch
#php makeTimeFile.php $LAT $LON 600 999360 410227200 > $LATLONFILE
php makeTimeFile.php $LAT $LON 600 486180 $THEN > $LATLONFILE

#./predict_tide < $SETUPFILE > /dev/null 2>&1 
../OTPS2/extract_HC < $SETUPFILE

if [ $SO -eq 1 ]
then
	cat $OUTFILE
	rm -f $OUTFILE
fi	

rm -f $SETUPFILE $LATLONFILE $TMPF
