#! /bin/sh
LAT=$1
LON=$2
TEMPLATE=./setup.template
OUTFILE=`mktemp -p /dev/shm`
SETUPFILE=`mktemp -p /dev/shm`
LATLONFILE=`mktemp -p /dev/shm`
TMPF=`mktemp -p /dev/shm`

cp $TEMPLATE $SETUPFILE

sed -i "s|INFILE|$LATLONFILE|g" $SETUPFILE
sed -i "s|OUTFILE|$OUTFILE|g" $SETUPFILE


ETY=583416000 # 18.5 years in seconds (approx) 972360
FTY=291708000	# 9 years in seconds (for brevity)
NOW=`date +%s`
THEN=`expr $NOW - $FTY`



# The step and start here are fixed to the US National Tidal Datum Epoch
#php makeTimeFile.php $LAT $LON 600 999360 410227200 > $LATLONFILE
php makeTimeFile.php $LAT $LON 600 486180 $THEN > $LATLONFILE

../OTPS2/predict_tide < $SETUPFILE > /dev/null 2>&1 
#./extract_HC < $SETUPFILE

#cat $OUTFILE
tail -n+8 $OUTFILE | awk {'print $5'} | sort -g > $TMPF

echo -n "HAT:"
tail -1 $TMPF

echo -n "LAT:"
head -1 $TMPF


rm -f $OUTFILE $SETUPFILE $LATLONFILE $TMPF
