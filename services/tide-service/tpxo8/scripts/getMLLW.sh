#! /bin/sh
LAT=$1
LON=$2
TEMPLATE=./setup.template
OUTFILE=`mktemp -p /dev/shm`
SETUPFILE=`mktemp -p /dev/shm`
LATLONFILE=`mktemp -p /dev/shm`
TMPF=`mktemp -p ../tmp`

cp $TEMPLATE $SETUPFILE

sed -i "s|INFILE|$LATLONFILE|g" $SETUPFILE
sed -i "s|OUTFILE|$OUTFILE|g" $SETUPFILE

# The step and start here are fixed to the US National Tidal Datum Epoch
php makeTimeFile.php $LAT $LON 600 999360 410227200 > $LATLONFILE

../OTPS2/predict_tide < $SETUPFILE > /dev/null 2>&1 

tail -n+8 $OUTFILE |awk '{
  min[$3] = !($3 in min) ? $5 : ($5 < min[$3]) ? $5 : min[$3]
}
END {
  for (i in min)
    print i, min[i]
}' |grep "\S" | awk '{sum+=$2} END {print sum/NR}' > $TMPF

echo -n "MLLW:"
head -1 $TMPF

rm -f $OUTFILE $SETUPFILE $LATLONFILE $TMPF
