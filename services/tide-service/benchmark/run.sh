#! /bin/sh
# shellcheck disable=SC2039

VUs="1 2 4 8 11 16 23 32"
duration="90s"

usage="
  Usage: runs.sh [base domain] [test run name]

  e.g:
    run.sh 'https://services.staging.surfline.com/tide' benchmark_staging
"


if [[ "$1" == "" ]]; then
  echo "${usage}"
  exit 1;
else
  export DOMAIN="$1";
fi

if [[ "$2" == "" ]]; then
  echo "${usage}"
  exit 1;
else
  name="$2";
fi

if [[ "$3" == "" ]]; then
  title="Request duration and Request per second vs k6 VUs"
else
  title="$3";
fi

mkdir -p "${name}"
echo "# Results file for GNU plot" > "${name}/summary.dat"
echo "# VUs, http_req_duration.med , http_req_duration.p(95), http_reqs.rate" > "${name}/summary.dat"

for vus in ${VUs}; do
  k6 run --vus "${vus}" --duration "${duration}" --summary-export "${name}/data_${vus}vus.json" script.js
  median=$(jq '.metrics.http_req_duration.med' "${name}/data_${vus}vus.json")
  p95=$(jq '.metrics.http_req_duration["p(95)"]' "${name}/data_${vus}vus.json")
  rps=$(jq '.metrics.http_reqs.rate' "${name}/data_${vus}vus.json")
  echo "${vus} ${median} ${p95} ${rps}" >> "${name}/summary.dat"
done;

echo "===== Createing '${name}.png' ====="

gnuplot <<<"
  set terminal png size 640,480 font 'helvetica,12'
  set ytics nomirror
  set y2tics 0, 1
  set yrange [0:]
  set y2range [0:]
  set style line 1 lt 1 lc rgb '#586e75' lw 2
  set style line 2 lt 1 lc rgb '#839496' lw 2
  set style line 3 lt 1 lc rgb '#d33682' lw 2
  set xlabel 'k6 VUs'
  set ylabel 'Request time (ms)'
  set y2label 'RPS'
  set title '${title}'
  set output '${name}.png'
  plot \
    '${name}/summary.dat' using 1:4 title 'rps' with lines linestyle 3 axis x1y2, \
    '${name}/summary.dat' using 1:2 title 'median duration' with lines linestyle 1, \
    '${name}/summary.dat' using 1:3 title 'P(95) duration' with lines linestyle 2
"
