import http from 'k6/http';

import { sleep } from 'k6';

const base = __ENV.DOMAIN;

const ports = [
  "River%20Yealm",
  "Devonport,%20England",
  "Whitsand%20Bay",
  "Lyme%20Regis",
  "St%20Peter%20Port,%20MSW",
  "Braye,%20Alderney%20Isl",
  "Weymouth,%20England",
  "Bude",
  "Newquay",
  "Perranporth",
  "Omonville",
  "Heaux-de-Brehat",
  "Dielette",
  "Plougrescan,%20Treguier%20Rvr",
  "Ploumanach",
  "Porthleven",
  "Ile%20de%20Brehat",
  "St.%20Helier,%20Jersey,%20Channel%20Islands",
  "Hinkley%20Point,%20England",
  "Lezardrieux",
  "Paimpol",
  "Cherbourg,%20France",
  "Bournemouth,%20England",
  "Ilfracombe,%20England",
  "Carteret",
  "Newlyn,%20England",
  "Roscoff",
  "Morlaix%20River%20Entr",
  "Les%20Minquiers",
  "Totland%20Bay",
];

export default function () {

  // Pick a timestamp randomly between 2001 and 2037
  const start = Math.floor(1000000000 + 1140000000 * Math.random());
  // Pick a random port
  const port = ports[Math.floor(Math.random() * ports.length)];

  // Get the data
  http.get(`${base}/getDataJSON.php?port=${port}&start=${start}`);
  // Get the image
  http.get(`${base}/getChart.php?port=${port}&start=${start}&timezone=UTC`);

  sleep(1);
}
