<?php

/**
 * This class is a wrapper for tide/xtide
 * @author Nick Lott
 * @date 22/03/05
 */
class Tide {

    var $location;
    var $start;
    var $end;
    var $step;

    // -- Format Settings --
    // Date Format
    var $dateFormat = '%x';

    // Time Format
    var $timeFormat = '%r';

    // Hour Format (time axis)
    var $hourFormat = '%k';

    // units (m|ft)
    var $units = 'm';

    //experimental UTC
    var $UTC = false;

    // -- Graph Settings --
    // Aspect - Controls end time!
    var $aspect = '1';

    // Graph Height also controls end time
    var $height = '60';

    // Graph Width
    var $width = '500';

    // Line Width
    var $lineWidth = "2.5";

    // Fill Tide Curves? (n|y)
    var $drawAsLine = "n";

    // Suppress astronomical events
    var $suppressAstro = "n";

    // Height Lines on Top
    var $topLines = "n";

    // Suppress moon text
    var $suppressMoon = "n";

    // Suppress title
    var $showTitle = "n";

    // Show time text
    var $showTimeText = "n";

    // Show time axis
    var $showTimeAxis = "n";

    // Suppress X?
    var $suppressX = "n";

    // Suppress Left margin?
    var $suppressMargin = "n";

    // -- Colour Settings
    // Daylight color
    var $dayColor = "FFFFFF";

    // Nighttime color
    var $nightColor = "999999";

    // Ebbing tide color
    var $ebbColor = "117711";

    // Flooding tide color
    var $floodColor = "1177BB";

    // -- Font Settings
    // Font colour
    var $fontColor = "000000";

    var $twilightColor = "DDDDDD";

    /**
     * Constructor, King of the Constructicons
     */
    function Tide($location,$time='') {
        if ($time =='') {
            $time = time();
        }
        $this->location = $location;
        $this->start    = gmdate("Y-m-d H:i",$time);
        $this->_startUTC = $time;
        $this->time     = $time;
        $this->tidePath = '/usr/local/bin/tide';
        putenv('HOME=/home/tide');
    }

    /**
     * Set the times span to operate over
     * @param string $start UTC Timestamp of Start
     * @param string $end UTC Timestamp of End
     */
    function setTimes($start, $end='') {
        $this->start        = gmdate("Y-m-d H:i",$start);
        $this->_startUTC    = $start;
        if($end == '') {
            $end = $start+(3600*48);
        }
        $this->_endUTC      = $end;
        $this->end          = gmdate("Y-m-d H:i", $end);
        $this->time         = $start;
    }

    /**
     * Set units.
     */
    function setUnits($unit) {
        if($unit == 'us' || $unit == 'ft') {
            $this->units = 'ft';
        } else {
            $this->units = 'm';
        }
    }

    function makeOptionString() {

        $cmdString = '';
        $cmdString .= ' -df "'.$this->dateFormat.'"';
        $cmdString .= ' -tf "'.$this->timeFormat.'"';
        $cmdString .= ' -hf "'.$this->hourFormat.'"';
        $cmdString .= ' -u '.$this->units;
        $cmdString .= " -ga ".$this->aspect;
        $cmdString .= " -gh ".$this->height;
        $cmdString .= " -gw ".$this->width;
        $cmdString .= " -lw ".$this->lineWidth;
        $cmdString .= " -nf ".$this->drawAsLine;
        $cmdString .= " -ns ".$this->suppressAstro;
        $cmdString .= " -tl ".$this->topLines;
        $cmdString .= " -nm ".$this->suppressMoon;
        $cmdString .= " -tc ".$this->hex2rgb($this->twilightColor);
        $cmdString .= " -st ".$this->showTitle;
        $cmdString .= " -tt ".$this->showTimeText;
        $cmdString .= " -ta ".$this->showTimeAxis;
        $cmdString .= " -nx ".$this->suppressX;
        $cmdString .= " -nl ".$this->suppressMargin;
        $cmdString .= " -dc ".$this->hex2rgb($this->dayColor);
        $cmdString .= " -nc ".$this->hex2rgb($this->nightColor);
        $cmdString .= " -ec ".$this->hex2rgb($this->ebbColor);
        $cmdString .= " -fc ".$this->hex2rgb($this->floodColor);
        //$cmdString .= " -fg ".$this->hex2rgb($this->fontColor);

        return $cmdString;
    }

    function hex2rgb($hex) {
        $ary = $this->str_split2($hex,2);
        return "rgb:".$ary[0]."/".$ary[1]."/".$ary[2];
    }

    function str_split2($string, $split_length = 1) {
        return explode("\r\n", chunk_split($string, $split_length));
    }

    /**
     * Returns the height of the tide at the given time
     */
    function getHeight() {
        //tide -z -l 'Yorktown' -b "2005-03-24 10:30" -e "2005-03-24 10:31" -mr
        $output = shell_exec($this->tidePath.' -u m -z -l "'.$this->location.'" -b "'.$this->start.'" -e "'.gmdate("Y-m-d H:i",$this->time + 60).'" -mr 2> /dev/null');
        $values = explode(" ",$output);
        return (float)$values[1];
    }

    /**
     * Returns an array containing Rising/Falling and the % "fullness" at the given time
     *
     * @param Boolean $getFullness - call $this->getMinMax() to retrieve % "fullness" - only use via CLI / for precaching, as this is very, VERY slow.
     */
    function getTideLevel($getFullness = true) {
        ///usr/local/bin/tide -z -l "Yorktown" -b "2005-03-22 11:07" -e "2005-03-24 11:07" -mp -ns -u m -tf "%s" -df " "
        $height = round($this->getHeight(),2);
        $cmdString = $this->tidePath.' -z -mm -ns -u '.$this->units.' -tf "%s" -df "--"';

        if(isset($this->end)) {
            $cmdString .= ' -e "'.$this->end.'"';
        }

        // round to nearest hour
        $mystart = round($this->_startUTC / 3600) *3600;

        $command = $cmdString.' -l "'.$this->location.'" -b "'.gmdate("Y-m-d H:i",$mystart-3600).'"  2> /dev/null';

        $output = explode("\n",shell_exec($command));

        $tidesArray = array();

        foreach($output as $line) {
            if (substr($line,0,2) == "--") {
                /*
                -- 1412899200 6.847860
                -- 1412902800 6.299984
                -- 1412906400 5.186018
                -- 1412910000 3.840856
                -- 1412913600 2.608613
                -- 1412917200 1.580331
                */
                // -- 1111603825   0.08 meters  Low Tide
                $bits = preg_split ( "/[ ]+/" , $line );
                $time = $bits[1];
                $height = $bits[2];

                $tidesArray[$time]['height'] = $height;
            }
        }

        $first = reset($tidesArray);
        $oldHeight = $first['height'];


        if ($getFullness) {
            $mm = $this->getMinMax();
            $max = $mm['max']['height'];
            $min = $mm['min']['height'];
            $difference = $max - $min;
        }

        foreach($tidesArray as $time => $tide) {
            if ($tide['height'] > $oldHeight) {
                $tidesArray[$time]['direction'] = 'Rising';
            } elseif ($tide['height'] < $oldHeight) {
                $tidesArray[$time]['direction'] = 'Falling';
            } else {
                $tidesArray[$time]['direction'] = 'Slack';
            }
            $oldHeight = $tide['height'];
            if ($getFullness) $tidesArray[$time]['fullness'] = round(($tide['height'] / $difference) * 100);
        }

        $returnArray = array();

        foreach ($tidesArray as $time => $tide) {
            if ($time >= $this->_startUTC && $time <= $this->_endUTC) {
                $returnArray[$time] = $tide;
            }
        }

        return $returnArray;
    }

    /**
     * Get Min and Max Tides for the set time period direct from xtide
     */
    function getMinMax() {

        /*
        Max was   7.20 meters at -- 1443614739
        Min was   0.15 meters at -- 1427008270
        */

        $cmdString = $this->tidePath.' -z -ms -ns -u m -tf "%s" -df "--"';

        $command = $cmdString.' -l "'.$this->location.'" -b "'.gmdate("Y-m-d H:i",$this->time-189216000).'" -e "'.gmdate("Y-m-d H:i",$this->time+189216000).'" | tail -2 2> /dev/null';

        $output = explode("\n",shell_exec($command));

        $ret = array();
        foreach($output as $line) {
            $bits = preg_split ( "/[ ]+/" , $line );
            $ret[strtolower($bits[0])]['height'] = trim($bits[2]);
            $ret[strtolower($bits[0])]['time'] = trim($bits[6]);
        }

        return $ret;
    }

    /**
     * Returns an array containing the next $qty low or high tides, from the given time out to 10 days in the future.
     */
    function getNextTides() {

        $this->setTimes($this->_startUTC, $this->time+(3600*24*10));

        return $this->getTides();

    }

    /**
     * Returns an array containing the next $qty low or high tides, for the given time period
     */
    function getTides() {
        $cmdString = $this->tidePath.' -z -mp -ns -u '.$this->units.' -tf "%s" -df "--"';

        $command = $cmdString.' -l "'.$this->location.'" -b "'.$this->start.'" -e "'.$this->end.'" 2> /dev/null';

        $output = explode("\n",shell_exec($command));

        $tidesArray = array();

        $counter = 0;
        foreach($output as $line) {
            if (substr($line,0,2) == "--") {
                $counter++;
                // -- 1111581634   0.74 meters  High Tide
                // -- 1111603825   0.08 meters  Low Tide

                $time   = substr($line,3,10);
                $myheight = trim(substr($line,15,5));
                if($this->units == 'm') {
                    $text   = trim(substr($line,29,4));
                } else {
                    $text   = trim(substr($line,27,4));
                }

                $tidesArray[] = array('time' => $time, 'height' => $myheight, 'text' => $text);
            }
        }
        return $tidesArray;
    }



    /**
     * This will create and return (in raw png) the tide graph
     */
    function getTideChart() {

        $cmdString = $this->tidePath.' -mg -fp';
        if($this->UTC) {
            $cmdString .= ' -z';
        }

        $cmdString .= ' -b "'.$this->start.'"';

        $command = $cmdString.$this->makeOptionString().' -l "'.$this->location.'" -b "'.$this->start.'" -e "'.gmdate("Y-m-d H:i",$this->time+(3600*24*10)).'"';

        return shell_exec($command);

    }

    function getStats() {
        $tideData= shell_exec($this->tidePath.' -l"'.$this->location.'" -mp -fc -b "'.$this->start.'" -e "'.$this->end.'" -u'.$this->units.' -ns -tf "%H:%M"');

        /*
        River Yealm,2009-12-29,21:47,1.63 m,Low Tide
        River Yealm,2009-12-30,04:07,5.06 m,High Tide
        River Yealm,2009-12-30,10:22,1.48 m,Low Tide
        */

        $dataAry = explode("\n",$tideData);

        $maxhi      = 0;
        $minhi      = 999;
        $maxlo      = 0;
        $minlo      = 999;
        $minRange   = 999;
        $maxRange   = 0;
        $loTotal    = 0;
        $hiTotal    = 0;
        $loCount    = 0;
        $hiCount    = 0;
        $rangeCount = 0;
        $rangeTotal = 0;

        foreach($dataAry as $dataLine) {
                $databits = explode(",",$dataLine);
                $heightbits = explode(" ",$databits[3]);
                if($heightbits[0] > $maxheight) {
                        $maxheight = $heightbits[0];
                }
                if($heightbits[0] < $minheight) {
                        $minheight = $heightbits[0];
                }

                $hilo = trim($databits[4]);
                switch($hilo) {
                    case 'High Tide':
                        $hiTotal += $heightbits[0];
                        $hiCount++;
                        if($heightbits[0] > $maxhi) {
                                $maxhi = $heightbits[0];
                        }
                        if($heightbits[0] < $minhi) {
                                $minhi = $heightbits[0];
                        }
                        $prevHigh = $heightbits[0];


                    break;
                    case 'Low Tide':
                        $loTotal += $heightbits[0];
                        $loCount++;
                         if($heightbits[0] > $maxlo) {
                                $maxlo = $heightbits[0];
                        }
                        if($heightbits[0] < $minlo) {
                                $minlo = $heightbits[0];
                        }
                        if(isset($prevHigh)) {
                                $range = $prevHigh-$heightbits[0];
                                $rangeTotal += $range;
                                $rangeCount++;
                                if($range>$maxRange) { $maxRange = $range;}
                                if($range<$minRange) { $minRange = $range;}
                        }
                    break;
                }
        }

        $avglo = round($loTotal / $loCount,3);
        $avghi = round($hiTotal / $hiCount,3);
        $avgRange = round($rangeTotal / $rangeCount,3);

        $out = "Port: ".$this->location."\n";
        $out .= "Data Points: ".count($dataAry)."\n";
        $out .= "Max High: $maxhi\n";
        $out .= "Min High: $minhi\n";
        $out .= "Avg High: $avghi\n";
        $out .= "Max Low: $maxlo\n";
        $out .= "Min Low: $minlo\n";
        $out .= "Avg Low: $avglo\n";
        $out .= "Max Range: $maxRange\n";
        $out .= "Min Range: $minRange\n";
        $out .= "Avg Range: $avgRange\n";

        return $out;
    }
}
