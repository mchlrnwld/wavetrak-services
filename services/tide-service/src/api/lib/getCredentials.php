<?php

function getCredentials($env)
{
    if (array_key_exists("AWS_CONTAINER_CREDENTIALS_RELATIVE_URI", $env))
    {
        try {
            $g = new Guzzle\Http\Client();
            $req = $g->get("http://169.254.170.2".$env["AWS_CONTAINER_CREDENTIALS_RELATIVE_URI"]);
            $res = $req->send();
            $json = $res->json();
        }
        catch (Exception $e)
        {
            throw new Exception("Error getting ECS credentials ".$e);
        }

        return array(
          "key" => $json["AccessKeyId"],
          "secret" => $json["SecretAccessKey"],
          "token" => $json["Token"],

        );
    }
    else if (array_key_exists("AWS_REGION", $env)) {
        return array(
            "region" => $_ENV["AWS_REGION"],
            "signature" => "v4",
        );
    }

    return array();
}
