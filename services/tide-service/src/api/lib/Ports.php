<?php
namespace msw\api\lib;

include_once("/var/www/tide/vendor/autoload.php");

Class Ports
{
    const FIELDS = "name, lat, lon, minHeight, maxHeight";

    private $db;

    public function __construct($connection_str = "sqlite:/usr/share/xtide/ports.sqlite3")
    {
        $this->db =  new \PDO($connection_str);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }

    public function fetchAll()
    {
        $data = $this->db->query("SELECT ".self::FIELDS." FROM tideport order by name")->fetchAll();
        return array_map("self::transformPortData", $data);
    }

    public function fetchByName($name)
    {
        $f = self::FIELDS;
        $sql = "SELECT $f from tideport WHERE name = :name;";
        $statement = $this->db->prepare($sql);
        $statement->execute(array( ":name" => $name ));

        return array_map("self::transformPortData", $statement->fetchAll());
    }

    /**
     * @param Float $lat        Latitude
     * @param Float $lon        Longituce
     * @param Float $distance   Max distance in km
     * @param Int $limit        max number to return. Will return nearest if there are >= $limit
     *  within distance.
     */
    public function fetchNear($lat, $lon, $distance=100, $limit=20)
    {
        list($x, $y, $z) = \msw\tools\lib\Maths::makeXYZ($lat, $lon);

        $f = self::FIELDS;

        // Approximately distance squared
        $d2 = "(x - :x)*(x - :x) + (y -  :y)*(y -  :y) + (z - :z)*(z - :z)";

        // the bounds diercly on x, y, z are a box of side $distance around the point
        // providing an easy filter (since x, y, z as indexed). The final filter is using
        // x, y, z and pythagoras to bound the distace to a sphere aroung the desired point.
        // Not exaclty right as it's chord length rather than "great circle" length, But it's
        // accurate to <=30m over 300km, wich is a lot further that we should be looking for
        // tide ports.
        $sql = <<<SQL
            SELECT $f, $d2 as d2 FROM tideport
            WHERE x >= :x - :d AND x <= :x + :d
              AND y >= :y - :d AND y <= :y + :d
              AND z >= :z - :d AND z <= :z + :d
              AND $d2 <= :d * :d
            ORDER BY $d2 ASC
            LIMIT :l;
SQL;

        $values = array(
            ":x" => $x,
            ":y" => $y,
            ":z" => $z,
            ":d" => $distance,
            ":l" => $limit,
        );

        $statement = $this->db->prepare($sql);
        $statement->execute($values);

        return array_map("self::transformPortData", $statement->fetchAll());
    }

    protected static function transformPortData($row)
    {
        $data = array(
            "name" => utf8_encode($row["name"]),
            "lat" => (float)$row["lat"],
            "lon" => (float)$row["lon"],
            "minHeight" => (float)$row["minHeight"],
            "maxHeight" => (float)$row["maxHeight"],
        );

        if (array_key_exists("d2", $row))
        {
            // Convert d2 (square of chord length) to great circle distance.
            // Just square rooting is probalby enough for any distances we _should_ be using
            // but arcsin stuff makes the reported distance accurate over any distance..
            //
            // See: https://en.wikipedia.org/wiki/Great-circle_distance#From_chord_length
            $dia = 2 * \msw\tools\lib\Maths::EARTH_RADIUS;
            $distance = $dia * asin(sqrt((float)$row["d2"]) / $dia);
            $data["distance"] = round(100 * $distance)/100;
            $data["distance_unit"] = 'km';
        }

        return $data;
    }
}
