<?php
include_once("/var/www/tide/vendor/autoload.php");

use msw\api\lib;

class PortsTest
    extends \PHPUnit\Framework\TestCase
{
    const CONN_STR = "sqlite:/var/www/tide/api/test/assets/ports.sqlite3";

    public function testFetchAll()
    {
        $p = new lib\Ports(self::CONN_STR);
        $data = $p->fetchAll();
        $this->assertEquals(405, count($data));
    }

    public function testFetchByName()
    {
        $p = new lib\Ports(self::CONN_STR);

        $data = $p->fetchByName("Lyme Regis");

        $expected = array(
            array(
                "name" => "Lyme Regis",
                "lat" => 50.7167,
                "lon" => -2.9333,
                "minHeight" => -1.09,
                "maxHeight" => 6.4,
            )
        );

        $this->assertEquals($expected, $data);
    }

    public function testFetchNearBantham()
    {
        $p = new lib\Ports(self::CONN_STR);

        $data = $p->fetchNear(50.272560, -3.893977, 100, 3);

        $expected = array(
            array(
                "name" => "Bigbury Bay",
                "lat" => 50.279,
                "lon" => -3.885,
                "minHeight" => -0.06,
                "maxHeight" => 6.33,
                "distance" => 0.96,
                "distance_unit" => "km"
            ),
            array(
                "name" => "Thurlestone",
                "lat" => 50.26,
                "lon" => -3.86,
                "minHeight" => -0.02,
                "maxHeight" => 6.35,
                "distance" => 2.79,
                "distance_unit" => "km"
            ),
            array(
                "name" => "River Yealm",
                "lat" => 50.3,
                "lon" => -4.0667,
                "minHeight" => -0.89,
                "maxHeight" => 7.5,
                "distance" => 12.66,
                "distance_unit" => "km"
            ),
        );

        $this->assertEquals($expected, $data);
    }

    public function testFetchNearPerran()
    {
        $p = new lib\Ports(self::CONN_STR);

        $data = $p->fetchNear(50.349154, -5.171445, 5, 1);

        $expected = array(
            array(
                "name" => "Perranporth",
                "lat" => 50.35,
                "lon" => -5.15,
                "minHeight" => -0.94,
                "maxHeight" => 8.67,
                "distance" => 1.53,
                "distance_unit" => "km"
            ),
        );

        $this->assertEquals($expected, $data);
    }
}
