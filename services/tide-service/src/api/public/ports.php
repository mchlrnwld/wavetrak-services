<?php
include_once("/var/www/tide/vendor/autoload.php");

header("Content-type: application/json");

$name = array_key_exists("port", $_GET) ? urldecode(stripslashes(iconv("utf8", "latin1", $_GET["port"]))) : null;
$lat = array_key_exists("lat", $_GET) ? (float)$_GET["lat"] : null;
$lon = array_key_exists("lon", $_GET) ? (float)$_GET["lon"] : null;
$distance = array_key_exists("distance", $_GET) ? (float)$_GET["distance"] : 100.0;
$limit = array_key_exists("limit", $_GET) ? (int)$_GET["limit"] :  10;

try
{
    $ports = new \msw\api\lib\Ports();

    if (isset($name))
    {
        $data = $ports->fetchByName($name);
    }
    elseif (isset($lat) && isset($lon))
    {
        $data = $ports->fetchNear($lat, $lon, $distance, $limit);
    }
    else
    {
        $data = $ports->fetchAll();
    }
}
catch (Exception $e)
{
    if (extension_loaded('newrelic'))
    {
        newrelic_notice_error($e);
    }
    header('HTTP/1.1 500 Server Error', true, 500);
    print(json_encode(array("error" => "server error")));
    exit();
}


header("Cache-Control: max-age=1800");
header("Content-type: application/json");
print json_encode($data);
