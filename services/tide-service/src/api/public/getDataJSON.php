<?php
require_once "../lib/Tide.class.php";
include_once("/var/www/tide/vendor/autoload.php");

/**
 * JSON high and low tide times endpoint
 *
 * Only with UTC timestamps and heights in meters.
 */

$name = array_key_exists("port", $_GET) ? urldecode(stripslashes(iconv("utf8", "latin1", $_GET["port"]))) : null;
$lat = array_key_exists("lat", $_GET) ? (float)$_GET["lat"] : null;
$lon = array_key_exists("lon", $_GET) ? (float)$_GET["lon"] : null;
$distance = array_key_exists("distance", $_GET) ? (float)$_GET["distance"] : 100.0;

// Start unix timestamp. defaults to now.
$start = intval($_GET["start"]);

// End unix timestamp. defaults to $start plus 24 hours.
$end = intval($_GET["end"]);

if ($start === 0)
{
    $start = time();
    // Dynamic start time, cache for half an hour
    header("Cache-Control: max-age=1800");
}
else
{
    // Can be cached for a looong time. (31536000s = 1 year.)
    header("Cache-Control: max-age=31536000");
}

if ($end === 0)
{
    $end = $start + 86400;
}

try
{
    $ports = new \msw\api\lib\Ports();

    if (isset($name))
    {
        $data = $ports->fetchByName($name);
    }
    elseif (isset($lat) && isset($lon))
    {
        $data = $ports->fetchNear($lat, $lon, $distance, 1);
    }
    else
    {
        header('HTTP/1.1 400 Bad Request', true, 400);
        print(json_encode(array("error" => "Either `port` or `lat` and `lon` required")));
        exit();
    }
}
catch (Exception $e)
{
    if (extension_loaded('newrelic'))
    {
        newrelic_notice_error($e);
    }
    header('HTTP/1.1 500 Server Error', true, 500);
    print(json_encode(array("error" => "server error")));
    exit();
}

if (empty($data))
{
    header('HTTP/1.1 404 Not Found', true, 404);
    print(json_encode(array("error" => "No port found with the given name or near the given lat/lon")));
    exit();
}

$port = $data[0];
$t = new Tide(iconv("utf-8", "latin1", $port["name"]));

$t->setTimes($start, $end);
$t->units = "m";

$tides = $t->getTides();

$levelsArr = array();

if (!empty($tides)) {
    // Rename and convert fields
    $tides = array_map(function ($data) {
        return array(
            "time" => +$data["time"],
            "shift" => +$data["height"],
            "state" => $data["text"]
        );
    }, $tides);

    // Increment the $end by an hour so that the levels are inclusive
    // of the $end value.
    $t->setTimes($start, $end + 3600);

    $levels = $t->getTideLevel(false);

    // Reshape and convert to numbers
    foreach ($levels as $timestamp => $data)
    {
        $levelsArr[] = array(
            "time" => +$timestamp,
            "shift" => +$data["height"]
        );
    }
}

header("Content-type: application/json");

print json_encode(array(
    "port" => $port,
    "tide" => $tides,
    "levels" => $levelsArr
));
