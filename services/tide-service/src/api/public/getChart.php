<?php
include_once("../../vendor/autoload.php");
require_once "../lib/Tide.class.php";
require_once "../lib/getCredentials.php";

$days= array_key_exists("days", $_GET) ? intval($_GET["days"]) : 0;
$port = urldecode(stripslashes(iconv('utf8', 'latin1',$_GET['port'])));
$timezone   = urldecode(stripslashes($_GET['timezone']));

date_default_timezone_set("UTC");
$utc = new DateTimeZone("UTC");

//$fileroot = $conf->get('IMAGE_DIR').'/tide/';
$bucket = $_ENV["TIDE_BUCKET"];

if (empty($timezone))
{
    throw new Exception('timezone required');
}

if (empty($port))
{
    throw new Exception('port required');
}

$tz = new DateTimeZone($timezone);

if(!array_key_exists("starttime", $_GET)) {
    $startTime = gmmktime(0,0,0);
} else {
    $startTime = intval($_GET['starttime']);
}

if($days == '' || $days == 0) {
    $days = 1;
}

$s3Client = Aws\S3\S3Client::factory(getCredentials($_ENV));

header("Content-type: text/plain");

for($step=0;$step<$days;$step++) {

    //starttime is GMT. xtide works in local time (hurrah!)
    $dt = new DateTime("@".($startTime + $step * 86400), $utc);
    $dt->setTimezone($tz);
    $mytime = $startTime + $dt->getOffset() + ($step * 86400) ;

    $ts = gmmktime(0,0,0, date('m',$mytime), date('d',$mytime), date('Y',$mytime));

    $t = new Tide($port, $ts);

    $t->dateFormat = ' ';
    $t->timeFormat = ' ';
    $t->units = 'm';
    $t->suppressMoon = "y";
    $t->suppressAstro = "n";
    $t->showTitle = "n";
    $t->showTimeText = "n";
    $t->showTimeAxis = "n";
    $t->suppressX = "y";
    $t->suppressMargin = "y";

    $tideParams = array('aspect'        => 3.95,
                        'height'        => 50,
                        'width'         => 390,
                        'nightColor'    => "898989",
                        'dayColor'      => "ECF0F1",
                        'twilightColor' => "C7C7C7",
                        'ebbColor'      => "197938",
                        'floodColor'    => "34AD5B"
                        );

    $hashString = $t->location.$timezone;

    foreach($tideParams as $param => $default) {
        $t->$param      = (array_key_exists($param, $_GET) ? $_GET[$param] : $default);
        $hashString .= ".".$t->$param;
    }

    $key = md5($hashString).'/'.$ts.'.png';

    $exists = $s3Client->doesObjectExist($bucket, $key);

    if(!$exists) {
        $data = array(
            "Bucket" => $bucket,
            "Key" => $key,
            "Body" => $t->getTideChart(),
            "ContentType" => "image/png",
        );

        try
        {
            $s3Client->putObject($data);
        }
        catch(Exception $e)
        {
            throw new Exception("Unable to write to $bucket: ".$e);
        }
    }

    print("$key\n");
}
print $t->location."\n";
print "-\n";
