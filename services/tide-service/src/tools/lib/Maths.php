<?php

namespace msw\tools\lib;

class Maths
{
    const EARTH_RADIUS = 6378.1; # In km

    /**
     * Converts a lat/lon to a coordinate (x, y, z) in 3d cartesian space
     * assuming the earth is a sphere of radius self::EARTH_RADIUS centered at (0, 0, 0).
     *
     * @param float $lat        Latitude in decimal degrees
     * @param float $lon        Longitude in decimal degrees
     * @return array            Array of length three of x, y, z, position
     */
    public static function makeXYZ($lat, $lon)
    {
        # convert to radians
        $lat *= pi() / 180.0;
        $lon *= pi() / 180.0;

        return array(
            self::EARTH_RADIUS * cos($lat)*cos($lon),
            self::EARTH_RADIUS * cos($lat)*sin($lon),
            self::EARTH_RADIUS * sin($lat)
        );
    }
}
