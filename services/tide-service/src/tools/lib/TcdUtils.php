<?php

namespace msw\tools\lib;

class TcdUtils
{
    /**
     * Parses a harmonics text file built by `restore_tide_db`, returning an array of
     * tide station information.
     *
     * @param string $filename  - The file to parse
     * @return array - Array of arrays, each containing keys "name", "lat", and "lon"
     */
    public static function parsetxt($filename)
    {
        $lines = file($filename);

        if ($lines === false)
        {
            throw new \Exception("Cannot open $filename");
        }
        $lineno = 0;

        // Walk past opening comments
        while (substr($lines[$lineno], 0, 1) === "#") $lineno++;

        // First non-comment line is the nuber of harmonic constituents
        $constituents = (int)$lines[$lineno];

        if (!array_key_exists($lineno, $lines))
        {
            throw new \Exception("File '$filename' is all comments and no data");
        }

        // Find start of tide port data
        $lineno = array_search("# Harmonic constants.\n", $lines);

        if ($lineno === false)
        {
            throw new \Exception("Start of harmonic constants section in file '$filename' not found");
        }

        $data = array();

        while (array_key_exists($lineno, $lines))
        {
            $lat = null;
            $lon = null;
            $unit = null;
            $name = null;

            // A bunch of "comments" proceed the harmoncs data. A sections called "HOT COMMENTS"
            // Actually contains some relevant meta data.
            while (array_key_exists($lineno, $lines) && substr($lines[$lineno], 0, 1) === "#") {
                if (preg_match("/# !longitude: (-?[0-9.]+)/", $lines[$lineno], $matches))
                {
                    $lon = (float)$matches[1];
                }
                if (preg_match("/# !latitude: (-?[0-9.]+)/", $lines[$lineno], $matches))
                {
                    $lat = (float)$matches[1];
                }
                if (preg_match("/# !units: (.+)/", $lines[$lineno], $matches))
                {
                    $units = trim($matches[1]);
                }

                $lineno++;
            }

            if (!array_key_exists($lineno, $lines))
            {
                throw new \Exception("Something is awry. Unexpected end of file in $filename");
            }

            // First line after the HOT COMMENTS is the port name
            $name = trim($lines[$lineno]); // (trim \n)

            if (empty($lat) || empty($lon) || empty($name))
            {
                throw new \Exception("Something is awry. Bad tide port data in $filename:$lineno. ($name, $lat, $lon)");
            }

            // There are tide stations configured for current (unit: knots) and other
            // params we don't care about. Only intersted in tide stations reporting
            // height, for which the unts are either feet of meters
            if ($units === "feet" || $units === "meters")
            {
                $data[] = array(
                    "name" => $name,
                    "lat" => $lat,
                    "lon" => $lon,
                );
            }

            // Skip over lines for timzone, mean low water, and the harmonica data
            $lineno += (3 + $constituents);
        }

        return $data;
    }

    /**
     * Parses a offset xml file built by `restore_tide_db`, returning an array of
     * tide station information.
     *
     * @param string $filename  - The file to parse
     * @param array $baseStationMap - Were only interesting in startions that are an offset
     *   of the ones here. Should be an array with stations names as the key.
     * @return array - Array of arrays, each containing keys "name", "lat", and "lon"
     */
    public static function parsexml($filename, $baseStationMap)
    {
        $data = array();

        // XML parsing is a bit wierd. Here we set up the parser object, with a callback that
        // will be called on each tag
        $xml_parser = xml_parser_create('ISO-8859-1');
        xml_set_element_handler($xml_parser, function ($parser, $name, $attributes) use (&$data, $baseStationMap) {
            if ($name !== "SUBORDINATESTATION") return;

            if (empty($attributes["NAME"]) || empty($attributes["LONGITUDE"]) || empty($attributes["LATITUDE"]))
            {
                throw new \Exception("Missing data for tide station '".$attributes["NAME"]."' in $filename");
            }

            // Only interested in subordinate stations that reference one of the good
            // base stations.
            if (array_key_exists($attributes["REFERENCE"], $baseStationMap))
            {
                $data[] = array(
                    "name" => preg_replace('/"/', "&quot;", $attributes["NAME"]),
                    "lat" => (float)$attributes["LATITUDE"],
                    "lon" => (float)$attributes["LONGITUDE"],
                );
            }
        }, false);

        $xml = file_get_contents($filename);

        if ($xml === false)
        {
            throw new \Exception("Cannot open $filename");
        }

        // `xml_parse` uses the parser object set up above, to parse the string `$xml`, so
        // after this call, if all was sucessful, the callbacl will have populated `$data`.
        $success = xml_parse($xml_parser, $xml, true);

        if ($success === 0)
        {
            throw new \Exception("Error parsing XML file $filename");
        }

        return $data;
    }
}
