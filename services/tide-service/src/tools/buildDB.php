<?php
include_once("/var/www/tide/vendor/autoload.php");
/**
 * This script parses data from files created by restore_tide_db and populated
 * a sqlite3 database. It exports a function to do so, but if executed directly
 * on the commant line expects the filename f\r the sqllite database as the first
 * commad like arg, and tide port data files (.txt and/or .xml) as susequent
 * arguments.
 */


/**
 * Get the mathematical minimum and maximum tide heights for the port, in meters
 *
 * @param String $port      The name of the port.
 * @return array            Array contianing min height and max height
 */
function getMinMaxTide($port)
{
    $min = null; $max = null;

    $stats = shell_exec("tide -l '".preg_replace("/'/", "'\''", $port)."' -u m -m s 2>/dev/null");

    if (empty($stats))
    {
        throw new Exception("Error getting long term min/max tide heights for port $port. `tide` returned no data.");
    }

    $lines = explode("\n", $stats);

    foreach ($lines as $line)
    {
        if (preg_match("/^Mathematical upper bound:[\s]+(-?[0-9\.]+) meters/", $line, $matches) === 1)
        {
            $max = (float)$matches[1];
        }
        else if (preg_match("/^Mathematical lower bound:[\s]+(-?[0-9\.]+) meters/", $line, $matches) === 1)
        {
            $min = (float)$matches[1];
        }
    }

    return array($min, $max);
}

/**
 * Builds a slite3 database file from an assortment of txt and xml files understood by
 * tcd-utils.
 *
 * @param String $dbfile    Name of file to create. Should not already exist
 * @param array $files      Array of file names.
 * @param Boolean $log      Print loging to stdout.
 */
function buildDB($dbfile, array $files, $log = false)
{
    $txtFiles = array_filter($files, function ($name) {
        return substr($name, -4) === ".txt";
    });
    $xmlFiles = array_filter($files, function ($name) {
        return substr($name, -4) === ".xml";
    });
    $badFiles = array_diff($files, $txtFiles, $xmlFiles);

    if (count($badFiles) > 0)
    {
        throw new Exception("Only txt and xml files are acceptable. E.g ".$badFiles[0]." is bad");
    }

    if(file_exists($dbfile))
    {
        throw new Exception("File $dbfile already exists");
    }

    $db = new PDO("sqlite:$dbfile");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    $db->exec(<<<SQL

    CREATE TABLE tideport (
        name TEXT NOT NULL,
        lat FLOAT NOT NULL,
        lon FLOAT NOT NULL,
        x FLOAT NOT NULL,
        y FLOAT NOT NULL,
        z FLOAT NOT NULL,
        minHeight FLOAT NOT NULL,
        maxHeight FLOAT NOT NULL
    );
    CREATE INDEX tideport_name ON tideport (name);
    CREATE INDEX tideport_lat ON tideport (lat);
    CREATE INDEX tideport_lon ON tideport (lon);
    CREATE INDEX tideport_x ON tideport (x);
    CREATE INDEX tideport_y ON tideport (y);
    CREATE INDEX tideport_z ON tideport (z);
    CREATE INDEX tideport_lat_lon ON tideport (lat, lon);

SQL
    );

    $insert = $db->prepare("INSERT INTO tideport (name, lat, lon, minHeight, maxHeight, x, y, z) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

    $count = 1;

    $baseStations = array();
    $subordinateStations = array();

    foreach ($txtFiles as $file)
    {
        if ($log) print("Parsing $file\n");
        $baseStations = array_merge($baseStations, \msw\tools\lib\TcdUtils::parsetxt($file));
    }
    $baseStationMap = array_fill_keys(array_map(function ($port) { return $port["name"]; }, $baseStations), 1);
    foreach ($xmlFiles as $file)
    {
        if ($log) print("Parsing $file\n");
        $subordinateStations = array_merge($subordinateStations, \msw\tools\lib\TcdUtils::parsexml($file, $baseStationMap));
    }

    if ($log) print("Calulating min/max tides and inserting to db\n");
    $ports = array_merge($baseStations, $subordinateStations);
    $total = count($ports);
    $start = time();

    foreach ($ports as $port)
    {
        list($min, $max) = getMinMaxTide($port["name"]);
        list($x, $y, $z) = \msw\tools\lib\Maths::makeXYZ($port["lat"], $port["lon"]);

        try
        {
            $insert->execute(array($port["name"], $port["lat"], $port["lon"], $min, $max, $x, $y, $z));
        }
        catch (Exception $e)
        {
            if ($log) print "Error inserting (".$port["name"].", ".$port["lat"].", ".$port["lon"].", $min, $max)\n";
            throw $e;
        }

        if ($log && $count % 100 === 0)
        {
            $time = time() - $start;
            print("Completed $count/$total ports in ${time}s\n");
        }
        $count++;
    }

    if ($log) print("Done! $count ports\n");
}


if (!empty($argv) && basename(__FILE__) === basename($argv[0]))
{
    if (count($argv) < 3)
    {
        print(<<<USAGE

Usage:
    php buildDB.php OUTPUT_FILE INPUT_FILE [INPUT_FILE]...

where:
    OUTPUT_FILE - the sqlite3 database to create
    INPUT_FILE  - one or more txt and/or xml files created by the tcd-utils program restore_tide_db

USAGE
        );

        exit(1);
    }
    buildDB($argv[1], array_slice($argv, 2), true);
}
