<?php

include_once "tools/buildDB.php";

class BuildDBTest
    extends \PHPUnit\Framework\TestCase
{
    const DB_FILE = "tools/test/assets/test.sqlite3";

    public function setUp()
    {
        if (file_exists(self::DB_FILE)) unlink(self::DB_FILE);
    }
    public static function tearDownAfterClass()
    {
        if (file_exists(self::DB_FILE)) unlink(self::DB_FILE);
    }

    public function testHappyCase()
    {
        $this->assertFalse(file_exists(self::DB_FILE));
        buildDB(self::DB_FILE, array("tools/test/assets/misc.txt", "tools/test/assets/misc.xml"));
        $this->assertTrue(file_exists(self::DB_FILE));

        $db = new PDO("sqlite:".self::DB_FILE);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        $data = $db->query("SELECT name, lat, lon FROM tideport ORDER BY NAME")->fetchAll();

        $expected = array(
            array( "name" => "Aden, Yemen", "lat" => "12.7833", "lon" => "44.9833"),
            array( "name" => "Air Musi, Sumatra, Indonesia", "lat" => "-2.2000", "lon" => "104.9500"),
            array("name" => "Al Mukha", "lat" => "13.3167", "lon" => "43.2333"),
            array("name" => "Mirbat", "lat" => "16.9833", "lon" => "54.7000"),
            array("name" => "Mukalla", "lat" => "14.5167", "lon" => "49.1333"),
            array("name" => "Perim", "lat" => "12.6500", "lon" => "43.4000"),
        );

        $this->assertEquals($expected, $data);
    }

    public function testAddsMixMaxHeights()
    {
        $this->assertFalse(file_exists(self::DB_FILE));
        buildDB(self::DB_FILE, array("tools/test/assets/misc.txt", "tools/test/assets/misc.xml"));
        $this->assertTrue(file_exists(self::DB_FILE));

        $db = new PDO("sqlite:".self::DB_FILE);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        $data = $db->query("SELECT name, minHeight, maxHeight FROM tideport")->fetchAll();

        foreach ($data as $port)
        {
            $this->assertTrue($port["minHeight"] < $port["maxHeight"], "Bad min/max heights for ".$port["name"]);
        }
    }
}
