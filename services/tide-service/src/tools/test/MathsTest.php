<?php

include_once("/var/www/tide/vendor/autoload.php");

use \msw\tools\lib;

class MathsTest
    extends \PHPUnit\Framework\TestCase
{
    public function testEquator()
    {
        $r = lib\Maths::EARTH_RADIUS;
        $this->assertEquals(array($r, 0, 0), lib\Maths::makeXYZ(0, 0));
        $this->assertEquals(array(0, $r, 0), lib\Maths::makeXYZ(0, 90));
        $this->assertEquals(array(-$r, 0, 0), lib\Maths::makeXYZ(0, 180));
        $this->assertEquals(array(0, -$r, 0), lib\Maths::makeXYZ(0, 270));

        $this->assertEquals(array($r / sqrt(2), $r / sqrt(2), 0), lib\Maths::makeXYZ(0, 45));
    }

    public function testSomewehereElse()
    {
        $r = lib\Maths::EARTH_RADIUS;
        $this->assertEquals(array($r / 2, $r / 2, $r / sqrt(2)), lib\Maths::makeXYZ(45, 45));
    }
}
