<?php

include_once("/var/www/tide/vendor/autoload.php");

class ParsexmlTest
    extends \PHPUnit\Framework\TestCase
{
    public function testHappyCase()
    {
        $data = \msw\tools\lib\TcdUtils::parsexml("tools/test/assets/misc.xml", array( "Aden, Yemen" => 1 ));

        $expected = array(
            array("name" => "Mirbat", "lat" => 16.9833, "lon" => 54.7000),
            array("name" => "Mukalla", "lat" => 14.5167, "lon" => 49.1333),
            array("name" => "Perim", "lat" => 12.6500, "lon" => 43.4000),
            array("name" => "Al Mukha", "lat" => 13.3167, "lon" => 43.2333),
        );

        $this->assertEquals($expected, $data);
    }

    public function testMissingLatLon()
    {
        try
        {
            $data = \msw\tools\lib\TcdUtils::parsexml("tools/test/assets/misc-missing-lat-lon.xml");
        }
        catch (Exception $e)
        {
            return;
        }

        // should not get here
        $this->assertTrue(false, "Missing latitude should thrown an error");
    }

    public function testBadXML()
    {
        try
        {
            $data = \msw\tools\lib\TcdUtils::parsexml("tools/test/assets/misc-bad.xml");
        }
        catch (Exception $e)
        {
            return;
        }

        // should not get here
        $this->assertTrue(false, "Truncated file should thrown an error");
    }
}
