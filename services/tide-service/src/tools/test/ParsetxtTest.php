<?php

include_once("/var/www/tide/vendor/autoload.php");

class ParsetxtTest
    extends \PHPUnit\Framework\TestCase
{
    public function testHappyCase()
    {
        $data = \msw\tools\lib\TcdUtils::parsetxt("tools/test/assets/misc.txt");
        $expected = array(
            array( "name" => "Aden, Yemen", "lat" => 12.7833, "lon" => 44.9833),
            array( "name" => "Air Musi, Sumatra, Indonesia", "lat" => -2.2000, "lon" => 104.9500),
        );
        $this->assertEquals($expected, $data);
    }

    public function testMissingLatitude()
    {
        try
        {
            $data = \msw\tools\lib\TcdUtils::parsetxt("tools/test/assets/misc-no-lat.txt");
        }
        catch (Exception $e)
        {
            return;
        }

        // should not get here
        $this->assertTrue(false, "Missing latitude should thrown an error");
    }

    public function testTruncatedFile()
    {
        try
        {
            $data = \msw\tools\lib\TcdUtils::parsetxt("tools/test/assets/misc-truncated.txt");
        }
        catch (Exception $e)
        {
            return;
        }

        // should not get here
        $this->assertTrue(false, "Truncated file should thrown an error");
    }
}
