#!/bin/bash
set -euxo pipefail

cd smoketests

echo 'Building smoketest container:'
docker build -t tide-service:smoketest .

echo 'Running smoketest container:'
docker run --env SERVICE_URL="${SERVICE_URL}" tide-service:smoketest npm run test
