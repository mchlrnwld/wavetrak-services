const fetch = require('node-fetch');

describe('/data', () => {
  const SERVICE_URL = process.env.SERVICE_URL;

  const portshape = {
      name: expect.any(String),
      lat: expect.any(Number),
      lon: expect.any(Number),
      minHeight: expect.any(Number),
      maxHeight: expect.any(Number),
  };

  const stateshape = {
    time: expect.any(Number),
    shift: expect.any(Number),
    state: expect.stringMatching(/High|Low/),
  };

  const levelshape = {
    time: expect.any(Number),
    shift: expect.any(Number),
  };

  test('Get data for a named location with a non-ascii name', async () => {
    const name = encodeURI('Île-Aux-Lievres, Québec');
    const response = await fetch(`${SERVICE_URL}/data?port=${name}`);
    expect(response.ok).toBe(true);
    const data = await response.json();

    expect(data.port).toEqual(portshape);

    expect(data.tide).toEqual(expect.any(Array));
    expect(data.tide.length).toBeGreaterThan(0);
    data.tide.forEach(tidestate => expect(tidestate).toEqual(stateshape));

    expect(data.levels).toEqual(expect.any(Array));
    expect(data.levels.length).toBeGreaterThan(22);
    data.levels.forEach(level => expect(level).toEqual(levelshape));
  });


  test('Get data for near Trestles', async () => {
    const response = await fetch(`${SERVICE_URL}/data?lat=33.38&lon=-117.6`);
    expect(response.ok).toBe(true);
    const data = await response.json();

    expect(data.port).toEqual({
      ...portshape,
      distance: expect.any(Number),
      distance_unit: 'km',
    });

    expect(data.tide).toEqual(expect.any(Array));
    expect(data.tide.length).toBeGreaterThan(0);
    data.tide.forEach(tidestate => expect(tidestate).toEqual(stateshape));

    expect(data.levels).toEqual(expect.any(Array));
    expect(data.levels.length).toBeGreaterThan(22);
    data.levels.forEach(level => expect(level).toEqual(levelshape));
  });
});
