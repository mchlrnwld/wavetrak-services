const fetch = require('node-fetch');

describe('/ports', () => {
  const SERVICE_URL = process.env.SERVICE_URL;

  const portshape = {
      name: expect.any(String),
      lat: expect.any(Number),
      lon: expect.any(Number),
      minHeight: expect.any(Number),
      maxHeight: expect.any(Number),
  };

  test('root returns data', async () => {
    const response = await fetch(`${SERVICE_URL}/ports`);
    expect(response.ok).toBe(true);
    const data = await response.json();
    expect(data).toEqual(expect.any(Array));

    data.forEach(row => expect(row).toMatchObject(portshape));
  });

  test('lat/lon serach near Bantham returns data', async () => {
    const response = await fetch(`${SERVICE_URL}/ports?lat=50.25&lon=-3.95&distance=60&limit=5`);
    expect(response.ok).toBe(true);
    const data = await response.json();
    expect(data).toEqual(expect.any(Array));
    expect(data).toHaveLength(5);

    data.forEach(row => expect(row).toMatchObject({
      ...portshape,
      distance: expect.any(Number),
      distance_unit: 'km',
    }));
  });

  test('get port with non-ascii name', async () => {
    const name = encodeURI('Île-Aux-Lievres, Québec');
    const response = await fetch(`${SERVICE_URL}/ports?port=${name}`);
    expect(response.ok).toBe(true);
    const data = await response.json();
    expect(data).toEqual(expect.any(Array));
    expect(data).toHaveLength(1);
    expect(data[0]).toMatchObject(portshape);
  });
});
