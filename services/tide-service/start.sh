if [ $NEW_RELIC_ENABLED ]
  then
    sed -i \
      -e "s/REPLACE_WITH_REAL_KEY/$NEW_RELIC_LICENSE_KEY/" \
      -e 's/newrelic.appname = "PHP Application"/newrelic.appname = "Tide Service"/' \
      -e 's/;newrelic.daemon.app_connect_timeout =.*/newrelic.daemon.app_connect_timeout=15s/' \
      -e 's/;newrelic.daemon.start_timeout =.*/newrelic.daemon.start_timeout=5s/' \
      /etc/php.d/newrelic.ini
fi

exec httpd -DFOREGROUND
