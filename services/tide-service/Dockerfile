FROM centos:6.10 as base
# Fix repos since 6.10 is deprecated (need to use vault) and update and clean.
RUN sed -i -e 's!mirrorlist!#mirrorlist!g' /etc/yum.repos.d/CentOS-Base.repo && \
    sed -i -e 's!#baseurl=http://mirror.centos.org/centos/\$releasever!baseurl=https://vault.centos.org/6.10/!g' /etc/yum.repos.d/CentOS-Base.repo && \
    yum -y update && \
    yum clean all


# Build `tide`, tcd-utils
FROM base as build-tooling
COPY ./xtide /root/xtide/
WORKDIR /root/xtide
RUN yum -y install gcc gcc-c++ libpng-devel && \
    ./configure && \
    make tide && \
    mv tide /usr/local/bin/ && \
    cd /root/xtide/tcd-utils && ./configure CPPFLAGS="-I/root/xtide/libtcd" LDFLAGS="-L/root/xtide/libtcd" && make && \
    cp build_tide_db restore_tide_db /usr/local/bin/


# Install php and all application dependenceis on top of `base`
# Grab `tide` binary from build-tooling stage
FROM base as base-php
RUN yum -y install httpd php php-pdo php-xml sqlite sqlite-devel libpng
RUN sed -i -e 's|^variables_order.*|variables_order = "EGPCS"|' /etc/php.ini && \
    sed -i -e 's|^disable_functions.*|disable_functions = phpinfo|' /etc/php.ini
RUN curl -L https://github.com/composer/composer/releases/download/1.8.4/composer.phar > /usr/bin/composer && \
    chmod a+x /usr/bin/composer
COPY ./composer.* /var/www/tide/
WORKDIR /var/www/tide
RUN composer install
COPY --from=build-tooling /usr/local/bin/tide /usr/local/bin/tide


# Side branch of the docker build used for tpxo8 tooling
FROM base-php as tpxo8
RUN yum install -y libgfortran


# Build the tcd files
FROM base-php as base-php-tcd
COPY --from=build-tooling /usr/local/bin/build_tide_db /usr/local/bin/build_tide_db
COPY ./harmonics /usr/share/xtide
COPY ./harmonics/xtide.conf /etc/
RUN cd /usr/share/xtide && ./build-tcd.sh


# Run all tests in a throw away stage
FROM base-php as test
COPY --from=build-tooling /usr/local/bin/build_tide_db /usr/local/bin/build_tide_db
COPY ./src/tools/test/assets/misc.* /usr/share/xtide/
COPY ./harmonics/build-tcd.sh /usr/share/xtide/
RUN echo "/usr/share/xtide/misc.tcd" > /etc/xtide.conf && \
    cd /usr/share/xtide && ./build-tcd.sh
WORKDIR /var/www/tide
COPY ./Makefile /var/www/tide/
COPY ./src/tools /var/www/tide/tools
RUN vendor/bin/phpunit tools/test
COPY ./src/api /var/www/tide/api
RUN vendor/bin/phpunit api/test


# Build the ports database
FROM base-php-tcd as build-tide-db
COPY ./src/tools /var/www/tide/tools
RUN cd /var/www/tide/tools && \
    echo "Building tide port database..." && \
    php buildDB.php /usr/share/xtide/ports.sqlite3 /usr/share/xtide/*.xml /usr/share/xtide/*.txt


# Final FROM, define tha actual run-time environment.
FROM base-php-tcd
WORKDIR /var/www/tide
RUN \
  curl -L "https://download.newrelic.com/php_agent/archive/8.2.0.221/newrelic-php5-8.2.0.221-linux.tar.gz" | tar -C /tmp -zx && \
  export NR_INSTALL_USE_CP_NOT_LN=1 && \
  export NR_INSTALL_SILENT=1 && \
  /tmp/newrelic-php5-*/newrelic-install install && \
  rm -rf /tmp/newrelic-php5-* /tmp/nrinstall*
COPY ./httpd.conf /etc/httpd/conf/httpd.conf
COPY --from=build-tide-db /usr/share/xtide/ports.sqlite3 /usr/share/xtide/ports.sqlite3
COPY start.sh /root/
COPY ./tides.conf /etc/httpd/conf.d/
COPY ./src/tools /var/www/tide/tools
COPY ./src/api /var/www/tide/api


EXPOSE 8080
CMD ["sh", "/root/start.sh"]
