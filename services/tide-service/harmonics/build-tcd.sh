#!/bin/env sh

find . -name '*.txt' | sed 's/.txt//' | while read -r file_prefix; do
  if [ -f "${file_prefix}.xml" ]; then
    build_tide_db "${file_prefix}.tcd" "${file_prefix}.txt" "${file_prefix}.xml"
  else
    build_tide_db "${file_prefix}.tcd" "${file_prefix}.txt"
  fi;
done;
