variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "service_td_count" {
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "alb_listener_arn" {
}

variable "default_vpc" {
}

variable "ecs_cluster" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_request_count"
}

variable "auto_scaling_min_size" {
  default = 2
}

variable "auto_scaling_max_size" {
  default = 20
}

variable "auto_scaling_up_count" {
  default = 1
}

variable "auto_scaling_down_count" {
  default = -1
}

variable "auto_scaling_target_value" {
  default = 600
}

variable "auto_scaling_evaluation_period" {
  default = 60
}

variable "tide_cdn_fqdn" {
  description = "List of domains associated with ACM certificate on the Cloudfront distribution"
  type        = map(list(string))
}

variable "tide_cdn_acm_domains" {
  description = "List of domains associated with Cloudfront distribution"
  type        = list(string)
}

# New Relic apdex alert variables

variable "newrelic_apdex_create_alert" {
  description = "Create New Relic apdex alert"
}

variable "newrelic_apdex_runbook_url" {
  description = "URL of runbook that explains how to resolve the alert"
}

variable "newrelic_apdex_alert_threshold" {
  description = "Condition violation threshold for the alert"
}

variable "newrelic_apdex_warning_threshold" {
  description = "Alert condition violation threshold for the warning"
}

# New Relic error rate alert variables

variable "newrelic_error_rate_create_alert" {
  description = "Create New Relic error rate alert"
}

variable "newrelic_error_rate_runbook_url" {
  description = "URL of runbook that explains how to resolve the alert"
}

variable "newrelic_error_rate_alert_threshold" {
  description = "Condition violation threshold for the alert (as a percentage)"
}

variable "newrelic_error_rate_warning_threshold" {
  description = "Condition violation threshold for the warning (as a percentage)"
}

variable "newrelic_apdex_violation_close_timer" {
  description = "Number of hours before violation is automatically closed"
}

variable "newrelic_error_rate_violation_close_timer" {
  description = "Number of hours before violation is automatically closed"
}
