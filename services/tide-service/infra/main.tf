locals {
  newrelic_app_name    = "Tide Service"
  newrelic_policy_name = "tide-service-${var.environment}"
}

module "tide_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "wt-core-svc-${var.environment}-tide-service"
  service_td_count            = var.service_td_count
  service_td_container_name   = var.application
  service_port                = 8080
  service_alb_priority        = 540

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id
  ecs_cluster = var.ecs_cluster

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled           = var.auto_scaling_enabled
  auto_scaling_target_value      = var.auto_scaling_target_value
  auto_scaling_scale_by          = var.auto_scaling_scale_by
  auto_scaling_min_size          = var.auto_scaling_min_size
  auto_scaling_max_size          = var.auto_scaling_max_size
  auto_scaling_alb_arn_suffix    = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
  auto_scaling_up_count          = var.auto_scaling_up_count
  auto_scaling_down_count        = var.auto_scaling_down_count
  auto_scaling_evaluation_period = var.auto_scaling_evaluation_period

  # New Relic shared alert variables
  newrelic_app_name    = local.newrelic_app_name
  newrelic_policy_name = local.newrelic_policy_name

  # New Relic apdex alert variable
  newrelic_apdex_create_alert          = var.newrelic_apdex_create_alert
  newrelic_apdex_runbook_url           = var.newrelic_apdex_runbook_url
  newrelic_apdex_alert_threshold       = var.newrelic_apdex_alert_threshold
  newrelic_apdex_warning_threshold     = var.newrelic_apdex_warning_threshold
  newrelic_apdex_violation_close_timer = var.newrelic_apdex_violation_close_timer

  # New Relic error rate alert variable
  newrelic_error_rate_create_alert          = var.newrelic_error_rate_create_alert
  newrelic_error_rate_runbook_url           = var.newrelic_error_rate_runbook_url
  newrelic_error_rate_alert_threshold       = var.newrelic_error_rate_alert_threshold
  newrelic_error_rate_warning_threshold     = var.newrelic_error_rate_warning_threshold
  newrelic_error_rate_violation_close_timer = var.newrelic_error_rate_violation_close_timer
}

# Add tide image S3 bucket and cloudfront distribution
module "tide_cdn" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/cloudfront-with-s3"

  company     = var.company
  environment = var.environment
  application = var.application
  service     = "tide-image-cdn"

  cdn_acm_domains    = var.tide_cdn_acm_domains
  cdn_acm_count      = length(var.tide_cdn_acm_domains)
  cdn_fqdn           = var.tide_cdn_fqdn
  versioning_enabled = false
  comment            = "${var.environment} Tide Image CDN wth S3"
  allowed_origins    = "*"
}

# Permit ECS to write to tide S3 bucket
resource "aws_iam_policy" "tide_cdn_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-tide-cdn-bucket-mgmt-policy-${var.environment}"
  description = "Policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resource/cdn-bucket-mgmt-policy.json", {
    s3_bucket = module.tide_cdn.bucket_arn
  })
}

resource "aws_iam_role_policy_attachment" "tide_cdn_bucket_mgmt_policy_attachement" {
  role       = module.tide_service.task_role_name
  policy_arn = aws_iam_policy.tide_cdn_bucket_mgmt_policy.arn
}

resource "aws_iam_policy" "s3_read_policy" {
  name        = "${var.company}-${var.application}-s3-read-policy-${var.environment}"
  description = "Policy to read objects in an s3 bucket"
  policy = templatefile("${path.module}/resource/s3-read-policy.json", {
    s3_bucket = module.tide_cdn.bucket_arn
  })
}

resource "aws_iam_role_policy_attachment" "tide_cdn_bucket_s3_read_policy_attachement" {
  role       = module.tide_service.task_role_name
  policy_arn = aws_iam_policy.s3_read_policy.arn
}

resource "newrelic_alert_policy" "newrelic_tide_service_policy" {
  name = local.newrelic_policy_name
}

data "aws_ssm_parameter" "newrelic_integration_key" {
  name = "/${var.environment}/pagerduty/msw-web/newrelic-integration-key"
}

resource "newrelic_alert_channel" "pagerduty_channel" {
  name = "pagerduty-tide-service"
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.newrelic_integration_key.value
  }
}

resource "newrelic_alert_policy_channel" "newrelic_tide_service_channel" {
  policy_id = newrelic_alert_policy.newrelic_tide_service_policy.id
  channel_ids = [
    newrelic_alert_channel.pagerduty_channel.id
  ]
}
