provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "tide-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company        = "wt"
  application    = "tide-service"
  environment    = "staging"
  aws_account_id = "665294954271"
  dns_zone_id    = "Z3JHKQ8ELQG5UE"

  iam_role_arn = "arn:aws:iam::${local.aws_account_id}:role/sl-ecs-service-core-svc-staging"

  default_vpc = "vpc-981887fd"
  dns_name    = "${local.application}.${local.environment}.surfline.com"

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:${local.aws_account_id}:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:${local.aws_account_id}:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"

  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  tide_cdn_acm_domains = ["staging.surfline.com"]
  tide_cdn_fqdn = {
    "staging.surfline.com" = ["tide-image-cdn.staging.surfline.com"]
  }
}

module "tide-service" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  tide_cdn_fqdn = local.tide_cdn_fqdn

  default_vpc = local.default_vpc
  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id

  service_td_count = 1
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = local.alb_listener_arn
  load_balancer_arn = local.load_balancer_arn

  ecs_cluster = "sl-core-svc-${local.environment}"

  iam_role_arn = local.iam_role_arn

  tide_cdn_acm_domains = local.tide_cdn_acm_domains

  auto_scaling_enabled  = true
  auto_scaling_min_size = 1
}
