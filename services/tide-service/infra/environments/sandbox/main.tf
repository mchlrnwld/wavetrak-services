provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "tide-service/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company        = "wt"
  application    = "tide-service"
  environment    = "sandbox"
  aws_account_id = "665294954271"
  dns_zone_id    = "Z3DM6R3JR1RYXV"
  iam_role_arn   = "arn:aws:iam::${local.aws_account_id}:role/sl-ecs-service-core-svc-sandbox"
  default_vpc    = "vpc-981887fd"
  dns_name       = "${local.application}.${local.environment}.surfline.com"

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:${local.aws_account_id}:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:${local.aws_account_id}:loadbalancer/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"

  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  tide_cdn_acm_domains = ["sandbox.surfline.com"]
  tide_cdn_fqdn = {
    "sandbox.surfline.com" = ["tide-image-cdn.sandbox.surfline.com"]
  }
}

module "tide-service" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  tide_cdn_fqdn = local.tide_cdn_fqdn

  default_vpc = local.default_vpc
  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id

  service_td_count = 1
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = local.alb_listener_arn
  load_balancer_arn = local.load_balancer_arn

  ecs_cluster = "sl-core-svc-${local.environment}"

  iam_role_arn = local.iam_role_arn

  tide_cdn_acm_domains = local.tide_cdn_acm_domains

  auto_scaling_enabled  = true
  auto_scaling_min_size = 1

  # New Relic apdex alert configuration
  newrelic_apdex_create_alert          = true
  newrelic_apdex_alert_threshold       = ".70"
  newrelic_apdex_warning_threshold     = ".85"
  newrelic_apdex_runbook_url           = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
  newrelic_apdex_violation_close_timer = null

  # New Relic error rate alert configuration
  newrelic_error_rate_create_alert          = true
  newrelic_error_rate_alert_threshold       = ".05"
  newrelic_error_rate_warning_threshold     = ".03"
  newrelic_error_rate_runbook_url           = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
  newrelic_error_rate_violation_close_timer = null
}
