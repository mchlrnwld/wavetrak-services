provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "tide-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company        = "wt"
  application    = "tide-service"
  environment    = "prod"
  aws_account_id = "833713747344"
  dns_zone_id    = "Z3LLOZIY0ZZQDE"
  iam_role_arn   = "arn:aws:iam::${local.aws_account_id}:role/sl-ecs-service-core-svc-prod"
  default_vpc    = "vpc-116fdb74"
  dns_name       = "${local.application}.${local.environment}.surfline.com"

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:${local.aws_account_id}:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:${local.aws_account_id}:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"

  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  tide_cdn_acm_domains = ["cdn-surfline.com"]
  tide_cdn_fqdn = {
    "cdn-surfline.com" = ["tide-image-cdn.cdn-surfline.com"]
  }
}

module "tide-service" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  tide_cdn_fqdn = local.tide_cdn_fqdn

  default_vpc = local.default_vpc
  dns_name    = local.dns_name
  dns_zone_id = local.dns_zone_id

  service_td_count = 1
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = local.alb_listener_arn
  load_balancer_arn = local.load_balancer_arn

  ecs_cluster = "sl-core-svc-${local.environment}"

  iam_role_arn = local.iam_role_arn

  tide_cdn_acm_domains = local.tide_cdn_acm_domains

  auto_scaling_enabled = true

  # New Relic apdex alert configuration
  newrelic_apdex_create_alert          = true
  newrelic_apdex_alert_threshold       = ".70"
  newrelic_apdex_warning_threshold     = ".85"
  newrelic_apdex_runbook_url           = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
  newrelic_apdex_violation_close_timer = null

  # New Relic error rate alert configuration
  newrelic_error_rate_create_alert          = true
  newrelic_error_rate_alert_threshold       = ".05"
  newrelic_error_rate_warning_threshold     = ".03"
  newrelic_error_rate_runbook_url           = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
  newrelic_error_rate_violation_close_timer = null
}
