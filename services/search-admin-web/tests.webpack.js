import chai from 'chai';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';

chai.use(dirtyChai);
chai.use(sinonChai);

const testsContext = require.context('./src', true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);
