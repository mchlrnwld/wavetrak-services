/* eslint no-console: 0 */
const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const cookieParser = require('cookie-parser');
const config = require('./webpack.config');

const DEVELOPING = process.env.NODE_ENV === 'development' || !process.env.NODE_ENV;
const isDeveloping = DEVELOPING;
const port = process.env.PORT || 3000;
const app = express();
app.use(cookieParser()); // / used for securityHandler()

app.get('/search/health', (req, res) =>
  res.send({
    status: 200,
    message: 'OK',
    version: process.env.APP_VERSION || 'unknown',
  }),
);

if (isDeveloping) {
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false,
    },
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('/search/*', (req, res) => {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist', 'index.html')));
    res.end();
  });
} else {
  app.use('/search/public/', express.static(path.join(__dirname, '/dist')));
  app.get('/search/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

app.listen(port, '0.0.0.0', (err) => {
  if (err) {
    console.log(err);
  }
  console.info(
    `==> (developing ${isDeveloping}) Listening on port ${port}. ` +
      `http://0.0.0.0:${port}/search/ in your browser.`,
  );
});
