docker build -t admin-tools/search .
docker tag admin-tools/search:latest \
  665294954271.dkr.ecr.us-west-1.amazonaws.com/admin-tools/search:latest

$(aws ecr get-login --no-include-email --region us-west-1)

docker push \
  665294954271.dkr.ecr.us-west-1.amazonaws.com/admin-tools/search:latest

cd ../../

ecs-cli compose --file docker/modules.yml --project-name surfline-admin create

python tools/cluster_manager.py
