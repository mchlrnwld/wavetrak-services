FROM node:12 as base
WORKDIR /opt/app
COPY .npmrc package.json package-lock.json ./
RUN npm ci --prefer-offline


FROM monsonjeremy/node-chrome:12 as test
WORKDIR /opt/app
COPY --from=base /opt/app/node_modules /opt/app/node_modules
COPY . .


FROM test AS build
WORKDIR /opt/app
ARG APP_ENV=sandbox
ARG APP_VERSION=master
ENV NODE_ENV=$APP_ENV
ENV APP_VERSION=$APP_VERSION
RUN npm run build && \
    npm install --production --ignore-scripts --prefer-offline


FROM node:12-alpine
WORKDIR /opt/app
ARG APP_ENV=sandbox
ARG APP_VERSION=master
ARG NEW_RELIC_APP_NAME='Surfline Admin Tools (Sandbox)'
ENV APP_VERSION=$APP_VERSION
ENV NEW_RELIC_APP_NAME=$NEW_RELIC_APP_NAME
ENV NODE_ENV=$APP_ENV
COPY --from=build /opt/app/package.json package.json
COPY --from=build /opt/app/node_modules node_modules
COPY --from=build /opt/app/dist dist
COPY --from=build /opt/app/server.js server.js
COPY --from=build /opt/app/webpack.config.js webpack.config.js
COPY --from=build /opt/app/newrelic.js newrelic.js
COPY --from=build /opt/app/test.sh test.sh
