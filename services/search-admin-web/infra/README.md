# Surfline Admin Search

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

  - [Development](#development)
  - [Supported routes](#supported-routes)
- [Admin Search Service](#admin-search-service)
  - [Naming Conventions](#naming-conventions)
  - [Terraform](#terraform)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Development

Install correct node version:

```
nvm install 10.20.1
nvm use 10.20.1
```

Setup `.npmrc` file for Surlfine artifactory: https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/763002972/Artifactory+Development+Workflow

To run the application locally:

```
npm install
npm start

# Custom port
PORT=3001 npm start
```

## Supported routes

Routes are configured [here](https://github.com/Surfline/surfline-admin/blob/master/modules/search/src/client.js#L28).

Running locally, the following routes should be accessible:

- [/search/spots/](http://localhost:3000/search/spots/)
- [/search/spotspoi/](http://localhost:3000/search/spotspoi/)
- [/search/mswspots/](http://localhost:3000/search/mswspots/)
- [/search/subregions/](http://localhost:3000/search/subregions/)
- [/search/regions/](http://localhost:3000/search/regions/)
- [/search/users/](http://localhost:3000/search/users/)
- [/search/reports/](http://localhost:3000/search/reports/)
- [/search/forecasts/](http://localhost:3000/search/forecasts/)

# Admin Search Service

This module contains the **infrastructure code** for the `admin-search-service` formerly configured as part of the `surfline-admin-tools`.

The application code can be found at [surfline-admin/modules/search].

## Naming Conventions

See [naming-conventions] for AWS resources.

## Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV={env} make plan
ENV={env} make apply
```

Available environments are:

- sandbox
- staging
- prod

[surfline-admin/modules/search]: https://github.com/Surfline/surfline-admin/tree/master/modules/search
[naming-conventions]: https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions
