import createReducerFromHandlers from './createReducerFromHandlers';

import { SEARCH, SEARCH_SUCCESS, SEARCH_FAILURE } from '../actions/search';

const initialState = {
  loading: false,
  error: null,
  searched: false,
  results: [],
};

const handlers = {};

handlers[SEARCH] = (state) => ({
  ...state,
  loading: true,
  searched: false,
  error: null,
});

handlers[SEARCH_SUCCESS] = (state, { results }) => ({
  ...state,
  loading: false,
  error: null,
  searched: true,
  results,
});

handlers[SEARCH_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducerFromHandlers(handlers, initialState);
