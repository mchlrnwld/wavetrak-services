import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from './search';
import { SEARCH, SEARCH_SUCCESS, SEARCH_FAILURE } from '../actions/search';

describe('Reducers / Edit', () => {
  const initialState = deepFreeze({
    loading: false,
    error: null,
    results: [],
    searched: false,
  });

  const results = deepFreeze([
    { primaryVal: 'Larry Fine', secondaryVal: 'larry@onionoil.com' },
    { primaryVal: 'Moe Howard', secondaryVal: 'moe@cafecasbahbah.com' },
    { primaryVal: 'Curly Howard', secondaryVal: 'curly@costaplentehotel.com' },
  ]);

  it('defaults to initial state', () => {
    expect(reducer()).to.deep.equal(initialState);
  });

  it('handles search', () => {
    const action = deepFreeze({ type: SEARCH });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: true,
      error: null,
      results: [],
      searched: false,
    });
  });

  it('handles search success', () => {
    const action = deepFreeze({ type: SEARCH_SUCCESS, results });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: false,
      error: null,
      results,
      searched: true,
    });
  });

  it('handles search failure', () => {
    const action = deepFreeze({ type: SEARCH_FAILURE, error: 'Something blew up' });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: false,
      error: 'Something blew up',
      results: [],
      searched: false,
    });
  });
});
