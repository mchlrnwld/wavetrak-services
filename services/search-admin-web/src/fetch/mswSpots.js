import config from '../config';

const queryParams = (params) =>
  `?${Object.keys(params)
    .map((key) => `${key}=${params[key]}`)
    .join('&')}`;

/**
 * Given a string the msw api will return a list of spots
 * where the name of the spot fuzzy matches the string
 *
 * @param spotName the name (or partial name) of the spot to search for
 */
export default async (spotName) => {
  const credentialsOptions = config.mswCredentials
    ? {
        credentials: 'include',
        headers: new Headers({
          Authorization: `Basic ${btoa(config.mswCredentials)}`,
        }),
      }
    : null;

  const spotFields = ['*', 'pointOfInterestId', 'hidden', 'breadcrumb'];

  const options = {
    limit: 12,
    match: 'CONTAINS',
    fields: spotFields.join(','),
    type: 'SPOT',
    query: spotName,
    showHidden: true,
  };

  const response = await fetch(
    `${config.mswEndpoint}search${queryParams(options)}`,
    credentialsOptions,
  );

  const result = await response.json();

  return result[0].results;
};
