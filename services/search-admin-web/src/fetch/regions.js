import { fetchWithAuth } from '@surfline/admin-common';

export const findRegions = query => fetchWithAuth(`regions/?search=${query}`); // eslint-disable-line
