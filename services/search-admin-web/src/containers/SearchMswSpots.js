import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import queryString from 'query-string';
import Search from '../components/Search/Search';
import Badge from '../components/SearchSpots/Badge';
import { doSearchCreator } from '../actions/search';
import findMswSpots from '../fetch/mswSpots';

class SearchMswSpots extends Component {
  constructor(props) {
    super(props);
    this.state = { spots: [] };
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    const { doSearch, location } = this.props;
    const defaultValue = queryString.parse(location.search)?.q;

    if (defaultValue) {
      doSearch(defaultValue);
    }
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({ spots: props.results || [] });
  }

  editPoi = ({ pointOfInterestId }) => `/poi/edit/${pointOfInterestId}`;

  transform = (results) =>
    results &&
    results.map((result) => {
      const { id, name, hidden, breadcrumb } = result;

      return {
        primaryVal: `${name}${hidden ? ' (DRAFT)' : ''}`,
        secondaryVal: breadcrumb ? `${breadcrumb.country.name} / ${breadcrumb.surfArea.name}` : '',
        key: id,
        badge: <Badge result={result} edit={this.editPoi} />,
      };
    });

  styles = {
    search: {
      SearchResults: {
        description: {
          padding: '20px 32px',
        },
        primary: {
          marginTop: 6,
        },
        li: {
          padding: 0,
        },
      },
      SearchInput: {
        Button: {
          width: 154,
        },
      },
    },
  };

  render() {
    const { doSearch, loading, location, searched } = this.props;
    const { error, spots } = this.state;
    return (
      <div>
        {error ? <Alert type="error">{error}</Alert> : null}
        <h4>MSW Spots</h4>
        <Search
          doSearch={doSearch}
          results={this.transform(spots)}
          defaultValue={queryString.parse(location.search)?.q}
          style={this.styles.search}
          loading={loading}
        />
        {searched && !spots.length ? <div>No Results Found</div> : null}
      </div>
    );
  }
}

SearchMswSpots.propTypes = {
  doSearch: PropTypes.func,
  results: PropTypes.array,
  searched: PropTypes.bool,
  location: PropTypes.object,
  loading: PropTypes.bool,
};

export default connect(
  (state) => ({ ...state.search }),
  (dispatch) => ({
    doSearch: (query) => dispatch(doSearchCreator(findMswSpots)(query)),
  }),
)(SearchMswSpots);
