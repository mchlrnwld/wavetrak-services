import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import { spots } from '@surfline/admin-common';
import queryString from 'query-string';
import Search from '../components/Search/Search';
import Badge from '../components/SearchSpots/Badge';
import { doSearchCreator } from '../actions/search';

export class SearchSpots extends Component {
  constructor(props) {
    super(props);

    this.state = { spots: [] };
    this.poi = false;
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    const { doSearch, location } = this.props;
    const defaultValue = queryString.parse(location.search)?.q;
    const version = queryString.parse(location.search)?.version ?? 1;

    if (defaultValue) {
      doSearch(defaultValue, { version });
    }
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({ spots: props.results || [] });
  }

  edit = ({ _id, pointOfInterestId }) => {
    if (this.poi) {
      return `/poi/edit/${pointOfInterestId}`;
    }
    return `/spots/edit/${_id}`;
  };

  remove = async (spotId) => {
    this.setState((prevState) => ({
      spots: prevState.spots.map((spot) => ({
        ...spot,
        removing: spot._id === spotId ? true : spot.removing,
      })),
    }));
    try {
      await spots.deleteSpot(spotId);
      this.setState((prevState) => ({
        spots: prevState.spots.filter((spot) => spot._id !== spotId),
        error: null,
      }));
    } catch (err) {
      this.setState((prevState) => ({
        error: 'Error: Unable to delete spot',
        spots: prevState.spots.map((spot) => ({
          ...spot,
          removing: spot._id === spotId ? false : spot.removing,
        })),
      }));
    }
  };

  transformTaxonomy = ({ in: breadcrumbs }) =>
    breadcrumbs
      .filter(({ depth }) => depth <= 2)
      .map(({ name }) => name)
      .filter((item, index, arr) => arr.indexOf(item) === index)
      .reverse()
      .slice(0, 3)
      .join(' / ');

  transform = (results) =>
    results &&
    results.map((result) => {
      const { _id, name, status, taxonomy } = result;

      return {
        primaryVal: `${name}${status === 'DRAFT' ? ' (DRAFT)' : ''}`,
        secondaryVal: taxonomy ? this.transformTaxonomy(taxonomy) : '',
        key: _id,
        badge: <Badge result={result} edit={this.edit} remove={this.remove} />,
      };
    });

  styles = {
    search: {
      SearchResults: {
        description: {
          padding: '20px 32px',
        },
        primary: {
          marginTop: 6,
        },
        li: {
          padding: 0,
        },
      },
      SearchInput: {
        Button: {
          width: 154,
        },
      },
    },
  };

  render() {
    const { doSearch, location, loading, searched } = this.props;
    const { error, spots: spotsState } = this.state;
    return (
      <div>
        {error ? <Alert type="error">{error}</Alert> : null}
        <h4>Surfline Spots</h4>
        <Search
          doSearch={doSearch}
          results={this.transform(spotsState)}
          defaultValue={queryString.parse(location.search)?.q}
          style={this.styles.search}
          loading={loading}
        />
        {searched && !spotsState.length ? <div>No Results Found</div> : null}
      </div>
    );
  }
}

SearchSpots.propTypes = {
  doSearch: PropTypes.func,
  loading: PropTypes.bool,
  location: PropTypes.object,
  results: PropTypes.array,
  searched: PropTypes.bool,
};

export const connector = (className) =>
  connect(
    (state) => ({ ...state.search }),
    (dispatch) => ({
      doSearch: (query) =>
        dispatch(doSearchCreator(spots.findSpots)(`${query}&includeTaxonomy=true`)),
    }),
  )(className);

export default connector(SearchSpots);
