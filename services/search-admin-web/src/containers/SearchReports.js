import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { spots, TextButton } from '@surfline/admin-common';
import queryString from 'query-string';
import Search from '../components/Search/Search';
import { doSearchCreator } from '../actions/search';

class SearchReports extends Component {
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    const { doSearch, location } = this.props;
    const defaultValue = queryString.parse(location.search)?.q;
    if (defaultValue) {
      doSearch(defaultValue);
    }
  }

  styles = {
    search: {
      SearchResults: {
        primary: {
          marginTop: 6,
        },
      },
      SearchInput: {
        Button: {
          width: 154,
        },
      },
    },
  };

  transform = (results) =>
    results.map((result) => ({
      primaryVal: result.name,
      secondaryVal: null,
      link: `/reports/${result._id}/`,
      key: result._id,
      badge: <TextButton style={{ height: 35 }}>Report</TextButton>,
    }));

  render() {
    const { doSearch, results, location, loading, searched } = this.props;
    return (
      <div>
        <h4>Reports</h4>
        <Search
          doSearch={doSearch}
          results={this.transform(results)}
          defaultValue={queryString.parse(location.search)?.q}
          message="Use sub-region"
          style={this.styles.search}
          loading={loading}
        />
        {searched && !results.length ? <div>No Results Found</div> : null}
      </div>
    );
  }
}

SearchReports.propTypes = {
  doSearch: PropTypes.func,
  results: PropTypes.array,
  searched: PropTypes.bool,
  location: PropTypes.object,
  loading: PropTypes.bool,
};

export default connect(
  (state) => ({ ...state.search }),
  (dispatch) => ({
    doSearch: (query) => dispatch(doSearchCreator(spots.findSubregions)(query)),
  }),
)(SearchReports);
