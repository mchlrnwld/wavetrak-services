import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { users } from '@surfline/admin-common';
import Search from '../components/Search/Search';
import { doSearchCreator } from '../actions/search';

class SearchUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      version: 1,
    };
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    const { doSearch, location } = this.props;
    const defaultValue = queryString.parse(location.search)?.q;
    const version = queryString.parse(location.search)?.version ?? 1;

    this.setState({ version });

    let link;
    if (location && location.query && location.query.link) {
      link = location.query.link;
    }

    if (defaultValue) {
      doSearch(defaultValue, { link, version });
    }
  }

  styles = {
    search: {
      SearchInput: {
        Button: {
          width: 154,
        },
      },
    },
  };

  transform = (results, link) =>
    results.map((result) => ({
      primaryVal: `${result.firstName} ${result.lastName}`,
      secondaryVal: result.email,
      link: `${link}${result._id}`,
      key: result._id,
    }));

  render() {
    const { doSearch, results, location, loading, searched } = this.props;
    const { version } = this.state;
    let link = '/users/edit/';
    const search = queryString.parse(location.search);

    if (search?.link) {
      link = search?.link;
    }

    return (
      <div>
        <h4>Users</h4>
        <Search
          doSearch={(query) => doSearch(query, { version })}
          results={this.transform(results, link)}
          defaultValue={queryString.parse(location.search)?.q}
          message="Use email"
          style={this.styles.search}
          loading={loading}
        />
        {searched && !results.length ? <div>No Results Found</div> : null}
      </div>
    );
  }
}

SearchUsers.propTypes = {
  doSearch: PropTypes.func,
  results: PropTypes.array,
  searched: PropTypes.bool,
  location: PropTypes.object,
  loading: PropTypes.bool,
};

export default connect(
  (state) => ({ ...state.search }),
  (dispatch) => ({
    doSearch: (query, opts) => dispatch(doSearchCreator(users.findUsers)(query, opts)),
  }),
)(SearchUsers);
