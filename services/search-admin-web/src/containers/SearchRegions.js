import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { TextButton } from '@surfline/admin-common';
import queryString from 'query-string';
import * as RegionsAPI from '../fetch/regions';
import Search from '../components/Search/Search';
import { doSearchCreator } from '../actions/search';

class SearchRegions extends Component {
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    const { doSearch, location } = this.props;
    const defaultValue = queryString.parse(location.search)?.q;
    if (defaultValue) {
      doSearch(defaultValue);
    }
  }

  styles = {
    search: {
      SearchResults: {
        primary: {
          marginTop: 6,
        },
      },
      SearchInput: {
        Button: {
          width: 154,
        },
      },
    },
  };

  edit = (regionId) => {
    window.location.href = `/regions/edit/${regionId}`;
  };

  transform = (results) =>
    results.map((result) => ({
      primaryVal: result.name,
      link: `/spots/regions/edit/${result._id}`,
      key: result._id,
      badge: <TextButton style={{ height: 35 }}>Edit</TextButton>,
    }));

  render() {
    const { doSearch, results, location, loading, searched } = this.props;
    return (
      <div>
        <h4>Regions</h4>
        {results ? (
          <Search
            doSearch={doSearch}
            results={this.transform(results)}
            defaultValue={queryString.parse(location.search)?.q}
            style={this.styles.search}
            loading={loading}
          />
        ) : null}
        {searched && !results.length ? <div>No Results Found</div> : null}
      </div>
    );
  }
}

SearchRegions.propTypes = {
  doSearch: PropTypes.func,
  results: PropTypes.array,
  searched: PropTypes.bool,
  location: PropTypes.object,
  loading: PropTypes.bool,
};

export default connect(
  (state) => ({ ...state.search }),
  (dispatch) => ({
    doSearch: (query) => dispatch(doSearchCreator(RegionsAPI.findRegions)(query)),
  }),
)(SearchRegions);
