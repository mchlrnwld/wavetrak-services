import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { TextButton } from '@surfline/admin-common';
import queryString from 'query-string';
import * as regionsAPI from '../fetch/regions';
import Search from '../components/Search/Search';
import { doSearchCreator } from '../actions/search';

class SearchForecasts extends Component {
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    const { doSearch, location } = this.props;
    const defaultValue = queryString.parse(location.search)?.q;
    if (defaultValue) {
      doSearch(defaultValue);
    }
  }

  styles = {
    search: {
      SearchResults: {
        primary: {
          marginTop: 6,
        },
      },
      SearchInput: {
        Button: {
          width: 154,
        },
      },
    },
  };

  transform = (results) =>
    results.map((result) => ({
      primaryVal: result.name,
      secondaryVal: null,
      link: `/forecasts/${result._id}/edit`,
      key: result._id,
      badge: <TextButton style={{ height: 35 }}>Forecast</TextButton>,
    }));

  render() {
    const { doSearch, results, location, loading, searched } = this.props;
    return (
      <div>
        <h4>Forecasts</h4>
        <Search
          doSearch={doSearch}
          results={this.transform(results)}
          defaultValue={queryString.parse(location.search)?.q}
          message="Use region"
          style={this.styles.search}
          loading={loading}
        />
        {searched && !results.length ? <div>No Results Found</div> : null}
      </div>
    );
  }
}

SearchForecasts.propTypes = {
  doSearch: PropTypes.func,
  loading: PropTypes.bool,
  location: PropTypes.object,
  results: PropTypes.array,
  searched: PropTypes.bool,
};

export default connect(
  (state) => ({ ...state.search }),
  (dispatch) => ({
    doSearch: (query) => dispatch(doSearchCreator(regionsAPI.findRegions)(query)),
  }),
)(SearchForecasts);
