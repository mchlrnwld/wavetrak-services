import { SearchSpots, connector } from './SearchSpots';

class SearchSpotsPoi extends SearchSpots {
  constructor(props) {
    super(props);
    this.poi = true;
  }
}

export default connector(SearchSpotsPoi);
