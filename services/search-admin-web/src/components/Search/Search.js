import React from 'react';
import { PropTypes } from 'prop-types';
import { SearchResults, SearchInput } from '@surfline/quiver-react';

import './Search.scss';
// comment test //
const Search = ({ defaultValue, doSearch, loading, message, results, style }) => (
  <div className="search" style={style}>
    <SearchInput
      defaultValue={defaultValue}
      onSubmit={doSearch}
      message={message}
      style={style && style.SearchInput}
      loading={loading}
    />
    <SearchResults results={results || []} style={style && style.SearchResults} />
  </div>
);

Search.propTypes = {
  defaultValue: PropTypes.string,
  doSearch: PropTypes.func,
  loading: PropTypes.bool,
  message: PropTypes.string,
  results: PropTypes.array,
  style: PropTypes.object,
};

export default Search;
