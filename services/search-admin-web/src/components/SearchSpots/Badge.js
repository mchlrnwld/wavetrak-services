import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { TextButton } from '@surfline/admin-common';

class Badge extends Component {
  constructor(props) {
    super(props);
    this.state = { confirmRemove: false };
  }

  remove = () => {
    const { remove, result } = this.props;
    remove(result._id);
  };

  toggleConfirmRemove = () => {
    const { confirmRemove } = this.state;
    this.setState({ confirmRemove: !confirmRemove });
  };

  style = {
    button: {
      width: 'auto',
      height: 73,
      display: 'inline-block',
    },
    link: {
      fontFamily: '"Futura-Dem", Helvetica, sans-serif',
      color: '#42A5FC',
      display: 'block',
      fontSize: '14px',
      textTransform: 'uppercase',
      letterSpacing: '1.5px',
      textDecoration: 'none',
      textAlign: 'center',
      marginTop: '28px',
      float: 'left',
      width: '115px',
    },
  };

  render() {
    const { edit, remove, result } = this.props;
    const { confirmRemove } = this.state;
    if (confirmRemove) {
      return (
        <div>
          {!result.removing ? (
            <TextButton onClick={this.toggleConfirmRemove} style={this.style.button}>
              Cancel
            </TextButton>
          ) : null}
          <TextButton
            type="active"
            onClick={this.remove}
            loading={result.removing}
            style={{ ...this.style.button }}
          >
            Delete
          </TextButton>
        </div>
      );
    }

    return (
      <div>
        {edit ? (
          <a href={edit(result)} style={this.style.link}>
            Edit
          </a>
        ) : null}
        {remove ? (
          <TextButton onClick={this.toggleConfirmRemove} style={this.style.button}>
            Remove
          </TextButton>
        ) : null}
      </div>
    );
  }
}

Badge.propTypes = {
  result: PropTypes.object,
  edit: PropTypes.func,
  remove: PropTypes.func,
};

export default Badge;
