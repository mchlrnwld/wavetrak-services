import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Switch } from 'react-router';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { AppContainer } from '@surfline/admin-common';
import { withErrorBoundary } from './containers/ErrorBoundary';
import SearchUsers from './containers/SearchUsers';
import SearchSpotsConnected from './containers/SearchSpots';
import SearchMswSpots from './containers/SearchMswSpots';
import SearchSubregions from './containers/SearchSubregions';
import SearchRegions from './containers/SearchRegions';
import SearchReports from './containers/SearchReports';
import SearchForecasts from './containers/SearchForecasts';
import reducer from './reducer';
import SearchSpotsPoi from './containers/SearchSpotsPoi';
import history from './history';

import './styles.scss';

const enhancers = compose(applyMiddleware(thunk));
const store = createStore(reducer, enhancers);

const App = withErrorBoundary(() => (
  <AppContainer>
    <Switch>
      <Route path="/search/users" component={SearchUsers} />
      <Route path="/search/spots" component={SearchSpotsConnected} />
      <Route path="/search/spotsPoi" component={SearchSpotsPoi} />
      <Route path="/search/mswSpots" component={SearchMswSpots} />
      <Route path="/search/subregions" component={SearchSubregions} />
      <Route path="/search/regions" component={SearchRegions} />
      <Route path="/search/reports" component={SearchReports} />
      <Route path="/search/forecasts" component={SearchForecasts} />
    </Switch>
  </AppContainer>
));

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root'),
);
