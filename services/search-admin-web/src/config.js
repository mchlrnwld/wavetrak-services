export default {
  development: {
    mswEndpoint: 'https://sandbox.magicseaweed.com/api/mdkey/',
    mswCredentials: 'msw:bantham',
  },
  sandbox: {
    mswEndpoint: 'https://sandbox.magicseaweed.com/api/mdkey/',
    mswCredentials: 'msw:bantham',
  },
  staging: {
    mswEndpoint: 'https://sandbox.magicseaweed.com/api/mdkey/',
    mswCredentials: 'msw:bantham',
  },
  production: {
    mswEndpoint: 'https://magicseaweed.com/api/mdkey/',
  },
}[process.env.APP_ENV || 'development'];
