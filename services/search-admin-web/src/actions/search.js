import history from '../history';

export const SEARCH = 'SEARCH';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'SEARCH_FAILURE';

export const doSearchCreator = (fetch) => (query, options = { link: null, version: 1 }) => async (
  dispatch,
) => {
  dispatch({ type: SEARCH });

  try {
    const response = await fetch(query, options.version);

    dispatch({
      type: SEARCH_SUCCESS,
      results: response,
    });

    const baseUrl = `${window.location.pathname}?q=${query}&version=${options.version}`;
    const url = options.link ? `${baseUrl}&link=${options.link}` : baseUrl;

    history.push(url);
  } catch (error) {
    dispatch({ type: SEARCH_FAILURE, error: 'Search failed' });
  }
};
