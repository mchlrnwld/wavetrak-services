# Backplane API

Base Endpoint: http://web-backplane.[env].surfline.com

## `GET /render:.:format`

```http
render.html?bundle=margins&renderOptions[largeFavoritesBar]=true
render.json?bundle=margins&userId=58f00c0fb016012247d2ceb5
```

### Query Params

| Param                               | Required | Value      | Description             |
| ----------------------------------- | -------- | ---------- | ----------------------- |
| `userId`                            | No       | string     | User ID                 |
| `bundle`                            | Yes      | 'margins'  | Bundle config name      |
| `renderOptions[bannerAd]`           | No       | 'homepage' | Banner ad to render     |
| `renderOptions[largeFavoritesBar]`  | No       | boolean    | Large Favorites toggle  |
| `renderOptions[showAdBlock]`        | No       | boolean    |                         |
| `renderOptions[qaFlag]`             | No       | boolean    |                         |
| `renderOptions[rateLimitingEnabled]`| No       | boolean    |                         |
| `renderOptions[isNavSticky]`        | No       | boolean    | Should the navigation stick to the window top on scroll |

### Sample Response

* `associated.timing`
* `data.bundles`
* `data.bundles[bundle]`

## `GET /backplane/static`

Serves static content from `src/public`.

## `GET /health`

Gets a JSON response containing status of the Backplane instance.

## `POST /heroad`
