# Extending Backplane Modules

Backplane uses a `bundle` to denote a set of react components, js files and css files. Each bundle can be rendered on the server for display in any application.

Examples of components that could live as a bundle

* embeddable cam player
* page level header and footers
* static error pages
* feedback widget

Any web based component that needs to be embedded in multiple logical applications but managed centrally are good candidates for a bundle.

## Organization
