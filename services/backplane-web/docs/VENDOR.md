# Vendor Bundles

Backplane manages vendor bundles that contain common frontend javascript libraries. These vendored libraries can be used across different frontend applications. Since these bundles are managed globably there is a greater likelihood that requests will result in a cache-hit.

## Using vendored libraries

The vendor bundle can be accessed through a Backplane response by accessing the `associated.vendor.js` property. This bundle is fingerprinted and busted every time a new version of Backplane is deployed. The bundle can be included onto a page via a `script` tag.

```html
<script src="https://product-cdn.sandbox.surfline.com/backplane/vendor-5040c88e871715fa7d0599fc292381a7dc691a01.js"></script>
```

See `src/server/template.js` for an example how to integrate.

The bundle contains an iife that will set vendored libraries onto the global `vendor` property.

## Setting up webpack to use vendored libraries

Vendored libraries can be brought into existing applications through the webpack [`externals`](https://webpack.js.org/configuration/externals/) keyword. An entry might look like

```javascript
externals: {
  react: 'vendor.React',
  'react-dom': 'vendor.ReactDOM',
}
```

This sets both `react` and `react-dom` to use libraries provided by the Backplane vendor bundle. By keeping vendored libraries under the `vendor` global property individual apps can still bundle their own version of a library. This should make upgrade paths easier down the road.

## Adding new libraries to the vendor bundle

Vendored libraries are defined in `src/vendor.js`. Adding new libraries to this file will automatically add them to the bundle. After the library has been added be sure to mark that library as an `external` in consuming applications.

## Implementation Details

To build vendor libraries the Rollup tool is used. Rollup provides the following benefits over Webpack.

* Rollup was designed with libraries rather than apps in mind
* Scope hoisting works out of the box and flattens libraries. This is contrast to webpack that separates code via modules.
  * Bundles can be built as an `iife` leading to smaller vendor bundles that execute faster
* There have been problems in the past bundling libs w/ webpack (specifically mapbox libs). rollup + closure should fix those build problems moving forward.

Code is minified using Closure rather than UglifyJS.

## Future Work

Currently Backplane doesn't support shipping vendor bundles with different versions of a library. Something like [npm-install-version](https://www.npmjs.com/package/npm-install-version) could be used to support building multiple bundles. As React get's upgraded this will likely become a requirement.

## Related Reading

* https://nolanlawson.com/2016/08/15/the-cost-of-small-modules/
* https://medium.com/webpack/webpack-and-rollup-the-same-but-different-a41ad427058c
