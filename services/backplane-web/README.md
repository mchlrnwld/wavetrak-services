# Web - Backplane

Backplane provides support APIs for web frontends and reduces the operational overhead of running a micro-frontend architecture. It coordinates common component rendering (such as Surfline’s navigation menu), critical path api requests and building common vendor javascript bundles.

## More Documentation

* [API](docs/API.md)
* [Architecture](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/203587590/Web+Backplane)
* [Vendor Bundles](docs/VENDOR.md)

## Developing

```
npm run dev
```

The render endpoint can be accessed as an html response, for example:

- [http://localhost:8080/render.html?bundle=margins&renderOptions[largeFavoritesBar]=true](http://localhost:8080/render.html?bundle=margins&renderOptions[largeFavoritesBar]=true)
- [http://web-backplane.sandbox.surfline.com/render.html?bundle=margins](http://web-backplane.sandbox.surfline.com/render.html?bundle=margins)

This is a good entrypoint for bundle development.

In development mode this html representation sets up HMR and other React development tooling.

### Environment Variables

Check the `.env.sample` for examples of the various environment variable values.

## Deploying

The backplane service can be deployed using the standard [`deploy-frontend-to-ecs`](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-frontend-to-ecs/build?delay=0sec) job.
