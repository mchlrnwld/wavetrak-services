import path from 'path';

const userRootPath = path.resolve(process.cwd());
const buildPath = path.join(userRootPath, 'build');
const srcPath = path.join(userRootPath, 'src');
const publicBuildPath = path.join(buildPath, 'public');

export default {
  userRootPath,
  srcPath,
  buildPath,
  publicBuildPath,
  publicSrcPath: path.join(srcPath, 'public'),
  serverSrcPath: path.join(srcPath, 'server'),
  clientSrcPath: path.join(srcPath, 'client'),
  clientBuildPath: path.join(buildPath, 'client'),
  serverBuildPath: path.join(buildPath, 'server'),
  testBuildPath: path.join(buildPath, 'test'),
  assetsBuildPath: path.join(publicBuildPath, '/'),
  vendorPath: '',
};
