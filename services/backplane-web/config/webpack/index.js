import merge from 'webpack-merge';

import baseConfig from './base.config';
import devClientConfig from './dev.client.config';
import devServerConfig from './dev.server.config';
import prodClientConfig from './prod.client.config';
import prodServerConfig from './prod.server.config';

export default (config, env = 'dev') => {
  let envClientConfig = devClientConfig;
  let envServerConfig = devServerConfig;

  if (env === 'prod') {
    envClientConfig = prodClientConfig;
    envServerConfig = prodServerConfig;
  }

  const clientConfig = merge.smart(baseConfig(), envClientConfig());
  const serverConfig = merge.smart(baseConfig(), envServerConfig());

  return {
    clientConfig,
    serverConfig,
  };
};
