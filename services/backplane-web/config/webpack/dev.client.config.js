import path from 'path';
import webpack from 'webpack';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import { getEntries } from './base.config';
import paths from '../paths';

const { buildPath } = paths;

const clientAssetPath = '//localhost:3002';

export default () => ({
  mode: 'development',
  target: 'web',
  entry: getEntries([
    'react-hot-loader/patch',
    `webpack-hot-middleware/client?reload=true&path=${clientAssetPath}/__webpack_hmr`,
  ]),
  output: {
    filename: 'bundle-[name].js',
    path: path.resolve(__dirname, '../../build'),
    publicPath: 'http://localhost:3002/',
    // Namespace source-map to ensure file-names do not clash
    library: 'backplane',
    devtoolNamespace: 'backplane',
  },
  context: path.resolve(__dirname, '../../src'),
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    contentBase: path.resolve(__dirname, '../../build'),
    publicPath: 'http://localhost:3002/',
  },
  externals: {
    react: 'vendor.React',
    'react-dom': 'vendor.ReactDOM',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
        exclude: [/node_modules/, path.resolve(__dirname, '../../build')],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?sourceMap', 'resolve-url-loader', 'postcss-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader?sourceMap',
          'resolve-url-loader',
          'sass-loader?sourceMap',
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
    new WebpackManifestPlugin({
      fileName: path.resolve(buildPath, 'clientAssets.json'),
      writeToFileEmit: true,
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
});
