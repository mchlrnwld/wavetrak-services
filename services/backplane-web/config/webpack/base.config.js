/** @format */

import path from 'path';
import webpack from 'webpack';
import paths from '../paths';
import bundles from '../../src/bundles/config';

export const getEntries = (loaders, relPath = '../../src/bundles/') =>
  Object.keys(bundles).reduce(
    (acc, bundle) => ({
      ...acc,
      [bundle]: [...loaders, path.resolve(__dirname, relPath, bundles[bundle].entryPoint)],
    }),
    {},
  );

export default () => {
  const { buildPath } = paths;

  return {
    node: {
      __dirname: true,
      __filename: true,
    },

    devtool: 'source-map',

    resolve: {
      extensions: ['.js', '.json'],
    },

    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.APP_ENV': JSON.stringify(process.env.APP_ENV),
        'process.env.CLIENT_PORT': JSON.stringify(process.env.CLIENT_PORT),
        SERVER_PORT: JSON.stringify(process.env.SERVER_PORT),
        CLIENT_PORT: JSON.stringify('8081'),
        'process.env.ASSETS_MANIFEST': JSON.stringify(path.join(buildPath, 'clientAssets.json')),
        'process.env.VENDOR_MANIFEST': JSON.stringify(
          path.join(buildPath, '/public/vendorManifest.json'),
        ),
      }),
    ],

    module: {
      noParse: /iconv-loader\.js/,
      rules: [
        {
          test: /\.html$/,
          loader: 'file?name=[name].[ext]',
        },
        {
          test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
          loader: 'file-loader',
        },
        {
          test: /\.(js|jsx)$/,
          use: {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
            },
          },
          exclude: [/node_modules/, buildPath],
        },
      ],
    },
  };
};
