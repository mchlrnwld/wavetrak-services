const webpack = require('webpack');

module.exports = () => {
  const config = {
    mode: 'development',
    devtool: 'inline-source-map',
    module: {
      rules: [
        {
          test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: '/',
            },
          },
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@surfline/babel-preset-web/base',
                  {
                    // Webpack 4 and Babel 7 has changed the way modules are
                    // compiled which breaks sinon using CJS modules allows sinon to work properly
                    modules: 'cjs',
                  },
                ],
              ],
              plugins: [
                [
                  'istanbul',
                  {
                    include: ['src/**/*.js'],
                    exclude: ['src/**/*.spec.js'],
                  },
                ],
              ],
            },
          },
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        sinon: 'sinon/pkg/sinon',
        react: require.resolve('react'),
      },
    },
    externals: ['react/addons', 'react/lib/ExecutionEnvironment', 'react/lib/ReactContext'],
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('development'),
        'process.env.APP_ENV': JSON.stringify('test'),
        'process.env.USER_SERVICE': JSON.stringify('user'),
        'process.env.ENTITLEMENTS_SERVICE': JSON.stringify('entitlements'),
        'process.env.AUTH_SERVICE': JSON.stringify('auth'),
        'process.env.KBYG_PRODUCT_API': JSON.stringify('kbyg'),
        'process.env.GEO_TARGET_SERVICE': JSON.stringify('geo'),
        'process.env.SUBSCRIPTIONS_SERVICE': JSON.stringify('subscription'),
        'process.env.SERVICES_URL': JSON.stringify('servicesURL'),
      }),
    ],
  };

  return config;
};
