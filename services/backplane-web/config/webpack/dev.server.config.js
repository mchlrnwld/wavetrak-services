import webpack from 'webpack';
import nodeExternals from 'webpack-node-externals';
import paths from '../paths';

const { serverSrcPath } = paths;

const cssLoaders = [
  {
    loader: 'css-loader',
    options: {
      modules: {
        localIdentName: '[name]-[local]--[hash:base64:5]',
        exportOnlyLocals: true,
      },
    },
  },
];

export default () => ({
  target: 'node',
  mode: 'development',
  node: {
    __dirname: false,
    __filename: false,
  },

  externals: nodeExternals(),

  entry: {
    main: [`${serverSrcPath}/index.js`],
  },

  output: {
    path: paths.serverBuildPath,
    filename: '[name].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: '/public/',
    libraryTarget: 'commonjs2',
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: cssLoaders,
      },
      {
        test: /\.scss$/,
        use: [...cssLoaders, 'sass-loader'],
      },
    ],
  },

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
  ],
});
