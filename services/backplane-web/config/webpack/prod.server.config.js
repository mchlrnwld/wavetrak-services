import nodeExternals from 'webpack-node-externals';
import paths from '../paths';

const cssStyleLoaders = [
  {
    loader: 'css-loader/locals',
    options: { modules: true, localIdentName: '[name]-[local]--[hash:base64:5]' },
  },
];

const { serverBuildPath, serverSrcPath } = paths;

export default () => ({
  mode: 'production',
  target: 'node',

  node: {
    __dirname: false,
    __filename: false,
  },

  externals: nodeExternals(),

  entry: {
    main: [`${serverSrcPath}/index.js`],
  },

  output: {
    path: serverBuildPath,
    filename: '[name].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: '/public/',
    libraryTarget: 'commonjs2',
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: cssStyleLoaders,
      },
      {
        test: /\.scss$/,
        use: [...cssStyleLoaders, ...'sass-loader'],
      },
    ],
  },
});
