import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import paths from '../paths';
import { getEntries } from './base.config';
import config from '../../src/config';

const { assetsBuildPath, buildPath } = paths;
export default () => ({
  mode: 'production',
  target: 'web',
  devtool: 'source-map',

  entry: getEntries([]),

  externals: {
    react: 'vendor.React',
    'react-dom': 'vendor.ReactDOM',
  },

  output: {
    path: assetsBuildPath,
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[hash].js',
    publicPath: config.cdn,
    // Namespace source-map to ensure file-names do not clash
    library: 'backplane',
    devtoolNamespace: 'backplane',
  },

  module: {
    rules: [
      {
        test: /\.css$|\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: () => [autoprefixer],
              },
            },
          },
          { loader: 'resolve-url-loader' },
          { loader: 'sass-loader', options: { sourceMap: true } },
        ],
      },
    ],
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: true,
      options: {
        context: process.cwd(),
        output: { path: path.resolve(__dirname, '../../build/') },
      },
    }),

    new MiniCssExtractPlugin({
      filename: '[name]-[contenthash].css',
      chunkFilename: '[id]-[contenthash].css',
    }),

    new WebpackManifestPlugin({
      fileName: path.resolve(buildPath, 'clientAssets.json'),
      writeToFileEmit: true,
    }),
  ],
});
