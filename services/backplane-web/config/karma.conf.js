const testWebpackConfig = require('./webpack/tests.config.js')({});

module.exports =
  /**
   * @param {import('karma').Config} config
   */
  (config) => {
    config.set({
      basePath: '',
      client: {
        mocha: {
          timeout: 60000, // 6 seconds - upped from 2 seconds
        },
      },
      frameworks: ['mocha'],
      files: [
        { pattern: './bundleTests.js', watchers: false },
        { pattern: 'http://www.googletagservices.com/tag/js/gpt.js', watchers: false },
      ],
      exclude: [],
      customLaunchers: {
        ChromeCustom: {
          base: 'ChromeHeadless',
          flags: ['--no-sandbox'],
        },
      },
      preprocessors: {
        './bundleTests.js': ['webpack', 'sourcemap', 'sourcemap-writer'],
      },
      webpack: testWebpackConfig,
      reporters: ['mocha', 'coverage'],
      webpackMiddleware: {
        stats: 'errors-only',
      },
      coverageReporter: {
        dir: '../coverage',
        reporters: [
          { type: 'text-summary' },
          { type: 'json' },
          { type: 'html' },
          { type: 'lcovonly' },
        ],
        includeAllSources: true,
      },
      plugins: [
        'karma-chrome-launcher',
        'karma-webpack',
        'karma-mocha',
        'karma-mocha-reporter',
        'karma-sourcemap-loader',
        'karma-sourcemap-writer',
        'karma-coverage',
      ],
      port: 9876,
      colors: true,
      logLevel: config.LOG_INFO,
      autoWatch: false,
      browsers: ['ChromeCustom'],
      singleRun: true,
      concurrency: Infinity,
      browserNoActivityTimeout: 30000,
    });
  };
