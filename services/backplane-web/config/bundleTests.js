import chai from 'chai';
import dirtyChai from 'dirty-chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import chaiSubset from 'chai-subset';

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

chai.use(chaiSubset);
chai.use(sinonChai);
chai.use(dirtyChai);

window.analytics = {
  track: sinon.spy(),
  identify: sinon.spy(),
  trackLink: sinon.spy(),
  page: sinon.spy(),
};

const testsContext = require.context('../src', true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);
