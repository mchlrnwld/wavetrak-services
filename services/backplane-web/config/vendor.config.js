import resolve from '@rollup/plugin-node-resolve';
import outputManifest from 'rollup-plugin-output-manifest';
import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import closure from '@ampproject/rollup-plugin-closure-compiler';
import appConfig from '../src/config';

export default (vendors) => {
  const inputs = vendors.reduce(
    (prev, current) => ({ ...prev, [current.name]: current.source }),
    {},
  );

  return {
    input: inputs,
    output: {
      format: 'iife',
      sourcemap: true,
      dir: 'build/public/',
      name: 'vendor',
      entryFileNames: '[name].[hash].js',
    },
    plugins: [
      resolve(),
      commonjs(),
      replace({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.APP_ENV': JSON.stringify(process.env.APP_ENV),
        preventAssignment: false,
      }),
      closure({
        compilationLevel: 'SIMPLE',
        languageIn: 'ECMASCRIPT6',
        languageOut: 'ECMASCRIPT5_STRICT',
        env: 'CUSTOM',
        warningLevel: 'DEFAULT',
        applyInputSourceMaps: false,
        useTypesForOptimization: false,
        processCommonJsModules: true,
      }),
      outputManifest({
        fileName: 'vendorManifest.json',
        publicPath: appConfig.cdn,
        nameWithExt: false,
      }),
    ],
  };
};
