/* istanbul ignore file */
import { baseFetch } from '@surfline/web-common';
import config from '../config';

export const fetchArticleSponsor = (id) =>
  baseFetch(`${config.surflineHost}/wp-json/sl/v1/post/sponsor?id=${id}`);

/**
 * @param {"success" | "failure"} result
 * @param {"on" | "off"} treatment
 * @param {string | number} width
 * @param {string | number } height
 * @returns {baseFetch<{ message: string }>}
 */
export const trackHeroAd = (result, treatment, width, height) =>
  baseFetch(`${config.servicesURL}/backplane/heroad`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ result, treatment, width, height }),
  });

export default trackHeroAd;
