/* istanbul ignore file */

import {
  baseFetch,
  incrementAnonymousMeter as doIncrementAnonymousMeter,
} from '@surfline/web-common';
import config from '../config';

const { servicesURL } = config;

/**
 * @function getMeter
 * @param {string} accessToken
 * @return {Promise<import('@surfline/web-common').MeterState}
 */
export const getMeter = (accessToken) =>
  baseFetch(`${servicesURL}/meter/`, {
    method: 'GET',
    headers: {
      ...(accessToken && { 'X-Auth-AccessToken': accessToken }),
    },
  });

/**
 * @param {{ accessToken: string, anonymousId: string, timezone: string, countryCode: string }} data
 * @returns {Promise<{id: string, meterLimit: number, meterRemaining: number}>}
 */
export const postMeter = (data) => {
  const { accessToken, anonymousId, timezone, countryCode } = data;
  const body = { anonymousId, timezone, countryCode };
  return baseFetch(`${servicesURL}/meter/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      ...(accessToken && { 'X-Auth-AccessToken': accessToken }),
    },
    body: JSON.stringify(body),
  });
};

/**
 * @async
 * @function getAnonymousMeter
 * @param {string} anonymousId
 * @return {Promise<{ meterRemaining: number | null }>}
 */
export const getAnonymousMeter = async (anonymousId) => {
  try {
    const response = await baseFetch(`${servicesURL}/meter/anonymous/${anonymousId}/`, {
      method: 'GET',
    });
    return response;
  } catch (err) {
    return { meterRemaining: null };
  }
};

/**
 * @function postAnonymousMeter
 * @param {string} anonymousId
 * @return {Promise<void>}
 */
export const postAnonymousMeter = (anonymousId) => {
  const body = { anonymousId };
  return baseFetch(`${servicesURL}/meter/anonymous/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  });
};

/**
 * @function patchMeteringSurfCheck
 * @param {string} accessToken
 * @return {Promise<{ meterRemaining: number | null }>}
 */
export const patchMeteringSurfCheck = (accessToken) =>
  baseFetch(`${servicesURL}/meter/surf-check/`, {
    method: 'PATCH',
    headers: {
      ...(accessToken && { 'X-Auth-AccessToken': accessToken }),
    },
  });

/**
 * @function incrementAnonymousMeter
 * @param {string} anonymousId
 * @return {Promise<{ meterRemaining: number | null }>}
 */
export const incrementAnonymousMeter = (anonymousId) => doIncrementAnonymousMeter(anonymousId);
