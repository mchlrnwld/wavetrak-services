/* istanbul ignore file */

import { baseFetch } from '@surfline/web-common';
import config from '../config';

/**
 * @param {string} baseUrl
 * @param {string} ipAddress
 */
export const region = (baseUrl, ipAddress) => baseFetch(`${baseUrl}/region?ipAddress=${ipAddress}`);

/** @param {string} ipAddress */
export const serverSideGetRegion = (ipAddress) => region(config.geotarget, ipAddress);

/** @param {string} ipAddress */
export const getRegion = (ipAddress) => region(config.servicesURL, ipAddress);

export default region;
