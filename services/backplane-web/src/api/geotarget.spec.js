import * as baseFetch from '@surfline/web-common/esm/api/baseFetch';
import { expect } from 'chai';
import sinon from 'sinon';

import * as api from './geotarget';

// TODO: We can't make these work till we switch to Jest. It has to do with how ESM is compiled by
// babel in web-common
describe.skip('api / geotarget', () => {
  beforeEach(() => {
    sinon.stub(baseFetch, 'default');
  });
  afterEach(() => {
    baseFetch.default.restore();
  });

  it(`should calls the region endpoint`, () => {
    api.region('baseUrl', 'ipAddress');

    expect(baseFetch.default).to.have.been.calledOnceWithExactly(
      'baseUrl/region?ipAddress=ipAddress',
    );
  });

  it(`should handle server side calls`, () => {
    api.serverSideGetRegion('ipAddress');

    expect(baseFetch.default).to.have.been.calledOnceWithExactly('geo/region?ipAddress=ipAddress');
  });
});
