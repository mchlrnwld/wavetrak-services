/* istanbul ignore file */
import {
  baseFetch,
  fetchWithAuth,
  authenticateAccessToken,
  fetchUserSettings,
  fetchUserFavorites,
  fetchUserEntitlements,
  fetchUserDetails,
  setAuthCookies,
} from '@surfline/web-common';
import config from '../config';

const { clientId, clientSecret, servicesURL, subscription } = config;

const AUTHORIZATION_STRING = `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString(
  'base64',
)}`;

/**
 * @param {string} [customUrl]
 * @param {string} [userId]
 */
export const fetchSubscriptionWarnings = (customUrl = null, userId = null) =>
  fetchWithAuth(
    '/payment/alerts?product=sl',
    {
      headers: {
        ...(userId ? { 'x-auth-userid': userId } : null),
      },
    },
    { customUrl },
  );

/**
 * @description Generate an access token for a user.
 *
 * @function generateToken
 * @param {boolean} staySignedIn
 * @param {string} encodedFormString - String of encoded form key-value pairs
 * @returns {Promise<{ accessToken: string, refreshToken: string, expiresIn: number }>}
 */
export const generateToken = async (staySignedIn, encodedFormString) => {
  try {
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await baseFetch(
      `${servicesURL}/trusted/token?isShortLived=${!staySignedIn}`,
      {
        method: 'POST',
        headers: {
          credentials: 'same-origin',
          Authorization: AUTHORIZATION_STRING,
          'content-type': 'application/x-www-form-urlencoded',
        },
        body: encodedFormString,
      },
      { addAccessToken: false },
    );

    setAuthCookies(accessToken, refreshToken, expiresIn, staySignedIn);
    return { accessToken, refreshToken, expiresIn };
  } catch (err) {
    const body = await err.response?.json();
    if (body) {
      throw new Error(body.error_description || 'Something went wrong');
    }
    throw err;
  }
};

/**
 * @template T
 * @param {boolean} isShortLived
 * @param {object} user
 * @param {string} user.firstName
 * @param {string} user.lastName
 * @param {string} user.email
 * @param {string} user.password
 * @param {object} user.settings
 * @param {string} user.settings.receivePromotions
 * @param {string} user.client_id
 * @param {string} user.device_id
 * @param {string} user.device_type
 * @returns {Promise<T>}
 */
export const postUser = async (isShortLived, user) => {
  try {
    const {
      token: { access_token: accessToken, refresh_token: refreshToken, expires_in: expiresIn },
      user: { _id: userId },
    } = await baseFetch(
      `${servicesURL}/user?isShortLived=${isShortLived}`,
      {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
        },
        body: JSON.stringify(user),
      },
      { addAccessToken: false },
    );

    setAuthCookies(accessToken, refreshToken, expiresIn, !isShortLived);
    return { userId, accessToken };
  } catch (err) {
    const body = await err.response?.json();
    if (body) {
      throw new Error(body.error_description || 'Something went wrong');
    }
    throw err;
  }
};

/**
 * @description Wrapper function which adds the headers that are needed to
 * user backplane as a proxy for making the user requests. These headers are
 * necessary since these API's use geoLocation based on IP (which is lost
 * when using a server as a request proxy)
 * @param {string} userId
 * @param {string} countryIso
 * @param {string} clientIp
 */
export const serverSideFetchUserSettings = (userId, countryIso, clientIp) =>
  fetchUserSettings(
    {
      headers: {
        'x-geo-country-iso': countryIso,
        'x-client-ip': clientIp,
        ...(userId && { 'x-auth-userid': userId }),
      },
    },
    { customUrl: config.user },
  );

/**
 * @description Wrapper function which adds the headers that are needed to
 * user backplane as a proxy for making the user requests. These headers are
 * necessary since these API's use geoLocation based on IP (which is lost
 * when using a server as a request proxy)
 * @param {string} userId
 * @param {string} countryIso
 * @param {string} clientIp
 * @param {{ latitude: string | number, longitude: string | number }} location
 */
export const serverSideFetchUserFavorites = (userId, countryIso, clientIp, location) =>
  fetchUserFavorites(
    {
      headers: {
        'x-geo-latitude': location?.latitude,
        'x-geo-longitude': location?.longitude,
        'x-client-ip': clientIp,
        'x-geo-country-iso': countryIso,
        ...(userId && { 'x-auth-userid': userId }),
      },
    },
    { customUrl: config.kbyg },
  );

/**
 * @description Wrapper function which adds the headers that are needed to
 * user backplane as a proxy for making the user requests. These headers are
 * necessary since these API's use geoLocation based on IP (which is lost
 * when using a server as a request proxy)
 * @param {string} userId
 */
export const serverSideFetchUserEntitlements = (userId) =>
  fetchUserEntitlements(
    {
      headers: {
        'x-auth-userid': userId,
      },
    },
    { customUrl: config.entitlements },
  );

/**
 * @description Wrapper function which adds the headers that are needed to
 * user backplane as a proxy for making the user requests. These headers are
 * necessary since these API's use geoLocation based on IP (which is lost
 * when using a server as a request proxy)
 * @param {string} userId
 */
export const serverSideFetchUserDetails = (userId) =>
  fetchUserDetails(
    {
      headers: {
        'x-auth-userid': userId,
      },
    },
    { customUrl: config.user },
  );

/**
 * @param {string} accessToken
 */
export const authoriseAccessToken = (accessToken) =>
  authenticateAccessToken(servicesURL, accessToken);

/**
 * @param {string} userId
 */
export const serverSideFetchSubscriptionWarnings = (userId) =>
  fetchSubscriptionWarnings(subscription, userId);

export const getUserSubscriptionWarnings = (userId) => fetchSubscriptionWarnings(undefined, userId);

export const getUserDetails = () => fetchUserDetails(undefined, { useAccessTokenQueryParam: true });

export const getUserEntitlements = () =>
  fetchUserEntitlements(undefined, { useAccessTokenQueryParam: true });

export const getUserSettings = () =>
  fetchUserSettings(undefined, { useAccessTokenQueryParam: true });

export const getUserFavorites = () =>
  fetchUserFavorites(undefined, { customUrl: `${servicesURL}/kbyg` });
