import * as baseFetch from '@surfline/web-common/esm/api/baseFetch';
import { expect } from 'chai';
import sinon from 'sinon';
import * as setAuthCookies from '@surfline/web-common/esm/utils/setAuthCookies';

import * as api from './user';

// TODO: We can't make these work till we switch to Jest. It has to do with how ESM is compiled by
// babel in web-common
describe.skip('api / user', () => {
  beforeEach(() => {
    sinon.stub(baseFetch, 'default');
    sinon.stub(setAuthCookies, 'default');
  });
  afterEach(() => {
    baseFetch.default.restore();
    setAuthCookies.default.restore();
  });

  it(`should generate a token and set the cookies - staySignedIn`, async () => {
    const accessToken = '1';
    const refreshToken = '2';
    const expiresIn = 100;
    baseFetch.default.resolves({
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    });
    const result = await api.generateToken(true, 'form');

    expect(result).to.deep.equal({ accessToken, refreshToken, expiresIn });
    expect(baseFetch.default).to.have.been.calledOnceWithExactly(
      'servicesURL/trusted/token?isShortLived=false',
      {
        method: 'POST',
        headers: {
          credentials: 'same-origin',
          Authorization: 'Basic dW5kZWZpbmVkOnVuZGVmaW5lZA==',
          'content-type': 'application/x-www-form-urlencoded',
        },
        body: 'form',
      },
      { addAccessToken: false },
    );
    expect(setAuthCookies.default).to.have.been.calledOnceWithExactly(
      accessToken,
      refreshToken,
      expiresIn,
      true,
    );
  });

  it(`should generate a token and set the cookies`, async () => {
    const accessToken = '1';
    const refreshToken = '2';
    const expiresIn = 100;
    baseFetch.default.resolves({
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    });
    const result = await api.generateToken(false, 'form');

    expect(result).to.deep.equal({ accessToken, refreshToken, expiresIn });
    expect(baseFetch.default).to.have.been.calledOnceWithExactly(
      'servicesURL/trusted/token?isShortLived=true',
      {
        method: 'POST',
        headers: {
          credentials: 'same-origin',
          Authorization: 'Basic dW5kZWZpbmVkOnVuZGVmaW5lZA==',
          'content-type': 'application/x-www-form-urlencoded',
        },
        body: 'form',
      },
      { addAccessToken: false },
    );
    expect(setAuthCookies.default).to.have.been.calledOnceWithExactly(
      accessToken,
      refreshToken,
      expiresIn,
      false,
    );
  });
});
