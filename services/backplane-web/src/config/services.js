export default {
  user: process.env.USER_SERVICE,
  entitlements: process.env.ENTITLEMENTS_SERVICE,
  auth: process.env.AUTH_SERVICE,
  kbyg: process.env.KBYG_PRODUCT_API,
  geotarget: process.env.GEO_TARGET_SERVICE,
  subscription: process.env.SUBSCRIPTIONS_SERVICE,
  servicesURL: process.env.SERVICES_URL,
};
