/* istanbul ignore file */

/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import { canUseDOM } from '@surfline/web-common';
import baseConfig from './base';

/**
 * @typedef {Object} Config
 * @property {string} clientAssetPath
 * @property {string} surflineHost
 * @property {string} cdn
 * @property {string} servicesURL
 * @property {'/upgrade'} funnelUrl
 * @property {Object} appKeys
 * @property {string} appKeys.segment
 * @property {string} appKeys.newrelic
 * @property {string} appKeys.splitio
 * @property {string} [user]
 * @property {string?} [entitlements]
 * @property {string?} [auth]
 * @property {string?} [kbyg]
 * @property {string?} [geotarget]
 * @property {string?} [subscription]
 */

const envConfig = require(`./envs/${process.env.APP_ENV}`);

/** @type {Config} */
const config = {
  ...baseConfig,
  ...(envConfig.default || {}),
  ...(!canUseDOM && (require('./services').default || {})),
};

export default config;
