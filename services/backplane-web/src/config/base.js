import { accountPaths } from '@surfline/web-common';

export default {
  clientAssetPath: `//localhost:${process.env.CLIENT_PORT}/`,
  surflineHost: 'localhost:8080',
  cdn: '/backplane/static/',
  servicesURL: 'https://services.sandbox.surfline.com',
  funnelUrl: accountPaths.funnelPath,
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  },
};
