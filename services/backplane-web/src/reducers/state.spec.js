import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  createMeter,
  setMeterLoaded,
  getAnonymousMeter,
  getMeter,
  performedMeterModalRecompute,
  triggerMeterModalRecompute,
} from '../actions';
import state from './state';

describe('reducers / state', () => {
  const initialState = deepFreeze({
    shouldRecomputeMeterModal: false,
    meterLoaded: false,
    meteringEnabled: false,
  });

  it('initializes with default values', () => {
    expect(state(undefined, {})).to.deep.equal(initialState);
  });

  it(`should handle ${triggerMeterModalRecompute.type}`, () => {
    expect(state(initialState, triggerMeterModalRecompute())).to.deep.equal({
      shouldRecomputeMeterModal: true,
      meterLoaded: false,
      meteringEnabled: false,
    });
  });

  it(`should handle ${performedMeterModalRecompute.type}`, () => {
    expect(state(initialState, performedMeterModalRecompute())).to.deep.equal({
      shouldRecomputeMeterModal: false,
      meterLoaded: false,
      meteringEnabled: false,
    });
  });

  it(`should handle setting meterLoaded when ${setMeterLoaded.type} dispatched`, () => {
    expect(state(initialState, { type: setMeterLoaded.type }).meterLoaded).to.equal(true);
  });

  it(`should handle setting meterLoaded false`, () => {
    expect(
      state({ ...initialState, meterLoaded: true }, { type: setMeterLoaded.type, payload: false })
        .meterLoaded,
    ).to.equal(false);
  });

  it(`should handle setting meterLoaded when ${createMeter.fulfilled} dispatched`, () => {
    expect(state(initialState, { type: createMeter.fulfilled }).meterLoaded).to.equal(true);
  });

  it(`should handle setting meterLoaded when ${getMeter.fulfilled} dispatched`, () => {
    expect(state(initialState, { type: getMeter.fulfilled }).meterLoaded).to.equal(true);
  });

  it(`should handle setting meterLoaded when ${getAnonymousMeter.fulfilled} dispatched`, () => {
    expect(state(initialState, { type: getAnonymousMeter.fulfilled }).meterLoaded).to.equal(true);
  });
});
