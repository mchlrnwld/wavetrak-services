import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { setMeterRemaining, performAnonymousMeteringSurfCheck } from '@surfline/web-common';
import {
  setMeter,
  getMeter,
  getAnonymousMeter,
  createMeter,
  clearMeter,
  performMeteringSurfCheck,
} from '../actions/metering';
import metering from './metering';

describe('reducers / metering', () => {
  const initialState = null;
  const stateWithMeter = deepFreeze({
    meterRemaining: 1,
  });

  it('should initialize with default values', () => {
    expect(metering(undefined, { type: 'unrelated' })).to.deep.equal(null);
  });

  it(`should set the meter on ${setMeter.type}`, () => {
    expect(metering(initialState, setMeter({ meterRemaining: 5 }))).to.deep.equal({
      meterRemaining: 5,
    });
  });

  it(`shoul clear meter on ${clearMeter.type}`, () => {
    expect(metering(stateWithMeter, clearMeter())).to.deep.equal(null);
  });
  it(`should set the meter on ${setMeterRemaining.type}`, () => {
    expect(metering(stateWithMeter, setMeterRemaining(0))).to.deep.equal({
      meterRemaining: 0,
    });
  });

  it(`should update the meter on ${getMeter.fulfilled}`, () => {
    expect(
      metering(stateWithMeter, { type: getMeter.fulfilled, payload: { meterRemaining: 2 } }),
    ).to.deep.equal({
      meterRemaining: 2,
    });
  });

  it(`should not update the meter on ${getMeter.fulfilled} - createdNewMeter`, () => {
    expect(
      metering(stateWithMeter, {
        type: getMeter.fulfilled,
        payload: { meterRemaining: 2, createdNewMeter: true },
      }),
    ).to.deep.equal(stateWithMeter);
  });

  it(`should clear the meter on ${getMeter.rejected}`, () => {
    expect(metering(stateWithMeter, { type: getMeter.rejected })).to.be.equal(null);
  });

  it(`should handle ${getMeter.rejected}`, () => {
    expect(metering(initialState, { type: getMeter.rejected })).to.be.equal(null);
  });

  it(`should update the meter on ${getAnonymousMeter.fulfilled}`, () => {
    expect(
      metering(stateWithMeter, {
        type: getAnonymousMeter.fulfilled,
        payload: { meterRemaining: 2 },
      }),
    ).to.deep.equal({
      meterRemaining: 2,
    });
  });

  it(`should clear the state if ${getAnonymousMeter.fulfilled} has no payload`, () => {
    expect(
      metering(stateWithMeter, {
        type: getAnonymousMeter.fulfilled,
        payload: null,
      }),
    ).to.deep.equal(null);
  });

  it(`should update the meter on ${createMeter.fulfilled}`, () => {
    expect(
      metering(stateWithMeter, { type: createMeter.fulfilled, payload: { meterRemaining: 2 } }),
    ).to.deep.equal({
      meterRemaining: 2,
    });
  });

  it(`should clear the meter on ${createMeter.rejected}`, () => {
    expect(metering(stateWithMeter, { type: createMeter.rejected })).to.be.equal(null);
  });

  it(`should handle ${createMeter.rejected}`, () => {
    expect(metering(initialState, { type: createMeter.rejected })).to.be.equal(null);
  });

  it(`should update the meter on ${performMeteringSurfCheck.fulfilled}`, () => {
    expect(
      metering(stateWithMeter, {
        type: performMeteringSurfCheck.fulfilled,
        payload: { meterRemaining: 2 },
      }),
    ).to.deep.equal({
      meterRemaining: 2,
    });
  });

  it(`should handle ${performAnonymousMeteringSurfCheck.fulfilled}`, () => {
    expect(
      metering(stateWithMeter, { type: performAnonymousMeteringSurfCheck.fulfilled, payload: -1 }),
    ).to.deep.equal({ meterRemaining: -1 });
  });
});
