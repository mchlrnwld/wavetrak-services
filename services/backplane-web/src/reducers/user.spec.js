import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  resendEmailVerification,
  resetEmailVerificationButton,
  favoriteSpot,
  unfavoriteSpot,
  setRecentlyVisited,
  SL_METERED,
} from '@surfline/web-common';
import {
  fetchUserDetails,
  fetchUserEntitlements,
  fetchUserFavorites,
  fetchUserSettings,
  fetchUserSubscriptionWarnings,
  setMeteringEntitlement,
  setUserMetadata,
} from '../actions/user';
import user from './user';

describe('reducers / user', () => {
  const initialState = deepFreeze({
    details: null,
    entitlements: [],
    promotionEntitlements: [],
    favorites: [],
    settings: null,
    warnings: [],
  });

  it('initializes with default values', () => {
    expect(user(undefined, {})).to.deep.equal(initialState);
  });

  it(`handles ${setUserMetadata.type}`, () => {
    expect(
      user(
        initialState,
        setUserMetadata({
          countryCode: 'countryCode',
          subdivisionCode: 'subdivisionCode',
          location: { time_zone: 'timezone' },
          ip: '8.8.8.8',
        }),
      ),
    ).to.deep.equal({
      ...initialState,
      countryCode: 'countryCode',
      subdivisionCode: 'subdivisionCode',
      location: { time_zone: 'timezone' },
      ip: '8.8.8.8',
    });
  });

  it(`handles ${setMeteringEntitlement.type}`, () => {
    expect(user(initialState, setMeteringEntitlement())).to.deep.equal({
      ...initialState,
      entitlements: [SL_METERED],
    });
  });

  it(`sets loading on ${fetchUserDetails.pending}`, () => {
    expect(user(initialState, { type: fetchUserDetails.pending.toString() })).to.deep.equal({
      ...initialState,
      detailsLoading: true,
    });
  });

  it(`sets loading and userDetails on ${fetchUserDetails.fulfilled}`, () => {
    const action = deepFreeze({
      type: fetchUserDetails.fulfilled,
      payload: {
        details: {
          firstName: 'Lucy',
          lastName: 'Goosey',
          email: 'lucybear@gmail.com',
          _id: 'a',
          isEmailVerified: true,
          password: 'salted-hashed-password',
        },
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      details: { ...action.payload.details, password: undefined },
      detailsLoading: false,
    });
  });

  it(`sets error and loading on ${fetchUserDetails.rejected}`, () => {
    const action = deepFreeze({
      type: fetchUserDetails.rejected.toString(),
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      detailsLoading: false,
      detailsError: 'An error occurred',
    });
  });

  it(`sets loading on ${fetchUserEntitlements.pending}`, () => {
    expect(user(initialState, { type: fetchUserEntitlements.pending.toString() })).to.deep.equal({
      ...initialState,
      details: null,
      entitlementsLoading: true,
    });
  });

  it(`sets loading and entitlements on ${fetchUserEntitlements.fulfilled}`, () => {
    const action = deepFreeze({
      type: fetchUserEntitlements.fulfilled,
      payload: {
        entitlements: {
          entitlements: ['sl_premium'],
          promotions: ['sl_promotional'],
          legacyEntitlements: ['sl_legacy'],
          hasNewAccount: true,
        },
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      entitlements: ['sl_premium'],
      promotionEntitlements: ['sl_promotional'],
      entitlementsLoading: false,
      legacyEntitlements: ['sl_legacy'],
      hasNewAccount: true,
    });
  });

  it(`sets error and loading on ${fetchUserEntitlements.rejected}`, () => {
    const action = deepFreeze({
      type: fetchUserEntitlements.rejected.toString(),
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      entitlements: [],
      promotionEntitlements: [],
      entitlementsLoading: false,
      entitlementsError: 'An error occurred',
    });
  });

  it(`sets loading on ${fetchUserFavorites.pending}`, () => {
    expect(user(initialState, { type: fetchUserFavorites.pending.toString() })).to.deep.equal({
      ...initialState,
      favoritesLoading: true,
    });
  });

  it(`sets loading and favorites on ${fetchUserFavorites.fulfilled}`, () => {
    const action = deepFreeze({
      type: fetchUserFavorites.fulfilled,
      payload: {
        favorites: [{ _id: '123' }],
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favorites: [{ _id: '123' }],
      favoritesLoading: false,
    });
  });

  it(`sets error and loading on ${fetchUserFavorites.rejected}`, () => {
    const action = deepFreeze({
      type: fetchUserFavorites.rejected.toString(),
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favorites: [],
      favoritesLoading: false,
      favoritesError: 'An error occurred',
    });
  });

  it(`sets loading on ${fetchUserSettings.pending}`, () => {
    expect(user(initialState, { type: fetchUserSettings.pending.toString() })).to.deep.equal({
      ...initialState,
      settings: null,
      settingsLoading: true,
    });
  });

  it(`sets loading and settings on ${fetchUserSettings.fulfilled}`, () => {
    const action = deepFreeze({
      type: fetchUserSettings.fulfilled,
      payload: {
        settings: { _id: '123' },
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      settings: { _id: '123' },
      settingsLoading: false,
    });
  });

  it(`sets error and loading on ${fetchUserSettings.rejected}`, () => {
    const action = deepFreeze({
      type: fetchUserSettings.rejected.toString(),
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      settings: null,
      settingsLoading: false,
      settingsError: 'An error occurred',
    });
  });

  it(`sets loading on ${resendEmailVerification.pending}`, () => {
    expect(user(initialState, { type: resendEmailVerification.pending.toString() })).to.deep.equal({
      ...initialState,
      resendEmailLoading: true,
    });
  });

  it(`sets loading and settings on ${resendEmailVerification.fulfilled}`, () => {
    const action = deepFreeze({
      type: resendEmailVerification.fulfilled,
      payload: true,
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      emailSent: true,
      resendEmailLoading: false,
    });
  });

  it(`sets error and loading on ${resendEmailVerification.rejected}`, () => {
    const action = deepFreeze({
      type: resendEmailVerification.rejected.toString(),
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      resendEmailLoading: false,
      resendEmailError: 'An error occurred',
      emailSent: false,
    });
  });

  it(`resets emailSent and loading ${resetEmailVerificationButton}`, () => {
    const action = deepFreeze({
      type: resetEmailVerificationButton.type,
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      resendEmailLoading: false,
      emailSent: false,
    });
  });

  it(`sets loading on ${favoriteSpot.pending}`, () => {
    const action = deepFreeze({
      type: favoriteSpot.pending,
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favoriteSpotLoading: true,
    });
  });

  it(`sets favorites and loading on ${favoriteSpot.fulfilled}`, () => {
    const action = deepFreeze({
      type: favoriteSpot.fulfilled,
      payload: { favorites: [{ _id: '123' }] },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favorites: [{ _id: '123' }],
      favoriteSpotLoading: false,
    });
  });

  it(`sets error and loading on ${favoriteSpot.rejected}`, () => {
    const action = deepFreeze({
      type: favoriteSpot.rejected,
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favoriteSpotLoading: false,
      favoriteSpotError: 'An error occurred',
    });
  });

  it(`sets loading on ${unfavoriteSpot.pending}`, () => {
    const action = deepFreeze({
      type: unfavoriteSpot.pending,
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favoriteSpotLoading: true,
    });
  });

  it(`sets favorites and loading on ${unfavoriteSpot.fulfilled}`, () => {
    const action = deepFreeze({
      type: unfavoriteSpot.fulfilled,
      payload: { spotId: '123' },
    });

    expect(user({ ...initialState, favorites: [{ _id: '123' }] }, action)).to.deep.equal({
      ...initialState,
      favoriteSpotLoading: false,
    });
  });

  it(`sets error and loading on ${unfavoriteSpot.rejected}`, () => {
    const action = deepFreeze({
      type: unfavoriteSpot.rejected,
      error: {
        message: 'An error occurred',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      favoriteSpotLoading: false,
      favoriteSpotError: 'An error occurred',
    });
  });

  it(`sets favorites and loading on ${setRecentlyVisited.fulfilled}`, () => {
    const action = deepFreeze({
      type: setRecentlyVisited.fulfilled,
      payload: { recentlyVisited: { spots: [{ _id: '123' }] } },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      settings: {
        recentlyVisited: {
          spots: [{ _id: '123' }],
        },
      },
    });
  });

  it(`sets warnings on ${fetchUserSubscriptionWarnings.fulfilled}`, () => {
    const action = deepFreeze({
      type: fetchUserSubscriptionWarnings.fulfilled,
      payload: {
        warnings: [{ warning: true }],
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      ...initialState,
      warnings: [{ warning: true }],
    });
  });
});
