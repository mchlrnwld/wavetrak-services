import { createReducer } from '@reduxjs/toolkit';
import {
  resendEmailVerification,
  resetEmailVerificationButton,
  favoriteSpot,
  unfavoriteSpot,
  setRecentlyVisited,
  SL_METERED,
} from '@surfline/web-common';

import {
  fetchUserDetails,
  fetchUserEntitlements,
  fetchUserFavorites,
  fetchUserSettings,
  fetchUserSubscriptionWarnings,
  setMeteringEntitlement,
  setUserMetadata,
} from '../actions/user';

const initialState = {
  details: null,
  entitlements: [],
  promotionEntitlements: [],
  favorites: [],
  settings: null,
  warnings: [],
};

const reducer = createReducer(initialState, {
  [setUserMetadata]: (state, action) => ({
    ...state,
    countryCode: action.payload?.countryCode,
    subdivisionCode: action.payload?.subdivisionCode,
    location: action.payload?.location,
    ip: action.payload?.ip,
  }),

  [fetchUserDetails.pending]: (state) => ({
    ...state,
    detailsLoading: true,
  }),

  [fetchUserDetails.fulfilled]: (state, { payload: { details } }) => ({
    ...state,
    detailsLoading: false,
    details: {
      ...details,
      firstName: details.firstName,
      lastName: details.lastName,
      email: details.email,
      _id: details._id,
      isEmailVerified: details.isEmailVerified,
      // We don't want the hashed/salted password in the store
      password: undefined,
    },
  }),

  [fetchUserDetails.rejected]: (state, { error }) => ({
    ...state,
    detailsLoading: false,
    detailsError: error.message,
  }),

  [fetchUserEntitlements.pending]: (state) => ({
    ...state,
    entitlementsLoading: true,
  }),

  [fetchUserEntitlements.fulfilled]: (state, { payload: { entitlements } }) => ({
    ...state,
    entitlementsLoading: false,
    entitlements: entitlements.entitlements,
    promotionEntitlements: entitlements.promotions,
    legacyEntitlements: entitlements.legacyEntitlements,
    hasNewAccount: entitlements.hasNewAccount,
  }),

  [fetchUserEntitlements.rejected]: (state, { error }) => ({
    ...state,
    entitlementsLoading: false,
    entitlementsError: error.message,
  }),

  [fetchUserFavorites.pending]: (state) => ({
    ...state,
    favoritesLoading: true,
  }),

  [fetchUserFavorites.fulfilled]: (state, { payload: { favorites } }) => ({
    ...state,
    favoritesLoading: false,
    favorites,
  }),

  [fetchUserFavorites.rejected]: (state, { error }) => ({
    ...state,
    favoritesLoading: false,
    favoritesError: error.message,
  }),

  [fetchUserSettings.pending]: (state) => ({
    ...state,
    settingsLoading: true,
  }),

  [fetchUserSettings.fulfilled]: (state, { payload: { settings } }) => ({
    ...state,
    settingsLoading: false,
    settings,
  }),

  [fetchUserSettings.rejected]: (state, { error }) => ({
    ...state,
    settingsLoading: false,
    settingsError: error.message,
  }),

  [fetchUserSubscriptionWarnings.fulfilled]: (state, { payload: { warnings } }) => ({
    ...state,
    warnings,
  }),

  [resendEmailVerification.pending]: (state) => ({
    ...state,
    resendEmailLoading: true,
  }),

  [resendEmailVerification.fulfilled]: (state, action) => ({
    ...state,
    resendEmailLoading: false,
    emailSent: action.payload,
  }),

  [resendEmailVerification.rejected]: (state, { error }) => ({
    ...state,
    resendEmailLoading: false,
    emailSent: false,
    resendEmailError: error.message,
  }),

  [resetEmailVerificationButton]: (state) => ({
    ...state,
    resendEmailLoading: false,
    emailSent: false,
  }),

  [favoriteSpot.pending]: (state) => ({
    ...state,
    favoriteSpotLoading: true,
  }),

  [favoriteSpot.fulfilled]: (state, { payload: { favorites } }) => ({
    ...state,
    favoriteSpotLoading: false,
    favorites,
  }),

  [favoriteSpot.rejected]: (state, { error }) => ({
    ...state,
    favoriteSpotLoading: false,
    favoriteSpotError: error.message,
  }),

  [unfavoriteSpot.pending]: (state) => ({
    ...state,
    favoriteSpotLoading: true,
  }),

  [unfavoriteSpot.fulfilled]: (state, { payload: { spotId } }) => ({
    ...state,
    favoriteSpotLoading: false,
    favorites: state.favorites.filter((favorite) => favorite?._id !== spotId),
  }),

  [unfavoriteSpot.rejected]: (state, { error }) => ({
    ...state,
    favoriteSpotLoading: false,
    favoriteSpotError: error.message,
  }),

  [setRecentlyVisited.fulfilled]: (state, { payload: { recentlyVisited } }) => ({
    ...state,
    settings: {
      ...state.settings,
      recentlyVisited,
    },
  }),
  [setMeteringEntitlement]: (state) => ({
    ...state,
    entitlements: [...state.entitlements, SL_METERED],
  }),
});

export default reducer;
