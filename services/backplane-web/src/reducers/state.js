import { createReducer } from '@reduxjs/toolkit';
import {
  createMeter,
  setMeterLoaded,
  getAnonymousMeter,
  getMeter,
  performedMeterModalRecompute,
  triggerMeterModalRecompute,
  setMeteringDisabled,
  setMeteringEnabled,
} from '../actions';

/**
 * @typedef {object} State
 * @property {boolean} meterLoaded
 * @property {boolean} shouldRecomputeMeterModal
 */

const whitelistedMeterLoadedActions = [createMeter, getMeter, getAnonymousMeter].map(
  ({ fulfilled }) => fulfilled?.toString(),
);

const initialState = {
  // TODO: SA-3317 remove this flag once rate-limiting is 100%
  meterLoaded: false,
  shouldRecomputeMeterModal: false,
  // TODO: SA-3317 remove this flag once rate-limiting is 100%
  meteringEnabled: false,
};

/**
 * @param {import('redux').Action<string>} action
 */
const isMeterLoadedAction = (action) =>
  whitelistedMeterLoadedActions.includes(action.type?.toString());

export default createReducer(initialState, (builder) => {
  builder.addCase(setMeterLoaded, (state, { payload }) => ({
    ...state,
    meterLoaded: payload ?? true,
  }));

  builder.addCase(triggerMeterModalRecompute, (state) => ({
    ...state,
    shouldRecomputeMeterModal: true,
  }));

  builder.addCase(performedMeterModalRecompute, (state) => ({
    ...state,
    shouldRecomputeMeterModal: false,
  }));

  // TODO: SA-3317 remove this flag once rate-limiting is 100%
  builder.addCase(setMeteringDisabled, (state) => ({
    ...state,
    meteringEnabled: false,
  }));
  // TODO: SA-3317 remove this flag once rate-limiting is 100%
  builder.addCase(setMeteringEnabled, (state) => ({
    ...state,
    meteringEnabled: true,
  }));

  builder.addMatcher(isMeterLoadedAction, (state) => ({
    ...state,
    meterLoaded: true,
  }));
});
