import { combineReducers } from 'redux';

import user from './user';
import metering from './metering';
import errors from './errors';
import state from './state';

export default combineReducers({
  user,
  meter: metering,
  errors,
  state,
});
