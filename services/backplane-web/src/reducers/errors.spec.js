import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import * as utils from '../utils';
import errors from './errors';

describe('reducers / metering', () => {
  const stateWithError = deepFreeze(['some_error']);

  let getCanUseDOMStub;

  beforeEach(() => {
    getCanUseDOMStub = sinon.stub(utils, 'getCanUseDOM');
  });

  afterEach(() => {
    getCanUseDOMStub.restore();
  });

  it('should initialize with default values', () => {
    expect(errors([], { type: 'unrelated_action' })).to.deep.equal([]);
  });

  it(`should not catch rejected actions on the client side`, () => {
    getCanUseDOMStub.returns(true);
    expect(errors([], { type: 'action/rejected' })).to.deep.equal([]);
  });

  it(`should  catch rejected actions on the server side`, () => {
    getCanUseDOMStub.returns(false);
    expect(errors([], { type: 'action/rejected', error: { message: 'some_error' } })).to.deep.equal(
      stateWithError,
    );
  });
});
