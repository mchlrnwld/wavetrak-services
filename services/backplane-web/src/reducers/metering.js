import { createReducer } from '@reduxjs/toolkit';
import { setMeterRemaining, performAnonymousMeteringSurfCheck } from '@surfline/web-common';
import {
  setMeter,
  getMeter,
  createMeter,
  getAnonymousMeter,
  performMeteringSurfCheck,
  clearMeter,
} from '../actions/metering';

const initialState = null;

export default createReducer(initialState, {
  [clearMeter]: () => null,
  [setMeter]: (state, { payload: meter }) => ({
    ...state,
    ...meter,
  }),
  [setMeterRemaining]: (state, action) => ({
    ...state,
    meterRemaining: action.payload,
  }),
  [getMeter.fulfilled]: (state, action) => {
    const { payload } = action;
    // If a meter was created, defer to `createMeter` action
    // to change the state
    if (payload?.createdNewMeter) {
      return state;
    }
    return {
      ...state,
      ...action.payload,
    };
  },
  [getMeter.rejected]: (state) => {
    // If a meter state already exists we should clear it
    // since this action is triggered by someone logging in
    if (state) {
      return initialState;
    }
    return state;
  },
  [getAnonymousMeter.fulfilled]: (state, action) => {
    const { payload } = action;
    if (!payload) {
      return null;
    }
    return {
      ...state,
      ...payload,
    };
  },
  [createMeter.fulfilled]: (state, action) => ({
    ...state,
    ...action.payload,
  }),
  [createMeter.rejected]: (state) => {
    // If a meter state already exists we should clear it
    // since this action is triggered by someone creating
    // an account
    if (state) {
      return initialState;
    }
    return state;
  },
  [performMeteringSurfCheck.fulfilled]: (state, action) => ({
    ...state,
    ...action.payload,
  }),
  [performAnonymousMeteringSurfCheck.fulfilled]: (state, action) => ({
    ...state,
    meterRemaining: action.payload,
  }),
});
