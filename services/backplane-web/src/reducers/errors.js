import { createReducer } from '@reduxjs/toolkit';
import { getCanUseDOM } from '../utils';

/** @type {string[]} */
const initialState = [];

function isRejectedServerSideAction(action) {
  if (!getCanUseDOM()) {
    return action.type.endsWith('/rejected');
  }
  return false;
}

export default createReducer(initialState, (builder) => {
  builder.addMatcher(isRejectedServerSideAction, (state, action) => {
    state.push(action.error?.message);
  });
});
