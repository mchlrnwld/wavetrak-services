import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import reducer from '../reducers';

/**
 * @typedef {ReturnType<createStore>} Store
 */

const createStore = (preloadedState) => {
  const store = configureStore({
    preloadedState,
    reducer: { backplane: reducer },
    devTools: process.env.APP_ENV !== 'production' && {
      name: `Surfline Backplane - ${process.env.APP_ENV}`,
    },
  });

  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      /* eslint-disable-next-line global-require */
      store.replaceReducer(combineReducers({ backplane: require('../reducers').default }));
    });
  }

  return store;
};

export default createStore;
