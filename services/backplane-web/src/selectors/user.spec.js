import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './user';

describe('selectors / user', () => {
  const user = deepFreeze({
    details: {},
    settings: {},
    entitlements: [],
    promotionEntitlements: [],
    favorites: [],
    detailsError: 'error',
    settingsError: 'error',
    favoritesError: 'error',
    entitlementsError: 'error',
  });

  const state = deepFreeze({
    backplane: {
      user,
    },
  });

  const emptyState = deepFreeze({
    backplane: {},
  });

  it('should get the details error state', () => {
    const error = selectors.getUserDetailsError(state);
    expect(error).to.deep.equal('error');
  });

  it('should get the settings error state', () => {
    const error = selectors.getUserSettingsError(state);
    expect(error).to.deep.equal('error');
  });

  it('should get the favorites error state', () => {
    const error = selectors.getUserFavoritesError(state);
    expect(error).to.deep.equal('error');
  });

  it('should get the entitlements error state', () => {
    const error = selectors.getUserEntitlementsError(state);
    expect(error).to.deep.equal('error');
  });

  it('should handle the empty state', () => {
    const error = selectors.getUserEntitlementsError(emptyState);
    expect(error).to.deep.equal(undefined);
  });
});
