import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './metering';

describe('selectors / metering', () => {
  const meter = deepFreeze({
    meterRemaining: 1,
    treatmentState: 'on',
    anonymousId: 'anonymousId',
  });

  const state = deepFreeze({
    backplane: {
      meter,
      state: {
        meteringEnabled: true,
      },
    },
  });

  const inactiveState = deepFreeze({
    backplane: {
      meter: {
        ...meter,
        treatmentState: 'off',
      },
    },
  });
  const disabledState = deepFreeze({
    backplane: {
      meter,
      state: {
        meteringEnabled: false,
      },
    },
  });

  it('should get the active meter state', () => {
    const meterState = selectors.getActiveMeter(state);
    expect(meterState).to.deep.equal(meter);
  });

  it('should not get the meter state if metering is disabled', () => {
    const meterState = selectors.getActiveMeter(disabledState);
    expect(meterState).to.deep.equal(null);
  });

  it('should not get the inactive meter state', () => {
    const meterState = selectors.getActiveMeter(inactiveState);
    expect(meterState).to.equal(null);
  });
});
