/**
 * @param {import('@surfline/web-common').BackplaneState} state
 */
export const getMeterModalRecomputeFlag = (state) =>
  state.backplane?.state?.shouldRecomputeMeterModal;

/**
 * @param {import('@surfline/web-common').BackplaneState} state
 */
export const getMeterLoaded = (state) => state.backplane?.state?.meterLoaded;

/**
 * @param {import('@surfline/web-common').BackplaneState} state
 */
export const getBackplaneAppState = (state) => state.backplane?.state;

export const getMeteringEnabled = (state) => state.backplane?.state?.meteringEnabled;
