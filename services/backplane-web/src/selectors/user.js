import { useSelector, shallowEqual } from 'react-redux';
import {
  getUserDetails,
  getUserFavorites,
  getUserSettings,
  getEntitlements,
  getUserSubscriptionWarnings,
  getUser,
  getPromotionEntitlements,
} from '@surfline/web-common';

const getUserError = (state, key) => state.backplane?.user?.[`${key}Error`];

/** Selectors */
export const getUserDetailsError = (state) => getUserError(state, 'details');
export const getUserEntitlementsError = (state) => getUserError(state, 'entitlements');
export const getUserFavoritesError = (state) => getUserError(state, 'favorites');
export const getUserSettingsError = (state) => getUserError(state, 'settings');

/** Selector Hooks */
export const usePromotionEntitlementsSelector = () =>
  useSelector(getPromotionEntitlements, shallowEqual);
export const useEntitlementsSelector = () => useSelector(getEntitlements, shallowEqual);
export const useUserDetailsSelector = () => useSelector(getUserDetails, shallowEqual);
export const useUserFavoritesSelector = () => useSelector(getUserFavorites, shallowEqual);
export const useUserSettingsSelector = () => useSelector(getUserSettings, shallowEqual);
export const useUserSelector = () => useSelector(getUser, shallowEqual);
export const useSubscriptionWarningsSelector = () =>
  useSelector(getUserSubscriptionWarnings, shallowEqual);
