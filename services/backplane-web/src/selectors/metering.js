import { useSelector, shallowEqual } from 'react-redux';
import { getMeter } from '@surfline/web-common';
import { getMeteringEnabled } from './state';

export const getActiveMeter = (state) => {
  const meteringEnabled = getMeteringEnabled(state);
  const meter = meteringEnabled ? getMeter(state) : null;
  if (meter?.treatmentState !== 'off') {
    return meter;
  }
  return null;
};

export const useMeterSelector = () => useSelector(getMeter, shallowEqual);
