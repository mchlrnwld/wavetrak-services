import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './state';

describe('selectors / state', () => {
  const state = deepFreeze({
    backplane: {
      state: {
        shouldRecomputeMeterModal: true,
        meterLoaded: true,
      },
    },
  });
  const emptyState = deepFreeze({
    backplane: {
      state: null,
    },
  });

  it('should get the meter modal recompute state', () => {
    const recompute = selectors.getMeterModalRecomputeFlag(state);
    expect(recompute).to.deep.equal(true);
  });

  it('should return undefined if the recompute meter modal state is missing', () => {
    const recompute = selectors.getMeterModalRecomputeFlag(emptyState);
    expect(recompute).to.equal(undefined);
  });

  it('should get the meter loaded state', () => {
    const loaded = selectors.getMeterLoaded(state);
    expect(loaded).to.deep.equal(true);
  });

  it('should return undefined if the meter loaded state is missing', () => {
    const loaded = selectors.getMeterLoaded(emptyState);
    expect(loaded).to.equal(undefined);
  });

  it('should get the app state', () => {
    const app = selectors.getBackplaneAppState(state);
    expect(app).to.deep.equal(state.backplane.state);
  });

  it('should return undefined if the meter loaded state is missing', () => {
    const app = selectors.getMeterLoaded({});
    expect(app).to.equal(undefined);
  });
});
