import { SL_METERED, SL_PREMIUM } from '@surfline/web-common';
import { setMeteringEnabled } from '../actions';
import { getAnonymousMeter, getMeter, createMeter } from '../actions/metering';
import { ON } from '../common/constants';

// TODO: SA-3317 - Refactor after rate limiting is fully on since
// We won't have to conditionally load meters, we will be able to
// do it fully server side

/**
 * @description On the server-side we want to pre-load existing
 * meters so that we can prevent the flash of content that happens
 * on the client if we still aren't aware of the user's rate limiting
 * status. This will only happen so long as rate limiting is behind a split.
 * Once we have removed the split we will not have a client-side coin toss
 * determine what content we need to show, and we will be able.
 *
 * The logic here is the following:
 *  - If the requester is a Premium user then do not proceed
 *  - If the requester is an existing user with the `sl_metered` entitlement, load their meter
 *  - If the requester is an existing user who do not have an existing meter, then create meter from the server
 *  - If the requester is an anonymous user with an existing anonymous meter, load their meter
 *  - If the requester is an anonymous user who do not have an existing meter, defer to the client-side to computer it
 * @param {object} props
 * @param {import('../stores').Store} props.store
 * @param {string?} props.accessToken
 * @param {string?} props.anonymousId
 * @param {string?} props.userId
 * @param {import('@surfline/web-common').UserState['entitlements']?} props.entitlements
 * @param {string?} props.countryCode
 * @param {object?} props.location
 * @param {string?} props.location.time_zone
 */
export const preloadMeters = async ({
  store,
  accessToken,
  anonymousId,
  userId,
  entitlements = [],
  countryCode,
  location,
}) => {
  const hasAnonymousId = !!anonymousId && !['null', 'undefined'].includes(anonymousId);
  const isPremium = entitlements?.includes(SL_PREMIUM);
  if (isPremium) return; // For Premium users don't proceed
  if (userId) {
    if (entitlements?.includes(SL_METERED)) {
      // TODO: SA-3317 remove once rate-limiting is 100%
      store.dispatch(setMeteringEnabled());
      await store.dispatch(
        getMeter({
          accessToken,
          createIfNotFound: false,
          anonymousId,
          performMeterSurfCheck: true,
        }),
      );
    } else {
      const { time_zone: timezone } = location || {};
      store.dispatch(setMeteringEnabled());
      await store.dispatch(
        createMeter({
          accessToken,
          anonymousId,
          countryCode,
          timezone,
          performMeterSurfCheck: true,
        }),
      );
    }
  } else if (hasAnonymousId) {
    const { payload: meter } = await store.dispatch(
      getAnonymousMeter({
        anonymousId,
        performMeterSurfCheck: true,
        treatmentState: ON,
      }),
    );
    if (meter) {
      // TODO: SA-3317 remove once rate-limiting is 100%
      store.dispatch(setMeteringEnabled());
    }
  }
};
