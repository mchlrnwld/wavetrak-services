/* eslint-disable import/no-mutable-exports */
import Redis from 'ioredis';

/** @type {Redis.Redis} */
export let redis;

export const initRedis = async () => {
  const client = new Redis(process.env.REDIS_URL, { lazyConnect: true });
  await client.connect();
  redis = client;
};
