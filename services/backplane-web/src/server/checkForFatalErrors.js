import newrelic from 'newrelic';

/**
 * @param {Record<string, string>} fetchErrors
 * @param {boolean | string} fatalError
 */
const checkForFatalError = (fetchErrors, fatalError = false) => {
  if (fetchErrors) {
    newrelic.addCustomAttributes(fetchErrors);
  }

  if (fatalError) {
    newrelic.noticeError(fatalError);
    throw new Error('Fatal server-side fetch error.');
  }
};

export default checkForFatalError;
