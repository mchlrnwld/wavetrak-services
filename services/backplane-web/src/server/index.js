/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-console */
import newrelic from 'newrelic';
import React from 'react';
import path from 'path';
import express from 'express';
import cors from 'cors';
import { json } from 'body-parser';
import cookieParser from 'cookie-parser';
import cookiesMiddleware from 'universal-cookie-express';
import { CookiesProvider } from 'react-cookie';
import compression from 'compression';
import { Provider } from 'react-redux';
import { authenticateAccessToken, getUser } from '@surfline/web-common';
import { renderToString } from 'react-dom/server';
import { locationDetails } from '../common/helpers';
import logger from '../common/logger';
import template from './template';
import heroAd from './heroAd';
import { initRedis } from './redis';
import config from '../config';
import createStore from '../stores';
import bundleConfig from '../bundles/config';
import { setupUser, setUserMetadata } from '../actions/user';
import { heroAdConfigMap } from '../bundles/margins/Header/heroAdConfig';
import getHeroAdTreatment from './heroAd/getHeroAdTreatment';
import { preloadMeters } from './metering';
import checkForFatalErrors from './checkForFatalErrors';

const clientAssets = require(process.env.ASSETS_MANIFEST);
const vendorAssets = require(process.env.VENDOR_MANIFEST);

const app = express();
const log = logger('web-backplane');

const serverPort = process.env.SERVER_PORT ? parseInt(process.env.SERVER_PORT, 10) : 8080;

app.enable('trust proxy');
app.disable('x-powered-by');
app.use(compression());
app.use(cookieParser());
app.use(cookiesMiddleware());

app.get('/health', (req, res) =>
  res.send({
    status: 200,
    message: 'OK',
    version: process.env.APP_VERSION || 'unknown',
  }),
);

app.post('/heroad', cors(), json(), heroAd());
app.use('/backplane/static', express.static(path.join(process.cwd(), './build/public')));

app.use((_, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Credentials',
  );
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT');
  next();
});

app.get('/render.:format', async (req, res) => {
  const startTime = process.hrtime();
  try {
    const bundle = bundleConfig[req.query.bundle];
    const apiVersion = req.query.apiVersion || 'v1';
    const skipFetch = !!req.query.skipFetch;

    if (!bundle) {
      return res.status(400).send({ message: 'Not a valid bundle' });
    }

    const { ip, countryCode, location, subdivisionCode } = await locationDetails(req, log);

    const { app: appName, anonymousId } = req.query;
    let { userId } = req.query;

    let accessToken = req.query.accessToken || req.headers['x-auth-accesstoken'];

    // Handle accessToken string edge cases
    if (accessToken === 'null' || accessToken === 'undefined') accessToken = null;

    // If the request has not been authenticated yet, we need to authenticate the token and retrieve
    // the userId
    if (accessToken && !userId) {
      try {
        /** @type {{id: string, scopes: string[]}} */
        const authenticated = await authenticateAccessToken(config.servicesURL, accessToken);

        if (authenticated) {
          userId = authenticated.id;
        }
      } catch (err) {
        log.debug(err);
      }
    }

    newrelic.addCustomAttributes({
      userId,
      countryCode,
      ip,
      location,
    });

    // Create the redux store
    const store = createStore();

    if (!skipFetch) {
      await store.dispatch(
        setupUser({
          userId,
          countryIso: countryCode,
          clientIp: ip,
          location,
          serverSide: true,
        }),
      );
    }

    const {
      settings,
      favorites,
      entitlements,
      promotionEntitlements,
      hasNewAccount,
      legacyEntitlements,
      details,
      warnings,
      detailsError,
      entitlementsError,
      favoritesError,
      settingsError,
    } = getUser(store.getState());

    const fatalError = !!settingsError;

    checkForFatalErrors(
      { detailsError, entitlementsError, favoritesError, settingsError },
      fatalError,
    );

    const userEntitlements = {
      entitlements,
      promotions: promotionEntitlements,
      hasNewAccount,
      legacyEntitlements,
    };

    const rateLimitingEnabled = appName === 'kbyg';
    if (rateLimitingEnabled) {
      await preloadMeters({
        store,
        accessToken,
        anonymousId,
        userId,
        entitlements,
        countryCode,
        location,
      });
    }

    let renderOptions = req.query.renderOptions || {};

    let heroAdPlaceholder;
    if (renderOptions.bannerAd !== 'false' && heroAdConfigMap[renderOptions.bannerAd]) {
      heroAdPlaceholder = await getHeroAdTreatment();
    }

    renderOptions = {
      ...renderOptions,
      rateLimitingEnabled,
      heroAdPlaceholder,
    };

    store.dispatch(setUserMetadata({ ip, countryCode, location, subdivisionCode }));

    // We want the API to be backwards compatible and return the least amount of data
    // needed in order to keep the response times low. To do this we exclude the user
    // info for API Version "v2"
    const api = {
      ...(apiVersion !== 'v2' && { details }),
      ...(apiVersion !== 'v2' && { entitlements: userEntitlements }),
      ...(apiVersion !== 'v2' && { settings }),
      ...(apiVersion !== 'v2' && { favorites: { data: { favorites } } }),
      ...(apiVersion !== 'v2' && { warnings }),
      // TODO: Verify that all server-side front-ends are on v2 and remove all `apiVersion` code
      geo: {
        ip,
        countryCode,
        location,
      },
      apiVersion,
    };

    const apiResponseTime = process.hrtime(startTime)[1] / 1000000;

    // TODO inline css, https://gist.github.com/drew/4f04d7ce501f42d3ff00ebb68a4c1b31

    const props = { ...api, renderOptions };
    const components = Object.entries(bundle.components).reduce(
      (acc, [name, Component]) => ({
        ...acc,
        [name]: renderToString(
          <CookiesProvider cookies={req.universalCookies}>
            <Provider store={store}>
              <Component {...props} />
            </Provider>
          </CookiesProvider>,
          null,
        ),
      }),
      {},
    );

    const renderResponseTime = process.hrtime(startTime)[1] / 1000000;
    const vendorVersion = 'React';
    const apiResponse = {
      associated: {
        timing: {
          api: apiResponseTime,
          render: renderResponseTime - apiResponseTime,
          total: process.hrtime(startTime)[1] / 1000000,
        },
        vendor: {
          js: vendorAssets[vendorVersion],
        },
      },
      data: {
        components,
        js: clientAssets[`${req.query.bundle}.js`],
        css: clientAssets[`${req.query.bundle}.css`],
        api: { ...api, renderOptions },
        redux: store.getState(),
      },
    };

    switch (req.params.format) {
      case 'html':
        return res.send(
          template({
            bundle: req.query.bundle,
            backplane: apiResponse,
            config,
          }),
        );
      case 'json':
        return res.send(apiResponse);
      default:
        return res.status(400).send(400);
    }
  } catch (err) {
    log.error({
      message: 'Error returned from backplane /render',
      err,
    });
    newrelic.noticeError(err);
    return res.status(500).send({ message: 'Something went wrong' });
  }
});

initRedis().then(() => {
  app.listen(serverPort, () => {
    log.info(`Express Server listening on ${serverPort}`);
  });
});
