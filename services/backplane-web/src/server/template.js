import { googletagservices, snippet as segment } from '@surfline/web-common';
import config from '../config';
/* eslint-disable prefer-template, max-len */

export default ({ backplane, bundle }) => `

<!doctype html>
<html lang="en-US">

  <head>
    <meta charset='utf-8' />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    ${segment(config.appKeys.segment)}
    ${googletagservices()}

    <!-- Begin HTL includes -->
    <link rel="stylesheet" type="text/css" href="${config.htl.cssUrl}" />
    <script src="${config.htl.scriptUrl}" />
    <script>
      window.htlbid = window.htlbid || {cmd: []};
    </script>
    <!-- End HTL includes -->

    ${
      backplane.data.css
        ? '<link rel="stylesheet" type="text/css" href="' + backplane.data.css + '">'
        : ''
    }
  </head>

  <style>
    body { padding: 0; margin: 0; }
  </style>

  <body ${
    backplane?.data?.redux?.backplane?.user?.entitlements.includes('sl_premium')
      ? 'class="htl-usertype-premium"'
      : ''
  }>

    <h2>Render for Bundle: <code>${bundle}</code></h2>
    ${Object.entries(backplane.data.components)
      .map(
        ([comp, html]) =>
          `<h3 style="font-family: courier">${comp}</h3><div id="backplane-${comp}">${html}</div>`,
      )
      .reduce((acc, comp) => acc + comp, '')}

    <h2>Data for Bundle</h2>
    <pre style="font-size: 12px; line-height: 1em; font-weight: normal;">
      ${JSON.stringify(backplane.data.api, null, 4)}
      ${JSON.stringify(backplane.data.redux, null, 2)}
    </pre>

    <script>
      window.__BACKPLANE_API__ = ${JSON.stringify(backplane.data.api)}
      window.__BACKPLANE_REDUX__ = ${JSON.stringify(backplane.data.redux)}
    </script>
    <script src="${backplane.associated.vendor.js}"></script>
    <script src="${backplane.data.js}"></script>

  </body>

</html>

`;
