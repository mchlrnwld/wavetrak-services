import newrelic from 'newrelic';
import trackHeroAdResult from './trackHeroAdResult';
import removeOldTrackResults from './removeOldTrackResults';
import { redis } from '../redis';

/**
 * @typedef {object} RequestBody
 * @property {"success" | "failure"} result
 * @property {"on" | "off"} treatment
 * @property {number | string} width
 * @property {number | string} height
 */

const heroAd =
  () =>
  /**
   * @param {import('express').Request<Record<string, any>, { message: string }, RequestBody>} req
   * @param {import('express').Response<{message: string}>} res
   */
  async (req, res) => {
    try {
      const { result, treatment, width, height } = req.body;

      if (!result) {
        return res.status(400).send({
          message: 'Bad request',
        });
      }

      const { previousMinute } = await trackHeroAdResult(redis, treatment, result, width, height);

      res.on('finish', async () => {
        try {
          await removeOldTrackResults(redis, previousMinute);
        } catch (err) {
          newrelic.noticeError(err);
        }
      });

      return res.send({ message: 'OK' });
    } catch (err) {
      newrelic.noticeError(err);
      return res.status(500).send({ message: 'Internal server error' });
    }
  };

export default heroAd;
