import { HERO_AD_WIDTH_HEIGHT } from './constants';

/**
 * @param {import('ioredis').Redis} redis
 * @param {number | string} width
 * @param {number | string} height
 */
const trackWidthAndHeight = async (redis, width, height) => {
  if (!width || !height) return;

  const currentWidth = await redis.zscore(HERO_AD_WIDTH_HEIGHT, height);
  if (!currentWidth || width < currentWidth) {
    await redis.zadd(HERO_AD_WIDTH_HEIGHT, width, height);
  }
};

export default trackWidthAndHeight;
