import sinon from 'sinon';
import removeOldTrackResultsSpec from './removeOldTrackResults.spec';
import trackHeroAdResultSpec from './trackHeroAdResult.spec';
import trackWidthAndHeightSpec from './trackWidthAndHeight.spec';

describe('/heroad', () => {
  let clock;
  const now = 1527112468; // 05/23/2018@21:54:28 UTC
  const lastMinute = 1527112440; // 05/23/2018@21:54:00 UTC
  const previousMinute = 1527112380; // 05/23/2018@21:53:00 UTC

  before(() => {
    clock = sinon.useFakeTimers(now * 1000);
  });

  after(() => {
    clock.restore();
  });

  removeOldTrackResultsSpec(lastMinute, previousMinute);
  trackHeroAdResultSpec(lastMinute, previousMinute);
  trackWidthAndHeightSpec();
});
