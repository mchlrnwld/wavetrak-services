export const HERO_AD_USERS_KEY = 'backplane:hero-ad-users';
export const HERO_AD_SUCCESSES_KEY = 'backplane:hero-ad-successes';
export const HERO_AD_WIDTH_HEIGHT = 'backplane:hero-ad-width-height';
