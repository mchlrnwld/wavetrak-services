/* istanbul ignore next */
import { HERO_AD_PLACEHOLDER } from '../../common/constants';
import { redis } from '../redis';

const getHeroAdTreatment = () => redis.get(HERO_AD_PLACEHOLDER);

export default getHeroAdTreatment;
