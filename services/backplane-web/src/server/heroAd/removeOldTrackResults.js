import { HERO_AD_USERS_KEY, HERO_AD_SUCCESSES_KEY } from './constants';

/**
 * @param {import('ioredis').Redis} redis
 * @param {number} previousMinute
 */
const removeOldTrackResults = async (redis, previousMinute) => {
  const [users, successes] = await Promise.all([
    redis.hgetall(HERO_AD_USERS_KEY),
    redis.hgetall(HERO_AD_SUCCESSES_KEY),
  ]);

  const getHashesToDelete = (object) =>
    Object.keys(object || {}).filter((key) => parseInt(key, 10) < previousMinute);
  const usersHashesToDelete = getHashesToDelete(users);
  const successesHashesToDelete = getHashesToDelete(successes);

  await Promise.all([
    usersHashesToDelete.length > 0 ? redis.hdel(HERO_AD_USERS_KEY, ...usersHashesToDelete) : null,
    successesHashesToDelete.length > 0
      ? redis.hdel(HERO_AD_SUCCESSES_KEY, ...successesHashesToDelete)
      : null,
  ]);
};

export default removeOldTrackResults;
