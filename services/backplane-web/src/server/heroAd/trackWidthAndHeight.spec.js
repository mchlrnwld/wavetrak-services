import { expect } from 'chai';
import sinon from 'sinon';
import { HERO_AD_WIDTH_HEIGHT } from './constants';
import trackWidthAndHeight from './trackWidthAndHeight';

const trackWidthAndHeightSpec = () => {
  describe('trackWidthAndHeight', () => {
    const redisClient = {
      zadd: sinon.stub(),
      zscore: sinon.stub(),
    };

    beforeEach(() => {
      redisClient.zadd.reset();
      redisClient.zscore.reset();
    });

    it('adds width for height if one is not already stored', async () => {
      await trackWidthAndHeight(redisClient, 968, 300);

      expect(redisClient.zscore).to.have.been.calledOnce();
      expect(redisClient.zscore).to.have.been.calledWithExactly(HERO_AD_WIDTH_HEIGHT, 300);
      expect(redisClient.zadd).to.have.been.calledOnce();
      expect(redisClient.zadd).to.have.been.calledWithExactly(HERO_AD_WIDTH_HEIGHT, 968, 300);
    });

    it('updates width for height if the provided width is less than the currently stored width for the height', async () => {
      redisClient.zscore.resolves(1024);

      await trackWidthAndHeight(redisClient, 968, 300);

      expect(redisClient.zscore).to.have.been.calledOnce();
      expect(redisClient.zscore).to.have.been.calledWithExactly(HERO_AD_WIDTH_HEIGHT, 300);
      expect(redisClient.zadd).to.have.been.calledOnce();
      expect(redisClient.zadd).to.have.been.calledWithExactly(HERO_AD_WIDTH_HEIGHT, 968, 300);
    });

    it('does nothing if the provided width is not less than the currently stored width for the height', async () => {
      redisClient.zscore.resolves(768);

      await trackWidthAndHeight(redisClient, 968, 300);

      expect(redisClient.zscore).to.have.been.calledOnce();
      expect(redisClient.zscore).to.have.been.calledWithExactly(HERO_AD_WIDTH_HEIGHT, 300);
      expect(redisClient.zadd).not.to.have.been.called();
    });

    it('does nothing if width is not provided', async () => {
      await trackWidthAndHeight(redisClient, null, 300);

      expect(redisClient.zscore).not.to.have.been.called();
      expect(redisClient.zadd).not.to.have.been.called();
    });

    it('does nothing if width is not provided', async () => {
      await trackWidthAndHeight(redisClient, 968, null);

      expect(redisClient.zscore).not.to.have.been.called();
      expect(redisClient.zadd).not.to.have.been.called();
    });
  });
};

export default trackWidthAndHeightSpec;
