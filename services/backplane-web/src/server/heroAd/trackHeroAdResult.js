import { HERO_AD_PLACEHOLDER } from '../../common/constants';
import { HERO_AD_USERS_KEY, HERO_AD_SUCCESSES_KEY, HERO_AD_WIDTH_HEIGHT } from './constants';
import trackWidthAndHeight from './trackWidthAndHeight';

/**
 * @param {import('ioredis').Redis} redis
 * @param {number} previousMinute
 */
const getPercentSuccessRate = async (redis, previousMinute) => {
  const [previousUsers, previousSuccesses] = await Promise.all([
    redis.hget(HERO_AD_USERS_KEY, previousMinute),
    redis.hget(HERO_AD_SUCCESSES_KEY, previousMinute),
  ]);

  const percentSuccess = 100 * (previousSuccesses / previousUsers);
  return percentSuccess;
};
/**
 * @param {import('ioredis').Redis} redis
 * @param {"on" | "off"} placeholder
 * @param {"failure" | "success"} result
 * @param {number | string} width
 * @param {number | string} height
 */
const trackHeroAdResult = async (redis, placeholder, result, width, height) => {
  const lastMinute = Math.floor(new Date() / 1000 / 60) * 60;
  const previousMinute = lastMinute - 60;
  let turnedOffPlaceholder = false;

  if (placeholder === 'on') {
    // Handle the case where the placeholder is on and the ad failed to load
    if (result === 'failure') {
      const percentSuccess = await getPercentSuccessRate(redis, previousMinute);
      if (Math.round(percentSuccess) === 0) {
        await redis.set(HERO_AD_PLACEHOLDER, 'off');
        await redis.del(HERO_AD_USERS_KEY, HERO_AD_SUCCESSES_KEY, HERO_AD_WIDTH_HEIGHT);
        turnedOffPlaceholder = true;
      }
    }

    if (!turnedOffPlaceholder) {
      // Increment total hits and successes keys (if successful)
      await Promise.all([
        redis.hincrby(HERO_AD_USERS_KEY, lastMinute, 1),
        result === 'success' ? redis.hincrby(HERO_AD_SUCCESSES_KEY, lastMinute, 1) : null,
        trackWidthAndHeight(redis, width, height),
      ]);
    }
  } else if (result === 'success' && placeholder === 'off') {
    await Promise.all([
      trackWidthAndHeight(redis, width, height),
      redis.set(HERO_AD_PLACEHOLDER, 'on'),
    ]);
  }

  return { previousMinute };
};

export default trackHeroAdResult;
