import { expect } from 'chai';
import sinon from 'sinon';
import * as trackWidthAndHeight from './trackWidthAndHeight';
import { HERO_AD_USERS_KEY, HERO_AD_SUCCESSES_KEY, HERO_AD_WIDTH_HEIGHT } from './constants';
import trackHeroAdResult from './trackHeroAdResult';
import { HERO_AD_PLACEHOLDER } from '../../common/constants';

const trackHeroAdResultSpec = (lastMinute, previousMinute) => {
  describe('trackHeroAdResult', () => {
    const width = 968;
    const height = 300;
    const redisClient = {
      del: sinon.stub(),
      hget: sinon.stub(),
      hincrby: sinon.stub(),
      set: sinon.stub(),
    };

    beforeEach(() => {
      sinon.stub(trackWidthAndHeight, 'default');
      redisClient.del.reset();
      redisClient.hget.reset();
      redisClient.hincrby.reset();
      redisClient.set.reset();
    });

    afterEach(() => {
      trackWidthAndHeight.default.restore();
    });

    it('does nothing if failure and treatment is off', async () => {
      const result = await trackHeroAdResult(redisClient, 'off', 'failure', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(trackWidthAndHeight.default).not.to.have.been.called();
      expect(redisClient.del).not.to.have.been.called();
      expect(redisClient.hget).not.to.have.been.called();
      expect(redisClient.hincrby).not.to.have.been.called();
      expect(redisClient.set).not.to.have.been.called();
    });

    it('turns treatment on if success and treatment is off', async () => {
      const result = await trackHeroAdResult(redisClient, 'off', 'success', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(trackWidthAndHeight.default).to.have.been.calledOnce();
      expect(trackWidthAndHeight.default.firstCall.args.slice(1)).to.deep.equal([width, height]);

      expect(redisClient.set).to.have.been.calledOnceWithExactly(HERO_AD_PLACEHOLDER, 'on');

      expect(redisClient.del).not.to.have.been.called();
      expect(redisClient.hget).not.to.have.been.called();
      expect(redisClient.hincrby).not.to.have.been.called();
    });

    it('increments user only if failure and treatment is off but no users tracked in previous minute', async () => {
      const result = await trackHeroAdResult(redisClient, 'on', 'failure', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(redisClient.hget).to.have.been.calledTwice();
      expect(redisClient.hget).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, previousMinute);
      expect(redisClient.hget).to.have.been.calledWithExactly(
        HERO_AD_SUCCESSES_KEY,
        previousMinute,
      );
      expect(redisClient.hincrby).to.have.been.calledOnce();
      expect(redisClient.hincrby).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, lastMinute, 1);
      expect(trackWidthAndHeight.default).to.have.been.calledOnce();
      expect(trackWidthAndHeight.default.firstCall.args.slice(1)).to.deep.equal([width, height]);

      expect(redisClient.set).to.not.have.been.called();
      expect(redisClient.del).not.to.have.been.called();
    });

    it('increments user only if failure and treatment is on', async () => {
      const result = await trackHeroAdResult(redisClient, 'on', 'failure', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(redisClient.hget).to.have.been.calledTwice();
      expect(redisClient.hget).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, previousMinute);
      expect(redisClient.hget).to.have.been.calledWithExactly(
        HERO_AD_SUCCESSES_KEY,
        previousMinute,
      );
      expect(redisClient.hincrby).to.have.been.calledOnce();
      expect(redisClient.hincrby).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, lastMinute, 1);
      expect(trackWidthAndHeight.default).to.have.been.calledOnce();
      expect(trackWidthAndHeight.default.firstCall.args.slice(1)).to.deep.equal([width, height]);

      expect(redisClient.set).to.not.have.been.called();
      expect(redisClient.del).not.to.have.been.called();
    });

    it('increments user and successes if success and treatment is on', async () => {
      const result = await trackHeroAdResult(redisClient, 'on', 'success', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(redisClient.hincrby).to.have.been.calledTwice();
      expect(redisClient.hincrby).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, lastMinute, 1);
      expect(redisClient.hincrby).to.have.been.calledWithExactly(
        HERO_AD_SUCCESSES_KEY,
        lastMinute,
        1,
      );
      expect(trackWidthAndHeight.default).to.have.been.calledOnce();
      expect(trackWidthAndHeight.default.firstCall.args.slice(1)).to.deep.equal([width, height]);

      expect(redisClient.set).to.not.have.been.called();
      expect(redisClient.hget).not.to.have.been.called();
      expect(redisClient.del).not.to.have.been.called();
    });

    it('turns treatment off if failure, treatment is on, and previous minute results are 100% failures', async () => {
      redisClient.hget.withArgs(HERO_AD_USERS_KEY, previousMinute).resolves(100);
      redisClient.hget.withArgs(HERO_AD_SUCCESSES_KEY, previousMinute).resolves(null);

      const result = await trackHeroAdResult(redisClient, 'on', 'failure', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(redisClient.hget).to.have.been.calledTwice();
      expect(redisClient.hget).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, previousMinute);
      expect(redisClient.hget).to.have.been.calledWithExactly(
        HERO_AD_SUCCESSES_KEY,
        previousMinute,
      );
      expect(redisClient.del).to.have.been.calledOnce();
      expect(redisClient.del).to.have.been.calledWithExactly(
        HERO_AD_USERS_KEY,
        HERO_AD_SUCCESSES_KEY,
        HERO_AD_WIDTH_HEIGHT,
      );

      expect(trackWidthAndHeight.default).not.to.have.been.called();
      expect(redisClient.set).to.have.been.calledOnceWithExactly(HERO_AD_PLACEHOLDER, 'off');
      expect(redisClient.hincrby).not.to.have.been.called();
    });

    it('turns treatment off if failure, treatment is on, and previous minute results are near 100% failures', async () => {
      redisClient.hget.withArgs(HERO_AD_USERS_KEY, previousMinute).resolves(1000);
      redisClient.hget.withArgs(HERO_AD_SUCCESSES_KEY, previousMinute).resolves(1);

      const result = await trackHeroAdResult(redisClient, 'on', 'failure', width, height);

      expect(result.previousMinute).to.equal(previousMinute);

      expect(redisClient.hget).to.have.been.calledTwice();
      expect(redisClient.hget).to.have.been.calledWithExactly(HERO_AD_USERS_KEY, previousMinute);
      expect(redisClient.hget).to.have.been.calledWithExactly(
        HERO_AD_SUCCESSES_KEY,
        previousMinute,
      );
      expect(redisClient.del).to.have.been.calledOnce();
      expect(redisClient.del).to.have.been.calledWithExactly(
        HERO_AD_USERS_KEY,
        HERO_AD_SUCCESSES_KEY,
        HERO_AD_WIDTH_HEIGHT,
      );
      expect(redisClient.set).to.have.been.calledOnceWithExactly(HERO_AD_PLACEHOLDER, 'off');

      expect(trackWidthAndHeight.default).not.to.have.been.called();
      expect(redisClient.hincrby).not.to.have.been.called();
    });
  });
};

export default trackHeroAdResultSpec;
