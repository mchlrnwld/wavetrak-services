import { expect } from 'chai';
import sinon from 'sinon';
import { HERO_AD_USERS_KEY, HERO_AD_SUCCESSES_KEY } from './constants';
import removeOldTrackResults from './removeOldTrackResults';

const removeOldTrackResultsSpec = (lastMinute, previousMinute) => {
  describe('removeOldTrackResults', () => {
    const redisClient = {
      hdel: sinon.stub(),
      hgetall: sinon.stub(),
    };

    beforeEach(() => {
      redisClient.hdel.reset();
      redisClient.hgetall.reset();
    });

    it('does nothing if keys are not found', async () => {
      await removeOldTrackResults(redisClient, previousMinute);

      expect(redisClient.hgetall).to.have.been.calledTwice();
      expect(redisClient.hgetall).to.have.been.calledWithExactly(HERO_AD_USERS_KEY);
      expect(redisClient.hgetall).to.have.been.calledWithExactly(HERO_AD_SUCCESSES_KEY);

      expect(redisClient.hdel).not.to.have.been.called();
    });

    it('does nothing if keys are found without old hashes', async () => {
      redisClient.hgetall.withArgs(HERO_AD_USERS_KEY).resolves({
        [lastMinute]: 1,
        [previousMinute]: 2,
      });
      redisClient.hgetall.withArgs(HERO_AD_SUCCESSES_KEY).resolves({
        [lastMinute]: 1,
        [previousMinute]: 2,
      });

      await removeOldTrackResults(redisClient, previousMinute);

      expect(redisClient.hgetall).to.have.been.calledTwice();
      expect(redisClient.hgetall).to.have.been.calledWithExactly(HERO_AD_USERS_KEY);
      expect(redisClient.hgetall).to.have.been.calledWithExactly(HERO_AD_SUCCESSES_KEY);

      expect(redisClient.hdel).not.to.have.been.called();
    });

    it('deletes keys older than previous minute', async () => {
      redisClient.hgetall.withArgs(HERO_AD_USERS_KEY).resolves({
        [lastMinute]: 1,
        [previousMinute]: 2,
        [previousMinute - 60]: 3,
      });
      redisClient.hgetall.withArgs(HERO_AD_SUCCESSES_KEY).resolves({
        [lastMinute]: 1,
        [previousMinute]: 2,
        [previousMinute - 60]: 3,
      });

      await removeOldTrackResults(redisClient, previousMinute);

      expect(redisClient.hgetall).to.have.been.calledTwice();
      expect(redisClient.hgetall).to.have.been.calledWithExactly(HERO_AD_USERS_KEY);
      expect(redisClient.hgetall).to.have.been.calledWithExactly(HERO_AD_SUCCESSES_KEY);
      expect(redisClient.hdel).to.have.been.calledTwice();
      expect(redisClient.hdel).to.have.been.calledWithExactly(
        HERO_AD_USERS_KEY,
        `${previousMinute - 60}`,
      );
      expect(redisClient.hdel).to.have.been.calledWithExactly(
        HERO_AD_SUCCESSES_KEY,
        `${previousMinute - 60}`,
      );
    });
  });
};

export default removeOldTrackResultsSpec;
