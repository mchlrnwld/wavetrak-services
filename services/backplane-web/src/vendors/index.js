/* eslint-disable camelcase */

import fs from 'fs';
import path from 'path';

const isDirectory = ({ source }) => fs.lstatSync(source).isDirectory();

const getDirectories = (source) =>
  fs
    .readdirSync(source)
    .map((name) => ({
      name,
      source: path.join(source, name),
    }))
    .filter(isDirectory);

export default () => {
  const vendors = getDirectories(path.join(process.cwd(), '/src/vendors'));

  return vendors;
};
