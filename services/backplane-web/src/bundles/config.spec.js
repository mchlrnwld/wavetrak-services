import { expect } from 'chai';
import config from './config';

describe('bundle config', () => {
  it('should export a valid config', () => {
    expect(config.margins).to.haveOwnProperty('entryPoint');
    expect(config.margins.entryPoint).to.be.equal('margins/entry.js');
    expect(config.margins).to.haveOwnProperty('components');
    expect(config.margins.components).to.be.instanceOf(Object);
  });
});
