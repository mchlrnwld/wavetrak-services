const usertype = (userDetails, userEntitlements) => {
  if (!userDetails) return 'ANONYMOUS';
  if (userEntitlements.includes('sl_vip_advertiser')) return 'VIP_ADVERTISER';
  if (userEntitlements.includes('sl_vip')) return 'VIP';
  if (userEntitlements.includes('sl_premium')) return 'PREMIUM';
  return 'REGISTERED';
};

const getAdTargets = (qaFlag, user, entitlements, contentKeyword) => {
  const adTargets = [
    ['position', 'SPONSORSHIP'],
    ['qaFlag', qaFlag],
    ['usertype', usertype(user, entitlements)],
  ];
  if (contentKeyword) adTargets.push(['contentKeyword', contentKeyword]);
  return adTargets;
};

const defaultAdSizeMappings = [
  [
    [1200, 0],
    ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]],
  ],
  [
    [992, 0],
    ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [750, 250], [728, 90]],
  ],
  [
    [768, 0],
    ['fluid', [750, 250], [750, 160], [728, 90]],
  ],
  [
    [320, 0],
    ['fluid', [320, 80], [320, 50]],
  ],
  [
    [0, 0],
    ['fluid', [320, 50]],
  ],
];

const editorialHeroAdSizes = {
  adUnit: 'Horizontal_Variable',
  adId: 'Horizontal_Variable',
  adSizeMappings: [
    [
      [1200, 0],
      ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]],
    ],
    [
      [992, 0],
      ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]],
    ],
    [
      [320, 0],
      ['fluid', [320, 80], [320, 50]],
    ],
    [
      [0, 0],
      ['fluid', [320, 50]],
    ],
  ],
};

export const heroAdConfigMap = {
  homepage: {
    adUnit: 'HP_Horizontal_Variable',
    adId: 'HP_Horizontal_Variable',
    adViewType: 'HOMEPAGE',
    adSizeMappings: defaultAdSizeMappings,
  },
  contest_sos: {
    adViewType: 'CONTEST',
    adUnit: 'SL_Cuervo_Horizontal_Variable',
    adId: 'SL_Cuervo_Horizontal_Variable',
    adSizeMappings: [
      [[1200, 0], ['fluid']],
      [[992, 0], ['fluid']],
      [[320, 0], ['fluid']],
      [[0, 0], ['fluid']],
    ],
  },
  wotw_2021: {
    adViewType: 'CONTEST',
    adUnit: 'SL_WOTW_Horizontal_Variable_21',
    adId: 'SL_WOTW_Horizontal_Variable_21',
    adSizeMappings: [
      [[1200, 0], ['fluid']],
      [[992, 0], ['fluid']],
      [[320, 0], ['fluid']],
      [[0, 0], ['fluid']],
    ],
  },
  wotw_regional_2021: {
    adViewType: 'CONTEST',
    adUnit: 'SL_WOTW_Regional_Horizontal_Variable_21',
    adId: 'SL_WOTW_Regional_Horizontal_Variable_21',
    adSizeMappings: [
      [[1200, 0], ['fluid']],
      [[992, 0], ['fluid']],
      [[320, 0], ['fluid']],
      [[0, 0], ['fluid']],
    ],
  },
  editorial_article: {
    adViewType: 'CONTENT_ARTICLE',
    ...editorialHeroAdSizes,
  },
  editorial_home: {
    adViewType: 'CONTENT_HOME',
    ...editorialHeroAdSizes,
  },
  editorial_category: {
    adViewType: 'CONTENT_CATEGORY',
    ...editorialHeroAdSizes,
  },
  editorial_tag: {
    adViewType: 'CONTENT_TAG',
    ...editorialHeroAdSizes,
  },
  editorial_series: {
    adViewType: 'CONTENT_SERIES',
    ...editorialHeroAdSizes,
  },
  forecast_article: {
    adUnit: 'SL_Forecast_ExpertContent_HorizontalVariable',
    adId: 'SL_Forecast_ExpertContent_HorizontalVariable',
    adViewType: 'FORECAST_ARTICLE',
    adSizeMappings: defaultAdSizeMappings,
  },
  search_results: {
    adUnit: 'SL_SearchResults',
    adId: 'SL_SearchResults',
    adViewType: 'SEARCH_RESULTS',
    adSizeMappings: defaultAdSizeMappings,
  },
  sessions_clips: {
    adUnit: 'SL_Sessions_Horizontal_Variable',
    adId: 'SL_Sessions_Horizontal_Variable',
    adViewType: 'SESSIONS_CLIPS',
    adSizeMappings: defaultAdSizeMappings,
  },
};

const heroAdConfig = (adType, user, entitlements, qaFlag, contentKeyword) => ({
  adUnit: `/1024858/${heroAdConfigMap[adType].adUnit}`,
  adId: heroAdConfigMap[adType].adId,
  adSizes: [
    'fluid',
    [1170, 250],
    [1170, 160],
    [990, 90],
    [970, 250],
    [320, 50],
    [970, 90],
    [970, 160],
    [320, 80],
    [300, 250],
    [750, 250],
    [750, 160],
    [728, 90],
  ],
  adSizeMappings: heroAdConfigMap[adType].adSizeMappings,
  adClass: 'sl-hero-ad-unit__dfp',
  adViewType: heroAdConfigMap[adType].adViewType,
  adTargets: getAdTargets(qaFlag, user, entitlements, contentKeyword),
});

export default heroAdConfig;
