/** @prettier */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { GoogleDFP, entitlementsPropType, userPropType } from '@surfline/quiver-react';
import { getWindow } from '@surfline/web-common';
import heroAd, { fetchArticleSponsor } from '../../../api/heroAd';
import heroAdConfig from './heroAdConfig';
import config from '../../../config';

class HeroAdUnit extends Component {
  constructor() {
    super();
    this.state = {
      tracked: false,
      error: false,
      contentKeyword: null,
      ready: false,
    };
  }

  componentDidMount() {
    this.setAdIsReady();
    const { placeholder } = this.props;
    if (placeholder === 'on') {
      this.setupAdErrorListener();
    }
  }

  componentWillUnmount() {
    if (this.open) {
      XMLHttpRequest.prototype.open = this.open;
    }
  }

  setAdIsReady = async () => {
    const { entitlements, adType, articleId } = this.props;
    const isPremium = entitlements.includes('sl_premium');
    if (adType === 'editorial_article') {
      try {
        const { dfpKeyword, hideAds } = await fetchArticleSponsor(articleId);
        if (dfpKeyword) this.setState({ ready: true, contentKeyword: dfpKeyword });
        else if (!isPremium && !hideAds) this.setState({ ready: true });
      } catch (err) {
        // TODO: Consider logging to New Relic
        // eslint-disable-next-line no-console
        console.log(`Unable to fetch hero ad for editorial article ${articleId}`, err);
      }
    } else this.setState({ ready: true });
  };

  /**
   * Overrides XMLHttpRequest prototype's open method to inject listeners. We listen for opened
   * requests to https://securepubads.g.doubleclick.net that have errored. We also consider an error
   * to have taken place if no such request has been opened for 1 second.
   */
  setupAdErrorListener = () => {
    const self = this;
    self.open = XMLHttpRequest.prototype.open;
    self.adRequestCounter = 0;

    XMLHttpRequest.prototype.open = function openReplacement(...args) {
      const [method, url] = args;
      if (method === 'GET' && url.startsWith('https://securepubads.g.doubleclick.net')) {
        self.adRequestCounter += 1;
        this.onreadystatechange = () => {
          if (this.readyState === 4 && this.status !== 200) {
            self.heroAdErrored();
          }
        };
      }
      self.open.apply(this, args);
    };

    setTimeout(() => {
      if (this.adRequestCounter === 0) {
        this.heroAdErrored();
      }
    }, 1000);
  };

  heroAdErrored = () => {
    XMLHttpRequest.prototype.open = this.open;
    this.setState({
      error: true,
    });
  };

  /**
   * DFP even listener tracks hero ad render result for future placeholders.
   * @param  {Boolean} isEmpty True if hero ad did not render successfully
   */
  dfpEventListener = ({ isEmpty }) => {
    const { adType } = this.props;
    const { tracked } = this.state;
    if (tracked) return;

    // Do not track article page hero ads
    if (adType === 'editorial_article' || adType === 'forecast_article') return;

    if (isEmpty) {
      heroAd('failure', 'off');
    } else {
      setTimeout(() => {
        if (this.dfpRef.ref.offsetHeight > 0) {
          heroAd('success', 'on', getWindow()?.innerWidth, this.dfpRef.ref.offsetHeight);
        }
      }, 2000);
    }
    this.setState({ tracked: true });
  };

  heroAdClass = () => {
    const { placeholder } = this.props;
    const { error } = this.state;
    return classNames({
      'sl-hero-ad-unit': true,
      'sl-hero-ad-unit--placeholder': placeholder === 'on' && !error,
    });
  };

  render() {
    const { adType, user, entitlements, qaFlag } = this.props;
    const { ready, contentKeyword } = this.state;
    return (
      <div className={this.heroAdClass()}>
        {ready ? (
          <GoogleDFP
            ref={(el) => {
              this.dfpRef = el;
            }}
            adConfig={heroAdConfig(adType, user, entitlements, qaFlag, contentKeyword)}
            preventRefresh
            eventListeners={[{ type: 'slotRenderEnded', listener: this.dfpEventListener }]}
            isHtl
            isTesting={config.htl.isTesting}
          />
        ) : null}
      </div>
    );
  }
}

HeroAdUnit.propTypes = {
  adType: PropTypes.string.isRequired,
  user: userPropType,
  entitlements: entitlementsPropType.isRequired,
  qaFlag: PropTypes.string,
  placeholder: PropTypes.oneOf(['on', 'off']),
  articleId: PropTypes.string,
};

HeroAdUnit.defaultProps = {
  user: null,
  qaFlag: null,
  placeholder: 'off',
  articleId: null,
};

export default HeroAdUnit;
