import React, { Component } from 'react';
import { ModalContainer, Button, TrackableLink } from '@surfline/quiver-react';
import { getWindow, trackEvent } from '@surfline/web-common';
import PRODUCT_CDN from '@surfline/quiver-assets';
import CtaHackChecker from '../../../utils/ctaHackChecker';
import config from '../../../config';
import { CLICKED_SUBSCRIBE_CTA } from '../../../common/constants';

class AdBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAdBlocker: false,
    };

    this.signInLinkRef = React.createRef();
    this.signUpLinkRef = React.createRef();
  }

  componentDidMount() {
    const ctaHackConfig = {
      targetClasses: ['quiver-modal-container'],
      backgroundTransMatch: '0.7',
      blurPxMatch: '6px',
    };

    this.ctaHackChecker = new CtaHackChecker(ctaHackConfig);
    this.ctaHackChecker.observe();
    this.ctaHackChecker.startDOMCheck();
    getWindow()?.addEventListener('resize', this.resizeListener);
    this.detectAdBlocker();
  }

  componentDidUpdate(prevProps, prevState) {
    const { showAdBlocker } = this.state;

    const win = getWindow();

    if (showAdBlocker && showAdBlocker !== prevState.showAdBlocker && !!win) {
      win.document.body.style.position = 'fixed';
      win.document.body.style.width = '100%';

      trackEvent('Detected Ad Blocker', {
        platform: 'web',
      });
    }
  }

  componentWillUnmount() {
    this.ctaHackChecker.disconnect();
    this.ctaHackChecker.stopDOMCheck();
    getWindow()?.removeEventListener('resize', this.resizeListener);
  }

  onClickUnblock = () => {
    trackEvent('Clicked Link', {
      linkUrl: 'https://support.surfline.com/hc/en-us/articles/360029359271-Why-can-t-I-block-ads-',
      linkName: 'Ad Blocker - Unblock Ads',
      linkLocation: 'Ad Blocker Modal',
      category: 'modal',
    });
    getWindow()?.open(
      'https://support.surfline.com/hc/en-us/articles/360029359271-Why-can-t-I-block-ads-',
      '_blank',
    );
  };

  detectAdBlocker = () => {
    const win = getWindow();
    const [head] = win?.document.getElementsByTagName('head');
    const oldScript = win?.document.getElementById('ads-blocker-detection');
    if (oldScript) head.removeChild(oldScript);
    const script = win?.document.createElement('script');
    script.id = 'ads-blocker-detection';
    script.type = 'text/javascript';
    script.src = `${PRODUCT_CDN}/scripts/ad/doubleclick.js`;
    script.onload = () => {
      this.setState({ showAdBlocker: false });
    };
    script.onerror = () => {
      this.setState({ showAdBlocker: true });
    };
    head.appendChild(script);
  };

  onClickWhitelist = () => {
    trackEvent('Clicked Link', {
      linkUrl: getWindow()?.location.href,
      linkName: 'Ad Blocker - Whitelisted Ads',
      linkLocation: 'Ad Blocker Modal',
      category: 'modal',
    });
    setTimeout(() => getWindow()?.location.reload(), 300);
  };

  getSignInEventProperties = () => ({
    linkUrl: `${config.surflineHost}/sign-in?returnUrl=${getWindow()?.location}`,
    linkName: 'Ad Blocker - Sign In',
    linkLocation: 'Ad Blocker Modal',
    category: 'modal',
  });

  getSignUpEventProperties = () => ({
    location: 'modal ad blocker',
  });

  resizeListener = () => {
    this.ctaHackChecker.disconnect();
    this.ctaHackChecker.stopDOMCheck();
    if (this.resizeTimeout) return;

    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.ctaHackChecker.observe();
      this.ctaHackChecker.startDOMCheck();
    }, 3000);
  };

  render() {
    const { showAdBlocker } = this.state;
    return (
      <div>
        {showAdBlocker ? (
          <div>
            <ModalContainer
              isOpen={showAdBlocker}
              contentLabel="Surfline Ad Blocker"
              location="adblock"
            >
              <h5>We See You&#39;re Using an Ad Blocker</h5>
              <p>
                {`Advertising supports our efforts to provide
              much of our forecast tools and editorial content
              to our users for free. To continue using our services,
              whitelist Surfline in your ad blocker.`}
              </p>
              <div className="sl-ad-blocker__image__container">
                <img
                  className="sl-ad-blocker__hand"
                  alt="Hand Stop"
                  src={`${PRODUCT_CDN}/icons/hand-stop.png`}
                />
              </div>
              <Button onClick={() => this.onClickUnblock()}>Unblock Ads</Button>
              <Button onClick={() => this.onClickWhitelist()} id="sl-ad-block__whitelisted">
                I&#39;ve whitelisted
              </Button>
              <p>
                {'Already a premium member? '}
                <TrackableLink
                  ref={this.signInLinkRef}
                  eventName="Clicked Link"
                  eventProperties={this.getSignInEventProperties}
                >
                  <a
                    ref={this.signInLinkRef}
                    href={`${config.surflineHost}/sign-in?redirectUrl=${getWindow()?.location}`}
                  >
                    Sign In
                  </a>
                </TrackableLink>
                {' or '}
                <TrackableLink
                  ref={this.signUpLinkRef}
                  eventName={CLICKED_SUBSCRIBE_CTA}
                  eventProperties={this.getSignUpEventProperties}
                >
                  <a ref={this.signUpLinkRef} href={`${config.surflineHost}/upgrade`}>
                    Sign Up
                  </a>
                </TrackableLink>
                {' to skip this message.'}
              </p>
            </ModalContainer>
          </div>
        ) : null}
      </div>
    );
  }
}

export default AdBlock;
