import React, { useRef } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { parse } from 'query-string';
import {
  NewNavigationBar,
  FavoritesBar,
  Notification,
  TrackableLink,
  withDevice,
} from '@surfline/quiver-react';
import {
  getUserPremiumStatus,
  getUserFreeTrialEligible,
  getUserEmailVerified,
  getWindow,
} from '@surfline/web-common';
import Sticky from 'react-stickynode';
import HeroAdUnit from './HeroAdUnit';
import AdBlock from './AdBlock';
import NotificationBarController from '../NotificationBarController';
import { CLICKED_SUBSCRIBE_CTA, STICKY_ENABLED_MIN_WIDTH } from '../../../common/constants';
import {
  useEntitlementsSelector,
  useUserDetailsSelector,
  useUserSettingsSelector,
  useUserFavoritesSelector,
  useSubscriptionWarningsSelector,
} from '../../../selectors/user';
import { heroAdConfigMap } from './heroAdConfig';
import config from '../../../config';

const SUBSCRIBE_CTA_PROPERTIES = { location: 'button top' };
const STICKY_NAV_ZINDEX = 89;

const Header = ({ renderOptions, ...props }) => {
  const freeTrialLinkRef = useRef();
  const user = useUserDetailsSelector();
  const settings = useUserSettingsSelector();
  const favorites = useUserFavoritesSelector();
  const entitlements = useEntitlementsSelector();
  const isPremium = useSelector((state) => getUserPremiumStatus(state, true));
  const freeTrialEligible = useSelector(getUserFreeTrialEligible);
  const warnings = useSubscriptionWarningsSelector();

  // Check if an alert should be displayed to the current user
  const isEmailVerified = useSelector(getUserEmailVerified);
  const accountAlert = user && !isEmailVerified;

  // Check if a warning should be displayed to the current user
  const accountWarning = warnings.alert;

  const largeFavoritesBar = !!renderOptions.largeFavoritesBar;
  const hideCTA = renderOptions.hideCTA === 'true';
  const hideNav = renderOptions.hideNav === 'true';
  const { heroAdPlaceholder } = renderOptions;

  const smallNotification = (
    <Notification type="small" level="information">
      <TrackableLink
        ref={freeTrialLinkRef}
        eventName={CLICKED_SUBSCRIBE_CTA}
        eventProperties={SUBSCRIBE_CTA_PROPERTIES}
      >
        <a ref={freeTrialLinkRef} href="/upgrade" className="quiver-notification__cta">
          START FREE TRIAL
        </a>
      </TrackableLink>
    </Notification>
  );

  const qaFlag = renderOptions?.qaFlag
    ? renderOptions.qaFlag
    : parse(getWindow()?.location?.search)?.qaFlag;

  return (
    <div>
      {!isPremium && renderOptions.showAdBlock === 'true' ? <AdBlock /> : null}
      {renderOptions.bannerAd !== 'false' && heroAdConfigMap[renderOptions.bannerAd] ? (
        <HeroAdUnit
          adType={renderOptions.bannerAd}
          user={user}
          entitlements={entitlements}
          qaFlag={qaFlag}
          placeholder={heroAdPlaceholder}
          articleId={renderOptions.bannerAdId}
        />
      ) : null}
      {!hideNav ? (
        <div>
          {/* {!hideCTA ? ( */}
          <NotificationBarController isPremium={isPremium} user={user} warnings={warnings} />
          {/* ) : null} */}
          <Sticky
            enabled={props.device?.width >= STICKY_ENABLED_MIN_WIDTH && !!renderOptions.isNavSticky}
            innerZ={STICKY_NAV_ZINDEX}
          >
            <NewNavigationBar
              user={user}
              favorites={favorites}
              settings={settings}
              smallNotification={smallNotification}
              hideCTA={hideCTA}
              entitlements={entitlements}
              serviceConfig={{
                serviceUrl: config.servicesURL,
              }}
              freeTrialEligible={freeTrialEligible}
              accountAlert={accountAlert}
              accountWarning={accountWarning}
            />
            <FavoritesBar
              settings={settings}
              favorites={favorites}
              user={user}
              large={largeFavoritesBar}
            />
          </Sticky>
        </div>
      ) : null}
    </div>
  );
};

Header.propTypes = {
  renderOptions: PropTypes.shape({
    largeFavoritesBar: PropTypes.string,
    isNavSticky: PropTypes.bool,
    qaFlag: PropTypes.string,
    bannerAd: PropTypes.string,
    hideCTA: PropTypes.oneOf(['true', 'false']),
    hideNav: PropTypes.oneOf(['true', 'false']),
    showAdBlock: PropTypes.oneOf(['true', 'false']),
    bannerAdId: PropTypes.string,
    heroAdPlaceholder: PropTypes.oneOf(['on', 'off']),
  }),
  device: PropTypes.shape({
    width: PropTypes.number,
  }),
  features: PropTypes.shape({
    treatments: PropTypes.shape({}),
  }).isRequired,
};

Header.defaultProps = {
  device: {
    width: null,
  },
  renderOptions: {
    isNavSticky: false,
    largeFavoritesBar: null,
    qaFlag: null,
    bannerAd: 'false',
    hideCTA: 'false',
    hideNav: 'false',
  },
};

export default withDevice(Header);
