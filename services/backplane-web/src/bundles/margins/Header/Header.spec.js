import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';
import Header from './Header';
import createStore from '../../../stores';
import { STICKY_ENABLED_MIN_WIDTH } from '../../../common/constants';

describe('Header', () => {
  const { innerWidth } = global;

  afterEach(() => {
    // reset inner width
    global.innerWidth = innerWidth;
  });

  it('should render the header', () => {
    const props = {
      features: {
        treatments: {},
      },
    };
    const store = createStore();
    const wrapper = shallow(
      <Provider store={store}>
        <Header {...props} />
      </Provider>,
    );
    expect(wrapper).to.be.ok();
  });

  it('should render the header with large favorites bar', () => {
    const props = {
      features: {
        treatments: {},
      },
      renderOptions: {
        largeFavoritesBar: 'true',
        rateLimitingEnabled: 'true',
      },
    };
    const store = createStore();
    const wrapper = mount(
      <Provider store={store}>
        <Header {...props} />
      </Provider>,
    );
    expect(
      wrapper.find('.quiver-favorites-bar__content-multi-cam-link_anchor--large'),
    ).to.have.lengthOf(1);
  });

  it('should render the header with small favorites bar', () => {
    const props = {
      features: {
        treatments: {},
      },
      renderOptions: {
        largeFavoritesBar: null,
        rateLimitingEnabled: 'true',
      },
    };
    const store = createStore();
    const wrapper = mount(
      <Provider store={store}>
        <Header {...props} />
      </Provider>,
    );
    expect(
      wrapper.find('.quiver-favorites-bar__content-multi-cam-link_anchor--large'),
    ).to.have.lengthOf(0);
  });

  it('should render the header with sticky nav disabled if view port is less than 768', () => {
    global.innerWidth = STICKY_ENABLED_MIN_WIDTH - 1;

    // Trigger the window resize event.
    global.dispatchEvent(new Event('resize'));

    const props = {
      features: {
        treatments: {},
      },
      renderOptions: {
        isNavSticky: true,
        largeFavoritesBar: null,
        rateLimitingEnabled: 'true',
      },
    };
    const store = createStore();
    const wrapper = mount(
      <Provider store={store}>
        <Header {...props} />
      </Provider>,
    );
    expect(wrapper.find('Sticky').prop('enabled')).to.be.false();
  });

  it('should render the header with sticky nav disabled if isNavSticky is false', () => {
    global.innerWidth = STICKY_ENABLED_MIN_WIDTH;

    // Trigger the window resize event.
    global.dispatchEvent(new Event('resize'));

    const props = {
      features: {
        treatments: {},
      },
      renderOptions: {
        isNavSticky: false,
        largeFavoritesBar: null,
        rateLimitingEnabled: 'true',
      },
    };
    const store = createStore();
    const wrapper = mount(
      <Provider store={store}>
        <Header {...props} />
      </Provider>,
    );
    expect(wrapper.find('Sticky').prop('enabled')).to.be.false();
  });

  it('should render the header with sticky nav enabled', () => {
    global.innerWidth = STICKY_ENABLED_MIN_WIDTH;

    // Trigger the window resize event.
    global.dispatchEvent(new Event('resize'));

    const props = {
      features: {
        treatments: {},
      },
      renderOptions: {
        isNavSticky: true,
        largeFavoritesBar: null,
        rateLimitingEnabled: 'true',
      },
    };
    const store = createStore();
    const wrapper = mount(
      <Provider store={store}>
        <Header {...props} />
      </Provider>,
    );
    expect(wrapper.find('Sticky').prop('enabled')).to.be.true();
  });
});
