import { expect } from 'chai';
import NotificationBarController from './NotificationBarController';
import { mountWithRedux } from '../../utils/testUtils';

// TODO: SA-3375 - Add Braze test after Card converted to prop

describe('NotificationBarController', () => {
  it(`should render the NotificationBar`, () => {
    const { wrapper } = mountWithRedux(NotificationBarController);
    expect(wrapper.find('NotificationBarController')).to.have.length(1);
    expect(wrapper.find('NotificationBar')).to.have.length(1);
  });
});
