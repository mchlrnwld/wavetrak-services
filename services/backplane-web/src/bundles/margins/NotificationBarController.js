import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  NotificationBar,
  Notification,
  TrackableLink,
  withContentCards,
} from '@surfline/quiver-react';
import {
  dismissContentCard,
  getWindow,
  trackContentCardClick,
  trackContentCardImpressions,
} from '@surfline/web-common';
import { CLICKED_SUBSCRIBE_CTA, BrazeContentCards } from '../../common/constants';

const SUBSCRIBE_CTA_PROPERTIES = { location: 'upsell bar' };

const NotificationBarController = ({ card, warnings, slDisableNotificationCTA }) => {
  const freeTrialLinkRef = useRef(null);
  const [rendered, setRendered] = useState(false);

  const handleClickCTA = useCallback(
    (e) => {
      e.preventDefault();
      trackContentCardClick(card);
      setTimeout(() => {
        getWindow()?.location.assign(card.extras.href__url, 200);
      });
    },
    [card],
  );

  const renderBrazeNotification = useMemo(
    () =>
      card?.extras && (
        <Notification
          closable
          type="large"
          level="information"
          onCloseComplete={() => dismissContentCard(BrazeContentCards.NOTIFICATION_BAR)}
        >
          <TrackableLink
            ref={freeTrialLinkRef}
            eventName={CLICKED_SUBSCRIBE_CTA}
            eventProperties={SUBSCRIBE_CTA_PROPERTIES}
          >
            <a
              ref={freeTrialLinkRef}
              href={card.extras.href__url}
              className="quiver-notification__tag-line"
              style={{ ...card.extras.css }}
              onClick={handleClickCTA}
            >
              {card.extras.href__text}
            </a>
          </TrackableLink>
        </Notification>
      ),
    [card, handleClickCTA],
  );

  const renderPaymentWarningsNotification = useMemo(() => {
    const { message: notification, type } = warnings;
    return (
      <div>
        <Notification closable type="warning-notification" level="error">
          {type === 'web' ? (
            <a href="/account/subscription#payment-details">{notification}</a>
          ) : (
            <span>{notification}</span>
          )}
        </Notification>
      </div>
    );
  }, [warnings]);

  const renderNotification = useMemo(() => {
    if (slDisableNotificationCTA === 'on') return null;
    if (warnings.alert) return renderPaymentWarningsNotification;
    if (card?.extras !== null) {
      if (!rendered) {
        setRendered(true);
        trackContentCardImpressions([card]);
      }
      return renderBrazeNotification;
    }
    return null;
  }, [
    card,
    rendered,
    renderPaymentWarningsNotification,
    renderBrazeNotification,
    slDisableNotificationCTA,
    warnings,
  ]);

  const [notification, setNotification] = useState(renderNotification);
  // Updates the bar when the Braze card is set
  useEffect(() => {
    setNotification(renderNotification);
  }, [renderNotification]);

  return <NotificationBar largeNotification={notification} />;
};

NotificationBarController.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string,
    extras: PropTypes.shape({
      css: PropTypes.objectOf(PropTypes.shape),
      href__text: PropTypes.string,
      href__url: PropTypes.string,
    }),
    title: PropTypes.string,
  }),
  slDisableNotificationCTA: PropTypes.string,
  warnings: PropTypes.shape({
    alert: PropTypes.bool,
    message: PropTypes.string,
    type: PropTypes.string,
  }),
};

NotificationBarController.defaultProps = {
  card: { name: BrazeContentCards.NOTIFICATION_BAR, extras: null },
  slDisableNotificationCTA: 'off',
  warnings: [],
};

export default withContentCards(NotificationBarController);
