/* istanbul ignore file */
import React from 'react';
import { CookiesProvider } from 'react-cookie';
import { hydrate, unmountComponentAtNode } from 'react-dom';
import { Provider } from 'react-redux';
import { getWindow } from '@surfline/web-common';

import Header from './Header';
import Footer from './Footer';
import RegWall from './RegWall';
import fetchBackplane from '../../utils/fetchBackplane';

const win = getWindow();
const headerRoot = win?.document.getElementById('backplane-header');
const footerRoot = win?.document.getElementById('backplane-footer');
const regWallRoot = win?.document.getElementById('backplane-regwall');

/**
 * @param {import('../../stores').Store} store
 */
export const getWrappedHydrate =
  (store) =>
  /**
   * @param {Parameters<hydrate>[0]} entrypointToWrap
   * @param {Parameters<hydrate>[1]} rootToRenderTo
   */
  (entrypointToWrap, rootToRenderTo) =>
    hydrate(
      <CookiesProvider>
        <Provider store={store}>{entrypointToWrap}</Provider>
      </CookiesProvider>,
      rootToRenderTo,
    );

/**
 * @param {import('../../stores').Store} store
 * @param {Record<string, unknown>} preloadedState
 */
export const getDoHydrate = (store, preloadedState) => {
  const doHydrate = async () => {
    const wrappedHydrate = getWrappedHydrate(store);

    wrappedHydrate(<Header {...preloadedState} />, headerRoot);
    wrappedHydrate(<Footer {...preloadedState} />, footerRoot);
    if (regWallRoot) {
      wrappedHydrate(<RegWall {...preloadedState} />, regWallRoot);
    }
  };

  if (module.hot) {
    module.hot.accept(['./Header', './Footer', './RegWall'], () => {
      unmountComponentAtNode(headerRoot);
      unmountComponentAtNode(footerRoot);
      unmountComponentAtNode(regWallRoot);
      doHydrate();
    });
  }

  return doHydrate;
};

export const getPreloadedState = async () => {
  const backplane = await fetchBackplane();

  win?.document.dispatchEvent(
    new CustomEvent('backplane_loaded', {
      detail: backplane.data.api,
    }),
  );

  return backplane?.data?.api;
};
