import React from 'react';
import PropTypes from 'prop-types';
import { SurflineFooter } from '@surfline/quiver-react';
import { useEntitlementsSelector } from '../../selectors/user';

const Footer = ({ renderOptions }) => {
  const entitlements = useEntitlementsSelector();
  if (renderOptions.hideNav === 'true') return null;
  return <SurflineFooter entitlements={entitlements || []} />;
};

Footer.propTypes = {
  renderOptions: PropTypes.shape({
    hideNav: PropTypes.oneOf(['true', 'false']),
  }),
};

Footer.defaultProps = {
  renderOptions: {
    hideNav: 'false',
  },
};

export default Footer;
