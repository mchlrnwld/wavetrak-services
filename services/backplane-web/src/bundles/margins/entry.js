/* istanbul ignore file */
import { once } from 'lodash';
import {
  BrazeEvents,
  createSplitFilter,
  getUser,
  getWavetrakFeatureFrameworkCB,
  getWavetrakSplitFilters,
  getWindow,
  setWavetrakAnalyticsReadyCB,
  setWavetrakFeatureFrameworkCB,
  setupFeatureFramework,
  refreshContentCards,
  getAppboy,
} from '@surfline/web-common';

import reducer from '../../reducers';
import createStore from '../../stores';
import * as treatments from '../../common/treatments';
import * as treatmentsWithConfig from '../../common/treatmentsWithConfig';
import { getLoadMeterBeforeHydration } from '../../common/metering';
import config from '../../config';
import { getDoHydrate, getPreloadedState } from './render';
import './styles.scss';

const win = getWindow();

const isNextApp = win.wavetrakNextApp;
const preloadedState = win?.__BACKPLANE_API__; // eslint-disable-line no-underscore-dangle
const preloadedReduxState = win?.__BACKPLANE_REDUX__; // eslint-disable-line no-underscore-dangle

const doBackplaneDevWarning = () => {
  if (!preloadedReduxState && process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line no-console
    console.warn(
      'Preloaded Redux state was not found: Backplane components ' +
        'will render without server side preloaded state',
    );
  }
};

/** @type {import('../../stores').Store} */
let store;

const mount = async () => {
  // If there exists a store to merge, then perform the merge. If not create our own store here.
  if (win.wavetrakStore) {
    store = win.wavetrakStore;
    win.wavetrakStore.injectReducer('backplane', reducer);
  } else {
    doBackplaneDevWarning();
    store = createStore(preloadedReduxState);
  }

  try {
    const doHydrate = getDoHydrate(store, preloadedState || (await getPreloadedState()));
    const { rateLimitingEnabled } = preloadedState?.renderOptions || {};

    const setupCallback = once(
      getLoadMeterBeforeHydration(
        store,
        doHydrate,
        rateLimitingEnabled,
        getWavetrakFeatureFrameworkCB(),
      ),
    );
    setWavetrakFeatureFrameworkCB(setupCallback);

    const brazeTimeout = setTimeout(() => {
      getWindow()?.document.dispatchEvent(new CustomEvent(BrazeEvents.BRAZE_TIMEOUT));
    }, 3000);

    const analyticsCallback = () => {
      getWindow()?.document.dispatchEvent(new CustomEvent(BrazeEvents.BRAZE_LOADED));
      setTimeout(() => {
        if (!getAppboy()?.getCachedContentCards()?.cards.length) {
          refreshContentCards();
        }
      }, 6000);
      clearTimeout(brazeTimeout);
    };
    setWavetrakAnalyticsReadyCB(analyticsCallback);

    try {
      await setupFeatureFramework({
        authorizationKey: config.appKeys.splitio,
        user: getUser(store.getState()),
        splitFilters: [
          ...createSplitFilter(treatments, treatmentsWithConfig),
          ...getWavetrakSplitFilters(),
        ],
      });
    } catch (_) {
      // We can ignore errors for the time being since it can fail for users with adblock
    }
  } catch (error) {
    // TODO: Use Web Common Function for this
    getWindow()?.newrelic?.noticeError(error);
  }
};

if (!isNextApp) {
  mount();
} else {
  win.wavetrakBackplaneMount = mount;
}
