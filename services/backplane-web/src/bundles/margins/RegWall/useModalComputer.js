import { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useUpdateEffect } from 'react-use';
import { computeMeterModal } from '../../../common/metering';
import { performedMeterModalRecompute, triggerMeterModalRecompute } from '../../../actions';
import { getMeterModalRecomputeFlag } from '../../../selectors/state';
import { useMeterSelector } from '../../../selectors/metering';
import { useEntitlementsSelector, useUserDetailsSelector } from '../../../selectors/user';

const useModalRecomputer = () => {
  const dispatch = useDispatch();

  const entitlements = useEntitlementsSelector();
  const user = useUserDetailsSelector();
  const meter = useMeterSelector();
  const shouldRecompute = useSelector(getMeterModalRecomputeFlag);

  const [modalToRender, setModalToRender] = useState(computeMeterModal(meter, user, entitlements));
  const recomputeModal = useCallback(
    () => setModalToRender(computeMeterModal(meter, user, entitlements)),
    [meter, user, entitlements],
  );

  useEffect(() => {
    if (shouldRecompute) {
      recomputeModal();
      dispatch(performedMeterModalRecompute());
    }
  }, [shouldRecompute, dispatch, recomputeModal]);

  return modalToRender;
};

/**
 * @description Since we have to perform a few different fetches
 * when performing inline registration/sign-in (user info, meter info)
 * we want to defer recomputing the regwall state until all the different
 * fetches have completed successfully
 * @return {{ modal: JSX.Element | null, scrollable: bollean }}
 */
export const useModalComputer = () => {
  const dispatch = useDispatch();

  const location = useLocation();

  const modalToRender = useModalRecomputer();

  // If the route changes, we also want to re-compute the modal since
  // the `meterRemaining` may have changed
  useUpdateEffect(() => {
    if (location.pathname) {
      dispatch(triggerMeterModalRecompute());
    }
  }, [location.pathname, dispatch]);

  return modalToRender;
};
