import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';

import { getWindow, SL_METERED } from '@surfline/web-common';
import RegWall from './RegWall';
import createStore from '../../../stores';
import { mountWithRedux } from '../../../utils/testUtils';
import * as metering from '../../../common/metering';
import * as modalComputer from './useModalComputer';
import { getBackplaneAppState } from '../../../selectors/state';

describe('RegWall', () => {
  /** @type {sinon.SinonStub} */
  let useModalComputerStub;

  /** @type {sinon.SinonStub} */
  let rateLimitUserDetailsStub;

  /** @type {sinon.SinonSpy} */
  let scrollToSpy;

  beforeEach(() => {
    useModalComputerStub = sinon.stub(modalComputer, 'useModalComputer');
    rateLimitUserDetailsStub = sinon.stub(metering, 'rateLimitUserDetails');
    scrollToSpy = sinon.spy(getWindow(), 'scrollTo');
  });

  afterEach(() => {
    scrollToSpy.restore();
    useModalComputerStub.restore();
    rateLimitUserDetailsStub.restore();
  });

  it('should not render a regwall if rate limiting is not enabled', () => {
    useModalComputerStub.returns(<div className="modal" />);

    const store = createStore({
      backplane: {
        state: {
          meterLoaded: true,
        },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      {
        renderOptions: { rateLimitingEnabled: false },
      },
      { store },
    );

    const content = wrapper.find('.modal');
    expect(content.exists()).to.be.false();
    expect(rateLimitUserDetailsStub).to.have.not.been.called();
    expect(scrollToSpy).to.not.have.been.called();
  });

  it('should not render a regwall for if meterLoaded is false', () => {
    useModalComputerStub.returns(<div className="modal" />);

    const store = createStore({
      backplane: {
        state: {
          meterLoaded: false,
        },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    const content = wrapper.find('.modal');
    expect(content.exists()).to.be.false();
    expect(scrollToSpy).to.not.have.been.called();
  });

  it('should call rateLimitUserDetails if meter is not loaded', () => {
    const store = createStore({
      backplane: {
        state: {
          meterLoaded: false,
          meteringEnabled: true,
        },
      },
    });

    mountWithRedux(RegWall, { renderOptions: { rateLimitingEnabled: true } }, { store });

    expect(rateLimitUserDetailsStub).to.have.been.calledOnceWithExactly(true, store);
  });

  it('should not render a reg wall for an anonymous user with no meter', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const store = createStore({
      backplane: {
        user: {
          entitlements: [],
        },
        state: {
          meterLoaded: true,
          meteringEnabled: true,
        },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    const content = wrapper.find('div');
    expect(content.children().exists()).to.be.false();
  });

  it('should not render a reg wall for a metered anonymous user with remaining checks', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      treatmentState: 'on',
      meterRemaining: 1,
    };
    const user = {
      details: null,
    };
    const store = createStore({
      backplane: {
        user,
        meter,
        state: { meterLoaded: true, meteringEnabled: true },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    const content = wrapper.find('MeterBottomSheetRegwall');
    expect(content.children().exists()).to.be.false();
  });

  it('should not render a reg wall for a metered anonymous user with meterRemaining 0', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      treatmentState: 'on',
      meterRemaining: 0,
    };
    const user = {
      details: null,
    };
    const store = createStore({
      backplane: {
        user,
        meter,
        state: { meterLoaded: true, meteringEnabled: true },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    const content = wrapper.find('MeterBottomSheetRegwall');
    expect(content.children().exists()).to.be.false();
  });

  it('should not render a reg wall for a metered anonymous user with an off treatment', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      treatmentState: 'off',
      meterRemaining: 0,
    };
    const user = {
      details: null,
    };
    const store = createStore({
      backplane: {
        user,
        meter,
        state: { meterLoaded: true, meteringEnabled: true },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    const content = wrapper.find('div');
    expect(content.children().exists()).to.be.false();
  });

  it('should render a reg wall for a metered anonymous user', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      treatmentState: 'on',
      meterRemaining: -1,
    };
    const user = {
      details: null,
    };
    const store = createStore({
      backplane: {
        user,
        meter,
        state: { meterLoaded: true, meteringEnabled: true },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    expect(wrapper.find('MeterBottomSheetRegWall')).to.have.length(1);
    expect(scrollToSpy).to.have.been.calledOnceWithExactly({ top: 0, behavior: 'smooth' });
  });

  it('should not render a reg wall for a metered user with remaining checks', async () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      meterRemaining: 5,
    };

    const user = {
      entitlements: [SL_METERED],
      details: { email: 'jmonson@surfline.com' },
    };

    const store = createStore({
      backplane: {
        user,
        meter,
        state: {
          meterLoaded: true,
          meteringEnabled: true,
        },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: true } },
      { store },
    );

    const content = wrapper.find('MeterPremiumPaywall');
    expect(content.children().exists()).to.be.false();

    expect(scrollToSpy).to.not.have.been.called();
    expect(rateLimitUserDetailsStub).to.not.have.been.called();
  });

  it('should not render a reg wall for a metered user in the last surf check', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      meterRemaining: 0,
    };

    const user = {
      entitlements: [SL_METERED],
      details: { email: 'jmonson@surfline.com' },
    };

    const store = createStore({
      backplane: {
        user,
        meter,
        state: { meterLoaded: true, meteringEnabled: true },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: 'true' } },
      { store },
    );

    const content = wrapper.find('MeterPremiumPaywall');
    expect(content.children().exists()).to.be.false();

    expect(scrollToSpy).to.not.have.been.called();
    expect(rateLimitUserDetailsStub).to.not.have.been.called();
  });

  it('should render a reg wall for a metered user with no surf checks left', () => {
    useModalComputerStub.restore();
    useModalComputerStub = sinon.spy(modalComputer, 'useModalComputer');

    const meter = {
      meterRemaining: -1,
    };

    const user = {
      entitlements: [SL_METERED],
      details: { email: 'jmonson@surfline.com' },
    };

    const store = createStore({
      backplane: {
        user,
        meter,
        state: {
          shouldRecomputeMeterModal: false,
          meterLoaded: true,
          meteringEnabled: true,
        },
      },
    });

    const { wrapper } = mountWithRedux(
      RegWall,
      { renderOptions: { rateLimitingEnabled: 'true' } },
      { store },
    );
    expect(wrapper.find('MeterPremiumPaywall')).to.have.length(1);
    expect(getBackplaneAppState(store.getState())).to.deep.equal({
      shouldRecomputeMeterModal: false,
      meterLoaded: true,
      meteringEnabled: true,
    });
    expect(scrollToSpy).to.have.been.calledOnceWithExactly({ top: 0, behavior: 'smooth' });
    expect(rateLimitUserDetailsStub).to.not.have.been.called();
  });
});
