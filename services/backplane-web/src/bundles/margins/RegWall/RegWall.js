import React, { useEffect, useState } from 'react';
import { useMount } from 'react-use';
import { useSelector, useStore } from 'react-redux';
import PropTypes from 'prop-types';
import { canUseDOM, getWindow } from '@surfline/web-common';
import { useModalComputer } from './useModalComputer';
import { rateLimitUserDetails } from '../../../common/metering';
import { getMeteringEnabled, getMeterLoaded } from '../../../selectors/state';

const RegWall = ({ renderOptions: { rateLimitingEnabled } }) => {
  const [ready, setReady] = useState(false);
  const store = useStore();
  const meteringEnabled = useSelector(getMeteringEnabled);
  const meterLoaded = useSelector(getMeterLoaded);

  const { modal, scrollable } = useModalComputer() ?? {};
  const [isExcludedPath, setIsExcludedPath] = useState(false);

  useMount(() => {
    if (!meterLoaded && meteringEnabled) {
      rateLimitUserDetails(rateLimitingEnabled, store);
    }
  });

  useEffect(() => {
    if (canUseDOM && rateLimitingEnabled && meterLoaded) {
      setReady(true);
      const excludedPaths = ['surf-reports-forecasts-cams-map', 'surf-reports-forecasts-cams'];
      setIsExcludedPath(excludedPaths.some((path) => getWindow()?.location.href.includes(path)));
    }
  }, [rateLimitingEnabled, ready, meterLoaded, modal]);

  // When the reg-wall mounts, we want to scroll the page to the top
  useEffect(() => {
    if (ready && scrollable === false) {
      getWindow()?.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }, [ready, scrollable]);

  return <>{ready && modal && meteringEnabled && !isExcludedPath ? modal : null}</>;
};

RegWall.propTypes = {
  renderOptions: PropTypes.shape({
    rateLimitingEnabled: PropTypes.oneOf([true, false]),
  }),
};

RegWall.defaultProps = {
  renderOptions: PropTypes.shape({}),
};

export default RegWall;
