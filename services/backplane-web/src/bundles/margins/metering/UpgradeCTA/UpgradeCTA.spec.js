import { expect } from 'chai';
import { mountWithRedux } from '../../../../utils/testUtils';
import UpgradeCTA from './UpgradeCTA';

describe('UpgradeCTA', () => {
  it(`should render the UpgradeCTA with a valid Braze card`, () => {
    const { wrapper } = mountWithRedux(
      UpgradeCTA,
      {
        card: {
          name: 'RATE_LIMITING_UPGRADE_CARD',
          extras: {
            box__cta_copy:
              'Sign up for a Premium subscription and receive one month free. Unlock unlimited access to the tools that help you score better waves, more often.',
            box__subtext: 'Get unlimited access to cams, reports, and everything else you love.',
            box__title: 'Special Offer: One Month Free',
            button__color: '#0058B0',
            button__copy: 'START FREE TRIAL',
            button__subtext: 'Trusted by surfers everywhere.',
            button__subtext__bold: 'Cancel anytime.',
            button__url: '/upgrade',
          },
        },
      },
      undefined,
    );
    expect(wrapper.find('.sl-upgrade-cta__box-title').text()).to.be.equal(
      'Special Offer: One Month Free',
    );
    expect(wrapper.find('UpgradeCTA')).to.have.length(1);
  });

  it(`should not render the UpgradeCTA if there is no Braze card`, () => {
    const { wrapper } = mountWithRedux(
      UpgradeCTA,
      {
        card: {
          name: 'RATE_LIMITING_UPGRADE_CARD',
          extras: null,
        },
      },
      undefined,
    );
    expect(wrapper.find('.sl-upgrade-cta__box-title').exists()).to.equal(false);
  });
});
