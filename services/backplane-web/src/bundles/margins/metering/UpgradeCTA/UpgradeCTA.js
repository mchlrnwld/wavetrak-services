import React, { useState, useCallback } from 'react';
import { useEvent } from 'react-use';
import {
  Button,
  Chevron,
  NotificationDialog,
  createAccessibleOnClick,
  withContentCards,
} from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';
import {
  getWindow,
  trackClickedSubscribeCTA,
  trackContentCardClick,
  trackContentCardImpressions,
  trackEvent,
} from '@surfline/web-common';
import PropTypes from 'prop-types';
import {
  BrazeContentCards,
  CLICKED_POPUP,
  CLICKED_SUBSCRIBE_CTA,
  UPGRADE_CTA,
} from '../../../../common/constants';

const UpgradeCTA = ({ card }) => {
  const [expanded, setExpanded] = useState(true);
  const [rendered, setRendered] = useState(false);

  const handleOnScroll = useCallback(() => {
    if (expanded) {
      setTimeout(() => {
        setExpanded(false);
      }, 300);
    }
    if (getWindow()?.scrollY === 0) {
      setExpanded(true);
    }
  }, [expanded]);

  const onClickCTA = useCallback(() => {
    trackContentCardClick(card);
    trackEvent(CLICKED_POPUP, {
      action: CLICKED_SUBSCRIBE_CTA,
      category: UPGRADE_CTA,
    });
    trackClickedSubscribeCTA({ location: UPGRADE_CTA });
    setTimeout(() => {
      getWindow()?.location.assign(card.extras.button__url);
    }, 500);
  }, [card]);

  const counterClicked = () => {
    setTimeout(() => {
      setExpanded(!expanded);
    }, 10);
  };

  if (!rendered && card?.extras) {
    setRendered(true);
    trackContentCardImpressions([card]);
  }

  useEvent('scroll', handleOnScroll);

  const counterClickProps = createAccessibleOnClick(counterClicked, 'BTN');

  const styles = `
    .sl-upgrade-cta .quiver-notification-dialog .quiver-button {
      background-color: ${card?.extras?.button__color};
      border-color: ${card?.extras?.button__color};
    }
  `;
  return (
    card?.extras && (
      <div className="sl-upgrade-cta" {...counterClickProps}>
        <style>{styles}</style>
        <NotificationDialog
          title={
            <div className="sl-upgrade-cta__box-header">
              <h5 className="sl-upgrade-cta__box-title">{card.extras.box__title}</h5>
              <div className="sl-upgrade-cta__chevron">
                <Chevron direction={expanded ? 'down' : 'up'} />
              </div>
            </div>
          }
          expanded={expanded}
          bgColor={color('gray', 10)}
        >
          <div className="sl-upgrade-cta__container">
            <div className="sl-upgrade-cta__box">
              <div className="sl-upgrade-cta__box--content">
                <h4>{card.extras.box__h1}</h4>
                <span>{card.extras.box__h2}</span>
              </div>
              <div className="sl-upgrade-cta__box--action">
                <Button onClick={onClickCTA}>{card.extras.button__text}</Button>
                <div className="sl-upgrade-cta__box--sub-text">
                  <span>{card.extras.footnote__subtext_bold} </span>
                  {card.extras.footnote__subtext}
                </div>
              </div>
            </div>
          </div>
        </NotificationDialog>
      </div>
    )
  );
};

UpgradeCTA.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string,
    extras: PropTypes.shape({
      box__title: PropTypes.string,
      box__h1: PropTypes.string,
      box__h2: PropTypes.string,
      button__color: PropTypes.string,
      button__url: PropTypes.string,
      button__text: PropTypes.string,
      footnote__subtext_bold: PropTypes.string,
      footnote__subtext: PropTypes.string,
    }),
  }),
};

UpgradeCTA.defaultProps = {
  card: {
    name: BrazeContentCards.RATE_LIMITING_UPGRADE_CARD,
    extras: null,
  },
};

export default withContentCards(UpgradeCTA);
