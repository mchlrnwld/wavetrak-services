import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Button, CalendarIcon, HollowCamIcon, WaveIcon } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import { color } from '@surfline/quiver-themes';
import {
  SIGN_IN,
  HOME,
  CLICKED_POPUP,
  FORCED_REGISTRATION,
  NAVIGATED_TO_CREATE_ACCOUNT,
  NAVIGATED_TO_SIGN_IN,
} from '../../../../common/constants';

const trackNavigated = (location) =>
  trackEvent(CLICKED_POPUP, {
    action: location === HOME ? NAVIGATED_TO_CREATE_ACCOUNT : NAVIGATED_TO_SIGN_IN,
    category: FORCED_REGISTRATION,
  });

const LearnMore = ({ setForm }) => {
  const handleNavigation = useCallback(
    (formName) => {
      trackNavigated(formName);
      setForm(formName);
    },
    [setForm],
  );

  return (
    <div className="sl-meter-learnmore__container" data-scroll-lock-scrollable="true">
      <h1 className="sl-meter-learnmore__container_title">
        Get 3 Surf Checks Per Week With a Free Account
      </h1>
      <div className="sl-meter-learnmore__container_subtitle">
        Surf Checks get you more access to Surfline so you can score better waves, more often.
      </div>
      <ul className="sl-meter-learnmore__container__benefits">
        <li>
          <span className="sl-meter-learnmore__container__benefits-icon">
            <HollowCamIcon />
          </span>
          <span>
            <b>Ad-Free Cams.</b> View 750+ Surf Cams at your favorite surf spots across the globe.
            See the conditions live in high definition. All ad-free.
          </span>
        </li>
        <li>
          <span className="sl-meter-learnmore__container__benefits-icon">
            <CalendarIcon />
          </span>
          <span>
            <b>Long-Range Forecasts.</b> Check long-range 16-day surf forecasts to plan your next
            surf.
          </span>
        </li>
        <li>
          <span className="sl-meter-learnmore__container__benefits-icon">
            <WaveIcon stroke={color('dark-blue', 10)} />
          </span>
          <span>
            <b>Expert Analysis.</b> Daily surf reports and forecast analysis from Surfline&apos;s
            team of forecasters.
          </span>
        </li>
      </ul>
      <div className="sl-meter-learnmore__container__actions">
        <Button onClick={() => handleNavigation(HOME)}>CREATE A FREE ACCOUNT</Button>
        <div className="sl-meter-learnmore__container__actions__signin">
          Already have an account?{' '}
          <button
            type="button"
            onClick={() => handleNavigation(SIGN_IN)}
            className="sl-meter-learnmore__container__actions__signin__button"
          >
            Sign In.
          </button>
        </div>
      </div>
    </div>
  );
};

LearnMore.propTypes = {
  setForm: PropTypes.func.isRequired,
};

export default LearnMore;
