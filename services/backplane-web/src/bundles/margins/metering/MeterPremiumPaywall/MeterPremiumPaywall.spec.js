import { expect } from 'chai';
import { mountWithRedux } from '../../../../utils/testUtils';
import MeterPremiumPaywall from './MeterPremiumPaywall';

describe('MeterPremiumPaywall', () => {
  const meter = {
    active: true,
    anonymousId: 'a14d46e0-dcbc-11ea-ac64-0b5f2c83e2f2',
    countryCode: 'US',
    treatmentName: 'sl_rate_limiting_v3',
    meterLimit: 0,
    meterRemaining: 0,
    treatmentState: 'on',
    createdAt: '2020-09-10T20:45:44.823+0000',
    updatedAt: '2020-09-10T20:45:44.823+0000',
  };

  it(`should render the Metered Premium Paywall`, () => {
    const initialState = {
      backplane: {
        user: {
          details: { firstName: 'firstName' },
          entitlements: [],
          promotionEntitlements: [],
          favorites: [],
          settings: null,
        },
        meter,
      },
    };
    const { wrapper } = mountWithRedux(MeterPremiumPaywall, undefined, { initialState });
    expect(wrapper.find('MeterPremiumPaywall')).to.have.length(1);
  });
});
