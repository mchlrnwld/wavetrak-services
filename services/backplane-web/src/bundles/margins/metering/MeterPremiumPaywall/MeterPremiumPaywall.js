import React, { useMemo, useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, RegWallContainer, withContentCards, NewCloseIcon } from '@surfline/quiver-react';
import {
  SL_FREE_TRIAL,
  TABLET_LARGE_WIDTH,
  getMeterRemaining,
  getMeter,
  getWindow,
  trackClickedSubscribeCTA,
  trackContentCardClick,
  trackContentCardImpressions,
  trackEvent,
} from '@surfline/web-common';
import PropTypes from 'prop-types';

import { SERVED_POPUP, CLICKED_POPUP, RATE_LIMITING } from '../../../../common/constants';
import {
  usePromotionEntitlementsSelector,
  useUserDetailsSelector,
} from '../../../../selectors/user';

const RATE_LIMITING_PAYWALL = 'RATE_LIMITING_PAYWALL';

const MeterPremiumPaywall = ({ card }) => {
  const meterRemaining = useSelector(getMeterRemaining);
  const { meterLimit } = useSelector(getMeter);
  const { firstName } = useUserDetailsSelector();
  const promotionEntitlements = usePromotionEntitlementsSelector();
  const [rendered, setRendered] = useState(false);
  const [isClosed, setIsClosed] = useState(false);

  const handleClose = useCallback(() => {
    setIsClosed(true);
  }, []);
  const isFreeTrialEligible = useMemo(
    () => promotionEntitlements.includes(SL_FREE_TRIAL),
    [promotionEntitlements],
  );

  const ctaButtonText = isFreeTrialEligible
    ? card?.extras?.button__text__trialeligible
    : card?.extras?.button__text__nontrialeligible;

  useEffect(() => {
    trackEvent(SERVED_POPUP, {
      popupName: 'Rate Limiting Paywall',
      category: RATE_LIMITING,
      platform: 'web',
      isMobileView: getWindow()?.innerWidth <= TABLET_LARGE_WIDTH,
    });
  }, []);

  const onClickCTA = useCallback(() => {
    trackEvent(CLICKED_POPUP, {
      action: 'Clicked CTA',
      category: 'Rate Limiting Paywall',
    });
    trackClickedSubscribeCTA({
      location: 'Rate Limiting Paywall',
      checksRemaining: meterRemaining,
      platform: 'web',
    });
    trackContentCardClick(card);
    setTimeout(() => getWindow()?.location.assign(card.extras.button__url), 300);
  }, [meterRemaining, card]);

  if (!rendered && meterRemaining !== null && card?.extras) {
    setRendered(true);
    trackContentCardImpressions([card]);
  }

  const subHeaderText = card?.extras?.subheader__text.replace('{x}', meterLimit.toString());

  const styles = `
    .sl-meter-premium-paywall__content .quiver-button{
      background-color: ${card?.extras?.button__color};
      border-color: ${card?.extras?.button__color};
    }
    .sl-meter-premium-paywall__content .quiver-button:hover{
      background-color: ${card?.extras?.button__highlight_color};
      border-color: ${card?.extras?.button__highlight_color};
    }
  `;

  if (isClosed) return null;

  if (meterRemaining !== null && card?.extras) {
    return (
      <div className="sl-meter-premium-paywall__container">
        <RegWallContainer>
          <style>{styles}</style>
          <button
            className="sl-meter-bottom-sheet__close"
            onClick={handleClose}
            onKeyDown={handleClose}
            tabIndex="0"
            type="button"
          >
            <NewCloseIcon />
          </button>
          <div className="sl-meter-premium-paywall__content">
            <div className="sl-meter-premium-paywall__box-header">
              <p className="sl-meter-premium-paywall__count-message">
                {card.extras.count__message_1}
              </p>
              <div className="sl-meter-premium-paywall__count">
                <span className="sl-meter-premium-paywall__count--text">
                  0 {card.extras.surfcheck__unit}
                </span>
              </div>
              <p className="sl-meter-premium-paywall__count-message">
                {card.extras.count__message_2}
              </p>
            </div>
            <h5 className="sl-meter-premium-paywall__title">
              {`${card.extras.header__text_1 || firstName}${card.extras.header__text_2}`}
            </h5>
            <p className="sl-meter-premium-paywall__subtitle">{subHeaderText}</p>
            <p
              className="sl-meter-premium-paywall__subtitle-two"
              /* eslint-disable-next-line react/no-danger */
              dangerouslySetInnerHTML={{ __html: card.extras.subtitle__text }}
            />
            <Button className="sl-meter-premium-paywall__actions" onClick={onClickCTA}>
              {ctaButtonText}
            </Button>
            <div className="sl-meter-premium-paywall__actions--mobile">
              <Button onClick={onClickCTA}>
                <p>{ctaButtonText}</p>
              </Button>
            </div>
          </div>
        </RegWallContainer>
      </div>
    );
  }
  return null;
};

MeterPremiumPaywall.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string,
    extras: PropTypes.shape({
      button__text__trialeligible: PropTypes.string,
      button__text__nontrialeligible: PropTypes.string,
      button__color: PropTypes.string,
      button__highlight_color: PropTypes.string,
      button__url: PropTypes.string,
      subheader__text: PropTypes.string,
      count__message_1: PropTypes.string,
      count__message_2: PropTypes.string,
      surfcheck__unit: PropTypes.string,
      header__text_1: PropTypes.string,
      header__text_2: PropTypes.string,
      subtitle__text: PropTypes.string,
    }),
    title: PropTypes.string,
  }),
  defaultCard: PropTypes.shape({
    extras: PropTypes.shape({}),
  }),
};

MeterPremiumPaywall.defaultProps = {
  card: { name: RATE_LIMITING_PAYWALL, extras: null },
  defaultCard: {
    extras: {
      button__text__trialeligible: 'START FREE TRIAL',
      button__text__nontrialeligible: 'GO PREMIUM',
      button__color: '#0058B0',
      button__highlight_color: '#004488',
      button__url: '/upgrade',
      subheader__text: 'You’ll get {x} new Surf Checks on Monday',
      count__message_1: 'You have',
      count__message_2: 'left',
      surfcheck__unit: 'Surf Check',
      header__text_1: null,
      header__text_2: `, you’ve run out of Surf Checks this week`,
      subtitle__text:
        'A Premium account will get you back in action with unlimited <br /> Surf Checks, ad-free cams, long-range forecasts and more!',
    },
  },
};

export default withContentCards(MeterPremiumPaywall);
