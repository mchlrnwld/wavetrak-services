import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useEvent } from 'react-use';
import {
  Button,
  Chevron,
  NotificationDialog,
  createAccessibleOnClick,
  withContentCards,
} from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';
import {
  getMeterRemaining,
  getUserFreeTrialEligible,
  getWindow,
  trackClickedSubscribeCTA,
  trackContentCardClick,
  trackContentCardImpressions,
  trackEvent,
  getMeter,
} from '@surfline/web-common';
import PropTypes from 'prop-types';
import { BrazeContentCards, CLICKED_POPUP } from '../../../../common/constants';

const RATE_LIMIT_CARD = 'Rate Limit Card';

const SurfCheckCounter = ({ card }) => {
  const meterRemaining = useSelector(getMeterRemaining);
  const { meterLimit } = useSelector(getMeter);
  const isFreeTrialEligible = useSelector(getUserFreeTrialEligible);
  const [expanded, setExpanded] = useState(true);
  const [rendered, setRendered] = useState(false);

  const handleOnScroll = useCallback(() => {
    if (expanded) {
      setTimeout(() => {
        setExpanded(false);
      }, 300);
    }
    if (getWindow()?.scrollY === 0) {
      setExpanded(true);
    }
  }, [expanded]);

  const onClickCTA = useCallback(() => {
    trackEvent(CLICKED_POPUP, {
      action: 'Clicked CTA',
      category: RATE_LIMIT_CARD,
    });
    trackClickedSubscribeCTA({
      location: RATE_LIMIT_CARD,
      checksRemaining: meterRemaining,
      platform: 'web',
    });
    trackContentCardClick(card);
    setTimeout(() => {
      getWindow()?.location.assign(card.extras.button__url);
    }, 500);
  }, [meterRemaining, card]);

  const counterClicked = () => {
    setTimeout(() => {
      setExpanded(!expanded);
    }, 10);
  };

  if (!rendered && card?.extras) {
    setRendered(true);
    trackContentCardImpressions([card]);
  }

  useEvent('scroll', handleOnScroll);

  const counterText =
    meterRemaining !== 0 ? `${card?.extras?.surfcheck__unit}s` : card?.extras?.surfcheck__unit;
  const subHeaderText =
    card?.extras?.subheader__text ||
    `Your ${meterLimit} checks reset every Monday. Want unlimited Surf Checks?`;

  const counterClickProps = createAccessibleOnClick(counterClicked, 'BTN');

  const styles = `
    .sl-surf-check-counter .quiver-notification-dialog .quiver-button{
      background-color: ${card?.extras?.button__color};
      border-color: ${card?.extras?.button__color};
    }
    .sl-surf-check-counter .quiver-notification-dialog .quiver-button:hover{
      background-color: ${card?.extras?.button__highlight_color};
      border-color: ${card?.extras?.button__highlight_color};
    }
  `;

  return (
    card?.extras && (
      <div className="sl-surf-check-counter" {...counterClickProps}>
        <style>{styles}</style>
        <NotificationDialog
          title={
            <div className="sl-surf-check-counter__box-header">
              <h5 className="sl-surf-check-counter__box-title">{card.extras.box__title_1}</h5>
              <div className="sl-surf-check-counter__count">
                <span>
                  {meterRemaining + 1} {counterText}
                </span>
              </div>
              <h5 className="sl-surf-check-counter__box-title">{card.extras.box__title_2}</h5>
              <div className="sl-surf-check-counter__chevron">
                <Chevron direction={expanded ? 'up' : 'down'} />
              </div>
            </div>
          }
          expanded={expanded}
          bgColor={color('gray', 10)}
        >
          <p className="sl-surf-check-counter__box-text">{subHeaderText}</p>
          <Button onClick={onClickCTA}>
            {isFreeTrialEligible
              ? card.extras.button__text__trialeligible
              : card.extras.button__text__nontrialeligible}
          </Button>
        </NotificationDialog>
      </div>
    )
  );
};

SurfCheckCounter.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string,
    extras: PropTypes.shape({
      box__title_1: PropTypes.string,
      box__title_2: PropTypes.string,
      surfcheck__unit: PropTypes.string,
      subheader__text: PropTypes.string,
      button__url: PropTypes.string,
      button__color: PropTypes.string,
      button__highlight_color: PropTypes.string,
      button__text__trialeligible: PropTypes.string,
      button__text__nontrialeligible: PropTypes.string,
    }),
  }),
  defaultCard: PropTypes.shape({
    extras: PropTypes.shape({}),
  }),
};

SurfCheckCounter.defaultProps = {
  card: {
    name: BrazeContentCards.RATE_LIMITING_CHECK_COUNTER,
    extras: null,
  },
  defaultCard: {
    extras: {
      box__title_1: 'You have',
      box__title_2: 'left',
      surfcheck__unit: 'Surf Check',
      subheader__text: null,
      button__url: '/upgrade',
      button__color: color('dark-blue', 10),
      button__highlight_color: color('dark-blue', 30),
      button__text__trialeligible: 'START FREE TRIAL',
      button__text__nontrialeligible: 'GO PREMIUM',
    },
  },
};

export default withContentCards(SurfCheckCounter);
