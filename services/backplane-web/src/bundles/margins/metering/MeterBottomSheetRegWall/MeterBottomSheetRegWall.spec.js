import { expect } from 'chai';
import MeterBottomSheetRegWall from './MeterBottomSheetRegWall';
import createStore from '../../../../stores';
import { mountWithRedux } from '../../../../utils/testUtils';
import { BrazeContentCards } from '../../../../common/constants';

describe('MeterBottomSheetRegWall', () => {
  it(`should render the default reg wall create account page`, () => {
    const store = createStore();
    const { wrapper } = mountWithRedux(MeterBottomSheetRegWall, undefined, { store });
    expect(wrapper.find('RegWallContainer')).to.have.length(1);
    expect(wrapper.find('Home')).to.have.length(1);
    expect(wrapper.find('.sl-meter-bottom-sheet__modal__action__back')).to.have.length(0);
  });

  it(`should render the inline regwall if there is a Braze card`, () => {
    const { wrapper } = mountWithRedux(MeterBottomSheetRegWall, {
      card: {
        name: BrazeContentCards.REGISTRATION_WALL,
        extras: {
          box1__header: 'Get unlimited access',
          box1__description: `Receive your first month free after signing up for a Premium membership.
              Unlock unlimited access to cams, reports, long-range forecasts, and more.`,
          box1__button__url: '/upgrade',
          box1__button__text: 'Start free trial',
          box1__button__color: '#0058B0',
          box1__button__border_color: '#0058B0',
          box1__button__hover_color: '#0058B0',
          box1__button__text_color: '#fff',
          box1__footnote_text: 'Trusted by surfers everywhere.',
          box1__footnote_text_bold: 'Cancel anytime. ',
          box2__header: 'Create a free account',
          box2__description: `Registered free users can check the surf three times per week.`,
          box2__button__url: '/create-account',
          box2__button__text: 'Create a free account',
          box2__button__color: '#fff',
          box2__button__border_color: '#0058B0',
          box2__button__hover_color: '#fff',
          box2__button__text_color: '#0058B0',
          header__text: 'Keep checking the surf',
          is_single_page: 'true',
        },
      },
    });
    expect(wrapper.find('.sl-meter-bottom-sheet-inline-regwall').exists()).to.equal(true);
  });
});
