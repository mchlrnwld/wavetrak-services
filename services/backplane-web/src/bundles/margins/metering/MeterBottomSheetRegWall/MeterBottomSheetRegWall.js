import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, RegWallContainer, withContentCards, NewCloseIcon } from '@surfline/quiver-react';
import {
  TABLET_LARGE_WIDTH,
  getWindow,
  trackClickedSubscribeCTA,
  trackContentCardClick,
  trackContentCardImpressions,
  trackEvent,
} from '@surfline/web-common';
import SignIn from '../SignIn';
import LearnMore from '../LearnMore';
import Home from '../Home';
import {
  BrazeContentCards,
  CLICKED_POPUP,
  FORCED_REGISTRATION,
  HOME,
  LEARN_MORE,
  SERVED_POPUP,
  SIGN_IN,
} from '../../../../common/constants';

const trackPopupServed = () =>
  trackEvent(SERVED_POPUP, {
    popupName: FORCED_REGISTRATION,
    category: FORCED_REGISTRATION,
    platform: 'web',
    isMobileView: getWindow()?.innerWidth <= TABLET_LARGE_WIDTH,
  });

/**
 * @function MeterBottomSheetRegWall
 * @param {object} props
 * @return {JSX.Elemet}
 */
const MeterBottomSheetRegWall = ({ card }) => {
  useEffect(() => {
    trackPopupServed();
  }, []);

  const isSinglePage = card?.extras?.is_single_page === 'true';

  const [form, setForm] = useState(HOME);
  const [rendered, setRendered] = useState(false);
  const [isClosed, setIsClosed] = useState(false);

  const handleClose = useCallback(() => {
    setIsClosed(true);
  }, []);

  const handleClick = useCallback(
    (url) => {
      trackEvent(CLICKED_POPUP, {
        action: 'Clicked CTA',
        category: FORCED_REGISTRATION,
      });
      if (url === '/upgrade') {
        trackClickedSubscribeCTA({ location: FORCED_REGISTRATION });
      }
      trackContentCardClick(card);
      setTimeout(() => getWindow()?.location.assign(url), 300);
    },
    [card],
  );

  const style = useMemo(
    () =>
      isSinglePage
        ? `
    .sl-meter-bottom-sheet-inline-regwall__box1 .quiver-button {
      background-color: ${card.extras.box1__button__color};
      border-color: ${card.extras.box1__button__border_color};
      color: ${card.extras.box1__button__text_color};
    }
    .sl-meter-bottom-sheet-inline-regwall__box1 .quiver-button:hover {
      background-color: ${card.extras.box1__button__hover_color};
    }
    .sl-meter-bottom-sheet-inline-regwall__box2 .quiver-button {
      background-color: ${card.extras.box2__button__color};
      border-color: ${card.extras.box2__button__border_color};
      color: ${card.extras.box2__button__text_color};
    }
    .sl-meter-bottom-sheet-inline-regwall__box2 .quiver-button:hover {
      background-color: ${card.extras.box2__button__hover_color};
    }
  `
        : null,
    [card, isSinglePage],
  );

  const renderSinglePage = () => {
    if (!card?.extras) return null;
    if (!rendered) {
      setRendered(true);
      trackContentCardImpressions([card]);
    }
    return (
      <div className="sl-meter-bottom-sheet-inline-regwall" data-scroll-lock-scrollable="true">
        <style>{style}</style>
        <div className="sl-meter-bottom-sheet-inline-regwall__cta-container">
          <h1>{card.extras.header__text}</h1>
          <div className="sl-meter-bottom-sheet-inline-regwall__box-container">
            <div className="sl-meter-bottom-sheet-inline-regwall__box sl-meter-bottom-sheet-inline-regwall__box1">
              <h3>{card.extras.box1__header}</h3>
              <div
                className="sl-meter-bottom-sheet-inline-regwall__box__description"
                /* eslint-disable-next-line react/no-danger */
                dangerouslySetInnerHTML={{ __html: card.extras.box1__description }}
              />
              <Button onClick={() => handleClick(card.extras.box1__button__url)}>
                {card.extras?.box1__button__text}
              </Button>
              <span className="sl-meter-bottom-sheet-inline-regwall__box__footnote-bold">
                {card.extras.box1__footnote_text_bold}
              </span>
              <span className="sl-meter-bottom-sheet-inline-regwall__box__footnote">
                {card.extras.box1__footnote_text}
              </span>
            </div>
            <div className="sl-meter-bottom-sheet-inline-regwall__divider">- or -</div>
            <div className="sl-meter-bottom-sheet-inline-regwall__box sl-meter-bottom-sheet-inline-regwall__box2">
              <h3>{card.extras.box2__header}</h3>
              <div
                className="sl-meter-bottom-sheet-inline-regwall__box__description"
                /* eslint-disable-next-line react/no-danger */
                dangerouslySetInnerHTML={{ __html: card.extras.box2__description }}
              />
              <Button onClick={() => handleClick(card.extras.box2__button__url)}>
                {card.extras.box2__button__text}
              </Button>
              <div className="sl-meter-bottom-sheet-inline-regwall__box__signin_label">
                Already have an account? <a href="/sign-in">Sign In.</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const renderForm = () => {
    switch (form) {
      case SIGN_IN:
        return <SignIn setForm={setForm} />;
      case LEARN_MORE:
        return <LearnMore setForm={setForm} />;
      default:
        return <Home setForm={setForm} />;
    }
  };

  const renderMultipage = () => (
    <div className="sl-meter-bottom-sheet__modal">
      {form === HOME ? null : (
        <button
          className="sl-meter-bottom-sheet__modal__action__back"
          onClick={() => setForm(HOME)}
          type="button"
        >
          <div className="sl-meter-bottom-sheet__modal__action__arrow" />
          <span>BACK</span>
        </button>
      )}
      {renderForm()}
    </div>
  );

  if (isClosed) return null;
  return (
    <div className="sl-meter-bottom-sheet">
      <RegWallContainer>
        <button
          className="sl-meter-bottom-sheet__close"
          onClick={handleClose}
          onKeyDown={handleClose}
          tabIndex="0"
          type="button"
        >
          <NewCloseIcon />
        </button>
        {isSinglePage ? renderSinglePage() : renderMultipage()}
      </RegWallContainer>
    </div>
  );
};

MeterBottomSheetRegWall.propTypes = {
  card: PropTypes.shape({
    extras: PropTypes.shape({
      is_single_page: PropTypes.string,
      box1__header: PropTypes.string,
      box1__description: PropTypes.string,
      box1__button__border_color: PropTypes.string,
      box1__button__url: PropTypes.string,
      box1__button__text: PropTypes.string,
      box1__button__color: PropTypes.string,
      box1__button__hover_color: PropTypes.string,
      box1__button__text_color: PropTypes.string,
      box1__footnote_text: PropTypes.string,
      box1__footnote_text_bold: PropTypes.string,
      box2__header: PropTypes.string,
      box2__description: PropTypes.string,
      box2__button__border_color: PropTypes.string,
      box2__button__url: PropTypes.string,
      box2__button__text: PropTypes.string,
      box2__button__color: PropTypes.string,
      box2__button__hover_color: PropTypes.string,
      box2__button__text_color: PropTypes.string,
      header__text: PropTypes.string,
    }),
  }),
};

MeterBottomSheetRegWall.defaultProps = {
  card: { name: BrazeContentCards.REGISTRATION_WALL },
};

export default withContentCards(MeterBottomSheetRegWall);
