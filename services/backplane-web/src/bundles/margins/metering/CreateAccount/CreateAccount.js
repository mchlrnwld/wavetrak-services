import React from 'react';
import { CreateAccountForm, Disclaimer } from '@surfline/quiver-react';

import { useCreateAccount } from './useCreateAccount';

/**
 * @function CreateAccount
 * @return {JSX.Element}
 */
const CreateAccount = () => {
  const { createAccount, loading, error } = useCreateAccount();

  return (
    <div className="sl-meter-create-account">
      <CreateAccountForm
        onSubmit={createAccount}
        loading={loading}
        error={error}
        receivePromotionsLabel="Send me product and marketing updates from Surfline and its partners"
        buttonLabel="CREATE A FREE ACCOUNT"
      />
      <Disclaimer referenceText="Create Account" hideTrialText />
    </div>
  );
};
export default CreateAccount;
