import { useDispatch } from 'react-redux';
import { useCallback, useState } from 'react';
import {
  LOGGED_IN,
  SL,
  SL_METERED,
  deviceInfo,
  getWavetrakIdentity,
  getWindow,
  setWavetrakIdentity,
  trackEvent,
} from '@surfline/web-common';
import { postUser } from '../../../../api/user';
import { setupUser, triggerMeterModalRecompute, createMeter } from '../../../../actions';
import config from '../../../../config';
import { useUserSelector } from '../../../../selectors/user';
import { useMeterSelector } from '../../../../selectors/metering';
import {
  CLICKED_CREATE_ACCOUNT,
  CLICKED_POPUP,
  FORCED_REGISTRATION,
} from '../../../../common/constants';

const { clientId } = config;

export const useCreateAccount = () => {
  const dispatch = useDispatch();
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const user = useUserSelector();
  const meter = useMeterSelector();

  const { anonymousId } = meter || {};
  const { location, countryCode } = user;
  const { time_zone: timezone } = location || {};

  const createAccount = useCallback(
    /**
     * @param {Object} values
     * @param {string} values.firstName
     * @param {string} values.lastName
     * @param {string} values.password
     * @param {string} values.email
     * @param {boolean?} values.staySignedIn
     * @param {boolean?} values.receivePromotions
     * @return {Promise<void>}
     */
    async (values) => {
      try {
        trackEvent(CLICKED_POPUP, {
          action: CLICKED_CREATE_ACCOUNT,
          category: FORCED_REGISTRATION,
        });

        trackEvent(CLICKED_CREATE_ACCOUNT, {
          location: FORCED_REGISTRATION,
          pageName: getWindow()?.location?.pathname,
        });

        setLoading(true);
        const { firstName, lastName, email, password, receivePromotions, staySignedIn } = values;
        const { deviceId, deviceType } = deviceInfo();

        const valuesForRegistration = {
          firstName,
          lastName,
          email,
          password,
          brand: SL,
          settings: {
            receivePromotions,
          },
          client_id: clientId,
          device_id: deviceId,
          device_type: deviceType,
        };

        const { userId, accessToken } = await postUser(!staySignedIn, valuesForRegistration);

        getWindow()?.analytics?.identify(userId);

        await dispatch(setupUser({ accessToken }));

        await dispatch(
          createMeter({
            accessToken,
            anonymousId,
            countryCode,
            timezone,
            performMeterSurfCheck: true,
          }),
        );

        setWavetrakIdentity({
          ...getWavetrakIdentity(),
          userId,
          entitlements: [SL_METERED],
          email: values.email,
          rl_forced: true,
          type: LOGGED_IN,
        });

        setLoading(false);
        dispatch(triggerMeterModalRecompute());
      } catch (err) {
        setLoading(false);
        setError(err.message);
      }
    },
    [anonymousId, dispatch, countryCode, timezone],
  );
  return { createAccount, loading, error };
};
