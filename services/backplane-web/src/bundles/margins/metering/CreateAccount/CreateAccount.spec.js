import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import {
  getUserDetails,
  getUserSettings,
  getEntitlements,
  getUserFavorites,
  deviceInfo,
  getMeter,
  getWavetrakIdentity,
  SL_METERED,
  LOGGED_IN,
} from '@surfline/web-common';
import { mountWithRedux } from '../../../../utils/testUtils';
import CreateAccount from './CreateAccount';
import * as userApi from '../../../../api/user';
import * as meterApi from '../../../../api/meter';
import createStore from '../../../../stores';
import config from '../../../../config';
import { getMeterModalRecomputeFlag } from '../../../../selectors/state';

const { clientId } = config;

describe('CreateAccount', () => {
  /** @type {sinon.SinonStub} */
  let getUserDetailsStub;

  /** @type {sinon.SinonStub} */
  let getUserEntitlementsStub;

  /** @type {sinon.SinonStub} */
  let getUserSettingsStub;

  /** @type {sinon.SinonStub} */
  let getUserFavoritesStub;

  /** @type {sinon.SinonStub} */
  let getUserSubscriptionWarningsStub;

  /** @type {sinon.SinonStub} */
  let postUserStub;

  /** @type {sinon.SinonStub} */
  let postMeterStub;

  /** @type {sinon.SinonStub} */
  let patchMeteringSurfCheckStub;

  const favorites = deepFreeze([{ _id: '1' }]);
  const details = deepFreeze({
    firstName: 'Lucy',
    lastName: 'Goosey',
    email: 'lucybear@gmail.com',
    _id: 'a',
    isEmailVerified: true,
  });
  const entitlements = deepFreeze([]);
  const settings = deepFreeze({ settings: true });
  const storeState = {
    backplane: { state: { meteringEnabled: true, shouldRecomputeMeterModal: false } },
  };

  const values = {
    firstName: 'Lucy',
    lastName: 'Goosey',
    email: 'jmonson@surfline.com',
    password: 'test1234',
    receivePromotions: true,
    staySignedIn: true,
  };

  beforeEach(() => {
    postUserStub = sinon.stub(userApi, 'postUser');
    getUserDetailsStub = sinon.stub(userApi, 'getUserDetails');
    getUserEntitlementsStub = sinon.stub(userApi, 'getUserEntitlements');
    getUserSettingsStub = sinon.stub(userApi, 'getUserSettings');
    getUserFavoritesStub = sinon.stub(userApi, 'getUserFavorites');
    getUserSubscriptionWarningsStub = sinon.stub(userApi, 'getUserSubscriptionWarnings');
    postMeterStub = sinon.stub(meterApi, 'postMeter');
    patchMeteringSurfCheckStub = sinon.stub(meterApi, 'patchMeteringSurfCheck');
  });

  afterEach(() => {
    postUserStub.restore();
    getUserDetailsStub.restore();
    getUserEntitlementsStub.restore();
    getUserSettingsStub.restore();
    getUserFavoritesStub.restore();
    getUserSubscriptionWarningsStub.restore();
    postMeterStub.restore();
    patchMeteringSurfCheckStub.restore();
  });

  it(`should render the CreateAccountForm`, () => {
    const { wrapper } = mountWithRedux(CreateAccount);
    expect(wrapper.find('.quiver-button').text()).to.be.equal('CREATE A FREE ACCOUNT');
    expect(wrapper.find('CreateAccountForm')).to.have.length(1);
  });

  it('should handle creating an account', async () => {
    const store = createStore({
      backplane: {
        meter: {
          anonymousId: 'anonymousId',
        },
        user: {
          countryCode: 'countryCode',
          location: { time_zone: 'timezone' },
        },
        state: { meteringEnabled: true },
      },
    });
    const meterRemaining = 4;
    const { wrapper } = mountWithRedux(CreateAccount, undefined, {
      store,
    });

    postUserStub.resolves({
      accessToken: 'accessToken',
      userId: 'userId',
    });
    postMeterStub.resolves({ meter: true });
    patchMeteringSurfCheckStub.resolves({ meterRemaining });
    getUserDetailsStub.resolves(details);
    getUserEntitlementsStub.resolves({ entitlements: [], promotions: [] });
    getUserSettingsStub.resolves(settings);
    getUserFavoritesStub.resolves({ data: { favorites } });
    getUserSubscriptionWarningsStub.resolves([]);

    await wrapper.find('CreateAccountForm').prop('onSubmit')(values);

    const { deviceId, deviceType } = deviceInfo();

    expect(postUserStub).to.have.been.calledOnceWith(false, {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      password: values.password,
      settings: {
        receivePromotions: true,
      },
      brand: 'sl',
      client_id: clientId,
      device_id: deviceId,
      device_type: deviceType,
    });

    expect(postMeterStub).to.have.been.calledOnceWithExactly({
      accessToken: 'accessToken',
      anonymousId: 'anonymousId',
      timezone: 'timezone',
      countryCode: 'countryCode',
    });
    expect(patchMeteringSurfCheckStub).to.have.been.calledOnceWithExactly('accessToken');

    expect(getUserDetailsStub).to.have.been.calledOnce();
    expect(getUserEntitlementsStub).to.have.been.calledOnce();
    expect(getUserFavoritesStub).to.have.been.calledOnce();
    expect(getUserSettingsStub).to.have.been.calledOnce();
    expect(getUserSubscriptionWarningsStub).to.have.been.calledOnce();

    const state = store.getState();

    expect(getUserDetails(state)).to.deep.equal({ ...details, password: undefined });
    expect(getUserSettings(state)).to.deep.equal(settings);
    expect(getEntitlements(state)).to.deep.equal([SL_METERED]);
    expect(getUserFavorites(state)).to.deep.equal(favorites);
    expect(getMeter(state)).to.deep.equal({
      anonymousId: 'anonymousId',
      meter: true,
      meterRemaining,
      showPaywall: false,
    });
    expect(getMeterModalRecomputeFlag(state)).to.equal(true);
    const identity = getWavetrakIdentity();
    expect(identity.userId).to.be.equal('userId');
    expect(identity.entitlements).to.deep.equal([SL_METERED]);
    expect(identity.type).to.be.equal(LOGGED_IN);
    expect(identity.rl_forced).to.be.equal(true);
    expect(identity.email).to.be.equal(values.email);
  });

  it(`should handle create account errors with a bad body`, async () => {
    const store = createStore(storeState);
    const { wrapper } = mountWithRedux(CreateAccount, undefined, { store });

    postUserStub.rejects({ message: 'You need a body with email and password.' });

    const { deviceId, deviceType } = deviceInfo();

    await wrapper.find('CreateAccountForm').prop('onSubmit')(values);

    expect(postUserStub).to.have.been.calledOnceWith(false, {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      password: values.password,
      settings: {
        receivePromotions: true,
      },
      brand: 'sl',
      client_id: clientId,
      device_id: deviceId,
      device_type: deviceType,
    });
    expect(getUserDetailsStub).to.not.have.been.called();
    expect(getUserEntitlementsStub).to.not.have.been.called();
    expect(getUserFavoritesStub).to.not.have.been.called();
    expect(getUserSettingsStub).to.not.have.been.called();
    expect(patchMeteringSurfCheckStub).to.not.have.been.called();

    wrapper.update();

    const createAccountForm = wrapper.find('CreateAccountForm');
    expect(createAccountForm.prop('error')).to.be.equal('You need a body with email and password.');

    const state = store.getState();
    expect(getMeterModalRecomputeFlag(state)).to.equal(false);
  });

  it('should gracefully handle failed meter creation', async () => {
    const store = createStore({
      backplane: {
        meter: {
          anonymousId: 'anonymousId',
        },
        user: {
          countryCode: 'countryCode',
          location: { time_zone: 'timezone' },
        },
        state: {
          shouldRecomputeMeterModal: false,
          meteringEnabled: true,
        },
      },
    });
    const { wrapper } = mountWithRedux(CreateAccount, undefined, {
      store,
    });

    postUserStub.resolves({
      accessToken: 'accessToken',
      userId: 'userId',
    });
    postMeterStub.rejects({ message: 'some error' });
    getUserDetailsStub.resolves(details);
    getUserEntitlementsStub.resolves({ entitlements, promotions: [] });
    getUserSettingsStub.resolves(settings);
    getUserFavoritesStub.resolves({ data: { favorites } });

    await wrapper.find('CreateAccountForm').prop('onSubmit')(values);

    expect(postMeterStub).to.have.been.calledOnceWithExactly({
      accessToken: 'accessToken',
      anonymousId: 'anonymousId',
      timezone: 'timezone',
      countryCode: 'countryCode',
    });
    const state = store.getState();

    expect(getMeter(state)).to.equal(null);
    expect(getMeterModalRecomputeFlag(state)).to.equal(true);
    expect(patchMeteringSurfCheckStub).to.not.have.been.called();
  });
});
