import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDeviceSize, withContentCards } from '@surfline/quiver-react';
import {
  trackEvent,
  trackContentCardClick,
  trackContentCardImpressions,
} from '@surfline/web-common';
import CreateAccount from '../CreateAccount';
import {
  LEARN_MORE,
  SIGN_IN,
  CLICKED_POPUP,
  FORCED_REGISTRATION,
  NAVIGATED_TO_SIGN_IN,
  NAVIGATED_TO_LEARN_MORE,
  BrazeContentCards,
} from '../../../../common/constants';

/**
 * @param {SIGN_IN | LEARN_MORE} form
 */
const trackClickedNavigate = (form) =>
  trackEvent(CLICKED_POPUP, {
    action: form === SIGN_IN ? NAVIGATED_TO_SIGN_IN : NAVIGATED_TO_LEARN_MORE,
    category: FORCED_REGISTRATION,
  });

const Home = ({ setForm, card }) => {
  const { isMobile } = useDeviceSize();
  const [rendered, setRendered] = useState(false);
  const headerText = card?.extras?.header__text;
  const subHeaderText = isMobile
    ? card?.extras?.subheader__text__mobile
    : card?.extras?.subheader__text;

  const handleNavigation = useCallback(
    (formName) => {
      trackClickedNavigate(formName);
      setForm(formName);
      trackContentCardClick(card);
    },
    [setForm, card],
  );

  if (!rendered && card?.extras) {
    setRendered(true);
    trackContentCardImpressions([card]);
  }

  return card?.extras ? (
    <div style={{ width: '100%' }} data-scroll-lock-scrollable="true">
      <div className="sl-meter-bottom-sheet-home__container">
        <h1 className="sl-meter-bottom-sheet-home__header">{headerText}</h1>
        <div className="sl-meter-bottom-sheet-home__subheader">
          <h4>{subHeaderText}</h4>
          <button
            style={{ paddingLeft: '4px' }}
            type="button"
            onClick={() => handleNavigation(LEARN_MORE)}
          >
            {card.extras.href__text}
          </button>
        </div>
        <CreateAccount />
        <div className="sl-meter-bottom-sheet-home__actions">
          <div className="sl-meter-bottom-sheet-home__signin__label">
            Already have an account?
            <button
              type="button"
              onClick={() => handleNavigation(SIGN_IN)}
              className="sl-meter-bottom-sheet-home__signin__button"
            >
              Sign In.
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

Home.propTypes = {
  setForm: PropTypes.func.isRequired,
  card: PropTypes.shape({
    name: PropTypes.string,
    extras: PropTypes.shape({
      href__text: PropTypes.string,
      header__text: PropTypes.string,
      subheader__text: PropTypes.string,
      subheader__text__mobile: PropTypes.string,
    }),
    title: PropTypes.string,
  }),
  defaultCard: PropTypes.shape({
    extras: PropTypes.shape({
      href__text: PropTypes.string,
      header__text: PropTypes.string,
      subheader__text: PropTypes.string,
      subheader__text__mobile: PropTypes.string,
    }),
  }),
};

Home.defaultProps = {
  card: { name: BrazeContentCards.REGISTRATION_WALL, extras: null },
  defaultCard: {
    extras: {
      href__text: 'Learn More',
      header__text: 'Keep Checking the Surf with a Free Account',
      subheader__text: 'Registered users get more access to ad-free cams and long-range forecasts.',
      subheader__text__mobile: 'Registered users get more access',
    },
  },
};

export default withContentCards(Home);
