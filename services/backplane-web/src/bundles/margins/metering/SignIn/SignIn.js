import React from 'react';
import { LoginForm, useDeviceSize } from '@surfline/quiver-react';

import { useSignIn } from './useSignIn';

/**
 * @function SignIn
 * @return {JSX.Element}
 */
const SignIn = () => {
  const { signIn, error, loading } = useSignIn();
  const { isMobile } = useDeviceSize();
  const title = isMobile ? 'Sign In' : 'Sign into your Surfline Account';
  return (
    <div className="sl-backplane-sign-in" data-scroll-lock-scrollable="true">
      <div className="sl-meter-bottom-sheet__title">{title}</div>
      <LoginForm onSubmit={signIn} error={error} loading={loading} />
    </div>
  );
};

export default SignIn;
