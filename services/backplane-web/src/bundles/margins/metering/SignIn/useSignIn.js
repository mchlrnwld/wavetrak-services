import { useDispatch, useStore } from 'react-redux';
import { useCallback, useState } from 'react';
import {
  LOGGED_IN,
  TABLET_LARGE_WIDTH,
  deviceInfo,
  getEntitlements,
  getUserPremiumStatus,
  getWavetrakIdentity,
  getWindow,
  setWavetrakIdentity,
  trackEvent,
} from '@surfline/web-common';
import encodeFormValues from '../../../../utils/encodeFormValues';
import { generateToken, authoriseAccessToken } from '../../../../api/user';
import { setupUser, getMeter, triggerMeterModalRecompute } from '../../../../actions';
import {
  getUserDetailsError,
  getUserEntitlementsError,
  useUserSelector,
} from '../../../../selectors/user';
import { useMeterSelector } from '../../../../selectors/metering';
import {
  CLICKED_POPUP,
  CLICKED_SIGN_IN,
  FORCED_REGISTRATION,
  SIGNED_IN,
} from '../../../../common/constants';

export const useSignIn = () => {
  const dispatch = useDispatch();
  const store = useStore();
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const user = useUserSelector();
  const meter = useMeterSelector();

  const { anonymousId } = meter || {};
  const { location, countryCode } = user;
  const { time_zone: timezone } = location || {};

  const signIn = useCallback(
    /**
     * @param {Object} values
     * @param {string} values.password
     * @param {string} values.email
     * @param {boolean?} values.staySignedIn
     * @return {Promise<void>}
     */
    async (values) => {
      try {
        trackEvent(CLICKED_POPUP, {
          action: CLICKED_SIGN_IN,
          category: FORCED_REGISTRATION,
        });

        setLoading(true);
        const { staySignedIn } = values;
        const { deviceId, deviceType } = deviceInfo();
        const encodedFormString = encodeFormValues({
          grant_type: 'password',
          username: values.email,
          password: values.password,
          forced: true,
          device_id: deviceId,
          device_type: deviceType,
        });

        const { accessToken } = await generateToken(!!staySignedIn, encodedFormString);
        const { id: userId } = await authoriseAccessToken(accessToken);

        await dispatch(setupUser({ accessToken }));

        // If details or entitlements failed to fetch, we need to fail the sign-in
        // since these values are hard dependencies for determining the user experience
        const state = store.getState();
        const detailsError = getUserDetailsError(state);
        const entitlementsError = getUserEntitlementsError(state);

        if (detailsError || entitlementsError) {
          throw new Error('Something went wrong loading the user.');
        }

        await dispatch(
          getMeter({
            accessToken,
            createIfNotFound: true,
            countryCode,
            anonymousId,
            timezone,
            performMeterSurfCheck: true,
          }),
        );

        // Retrieve the entitlements, since they may have been updated by
        // the `getMeter` action (calling `createMeter` under the hood in some cases)
        const entitlements = getEntitlements(store.getState());
        const isPremium = getUserPremiumStatus(store.getState());
        const win = getWindow();

        win?.analytics?.identify(userId);
        trackEvent(SIGNED_IN, {
          pageName: win?.location?.pathname,
          location: FORCED_REGISTRATION,
          platform: 'web',
          isMobileView: win?.innerWidth <= TABLET_LARGE_WIDTH,
        });

        // we'll now be logged in so we want to update the identity stored on the window
        setWavetrakIdentity({
          ...getWavetrakIdentity(),
          userId,
          entitlements,
          email: values.email,
          rl_forced: !isPremium,
          type: LOGGED_IN,
        });

        setLoading(false);
        dispatch(triggerMeterModalRecompute());
      } catch (err) {
        setLoading(false);
        setError(err.message);
      }
    },
    [dispatch, timezone, countryCode, store, anonymousId],
  );
  return { signIn, loading, error };
};
