import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import {
  getUserDetails,
  getUserSettings,
  getEntitlements,
  getUserFavorites,
  deviceInfo,
  getMeter,
  getWavetrakIdentity,
  SL_METERED,
  LOGGED_IN,
} from '@surfline/web-common';
import { mountWithRedux } from '../../../../utils/testUtils';
import SignIn from './SignIn';
import * as userApi from '../../../../api/user';
import * as meterApi from '../../../../api/meter';
import createStore from '../../../../stores';
import encodeFormValues from '../../../../utils/encodeFormValues';
import { getMeterModalRecomputeFlag } from '../../../../selectors/state';

describe('SignIn', () => {
  /** @type {sinon.SinonStub} */
  let generateTokenStub;

  /** @type {sinon.SinonStub} */
  let authoriseAccessTokenStub;

  /** @type {sinon.SinonStub} */
  let getUserDetailsStub;

  /** @type {sinon.SinonStub} */
  let getUserEntitlementsStub;

  /** @type {sinon.SinonStub} */
  let getUserSettingsStub;

  /** @type {sinon.SinonStub} */
  let getUserFavoritesStub;

  /** @type {sinon.SinonStub} */
  let getUserSubscriptionWarningsStub;

  /** @type {sinon.SinonStub} */
  let getMeterStub;

  /** @type {sinon.SinonStub} */
  let postMeterStub;

  /** @type {sinon.SinonStub} */
  let patchMeteringSurfCheckStub;

  beforeEach(() => {
    generateTokenStub = sinon.stub(userApi, 'generateToken');
    authoriseAccessTokenStub = sinon.stub(userApi, 'authoriseAccessToken');
    getUserDetailsStub = sinon.stub(userApi, 'getUserDetails');
    getUserEntitlementsStub = sinon.stub(userApi, 'getUserEntitlements');
    getUserSettingsStub = sinon.stub(userApi, 'getUserSettings');
    getUserFavoritesStub = sinon.stub(userApi, 'getUserFavorites');
    getUserSubscriptionWarningsStub = sinon.stub(userApi, 'getUserSubscriptionWarnings');
    getMeterStub = sinon.stub(meterApi, 'getMeter');
    postMeterStub = sinon.stub(meterApi, 'postMeter');
    patchMeteringSurfCheckStub = sinon.stub(meterApi, 'patchMeteringSurfCheck');
  });

  afterEach(() => {
    generateTokenStub.restore();
    authoriseAccessTokenStub.restore();
    getUserDetailsStub.restore();
    getUserEntitlementsStub.restore();
    getUserSettingsStub.restore();
    getUserFavoritesStub.restore();
    getMeterStub.restore();
    postMeterStub.restore();
    patchMeteringSurfCheckStub.restore();
    getUserSubscriptionWarningsStub.restore();
  });

  const accessToken = 'accessToken';
  const timezone = 'timezone';
  const countryCode = 'countryCode';
  const anonymousId = 'anonymousId';

  const meter = deepFreeze({
    meterRemaining: 2,
    treatmentState: 'on',
    anonymousId,
  });
  const favorites = deepFreeze([{ _id: '1' }]);
  const details = deepFreeze({
    firstName: 'Lucy',
    lastName: 'Goosey',
    email: 'lucybear@gmail.com',
    _id: 'a',
    isEmailVerified: true,
    location: { time_zone: timezone },
    countryCode,
  });
  const entitlements = deepFreeze([]);
  const settings = deepFreeze({ settings: true });
  const storeState = {
    backplane: { state: { meteringEnabled: true, shouldRecomputeMeterModal: false } },
  };

  const { deviceId, deviceType } = deviceInfo();

  it(`should render the LoginForm`, () => {
    const { wrapper } = mountWithRedux(SignIn);
    expect(wrapper.find('.sl-meter-bottom-sheet__title').text()).to.be.equal(
      'Sign into your Surfline Account',
    );
    expect(wrapper.find('LoginForm')).to.have.length(1);
  });

  it(`should handle signing in`, async () => {
    const store = createStore(storeState);
    const { wrapper } = mountWithRedux(SignIn, undefined, { store });
    const meterRemaining = 1;

    generateTokenStub.resolves({ accessToken: 'accessToken' });
    authoriseAccessTokenStub.resolves({ id: 'userId' });
    getUserDetailsStub.resolves(details);
    getUserEntitlementsStub.resolves({ entitlements: [SL_METERED], promotions: [] });
    getUserSettingsStub.resolves(settings);
    getUserFavoritesStub.resolves({ data: { favorites } });
    getMeterStub.resolves(meter);
    patchMeteringSurfCheckStub.resolves({ meterRemaining });
    const values = {
      email: 'jmonson@surfline.com',
      password: 'test1234',
      staySignedIn: 'true',
    };

    await wrapper.find('LoginForm').prop('onSubmit')(values);

    expect(generateTokenStub).to.have.been.calledOnceWithExactly(
      true,
      encodeFormValues({
        grant_type: 'password',
        username: values.email,
        password: values.password,
        forced: true,
        device_id: deviceId,
        device_type: deviceType,
      }),
    );
    expect(authoriseAccessTokenStub).to.have.been.calledOnceWithExactly('accessToken');
    expect(getUserDetailsStub).to.have.been.calledOnce();
    expect(getUserEntitlementsStub).to.have.been.calledOnce();
    expect(getUserFavoritesStub).to.have.been.calledOnce();
    expect(getUserSettingsStub).to.have.been.calledOnce();
    expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
    expect(patchMeteringSurfCheckStub).to.have.been.calledOnceWithExactly(accessToken);

    const state = store.getState();

    expect(getUserDetails(state)).to.deep.equal({ ...details, password: undefined });
    expect(getUserSettings(state)).to.deep.equal(settings);
    expect(getEntitlements(state)).to.deep.equal([SL_METERED]);
    expect(getUserFavorites(state)).to.deep.equal(favorites);
    expect(getMeter(state)).to.deep.equal({
      ...{
        ...meter,
        meterRemaining,
        showPaywall: meter?.meterRemaining < 0,
      },
    });
    expect(getMeterModalRecomputeFlag(state)).to.be.equal(true);

    const identity = getWavetrakIdentity();
    expect(identity.userId).to.be.equal('userId');
    expect(identity.entitlements).to.deep.equal([SL_METERED]);
    expect(identity.type).to.be.equal(LOGGED_IN);
    expect(identity.rl_forced).to.be.equal(true);
    expect(identity.email).to.be.equal(values.email);
  });

  it(`should handle sign in errors`, async () => {
    const store = createStore(storeState);
    const { wrapper } = mountWithRedux(SignIn, undefined, { store });

    generateTokenStub.rejects({ message: 'error' });

    const values = {
      email: 'jmonson@surfline.com',
      password: 'test1234',
    };

    await wrapper.find('LoginForm').prop('onSubmit')(values);

    expect(generateTokenStub).to.have.been.calledOnceWith(
      false,
      encodeFormValues({
        grant_type: 'password',
        username: values.email,
        password: values.password,
        forced: true,
        device_id: deviceId,
        device_type: deviceType,
      }),
    );
    expect(authoriseAccessTokenStub).to.not.have.not.been.called();
    expect(getUserDetailsStub).to.not.have.been.called();
    expect(getUserEntitlementsStub).to.not.have.been.called();
    expect(getUserFavoritesStub).to.not.have.been.called();
    expect(getUserSettingsStub).to.not.have.been.called();
    expect(patchMeteringSurfCheckStub).to.not.have.been.called();

    wrapper.update();

    const loginForm = wrapper.find('LoginForm');
    expect(loginForm.prop('error')).to.be.equal('error');
    const state = store.getState();
    expect(getMeterModalRecomputeFlag(state)).to.be.equal(false);
  });

  it(`should show an error if details fail to fetch`, async () => {
    const store = createStore(storeState);
    const { wrapper } = mountWithRedux(SignIn, undefined, { store });

    getUserDetailsStub.rejects({ message: 'error' });
    generateTokenStub.resolves({ accessToken: 'accessToken' });
    authoriseAccessTokenStub.resolves({ id: 'userId' });

    const values = {
      email: 'jmonson@surfline.com',
      password: 'test1234',
    };

    await wrapper.find('LoginForm').prop('onSubmit')(values);

    expect(generateTokenStub).to.have.been.calledOnceWith(
      false,
      encodeFormValues({
        grant_type: 'password',
        username: values.email,
        password: values.password,
        forced: true,
        device_id: deviceId,
        device_type: deviceType,
      }),
    );
    expect(authoriseAccessTokenStub).to.have.been.calledOnceWithExactly('accessToken');
    expect(getUserDetailsStub).to.have.been.calledOnce();
    expect(getUserEntitlementsStub).to.have.been.calledOnce();
    expect(getUserFavoritesStub).to.have.been.calledOnce();
    expect(getUserSettingsStub).to.have.been.calledOnce();
    expect(patchMeteringSurfCheckStub).to.have.not.been.called();

    wrapper.update();

    const loginForm = wrapper.find('LoginForm');
    expect(loginForm.prop('error')).to.be.equal('Something went wrong loading the user.');
    const state = store.getState();
    expect(getMeterModalRecomputeFlag(state)).to.be.equal(false);
  });

  it(`should show an error if entitlements fail to fetch`, async () => {
    const store = createStore(storeState);
    const { wrapper } = mountWithRedux(SignIn, undefined, { store });

    getUserEntitlementsStub.rejects({ message: 'error' });
    generateTokenStub.resolves({ accessToken: 'accessToken' });
    authoriseAccessTokenStub.resolves({ id: 'userId' });

    const values = {
      email: 'jmonson@surfline.com',
      password: 'test1234',
    };

    await wrapper.find('LoginForm').prop('onSubmit')(values);

    expect(generateTokenStub).to.have.been.calledOnceWith(
      false,
      encodeFormValues({
        grant_type: 'password',
        username: values.email,
        password: values.password,
        forced: true,
        device_id: deviceId,
        device_type: deviceType,
      }),
    );
    expect(authoriseAccessTokenStub).to.have.been.calledOnceWithExactly(accessToken);
    expect(getUserDetailsStub).to.have.been.calledOnce();
    expect(getUserEntitlementsStub).to.have.been.calledOnce();
    expect(getUserFavoritesStub).to.have.been.calledOnce();
    expect(getUserSettingsStub).to.have.been.calledOnce();
    expect(patchMeteringSurfCheckStub).to.have.not.been.called();

    wrapper.update();

    const loginForm = wrapper.find('LoginForm');
    expect(loginForm.prop('error')).to.be.equal('Something went wrong loading the user.');
    const state = store.getState();
    expect(getMeterModalRecomputeFlag(state)).to.be.equal(false);
  });

  it('should create a meter if one does not exist', async () => {
    const store = createStore({
      backplane: {
        meter: {
          anonymousId: 'anonymousId',
        },
        user: { location: { time_zone: timezone }, countryCode },
        state: { meteringEnabled: true },
      },
    });
    const { wrapper } = mountWithRedux(SignIn, undefined, { store });
    const meterRemaining = 1;

    generateTokenStub.resolves({ accessToken: 'accessToken' });
    authoriseAccessTokenStub.resolves({ id: 'userId' });
    getUserDetailsStub.resolves(details);
    getUserEntitlementsStub.resolves({ entitlements, promotions: [] });
    getUserSettingsStub.resolves(settings);
    getUserFavoritesStub.resolves({ data: { favorites } });
    getMeterStub.rejects({ statusCode: 404 });
    postMeterStub.resolves(meter);
    patchMeteringSurfCheckStub.resolves({ meterRemaining });

    const values = {
      email: 'jmonson@surfline.com',
      password: 'test1234',
      staySignedIn: 'true',
    };

    await wrapper.find('LoginForm').prop('onSubmit')(values);

    expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
    expect(postMeterStub).to.have.been.calledOnceWithExactly({
      accessToken,
      anonymousId,
      timezone,
      countryCode,
    });
    expect(patchMeteringSurfCheckStub).to.have.been.calledOnceWithExactly(accessToken);

    const state = store.getState();

    expect(getMeter(state)).to.deep.equal({
      ...meter,
      meterRemaining,
      showPaywall: meter?.meterRemaining < 0,
    });
    expect(getMeterModalRecomputeFlag(state)).to.be.equal(true);
  });

  it('should gracefully handle failed meter retrieval', async () => {
    const store = createStore({
      backplane: {
        meter: {
          anonymousId: 'anonymousId',
        },
        state: {
          meteringEnabled: true,
        },
      },
    });
    const { wrapper } = mountWithRedux(SignIn, undefined, { store });

    generateTokenStub.resolves({ accessToken: 'accessToken' });
    authoriseAccessTokenStub.resolves({ id: 'userId' });
    getUserDetailsStub.resolves(details);
    getUserEntitlementsStub.resolves({ entitlements, promotions: [] });
    getUserSettingsStub.resolves(settings);
    getUserFavoritesStub.resolves({ data: { favorites } });
    getMeterStub.rejects({ message: 'error' });

    const values = {
      email: 'jmonson@surfline.com',
      password: 'test1234',
      staySignedIn: 'true',
    };

    await wrapper.find('LoginForm').prop('onSubmit')(values);

    expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
    const state = store.getState();

    expect(getMeter(state)).to.equal(null);
    expect(patchMeteringSurfCheckStub).to.not.have.been.called();
    expect(getMeterModalRecomputeFlag(state)).to.be.equal(true);
  });
});
