import { expect } from 'chai';
import Footer from './Footer';
import { mountWithRedux } from '../../utils/testUtils';

describe('Footer', () => {
  it(`should render the SurflineFooter`, () => {
    const { wrapper } = mountWithRedux(Footer, undefined);
    expect(wrapper.find('SurflineFooter'));
  });
});
