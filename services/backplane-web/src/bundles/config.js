import Header from './margins/Header';
import Footer from './margins/Footer';
import RegWall from './margins/RegWall';

export default {
  margins: {
    entryPoint: 'margins/entry.js',
    components: {
      header: Header,
      footer: Footer,
      regwall: RegWall,
    },
  },
};
