import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import deepFreeze from 'deep-freeze';
import thunk from 'redux-thunk';
import {
  fetchUserDetails,
  fetchUserEntitlements,
  fetchUserSettings,
  fetchUserFavorites,
  setupUser,
  fetchUserSubscriptionWarnings,
  setUserMetadata,
  setMeteringEntitlement,
} from './user';
import * as userApi from '../api/user';

describe('actions / user', () => {
  const mockStore = configureStore([thunk]);
  /** @type {sinon.SinonStub} */
  let getUserDetailsStub;
  /** @type {sinon.SinonStub} */
  let getUserEntitlementsStub;
  /** @type {sinon.SinonStub} */
  let getUserSettingsStub;
  /** @type {sinon.SinonStub} */
  let getUserFavoritesStub;
  /** @type {sinon.SinonStub} */
  let getUserSubscriptionWarningsStub;
  /** @type {sinon.SinonStub} */
  let serverSideFetchUserFavoritesStub;
  /** @type {sinon.SinonStub} */
  let serverSideFetchUserDetailsStub;
  /** @type {sinon.SinonStub} */
  let serverSideFetchUserEntitlementsStub;
  /** @type {sinon.SinonStub} */
  let serverSideFetchUserSettingsStub;
  /** @type {sinon.SinonStub} */
  let serverSideFetchSubscriptionWarningsStub;

  const userId = 'userId';
  const clientIp = 'clientIp';
  const countryIso = 'countryIso';
  const location = { latitude: 1, longitude: 1 };

  beforeEach(() => {
    getUserDetailsStub = sinon.stub(userApi, 'getUserDetails');
    getUserEntitlementsStub = sinon.stub(userApi, 'getUserEntitlements');
    getUserSettingsStub = sinon.stub(userApi, 'getUserSettings');
    getUserFavoritesStub = sinon.stub(userApi, 'getUserFavorites');
    getUserSubscriptionWarningsStub = sinon.stub(userApi, 'getUserSubscriptionWarnings');
    serverSideFetchUserDetailsStub = sinon.stub(userApi, 'serverSideFetchUserDetails');
    serverSideFetchUserEntitlementsStub = sinon.stub(userApi, 'serverSideFetchUserEntitlements');
    serverSideFetchUserSettingsStub = sinon.stub(userApi, 'serverSideFetchUserSettings');
    serverSideFetchUserFavoritesStub = sinon.stub(userApi, 'serverSideFetchUserFavorites');
    serverSideFetchSubscriptionWarningsStub = sinon.stub(
      userApi,
      'serverSideFetchSubscriptionWarnings',
    );
  });

  afterEach(() => {
    getUserDetailsStub.restore();
    getUserEntitlementsStub.restore();
    getUserSettingsStub.restore();
    getUserFavoritesStub.restore();
    getUserSubscriptionWarningsStub.restore();
    serverSideFetchUserDetailsStub.restore();
    serverSideFetchUserEntitlementsStub.restore();
    serverSideFetchUserSettingsStub.restore();
    serverSideFetchUserFavoritesStub.restore();
    serverSideFetchSubscriptionWarningsStub.restore();
  });

  describe('setUserMetadata', () => {
    it(`should dispatch ${setUserMetadata.type}`, () => {
      const store = mockStore();

      store.dispatch(setUserMetadata({ countryCode: 'countryCode' }));

      expect(store.getActions()).to.containSubset([
        { type: setUserMetadata.type.toString(), payload: { countryCode: 'countryCode' } },
      ]);
    });
  });

  describe('setMeteringEntitlement', () => {
    it(`should dispatch ${setMeteringEntitlement.type}`, () => {
      const store = mockStore();

      store.dispatch(setMeteringEntitlement());

      expect(store.getActions()).to.containSubset([
        { type: setMeteringEntitlement.type.toString() },
      ]);
    });
  });

  describe('fetchUserDetails', () => {
    it(`should handle ${fetchUserDetails.fulfilled} - client side`, async () => {
      const store = mockStore();
      const details = deepFreeze({ _id: '1' });
      getUserDetailsStub.resolves(details);

      await store.dispatch(fetchUserDetails());

      expect(store.getActions()).to.containSubset([
        { type: fetchUserDetails.pending.toString() },
        { type: fetchUserDetails.fulfilled.toString(), payload: { details } },
      ]);

      expect(getUserDetailsStub).to.have.been.calledOnceWithExactly();
    });

    it(`should handle ${fetchUserDetails.fulfilled} - server side`, async () => {
      const store = mockStore();
      const details = deepFreeze({ _id: '1' });
      serverSideFetchUserDetailsStub.resolves(details);

      await store.dispatch(fetchUserDetails({ userId, serverSide: true }));

      expect(store.getActions()).to.containSubset([
        { type: fetchUserDetails.pending.toString() },
        { type: fetchUserDetails.fulfilled.toString(), payload: { details } },
      ]);

      expect(serverSideFetchUserDetailsStub).to.have.been.calledOnceWithExactly(userId);
    });
  });

  describe('fetchUserFavorites', () => {
    it(`should handle ${fetchUserFavorites.fulfilled} - client side`, async () => {
      const store = mockStore();
      const favorites = deepFreeze({ _id: '1' });
      getUserFavoritesStub.resolves({ data: { favorites } });

      await store.dispatch(fetchUserFavorites());

      expect(store.getActions()).to.containSubset([
        { type: fetchUserFavorites.pending.toString() },
        { type: fetchUserFavorites.fulfilled.toString(), payload: { favorites } },
      ]);

      expect(getUserFavoritesStub).to.have.been.calledOnceWithExactly();
    });

    it(`should handle ${fetchUserFavorites.fulfilled} - server side`, async () => {
      const store = mockStore();
      const favorites = deepFreeze({ _id: '1' });
      serverSideFetchUserFavoritesStub.resolves({ data: { favorites } });

      await store.dispatch(
        fetchUserFavorites({
          userId,
          clientIp,
          countryIso,
          location,
          serverSide: true,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: fetchUserFavorites.pending.toString() },
        { type: fetchUserFavorites.fulfilled.toString(), payload: { favorites } },
      ]);

      expect(serverSideFetchUserFavoritesStub).to.have.been.calledOnceWithExactly(
        userId,
        countryIso,
        clientIp,
        location,
      );
    });

    it(`should handle ${fetchUserFavorites.fulfilled} for anonymous user - server side`, async () => {
      const store = mockStore();
      const favorites = deepFreeze({ _id: '1' });
      serverSideFetchUserFavoritesStub.resolves({ data: { favorites } });

      await store.dispatch(
        fetchUserFavorites({
          clientIp,
          countryIso,
          location,
          serverSide: true,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: fetchUserFavorites.pending.toString() },
        { type: fetchUserFavorites.fulfilled.toString(), payload: { favorites } },
      ]);

      expect(serverSideFetchUserFavoritesStub).to.have.been.calledOnceWithExactly(
        undefined,
        countryIso,
        clientIp,
        location,
      );
    });
  });

  describe('fetchUserSettings', () => {
    it(`should handle ${fetchUserSettings.fulfilled} - client side`, async () => {
      const store = mockStore();
      const settings = deepFreeze({ units: {} });
      getUserSettingsStub.resolves(settings);

      await store.dispatch(fetchUserSettings());

      expect(store.getActions()).to.containSubset([
        { type: fetchUserSettings.pending.toString() },
        { type: fetchUserSettings.fulfilled.toString(), payload: { settings } },
      ]);

      expect(getUserSettingsStub).to.have.been.calledOnceWithExactly();
    });

    it(`should handle ${fetchUserSettings.fulfilled} - server side`, async () => {
      const store = mockStore();
      const settings = deepFreeze({ units: {} });
      serverSideFetchUserSettingsStub.resolves(settings);

      await store.dispatch(
        fetchUserSettings({
          userId,
          clientIp,
          countryIso,
          serverSide: true,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: fetchUserSettings.pending.toString() },
        { type: fetchUserSettings.fulfilled.toString(), payload: { settings } },
      ]);

      expect(serverSideFetchUserSettingsStub).to.have.been.calledOnceWithExactly(
        userId,
        countryIso,
        clientIp,
      );
    });
    it(`should handle ${fetchUserSettings.fulfilled} for anonymous user - server side`, async () => {
      const store = mockStore();
      const settings = deepFreeze({ units: {} });
      serverSideFetchUserSettingsStub.resolves(settings);

      await store.dispatch(fetchUserSettings({ clientIp, countryIso, serverSide: true }));

      expect(store.getActions()).to.containSubset([
        { type: fetchUserSettings.pending.toString() },
        { type: fetchUserSettings.fulfilled.toString(), payload: { settings } },
      ]);

      expect(serverSideFetchUserSettingsStub).to.have.been.calledOnceWithExactly(
        undefined,
        countryIso,
        clientIp,
      );
    });
  });

  describe('fetchUserEntitlements', () => {
    it(`should handle ${fetchUserEntitlements.fulfilled} - client side`, async () => {
      const store = mockStore();
      const entitlements = deepFreeze({
        entitlements: [],
        promotions: [],
        legacyEntitlements: [],
        hasNewAccount: true,
      });
      getUserEntitlementsStub.resolves(entitlements);

      await store.dispatch(fetchUserEntitlements());

      expect(store.getActions()).to.containSubset([
        { type: fetchUserEntitlements.pending.toString() },
        { type: fetchUserEntitlements.fulfilled.toString(), payload: { entitlements } },
      ]);

      expect(getUserEntitlementsStub).to.have.been.calledOnceWithExactly();
    });

    it(`should handle ${fetchUserEntitlements.fulfilled} - Server Side`, async () => {
      const store = mockStore();
      const entitlements = deepFreeze({ entitlements: [], promotions: [] });
      serverSideFetchUserEntitlementsStub.resolves(entitlements);

      await store.dispatch(fetchUserEntitlements({ userId, serverSide: true }));

      expect(store.getActions()).to.containSubset([
        { type: fetchUserEntitlements.pending.toString() },
        { type: fetchUserEntitlements.fulfilled.toString(), payload: { entitlements } },
      ]);

      expect(serverSideFetchUserEntitlementsStub).to.have.been.calledOnceWithExactly(userId);
    });
  });

  describe('fetchUserSubscriptionWarnings', () => {
    it(`should handle ${fetchUserSubscriptionWarnings.fulfilled} - client side`, async () => {
      const store = mockStore();
      const warnings = deepFreeze({ alert: false, message: 'No payment alerts found.' });
      getUserSubscriptionWarningsStub.resolves(warnings);

      await store.dispatch(fetchUserSubscriptionWarnings());
      expect(store.getActions()).to.containSubset([
        { type: fetchUserSubscriptionWarnings.pending.toString() },
        { type: fetchUserSubscriptionWarnings.fulfilled.toString(), payload: { warnings } },
      ]);

      expect(getUserSubscriptionWarningsStub).to.have.been.calledOnceWithExactly(undefined);
    });

    it(`should handle ${fetchUserSubscriptionWarnings.fulfilled} - Server Side`, async () => {
      const store = mockStore();
      const warnings = deepFreeze({ alert: false, message: 'No payment alerts found.' });
      serverSideFetchSubscriptionWarningsStub.resolves(warnings);

      await store.dispatch(fetchUserSubscriptionWarnings({ userId, serverSide: true }));

      expect(store.getActions()).to.containSubset([
        { type: fetchUserSubscriptionWarnings.pending.toString() },
        { type: fetchUserSubscriptionWarnings.fulfilled.toString(), payload: { warnings } },
      ]);

      expect(serverSideFetchSubscriptionWarningsStub).to.have.been.calledOnceWithExactly(userId);
    });
  });

  describe('setupUser', () => {
    it(`should handle ${setupUser.fulfilled} for an anonymous user`, async () => {
      const store = mockStore();

      const settings = deepFreeze({ units: {} });
      const favorites = deepFreeze({ _id: '1' });

      getUserSettingsStub.resolves(settings);
      getUserFavoritesStub.resolves({ data: { favorites } });

      await store.dispatch(setupUser());

      expect(store.getActions()).to.containSubset([
        { type: setupUser.pending.toString() },
        { type: fetchUserSettings.pending.toString() },
        { type: fetchUserSettings.fulfilled.toString(), payload: { settings } },
        { type: fetchUserFavorites.pending.toString() },
        { type: fetchUserFavorites.fulfilled.toString(), payload: { favorites } },
        { type: setupUser.fulfilled.toString() },
      ]);

      expect(getUserEntitlementsStub).to.not.have.been.called();
      expect(getUserDetailsStub).to.not.have.been.called();
      expect(getUserSubscriptionWarningsStub).to.not.have.been.called();
      expect(getUserFavoritesStub).to.have.been.calledOnce();
      expect(getUserSettingsStub).to.have.been.calledOnce();
    });

    it(`should handle ${setupUser.fulfilled} for a logged in user`, async () => {
      const store = mockStore();

      const entitlements = deepFreeze({ entitlements: [], promotions: [] });
      const settings = deepFreeze({ units: {} });
      const favorites = deepFreeze({ _id: '1' });
      const details = deepFreeze({ _id: '1' });
      const warnings = deepFreeze({ alert: false, message: 'No payment alerts found.' });

      getUserDetailsStub.resolves(details);
      getUserSettingsStub.resolves(settings);
      getUserFavoritesStub.resolves({ data: { favorites } });
      getUserEntitlementsStub.resolves(entitlements);
      getUserSubscriptionWarningsStub.resolves(warnings);

      await store.dispatch(setupUser({ accessToken: '123' }));

      expect(store.getActions()).to.containSubset([
        { type: setupUser.pending.toString() },
        { type: fetchUserEntitlements.pending.toString() },
        { type: fetchUserSettings.pending.toString() },
        { type: fetchUserFavorites.pending.toString() },
        { type: fetchUserDetails.pending.toString() },
        { type: fetchUserEntitlements.fulfilled.toString(), payload: { entitlements } },
        { type: fetchUserSettings.fulfilled.toString(), payload: { settings } },
        { type: fetchUserFavorites.fulfilled.toString(), payload: { favorites } },
        { type: fetchUserDetails.fulfilled.toString(), payload: { details } },
        { type: fetchUserSubscriptionWarnings.fulfilled.toString(), payload: { warnings } },
        { type: setupUser.fulfilled.toString() },
      ]);

      expect(getUserEntitlementsStub).to.have.been.calledOnce();
      expect(getUserDetailsStub).to.have.been.calledOnce();
      expect(getUserFavoritesStub).to.have.been.calledOnce();
      expect(getUserSettingsStub).to.have.been.calledOnce();
      expect(getUserSubscriptionWarningsStub).to.have.been.calledOnce();
    });

    it(`should gracefully fail ${setupUser.fulfilled} child actions`, async () => {
      const store = mockStore();

      const entitlements = deepFreeze({ entitlements: [], promotions: [] });
      const favorites = deepFreeze({ _id: '1' });
      const details = deepFreeze({ _id: '1' });
      const warnings = deepFreeze({ alert: false, message: 'No payment alerts found.' });

      getUserSettingsStub.rejects({ message: 'error' });
      getUserFavoritesStub.resolves({ data: { favorites } });
      getUserEntitlementsStub.resolves(entitlements);
      getUserDetailsStub.resolves(details);
      getUserSubscriptionWarningsStub.resolves(warnings);

      await store.dispatch(setupUser({ userId: '1' }));

      expect(store.getActions()).to.containSubset([
        { type: setupUser.pending.toString() },
        { type: fetchUserEntitlements.pending.toString() },
        { type: fetchUserSettings.pending.toString() },
        { type: fetchUserFavorites.pending.toString() },
        { type: fetchUserDetails.pending.toString() },
        { type: fetchUserSettings.rejected.toString(), error: { message: 'error' } },
        { type: fetchUserEntitlements.fulfilled.toString(), payload: { entitlements } },
        { type: fetchUserFavorites.fulfilled.toString(), payload: { favorites } },
        { type: fetchUserDetails.fulfilled.toString(), payload: { details } },
        {
          type: fetchUserSubscriptionWarnings.fulfilled.toString(),
          payload: {
            warnings,
          },
        },
        { type: setupUser.fulfilled.toString() },
      ]);

      expect(getUserEntitlementsStub).to.have.been.calledOnce();
      expect(getUserDetailsStub).to.have.been.calledOnce();
      expect(getUserFavoritesStub).to.have.been.calledOnce();
      expect(getUserSettingsStub).to.have.been.calledOnce();
    });
  });
});
