import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { canUseDOM, getUserPremiumStatus } from '@surfline/web-common';
import {
  getMeter as fetchMeter,
  postMeter,
  getAnonymousMeter as fetchAnonymousMeter,
  patchMeteringSurfCheck,
  incrementAnonymousMeter,
} from '../api/meter';
import { SL_RATE_LIMITING_V3 } from '../common/treatments';
import { getMeteringEnabled } from '../selectors/state';
import { generateAnonymousId } from '../utils/identify';
import { setMeteringDisabled } from './state';
import { setMeteringEntitlement } from './user';
import { getCanUseDOM } from '../utils';

export const setMeter = createAction('SET_METER');
export const clearMeter = createAction('CLEAR_METER');

export const performMeteringSurfCheck = createAsyncThunk(
  'PERFORM_METER_SURF_CHECK',
  /**
   * @param {object} args
   * @param {string} args.accessToken
   */
  async ({ accessToken }) => {
    /* istanbul ignore next */
    if (!canUseDOM && !accessToken) {
      throw new Error('Access token required for this operation.');
    }
    const { meterRemaining } = await patchMeteringSurfCheck(accessToken);
    return { meterRemaining };
  },
);

export const createMeter = createAsyncThunk(
  'CREATE_METER',
  /**
   * @async
   * @param {object} args
   * @param {string?} args.accessToken
   * @param {string} args.anonymousId
   * @param {string?} args.timezone
   * @param {string?} args.countryCode
   * @param {boolean?} args.performMeterSurfCheck
   */
  async (
    { accessToken, anonymousId, countryCode, timezone, performMeterSurfCheck = false },
    thunkApi,
  ) => {
    const isPremium = getUserPremiumStatus(thunkApi.getState());

    // If the user is premium, we don't want to create a meter document
    if (isPremium) {
      thunkApi.dispatch(clearMeter());
      thunkApi.dispatch(setMeteringDisabled());
      return null;
    }

    let meter;
    try {
      meter = await postMeter({
        accessToken,
        anonymousId: anonymousId || generateAnonymousId(),
        timezone,
        countryCode,
      });
    } catch (err) {
      thunkApi.dispatch(clearMeter());
      thunkApi.dispatch(setMeteringDisabled());
      throw err;
    }

    // Since we have created a meter, the user entitlements will be out of date.
    // We should update it to add `sl_metered`
    thunkApi.dispatch(setMeteringEntitlement());

    const meteringEnabled = getMeteringEnabled(thunkApi.getState());

    if (performMeterSurfCheck && meteringEnabled) {
      const {
        payload: { meterRemaining },
      } = await thunkApi.dispatch(performMeteringSurfCheck({ accessToken }));
      meter = {
        ...meter,
        meterRemaining,
        showPaywall: meter?.meterRemaining < 0,
      };
    }
    return meter;
  },
);

export const getMeter = createAsyncThunk(
  'GET_METER',
  /**
   * @param {object} args
   * @param {string?} args.accessToken
   * @param {boolean?} args.createIfNotFound
   * @param {string?} args.anonymousId
   * @param {string?} args.timezone
   * @param {string?} args.countryCode
   * @param {boolean?} args.performMeterSurfCheck
   */
  async (
    {
      accessToken,
      createIfNotFound = false,
      anonymousId = null,
      countryCode = null,
      timezone = null,
      performMeterSurfCheck = false,
    } = {},
    thunkApi,
  ) => {
    /* istanbul ignore next */
    if (!canUseDOM && !accessToken) {
      throw new Error('Access token required to get meter.');
    }

    const isPremium = getUserPremiumStatus(thunkApi.getState());
    if (isPremium) {
      thunkApi.dispatch(clearMeter());
      thunkApi.dispatch(setMeteringDisabled());
      return null;
    }

    try {
      let meter = await fetchMeter(accessToken);
      const meteringEnabled = getMeteringEnabled(thunkApi.getState());
      if (performMeterSurfCheck && meteringEnabled && meter?.meterRemaining > -1) {
        const {
          payload: { meterRemaining },
        } = await thunkApi.dispatch(performMeteringSurfCheck({ accessToken }));
        meter = {
          ...meter,
          meterRemaining,
          showPaywall: meter?.meterRemaining < 0,
        };
      }

      return { ...meter, showPaywall: meter?.meterRemaining < 0 };
    } catch (err) {
      if (createIfNotFound && err.statusCode === 404) {
        await thunkApi.dispatch(
          createMeter({ accessToken, countryCode, anonymousId, timezone, performMeterSurfCheck }),
        );

        return { createdNewMeter: true };
      }

      throw err;
    }
  },
);

export const getAnonymousMeter = createAsyncThunk(
  'GET_ANONYMOUS_METER',
  /**
   * @param {object} args
   * @param {string} args.anonymousId
   * @param {'on' | 'off'} args.treatmentState
   * @param {boolean?} args.performMeterSurfCheck
   */
  async ({ anonymousId, treatmentState, performMeterSurfCheck = false }) => {
    // Check if this anonymous session has used any surf checks before
    const meter = await fetchAnonymousMeter(anonymousId);
    // If there is no key stored (new anonymous session) then meterRemaining would be null sent from the API
    let { meterRemaining } = meter;
    const hasMeter = meterRemaining !== null;

    // If there is no meter available in the server we just fail the action so that it can be created from the client side.
    if (!hasMeter && !getCanUseDOM()) {
      throw new Error(`No meter found for ${anonymousId}`);
    }

    if (performMeterSurfCheck && meterRemaining !== -1) {
      const { meterRemaining: newMeterRemaining } = await incrementAnonymousMeter(anonymousId);
      meterRemaining = newMeterRemaining;
    }

    return {
      anonymousId,
      treatmentName: SL_RATE_LIMITING_V3,
      treatmentState,
      meterLimit: 0,
      meterRemaining,
      showPaywall: true,
    };
  },
);
