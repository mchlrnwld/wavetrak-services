import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import deepFreeze from 'deep-freeze';
import thunk from 'redux-thunk';
import { SL_PREMIUM } from '@surfline/web-common';
import * as actions from './metering';
import * as meterApi from '../api/meter';
import * as identify from '../utils/identify';
import * as utils from '../utils';
import { setMeteringEntitlement } from './user';
import { setMeteringDisabled } from './state';

describe('actions / metering', () => {
  const mockStore = configureStore([thunk]);

  /** @type {sinon.SinonStub} */
  let getMeterStub;

  /** @type {sinon.SinonStub} */
  let getAnonymousMeterStub;

  /** @type {sinon.SinonStub} */
  let incrementAnonymousMeterStub;

  /** @type {sinon.SinonStub} */
  let postMeterStub;

  /** @type {sinon.SinonStub} */
  let uuidV1Stub;

  /** @type {sinon.SinonStub} */
  let patchMeteringSurfCheckStub;

  /** @type {sinon.SinonStub} */
  let getCanUseDOMStub;

  const accessToken = 'accessToken';
  const anonymousId = 'anonymousId';
  const rateLimitTreatment = 'sl_rate_limiting_v3';
  const treatmentState = 'on';
  const countryCode = 'countryCode';
  const timezone = 'timezone';

  const storeState = { backplane: { state: { meteringEnabled: true } } };

  const meter = deepFreeze({
    anonymousId,
    treatmentName: rateLimitTreatment,
    treatmentState,
    user: 'user',
    meterLimit: 1,
    meterRemaining: 0,
  });

  const anonymousMeter = deepFreeze({
    anonymousId,
    treatmentName: rateLimitTreatment,
    treatmentState,
    meterLimit: 0,
    meterRemaining: 0,
  });

  beforeEach(() => {
    getMeterStub = sinon.stub(meterApi, 'getMeter');
    getAnonymousMeterStub = sinon.stub(meterApi, 'getAnonymousMeter');
    incrementAnonymousMeterStub = sinon.stub(meterApi, 'incrementAnonymousMeter');
    postMeterStub = sinon.stub(meterApi, 'postMeter');
    uuidV1Stub = sinon.stub(identify, 'generateAnonymousId');
    patchMeteringSurfCheckStub = sinon.stub(meterApi, 'patchMeteringSurfCheck');
    getCanUseDOMStub = sinon.stub(utils, 'getCanUseDOM');
  });

  afterEach(() => {
    getMeterStub.restore();
    getAnonymousMeterStub.restore();
    incrementAnonymousMeterStub.restore();
    postMeterStub.restore();
    uuidV1Stub.restore();
    patchMeteringSurfCheckStub.restore();
    getCanUseDOMStub.restore();
  });

  it(`should dispatch ${actions.setMeter}`, () => {
    const action = actions.setMeter({ meter: true });

    expect(action).to.deep.equal({ type: actions.setMeter.type, payload: { meter: true } });
  });

  describe('getMeter', () => {
    it(`should handle ${actions.getMeter.fulfilled}`, async () => {
      const store = mockStore(storeState);
      getMeterStub.resolves({ ...meter, showPaywall: meter?.meterRemaining < 0 });

      await store.dispatch(actions.getMeter({ accessToken }));

      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        {
          type: actions.getMeter.fulfilled.toString(),
          payload: { ...meter },
        },
      ]);
      expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
    });

    it(`should short circuit ${actions.getMeter.fulfilled} for premium users`, async () => {
      const store = mockStore({ backplane: { user: { entitlements: [SL_PREMIUM] } } });
      getMeterStub.resolves(meter);

      await store.dispatch(actions.getMeter({ accessToken }));

      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        { type: actions.clearMeter.type },
        { type: setMeteringDisabled.type },
        { type: actions.getMeter.fulfilled.toString(), payload: null },
      ]);

      expect(getMeterStub).to.have.not.been.called();
    });

    it(`should handle ${actions.getMeter.fulfilled} with meter surf check created`, async () => {
      const store = mockStore(storeState);
      getMeterStub.resolves({ ...meter, showPaywall: meter?.meterRemaining < 0 });
      patchMeteringSurfCheckStub.resolves({ meterRemaining: 0 });
      await store.dispatch(actions.getMeter({ accessToken, performMeterSurfCheck: true }));
      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        { type: actions.getMeter.fulfilled.toString(), payload: { ...meter, meterRemaining: 0 } },
        { type: actions.performMeteringSurfCheck.pending.toString() },
        {
          type: actions.performMeteringSurfCheck.fulfilled.toString(),
          payload: { meterRemaining: 0 },
        },
      ]);

      expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
      expect(patchMeteringSurfCheckStub).to.have.been.calledOnceWithExactly(accessToken);
    });

    it(`should handle ${actions.getMeter.fulfilled} with no performMeteringSurfCheck call if there is no meterRemaining`, async () => {
      const store = mockStore(storeState);
      getMeterStub.resolves({ ...meter, meterRemaining: -1, showPaywall: true });
      await store.dispatch(actions.getMeter({ accessToken, performMeterSurfCheck: true }));
      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        { type: actions.getMeter.fulfilled.toString(), payload: { ...meter, meterRemaining: -1 } },
        {
          payload: { meterRemaining: -1 },
        },
      ]);

      expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
      expect(patchMeteringSurfCheckStub).to.have.not.been.called();
    });

    it(`should handle ${actions.getMeter.fulfilled} - create meter if not found`, async () => {
      const store = mockStore(storeState);
      getMeterStub.rejects({ statusCode: 404 });
      postMeterStub.resolves(meter);

      await store.dispatch(
        actions.getMeter({
          accessToken,
          countryCode,
          timezone,
          anonymousId,
          createIfNotFound: true,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        { type: actions.createMeter.pending.toString() },
        { type: actions.createMeter.fulfilled.toString(), payload: meter },
        { type: actions.getMeter.fulfilled.toString(), payload: { createdNewMeter: true } },
      ]);

      expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
      expect(postMeterStub).to.have.been.calledOnceWithExactly({
        accessToken,
        anonymousId,
        timezone,
        countryCode,
      });
    });

    it(`should handle ${actions.getMeter.rejected}`, async () => {
      const store = mockStore(storeState);
      getMeterStub.rejects({ message: 'error' });

      await store.dispatch(actions.getMeter({ accessToken }));

      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        { type: actions.getMeter.rejected.toString(), error: { message: 'error' } },
      ]);

      expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
    });

    it(`should handle ${actions.getMeter.rejected} - patchMeteringSurfCheckStub should not be called`, async () => {
      const store = mockStore(storeState);
      getMeterStub.rejects({ message: 'error' });
      await store.dispatch(actions.getMeter({ accessToken, performMeterSurfCheck: true }));

      expect(store.getActions()).to.containSubset([
        { type: actions.getMeter.pending.toString() },
        { type: actions.getMeter.rejected.toString(), error: { message: 'error' } },
      ]);

      expect(getMeterStub).to.have.been.calledOnceWithExactly(accessToken);
      expect(patchMeteringSurfCheckStub).to.have.not.been.called();
    });
  });

  describe('getAnonymousMeter', () => {
    it(`should handle ${actions.getAnonymousMeter.fulfilled} - gets an existing anonymous meter with surf checks remaining and perform a surf check`, async () => {
      const store = mockStore(storeState);
      getAnonymousMeterStub.resolves({ meterRemaining: 2 });
      incrementAnonymousMeterStub.resolves({ meterRemaining: 1 });
      await store.dispatch(
        actions.getAnonymousMeter({
          anonymousId,
          performMeterSurfCheck: true,
          treatmentState,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.getAnonymousMeter.pending.toString() },
        {
          type: actions.getAnonymousMeter.fulfilled.toString(),
          payload: { ...anonymousMeter, meterRemaining: 1, showPaywall: true },
        },
      ]);

      expect(getAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
    });

    it(`should handle ${actions.getAnonymousMeter.fulfilled} - creates an anonymous meter if not found and perform a surf check`, async () => {
      const store = mockStore(storeState);
      getAnonymousMeterStub.resolves({ meterRemaining: null });
      getCanUseDOMStub.returns(true);
      incrementAnonymousMeterStub.resolves({ meterRemaining: 2 });
      await store.dispatch(
        actions.getAnonymousMeter({
          anonymousId,
          performMeterSurfCheck: true,
          treatmentState,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.getAnonymousMeter.pending.toString() },
        {
          type: actions.getAnonymousMeter.fulfilled.toString(),
          payload: { ...anonymousMeter, meterRemaining: 2, showPaywall: true },
        },
      ]);

      expect(getAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
    });

    it(`should dispatch a failure action if no meter exists in the server`, async () => {
      const store = mockStore(storeState);
      getAnonymousMeterStub.resolves({ meterRemaining: null });
      getCanUseDOMStub.returns(false);
      await store.dispatch(
        actions.getAnonymousMeter({
          anonymousId,
          performMeterSurfCheck: true,
          treatmentState,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.getAnonymousMeter.pending.toString() },
        {
          type: actions.getAnonymousMeter.rejected.toString(),
          error: { message: `No meter found for ${anonymousId}` },
        },
      ]);

      expect(getAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
      expect(incrementAnonymousMeterStub).to.have.not.been.called();
    });

    it(`should not perform a surf check if there are no surf checks available(meterRemaining is -1)`, async () => {
      const store = mockStore(storeState);
      getAnonymousMeterStub.resolves({ meterRemaining: -1 });

      await store.dispatch(
        actions.getAnonymousMeter({
          anonymousId,
          performMeterSurfCheck: true,
          treatmentState,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.getAnonymousMeter.pending.toString() },
        {
          type: actions.getAnonymousMeter.fulfilled.toString(),
          payload: { ...anonymousMeter, meterRemaining: -1, showPaywall: true },
        },
      ]);

      expect(getAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
      expect(incrementAnonymousMeterStub).to.have.not.been.called();
    });

    it(`should perform a surf check if surf checks are available in the server`, async () => {
      const store = mockStore(storeState);
      getAnonymousMeterStub.resolves({ meterRemaining: 1 });
      getCanUseDOMStub.returns(false);
      incrementAnonymousMeterStub.resolves({ meterRemaining: 0 });
      await store.dispatch(
        actions.getAnonymousMeter({
          anonymousId,
          treatmentState,
          performMeterSurfCheck: true,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.getAnonymousMeter.pending.toString() },
        {
          type: actions.getAnonymousMeter.fulfilled.toString(),
          payload: { ...anonymousMeter, meterRemaining: 0, showPaywall: true },
        },
      ]);

      expect(getAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly(anonymousId);
    });
  });

  describe('createMeter', () => {
    it(`should handle ${actions.createMeter.fulfilled}`, async () => {
      const store = mockStore(storeState);
      postMeterStub.resolves(meter);
      uuidV1Stub.returns('uuidv1');

      await store.dispatch(
        actions.createMeter({
          accessToken,
          countryCode,
          timezone,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.createMeter.pending.toString() },
        { type: setMeteringEntitlement.toString() },
        { type: actions.createMeter.fulfilled.toString(), payload: meter },
      ]);

      expect(postMeterStub).to.have.been.calledOnceWithExactly({
        accessToken,
        anonymousId: 'uuidv1',
        timezone,
        countryCode,
      });
    });

    it(`should short circuit ${actions.createMeter.fulfilled} for premium users`, async () => {
      const store = mockStore({ backplane: { user: { entitlements: [SL_PREMIUM] } } });
      postMeterStub.resolves(meter);
      uuidV1Stub.returns('uuidv1');

      await store.dispatch(
        actions.createMeter({
          accessToken,
          countryCode,
          timezone,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.createMeter.pending.toString() },
        { type: actions.createMeter.fulfilled.toString(), payload: null },
      ]);

      expect(store.getActions()).to.not.containSubset([
        { type: setMeteringEntitlement.toString() },
      ]);

      expect(postMeterStub).to.have.not.been.called();
    });

    it(`should handle ${actions.createMeter.fulfilled} with meter surf check created `, async () => {
      const store = mockStore(storeState);
      postMeterStub.resolves(meter);
      patchMeteringSurfCheckStub.resolves({ meterRemaining: 4 });
      uuidV1Stub.returns('uuidv1');

      await store.dispatch(
        actions.createMeter({
          accessToken,
          countryCode,
          timezone,
          performMeterSurfCheck: true,
        }),
      );
      expect(store.getActions()).to.containSubset([
        { type: actions.createMeter.pending.toString() },
        {
          type: actions.createMeter.fulfilled.toString(),
          payload: { ...meter, meterRemaining: 4 },
        },
        { type: actions.performMeteringSurfCheck.pending.toString() },
        {
          type: actions.performMeteringSurfCheck.fulfilled.toString(),
          payload: { meterRemaining: 4 },
        },
      ]);

      expect(postMeterStub).to.have.been.calledOnceWithExactly({
        accessToken,
        anonymousId: 'uuidv1',
        timezone,
        countryCode,
      });
      expect(patchMeteringSurfCheckStub).to.have.been.calledOnceWithExactly(accessToken);
    });

    it(`should handle ${actions.createMeter.rejected}`, async () => {
      const store = mockStore(storeState);
      postMeterStub.rejects({ message: 'error' });

      await store.dispatch(
        actions.createMeter({
          anonymousId,
          accessToken,
          countryCode,
          timezone,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.createMeter.pending.toString() },
        { type: actions.createMeter.rejected.toString(), error: { message: 'error' } },
      ]);

      expect(postMeterStub).to.have.been.calledOnceWithExactly({
        accessToken,
        anonymousId,
        timezone,
        countryCode,
      });
    });

    it(`should handle ${actions.createMeter.rejected} - patchMeteringSurfCheckStub should not be called`, async () => {
      const store = mockStore(storeState);
      postMeterStub.rejects({ message: 'error' });

      await store.dispatch(
        actions.createMeter({
          anonymousId,
          accessToken,
          countryCode,
          timezone,
          performMeterSurfCheck: true,
        }),
      );

      expect(store.getActions()).to.containSubset([
        { type: actions.createMeter.pending.toString() },
        { type: actions.createMeter.rejected.toString(), error: { message: 'error' } },
      ]);

      expect(postMeterStub).to.have.been.calledOnceWithExactly({
        accessToken,
        anonymousId,
        timezone,
        countryCode,
      });
      expect(patchMeteringSurfCheckStub).to.have.not.been.called();
    });
  });
  describe('performMeteringSurfCheck', () => {
    it(`should handle ${actions.performMeteringSurfCheck.fulfilled}`, async () => {
      const store = mockStore(storeState);
      const meterRemaining = 4;
      patchMeteringSurfCheckStub.resolves({ meterRemaining });

      await store.dispatch(
        actions.performMeteringSurfCheck({
          accessToken,
        }),
      );
      expect(store.getActions()).to.containSubset([
        { type: actions.performMeteringSurfCheck.pending.toString() },
        {
          type: actions.performMeteringSurfCheck.fulfilled.toString(),
          payload: { meterRemaining },
        },
      ]);

      expect(patchMeteringSurfCheckStub).to.have.been.calledOnceWithExactly(accessToken);
    });
  });
});
