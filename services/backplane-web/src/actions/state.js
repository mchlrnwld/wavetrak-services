import { createAction } from '@reduxjs/toolkit';

export const performedMeterModalRecompute = createAction('METER_MODAL_RECOMPUTED');
export const triggerMeterModalRecompute = createAction('TRIGGER_METER_MODAL_RECOMPUTE');
export const setMeterLoaded = createAction('SET_METERING_LOADED');
export const setMeteringDisabled = createAction('SET_METERING_DISABLED');
export const setMeteringEnabled = createAction('SET_METERING_ENABLED');
