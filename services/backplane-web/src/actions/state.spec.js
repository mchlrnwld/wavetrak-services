import { expect } from 'chai';
import * as actions from './state';

describe('actions / state', () => {
  it(`should dispatch ${actions.performedMeterModalRecompute}`, () => {
    const action = actions.performedMeterModalRecompute();

    expect(action).to.deep.equal({
      type: actions.performedMeterModalRecompute.type,
      payload: undefined,
    });
  });

  it(`should dispatch ${actions.triggerMeterModalRecompute}`, () => {
    const action = actions.triggerMeterModalRecompute();

    expect(action).to.deep.equal({
      type: actions.triggerMeterModalRecompute.type,

      payload: undefined,
    });
  });

  it(`should dispatch ${actions.setMeterLoaded}`, () => {
    const action = actions.setMeterLoaded();

    expect(action).to.deep.equal({
      type: actions.setMeterLoaded.type,

      payload: undefined,
    });
  });
});
