import { createAsyncThunk, createAction } from '@reduxjs/toolkit';

import {
  serverSideFetchUserSettings,
  serverSideFetchUserFavorites,
  serverSideFetchUserDetails,
  serverSideFetchUserEntitlements,
  getUserDetails,
  getUserEntitlements,
  getUserFavorites,
  getUserSettings,
  serverSideFetchSubscriptionWarnings,
  getUserSubscriptionWarnings,
} from '../api/user';

/**
 * @description User Actions defined in this file should only be
 * user actions which are to be used exclusively in Backplane.
 * Due to the usage of backplane as a pseudo orchestration layer
 * for the user and meter state, we want to ensure that we expose
 * actions that allow us to alter this state in meaningful ways
 * from a package like `@surfline/web-common` in order to give
 * consuming applications levers of control on the usage of
 * the orchestration layer.
 */

export const setUserMetadata = createAction('SET_USER_METADATA');
export const setMeteringEntitlement = createAction('SET_METERING_ENTITLEMENT');

export const fetchUserDetails = createAsyncThunk(
  'FETCH_USER_DETAILS',
  /**
   * @param {Object} params
   * @param {string?} params.userId
   * @param {boolean?} params.serverSide
   * @returns {{ details: {} }}
   */
  async ({ userId, serverSide = false } = {}) => {
    const details = serverSide ? await serverSideFetchUserDetails(userId) : await getUserDetails();
    return { details };
  },
);

export const fetchUserSettings = createAsyncThunk(
  'FETCH_USER_SETTINGS',
  /**
   * @param {Object} params
   * @param {string?} params.clientIp
   * @param {string?} params.userId
   * @param {string?} params.countryIso
   * @param {boolean?} params.serverSide
   * @returns {{ settings: {} }}
   */
  async ({ clientIp, countryIso, userId, serverSide = false } = {}) => {
    const settings = serverSide
      ? await serverSideFetchUserSettings(userId, countryIso, clientIp)
      : await getUserSettings();
    return { settings };
  },
);

export const fetchUserFavorites = createAsyncThunk(
  'FETCH_USER_FAVORITES',
  /**
   * @param {Object} params
   * @param {?{ latitude: string | number, longitude: string | number }} params.location
   * @param {string?} params.userId
   * @param {string?} params.clientIp
   * @param {string?} params.countryIso
   * @param {boolean?} params.serverSide
   * @returns {{ favorites: {}[] }}
   */
  async ({ userId, clientIp, location, countryIso, serverSide = false } = {}) => {
    const favorites = serverSide
      ? await serverSideFetchUserFavorites(userId, countryIso, clientIp, location)
      : await getUserFavorites();
    return { favorites: favorites.data.favorites };
  },
);

export const fetchUserEntitlements = createAsyncThunk(
  'FETCH_USER_ENTITLEMENTS',
  /**
   * @param {Object} params
   * @param {string} params.userId
   * @returns {{ entitlements: { entitlements: string[], promotions: string[] } }}
   */
  async ({ userId, serverSide = false } = {}) => {
    const entitlements = serverSide
      ? await serverSideFetchUserEntitlements(userId)
      : await getUserEntitlements();
    return { entitlements };
  },
);

export const fetchUserSubscriptionWarnings = createAsyncThunk(
  'FETCH_USER_SUBSCRIPTION_WARNINGS',
  /**
   * @param {Object} params
   * @param {string} params.userId
   * @returns {{ entitlements: { entitlements: string[], promotions: string[] } }}
   */
  async ({ userId, serverSide = false } = {}) => {
    const warnings = serverSide
      ? await serverSideFetchSubscriptionWarnings(userId)
      : await getUserSubscriptionWarnings(userId);

    return { warnings };
  },
);

export const setupUser = createAsyncThunk(
  'SETUP_USER',
  /**
   * @param {Object} params
   * @param {?{ latitude: string | number, longitude: string | number }} params.location
   * @param {string?} params.userId
   * @param {string?} params.accessToken
   * @param {string?} params.clientIp
   * @param {string?} params.countryIso
   * @param {boolean?} params.serverSide
   * @returns {void}
   */
  async (
    {
      userId = null,
      accessToken = null,
      countryIso = null,
      clientIp = null,
      location = null,
      serverSide = false,
    } = {},
    thunkAPI,
  ) => {
    const isAuthenticated = !!accessToken || !!userId;
    await Promise.allSettled(
      [
        fetchUserFavorites,
        fetchUserSettings,
        isAuthenticated && fetchUserDetails,
        isAuthenticated && fetchUserEntitlements,
        isAuthenticated && fetchUserSubscriptionWarnings,
      ]
        .filter(Boolean)
        .map((action) =>
          thunkAPI.dispatch(action({ userId, countryIso, clientIp, location, serverSide })),
        ),
    );
  },
);
