import { SL_METERED, SL_PREMIUM } from '@surfline/web-common';
import { ON } from '../../common/constants';

const anonymousUser = {
  backplane: {
    user: {
      details: null,
      entitlements: [],
      promotionEntitlements: [],
      favorites: [],
      settings: null,
    },
    meter: null,
  },
};

export const premiumUser = {
  backplane: {
    user: {
      details: {
        email: 'premium@sl.com',
        firstName: 'premium',
        lastName: 'user',
      },
      location: { time_zone: 'America/Los_Angeles' },
      entitlements: [SL_PREMIUM],
    },
  },
};

export const freeUser = {
  backplane: {
    user: {
      details: {
        email: 'free@sl.com',
        firstName: 'free',
        lastName: 'user',
      },
      countryCode: 'US',
      location: { time_zone: 'America/Los_Angeles' },
      entitlements: [],
    },
  },
};

export const meteredUser = {
  backplane: {
    user: {
      details: {
        email: 'metered@sl.com',
        firstName: 'metered',
        lastName: 'user',
      },
      entitlements: [SL_METERED],
    },
    meter: {
      treatmentState: ON,
      meterRemaining: 1,
      anonymousId: '123',
    },
    state: {
      meterLoaded: true,
    },
  },
};

export default anonymousUser;
