import { v1 as uuidV1 } from 'uuid';

export const generateAnonymousId = () => uuidV1();
