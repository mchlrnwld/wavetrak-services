import React from 'react';
import { mount } from 'enzyme';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import defaultState from './fixtures/store';

// eslint-disable-next-line react/prop-types
const Wrapper = ({ children, store }) => <Provider store={store}>{children}</Provider>;

/**
 * @param {React.ReactNode} Component
 * @param {React.Props} [props]
 * @param {Object} [options]
 * @param {import('redux').PreloadedState} options.initialState
 * @param {import('redux-mock-store').MockStoreEnhanced} options.store
 */
export const mountWithRedux = (
  Component,
  props = undefined,
  { initialState = defaultState, store = configureStore([thunk])(initialState) } = {},
) => {
  const wrapper = mount(<Component {...props} />, {
    wrappingComponent: Wrapper,
    wrappingComponentProps: {
      store,
    },
  });
  return {
    wrapper,
    store,
  };
};
