import { getCanUseDOM as doGetCanUseDOM } from '@surfline/web-common';

export const getCanUseDOM = () => doGetCanUseDOM();
