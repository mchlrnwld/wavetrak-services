import fetch from 'isomorphic-fetch';
import { Cookies } from 'react-cookie';
import config from '../config';

const fetchBackplane = async () => {
  const cookies = new Cookies();
  const accessToken = cookies.get('access_token');
  const at = accessToken ? `&accessToken=${accessToken}` : '';
  const url = `${config.servicesURL}/backplane/render.json?bundle=margins${at}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export default fetchBackplane;
