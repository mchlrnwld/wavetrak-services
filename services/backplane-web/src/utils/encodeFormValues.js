/* instanbul ignore file */

/**
 * Creates an encoded string from a map of form values
 *
 * @function encodeFormValues
 * @param {{[key: string]: string | number | boolean }} formValues
 * @returns {string}
 */
const encodeFormValues = (formValues) => {
  const keysToEncode = Object.keys(formValues);

  const encodedValues = keysToEncode.map(
    /**
     * @param {string} key
     * @returns {string}
     */
    (key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(formValues[key]);
      return `${encodedKey}=${encodedValue}`;
    },
  );

  return encodedValues.join('&');
};

export default encodeFormValues;
