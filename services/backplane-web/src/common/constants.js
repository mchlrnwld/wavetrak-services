export const CLICKED_SUBSCRIBE_CTA = 'Clicked Subscribe CTA';
export const SIGN_IN = 'signIn';
export const LEARN_MORE = 'learnMore';
export const HOME = 'home';
export const OPEN_HOUSE_COOKIE = 'open_house_modal';
export const LARGE_DESKTOP = 'largeDesktop';
export const DESKTOP = 'desktop';
export const TABLET = 'tablet';
export const SMALL_TABLET = 'smallTablet';
export const EXTRA_LARGE_MOBILE = 'extralargeMobile';
export const LARGE_MOBILE = 'largeMobile';
export const MEDIUM_MOBILE = 'mediumMobile';
export const SMALL_MOBILE = 'smallMobile';
export const RL_VERIFY_EMAIL_CLICKED = 'Clicked Verify Email';
export const RL_CTA_CLICKED = 'CTA Clicked';
export const RL_PAYWALL_DISPLAYED = 'Premium Paywall Displayed';
export const FORCED_REGISTRATION = 'Forced Registration';
export const CLICKED_POPUP = 'Clicked Popup';
export const SERVED_POPUP = 'Served Popup';
export const NAVIGATED_TO_LEARN_MORE = 'Navigated To Learn More';
export const NAVIGATED_TO_CREATE_ACCOUNT = 'Navigated To Create Account';
export const NAVIGATED_TO_SIGN_IN = 'Navigated To Sign In';
export const CLICKED_CREATE_ACCOUNT = 'Clicked Create Account';
export const SIGNED_IN = 'Signed In';
export const CLICKED_SIGN_IN = 'Clicked Sign In';
export const RATE_LIMITING = 'Rate Limiting';
export const STICKY_ENABLED_MIN_WIDTH = 769;
export const UPGRADE_CTA = 'Anon User Upsell Card';

export const HERO_AD_PLACEHOLDER = 'hero_ad_placeholder';
export const ON = 'on';
export const OFF = 'off';
export const CONTROL = 'control';

export const BrazeContentCards = {
  NOTIFICATION_BAR: 'NOTIFICATION_BAR',
  PAYMENT_WARNING__DEFAULT: 'PAYMENT_WARNING__DEFAULT',
  PAYMENT_WARNING__INVALIDZIP: 'PAYMENT_WARNING__INVALID_ZIP',
  REGISTRATION_WALL: 'REGISTRATION_WALL',
  RATE_LIMITING_CHECK_COUNTER: 'RATE_LIMITING_CHECK_COUNTER',
  RATE_LIMITING_UPGRADE_CARD: 'RATE_LIMITING_UPGRADE_CARD',
};
