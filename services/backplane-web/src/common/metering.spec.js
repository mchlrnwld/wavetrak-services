import { expect } from 'chai';
import { SL_METERED, SL_PREMIUM } from '@surfline/web-common';
import * as features from '@surfline/web-common/esm/features/splitio';
import sinon from 'sinon';

import { mountWithRedux } from '../utils/testUtils';
import { computeMeterModal } from './metering';
import { OFF, ON } from './constants';

describe('common / metering', () => {
  /** @type {sinon.SinonStub} */
  let getWavetrakIdentityStub;
  /** @type {sinon.SinonStub} */
  let getTreatmentStub;

  const getStateStub = sinon.stub();
  const dispatchStub = sinon.stub();

  const anonymousId = '123';
  beforeEach(() => {
    getWavetrakIdentityStub = sinon.stub(features, 'getWavetrakIdentity').returns({ anonymousId });
    getTreatmentStub = sinon.stub(features, 'getTreatment');
  });

  afterEach(() => {
    getWavetrakIdentityStub.restore();
    getTreatmentStub.restore();
    getStateStub.resetHistory();
    dispatchStub.resetHistory();
  });

  describe('computeMeterModal', () => {
    const meter = { treatmentState: ON, meterRemaining: 1, meterLimit: 3 };
    const meterOff = { treatmentState: OFF };
    const meterNoChecks = { treatmentState: ON, meterRemaining: -1, meterLimit: 3 };
    const user = { email: 'test@test.com' };

    it('should return null for a premium user', () => {
      const result = computeMeterModal(meter, user, [SL_PREMIUM]);

      expect(result).to.be.null();
    });

    it('should return null without a meter', () => {
      const result = computeMeterModal(null, user, []);

      expect(result).to.be.null();
    });

    it('should return null if the treatment state if `OFF`', () => {
      const result = computeMeterModal(meterOff, user, []);

      expect(result).to.be.null();
    });

    it('should return SurfCheckCounter if there are remaining surf checks', () => {
      const { modal } = computeMeterModal(meter, user, []);

      const { wrapper } = mountWithRedux(() => modal, undefined, {
        initialState: { backplane: { user: { details: {} }, meter } },
      });

      expect(wrapper.find('SurfCheckCounter').exists()).to.be.true();
    });

    it('should return reg-wall for anonymous metered users with no checks left', () => {
      const { modal } = computeMeterModal(meterNoChecks, null, []);

      const { wrapper } = mountWithRedux(() => modal);

      expect(wrapper.find('MeterBottomSheetRegWall').exists()).to.be.true();
    });

    it('should return pay-wall for metered users with no checks left', () => {
      const { modal } = computeMeterModal(meterNoChecks, user, [SL_METERED]);

      const { wrapper } = mountWithRedux(() => modal, undefined, {
        initialState: { backplane: { user: { details: {} }, meter } },
      });

      expect(wrapper.find('MeterPremiumPaywall').exists()).to.be.true();
    });
  });
});
