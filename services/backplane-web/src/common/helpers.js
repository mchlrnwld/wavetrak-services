import { getClientIp, findSubdivisionCode } from '@surfline/web-common';
import { serverSideGetRegion } from '../api/geotarget';

/**
 *
 * @param {import('express').Request} req
 * @param {ReturnType<import('../common/logger')["default"]>} log
 */
export const locationDetails = async (req, log) => {
  const clientIp = req.headers['x-client-ip'] || getClientIp(req);
  const details = {
    ip: clientIp,
    countryCode: 'US',
    subdivisionCode: null,
    location: { latitude: 32.715738, longitude: -117.161084 },
  };

  try {
    const {
      location,
      country: { iso_code: isoCode },
      subdivisions = null,
    } = await serverSideGetRegion(clientIp);
    details.countryCode = isoCode;
    details.location = location;
    details.subdivisionCode = findSubdivisionCode(subdivisions) || null;
  } catch (err) {
    log.error({
      error: err.stack,
      resp: JSON.stringify(err),
    });
  }
  return details;
};
