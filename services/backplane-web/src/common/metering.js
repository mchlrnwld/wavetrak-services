import React from 'react';
import {
  ANONYMOUS,
  SL_PREMIUM,
  getMeter as getMeterState,
  getTreatment,
  getUser,
  getWavetrakIdentity,
  getWindow,
} from '@surfline/web-common';

import {
  clearMeter,
  setMeterLoaded,
  getAnonymousMeter,
  triggerMeterModalRecompute,
  setMeteringDisabled,
} from '../actions';
import { getMeterLoaded } from '../selectors/state';

import { SL_RATE_LIMITING_V3 } from './treatments';
import MeterPremiumPaywall from '../bundles/margins/metering/MeterPremiumPaywall';
import MeterBottomSheetRegWall from '../bundles/margins/metering/MeterBottomSheetRegWall';
import SurfCheckCounter from '../bundles/margins/metering/SurfCheckCounter';
import UpgradeCTA from '../bundles/margins/metering/UpgradeCTA';

import { ON, OFF, CONTROL } from './constants';

/**
 * A simple check on a meter to determine if it is eligible for a surf check.
 * @param {import('@surfline/web-common').MeterState} meter
 * @returns {boolean}
 */
export const isSurfCheckEligible = (meter) => meter != null && meter?.meterRemaining > -1;

/**
 * Utility function for Metering Modals. This is a centralized location for all Meter related logic
 * in backplane. Depending on the meter state of a user this function determines which Modal to be
 * rendered based on the treatment variant the user is in.
 * @param {import('@surfline/web-common').MeterState} meter
 * @param {import('@surfline/web-common').UserState["details"]} user
 * @param {string[]} entitlements
 * @return {{ modal: JSX.Element | null, scrollable: boolean} | null}
 */
export const computeMeterModal = (meter, user, entitlements) => {
  const isPremium = entitlements?.includes(SL_PREMIUM);

  const isAnonymous = !user;

  if (!meter || isPremium) {
    return null;
  }

  const { treatmentState } = meter;

  // If treatment is OFF then don't show the Modals
  if (treatmentState === OFF) {
    return null;
  }

  if (isSurfCheckEligible(meter)) {
    return isAnonymous
      ? { modal: <UpgradeCTA />, scrollable: true }
      : { modal: <SurfCheckCounter />, scrollable: true };
  }

  // Anonymous Sessions
  if (isAnonymous) {
    return {
      modal: (
        <div data-scroll-lock-scrollable>
          <MeterBottomSheetRegWall />
        </div>
      ),
      scrollable: false,
    };
  }

  return {
    modal: (
      <div data-scroll-lock-scrollable>
        <MeterPremiumPaywall />
      </div>
    ),
    scrollable: false,
  };
};

const getRateLimitTreatment = () => {
  // Free user. Evaluate Split
  const treatment = getTreatment(SL_RATE_LIMITING_V3);
  const treatmentState = treatment !== CONTROL && treatment !== OFF ? ON : OFF;
  return treatmentState;
};

// TODO: SA-3317 - remove after rate limiting is fully on
// Should be handled server side
/**
 * @param {boolean} rateLimitingEnabled
 * @param {import('redux').Store} store
 * @return {Promise<boolean | null>}
 */
export const rateLimitUserDetails = async (rateLimitingEnabled, store) => {
  const identity = getWavetrakIdentity();
  const { anonymousId } = identity || {};

  const state = store.getState();
  const { entitlements, details } = getUser(state);
  const isPremium = entitlements.indexOf(SL_PREMIUM) > -1;

  if (isPremium || !rateLimitingEnabled) {
    return null;
  }

  if (!details) {
    // Free user. Evaluate Split
    const treatment = getRateLimitTreatment();
    if (treatment === OFF) return null;

    await store.dispatch(
      getAnonymousMeter({
        anonymousId,
        treatmentState: treatment,
        performMeterSurfCheck: true,
      }),
    );
  }

  // Now that we have loaded the meter, we want to render the reg-wall
  store.dispatch(triggerMeterModalRecompute());
  return true;
};

// TODO: SA-3317 - remove after rate limiting is fully on
/**
 * @description It is possible that we get a cached version of the page for anonymous users so we need
 * to check that the anonymous ID that is in the meter state matches the one that exists on the
 * `wavetrakIdentity` stored on the window. If these two do not match then we know we have received a
 * cached version of the page. Here we invalidate the existing metering state that may exist in `__BACKPLANE_REDUX__`
 * @param {import('../stores').Store} store
 */
const clearCachedMeterStateForAnonymousUser = (store) => {
  const identity = getWavetrakIdentity();
  const isAnonymous = identity?.type === ANONYMOUS;
  const meter = getMeterState(store.getState());

  if (isAnonymous && meter?.anonymousId && meter.anonymousId !== identity.anonymousId) {
    store.dispatch(clearMeter());
    store.dispatch(setMeterLoaded(false));
    getWindow()?.newrelic?.addPageAction('Cached Anonymous ID Conflict', {
      anonymousId: identity.anonymousId,
    });
  }
};

// TODO: SA-3317 - remove after rate limiting is fully on since this can be loaded fully server side
/**
 * @param {import('../stores').Store} store
 * @param {() => void} hydrate
 * @param {boolean} [rateLimitingEnabled]
 * @param {(success: boolean) => void} [existingCallback]
 */
export const getLoadMeterBeforeHydration =
  (store, hydrate, rateLimitingEnabled, existingCallback) =>
  /** @param {boolean} setupSuccessful */
  async (setupSuccessful) => {
    if (!rateLimitingEnabled) {
      existingCallback?.(setupSuccessful);
      return hydrate();
    }

    clearCachedMeterStateForAnonymousUser(store);

    const meterLoaded = getMeterLoaded(store.getState());

    // If the meter was not loaded server-side (or cleared) then we
    // need to determine if the user is rate-limited before we hydrate
    if (!meterLoaded) {
      // We want to disable metering when it is not loaded on the server-side
      store.dispatch(setMeteringDisabled());

      // We do still want to create a meter and check the metering state so that
      // the next time the user does a server-side page view, the meter will be
      // available
      await rateLimitUserDetails(rateLimitingEnabled, store);
    }

    existingCallback?.(setupSuccessful);
    return hydrate();
  };
