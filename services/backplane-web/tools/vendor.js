import { rollup } from 'rollup';
import config from '../config/vendor.config';
import getVendors from '../src/vendors';

export default async () => {
  console.log('building vendor libraries');

  const vendors = getVendors();
  const vendorConfig = config(vendors);
  const bundle = await rollup(vendorConfig);
  await bundle.generate(vendorConfig.output);
  await bundle.write(vendorConfig.output);

  console.log('done');
};
