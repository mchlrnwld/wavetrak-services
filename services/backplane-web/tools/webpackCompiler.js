const webpack = require('webpack');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');

const compiler = (webpackConfig, cb) => {
  let webpackCompiler;
  const type = webpackConfig.target === 'web' ? 'Client' : 'Server';

  try {
    webpackCompiler = webpack(webpackConfig);
    console.log(`${type} webpack configuration compiled`);
  } catch (error) {
    console.log(`${type} webpack config is invalid\n`, error);
    process.exit();
  }

  webpackCompiler.plugin('done', (stats) => {
    const rawMessages = stats.toJson({ chunks: false }, true);
    const messages = formatWebpackMessages(rawMessages);
    if (!messages.errors.length && !messages.warnings.length) {
      console.log('Compiled successfully!');
    }
    if (messages.errors.length) {
      console.log('Failed to compile.');
      messages.errors.forEach(console.log);
      return;
    }
    if (messages.warnings.length) {
      console.log('Compiled with warnings.');
      messages.warnings.forEach(console.log);
    }

    if (cb) {
      cb(stats);
    }
  });

  return webpackCompiler;
};


export default compiler;
