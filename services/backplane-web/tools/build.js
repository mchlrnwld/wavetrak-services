/** @format */

import sh from 'shelljs';
import webpackCompiler from './webpackCompiler';
import config from '../config/webpack';
import vendor from './vendor';

const clean = () => {
  sh.rm('-rf', './build');
};

export default async () => {
  clean();
  const conf = config({}, 'prod');

  const compileApp = () =>
    webpackCompiler(conf.clientConfig, (clientStats) => {
      // build client
      if (clientStats.hasErrors()) return;
      sh.cp('-r', './src/public/*', './build/public/');
      console.log('done with client');
      webpackCompiler(conf.serverConfig, (serverStats) => {
        // build server
        console.log('starting server');
        sh.cp('./package.json', './build/server');
        sh.cp('./config/newrelic.js', './build/server/');
        if (serverStats.hasErrors()) return;
      }).run(() => undefined);
    });

  await vendor();
  compileApp().run(() => undefined);
};
