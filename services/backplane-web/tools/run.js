const args = process.argv.slice(3);
const fn = require(`./${process.argv[2]}`);
fn.default(args).catch((err) => {
  console.log(err);
  process.exit(1);
});
