provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "web-backplane/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "web-backplane.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "web-backplane-service" {
  source = "../../"

  company     = "sl"
  application = "web-backplane"
  environment = "prod"

  default_vpc = "vpc-116fdb74"
  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"
  ecs_cluster = "sl-core-svc-prod"

  service_td_count = 3
  service_lb_rules = local.service_lb_rules

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/4796722fa18acc2f"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b"
}
