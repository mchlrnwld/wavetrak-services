variable "company" {
  type = string
}

variable "application" {
  type = string
}

variable "environment" {
  type = string
}

variable "ecs_cluster" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "dns_name" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "load_balancer" {
  type = string
}

variable "iam_role" {
  type = string
}
