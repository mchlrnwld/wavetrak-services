[
  {
    "name": "${name}",
    "image": "${image}:${version}",
    "memory": ${memory},
    "cpu": ${cpu},
    "portMappings": [
      {
        "hostPort": 0,
        "containerPort": ${port},
        "protocol": "tcp"
      }
    ],
    "environment": [
      {
        "name": "APP_VERSION",
        "value": "${version}"
      }
    ]
  }
]
