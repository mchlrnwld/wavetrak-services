terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "wavetrak-core-gpu/staging/simple-service.tfstate"
    region = "us-west-1"
  }
}

module "service" {
  source = "../../"

  company     = "wt"
  application = "simple-service"
  environment = "staging"

  vpc_id      = "vpc-981887fd"
  dns_name    = "simple-service.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  ecs_cluster   = "wt-core-gpu-staging"
  iam_role      = "wt-core-gpu-iam-role-ecs-service-staging"
  load_balancer = "wt-core-gpu-alb-staging"
}
