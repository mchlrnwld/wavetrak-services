terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "wavetrak-core-gpu/sandbox/simple-service.tfstate"
    region = "us-west-1"
  }
}

module "service" {
  source = "../../"

  company     = "wt"
  application = "simple-service"
  environment = "sandbox"

  vpc_id      = "vpc-981887fd"
  dns_name    = "simple-service.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"

  ecs_cluster   = "wt-core-gpu-sandbox"
  iam_role      = "wt-core-gpu-iam-role-ecs-service-sandbox"
  load_balancer = "wt-core-gpu-alb-sandbox"
}
