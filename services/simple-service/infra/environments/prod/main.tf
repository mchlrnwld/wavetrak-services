terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wavetrak-core-gpu/prod/simple-service.tfstate"
    region = "us-west-1"
  }
}

module "service" {
  source = "../../"

  company     = "wt"
  application = "simple-service"
  environment = "prod"

  vpc_id      = "vpc-116fdb74"
  dns_name    = "simple-service.prod.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  ecs_cluster   = "wt-core-gpu-prod"
  iam_role      = "wt-core-gpu-iam-role-ecs-service-prod"
  load_balancer = "wt-core-gpu-alb-prod"
}
