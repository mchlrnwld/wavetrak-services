terraform {
  required_version = ">= 0.12"
  required_providers {
    aws  = "~> 2.70"
    null = "~> 3.1"
  }
}
