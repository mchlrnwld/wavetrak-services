# Simple Service

A minimal application in NodeJS that returns an "OK" response at any route.

## Table of Contents <!-- omit in toc -->

- [Summary](#summary)
- [Installation](#installation)
  - [We have a job for that](#we-have-a-job-for-that)
  - [Build locally](#build-locally)
  - [Upload the image to Amazon ECR](#upload-the-image-to-amazon-ecr)
    - [The repository in the Elastic Container Registry](#the-repository-in-the-elastic-container-registry)
    - [Tag](#tag)
    - [Push](#push)
    - [Verify](#verify)
    - [Why `prod`?](#why-prod)
- [The service infrastructure](#the-service-infrastructure)
- [Validation](#validation)

## Summary

This simple service was created with the purpose to test a service running within the GPU-enabled ECS Cluster created from [Wavetrak Infrastructure Core GPU] Terraform configuration.

## Installation

You can build the image locally, but the creation of the service will be executed in AWS.

### We have a job for that

To deploy this simple-service, you can run the [deploy-service-to-ecs] Jenkins job, setting `simple-service` as the service parameter, as well as the environment and branch you'll want to deploy to.

And that will be it.

But if you want to test every step of the process the Jenkins job is doing, follow the next steps.

### Build locally

Run the `docker build` command tagging the image as `simple-service`:

```bash
docker build -t simple-service .
```

```shellscript
Sending build context to Docker daemon  308.6MB
Step 1/7 : FROM node:14
 ---> 5a31c897a02a
Step 2/7 : WORKDIR /opt/app
 ---> Using cache
 ---> 47038bf0cc7b
Step 3/7 : COPY . /opt/app
 ---> 3cf343d35c6b
Step 4/7 : RUN npm install
 ---> Running in 61a6a53d88d4
added 50 packages from 37 contributors and audited 50 packages in 5.816s
found 0 vulnerabilities
Removing intermediate container 61a6a53d88d4
 ---> 2e6763b6c6e3
Step 5/7 : ARG APP_VERSION="dev"
 ---> Running in 12a7da905f71
Removing intermediate container 12a7da905f71
 ---> e600dfbd82b8
Step 6/7 : ENV APP_VERSION="${APP_VERSION}"
 ---> Running in 9efbbd7ef694
Removing intermediate container 9efbbd7ef694
 ---> 39dcd0ab1026
Step 7/7 : CMD ["npm", "start"]
 ---> Running in 7a5cf08f0da7
Removing intermediate container 7a5cf08f0da7
 ---> cb8f60e265d2
Successfully built cb8f60e265d2
Successfully tagged simple-service:latest
```

### Upload the image to Amazon ECR

#### The repository in the Elastic Container Registry

We need to create a repository in AWS for the service to be pulled from a deploy job and execute a task in the GPU-Enabled ECS Cluster.

For that, we have another Jenkins job that [creates an ECR repository] in AWS, that you can verify if you have access to our `prod` AWS account.

*The repository `services/simple-service` in `prod` was already created using this job.*

#### Tag

Tag the local image you've just created adding the ECR url:

```bash
docker tag \
    simple-service:latest \
    833713747344.dkr.ecr.us-west-1.amazonaws.com/services/simple-service:latest
```

#### Push

Now you'll need to login to ECR:

```bash
assume-role prod
aws ecr get-login-password \
    --region us-west-1 | \
docker login \
    --username AWS \
    --password-stdin \
    833713747344.dkr.ecr.us-west-1.amazonaws.com
```

```shellscript
WARNING! Your password will be stored unencrypted in /home/user/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

And then push the service image to AWS:

```bash
docker push 833713747344.dkr.ecr.us-west-1.amazonaws.com/services/simple-service:latest
```

```shellscript
The push refers to repository [833713747344.dkr.ecr.us-west-1.amazonaws.com/services/simple-service]
f69ddd570325: Pushed
f4790420bbed: Pushed
dd2e3d68cecb: Pushed
98e90b9ff8ad: Pushed
279f16edad53: Pushed
0da4e4a08626: Pushed
3dd8c0438e7e: Pushed
3da373b12072: Pushed
6f562b6e2ead: Pushed
90707c11bb17: Pushed
53880c3b885f: Pushed
44f663453d2d: Pushed
latest: digest: sha256:0faab468ea0efac9937fc0ae135c53778d3c9ad355709da0552fcd749832e7b1 size: 2846
```

#### Verify

Sign in with the `prod` account into the AWS Console and browse to the Elastic Container Service repositories.

Looking for `services/simple-service`, you should [see the image you've just pushed] tagged as `latest` with the digest code above:

|Image tag|Pushed at|Size (MB)|Digest|Scan status|Vulnerabilities|
|- |- |- |- |- |- |
|latest|May 01, 2021 12:45...|500.85|sha256:0faab468ea0efac9...|-|-|

#### Why `prod`?

We use the `prod` AWS account to store the service images as a single resource point to be pulled from `prod` itself and any other AWS account specified in the access policies. By the moment, this other AWS accounts belong to `dev` and `legacy`.

## The service infrastructure

Using [Terraform], you can deploy the infrastructure needed for the service to run inside the ECS Cluster.

Stepped into the `infra/` folder, the following commands are used to develop and deploy infrastructure changes:


```bash
assume-role {env}
ENV={env} make plan
ENV={env} make apply
```

Replace the `{env}` string with the name of the environment you're working on.

Available environments are: `sandbox`, `staging`, `prod`.

> TODO: Replace this information with the corresponding tasks once we add Terraform to a CI/CD tooling instead of executing commands from the user's console.

## Validation

Connected to the Surfline/Wavetrak VPN, after applying the configuration into the selected environment, a simple http request to the service url should be enough:

```bash
http simple-service.{env}.surfline.com
```

```shellscript
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 48
Content-Type: application/json; charset=utf-8
Date: Fri, 30 Apr 2021 22:08:41 GMT
ETag: W/"30-HM66ciJmEZ0MbHgDk7VhQBAhGgo"
X-Powered-By: Express

{
    "message": "OK",
    "status": 200,
    "version": "latest"
}
```

Replace the `{env}` string with the name of the environment you're working on.

Available environments are: `sandbox`, `staging`, `prod`.

[Wavetrak Infrastructure Core GPU]: https://github.com/Surfline/wavetrak-infrastructure/terraform/wavetrak-core-gpu
[Terraform]: https://www.terraform.io/
[deplo-service-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/
[creates an ECR repository]: https://surfline-jenkins-master-prod.surflineadmin.com/job/misc/job/docker/job/create-ecr-repository/160/console
[see the image you've just pushed]: https://us-west-1.console.aws.amazon.com/ecr/repositories/private/833713747344/services/simple-service?region=us-west-1
