import newrelic from 'newrelic';
import { wrapErrors } from '@surfline/services-common/dist';
import { Router } from 'express';
import * as logger from '../common/logger';
import { getEntitlements } from '../model/entitlements';
import * as userModel from '../model/user';

const DEFAULT_ENTITLEMENTS = {
  legacyEntitlements: [],
  entitlements: [],
  promotions: [],
  hasNewAccount: true,
};

export function entitlements() {
  const api = Router();
  const log = logger.logger('entitlements-service');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/entitlements',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const getEntitlementsHandler = async (req, res) => {
    const {
      query: { objectId },
      authenticatedUserId,
    } = req;

    const userId = authenticatedUserId || objectId;

    if (!userId) {
      log.warn('Entitlements requested without a valid identifier');
      return res.status(400).send({ message: 'You must specify a valid identifier' });
    }

    try {
      const user = await userModel.findUser(userId);
      if (!user) {
        return res.status(200).json(DEFAULT_ENTITLEMENTS);
      }
      const userEntitlements = await getEntitlements(user);
      return res.status(200).json(userEntitlements);
    } catch (error) {
      log.error({
        action: 'Entitlements Service - Error returned from getEntitlementsHandler',
        error: error.stack,
      });
      newrelic.noticeError(error);
      return res.status(400).send({ message: error.message });
    }
  };

  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getEntitlementsHandler(req, res)),
  );

  return api;
}
