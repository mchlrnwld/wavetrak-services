import newrelic from 'newrelic';
import { setupExpress } from '@surfline/services-common';
import { entitlements } from './entitlements';
import { admin } from './admin';
import { logger } from '../common/logger';
import * as dbContext from '../model/dbContext';

const log = logger('entitlements-service:server');

const customErrorLogger = (error, req, _, next) => {
  const userId = req.headers['x-auth-userid'] || -1;

  newrelic.addCustomAttribute('userId', userId);
  try {
    log.error({
      userId,
    });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log({ userId });
  }

  return next(error);
};

const startApp = () => {
  setupExpress({
    log,
    allowedMethods: 'GET, HEAD, OPTIONS, POST, PUT, DELETE',
    name: 'entitlements-service',
    port: process.env.EXPRESS_PORT || 8080,
    handlers: [
      ['/Entitlements', entitlements()],
      ['/Admin/entitlements', admin()],
      customErrorLogger,
    ],
  });
};

Promise.all([dbContext.initMongoDB()])
  .then(startApp)
  .catch((error) => {
    log.error({ message: "App Initialization failed. Can't connect to database", stack: error });
    process.exit(1);
  });

export default startApp;
