import { Router } from 'express';
import * as bodyParser from 'body-parser';
import * as logger from '../common/logger';
import * as adminModel from '../model/admin';

const jsonBodyParser = bodyParser.json();

export function admin() {
  const api = Router();
  const log = logger.logger('adminEntitlements-service');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/Admin/Entitlements',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const getUserEntitlementsHandler = async (req, res) => {
    try {
      const user = await adminModel.getUserAccount(req.params.userId);
      return res.send(user);
    } catch (err) {
      log.error({
        message: 'Unable to get entitlements for user',
        stack: err,
      });
      if (err.statusCode) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      return res.sendStatus(500);
    }
  };

  const getEntitlementsHandler = async (req, res) => {
    if (req.query.objectId && req.query.objectId.length > 0) {
      try {
        const userIdList = req.query.objectId;
        const userAccountList = await adminModel.getUserAccounts(userIdList);
        return res.status(200).json(userAccountList);
      } catch (err) {
        log.error({
          message: 'Entitlements Service getHandler.',
          stack: err,
        });
        if (err.statusCode) {
          return res.status(err.statusCode).json({ message: err.message });
        }
        return res.sendStatus(500);
      }
    } else {
      log.warn('Entitlements requested without a valid identifier');
      return res.status(400).send({ message: 'You must specify a valid identifier' });
    }
  };

  api.use('*', jsonBodyParser);
  api.get('/:userId/Entitlements', trackRequest, getUserEntitlementsHandler);
  api.get('/', trackRequest, getEntitlementsHandler);

  return api;
}
