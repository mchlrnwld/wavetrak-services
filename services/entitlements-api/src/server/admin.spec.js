/* eslint no-unused-expressions:0 */

import express from 'express';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import * as logger from '../common/logger';

const model = require('../model/entitlements');
const adminModel = require('../model/admin');
const { entitlements } = require('./entitlements');
const { admin: adminEntitlements } = require('./admin');

const { expect } = chai;
// const assert = chai.assert;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('entitlements/admin route', () => {
  let loggerStub;
  let getUserAccountsStub;
  let getUserAccountStub;
  let getEntitlementsStub;

  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace: () => {},
      debug: () => {},
      info: () => {},
      error: () => {},
      warn: () => {},
    });

    app.use('/entitlements', entitlements());
    app.use('/admin/entitlements', adminEntitlements());
  });

  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    getEntitlementsStub = sinon.stub(model, 'getEntitlements').returnsPromise();
    getUserAccountsStub = sinon.stub(adminModel, 'getUserAccounts').returnsPromise();
    getUserAccountStub = sinon.stub(adminModel, 'getUserAccount').returnsPromise();
  });

  afterEach(() => {
    getEntitlementsStub.restore();
    getUserAccountsStub.restore();
    getUserAccountStub.restore();
  });

  it('should initialize as an object', (done) => {
    expect(entitlements).to.exist;
    expect(adminEntitlements).to.exist;
    done();
  });

  it('should return entitlements when requested with objectId', (done) => {
    chai
      .request(app)
      .get('/admin/entitlements')
      .query({ objectId: '5707e96e5c9640bb2bc6ee83' })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    getUserAccountsStub.resolves([
      {
        _id: '5707e96e5c9640bb2bc6ee83',
        user: null,
        entitlements: [
          {
            _id: '572accef7789433c11b78cd3',
            name: 'sl_premium',
            features: ['vip', 'hd_cams', 'ad_free', 'premium', 'test', 'night_vision'],
          },
        ],
      },
    ]);
  });

  it('should return entitlements when requested with objectId', (done) => {
    chai
      .request(app)
      .get('/admin/entitlements')
      .query({ objectId: ['5707e96e5c9640bb2bc6ee83', '5707e96e5c9640bb2bc6ee84'] })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    getUserAccountsStub.resolves([
      {
        _id: '5707e96e5c9640bb2bc6ee83',
        user: {
          _id: '5733b498de23de02271f1f69',
          firstName: 'Aj',
          lastName: 'Mn',
          usID: 769125,
          brand: 'sl',
        },
        entitlements: [
          {
            _id: '572accef7789433c11b78cd3',
            name: 'sl_premium',
            features: ['vip', 'hd_cams', 'ad_free', 'premium', 'test', 'night_vision'],
          },
        ],
      },
      {
        _id: '5707e96e5c9640bb2bc6ee84',
        user: {
          _id: '5733b498de23de02271f1f70',
          firstName: 'Sc',
          lastName: 'Di',
          usID: 769127,
          brand: 'bw',
        },
        entitlements: [
          {
            _id: '572accef7789433c11b78cd4',
            name: 'bw_premium',
            features: ['premium', 'test'],
          },
        ],
      },
    ]);
  });

  it('should return a 500 internal server error if one occurs', (done) => {
    chai
      .request(app)
      .get('/admin/entitlements')
      .query({ objectId: 9999 })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(500);
        done();
      });
    getUserAccountsStub.rejects({});
  });

  it('should return a 500 internal server error if one occurs', (done) => {
    chai
      .request(app)
      .get('/admin/entitlements')
      .query({ objectId: [9999, 9998] })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(500);
        done();
      });
    getUserAccountsStub.rejects({});
  });

  it('should return a 400 error when unidentified parameters are sent.', (done) => {
    chai
      .request(app)
      .get('/admin/entitlements')
      .query({ invalidParam: 1234 })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.be.json;
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return user account for userId', async (done) => {
    const userId = '574f526bf8bcdf7c0dc190d9';
    const userAccount = {
      user: {
        _id: userId,
        firstName: 'Test',
        lastName: 'Account',
        email: 'test@account.com',
      },
      entitlements: [{ name: 'sl_premium' }],
    };
    getUserAccountStub.resolves(userAccount);

    try {
      const res = await chai.request(app).get(`/admin/entitlements/${userId}/entitlements`).send();
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(userAccount);
      return done();
    } catch (err) {
      return done(err);
    }
  });
});
