/* eslint no-unused-expressions:0 */

import express from 'express';
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import * as logger from '../common/logger';

const model = require('../model/entitlements');
const { entitlements } = require('./entitlements');
const user = require('../model/user');

const { expect } = chai;
// const assert = chai.assert;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('entitlements route', () => {
  let loggerStub;
  let findUserStub;
  let findUserPromise;
  let getEntitlementsStub;
  let getEntitlementsPromise;
  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    loggerStub = sinon.stub(logger, 'logger').returns({
      trace: () => {},
      debug: () => {},
      info: () => {},
      error: () => {},
      warn: () => {},
    });
    app.use('/entitlements', entitlements());
  });

  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    getEntitlementsStub = sinon.stub(model, 'getEntitlements');
    getEntitlementsPromise = getEntitlementsStub.returnsPromise();

    findUserStub = sinon.stub(user, 'findUser');
    findUserPromise = findUserStub.returnsPromise();
  });

  afterEach(() => {
    getEntitlementsStub.restore();
    findUserStub.restore();
  });

  it('should initialize as an object', (done) => {
    expect(entitlements).to.exist;
    done();
  });

  it('should return entitlements when requested with an objectId', (done) => {
    chai
      .request(app)
      .get('/entitlements')
      .query({ objectId: 1234 })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        done();
      });
    findUserPromise.resolves({ user: '1234' });
    getEntitlementsPromise.resolves({ entitlements: 'new' });
  });

  it('should return a 400 error when unidentified parameters are sent.', (done) => {
    chai
      .request(app)
      .get('/entitlements')
      .query({ invalidParam: 1234 })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.be.json;
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return a 400 error if an invalid objectId is passed', (done) => {
    chai
      .request(app)
      .get('/entitlements')
      .query({ objectId: 9999 })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        done();
      });
    findUserPromise.rejects({ statusCode: 400 });
  });

  it('should return a 400 error if one occurs', (done) => {
    chai
      .request(app)
      .get('/entitlements')
      .query({ objectId: 9999 })
      .end((err, res) => {
        expect(err).to.not.be.null;
        expect(res).to.have.status(400);
        done();
      });
    findUserPromise.rejects({});
  });
});
