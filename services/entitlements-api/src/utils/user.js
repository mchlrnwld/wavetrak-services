export const user = {
  _id: '5abc6def',
};

export const activeUserAccount = {
  _id: 'abc123',
  userId: '5abc6def',
  entitlements: [{ name: 'sl_premium' }],
  subscriptions: [
    {
      type: 'stripe',
      subscriptionId: '54321',
      entitlement: 'sl_premium',
    },
  ],
  archivedSubscriptions: ['testing expired'],
};

export const expiredUserAccount = {
  _id: 'abc123',
  userId: '5abc6def',
  entitlements: [],
  subscriptions: [],
  archivedSubscriptions: ['testing expired'],
};
