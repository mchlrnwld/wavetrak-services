export const legacyEntitlements = [
  {
    userType_alias: 'premium',
    company: 'sl',
    userType_name: 'Premium Surfline User',
    isActive: true,
  },
];

export const mappedLegacyEntitlements = {
  legacyEntitlements,
  entitlements: [],
};

export const mappedDefaultLegacyEntitlements = {
  legacyEntitlements: [
    {
      userType_alias: 'sl_basic',
      company: 'sl',
      userType_name: 'Basic Surfline User',
      isActive: true,
    },
  ],
  entitlements: [],
};

export const legacyProPhotoEntitlements = [
  {
    userType_alias: 'sl_prophotog',
    company: 'sl',
    userType_name: 'Surfline Pro Photographer',
    isActive: true,
  },
];

export const legacyMarineEntitlements = [
  {
    userType_alias: 'bw_basic',
    company: 'bw',
    userType_name: 'Registered Buoyweather User',
    isActive: false,
  },
  {
    userType_alias: 'fs_basic',
    company: 'fs',
    userType_name: 'Registered Fishtrack User',
    isActive: false,
  },
];

export const allBasicLegacyEntitlements = [
  legacyMarineEntitlements[0],
  legacyMarineEntitlements[1],
  mappedDefaultLegacyEntitlements.legacyEntitlements[0],
];

export const premiumSurflineWithMarineEntitlements = [
  legacyMarineEntitlements[0],
  legacyMarineEntitlements[1],
  mappedLegacyEntitlements.legacyEntitlements[0],
];

export const correctedLegacyEntitlements = [
  legacyMarineEntitlements[0],
  legacyMarineEntitlements[1],
  legacyProPhotoEntitlements[0],
  mappedDefaultLegacyEntitlements.legacyEntitlements[0],
];
