import mongoose from 'mongoose';
import UserModel from './UserInfo';

/**
 * Returns a user object
 *
 * @param identifier('id' or 'usID'), value(objectId or usID)
 * @returns {Promise}
 */
export const findUser = async (userId) => {
  if (mongoose.Types.ObjectId.isValid(userId)) {
    const user = await UserModel.findOne({ _id: userId });
    return user;
  }
  const user = await UserModel.findOne({ usID: userId });
  return user;
};
