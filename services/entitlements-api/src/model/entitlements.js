import { remove, split, concat } from 'lodash';
import UserAccountModel from './UserAccount';
import MeterModel from './Meter';
import SubscriptionModel from './Subscription';
import SubscriptionUsersModel from './SubscriptionUsers';
import legacyMap from './legacy-map';

export function convertNewToLegacy(activeEntitlementTypes = []) {
  const legacyEntitlements = [];

  if (activeEntitlementTypes.length) {
    activeEntitlementTypes.forEach((type) => {
      switch (type) {
        case 'sl_premium':
          legacyEntitlements.push(legacyMap.premium);
          break;
        case 'bw_premium':
          legacyEntitlements.push(legacyMap.buoyweather.premium);
          break;
        case 'fs_premium':
          legacyEntitlements.push(legacyMap.fishtrack.premium);
          break;
        default:
          break;
      }
    });
  } else {
    legacyEntitlements.push(legacyMap.registered);
    legacyEntitlements.push(legacyMap.buoyweather.registered);
    legacyEntitlements.push(legacyMap.fishtrack.registered);
  }
  return legacyEntitlements;
}

/**
 * @param sqlEntitlements
 * For consitency accross all products, all users should have the
 * minimum basic entitlement for each brand.
 */
export function addBasicEntitlements(sqlEntitlements = []) {
  const baseEntitlements = [
    legacyMap.registered,
    legacyMap.buoyweather.registered,
    legacyMap.fishtrack.registered,
  ];
  // eslint-disable-next-line no-restricted-syntax
  for (const entitlement of sqlEntitlements) {
    if (
      entitlement.userType_alias.includes('premium') ||
      entitlement.userType_alias.includes('basic')
    ) {
      remove(baseEntitlements, (base) => base.company === entitlement.company);
    }
  }
  return concat(sqlEntitlements, baseEntitlements);
}

/**
 * If entitlements already exits for any user, remove "premium" and
 * pass them along.
 */
export function computeLegacyEntitlements(activeEntitlementTypes) {
  const convertedEntitlements = convertNewToLegacy(activeEntitlementTypes);
  return addBasicEntitlements(convertedEntitlements);
}

export const computeMeteredEntitlement = async (entitlements, userId) => {
  if (!entitlements) return [];
  const isSLPremium = entitlements.indexOf('sl_premium') > -1;
  if (isSLPremium) return [];
  const meterExists = await MeterModel.exists({ user: userId, active: true });
  if (meterExists) return ['sl_metered'];
  return [];
};

export const getSubscriptionEntitlements = async (userId) => {
  const subscriptions = await SubscriptionModel.find({ user: userId });
  const v1Accounts = await UserAccountModel.find(
    { user: userId },
    { subscriptions: 1, archivedSubscriptions: 1 },
  );

  const v1Subscriptions = v1Accounts
    .map(({ subscriptions: v1Subs }) => v1Subs)
    .flat()
    .filter(({ type }) => type !== 'gift');

  const v1ArchivedSubscriptions = v1Accounts
    .map(({ archivedSubscriptions }) => archivedSubscriptions)
    .flat()
    .filter(({ type }) => type !== 'gift');

  const subscriptionDuplicates = await SubscriptionUsersModel.find({
    user: userId,
  });

  const activeSubscriptions = subscriptions.filter(({ active }) => !!active);
  const inactiveSubscriptions = subscriptions.filter(({ active }) => !active);
  const activeDuplicates = subscriptionDuplicates.filter(
    ({ active, isEntitled }) => !!active && isEntitled !== false,
  );
  const activeEntitlements = [...activeSubscriptions, ...v1Subscriptions, ...activeDuplicates].map(
    ({ entitlement }) => entitlement,
  );
  const inactiveEntitlements = [...inactiveSubscriptions, ...v1ArchivedSubscriptions].map(
    ({ entitlement }) => entitlement,
  );

  const freeTrialEntitlements = ['sl_free_trial', 'bw_free_trial', 'fs_free_trial'];
  remove(freeTrialEntitlements, (freeTrialEntitlement) => {
    const brand = split(freeTrialEntitlement, '_', 1)[0];
    const premiumEntitlement = `${brand}_premium`;
    const pendingEntitlement = `${brand}_pending`;
    return (
      activeEntitlements.includes(premiumEntitlement) ||
      inactiveEntitlements.includes(premiumEntitlement) ||
      activeEntitlements.includes(pendingEntitlement)
    );
  });

  remove(activeEntitlements, (entitlement) => {
    const brand = split(entitlement, '_', 1)[0];
    return entitlement.includes('pending') && activeEntitlements.includes(`${brand}_premium`);
  });

  return { entitlements: activeEntitlements, promotions: freeTrialEntitlements };
};

export const getEntitlements = async (userId) => {
  const { entitlements, promotions } = await getSubscriptionEntitlements(userId);
  const meteredEntitlement = await computeMeteredEntitlement(entitlements, userId);
  const legacyEntitlements = computeLegacyEntitlements(entitlements);
  return {
    legacyEntitlements,
    entitlements: [...entitlements, ...meteredEntitlement],
    promotions,
    hasNewAccount: true,
  };
};
