import mongoose from 'mongoose';

const { Schema } = mongoose;

const userAccountSchema = Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: 'UserInfo' },
    subscriptions: [Schema.Types.Mixed],
    archivedSubscriptions: [Schema.Types.Mixed],
    premiumPending: [
      {
        type: Schema.Types.String,
      },
    ],
  },
  { collection: 'UserAccounts' },
);

export default mongoose.model('UserAccount', userAccountSchema);
