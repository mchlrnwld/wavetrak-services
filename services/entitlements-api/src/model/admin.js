import * as logger from '../common/logger';
import UserAccountModel from './UserAccount';

const { ObjectId } = require('mongoose').Types;

const log = logger.logger('entitlements-service:model');

export function getUserAccount(id) {
  return new Promise((resolve, reject) => {
    UserAccountModel.findOne({ user: new ObjectId(id) })
      .populate('user')
      .exec((err, data) => {
        if (err) {
          log.error({
            action: 'getStripeEntitlements():find',
            stack: err,
          });
          reject(err);
        } else {
          resolve(data);
        }
      });
  });
}

export const getUserAccounts = (idList) =>
  UserAccountModel.find({ user: { $in: idList } })
    .select('_id user')
    .populate([{ path: 'user', select: '_id firstName lastName brand usID' }]);
