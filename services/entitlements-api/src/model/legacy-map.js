export default {
  premium: {
    userType_alias: 'premium',
    userType_name: 'Premium Surfline User',
    company: 'sl',
    isActive: true,
  },
  registered: {
    userType_alias: 'sl_basic',
    userType_name: 'Basic Surfline User',
    company: 'sl',
    isActive: true,
  },
  expired: {
    userType_alias: 'premium',
    userType_name: 'Premium Surfline User',
    company: 'sl',
    isActive: false,
  },
  buoyweather: {
    premium: {
      userType_alias: 'bwpremium',
      company: 'bw',
      userType_name: 'Premium Buoyweather User',
      isActive: true,
    },
    registered: {
      userType_alias: 'bw_basic',
      company: 'bw',
      userType_name: 'Registered Buoyweather User',
      isActive: false,
    },
    expired: {
      userType_alias: 'bwpremium',
      company: 'bw',
      userType_name: 'Premium Buoyweather User',
      isActive: false,
    },
  },
  fishtrack: {
    premium: {
      userType_alias: 'fspremium',
      company: 'fs',
      userType_name: 'Premium Fishtrack User',
      isActive: true,
    },
    registered: {
      userType_alias: 'fs_basic',
      company: 'fs',
      userType_name: 'Registered Fishtrack User',
      isActive: false,
    },
    expired: {
      userType_alias: 'fspremium',
      company: 'fs',
      userType_name: 'Premium Fishtrack User',
      isActive: false,
    },
  },
};
