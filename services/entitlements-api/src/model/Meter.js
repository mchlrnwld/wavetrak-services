import mongoose from 'mongoose';

const { Schema } = mongoose;

const meterSchema = Schema(
  {
    _id: { type: Schema.Types.ObjectId },
    user: { type: Schema.Types.ObjectId },
    anonymousId: { type: String },
    active: { type: Boolean },
  },
  { collection: 'Meters' },
);

export default mongoose.model('Meter', meterSchema);
