import mongoose from 'mongoose';

const { Schema } = mongoose;

const subscriptionSchema = Schema(
  {
    user: { type: Schema.Types.ObjectId },
    type: { type: Schema.Types.String },
    entitlement: { type: Schema.Types.String },
    active: { type: Schema.Types.Boolean },
    paymentAlertType: { type: Schema.Types.String },
    paymentAlertStatus: { type: Schema.Types.String },
  },
  { collection: 'Subscriptions' },
);

export default mongoose.model('subscriptions', subscriptionSchema);
