import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'SubscriptionUsers',
};

const SubscriptionUsersSchema = new mongoose.Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    entitlement: {
      type: Schema.Types.String,
    },
    active: {
      type: Schema.Types.Boolean,
    },
    isEntitled: {
      type: Schema.Types.Boolean,
    },
  },
  options,
);

SubscriptionUsersSchema.index({ user: 1 });

export default mongoose.model('SubscriptionUser', SubscriptionUsersSchema);
