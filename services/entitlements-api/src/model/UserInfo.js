import mongoose from 'mongoose';

const { Schema } = mongoose;

const userInfoSchema = Schema(
  {
    _id: { type: Schema.Types.ObjectId },
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String },
    usID: { type: Number },
  },
  { collection: 'UserInfo' },
);

export default mongoose.model('UserInfo', userInfoSchema);
