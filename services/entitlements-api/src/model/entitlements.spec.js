/* eslint-disable no-unused-expressions */
import chai from 'chai';
import chaihttp from 'chai-http';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import * as logger from '../common/logger';
import * as entitlements from './entitlements';
import * as utils from '../utils/entitlement';
import * as user from '../utils/user';
import Meter from './Meter';
import Subscription from './Subscription';
import SubscriptionUsers from './SubscriptionUsers';
import UserAccount from './UserAccount';
import legacyMap from './legacy-map';

const { expect } = chai;
const { assert } = chai;
chai.should();
chai.use(chaihttp);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('entitlement model', () => {
  let loggerStub;
  let getEntitlementsStub;
  let getMeterStub;
  let getMeterPromise;
  let getSubscriptionEntitlementstub;
  let getSubscriptionPromise;
  let getUserAccountStub;
  let getUserAccountPromise;
  let getSubscriptionDuplicateStub;
  let getSubscriptionDuplicatePromise;

  before(() => {
    loggerStub = sinon.stub(logger, 'logger').returns({
      /* eslint-disable no-console */
      info: console.log,
      error: console.log,
      debug: console.log,
      trace: console.log,
      // eslint-enable no-console
    });
  });

  after(() => {
    loggerStub.restore();
  });

  beforeEach(() => {
    getEntitlementsStub = sinon.stub(entitlements, 'getEntitlements');
    getEntitlementsStub.returnsPromise();

    getMeterStub = sinon.stub(Meter, 'exists');
    getMeterPromise = getMeterStub.returnsPromise();

    getSubscriptionEntitlementstub = sinon.stub(Subscription, 'find');
    getSubscriptionPromise = getSubscriptionEntitlementstub.returnsPromise();

    getUserAccountStub = sinon.stub(UserAccount, 'find');
    getUserAccountPromise = getUserAccountStub.returnsPromise();

    getSubscriptionDuplicateStub = sinon.stub(SubscriptionUsers, 'find');
    getSubscriptionDuplicatePromise = getSubscriptionDuplicateStub.returnsPromise();
  });

  afterEach(() => {
    getEntitlementsStub.restore();
    getMeterStub.restore();
    getSubscriptionEntitlementstub.restore();
    getUserAccountStub.restore();
    getSubscriptionDuplicateStub.restore();
  });

  it('should initialize as an object', () => {
    assert(expect(entitlements).to.exist);
  });

  it('should return entitlements for a Legacy user', () => {
    entitlements.getEntitlements(user.user, true).then((res) => {
      expect(res).to.exist;
      expect(res).to.eql({
        legacyEntitlements: [
          {
            userType_alias: 'sl_basic',
            company: 'sl',
            userType_name: 'Basic Surfline User',
            isActive: true,
          },
        ],
        entitlements: [],
      });
    });
  });

  it('should return an error if legacy entitlements are not found', () => {
    entitlements.getEntitlements(user.user).then((res) => {
      expect(res).to.exist;
      expect(res).to.not.equal({
        legacyEntitlements: [
          {
            userType_alias: 'sl_basic',
            company: 'sl',
            userType_name: 'Basic Surfline User',
            isActive: true,
          },
        ],
        entitlements: [],
      });
    });
  });

  describe('entitlementsModel:convertNewToLegacy', () => {
    it('should convert premium to legacy premium', () => {
      const entitlementTypes = ['sl_premium'];
      const legacyEntitlements = entitlements.convertNewToLegacy(entitlementTypes);
      expect(legacyEntitlements).to.eql([legacyMap.premium]);
    });

    it('should return a registered user if no entitlements exist', () => {
      const entitlementTypes = [];
      const legacyEntitlements = entitlements.convertNewToLegacy(entitlementTypes);
      expect(legacyEntitlements).to.eql([
        legacyMap.registered,
        legacyMap.buoyweather.registered,
        legacyMap.fishtrack.registered,
      ]);
    });

    it('should return an empty array if none match', () => {
      const entitlementTypes = ['abcde'];
      const legacyEntitlements = entitlements.convertNewToLegacy(entitlementTypes);
      expect(legacyEntitlements).to.eql([]);
    });
  });

  describe('entitlementsModel: addBasicEntitlements', () => {
    const order = (a, b) => {
      if (a.company < b.company) {
        return -1;
      }
      if (a.company > b.company) {
        return 1;
      }
      return 0;
    };

    it('should add basic surfline entitlements', () => {
      const legacyEntitlements = entitlements.addBasicEntitlements(utils.legacyMarineEntitlements);
      expect(legacyEntitlements.sort(order)).to.eql(utils.allBasicLegacyEntitlements.sort(order));
    });

    it('should add basic entitlements if they are unexpectedly missing', () => {
      const legacyEntitlements = entitlements.addBasicEntitlements(
        utils.legacyProPhotoEntitlements,
      );
      expect(legacyEntitlements.sort(order)).to.eql(utils.correctedLegacyEntitlements.sort(order));
    });

    it('should remove basic entitlements if premium entitlements exist', () => {
      const legacyEntitlements = entitlements.addBasicEntitlements(utils.legacyEntitlements);
      expect(legacyEntitlements.sort(order)).to.eql(
        utils.premiumSurflineWithMarineEntitlements.sort(order),
      );
    });
  });

  describe('entitlements model: computeMeteredEntitlement', () => {
    it('should return an empty array if user is a Premium SL subscriber', async () => {
      const premiumPendingEntitlements = await entitlements.computeMeteredEntitlement(
        ['sl_premium'],
        '5b3401435bac30361a55bc28',
      );
      expect(premiumPendingEntitlements).to.eql([]);
    });
    it('should return sl_metered entitlement if Meter document exists', async () => {
      getMeterPromise.resolves(true);
      const premiumPendingEntitlements = await entitlements.computeMeteredEntitlement(
        [],
        '5b3401435bac30361a55bc28',
      );
      expect(premiumPendingEntitlements).to.eql(['sl_metered']);
    });
    it('should return an empty array if Meter document does not exists', async () => {
      getMeterPromise.resolves(false);
      const premiumPendingEntitlements = await entitlements.computeMeteredEntitlement(
        [],
        '5b3401435bac30361a55bc28',
      );
      expect(premiumPendingEntitlements).to.eql([]);
    });
  });

  describe('entitlements model: getSubscriptionEntitlements', () => {
    it('should return free trial entitlements for all brands', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal([]);
      expect(res.promotions).to.deep.equal(['sl_free_trial', 'bw_free_trial', 'fs_free_trial']);
    });
    it('should not return v1 gift subscription entitlements', async () => {
      getSubscriptionPromise.resolves([{ active: true, entitlement: 'sl_premium' }]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'sl_premium' }, { entitlement: 'bw_premium' }],
          subscriptions: [{ type: 'gift', entitlement: 'bw_premium' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['fs_free_trial']);
    });
    it('should return premium entitlements for Surfline Premium', async () => {
      getSubscriptionPromise.resolves([{ active: true, entitlement: 'sl_premium' }]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        { archivedSubscriptions: [{ entitlement: 'sl_premium' }], subscriptions: [] },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['bw_free_trial', 'fs_free_trial']);
    });
    it('should return premium entitlements for Surfline Premium', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'sl_premium' }],
          subscriptions: [{ entitlement: 'sl_premium', type: 'stripe' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['bw_free_trial', 'fs_free_trial']);
    });
    it('should return premium entitlements for Surfline Premium', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'bw_premium' }],
          subscriptions: [{ entitlement: 'sl_premium', type: 'stripe' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['fs_free_trial']);
    });
    it('should return premium entitlements for Surfline Premium', async () => {
      getSubscriptionPromise.resolves([
        { type: 'stripe', active: true, entitlement: 'sl_premium' },
      ]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'bw_premium', type: 'stripe' }],
          subscriptions: [],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['fs_free_trial']);
    });
    it('should return premium entitlements for Buoyweather Premium', async () => {
      getSubscriptionPromise.resolves([{ active: true, entitlement: 'bw_premium' }]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        { archivedSubscriptions: [{ entitlement: 'bw_premium' }], subscriptions: [] },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['bw_premium']);
      expect(res.promotions).to.deep.equal(['sl_free_trial', 'fs_free_trial']);
    });
    it('should return premium entitlements for Buoyweather Premium', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'bw_premium' }],
          subscriptions: [{ entitlement: 'bw_premium', type: 'stripe' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['bw_premium']);
      expect(res.promotions).to.deep.equal(['sl_free_trial', 'fs_free_trial']);
    });
    it('should return premium entitlements for Buoyweather Premium', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'fs_premium' }],
          subscriptions: [{ entitlement: 'bw_premium', type: 'stripe' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['bw_premium']);
      expect(res.promotions).to.deep.equal(['sl_free_trial']);
    });

    it('should return premium entitlements for Fishtrack Premium', async () => {
      getSubscriptionPromise.resolves([{ active: true, entitlement: 'fs_premium' }]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        { archivedSubscriptions: [{ entitlement: 'fs_premium' }], subscriptions: [] },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['fs_premium']);
      expect(res.promotions).to.deep.equal(['sl_free_trial', 'bw_free_trial']);
    });
    it('should return premium entitlements for Fishtrack Premium', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'fs_premium' }],
          subscriptions: [{ entitlement: 'fs_premium', type: 'stripe' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['fs_premium']);
      expect(res.promotions).to.deep.equal(['sl_free_trial', 'bw_free_trial']);
    });
    it('should return premium entitlements for Fishtrack Premium', async () => {
      getSubscriptionPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        {
          archivedSubscriptions: [{ entitlement: 'sl_premium' }],
          subscriptions: [{ entitlement: 'fs_premium', type: 'stripe' }],
        },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['fs_premium']);
      expect(res.promotions).to.deep.equal(['bw_free_trial']);
    });
    it('should remove the pending entilement in respect to Premium entitlement', async () => {
      getSubscriptionPromise.resolves([
        { active: true, entitlement: 'sl_premium' },
        { active: true, entitlement: 'sl_pending' },
      ]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        { archivedSubscriptions: [{ entitlement: 'sl_premium' }], subscriptions: [] },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['bw_free_trial', 'fs_free_trial']);
    });
    it('should return the pending entilement in lue of Premium entitlement', async () => {
      getSubscriptionPromise.resolves([{ active: true, entitlement: 'bw_pending' }]);
      getSubscriptionDuplicatePromise.resolves([]);
      getUserAccountPromise.resolves([
        { archivedSubscriptions: [{ entitlement: 'sl_premium' }], subscriptions: [] },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['bw_pending']);
      expect(res.promotions).to.deep.equal(['fs_free_trial']);
    });
    it('should return premium entitlements for duplicate subscriptions', async () => {
      getSubscriptionPromise.resolves([]);
      getUserAccountPromise.resolves([]);
      getSubscriptionDuplicatePromise.resolves([
        { active: true, entitlement: 'sl_premium' },
        { active: true, entitlement: 'sl_premium', isEntitled: false },
      ]);

      const res = await entitlements.getSubscriptionEntitlements('5b3401435bac30361a55bc28');
      expect(res.entitlements).to.deep.equal(['sl_premium']);
      expect(res.promotions).to.deep.equal(['bw_free_trial', 'fs_free_trial']);
    });
  });
});
