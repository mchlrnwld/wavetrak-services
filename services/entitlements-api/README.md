#Entitlements service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


  - [Getting Started](#getting-started)
- [API Endpoints](#api-endpoints)
  - [Get User Entitlements](#get-user-entitlements)
    - [Query Parameters](#query-parameters)
    - [Response Properties](#response-properties)
- [Admin Endpoints](#admin-endpoints)
  - [Get User Entitlements and Info](#get-user-entitlements-and-info)
    - [Request](#request)
    - [Response](#response)
  - [Add Entitlement](#add-entitlement)
    - [Request](#request-1)
    - [Response](#response-1)
  - [Remove Entitlement](#remove-entitlement)
    - [Request](#request-2)
    - [Response](#response-2)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Getting Started
Configure global environemnt -
`nvm install v4.3.0`
& `npm install -g eslint eslint-config-airbnb`

Configure .env file appropriately (.env.sample provided)

Configuring the project -
`npm install`

Testing the project -
`npm run test`

Run the project -
`npm run startlocal`


## API Endpoints

* Service URL: http://entitlements-service.prod.surfline.com
* Public URL: https://services.surfline.com/entitlements

### Get User Entitlements

Path: `/entitlements`

#### Query Parameters


_Note: If you are using the entitlements service from the services proxy, these query params are populated automatically via the user's access token. Access tokens can be passed either via the header value `X-Auth-AccessToken` or query param `accesstoken`._


|Param|Description|
|-----|-----------|
|`objectId`|The userId of the user to fetch entitlements for|
|`usID`|Used to fetch entitlements for legacy systems such as ColdFusion|


#### Response Properties

* `entitlements` (type `Array<String>`) - An array of entitlements for the user. You can find a list of valid entitlements in the `UserDB.Entitlements` collection. Eg `sl_premium`, `sl_premium_pending`
* `promotions` (type: `Array<String>`) - A list of promotions a user is eligible to receive. Eg `sl_free_trial`.
* `hasNewAccount` (type: `Boolean`) - Has an account with a mongo id. _Deprecated_.
* `legacyEntitlements` (type: `Array<Object>`) - These values are used for legacy systems. Don't rely on these unless you know why you're using them. _Deprecated_
	* `userType_alias` (type: `String`
	* `company` (type `Enum`)
	* `userType_name` (type: `String`)
	* `isActive` (type: `Boolean`)




## Admin Endpoints

_Admin endpoints will be mounted at tools.surflineadmin.com/api/entitlements._

Because these endpoints will be used for our admin tool kit, authentication will be handled before the request gets to the service and the service does not have to worry about authentication.

### Get User Entitlements and Info

#### Request

```
GET /api/entitlements/574f526bf8bcdf7c0dc190d9/entitlements HTTP/1.1
Host: tools.surflineadmin.com
Content-Type: application/json
```

#### Response

```
{
  "_id": "574f5285ce23bb28273b3258",
  "updatedAt": "2016-09-07T21:27:16.511Z",
  "createdAt": "2016-06-01T21:24:21.687Z",
  "user": {
    "_id": "574f526bf8bcdf7c0dc190d9",
    "firstName": "Spencer",
    "lastName": "Test21",
    "email": "spencer@test21.com",
    "password": "$2a$10$aMDSgR8uvKH5dFJwT9uW9uA.mronrwdk0MfWMm544epX1b32ul4dK",
    "usID": 1000045,
    "locale": "en-US",
    "brand": "sl",
    "modifiedDate": "2016-06-01T00:06:31.425Z",
    "isActive": true,
    "__v": 0
  },
  "__v": 64,
  "stripeCustomerId": "cus_8YoR0USFAtvsSo",
  "receipts": [],
  "winbacks": [
    {
      "percent_off": 10,
      "ref": "WB-10-OFF",
      "type": "winback",
      "code": "winback-10-574f5285ce23bb28273b3258-35"
    },
    {
      "percent_off": 20,
      "ref": "WB-20-OFF",
      "type": "winback",
      "code": "winback-20-574f5285ce23bb28273b3258-98"
    },
    {
      "percent_off": 50,
      "ref": "WB-50-OFF",
      "type": "winback",
      "code": "winback-50-574f5285ce23bb28273b3258-80"
    }
  ],
  "entitlements": [
    {
      "_id": "574cc9c268387f052613a373",
      "updated_at": "2016-06-02T18:44:20.443Z",
      "created_at": "2016-05-30T23:16:18.256Z",
      "name": "sl_premium",
      "__v": 3,
      "plans": [],
      "features": [
        "vip",
        "hd_cams",
        "add_free",
        "premium",
        "sl_premium"
      ]
    }
  ],
  "archivedSubscriptions": [],
  "subscriptions": [
    {
      "failedPaymentAttempts": 0,
      "expiring": false,
      "createdAt": "2016-06-01T21:24:21.679Z",
      "stripePlanId": "sl_premium_monthly",
      "entitlement": "sl_premium",
      "periodEnd": "2016-10-04T21:24:21.000Z",
      "periodStart": "2016-09-04T21:24:21.000Z",
      "subscriptionId": "sub_8YoSQLLdsLljxj",
      "type": "stripe"
    }
  ]
}
```

### Add Entitlement

#### Request

```
POST /api/entitlements/574f526bf8bcdf7c0dc190d9/entitlements HTTP/1.1
Host: tools.surflineadmin.com
Content-Type: application/json

{
  "entitlementId": "574cc9c268387f052613a373"
}
```

#### Response

```
[
  {
    "_id": "574cc9c268387f052613a373",
    "updated_at": "2016-06-02T18:44:20.443Z",
    "created_at": "2016-05-30T23:16:18.256Z",
    "name": "sl_premium",
    "__v": 3,
    "plans": [],
    "features": [
      "vip",
      "hd_cams",
      "add_free",
      "premium",
      "sl_premium"
    ]
  }
]
```

### Remove Entitlement

#### Request

``` DELETE /api/entitlements/574f526bf8bcdf7c0dc190d9/entitlements/574cc9c268387f052613a373 HTTP/1.1
Host: tools.surflineadmin.com
Content-Type: application/json
```

#### Response

```
{
  "message": "Success"
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `entitlements-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4T0RFM09UQTFOZyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE9ERTNPVEExTmciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests on the endpoints detailed in the [API Endpoints](#api-endpoints) and/or the [Admin Endpoints](#admin-endpoints) section above. 

**Note:** An `objectId ` must be passed as a parameter in the following `curl` request. This value corresponds to the `_id` value in the Mongo `userInfo` collection.

**Ex:**

Test `GET` requests against the `/entitlements` endpoint:

```
curl \
    -X GET \
    -i \
    http://entitlements-service.sandbox.surfline.com/entitlements?objectId=5cb8c278a9680b000ffc68be
```
**Expected response:** The response should look something like:

```
curl \
  -X GET \
  -i \
    http://entitlements-service.sandbox.surfline.com/entitlements\?objectId\=5cb8c278a9680b000ffc68be
HTTP/1.1 200 OK
Date: Wed, 25 Nov 2020 08:23:05 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 472
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Credentials, Authorization
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: GET, HEAD, OPTIONS, POST, PUT, DELETE
ETag: W/"1d8-43A7vsmOxzx58BGF67wCFS1SFyk"

{"entitlements":["sl_premium","false_premium","false_promotional"],"legacyEntitlements":[{"userType_alias":"premium","userType_name":"Premium Surfline User","company":"sl","isActive":true},{"userType_alias":"bw_basic","company":"bw","userType_name":"Registered Buoyweather User","isActive":false},{"userType_alias":"fs_basic","company":"fs","userType_name":"Registered Fishtrack User","isActive":false}],"hasNewAccount":true,"promotions":["bw_free_trial","fs_free_trial"]}% 
```
