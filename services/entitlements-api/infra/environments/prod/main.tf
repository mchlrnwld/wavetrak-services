provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/entitlements-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "entitlements_service" {
  source = "../../"

  company     = "sl"
  application = "entitlements-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 11
  service_lb_rules = [
    {
      field = "host-header"
      value = "entitlements-service.prod.surfline.com"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 1000
  auto_scaling_min_size     = 4
  auto_scaling_max_size     = 5000
  task_deregistration_delay = 60

  dns_name    = "entitlements-service.prod.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"
}
