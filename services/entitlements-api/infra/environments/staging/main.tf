provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/entitlements-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "entitlements_service" {
  source = "../../"

  company     = "sl"
  application = "entitlements-service"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "entitlements-service.staging.surfline.com"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-staging/26ee81426b4723db"

  auto_scaling_enabled      = false
  task_deregistration_delay = 60

  dns_name    = "entitlements-service.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"
}
