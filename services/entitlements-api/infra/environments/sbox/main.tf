provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/entitlements-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "entitlements_service" {
  source = "../../"

  company     = "sl"
  application = "entitlements-service"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "entitlements-service.sandbox.surfline.com"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac"

  auto_scaling_enabled      = false
  task_deregistration_delay = 60

  dns_name    = "entitlements-service.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"
}
