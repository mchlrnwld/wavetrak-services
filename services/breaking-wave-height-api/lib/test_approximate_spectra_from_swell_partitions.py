from typing import Any

import numpy as np  # type: ignore
import pytest  # type: ignore
from science_algorithms import SwellPartition  # type: ignore

from lib.approximate_spectra_from_swell_partitions import (
    _create_jonswap_spectrum,
    approximate_spectra_from_swell_partitions,
)
from lib.frequency_calculations import (
    create_discrete_frequency_steps,
    create_frequency_distribution,
)


def create_synthetic_height_from_spectra(
    discrete_frequencies: Any, direction_bin_size: float, spectra: Any
) -> float:
    """
    The synthentic height is calculated based on
    the approximated 2D spectra from approximate_spectra_from_swell_partitions
    algorithm. This function exists purely for testing purposes
    to ensure that the approximate_spectra_from_swell_partitions
    is approximating 2D spectra correctly. The synthentic swell height
    can be compared to the overall swell height to ensure accuracy of the
    approximated 2D spectra.

    Args:
        discrete_frequencies: A numpy array of discrete frequencies.
        direction_bin_size: Directional bin size in degrees
        spectra: 2D spectra to calculate synthentic height for.
    """
    return 4 * np.sqrt(
        sum(
            spectra[i, j]
            * discrete_frequencies[i]
            * direction_bin_size
            * np.pi
            / 180
            for i in range(spectra.shape[0])
            for j in range(spectra.shape[1])
        )
    )


def test_generate_jonswap_spectrum():
    frequencies = np.array(
        [
            0.035,
            0.03848704,
            0.0423215,
            0.04653798,
            0.05117455,
            0.05627306,
            0.06187953,
            0.06804458,
            0.07482385,
            0.08227853,
            0.09047592,
            0.09949002,
            0.10940219,
            0.12030191,
            0.13228757,
            0.14546735,
            0.15996023,
            0.17589704,
            0.19342162,
            0.21269218,
            0.23388266,
            0.25718434,
            0.28280756,
            0.31098362,
            0.34196685,
            0.37603694,
            0.41350142,
            0.45469848,
            0.5,
        ]
    )
    swell_1 = SwellPartition(1.77, 10.95, 182.0, 14.0)
    assert _create_jonswap_spectrum(frequencies, swell_1).tolist() == [
        5.813601230991276e-23,
        3.2467112617637467e-15,
        5.555243909005773e-10,
        1.8143312088610194e-06,
        0.00039526416848287076,
        0.013516308611074125,
        0.13026306166120602,
        0.5287704685013822,
        1.2348547384400745,
        2.7425005800726887,
        6.542781335157611,
        3.8922922813800147,
        1.719530485932127,
        1.1677982939898284,
        0.8261924264473354,
        0.5621050873000071,
        0.3717393453463003,
        0.24112108682490255,
        0.15433710012801438,
        0.09789596696200957,
        0.0617112281421481,
        0.03873649660045554,
        0.024244644982912084,
        0.0151442954485292,
        0.009446972520734094,
        0.005887526463289823,
        0.00366688495871793,
        0.0022828271673944072,
        0.00142075674660425,
    ]
    swell_2 = SwellPartition(0.27, 10.62, 166.0, 6.0)
    assert _create_jonswap_spectrum(frequencies, swell_2).tolist() == [
        8.130766688622038e-28,
        4.928387810231112e-19,
        4.307548716143756e-13,
        4.2919012530200575e-09,
        2.0050374501938926e-06,
        0.0001155264753563219,
        0.0015907672565281016,
        0.008233233756879734,
        0.022149385110219105,
        0.04618956491735427,
        0.12416894442254386,
        0.11780119889113232,
        0.04786131281922656,
        0.029474629732631603,
        0.021046613995457035,
        0.014486853756992407,
        0.009657519253260147,
        0.006298481195219793,
        0.004046638031479603,
        0.002573351473776656,
        0.001625016805156519,
        0.0010212530318749497,
        0.0006397113416698582,
        0.0003998160553564887,
        0.0002494996609726982,
        0.0001555334539332815,
        9.688709827198314e-05,
        6.0324655674572606e-05,
        3.7547229828961734e-05,
    ]


def test_approximate_spectra_from_swell_partitions():
    # Create 2D spectra from 3 swell partitions
    heights = np.asarray([1.77, 0.76, 0.27])
    periods = np.asarray([10.95, 8.32, 10.62])
    directions = np.asarray([182.0, 106.0, 166.0])
    spreads = np.asarray([14.0, 12.0, 6.0])
    swell_partitions = [
        SwellPartition(height, period, direction, spread)
        for height, period, direction, spread in zip(
            heights, periods, directions, spreads
        )
    ]
    # Actual overall swell height taken from the Lotus file.
    overall_swell_height = 1.95
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10
    spec_data = approximate_spectra_from_swell_partitions(
        swell_partitions=swell_partitions,
        minimum_frequency=minimum_frequency,
        maximum_frequency=maximum_frequency,
        direction_bin_size=direction_bin_size,
    )
    frequencies = create_frequency_distribution(
        minimum_frequency, maximum_frequency
    )
    discrete_frequencies = create_discrete_frequency_steps(frequencies)
    # Compare the synthetic height to the actual overall swell height.
    # The synthentic height is calculated from the approximated 2D Spectra.
    # If the difference between the synthentic height and actual overall height
    # is less than 1/10th of the actual overall height, then the function is
    # approximating 2D spectra correctly.
    assert create_synthetic_height_from_spectra(
        discrete_frequencies, direction_bin_size, spec_data
    ) == pytest.approx(overall_swell_height, abs=0.1)

    # Create 2D spectra from 6 swell partitions
    heights = np.asarray([1.47, 0.89, 0.62, 0.40, 0.23, 0.13, 0.12])
    periods = np.asarray([7.32, 11.79, 16.05, 16.08, 8.73, 11.05, 21.51])
    directions = np.asarray(
        [304.64, 307.74, 210.41, 301.98, 204.36, 222.12, 228.26]
    )
    spreads = np.asarray([20.71, 12.23, 16.16, 6.65, 8.54, 10.74, 8.74])
    swell_partitions = [
        SwellPartition(height, period, direction, spread)
        for height, period, direction, spread in zip(
            heights, periods, directions, spreads
        )
    ]
    # Actual overall swell height taken from the Lotus file.
    overall_swell_height = 1.89
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10
    spec_data = approximate_spectra_from_swell_partitions(
        swell_partitions=swell_partitions,
        minimum_frequency=minimum_frequency,
        maximum_frequency=maximum_frequency,
        direction_bin_size=direction_bin_size,
    )
    frequencies = create_frequency_distribution(
        minimum_frequency, maximum_frequency
    )
    discrete_frequencies = create_discrete_frequency_steps(frequencies)
    # Compare the synthetic height to the actual overall swell height.
    # The synthentic height is calculated from the approximated 2D Spectra.
    # If the difference between the synthentic height and actual overall height
    # is less than 1/10th of the actual overall height, then the function is
    # approximating 2D spectra correctly.
    assert create_synthetic_height_from_spectra(
        discrete_frequencies, direction_bin_size, spec_data
    ) == pytest.approx(overall_swell_height, abs=0.1)

    # Create 2D spectra from 6 swell partitions, with 4 noisy partitions
    heights = np.asarray([1.47, 0.89, 0.62, 0.04, 0.02, 0.02])
    periods = np.asarray([7.32, 11.79, 16.05, 0.4, 0.2, 0.2])
    directions = np.asarray([304.64, 307.74, 210.41, 301.98, 204.36, 222.12])
    spreads = np.asarray([20.71, 12.23, 16.16, 6.65, 8.54, 10.74])
    swell_partitions = [
        SwellPartition(height, period, direction, spread)
        for height, period, direction, spread in zip(
            heights, periods, directions, spreads
        )
    ]
    # Actual overall swell height taken from the Lotus file.
    overall_swell_height = 1.89
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10
    spec_data = approximate_spectra_from_swell_partitions(
        swell_partitions=swell_partitions,
        minimum_frequency=minimum_frequency,
        maximum_frequency=maximum_frequency,
        direction_bin_size=direction_bin_size,
    )
    frequencies = create_frequency_distribution(
        minimum_frequency, maximum_frequency
    )
    discrete_frequencies = create_discrete_frequency_steps(frequencies)
    # Compare the synthetic height to the actual overall swell height.
    # The synthentic height is calculated from the approximated 2D Spectra.
    # If the difference between the synthentic height and actual overall height
    # is less than 1/10th of the actual overall height, then the function is
    # approximating 2D spectra correctly.
    assert create_synthetic_height_from_spectra(
        discrete_frequencies, direction_bin_size, spec_data
    ) == pytest.approx(overall_swell_height, abs=0.1)

    # Expect 2D spectra of 0's when all swell partitions are noisy
    heights = np.asarray([0.04, 0.02, 0.03])
    periods = np.asarray([0.4, 0.3, 0.2])
    directions = np.asarray([10, 12, 5])
    spreads = np.asarray([14, 12, 6])
    swell_partitions = [
        SwellPartition(height, period, direction, spread)
        for height, period, direction, spread in zip(
            heights, periods, directions, spreads
        )
    ]
    spec_data = approximate_spectra_from_swell_partitions(swell_partitions)
    assert spec_data.tolist() == [[0.0 for i in range(36)] for j in range(29)]
