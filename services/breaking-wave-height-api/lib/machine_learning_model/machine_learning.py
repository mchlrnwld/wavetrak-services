from typing import Any, List, Tuple

import numpy as np  # type: ignore
from pkg_resources import resource_filename
from science_algorithms import SwellPartition  # type: ignore
from tensorflow import keras as k  # type: ignore

M_PER_FT = 12 * 2.54 * (1 / 100)
FT_PER_M = 100 * (1 / 2.54) * (1 / 12)


class SurfHeightML:
    """
    SurfHeightML loads the machine learning model for predicting surf heights
    for a set of 4 partitioned swells.
    """

    def __init__(self):
        self._model = k.models.load_model(
            resource_filename(__name__, 'v0.1.0.h5')
        )

    def _features_from_swell_partitions(self, swell_partitions) -> Any:
        """
        Transforms a list of swell partitions into a feature tensor for model
        inference.

        Args:
            swell_partitions: A list of SwellPartition objects.

        Returns:
            A 1x8 feature tensor (numpy array) with components:

            H1, P1, H2, P2, H3, P3, H4, P4

            where Hn and Pn are the nth height (ft) and period.
        """
        features = np.zeros((1, 8))
        nonzero_features = np.array(
            [
                [swell.height * FT_PER_M, swell.period]
                for swell in swell_partitions[:4]
            ]
        ).flatten()
        features[0][: nonzero_features.size] = nonzero_features
        return features

    def _model_heights_ft(
        self, swell_partitions: List[SwellPartition]
    ) -> Tuple[int, int]:
        """
        Runs model inference on a list of swell partitions and buckets the
        mean height into the correct min/max height (ft) range by closest mean
        height.

        Args:
            swell_partitions: A list of SwellPartition objects.

        Returns:
            A tuple of (min, max) surf heights (ft) in integers.
        """

    def calculate(
        self, swell_partitions: List[SwellPartition]
    ) -> Tuple[float, float]:
        """
        Calculates surf heights using machine learning model.

        Args:
            swell_partitions: A list of SwellPartition objects.

        Returns:
            A tuple of (min, max) surf heights (m).
        """
        results = self._model.predict(
            self._features_from_swell_partitions(swell_partitions)
        ).flatten()
        return results[1] * M_PER_FT, results[0] * M_PER_FT
