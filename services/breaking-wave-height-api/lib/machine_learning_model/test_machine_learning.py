import numpy as np  # type: ignore
from science_algorithms import SwellPartition  # type: ignore

from lib.machine_learning_model.machine_learning import FT_PER_M, SurfHeightML

swell_partitions = [
    SwellPartition(0.13, 11.85, 229.78),
    SwellPartition(0.17, 5.55, 141.8),
]


def test_surf_height_ml_features_from_swell_partitions():
    surf_height_ml = SurfHeightML()
    expected_features = np.array(
        [[0.13 * FT_PER_M, 11.85, 0.17 * FT_PER_M, 5.55, 0, 0, 0, 0]]
    )
    features = surf_height_ml._features_from_swell_partitions(swell_partitions)
    assert features.shape == expected_features.shape
    assert (features == expected_features).all()


def test_surf_height_ml():
    surf_height_ml = SurfHeightML()
    min_height, max_height = surf_height_ml.calculate(swell_partitions)
    assert round(min_height, 2) == 0.34
    assert round(max_height, 2) == 0.57
