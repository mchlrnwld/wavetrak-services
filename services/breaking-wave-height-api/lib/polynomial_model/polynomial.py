from typing import List, Tuple

from science_algorithms import (  # type: ignore
    SwellPartition,
    partition_wave_height_poly,
)


def surf_height_poly(
    swell_partitions: List[SwellPartition], optimal_swell_direction: float
) -> Tuple[float, float]:
    """
    Calculate a break wave forecast for a collection of swell partitions
    using a polynomial algorithm, and the spots optimal swell direction.

    Args:
        swell_partitions: A list of instances of SwellPartition
        optimal_swell_direction: Angle, in degrees, that is optimal for a swell
            to be coming from. E.g. 270 means swell coming from the west.

    Returns:
        A tuple of two floats. Typical breaking wave height, and (second)
        breaking wave height of sets. In meters.
    """
    # Constant multiplier for the breaking wave forecast range
    RATIO_AVG_HEIGHT = 0.64

    heights = [
        partition_wave_height_poly(partition, optimal_swell_direction)
        for partition in swell_partitions
        if abs(partition.angle_difference(optimal_swell_direction)) <= 110
    ]

    if not heights:
        # No swells at a good angle => flat.
        return (0, 0)

    height = max(heights)

    return (RATIO_AVG_HEIGHT * height, height)
