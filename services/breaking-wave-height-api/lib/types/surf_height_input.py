from typing import Any, Optional

from lib.approximate_spectra_from_swell_partitions import (
    approximate_spectra_from_swell_partitions,
)
from lib.constants import (
    DEFAULT_SPECTRA_DIRECTIONS,
    DEFAULT_SPECTRA_FREQUENCIES,
)


class SurfHeightInput:
    """
    Inputs for a single surf height calculation.

    Args:
        algorithm: Type of algorithm to use.
                   Options:
                   POLYNOMIAL, MACHINE_LEARNING,
                   SPECTRAL_REFRACTION
        swell_partitions: A list of instances of SwellPartition
        optimal_swell_direction: Angle, in degrees, that is optimal
                                 for a swell to be coming from.
                                 E.g. 270 means swell coming
                                 from the west. Defaults to None.
        breaking_wave_height_coefficient: A fixed multiplication factor
                                          applied to the spot's
                                          breaking wave forecast.
                                          Defaults to 1.
        breaking_wave_height_intercept: The breaking wave height intercept
                                        added to the surf height min and max.
                                        Defaults to 0.
        frequencies: Numpy array of 29 frequencies
                     for Lotus wave spectra. For the
                     SPECTRAL_REFRACTION algorithm.
        directions: Numpy array of 36 directions
                    for Lotus wave spectra. For the
                    SPECTRAL_REFRACTION algorithm.
        spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                             spectra wave energy values.
                             Calculate from Swell Partitions
                             if no array given.
                             For the SPECTRAL_REFRACTION algorithm.
        spectral_refraction_matrix: Matrix used to calculate surf heights.
                                    For the SPECTRAL_REFRACTION algorithm.
    """

    def __init__(
        self,
        algorithm: Optional[str],
        breaking_wave_height_coefficient: float = 0,
        breaking_wave_height_intercept: float = 1,
        swell_partitions=None,
        optimal_swell_direction: Optional[float] = None,
        spectra_wave_energy: Any = None,
        spectral_refraction_matrix: Any = None,
    ):
        self.algorithm = algorithm
        self.swell_partitions = (
            [
                swell_partition
                for swell_partition in swell_partitions
                if swell_partition
            ]
            if swell_partitions is not None
            else None
        )
        self.optimal_swell_direction = optimal_swell_direction
        self.breaking_wave_height_coefficient = (
            breaking_wave_height_coefficient
        )
        self.breaking_wave_height_intercept = breaking_wave_height_intercept
        self.frequencies = DEFAULT_SPECTRA_FREQUENCIES
        self.directions = DEFAULT_SPECTRA_DIRECTIONS
        self.spectra_wave_energy = (
            approximate_spectra_from_swell_partitions(swell_partitions)
            if spectra_wave_energy is None
            and algorithm == 'SPECTRAL_REFRACTION'
            and swell_partitions
            else spectra_wave_energy
        )
        self.spectral_refraction_matrix = spectral_refraction_matrix

    def __repr__(self):
        return (
            f'SurfHeightInput ('
            f'algorithm: {self.algorithm},'
            f'swell_partitions: {self.swell_partitions},'
            f'optimal_swell_direction: {self.optimal_swell_direction},'
            f'breaking_wave_height_intercept: '
            f'{self.breaking_wave_height_intercept},'
            f'breaking_wave_height_coefficient: '
            f'{self.breaking_wave_height_coefficient},'
            f'frequencies: {self.frequencies},'
            f'directions: {self.directions},'
            f'spectra_wave_energy: {self.spectra_wave_energy},'
            f'spectral_refraction_matrix: {self.spectral_refraction_matrix}'
            f')'
        )
