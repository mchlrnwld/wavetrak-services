import json
import os
from unittest import mock

import numpy as np  # type: ignore
import pytest  # type: ignore
from science_algorithms import SwellPartition  # type: ignore

from lib.spectral_refraction_model.spectral_refraction import (
    SpectralRefraction,
)
from lib.types.surf_height_input import SurfHeightInput


def test_spectral_refraction_calculate_valid_inputs():
    with open(
        os.path.join('fixtures', 'spectral_refraction_pois.json')
    ) as spectral_pois_file:
        values = json.load(spectral_pois_file)
        surf_height_inputs = [
            SurfHeightInput(
                'SPECTRAL_REFRACTION',
                spectra_wave_energy=np.array(value[0]),
                spectral_refraction_matrix=np.array(value[1]),
            )
            for value in values
        ]

        surf_heights = SpectralRefraction().calculate(surf_height_inputs)
        assert all(
            surf_height[0][0] == pytest.approx(value[2][0], abs=0.02, rel=0.02)
            and surf_height[0][1]
            == pytest.approx(value[2][1], abs=0.02, rel=0.02)
            for surf_height, value in zip(surf_heights, values)
            if surf_height[0] is not None
        )


def test_spectra_to_swell_partitions():
    spectral_refraction = SpectralRefraction()
    # Return the top 3 swell partitions.
    spectra = [[0.12, 0.08, 0.3], [0.45, 0.06, 0.02]]
    with mock.patch(
        (
            'lib'
            '.spectral_refraction_model'
            '.spectral_refraction'
            '.SpectralRefraction._sort_spectra'
        ),
        return_value=[[0.012, 0.08, 0.03], [0.05, 0.06, 0.02]],
    ):
        _, swell_partitions = spectral_refraction._spectra_to_swell_partitions(
            spectra
        )
        # All swell partitions have height and period > 0.0.
        assert all(swell_partitions)

    # Return empty swell partitions if spectra all 0.
    spectra = [[0.12, 0.08, 0.3], [0.45, 0.06, 0.02]]
    with mock.patch(
        (
            'lib'
            '.spectral_refraction_model'
            '.spectral_refraction'
            '.SpectralRefraction._sort_spectra'
        ),
        return_value=[[0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
    ):
        _, swell_partitions = spectral_refraction._spectra_to_swell_partitions(
            spectra
        )
        assert not all(swell_partitions)


def test_surf_heights_from_swell_partitions():
    spectral_refraction = SpectralRefraction()
    # Surf max is less than 3,
    # use overall swell height instead for surf min/max.
    overall_swell_height = 1.52
    swell_partitions = [
        SwellPartition(1.524, 7.633, None),
        SwellPartition(0.0, 0.0, None),
        SwellPartition(0.0, 0.0, None),
    ]
    (
        surf_min,
        surf_max,
    ) = spectral_refraction._surf_heights_from_swell_partitions(
        overall_swell_height, swell_partitions
    )
    assert surf_min == pytest.approx(0.16, abs=1e-2)
    assert surf_max == pytest.approx(0.47, abs=1e-2)

    # Invalid amount of swell partitions
    swell_partitions = [
        SwellPartition(0.0, 0.0, None),
        SwellPartition(0.0, 0.0, None),
    ]
    overall_swell_height = 4.0

    with pytest.raises(ValueError) as err:
        spectral_refraction._surf_heights_from_swell_partitions(
            overall_swell_height, swell_partitions
        )
        assert isinstance(err.value, ValueError)
        assert '3 Swell Partitions must be given' in str(err.value)

    # Surf min/max equal, use overall swell height instead.
    with mock.patch(
        (
            'lib'
            '.spectral_refraction_model'
            '.spectral_refraction'
            '.SpectralRefraction._calculate_surf_height'
        ),
        return_value=3.5,
    ):
        swell_partitions = [
            SwellPartition(2.5, 1.3, None),
            SwellPartition(3.2, 1.6, None),
            SwellPartition(3.1, 1.4, None),
        ]
        overall_swell_height = 4.0
        (
            surf_min,
            surf_max,
        ) = spectral_refraction._surf_heights_from_swell_partitions(
            overall_swell_height, swell_partitions
        )
        assert surf_min == pytest.approx(0.76, abs=1e-2)
        assert surf_max == pytest.approx(1.06, abs=1e-2)

    # Surf min/max equal, use  surf max to compute surf min.
    with mock.patch(
        (
            'lib'
            '.spectral_refraction_model'
            '.spectral_refraction'
            '.SpectralRefraction._calculate_surf_height'
        ),
        return_value=3.5,
    ):
        swell_partitions = [
            SwellPartition(2.5, 1.3, None),
            SwellPartition(3.2, 1.6, None),
            SwellPartition(3.1, 1.4, None),
        ]
        overall_swell_height = 4.0
        (
            surf_min,
            surf_max,
        ) = spectral_refraction._surf_heights_from_swell_partitions(
            overall_swell_height, swell_partitions
        )
        assert surf_min == pytest.approx(0.76, abs=1e-2)
        assert surf_max == pytest.approx(1.06, abs=1e-2)

    # Surf min is less than 0, set it to 0.
    with mock.patch(
        (
            'lib'
            '.spectral_refraction_model'
            '.spectral_refraction'
            '.SpectralRefraction._calculate_surf_height'
        ),
        return_value=0.28,
    ):
        swell_partitions = [
            SwellPartition(0.23, 1.3, None),
            SwellPartition(0.45, 1.6, None),
            SwellPartition(0.12, 1.4, None),
        ]
        overall_swell_height = 0.5
        (
            surf_min,
            surf_max,
        ) = spectral_refraction._surf_heights_from_swell_partitions(
            overall_swell_height, swell_partitions
        )
        assert surf_min == 0.0
        assert surf_max == pytest.approx(0.15, abs=1e-2)


def test_merge_spectra_with_matrix():
    with open(
        os.path.join('fixtures', 'lotus_directions.json')
    ) as directions_file, open(
        os.path.join('fixtures', 'lotus_frequencies.json')
    ) as frequencies_file, open(
        os.path.join('fixtures', 'merged_spectra.json')
    ) as merged_spectra_file, open(
        os.path.join('fixtures', 'example_poi_spectra.json')
    ) as spectra_file, open(
        os.path.join('fixtures', 'example_poi_matrix.json')
    ) as matrix_file:
        frequencies = json.load(frequencies_file)
        directions = json.load(directions_file)
        merged_spectra = json.load(merged_spectra_file)
        spectra = json.load(spectra_file)
        matrix = json.load(matrix_file)
        assert (
            SpectralRefraction()._merge_spectra_with_matrix(
                np.array(frequencies),
                np.array(directions),
                np.array(spectra),
                np.array(matrix),
            )
            == merged_spectra
        )


def test_calculate_surf_height():
    spectral_refraction = SpectralRefraction()
    swell_partitions = [
        SwellPartition(1.524, 7.633, None),
        SwellPartition(0.0, 0.0, None),
        SwellPartition(0.0, 0.0, None),
    ]
    min_factor, max_factor = (0.07, 0.10)
    assert spectral_refraction._calculate_surf_height(
        min_factor, swell_partitions
    ) == pytest.approx(1.11, abs=1e-2)
    assert spectral_refraction._calculate_surf_height(
        max_factor, swell_partitions
    ) == pytest.approx(1.33, abs=1e-2)

    # Invalid swell partitions length.
    swell_partitions = [
        SwellPartition(1.524, 7.633, None),
        SwellPartition(0.0, 0.0, None),
    ]
    with pytest.raises(ValueError) as err:
        spectral_refraction._calculate_surf_height(0.05, swell_partitions)
        assert isinstance(err.value, ValueError)
        assert '3 Swell Partitions must be given' in str(err.value)

    # Invalid multiplication factor.
    swell_partitions = [
        SwellPartition(1.524, 7.633, None),
        SwellPartition(0.0, 0.0, None),
        SwellPartition(0.0, 0.0, None),
    ]
    invalid_factor = -4.3
    with pytest.raises(ValueError) as err:
        spectral_refraction._calculate_surf_height(
            invalid_factor, swell_partitions
        )
        assert isinstance(err.value, ValueError)
        assert (
            'Multiplication factor must be greater than or equal to 0'
            in str(err.value)
        )
