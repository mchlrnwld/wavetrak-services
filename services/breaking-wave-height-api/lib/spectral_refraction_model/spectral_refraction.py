import math
from typing import Any, List, Optional, Tuple

import numpy as np  # type: ignore
from science_algorithms import SwellPartition  # type: ignore
from scipy.interpolate import RectBivariateSpline  # type: ignore

from lib.frequency_calculations import (
    create_discrete_frequency_steps,
    create_frequency_distribution,
)
from lib.types.surf_height_input import SurfHeightInput


class SpectralRefraction:
    """
    Initializes all values necessary to compute
    surf heights using the Spectral Refraction algorithm.
    """

    def __init__(self):
        self._direction_bin_size = 15
        minimum_frequency = 0.0418
        maximum_frequency = 0.411

        self._lola_directions = np.arange(
            self._direction_bin_size,
            360 + self._direction_bin_size,
            self._direction_bin_size,
        )

        self._lola_frequencies = create_frequency_distribution(
            minimum_frequency, maximum_frequency
        )

        self._discrete_frequency_steps = create_discrete_frequency_steps(
            self._lola_frequencies
        )

        self._frequencies = [
            0.0418,
            0.0459,
            0.0505,
            0.0556,
            0.0612,
            0.0673,
            0.0740,
            0.0814,
            0.0895,
            0.0985,
            0.108,
            0.119,
            0.131,
            0.144,
            0.159,
            0.174,
            0.192,
            0.211,
            0.232,
            0.255,
            0.281,
            0.309,
            0.340,
            0.374,
            0.411,
        ]

    def _spectra_to_swell_partitions(
        self, spectra: List[List[float]]
    ) -> Tuple[float, List[SwellPartition]]:
        """
        Generates the top 3 swell partitions
        from spectra.

        Args:
            spectra: Spectra from which to generate top 3 swell partitions.

        Returns:
            Overall swell height and the top 3 swell partitions.
        """

        overall_swell_height = (
            3.28 * 4 * math.sqrt(sum(data for row in spectra for data in row))
        )

        # Now find direction peaks.
        sorted_spectra = self._sort_spectra(spectra)

        # Then find the top 3 swell partitions.
        swell_partitions = []
        for swell_index, row_index_and_data in enumerate(
            sorted(
                [
                    (row_index, data)
                    for row_index, row in enumerate(sorted_spectra)
                    for data in row
                    if data > 0.0
                ],
                reverse=True,
                key=lambda row_index_and_data: row_index_and_data[1],
            )
        ):
            row_index, data = (row_index_and_data[0], row_index_and_data[1])
            height = 3.28 * 4 * math.sqrt(data)

            if height <= 0.05:
                break
            if height <= 0.5 and swell_index > 2:
                break

            period = 1.0 / self._frequencies[row_index]

            swell_partitions.append(
                SwellPartition(
                    height if height > 0.0 else 0.0,
                    period if 1.0 / period > 0.0 else 0.0,
                    None,
                )
            )

        return (
            overall_swell_height if overall_swell_height > 0.0 else 0.0,
            [
                swell_partitions[i]
                if i < len(swell_partitions)
                else SwellPartition(0.0, 0.0, None)
                for i in range(3)
            ],
        )

    def _sweep_through_spectra(
        self,
        spectra: List[List[float]],
        current_sorted_spectra: List[List[float]],
    ) -> List[List[float]]:
        """
        Sweeps through the spectra.

        Args:
            spectra: The original spectra unmodified.
            sorted_spectra: The sorted spectra to sweep and sort through.

        Returns:
            The previous sorted spectra and current sorted spectra.
        """
        spectra_row_length = len(spectra)
        spectra_column_length = len(spectra[0])
        previous_sorted_spectra = [
            [data for data in row] for row in current_sorted_spectra
        ]

        # First directional pass
        # First compare end directions
        for j in range(spectra_column_length):
            if spectra[1][j] >= spectra[spectra_row_length - 1][j]:
                current_sorted_spectra[1][j] = (
                    current_sorted_spectra[1][j]
                    + current_sorted_spectra[spectra_row_length - 1][j]
                )
                current_sorted_spectra[spectra_row_length - 1][j] = 0

        for i in range(1, spectra_row_length):
            for j in range(spectra_column_length):
                if spectra[i][j] >= spectra[i - 1][j]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i - 1][j]
                    )
                    current_sorted_spectra[i - 1][j] = 0

        # Second second directional pass
        # First compare end directions
        for j in range(spectra_column_length):
            if spectra[spectra_row_length - 1][j] >= spectra[1][j]:
                current_sorted_spectra[spectra_row_length - 1][j] = (
                    current_sorted_spectra[spectra_row_length - 1][j]
                    + current_sorted_spectra[1][j]
                )
                current_sorted_spectra[0][j] = 0

        for i in range(spectra_row_length - 2, -1, -1):
            for j in range(spectra_column_length):
                if spectra[i][j] >= spectra[i + 1][j]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i + 1][j]
                    )
                    current_sorted_spectra[i + 1][j] = 0

        # First frequency pass
        for j in range(1, spectra_column_length):
            for i in range(spectra_row_length):
                if spectra[i][j] >= spectra[i][j - 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i][j - 1]
                    )
                    current_sorted_spectra[i][j - 1] = 0

        # Second frequency pass
        for j in range(spectra_column_length - 2, -1, -1):
            for i in range(spectra_row_length):
                if spectra[i][j] >= spectra[i][j + 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i][j + 1]
                    )
                    current_sorted_spectra[i][j + 1] = 0

        # First diagonal pass
        for i in range(1, spectra_row_length):
            for j in range(1, spectra_column_length):
                if spectra[i][j] >= spectra[i - 1][j - 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i - 1][j - 1]
                    )
                    current_sorted_spectra[i - 1][j - 1] = 0

        # Second diagonal pass
        for i in range(spectra_row_length - 2, -1, -1):
            for j in range(spectra_column_length - 1):
                if spectra[i][j] >= spectra[i + 1][j + 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i + 1][j + 1]
                    )
                    current_sorted_spectra[i + 1][j + 1] = 0

        # Third diagonal pass
        for i in range(1, spectra_row_length):
            for j in range(spectra_column_length - 1):
                if spectra[i][j] >= spectra[i - 1][j + 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i - 1][j + 1]
                    )
                    current_sorted_spectra[i - 1][j + 1] = 0

        # Fourth diagonal pass
        for i in range(spectra_row_length - 2, -1, -1):
            for j in range(1, spectra_column_length):
                if spectra[i][j] >= spectra[i + 1][j - 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i + 1][j - 1]
                    )
                    current_sorted_spectra[i + 1][j - 1] = 0

        return previous_sorted_spectra

    def _sort_spectra(self, spectra: List[List[float]]) -> List[List[float]]:
        """
        Sort and sweep through spectra until the spectra no longer changes.
        Or until 100 passes have been made.

        Args:
            spectra: The spectra to sort and sweep through.

        Returns:
            Sorted spectra.
        """
        current_sorted_spectra = [[data for data in row] for row in spectra]

        for _ in range(100):
            previous_sorted_spectra = self._sweep_through_spectra(
                spectra, current_sorted_spectra
            )
            if all(
                math.isclose(
                    current_spectra_value,
                    previous_spectra_value,
                    abs_tol=1e-3,
                    rel_tol=1e-3,
                )
                for current_row, previous_row in zip(
                    current_sorted_spectra, previous_sorted_spectra
                )
                for current_spectra_value, previous_spectra_value in zip(
                    current_row, previous_row
                )
            ):
                break

        return current_sorted_spectra

    def _calculate_surf_height(
        self, factor: float, swell_partitions: List[SwellPartition]
    ) -> float:
        """
        Calculate surf heights given swell partitions
        and a multiplication factor.

        Args:
            factor: Multiplication factor to calculate surf height.
            sorted_spectra: The sorted spectra to sweep and sort through.

        Returns:
            Surf height.
        """
        if len(swell_partitions) < 3:
            raise ValueError('3 Swell Partitions must be given')

        if factor < 0.0:
            raise ValueError(
                'Multiplication factor must be greater than or equal to 0'
            )

        return math.sqrt(
            (
                swell_partitions[0].height
                * math.sqrt(swell_partitions[0].period * factor)
            )
            ** 2
            + (
                swell_partitions[1].height
                * math.sqrt(swell_partitions[1].period * factor)
            )
            ** 2
            + (
                swell_partitions[2].height
                * math.sqrt(swell_partitions[2].period * factor)
            )
            ** 2
        )

    def _normal_round(self, n):
        if n - math.floor(n) < 0.5:
            return math.floor(n)
        return math.ceil(n)

    def _surf_heights_from_swell_partitions(
        self,
        overall_swell_height: float,
        swell_partitions: List[SwellPartition],
    ) -> Tuple[float, float]:
        """
        Calculate surf height min and max.

        Args:
            overall_swell_height: Swell height used to calculate surf height.
            swell_partitions: Swell partitions used to calculate surf heights.

        Returns:
            Surf height min and max.
        """
        if len(swell_partitions) < 3:
            raise ValueError('3 Swell Partitions must be given')

        swell_partition_1, swell_partition_2, swell_partition_3 = (
            swell_partitions[0],
            swell_partitions[1],
            swell_partitions[2],
        )

        surf_min = self._calculate_surf_height(0.07, swell_partitions)
        surf_max = self._calculate_surf_height(0.10, swell_partitions)

        if surf_min < swell_partition_1.height:
            surf_min = swell_partition_1.height

        if surf_min < swell_partition_2.height:
            surf_min = swell_partition_2.height

        if surf_min < swell_partition_3.height:
            surf_min = swell_partition_3.height

        if surf_max < swell_partition_1.height:
            surf_max = swell_partition_1.height

        if surf_max < swell_partition_2.height:
            surf_max = swell_partition_2.height

        if surf_max < swell_partition_3.height:
            surf_max = swell_partition_3.height

        if surf_max <= 3:
            surf_max = overall_swell_height
            surf_min = overall_swell_height - 1

        if self._normal_round(surf_min) == self._normal_round(surf_max):
            surf_min = surf_max - 1

        if surf_min <= 0:
            surf_min = 0

        return surf_min / 3.28, surf_max / 3.28

    def _generate_lola_spectra(
        self, frequencies: Any, directions: Any, spectra_wave_energy: Any
    ) -> Any:
        """
        Generate LOLA spectra based on the frequencies and directions.

        Args:
            frequencies: Numpy array of 29 frequencies
                         for Lotus wave spectra.
            directions:  Numpy array of 36 directions
                         for Lotus wave spectra.
            spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                                 spectra wave energy values.

        Returns:
            LOLA spectra as numpy array.
        """
        adjusted_directions = np.concatenate([directions[1:], np.array([360])])
        adjusted_spectra_wave_energy = np.concatenate(
            (spectra_wave_energy[:, 1:], spectra_wave_energy[:, 0][:, None]),
            axis=1,
        )

        spline_interpolation = RectBivariateSpline(
            frequencies, adjusted_directions, adjusted_spectra_wave_energy
        )

        lola_spectra = spline_interpolation(
            self._lola_frequencies, self._lola_directions
        )

        # Remove any negative values artifacts
        # created by the interpolation
        lola_spectra[lola_spectra < 0] = 0
        return lola_spectra

    def _merge_spectra_with_matrix(
        self,
        frequencies: Any,
        directions: Any,
        spectra_wave_energy: Any,
        spectral_refraction_matrix: Any,
    ) -> List[List[float]]:
        """
        Merges spectra wave energy with the spectral refraction matrix,
        frequencies and directions.

        Args:
            frequencies: Numpy array of 29 frequencies
                         for Lotus wave spectra.
            directions:  Numpy array of 36 directions
                         for Lotus wave spectra.
            spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                                 spectra wave energy values.
            spectral_refraction_matrix: Numpy array of 2D (24 x 25)
                                        spectral refraction matrix.

        Returns:
            Transformed spectra as list.
        """

        lola_spectra = self._generate_lola_spectra(
            frequencies, directions, spectra_wave_energy
        )
        return [
            [
                self._discrete_frequency_steps[row_index]
                * (self._direction_bin_size * math.pi / 180)
                * (
                    data
                    * (
                        spectral_refraction_matrix[column_index][17]
                        # Use the last column of the matrix
                        # for the remaining spectra.
                        if row_index >= 18
                        else spectral_refraction_matrix[column_index][
                            row_index
                        ]
                    )
                )
                for column_index, data in enumerate(row)
            ]
            for row_index, row in enumerate(lola_spectra)
        ]

    def _calculate_helper(
        self,
        frequencies: Any,
        directions: Any,
        spectra_wave_energy: Any,
        spectral_refraction_matrix: Any,
    ) -> Tuple[Optional[Tuple[float, float]], Optional[ValueError]]:
        """
        Helper function to calculate surf heights.

        Args:
            frequencies: Numpy array of 29 frequencies
                         for Lotus wave spectra.
            directions:  Numpy array of 36 directions
                         for Lotus wave spectra.
            spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                                 spectra wave energy values.
            spectral_refraction_matrix: Numpy array of 2D (24 x 25)
                                        spectral refraction matrix.

        Returns:
            Surf height for given input.
        """
        merged_spectra = self._merge_spectra_with_matrix(
            frequencies,
            directions,
            spectra_wave_energy,
            spectral_refraction_matrix,
        )

        (
            overall_swell_height,
            swell_partitions,
        ) = self._spectra_to_swell_partitions(merged_spectra)

        surf_min, surf_max = self._surf_heights_from_swell_partitions(
            overall_swell_height, swell_partitions
        )
        return (round(surf_min, 2), round(surf_max, 2)), None

    def calculate(
        self, surf_height_inputs: List[SurfHeightInput]
    ) -> List[Tuple[Optional[Tuple[float, float]], Optional[ValueError]]]:
        """
        Calculate surf heights using the Spectral Refraction method.

        Args:
            surf_height_inputs: The inputs to calculate surf heights for.

        Returns:
            Surf height for each input.
        """
        return [
            self._calculate_helper(
                surf_height_input.frequencies,
                surf_height_input.directions,
                surf_height_input.spectra_wave_energy,
                surf_height_input.spectral_refraction_matrix,
            )
            for surf_height_input in surf_height_inputs
        ]
