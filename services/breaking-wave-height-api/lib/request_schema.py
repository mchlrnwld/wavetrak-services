polynomial_schema = {
    'algorithm': {'type': 'string', 'required': True, 'regex': '^POLYNOMIAL$'},
    'breakingWaveHeightCoefficient': {'type': 'number', 'required': False},
    'breakingWaveHeightIntercept': {'type': 'number', 'required': False},
    'optimalSwellDirection': {'type': 'number', 'required': True},
    'swellPartitions': {
        'type': 'list',
        'required': True,
        'schema': {
            'type': 'dict',
            'required': True,
            'schema': {
                'height': {'type': 'number', 'required': True},
                'period': {'type': 'number', 'required': True},
                'direction': {'type': 'number', 'required': True},
            },
        },
    },
}

machine_learning_schema = {
    'algorithm': {
        'type': 'string',
        'required': True,
        'regex': '^MACHINE_LEARNING$',
    },
    'breakingWaveHeightCoefficient': {'type': 'number', 'required': False},
    'breakingWaveHeightIntercept': {'type': 'number', 'required': False},
    'optimalSwellDirection': {'type': 'number', 'required': False},
    'swellPartitions': {
        'type': 'list',
        'required': True,
        'schema': {
            'type': 'dict',
            'required': True,
            'schema': {
                'height': {'type': 'number', 'required': True},
                'period': {'type': 'number', 'required': True},
                'direction': {'type': 'number', 'required': True},
            },
        },
    },
}

spectral_refraction_swells = {
    'algorithm': {
        'type': 'string',
        'required': True,
        'regex': '^SPECTRAL_REFRACTION$',
    },
    'breakingWaveHeightCoefficient': {'type': 'number', 'required': False},
    'breakingWaveHeightIntercept': {'type': 'number', 'required': False},
    'optimalSwellDirection': {'type': 'number', 'required': False},
    'frequencies': {
        'type': 'list',
        'required': False,
        'schema': {'type': 'number', 'required': True},
    },
    'directions': {
        'type': 'list',
        'required': False,
        'schema': {'type': 'number', 'required': True},
    },
    'swellPartitions': {
        'type': 'list',
        'required': True,
        'schema': {
            'type': 'dict',
            'required': True,
            'schema': {
                'height': {'type': 'number', 'required': True},
                'period': {'type': 'number', 'required': True},
                'direction': {'type': 'number', 'required': False},
            },
        },
    },
    'spectralRefractionMatrix': {
        'type': 'list',
        'required': True,
        'minlength': 24,
        'maxlength': 24,
        'schema': {
            'type': 'list',
            'required': True,
            'minlength': 18,
            'maxlength': 18,
            'schema': {'type': 'number', 'required': True},
        },
    },
}

spectral_refraction_spectra = {
    'algorithm': {
        'type': 'string',
        'required': True,
        'regex': '^SPECTRAL_REFRACTION$',
    },
    'breakingWaveHeightCoefficient': {'type': 'number', 'required': False},
    'breakingWaveHeightIntercept': {'type': 'number', 'required': False},
    'optimalSwellDirection': {'type': 'number', 'required': False},
    'spectraWaveEnergy': {
        'type': 'list',
        'required': True,
        'minlength': 29,
        'maxlength': 29,
        'schema': {
            'type': 'list',
            'required': True,
            'schema': {
                'type': 'number',
                'required': True,
                'minlength': 36,
                'maxlength': 36,
            },
        },
    },
    'spectralRefractionMatrix': {
        'type': 'list',
        'required': True,
        'minlength': 24,
        'maxlength': 24,
        'schema': {
            'type': 'list',
            'required': True,
            'minlength': 18,
            'maxlength': 18,
            'schema': {'type': 'number', 'required': True},
        },
    },
    'frequencies': {
        'type': 'list',
        'required': False,
        'schema': {'type': 'number', 'required': True},
    },
    'directions': {
        'type': 'list',
        'required': False,
        'schema': {'type': 'number', 'required': True},
    },
}

spectral_refraction_schema = {
    'spectral_refraction': {
        'type': 'dict',
        'oneof_schema': [
            spectral_refraction_swells,
            spectral_refraction_spectra,
        ],
    }
}

algorithm_schema = {
    'algorithm': {
        'type': 'string',
        'required': True,
        'allowed': ['SPECTRAL_REFRACTION', 'POLYNOMIAL', 'MACHINE_LEARNING'],
    }
}

core_schema = {'inputs': {'type': 'list', 'required': True}}
