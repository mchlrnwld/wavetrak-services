"""
Functions for calculating surf height forecasts from swell forecast data and
spot parameters.
"""

from itertools import groupby
from typing import Generator, Iterable, List, Optional, Tuple

from lib.machine_learning_model.machine_learning import SurfHeightML
from lib.polynomial_model.polynomial import surf_height_poly
from lib.spectral_refraction_model.spectral_refraction import (
    SpectralRefraction,
)
from lib.types.surf_height_input import SurfHeightInput


class SurfHeight:
    """
    Initializes algorithms to be used for calculating surf heights from swell
    partitions or spectra data.

    Args:
        data_dir: Path to write temporary files to for enabling calculation.
    """

    def __init__(self):
        self._spectral_refraction = SpectralRefraction()

    def _group_inputs_by_algorithm(
        self, surf_height_inputs: Iterable[SurfHeightInput]
    ) -> List[Tuple[Optional[str], List[Tuple[SurfHeightInput, int]]]]:
        return [
            (algorithm, list(input_group))
            for algorithm, input_group in groupby(
                sorted(
                    [
                        (surf_height_input, i)
                        for i, surf_height_input in enumerate(
                            surf_height_inputs
                        )
                    ],
                    key=lambda x: x[0].algorithm or 'None',
                ),
                key=lambda x: x[0].algorithm or 'None',
            )
        ]

    def _apply_breaking_wave_height_corrections(
        self,
        surf_heights: Optional[Tuple[float, float]],
        error: Optional[ValueError],
        breaking_wave_height_coefficient: float,
        breaking_wave_height_intercept: float,
    ) -> Tuple[Optional[Tuple[float, float]], Optional[ValueError]]:
        if error:
            return (None, error)

        if surf_heights is None:
            return (None, error)

        surf_min, surf_max = surf_heights

        return (
            (
                max(
                    0,
                    (
                        (breaking_wave_height_coefficient * surf_min)
                        + breaking_wave_height_intercept
                    ),
                ),
                max(
                    0,
                    (
                        (breaking_wave_height_coefficient * surf_max)
                        + breaking_wave_height_intercept
                    ),
                ),
            ),
            None,
        )

    def _calculate_polynomial(
        self, surf_height_inputs: List[Tuple[SurfHeightInput, int]]
    ) -> Generator[
        Tuple[Optional[Tuple[float, float]], Optional[ValueError], int],
        None,
        None,
    ]:
        for surf_height_input, i in surf_height_inputs:
            if (
                surf_height_input.swell_partitions
                and surf_height_input.optimal_swell_direction
            ):
                surf_min, surf_max = surf_height_poly(
                    surf_height_input.swell_partitions,
                    surf_height_input.optimal_swell_direction,
                )
                yield ((surf_min, surf_max), None, i)
            else:
                yield ((0, 0), None, i)

    def _calculate_machine_learning(
        self, surf_height_inputs: List[Tuple[SurfHeightInput, int]]
    ) -> Generator[
        Tuple[Optional[Tuple[float, float]], Optional[ValueError], int],
        None,
        None,
    ]:
        for surf_height_input, i in surf_height_inputs:
            if surf_height_input.swell_partitions:
                surf_min, surf_max = self._surf_height_ml.calculate(
                    surf_height_input.swell_partitions
                )
                yield ((surf_min, surf_max), None, i)
            else:
                yield ((0, 0), None, i)

    def _calculate_spectral_refraction(
        self, surf_height_inputs: List[Tuple[SurfHeightInput, int]]
    ) -> Generator[
        Tuple[Optional[Tuple[float, float]], Optional[ValueError], int],
        None,
        None,
    ]:
        surf_height_results = self._spectral_refraction.calculate(
            [surf_height_input for surf_height_input, _ in surf_height_inputs]
        )
        for (surf_heights, error), index_and_input in zip(
            surf_height_results, surf_height_inputs
        ):
            _, i = index_and_input
            yield (surf_heights, error, i)

    def calculate(
        self, surf_height_inputs: List[SurfHeightInput]
    ) -> List[Tuple[Optional[Tuple[float, float]], Optional[ValueError]]]:
        """
        Calculate surf heights using requested algorithms.

        Args:
            surf_height_inputs: Batch of inputs to calculate surf heights for.

        Returns:
            List of surf min/max and errors for each input.
        """
        indexed_surf_heights: List[
            Tuple[Optional[Tuple[float, float]], Optional[ValueError], int]
        ] = []
        for algorithm, input_group in self._group_inputs_by_algorithm(
            surf_height_inputs
        ):
            if algorithm == 'POLYNOMIAL':
                indexed_surf_heights.extend(
                    list(self._calculate_polynomial(input_group))
                )
            elif algorithm == 'MACHINE_LEARNING':
                # TODO: Update the machine learning
                # code so that the processing load can be
                # shared amongst multiple child processes.
                self._surf_height_ml = SurfHeightML()
                indexed_surf_heights.extend(
                    list(self._calculate_machine_learning(input_group))
                )
            elif algorithm == 'SPECTRAL_REFRACTION':
                indexed_surf_heights.extend(
                    list(self._calculate_spectral_refraction(input_group))
                )
            else:
                indexed_surf_heights.extend(
                    [(None, None, i) for _, i in input_group]
                )

        # Sort results to match order of original inputs.
        return [
            self._apply_breaking_wave_height_corrections(
                surf_heights,
                error,
                surf_height_inputs[i].breaking_wave_height_coefficient,
                surf_height_inputs[i].breaking_wave_height_intercept,
            )
            for surf_heights, error, i in sorted(
                indexed_surf_heights, key=lambda x: x[2]
            )
        ]
