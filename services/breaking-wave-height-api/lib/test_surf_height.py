"""
Tests for the surf_height module
"""
import json
import os

import numpy as np  # type: ignore
import pytest  # type: ignore
from science_algorithms import SwellPartition  # type: ignore

from lib.surf_height import SurfHeight, SurfHeightInput


def test_group_inputs_by_algorithm():
    surf_height_inputs = [
        SurfHeightInput(None),
        SurfHeightInput('POLYNOMIAL'),
        SurfHeightInput(None),
        SurfHeightInput('SPECTRAL_REFRACTION'),
        SurfHeightInput('POLYNOMIAL'),
        SurfHeightInput('MACHINE_LEARNING'),
        SurfHeightInput('SPECTRAL_REFRACTION'),
    ]
    grouped_inputs = {
        algorithm: list(input_group)
        for algorithm, input_group in SurfHeight()._group_inputs_by_algorithm(
            surf_height_inputs
        )
    }
    assert grouped_inputs['None'] == [
        (surf_height_inputs[0], 0),
        (surf_height_inputs[2], 2),
    ]
    assert grouped_inputs['POLYNOMIAL'] == [
        (surf_height_inputs[1], 1),
        (surf_height_inputs[4], 4),
    ]
    assert grouped_inputs['SPECTRAL_REFRACTION'] == [
        (surf_height_inputs[3], 3),
        (surf_height_inputs[6], 6),
    ]
    assert grouped_inputs['MACHINE_LEARNING'] == [(surf_height_inputs[5], 5)]


def test_surf_heights_calculate():
    with open(
        os.path.join('fixtures', 'example_poi_spectra.json')
    ) as spectra_file, open(
        os.path.join('fixtures', 'example_poi_matrix.json')
    ) as matrix_file:
        matrix = json.load(matrix_file)
        spectra = json.load(spectra_file)
        bantham_swell_partitions = [
            SwellPartition(0.13, 11.85, 229.78),
            SwellPartition(0.17, 5.55, 141.8),
            SwellPartition(
                None, None, None
            ),  # Ignored without error in calculation
        ]
        hb_pier_swell_partitions = [
            SwellPartition(0.4, 5.95, 266.53),
            SwellPartition(0.66, 11.35, 185.61),
            SwellPartition(0.06, 13.26, 261.72),
        ]
        nazare_swell_partitions = [
            SwellPartition(1.67, 10.37, 296.93),
            SwellPartition(1.01, 14.43, 279.17),
            SwellPartition(0.17, 21.41, 270.19),
        ]

        surf_heights = SurfHeight().calculate(
            [
                SurfHeightInput(None),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=[],
                    optimal_swell_direction=0,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=[
                        SwellPartition(None, None, None),
                        SwellPartition(None, None, None),
                    ],
                    optimal_swell_direction=0,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=[
                        SwellPartition(10, 10, 131),
                        SwellPartition(10, 10, 269),
                    ],
                    optimal_swell_direction=20,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=bantham_swell_partitions,
                    optimal_swell_direction=230,
                    breaking_wave_height_coefficient=0.8,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=hb_pier_swell_partitions,
                    optimal_swell_direction=205,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=hb_pier_swell_partitions,
                    optimal_swell_direction=205,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=1.0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=hb_pier_swell_partitions,
                    optimal_swell_direction=205,
                    breaking_wave_height_coefficient=1.5,
                    breaking_wave_height_intercept=1.0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=hb_pier_swell_partitions,
                    optimal_swell_direction=205,
                    breaking_wave_height_coefficient=1.0,
                    breaking_wave_height_intercept=-1.0,
                ),
                SurfHeightInput(
                    'POLYNOMIAL',
                    swell_partitions=nazare_swell_partitions,
                    optimal_swell_direction=295,
                    breaking_wave_height_coefficient=1.5,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'MACHINE_LEARNING',
                    swell_partitions=bantham_swell_partitions,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'MACHINE_LEARNING',
                    swell_partitions=hb_pier_swell_partitions,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'MACHINE_LEARNING',
                    swell_partitions=nazare_swell_partitions,
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'MACHINE_LEARNING',
                    swell_partitions=[],
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
                SurfHeightInput(
                    'SPECTRAL_REFRACTION',
                    spectra_wave_energy=np.array(spectra),
                    spectral_refraction_matrix=np.array(matrix),
                    breaking_wave_height_coefficient=1,
                    breaking_wave_height_intercept=0,
                ),
            ]
        )
        assert surf_heights[0] == (
            None,
            None,
        ), 'None algorithm returns None values.'
        assert (0, 0) == surf_heights[1][
            0
        ], 'POLYNOMIAL should be flat if no valid swells.'
        assert (0, 0) == surf_heights[2][
            0
        ], 'POLYNOMIAL should be flat if no swells.'
        assert (0, 0) == surf_heights[3][0], (
            'POLYNOMIAL should be flat if swell angle are beyond 110 degress '
            'from optimal.'
        )
        tolerance = 0.001
        assert (0.182, 0.284) == pytest.approx(
            surf_heights[4][0], abs=tolerance
        ), 'POLYNOMIAL Bantham example.'
        assert (0.782, 1.221) == pytest.approx(
            surf_heights[5][0], abs=tolerance
        ), 'POLYNOMIAL HB Pier example.'
        assert (1.782, 2.221) == pytest.approx(
            surf_heights[6][0], abs=tolerance
        ), 'POLYNOMIAL HB Pier example.'
        assert (2.173, 2.8315) == pytest.approx(
            surf_heights[7][0], abs=tolerance
        ), 'POLYNOMIAL HB Pier example.'
        assert (0.0, 0.221) == pytest.approx(
            surf_heights[8][0], abs=tolerance
        ), 'POLYNOMIAL HB Pier example.'
        assert (2.34, 3.656) == pytest.approx(
            surf_heights[9][0], abs=tolerance
        ), 'POLYNOMIAL Nazare example.'
        assert (0.338, 0.569) == pytest.approx(
            surf_heights[10][0], abs=tolerance
        ), 'MACHINE_LEARNING Bantham example.'
        assert (0.414, 0.681) == pytest.approx(
            surf_heights[11][0], abs=tolerance
        ), 'MACHINE_LEARNING HB Pier example.'
        assert (1.206, 1.748) == pytest.approx(
            surf_heights[12][0], abs=tolerance
        ), 'MACHINE_LEARNING Nazare example.'
        assert (0, 0) == pytest.approx(
            surf_heights[13][0], abs=tolerance
        ), 'MACHINE_LEARNING should be flat if no swells.'
        assert (0.16, 0.47) == pytest.approx(
            surf_heights[14][0], abs=tolerance
        ), 'SPECTRAL_REFRACTION example.'
