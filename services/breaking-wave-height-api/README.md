## Breaking Wave Height API

Calculate breaking wave heights for any given grid point.

### Running Locally

The Breaking Wave Height API uses Sanic, a lightweight and asynchronous web server and framework. Follow the steps below to get the app up and running:

1. Create conda environment: `conda devenv`.
2. Activate the conda environment: `conda activate breaking-wave-height-api`.
3. Set `PORT` in `.env` to the desired value (ex: `8080`).
4. After activating the conda environment, the app can be started: `env $(xargs < .env) make start`.


### Deploying

The Breaking Wave Height API can be deployed to 3 different environments: `sandbox` and `staging` for lower tiers, and `prod` for higher tiers. Use the `deploy-service-to-ecs` jenkins job which is the standard deploy job for all ECS services at Wavetrak. This can be found under `Surfline -> Web -> deploy-service-to-ecs`.

### Testing

The Breaking Wave Height API uses `pytest` to run all unit tests. `pytest` is the standard testing library used for all projects at Wavetrak. An additional library, `pytest-sanic`, used which mocks HTTP endpoints. All tests can be run by using: `make test`.

### Requests

#### Payload Contract

| Property | Description |
| :--- | ---: |
| inputs[].algorithm | Algorithm to use for calculating breaking wave heights. POLYNOMIAL, MACHINE_LEARNING, or SPECTRAL_REFRACTION. |
| inputs[].breakingWaveHeightCoefficient | Breaking wave heights multiplied by this value for bias correction. Defaults to 1. |
| inputs[].breakingWaveHeightIntercept | Added to scaled breaking wave heights for bias correction. Defaults to 0. |
| inputs[].optimalSwellDirection | Optimal swell direction between 0 and 360. Required for POLYNOMIAL algorithm. |
| inputs[].swellPartitions | List of swell partitions. Required for POLYNOMIAL, MACHINE_LEARNING algorithms, or SPECTRAL_REFRACTION if spectraWaveEnergy is not given. |
| inputs[].swellPartitions[].height | Height of swell partition in meters. Required for each swell partition. |
| inputs[].swellPartitions[].period | Period of swell partition in seconds. Required for each swell partition. |
| inputs[].swellPartitions[].direction | Direction of swell partition between 0 and 360. Required for each swell partition. |
| inputs[].spectraWaveEnergy | 29 x 36 matrix of spectra wave energy. Either spectraWaveEnergy or swellPartitions are required for SPECTRAL_REFRACTION algorithm. |
| inputs[].spectralRefractionMatrix | 24 x 18 matrix used to refract spectra wave energy. Required for SPECTRAL_REFRACTION algorithm. |

#### Sample Request
```
HTTP/1.1 POST /breaking-wave-heights
{
    "inputs": [
        {
            "algorithm": "POLYNOMIAL",
            "breakingWaveHeightCoefficient": 0.8,
            "breakingWaveHeightIntercept": 0,
            "optimalSwellDirection": 230,
            "swellPartitions:" [
                {
                    "height": 0.13,
                    "period": 11.85,
                    "direction": 229.78
                },
                {
                    "height": 0.17,
                    "period": 5.55,
                    "direction": 141.8
                }
            ]
        }
    ]
}
```
### Responses

#### Payload Contract
| Property | Description |
| :--- | ---: |
| data.breakingWaveHeights[].min | Minimum breaking wave height in meters. |
| data.breakingWaveHeights[].max | Maximum breaking wave height in meters. |

#### Sample Response
```
{
    "data": {
        "breakingWaveHeights": [
            {
                "min": 0.182,
                "max": 0.284
            }
        ]
    }
}
```

### Error Handling

All errors will be returned as a json object, with the following format:

#### General Error
```
{
	"description": "Method Not Allowed",
	"status": 405,
	"message": "Method GET not allowed for URL /breaking-wave-heights"
}

```

#### Invalid Parameters

An invalid parameters error will be followed by an `inputs` dictionary. The `inputs` dictionary contains entries which do not match the Request Payload contract. The key for each entry in the `inputs` dictionary corresponds to the order in which the entry was passed in. For example:
```
Missing field:
{
	"description": "Invalid Parameters",
	"message": {
		"inputs": {
			"0": {
				"swellPartitions": [{
					"0": [{
						"period": ["required field"]
					}],
					"1": [{
						'period': ["required field"]
					}]
				}]
			},
            "2": {
				"algorithm": ["required field"]
            }
		}
	},
	"status": 400
}
Incorrect field name:
{
	"description": "Invalid Parameters",
	"message": {
		"inputs": {
			"0": {
				"optimalSwellDirection": ["required field"],
				"optimalSwellDirecton": ["unknown field"]
			}
		}
	},
	"status": 400
}
```
