import json
import os

import pytest

from server import app


@pytest.fixture
def app_test():
    yield app


@pytest.fixture
def test_cli(loop, app_test, sanic_client):
    return loop.run_until_complete(sanic_client(app))


async def test_valid_breaking_wave_heights_general(test_cli):
    with open(
        os.path.join('fixtures', 'lotus_directions.json')
    ) as directions_file, open(
        os.path.join('fixtures', 'lotus_frequencies.json')
    ) as frequencies_file, open(
        os.path.join('fixtures', 'example_poi_matrix.json')
    ) as matrix_file:
        json.load(frequencies_file)
        json.load(directions_file)
        matrix = json.load(matrix_file)
        resp = await test_cli.post(
            '/breaking-wave-heights',
            json={
                'inputs': [
                    {
                        'algorithm': 'POLYNOMIAL',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'optimalSwellDirection': 230,
                        'swellPartitions': [
                            {
                                'height': 0.13,
                                'period': 11.85,
                                'direction': 229.78,
                            },
                            {
                                'height': 0.17,
                                'period': 5.55,
                                'direction': 141.8,
                            },
                        ],
                    },
                    {
                        'algorithm': 'MACHINE_LEARNING',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'swellPartitions': [
                            {
                                'height': 0.13,
                                'period': 11.85,
                                'direction': 229.78,
                            },
                            {
                                'height': 0.17,
                                'period': 5.55,
                                'direction': 141.8,
                            },
                        ],
                    },
                    {
                        'algorithm': 'SPECTRAL_REFRACTION',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'spectralRefractionMatrix': matrix,
                        'swellPartitions': [
                            {
                                'height': 0.13,
                                'period': 11.85,
                                'direction': 229.78,
                            },
                            {
                                'height': 0.17,
                                'period': 5.55,
                                'direction': 141.8,
                            },
                        ],
                    },
                ]
            },
        )
        json_resp = resp.json()
        assert json_resp['data']['breakingWaveHeights'][0][
            'min'
        ] == pytest.approx(0.18, abs=1e-2, rel=1e-2)
        assert json_resp['data']['breakingWaveHeights'][0][
            'max'
        ] == pytest.approx(0.28, abs=1e-2, rel=1e-2)
        assert json_resp['data']['breakingWaveHeights'][1][
            'min'
        ] == pytest.approx(0.27, abs=1e-2, rel=1e-2)
        assert json_resp['data']['breakingWaveHeights'][1][
            'max'
        ] == pytest.approx(0.45, abs=1e-2, rel=1e-2)
        assert json_resp['data']['breakingWaveHeights'][2]['min'] == 0
        assert json_resp['data']['breakingWaveHeights'][2][
            'max'
        ] == pytest.approx(0.12, abs=1e-2, rel=1e-2)
        assert resp.status_code == 200


async def test_invalid_breaking_wave_heights_general(test_cli):
    # Input is misspelled.
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'input': [
                {
                    'algorithm': 'POLYNOMIAL',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 229.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 141.8},
                    ],
                }
            ]
        },
    )

    assert resp.json()['message'] == {
        'input': ['unknown field'],
        'inputs': ['required field'],
    }
    assert resp.status_code == 400

    # Missing algorithm.

    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 229.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 141.8},
                    ],
                }
            ]
        },
    )

    assert resp.json()['message']['inputs']['0'] == {
        'algorithm': ['required field']
    }
    assert resp.status_code == 400

    # Invalid algorithm.

    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'EXPONENTIAL',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 229.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 141.8},
                    ],
                }
            ]
        },
    )

    assert resp.json()['message']['inputs']['0'] == {
        'algorithm': ['unallowed value EXPONENTIAL']
    }
    assert resp.status_code == 400


async def test_valid_breaking_wave_heights_machine_learning(test_cli):
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'MACHINE_LEARNING',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 229.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 141.8},
                    ],
                }
            ]
        },
    )
    json_resp = resp.json()
    assert json_resp['data']['breakingWaveHeights'][0]['min'] == pytest.approx(
        0.27, abs=1e-2, rel=1e-2
    )
    assert json_resp['data']['breakingWaveHeights'][0]['max'] == pytest.approx(
        0.45, abs=1e-2, rel=1e-2
    )
    assert resp.status_code == 200


async def test_invalid_breaking_wave_heights_machine_learning(test_cli):
    # Missing period in Swell Partitions.
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'MACHINE_LEARNING',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [{'height': 0.13, 'direction': 229.78}],
                }
            ]
        },
    )

    assert resp.json()['message']['inputs']['0'] == {
        'swellPartitions': [{'0': [{'period': ['required field']}]}]
    }
    assert resp.status_code == 400


async def test_valid_breaking_wave_heights_polynomial(test_cli):
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'POLYNOMIAL',
                    'breakingWaveHeightCoefficient': 1,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 29.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 31.8},
                    ],
                }
            ]
        },
    )
    json_resp = resp.json()
    assert json_resp['data']['breakingWaveHeights'][0]['min'] == 0
    assert json_resp['data']['breakingWaveHeights'][0]['max'] == 0
    assert resp.status_code == 200

    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'POLYNOMIAL',
                    'breakingWaveHeightCoefficient': 1,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 29.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 31.8},
                    ],
                    # Unnecessary parameters should be ignored.
                    'spectralRefractionMatrix': None,
                }
            ]
        },
    )
    json_resp = resp.json()
    assert json_resp['data']['breakingWaveHeights'][0]['min'] == 0
    assert json_resp['data']['breakingWaveHeights'][0]['max'] == 0
    assert resp.status_code == 200


async def test_invalid_breaking_wave_heights_polynomial(test_cli):
    # Missing optimal swell direction.
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'POLYNOMIAL',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'swellPartitions': [
                        {'height': 0.13, 'period': 11.85, 'direction': 229.78},
                        {'height': 0.17, 'period': 5.55, 'direction': 141.8},
                    ],
                }
            ]
        },
    )

    assert resp.json()['message']['inputs']['0'] == {
        'optimalSwellDirection': ['required field']
    }
    assert resp.status_code == 400

    # Missing period in Swell Partitions.
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'POLYNOMIAL',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                    'optimalSwellDirection': 230,
                    'swellPartitions': [
                        {'height': 0.13, 'direction': 229.78},
                        {'height': 0.17, 'direction': 141.8},
                    ],
                }
            ]
        },
    )
    assert resp.json()['message']['inputs']['0']['swellPartitions'][0]['0'][
        0
    ] == {'period': ['required field']}
    assert resp.status_code == 400


async def test_valid_breaking_wave_heights_spectral_refraction(test_cli):
    # With spectra.
    with open(
        os.path.join('fixtures', 'example_poi_spectra.json')
    ) as spectra_file, open(
        os.path.join('fixtures', 'example_poi_matrix.json')
    ) as matrix_file:
        spectra = json.load(spectra_file)
        matrix = json.load(matrix_file)
        resp = await test_cli.post(
            '/breaking-wave-heights',
            json={
                'inputs': [
                    {
                        'algorithm': 'SPECTRAL_REFRACTION',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'spectraWaveEnergy': spectra,
                        'spectralRefractionMatrix': matrix,
                    }
                ]
            },
        )
        json_resp = resp.json()
        assert resp.status_code == 200
        assert json_resp['data']['breakingWaveHeights'][0][
            'min'
        ] == pytest.approx(0.12, abs=1e-2, rel=1e-2)
        assert json_resp['data']['breakingWaveHeights'][0][
            'max'
        ] == pytest.approx(0.37, abs=1e-2, rel=1e-2)

    # With swell partitions.
    with open(
        os.path.join('fixtures', 'example_poi_matrix.json')
    ) as matrix_file:
        matrix = json.load(matrix_file)
        resp = await test_cli.post(
            '/breaking-wave-heights',
            json={
                'inputs': [
                    {
                        'algorithm': 'SPECTRAL_REFRACTION',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'swellPartitions': [
                            {
                                'height': 0.13,
                                'period': 11.85,
                                'direction': 229.78,
                            },
                            {
                                'height': 0.17,
                                'period': 5.55,
                                'direction': 141.8,
                            },
                        ],
                        'spectralRefractionMatrix': matrix,
                    }
                ]
            },
        )
        json_resp = resp.json()
        assert resp.status_code == 200
        assert json_resp['data']['breakingWaveHeights'][0]['min'] == 0
        assert json_resp['data']['breakingWaveHeights'][0][
            'max'
        ] == pytest.approx(0.12, abs=1e-2, rel=1e-2)


async def test_invalid_breaking_wave_heights_spectral_refraction(test_cli):
    # Missing Spectra and Swell partitions.
    # Either can be used to calculate
    # spectral refraction surf heights.
    resp = await test_cli.post(
        '/breaking-wave-heights',
        json={
            'inputs': [
                {
                    'algorithm': 'SPECTRAL_REFRACTION',
                    'breakingWaveHeightCoefficient': 0.8,
                    'breakingWaveHeightIntercept': 0,
                }
            ]
        },
    )
    assert resp.json()['message']['inputs']['0'] == [
        'none or more than one rule validate',
        {
            'oneof definition 0': [
                {
                    'spectralRefractionMatrix': ['required field'],
                    'swellPartitions': ['required field'],
                }
            ],
            'oneof definition 1': [
                {
                    'spectraWaveEnergy': ['required field'],
                    'spectralRefractionMatrix': ['required field'],
                }
            ],
        },
    ]
    assert resp.status_code == 400

    # Spectra is the incorrect size.
    with open(
        os.path.join('fixtures', 'example_poi_matrix.json')
    ) as matrix_file:
        matrix = json.load(matrix_file)
        spectra = [[1.4, 5.6, 2.3], [7.6, 3.2, 51.1]]
        resp = await test_cli.post(
            '/breaking-wave-heights',
            json={
                'inputs': [
                    {
                        'algorithm': 'SPECTRAL_REFRACTION',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'spectraWaveEnergy': spectra,
                        'spectralRefractionMatrix': matrix,
                    }
                ]
            },
        )
        assert resp.json()['message']['inputs']['0'][1]['oneof definition 1'][
            0
        ] == {'spectraWaveEnergy': ['min length is 29']}
        assert resp.status_code == 400

    # Matrix is incorrect size.
    with open(
        os.path.join('fixtures', 'example_poi_spectra.json')
    ) as spectra_file:
        matrix = [[1.4, 5.6], [7.6, 3.2]]
        spectra = json.load(spectra_file)
        resp = await test_cli.post(
            '/breaking-wave-heights',
            json={
                'inputs': [
                    {
                        'algorithm': 'SPECTRAL_REFRACTION',
                        'breakingWaveHeightCoefficient': 0.8,
                        'breakingWaveHeightIntercept': 0,
                        'spectraWaveEnergy': spectra,
                        'spectralRefractionMatrix': matrix,
                    }
                ]
            },
        )
        assert resp.json()['message']['inputs']['0'][1]['oneof definition 1'][
            0
        ] == {
            'spectralRefractionMatrix': [
                'min length is 24',
                {'0': ['min length is 18'], '1': ['min length is 18']},
            ]
        }

        assert resp.status_code == 400


async def test_valid_breaking_wave_heights_options_request(test_cli):
    resp = await test_cli.options('/breaking-wave-heights')
    assert resp.headers == {
        'access-control-allow-methods': 'POST,OPTIONS',
        'access-control-allow-origin': '*',
        'access-control-allow-credentials': 'true',
        'access-control-allow-headers': (
            'origin, content-type, accept, '
            'authorization, x-xsrf-token, x-request-id'
        ),
        'content-type': 'text/plain; charset=utf-8',
        'connection': 'keep-alive',
        'keep-alive': '5',
    }
    assert resp.status_code == 204

    resp = await test_cli.options('/health')
    assert resp.headers == {
        'access-control-allow-methods': 'GET,OPTIONS',
        'access-control-allow-origin': '*',
        'access-control-allow-credentials': 'true',
        'access-control-allow-headers': (
            'origin, content-type, accept, '
            'authorization, x-xsrf-token, x-request-id'
        ),
        'content-type': 'text/plain; charset=utf-8',
        'connection': 'keep-alive',
        'keep-alive': '5',
    }
    assert resp.status_code == 204
