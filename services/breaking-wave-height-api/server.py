import asyncio
import json
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from typing import Dict, List

import numpy as np  # type: ignore
from cerberus import Validator  # type: ignore
from sanic import Sanic
from sanic.exceptions import InvalidUsage
from sanic.log import logger
from sanic.response import json as sanic_json
from sanic.response import text
from science_algorithms import SwellPartition  # type: ignore

from lib.request_schema import (
    algorithm_schema,
    core_schema,
    machine_learning_schema,
    polynomial_schema,
    spectral_refraction_schema,
)
from lib.surf_height import SurfHeight
from lib.types.surf_height_input import SurfHeightInput

app = Sanic('breaking-wave-height-api')
surf_height = SurfHeight()


def add_cors_headers(methods: List[str]) -> Dict[str, str]:
    return {
        'Access-Control-Allow-Methods': ','.join(methods),
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': (
            'origin, content-type, accept, '
            'authorization, x-xsrf-token, x-request-id'
        ),
    }


@app.exception(InvalidUsage)
async def invalid_usage(request, exception):
    return sanic_json(
        {
            'description': 'Invalid Parameters',
            'status': 400,
            'message': json.loads(str(exception)),
        },
        status=400,
    )


app.config.FALLBACK_ERROR_FORMAT = 'json'

executor = ProcessPoolExecutor()

HEALTH_METHODS = ['GET', 'OPTIONS']


@app.route('/health', methods=HEALTH_METHODS)
async def health_check(request):
    if request.method == 'OPTIONS':
        return text('', headers=add_cors_headers(HEALTH_METHODS), status=204)
    return sanic_json(
        {'message': 'OK'}, headers={'Access-Control-Allow-Origin': '*'}
    )


def validate_schema(request_json):
    v = Validator(core_schema)
    v.validate(request_json)
    if v.errors:
        logger.error('Errors in the given request payload.')
        raise InvalidUsage(json.dumps(v.errors))
    input_errors = {}
    for i, surf_input in enumerate(request_json['inputs']):
        v = Validator(algorithm_schema, allow_unknown=True)
        v.validate(surf_input)
        if v.errors:
            input_errors[i] = v.errors
            continue
        if surf_input['algorithm'] == 'MACHINE_LEARNING':
            v = Validator(machine_learning_schema, allow_unknown=True)
            v.validate(surf_input)
            if v.errors:
                input_errors[i] = v.errors
                continue
        if surf_input['algorithm'] == 'POLYNOMIAL':
            v = Validator(polynomial_schema, allow_unknown=True)
            v.validate(surf_input)
            if v.errors:
                input_errors[i] = v.errors
                continue
        if surf_input['algorithm'] == 'SPECTRAL_REFRACTION':
            v = Validator(spectral_refraction_schema, allow_unknown=True)
            v.validate({'spectral_refraction': surf_input})
            if v.errors:
                input_errors[i] = v.errors['spectral_refraction']
                continue

    if input_errors:
        logger.error('Errors in the given request payload.')
        raise InvalidUsage(json.dumps({'inputs': input_errors}))


BREAKING_WAVE_HEIGHTS_METHODS = ['POST', 'OPTIONS']


@app.route('/breaking-wave-heights', methods=BREAKING_WAVE_HEIGHTS_METHODS)
async def breaking_wave_heights(request):
    if request.method == 'OPTIONS':
        return text(
            '',
            headers=add_cors_headers(BREAKING_WAVE_HEIGHTS_METHODS),
            status=204,
        )
    validate_schema(request.json)
    surf_height_inputs = []
    for surf_input in request.json['inputs']:
        surf_height_inputs.append(
            SurfHeightInput(
                surf_input['algorithm'],
                surf_input['breakingWaveHeightCoefficient']
                if 'breakingWaveHeightCoefficient' in surf_input
                else None,
                surf_input['breakingWaveHeightIntercept']
                if 'breakingWaveHeightIntercept' in surf_input
                else None,
                [
                    SwellPartition(
                        swell['height'] if 'height' in swell else None,
                        swell['period'] if 'period' in swell else None,
                        swell['direction'] if 'direction' in swell else None,
                    )
                    for swell in surf_input['swellPartitions']
                ]
                if 'swellPartitions' in surf_input
                else None,
                surf_input['optimalSwellDirection']
                if 'optimalSwellDirection' in surf_input
                else None,
                np.array(surf_input['spectraWaveEnergy'])
                if 'spectraWaveEnergy' in surf_input
                else None,
                np.array(surf_input['spectralRefractionMatrix'])
                if 'spectralRefractionMatrix' in surf_input
                else None,
            )
        )

    logger.info(
        f'Calculating surf heights for: {len(surf_height_inputs)} inputs'
    )
    loop = asyncio.get_running_loop()
    all_surf_heights = await loop.run_in_executor(
        executor, partial(surf_height.calculate, surf_height_inputs)
    )

    return sanic_json(
        {
            'data': {
                'breakingWaveHeights': [
                    {'min': surf_height[0][0], 'max': surf_height[0][1]}
                    for surf_height in all_surf_heights
                    if surf_height[0] is not None
                ]
            }
        },
        headers={'Access-Control-Allow-Origin': '*'},
    )
