provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "breaking-wave-height-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "breaking-wave-height-api.staging.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "breaking_wave_height_api" {
  source = "../../"

  environment      = "staging"
  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3JHKQ8ELQG5UE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-staging/26ee81426b4723db"
}
