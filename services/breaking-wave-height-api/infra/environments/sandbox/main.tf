provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "breaking-wave-height-api/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "breaking-wave-height-api.sandbox.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "breaking_wave_height_api" {
  source = "../../"

  environment      = "sandbox"
  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3DM6R3JR1RYXV"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac"
}
