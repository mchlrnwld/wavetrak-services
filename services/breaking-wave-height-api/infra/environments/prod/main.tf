provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "breaking-wave-height-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "breaking-wave-height-api.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "breaking_wave_height_api" {
  source = "../../"

  environment      = "prod"
  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3LLOZIY0ZZQDE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"
}
