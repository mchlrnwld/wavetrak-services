variable "application" {
  type        = string
  default     = "breaking-wave-height-api"
  description = "Application name."
}

variable "environment" {
  type        = string
  description = "Environment name (sandbox, staging or prod)."
}

variable "company" {
  type        = string
  default     = "wt"
  description = "Company name (ex: wt)."
}

variable "default_vpc" {
  type        = string
  description = "VPC to place ECS service in."
}

variable "ecs_cluster" {
  type        = string
  description = "ECS cluster to create application service."
}

variable "alb_listener_arn" {
  type        = string
  description = "Application load balancer listener ARN to load balance requests to service."
}

variable "service_lb_rules" {
  type        = list(map(string))
  description = "List of load balancer rules."
}

variable "iam_role_arn" {
  type        = string
  description = "ECS service IAM role ARN."
}

variable "dns_name" {
  type        = string
  description = "DNS name for service."
}

variable "dns_zone_id" {
  type        = string
  description = "DNS zone to create DNS record in."
}

variable "load_balancer_arn" {
  type        = string
  description = "Application load balancer ARN to load balance requests to service."
}

variable "healthcheck_path" {
  type        = string
  default     = "/health"
  description = "Path ECS uses to check the health of the service."
}

locals {
  service_td_name = "sl-core-svc-${var.environment}-${var.application}"
}

variable "service_port" {
  type        = number
  default     = 8080
  description = "Port that the internal service listens on."
}

variable "service_alb_priority" {
  type        = number
  default     = 450
  description = "Priority of ALB listener rules."
}

variable "service_td_count" {
  type        = number
  default     = 2
  description = "Number of task definitions for the ECS service."
}

variable "auto_scaling_enabled" {
  type        = bool
  default     = false
  description = "Enable autoscaling on the service."
}

variable "auto_scaling_scale_by" {
  type        = string
  default     = "alb_request_count"
  description = "Scaling mode."
}

variable "auto_scaling_target_value" {
  type        = number
  default     = 2
  description = "Target size of the service."
}

variable "auto_scaling_min_size" {
  type        = number
  default     = 2
  description = "Minimum size of the service."
}

variable "auto_scaling_max_size" {
  type        = number
  default     = 4
  description = "Maximum size of the service."
}
