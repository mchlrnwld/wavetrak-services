terraform {
  required_version = ">= 0.12"
  required_providers {
    aws      = "~> 2.70"
    null     = "~> 2.1"
    template = "~> 2.1"
  }
}
