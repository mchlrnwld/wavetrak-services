# Surfline Location View Micro-service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


    - [QuickStart](#quickstart)
  - [Development](#development)
    - [Location-View-Service Directory](#location-view-service-directory)
      - [/common](#common)
      - [/error](#error)
      - [/handlers](#handlers)
      - [/model](#model)
      - [/server](#server)
- [API](#api)
      - [Table of Contents](#table-of-contents)
  - [MapView](#mapview)
    - [Authentication](#authentication)
        - [Headers](#headers)
    - [Required Parameters](#required-parameters)
        - [`type`](#type)
  - [mapView](#mapview)
        - [`type: "mapview"`](#type-mapview)
      - [The mapView object](#the-mapview-object)
    - [`GET /location/view`](#get-locationview)
      - [Parameters](#parameters)
      - [Sample Request](#sample-request)
      - [Sample Response](#sample-response)
  - [worldTaxonomy](#worldtaxonomy)
      - [Sample Request](#sample-request-1)
      - [Sample Response](#sample-response-1)
    - [API Errors](#api-errors)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


### QuickStart
 1. `nvm use v6.9.2`
 2. `cat .env.sample >> .env && npm install && npm run startlocal`
 3. [localhost:8083/health](http://localhost:8083/health)

Skip to [API docs](#api)
## Development
### Location-View-Service Directory
```
 - /src
   | - /common
   | - /error
   | - /handlers
   | - /model
   | - /server
```
#### /common
Files included in the common directory should be generic and reusable throughout our suite of microservices.
#### /error
All errors thrown by the favorites service are defined within individual files named after their constructor type, and exported from `index.js`. **NOTE:** Try to make these errors as generic as possible.  We may want to reuse this logic within other services.
#### /handlers
Handlers for this service are specific to the `kind` of location view being requested.  The root location-view-service handler delegates **REST**ful requests to for a these `kind`s by pulling a `type` value off either the query param or request body (depending on the request).

Each `kind` of handler function is responsible for calling the appropriate model functions and returning `200` level responses with contract defined data attached as json.  
**We do not handle errors within handlers**.

Folder names should represent the `kind` of handlers included.  For example: functions specific to location view for "MapView", should be defined in `handlers/mapView/index.js`.
#### /model
Models included most of the business logic for this service.  Elastic search related handlers are in here.

#### /server
The location view service API is exposed under one route; `/location/view` and is agnostic to the `kind` of location view being requested.

# API
#### Table of Contents
 - [MapView](##mapView)
 - [TravelView](##travelView)

## MapView
This service is organized around REST, and provides a flexible yet predictable API that supports multiple type of mapView responses.
### Authentication
##### Headers
No authentication is required for the location/view service as it simply support taxonomy and location based responses.

### Required Parameters
Since this API is build to handle many types of Location Views from a single endpoint, each request must specify which `type` of data it expects the response to return.
##### `type`
```
// Valid Request: GET
$ curl -X GET 'https://services.surfline.com/location/view?type=mapView'

// Valid Request: POST, PUT, DELETE
$ curl -X POST 'https://services.surfline.com/location/view' \
  -d type=mapView \
  **additional params**
```
```
// Error
$ curl -X POST 'https://services.surfline.com/location/view'

/* Response
HTTP 500 Internal Server Error
  {
	message: "Internal Server Error"
  }
*/

```
## mapView
##### `type: "mapview"`
mapView returns taxonomy and spot data for a particular lat/lng or geonameId for a location in our taxonomy api.
#### The mapView object
| Attribute     | Type             | Description
| ------------- |----------------- |-----------
| url           | String           | URL of this location
| boundingBox   | Object           | Object containing north, south, east, west lat/lng coordinates
| breadCrumbs   | Array of Objects | Array with taxonomy information for all items from country to current location
| taxonomy      | Object           | Taxonomy for currently targeted location
| siblings      | Array            | Array of taxonomy data for siblings of current taxonomy
| topSpots      | Array            | Array of up to 20 spots within the boundingBox sorted by conditions/wave height

### `GET /location/view`
#### Parameters
| Parameter    | Required | Description
| ------------- |---------|-----------
| type          | Yes     | Type of location view to return

Location View service only support GET requests since it is purely a view api

#### Sample Request
```
$ curl -X GET 'https://services.surfline.com/location/view/?type=mapView'
```

#### Sample Response
```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
{
  "url": "/united-states/california/orange-county/huntington-beach/5358705",
  "boundingBox": {
    "north": 33.688935321434364,
    "south": 33.6409625047537,
    "east": -117.98086315765381,
    "west": -118.044196169064
  },
  "breadCrumbs": [
    {
      "name": "United States",
      "url": "/united-states/6252001",
      "geonameId": 6252001,
      "id": "58f7ed51dadb30820bb3879c"
    },
    {
      "name": "California",
      "url": "/united-states/california/5332921",
      "geonameId": 5332921,
      "id": "58f7ed51dadb30820bb387a6"
    },
    {
      "name": "Orange County",
      "url": "/united-states/california/orange-county/5379524",
      "geonameId": 5379524,
      "id": "58f7ed5ddadb30820bb39651"
    },
    {
      "name": "Huntington Beach",
      "url": "/united-states/california/orange-county/huntington-beach/5358705",
      "geonameId": 5358705,
      "id": "58f7ed5fdadb30820bb3987c"
    }
  ],
  "taxonomy": {
    "_id": "58f7ed5fdadb30820bb3987c",
    "geonameId": 5358705,
    "type": "geoname",
    "liesIn": [
      "58f7ed5ddadb30820bb39651"
    ],
    "geonames": {
      "fcode": "PPL",
      "lat": "33.6603",
      "adminName1": "California",
      "fcodeName": "populated place",
      "countryName": "United States",
      "fclName": "city, village,...",
      "name": "Huntington Beach",
      "countryCode": "US",
      "population": 201899,
      "fcl": "P",
      "countryId": "6252001",
      "toponymName": "Huntington Beach",
      "geonameId": 5358705,
      "lng": "-117.99923",
      "adminCode1": "CA"
    },
    "location": {
      "coordinates": [
        -117.99923,
        33.6603
      ],
      "type": "Point"
    },
    "enumeratedPath": ",Earth,North America,United States,California,Orange County,Huntington Beach",
    "name": "Huntington Beach",
    "category": "geonames",
    "hasSpots": true,
    "associated": {
      "links": [
        {
          "key": "taxonomy",
          "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5358705&type=geoname"
        },
        {
          "key": "www",
          "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/huntington-beach/5358705"
        },
        {
          "key": "travel",
          "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/huntington-beach-surfing-and-beaches/5358705"
        }
      ]
    },
    "in": [
      {
        "_id": "58f7ed51dadb30820bb38782",
        "geonameId": 6295630,
        "type": "geoname",
        "geonames": {
          "population": 6814400000,
          "fcode": "AREA",
          "fcl": "L",
          "lat": "0",
          "adminName1": "",
          "fcodeName": "area",
          "toponymName": "Earth",
          "fclName": "parks,area, ...",
          "name": "Earth",
          "geonameId": 6295630,
          "lng": "0"
        },
        "location": {
          "coordinates": [
            0,
            0
          ],
          "type": "Point"
        },
        "enumeratedPath": ",Earth",
        "name": "Earth",
        "category": "geonames",
        "hasSpots": true,
        "depth": 5,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=6295630&type=geoname"
            },
            null,
            null
          ]
        }
      },
      {
        "_id": "58f7ed51dadb30820bb38791",
        "geonameId": 6255149,
        "type": "geoname",
        "liesIn": [
          "58f7ed51dadb30820bb38782"
        ],
        "geonames": {
          "population": 0,
          "fcode": "CONT",
          "fcl": "L",
          "lat": "46.07323",
          "adminName1": "",
          "fcodeName": "continent",
          "toponymName": "North America",
          "fclName": "parks,area, ...",
          "name": "North America",
          "geonameId": 6255149,
          "lng": "-100.54688"
        },
        "location": {
          "coordinates": [
            -100.54688,
            46.07323
          ],
          "type": "Point"
        },
        "enumeratedPath": ",Earth,North America",
        "name": "North America",
        "category": "geonames",
        "hasSpots": true,
        "depth": 4,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=6255149&type=geoname"
            },
            null,
            null
          ]
        }
      },
      {
        "_id": "58f7ed51dadb30820bb3879c",
        "geonameId": 6252001,
        "type": "geoname",
        "liesIn": [
          "58f7ed51dadb30820bb38791"
        ],
        "geonames": {
          "fcode": "PCLI",
          "lat": "39.76",
          "adminName1": "",
          "fcodeName": "independent political entity",
          "countryName": "United States",
          "fclName": "country, state, region,...",
          "name": "United States",
          "countryCode": "US",
          "population": 310232863,
          "fcl": "A",
          "countryId": "6252001",
          "toponymName": "United States",
          "geonameId": 6252001,
          "lng": "-98.5",
          "adminCode1": "00"
        },
        "location": {
          "coordinates": [
            -98.5,
            39.76
          ],
          "type": "Point"
        },
        "enumeratedPath": ",Earth,North America,United States",
        "name": "United States",
        "category": "geonames",
        "hasSpots": true,
        "depth": 3,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=6252001&type=geoname"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001"
            },
            {
              "key": "travel",
              "href": "https://sandbox.surfline.com/travel/united-states-surfing-and-beaches/6252001"
            }
          ]
        }
      },
      {
        "_id": "58f7ed51dadb30820bb387a6",
        "geonameId": 5332921,
        "type": "geoname",
        "liesIn": [
          "58f7ed51dadb30820bb3879c"
        ],
        "geonames": {
          "fcode": "ADM1",
          "lat": "37.25022",
          "adminName1": "California",
          "fcodeName": "first-order administrative division",
          "countryName": "United States",
          "fclName": "country, state, region,...",
          "name": "California",
          "countryCode": "US",
          "population": 37691912,
          "fcl": "A",
          "countryId": "6252001",
          "toponymName": "California",
          "geonameId": 5332921,
          "lng": "-119.75126",
          "adminCode1": "CA"
        },
        "location": {
          "coordinates": [
            -119.75126,
            37.25022
          ],
          "type": "Point"
        },
        "enumeratedPath": ",Earth,North America,United States,California",
        "name": "California",
        "category": "geonames",
        "hasSpots": true,
        "depth": 2,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5332921&type=geoname"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921"
            },
            {
              "key": "travel",
              "href": "https://sandbox.surfline.com/travel/united-states/california-surfing-and-beaches/5332921"
            }
          ]
        }
      },
      {
        "_id": "58f7ed5ddadb30820bb39651",
        "geonameId": 5379524,
        "type": "geoname",
        "liesIn": [
          "58f7ed51dadb30820bb387a6"
        ],
        "geonames": {
          "fcode": "ADM2",
          "lat": "33.67691",
          "adminName1": "California",
          "fcodeName": "second-order administrative division",
          "countryName": "United States",
          "fclName": "country, state, region,...",
          "name": "Orange County",
          "countryCode": "US",
          "population": 3010232,
          "fcl": "A",
          "countryId": "6252001",
          "toponymName": "Orange County",
          "geonameId": 5379524,
          "lng": "-117.77617",
          "adminCode1": "CA"
        },
        "location": {
          "coordinates": [
            -117.77617,
            33.67691
          ],
          "type": "Point"
        },
        "enumeratedPath": ",Earth,North America,United States,California,Orange County",
        "name": "Orange County",
        "category": "geonames",
        "hasSpots": true,
        "depth": 1,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5379524&type=geoname"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/5379524"
            },
            {
              "key": "travel",
              "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county-surfing-and-beaches/5379524"
            }
          ]
        }
      }
    ],
    "contains": [
      {
        "_id": "58f7f27bdadb30820bba1394",
        "spot": "584204204e65fad6a77091aa",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -118.0062210559845,
            33.653798240213476
          ],
          "type": "Point"
        },
        "name": "HB Pier Northside Overview",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=584204204e65fad6a77091aa&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/584204204e65fad6a77091aa"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/hb-pier-northside-overview/584204204e65fad6a77091aa/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f7eda8dadb30820bb3e574",
        "spot": "5842041f4e65fad6a77088ea",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -118.0192012569068,
            33.66704400178788
          ],
          "type": "Point"
        },
        "name": "Goldenwest",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5842041f4e65fad6a77088ea&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/5842041f4e65fad6a77088ea"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/goldenwest/5842041f4e65fad6a77088ea/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f7eda8dadb30820bb3e4c9",
        "spot": "5842041f4e65fad6a77088e8",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -118.04235577583313,
            33.68760321488161
          ],
          "type": "Point"
        },
        "name": "Bolsa Chica State Beach",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5842041f4e65fad6a77088e8&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/5842041f4e65fad6a77088e8"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/bolsa-chica-state-beach/5842041f4e65fad6a77088e8/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f7eda9dadb30820bb3e618",
        "spot": "5842041f4e65fad6a77088eb",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "type": "Point",
          "coordinates": [
            -118.0143556189923,
            33.66315613358258
          ]
        },
        "name": "17th Street",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5842041f4e65fad6a77088eb&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/5842041f4e65fad6a77088eb"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/17th-street/5842041f4e65fad6a77088eb/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f7edaadadb30820bb3e72c",
        "spot": "5842041f4e65fad6a77088ed",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "type": "Point",
          "coordinates": [
            -118.0032588192,
            33.654213041213
          ]
        },
        "name": "HB Pier, Southside",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5842041f4e65fad6a77088ed&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/5842041f4e65fad6a77088ed"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/hb-pier-southside/5842041f4e65fad6a77088ed/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f80848dadb30820bce9644",
        "spot": "584204204e65fad6a770998d",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -118.043096169064,
            33.68730096183094
          ],
          "type": "Point"
        },
        "name": "Bolsa Chica Overview",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=584204204e65fad6a770998d&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/584204204e65fad6a770998d"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/bolsa-chica-overview/584204204e65fad6a770998d/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f80ac6dadb30820bd15c36",
        "spot": "58bdea400cec4200133464f0",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -118.00136089324951,
            33.65420905128059
          ],
          "type": "Point"
        },
        "name": "HB Pier Southside Overview",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=58bdea400cec4200133464f0&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/58bdea400cec4200133464f0"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/hb-pier-southside-overview/58bdea400cec4200133464f0/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f80ac9dadb30820bd15f34",
        "spot": "58bdee240cec4200133464f1",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -118.04229140281677,
            33.68783532143436
          ],
          "type": "Point"
        },
        "name": "Bolsa Chica State Beach North",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=58bdee240cec4200133464f1&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/58bdee240cec4200133464f1"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/bolsa-chica-state-beach-north/58bdee240cec4200133464f1/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f7ed5fdadb30820bb39890",
        "spot": "5842041f4e65fad6a7708827",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "type": "Point",
          "coordinates": [
            -118.0064678192,
            33.656781041213
          ]
        },
        "name": "HB Pier, Northside",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5842041f4e65fad6a7708827&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/5842041f4e65fad6a7708827"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/hb-pier-northside/5842041f4e65fad6a7708827/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f80ac7dadb30820bd15d04",
        "spot": "58bdebbc82d034001252e3d2",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "coordinates": [
            -117.98196315765381,
            33.6420625047537
          ],
          "type": "Point"
        },
        "name": "Huntington Street Overview",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=58bdebbc82d034001252e3d2&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/58bdebbc82d034001252e3d2"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/huntington-street-overview/58bdebbc82d034001252e3d2/forecast"
            }
          ]
        }
      },
      {
        "_id": "58f80847dadb30820bce9537",
        "spot": "584204204e65fad6a770998c",
        "type": "spot",
        "liesIn": [
          "58f7ed5fdadb30820bb3987c",
          "58f7ed5ddadb30820bb39689"
        ],
        "location": {
          "type": "Point",
          "coordinates": [
            -117.985944738959,
            33.64377868530139
          ]
        },
        "name": "Huntington State Beach",
        "category": "surfline",
        "hasSpots": false,
        "depth": 0,
        "associated": {
          "links": [
            {
              "key": "taxonomy",
              "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=584204204e65fad6a770998c&type=spot"
            },
            {
              "key": "api",
              "href": "http://spots-api.sandbox.surfline.com/admin/spots/584204204e65fad6a770998c"
            },
            {
              "key": "www",
              "href": "https://sandbox.surfline.com/surf-report/huntington-state-beach/584204204e65fad6a770998c/forecast"
            }
          ]
        }
      }
    ]
  },
  "travelContent": {
    "id": "1054",
    "contentType": "TRAVEL",
    "createdAt": 1495466241,
    "updatedAt": 1497049299,
    "content": {
      "title": "Huntington Beach",
      "subtitle": "Catch a wave in Surf City",
      "summary": "<p>Huntington Beach, California has been dubbed “Surf City” by many surfers.</p>\n<p>With consistent waves throughout most of the year, and plenty of things to see and do, it has become a place to see and be seen in the Southern California surfing community.</p>\n<p>What used to be a grungy surf town is growing quickly into a high class location for people all around Orange County.  With new hotels, shopping centers and a totally revamped downtown, there’s something for everyone in good old HB.</p>\n",
      "body": "<p>Test copy for travel page</p>\n<div id=\"attachment_989\" style=\"width: 2510px\" class=\"wp-caption alignnone\"><img class=\"size-full wp-image-989\" src=\"https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE.jpg\" alt=\"\" width=\"2500\" height=\"1667\" srcset=\"https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE.jpg 2500w, https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE-540x360.jpg 540w, https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE-768x512.jpg 768w, https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE-1024x683.jpg 1024w\" sizes=\"(max-width: 2500px) 100vw, 2500px\" /><p class=\"wp-caption-text\">The Box! Photo: Cestari/WSL</p></div>\n<p> </p>\n"
    },
    "media": {
      "large": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE-1024x683.jpg",
      "medium": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE-540x360.jpg",
      "type": "image",
      "thumbnail": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE-150x150.jpg",
      "full": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/30054852/Box-MargaretRiver-20170330-6_SLOANE.jpg"
    }
  },
  "siblings": [
    {
      "_id": "58f7eda3dadb30820bb3e0b0",
      "geonameId": 5364329,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.52253",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Laguna Niguel",
        "countryCode": "US",
        "population": 65806,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Laguna Niguel",
        "geonameId": 5364329,
        "lng": "-117.70755",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.70755,
          33.52253
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Laguna Niguel",
      "name": "Laguna Niguel",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5364329&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/laguna-niguel/5364329"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329"
          }
        ]
      }
    },
    {
      "_id": "58f7ed60dadb30820bb39a11",
      "geonameId": 5376890,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.61891",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Newport Beach",
        "countryCode": "US",
        "population": 87127,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Newport Beach",
        "geonameId": 5376890,
        "lng": "-117.92895",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.92895,
          33.61891
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Newport Beach",
      "name": "Newport Beach",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5376890&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/newport-beach/5376890"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890"
          }
        ]
      }
    },
    {
      "_id": "58f7ed63dadb30820bb39cad",
      "geonameId": 5341483,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.46697",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Dana Point",
        "countryCode": "US",
        "population": 34181,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Dana Point",
        "geonameId": 5341483,
        "lng": "-117.69811",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.69811,
          33.46697
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Dana Point",
      "name": "Dana Point",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5341483&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/dana-point/5341483"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/dana-point-surfing-and-beaches/5341483"
          }
        ]
      }
    },
    {
      "_id": "58f7ed5ddadb30820bb3965c",
      "geonameId": 5394086,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.74141",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Seal Beach",
        "countryCode": "US",
        "population": 24619,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Seal Beach",
        "geonameId": 5394086,
        "lng": "-118.10479",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -118.10479,
          33.74141
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Seal Beach",
      "name": "Seal Beach",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5394086&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/seal-beach/5394086"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/seal-beach-surfing-and-beaches/5394086"
          }
        ]
      }
    },
    {
      "_id": "58f7ed64dadb30820bb39dcd",
      "geonameId": 5391791,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.42697",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "San Clemente",
        "countryCode": "US",
        "population": 65526,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "San Clemente",
        "geonameId": 5391791,
        "lng": "-117.61199",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.61199,
          33.42697
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,San Clemente",
      "name": "San Clemente",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5391791&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/san-clemente/5391791"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/san-clemente-surfing-and-beaches/5391791"
          }
        ]
      }
    },
    {
      "_id": "58f7eda4dadb30820bb3e116",
      "geonameId": 5364275,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.54225",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "Laguna Beach",
        "countryCode": "US",
        "population": 23365,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "Laguna Beach",
        "geonameId": 5364275,
        "lng": "-117.78311",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.78311,
          33.54225
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,Laguna Beach",
      "name": "Laguna Beach",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5364275&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/laguna-beach/5364275"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/laguna-beach-surfing-and-beaches/5364275"
          }
        ]
      }
    },
    {
      "_id": "58f7ed62dadb30820bb39b2b",
      "geonameId": 5392148,
      "type": "geoname",
      "liesIn": [
        "58f7ed5ddadb30820bb39651"
      ],
      "geonames": {
        "fcode": "PPL",
        "lat": "33.61169",
        "adminName1": "California",
        "fcodeName": "populated place",
        "countryName": "United States",
        "fclName": "city, village,...",
        "name": "San Joaquin Hills",
        "countryCode": "US",
        "population": 3176,
        "fcl": "P",
        "countryId": "6252001",
        "toponymName": "San Joaquin Hills",
        "geonameId": 5392148,
        "lng": "-117.83672",
        "adminCode1": "CA"
      },
      "location": {
        "coordinates": [
          -117.83672,
          33.61169
        ],
        "type": "Point"
      },
      "enumeratedPath": ",Earth,North America,United States,California,Orange County,San Joaquin Hills",
      "name": "San Joaquin Hills",
      "category": "geonames",
      "hasSpots": true,
      "depth": 0,
      "associated": {
        "links": [
          {
            "key": "taxonomy",
            "href": "http://spots-api.sandbox.surfline.com/taxonomy?id=5392148&type=geoname"
          },
          {
            "key": "www",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/san-joaquin-hills/5392148"
          },
          {
            "key": "travel",
            "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/san-joaquin-hills-surfing-and-beaches/5392148"
          }
        ]
      }
    }
  ]
}
```

## worldTaxonomy
worldTaxonomy returns countries of the world containing surf spots, grouped by continent and then by letter.

#### Sample Request
```
$ curl -X GET 'https://services.surfline.com/location/view/worldtaxonomy'
```

#### Sample Response
```JSON
{
  "data": [
    {
      "_id": "58f7f00ddadb30820bb69bbc",
      "geonameId": 6255146,
      "name": "Africa",
      "slug": "africa",
      "links": [
        {
          "key": "taxonomy",
          "href": "http://spots-api.staging.surfline.com/taxonomy?id=6255146&type=geoname"
        },
        null,
        null
      ],
      "countries": [
        {
          "key": "A",
          "countries": [
            {
              "_id": "58f834f2dadb30820bf1d7ad",
              "geonameId": 3351879,
              "name": "Angola",
              "slug": "angola",
              "link": [
                {
                  "key": "taxonomy",
                  "href": "http://spots-api.staging.surfline.com/taxonomy?id=3351879&type=geoname"
                },
                {
                  "key": "www",
                  "href": "https://staging.surfline.com/surf-reports-forecasts-cams/angola/3351879"
                },
                {
                  "key": "travel",
                  "href": "https://staging.surfline.com/travel/angola-surfing-and-beaches/3351879"
                }
              ]
            }
          ]
        },
        {
          "key": "C",
          "countries": [
            {
              "_id": "58f83471dadb30820bf17182",
              "geonameId": 3374766,
              "name": "Cape Verde",
              "slug": "cape-verde",
              "link": [
                {
                  "key": "taxonomy",
                  "href": "http://spots-api.staging.surfline.com/taxonomy?id=3374766&type=geoname"
                },
                {
                  "key": "www",
                  "href": "https://staging.surfline.com/surf-reports-forecasts-cams/cape-verde/3374766"
                },
                {
                  "key": "travel",
                  "href": "https://staging.surfline.com/travel/cape-verde-surfing-and-beaches/3374766"
                }
              ]
            }
          ]
        },
        ...
      ]
    },
    {
      "_id": "58f7eef1dadb30820bb556be",
      "geonameId": 6255147,
      "name": "Asia",
      "slug": "asia",
      "links": [
        {
          "key": "taxonomy",
          "href": "http://spots-api.staging.surfline.com/taxonomy?id=6255147&type=geoname"
        },
        null,
        null
      ],
      "countries": [
        {
          "key": "C",
          "countries": [
            {
              "_id": "58f7f279dadb30820bba0d70",
              "geonameId": 1814991,
              "name": "China",
              "slug": "china",
              "link": [
                {
                  "key": "taxonomy",
                  "href": "http://spots-api.staging.surfline.com/taxonomy?id=1814991&type=geoname"
                },
                {
                  "key": "www",
                  "href": "https://staging.surfline.com/surf-reports-forecasts-cams/china/1814991"
                },
                {
                  "key": "travel",
                  "href": "https://staging.surfline.com/travel/china-surfing-and-beaches/1814991"
                }
              ]
            }
          ]
        },
        {
          "key": "H",
          "countries": [
            {
              "_id": "58f80a47dadb30820bd0d351",
              "geonameId": 1819730,
              "name": "Hong Kong",
              "slug": "hong-kong",
              "link": [
                {
                  "key": "taxonomy",
                  "href": "http://spots-api.staging.surfline.com/taxonomy?id=1819730&type=geoname"
                },
                {
                  "key": "www",
                  "href": "https://staging.surfline.com/surf-reports-forecasts-cams/hong-kong/1819730"
                },
                {
                  "key": "travel",
                  "href": "https://staging.surfline.com/travel/hong-kong-surfing-and-beaches/1819730"
                }
              ]
            }
          ]
        }
        ...
      ]
    }
    ...
  ]
}
```

### API Errors
| StatusCode| Message | Triggered by
| ----------|---------|-----------
| 401 | "You are unauthorized to view this resource"|Invalid request headers
| 400 | "Invalid Parameters"| Ommitting `spotId` or other invalid, missing, or malformed parmeters.
| 500 | "Internal Server Error" | We messed up...

# Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `location-view-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTnpreE9UZzNOZyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME56a3hPVGczTmciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [API](#api) section. (Note: you'll have to access the `location-view-service` via the services proxy.) 

**Ex:**

Test `GET` requests against the `/location/view` endpoint:

```
curl \
    -X GET \
    -i \
    http://services.sandbox.surfline.com/location/view?type=mapView
```
**Expected response:** See [above](#sample-response) for sample response.