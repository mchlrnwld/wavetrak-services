# Index management

Elasticsearch's [mapping datatypes](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html)

Elasticsearch should respond back with `200` and `{ "acknowledged": true }`

## Basics
### Add index
`PUT localhost:9200/${index_name}/`

### Delete index
`DELETE localhost:9200/${index_name}/`

### Configure index mapping
Use mapping documents in this directory (broken up by index name)

`PUT localhost:9200/${index_name}/_mapping/${doc_type}`

### Docker

From the [Elasticsearch with Docker Guide](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html) `docker-compose -f docker-compose.yml up` will bring up an Elasticsearch cluster locally. 

Run `node initialize_local_travel.js` from the `import` directory to create indices and mappings for travel pages to an elasticsearch cluster available on `localhost:9200`.
