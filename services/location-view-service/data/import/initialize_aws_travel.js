require('babel-register');
const es = require('elasticsearch');
const AWS = require('aws-sdk');

const client = new es.Client({
  host: process.env.ELASTIC_SEARCH_CONNECTION_STRING,
  connectionClass: require('http-aws-es'),
  amazonES: {
    region: process.env.AWS_DEFAULT_REGION || 'us-west-1',
    credentials: new AWS.EnvironmentCredentials('AWS'),
  },
  log: 'trace',
});

require('./load_travel_mappings')(client);
