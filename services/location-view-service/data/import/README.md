# Initializing Elasticsearch on AWS

* `initialize_aws_travel.js`: initializer for aws-credentialed ES domain; calls `load__travel_mappings`
* `initialize_local_travel.js`: initializer for locally managed ES cluster; calls `load__travel_mappings`
* `load__travel_mappings.js`: creates travel_pages index + travel mapping

If running locally, you can use environment variables for AWS config:
```
AWS_REGION
AWS_SECRET_KEY
AWS_ACCESS_KEY
ELASTIC_SEARCH_CONNECTION_STRING
```
