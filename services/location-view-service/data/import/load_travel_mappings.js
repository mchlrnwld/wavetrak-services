import travelMapping from '../mapping/travel/travel.json';

module.exports = function (client) {
  // Deletes Indices
  async function deleteIndices() {
    const deleteResult = await client.indices
      .delete({ index: 'travel_pages' });
    console.log('Deleted travel_pages index', deleteResult);
  }

  // Creates spots index -> mapping -> deletes index.
  async function initializeTravelIndexAndMapping() {
    const indexResult = await client.indices
      .create({ index: 'travel_pages' });
    console.log('Created travel_pages index', indexResult);
    const mappingResult = await client.indices
      .putMapping({ index: 'travel_pages', type: 'travel', body: travelMapping });
    console.log('Created travel_pages index with travel mapping', mappingResult);
  }

  async function run() {
    try {
      await deleteIndices();
      await initializeTravelIndexAndMapping();
      process.exit(0);
    } catch (e) {
      console.error(e);
      process.exit(1);
    }
  }

  run();
};
