require('babel-register');
const es = require('elasticsearch');

const esConnectionString = process.env.ELASTIC_SEARCH_CONNECTION_STRING || 'http://localhost:9200';

// May require the following httpAuth option, double check the user/pass value associated with the local elasticsearch docker image.
// httpAuth: 'elastic:changeme',

const client = new es.Client({
  host: esConnectionString,
  log: 'trace',
});

require('./load_travel_mappings')(client);
