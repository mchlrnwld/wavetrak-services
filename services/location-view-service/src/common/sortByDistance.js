const calculateDistance = (lat1, lon1, lat2, lon2, unit = 'K') => {
  const radlat1 = Math.PI * (lat1 / 180);
  const radlat2 = Math.PI * (lat2 / 180);
  const theta = lon1 - lon2;
  const radtheta = Math.PI * (theta / 180);
  let dist =
    (Math.sin(radlat1) * Math.sin(radlat2)) +
    (Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta));
  dist = Math.acos(dist);
  dist *= (180 / Math.PI);
  dist *= (60 * 1.1515);
  if (unit === 'K') { dist *= 1.609344; }
  if (unit === 'N') { dist *= 0.8684; }
  return dist;
};

export default (origSiblings, origTaxonomy) => {
  const siblings = origSiblings.map((sibling) => {
    const newSibling = Object.assign({}, sibling);
    newSibling.distance = calculateDistance(origTaxonomy.lat, origTaxonomy.lng,
       sibling.geonames.lat, sibling.geonames.lng, 'K');
    return newSibling;
  });
  return siblings.sort((a, b) => a.distance - b.distance);
};
