import { getTaxonomy } from '../external/taxonomy';
import sortedSiblings from './sortByDistance';

export default async(taxonomy) => {
  let siblings = [];
  const taxParentId = (taxonomy && taxonomy.in && taxonomy.in.length) ?
    taxonomy.in.filter(geoData => geoData.type === 'geoname').pop().geonames.geonameId
    :
    null;

  if (taxParentId) {
    const taxonomyParent = await getTaxonomy('geoname', taxParentId, 0) || null;
    siblings = taxonomyParent ?
      taxonomyParent
      .contains
      .filter(
        possibleSibling =>
          possibleSibling.type.includes('geoname')
          &&
          possibleSibling.depth === 0
          &&
          possibleSibling.geonameId !== taxonomy.geonameId
          &&
          possibleSibling.hasSpots
        )
        :
        [];
  }
  return sortedSiblings(siblings, taxonomy.geonames);
};
