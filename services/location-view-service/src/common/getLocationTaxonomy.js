import { getTaxonomy } from '../external/taxonomy';
import { getNearbySpotFromRequest } from '../external/spots';

export default async(req, maxDepth) => {
  let taxonomy = null;
  if (req.query.id) {
    taxonomy = await getTaxonomy('geoname', req.query.id, maxDepth);
  } else {
    const nearbySpot = await getNearbySpotFromRequest(req);
    if (nearbySpot.length > 0) {
      taxonomy = await getTaxonomy('spot', nearbySpot[0]._id);
      taxonomy = await getTaxonomy(
        'geoname',
        taxonomy.in.filter(geoLoc => geoLoc.type === 'geoname').pop().geonames.geonameId
      );
    }
  }
  return taxonomy;
};
