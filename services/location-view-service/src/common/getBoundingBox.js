const createBoundingBoxFromMultiplePoints = (latitudes, longitudes) => {
  const epsilon = 0.0001 * latitudes.length; // assumes even # of lat and lng
  const result = {
    north: Math.max(...latitudes) + epsilon,
    south: Math.min(...latitudes) - epsilon,
    east: Math.max(...longitudes) + epsilon,
    west: Math.min(...longitudes) - epsilon,
  };

  return result;
};

export default (taxonomy) => {
  let boundingBox = null;
  const spots = taxonomy.contains.filter(geoLoc => geoLoc.type.includes('spot'));
  if (spots.length > 0) {
    const latitudes = spots.map(spot => spot.location.coordinates[1]);
    const longitudes = spots.map(spot => spot.location.coordinates[0]);
    boundingBox = createBoundingBoxFromMultiplePoints(
      latitudes,
      longitudes
    );
  }
  return boundingBox;
};
