import bunyan from 'bunyan';
import Logsene from '@surfline/bunyan-logsene';
import PrettyStream from 'bunyan-prettystream';
import config from '../config';

const logseneStream = new Logsene({
  token: config.LOGSENE_KEY, // default to sandbox
});

const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);

export default (name = 'default-js-logger') => {
  const log = bunyan.createLogger({
    name,
    serializers: bunyan.stdSerializers,
    streams: [
      {
        level: config.CONSOLE_LOG_LEVEL,
        type: 'raw',
        stream: prettyStdOut,
      },
      {
        level: config.LOGSENE_LEVEL,
        stream: logseneStream,
        type: 'raw',
        reemitErrorEvents: true,
      },
    ],
  });
  log.on('error', (err, stream) => {
    console.error('Problem communicating with logging server...');
    return console.error(stream);
  });
  log.trace(`${name} logging started.`);
  return log;
};
