export default (taxonomy, type) => {
  let url = '';
  let breadCrumbs = null;
  const extraText = type === 'travel' ? '-surfing-and-beaches' : '';
  breadCrumbs = taxonomy.in.filter(geoLoc => geoLoc.type === 'geoname' && !(
    geoLoc.geonames.fcode.includes('AREA') || geoLoc.geonames.fcode.includes('CONT')
  ));
  breadCrumbs = breadCrumbs.map((geoLoc) => {
    url += `/${geoLoc.name.replace(/[^a-z0-9]/gi, '-').toLowerCase()}`;
    return {
      name: geoLoc.name,
      url: `${url}${extraText}/${geoLoc.geonames.geonameId}`,
      geonameId: geoLoc.geonames.geonameId,
      id: geoLoc._id,
    };
  });
  if (taxonomy.type === 'geoname') {
    breadCrumbs.push({
      name: taxonomy.name,
      url: `${url}/${taxonomy.name.replace(/[^a-z0-9]/gi, '-').toLowerCase()}${extraText}/${taxonomy.geonames.geonameId}`,
      geonameId: taxonomy.geonames.geonameId,
      id: taxonomy._id,
    });
  }
  return breadCrumbs;
};
