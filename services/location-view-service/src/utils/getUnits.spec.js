import { expect } from 'chai';
import sinon from 'sinon';
import * as getUserSettings from '../external/user';
import getUnits from './getUnits';

describe('getUnits', () => {
  beforeEach(() => {
    sinon.stub(getUserSettings, 'default');
  });

  afterEach(() => {
    getUserSettings.default.restore();
  });

  it('should return units for a user ID', async () => {
    const units = {
      surfHeight: 'M',
      temperature: 'C',
      tideHeight: 'M',
      windSpeed: 'KPH',
    };
    getUserSettings.default.resolves({ units });
    const result = await getUnits('582495992d06cd3b71d66229');
    expect(result).to.deep.equal({
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KPH',
    });
  });

  it('returns default units when user not found', async () => {
    const result = await getUnits('582495992d06cd3b71d66229');
    expect(result).to.deep.equal({
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    });
  });

  it('returns default units when get user settings errors', async () => {
    getUserSettings.default.rejects();
    const result = await getUnits('582495992d06cd3b71d66229');
    expect(result).to.deep.equal({
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    });
  });
});
