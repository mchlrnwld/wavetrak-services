import getUserSettings from '../external/user';

const getUnits = async (userId, geoCountryIso) => {
  try {
    const userSettings = await getUserSettings(userId, geoCountryIso);
    return {
      temperature: userSettings.units.temperature,
      tideHeight: userSettings.units.tideHeight,
      waveHeight: userSettings.units.surfHeight,
      windSpeed: userSettings.units.windSpeed,
    };
  } catch (err) {
    return {
      temperature: 'C',
      tideHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KTS',
    };
  }
};

export default getUnits;
