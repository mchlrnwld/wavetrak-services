import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import { getHandler, postHandler, deleteHandler, putHandler } from '../../handlers';
import { getWorldTaxonomy } from '../../handlers/worldTaxonomy';

export const trackRequests = log => (req, _, next) => {
  log.trace({
    action: '/location/view',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const locationView = (log) => {
  const api = Router();
  api.use('*', json(), trackRequests(log));
  api.get('/', wrapErrors(getHandler));
  api.post('/', wrapErrors(postHandler));
  api.put('/', wrapErrors(putHandler));
  api.delete('/', wrapErrors(deleteHandler));
  api.get('/worldtaxonomy', wrapErrors(getWorldTaxonomy));
  return api;
};

export default locationView;
