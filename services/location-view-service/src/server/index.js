import { setupExpress } from '@surfline/services-common';
import locationView from './locationView';
import logger from '../common/logger';
import userUnits from './middleware/userUnits';

const log = logger('location-view-service');

const setupApp = () =>
  setupExpress({
    log,
    name: 'location-view-service',
    port: process.env.EXPRESS_PORT || 8083,
    allowedMethods: ['GET', 'OPTIONS'],
    handlers: [
      // No top-level /feed endpoint, handled in separate /feed microservice.
      ['/location/view', userUnits, locationView(log)],
    ],
  });

export default setupApp;
