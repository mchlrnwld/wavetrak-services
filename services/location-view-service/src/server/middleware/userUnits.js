import getUnits from '../../utils/getUnits';

const userUnits = async (req, res, next) => {
  // eslint-disable-next-line no-param-reassign
  req.units = await getUnits(req.authenticatedUserId, req.geoCountryIso);
  return next();
};

export default userUnits;
