import { getFeedPosts } from '../../model/feed';
import { getArticleByTaxonomy, getArticlesByTaxonomy } from '../../model/travelContent';
import getLocationTaxonomy from '../../common/getLocationTaxonomy';
import getBoundingBox from '../../common/getBoundingBox';
import getBreadCrumbs from '../../common/getBreadCrumbs';

// eslint-disable-next-line
export const getTravelViewHandler = async (req, res) => {
  const taxonomy = await getLocationTaxonomy(req, 1);

  if (taxonomy.status && taxonomy.status !== 200) {
    return res.status(taxonomy.status).send({ message: taxonomy.message });
  }

  const boundingBox = getBoundingBox(taxonomy);
  const breadCrumbs = getBreadCrumbs(taxonomy, 'travel');
  const travelContent = await getArticleByTaxonomy(taxonomy._id);
  const taxonomyChildrenIds = taxonomy.contains.length ? taxonomy.contains
    .filter(geoLoc => geoLoc.hasSpots && geoLoc.depth === 0 && geoLoc.type === 'geoname')
    .map(child => child._id)
    : [];

  const relatedTravelArticles = taxonomyChildrenIds.length ?
    await getArticlesByTaxonomy(taxonomyChildrenIds)
    : [];

  const children = await Promise.all(Object.keys(taxonomy.contains)
  .map(key => (taxonomy.contains[key]))
  .filter(child => child.depth === 0 && child.type === 'geoname' && child.hasSpots)
  .map(async(child) => {
    const matchingArticle = relatedTravelArticles ?
      relatedTravelArticles.filter(article => article.targetTaxonomy === child._id)
      :
      [];
    const travelArticle = matchingArticle.length ? matchingArticle[0] : {};
    const associatedLink = child.associated.links.filter(link => link.key.includes('travel'))[0] || null;
    return {
      name: child.name,
      media: travelArticle && travelArticle.media,
      url: associatedLink && associatedLink.href,
    };
  }));

  const relatedPosts =
  travelContent
  &&
  travelContent.relatedPosts
  &&
  travelContent.relatedPosts.length
  ?
  await getFeedPosts(travelContent.relatedPosts)
  : [];

  const { units } = req;
  const associated = {
    units,
  };

  const response = {
    associated,
    url: breadCrumbs.length > 0 ? breadCrumbs[breadCrumbs.length - 1].url : null,
    boundingBox,
    breadCrumbs,
    taxonomy,
    children,
    travelContent: travelContent && {
      ...travelContent,
      relatedPosts,
    },
  };

  return res.send(response);
};
