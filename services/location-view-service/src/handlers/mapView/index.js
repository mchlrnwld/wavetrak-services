import { getArticleByTaxonomy } from '../../model/travelContent';
import getLocationTaxonomy from '../../common/getLocationTaxonomy';
import getBoundingBox from '../../common/getBoundingBox';
import getBreadCrumbs from '../../common/getBreadCrumbs';
import getSiblings from '../../common/getSiblings';

// eslint-disable-next-line
export const getMapViewHandler = async (req, res) => {
  const taxonomy = await getLocationTaxonomy(req);
  const spots = taxonomy.contains.filter(geoLoc => geoLoc.type.includes('spot'));

  if (taxonomy.status && taxonomy.status !== 200) {
    return res.status(taxonomy.status).send({ message: taxonomy.message });
  }
  if (!spots.length) {
    return res.status(400).send({ message: 'No spots exist for this taxonomy' });
  }
  const boundingBox = getBoundingBox(taxonomy);
  const siblings = await getSiblings(taxonomy);
  const breadCrumbs = getBreadCrumbs(taxonomy);
  const travelContent = await getArticleByTaxonomy(taxonomy._id);

  const response = {
    url: breadCrumbs.length > 0 ? breadCrumbs[breadCrumbs.length - 1].url : null,
    boundingBox,
    breadCrumbs,
    taxonomy,
    travelContent,
    siblings,
  };

  return res.send(response);
};
