import { filter, flow, last, map, reduce, sortBy } from 'lodash/fp';
import { slugify } from '@surfline/web-common';
import { getTaxonomy } from '../../external/taxonomy';

const sortByName = sortBy(x => x.name);

const countriesInContinent = (continent, taxonomy) => flow(
  filter(({ depth, liesIn, hasSpots }) =>
    depth === 1
    && hasSpots
    && liesIn.includes(continent._id)
  ),
  map(country => ({
    _id: country._id,
    geonameId: country.geonameId,
    name: country.name,
    slug: slugify(country.name),
    links: country.associated.links,
  })),
  sortByName,
)(taxonomy);

const continentsInWorld = taxonomy => flow(
  filter(location => location.depth === 0),
  map((continent) => {
    const sortedCountriesInContinent = countriesInContinent(continent, taxonomy);
    const countriesPerLetter = reduce((letters, nextCountry) => {
      const lastLetters = last(letters);
      if (lastLetters && lastLetters.key.toUpperCase() === nextCountry.name[0].toUpperCase()) {
        lastLetters.countries.push(nextCountry);
      } else {
        letters.push({ key: nextCountry.name[0].toUpperCase(), countries: [nextCountry] });
      }
      return letters;
    }, [], sortedCountriesInContinent);
    return {
      _id: continent._id,
      geonameId: continent.geonameId,
      name: continent.name,
      slug: slugify(continent.name),
      links: continent.associated.links,
      countries: countriesPerLetter,
    };
  }),
  sortByName,
)(taxonomy);

const buildWorldTaxonomy = async () => {
  const earthTaxonomy = await getTaxonomy('geoname', 6295630);
  return continentsInWorld(earthTaxonomy.contains);
};

export default buildWorldTaxonomy;
