import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import * as worldTaxonomyModel from '../../model/worldTaxonomy';
import * as buildWorldTaxonomy from './buildWorldTaxonomy';
import { getWorldTaxonomy } from './index';

describe('handlers / worldTaxonomy', () => {
  let request;
  const worldTaxonomy = [];

  before(() => {
    const app = express();
    app.get('/worldtaxonomy', getWorldTaxonomy);
    request = chai.request(app);
  });

  beforeEach(() => {
    sinon.stub(worldTaxonomyModel, 'getWorldTaxonomy');
    sinon.stub(worldTaxonomyModel, 'setWorldTaxonomy');
    sinon.stub(buildWorldTaxonomy, 'default');
  });

  afterEach(() => {
    worldTaxonomyModel.getWorldTaxonomy.restore();
    worldTaxonomyModel.setWorldTaxonomy.restore();
    buildWorldTaxonomy.default.restore();
  });

  describe('getWorldTaxonomy', () => {
    it('returns cached world taxonomy if found', async () => {
      worldTaxonomyModel.getWorldTaxonomy.resolves(worldTaxonomy);
      const res = await request.get('/worldtaxonomy').send();
      expect(res.status).to.equal(200);
      expect(res.body).to.deep.equal({ data: worldTaxonomy });
      expect(worldTaxonomyModel.getWorldTaxonomy).to.have.been.calledOnce();
      expect(worldTaxonomyModel.setWorldTaxonomy).not.to.have.been.called();
      expect(buildWorldTaxonomy.default).not.to.have.been.called();
    });

    it('returns built world taxonomy and caches it if not already cached', async () => {
      buildWorldTaxonomy.default.resolves(worldTaxonomy);
      const res = await request.get('/worldtaxonomy').send();
      expect(res.status).to.equal(200);
      expect(res.body).to.deep.equal({ data: worldTaxonomy });
      expect(worldTaxonomyModel.getWorldTaxonomy).to.have.been.calledOnce();
      expect(worldTaxonomyModel.setWorldTaxonomy).to.have.been.calledOnce();
      expect(worldTaxonomyModel.setWorldTaxonomy).to.have.been.calledWithExactly(worldTaxonomy);
      expect(buildWorldTaxonomy.default).to.have.been.calledOnce();
    });
  });
});
