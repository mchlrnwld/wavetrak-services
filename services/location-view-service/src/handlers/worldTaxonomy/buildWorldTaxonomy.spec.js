import { expect } from 'chai';
import sinon from 'sinon';
import * as taxonomyAPI from '../../external/taxonomy';
import earthTaxonomy from './fixtures/earthTaxonomy.json';
import buildWorldTaxonomy from './buildWorldTaxonomy';

describe('handlers / worldTaxonomy', () => {
  beforeEach(() => {
    sinon.stub(taxonomyAPI, 'getTaxonomy');
  });

  afterEach(() => {
    taxonomyAPI.getTaxonomy.restore();
  });

  it('builds the world taxonomy with countries that contain surf spots, grouped by continent and then by letter', async () => {
    taxonomyAPI.getTaxonomy.resolves(earthTaxonomy);
    const worldTaxonomy = await buildWorldTaxonomy();
    expect(taxonomyAPI.getTaxonomy).to.have.been.calledOnce();
    expect(taxonomyAPI.getTaxonomy).to.have.been.calledWithExactly('geoname', 6295630);
    expect(worldTaxonomy).to.be.ok();
    expect(worldTaxonomy).to.have.length(3);

    const africa = worldTaxonomy[0];
    expect(africa).to.deep.equal({
      _id: '58f7f00ddadb30820bb69bbc',
      geonameId: 6255146,
      name: 'Africa',
      slug: 'africa',
      links: [],
      countries: [],
    });

    const asia = worldTaxonomy[1];
    expect(asia).to.deep.equal({
      _id: '58f7eef1dadb30820bb556be',
      geonameId: 6255147,
      name: 'Asia',
      slug: 'asia',
      links: [],
      countries: [
        {
          key: 'T',
          countries: [
            {
              _id: '58f80922dadb30820bcf8d96',
              geonameId: 1668284,
              name: 'Taiwan',
              slug: 'taiwan',
              links: [],
            },
            {
              _id: '58f83405dadb30820bf11779',
              geonameId: 1605651,
              name: 'Thailand',
              slug: 'thailand',
              links: [],
            },
          ],
        },
      ],
    });

    const northAmerica = worldTaxonomy[2];
    expect(northAmerica).to.deep.equal({
      _id: '58f7ed51dadb30820bb38791',
      geonameId: 6255149,
      name: 'North America',
      slug: 'north-america',
      links: [],
      countries: [
        {
          key: 'M',
          countries: [
            {
              _id: '58f7eeecdadb30820bb550cc',
              geonameId: 3996063,
              name: 'Mexico',
              slug: 'mexico',
              links: [],
            },
          ],
        },
        {
          key: 'U',
          countries: [
            {
              _id: '58f7ed51dadb30820bb3879c',
              geonameId: 6252001,
              name: 'United States',
              slug: 'united-states',
              links: [],
            },
          ],
        },
      ],
    });
  });
});
