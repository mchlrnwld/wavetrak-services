import * as worldTaxonomyModel from '../../model/worldTaxonomy';
import buildWorldTaxonomy from './buildWorldTaxonomy';

// eslint-disable-next-line
export const getWorldTaxonomy = async (req, res) => {
  const cachedWorldTaxonomy = await worldTaxonomyModel.getWorldTaxonomy();
  const worldTaxonomy = cachedWorldTaxonomy || await buildWorldTaxonomy();

  if (!cachedWorldTaxonomy) {
    // No need to await this asynchronous call. If it fails we will rebuild next time.
    // Failing to store the cache should not prevent the client from seeing the result.
    worldTaxonomyModel.setWorldTaxonomy(worldTaxonomy);
  }

  return res.send({
    data: worldTaxonomy,
  });
};
