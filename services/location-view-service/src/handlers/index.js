import * as mapView from './mapView';
import * as travel from './travel';

const mapping = {
  GET: {
    mapView: mapView.getMapViewHandler,
    travel: travel.getTravelViewHandler,
  },
  POST: {},
  PUT: {},
  DELETE: {},
};

export const getHandler = (req, res) => mapping.GET[req.query.type](req, res);
export const postHandler = (req, res) => mapping.POST[req.body.type](req, res);
export const putHandler = (req, res) => mapping.PUT[req.body.type](req, res);
export const deleteHandler = (req, res) => mapping.DELETE[req.body.type](req, res);
