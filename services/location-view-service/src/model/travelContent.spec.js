import { expect } from 'chai';
import sinon from 'sinon';
import elasticsearch from './elasticsearch';
import travelArticlesFixture from './fixtures/travelArticles.json';
import {
  parseDocumentData,
  getArticleByTaxonomy,
} from './travelContent';

describe('model / travel content', () => {
  const client = {
    get: sinon.stub(),
  };

  beforeEach(() => {
    sinon.stub(elasticsearch, 'getClient');
    elasticsearch.getClient.resolves(client);
    client.get.resolves(travelArticlesFixture);
  });

  afterEach(() => {
    client.get.reset();
    elasticsearch.getClient.restore();
  });

  describe('parseDocumentData', () => {
    it('maps related content search results', () => {
      expect(parseDocumentData({
        _type: 'travel',
        _source: {
          articleId: 303,
          createdAt: new Date(1489194883454),
          updatedAt: new Date(1489194883454),
          relatedPosts: [],
          targetTaxonomy: '58f7ed5fdadb30820bb3987c',
          content: {
            title: 'Winter is Coming!',
            subtitle: 'So say we all.',
            summary: 'Summary and warm.',
            body: 'Test body',
          },
          media: {
            type: 'image',
            thumbnail: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
            medium: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
            large:
            'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
            full: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          },
        },
      })
      ).to.deep.equal({
        id: '303',
        contentType: 'TRAVEL',
        createdAt: 1489194883,
        updatedAt: 1489194883,
        relatedPosts: [],
        content: {
          title: 'Winter is Coming!',
          subtitle: 'So say we all.',
          summary: 'Summary and warm.',
          body: 'Test body',
        },
        media: {
          type: 'image',
          thumbnail: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          medium: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          large: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          full: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
        },
      });
    });
  });

  describe('getArticleByTaxonomy', () => {
    it('searches targeted articles by taxonomy id', async () => {
      const targetTaxonomy = '58f7ed5fdadb30820bb3987c';
      await getArticleByTaxonomy(targetTaxonomy);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.get).to.have.been.calledOnce();
      expect(client.get.firstCall.args[0].index).to.equal('travel_pages');
      expect(client.get.firstCall.args[0].type).to.equal('travel');
      expect(client.get.firstCall.args[0].id).to.equal(targetTaxonomy);
    });
  });
  describe('getArticlesByTaxonomy', () => {
    it('searches targeted articles by taxonomy id', async () => {
      const targetTaxonomy = ['58f7ed5fdadb30820bb3987c'];
      await getArticleByTaxonomy(targetTaxonomy);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.get).to.have.been.calledOnce();
      expect(client.get.firstCall.args[0].index).to.equal('travel_pages');
      expect(client.get.firstCall.args[0].type).to.equal('travel');
      expect(client.get.firstCall.args[0].id).to.equal(targetTaxonomy);
    });
  });
});
