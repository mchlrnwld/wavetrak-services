import redis from 'redis';
import logger from '../common/logger';

const log = logger('location-view-service:common:dbContext');

let redisClient = {};

export function initCache() {
  const connectionString = `redis://${process.env.REDIS_IP}:${process.env.REDIS_PORT}`;
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Redis ConnectionString: ${connectionString} `);
      redisClient = redis.createClient(connectionString);
      redisClient.once('ready',
        () => {
          log.info(`Redis connected on ${connectionString} and ready to accept commands`);
          resolve();
        }
      );
      redisClient.on('error',
        (error) => {
          log.error({
            action: 'Redis:ConnectionError',
            error,
          });
          reject(error);
        }
      );
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export function cacheClient() {
  return redisClient;
}
