import { cacheClient } from './dbContext';

const redisKey = 'location-view-service:world-taxonomy';
const ttl = 60 * 60 * 24; // 1 day

export const getWorldTaxonomy = () =>
  new Promise((resolve, reject) => {
    const client = cacheClient();
    client.get(redisKey, (err, value) => {
      if (err) return reject(err);
      return resolve(JSON.parse(value));
    });
  });

export const setWorldTaxonomy = worldTaxonomy =>
  new Promise((resolve, reject) => {
    const client = cacheClient();
    client.set(redisKey, JSON.stringify(worldTaxonomy), 'EX', ttl, (err) => {
      if (err) return reject(err);
      return resolve();
    });
  });
