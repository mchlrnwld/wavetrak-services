import elasticsearch from './elasticsearch';

const parseFeedSearchHits = ({ hits }, premium) => hits.map(({ _type, _source, sort }) => ({
  id: _source.articleId.toString(),
  contentType: _type.toUpperCase(),
  createdAt: Math.floor(new Date(_source.createdAt) / 1000),
  updatedAt: Math.floor(new Date(_source.updatedAt) / 1000),
  premium: _source.premium,
  promoted: _source.promoted,
  externalSource: _source.externalSource || null,
  externalLink: _source.externalLink || null,
  newWindow: _source.newWindow || false,
  content: {
    title: _source.content.title,
    subtitle: _source.content.subtitle || null,
    body: ((premium || !_source.premium) && _source.content.body) || null,
  },
  permalink: _source.permalink || null,
  media: _source.media || null,
  author: _source.author || null,
  tags: _source.tags || [],
  nextPageStart: sort ? sort.join(',') : null,
}));

const search = async (searchPayload, premium) => {
  const client = await elasticsearch.getClient();
  const response = await client.search({
    index: 'feed',
    body: {
      _source: [
        'articleId',
        'createdAt',
        'updatedAt',
        'premium',
        'promoted',
        'externalSource',
        'externalLink',
        'newWindow',
        'content.title',
        'content.subtitle',
        'content.body',
        'permalink',
        'media.type',
        'media.feed1x',
        'media.feed2x',
        'media.promobox1x',
        'media.promobox2x',
        'author.iconUrl',
        'author.name',
        'tags',
      ],
      ...searchPayload,
    },
  });
  return parseFeedSearchHits(response.hits, premium);
};

const getFeedPost = id => search({
  query: {
    bool: {
      filter: {
        term: {
          articleId: id,
        },
      },
    },
  },
});

const getFeedPosts = ids => search({
  query: { ids: { values: ids } },
});

export { getFeedPost, getFeedPosts, parseFeedSearchHits }; // eslint-disable-line
