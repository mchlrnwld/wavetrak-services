import elasticsearch from './elasticsearch';

const parseDocumentData = (document) => {
  const { _type, _source } = document;
  let esDoc = null;
  if (_type && _source) {
    esDoc = {
      id: _source.articleId.toString(),
      contentType: _type.toUpperCase(),
      createdAt: Math.floor(new Date(_source.createdAt) / 1000),
      updatedAt: Math.floor(new Date(_source.updatedAt) / 1000),
      content: {
        title: _source.content.title,
        subtitle: _source.content.subtitle || null,
        summary: _source.content.summary || null,
        body: _source.content.body || null,
      },
      media: _source.media || null,
      relatedPosts: _source.relatedPosts || [],
    };
  }
  return esDoc;
};

const parseTravelDocHits = ({ hits }) => hits.map(({ _type, _source }) => ({
  id: _source.articleId.toString(),
  targetTaxonomy: _source.targetTaxonomy,
  contentType: _type.toUpperCase(),
  createdAt: Math.floor(new Date(_source.createdAt) / 1000),
  updatedAt: Math.floor(new Date(_source.updatedAt) / 1000),
  content: {
    title: _source.content.title,
    subtitle: _source.content.subtitle || null,
    summary: _source.content.summary || null,
    body: _source.content.body || null,
  },
  media: _source.media || null,
  relatedPosts: _source.relatedPosts || [],
}));

const getArticlesByTaxonomy = async (targetTaxonomies) => {
  let travelDocuments = null;
  const client = await elasticsearch.getClient();
  try {
    const response = await client.search({
      index: 'travel_pages',
      type: 'travel',
      body: {
        query: { ids: { values: targetTaxonomies } },
      },
    });
    travelDocuments = await parseTravelDocHits(response.hits);
  } catch (e) {
    // no document found at the specified taxonomy
  }
  return travelDocuments;
};

const getArticleByTaxonomy = async (targetTaxonomy) => {
  let travelDocument = null;
  const client = await elasticsearch.getClient();
  try {
    const response = await client.get({
      index: 'travel_pages',
      type: 'travel',
      id: targetTaxonomy,
    });
    travelDocument = await parseDocumentData(response);
  } catch (e) {
    // no document found at the specified taxonomy
  }
  return travelDocument;
};

export { getArticleByTaxonomy, parseDocumentData, getArticlesByTaxonomy, parseTravelDocHits }; // eslint-disable-line
