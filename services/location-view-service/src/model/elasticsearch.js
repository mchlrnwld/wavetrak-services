import es from 'elasticsearch';
import httpAwsEs from 'http-aws-es';
import AWS from 'aws-sdk';
import config from '../config';

/*
Note: AWS_CONTAINER_CREDENTIALS_RELATIVE_URI is set automatically by the
ECS agent and used by the service to identify when running locally vs on ECS.
*/
const ECS_CREDENTIALS = config.AWS_CONTAINER_CREDENTIALS_RELATIVE_URI;

const createElasticSearchClient = (credentials) => {
  const client = new es.Client({
    host: config.ELASTIC_SEARCH_HOST,
    apiVersion: '5.0',
    connectionClass: httpAwsEs,
    amazonES: {
      region: config.AWS_REGION,
      credentials,
    },
    log: 'info',
  });
  return client;
};

class Elasticsearch {
  constructor() {
    this.credentials = ECS_CREDENTIALS ?
      new AWS.ECSCredentials() :
      new AWS.EnvironmentCredentials('AWS');
    this.client = createElasticSearchClient(this.credentials);
  }

  async updateCredentials() {
    if (ECS_CREDENTIALS) {
      await this.credentials.getPromise();
      await this.credentials.refreshPromise();
    }
  }

  async getClient() {
    if (ECS_CREDENTIALS && this.credentials.needsRefresh()) {
      await this.updateCredentials();
      this.client = createElasticSearchClient(this.credentials);
    }
    return this.client;
  }
}

export default new Elasticsearch();
