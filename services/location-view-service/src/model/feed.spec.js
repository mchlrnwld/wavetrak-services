import { expect } from 'chai';
import sinon from 'sinon';
import elasticsearch from './elasticsearch';
import feedArticlesFixture from './fixtures/feedArticles.json';
import {
  parseFeedSearchHits,
  getFeedPost,
  getFeedPosts,
} from './feed';

describe('model / feed', () => {
  const client = {
    search: sinon.stub(),
  };

  const sourceFilterFixture = [
    'articleId',
    'createdAt',
    'updatedAt',
    'premium',
    'promoted',
    'externalSource',
    'externalLink',
    'newWindow',
    'content.title',
    'content.subtitle',
    'content.body',
    'permalink',
    'media.type',
    'media.feed1x',
    'media.feed2x',
    'media.promobox1x',
    'media.promobox2x',
    'author.iconUrl',
    'author.name',
    'tags',
  ];

  beforeEach(() => {
    sinon.stub(elasticsearch, 'getClient');
    elasticsearch.getClient.resolves(client);
    client.search.resolves(feedArticlesFixture);
  });

  afterEach(() => {
    client.search.reset();
    elasticsearch.getClient.restore();
  });

  describe('parseFeedSearchHits', () => {
    it('maps feed search results', () => {
      expect(parseFeedSearchHits({
        hits: [
          {
            _type: 'Editorial',
            _source: {
              articleId: '2NvDL0XJXXcG8YAwgS',
              createdAt: new Date(1489194883454),
              updatedAt: new Date(1489194883454),
              premium: false,
              promoted: [],
              externalSource: null,
              externalLink: null,
              newWindow: false,
              content: {
                title: 'Winter is Coming!',
                subtitle: 'So say we all.',
                body: 'Test body',
              },
              permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
              media: {
                type: 'image',
                url: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
              },
              author: {
                iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
                name: 'Shaler Perry',
              },
              tags: [
                {
                  name: 'Waves',
                  url: '/',
                },
              ],
            },
            sort: [1489194883454, '2NvDL0XJXXcG8YAwgS'],
          },
        ],
      }, false)).to.deep.equal([
        {
          id: '2NvDL0XJXXcG8YAwgS',
          contentType: 'EDITORIAL',
          createdAt: 1489194883,
          updatedAt: 1489194883,
          premium: false,
          promoted: [],
          externalSource: null,
          externalLink: null,
          newWindow: false,
          content: {
            title: 'Winter is Coming!',
            subtitle: 'So say we all.',
            body: 'Test body',
          },
          permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
          media: {
            type: 'image',
            url: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          },
          author: {
            iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
            name: 'Shaler Perry',
          },
          tags: [
            {
              name: 'Waves',
              url: '/',
            },
          ],
          nextPageStart: '1489194883454,2NvDL0XJXXcG8YAwgS',
        },
      ]);
    });

    it('defaults missing fields', () => {
      expect(parseFeedSearchHits({
        hits: [{
          _type: 'Editorial',
          _source: {
            articleId: '2NvDL0XJXXcG8YAwgS',
            createdAt: 1489194883454,
            updatedAt: 1489194883454,
            premium: false,
            promoted: [],
            externalSource: null,
            externalLink: null,
            newWindow: false,
            content: {
              title: 'Winter is Coming!',
            },
          },
        }],
      })).to.deep.equal([{
        id: '2NvDL0XJXXcG8YAwgS',
        contentType: 'EDITORIAL',
        createdAt: 1489194883,
        updatedAt: 1489194883,
        premium: false,
        promoted: [],
        externalSource: null,
        externalLink: null,
        newWindow: false,
        content: {
          title: 'Winter is Coming!',
          subtitle: null,
          body: null,
        },
        permalink: null,
        media: null,
        author: null,
        tags: [],
        nextPageStart: null,
      }]);
    });

    it('clears content body if source premium is true and premium is false', () => {
      const feedSearchHits = {
        hits: [
          {
            _type: 'Editorial',
            _source: {
              articleId: '12CCDDpSuBQDe86u4GmssPPE',
              createdAt: new Date(1491174682685),
              updatedAt: new Date(1491174682685),
              premium: true,
              promoted: [],
              externalSource: null,
              externalLink: null,
              newWindow: false,
              content: {
                title: 'Winter is Coming!',
                subtitle: 'So say we all.',
                body: 'Test body',
              },
            },
            sort: [
              1491174682685,
              '12CCDDpSuBQDe86u4GmssPPE',
            ],
          },
        ],
      };
      expect(parseFeedSearchHits(feedSearchHits, false)[0].content.body).to.be.null();
      expect(parseFeedSearchHits(feedSearchHits, true)[0].content.body).to.equal('Test body');
    });
  });

  describe('getFeedPost', () => {
    it('searches articles by post ID', async () => {
      const promotedFeedArticles = await getFeedPost('2NvDL0XJXXcG8YAwgS');
      expect(promotedFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: {
            term: {
              articleId: '2NvDL0XJXXcG8YAwgS',
            },
          },
        },
      });
      // eslint-disable-next-line
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('getFeedPosts', () => {
    it('searches articles by array of post IDs', async () => {
      const promotedFeedArticles = await getFeedPosts(['2NvDL0XJXXcG8YAwgS']);
      expect(promotedFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        ids: {
          values: ['2NvDL0XJXXcG8YAwgS'],
        },
      });
      // eslint-disable-next-line
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });
});
