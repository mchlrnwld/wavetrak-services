import fetch from 'node-fetch';
import config from '../config';
import { getGeoLocation } from './geotarget';

export const getNearbySpot = async (lat, lon) => {
  const result = await fetch(
    `${config.SPOTS_API}/admin/spots?filter=nearby&select=name&limit=1&coordinates=${lon}&coordinates=${lat}`
  );

  const response = await result.json();
  if (result.status === 200) return response;
  throw response;
};

export const getNearbySpotFromRequest = async (request) => {
  const ip = request.query.ipAddress || request.ip;
  let { lat, lon } = request.query;

  if (!lat || !lon) {
    const userGeoLocation = await getGeoLocation(ip);
    lat = userGeoLocation.location.latitude;
    lon = userGeoLocation.location.longitude;
  }
  const nearbySpot = await getNearbySpot(
    lat,
    lon,
  );
  return nearbySpot;
};
