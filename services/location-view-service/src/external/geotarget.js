import fetch from 'node-fetch';
import config from '../config';

const getGeoLocation = async (ipAddress) => {
  const result = await fetch(
    `${config.GEOTARGET_API}/Region/?ipAddress=${ipAddress}`
  );

  const response = await result.json();
  if (result.status === 200) return response;
  throw response;
};

export { getGeoLocation }; // eslint-disable-line
