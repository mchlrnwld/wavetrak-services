import fetch from 'node-fetch';
import config from '../config';
import logger from '../common/logger';

const log = logger('location-view-service');

const getTaxonomy = async (type, id, maxDepth = 5) => {
  const result = await fetch(`${config.SPOTS_API}/taxonomy?type=${type}&id=${id}&maxDepth=${maxDepth}`);
  const response = await result.json();

  if (result.status === 200) return response;
  if (result.status === 400) {
    try {
      log.error({
        statusCode: response.status,
        message: `Error encountered (${response.message}) for taxonomy id ${id} and type ${type}`,
      });
    } catch (fatalErr) {
      console.log(result);
      console.log(fatalErr);
    }
    return {
      error: true,
      status: 400,
      message: response.message,
    };
  }
  throw response;
};

export { getTaxonomy }; // eslint-disable-line
