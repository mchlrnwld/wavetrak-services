import fetch from 'node-fetch';
import config from '../config';

const getConditions = async () => {
  const result = await fetch(
    `${config.REPORTS_API}/admin/reports/conditions`
  );

  const conditionsRatings = await result.json();
  const response = conditionsRatings.ratings.map(rating => rating.value);
  if (result.status === 200) return response;
  throw response;
};

export { getConditions }; // eslint-disable-line
