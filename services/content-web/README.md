# Surfline Content App

This is the `content-web` [Next.js](https://nextjs.org/) web application bootstrapped with [create-next-app](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Run the development server:

```
npm run dev
```

## Bundling and Deploying

Building a non development distribution

```
npm run build
```

Running the non development distribution

```
npm start
```

## Testing

Uses [Jest](https://jestjs.io), [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/), [eslint](https://eslint.org), and [Istanbul](https://istanbul.js.org) for test coverage.

Running eslint:

```
npm run lint
```

Running unit tests (Jest and RTL):

```
npm run test
```

Opening e2e testing of app (Cypress and Percy):

```
npm run dev
npm run test:e2e:open
```

Opening e2e testing of Storybook (Cypress and Percy):

```
npm run storybook
npm run test:storybook:open
```

### Environment Variables

|Key|Description|
|---|-----------|
|`SERVER_PORT`|Port allocated for express. This serves the app.|
|`NODE_ENV`|The current environment. Valid options are `development`, `sandbox`, `staging`, and `production`.|
|`NEWRELIC_ENABLED`|Valid options are `true` or `false`.|

### Named Environment Configurations

Environment configurations are located in `src/config`. These variables are made
available to the client. Any sensitive keys should use an environment variable.

## Performance and Monitoring

Instruments...
* New Relic APM
* New Relic Browser

## Analytics

Segment is setup.

### Tracking Events in Segment

Redux middleware to handle segment tracking.
