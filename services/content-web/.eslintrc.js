module.exports = {
  rules: {
    'prettier/prettier': 'error',
  },
  overrides: [
    {
      files: ['**/*.js', '**/*.jsx', '**/*.mjs'],
      extends: [
        '@surfline/eslint-config-web/base',
        '@surfline/eslint-config-web/react',
        '@surfline/eslint-config-web/jest',
        'next/core-web-vitals',
        'prettier',
      ],
      rules: {
        'import/extensions': [
          'error',
          'ignorePackages',
          {
            js: 'never',
            jsx: 'never',
            ts: 'never',
            tsx: 'never',
          },
        ],
      },
    },
    {
      parserOptions: {
        project: ['./tsconfig.json', './tsconfig.server.json'],
      },
      files: ['**/*.ts', '**/*.tsx'],
      extends: [
        '@surfline/eslint-config-web/jest',
        '@surfline/eslint-config-web/typescript',
        '@surfline/eslint-config-web/typescript-react',
        'next/core-web-vitals',
        'prettier',
      ],
      rules: {
        'jsx-a11y/anchor-is-valid': 'off',
        'no-restricted-exports': 'off',
        'react/jsx-props-no-spreading': 'off',
      },
    },
  ],
};
