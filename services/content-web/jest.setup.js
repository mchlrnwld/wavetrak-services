import '@testing-library/jest-dom/extend-expect';
import 'jest-canvas-mock';

jest.mock('next/config', () => () => ({
  publicRuntimeConfig: {
    APP_ENV: 'testing',
  },
}));
