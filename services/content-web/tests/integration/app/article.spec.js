/// <reference types="cypress" />
/* eslint-disable jest/expect-expect */

describe('Article', () => {
  beforeEach(() => {
    cy.visit(`${Cypress.config('baseUrl')}${Cypress.env('URL_ARTICLE')}`);
  });

  it('Should render default page', () => {
    cy.get('[data-test=article-title]').should('contain.text', 'Winterlust Test');
    cy.visualDiff({
      name: 'Article: Article Hero',
    });
  });
});
