/// <reference types="cypress" />
/* eslint-disable jest/expect-expect */

describe('Travel Landing', () => {
  beforeEach(() => {
    cy.visit(`${Cypress.config('baseUrl')}${Cypress.env('URL_TRAVEL_LANDING')}`);
  });

  it('Should render default page', () => {
    cy.get('[data-test=hero-headline]').should('contain.text', 'Surfline Travel');
    cy.visualDiff({
      name: 'Travel Landing: Default state',
    });
  });
});
