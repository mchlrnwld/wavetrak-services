/// <reference types="cypress" />
/* eslint-disable jest/expect-expect */

describe('Search', () => {
  beforeEach(() => {
    cy.visit(`${Cypress.config('baseUrl')}${Cypress.env('URL_SEARCH')}`);
  });

  it('Should render default page', () => {
    cy.get('[data-test=headline]').should('contain.text', 'California');
    cy.visualDiff({
      name: 'Search: Default state',
    });
  });

  it('Should expand Surf News', () => {
    cy.get('[data-test=result-group-expand-toggle]').first().trigger('click');
    cy.visualDiff({
      name: 'Search: Expanded state',
    });
  });
});
