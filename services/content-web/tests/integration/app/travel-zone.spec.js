/// <reference types="cypress" />
/* eslint-disable jest/expect-expect */

describe('Travel Zone', () => {
  beforeEach(() => {
    cy.visit(`${Cypress.config('baseUrl')}${Cypress.env('URL_TRAVEL_ZONE')}`);
  });

  it('Should render default page', () => {
    cy.get('[data-test=hero-headline]').should('contain.text', 'Travel Zone');
    cy.visualDiff({
      name: 'Travel Zone: Default state',
    });
  });
});
