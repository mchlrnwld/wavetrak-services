/// <reference types="cypress" />
/* eslint-disable jest/expect-expect */

describe('Surf News', () => {
  beforeEach(() => {
    cy.visit(`${Cypress.config('baseUrl')}${Cypress.env('URL_SURF_NEWS')}`);
  });

  it('Should render default page', () => {
    cy.get('[data-test=taxonomy-title]').should('contain.text', 'Surf News');
    cy.visualDiff({
      name: 'Surf News: Default state',
    });
  });

  it('Should load more Surf News', () => {
    cy.get('[data-test=load-more] button').first().trigger('click');
    cy.visualDiff({
      name: 'Surf News: Expanded state',
    });
  });
});
