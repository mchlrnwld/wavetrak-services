/// <reference types="cypress" />
Cypress.Commands.add('visualDiff', (config) => {
  const { name, ...rest } = config;
  cy.percySnapshot(name, { rest });
});
