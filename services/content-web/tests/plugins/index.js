/// <reference types="cypress" />
const dotenvPlugin = require('cypress-dotenv');

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (_, config) => {
  config = dotenvPlugin(config); // eslint-disable-line no-param-reassign

  return config;
};
