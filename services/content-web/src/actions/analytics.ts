import { createAction } from '@reduxjs/toolkit';

const ANALYTICS_READY = 'ANALYTICS_READY';

export const analyticsReady = createAction(ANALYTICS_READY);

const defaultExport = {
  analyticsReady,
};

export default defaultExport;
