import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as spotsAPI from '../common/api/spots';
import {
  FETCH_TOP_SPOTS,
  FETCH_TOP_SPOTS_SUCCESS,
  FETCH_TOP_SPOTS_FAILURE,
  fetchTopSpots,
} from './topSpots';
import topSpotsFixture from './fixtures/topSpots.json';

describe('actions / locationView', () => {
  let mockStore;

  beforeAll(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchTopSpots', () => {
    afterEach(() => {
      spotsAPI.getTopSpots.mockRestore();
    });

    test('dispatches FETCH_TOP_SPOTS_SUCCESS with geonameId', async () => {
      jest.spyOn(spotsAPI, 'getTopSpots').mockResolvedValueOnce(topSpotsFixture);

      const store = mockStore({});
      const geonameId = '12345';

      await store.dispatch(fetchTopSpots(geonameId));

      expect(spotsAPI.getTopSpots).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual([
        {
          type: FETCH_TOP_SPOTS,
        },
        {
          type: FETCH_TOP_SPOTS_SUCCESS,
          topSpots: topSpotsFixture.data.topSpots,
        },
      ]);
    });

    test('dispatches FETCH_TOP_SPOTS_FAILURE with invalid geonameId', async () => {
      const errorMessage = { message: 'Unable to find a suitable node for that taxonomy' };
      jest.spyOn(spotsAPI, 'getTopSpots').mockRejectedValueOnce(errorMessage);

      const store = mockStore({});
      const geonameId = '12345';

      await store.dispatch(fetchTopSpots(geonameId));

      expect(spotsAPI.getTopSpots).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual([
        {
          type: FETCH_TOP_SPOTS,
        },
        {
          type: FETCH_TOP_SPOTS_FAILURE,
          error: { message: 'Unable to find a suitable node for that taxonomy' },
        },
      ]);
    });
  });
});
