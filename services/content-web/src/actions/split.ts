import { createAction } from '@reduxjs/toolkit';

const SPLIT_READY = 'SPLIT_READY';

export const splitReady = createAction(SPLIT_READY);

const defaultExport = {
  splitReady,
};

export default defaultExport;
