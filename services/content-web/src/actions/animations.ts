export const SET_SCROLL_POSITION = 'SET_SCROLL_POSITION';

export const setTopSpotsScrollPosition = (scrollPosition: string | number) => ({
  type: SET_SCROLL_POSITION,
  component: 'topSpots',
  scrollPosition,
});
