import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as fetchLocationViewAPI from '../common/api/locationView';
import {
  FETCH_LOCATION_VIEW,
  FETCH_LOCATION_VIEW_SUCCESS,
  FETCH_LOCATION_VIEW_FAILURE,
  fetchLocationView,
} from './locationView';

describe('actions / locationView', () => {
  let mockStore;

  beforeAll(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchLocationViewTravel', () => {
    afterEach(() => {
      fetchLocationViewAPI.fetchLocationViewTravel.mockRestore();
    });

    test('dispatches FETCH_LOCATION_VIEW_SUCCESS with geonameId', async () => {
      const locationViewResponse = {
        url: 'https://www.surfline.com/travel/12345',
        taxonomy: {},
        associated: {},
        boundingBox: {},
        children: [],
        travelContent: {},
        breadCrumbs: [],
      };

      jest
        .spyOn(fetchLocationViewAPI, 'fetchLocationViewTravel')
        .mockResolvedValueOnce(locationViewResponse);

      const store = mockStore({});
      const geonameId = '12345';

      await store.dispatch(fetchLocationView(geonameId));

      expect(fetchLocationViewAPI.fetchLocationViewTravel).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual([
        {
          type: FETCH_LOCATION_VIEW,
        },
        {
          type: FETCH_LOCATION_VIEW_SUCCESS,
          url: locationViewResponse.url,
          taxonomy: locationViewResponse.taxonomy,
          associated: locationViewResponse.associated,
          boundingBox: locationViewResponse.boundingBox,
          children: locationViewResponse.children,
          travelContent: locationViewResponse.travelContent,
          breadCrumbs: locationViewResponse.breadCrumbs,
        },
      ]);
    });

    test('dispatches FETCH_USER_DETAILS_FAILURE with invalid geonameId', async () => {
      jest
        .spyOn(fetchLocationViewAPI, 'fetchLocationViewTravel')
        .mockRejectedValueOnce({ statusCode: 404 });

      const store = mockStore({});
      const geonameId = '12345';

      await store.dispatch(fetchLocationView(geonameId));
      expect(fetchLocationViewAPI.fetchLocationViewTravel).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual([
        {
          type: FETCH_LOCATION_VIEW,
        },
        {
          type: FETCH_LOCATION_VIEW_FAILURE,
          error: { statusCode: 404 },
        },
      ]);
    });
  });
});
