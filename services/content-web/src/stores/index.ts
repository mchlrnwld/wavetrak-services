import { useMemo } from 'react';
import { configureStore, ThunkAction, Action, ConfigureStoreOptions } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query/react';
import { createReducerInjecter, AsyncReducers, injectReducer } from '@surfline/web-common';
import getConfig from 'next/config';

import rootReducer, { ContentAsyncReducers } from '../reducers';
import createReducer from '../reducers/createReducer';

export type WavetrakStore = ReturnType<typeof initStore>;

export type AppState = ReturnType<WavetrakStore['getState']>;

export type AppDispatch = ReturnType<typeof getOrCreateStore>['dispatch'];
// https://redux.js.org/usage/usage-with-typescript#define-root-state-and-dispatch-types
export type RootState = ReturnType<typeof getOrCreateStore>['getState'];

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;

export type { AsyncReducers, injectReducer };

const { APP_ENV } = getConfig().publicRuntimeConfig;

let store: WavetrakStore;

function initStore(initialState?: ConfigureStoreOptions['preloadedState']) {
  // We need to force cast this to the ContentAsyncReducers since these are injected and managed by
  // Backplane and the types aren't available here.
  // This will and should change when we move backplane to a package, or to typescript
  const extraReducers = {
    backplane: createReducer({}, {}),
  } as any as ContentAsyncReducers;

  const initializedStore = configureStore({
    preloadedState: initialState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({ immutableCheck: { warnAfter: 100 } }),
    // The initial backplane reducer is just for the server render, it doesn't need any
    // handlers. It just needs to be present to set the preloaded state from the server side
    reducer: rootReducer(extraReducers),
    devTools:
      APP_ENV !== 'production'
        ? {
            name: 'Surfline - Content',
          }
        : false,
  });

  createReducerInjecter(initializedStore, (asyncReducers) =>
    rootReducer(asyncReducers as ContentAsyncReducers),
  );

  setupListeners(initializedStore.dispatch);

  return initializedStore;
}

export const getOrCreateStore = (preloadedState?: ConfigureStoreOptions['preloadedState']) => {
  // eslint-disable-next-line no-underscore-dangle
  const s = store ?? initStore(preloadedState);

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return s;

  // Create the store once in the client
  if (!store) {
    store = s;
  }

  return s;
};

export function useStore(initialState: AppState) {
  const memoizedStore = useMemo(() => getOrCreateStore(initialState), [initialState]);
  return memoizedStore;
}
