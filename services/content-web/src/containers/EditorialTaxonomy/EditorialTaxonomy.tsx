import React, { useCallback, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import Link from 'next/link';
import classnames from 'classnames';
import { ContentContainer, FeedCard, FeatureCard, GoogleDFP } from '@surfline/quiver-react';
import {
  getUserPremiumStatus,
  getUserDetails,
  getEntitlements,
  trackNavigatedToPage,
} from '@surfline/web-common';
import { WithRouterProps } from 'next/dist/client/with-router';
import { NextComponentType } from 'next';
import MetaTags from '../../components/MetaTags';
import { PostsBlock, LoadMorePosts } from '../../components/Editorial';
import PageLoading from '../../components/PageLoading';
import Chevron from '../../components/Chevron';
import getTaxonomyDetails from '../../selectors/editorialTaxonomyDetails';
import getTaxonomyPosts from '../../selectors/editorialTaxonomyPosts';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';
import { AppState } from '../../stores/index';
import { BreadCrumbs } from '../../types/breadCrumbs';

const getRecentPostsClassName = (hasSubcategories: any, isPremium: any) =>
  classnames({
    'sl-taxonomy-term-recent-posts': true,
    'sl-taxonomy-term-recent-posts--with-categories': hasSubcategories,
    'sl-taxonomy-term-recent-posts--not-premium': !isPremium,
  });

const getNavClassName = (isActive: boolean) =>
  classnames({
    'sl-editorial-taxonomy__header__nav__terms__term': true,
    'sl-editorial-taxonomy__header__nav__terms__term--active': isActive,
  });

const getArticleImage = (media: any, imageResizing: any, feature = false) => {
  const mediaUrl = feature && media?.promobox1x ? media?.promobox1x : media?.feed2x;
  const settings = feature ? 'w=1200,q=85,f=auto,fit=contain' : 'w=740,q=75,f=auto,fit=contain';
  return imageResizing ? `${config.cloudflareImageResizingUrl(mediaUrl, settings)}` : mediaUrl;
};

type TaxonomyDetails = {
  name: string;
  slug: string;
  taxonomy: string;
  id: number;
  subCategories: Array<any>;
  navTerms: Array<any>;
  breadcrumbs: BreadCrumbs;
};

type TaxonomyPosts = {
  posts: Array<any>;
  loading: boolean;
  infiniteScroll: boolean;
};

type Props = {
  taxonomyDetails: TaxonomyDetails;
  taxonomyPosts: TaxonomyPosts;
  isPremium: boolean;
  pathname: string;
  entitlements: Array<never> | undefined;
  userDetails: {
    _id: string;
    email: string;
    firstName: string;
    lastName: string;
  };
} & WithRouterProps;

const EditoralTaxonomy: NextComponentType<any, any, any> = (props: Props) => {
  const {
    entitlements,
    isPremium,
    pathname,
    router: {
      query: { subCategory, term },
    },
    taxonomyDetails,
    taxonomyPosts,
    userDetails,
  } = props;

  const { taxonomy } = taxonomyDetails;

  const subCategories = taxonomyDetails && taxonomyDetails.subCategories;

  const navTerms = taxonomyDetails && taxonomyDetails.navTerms;

  const isAllNews = taxonomyDetails.slug === 'category';

  const currentSlug = taxonomyDetails && taxonomyDetails.slug;

  const taxonomyId = taxonomyDetails && taxonomyDetails.id;

  const breadcrumbs = taxonomyDetails && taxonomyDetails.breadcrumbs;

  const hasSubcategories = subCategories && subCategories.length > 0;

  const categoryWhitelist = [
    'ALL',
    'PREMIUM',
    'BREAKING',
    'FEATURES',
    'VIDEO',
    'TRAVEL',
    'GEAR',
    'TRAINING',
    'SCIENCE',
  ];

  const adTarget: any = [];

  const aboveHeaderAd = useCallback(
    (taxonomyId = 0, taxonomyValue = '') => {
      const headerAdElement = document.getElementById('sl-header-ad');
      const adTarget: any = [];
      let adConfigName = 'editorial_home';
      if (taxonomyValue.indexOf('series') !== -1) {
        adTarget.push(['contentSeriesId', taxonomyId]);
        adConfigName = 'editorial_series';
      } else if (taxonomyValue.indexOf('category') !== -1) {
        adTarget.push(['contentCategoryId', taxonomyId]);
        adConfigName = 'editorial_category';
      } else if (taxonomyValue.indexOf('tag') !== -1) {
        adTarget.push(['contentTagId', taxonomyId]);
        adConfigName = 'editorial_tag';
      }
      adTarget.push(['qaFlag', false]);
      if (headerAdElement) {
        ReactDOM.render(
          <GoogleDFP
            adConfig={loadAdConfig(adConfigName, adTarget, entitlements, !!userDetails, null)}
            isHtl
            isTesting={config.htl.isTesting}
          />,
          headerAdElement,
        );
      }
    },
    [entitlements, userDetails],
  );

  const trackPage = useCallback(
    ({ taxonomy: taxonomyValue, name, pageId }: any) => {
      const pageProperties: any = {
        channel: 'editorial',
        category: 'editorial',
        path: pathname,
        url: window && window.location.href,
      };

      const pageName = `${name.charAt(0).toUpperCase()}${name.slice(1)}`;

      if (!taxonomyValue || taxonomyValue === 'category') {
        pageProperties.categoryName = pageName || 'Category';
        pageProperties.categoryId = pageId || 0;
        pageProperties.name = pageProperties.categoryName;
      } else if (taxonomyValue === 'series') {
        pageProperties.seriesName = pageName || 'Series';
        pageProperties.seriesId = pageId || 0;
        pageProperties.name = pageProperties.seriesName;
      } else if (taxonomyValue === 'tag') {
        pageProperties.tagName = pageName || 'Tag';
        pageProperties.tagId = pageId || 0;
        pageProperties.name = pageProperties.tagName;
      }

      trackNavigatedToPage(pageName, pageProperties);
    },
    [pathname],
  );

  useEffect(() => {
    if (taxonomyDetails) {
      trackPage({
        taxonomy,
        name: taxonomyDetails.name,
        pageId: taxonomyDetails.id,
      });
      aboveHeaderAd(taxonomyDetails.id, taxonomyDetails.taxonomy);
    }
  }, [aboveHeaderAd, trackPage, taxonomy, taxonomyDetails]);

  if (!taxonomyDetails || !taxonomyPosts) return null;

  let pageTitle = taxonomy === 'category' ? null : taxonomy;

  if (term || subCategory) pageTitle = term?.toString() || subCategory?.toString() || '';

  pageTitle = pageTitle ? ` ${pageTitle.charAt(0).toUpperCase()}${pageTitle.slice(1)} ` : ' ';

  let adViewType = 'CONTENT_HOME';

  if (taxonomy) {
    if (taxonomy.indexOf('series') !== -1) {
      adTarget.push(['contentSeriesId', taxonomyId]);
      adViewType = 'CONTENT_SERIES';
    } else if (taxonomy.indexOf('category') !== -1) {
      adTarget.push(['contentCategoryId', taxonomyId]);
      adViewType = 'CONTENT_CATEGORY';
    } else if (taxonomy.indexOf('tag') !== -1) {
      adTarget.push(['contentTagId', taxonomyId]);
      adViewType = 'CONTENT_TAG';
    }
    adTarget.push(['qaFlag', false]);
  }

  return (
    <div className="sl-editorial-taxonomy">
      <MetaTags
        title={`Surfing${pageTitle}Articles:
            Latest Surf${pageTitle}News, Videos, & Photos at Surfline`}
        description={`Access Surfline's daily digest of the latest in surf
            ${pageTitle.toLowerCase()}journalism. Get breaking news in all things surf,
            featured stories, and our renowned surf forecast and science articles.`}
        yoastMeta={undefined /* TODO: Add yoast meta from response here */}
        permalink={undefined /* TODO: Add taxonomy.permalink from response */}
        tags={[]}
      />
      <div className="sl-editorial-taxonomy__ad">
        {!isPremium && (
          <GoogleDFP
            adConfig={loadAdConfig(
              'editorialTaxonomyBelowNav',
              adTarget,
              entitlements,
              !!userDetails,
              adViewType,
            )}
            isHtl
            isTesting={config.htl.isTesting}
          />
        )}
      </div>
      <ContentContainer>
        <div className="sl-editorial-taxonomy__header">
          {breadcrumbs && (
            <div className="sl-editorial-taxonomy__header__breadcrumbs">
              <div className="sl-editorial-taxonomy__header__breadcrumbs__crumb">
                <Link href="/surf-news/">All Surf News</Link>
                <Chevron direction="right" />
              </div>
              {breadcrumbs.map((crumb) => (
                <div
                  className="sl-editorial-taxonomy__header__breadcrumbs__crumb"
                  key={`${crumb.slug}-${crumb.name}`}
                >
                  <Link href={`/${taxonomy}/${crumb.slug}`}>{crumb.name}</Link>
                  <Chevron direction="right" />
                </div>
              ))}
            </div>
          )}
          <h1 data-test="taxonomy-title">{isAllNews ? 'Surf News' : taxonomyDetails.name}</h1>
          {!breadcrumbs && (
            <div className="sl-editorial-taxonomy__header__nav">
              <div className="sl-editorial-taxonomy__header__nav__terms">
                <Link href="/surf-news/">
                  <a className={getNavClassName(isAllNews)}>All</a>
                </Link>
                {navTerms &&
                  navTerms
                    .filter(
                      (navTerm: any) =>
                        categoryWhitelist.indexOf(navTerm.slug.toUpperCase()) !== -1,
                    )
                    .map((navTerm: any) => (
                      <Link href={`/category/${navTerm.slug}`} key={navTerm}>
                        <a className={getNavClassName(currentSlug === navTerm.slug)}>
                          {navTerm.name}
                        </a>
                      </Link>
                    ))}
              </div>
            </div>
          )}
        </div>
        {taxonomyPosts.loading && !taxonomyPosts.infiniteScroll ? (
          <PageLoading />
        ) : (
          <>
            <div className="sl-tax-cat-blocks">
              {subCategories &&
                subCategories.map((cat: any) => {
                  const { posts, name, slug } = cat;
                  return (
                    posts &&
                    !(cat.breadcrumbs && isAllNews) && (
                      <PostsBlock
                        name={name}
                        posts={posts}
                        slug={slug}
                        taxonomy={taxonomy}
                        term={term?.toString() || ''}
                        imageResizing
                      />
                    )
                  );
                })}
            </div>
            <div className={getRecentPostsClassName(hasSubcategories, isPremium)}>
              {!isPremium && hasSubcategories && (
                <div className="sl-taxonomy-term-recent-posts__ad">
                  <GoogleDFP
                    adConfig={loadAdConfig(
                      'editorialTaxonomyMidPage',
                      adTarget,
                      entitlements,
                      !!userDetails,
                      adViewType,
                    )}
                    isHtl
                    isTesting={config.htl.isTesting}
                  />
                </div>
              )}
              <h2>Recent</h2>
              <div className="sl-taxonomy-term-recent-posts__posts">
                {taxonomyPosts &&
                  taxonomyPosts.posts &&
                  taxonomyPosts.posts.map((post: any) => {
                    if (!post) return null;
                    const timestamp = Math.floor(new Date(post.createdAt).getTime() / 1000);
                    return post.isFeatured ? (
                      <section>
                        {!isPremium && (
                          <div className="sl-taxonomy-term-recent-posts__ad">
                            <GoogleDFP
                              adConfig={loadAdConfig(
                                'editorialTaxonomyMidPage',
                                adTarget,
                                entitlements,
                                !!userDetails,
                                adViewType,
                              )}
                              isHtl
                              isTesting={config.htl.isTesting}
                            />
                          </div>
                        )}
                        {post.id && (
                          <FeatureCard
                            isPremium={post.premium}
                            imageUrl={getArticleImage(post.media, true, true)}
                            permalink={post.permalink}
                            title={post.title}
                            onClickLink={() => null}
                          />
                        )}
                      </section>
                    ) : (
                      <FeedCard
                        isPremium={post.premium}
                        category={post.categories[0]}
                        imageUrl={getArticleImage(post.media, true)}
                        title={post.title}
                        subtitle={post.subtitle}
                        createdAt={timestamp}
                        permalink={post.permalink}
                        showTimestamp={!Number.isNaN(Number(timestamp))}
                        onClickLink={() => null}
                        onClickTag={() => null}
                      />
                    );
                  })}
              </div>
              <LoadMorePosts
                subCategory={subCategory?.toString() || ''}
                taxonomy={taxonomy}
                term={term?.toString() || ''}
              />
            </div>
          </>
        )}
      </ContentContainer>
    </div>
  );
};

export default connect((state: AppState) => ({
  taxonomyDetails: getTaxonomyDetails(state),
  taxonomyPosts: getTaxonomyPosts(state),
  isPremium: getUserPremiumStatus(state),
  userDetails: getUserDetails(state),
  entitlements: getEntitlements(state),
}))(withRouter(EditoralTaxonomy));
