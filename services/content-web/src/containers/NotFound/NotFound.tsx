import React from 'react';

type Props = {
  staticContext?: {
    status: number;
    url: number;
  };
};

const NotFound: React.FC<Props> = ({ staticContext = { status: undefined, url: undefined } }) => {
  if (staticContext) {
    // This will force a redirect on the server side when the routes are evaluated
    // eslint-disable-next-line no-param-reassign
    staticContext.status = 404;
  }

  return (
    <div>
      <h1>Page not found</h1>
    </div>
  );
};

export default NotFound;
