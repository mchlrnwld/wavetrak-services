import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';
import { trackNavigatedToPage } from '@surfline/web-common';
import { useRouter } from 'next/router';
import { showAllToggle } from '../../actions/search';
import { SearchResult, SearchResults } from '../../components/SearchResults';
import config from '../../config';
import { getSearchResults, getSearchSections } from '../../selectors/search';
import type { AppState } from '../../stores';

import Styles from './Search.module.scss';

interface SearchProps {
  doShowAllToggle: Function;
  results?: SearchResult[];
  sections: any;
}

const Search = ({ doShowAllToggle, results, sections }: SearchProps) => {
  const router = useRouter();
  const { term } = router.query;
  const searchTerm = term?.toString() || '';

  useEffect(() => {
    trackNavigatedToPage('Search Results', { category: 'Search' });
  }, []);

  let totalHits = 0;
  let totalSuggestions = 0;
  results?.forEach((result: SearchResult) => {
    const [key] = Object.keys(result.suggest || {});
    const currentHits = result.hits.hits.length;
    totalHits += currentHits;
    totalSuggestions += (result.suggest as any)[key][0].options.length || 0;
  });
  let total = 0;
  if (totalHits > 0 && totalSuggestions > 0) {
    total = totalHits + totalSuggestions;
  } else if (totalHits > 0) {
    total = totalHits;
  } else if (totalSuggestions > 0) {
    total = totalSuggestions;
  }

  return (
    <>
      <Head>
        <title>{config.meta.title.search(searchTerm)}</title>
      </Head>
      <div className={Styles.totalResults}>
        <h1 className={Styles.headline} data-test="headline">
          Showing {total} results for <span className={Styles.highlight}>{searchTerm}</span>
        </h1>
      </div>
      <SearchResults
        term={searchTerm}
        results={results}
        sections={sections}
        doShowAllToggle={doShowAllToggle}
      />
    </>
  );
};

Search.defaultProps = {
  results: [],
};

export default connect(
  (state: AppState) => ({
    results: getSearchResults(state),
    sections: getSearchSections(state),
  }),
  (dispatch) => ({
    doShowAllToggle: (section: any) => dispatch(showAllToggle(section) as any),
  }),
)(Search);
