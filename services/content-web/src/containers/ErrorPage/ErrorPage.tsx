import React from 'react';
import { FiveHundred, NotFound } from '@surfline/quiver-react';
import Head from 'next/head';

interface Props {
  statusCode: number;
}

const ErrorPage: React.FC<Props> = ({ statusCode }) => {
  if (statusCode === 404) {
    return (
      <>
        <Head>
          <title>SURFLINE.COM | Not Found</title>
        </Head>
        <NotFound />
      </>
    );
  }
  return (
    <>
      <Head>
        <title>SURFLINE.COM | 500</title>
      </Head>
      <FiveHundred />
    </>
  );
};

export default ErrorPage;
