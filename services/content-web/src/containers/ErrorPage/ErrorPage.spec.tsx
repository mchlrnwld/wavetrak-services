import React from 'react';
import { render } from '@testing-library/react';

import ErrorPage from './ErrorPage';

describe('components / ErrorPage', () => {
  test('should render the 404 page', () => {
    const { container } = render(<ErrorPage statusCode={404} />);
    expect(container.firstChild).toHaveClass('body__404');
  });

  test('should render the 500 page', () => {
    const { container } = render(<ErrorPage statusCode={500} />);
    expect(container.firstChild).toHaveClass('body__500');
  });
});
