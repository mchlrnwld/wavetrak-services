import { NextPage } from 'next';
import Head from 'next/head';
import { CSSProperties } from 'react';
import config from '../../config';

const tempContainerStyle: CSSProperties = {
  height: '300px',
  margin: '0 auto',
  maxWidth: '1200px',
  padding: '20px',
};

const Travel: NextPage = () => (
  <>
    <Head>
      <title>{config.meta.title.travel.landing}</title>
    </Head>
    <main>
      <section
        style={{
          backgroundColor: 'grey',
          display: 'flex',
          alignContent: 'center',
          flexDirection: 'column',
          justifyContent: 'center',
          minHeight: 'calc(100vh - 280px)',
          textAlign: 'center',
        }}
      >
        <h1 data-test="hero-headline">Surfline Travel</h1>
      </section>
      <section style={tempContainerStyle}>
        <h2>Score in October</h2>
      </section>
      <section style={tempContainerStyle}>
        <h2>Best First Surf Trips</h2>
      </section>
      <section style={tempContainerStyle}>
        <h2>All Destinations</h2>
      </section>
      <section style={tempContainerStyle}>
        <h2>Where to stay to get shacked</h2>
      </section>
      <section style={tempContainerStyle}>
        <h2>Itching for a Surf Trip?</h2>
        <h3>Try the Maldives.</h3>
      </section>
      <section style={tempContainerStyle}>
        <h2>By Surfline Studios</h2>
      </section>
      <section style={tempContainerStyle}>
        <h2>Latest Travel Stories</h2>
      </section>
    </main>
  </>
);

export default Travel;
