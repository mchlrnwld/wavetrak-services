import { NextPage } from 'next';
import Head from 'next/head';
import config from '../../config';

import Styles from './Home.module.scss';

const Home: NextPage = () => (
  <>
    <Head>
      <title>{config.meta.title.homepage}</title>
      <meta name="description" content={config.meta.description.homepage} />
    </Head>
    <main>
      <h1 className={Styles.title}>Homepage</h1>
    </main>
  </>
);

export default Home;
