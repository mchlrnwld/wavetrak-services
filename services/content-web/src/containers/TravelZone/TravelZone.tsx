import { NextPage } from 'next';
import Head from 'next/head';
import { withRouter, NextRouter } from 'next/router';
import { CSSProperties } from 'react';
import config from '../../config';

const tempContainerStyle: CSSProperties = {
  height: '300px',
  margin: '0 auto',
  maxWidth: '1200px',
  padding: '20px',
};

type Props = {
  router: NextRouter;
};

const TravelZone: NextPage<Props> = ({ router }) => {
  const {
    query: { slug, id },
  } = router;

  return (
    <>
      <Head>
        <title>{config.meta.title.travel.zone(slug?.toString() || '')}</title>
      </Head>
      <main>
        <section
          style={{
            backgroundColor: 'grey',
            display: 'flex',
            alignContent: 'center',
            flexDirection: 'column',
            justifyContent: 'center',
            minHeight: 'calc(100vh - 280px)',
            textAlign: 'center',
          }}
        >
          <h1 data-test="hero-headline">Travel Zone</h1>
          <h3>
            SLUG: {slug} | ID: {id}
          </h3>
        </section>
        <section style={tempContainerStyle}>
          <h2>Where to stay</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>Forecasting note</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>Optimal Seasons</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>When to Score</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>Chance of Scoring</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>Forecaster Tips</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>History, culture &amp; customs</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>Local interview</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>The Waves</h2>
        </section>
        <section style={tempContainerStyle}>
          <h2>Nearby Surf Zones</h2>
        </section>
      </main>
    </>
  );
};

export default withRouter(TravelZone);
