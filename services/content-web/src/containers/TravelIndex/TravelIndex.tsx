import React from 'react';
import { connect } from 'react-redux';
import { WorldTaxonomy } from '@surfline/quiver-react';
import MetaTags from '../../components/MetaTags';
import { travelPaths, geonameMapPath } from '../../utils/urls/travelPath';
import type { AppState } from '../../stores';

/**
 * NOTE: For server side rendering to work properly, we have to instantiate the Loadable components
 * outside of the JSX so that they are preloaded when we call `Loadable.preloadAll()` on the server
 * side.
 */

type Link = {
  key: string;
  href: string;
};

type Taxonomy = {
  _id: string;
  geonameId: number;
  name: string;
  slug: string;
  links: Array<Link>;
};

type Country = {
  key: string;
  countries: Array<WorldTaxonomy>;
};

type Props = {
  countries?: Array<Country>;
  worldTaxonomy?: Taxonomy;
};

type State = {};

class TravelIndex extends React.Component<Props, State> {
  suffix = 'Beaches';

  countryComponent = (country: { slug: string; geonameId: string; name?: string }) => (
    <a href={geonameMapPath(country)}>
      {country.name} {this.suffix}
    </a>
  );

  render() {
    const { worldTaxonomy } = this.props;
    return (
      <div className="sl-cams-and-forecasts">
        <MetaTags
          title="Surfline Travel Info for Every Beach in the World"
          description="Know Before You Go. Get the best travel and weather info along with live HD beach cams. Access the world's best surf forecast team at Surfline."
          urlPath={travelPaths.base}
        />
        <WorldTaxonomy
          taxonomy={worldTaxonomy}
          countryComponent={this.countryComponent}
          headerSuffix={this.suffix}
        />
      </div>
    );
  }
}

// @ts-ignore
TravelIndex.defaultProps = {
  worldTaxonomy: [],
};

export default connect(
  (state: AppState) => ({
    worldTaxonomy: state.worldTaxonomy,
  }),
  null,
)(TravelIndex);
