import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { getUserPremiumStatus } from '@surfline/web-common';
import { getStatusCode } from '../selectors/status';
import NotFound from './NotFound';
import type { AppState } from '../stores';

type Props = {
  children: React.ReactNode;
  isPremium: boolean;
  status: number;
};

type State = {};

class TravelContainer extends Component<Props, State> {
  getClassName = (isPremium: boolean) =>
    classNames({
      'sl-travel-page-container': true,
      'sl-travel-page-container--is-premium': isPremium,
    });

  render() {
    const { children, status, isPremium } = this.props;
    return (
      <div className={this.getClassName(isPremium)}>{status === 404 ? <NotFound /> : children}</div>
    );
  }
}

// @ts-ignore
TravelContainer.defaultProps = {
  children: null,
};

export default connect((state: AppState) => ({
  isPremium: getUserPremiumStatus(state),
  status: getStatusCode(state),
}))(TravelContainer);
