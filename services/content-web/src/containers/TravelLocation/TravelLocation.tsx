import React from 'react';
import { withRouter, NextRouter } from 'next/router';
import { connect } from 'react-redux';
import { Element } from 'react-scroll';
import { get as _get } from 'lodash';
import { getUserPremiumStatus, trackNavigatedToPage } from '@surfline/web-common';
import { setTopSpotsScrollPosition } from '../../actions/animations';
import { fetchLocationView } from '../../actions/locationView';
import { fetchTopSpots } from '../../actions/topSpots';

import { getTopSpotsScrollPosition } from '../../selectors/animations';
import { getLocationView, getUnits } from '../../selectors/locationView';
import getTopSpots from '../../selectors/topSpots';

import MotionScrollWrapper from '../../components/MotionScrollWrapper';
import Hero from '../../components/Hero';
import TravelSummary from '../../components/TravelSummary';
import ChildGrid from '../../components/ChildGrid';
import RelatedArticles from '../../components/RelatedArticles';
import TopSpots from '../../components/TopSpots';
import TravelMap from '../../components/TravelMap';
import ViewForecast from '../../components/ViewForecast';
import MetaTags from '../../components/MetaTags';

import { ScrollPosition } from '../../types/scrollPosition';
import { LocationView } from '../../types/locationView';
import { Units } from '../../types/unit';
import { Spots } from '../../types/spot';

import { travelPaths } from '../../utils/urls/travelPath';
import { AppDispatch, AppState } from '../../stores';

type Props = {
  locationView: LocationView;
  units: Units;
  doSetTopSpotsPosition: (...args: any) => void;
  topSpots: Spots;
  topSpotsScrollPosition: ScrollPosition;
  isPremium: boolean;
  doFetchLocationView: (...args: any) => void;
  doFetchTopSpots: (...args: any) => void;
  router?: any;
} & NextRouter;

type State = {};

class TravelLocation extends React.Component<Props, State> {
  componentDidMount() {
    const {
      router: {
        query: { geonameId },
      },
    } = this.props;
    trackNavigatedToPage(`${_get(this.props, 'locationView.taxonomy.name', null)} Beaches`, {
      category: 'beaches',
      geonameId,
      geonameName: _get(this.props, 'locationView.taxonomy.name', null),
    });
  }

  componentDidUpdate(prevProps: Props) {
    /* fetch location view on client route change */
    const {
      doFetchLocationView,
      doFetchTopSpots,
      router: {
        query: { geonameId },
      },
    } = this.props;

    if (
      _get(prevProps, 'locationView.taxonomy.name', null) !==
      _get(this.props, 'locationView.taxonomy.name', null)
    ) {
      trackNavigatedToPage(`${_get(this.props, 'locationView.taxonomy.name', null)} Beaches`, {
        category: 'beaches',
        geonameId,
        geonameName: _get(this.props, 'locationView.taxonomy.name', null),
      });
    }

    if (geonameId !== prevProps.router.query.geonameId) {
      doFetchLocationView(geonameId);
      doFetchTopSpots(geonameId);
    }
  }

  render() {
    const {
      locationView,
      topSpots,
      units,
      doSetTopSpotsPosition,
      topSpotsScrollPosition,
      isPremium,
    } = this.props;
    const { taxonomy, url, children, travelContent, breadCrumbs } = locationView;

    const name = _get(locationView, 'taxonomy.name', '');
    const title = `Best Beaches in ${name} - Expert Guide to Traveling & Surfing in ${name} - Surfline`;
    const description = `Know Before You Go. Get the best ${name} travel and weather info along with live HD ${name} cams. Access to the world's best surf forecast team at Surfline.`;
    const urlPath = `${travelPaths.base}${url}`;

    /* Temp fix for travel jpgs routing here */
    if (!travelContent && !breadCrumbs) return null;

    return (
      <div>
        <MetaTags title={title} description={description} urlPath={urlPath} />
        <Hero
          travelContent={travelContent}
          taxonomy={taxonomy}
          breadCrumbs={breadCrumbs}
          imageResizing
        />
        <Element name="sl-travel-anchor" className="sl-travel-anchor" />
        {travelContent && travelContent.media.large ? (
          <TravelSummary travelContent={travelContent} />
        ) : null}
        {!isPremium ? <ViewForecast taxonomy={taxonomy} /> : null}
        {children && children.length ? (
          <ChildGrid taxonomy={taxonomy} geoChildren={children} imageResizing />
        ) : null}
        <MotionScrollWrapper scrollPosition={topSpotsScrollPosition}>
          <TopSpots
            taxonomy={taxonomy}
            topSpots={topSpots}
            units={units}
            doSetTopSpotsPosition={(pos) => doSetTopSpotsPosition(pos)}
            isPremium={isPremium}
          />
        </MotionScrollWrapper>
        <TravelMap taxonomy={taxonomy} />
        {travelContent && travelContent.relatedPosts.length ? (
          <RelatedArticles
            relatedArticles={travelContent.relatedPosts.filter((article) => !!article)}
            imageResizing
          />
        ) : null}
      </div>
    );
  }
}

// @ts-ignore
TravelLocation.defaultProps = {
  locationView: {
    taxonomy: null,
    url: null,
    associated: null,
    boundingBox: null,
  },
};

export default connect(
  (state: AppState) => ({
    locationView: getLocationView(state),
    topSpots: getTopSpots(state),
    units: getUnits(state),
    topSpotsScrollPosition: getTopSpotsScrollPosition(state),
    isPremium: getUserPremiumStatus(state),
  }),
  (dispatch: AppDispatch) => ({
    doSetTopSpotsPosition: (scrollPosition: number | string) =>
      dispatch(setTopSpotsScrollPosition(scrollPosition)),
    doFetchLocationView: (geonameId: string) => dispatch(fetchLocationView(geonameId)),
    doFetchTopSpots: (geonameId: string) => dispatch(fetchTopSpots(geonameId)),
  }),
  // @ts-ignore
)(withRouter(TravelLocation));
