/* eslint-disable react/no-danger */
import classnames from 'classnames';
import newrelic from 'newrelic';
import Document, { Head, Html, Main, NextScript } from 'next/document';
import {
  consentManagerConfig,
  getClientIp,
  googletagservices,
  renderFetch,
} from '@surfline/web-common';

import NewRelicBrowser from '../components/NewRelicBrowser';
import config from '../config';
import { WavetrakContext } from '../types/wavetrak';
import { BackplaneResponse, getBackplaneRenderOpts, showBackplaneFooter } from '../utils/backplane';
import segment from '../utils/segment';

const getBodyClass = (isPremium: boolean) =>
  classnames({
    'htl-usertype-premium': isPremium,
  });

interface CustomDocumentProps {
  backplane: BackplaneResponse;
  showFooter: boolean;
}

class CustomDocument extends Document<CustomDocumentProps> {
  static async getInitialProps(ctx: WavetrakContext) {
    const { pathname, renderPage, req, query, err } = ctx;

    if (err) {
      newrelic.noticeError(err);
    }

    const { qaFlag } = query;
    const { cookies } = req;
    const backplaneOpts = getBackplaneRenderOpts(pathname, query);
    const opts: any = {
      accessToken: req.cookies.access_token ?? null,
      apiVersion: 'v2',
      bundle: 'margins',
      'renderOptions[mvpForced]': req?.headers['x-surfline-mvp-forced'],
      ...(req.userId ? { userId: req.userId } : null),
      ...(cookies.ajs_anonymous_id ? { anonymousId: JSON.parse(cookies.ajs_anonymous_id) } : null),
      ...backplaneOpts,
    };
    if (qaFlag) {
      opts['renderOptions[qaFlag]'] = qaFlag;
    }
    const backplane: BackplaneResponse = await renderFetch(
      process.env.BACKPLANE_HOST || '',
      getClientIp(req) || '',
      opts,
    );
    const showFooter = showBackplaneFooter(pathname);
    const originalRenderPage = renderPage;
    ctx.renderPage = () =>
      originalRenderPage({
        // useful for wrapping the whole react tree
        // eslint-disable-next-line react/display-name
        enhanceApp: (App: any) => (props) =>
          <App {...props} backplaneServerReduxState={backplane.data.redux} />,
        // useful for wrapping in a per-page basis
        enhanceComponent: (Component) => Component,
      });
    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);

    return { ...initialProps, backplane, showFooter };
  }

  render() {
    const { backplane, showFooter } = this.props;

    const bodyClass = getBodyClass(
      !!backplane?.data?.redux?.backplane?.user?.entitlements.includes('sl_premium'),
    );

    return (
      <Html lang="en" data-sl-sha={process.env.APP_VERSION}>
        <Head>
          <script dangerouslySetInnerHTML={{ __html: `window.wavetrakNextApp = 'content'` }} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                // create the consent-manager
                const cm = document.createElement('div');
                cm.setAttribute('id', 'consent-manager');
                document.documentElement.appendChild(cm);
          `,
            }}
          />
          <script dangerouslySetInnerHTML={{ __html: segment() }} />
          <script
            dangerouslySetInnerHTML={{
              __html: googletagservices(false),
            }}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: consentManagerConfig(config.appKeys.segment, false),
            }}
          />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script
            src="https://unpkg.com/@segment/consent-manager@5.0.0/standalone/consent-manager.js"
            crossOrigin="anonymous"
            integrity="sha384-Faksh8FKgpBzGdQLDwKe5L9/6YZmofEhEQRwRhM/rUvcJ2fFTEnF6Y3SCjLMTne/"
          />
          <link rel="stylesheet" type="text/css" href={config.htl.cssUrl} />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script src={config.htl.scriptUrl} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.htlbid = window.htlbid || {cmd: []};
              `,
            }}
          />
          <link
            rel="preload"
            as="script"
            crossOrigin="anonymous"
            href={config.react.src}
            integrity={config.react.integrity}
          />
          <link
            rel="preload"
            as="script"
            crossOrigin="anonymous"
            href={config.reactDOM.src}
            integrity={config.reactDOM.integrity}
          />
          <link rel="preload" as="script" href={config.jwPlayerUrl} />
          <link rel="preload" as="script" href={backplane.data.js} />
          {backplane.data.css && (
            <>
              <link rel="preload" as="style" type="text/css" href={backplane.data.css} />
              <link rel="stylesheet" type="text/css" href={backplane.data.css} />
            </>
          )}
          <link rel="preconnect" href={config.cdnUrl} />
          <link rel="preconnect" href={config.servicesURL} />
          <link rel="preconnect" href="https://www.gravatar.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link rel="preconnect" href="https://surfline.zendesk.com" />
          <link rel="preconnect" href="https://unpkg.com" />
          <link rel="icon" href="/favicon.ico" />
          <link
            href="https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png"
            rel="apple-touch-icon"
          />
          <meta property="fb:app_id" content={config.appKeys.fbAppId} />
          <meta property="fb:page_id" content="255110695512" />
          <meta property="og:site_name" content="Surfline" />
          <meta
            name="apple-itunes-app"
            content="app-id=393782096,affiliate-data=at=1000lb9Z&ct=surfline-website-smart-banner&pt=261378"
          />
          {/* Start Google Structured Data */}
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: JSON.stringify({
                '@context': 'https://schema.org',
                '@type': 'Organization',
                name: 'Surfline',
                url: 'https://www.surfline.com/',
                logo: 'https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png',
                sameAs: [
                  'https://www.linkedin.com/company/surfline-wavetrak-inc',
                  'https://plus.google.com/+Surfline',
                  'https://www.facebook.com/Surfline/',
                  'https://www.instagram.com/surfline/',
                  'https://www.youtube.com/user/surfline',
                  'https://twitter.com/surfline',
                  'https://en.wikipedia.org/wiki/Surfline',
                ],
              }),
            }}
          />
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: JSON.stringify({
                '@context': 'http://schema.org',
                '@type': 'WebSite',
                url: 'https://www.surfline.com/',
                potentialAction: {
                  '@type': 'SearchAction',
                  target: 'https://www.surfline.com/search/{keywords}',
                  'query-input': 'required name=keywords',
                },
              }),
            }}
          />
          {/* End Google Structured Data */}
        </Head>
        <body className={bodyClass} style={{ margin: 0, padding: 0 }}>
          <NewRelicBrowser />
          <div className="quiver-page-container">
            <div
              id="backplane-header"
              dangerouslySetInnerHTML={{
                __html: backplane.data.components.header,
              }}
            />
            <main className="quiver-page-container__content" role="main" id="root">
              <Main />
            </main>
            {showFooter && (
              <div
                id="backplane-footer"
                dangerouslySetInnerHTML={{
                  __html: backplane.data.components.footer,
                }}
              />
            )}
          </div>
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script src={config.jwPlayerUrl} />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script
            crossOrigin="anonymous"
            src={config.react.src}
            integrity={config.react.integrity}
          />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script
            crossOrigin="anonymous"
            src={config.reactDOM.src}
            integrity={config.reactDOM.integrity}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.vendor = {}
                window.vendor.React = window.React;
                window.vendor.ReactDOM = window.ReactDOM;
                window.__BACKPLANE_API__ = ${JSON.stringify(backplane.data.api)};
                window.__BACKPLANE_REDUX__ = ${JSON.stringify(backplane.data.redux)};
              `,
            }}
          />
          <NextScript />
          <script defer src={backplane.data.js} />
        </body>
      </Html>
    );
  }
}

export default CustomDocument;
