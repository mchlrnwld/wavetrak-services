import { NextPage } from 'next';

import Travel from '../../../containers/Travel';
import { getOrCreateStore } from '../../../stores';

const TravelPage: NextPage = () => <Travel />;

TravelPage.getInitialProps = async (ctx) => {
  const store = getOrCreateStore();
  const isServer = !!ctx.req;

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default TravelPage;
