import { NextPage } from 'next';
import React from 'react';
import cookie from 'cookie';

import TravelContainer from '../../containers/TravelContainer';
import TravelIndex from '../../containers/TravelIndex';
import { fetchWorldTaxonomy } from '../../actions/worldTaxonomy';
import { getOrCreateStore } from '../../stores';

const TravelPage: NextPage = () => (
  <TravelContainer>
    <TravelIndex />
  </TravelContainer>
);

TravelPage.getInitialProps = async (context) => {
  const { dispatch, getState } = getOrCreateStore();

  const cookies = cookie.parse(context.req?.headers?.cookie || '');

  await dispatch(fetchWorldTaxonomy(null, cookies));

  const isServer = !!context.req;

  if (isServer) {
    return {
      ssrReduxState: getState(),
    };
  }

  return {};
};

export default TravelPage;
