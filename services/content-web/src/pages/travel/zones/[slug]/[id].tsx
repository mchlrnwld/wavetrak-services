import { NextPage } from 'next';
import TravelZone from '../../../../containers/TravelZone';
import { getOrCreateStore } from '../../../../stores';

const TravelZonePage: NextPage = () => <TravelZone />;

TravelZonePage.getInitialProps = async (context) => {
  const { getState } = getOrCreateStore();

  const isServer = !!context.req;

  if (isServer) {
    return {
      ssrReduxState: getState(),
    };
  }

  return {};
};

export default TravelZonePage;
