import { NextPage } from 'next';
import React from 'react';
import cookie from 'cookie';

import TravelContainer from '../../../containers/TravelContainer';
import TravelLocation from '../../../containers/TravelLocation';
import { fetchLocationView } from '../../../actions/locationView';
import { fetchTopSpots } from '../../../actions/topSpots';
import { getOrCreateStore } from '../../../stores';

const TravelPage: NextPage = () => (
  <TravelContainer>
    <TravelLocation />
  </TravelContainer>
);

TravelPage.getInitialProps = async (context) => {
  const { dispatch, getState } = getOrCreateStore();

  const cookies = cookie.parse(context.req?.headers?.cookie || '');

  await dispatch(fetchLocationView(context.query?.geonameId, cookies)); // maybe not cookies but probz need them
  await dispatch(fetchTopSpots(context.query?.geonameId, cookies)); // maybe not cookies but probz need them

  const isServer = !!context.req;

  if (isServer) {
    return {
      ssrReduxState: getState(),
    };
  }

  return {};
};

export default TravelPage;
