import { NextPage } from 'next';

import Home from '../containers/Home';
import { getOrCreateStore } from '../stores';

const Index: NextPage = () => <Home />;

Index.getInitialProps = async (ctx) => {
  const store = getOrCreateStore();
  const isServer = !!ctx.req;

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default Index;
