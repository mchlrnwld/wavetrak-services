import { NextPage } from 'next';
import cookie from 'cookie';
import EditorialArticle from '../../../containers/EditorialArticle';
import { getOrCreateStore } from '../../../stores';
import { fetchEditorialArticle } from '../../../actions/editorial';

const ArticlePage: NextPage = () => <EditorialArticle />;

ArticlePage.getInitialProps = async (context) => {
  const { dispatch, getState } = getOrCreateStore();

  const cookies = cookie.parse(context.req?.headers?.cookie || '');
  await dispatch(fetchEditorialArticle(context.query.id, null, cookies));

  const isServer = !!context.req;

  if (isServer) {
    return {
      ssrReduxState: getState(),
    };
  }

  return {};
};

export default ArticlePage;
