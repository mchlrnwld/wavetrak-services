import { NextPage } from 'next';
import { fetchEditorialTaxonomyDetails } from '../../actions/editorialTaxonomyDetails';
import {
  fetchEditorialTaxonomyPosts,
  resetEditorialTaxonomyPosts,
} from '../../actions/editorialTaxonomyPosts';

import EditorialTaxonomy from '../../containers/EditorialTaxonomy';
import { getOrCreateStore } from '../../stores';

const SurfNewsPage: NextPage = () => <EditorialTaxonomy />;

SurfNewsPage.getInitialProps = async (context) => {
  const { dispatch, getState } = getOrCreateStore();

  const { taxonomy, term, subCategory } = context.query;

  const isServer = !!context.req;
  await dispatch(resetEditorialTaxonomyPosts());
  await dispatch(fetchEditorialTaxonomyDetails(taxonomy?.toString(), term, subCategory));
  await dispatch(fetchEditorialTaxonomyPosts(taxonomy?.toString(), term, subCategory));

  if (isServer) {
    return {
      ssrReduxState: getState(),
    };
  }

  return {};
};

export default SurfNewsPage;
