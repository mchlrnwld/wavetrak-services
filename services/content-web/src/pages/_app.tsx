import type { AppProps } from 'next/app';
import Head from 'next/head';
import type { Router } from 'next/router';
import { useRef, useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { useRafLoop } from 'react-use';
import { getWindow, BackplaneState } from '@surfline/web-common';

import PageContainer from '../containers/PageContainer';
import { analyticsReady } from '../actions/analytics';
import { splitReady } from '../actions/split';
import Scripts from '../components/Scripts';
import { useStore } from '../stores';
import { instantiateClientSideBackplane, showBackplaneFooter } from '../utils/backplane';

import '../styles/base.scss';

export interface ContentAppProps extends AppProps<any> {
  pageProps: {
    ssrReduxState: any;
  };
  backplaneServerReduxState: BackplaneState;
  router: Router;
}

const ContentApp = ({
  Component,
  pageProps,
  backplaneServerReduxState,
  router,
}: ContentAppProps) => {
  const [backplaneReady, setBackplaneReady] = useState(false);
  const { ssrReduxState } = pageProps;
  const showFooter = showBackplaneFooter(router.asPath);
  const win = getWindow();

  const state = useRef({
    ...ssrReduxState,
    // eslint-disable-next-line no-underscore-dangle
    backplane: win?.__BACKPLANE_REDUX__?.backplane || backplaneServerReduxState?.backplane,
  });

  const store = useStore(state.current);

  // We want to keep lookng for the backplane script to have properly executed, at which point we can
  // proceed with hydrating. We can't reliably know when it will finish since order of execution is
  // not consistent
  const [loopStop] = useRafLoop(() => {
    if (win?.wavetrakBackplaneMount) {
      setBackplaneReady(true);
    }
  }, !!win);

  useEffect(
    () => {
      function handleClick() {
        const mobileNavMenu = document?.getElementsByClassName(
          'quiver-new-navigation-bar__mobile__nav__menu',
        );
        const quiverPageContainer: any = document?.getElementsByClassName('quiver-page-container');
        quiverPageContainer[0].style.position = '';
        quiverPageContainer[0].style.zIndex = '';
        const className = document?.activeElement?.className;
        if (className === 'quiver-navigation-item__container__link' && mobileNavMenu.length > 0) {
          quiverPageContainer[0].style.position = 'fixed';
          quiverPageContainer[0].style.zIndex = '99999';
        }
      }
      if (backplaneReady) {
        loopStop();
        instantiateClientSideBackplane(
          store,
          () => store.dispatch(splitReady()),
          () => store.dispatch(analyticsReady()),
        );
      }
      window.addEventListener('click', handleClick);
      return () => window.removeEventListener('click', handleClick);
    },
    // We only care about when backplane is ready here and this only needs to be called once.
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [backplaneReady],
  );

  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="Content-Language" content="en" />
        <meta
          name="viewport"
          content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
        />
      </Head>
      <Scripts showFooter={showFooter} />
      <Provider store={store}>
        <PageContainer>
          <Component {...pageProps} />
        </PageContainer>
      </Provider>
    </>
  );
};

export default ContentApp;
