/* istanbul ignore file */
import type { ErrorProps } from 'next/error';
import { NextPage } from 'next';

import { getStatusCode } from '../selectors/status';
import { getOrCreateStore } from '../stores';
import ErrorPage from '../containers/ErrorPage';
import { INTERNAL_SERVER_ERROR } from '../actions/status';

const Error: NextPage<ErrorProps> = ({ statusCode }) => <ErrorPage statusCode={statusCode} />;

Error.getInitialProps = async (ctx) => {
  const store = getOrCreateStore();
  const isServer = !!ctx.req;

  if (ctx.err) {
    store.dispatch({
      type: INTERNAL_SERVER_ERROR,
      message: ctx.err.message,
    });
  }

  const statusCode = ctx.res?.statusCode || getStatusCode(store.getState());

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
      statusCode,
    };
  }

  return {
    statusCode,
  };
};

export default Error;
