import cookie from 'cookie';
import { NextPage } from 'next';

import Search from '../../containers/Search';
import { fetchSearchResults } from '../../actions/search';
import { getOrCreateStore } from '../../stores';

const SearchPage: NextPage = () => <Search />;

SearchPage.getInitialProps = async (context) => {
  const { dispatch, getState } = getOrCreateStore();

  const cookies = cookie.parse(context.req?.headers?.cookie || '');

  const { term } = context.query;

  const isServer = !!context.req;

  await dispatch(fetchSearchResults(term?.toString() || '', cookies));

  if (isServer) {
    return {
      ssrReduxState: getState(),
    };
  }

  return {};
};

export default SearchPage;
