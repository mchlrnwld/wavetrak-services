import React from 'react';
import ReactDOM from 'react-dom';
import EmbedContainer from 'react-oembed-container';
import {
  fetchOembed,
  trackEvent,
  trackClickedSubscribeCTA,
  doExternalFetch,
} from '@surfline/web-common';
import { AddToCalendar, CamPlayer, GoogleDFP, PremiumCamCTA } from '@surfline/quiver-react';
import { fetchReport } from '../common/api/spot';
import PremiumVideoCTA from '../components/PremiumVideoCTA';
import loadAdConfig from './adConfig';
import { fetchCamEmbedDetails } from '../common/api/cameras';
import config from '../config';
import doJSFetch from '../common/baseFetch';

const swellEventArticleButton =
  "<div class='sl-swell-event__article__image__button__wrapper'><div class='sl-swell-event__article__image__button'><svg xmlns='http://www.w3.org/2000/svg' class='jw-svg-icon jw-svg-icon-fullscreen-on' viewBox='0 0 240 240' focusable='false'><path d='M96.3,186.1c1.9,1.9,1.3,4-1.4,4.4l-50.6,8.4c-1.8,0.5-3.7-0.6-4.2-2.4c-0.2-0.6-0.2-1.2,0-1.7l8.4-50.6c0.4-2.7,2.4-3.4,4.4-1.4l14.5,14.5l28.2-28.2l14.3,14.3l-28.2,28.2L96.3,186.1z M195.8,39.1l-50.6,8.4c-2.7,0.4-3.4,2.4-1.4,4.4l14.5,14.5l-28.2,28.2l14.3,14.3l28.2-28.2l14.5,14.5c1.9,1.9,4,1.3,4.4-1.4l8.4-50.6c0.5-1.8-0.6-3.6-2.4-4.2C197,39,196.4,39,195.8,39.1L195.8,39.1z'></path></svg></div></div>";

const getSkinPlayer = async (jwVideo, jwPlayer) => {
  try {
    const embedURL = `https://cdn.jwplayer.com/players/${jwVideo}-${jwPlayer}.js`;
    const data = await doJSFetch(embedURL);
    const result = data.substring(
      data.indexOf('var jwConfig = {'),
      data.indexOf('; // end config'),
    );
    const playerConfig = JSON.parse(result.replace('var jwConfig = ', ''));
    return playerConfig || {};
  } catch (error) {
    return {};
  }
};

const renderAd = (element, adNumber, adIdentifier, dfpKeyword, qaFlag) => {
  element.insertAdjacentHTML('afterend', `<div id="ad-${adNumber}" />`);
  ReactDOM.render(
    <GoogleDFP
      adConfig={loadAdConfig(adIdentifier, [
        ['qaFlag', qaFlag],
        ['contentKeyword', dfpKeyword],
      ])}
      isHtl
      isTesting={config.htl.isTesting}
    />,
    document.getElementById(`ad-${adNumber}`),
  );
};

export const renderArticleAds = (articleRef, children, dfpKeyword, createdAt, qaFlag) => {
  const adTracker = { count: 0, placed: 0 };
  children.forEach((node, index) => {
    if (
      node.tagName === 'P' ||
      node.tagName === 'IMG' ||
      node.className.indexOf('video') > -1 ||
      node.className.indexOf('img') > -1
    ) {
      adTracker.count += 1;
    }
    if (adTracker.count === 4 && adTracker.placed < 3) {
      const adNumber = adTracker.placed + 1;
      const adIdentifier =
        adNumber === 1 && new Date(createdAt) < new Date('2019-04-29 00:00:00')
          ? 'legacyEditContent'
          : `editBody${adNumber}`;
      renderAd(articleRef.current.children[index], adNumber, adIdentifier, dfpKeyword, qaFlag);
      adTracker.count = -1;
      adTracker.placed += 1;
    } else if (index === children.length - 1 && adTracker.placed === 0) {
      const adNumber = adTracker.placed + 1;
      renderAd(
        articleRef.current.children[index],
        adNumber,
        `editBody${adNumber}`,
        dfpKeyword,
        qaFlag,
      );
    }
  });
};

export const replaceWithClickableImage = (onClick, getImages) => {
  const imgElements = document.querySelectorAll(
    '.sl-travel-page-container p > img, .wp-caption > img',
  );
  const images = [];
  if (imgElements.length) {
    [...imgElements].map((img, index) => {
      /* eslint-disable-next-line no-param-reassign */
      img.onclick = () => onClick(index);
      img.insertAdjacentHTML('afterend', swellEventArticleButton);
      images.push({ src: img.src, w: img.clientWidth, h: img.clientHeight, index });
      return img;
    });
  }
  getImages(images);
  return null;
};

export const renderResizedImages = () => {
  const imgElements = document.getElementsByTagName('img');
  if (imgElements.length) {
    [...imgElements].map((img) => {
      if (img.src.indexOf('/wp-content/uploads') > -1) {
        /* eslint-disable-next-line no-param-reassign */
        img.src = config.cloudflareImageResizingUrl(img.src);
        return img;
      }
      return img;
    });
  }
};

export const renderIgElements = () => {
  const igElements = document.querySelectorAll('[data-sl-ig]');
  if (igElements.length) {
    [...igElements].map(async (ig) => {
      const {
        oembedObj: { html },
      } = await fetchOembed(ig.dataset.slIg, '/travel/oembed');
      ReactDOM.render(
        <EmbedContainer markup={html}>
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </EmbedContainer>,
        ig,
      );
    });
  }
};

export const renderTwitterElements = () => {
  const twitterElements = document.getElementsByClassName('twitter-tweet');
  if (twitterElements.length) {
    [...twitterElements].map(async (tweetElement) => {
      const anchorTags = tweetElement.getElementsByTagName('a');
      const tweetUrl = anchorTags[anchorTags.length - 1].href;
      const {
        oembedObj: { html },
      } = await fetchOembed(tweetUrl, '/travel/oembed');
      ReactDOM.render(
        <EmbedContainer markup={html}>
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </EmbedContainer>,
        tweetElement,
      );
    });
  }
};

export const renderFeaturedVideo = (jwId) => {
  const featuredVideoElements = document.getElementsByClassName('sl-swell-event-featured-media');
  if (featuredVideoElements.length) {
    [...featuredVideoElements].map(async (jw) => {
      if (window && window.jwplayer && jwId) {
        window
          .jwplayer(jw.id)
          .setup({ playlist: `https://content.jwplatform.com/feeds/${jwId}.json`, ph: 2 });
      }
    });
  }
};

export const renderJWElements = () => {
  const jwElements = document.querySelectorAll("[id*='jw']");
  if (jwElements.length) {
    [...jwElements].map(async (jw) => {
      const [, jwId, jwPlayer] = jw.id.split('_');
      const playerConfig = await getSkinPlayer(jwId, jwPlayer);
      if (window && window.jwplayer && jwId) {
        window.jwplayer(jw.id).setup(playerConfig);
      }
    });
  }
};

export const renderVideoPaywalls = (onClickVideoPaywall, onClickSignIn, shouldPaywall) => {
  const videoPaywallElement = document.getElementById('sl-premium-video-cta');
  const videoPaywallCaptionElements = document.getElementsByClassName('sl-premium-video-text');

  if (videoPaywallElement && shouldPaywall) {
    ReactDOM.render(
      <PremiumVideoCTA
        onClickVideoPaywall={onClickVideoPaywall}
        imageUrl={videoPaywallElement.getAttribute('data-image-url')}
        onClickSignIn={onClickSignIn}
        videoText={
          videoPaywallCaptionElements.length ? videoPaywallCaptionElements[0].innerHTML : ''
        }
      />,
      videoPaywallElement,
    );
  }
};

const createCameraObject = (cameraDetails) => ({
  streamUrl: cameraDetails.cam.streamUrl,
  stillUrl: cameraDetails.cam.stillUrl,
  status: {
    isDown: !!cameraDetails.cam.isDown.status,
    message: cameraDetails.cam.isDown.message,
    subMessage: cameraDetails.cam.isDown.subMessage,
  },
  isPrerecorded: false,
  isPremium: cameraDetails.cam.isPremium,
  title: 'title',
});

export const renderCameraEmbeds = (isPremium, articleId, articleTitle, category) => {
  const cameraEmbeds = document.getElementsByClassName('wp-cam-embed');
  if (cameraEmbeds.length) {
    [...cameraEmbeds].map(async (embedElement) => {
      const cameraId = embedElement.getAttribute('data-sl-cam-id');
      const detailsJson = await fetchCamEmbedDetails(cameraId);
      const spotReport = await fetchReport(detailsJson.spot.id);
      const camera = createCameraObject(detailsJson);

      const camStillUrl = camera.stillUrl;
      const isPremiumCam = camera.isPremium;
      const showPremiumCamPaywall = isPremiumCam && !isPremium;

      const segmentProperties = {
        location: 'premium only cam - cam embed',
        category,
        title: articleTitle,
        contentId: articleId.toString(),
        spotId: detailsJson.spot.id,
        spotName: detailsJson.spot.name,
      };
      if (showPremiumCamPaywall) {
        ReactDOM.render(
          <div className="video-wrap-container">
            <div className="video-wrap">
              <PremiumCamCTA
                camStillUrl={camStillUrl}
                href={`${config.surflineHost}${config.funnelUrl}`}
                onClickPaywall={() => trackClickedSubscribeCTA(segmentProperties)}
              />
            </div>
          </div>,
          embedElement,
        );
      } else {
        ReactDOM.render(
          <div className="video-wrap-container">
            <div className="video-wrap">
              <CamPlayer
                aspectRatio="16:9"
                adTag={{ iu: '/1024858/SL_Editorial_Embed_PreRoll' }}
                advertisingIds={spotReport.associated.advertising}
                playerId={cameraId || 'sl-editorial-embed'}
                camera={camera}
                spotName={detailsJson.spot.name}
                spotId={detailsJson.spot.id}
                isPremium={isPremium}
                isMultiCam={false}
              />
            </div>
          </div>,
          embedElement,
        );
      }
    });
  }
};

const onClickAddToCalendar = (trackData) => {
  trackEvent('Clicked Link', trackData);
};

export const renderCalendarEvents = (articleId) => {
  const eventElements = document.getElementsByClassName('cal-event');
  if (eventElements.length) {
    [...eventElements].map(async (event) => {
      try {
        const spotReport = await fetchReport(event.querySelector('#cal-event-spot').innerHTML);
        const {
          spot: { name: spotName },
          associated: { href: spotUrl },
        } = spotReport;
        const startTime = event.querySelector('#cal-event-start').innerHTML.toString();
        const endTime = event.querySelector('#cal-event-end').innerHTML.toString();
        const eventObj = {
          title: `Surf ${spotName}`,
          description: `Check out the forecast for ${spotName} at ${spotUrl}`,
          location: spotName,
          startTime,
          endTime,
        };
        const trackData = {
          articleId,
          category: 'editorial',
          linkLocation: 'editorialContent',
          linkName: 'Add to Calendar',
          linkUrl: spotUrl,
        };
        ReactDOM.render(
          <AddToCalendar onClickAdd={() => onClickAddToCalendar(trackData)} event={eventObj} />,
          event,
        );
      } catch (err) {
        console.error(err);
      }
    });
  }
};

export const renderCalendarCinemaEvents = (articleId) => {
  const eventElements = document.getElementsByClassName('cal-event-cinema');
  if (eventElements.length) {
    [...eventElements].map(async (event) => {
      try {
        const startTime = event.querySelector('#cal-event-start').innerHTML.toString();
        const endTime = event.querySelector('#cal-event-end').innerHTML.toString();
        const description = event.querySelector('#cal-event-description').innerHTML.toString();
        const title = event.querySelector('#cal-event-title').innerHTML.toString();
        const link = event.querySelector('#cal-event-link').innerHTML.toString();
        const eventObj = {
          title,
          description: `${description}
          ${link}`,
          startTime,
          endTime,
        };
        const trackData = {
          articleId,
          category: 'editorial',
          linkLocation: 'editorialContent',
          linkName: 'Add to Calendar',
          linkUrl: link,
        };
        ReactDOM.render(
          <AddToCalendar onClickAdd={() => onClickAddToCalendar(trackData)} event={eventObj} />,
          event,
        );
      } catch (err) {
        console.error(err);
      }
    });
  }
};

export const renderSurfNetworkVideo = async () => {
  const surfNetworkContainerElement = document.getElementById('sl-surfnetwork-video');
  if (surfNetworkContainerElement) {
    const videoId = surfNetworkContainerElement.getAttribute('data-sl-video-id');
    if (videoId) {
      const surfNetworkToken = await doExternalFetch('/travel/surfnetwork');
      ReactDOM.render(
        <div itemScope itemType="https://schema.org/VideoObject">
          <div style={{ position: 'relative', overflow: 'hidden', paddingBottom: '56.25%' }}>
            <iframe
              src={`https://api.nodplatform.com/partner/surfline/watch/${videoId}.html?access_token=${surfNetworkToken}`}
              width="100%"
              height="100%"
              frameBorder="0"
              scrolling="auto"
              title="SurfNetwork"
              style={{ position: 'absolute' }}
              allowFullScreen
            />
          </div>
        </div>,
        surfNetworkContainerElement,
      );
    }
  }
};

export const trackArticleLinkClicks = (articleId) => {
  const articleBody = document.getElementById('sl-editorial-article-body');
  const articleLinks = articleBody.querySelectorAll('a');
  if (articleLinks.length) {
    [...articleLinks].map(async (ig, index) => {
      articleLinks[index].addEventListener(
        'click',
        (event) =>
          trackEvent('Clicked Link', {
            articleId,
            category: 'editorial',
            linkLocation: 'editorialContent',
            linkName: event.target.innerText || articleLinks[index].innerText,
            linkUrl: event.target.href || articleLinks[index].href,
          }),
        false,
      );
    });
  }
};

export const trackSwellEventClickLinks = (articleId) => {
  const articleBody = document.getElementById('sl-swell-event__content__feed');
  const articleLinks = articleBody.querySelectorAll('a');
  if (articleLinks.length) {
    [...articleLinks].map(async (ig, index) => {
      articleLinks[index].addEventListener(
        'click',
        (event) =>
          trackEvent('Clicked Link', {
            articleId,
            category: 'editorial',
            linkLocation: 'Swell Alert',
            linkName: event.target.innerText,
            linkUrl: event.target.href,
          }),
        false,
      );
    });
  }
};
