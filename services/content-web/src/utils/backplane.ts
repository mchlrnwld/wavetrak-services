import {
  canUseDOM,
  getWindow,
  createSplitFilter,
  setWavetrakSplitFilters,
  getWavetrakSplitFilters,
  setWavetrakFeatureFrameworkCB,
  BackplaneState,
} from '@surfline/web-common';
import { once } from 'lodash';

import { WavetrakStore } from '../stores';
import * as treatments from '../common/treatments';

export interface BackplaneResponse {
  associated: {
    vendor: {
      js: string;
    };
  };
  data: {
    api: {
      geo: {
        countryCode: string;
        ip: string;
        location: {
          latitude: number;
          longitude: number;
        };
      };
      renderOptions: Record<string, string | boolean>;
    };
    components: {
      footer: string;
      header: string;
      regwall: string;
    };
    css: string;
    js: string;
    redux: BackplaneState;
  };
}

interface BackplaneRenderOptions {
  'renderOptions[bannerAd]'?: string;
  'renderOptions[largeFavoritesBar]'?: string;
  'renderOptions[qaFlag]'?: string;
  'renderOptions[showAdBlock]'?: string;
}

export const instantiateClientSideBackplane = (
  store: WavetrakStore,
  setSplitReady: () => void,
  setAnalyticsReady: () => void,
) => {
  if (canUseDOM) {
    const win = getWindow();
    win.wavetrakStore = store;

    // Get the list on enabled splits and create the list of treatment names to load
    const splitFilters = createSplitFilter(treatments);

    // Set the treatment names on the window so that backplane can access them
    setWavetrakSplitFilters([...splitFilters, ...getWavetrakSplitFilters()]);

    setWavetrakFeatureFrameworkCB(
      once(() => {
        setSplitReady();
        setAnalyticsReady();
      }),
    );

    win.wavetrakBackplaneMount?.();
  }
};

export const getBackplaneRenderOpts = (
  pathname: string,
  query: { native?: string; qaFlag?: string },
) => {
  if (pathname.indexOf('/contests/spirit-of-surfing/cuervo-challenge') > -1) {
    return { 'renderOptions[bannerAd]': 'contest_sos' };
  }

  if (pathname.indexOf('/contests/wave-of-the-winter/north-shore/2020-2021') > -1) {
    return { 'renderOptions[bannerAd]': 'wotw_2021' };
  }

  if (pathname.indexOf('/contests/wave-of-the-winter/regional/2020-2021') > -1) {
    return { 'renderOptions[bannerAd]': 'wotw_regional_2021' };
  }

  if (
    pathname.indexOf('/premium-benefits') > -1 ||
    pathname.indexOf('/contests') > -1 ||
    pathname.indexOf('/surf-cams') > -1
  ) {
    return {
      'renderOptions[bannerAd]': false,
    };
  }

  if (pathname.indexOf('/search') > -1) {
    let searchRenderOpts: BackplaneRenderOptions = {
      'renderOptions[bannerAd]': 'search_results',
    };
    if (query.qaFlag) {
      searchRenderOpts = { ...searchRenderOpts, 'renderOptions[qaFlag]': query.qaFlag };
    }
    return searchRenderOpts;
  }

  let defaultRenderOpts: BackplaneRenderOptions = {
    'renderOptions[largeFavoritesBar]': 'true',
    'renderOptions[bannerAd]': 'homepage',
    'renderOptions[showAdBlock]': 'true',
  };

  if (query.qaFlag) {
    defaultRenderOpts = { ...defaultRenderOpts, 'renderOptions[qaFlag]': query.qaFlag };
  }

  return defaultRenderOpts;
};

export const showBackplaneFooter = (pathname: string) =>
  pathname.indexOf('/surf-cams') === -1 && pathname.indexOf('/contests') === -1;
