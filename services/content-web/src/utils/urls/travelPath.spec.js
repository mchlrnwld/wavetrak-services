import { geonameMapPath } from './travelPath';

describe('utils / geonamePath', () => {
  test('creates a path to the travel page from geonameId and slug', () => {
    expect(
      geonameMapPath({
        slug: 'united-states',
        geonameId: '6252001',
      }),
    ).toBe('travel/united-states-surfing-and-beaches/6252001');
  });
});
