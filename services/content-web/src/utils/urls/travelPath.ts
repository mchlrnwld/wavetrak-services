export const travelPaths = {
  base: 'travel',
};

export const geonameMapPath = ({ slug, geonameId }: { slug: string; geonameId: string }) =>
  `${travelPaths.base}/${slug}-surfing-and-beaches/${geonameId}`;
