export const travelPaths = {
  base: 'travel',
};

export const geonameMapPath = ({ slug, geonameId }) =>
  `${travelPaths.base}/${slug}-surfing-and-beaches/${geonameId}`;
