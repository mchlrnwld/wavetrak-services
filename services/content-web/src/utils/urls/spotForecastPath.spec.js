import spotForecastPath from './spotForecastPath';

describe('utils / spotForecastPath', () => {
  test('creates a path to the spot forecast page from spot id and slug', () => {
    expect(
      spotForecastPath({
        _id: '58581a836630e24c44878fd6',
        name: 'HB Pier, Southside',
      }),
    ).toBe('surf-report/hb-pier-southside/58581a836630e24c44878fd6');
  });
});
