import { getUserSettings, getUser } from '@surfline/web-common';
import type { AppState } from '../stores';

const getFeedGeotarget = (state: AppState) => {
  const user = getUser(state);
  let geotarget = user && user?.countryCode ? user?.countryCode : 'US';
  const settings = getUserSettings(state);
  if (settings && settings?.feed && settings?.feed?.localized) {
    const localized = settings?.feed?.localized;
    geotarget = localized !== 'LOCALIZED' ? localized : geotarget;
  }

  return geotarget;
};

export default getFeedGeotarget;
