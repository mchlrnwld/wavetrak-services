import type { AppState } from '../stores';

export const getStatusCode = (state: AppState) => state.status.code;

export const getRedirectDestination = (state: AppState) => state.status.dest;

export const getStatusLoading = (state: AppState) => state.status.loading;
