import type { AppState } from '../stores';

export const getSearchResults = (state: AppState) => state.search.results || [];

export const getSearchSections = (state: AppState) => state.search.sections || [];
