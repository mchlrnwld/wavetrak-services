import type { AppState } from '../stores';

export const getLocationView = (state: AppState) => state.locationView;

export const getUnits = (state: AppState) => {
  const data = state.locationView.associated;
  if (data) {
    return data.units;
  }
  return null;
};
