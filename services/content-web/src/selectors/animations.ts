import type { AppState } from '../stores';

const getScrollPosition = (component: string) => (state: AppState) =>
  state.animations.scrollPositions[component];

export const getTopSpotsScrollPosition = getScrollPosition('topSpots');

const animations = (state: AppState) => state.animations;

export default animations;
