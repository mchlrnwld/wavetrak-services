import type { AppState } from '../stores';

const getTopSpots = (state: AppState) => state.topSpots.topSpots;

export default getTopSpots;
