import type { AppState } from '../stores';

const getTaxonomyDetails = (state: AppState) => {
  const data = state.editorialTaxonomyDetails.details;
  if (data) {
    return data;
  }
  return null;
};

export default getTaxonomyDetails;
