import type { AppState } from '../stores';

const getTaxonomyPosts = (state: AppState) => {
  const data = state.editorialTaxonomyPosts;
  if (data) {
    return data;
  }
  return null;
};

export default getTaxonomyPosts;
