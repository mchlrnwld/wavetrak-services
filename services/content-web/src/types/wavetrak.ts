/* eslint-disable @next/next/no-document-import-in-page */

import type { Request, Response } from 'express';
import type { NextPageContext } from 'next';
import type { DocumentContext } from 'next/document';

interface WavetrakRequest extends Request {
  userId?: string;
  cookies: Record<string, string>;
}

export interface WavetrakPageContext extends NextPageContext {
  req?: WavetrakRequest;
  res: Response;
}

// Used for the _document page
export interface WavetrakContext extends DocumentContext {
  req: WavetrakRequest;
  res: Response;
}
