export interface Unit {
  tideHeight: string;
  waveHeight: string;
  windSpeed: string;
}

export type Units = Array<Unit>;
