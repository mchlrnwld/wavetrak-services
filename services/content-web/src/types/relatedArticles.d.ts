export type RelatedArticles = Array<{
  id: string;
  createdAt: number;
  externalLink: string;
  externalSource: string;
  updatedAt: number;
  content: {
    title: string;
  };
  media: {
    feed1x: string;
    feed2x: string;
    type: string;
  };
  newWindow: any; // TODO: Not sure type
  permalink: string;
  tags: Array<{
    name: string;
    url: string;
  }>;
}>;
