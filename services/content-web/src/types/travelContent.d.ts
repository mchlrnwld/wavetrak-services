import { RelatedArticles } from './relatedArticles';

export interface TravelContent {
  id: string;
  contentType: string;
  createdAt: number;
  updatedAt: number;
  content: {
    title: string;
    subtitle: string;
    summary: string;
    body: string;
  };
  media: {
    large: string;
    medium: string;
    type: string;
    thumbnail: string;
    full: string;
  };
  relatedPosts: RelatedArticles;
}
