export interface Spot {
  _id: string;
  legacyRegionId: number;
  name: string;
  cameras: Array<{
    _id: string;
    title: string;
    streamUrl: string;
    stillUrl: string;
    rewindBaseUrl: string;
    status: {
      isDown: boolean;
      message: sstring;
    };
  }>;
}

export type Spots = Array<Spot>;
