import { BackplaneState } from '@surfline/web-common';
// import { ContentAsyncReducers } from './src/reducers';
// import { WavetrakStore } from './src/stores';

declare global {
  interface Window {
    wavetrakNextApp?: string;
    wavetrakStore: any; // WavetrakStore
    wavetrakAsyncReducers?: any; // ContentAsyncReducers
    __BACKPLANE_REDUX__: BackplaneState;
    wavetrakBackplaneMount?: () => void;
    analytics: { user: {}; identify: (userId: string, { sourceOrigin: string }) => void };
  }
}

declare module '*.module.scss' {
  const classes: { [key: string]: string };
  export default classes;
}
