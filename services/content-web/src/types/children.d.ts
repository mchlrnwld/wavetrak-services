export type GeoChild = {
  name: string;
  media: {
    medium: string;
  };
  url: string;
};

export type GeoChildren = Array<GeoChild>;
