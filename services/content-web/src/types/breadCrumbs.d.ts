export type BreadCrumb = {
  name: string;
  url: string;
  geonameId: number;
  id: string;
  slug?: string;
};

export type BreadCrumbs = Array<BreadCrumb>;
