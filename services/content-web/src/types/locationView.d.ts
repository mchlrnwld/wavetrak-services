import { GeoChildren } from './children';
import { Taxonomy } from './taxonomy';
import { TravelContent } from './travelContent';
import { BreadCrumbs } from './breadCrumbs';

export interface LocationView {
  travelContent: TravelContent;
  breadCrumbs: BreadCrumbs;
  loading: boolean;
  children: GeoChildren;
  url: string;
  taxonomy: Taxonomy;
  conditions: {
    human: boolean;
    value: string;
  };
  boundingBox: {
    north: number;
    south: number;
    east: number;
    west: number;
  };
  wind: {
    speed: number;
    direction: number;
  };
  waveHeight: {
    min: number;
    max: number;
    plus: boolean;
  };
  tide: {
    current: {
      type: string;
      height: number;
      timestamp: number;
      utcOffset: number;
    };
    next: {
      type: string;
      height: number;
      timestamp: number;
      utcOffset: number;
    };
  };
  thumbnail: string;
  rank: Array<{ 0: number; 1: number }>;
}
