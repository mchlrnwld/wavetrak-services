import { TagDescription } from '@reduxjs/toolkit/dist/query/endpointDefinitions';
import { Meta } from './yoast';

export type Premium = {
  premium: boolean;
  paywallHeading: string;
  paywallDescription: string;
  teaser: string;
  paywallPosts: Array<{
    id: string;
    title: string;
    subtitle: string;
    permalink: string;
    premium: boolean;
    createdAt: number;
    categories: Array<Taxonomy>;
    media: Media;
    series: Array<Taxonomy>;
  }>;
};

export type Media = {
  type: string;
  feed1x: string;
  feed2x: string;
};

export type Taxonomy = {
  name: string;
  slug: string;
  link: string;
  taxonomy: string;
};

export type Tags =
  | Array<{
      name: string;
      url: string;
    }>
  | [];

export type Article = {
  id: number;
  contentType: string;
  createdAt: Date;
  updatedAt: Date;
  permalink: string;
  series: Array<{ url: string; name: string }>; // TODO: find full shape
  sponsoredArticle: {
    dfpKeyword: string;
    sponsoredArticle: boolean;
    name: string;
    partnerContent: any;
  }; // TODO: find full shape
  displayOptions: { name: string; hideAds: boolean; hideAuthor: boolean; hideComments: boolean }; // TODO: find full shape
  promotions: Array<{ name: string }>; // TODO: find full shape
  categories: Array<{ url: string; name: string }>; // TODO: find full shape
  contentPillar: Array<{ name: string }>; // TODO: find full shape
  relatedPosts: Array<{
    id: string;
    createdAt: number;
    series: Array<any>;
    media: {
      feed2x: any;
    };
    categories: Array<any>;
    premium: boolean;
    title: string;
    subtitle: string;
    permalink: string;
  }>;
  hero: {
    // TODO: find actual shape - this is not confirmed
    align: {
      horizontal: string;
      vertical: string;
    };
    showHero: boolean;
    imageUrl: string;
    gradient: string;
    hideTitle: boolean;
    credit: any; // TODO: find actual shape
    location: any; // TODO: find actual shape
    type: any; // TODO: find actual shape
  };
  promoted: Array<string>;
  premium: Premium;
  content: {
    title: string;
    displayTitle: string;
    subtitle: string;
    body: string;
  };
  media: {
    type: string;
    feed1x: string;
    feed2x: string;
    promobox1x?: string;
    promobox2x?: string;
  };
  author: {
    iconUrl: string;
    name: string;
  };
  tags: Tags;
  yoastMeta: Meta;
};
