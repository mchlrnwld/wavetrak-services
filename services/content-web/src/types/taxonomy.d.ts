export interface Taxonomy {
  name: string;
  associated: {
    links: Array<{
      key: string;
      href: string;
    }>;
  };
}
