import baseFetch from '../baseFetch';

export const fetchSearchResults = async (term: string, cookies: { [key: string]: string }) =>
  baseFetch(`/search/site?q=${term}&querySize=50&suggestionSize=50&newsSearch=true`, { cookies });

export default fetchSearchResults;
