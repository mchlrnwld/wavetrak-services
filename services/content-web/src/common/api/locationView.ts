import baseFetch from '../baseFetch';

// eslint-disable-next-line
export const fetchLocationViewTravel = async (geonameId: string | string[] |  undefined, cookies: any) =>
  baseFetch(`/location/view?type=travel&id=${geonameId}`, { cookies });

// eslint-disable-next-line import/prefer-default-export
export const fetchLocationViewWorld = (cookies: any) =>
  baseFetch('/location/view/worldtaxonomy?', { cookies });
