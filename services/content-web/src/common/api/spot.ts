import baseFetch, { createParamString } from '../baseFetch';

export const fetchRegionalConditionForecast = (subregionId: string, days: string) =>
  baseFetch(`/kbyg/regions/forecasts/conditions?${createParamString({ subregionId, days })}`);

export const fetchRegionalOverview = (subregionId: string) =>
  baseFetch(`/kbyg/regions/overview?${createParamString({ subregionId })}`);

export const fetchNearestSpot = (lat: string, lon: string) =>
  baseFetch(`/kbyg/mapview/spot?${createParamString({ lat, lon })}`);

export const fetchBatchSubregions = (subregionIds: string) =>
  baseFetch(`/subregions/batch?${createParamString({ subregionIds })}`);

export const fetchReport = (spotId: string) => baseFetch(`/kbyg/spots/reports?spotId=${spotId}`);
