import baseFetch from '../baseFetch';

// eslint-disable-next-line
export const getTopSpots = async (geonameId: string | string[] | undefined, cookies: any) =>
  baseFetch(`/kbyg/spots/topspots?id=${geonameId}&type=geoname`, { cookies });
