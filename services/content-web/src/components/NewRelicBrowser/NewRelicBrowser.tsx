/* istanbul ignore file */
/* eslint-disable react/no-danger */

import React from 'react';
import newrelic from 'newrelic';

const NewRelicBrowser: React.FC = () => (
  <div
    className="newrelic"
    dangerouslySetInnerHTML={{ __html: newrelic.getBrowserTimingHeader() }}
  />
);

export default NewRelicBrowser;
