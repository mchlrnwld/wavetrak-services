import React from 'react';
// @ts-ignore
import Sticky from 'react-stickynode';
import { GoogleDFP } from '@surfline/quiver-react';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

type Props = {
  activeClass: string;
  className: string;
  isDesktop: boolean;
  qaFlag: string | null;
  adUnit: string;
  bottomBoundary: string;
  dfpKeyword: string;
};

const StickyAdUnit: React.FC<Props> = ({
  isDesktop = false,
  qaFlag = null,
  adUnit = null,
  bottomBoundary = '.quiver-page-rail--right',
  dfpKeyword = null,
  className = '',
  activeClass = '',
}) => {
  const feedAdUnit = (
    <div className="sl-sticky-ad-unit">
      <GoogleDFP
        adConfig={loadAdConfig(adUnit, [
          ['qaFlag', qaFlag],
          ['contentKeyword', dfpKeyword],
        ])}
        isHtl
        isTesting={config.htl.isTesting}
      />
    </div>
  );
  return isDesktop ? (
    <Sticky activeClass={activeClass} className={className} bottomBoundary={bottomBoundary}>
      {feedAdUnit}
    </Sticky>
  ) : (
    feedAdUnit
  );
};

export default StickyAdUnit;
