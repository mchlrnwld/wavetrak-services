import React from 'react';
import { Button as QuiverButton } from '@surfline/quiver-react';
// @ts-ignore
import productCDN from '@surfline/quiver-assets';
import { Taxonomy } from '../../types/taxonomy';

const TravelPageMap: React.FC<{ taxonomy: Taxonomy }> = ({ taxonomy }) => {
  const { name, associated } = taxonomy;
  const mapBackground = `${productCDN}/backgrounds/one-world-map.png`;

  return (
    <div className="sl-travel-map" style={{ backgroundImage: `url(${mapBackground})` }}>
      <div className="sl-travel-map__overlay">
        {associated &&
          associated.links &&
          associated.links.map((link) => {
            if (link.key === 'www') {
              return (
                <a href={link.href} className="sl-button-link" key={link.key}>
                  <h3>{name} Beaches Surf Reports and Forecasts</h3>
                  <QuiverButton>Launch Map View</QuiverButton>
                </a>
              );
            }
            return null;
          })}
      </div>
    </div>
  );
};

export default TravelPageMap;
