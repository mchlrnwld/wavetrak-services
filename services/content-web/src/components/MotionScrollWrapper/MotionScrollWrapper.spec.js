import React from 'react';
import { render, screen } from '@testing-library/react';
/* standard react-motion is not compatible with react17 this is a new package for support */
/* https://github.com/chenglou/react-motion/issues/604#issuecomment-770198710 */
import MotionScrollWrapper from './MotionScrollWrapper';

describe('components / MotionScrollWrapper', () => {
  const text = 'This should render inside of MotionScrollWrapper.';

  test('renders children even if disabled', () => {
    render(
      <MotionScrollWrapper scrollPosition={{ left: 20, top: 30 }} disabled>
        <div data-testid="children" className="children">
          {text}
        </div>
      </MotionScrollWrapper>,
    );

    const childrenWrapper = screen.getByText(text);
    expect(childrenWrapper).toBeInTheDocument();
  });
});
