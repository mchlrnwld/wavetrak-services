import React from 'react';
/* standard react-motion is not compatible with react17 this is a new package for support */
/* https://github.com/chenglou/react-motion/issues/604#issuecomment-770198710 */
// @ts-ignore
import { Motion, spring } from '@serprex/react-motion';
import { ScrollPosition } from '../../types/scrollPosition';

type Props = {
  scrollPosition: ScrollPosition;
  children: React.ReactElement;
  disable?: boolean;
  onRest?: (...args: any) => void;
};

const MotionScrollWrapper: React.FC<Props> = ({
  children,
  scrollPosition,
  disable = false,
  onRest = () => null,
}) => {
  const { top, left } = scrollPosition;
  const style = {
    motionScrollTop: spring(top, { stiffness: 400, damping: 50 }),
    motionScrollLeft: spring(left, { stiffness: 400, damping: 50 }),
  };
  return (
    <div className="sl-motion-scroll-wrapper">
      <Motion style={style} onRest={onRest}>
        {(interpolatedStypes: any) =>
          disable
            ? children
            : React.cloneElement(children, {
                scrollTop: interpolatedStypes.motionScrollTop,
                scrollLeft: interpolatedStypes.motionScrollLeft,
                forceScrolling: true,
              })
        }
      </Motion>
    </div>
  );
};

export default MotionScrollWrapper;
