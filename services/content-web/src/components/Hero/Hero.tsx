import React from 'react';
import { Link } from 'react-scroll';
import { get as _get } from 'lodash';
import { BreadCrumbs } from '../../types/breadCrumbs';
import { TravelContent } from '../../types/travelContent';
import { Taxonomy } from '../../types/taxonomy';
import resizeImage from '../../utils/resizeImage';
import Breadcrumb from '../Breadcrumb';
import config from '../../config';

type Props = {
  travelContent: TravelContent;
  taxonomy: Taxonomy;
  breadCrumbs: BreadCrumbs;
  imageResizing: boolean;
};

const Hero: React.FC<Props> = ({ travelContent = null, taxonomy, breadCrumbs, imageResizing }) => {
  const getBackgroundImage = () => {
    if (_get(travelContent, 'media.large', null)) {
      return travelContent?.media.large;
    }
    return config.defaultImageUrl;
  };
  return (
    <div
      className="sl-hero"
      style={{
        backgroundImage: `url("${resizeImage(getBackgroundImage(), imageResizing)}")`,
      }}
    >
      <div className="sl-hero__content">
        <Breadcrumb breadCrumbs={breadCrumbs.slice(0, -1)} baseUrl="/travel" />
        <h1>{taxonomy.name} Travel &amp; Surf Guide</h1>
        <div className="sl-hero__content__subtitle">
          <h2 className="sl-hero__content__subtitle__h2">
            {_get(travelContent, 'content.subtitle', null)
              ? travelContent?.content.subtitle
              : 'Get the best tips on local beaches, travel seasons and surf spots'}
          </h2>
        </div>
      </div>
      <Link className="sl-hero__anchor" to="sl-travel-anchor" duration={300} smooth>
        <div className="sl-hero__anchor__icon" />
      </Link>
    </div>
  );
};

export default Hero;
