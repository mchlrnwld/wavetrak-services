import React, { createRef } from 'react';
import ReactDOMServer from 'react-dom/server';
// @ts-ignore This library does not have solid types and no @types/html-to-react exits
import { Parser } from 'html-to-react';
import { ShareArticle } from '@surfline/quiver-react';
import { canUseDOM, trackClickedSubscribeCTA } from '@surfline/web-common';
import articleProcessingInstructions from './helpers';
import {
  renderArticleAds,
  renderIgElements,
  renderJWElements,
  trackArticleLinkClicks,
  renderVideoPaywalls,
  renderCalendarEvents,
  renderCalendarCinemaEvents,
  renderCameraEmbeds,
  renderTwitterElements,
  renderSurfNetworkVideo,
  renderResizedImages,
} from '../../utils/renderCustomComponents';
import config from '../../config';
import EditorialPaywall from '../EditorialPaywall';
import { Article } from '../../types/editorial';

type Props = {
  article: Article;
  imageResizing: boolean;
  isPremium?: boolean;
  native?: boolean;
  onClickLink: (...args: any) => void;
  qaFlag?: string;
};

type State = {};

class EditorialArticleBody extends React.Component<Props, State> {
  private articleRef = createRef<HTMLDivElement>();

  private htmlToReactParser = new Parser();

  componentDidMount() {
    const { article, qaFlag = null, isPremium = false, native = false, imageResizing } = this.props;

    const {
      id,
      createdAt,
      sponsoredArticle: { dfpKeyword },
      displayOptions: { hideAds },
      premium: { premium },
      content: { title },
    } = article;

    const children = [...(this.articleRef?.current?.children as any)];

    if (!hideAds && (dfpKeyword || !isPremium) && !premium) {
      renderArticleAds(this.articleRef, children, dfpKeyword, createdAt, qaFlag);
    }
    renderIgElements();
    renderTwitterElements();
    renderJWElements();
    trackArticleLinkClicks(id);

    // Render video paywalls if premium conditions are met and split is on
    const paywall = premium && !isPremium && !native;
    renderVideoPaywalls(this.doTrackVideoPaywallClicks, this.onClickPaywallSignIn, paywall);
    renderCalendarEvents(id);
    renderCalendarCinemaEvents(id);
    renderCameraEmbeds(isPremium, id, title, 'editorial');
    renderSurfNetworkVideo();
    if (imageResizing) renderResizedImages();
  }

  // eslint-disable-next-line class-methods-use-this
  private isValidNode = () => true;

  // eslint-disable-next-line class-methods-use-this
  onClickPaywallSignIn = () => {
    // TODO: what is this calling? This is not in scope
    sessionStorage.setItem('redirectUrl', window.location.href);
  };

  doTrackVideoPaywallClicks = () => {
    const { article } = this.props;

    const tags: string[] = [];
    article.tags.forEach((tag) => tags.push(tag.name));
    trackClickedSubscribeCTA({
      location: 'Premium Article - Video Paywall',
      title: article.content.title,
      category: 'editorial',
      contentType: 'Editorial',
      contentId: article.id.toString(),
      tags,
      mediaType: article.media.type,
    });
    if (canUseDOM) {
      setTimeout(() => window.location.assign(`${config.surflineHost}${config.funnelUrl}`), 300);
    }
  };

  render() {
    const { article, isPremium = false, onClickLink, native = false } = this.props;
    const paywalled = article.premium.premium && !isPremium && !native;
    const bodyContent = paywalled ? article.premium.teaser : article.content.body;

    return (
      <div className={paywalled ? 'paywall' : undefined}>
        <div className="sl-editorial-article-body__container">
          <div
            ref={this.articleRef}
            id="sl-editorial-article-body"
            className="sl-editorial-article-body"
            /* eslint-disable-next-line react/no-danger */
            dangerouslySetInnerHTML={{
              __html: ReactDOMServer.renderToStaticMarkup(
                this.htmlToReactParser.parseWithInstructions(
                  bodyContent,
                  this.isValidNode,
                  articleProcessingInstructions(isPremium),
                ),
              ),
            }}
          />
          {paywalled ? <div className="sl-editorial-paywall__fade" /> : null}
        </div>
        {!paywalled && article.tags.length ? (
          <ul className="sl-article-tags">
            {article.tags.map((tag) => (
              <li key={tag.name}>
                <a href={tag.url}>{tag.name}</a>
              </li>
            ))}
          </ul>
        ) : null}
        {!paywalled ? (
          <div className="sl-editorial-article-bottom__social">
            <hr />
            <h6>Share this story</h6>
            <ShareArticle url={article.permalink} onClickLink={onClickLink} />
          </div>
        ) : (
          <EditorialPaywall article={article} premium={article.premium} />
        )}
      </div>
    );
  }
}

export default EditorialArticleBody;
