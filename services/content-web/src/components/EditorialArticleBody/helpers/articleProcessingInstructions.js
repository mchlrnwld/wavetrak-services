import React from 'react';
import HtmlToReact from 'html-to-react';
import config from '../../../config';

// p with img child or wp-caption class, unless parent has img-cover class
const shouldProcessImg = (node) =>
  (node.name &&
    node.name === 'p' &&
    node.children &&
    [...node.children].filter((el) => el.name === 'img').length) ||
  (node.attribs &&
    node.attribs.class &&
    node.attribs.class.indexOf('wp-caption-text') <= -1 &&
    node.attribs.class.indexOf('wp-caption') > -1 &&
    (!node.parent ||
      !node.parent.attribs.class ||
      node.parent.attribs.class.indexOf('img-cover') <= -1));

// p with iframe child
const shouldProcessIframe = (node) =>
  node.name &&
  node.name === 'p' &&
  node.children &&
  node.children.length &&
  [...node.children].filter((el) => el.name === 'iframe').length &&
  [...node.children].filter(
    (el) => el.attribs && el.attribs.src && !el.attribs.src.includes('soundcloud'),
  ).length;

// div with id including jwplayer
const shouldProcessJWPlayer = (node) =>
  node && node.attribs && node.attribs.id && node.attribs.id.indexOf('jwplayer') > -1;

// div with id including sl cam embed class
const shouldProcessCamEmbed = (node) =>
  node && node.attribs && node.attribs.class && node.attribs.class.indexOf('sl-cam-embed') > -1;

// ol and ul lists
const shouldProcessList = (node) => node && node.name && (node.name === 'ol' || node.name === 'ul');

const shouldProcessCTA = (node) =>
  node &&
  node.name &&
  node.attribs.class &&
  node.attribs.class.indexOf('forecast') !== -1 &&
  [...node.children].filter(
    (el) => el.attribs && el.attribs.href && el.attribs.href.indexOf(config.funnelUrl) !== -1,
  );

const processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React);
const articleProcessingInstructions = (isPremium) => [
  {
    shouldProcessNode: (node) => shouldProcessImg(node),
    processNode: (node, children) => <div className="sl-img-container">{children}</div>,
  },
  {
    shouldProcessNode: (node) => shouldProcessIframe(node),
    processNode: (node, children) => (
      <div className="video-wrap-container">
        <div className="video-wrap">{children}</div>
      </div>
    ),
  },
  {
    shouldProcessNode: (node) => shouldProcessJWPlayer(node),
    processNode: (node, children) => (
      <div className="video-wrap-container">
        <div className="sl-jw-video-wrap">
          {processNodeDefinitions.processDefaultNode(node, children)}
        </div>
      </div>
    ),
  },
  {
    shouldProcessNode: (node) => shouldProcessCamEmbed(node),
    processNode: (node, children) => (
      <div className="video-wrap-container">
        <div className="video-wrap">
          {processNodeDefinitions.processDefaultNode(node, children)}
        </div>
      </div>
    ),
  },
  {
    shouldProcessNode: (node) => shouldProcessList(node),
    processNode: (node, children) => (
      <div className="sl-list-container">
        {processNodeDefinitions.processDefaultNode(node, children)}
      </div>
    ),
  },
  {
    shouldProcessNode: (node) => shouldProcessCTA(node),
    processNode: (node, children) =>
      !isPremium ? (
        <div className="sl-editorial-article-cta">
          {processNodeDefinitions.processDefaultNode(node, children)}
        </div>
      ) : null,
  },
  {
    shouldProcessNode: () => true,
    processNode: processNodeDefinitions.processDefaultNode,
  },
];

export default articleProcessingInstructions;
