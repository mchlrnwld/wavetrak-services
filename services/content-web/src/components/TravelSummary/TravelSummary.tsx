import React from 'react';
import { TravelContent } from '../../types/travelContent';

const Summary: React.FC<{ travelContent: TravelContent }> = ({ travelContent }) => (
  <div
    className="sl-travel-summary"
    /* eslint-disable-next-line react/no-danger */
    dangerouslySetInnerHTML={{ __html: travelContent.content.body }}
  />
);

export default Summary;
