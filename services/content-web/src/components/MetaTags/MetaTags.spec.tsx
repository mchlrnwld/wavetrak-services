import { render } from '@testing-library/react';
import MetaTags from './MetaTags';
import config from '../../config';

jest.mock('next/head', () => ({
  __esModule: true,
  // eslint-disable-next-line react/jsx-no-useless-fragment
  default: ({ children }: any) => <>{children}</>,
}));

function getMeta(meta: string) {
  const metas = document.getElementsByTagName('meta');
  for (let index = 0; index < metas.length; index += 1) {
    if (metas[index].getAttribute('property') === meta) {
      return metas[index].getAttribute('content');
    }
    if (metas[index].getAttribute('name') === meta) {
      return metas[index].getAttribute('content');
    }
  }

  return '';
}

describe('components / MetaTags', () => {
  const title = 'Surfing Articles: Latest Surf News, Videos, & Photos at Surfline';
  const description = `
  Access Surfline's daily digest of the latest in surf journalism. Get breaking news in all things surf, featured stories, and our renowned surf forecast and science articles.`;
  const urlPath = 'surf-news';
  const image = 'https://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg';
  const url = `${config.homepageUrl}/${urlPath}`;
  const tags = [
    {
      name: 'Tag One',
      slug: 'tag-one',
      url: 'https://sandbox.surfline.com/tag/tag-one',
      taxonomy: 'post_tag',
    },
  ];

  it('renders MetaTags with appropriate props', () => {
    render(
      <MetaTags
        title={title}
        description={description}
        image={image}
        urlPath={urlPath}
        tags={tags}
        // @ts-ignore
        yoastMeta={null}
        permalink=""
      />,
      { container: document.head },
    );

    expect(document.title).toBe(`${title} - Surfline`);
    expect(getMeta('robots')).toHaveLength(16);
    expect(getMeta('description')).toBe(description);
    expect(getMeta('og:title')).toBe(title);
    expect(getMeta('og:url')).toBe(url);
    expect(getMeta('og:image')).toBe(image);
    expect(getMeta('og:description')).toBe(description);
    expect(getMeta('twitter:title')).toBe(title);
    expect(getMeta('twitter:url')).toBe(url);
    expect(getMeta('twitter:image')).toBe(image);
    expect(getMeta('twitter:description')).toBe(description);
    expect(getMeta('canonical')).toBe(url);
  });
});
