import Head from 'next/head';
import config from '../../config';
import { Meta } from '../../types/yoast';

type Props = {
  title: string;
  description?: string;
  urlPath?: string;
  image?: string;
  yoastMeta?: Meta;
  tags?: Array<{ name: string }>;
  permalink?: string;
};

const MetaTags: React.FC<Props> = ({
  title,
  description,
  urlPath,
  image,
  yoastMeta,
  tags,
  permalink,
}) => {
  const seoUrl = `${config.hostForSEO}/${urlPath}`;
  const url = permalink || `${config.homepageUrl}/${urlPath}`;
  const meta = [
    {
      name: 'description',
      content: yoastMeta ? yoastMeta['og:description'] : description,
    },
    {
      name: 'og:type',
      content: 'Article',
    },
    {
      name: 'og:site_name',
      content: yoastMeta ? yoastMeta['og:site_name'] : 'Surfline',
    },
    {
      property: 'og:title',
      content: yoastMeta ? yoastMeta['og:title'] : title,
    },
    {
      property: 'og:url',
      content: yoastMeta ? yoastMeta['og:url'] : url,
    },
    {
      property: 'og:image',
      content: yoastMeta ? yoastMeta['og:image'] : image,
    },
    {
      property: 'og:description',
      content: yoastMeta ? yoastMeta['og:description'] : description,
    },
    {
      property: 'og:updated_time',
      content: yoastMeta ? yoastMeta['og:updated_time'] : null,
    },
    {
      property: 'canonical',
      content: yoastMeta ? yoastMeta.canonical : url,
    },
    {
      property: 'article:section',
      content: yoastMeta ? yoastMeta['article:section'] : null,
    },
    {
      property: 'article:published_time',
      content: yoastMeta ? yoastMeta['article:published_time'] : null,
    },
    {
      property: 'article:updated_time',
      content: yoastMeta ? yoastMeta['article:updated_time'] : null,
    },
    {
      property: 'twitter:card',
      content: yoastMeta ? yoastMeta['twitter:card'] : null,
    },
    {
      property: 'twitter:title',
      content: yoastMeta ? yoastMeta['twitter:title'] : title,
    },
    {
      property: 'twitter:image',
      content: yoastMeta ? yoastMeta['twitter:image'] : image,
    },
    {
      property: 'twitter:description',
      content: yoastMeta ? yoastMeta['twitter:description'] : description,
    },
    {
      property: 'twitter:url',
      content: yoastMeta ? yoastMeta['twitter:url'] : url,
    },
  ];
  tags?.forEach((tag) => {
    meta.push({
      property: 'article:tag',
      content: tag.name,
    });
  });

  const robotsMeta = config.robots
    ? {
        name: 'robots',
        content: config.robots,
      }
    : null;
  if (robotsMeta) {
    meta.push(robotsMeta);
  }

  return (
    <Head>
      <title>{`${title} - Surfline`}</title>
      {meta.map((item) => {
        if (item.name) {
          return <meta name={item.name} content={item.content!} key={item.name} />;
        }
        if (item.property) {
          return <meta property={item.property} content={item.content!} key={item.property} />;
        }
        return null;
      })}
      <link rel="canonical" href={permalink || seoUrl} />
    </Head>
  );
};
export default MetaTags;
