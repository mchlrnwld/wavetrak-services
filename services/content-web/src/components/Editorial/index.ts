import PostsBlock from './PostsBlock';
import LoadMorePosts from './LoadMorePosts';

export { PostsBlock, LoadMorePosts };
