import { connect } from 'react-redux';
import { Waypoint } from 'react-waypoint';
import { Button } from '@surfline/quiver-react';
import type { AppState } from '../../stores';
import {
  fetchEditorialTaxonomyPosts,
  startInfiniteScrolling,
} from '../../actions/editorialTaxonomyPosts';

interface LoadMorePostsProps {
  doStartInfiniteScrolling: any;
  doFetchTaxonomyPosts: any;
  infiniteScrollingEnabled: any;
  term: string;
  taxonomy: string;
  subCategory: string;
}

const LoadMorePosts = ({
  infiniteScrollingEnabled,
  doFetchTaxonomyPosts,
  doStartInfiniteScrolling,
  taxonomy,
  term,
  subCategory,
}: LoadMorePostsProps) => (
  <div className="sl-load-more-posts" data-test="load-more">
    {infiniteScrollingEnabled ? (
      <Waypoint onEnter={() => doFetchTaxonomyPosts(taxonomy, term, subCategory)} />
    ) : (
      <Button onClick={() => doStartInfiniteScrolling()}>Load More</Button>
    )}
  </div>
);

// TODO Need to adjust the store to include new stuff
export default connect(
  (state: AppState) => ({
    infiniteScrollingEnabled: state.editorialTaxonomyPosts.infiniteScroll,
  }),
  (dispatch: any) => ({
    doFetchTaxonomyPosts: (taxonomy: any, term: any, subCategory: any) =>
      dispatch(fetchEditorialTaxonomyPosts(taxonomy, term, subCategory)),
    doStartInfiniteScrolling: () => dispatch(startInfiniteScrolling()),
  }),
)(LoadMorePosts);
