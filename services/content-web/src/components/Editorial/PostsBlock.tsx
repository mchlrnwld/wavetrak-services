import Link from 'next/link';
import { FeedCard } from '@surfline/quiver-react';
import CircleArrow from '../CircleArrow';
import resizeImage from '../../utils/resizeImage';

interface Category {
  name: string;
  slug: string;
  url: string;
  imageUrl: string;
  taxonomy: string;
}

interface Post {
  id: number;
  title: string;
  subtitle: string;
  premium: boolean;
  permalink: string;
  createdAt: string;
  categories: Category[] | [];
  media: {
    type: string;
    feed1x: string;
    feed2x: string;
  };
}

interface PostsBlockProps {
  name: string;
  posts: Post[] | [];
  slug: string;
  taxonomy: string;
  term: string;
  imageResizing: boolean;
}

const PostsBlock = ({ name, posts, slug, taxonomy, term, imageResizing }: PostsBlockProps) => {
  const imageSettings = 'w=740,q=85,f=auto,fit=contain';
  const category = posts.length > 0 && posts[0].categories.filter((cat) => cat.name === name)[0];
  return (
    <div className="sl-tax-cat-block">
      <h2>{name}</h2>
      <Link href={`/${taxonomy}/${term}/${slug}`}>
        <a className="sl-tax-cat-block__view-all" href={`/${taxonomy}/${term}/${slug}`}>
          View All {name}
        </a>
      </Link>
      <div className="sl-tax-cat-block__posts">
        <div className="sl-tax-cat-block__posts__cards">
          {posts.map((post) => {
            const timestamp = Math.floor(new Date(post.createdAt).getTime() / 1000);
            return (
              <FeedCard
                key={post.id}
                isPremium={post.premium}
                category={category}
                imageUrl={resizeImage(post?.media?.feed2x || '', imageResizing, imageSettings)}
                title={post.title}
                createdAt={timestamp}
                permalink={post.permalink}
                showTimestamp={!Number.isNaN(Number(timestamp))}
                onClickLink={() => null}
                onClickTag={() => null}
                subtitle=""
              />
            );
          })}
          <Link href={`/${taxonomy}/${term}/${slug}`}>
            <a
              className="sl-tax-cat-block__posts__cards__view-all"
              href={`/${taxonomy}/${term}/${slug}`}
            >
              <div className="sl-tax-cat-block__posts__cards__view-all__arrow">
                <CircleArrow />
                <br />
                View All
              </div>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default PostsBlock;
