import classnames from 'classnames';
import React, { useRef, useMemo, ReactNode } from 'react';
import { v1 as uuidV1 } from 'uuid';
import {
  CamIcon,
  ForecastIcon,
  LocationIcon,
  TravelIcon,
  NewsIcon,
  TrackableLink,
} from '@surfline/quiver-react';
import { canUseDOM, slugify } from '@surfline/web-common';

import createAccessibleOnClick from '../../utils/createAccessibleOnClick';

import Styles from './SearchResults.module.scss';

const getClassNames = (_type: string) =>
  classnames({
    [Styles.resultName]: true,
    [Styles.resultNameNews]: _type === 'editorial' || _type === 'forecast',
  });

interface SearchResultsItemLinkProps {
  _source: {
    href?: string;
    permalink?: string;
    name?: string;
    content?: {
      title?: string;
    };
  };
  _type: string;
  result: {
    took: number | string;
  };
  searchResultIndex: number;
  term: string;
  children: ReactNode;
}

const SearchResultsItemLink = ({
  _source,
  searchResultIndex,
  term,
  result,
  _type,
  children,
}: SearchResultsItemLinkProps) => {
  // TODO: resolve any type
  // MutableRefObject<HTMLAnchorElement | undefined>
  // VS Ref and LegacyRef
  const linkRef: any = useRef();
  const eventProperties = useMemo(
    () => ({
      searchId: uuidV1(),
      searchResultRank: searchResultIndex + 1,
      sourcePage: canUseDOM ? window.location.href : null,
      destinationPage: _source.href || _source.permalink,
      queryTerm: term,
      resultName: _source.name || _source.content?.title,
      searchLatency: result.took,
      resultType: _type === 'editorial' || _type === 'forecast' ? 'news' : _type,
      location: 'results page',
    }),
    [
      _source.href,
      _source.permalink,
      term,
      _source.name,
      _source.content?.title,
      result.took,
      _type,
      searchResultIndex,
    ],
  );

  return (
    <TrackableLink
      eventName="Clicked Search Results"
      eventProperties={eventProperties}
      ref={linkRef}
    >
      <a className={Styles.resultLink} href={_source.href || _source.permalink} ref={linkRef}>
        {children}
      </a>
    </TrackableLink>
  );
};

type SearchResultSuggest = {
  [key in 'spot-suggest']: {
    text?: string;
    offset?: number;
    length?: number;
    options?: {
      text?: string;
      _index?: string;
      _type?: string;
      _id?: string;
      _score?: number;
      _source?: {
        breadCrumbs?: string[];
        name?: string;
        location?: {
          lon?: number;
          lat?: number;
        };
        cams?: string[];
        href?: string;
      };
      contexts?: {
        type?: string[];
        location?: string[];
      };
    }[];
  }[];
};

export interface SearchResult {
  took: number;
  timed_out: boolean;
  _shards: {
    total: number;
    successful: number;
    failed: number;
  };
  hits: {
    total: number;
    max_score: number;
    hits: {
      _index?: string;
      _type?: string;
      _id?: string;
      _score?: number;
      _source?: {
        breadCrumbs?: string[];
        name?: string;
        location?: {
          lon?: number;
          lat?: number;
        };
        cams?: string[];
        href?: string;
      };
    }[];
  };
  suggest?: SearchResultSuggest;
  status: number;
}

type SearchResultsSection = {
  [key in 'spot-suggest' | 'subregion-suggest' | 'geoname-suggest' | 'feed-tag-suggest']: {
    showAll?: boolean;
    name?: string;
    initialCount?: number;
  };
};

interface SearchResultsProps {
  doShowAllToggle: Function;
  results: SearchResult[] | undefined;
  sections: SearchResultsSection;
  term: string | undefined;
}

export const SearchResults = ({ doShowAllToggle, results, sections, term }: SearchResultsProps) => (
  <div className={Styles.results}>
    {results?.map((result: SearchResult) => {
      const [key] = Object.keys(result.suggest || {});
      const section = (sections as any)[key];
      const { hits } = result.hits;
      const suggestions = (result.suggest as any)[key][0].options;
      const transformSuggest = suggestions
        .filter((option: any) => hits.map((hit) => hit._id).indexOf(option._id) <= -1)
        .map((filteredOption: any) => ({ ...filteredOption, isSuggestion: true }));
      const searchResults = [...hits, ...transformSuggest].slice(0, 50);
      const count = (sections as any)[key]?.showAll ? searchResults.length : null;

      return (
        searchResults.length > 0 && (
          <section className={Styles.resultGroup} id={slugify(section?.name || '')} key={key}>
            <div className={Styles.resultGroupContainer}>
              <h2 className={Styles.resultGroupTitle}>{section?.name}</h2>
              {searchResults
                .slice(0, count || section?.initialCount)
                .map(({ _source, _id, _type, _index, isSuggestion }, searchResultIndex) => (
                  <div className={Styles.result} key={`${key}-${_id}`}>
                    <SearchResultsItemLink
                      _source={_source}
                      searchResultIndex={searchResultIndex}
                      term={term || ''}
                      result={result}
                      _type={_type}
                    >
                      {(() => {
                        if (key === 'spot-suggest') {
                          const hasCams =
                            _source.cams && _source.cams !== null && _source.cams.length > 0;
                          const isMultiCam = hasCams && _source.cams.length > 1;
                          return hasCams ? (
                            <CamIcon isMultiCam={isMultiCam} />
                          ) : (
                            <div className="blank" />
                          );
                        }
                        return null;
                      })()}
                      {key === 'subregion-suggest' ? <ForecastIcon /> : null}
                      {key === 'geoname-suggest' ? <LocationIcon /> : null}
                      {key === 'travel-suggest' ? <TravelIcon /> : null}
                      {key === 'feed-tag-suggest' ? (
                        <div>
                          <NewsIcon />
                        </div>
                      ) : null}
                      <div className={Styles.resultContainer}>
                        {(() => {
                          const newsResult = _index === 'feed';
                          const searchedField = newsResult ? _source.content.title : _source.name;
                          const indexOfSearchTerm = searchedField
                            .toLowerCase()
                            .indexOf(term?.toLowerCase());
                          if (newsResult && isSuggestion) {
                            return <span className={getClassNames(_type)}>{searchedField}</span>;
                          }
                          if (indexOfSearchTerm > -1) {
                            const searchTermLength = term?.length;
                            const resultTermLength = searchedField.length;
                            const endOfHighlight = indexOfSearchTerm + searchTermLength;
                            return (
                              <span className={getClassNames(_type)}>
                                {searchedField.substring(0, indexOfSearchTerm)}
                                <span className={Styles.highlight} key={indexOfSearchTerm}>
                                  {searchedField.substring(indexOfSearchTerm, endOfHighlight)}
                                </span>
                                {searchedField.substring(endOfHighlight, resultTermLength)}
                              </span>
                            );
                          }
                          return (
                            <span className={getClassNames(_type)}>
                              {searchedField
                                .toLowerCase()
                                .split('')
                                .map((char: string, index: number) =>
                                  (term || '').toLowerCase()?.indexOf(char) > -1 &&
                                  index <= searchedField.toLowerCase().indexOf(char) ? (
                                    <span className={Styles.highlight} key={searchedField[index]}>
                                      {searchedField[index]}
                                    </span>
                                  ) : (
                                    searchedField[index]
                                  ),
                                )}
                            </span>
                          );
                        })()}
                        {(() => {
                          if (_type === 'editorial') {
                            return _source.tags && _source.tags.length > 0 ? (
                              <span className={Styles.resultBreadcrumb}>
                                {_source.tags[0].name}
                              </span>
                            ) : null;
                          }
                          //eslint-disable-line
                          return _source.breadCrumbs && _source.breadCrumbs.length > 1 ? (
                            <span className={Styles.resultBreadcrumb}>
                              {_source.breadCrumbs[0]} /{' '}
                              {_source.breadCrumbs[_source.breadCrumbs.length - 1]}
                            </span>
                          ) : null;
                        })()}
                      </div>
                    </SearchResultsItemLink>
                  </div>
                ))}
            </div>
            {searchResults.length > section?.initialCount ? (
              <footer className={Styles.resultGroupFooter}>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <span
                  className={Styles.resultGroupFooterButton}
                  data-test="result-group-expand-toggle"
                  {...createAccessibleOnClick(() => doShowAllToggle(key))}
                >
                  {(sections as any)[key].showAll ? (
                    <span>Hide {searchResults.length - section.initialCount} results</span>
                  ) : (
                    <span>See all {searchResults.length} results</span>
                  )}
                </span>
              </footer>
            ) : null}
          </section>
        )
      );
    })}
  </div>
);

export default SearchResults;
