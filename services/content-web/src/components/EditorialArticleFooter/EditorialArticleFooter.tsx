import React from 'react';
import { DiscussionEmbed } from 'disqus-react';
import { GoogleDFP, FeedCard } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import StickyAdUnit from '../StickyAdUnit';
import loadAdConfig from '../../utils/adConfig';
import resizeImage from '../../utils/resizeImage';
import config from '../../config';
import { Article } from '../../types/editorial';

type Props = {
  article: Article;
  isPremium: boolean;
  imageResizing: boolean;
  isMobile: boolean;
  qaFlag: string | null;
};

const EditorialArticleFooter: React.FC<Props> = ({
  article,
  isPremium = false,
  qaFlag = null,
  isMobile = false,
  imageResizing,
}) => {
  const {
    permalink,
    createdAt,
    displayOptions: { hideAds, hideComments },
    sponsoredArticle: { dfpKeyword },
    relatedPosts,
  } = article;
  /*
    Legacy articles used peramlink for disqus id and must use same id to maintain comment history.
    Using id is preferable for new articles to prevent losing comments on title/permalink change.
  */
  const disqusIdentifier =
    new Date(createdAt) < new Date('2019-04-29 00:00:00') ? permalink : article.id;
  const disqusTitle = article.content.displayTitle || article.content.title;
  const disqusConfig = {
    url: permalink,
    identifier: disqusIdentifier,
    title: disqusTitle,
  } as any; // This is supposed to be DiscussionEmbedConfig from disqus-react package
  return (
    <div className="sl-editorial-article-footer" id="sl-editorial-article-footer">
      <div className="sl-editorial-article-footer__content">
        <div className="sl-editorial-article-footer__content__left">
          {!hideComments && isPremium && disqusTitle ? (
            <div className="sl-editorial-article-footer__disqus">
              <DiscussionEmbed shortname="surfline-comments" config={disqusConfig} />
            </div>
          ) : null}
          {isMobile && (dfpKeyword || (!hideAds && !isPremium)) ? (
            <div className="sl-editorial-article-footer__ad--mid">
              <GoogleDFP
                adConfig={loadAdConfig('editFooter', [
                  ['qaFlag', qaFlag],
                  ['contentKeyword', dfpKeyword],
                ])}
                isHtl
                isTesting={config.htl.isTesting}
              />
            </div>
          ) : null}
          <h5 className="sl-editorial-article-footer__title">Related Content</h5>
          <div className="sl_editorial-article-footer__related-articles">
            {relatedPosts.map((relatedArticle) => {
              const selectTag = relatedArticle.series[0] || relatedArticle.categories[0];
              return (
                <FeedCard
                  isPremium={relatedArticle.premium}
                  key={relatedArticle.title}
                  category={selectTag}
                  title={relatedArticle.title}
                  subtitle={relatedArticle.subtitle}
                  onClickLink={() =>
                    trackEvent('Clicked Link', {
                      linkUrl: relatedArticle.permalink,
                      category: 'editorial',
                      linkLocation: 'editorial related article',
                      linkName: relatedArticle.title,
                      articleId: relatedArticle.id,
                    })
                  }
                  onClickTag={() =>
                    trackEvent('Clicked Link', {
                      linkUrl: selectTag.url,
                      category: 'editorial',
                      linkLocation: 'editorial related article tag',
                      linkName: selectTag.name,
                    })
                  }
                  permalink={relatedArticle.permalink}
                  createdAt={relatedArticle.createdAt}
                  imageUrl={resizeImage(relatedArticle.media?.feed2x || '', imageResizing)}
                />
              );
            })}
          </div>
        </div>
        {!isMobile && (dfpKeyword || (!hideAds && !isPremium)) ? (
          <div className="sl-editorial-article-footer__content__right">
            <StickyAdUnit
              isDesktop
              qaFlag={qaFlag}
              adUnit="editFooter"
              bottomBoundary=".sl-editorial-article-footer__content__right"
              dfpKeyword={dfpKeyword}
              activeClass="sl-sticky-ad-unit-active-overrides"
              className="sl-sticky-ad-unit-base-overrides"
            />
          </div>
        ) : null}
      </div>
      {!hideAds && !isPremium ? (
        <div className="sl-editorial-article-footer__ad--bottom">
          <GoogleDFP
            adConfig={loadAdConfig('editFooterBottom', [
              ['qaFlag', qaFlag],
              ['contentKeyword', dfpKeyword],
            ])}
            isHtl
            isTesting={config.htl.isTesting}
          />
        </div>
      ) : null}
    </div>
  );
};

export default EditorialArticleFooter;
