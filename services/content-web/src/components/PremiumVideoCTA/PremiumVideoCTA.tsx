import React from 'react';
import Image from 'next/image';
import { PremiumTagLarge, Button } from '@surfline/quiver-react';
import config from '../../config';

type Props = {
  onClickVideoPaywall: (...args: any) => void;
  onClickSignIn: (...args: any) => void;
  imageUrl: string;
  videoText: string;
};

const PremiumVideoCTA: React.FC<Props> = ({
  onClickVideoPaywall,
  onClickSignIn,
  imageUrl,
  videoText = '',
}) => (
  <div>
    <div className="sl-premium-video__cta--image">
      <div className="sl-premium-video-cta__container">
        <Image className="sl-premium-video-cta__image" src={imageUrl} alt="imageHere" />
        <div className="sl-premium-video-cta__overlay">
          <PremiumTagLarge />
          <h4 className="sl-premium-video-cta__text--top">
            Premium Members Score Exclusive Video Premieres
          </h4>
          <span className="sl-premium-video-cta__text--middle">
            Enjoy 600+ ad-free HD surf cams and expert forecast analysis, as well as exclusive
            access to in-depth coverage of surf news and culture, travel, tips, movies and more.
          </span>
          <Button onClick={() => onClickVideoPaywall()}>Start Free Trial</Button>
          <span className="sl-premium-video-cta__text--bottom">
            Already a Surfline Premium subscriber?{' '}
            <a
              href={config.signInUrl}
              onClick={() => onClickSignIn()}
              className="sl-premium-video-cta__login"
            >
              Sign In
            </a>
          </span>
        </div>
      </div>
    </div>
    <div
      className="sl-premium-video-cta__caption"
      /* eslint-disable-next-line react/no-danger */
      dangerouslySetInnerHTML={{ __html: videoText }}
    />
  </div>
);

export default PremiumVideoCTA;
