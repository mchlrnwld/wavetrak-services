import React from 'react';
import Link from 'next/link';
import { BreadCrumbs } from '../../types/breadCrumbs';

const Breadcrumb: React.FC<{ baseUrl: string; breadCrumbs: BreadCrumbs }> = ({
  baseUrl = '',
  breadCrumbs,
}) => (
  <ul className="sl-breadcrumb">
    {/* eslint-disable jsx-a11y/anchor-is-valid */}
    {breadCrumbs.map((breadcrumb) => (
      <li key={breadcrumb.name}>
        {baseUrl ? (
          <Link href={`${baseUrl}${breadcrumb.url}`}>{breadcrumb.name}</Link>
        ) : (
          <a>{breadcrumb.name}</a>
        )}
      </li>
    ))}
    {/* eslint-enable jsx-a11y/anchor-is-valid */}
  </ul>
);

export default Breadcrumb;
