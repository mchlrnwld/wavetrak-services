import React from 'react';
import classNames from 'classnames';
import Chevron from '../Chevron';

const getClassNames = (size: string | null, direction: string) =>
  classNames({
    'sl-scroll-control': true,
    'sl-scroll-control--tall': size === 'tall',
    'sl-scroll-control--left': direction === 'left',
    'sl-scroll-control--right': direction === 'right',
  });

type Props = {
  direction: string;
  clickHandler: (...args: any) => void;
  size: string;
};

const ScrollControl: React.FC<Props> = ({ direction, size = null, clickHandler = undefined }) => (
  /* TODO: Why was this disabled specifically for eslint-disable jsx-a11y/click-events-have-key-events */
  /* eslint-disable-next-line */
  <div className={getClassNames(size, direction)} onClick={clickHandler}>
    <Chevron direction={direction} />
  </div>
);

export default ScrollControl;
