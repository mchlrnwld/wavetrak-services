import React from 'react';
import classnames from 'classnames';
import { formatDistance } from 'date-fns';
import { Button as QuiverButton } from '@surfline/quiver-react';
import ClockIcon from '../ClockIcon';
import { RelatedArticles as RelatedArticlesType } from '../../types/relatedArticles';
import resizeImage from '../../utils/resizeImage';

type Props = {
  relatedArticles: RelatedArticlesType;
  imageResizing: boolean;
};

type State = { seeMore: boolean };

class RelatedArticles extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      seeMore: false,
    };
  }

  static getRelatedArticlesClasses = (seeMore: boolean) =>
    classnames({
      'sl-related__articles': true,
      'sl-related__articles--open': seeMore,
    });

  static getSeeMoreClasses = (seeMore: boolean) =>
    classnames({
      'sl-related__more': true,
      'sl-related__more--open': seeMore,
    });

  toggleSeeMore = () => {
    this.setState({
      seeMore: true,
    });
  };

  render() {
    const { relatedArticles, imageResizing } = this.props;
    const { seeMore } = this.state;
    return (
      <div className="sl-related">
        <h3>Related Articles</h3>
        <div
          className={
            relatedArticles.length > 2
              ? RelatedArticles.getRelatedArticlesClasses(seeMore)
              : 'sl-related__articles sl-related__articles--open'
          }
        >
          {relatedArticles.slice(0, 6).map((article) => (
            <a
              href={article.permalink}
              key={article.id}
              className="sl-related__articles__article"
              target={article.newWindow ? '_blank' : '_self'}
              rel="noreferrer"
            >
              <div
                className="sl-related__articles__article__img"
                style={{
                  backgroundImage: `url(${resizeImage(
                    article.media?.feed1x || '',
                    imageResizing,
                  )})`,
                }}
              />
              <div className="sl-related__articles__article__content">
                <div className="sl-related__articles__article__content__top">
                  {article.tags.length && <a href={article.tags[0].url}>{article.tags[0].name}</a>}
                  <div className="sl-related__articles__article__content__top__time">
                    <ClockIcon className="sl-related__articles__article__content__top__time__clock" />
                    {`${formatDistance(article.createdAt * 1000, new Date())} ago`}
                    {article.createdAt !== article.updatedAt &&
                    article.externalLink !== 'External URL' ? (
                      <div className="sl-related__articles__article__content__top__time__updated">
                        <span>Updated</span>
                        {` ${formatDistance(article.updatedAt * 1000, new Date())} ago`}
                      </div>
                    ) : null}
                    {article.externalLink === 'External URL' ? (
                      <div className="sl-related__articles__article__content__top__time__updated">
                        {`Via ${article.externalSource}`}
                      </div>
                    ) : null}
                  </div>
                </div>
                <h4>{article.content.title}</h4>
              </div>
            </a>
          ))}
        </div>
        {relatedArticles.length > 2 ? (
          <div className={RelatedArticles.getSeeMoreClasses(seeMore)}>
            <QuiverButton type="info" onClick={() => this.toggleSeeMore()}>
              Load More
            </QuiverButton>
          </div>
        ) : null}
      </div>
    );
  }
}

export default RelatedArticles;
