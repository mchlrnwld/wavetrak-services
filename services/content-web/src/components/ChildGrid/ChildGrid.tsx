import React from 'react';
import classnames from 'classnames';
import { throttle as _throttle } from 'lodash';
import canUseDOM from '../../common/canUseDom';
import { GeoChildren } from '../../types/children';
import resizeImage from '../../utils/resizeImage';
import { Taxonomy } from '../../types/taxonomy';
import config from '../../config';

type Props = {
  taxonomy: Taxonomy;
  geoChildren: GeoChildren;
  imageResizing: boolean;
};

type State = { showMore: boolean };

class ChildGrid extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showMore: false,
    };
  }

  componentDidMount() {
    if (canUseDOM) {
      this.handleWindowResize();
      window.addEventListener('resize', _throttle(this.handleWindowResize, 250));
    }
  }

  componentWillUnmount() {
    if (canUseDOM) window.removeEventListener('resize', _throttle(this.handleWindowResize, 250));
  }

  handleWindowResize = () => {
    const {
      geoChildren: { length },
    } = this.props;
    const windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (length < 3 && windowWidth < config.childGridTwoCol) {
      this.setState({
        showMore: true,
      });
    } else if (
      length < 5 &&
      windowWidth >= config.childGridTwoCol &&
      windowWidth < config.childGridThreeCol
    ) {
      this.setState({
        showMore: true,
      });
    } else if (length < 7 && windowWidth >= config.childGridThreeCol) {
      this.setState({
        showMore: true,
      });
    }
  };

  toggleGridHeight = () => {
    const { showMore } = this.state;
    this.setState({
      showMore: !showMore,
    });
  };

  render() {
    const { taxonomy, geoChildren, imageResizing } = this.props;
    const { showMore } = this.state;
    return (
      <div className="sl-child-grid">
        <h3>
          Areas within <span>{taxonomy.name}</span>
        </h3>
        <div
          className={classnames({
            'sl-child-grid__grid': true,
            'sl-child-grid__grid--open': showMore,
          })}
        >
          {geoChildren.map((child) => (
            <a
              href={child.url}
              key={child.url}
              style={{
                backgroundImage: `url('${resizeImage(child.media?.medium || '', imageResizing)}')`,
              }}
              className={classnames({
                'sl-child-grid__grid__child': true,
                'sl-child-grid__grid__child--image': !!child.media && !!child.media.medium,
              })}
            >
              <h6>
                {child.name} <span>Travel &amp; Surf Guide</span>
              </h6>
            </a>
          ))}
        </div>
        {/* TODO: Why is this being disabled? Shouldnt it work? it was jsx-a11y/click-events-have-key-events  */}
        {/* eslint-disable-next-line */}
        <div
          onClick={() => this.toggleGridHeight()}
          className={classnames({
            'sl-child-grid__more': true,
            'sl-child-grid__more--hidden': showMore,
          })}
        >
          <span>See More</span>
        </div>
      </div>
    );
  }
}

export default ChildGrid;
