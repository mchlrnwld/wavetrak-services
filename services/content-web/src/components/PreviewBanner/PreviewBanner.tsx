import React from 'react';
import { Article } from '../../types/editorial';

const PreviewBanner: React.FC<{ article: Article }> = ({ article }) => (
  <a className="sl-preview-banner" href={`/wp-admin/post.php?post=${article.id}&action=edit`}>
    Edit Post
  </a>
);

export default PreviewBanner;
