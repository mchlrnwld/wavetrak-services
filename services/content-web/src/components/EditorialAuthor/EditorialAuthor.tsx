import React from 'react';
import Image from 'next/image';
import classnames from 'classnames';
import { formatDistance, format } from 'date-fns';
import { GoogleDFP, ShareArticle } from '@surfline/quiver-react';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

const getAuthorSocialClasses = (hideAuthor: boolean) =>
  classnames({
    'sl-editorial-author__social': true,
    'sl-editorial-author__social--author': !hideAuthor,
  });

type Author = {
  iconUrl: string;
  name: string;
};

type SponsoredArticle = {
  attributionText?: string;
  dfpKeyword: string;
};

type Props = {
  author: Author;
  createdAt: Date;
  hideAuthor: boolean;
  updatedAt: Date;
  url: string;
  onClickLink: (...args: any) => void;
  sponsoredArticle: SponsoredArticle;
  dateFormat: string;
};

const EditorialAuthor: React.FC<Props> = ({
  author,
  createdAt,
  hideAuthor = false,
  onClickLink,
  sponsoredArticle,
  updatedAt,
  url,
  dateFormat = 'MDY',
}) => {
  const defaultUrl = 'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427e?d=mm';
  const date = dateFormat === 'MDY' ? 'MMM do, yyyy' : 'do, MMM yyyy';

  return (
    <div className="sl-editorial-author__container">
      {sponsoredArticle.dfpKeyword ? (
        <div className="sl-editorial-author__presented-by">
          <span>{sponsoredArticle.attributionText}</span>
          <GoogleDFP
            adConfig={loadAdConfig('sponsoredArticleLogo', [
              ['contentKeyword', sponsoredArticle.dfpKeyword],
            ])}
            isHtl
            isTesting={config.htl.isTesting}
          />
        </div>
      ) : null}
      <div className="sl-editorial-author__author">
        {!hideAuthor ? (
          <div className="sl-editorial-author">
            <Image
              className="sl-editorial-author__image"
              src={`${author.iconUrl || defaultUrl}`}
              alt={author.name || 'Surfline Avatar'}
              width="34px"
              height="34px"
            />
            <div className="sl-editorial-author__details">
              <div className="sl-editorial-author__details__name">{author.name || null}</div>
              <div className="sl-editorial-author__details__date">
                {`${format(new Date(createdAt), date)}. `}
                {createdAt !== updatedAt ? (
                  <span>
                    <span className="sl-editorial-author__details__date__updated">Updated</span>
                    {` ${formatDistance(new Date(updatedAt), new Date())} ago.`}
                  </span>
                ) : null}
              </div>
            </div>
          </div>
        ) : null}
        <div className={getAuthorSocialClasses(hideAuthor)}>
          <ShareArticle url={url} onClickLink={onClickLink} />
        </div>
      </div>
    </div>
  );
};

export default EditorialAuthor;
