import { PreloadedState, Reducer } from 'redux';

const createReducer =
  (handlers: { [key: string]: Reducer }, initialState: PreloadedState<{}>) =>
  (state = initialState, action: any) => {
    const handler = action && action.type ? handlers[action.type] : null;

    if (handler) return handler(state, action);
    return state;
  };

export default createReducer;
