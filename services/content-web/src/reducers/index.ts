import { combineReducers, Reducer, CombinedState } from 'redux';
import { AsyncReducers, BackplaneState } from '@surfline/web-common';
import animations from './animations';
import analytics from './analytics';
import search from './search';
import split from './split';
import status from './status';
import topSpots from './topSpots';
import locationView from './locationView';
import worldTaxonomy from './worldTaxonomy';
import editorialTaxonomyDetails from './editorialTaxonomyDetails';
import editorialTaxonomyPosts from './editorialTaxonomyPosts';
import editorial from './editorial';

export type ContentAsyncReducers = AsyncReducers & {
  backplane: Reducer<CombinedState<BackplaneState['backplane']>>;
};

const rootReducer = (asyncReducers: ContentAsyncReducers) =>
  combineReducers({
    analytics,
    animations,
    editorial,
    editorialTaxonomyDetails,
    editorialTaxonomyPosts,
    locationView,
    search,
    split,
    status,
    topSpots,
    worldTaxonomy,
    ...asyncReducers,
  });

export default rootReducer;
