import { AnyAction, Reducer } from 'redux';
import createReducer from './createReducer';

import { FETCH_WORLD_TAXONOMY_SUCCESS } from '../actions/worldTaxonomy';

const initialState: Array<any> = [];

const handlers: { [key: string]: Reducer<any, AnyAction> } = {};

handlers[FETCH_WORLD_TAXONOMY_SUCCESS] = (state, { taxonomy }) => taxonomy;

export default createReducer(handlers, initialState);
