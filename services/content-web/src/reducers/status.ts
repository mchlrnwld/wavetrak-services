import { AnyAction, Reducer } from 'redux';

import createReducer from './createReducer';
import { INTERNAL_SERVER_ERROR, NOT_FOUND, REDIRECT_TEMPORARY } from '../actions/status';

const initialState = { code: 200 };

const handlers: { [key: string]: Reducer<any, AnyAction> } = {
  [NOT_FOUND]: (state) => ({
    ...state,
    code: 404,
  }),
  [REDIRECT_TEMPORARY]: (state, { dest }) => ({
    ...state,
    code: 302,
    dest,
  }),
  [INTERNAL_SERVER_ERROR]: (state, { message }) => ({
    ...state,
    code: 500,
    message,
  }),
};

export default createReducer(handlers, initialState);
