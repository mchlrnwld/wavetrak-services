const base = {
  addFavoritesUrl: '/setup/favorite-surf-spots/cams',
  baseContestUrl: (region: string, period: string, contest: string = 'wave-of-the-winter') =>
    `contests/${contest}/${region}/${period}/`,
  cdnUrl: 'http://e.cdn-surfline.com/images/sl_landing/',
  cloudflareImageResizingUrl: (
    mediaUrl: string,
    settings: string = 'w=600,q=75,f=auto,fit=contain',
  ) => `https://www.surfline.com/cdn-cgi/image/${settings}/${mediaUrl}`,
  createAccountUrl: '/create-account',
  desktopWidth: 1201,
  facebookOGImage: '/facebook-og-default.png',
  favoritesUrl: '/setup/favorite-surf-spots',
  funnelUrl: '/upgrade',
  hostForSEO: 'https://www.surfline.com',
  homepageSEOUrl: 'https://www.surfline.com/',
  jwPlayerUrl: 'https://wa.cdn-surfline.com/quiver/0.14.2/scripts/jwplayer.js',
  meta: {
    description: {
      homepage:
        'The most accurate and trusted surf reports, forecasts' +
        ' and coastal weather. Surfers from around the world choose Surfline for dependable' +
        ' and up to date surfing forecasts and high quality surf content, live surf cams and features.',
    },
    title: {
      homepage:
        'SURFLINE.COM | Global Surf Reports, Surf Forecasts, Live Surf Cams and Coastal Weather',
      search: (term: string) => `Search results for ${term} - Surfline`,
      travel: {
        landing: 'SURFLINE.COM | Travel',
        zone: (name: string) => `SURFLINE.COM | Travel Zone ${name}`,
      },
    },
    og: {
      image: '/facebook-og-default.png',
      title: 'Surfline.com',
    },
    twitter: {
      image: '/twitter-og-default.png',
    },
  },
  childGridTwoCol: 680,
  childGridThreeCol: 1090,
  mobileWidth: 512,
  multiCamConditionsPollingInterval: process.env.MULTI_CAM_CONDITIONS_POLLING_INTERVAL || 1200000,
  onboardingUrl: '/setup/favorite-surf-spots',
  signInUrl: '/sign-in',
  tabletWidth: 769,
};

export default base;
