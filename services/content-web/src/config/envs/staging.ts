const staging = {
  appKeys: {
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
  },
  baseSiteUrl: 'https://staging.surfline.com/',
  cdnUrl: 'https://product-cdn.staging.surfline.com/content-web/',
  defaultImageUrl: 'https://spot-thumbnails.staging.surfline.com/spots/default/default_1500.jpg',
  homepageUrl: 'https://staging.surfline.com/',
  htl: {
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
  },
  productAPI: 'https://services.staging.surfline.com',
  react: {
    integrity:
      'sha512-qlzIeUtTg7eBpmEaS12NZgxz52YYZVF5myj89mjJEesBd/oE9UPsYOX2QAXzvOAZYEvQohKdcY8zKE02ifXDmA==',
    src: 'https://unpkg.com/react@17.0.2/umd/react.production.min.js',
  },
  reactDOM: {
    integrity:
      'sha512-9jGNr5Piwe8nzLLYTk8QrEMPfjGU0px80GYzKZUxi7lmCfrBjtyCc1V5kkS5vxVwwIB7Qpzc7UxLiQxfAN30dw==',
    src: 'https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js',
  },
  robots: 'noindex,nofollow',
  servicesURL: 'https://services.staging.surfline.com',
};

export default staging;
