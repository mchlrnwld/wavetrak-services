const development = {
  appKeys: {
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  },
  baseSiteUrl: 'https://staging.surfline.com',
  cdnUrl: '/',
  defaultImageUrl: 'https://spot-thumbnails.sandbox.surfline.com/spots/default/default_1500.jpg',
  homepageUrl: `http://localhost:8080`,
  htl: {
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
  },
  productAPI: 'https://services.staging.surfline.com',
  react: {
    integrity:
      'sha512-Vf2xGDzpqUOEIKO+X2rgTLWPY+65++WPwCHkX2nFMu9IcstumPsf/uKKRd5prX3wOu8Q0GBylRpsDB26R6ExOg==',
    src: 'https://unpkg.com/react@17.0.2/umd/react.development.js',
  },
  reactDOM: {
    integrity:
      'sha512-Wr9OKCTtq1anK0hq5bY3X/AvDI5EflDSAh0mE9gma+4hl+kXdTJPKZ3TwLMBcrgUeoY0s3dq9JjhCQc7vddtFg==',
    src: 'https://unpkg.com/react-dom@17.0.2/umd/react-dom.development.js',
  },
  robots: 'noindex,nofollow',
  servicesURL: 'https://services.staging.surfline.com',
};

export default development;
