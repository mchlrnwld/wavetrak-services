const production = {
  appKeys: {
    fbAppId: process.env.FB_APP_ID || '218714858155230',
    segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  },
  cdnUrl: 'https://wa.cdn-surfline.com/content-web/',
  defaultImageUrl: 'https://spot-thumbnails.cdn-surfline.com/spots/default/default_1500.jpg',
  homepageUrl: 'https://www.surfline.com/',
  htl: {
    cssUrl: 'https://htlbid.com/v3/surfline.com/htlbid.css',
    isTesting: 'no',
    scriptUrl: 'https://htlbid.com/v3/surfline.com/htlbid.js',
  },
  productAPI: 'https://services.surfline.com',
  react: {
    src: 'https://unpkg.com/react@17.0.2/umd/react.production.min.js',
    integrity:
      'sha512-qlzIeUtTg7eBpmEaS12NZgxz52YYZVF5myj89mjJEesBd/oE9UPsYOX2QAXzvOAZYEvQohKdcY8zKE02ifXDmA==',
  },
  reactDOM: {
    src: 'https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js',
    integrity:
      'sha512-9jGNr5Piwe8nzLLYTk8QrEMPfjGU0px80GYzKZUxi7lmCfrBjtyCc1V5kkS5vxVwwIB7Qpzc7UxLiQxfAN30dw==',
  },
  servicesURL: 'https://services.surfline.com',
};

export default production;
