/* eslint-disable global-require, import/no-dynamic-require */
import getConfig from 'next/config';
import baseConfig from './base';

interface EnvConfig {
  appKeys: {
    fbAppId: string;
    segment: string;
    splitio: string;
  };
  baseSiteUrl: string;
  cdnUrl: string;
  cloudflareImageResizingUrl: (mediaUrl: string, settings: string) => void;
  funnelUrl: string;
  homepageUrl: string;
  hostForSEO: string;
  htl: {
    cssUrl: string;
    isTesting: string;
    scriptUrl: string;
  };
  productAPI: string;
  react: {
    integrity: string;
    src: string;
  };
  reactDOM: {
    integrity: string;
    src: string;
  };
  robots: string;
  servicesURL: string;
  childGridTwoCol: any; // TODO: not sure what this is
  childGridThreeCol: any; // TODO: not sure what this is
  defaultImageUrl: string;
  surflineEmbedURL: string;
  surflineHost: string;
  signInUrl: string;
}

const appEnv = process.env.APP_ENV || getConfig()?.publicRuntimeConfig?.APP_ENV;

const envConfig = require(`./envs/${appEnv}`) as { default: EnvConfig };

const config = {
  ...baseConfig,
  ...(envConfig.default || {}),
};

export default config;
