// const autoprefixer = require('autoprefixer');

module.exports = {
  stories: [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ],
  // webpackFinal: async (config) => {
  //   config.module.rules.push(
  //     {
  //       test: /\.scss$/,
  //       exclude: /\.module.scss$/,
  //       use: [
  //         'style-loader',
  //         { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
  //         {
  //           loader: 'postcss-loader',
  //           options: {
  //             postcssOptions: {
  //               plugins: [autoprefixer],
  //             },
  //           },
  //         },
  //         { loader: 'resolve-url-loader' },
  //         { loader: 'sass-loader', options: { sourceMap: true } },
  //       ],
  //     },
  //   );

  //   // Return the altered config
  //   return config;
  // }
}
