provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/content-web/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-staging"
}

locals {
  dns_name = "content-web.staging.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "content" {
  source = "../../"

  company     = "sl"
  application = "content-web"
  environment = "staging"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"
  ecs_cluster = "sl-core-svc-staging"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-staging/4042d775e313f612/e1f601835f792a75"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}
