provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/content-web/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-sandbox"
}

locals {
  dns_name = "content-web.sandbox.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "content" {
  source = "../../"

  company     = "sl"
  application = "content-web"
  environment = "sandbox"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"
  ecs_cluster = "sl-core-svc-sandbox"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-sandbox/224dfd1fa8567f7e/2270815960a67034"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}
