// eslint-disable-next-line import/extensions
import withImages from 'next-images';

const cdnUrls = {
  development: '/',
  sandbox: 'https://product-cdn.sandbox.surfline.com/content-web/',
  staging: 'https://product-cdn.staging.surfline.com/content-web/',
  production: 'https://wa.cdn-surfline.com/content-web/',
};

const nextConfig = () =>
  /**
   * @type {import('next').NextConfig}
   */
  ({
    webpack: (config, { isServer }) => {
      if (isServer) {
        return config;
      }

      // eslint-disable-next-line no-param-reassign
      config.externals = {
        ...config.externals,
        react: 'vendor.React',
        ReactDOM: 'vendor.ReactDOM',
      };

      return config;
    },
    images: {
      domains: [
        'wa.cdn-surfline.com',
        'product-cdn.staging.surfline.com',
        'product-cdn.sandbox.surfline.com',
        'd14fqx6aetz9ka.cloudfront.net',
        'd1542niisbwnmf.cloudfront.net',
        'd2pn5sjh0l9yc4.cloudfront.net',
        'secure.gravatar.com',
      ],
    },
    assetPrefix: cdnUrls[process.env.APP_ENV],
    productionBrowserSourceMaps: true,
    swcMinify: true,
    publicRuntimeConfig: {
      APP_ENV: process.env.APP_ENV,
    },
    env: {
      APP_ENV: process.env.APP_ENV,
    },
    crossOrigin: 'anonymous',
    eslint: {
      ignoreDuringBuilds: true,
    },
  });

const config = (phase) => withImages(nextConfig(phase));

export default config;
