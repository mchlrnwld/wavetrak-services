import mongoose from 'mongoose';
import { expect } from 'chai';
import { positionSchema } from './Position';

describe('Position schema', () => {
  it('validates required fields', async () => {
    const Position = mongoose.model('Position', positionSchema);
    const newPosition = new Position();
    try {
      await newPosition.validate();
    } catch (err) {
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      // TODO
    }
  });
});
