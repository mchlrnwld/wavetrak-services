import { expect } from 'chai';
import sinon from 'sinon';
import newrelic from 'newrelic';
import * as SessionsModel from './Session';
import sessionFixture from '../fixtures/session';
import Session from './Session';

describe('Sessions schema', () => {
  it('validates required fields', async () => {
    const newSession = new Session();
    try {
      await newSession.validate();
    } catch (err) {
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.startTimestamp).to.be.ok();
      expect(err.errors.startTimestamp.message).to.equal('Start Timestamp is required');
      expect(err.errors.endTimestamp).to.be.ok();
      expect(err.errors.endTimestamp.message).to.equal('End Timestamp is required');
      expect(err.errors.visibility).to.be.ok();
      expect(err.errors.visibility.message).to.equal('Visibility is required');
      // TODO
    }
  });

  describe('insertSession', () => {
    let createStub;
    /** @type {sinon.SinonSpy} */
    let noticeErrorStub;
    beforeEach(() => {
      createStub = sinon.stub(Session, 'create');
      noticeErrorStub = sinon.spy(newrelic, 'noticeError');
    });

    afterEach(() => {
      createStub.restore();
      noticeErrorStub.restore();
    });

    it('should insert a new session', async () => {
      createStub.resolves();
      await SessionsModel.insertSession(sessionFixture);
      expect(createStub).to.have.been.calledOnceWithExactly(sessionFixture);
    });

    it('should noticeError on error and throw a server error', async () => {
      createStub.throws();
      let error;
      try {
        await SessionsModel.insertSession(sessionFixture);
      } catch (err) {
        error = err;
        expect(err.name).to.equal('ServerError');
        expect(err.message).to.equal('Session insertion failed');
        expect(noticeErrorStub).to.have.been.calledOnce();
      }
      expect(error).to.exist();
    });
  });
});
