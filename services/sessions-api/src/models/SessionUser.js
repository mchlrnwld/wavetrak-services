import mongoose from 'mongoose';

const { ObjectId } = mongoose.Types;
export const sessionUserSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, 'User ID is required'],
    validate: {
      validator(v) {
        return ObjectId.isValid(v);
      },
      message: props => `${props.value} is not a valid user ID`,
    },
  },
  nickname: {
    type: String,
  },
  groups: {
    type: [String],
    enum: ['SESSION_INFLUENCER', 'SL_TEAM', 'RIPCURL', 'UAG'],
    required: [true, 'A valid user group is required'],
    validate: {
      validator(v) {
        return new Set(v).size === v.length;
      },
      message: 'Duplicate group entered',
    },
  },
  status: {
    type: String,
    enum: ['DELETED', 'ACTIVE'],
    required: [true, 'A valid user status is required'],
  },
});

const SessionUser = mongoose.model('SessionUser', sessionUserSchema);

export const insertSessionUser = sessionUser => SessionUser.create(sessionUser);

export default SessionUser;
