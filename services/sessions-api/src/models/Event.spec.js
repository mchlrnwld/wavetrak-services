import mongoose from 'mongoose';
import { expect } from 'chai';
import { eventSchema } from './Event';

describe('Event schema', () => {
  it('validates required fields', async () => {
    // TODO: refactor to not use Mongo
    const Event = mongoose.model('Event', eventSchema);
    const newEvent = new Event();
    try {
      await newEvent.validate();
    } catch (err) {
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      // TODO
    }
  });
});
