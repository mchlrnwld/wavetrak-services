import mongoose from 'mongoose';

const statsSchema = new mongoose.Schema(
  {
    startTimestamp: { type: Date, required: [true, 'Start Timestamp is required'] },
    endTimestamp: { type: Date, required: [true, 'End Timestamp is required'] },
    waveCount: { type: Number, default: 0 },
    speedMax: { type: Number },
    distancePaddled: { type: Number },
    distanceWaves: { type: Number },
    calories: { type: Number },
    wavesPerHour: { type: Number },
    speedAverage: { type: Number },
    longestWave: {
      distance: { type: Number },
      seconds: { type: Number },
    },
  },
  { _id: false },
);

export default statsSchema;
