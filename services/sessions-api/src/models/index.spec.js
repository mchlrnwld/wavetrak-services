import { assert, expect } from 'chai';
import mongoose from 'mongoose';
import sinon from 'sinon';
import * as SessionsModel from './Session';
import * as Model from './index';
import enrichedSessionFixture, { eventsFixture } from '../fixtures/enrichedSession';
import sessionFixture from '../fixtures/session';
import validatedSessionFixture from '../fixtures/validatedSession';
import * as sessionEvents from '../helpers/sessionEvents';
import * as getCaughtWaveCount from '../helpers/getCaughtWaveCount';
import * as aws from '../external/aws';
import config from '../config';

const Session = SessionsModel.default;
const { EnrichedSession } = SessionsModel;
const { APP_VERSION } = config;

describe('Session Model', () => {
  describe('getSessions', () => {
    beforeEach(() => {
      sinon.stub(Session, 'find');
    });

    afterEach(() => {
      Session.find.restore();
    });

    it('should get all sessions for given userId', async () => {
      // TODO
      expect(true).to.be.true();
    });
  });

  describe('addSession', () => {
    let uploadSessionEventsStub;
    let insertSessionStub;
    let rollbackEventsUploadStub;
    /** @type {sinon.SinonStub} */
    let objectIdStub;
    beforeEach(() => {
      uploadSessionEventsStub = sinon.stub(aws, 'uploadSessionEvents');
      insertSessionStub = sinon.stub(SessionsModel, 'insertSession');
      rollbackEventsUploadStub = sinon.stub(aws, 'rollbackEventsUpload');
      objectIdStub = sinon.stub(mongoose.Types, 'ObjectId');
      config.APP_VERSION = 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173';
    });

    afterEach(() => {
      uploadSessionEventsStub.restore();
      insertSessionStub.restore();
      rollbackEventsUploadStub.restore();
      objectIdStub.restore();
      config.APP_VERSION = APP_VERSION;
    });

    it('should properly add a session', async () => {
      uploadSessionEventsStub.resolves();
      insertSessionStub.resolves();
      rollbackEventsUploadStub.resolves();
      objectIdStub.returns(sessionFixture._id);
      objectIdStub.withArgs(sessionFixture.user).returns(sessionFixture.user);
      objectIdStub.withArgs(sessionFixture.client).returns(sessionFixture.client);

      const session = await Model.addSession(
        sessionFixture.user,
        sessionFixture.client,
        validatedSessionFixture,
        {
          longitude: sessionFixture.gpsCenter.coordinates[0],
          latitude: sessionFixture.gpsCenter.coordinates[1],
        },
        {
          watchOSVersion: 'testWatchOSVersion',
          watchAppVersion: 'testWatchAppVersion',
        },
      );

      expect(session).to.be.deep.equal(sessionFixture);
      expect(uploadSessionEventsStub).to.have.been.calledOnceWithExactly(
        sessionFixture._id,
        validatedSessionFixture.events,
      );
      expect(insertSessionStub).to.have.been.calledOnceWithExactly(sessionFixture);
      expect(rollbackEventsUploadStub).to.not.have.been.called();
    });

    it('should roll back the events upload if insertion fails', async () => {
      uploadSessionEventsStub.resolves();
      insertSessionStub.throws({ error: true });
      rollbackEventsUploadStub.resolves();
      objectIdStub.returns(sessionFixture._id);
      objectIdStub.withArgs(sessionFixture.user).returns(sessionFixture.user);
      objectIdStub.withArgs(sessionFixture.client).returns(sessionFixture.client);

      try {
        await Model.addSession(
          sessionFixture.user,
          sessionFixture.client,
          validatedSessionFixture,
          {
            longitude: sessionFixture.gpsCenter.coordinates[0],
            latitude: sessionFixture.gpsCenter.coordinates[1],
          },
          {
            watchOSVersion: 'testWatchOSVersion',
            watchAppVersion: 'testWatchAppVersion',
          },
        );
      } catch (_) {
        // do nothing
      }

      expect(uploadSessionEventsStub).to.have.been.calledOnceWithExactly(
        sessionFixture._id,
        validatedSessionFixture.events,
      );
      expect(insertSessionStub).to.have.been.calledOnceWithExactly(sessionFixture);
      expect(rollbackEventsUploadStub).to.have.been.calledOnceWithExactly(sessionFixture._id);
    });
  });

  describe('updateSession,', () => {
    beforeEach(() => {
      sinon.stub(Session, 'findOneAndUpdate');
    });

    afterEach(() => {
      Session.findOneAndUpdate.restore();
    });

    it('should update a valid session', async () => {
      // TODO
      expect(true).to.be.true();
    });
  });

  describe('deleteSession,', () => {
    beforeEach(() => {
      sinon.stub(Session, 'findOneAndUpdate');
      sinon.spy(Model, 'deleteSession');
      sinon.spy(Model, 'updateSessionStatus');
    });

    afterEach(() => {
      Session.findOneAndUpdate.restore();
      Model.deleteSession.restore();
      Model.updateSessionStatus.restore();
    });

    it('should delete a valid session', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const sessionId = '5d780e6da84b19a0bd50a4f5';
      Session.findOneAndUpdate.resolves({
        value: {
          _id: '5d780e6da84b19a0bd50a4f5',
          user: '5ce2edc6f8c849008de1d27e',
          status: 'DELETED',
        },
      });
      const response = await Model.deleteSession(userId, sessionId);
      expect(response.value.status).to.equal('DELETED');
      expect(response.value._id).to.equal(sessionId);
      assert(Session.findOneAndUpdate.calledOnce);
    });

    it('should throw an APIError if the sessionId is invalid', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      try {
        await Model.deleteSession(userId, null);
      } catch (err) {
        expect(err.name).to.equal('APIError');
      }
    });
  });

  describe('patchSessionWaveDetails,', () => {
    let overridesSetSpy;
    let overridesGetSpy;
    let hydrateSessionSpy;
    let getCaughtWaveCountSpy;
    let getSessionEventsStub;
    let enrichedSession;

    beforeEach(() => {
      enrichedSession = enrichedSessionFixture();
      sinon.stub(EnrichedSession, 'updateOne');
      getSessionEventsStub = sinon.stub(aws, 'getSessionEvents');
      overridesGetSpy = sinon.spy(enrichedSession.eventOverrides, 'get');
      overridesSetSpy = sinon.spy(enrichedSession.eventOverrides, 'set');
      hydrateSessionSpy = sinon.spy(sessionEvents, 'hydrateSession');
      getCaughtWaveCountSpy = sinon.spy(getCaughtWaveCount, 'default');
    });

    afterEach(() => {
      EnrichedSession.updateOne.restore();
      getSessionEventsStub.restore();
      overridesSetSpy.restore();
      overridesGetSpy.restore();
      hydrateSessionSpy.restore();
      getCaughtWaveCountSpy.restore();
    });

    it('should throw an error if a session is not passed', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const waveId = 'wave1';
      try {
        await Model.patchSessionWaveDetails(userId, undefined, waveId, {
          favorite: false,
        });
      } catch (error) {
        expect(error.message).to.equal('Invalid Parameters: Cannot update with invalid sessionId');
      }
    });

    it('should throw an error if a waveId is not passed', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const session = {};
      const waveId = null;
      try {
        await Model.patchSessionWaveDetails(userId, session, waveId, {
          favorite: false,
        });
      } catch (error) {
        expect(error.message).to.equal('Invalid Parameters: Cannot update with invalid waveId');
      }
    });

    it('should throw an error if the waveId does not exist on the session', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const waveId = 'wave3';
      const session = enrichedSession;

      try {
        await Model.patchSessionWaveDetails(userId, session, waveId, {
          favorite: false,
        });
      } catch (error) {
        expect(error.message).to.be.equal(`No matching waveId found for session ${session._id}`);
      }
    });

    it('should update a wave favorite using eventOverrides', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const sessionId = '5ba4749a0cd32de9632c9955';
      const waveId = 'wave1';
      const session = enrichedSession;
      const currentOverrides = enrichedSession.eventOverrides.get(waveId);
      overridesGetSpy.resetHistory();

      EnrichedSession.updateOne.returns({
        exec: () => ({
          nModified: 1,
        }),
      });

      await Model.patchSessionWaveDetails(userId, session, waveId, {
        favorite: false,
      });

      expect(overridesGetSpy).to.have.been.calledOnceWithExactly(waveId);
      expect(overridesSetSpy).to.have.been.calledOnceWithExactly(waveId, {
        ...currentOverrides,
        favorite: false,
      });
      expect(EnrichedSession.updateOne).to.have.been.calledOnceWithExactly(
        { _id: sessionId, user: userId },
        { eventOverrides: enrichedSession.eventOverrides },
      );
    });

    it('should increment the wave count when changing a wave to CAUGHT', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const sessionId = '5ba4749a0cd32de9632c9955';
      const waveId = 'wave1';
      const session = enrichedSession;

      const currentOverrides = enrichedSession.eventOverrides.get(waveId);

      EnrichedSession.updateOne.returns({
        exec: () => ({
          nModified: 1,
        }),
      });
      getSessionEventsStub.resolves(eventsFixture);

      await Model.patchSessionWaveDetails(userId, session, waveId, {
        outcome: 'CAUGHT',
      });

      expect(overridesSetSpy).to.have.been.calledOnceWithExactly(waveId, {
        ...currentOverrides,
        outcome: 'CAUGHT',
      });

      expect(hydrateSessionSpy).to.have.been.calledOnceWithExactly({ session: enrichedSession });
      expect(getCaughtWaveCountSpy).to.have.been.calledOnce();

      expect(EnrichedSession.updateOne).to.have.been.calledOnceWithExactly(
        { _id: sessionId, user: userId },
        {
          eventOverrides: enrichedSession.eventOverrides,
          waveCount: enrichedSession.waveCount + 1,
        },
      );
    });

    it('should decrement the wave count when changing a wave to ATTEMPTED', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const sessionId = '5ba4749a0cd32de9632c9955';
      const waveId = 'wave2';
      const session = enrichedSession;

      const currentOverrides = enrichedSession.eventOverrides.get(waveId);

      EnrichedSession.updateOne.returns({
        exec: () => ({
          nModified: 1,
        }),
      });
      getSessionEventsStub.resolves(eventsFixture);

      await Model.patchSessionWaveDetails(userId, session, waveId, {
        outcome: 'ATTEMPTED',
      });

      expect(overridesSetSpy).to.have.been.calledOnceWithExactly(waveId, {
        ...currentOverrides,
        outcome: 'ATTEMPTED',
      });

      expect(hydrateSessionSpy).to.have.been.calledOnceWithExactly({ session: enrichedSession });
      expect(getCaughtWaveCountSpy).to.have.been.calledOnce();

      expect(EnrichedSession.updateOne).to.have.been.calledOnceWithExactly(
        { _id: sessionId, user: userId },
        {
          eventOverrides: enrichedSession.eventOverrides,
          waveCount: enrichedSession.waveCount - 1,
        },
      );
    });

    it('should not change the wave count if the outcome passed it the same as the current one', async () => {
      const userId = '5ce2edc6f8c849008de1d27e';
      const sessionId = '5ba4749a0cd32de9632c9955';
      const waveId = 'wave1';
      const session = enrichedSession;

      const currentOverrides = enrichedSession.eventOverrides.get(waveId);

      EnrichedSession.updateOne.returns({
        exec: () => ({
          nModified: 1,
        }),
      });
      getSessionEventsStub.resolves(eventsFixture);

      await Model.patchSessionWaveDetails(userId, session, waveId, {
        outcome: 'ATTEMPTED',
        favorite: false,
      });

      expect(overridesSetSpy).to.have.been.calledOnceWithExactly(waveId, {
        ...currentOverrides,
        outcome: 'ATTEMPTED',
        favorite: false,
      });

      expect(hydrateSessionSpy).to.not.have.been.called();
      expect(getCaughtWaveCountSpy).to.not.have.been.called();

      expect(EnrichedSession.updateOne).to.have.been.calledOnceWithExactly(
        { _id: sessionId, user: userId },
        {
          eventOverrides: enrichedSession.eventOverrides,
        },
      );
    });
  });
});
