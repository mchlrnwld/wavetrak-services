import mongoose from 'mongoose';
import { expect } from 'chai';
import camSchema from './Cam';

describe('Cam schema', () => {
  it('instantiates cam', async () => {
    const Cam = mongoose.model('Cam', camSchema);
    const newCam = new Cam();
    try {
      await newCam.validate();
    } catch (err) {
      expect(newCam.id).to.be.equal(null);
      expect(newCam.title).to.be.equal(null);
      expect(newCam.alias).to.be.equal(null);
      expect(newCam.stillUrl).to.be.equal(null);
      expect(newCam.isPremium).to.be.equal(false);
      expect(newCam.visible).to.be.equal(true);
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
    }
  });
});
