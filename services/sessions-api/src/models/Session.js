import newrelic from 'newrelic';
import { NotFound, APIError, ServerError } from '@surfline/services-common';
import mongoose from 'mongoose';
import camSchema from './Cam';
import statsSchema from './Stats';
import spotReportViewSchema from './SpotReportView';
import extendSchema from '../helpers/extendSchema';
import { getTrustedClientName } from '../external/auth';

const latitude = Number;
const longitude = Number;

export const sessionSchema = new mongoose.Schema(
  {
    startTimestamp: { type: Date, required: [true, 'Start Timestamp is required'] },
    endTimestamp: { type: Date, required: [true, 'End Timestamp is required'] },
    partner: {
      stats: statsSchema,
    },
    visibility: {
      type: String,
      required: [true, 'Visibility is required'],
      enum: ['PUBLIC', 'PRIVATE'],
    },
    userSpotId: { type: mongoose.Schema.Types.ObjectId },
    originalSpotId: { type: mongoose.Schema.Types.ObjectId },
    name: { type: String, required: [true, 'Session name is required'] },
    rating: { type: Number },
    waveCount: { type: Number, required: [true, 'Wave Count is required'] },
    speedMax: { type: Number, required: [true, 'Max Speed is required'] },
    distancePaddled: { type: Number, required: [true, 'Distance Paddled is required'] },
    distanceWaves: { type: Number, required: [true, 'Wave Distance is required'] },
    distanceLongestWave: { type: Number, default: 0 },
    deviceType: { type: String, required: [true, 'Device type is required'] },
    surfline: {
      stats: statsSchema,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserInfo',
      required: [true, 'User ID is required'],
      index: true,
    },
    client: { type: mongoose.Schema.Types.ObjectId, required: [true, 'Client ID is required'] },
    spot: {
      id: {
        type: mongoose.Schema.Types.ObjectId,
      },
      name: {
        type: String,
      },
      location: {
        type: {
          type: String,
          enum: ['Point'],
        },
        coordinates: [longitude, latitude],
      },
    },
    status: {
      type: String,
      enum: ['FAILED', 'PENDING', 'DELETED'],
      index: 1,
    },
    gpsCenter: {
      type: {
        type: String,
        enum: ['Point'],
        required: true,
        default: 'Point',
      },
      coordinates: {
        type: [longitude, latitude],
        required: true,
      },
    },
    notified: { type: Boolean, default: false },
    startLatLon: { type: String },
    appVersion: { type: String },
    watchOSVersion: { type: String },
    watchAppVersion: { type: String },
  },
  {
    timestamps: true,
  },
);

const clipSchema = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId },
  eventId: { type: mongoose.Schema.Types.ObjectId },
});

const eventOverrideSchema = new mongoose.Schema(
  {
    outcome: {
      type: String,
      enum: ['ATTEMPTED', 'CAUGHT'],
    },
    favorite: Boolean,
    userVerified: Boolean,
  },
  { _id: false },
);

export const enrichedSessionSchema = extendSchema(
  sessionSchema,
  {
    cams: [camSchema],
    spotReportView: spotReportViewSchema,
    status: {
      type: String,
      enum: ['ENRICHED', 'FAILED', 'PENDING_RE-ENRICH'],
    },
    clips: [clipSchema],
    eventOverrides: {
      type: Map,
      of: eventOverrideSchema,
    },
  },
  { timestamps: true },
);

sessionSchema.index({ user: 1, _id: 1 }, { unique: true });
sessionSchema.index({ user: 1, status: 1, startTimestamp: -1 });
sessionSchema.index({
  _id: 1,
  user: 1,
  'cams._id': 1,
});
sessionSchema.index({ user: 1, client: 1 }, { unique: true });
sessionSchema.index({ user: 1, startTimestamp: -1 }, { unique: true });
sessionSchema.index({ 'clips._id': -1 }, { sparse: true, name: 'Clip ID Descending Index' });
sessionSchema.index({ status: -1, createdAt: -1 }, { name: 'Status and Creation Time Index' });
sessionSchema.index({ 'spot.id': 1 }, { name: 'Spot ID Ascending Index' });

const Session = mongoose.model('Session', sessionSchema);
const EnrichedSession = mongoose.model('EnrichedSession', enrichedSessionSchema, 'sessions');

export const createSession = model => new Session(model);

/**
 * @param {Omit<ValidatedSession, "events">} session
 */
export const insertSession = async session => {
  try {
    await Session.create(session);
  } catch (error) {
    newrelic.noticeError(`Session insertion failed: ${error.message}`, {
      event: 'Insertion Error',
    });
    throw new ServerError({ message: 'Session insertion failed' });
  }
};

export const removeSession = (sessionId, userId) =>
  Session.deleteOne({
    user: mongoose.Types.ObjectId(userId),
    _id: mongoose.Types.ObjectId(sessionId),
  });

export const getSessionByClipId = async clipId =>
  EnrichedSession.findOne({
    'clips._id': clipId,
  }).lean();

export const getEnrichedSession = async (userId, sessionId) => {
  if (!sessionId) throw new APIError('Cannot find sessions with invalid sessionId');

  const session = await EnrichedSession.findOne({
    _id: mongoose.Types.ObjectId(sessionId),
    user: mongoose.Types.ObjectId(userId),
  }).lean();

  if (!session) {
    throw new NotFound('Session not found for user');
  }
  const clientName = await getTrustedClientName(session.client);

  return { ...session, clientName };
};

export const getEnrichedSessionById = async sessionId => {
  if (!sessionId) throw new APIError('Cannot find sessions with invalid sessionId');

  const session = await EnrichedSession.findOne({
    _id: mongoose.Types.ObjectId(sessionId),
  }).lean();

  if (!session) {
    throw new NotFound('Session not found for user');
  }

  const clientName = await getTrustedClientName(session.client);

  return { ...session, clientName };
};

/**
 * @typedef {Object} Session
 * @property {string} createdAt
 * @property {string} endTimestamp
 * @property {string} startTimestamp
 * @property {string} client
 * @property {string} user
 * @property {string} name
 * @property {string} _id
 * @property {('ENRICHED'|'PENDING'|'FAILED'|'PENDING_RE-ENRICH')} status
 * @property {[string]} clips
 * @property {[{_id: string, title: string }]} cams
 *
 */

/**
 * Get user sessions.
 *
 * @async
 * @function getUserSessions
 * @param {string} userId
 * @return {Promise<[Session]>}
 */
export const getUserSessions = async userId => {
  if (!userId) throw new APIError('Cannot find user sessions without a user ID.');

  const sessions = await EnrichedSession.find({ user: userId })
    .select({
      spot: 1,
      clips: 1,
      startTimestamp: 1,
      endTimestamp: 1,
      client: 1,
      user: 1,
      name: 1,
      status: 1,
      createdAt: 1,
      cams: 1,
    })
    .lean();

  return sessions;
};

/**
 * Update notification flag for a Session
 *
 * @async
 * @function updateSessionNotification
 * @param {string} sessionId
 * @param {Boolean} notified
 * @return {Promise<[Session]>}
 */
export const updateSessionNotification = async (sessionId, notified) => {
  if (!sessionId) throw new APIError('Cannot find sessions with invalid sessionId');
  const session = await EnrichedSession.findOneAndUpdate(
    {
      _id: mongoose.Types.ObjectId(sessionId),
    },
    { notified },
  ).lean();
  return session;
};

export default Session;
export { EnrichedSession };
