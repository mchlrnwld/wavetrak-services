import mongoose from 'mongoose';
import { expect } from 'chai';
import statsSchema from './Stats';

describe('Stats schema', () => {
  it('instantiates stats', async () => {
    const Stats = mongoose.model('Stats', statsSchema);
    const newStats = new Stats();
    try {
      await newStats.validate();
    } catch (err) {
      expect(newStats.waveCount).to.be.equal(0);
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.startTimestamp).to.be.ok();
      expect(err.errors.startTimestamp.message).to.equal('Start Timestamp is required');
      expect(err.errors.endTimestamp).to.be.ok();
      expect(err.errors.endTimestamp.message).to.equal('End Timestamp is required');
    }
  });
});
