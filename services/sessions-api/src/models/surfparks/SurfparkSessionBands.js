/* eslint-disable no-useless-catch */
import mongoose from 'mongoose';

const surfparksSessionBands = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId },
  rfid: { type: String, required: [true, 'RFID band number is required'] },
  surfparkId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, 'Registered band requires a park ID'],
  },
  activeDate: { type: Date, required: [true, 'activeDate date-time is required'] },
  createdDate: { type: Date, required: [true, 'createdDate date-time is required'] },
  updatedDate: { type: Date, required: [true, 'updatedDate date-time is required'] },
  totalPaidSessions: { type: Number, default: 0 },
  userId: { type: mongoose.Schema.Types.ObjectId, default: null },
});

// unique indexing with RFID and dateTime --> catch error and return data that is already stored against this band mapping.
surfparksSessionBands.index({ rfid: 1, activeDate: 1 }, { unique: true });

const SurfparkSessionBandModel = mongoose.model('SurfparkSessionBand', surfparksSessionBands);

/**
 * @description creates a surfpark band object
 * @param {object} surfparkSessionBandObject object
 */
export const createSurfparkSessionBand = async surfparkSessionBandObject => {
  const nowDate = new Date();
  const date = `${nowDate.getFullYear()}/${nowDate.getMonth() + 1}/${nowDate.getDate()}`;

  const surfparkBand = new SurfparkSessionBandModel({
    _id: new mongoose.Types.ObjectId(),
    ...surfparkSessionBandObject,
    // active date is the date at which the person is surfing (this might be different from createdDate in the future)
    activeDate: date,
    createdDate: date,
    updatedDate: date,
  });
  return surfparkBand.save();
};

export default SurfparkSessionBandModel;
