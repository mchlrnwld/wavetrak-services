import mongoose from 'mongoose';
import newrelic from 'newrelic';
import { NotFound, APIError } from '@surfline/services-common';
import Session, { EnrichedSession, insertSession } from './Session';
import SessionUser from './SessionUser';
import { toUnixTimestamp } from '../utils/datetime';
import getDefaultSessionName from '../utils/getDefaultSessionName';
import * as analytics from '../common/analytics';
import validateEnrichedSession from '../helpers/validateEnrichedSession';
import { getSessionEvents, rollbackEventsUpload, uploadSessionEvents } from '../external/aws';
import hydrateEvents from '../helpers/hydrateEvents';
import { addEventsToSessions, hydrateSession } from '../helpers/sessionEvents';
import getCaughtWaveCount from '../helpers/getCaughtWaveCount';
import config from '../config';

export const getSessions = async userId => {
  if (!userId) throw new APIError('Cannot find sessions with invalid userId');

  const sessionsData = await Session.find({ user: mongoose.Types.ObjectId(userId) })
    .sort('-startTimestamp')
    .lean();

  const sessions = await Promise.all(
    sessionsData.map(async session => {
      const rawEvents = await getSessionEvents(session._id);
      const events = hydrateEvents(rawEvents, session.clips, session.eventOverrides);
      const sessionObject = {
        id: session._id,
        name: session.name,
        startTimestamp: toUnixTimestamp(session.startTimestamp),
        endTimestamp: toUnixTimestamp(session.endTimestamp),
        waveCount: session.waveCount,
        speedMax: session.speedMax,
        distancePaddled: session.distancePaddled,
        distanceWaves: session.distanceWaves,
        longestWave: {
          distance: session.distanceLongestWave,
          seconds: 10,
        },
        spot: {
          id: session?.spot?.id ? mongoose.Types.ObjectId(session.spot.id) : null,
          name: session?.spot?.name,
        },
        clips: session.clips,
        events: events.map(event => ({
          id: event._id,
          startTimestamp: toUnixTimestamp(event.startTimestamp),
          endTimestamp: toUnixTimestamp(event.endTimestamp),
          type: event.type,
          outcome: event.outcome,
          favorite: event.favorite,
          clips: event.clips,
          distance: event.distance,
          speedMax: event.speedMax,
          speedAverage: event.speedAverage,
          positionsLength: event.positions.length,
          positions: event.positions.map(position => ({
            timestamp: toUnixTimestamp(position.timestamp),
            latitude: position.location.coordinates[1],
            longitude: position.location.coordinates[0],
          })),
        })),
      };
      return sessionObject;
    }),
  );

  return { sessions };
};

export const getSessionsByStatus = async (status, withEvents = true) => {
  const sessions = await Session.find({ status }, { user: 1, client: 1 }).lean();
  if (withEvents) {
    return addEventsToSessions(sessions);
  }
  return sessions;
};

/**
 * @description Retrieves all sessions that have the status `PENDING` and are
 * more than an hour old (not recently synced so we assume `markSessionFailed`
 * was not successful) and all sessions with status `PENDING_RE-ENRICH`
 */
export const getStalePendingSessions = async () => {
  const oneHourAgoDate = new Date(new Date() - 1000 * 60 * 60 * 1);
  const sessions = await Session.find(
    {
      $or: [
        {
          status: 'PENDING',
          createdAt: { $lt: oneHourAgoDate },
        },
        { status: 'PENDING_RE-ENRICH' },
      ],
    },
    { user: 1, client: 1 },
  ).lean();

  return sessions;
};

export const getSessionById = async sessionId => {
  if (!sessionId) throw new APIError('Cannot find sessions with invalid sessionId');

  const session = await Session.findOne({
    _id: mongoose.Types.ObjectId(sessionId),
  }).lean();

  return session;
};

export const getSessionByClipId = async clipId => {
  if (!clipId) throw new APIError('Cannot find session with invalid clipId');

  const session = await EnrichedSession.findOne({ 'clips._id': clipId }).lean();

  return session;
};

/**
 * Given a user, client, and a session this function performs
 * validations, builds the session object, and then stores it
 * in mongo.
 * @param {String} userId
 * @param {String} clientId
 * @param {ValidatedSessionWithPartner} validatedSession
 * @param {{latitude: number, longitude: number}} gpsCenter
 */
export const addSession = async (userId, clientId, validatedSession, gpsCenter, watch) => {
  const firstCoordinates = validatedSession.events[0].positions[0].location.coordinates;

  const sessionName = getDefaultSessionName({
    timestamp: validatedSession.startTimestamp,
    coordinates: firstCoordinates,
  });

  let startLatLon = '';
  if (firstCoordinates) {
    // First coordinates are GeoJSON which is an array [longitude, latitude]
    startLatLon = [...firstCoordinates].reverse().join(',');
  }

  const session = {
    ...validatedSession,
    ...watch,
    user: mongoose.Types.ObjectId(userId),
    client: mongoose.Types.ObjectId(clientId),
    name: sessionName,
    status: 'PENDING',
    startLatLon,
    gpsCenter: {
      type: 'Point',
      coordinates: [gpsCenter.longitude, gpsCenter.latitude],
    },
    appVersion: config.APP_VERSION,
  };

  // Generate a Session ID
  const sessionId = mongoose.Types.ObjectId();
  session._id = sessionId;

  const { events } = session;

  // Insert events to S3 with insertedId as filename
  await uploadSessionEvents(sessionId, events);

  // Strip out events for Mongo
  delete session.events;

  try {
    // Insert to mongo without events and rollback the events upload to S3 if something goes wrong
    // so that we do not have dangling S3 objects
    await insertSession(session);
  } catch (error) {
    await rollbackEventsUpload(sessionId);
    throw error;
  }

  analytics.track({
    event: 'Added Session',
    userId: session.user.toString(),
    properties: {
      sessionId,
      client: session.client.toString(),
      waveCount: session.waveCount,
      maxSpeed: session.speedMax,
      maxDistance: session.distanceLongestWave,
      startLatLon,
      startTimestamp: session.startTimestamp,
      stopTimestamp: session.endTimestamp,
    },
  });

  return session;
};

/**
 * Updates given session's details
 * @param {String} userId
 * @param {String} sessionId
 * @param {EnrichedSession} sessionJson
 * @returns {Promise<EnrichedSession>}
 */
export const updateSessionDetails = async (userId, sessionId, sessionJson) => {
  if (!sessionId) throw new APIError('Cannot update with invalid sessionId');

  const events = await getSessionEvents(sessionId);

  // Perform the validation of entire session payload, including the events
  const validatedEnrichedSession = await newrelic.startSegment(
    'updatedSessionDetails:validateSession',
    true,
    () => validateEnrichedSession({ ...sessionJson, events }),
  );

  // Strip out events for Mongo
  delete validatedEnrichedSession.events;

  const session = await EnrichedSession.findOneAndUpdate(
    {
      _id: mongoose.Types.ObjectId(sessionId),
      user: mongoose.Types.ObjectId(userId),
    },
    {
      $set: {
        ...validatedEnrichedSession,
      },
    },
    {
      new: true,
      runValidators: true,
    },
  ).exec();

  return session;
};

/**
 * Patches given session's details
 * @param {String} userId
 * @param {String} sessionId
 * @param {Object} changedFields
 * @param {boolean} [needsReEnrichment]
 * @returns {Promise<EnrichedSession>} Enriched Session
 */
export const patchSessionDetails = async (
  userId,
  sessionId,
  changedFields,
  needsReEnrichment = false,
) => {
  if (!sessionId) throw new APIError('Cannot update with invalid sessionId');

  const session = await EnrichedSession.findOneAndUpdate(
    {
      _id: mongoose.Types.ObjectId(sessionId),
      user: mongoose.Types.ObjectId(userId),
    },
    {
      ...changedFields,
      // Update the status if we need to re-enrich
      ...(needsReEnrichment && { status: 'PENDING_RE-ENRICH' }),
    },
    {
      new: true,
      runValidators: true,
    },
  );

  return session;
};

/**
 * Patches given session's wave details
 * @param {Object} session
 * @param {String} waveId
 * @param {Object} changedFields
 * @returns {Promise<EnrichedSession>} Enriched Session
 */
export const patchSessionWaveDetails = async (userId, session, waveId, changedFields) => {
  if (!session) throw new APIError('Cannot update with invalid sessionId');
  if (!waveId) throw new APIError('Cannot update with invalid waveId');

  const { _id, eventOverrides, events } = session;
  let { waveCount } = session;

  const matchedWave = events.find(event => event._id.toString() === waveId);

  if (!matchedWave) {
    throw new NotFound(`No matching waveId found for session ${_id}`);
  }

  const currentOverrides = eventOverrides.get(waveId);
  eventOverrides.set(waveId, { ...currentOverrides, ...changedFields });

  const outcomeChanged =
    changedFields.outcome && changedFields.outcome !== currentOverrides?.outcome;

  if (outcomeChanged) {
    // Update the waveCount to reflect the changed wave
    const { events: hydratedEvents } = await hydrateSession({ session });
    waveCount = await getCaughtWaveCount(hydratedEvents);
  }

  await EnrichedSession.updateOne(
    { _id, user: userId },
    { eventOverrides, ...(outcomeChanged && { waveCount }) },
  ).exec();

  return true;
};

export const updateSessionStatus = async (userId, sessionId, status) => {
  if (!sessionId) throw new APIError('Cannot update with invalid sessionId');

  const conditions = {
    _id: mongoose.Types.ObjectId(sessionId),
    user: mongoose.Types.ObjectId(userId),
  };

  const session = await Session.findOneAndUpdate(
    conditions,
    {
      $set: {
        status,
      },
    },
    { runValidators: true },
  );
  return session;
};

export const deleteSession = async (userId, sessionId) => {
  if (!sessionId) throw new APIError('Cannot delete with invalid sessionId');

  return updateSessionStatus(userId, sessionId, 'DELETED');
};

export const getSession = async (userId, sessionId) => {
  if (!sessionId) throw new APIError('Cannot find sessions with invalid sessionId');

  const session = await Session.findOne({
    _id: mongoose.Types.ObjectId(sessionId),
    user: mongoose.Types.ObjectId(userId),
  }).lean();

  return session;
};

export const getSessionsSummary = async (userId, start, end) => {
  if (!userId) throw new APIError('Cannot find sessions with invalid userId');
  if (!start || !end) throw new APIError('Cannot find sessions with invalid date range');

  const sessions = await EnrichedSession.find({
    $and: [
      { user: mongoose.Types.ObjectId(userId) },
      { startTimestamp: { $gte: start, $lte: end } },
    ],
  })
    .sort({
      startTimestamp: 'desc',
    })
    .lean();

  const sessionsSummary = await addEventsToSessions(sessions);
  return sessionsSummary;
};

export const getSessionFeed = async (userId, limit, offset) => {
  if (!userId) throw new APIError('Cannot find sessions with invalid userId');

  const sessions = await EnrichedSession.find({
    user: mongoose.Types.ObjectId(userId),
    status: {
      $ne: 'DELETED',
    },
  })
    .limit(limit)
    .skip(offset)
    .sort({
      startTimestamp: 'desc',
    })
    .lean();

  const sessionFeed = await addEventsToSessions(sessions);
  return sessionFeed;
};

export const updateSessionCamVisibility = async (userId, sessionId, cameraId, visible) => {
  if (!userId) throw new APIError('Cannot find sessions with invalid userId');
  if (!sessionId) throw new APIError('Cannot find sessions with invalid sessionId');

  return EnrichedSession.findOneAndUpdate(
    {
      _id: sessionId,
      user: mongoose.Types.ObjectId(userId),
      cams: { $elemMatch: { id: { $eq: cameraId } } },
    },
    { $set: { 'cams.$.visible': visible } },
    { runValidators: true },
  );
};

export const getSessionUser = async sessionUserId => {
  if (!sessionUserId) throw new APIError('Cannot find user with invalid session user ID');
  const sessionUser = await SessionUser.find({
    _id: mongoose.Types.ObjectId(sessionUserId),
  }).lean();
  return sessionUser;
};

export const getSessionUserByUserId = async user => {
  if (!user) throw new APIError('Cannot find user with invalid user ID');
  const sessionUser = await SessionUser.findOne({
    user: mongoose.Types.ObjectId(user),
  }).lean();
  return sessionUser;
};
