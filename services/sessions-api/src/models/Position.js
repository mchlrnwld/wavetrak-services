import mongoose from 'mongoose';
import 'mongoose-geojson-schema';

export const validatePosition = {
  // TODO
  validator: function validatePosition(position) {
    return position;
  },
  message: 'Position must be valid',
};

export const positionSchema = new mongoose.Schema(
  {
    timestamp: { type: Date, required: [true, 'Timestamp is required'] },
    location: {
      index: '2dsphere',
      type: mongoose.Schema.Types.Point,
      required: [true, 'Position location is required'],
    },
    horizontalAccuracy: {
      type: Number,
    },
  },
  {
    _id: false,
  },
);
