import mongoose from 'mongoose';
import { positionSchema } from './Position';
import extendSchema from '../helpers/extendSchema';

export const validateEvent = {
  // TODO
  validator: function validateEvent(event) {
    return event;
  },
  message: 'Event must be valid',
};

export const eventSchema = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId },
  startTimestamp: { type: Date, required: [true, 'Start Timestamp is required'] },
  endTimestamp: { type: Date, required: [true, 'End Timestamp is required'] },
  distance: { type: Number, required: [true, 'Distance is required'] },
  speedMax: { type: Number, required: [true, 'Max speed is required'] },
  speedAverage: { type: Number, required: [true, 'Average speed is required'] },
  positions: [positionSchema],
  type: {
    type: String,
    required: [true, 'Type is required'],
    enum: ['PADDLE', 'WAVE'],
  },
  outcome: {
    type: String,
    enum: ['ATTEMPTED', 'CAUGHT'],
  },
});

export const enrichedEventSchema = extendSchema(eventSchema, {
  clips: [mongoose.Schema.Types.ObjectId],
  favorite: { type: Boolean, default: false },
});
