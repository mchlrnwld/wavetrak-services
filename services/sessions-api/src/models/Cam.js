import mongoose from 'mongoose';

const camSchema = new mongoose.Schema(
  {
    id: { type: mongoose.Schema.Types.ObjectId },
    title: { type: String, default: null },
    alias: { type: String, default: null },
    stillUrl: { type: String, default: null },
    isPremium: { type: Boolean, default: false },
    visible: { type: Boolean, default: true },
  },
  { _id: false },
);

export default camSchema;
