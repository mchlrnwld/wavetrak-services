import mongoose from 'mongoose';
import { expect } from 'chai';
import spotReportViewSchema from './SpotReportView';

describe('SpotReportView Model', () => {
  it('instantiates spot report views', async () => {
    const SpotReportView = mongoose.model('SpotReportView', spotReportViewSchema);
    const model = { _id: '583f491b4e65fad6a7701941', name: 'HB Pier, Southside' };
    const spotReportView = new SpotReportView(model);
    expect(spotReportView).to.be.an.instanceof(SpotReportView);
    expect(spotReportView.name).to.equal('HB Pier, Southside');
    expect(spotReportView.legacyRegionId).to.be.null();

    try {
      await spotReportView.validate();
    } catch (err) {
      expect(err.name).to.equal('ValidationError');
      expect(err.errors.name).to.not.exist();
      expect(err.errors.lat).to.be.ok();
      expect(err.errors.lat.message).to.equal('Path `lat` is required.');
      expect(err.errors.lon).to.be.ok();
      expect(err.errors.lon.message).to.equal('Path `lon` is required.');
    }
  });
});
