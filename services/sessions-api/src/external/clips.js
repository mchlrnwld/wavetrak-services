import fetch from 'node-fetch';
import _flatten from 'lodash/flatten';
import newrelic from 'newrelic';
import config from '../config';

// eslint-disable-next-line import/prefer-default-export
export const getClips = async clips => {
  // Result is an array of URLs containing the correct param strings (can be 1 URL)
  const CHUNK_SIZE = 70;
  const CLIPS_BASE_URL = `${config.CAMERAS_API}/clips?ids=`;
  const clipFetches = new Array(Math.ceil(clips.length / CHUNK_SIZE))
    .fill()
    .map((_, chunkNum) => clips.slice(chunkNum * CHUNK_SIZE, chunkNum * CHUNK_SIZE + CHUNK_SIZE))
    .map(clipIds =>
      fetch(`${CLIPS_BASE_URL}${clipIds.join(',')}`).then(response => response.json()),
    );

  const results = await Promise.all(clipFetches);

  try {
    const clipResponse = _flatten(results).map(sessionClip => {
      const clipUrl = sessionClip.clip?.url ? sessionClip.clip.url : null;
      const thumbnail = sessionClip.thumbnail?.url
        ? {
            url: sessionClip.thumbnail.url,
            sizes: sessionClip.thumbnail.sizes.map(String),
          }
        : null;

      const {
        status,
        clipId,
        cameraId,
        startTimestampInMs: startTimestamp,
        endTimestampInMs: endTimestamp,
      } = sessionClip;

      return {
        clipUrl,
        cameraId,
        startTimestamp,
        endTimestamp,
        thumbnail,
        status,
        clipId,
      };
    });
    return clipResponse || [];
  } catch (err) {
    newrelic.noticeError(err);
    throw err;
  }
};
