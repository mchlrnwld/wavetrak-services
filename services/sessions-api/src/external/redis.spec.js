import { expect } from 'chai';
import sinon from 'sinon';
import * as servicesCommon from '@surfline/services-common';
import { createPromisifiedRedisInstance, getFitFileTTL, setFitFileKey } from './redis';

describe('external / redis', () => {
  const getPromisifiedRedisStub = sinon.stub(servicesCommon, 'getPromisifiedRedis');
  const ttlStub = sinon.stub();
  const expireStub = sinon.stub();
  const setStub = sinon.stub();
  const getStub = sinon.stub();
  const decrStub = sinon.stub();

  getPromisifiedRedisStub.returns({
    ttl: ttlStub,
    expire: expireStub,
    set: setStub,
    get: getStub,
    decr: decrStub,
  });

  before(() => {
    createPromisifiedRedisInstance();
  });

  afterEach(() => {
    ttlStub.resetHistory();
    expireStub.resetHistory();
    setStub.resetHistory();
    getStub.resetHistory();
    decrStub.resetHistory();
  });

  describe('getFitFileTTL', () => {
    it('should return the TTL for the correct key', async () => {
      ttlStub.resolves(1);
      const result = await getFitFileTTL(
        '123',
        'https://healthapi.garmin.com/wellness-api/rest/activityFile?id=456',
      );
      expect(result).to.be.equal(1);
      expect(ttlStub).to.be.have.been.calledOnceWithExactly('123:456:fit-file-key');
    });
  });

  describe('setFitFileKey', async () => {
    it('should set the TTL for the correct key', async () => {
      setStub.resolves('OK');
      const result = await setFitFileKey(
        '123',
        'https://healthapi.garmin.com/wellness-api/rest/activityFile?id=456',
      );
      expect(result).to.be.equal('OK');
      expect(setStub).to.be.have.been.calledOnceWithExactly(
        '123:456:fit-file-key',
        'synced',
        'EX',
        10080,
      );
    });
  });
});
