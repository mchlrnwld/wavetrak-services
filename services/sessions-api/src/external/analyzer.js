/* istanbul ignore file */

import fetch from 'node-fetch';
import newrelic from 'newrelic';
import { APIError } from '@surfline/services-common';
import config from '../config';

/**
 * @description Makes a call to the session analyzer service to analyze a surfline
 * session
 * @param {{ positions: [{ latitude: number, longitude: number, timestamp: number }] }} gpsData
 * @param {{latitude: number, longitude: number}} gpsCenter
 */
const analyzeSessionGps = async (gpsData, gpsCenter) => {
  const url = `${config.SESSION_ANALYZER}/analyze`;
  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({ ...gpsData, gpsCenter }),
    headers: { 'Content-Type': 'application/json' },
  });

  const body = await response.json();
  const bodySize = response.headers.get('content-length') || 0;

  if (response.status !== 200 || response.body.error) {
    newrelic.noticeError(body.message || body);
    if (response.status === 400) {
      throw new APIError(body.message, 400);
    }
    throw response;
  }
  return { body, bodySize };
};

export default analyzeSessionGps;
