import { getPromisifiedRedis } from '@surfline/services-common';

/** @type {import('@surfline/services-common').PromisifiedRedis} */
let redis;

export const createPromisifiedRedisInstance = () => {
  redis = getPromisifiedRedis();
};

const FIT_FILE_KEY_EXPIRATION = 10080; // 7 days in seconds

/**
 * @param {string} userAccessToken
 * @param {string} callbackURL
 */
const createFitFileKey = (userAccessToken, callbackURL) => {
  // eslint-disable-next-line no-unused-vars
  const [_, id] = callbackURL.split('?id=');

  return `${userAccessToken}:${id}:fit-file-key`;
};

/**
 * @description Retrieves the key of a fit file (if it exists) to check if the fit file has already
 * been processed
 * @param {string} userAccessToken
 * @param {string} callbackURL
 * @returns {Promise<number>}
 */
export const getFitFileTTL = (userAccessToken, callbackURL) =>
  redis.ttl(createFitFileKey(userAccessToken, callbackURL));

/**
 * @description Sets the key of a fit file to ensure we do not re-process the same fit file
 * @param {string} userAccessToken
 * @param {string} callbackURL
 */
export const setFitFileKey = (userAccessToken, callbackURL) =>
  redis.set(
    createFitFileKey(userAccessToken, callbackURL),
    'synced',
    'EX',
    FIT_FILE_KEY_EXPIRATION,
  );
