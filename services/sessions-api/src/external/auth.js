/* istanbul ignore file */

import fetch from 'node-fetch';
import config from '../config';

// eslint-disable-next-line import/prefer-default-export
export const getTrustedClientName = async client => {
  const baseUrl = `${config.AUTH_SERVICE}/trusted/client-name`;
  const url = `${baseUrl}?clientId=${client}`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status && body.clientName) {
    if (body.clientName.status) {
      return null;
    }
    return body.clientName;
  }
  throw body;
};
