/* istanbul ignore file */

import fetch from 'node-fetch';
import config from '../config';
import createParamString from './createParamString';

export const getUserSettings = async (userId, geoCountryIso) => {
  const params = { geoCountryIso };
  const url = `${config.USER_SERVICE}/user/settings?${createParamString(params)}`;
  const headers = {};
  if (userId) {
    headers['x-auth-userid'] = userId;
  }

  const response = await fetch(url, {
    method: 'GET',
    headers,
  });
  const body = await response.json();
  return body;
};

export const getUserName = async userId => {
  try {
    const url = `${config.USER_SERVICE}/user`;
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'x-auth-userid': userId,
      },
    });
    const body = await response.json();

    return {
      firstName: body.firstName,
      lastName: body.lastName,
    };
  } catch (error) {
    return {
      firstName: null,
      lastName: null,
    };
  }
};

/**
 * @param {string} accessToken
 * @returns {{ userId: string, clientId: string }}
 */
export const getUserByClientAccessToken = async accessToken => {
  const url = `${config.USER_SERVICE}/user?clientToken=${accessToken}`;

  const response = await fetch(url, {
    method: 'GET',
  });

  const body = await response.json();
  return body;
};
