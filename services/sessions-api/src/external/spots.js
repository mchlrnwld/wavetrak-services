import fetch from 'node-fetch';
import config from '../config';
import createParamString from './createParamString';

export const getNearbyTaxonomy = async (count, maxDepth, coordinates) => {
  const lat = coordinates[1];
  const lon = coordinates[0];
  const url = `${config.SPOTS_API}/taxonomy/nearby`;
  const queryString = createParamString({
    type: 'spot',
    count,
    maxDepth,
    lat,
    lon,
  });
  const response = await fetch(`${url}?${queryString}`);
  const body = await response.json();
  if (response.status === 200) return body;
  throw body;
};

export const getSpot = async spotId => {
  const url = `${config.SPOTS_API}/admin/spots/${spotId}`;
  const spot = await fetch(`${url}`);
  return spot.json();
};
