/* istanbul ignore file */

import fetch from 'node-fetch';
import config from '../config';

const postToSlack = async ({ channel, username, text }) => {
  const body = {
    channel,
    username,
    text,
  };
  const response = await fetch(config.SLACK_HOOK_URL, {
    method: 'post',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' },
  });

  const bodyResponseText = await response.text();
  if (bodyResponseText?.length && response.status === 200) {
    try {
      return JSON.parse(bodyResponseText);
    } catch (err) {
      return {};
    }
  }
  return {};
};

export default postToSlack;
