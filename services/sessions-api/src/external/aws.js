/* istanbul ignore file */

import newrelic from 'newrelic';
import AWS from 'aws-sdk';
import { ServerError } from '@surfline/services-common';

const lambda = new AWS.Lambda({ region: 'us-west-1' });

/**
 * @param {string} sessionId
 * @param {string} bucket Bucket to upload the file to.
 * @param {Object} body Body of the file to upload to s3.
 * @param {string} type Type of file to upload to s3.
 * @returns {Promise<void>}
 */
export const uploadFileToS3 = async (sessionId, bucket, body, type) => {
  const s3Params = {
    Body: JSON.stringify(body),
    Bucket: bucket,
    Key: `${sessionId}.json`,
    ContentType: 'application/json',
    ContentEncoding: 'utf8',
  };

  try {
    const s3 = new AWS.S3();
    await s3.putObject(s3Params).promise();
  } catch (err) {
    newrelic.noticeError(err, { event: `${type} Upload Error` });
    throw new ServerError({
      message: `Could not upload session ${type} for session ${sessionId} to S3`,
    });
  }
};

/**
 * @param {string} sessionId
 * @param {Events} events
 * @returns {Promise<void>}
 */
export const uploadSessionEvents = async (sessionId, events) =>
  uploadFileToS3(sessionId, process.env.SESSIONS_EVENTS_BUCKET, events, 'Events');

/**
 * @param {string} sessionId
 * @param {Object} gpsData
 * @returns {Promise<void>}
 */
export const uploadSessionGpsData = async (sessionId, gpsData) =>
  uploadFileToS3(sessionId, process.env.SESSIONS_GPS_BUCKET, gpsData, 'GPS Data');

/**
 * @param {string} sessionId
 * @returns {Promise<void>}
 */
export const rollbackEventsUpload = async sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    const s3 = new AWS.S3();
    await s3.deleteObject(s3Params).promise();
  } catch (err) {
    // We tried to rollback but failed, swallow the error but send an event so we have
    // visbility into how often this happens
    newrelic.recordCustomEvent('Sessions', {
      event: 'Events Upload Rollback Failed',
      sessionId,
      error: err.message,
    });
  }
};

export const getSessionEvents = async sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    const s3 = new AWS.S3();
    const response = await s3.getObject(s3Params).promise();
    return JSON.parse(response.Body);
  } catch (err) {
    newrelic.noticeError(err);
    throw new ServerError({
      message: `Could not retrieve session events for session ${sessionId} from S3`,
    });
  }
};

/**
 * @param {string} functionName
 */
export const invokeLambdaAsync = async (functionName, payload) => {
  const params = {
    FunctionName: functionName,
    InvocationType: 'Event',
    Payload: JSON.stringify(payload),
  };
  const result = await lambda.invoke(params).promise();
  if (result.StatusCode === 202) return true;
  throw new ServerError({ message: 'Something went wrong invoking the lambda' });
};

/**
 * @param {string} sessionId
 */
export const getS3EventsUrl = sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
    Expires: 10800, // 3 hours in seconds
  };

  const s3 = new AWS.S3();

  return s3.getSignedUrl('getObject', s3Params);
};

/**
 * @param {string} sessionId
 */
export const headS3Events = sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  const s3 = new AWS.S3();

  return s3.headObject(s3Params).promise();
};

/**
 * @param {string} sessionId
 */
export const deleteSessionEvents = sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  const s3 = new AWS.S3();

  return s3.deleteObject(s3Params).promise();
};
