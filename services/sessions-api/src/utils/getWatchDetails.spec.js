import { expect } from 'chai';
import getWatchDetails from './getWatchDetails';

describe('utils / getWatchDetails', () => {
  it('Take a valid user-agent string and returns watchOS and watchApp version.', () => {
    const req = {
      headers: {
        'User-Agent':
          'Tracking Extension/7.0.0 (com.surfline.surfapp.watchkitapp.watchkitextension; watchOS 7.2.0)',
      },
    };
    expect(getWatchDetails(req)).to.eql({
      watchOSVersion: '7.2.0',
      watchAppVersion: '7.0.0',
    });
  });
  it('Take a different valid user-agent string and returns watchOS and watchApp version.', () => {
    const req = {
      headers: {
        'User-Agent':
          'Tracking Extension/7.0.1 (com.surfline.surfapp.watchkitapp.watchkitextension; build:202102120946; watchOS 7.3.0) Alamofire/4.9.1',
      },
    };
    expect(getWatchDetails(req)).to.eql({
      watchAppVersion: '7.0.1',
      watchOSVersion: '7.3.0',
    });
  });
  it('Takes a non-watch user-agent string and returns watchOS and watchApp version as null.', () => {
    const req = {
      headers: {
        'User-Agent':
          'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.2 (KHTML, like Gecko) Ubuntu/11.10 Chromium/15.0.874.106 Chrome/15.0.874.106 Safari/535.2',
      },
    };
    expect(getWatchDetails(req)).to.eql({
      watchAppVersion: null,
      watchOSVersion: null,
    });
  });
  it('If no user-agent is present it returns undefined.', () => {
    const req = {
      headers: {},
    };
    expect(getWatchDetails(req)).to.eql(undefined);
  });
  it('Takes a non-watch user-agent string and returns watchOS and watchApp version as null.', () => {
    const req = {
      headers: {
        'User-Agent':
          'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.2 (KHTML, like Gecko) Ubuntu/11.10 Chromium/15.0.874.106 Chrome/15.0.874.106 Safari/535.2',
      },
    };
    expect(getWatchDetails(req)).to.eql({
      watchAppVersion: null,
      watchOSVersion: null,
    });
  });
  it('If no user-agent is present it returns undefined.', () => {
    const req = {
      headers: {},
    };
    expect(getWatchDetails(req)).to.eql(undefined);
  });
});
