import { expect } from 'chai';
import { convertWindSpeed, convertWaveHeight, convertTideHeight } from './convertUnits';

describe('utils / convertUnits', () => {
  describe('convertWindSpeed', () => {
    it('converts between units and rounds to the nearest whole number', () => {
      expect(convertWindSpeed(10, 'KTS', 'KPH')).to.equal(19);
      expect(convertWindSpeed(10, 'KPH', 'KTS')).to.equal(5);
      expect(convertWindSpeed(10, 'KTS', 'MPH')).to.equal(12);
      expect(convertWindSpeed(10, 'MPH', 'KTS')).to.equal(9);
    });
    it('does not convert if desiredUnits are speedUnits', () => {
      expect(convertWindSpeed(1, 'KPH', 'KPH')).to.equal(1);
      expect(convertWindSpeed(1, 'KTS', 'KTS')).to.equal(1);
      expect(convertWindSpeed(1, 'MPH', 'MPH')).to.equal(1);
    });
    it('does not convert if passed a bogus unit', () => {
      expect(convertWindSpeed(1, 'minutes', 'hours')).to.equal(1);
    });
  });
  describe('convertWaveHeight', () => {
    it('converts between units and but rounds feet to the nearest whole number', () => {
      expect(convertWaveHeight(100, 'FT', 'M')).to.equal(30.5);
      expect(convertWaveHeight(10, 'M', 'FT')).to.equal(33);
    });
    it('does not convert if desiredUnits are speedUnits', () => {
      expect(convertWaveHeight(1, 'FT', 'FT')).to.equal(1);
      expect(convertWaveHeight(1, 'M', 'M')).to.equal(1);
    });
    it('does not convert if passed a bogus unit', () => {
      expect(convertWaveHeight(1, 'minutes', 'hours')).to.equal(1);
    });
  });
  describe('convertTideHeight', () => {
    it('converts between units and but rounds feet to the nearest whole number', () => {
      expect(convertTideHeight(100, 'FT', 'M')).to.equal(30.5);
      expect(convertTideHeight(10, 'M', 'FT')).to.equal(33);
    });
    it('does not convert if desiredUnits are speedUnits', () => {
      expect(convertTideHeight(1, 'FT', 'FT')).to.equal(1);
      expect(convertTideHeight(1, 'M', 'M')).to.equal(1);
    });
    it('does not convert if passed a bogus unit', () => {
      expect(convertTideHeight(1, 'minutes', 'hours')).to.equal(1);
    });
  });
});
