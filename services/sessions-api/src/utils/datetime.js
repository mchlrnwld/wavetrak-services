import moment from 'moment-timezone';
import tzlookup from 'tz-lookup';

const utcOffsetFromTimezone = timezone =>
  moment()
    .tz(timezone)
    .utcOffset() / 60;

const timezoneData = (lat, lon, date, id, path = 'associated.timezone') => {
  const key = tzlookup(lat, lon);
  const momentDate = date || new Date();
  const momentData = moment.tz(momentDate, key);
  const abbr = momentData.zoneAbbr();
  const offset = utcOffsetFromTimezone(key);
  return {
    key,
    abbr,
    offset,
    id,
    path,
  };
};

const getTimezoneArray = (lat, lon, start, end) => {
  const startEnd = [start, end];
  let lastOffset;
  const timeZoneArray = startEnd
    .map((date, idx) => {
      const path = 'associated.timezone';
      const tzDataThis = timezoneData(lat, lon, date, idx + 1, path);
      const tzData = lastOffset !== tzDataThis.offset ? tzDataThis : null;
      lastOffset = tzDataThis.offset;
      return tzData;
    })
    .filter(tzData => !!tzData);
  return timeZoneArray;
};

const getFeedTimezoneArray = sessionFeed => {
  const timeZoneArray = [];
  const timeZoneObj = {};
  let tzCounter = 1;
  if (sessionFeed.length) {
    sessionFeed.forEach(session => {
      const event = session.events[0];
      const position = event.positions[0];
      const { timestamp } = position;
      const { coordinates } = position.location;
      const lat = coordinates[1];
      const lon = coordinates[0];
      const tzData = timezoneData(lat, lon, timestamp, tzCounter);
      const tzObjKey = `${tzData.key}${tzData.offset}`;
      if (!timeZoneObj[tzObjKey]) {
        timeZoneObj[tzObjKey] = 1;
        timeZoneArray.push(tzData);
        tzCounter += 1;
      }
    });
  }

  return timeZoneArray;
};

const getTimezoneRefByOffset = (timezoneArray, offset) => {
  const timezoneRef = timezoneArray
    .filter(tzData => tzData.offset === offset)
    .map(tzData => {
      const tzRef = {
        id: tzData.id,
        path: 'associated.timezone',
      };
      return tzRef;
    });
  return timezoneRef.length ? timezoneRef[0] : {};
};

const isValidDate = date =>
  // eslint-disable-next-line no-restricted-globals
  date && Object.prototype.toString.call(date) === '[object Date]' && !isNaN(date);

const fromUnixTimestamp = timestamp => new Date(timestamp);

const toUnixTimestamp = date => Math.floor(isValidDate(date) ? date : new Date(date));

export {
  utcOffsetFromTimezone,
  timezoneData,
  getTimezoneArray,
  getFeedTimezoneArray,
  getTimezoneRefByOffset,
  fromUnixTimestamp,
  toUnixTimestamp,
};
