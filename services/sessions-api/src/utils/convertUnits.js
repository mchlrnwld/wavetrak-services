import { round } from 'lodash';

export const convertFeetToMeters = length => length * 0.3048;
export const convertMetersToFeet = length => length / 0.3048;
export const convertKnotsToMPH = speed => speed * 1.15078;
export const convertMPHToKnots = speed => speed / 1.15078;
export const convertKnotsToKPH = speed => speed * 1.852;
export const convertKPHToKnots = speed => speed / 1.852;
export const convertMPSToMPH = speed => speed * 2.237;
export const convertMPSToKPH = speed => speed * 3.6;
export const convertFahrenheitToCelsius = temp => (temp - 32) * (5 / 9);
export const convertCelsiusToFahrenheit = temp => (temp * 9) / 5 + 32;

export const convertLength = (length, desiredUnits, lengthUnits = 'M') => {
  if (desiredUnits === lengthUnits) return round(length, 2);
  if (desiredUnits === 'FT') {
    return round(convertMetersToFeet(length), 2);
  }
  return round(convertFeetToMeters(length), 2);
};

export const convertTemperature = (temp, tempUnits, desiredUnits) => {
  if (desiredUnits === tempUnits) return round(temp);
  if (desiredUnits === 'F') return round(convertCelsiusToFahrenheit(temp));
  return round(convertFahrenheitToCelsius(temp));
};

export const convertWindSpeed = (speed, speedUnits, desiredUnits) => {
  if (speedUnits === 'KTS' && desiredUnits === 'MPH') return round(convertKnotsToMPH(speed));
  if (speedUnits === 'KTS' && desiredUnits === 'KPH') return round(convertKnotsToKPH(speed));
  if (speedUnits === 'MPH' && desiredUnits === 'KTS') return round(convertMPHToKnots(speed));
  if (speedUnits === 'KPH' && desiredUnits === 'KTS') return round(convertKPHToKnots(speed));
  return speed;
};

export const convertSpeed = (speedMPS, desiredUnits) => {
  if (desiredUnits === 'MPH') {
    return round(convertMPSToMPH(speedMPS), 2);
  }
  return round(convertMPSToKPH(speedMPS), 2);
};

export const convertWaveHeight = (height, heightUnits, desiredUnits) => {
  if (heightUnits === 'FT' && desiredUnits === 'M') return round(convertFeetToMeters(height), 1);
  if (heightUnits === 'M' && desiredUnits === 'FT') return round(convertMetersToFeet(height));
  return height;
};

export const convertTideHeight = (height, heightUnits, desiredUnits) => {
  if (heightUnits === 'FT' && desiredUnits === 'M') return round(convertFeetToMeters(height), 1);
  if (heightUnits === 'M' && desiredUnits === 'FT') return round(convertMetersToFeet(height));
  return height;
};
