import newrelic from 'newrelic';
import { APIError } from '@surfline/services-common';

/**
 * @description Given an array of session positions, this helper will
 * convert the events to an array of positions used to calculate the
 * GPS centroid
 * @param {[{ positions: [{ location: { coordinates: [number, number] } }] }]} events
 * @returns {[{ longitude: number, latitude: number }]}
 */
export const convertEventsToPositions = events =>
  events
    .map(event =>
      event.positions.map(position => {
        const [longitude, latitude] = position.location.coordinates;
        return { longitude, latitude };
      }),
    )
    .flat();

/**
 * @description Given the session positions, this function calculates the
 * centroid of the GPS coordinates of the session in order to achieve
 * a more accurate calculation of the spot the user surfed at. This function
 * assumes that the Earth is a perfect sphere, so there will be a small error
 * margin, but given that a surf session happens in a small area, that assumption
 * will be accurate enough for this use case.
 * @param {[{ latitude: number, longitude: number }]} positions
 * @returns {{latitude: number, longitude: number}}
 */
export const calculateSessionGPSCenter = positions => {
  if (positions.length === 0) {
    newrelic.noticeError('Session missing positions');
    throw new APIError('A session is required to have positions');
  }

  if (positions.length === 1) {
    return positions[0];
  }

  let x = 0.0;
  let y = 0.0;
  let z = 0.0;

  positions.forEach(position => {
    const latitude = (position.latitude * Math.PI) / 180;
    const longitude = (position.longitude * Math.PI) / 180;

    x += Math.cos(latitude) * Math.cos(longitude);
    y += Math.cos(latitude) * Math.sin(longitude);
    z += Math.sin(latitude);
  });

  const total = positions.length;

  x /= total;
  y /= total;
  z /= total;

  const centralLongitude = Math.atan2(y, x);
  const centralSquareRoot = Math.sqrt(x * x + y * y);
  const centralLatitude = Math.atan2(z, centralSquareRoot);

  return {
    latitude: (centralLatitude * 180) / Math.PI,
    longitude: (centralLongitude * 180) / Math.PI,
  };
};
