import { expect } from 'chai';
import { convertEventsToPositions, calculateSessionGPSCenter } from './calculateSessionGPSCenter';

describe('convertEventsToPositions', () => {
  it('should convert events to positions', () => {
    const events = [
      {
        positions: [
          { location: { coordinates: [180, 90] } },
          { location: { coordinates: [-180, -90] } },
        ],
      },
      {
        positions: [
          { location: { coordinates: [170, 80] } },
          { location: { coordinates: [-1, 1] } },
        ],
      },
    ];

    expect(convertEventsToPositions(events)).to.deep.equal([
      { longitude: 180, latitude: 90 },
      { longitude: -180, latitude: -90 },
      { longitude: 170, latitude: 80 },
      { longitude: -1, latitude: 1 },
    ]);
  });

  it('should return an empty array if there are no positions', () => {
    const events = [];
    expect(convertEventsToPositions(events)).to.deep.equal([]);
  });
});

describe('calculateSessionGeoCenter', () => {
  it('should calculate the GPS centroid of positions', () => {
    const positions = [
      {
        latitude: 37.797749,
        longitude: -122.412147,
      },
      {
        latitude: 37.789068,
        longitude: -122.390604,
      },
      {
        latitude: 37.785269,
        longitude: -122.421975,
      },
    ];
    expect(calculateSessionGPSCenter(positions)).to.deep.equal({
      latitude: 37.790696058721714,
      longitude: -122.40824208245043,
    });
  });

  it('should return a single position if only one is passed', () => {
    const positions = [
      {
        latitude: 37.797749,
        longitude: -122.412147,
      },
    ];
    expect(calculateSessionGPSCenter(positions)).to.deep.equal({
      latitude: 37.797749,
      longitude: -122.412147,
    });
  });

  it('should throw an error if no positions are passed', () => {
    try {
      calculateSessionGPSCenter([]);
    } catch (error) {
      expect(error.name).to.be.equal('APIError');
      expect(error.message).to.be.equal(
        'Invalid Parameters: A session is required to have positions',
      );
    }
  });
});
