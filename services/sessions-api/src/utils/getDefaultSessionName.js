import tzlookup from 'tz-lookup';
import moment from 'moment-timezone';

const getDefaultSessionName = ({ timestamp, coordinates }) => {
  const [lon, lat] = coordinates;
  const timezone = tzlookup(lat, lon);
  const hr = moment.tz(timestamp, timezone).format('HH');

  if (!hr) {
    return 'Surf Session';
  }
  if (hr <= 6) {
    return 'Dawn Patrol';
  }
  if (hr <= 10) {
    return 'Morning Shift';
  }
  if (hr <= 13) {
    return 'Lunch Break';
  }
  if (hr <= 16) {
    return 'Afternoon Session';
  }
  return 'Evening Surf';
};

export default getDefaultSessionName;
