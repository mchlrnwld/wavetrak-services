import { expect } from 'chai';
import getCardinalDirection from './getCardinalDirection';

describe('utils / getCardinalDirecion', () => {
  it('defaults to N if not passed a direction', () => {
    expect(getCardinalDirection()).to.equal('N');
  });

  it('returns the appropriate cardinal direction for any given degrees', () => {
    expect(getCardinalDirection(45)).to.equal('NE');
    expect(getCardinalDirection(90)).to.equal('E');
    expect(getCardinalDirection(180)).to.equal('S');
    expect(getCardinalDirection(270)).to.equal('W');
    expect(getCardinalDirection(360)).to.equal('N');
    expect(getCardinalDirection(114)).to.equal('ESE');
    expect(getCardinalDirection(298)).to.equal('WNW');
    expect(getCardinalDirection(360)).to.equal('N');
  });
});
