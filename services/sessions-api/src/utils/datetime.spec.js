import { expect } from 'chai';
import { fromUnixTimestamp, toUnixTimestamp } from './datetime';

describe('utils / datetime', () => {
  describe('fromUnixTimestamp', () => {
    it('converts milliseconds from 01/01/1970 to UTC Date object', () => {
      expect(fromUnixTimestamp(1493771258000)).to.deep.equal(new Date('2017-05-03T00:27:38Z'));
    });
  });

  describe('toUnixTimestamp', () => {
    it('converts UTC Date object to milliseconds from 01/01/1970', () => {
      expect(toUnixTimestamp(new Date('2017-05-03T00:27:38Z'))).to.equal(1493771258000);
    });

    it('converts local Date object to milliseconds from 01/01/1970', () => {
      expect(toUnixTimestamp(new Date('2017-05-02T17:27:38-07:00'))).to.equal(1493771258000);
    });
  });
});
