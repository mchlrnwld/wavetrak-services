/**
 * Gets the OS and App versions from the useragent in headers
 * @property {Object} property - request object (hopefully containing headers)
 *
 */
const getWatchDetails = ({ headers }) => {
  if (!headers || !headers['User-Agent']) return;

  const app = headers['User-Agent'].match(/(?<=Tracking Extension\/)[0-9.]*/g);
  const os = headers['User-Agent'].match(/(?<=watchOS )[0-9.]*/g);

  // eslint-disable-next-line consistent-return
  return {
    watchOSVersion: os && os[0],
    watchAppVersion: app && app[0],
  };
};

export default getWatchDetails;
