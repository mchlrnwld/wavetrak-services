/**
 * @typedef {object} SessionValidation
 * @property {{ name: string, message: string}} error
 * @property {ValidatedSession} value
 */

/**
 * @typedef {{
 *   timestamp: number,
 *   latitude: number,
 *   longitude: number,
 * }[]} RawPositions
 */

/**
 * @typedef {{
 *   startTimestamp: number,
 *   endTimestamp: number,
 *   type: ('WAVE'|'PADDLE'),
 *   outcome?: ('CAUGHT'|'ATTEMPTED'),
 *   distance: number,
 *   speedMax: number,
 *   speedAverage: number,
 *   speedAverage: number,
 *   positions: RawPositions,
 * }[]} RawEvents
 */

/**
 * @typedef {object} RawSession
 * @property {number} startTimestamp
 * @property {number} endTimestamp
 * @property {'PUBLIC' | 'PRIVATE'} visibility
 * @property {number} waveCount
 * @property {number} speedMax
 * @property {number} distancePaddled
 * @property {number} distanceWaves
 * @property {number} distanceLongestWave
 * @property {string} deviceType
 * @property {RawEvents} events
 */

/**
 * @typedef {{
 *   timestamp: Date,
 *   latitude: number,
 *   longitude: number,
 * }[]} ValidatedPositions
 */

/**
 * @typedef {{
 *   startTimestamp: Date,
 *   endTimestamp: Date,
 *   type: ('WAVE'|'PADDLE'),
 *   outcome?: ('CAUGHT'|'ATTEMPTED'),
 *   distance: number,
 *   speedMax: number,
 *   speedAverage: number,
 *   speedAverage: number,
 *   positions: ValidatedPositions,
 * }[]} ValidatedEvents
 */

/**
 * @typedef {object} Session
 * @property {Date} startTimestamp
 * @property {Date} endTimestamp
 * @property {'PUBLIC' | 'PRIVATE'} visibility
 * @property {number} waveCount
 * @property {number} speedMax
 * @property {number} distancePaddled
 * @property {number} distanceWaves
 * @property {number} distanceLongestWave
 * @property {string} deviceType
 * @property {Date} createdAt
 * @property {Date} updatedAt
 * @property {Partner} partner
 * @property {string} user
 * @property {string} client
 * @property {'PENDING'} status
 * @property {string} startLatLon
 * @property {GpsCenter} gpsCenter
 * @property {string} _id
 * @property {ValidatedEvents} events
 */

/**
 * @typedef {object} Partner
 * @property {object} stats
 * @property {Date} stats.startTimestamp
 * @property {Date} stats.endTimestamp
 * @property {number} stats.waveCount
 * @property {number} stats.speedMax
 * @property {object} stats.longestWave
 * @property {number} stats.longestWave.distance
 * @property {number?} stats.longestWave.seconds
 * @property {number} stats.distancePaddled
 * @property {number} stats.distanceWaves
 * @property {number?} stats.calories
 */

/**
 * @typedef {object} GpsCenter
 * @property {'Point'} stats.type
 * @property {[number, number]} stats.coordinates
 */

/**
 * @typedef {{
 *   timestamp: Date,
 *   location: {
 *     type: ('Point'),
 *     coordinates: [number, number]
 *   }
 * }[]} Postions
 */

/**
 * @typedef {{
 *   _id: string
 *   startTimestamp: Date,
 *   endTimestamp: Date,
 *   type: ('WAVE'|'PADDLE'),
 *   outcome: ('CAUGHT'|'ATTEMPTED'),
 *   distance: number,
 *   speedMax: number,
 *   speedAverage: number,
 *   positions: Postions,
 * }[]} Events
 */

/**
 * @typedef {{
 *   _id: string
 *   startTimestamp: Date,
 *   endTimestamp: Date,
 *   favorite: boolean
 *   type: ('WAVE'|'PADDLE'),
 *   outcome: ('CAUGHT'|'ATTEMPTED'),
 *   distance: number,
 *   speedMax: number,
 *   speedAverage: number,
 *   speedAverage: number,
 *   positions: Postions,
 * }[]} EnrichedEvents
 */

/**
 * @typedef {object} ValidatedSession
 * @property {Date} startTimestamp
 * @property {Date} endTimestamp
 * @property {Date} createdAt
 * @property {Date} updatedAt
 * @property {'PUBLIC' | 'PRIVATE'} visibility
 * @property {number} waveCount
 * @property {number} speedMax
 * @property {number} distancePaddled
 * @property {number} distanceWaves
 * @property {number} distanceLongestWave
 * @property {string} deviceType
 * @property {Events} events
 */

/**
 * @typedef {ValidatedSession & Partner} ValidatedSessionWithPartner
 */

/**
 * @typedef {{
 *   title: string
 *   alias: string
 *   stillUrl: string
 *   isPremium: boolean,
 *   visible: boolean
 *   id: string
 * }[]} Cams
 */

/**
 * @typedef {{
 *   startTimestamp: Date,
 *   endTimestamp: Date,
 *   createdAt: Date,
 *   updatedAt: Date,
 *   visibility: ('PUBLIC'|'PRIVATE'),
 *   waveCount: number,
 *   speedMax: number,
 *   partner: Partner
 *   distancePaddled: number,
 *   distanceLongestWave: number,
 *   deviceType: number,
 *   events: EnrichedEvents,
 *   cams: Cams,
 *   spotReportView: Object,
 * }} EnrichedSession
 */
