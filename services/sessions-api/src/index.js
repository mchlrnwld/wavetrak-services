import 'newrelic';
import mongoose from 'mongoose';
import { initMongo, initRedis } from '@surfline/services-common';
import startApp from './server';
import config from './config';
import log from './common/logger';
import { createPromisifiedRedisInstance } from './external/redis';
// import { setupPactServers } from './pact/providers';

const start = async () => {
  // if (process.env.NODE_ENV === 'test') {
  //   try {
  //     await setupPactServers();
  //   } catch (err) {
  //     log.error({
  //       message: 'App Initialization failed. Can\'t setup Pact servers',
  //       stack: err,
  //     });
  //   }
  // }

  try {
    await Promise.all([
      initRedis(config.REDIS_IP, config.REDIS_PORT, 'sessions-api'),
      initMongo(mongoose, config.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api'),
    ]);
    createPromisifiedRedisInstance();
    startApp(log);
  } catch (err) {
    console.log(err);
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  }
};

start();
