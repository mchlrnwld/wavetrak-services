export default () => ({
  _id: '5ba4749a0cd32de9632c9955',
  startTimestamp: new Date('2019-05-29T12:00:42.000Z'),
  endTimestamp: new Date('2019-05-29T12:10:05.000Z'),
  visibility: 'PUBLIC',
  waveCount: 1,
  speedMax: 17.5,
  distancePaddled: 1026.83,
  distanceWaves: 839.19,
  distanceLongestWave: 544.63,
  deviceType: 'iOS',
  createdAt: new Date('2019-05-29T12:00:42.000Z'),
  updatedAt: new Date('2019-05-29T12:00:42.000Z'),
  partner: {
    stats: {
      waveCount: 2,
      speedMax: 17.5,
      longestWave: {
        distance: 544.63,
        seconds: null,
      },
      distancePaddled: 1026.83,
      distanceWaves: 839.19,
      calories: null,
    },
  },
  user: '5ce2edc6f8c849008de1d27e',
  client: '5b0cc082b5acfa365f447b6a',
  name: 'Morning Shift',
  status: 'ENRICHED',
  clips: [
    {
      _id: 'wave1clip1',
      eventId: 'wave1',
    },
    {
      _id: 'wave1clip2',
      eventId: 'wave1',
    },
    {
      _id: 'wave1clip3',
      eventId: 'wave1',
    },
    {
      _id: 'wave2clip1',
      eventId: 'wave2',
    },
    {
      _id: 'wave2clip2',
      eventId: 'wave2',
    },
    {
      _id: 'wave2clip3',
      eventId: 'wave2',
    },
  ],
  eventOverrides: new Map(
    Object.entries({
      wave1: {
        outcome: 'ATTEMPTED',
        favorite: true,
      },
    }),
  ),
  events: [
    {
      startTimestamp: new Date('2019-05-29T12:00:42.000Z'),
      endTimestamp: new Date('2019-05-29T12:01:05.000Z'),
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: new Date('2019-05-29T12:00:43.000Z'),
          location: {
            type: 'Point',
            coordinates: [-118.00316870212555, 33.65544147271585],
          },
        },
      ],
      _id: 'wave1',
      favorite: true,
      outcome: 'ATTEMPTED',
      clips: ['wave1clip1', 'wave1clip2', 'wave1clip3'],
    },
    {
      startTimestamp: new Date('2019-05-29T12:02:42.000Z'),
      endTimestamp: new Date('2019-05-29T12:03:05.000Z'),
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: new Date('2019-05-29T12:02:43.000Z'),
          location: {
            type: 'Point',
            coordinates: [-118.00316870212555, 33.65544147271585],
          },
        },
      ],
      _id: 'wave2',
      outcome: 'CAUGHT',
      clips: ['wave2clip1', 'wave2clip2', 'wave2clip3'],
    },
    {
      startTimestamp: new Date('2019-05-29T12:05:42.000Z'),
      endTimestamp: new Date('2019-05-29T12:09:05.000Z'),
      type: 'PADDLE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: new Date('2019-05-29T12:05:42.000Z'),
          location: {
            type: 'Point',
            coordinates: [-118.00316870212555, 33.65544147271585],
          },
        },
      ],
      _id: 'paddle1',
      outcome: 'CAUGHT',
    },
  ],
  surfline: {
    stats: {
      waveCount: 2,
      speedMax: 25.31,
      longestWave: {
        distance: 75.7,
        seconds: 563,
      },
      distancePaddled: null,
      distanceWaves: 149.60000000000002,
      wavesPerHour: 12.79,
      calories: null,
    },
  },
  spotReportView: {
    _id: '5977abb3b38c2300127471ec',
    updatedAt: new Date('2019-05-29T11:48:56.064Z'),
    createdAt: new Date('2017-08-03T18:56:59.981Z'),
    thumbnail: 'https://spot-thumbnails.staging.surfline.com/spots/default/default_1500.jpg',
    rank: 13,
    subregionId: '58581a836630e24c44878fd6',
    location: {
      coordinates: [-118.00316870212555, 33.65544147271585],
      type: 'Point',
    },
    lat: 33.65544147271585,
    lon: -118.00316870212555,
    name: 'HB Pier, South Closeup',
    tide: {
      previous: {
        type: 'LOW',
        height: 1.44,
        timestamp: 1559117580,
        utcOffset: -7,
      },
      current: {
        type: 'NORMAL',
        height: 2.9462444444444444,
        timestamp: 1559130533,
        utcOffset: -7,
      },
      next: {
        type: 'HIGH',
        height: 3.54,
        timestamp: 1559137560,
        utcOffset: -7,
      },
    },
    waterTemp: {
      min: 57.2,
      max: 59,
    },
    cameras: [
      {
        _id: '5978cb2518fd6daf0da062ae',
        title: 'OC - HB Pier Southside Closeup',
        streamUrl: 'https://cams.cdn-surfline.com/cdn-wc/wc-hbpiersclose/playlist.m3u8',
        stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small.jpg',
        pixelatedStillUrl:
          'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small_pixelated.png',
        rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/wc-hbpiersclose/wc-hbpiersclose',
        isPremium: false,
        alias: 'wc-hbpiersclose',
        status: {
          isDown: false,
          message: '',
          subMessage: '',
          altMessage: '',
        },
      },
    ],
    conditions: {
      human: false,
      value: null,
    },
    wind: {
      speed: 2.79,
      direction: 181.33,
    },
    weather: {
      temperature: 60.6,
      condition: 'CLEAR_NO_RAIN',
    },
    waveHeight: {
      human: false,
      min: 4.07,
      max: 4.49,
      occasional: null,
      humanRelation: null,
      plus: false,
    },
    legacyId: 148545,
    __v: 48862,
    timezone: 'America/Los_Angeles',
    legacyRegionId: 2143,
    saveFailures: 0,
    swells: [
      {
        height: 1.21,
        direction: 192.66,
        period: 12,
        _id: '5cee71a74f604c1f6ae360de',
      },
      {
        height: 0.79,
        direction: 194.06,
        period: 18,
        _id: '5cee71a74f604cf610e360dd',
      },
      {
        height: 1.21,
        direction: 272.81,
        period: 6,
        _id: '5cee71a74f604cdd07e360dc',
      },
      {
        height: 0.39,
        direction: 172.97,
        period: 9,
        _id: '5cee71a74f604c5921e360db',
      },
      {
        height: 0,
        direction: 0,
        period: 0,
        _id: '5cee71a74f604cad33e360da',
      },
      {
        height: 0,
        direction: 0,
        period: 0,
        _id: '5cee71a74f604c616de360d9',
      },
    ],
    offshoreDirection: 211,
    parentTaxonomy: [
      '58f7ed51dadb30820bb38782',
      '58f7ed51dadb30820bb38791',
      '58f7ed51dadb30820bb3879c',
      '5908c46bdadb30820b23c1f3',
      '58f7ed5ddadb30820bb39651',
      '58f7ed51dadb30820bb387a6',
      '58f7ed5ddadb30820bb39689',
      '58f7ed5fdadb30820bb3987c',
    ],
    abilityLevels: [],
    boardTypes: [],
    relivableRating: 0,
  },
  spot: {
    id: '5977abb3b38c2300127471ec',
    name: 'HB Pier, South Closeup',
    location: {
      coordinates: [-118.00316870212555, 33.65544147271585],
      type: 'Point',
    },
  },
  originalSpotId: '5977abb3b38c2300127471ec',
  cams: [
    {
      id: '5978cb2518fd6daf0da062ae',
      title: 'SOCAL - HB Pier Southside Closeup',
      alias: 'wc-hbpiersclose',
      isPremium: false,
      stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small.jpg',
      visible: true,
    },
    {
      id: '58a377daea714bf7668c000f',
      title: 'SOCAL - HB Pier Southside Overview',
      alias: 'wc-hbpierssov',
      isPremium: false,
      stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpierssov/latest_small.jpg',
      visible: true,
    },
    {
      id: '583499cb3421b20545c4b53f',
      title: 'SOCAL - HB Pier, Southside',
      alias: 'wc-hbpierss',
      isPremium: false,
      stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpierss/latest_small.jpg',
      visible: true,
    },
  ],
});

export const eventsFixture = [
  {
    startTimestamp: new Date('2019-05-29T12:00:42.000Z'),
    endTimestamp: new Date('2019-05-29T12:01:05.000Z'),
    type: 'WAVE',
    distance: 75.7,
    speedMax: 25.31,
    speedAverage: 19.47,
    positions: [
      {
        timestamp: new Date('2019-05-29T12:00:43.000Z'),
        location: {
          type: 'Point',
          coordinates: [-118.00316870212555, 33.65544147271585],
        },
      },
    ],
    _id: 'wave1',
  },
  {
    startTimestamp: new Date('2019-05-29T12:02:42.000Z'),
    endTimestamp: new Date('2019-05-29T12:03:05.000Z'),
    type: 'WAVE',
    distance: 75.7,
    speedMax: 25.31,
    speedAverage: 19.47,
    positions: [
      {
        timestamp: new Date('2019-05-29T12:02:43.000Z'),
        location: {
          type: 'Point',
          coordinates: [-118.00316870212555, 33.65544147271585],
        },
      },
    ],
    _id: 'wave2',
  },
  {
    startTimestamp: new Date('2019-05-29T12:05:42.000Z'),
    endTimestamp: new Date('2019-05-29T12:09:05.000Z'),
    type: 'PADDLE',
    distance: 73.9,
    speedMax: 23.69,
    speedAverage: 17.74,
    positions: [
      {
        timestamp: new Date('2019-05-29T12:05:42.000Z'),
        location: {
          type: 'Point',
          coordinates: [-118.00316870212555, 33.65544147271585],
        },
      },
    ],
    _id: 'paddle1',
  },
];
