import events from './events.json';

export default {
  startTimestamp: new Date('2020-03-03T08:00:00.001Z'),
  endTimestamp: new Date('2020-03-03T08:00:10.002Z'),
  visibility: 'PUBLIC',
  waveCount: 1,
  speedMax: 1,
  distancePaddled: 1,
  distanceWaves: 1,
  distanceLongestWave: 1,
  deviceType: 'test-session',
  createdAt: new Date('2021-01-14T20:43:20.128Z'),
  updatedAt: new Date('2021-01-14T20:43:20.128Z'),
  partner: {
    stats: {
      startTimestamp: new Date('2020-03-03T08:00:00.001Z'),
      endTimestamp: new Date('2020-03-03T08:00:10.002Z'),
      waveCount: 1,
      speedMax: 1,
      longestWave: {
        distance: 1,
        seconds: 1,
      },
      distancePaddled: 1,
      distanceWaves: 1,
      calories: undefined,
    },
  },
  events: events.events,
};
