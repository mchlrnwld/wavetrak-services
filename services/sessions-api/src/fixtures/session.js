export default {
  startTimestamp: new Date('2020-03-03T08:00:00.001Z'),
  endTimestamp: new Date('2020-03-03T08:00:10.002Z'),
  visibility: 'PUBLIC',
  waveCount: 1,
  speedMax: 1,
  distancePaddled: 1,
  distanceWaves: 1,
  distanceLongestWave: 1,
  deviceType: 'test-session',
  createdAt: new Date('2021-01-14T20:43:20.128Z'),
  updatedAt: new Date('2021-01-14T20:43:20.128Z'),
  partner: {
    stats: {
      startTimestamp: new Date('2020-03-03T08:00:00.001Z'),
      endTimestamp: new Date('2020-03-03T08:00:10.002Z'),
      waveCount: 1,
      speedMax: 1,
      longestWave: {
        distance: 1,
        seconds: 1,
      },
      distancePaddled: 1,
      distanceWaves: 1,
      calories: undefined,
    },
  },
  user: '59dd282894667a3a2669e343',
  client: '5c6f00c9f0b6cbda76bc6c40',
  name: 'Dawn Patrol',
  status: 'PENDING',
  startLatLon: '33.65544147271585,-118.00316870212555',
  gpsCenter: {
    type: 'Point',
    coordinates: [-118.0049726319257, 33.65507068980125],
  },
  _id: '6000ace8204c3e6dee08ca58',
  appVersion: 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173',
  watchOSVersion: 'testWatchOSVersion',
  watchAppVersion: 'testWatchAppVersion',
};
