/* istanbul ignore file */

import { createLogger, setupLogsene } from '@surfline/services-common';
import config from '../config';

setupLogsene(config.LOGSENE_KEY);

export { createLogger };
export default createLogger('sessions-api');
