/* istanbul ignore file */

import AWS from 'aws-sdk';
import config from '../config';

const { SNS_API_VERSION } = config;

const SNS = new AWS.SNS({ apiVersion: SNS_API_VERSION, region: 'us-west-1' });

/**
 * @description Subscription confirmation function for AWS SNS Subscriptions
 *
 * @param {string} token
 * @param {string} topicArn
 * @param {string} [authenticateOnUnsubscribe='false']
 */
export const confirmSubscription = async (token, topicArn, authenticateOnUnsubscribe = 'false') =>
  SNS.confirmSubscription({
    Token: token,
    TopicArn: topicArn,
    AuthenticateOnUnsubscribe: authenticateOnUnsubscribe,
  }).promise();

/**
 * @description This function is used to publish a message to
 * an SNS topic.
 * Before being sent, the `message` is passed into `JSON.stringify()`.
 * @param {string} topicArn
 * @param {Object} message
 */
export const publishMessage = async (topicArn, message) =>
  SNS.publish({
    TopicArn: topicArn,
    Message: JSON.stringify(message),
  }).promise();
