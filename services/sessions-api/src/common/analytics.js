import Analytics from 'analytics-node';

const slAnalytics = new Analytics(process.env.SEGMENT_WRITE_KEY || 'NoWrite', {
  flushAt: 1,
});

export const track = (params, callback = null) => {
  slAnalytics.track(params, callback);
};

export default track;
