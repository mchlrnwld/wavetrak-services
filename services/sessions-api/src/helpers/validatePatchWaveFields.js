import Joi from '@hapi/joi';
import { outcomeEnum } from './joi';

const fieldsSchema = Joi.object().keys({
  outcome: outcomeEnum.optional(),
  favorite: Joi.boolean().optional(),
  userVerified: Joi.boolean().optional(),
});

/**
 * Validates the types of the fields that can be updated by the
 * PATCH wave endpoint.
 * @param {{
 *  outcome: "CAUGHT"|"ATTEMPTED"
 *  favorite: boolean
 *  userVerified: boolean
 * }} fields
 * @throws {ValidationError}
 */
const validatePatchWaveFields = fields => {
  const validation = fieldsSchema.validate(fields, {
    stripUnknown: true,
    allowUnknown: false,
  });

  if (validation.error) {
    throw validation.error;
  }

  return validation.value;
};

export default validatePatchWaveFields;
