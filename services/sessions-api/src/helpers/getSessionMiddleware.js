import newrelic from 'newrelic';
import { hydrateSession } from './sessionEvents';

const getSessionMiddleware = async (req, res, next) => {
  try {
    const clientId = req.authenticatedClientId;
    const userId = req.authenticatedUserId;
    const { sessionId } = req.params;

    const session = await hydrateSession({ sessionId, userId, clientId });

    req.session = session;
  } catch (error) {
    if (error.name === 'NotFound') {
      return res.status(404).send({
        success: false,
        message: `Session not found ${req.authenticatedUserId ? 'for user' : ''}`,
      });
    }
    newrelic.noticeError(error);
    throw error;
  }
  return next();
};

export default getSessionMiddleware;
