import Joi from '@hapi/joi';
import '../jsdoc';
import { enrichedSession, isoDate } from './joi';

const enrichedSessionSchema = Joi.object(enrichedSession);

/**
 * Checks all the properties on the session to ensure they are
 * valid types so that they can then be inserted into mongo.
 * @param {RawSession} enrichedSessionJson
 * @returns {EnrichedSession} validated enriched session
 * @throws {ValidationError}
 */
const validateEnrichedSession = enrichedSessionJson => {
  const minStartTimestamp = Joi.attempt(
    enrichedSessionJson.startTimestamp,
    isoDate,
    'startTimestamp is invalid.',
  );
  const maxEndTimestamp = Joi.attempt(
    enrichedSessionJson.endTimestamp,
    isoDate,
    'endTimestamp is invalid.',
  );

  const validation = enrichedSessionSchema.validate(enrichedSessionJson, {
    stripUnknown: false,
    allowUnknown: true,
    context: {
      minStartTimestamp,
      maxEndTimestamp,
    },
  });

  if (validation.error) {
    throw validation.error;
  }

  return validation.value;
};

export default validateEnrichedSession;
