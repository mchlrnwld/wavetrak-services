import { NotFound } from '@surfline/services-common';
import hydrateEvents, { hydrateEventOverrides } from './hydrateEvents';
import { getEnrichedSession, getEnrichedSessionById } from '../models/Session';
import { getSessionEvents } from '../external/aws';

/**
 * @description Given either a full session or a session ID, user ID, and optionally a client ID this function
 * returns a fully hydrated session
 * @param {object} props
 * @param {{}} props.session
 * @param {string} props.sessionId
 * @param {string} props.clientId
 * @param {string} props.userId
 * @returns {Promise}
 */
export const hydrateSession = async ({ sessionId, userId, clientId, session: fullSession }) => {
  if (!sessionId && !fullSession) {
    throw new Error('Session ID or full session required');
  }

  let session;

  // If a full session is passed, it takes precendence
  if (fullSession) {
    session = fullSession;
  } else {
    // Web requests will not have a `clientId` and those are the ones
    // we want to be able to search sessions without a user ID
    session =
      userId && clientId
        ? await getEnrichedSession(userId, sessionId)
        : await getEnrichedSessionById(sessionId);
  }

  if (!session) {
    throw NotFound(`Session not found ${userId ? 'for user' : ''}`);
  }

  const hydratedEventOverrides = hydrateEventOverrides(session.eventOverrides);
  const rawEvents = await getSessionEvents(session._id);
  const events = hydrateEvents(rawEvents, session.clips, hydratedEventOverrides);
  return { ...session, events, eventOverrides: hydratedEventOverrides };
};

/**
 * @param {[{_id: string , client: string, user: string}]} sessions An array of session ID's
 */
export const addEventsToSessions = async sessions =>
  Promise.all(
    sessions.map(session =>
      hydrateSession({
        session,
      }),
    ),
  );
