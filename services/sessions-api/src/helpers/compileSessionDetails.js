import findIndex from 'lodash/findIndex';
import { toUnixTimestamp } from '../utils/datetime';
import { convertLength, convertSpeed } from '../utils/convertUnits';
import config from '../config';
import getConditionsFromSRV from './getConditionsFromSRV';
import filterWaveEvents from './filterWaveEvents';

const round = (value, decimals) => {
  const rounded = Math.round(`${value}e${decimals}`);
  return Number(`${rounded}e-${decimals}`);
};

const matchWaveClips = (waveClips, allClips) => {
  const matchingClips = waveClips
    .map(waveClip => {
      const allClipIdx = findIndex(allClips, { clipId: waveClip.toString() });
      return allClipIdx > -1 ? allClips[allClipIdx] : undefined;
    })
    .filter(match => !!match);

  return matchingClips;
};

const generateThumbnailData = (position, clips = []) => {
  const availableClips = clips?.length
    ? clips.filter(clip => clip.status === 'CLIP_AVAILABLE')
    : [];
  if (availableClips?.[0]?.thumbnail) return availableClips[0].thumbnail;

  const { coordinates } = position.location;
  const lon = coordinates[0];
  const lat = coordinates[1];

  const thumbnail = {
    url: `${config.mapTileUrl}/static/${lon},${lat},13/375x210.png?key=${config.mapTileKey}&attribution=0`,
    sizes: ['375x210'],
  };
  return thumbnail;
};

const compileSurflineStats = session => {
  const { events, partner, startTimestamp, endTimestamp } = session;

  const waves = events.filter(filterWaveEvents(true));
  const caughtWaves = waves.filter(filterWaveEvents(false));
  const waveCount = caughtWaves.length;
  const paddles = events.filter(event => event.type === 'PADDLE');

  // cannot assume both waves and paddles
  const distanceWaves = caughtWaves.length
    ? caughtWaves.map(wave => wave.distance).reduce((a, b) => a + b)
    : null;
  const speedMax = caughtWaves.length
    ? caughtWaves.map(wave => wave.speedMax).reduce((a, b) => (a > b ? a : b))
    : null;
  const distancePaddled = paddles.length
    ? paddles.map(paddle => paddle.distance).reduce((a, b) => a + b)
    : null;

  let waveDistance = 0;
  let waveSeconds = 0;
  caughtWaves.forEach(wave => {
    if (wave.distance > waveDistance) {
      waveDistance = wave.distance;
      // With new Date you can subtract to get milliseconds, then convert to seconds.
      const etsCompare = new Date(wave.endTimestamp);
      const stsCompare = new Date(wave.startTimestamp);
      waveSeconds = (etsCompare - stsCompare) / 1000;
    }
  });

  // longestWave data
  const longestWave = {
    distance: waveDistance || null,
    seconds: waveSeconds || null,
  };

  // gather wavesPerHour based on start end timestamps
  const etsSessionCompare = new Date(endTimestamp);
  const stsSessionCompare = new Date(startTimestamp);
  const sessionHours = (etsSessionCompare - stsSessionCompare) / 1000 / 60 / 60;
  const wavesPerHour = caughtWaves.length ? round(caughtWaves.length / sessionHours, 2) : null;

  const calories = partner?.stats?.calories;

  const stats = {
    waveCount,
    startTimestamp,
    endTimestamp,
    speedMax,
    longestWave,
    distancePaddled,
    distanceWaves,
    wavesPerHour,
    calories,
  };
  return stats;
};

export const compileStats = async sessionData => {
  /* Todo actually compile based on valid data we want to show */
  const partnerStats = sessionData?.partner?.stats || {};
  const surflineStats = compileSurflineStats(sessionData);
  const blendedStats = Object.assign(partnerStats, surflineStats);
  return blendedStats;
};

const formatPositions = positions => {
  if (positions?.length) {
    const formattedPositions = positions.map(position => ({
      timestamp: toUnixTimestamp(position.timestamp),
      latitude: position.location.coordinates[1],
      longitude: position.location.coordinates[0],
    }));
    return formattedPositions;
  }
  return null;
};

export const compileSessionDetails = ({
  session,
  stats,
  clips,
  units,
  waves = true,
  showAttempted = false,
}) => ({
  id: session._id,
  name: session.name,
  clientName: session.clientName,
  startTimestamp: toUnixTimestamp(session.startTimestamp),
  endTimestamp: toUnixTimestamp(session.endTimestamp),
  status: session.status,
  timezone: {
    key: 1,
    offset: 0,
  },
  clips: {
    total: clips.length,
    available: clips.filter(clip => clip.status === 'CLIP_AVAILABLE').length,
  },
  stats: {
    waveCount: stats.waveCount,
    speedMax: convertSpeed(stats.speedMax, units.speed),
    distancePaddled: convertLength(stats.distancePaddled, units.distance),
    distanceWaves: convertLength(stats.distanceWaves, units.distance),
    calories: stats.calories,
    wavesPerHour: stats.wavesPerHour || 0,
    longestWave: stats?.longestWave?.distance
      ? {
          distance: convertLength(stats.longestWave.distance, units.distance),
          seconds: stats.longestWave.seconds,
        }
      : null,
  },
  rating: session.rating || null,
  thumbnail: generateThumbnailData(session.events[0].positions[0], clips),
  waves: waves
    ? session.events.filter(filterWaveEvents(showAttempted)).map(wave => ({
        id: wave?._id,
        startTimestamp: toUnixTimestamp(wave.startTimestamp),
        endTimestamp: toUnixTimestamp(wave.endTimestamp),
        outcome: wave.outcome ? wave.outcome.toUpperCase() : 'CAUGHT',
        distance: convertLength(wave.distance, units.distance),
        speedMax: convertSpeed(wave.speedMax, units.speed),
        speedAverage: convertSpeed(wave.speedAverage, units.speed),
        clips: wave?.clips?.length ? matchWaveClips(wave.clips, clips) : null,
        positions: formatPositions(wave.positions),
        trackImage: generateThumbnailData(session.events[0].positions[0]),
        favorite: wave.favorite ? wave.favorite : false,
        userVerified: wave?.userVerified,
      }))
    : undefined,
  spot: {
    id: session?.spot?.id,
    name: session?.spot?.name,
    location: session?.spot?.location?.type ? session?.spot?.location : null,
    relivableRating: session?.spot?.relivableRating,
  },
  cams: session.cams
    ? session.cams.map(cam => {
        const cameraTitle = cam.title ? cam.title : '';
        const parsedCamTitle =
          cameraTitle.indexOf('- ') !== -1 ? cameraTitle.split('- ')[1] : cameraTitle;
        return {
          ...cam,
          title: parsedCamTitle,
        };
      })
    : [],
  conditions: getConditionsFromSRV(session.spotReportView, units),
});

export default compileSessionDetails;
