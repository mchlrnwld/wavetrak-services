import newrelic from 'newrelic';
import { updateSessionStatus } from '../models';

/**
 * @param {string} sessionId
 * @param {string} userId
 */
export default async (sessionId, userId) => {
  try {
    await updateSessionStatus(userId, sessionId, 'FAILED');
  } catch (err) {
    newrelic.recordCustomEvent('Sessions', { event: 'markSessionFailed failed' });
    newrelic.noticeError(err);
  }
  return true;
};
