import Joi from '@hapi/joi';
import newrelic from 'newrelic';
import '../jsdoc';
import { session, timestamp } from './joi';

const sessionSchema = Joi.object().keys(session);

/**
 * Checks all the properties on the session to ensure they are
 * valid types so that they can then be inserted into mongo.
 * @param {RawSession} sessionJson
 * @returns {ValidatedSession} validated session
 * @throws {ValidationError}
 */
const validateSession = sessionJson => {
  const minStartTimestamp = Joi.attempt(
    sessionJson.startTimestamp,
    timestamp,
    'startTimestamp is invalid',
  );
  const maxEndTimestamp = Joi.attempt(
    sessionJson.endTimestamp,
    timestamp,
    'endTimestamp is invalid',
  );

  const validation = sessionSchema.validate(sessionJson, {
    allowUnknown: true,
    context: {
      minStartTimestamp,
      maxEndTimestamp,
    },
  });

  if (validation.error) {
    newrelic.recordCustomEvent('Sessions', {
      event: 'validateSession Failed',
      error: validation.error,
    });
    throw validation.error;
  }

  return validation.value;
};

export default validateSession;
