import newrelic from 'newrelic';
import { EnrichedSession } from '../models/Session';
import { toUnixTimestamp } from '../utils/datetime';

/**
 * @description This is a helper function used by the /gps endpoint to determine whether a session is a duplicate or not
 *
 * @param {{latitude: number, longitude: number}} gpsCenter
 * @param {ValidatedSessionWithPartner} validatedSession
 * @param {Number} userId
 * @returns { Boolean }
 */
const existsDuplicateSession = async (gpsCenter, validatedSession, userId) => {
  const existsDuplicate = await EnrichedSession.findOne({
    gpsCenter: {
      type: 'Point',
      coordinates: [gpsCenter.longitude, gpsCenter.latitude],
    },
    startTimestamp: validatedSession.startTimestamp,
    endTimestamp: validatedSession.endTimestamp,
    user: userId,
    waveCount: validatedSession.waveCount,
  })
    .select('_id')
    .lean();

  if (existsDuplicate) {
    const id = existsDuplicate._id;
    newrelic.recordCustomEvent('Sessions', {
      event: 'Duplicate Session',
      userId,
      startTimestamp: toUnixTimestamp(validatedSession.startTimestamp),
      endTimestamp: toUnixTimestamp(validatedSession.endTimestamp),
      exisitingSession: id.toString(),
    });
  }

  return existsDuplicate;
};

export default existsDuplicateSession;
