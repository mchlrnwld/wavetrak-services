const WAVE = 'WAVE';
const CAUGHT = 'CAUGHT';
const ATTEMPTED = 'ATTEMPTED';

/**
 * @description Given a set of events and a boolean flag `showAttempted` filters
 * out wave events, including `ATTEMPTED` waves if `showAttempted` is `true`
 * @param {boolean} showAttempted
 * @returns {functon}
 */
export default showAttempted => event => {
  // default to caught if unspecified
  const outcome = event.outcome || CAUGHT;
  if (showAttempted) {
    return event.type === WAVE;
  }
  if (event.type !== WAVE) return false;
  return outcome.toUpperCase() !== ATTEMPTED;
};
