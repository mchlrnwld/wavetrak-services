import mongoose from 'mongoose';

const extendSchema = (schema, extension, options) =>
  new mongoose.Schema(
    {
      ...schema.obj,
      ...extension,
    },
    options,
  );

export default extendSchema;
