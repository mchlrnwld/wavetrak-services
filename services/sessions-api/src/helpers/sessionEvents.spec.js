import { expect } from 'chai';
import sinon from 'sinon';
import enrichedSessionFixture from '../fixtures/enrichedSession';
import { hydrateSession } from './sessionEvents';
import * as Session from '../models/Session';
import * as aws from '../external/aws';

const enrichedSession = enrichedSessionFixture();
describe('helpers / sessionEvents / hydrateSession', () => {
  beforeEach(() => {
    sinon.stub(Session, 'getEnrichedSessionById');
    sinon.stub(Session, 'getEnrichedSession');
    sinon.stub(aws, 'getSessionEvents');
  });

  afterEach(() => {
    Session.getEnrichedSessionById.restore();
    Session.getEnrichedSession.restore();
    aws.getSessionEvents.restore();
  });

  it('should hydrate a session when the full payload is passed', async () => {
    aws.getSessionEvents.resolves(enrichedSession.events);

    const session = await hydrateSession({ session: enrichedSession });

    session.events.forEach(event => {
      const overrides = session.eventOverrides.get(event._id);

      if (overrides) {
        expect(event.outcome).to.equal(overrides.outcome);
        expect(event.favorite).to.equal(overrides.favorite);
        expect(event.clips).to.exist();
      }
    });
  });

  it('should  query the session by session Id when the client Id is ommited', async () => {
    Session.getEnrichedSessionById.resolves(enrichedSession);
    aws.getSessionEvents.resolves(enrichedSession.events);

    await hydrateSession({
      sessionId: enrichedSession._id,
      userId: enrichedSession.user,
    });

    expect(Session.getEnrichedSessionById).to.have.been.calledOnceWithExactly(enrichedSession._id);
  });

  it('should query the session by user ID when a client ID is given', async () => {
    Session.getEnrichedSession.resolves(enrichedSession);
    aws.getSessionEvents.resolves(enrichedSession.events);

    await hydrateSession({
      sessionId: enrichedSession._id,
      userId: enrichedSession.user,
      clientId: enrichedSession.client,
    });

    expect(Session.getEnrichedSession).to.have.been.calledOnceWithExactly(
      enrichedSession.user,
      enrichedSession._id,
    );
  });
});
