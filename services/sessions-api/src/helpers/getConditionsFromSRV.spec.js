import { expect } from 'chai';
import getConditionsFromSRV from './getConditionsFromSRV';

describe('helpers / getConditionsFromSRV', () => {
  it('returns null if not passed a spot report view', () => {
    expect(getConditionsFromSRV(null, {})).to.be.null();
    expect(getConditionsFromSRV()).to.be.null();
  });
});
