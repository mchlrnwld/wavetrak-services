import { expect } from 'chai';
import hydrateEvents from './hydrateEvents';

describe('helpers / hydrateEvents', () => {
  const defaultEvents = [
    {
      _id: '1',
      type: 'WAVE',
      outcome: 'ATTEMPTED',
      clips: null,
      favorite: false,
    },
    {
      _id: '2',
      type: 'WAVE',
      outcome: 'CAUGHT',
      clips: null,
      favorite: true,
    },
    {
      _id: '3',
      type: 'PADDLE',
      outcome: 'ATTEMPTED',
      clips: null,
      favorite: false,
    },
  ];
  const eventOverrides = new Map([
    [
      '1',
      {
        favorite: true,
        outcome: 'CAUGHT',
      },
    ],
    [
      '2',
      {
        favorite: false,
      },
    ],
  ]);

  const clips = [
    {
      _id: 'clip1',
      eventId: '1',
    },
    {
      _id: 'clip2',
      eventId: '2',
    },
    {
      _id: 'clip3',
      eventId: '2',
    },
  ];

  const desiredEvents1 = [
    {
      _id: '1',
      type: 'WAVE',
      clips: null,
      outcome: 'CAUGHT',
      favorite: true,
    },
    {
      _id: '2',
      type: 'WAVE',
      clips: null,
      outcome: 'CAUGHT',
      favorite: false,
    },
    {
      _id: '3',
      type: 'PADDLE',
      clips: null,
      outcome: 'ATTEMPTED',
      favorite: false,
    },
  ];

  const desiredEvents2 = [
    {
      _id: '1',
      type: 'WAVE',
      outcome: 'ATTEMPTED',
      favorite: false,
      clips: ['clip1'],
    },
    {
      _id: '2',
      type: 'WAVE',
      outcome: 'CAUGHT',
      favorite: true,
      clips: ['clip2', 'clip3'],
    },
    {
      _id: '3',
      type: 'PADDLE',
      outcome: 'ATTEMPTED',
      clips: [],
      favorite: false,
    },
  ];

  const desiredEvents3 = [
    {
      _id: '1',
      type: 'WAVE',
      outcome: 'CAUGHT',
      favorite: true,
      clips: ['clip1'],
    },
    {
      _id: '2',
      type: 'WAVE',
      outcome: 'CAUGHT',
      favorite: false,
      clips: ['clip2', 'clip3'],
    },
    {
      _id: '3',
      type: 'PADDLE',
      outcome: 'ATTEMPTED',
      clips: [],
      favorite: false,
    },
  ];

  it('returns the same events if not passed overrides or clips', () => {
    expect(hydrateEvents(defaultEvents, undefined, undefined)).to.deep.equal(defaultEvents);
  });

  it('returns the original event array plus overrides when given overrides', () => {
    expect(hydrateEvents(defaultEvents, undefined, eventOverrides)).to.deep.equal(desiredEvents1);
  });

  it('returns the original event array plus clips when given clips', () => {
    expect(hydrateEvents(defaultEvents, clips, undefined)).to.deep.equal(desiredEvents2);
  });

  it('returns the original event array plus overrides and clips when given both', () => {
    expect(hydrateEvents(defaultEvents, clips, eventOverrides)).to.deep.equal(desiredEvents3);
  });
});
