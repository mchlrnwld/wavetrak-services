import getCardinalDirection from '../utils/getCardinalDirection';
import { convertWindSpeed, convertWaveHeight, convertTideHeight } from '../utils/convertUnits';

const DEFAULT_SRV_UNITS = {
  windSpeed: 'KTS',
  swellHeight: 'FT',
  surfHeight: 'FT',
  tideHeight: 'FT',
  temperature: 'F',
};

const getTideDirection = nextTideType => {
  switch (nextTideType) {
    case 'HIGH':
      return 'RISING';
    case 'LOW':
      return 'FALLING';
    default:
      return null;
  }
};

const getConditionsFromSRV = (spotReportView, desiredUnits) => {
  if (!spotReportView) {
    return null;
  }

  const waveHeight = {
    min: convertWaveHeight(
      spotReportView?.waveHeight?.min,
      DEFAULT_SRV_UNITS.surfHeight,
      desiredUnits.surfHeight,
    ),
    max: convertWaveHeight(
      spotReportView?.waveHeight?.max,
      DEFAULT_SRV_UNITS.surfHeight,
      desiredUnits.surfHeight,
    ),
    plus: spotReportView?.waveHeight?.plus,
  };
  const wind = {
    speed: convertWindSpeed(
      spotReportView?.wind?.speed,
      DEFAULT_SRV_UNITS.windSpeed,
      desiredUnits.windSpeed,
    ),
    directionCardinal: getCardinalDirection(spotReportView?.wind?.direction),
  };
  const conditions = {
    human: spotReportView?.conditions?.human,
    value: spotReportView?.conditions?.value,
  };
  const tide = {
    direction: getTideDirection(spotReportView?.tide?.next?.type),
    height: convertTideHeight(
      spotReportView?.tide?.current?.height,
      DEFAULT_SRV_UNITS.tideHeight,
      desiredUnits.tideHeight,
    ),
  };

  return {
    waveHeight,
    wind,
    conditions,
    tide,
  };
};

export default getConditionsFromSRV;
