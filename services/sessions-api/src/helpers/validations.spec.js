import { expect } from 'chai';
import mongoose from 'mongoose';
import session from '../fixtures/rawSession.json';
import enrichedSession from '../fixtures/enrichedSession.json';
import validateSession from './validateSession';
import validateEnrichedSession from './validateEnrichedSession';

describe('validations / validateSession', () => {
  it('properly validates a session', () => {
    expect(validateSession(session)).to.exist();
  });

  it('allows unknown values and strips values that schema says to strip', () => {
    const validation = validateSession({
      ...session,
      surfline_id: 'test',
      testval: 'test1',
    });
    expect(validation).to.exist();
    expect(validation).to.be.haveOwnProperty('testval');
    expect(validation).to.be.not.haveOwnProperty('surfline_id');
  });

  it('throws a validation error for ISO date strings', () => {
    let error;
    try {
      validateSession({
        ...session,
        startTimestamp: '2018-09-23T21:34:42.000Z',
      });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('startTimestamp is invalid');
  });

  it('throws a validation error for invalid timestamps', () => {
    let error;
    try {
      validateSession({
        ...session,
        startTimestamp: -2120070826000,
      });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('startTimestamp is invalid');
  });

  it('defaults createdAt and updatedAt', () => {
    const validation = validateSession(session);
    expect(validation.updatedAt).to.be.instanceOf(Date);
    expect(validation.createdAt).to.be.instanceOf(Date);
  });

  it('throws a validation error if the startTimestamp is greater than the endTimestamp', () => {
    let error;
    try {
      validateSession({
        ...session,
        startTimestamp: session.endTimestamp,
        endTimestamp: session.startTimestamp,
      });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
  });

  it('throws a validation error if an events startTimestamp is before the startTimestamp of the session', () => {
    const newEvents = session.events.map(event => ({
      ...event,
      startTimestamp: session.startTimestamp - 1000,
    }));
    const payload = {
      ...session,
      events: newEvents,
    };
    let error;
    try {
      validateSession(payload);
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('startTimestamp');
  });

  it('throws a validation error if the events.startTimestamp is greater than the events.endTimestamp', () => {
    const newEvents = session.events.map(event => ({
      ...event,
      startTimestamp: event.endTimestamp,
      endTimestamp: event.startTimestamp,
    }));
    const payload = {
      ...session,
      events: newEvents,
    };

    let error;
    try {
      validateSession(payload);
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
  });

  it('throws a validation error if the events.positions.timestamp is greater than the session startTimestamp', () => {
    const newEvents = session.events.map(event => ({
      ...event,
      positions: event.positions.map(position => ({
        ...position,
        timestamp: session.endTimestamp + 1000,
      })),
    }));
    const payload = { ...session, events: newEvents };

    let error;
    try {
      validateSession(payload);
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('timestamp" must be less than');
  });

  it('properly throws if enriched session properties are incorrect or missing', () => {
    let error;
    try {
      validateEnrichedSession({ ...session, startTimestamp: 'blyak' });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('startTimestamp');
  });

  it('properly validates an enriched session', () => {
    const validation = validateEnrichedSession(enrichedSession);
    expect(validation).to.exist();
    expect(validation).to.haveOwnProperty('spotReportView');
    expect(validation).to.haveOwnProperty('cams');
    expect(validation.events[0]).to.haveOwnProperty('_id');
    expect(validation.events[0]._id).to.be.instanceOf(mongoose.Types.ObjectId);
    expect(validation).to.haveOwnProperty('originalSpotId');
    expect(validation.originalSpotId).to.be.instanceOf(mongoose.Types.ObjectId);
    expect(validation).to.haveOwnProperty('cams');
    expect(validation.cams[0].id).to.be.instanceOf(mongoose.Types.ObjectId);
    expect(validation).to.haveOwnProperty('spot');
    expect(validation.spot.id).to.be.instanceOf(mongoose.Types.ObjectId);
    expect(validation._id).to.be.instanceOf(mongoose.Types.ObjectId);
  });

  it('properly throws if a enriched session properties are incorrect or missing', () => {
    let error;
    try {
      validateEnrichedSession({ ...enrichedSession, waveCount: 'somestring' });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('waveCount');
  });

  it('properly throws if an object ID is invalid', () => {
    let error;
    try {
      validateEnrichedSession({ ...enrichedSession, _id: '123' });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.message).to.contain('Must be a valid Object ID');
  });
});
