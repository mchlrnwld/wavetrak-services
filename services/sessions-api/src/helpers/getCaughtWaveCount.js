import filterWaveEvents from './filterWaveEvents';

/**
 * @param {[{{
 *  outcome?: 'ATTEMPTED' | 'CAUGHT'
 *  type: 'WAVE' | 'PADDLE'
 * }}]} hydratedEvents
 */
export default async hydratedEvents => {
  const showAttempted = false;
  return hydratedEvents.filter(filterWaveEvents(showAttempted)).length;
};
