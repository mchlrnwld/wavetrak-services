import '../jsdoc';

/**
 * @description This is a helper function used by the /gps endpoint to convert
 * the timestamp properties on a session from unix seconds since epoch to
 * milliseconds since epoch.
 *
 * @param {RawSession} session
 * @returns {RawSession}
 */
export default function convertSessionTimestamps(session) {
  return {
    ...session,
    startTimestamp: session.startTimestamp * 1000,
    endTimestamp: session.endTimestamp * 1000,
    events: session.events.map(event => ({
      ...event,
      startTimestamp: event.startTimestamp * 1000,
      endTimestamp: event.endTimestamp * 1000,
      positions: event.positions.map(position => ({
        ...position,
        timestamp: position.timestamp * 1000,
      })),
    })),
  };
}
