/**
 * @description When we query a session from Mongo we want to use `.lean()`. The downsides is that
 * the maps are returned as JS Objects without the `get` and `set` functions on the prototype. This
 * function rehydrates the object into a map.
 * @param {{} | void} eventOverrides
 * @returns {Map<string, {}>}
 */
export const hydrateEventOverrides = eventOverrides => {
  // For some endpoints we may re-hydrate the events in order
  // to update the waveCount. This ensures that we return the
  // overrides properly
  if (eventOverrides instanceof Map) {
    return eventOverrides;
  }
  return eventOverrides ? new Map(Object.entries(eventOverrides)) : new Map();
};

export default (events, clips, eventOverrides) =>
  events.map(event => {
    const eventId = event?._id?.toString();
    const filteredClips = clips?.length
      ? clips.filter(clip => clip.eventId.toString() === eventId).map(clip => clip._id.toString())
      : null;

    return {
      ...event,
      ...eventOverrides?.get(eventId),
      _id: eventId,
      clips: filteredClips,
    };
  });
