import Joi from '@hapi/joi';
import mongoose from 'mongoose';

export const timestamp = Joi.date()
  .timestamp('javascript')
  .greater(946684800000); // Timestamps must be greater than 01/01/2000
export const isoDate = Joi.date().iso();

export const lat = Joi.number()
  .min(-90)
  .max(90);
export const lon = Joi.number()
  .min(-180)
  .max(180);

export const coordinates = Joi.array()
  .ordered(lon.required(), lat.required())
  .length(2);

export const joiObjectId = Joi.extend(joi => ({
  type: 'objectId',
  base: joi.string().regex(/^[0-9a-fA-F]{24}$/),
  messages: {
    valid: 'Must be a valid Object ID',
  },
  validate(value, helpers) {
    if (helpers.prefs.convert) {
      if (mongoose.Types.ObjectId.isValid(value)) {
        return { value: mongoose.Types.ObjectId(value) };
      }
      return { value, errors: helpers.error('valid') };
    }
    return { value };
  },
}));

export const location = Joi.object({
  type: Joi.string()
    .valid('Point')
    .required(),
  coordinates: coordinates.required(),
});

export const url = Joi.string()
  .uri({ scheme: ['https', 'http'] })
  .uri();

export const typeEnum = Joi.string()
  .uppercase()
  .valid('WAVE', 'PADDLE');

export const outcomeEnum = Joi.string()
  .uppercase()
  .valid('ATTEMPTED', 'CAUGHT');

export const visibilityEnum = Joi.string()
  .uppercase()
  .valid('PRIVATE', 'PUBLIC');

export const positiveNumber = Joi.number()
  .positive()
  .allow(0);

export const camsSchema = Joi.array().items(
  Joi.object({
    title: Joi.string().required(),
    alias: Joi.string().optional(),
    stillUrl: url.required(),
    isPremium: Joi.boolean().required(),
    visible: Joi.boolean()
      .optional()
      .default(true),
    id: joiObjectId.objectId().required(),
  }),
);

const statsSchema = {
  startTimestamp: isoDate.allow(null).optional(),
  endTimestamp: isoDate.allow(null).optional(),
  waveCount: positiveNumber.allow(null).optional(),
  calories: positiveNumber
    .default(0)
    .allow(null)
    .optional(),
  speedMax: positiveNumber.allow(null).optional(),
  distancePaddled: positiveNumber.allow(null).optional(),
  distanceWaves: positiveNumber.allow(null).optional(),
  wavesPerHour: positiveNumber.allow(null).optional(),
  longestWave: Joi.object({
    distance: positiveNumber.allow(null).optional(),
    seconds: positiveNumber.allow(null).optional(),
  }).optional(),
};

const spotSchema = {
  name: Joi.string().required(),
  id: joiObjectId
    .objectId()
    .allow(null)
    .optional(),
  location: Joi.object({
    type: Joi.string()
      .valid('Point')
      .required(),
    coordinates: coordinates.required(),
  }),
};

/** RAW SESSION */

export const positions = {
  timestamp: timestamp
    .max(Joi.ref('$maxEndTimestamp'))
    .min(Joi.ref('$minStartTimestamp'))
    .required(),
  latitude: lat.required(),
  longitude: lon.required(),
  horizontalAccuracy: Joi.optional(),
};

export const events = {
  startTimestamp: timestamp.min(Joi.ref('$minStartTimestamp')).required(),
  endTimestamp: timestamp
    .min(Joi.ref('startTimestamp'))
    .max(Joi.ref('$maxEndTimestamp'))
    .required(),
  type: typeEnum.required(),
  outcome: outcomeEnum.optional(),
  distance: positiveNumber.required(),
  speedMax: positiveNumber.required(),
  speedAverage: positiveNumber.required(),
  positions: Joi.array()
    .items(Joi.object(positions))
    .optional(),
};

export const session = {
  startTimestamp: timestamp.max(Joi.ref('$maxEndTimestamp')).required(),
  endTimestamp: timestamp.min(Joi.ref('$minStartTimestamp')).required(),
  createdAt: timestamp.default(() => new Date()),
  updatedAt: timestamp.default(() => new Date()),
  visibility: visibilityEnum.required(),
  waveCount: positiveNumber.required(),
  calories: positiveNumber.allow(null).optional(),
  speedMax: positiveNumber.required(),
  distancePaddled: positiveNumber.required(),
  distanceWaves: positiveNumber.required(),
  distanceLongestWave: positiveNumber.required(),
  deviceType: Joi.string().required(),
  surfline_id: Joi.any().strip(),
  events: Joi.array()
    .items(Joi.object(events))
    .required(),
};

/** ENRICHED SESSION */

export const enrichedPositions = Joi.array().items(
  Joi.object({
    timestamp: isoDate
      .max(Joi.ref('$maxEndTimestamp'))
      .min(Joi.ref('$minStartTimestamp'))
      .required(),
    location: location.required(),
  }),
);

export const enrichedEvents = {
  ...events,
  startTimestamp: isoDate.min(Joi.ref('$minStartTimestamp')).required(),
  endTimestamp: isoDate
    .min(Joi.ref('startTimestamp'))
    .max(Joi.ref('$maxEndTimestamp'))
    .required(),
  positions: enrichedPositions.optional(),
  _id: joiObjectId.objectId().required(),
};

export const enrichedSession = {
  ...session,
  startTimestamp: isoDate.min(Joi.ref('$minStartTimestamp')).required(),
  endTimestamp: isoDate.max(Joi.ref('$maxEndTimestamp')).required(),
  cams: camsSchema,
  name: Joi.string().required(),
  user: joiObjectId.objectId().optional(),
  client: joiObjectId.objectId().optional(),
  _id: joiObjectId.objectId().optional(),
  createdAt: isoDate.default(() => new Date()),
  updatedAt: isoDate.default(() => new Date()),
  status: Joi.string()
    .uppercase()
    .valid('ENRICHED')
    .required(),
  spotReportView: Joi.object(),
  surfline: Joi.object({
    stats: Joi.object(statsSchema),
  }),
  partner: Joi.object({
    stats: Joi.object(statsSchema),
  }),
  eventOverrides: Joi.object().optional(),
  clips: Joi.array()
    .items(
      Joi.object({
        _id: joiObjectId.objectId().required(),
        eventId: joiObjectId.objectId().required(),
        favorite: Joi.boolean()
          .optional()
          .default(false),
      }),
    )
    .required(),
  spot: Joi.object(spotSchema).required(),
  originalSpotId: joiObjectId
    .objectId()
    .allow(null)
    .optional(),
  userSpotId: joiObjectId
    .objectId()
    .allow(null)
    .optional(),
  events: Joi.array()
    .items(enrichedEvents)
    .required(),
};
