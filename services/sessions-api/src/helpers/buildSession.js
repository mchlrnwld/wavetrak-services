import mongoose from 'mongoose';

/**
 * @description Uses the Session JSON from the post body to create the
 * session object that we send to mongo.
 * @param {ValidatedSession} validatedSession
 * @return {ValidatedSessionWithPartner}
 */
const buildSession = validatedSession => {
  const stats = {
    startTimestamp: validatedSession?.startTimestamp,
    endTimestamp: validatedSession?.endTimestamp,
    waveCount: validatedSession?.waveCount || 0,
    speedMax: validatedSession?.speedMax,
    longestWave: {
      distance: validatedSession?.distanceLongestWave || 0,
      seconds: null,
    },
    distancePaddled: validatedSession?.distancePaddled,
    distanceWaves: validatedSession?.distanceWaves,
    calories: validatedSession?.calories,
  };

  const session = {
    ...validatedSession,
    partner: {
      stats,
    },
  };

  if (validatedSession.events) {
    // Build events objects
    session.events = validatedSession.events
      .map(event => {
        const eventId = event._id ? mongoose.Types.ObjectId(event._id) : mongoose.Types.ObjectId();
        if (event.positions) {
          return {
            ...event,
            _id: eventId,
            outcome: event.outcome ? event.outcome.toUpperCase() : 'CAUGHT',
            positions: event.positions.map(position => {
              const pos = {
                timestamp: position.timestamp,
                location: {
                  type: 'Point',
                  coordinates: [position.longitude, position.latitude],
                },
              };

              if (position.horizontalAccuracy) {
                pos.horizontalAccuracy = position.horizontalAccuracy;
              }

              return pos;
            }),
          };
        }
        return {
          ...event,
          _id: eventId,
        };
      })
      .sort((firstEvent, secondEvent) => firstEvent.startTimestamp - secondEvent.startTimestamp);
  }
  return session;
};

export default buildSession;
