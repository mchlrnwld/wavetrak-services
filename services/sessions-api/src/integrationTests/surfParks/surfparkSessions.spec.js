/* eslint-disable max-len */
import chai, { expect } from 'chai';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { dummyData, slId, MOCK_BAND_DATA, MOCK_POST_BODY } from './fixtures/surfparkFixtures';
import startApp from '../../server/server';
import log from '../../common/logger';
import SurfparkSessionBandModel from '../../models/surfparks/SurfparkSessionBands';

let thisServer;
let mongoServer;

describe('/surfparks', () => {
  beforeEach(async () => {
    mongoServer = await MongoMemoryServer.create();

    const mongoUri = await mongoServer.getUri();

    await mongoose.connect(
      mongoUri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      err => err,
    );

    const { server } = await startApp(log);
    thisServer = server;
  });

  afterEach(async () => {
    await SurfparkSessionBandModel.deleteMany();
    mongoose.disconnect();
    await mongoServer.stop();
    thisServer = null;
    mongoServer = null;
  });

  describe('GET /surfparks/sessions/feed', () => {
    describe('successful request', () => {
      it('should return a wave parks json with dummy data', async () => {
        const res = await chai
          .request(thisServer)
          .get('/surfparks/sessions/feed/')
          .set('x-auth-userid', slId);
        expect(res.status).to.equal(200);
        expect(res.body).to.deep.equal(dummyData);
      });
    });
    describe('invalid request', () => {
      it('should return 400 where no userId in the headers is provided', async () => {
        const res = await chai.request(thisServer).get('/surfparks/sessions/feed');
        expect(res.body.message).to.equal('Cannot get surfpark session user without header info.');
        expect(res.status).to.equal(400);
      });
    });
  });

  describe('POST /surfparks/sessions/band', () => {
    describe('successful request', () => {
      it('takes correctly populated body and userId in headers, and returns a json with saved band data, or duplicate message for an existing band-date mapping.', async () => {
        // This ensures in memory mongos indexes are created so we can test this case see this https://github.com/nodkz/mongodb-memory-server/issues/102
        await SurfparkSessionBandModel.createIndexes();

        const res = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .send(MOCK_POST_BODY)
          .set('x-auth-userid', slId);

        const data1 = await SurfparkSessionBandModel.find().exec();
        expect(data1.length).to.equal(1);

        expect(res.status).to.equal(200);
        const { activeDate, _id } = res.body;
        const expectedRes = MOCK_BAND_DATA(_id, activeDate);
        expect(res.body).to.deep.equal(expectedRes);

        const res2 = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .send(MOCK_POST_BODY)
          .set('x-auth-userid', slId);

        const data2 = await SurfparkSessionBandModel.find().exec();

        expect(res2.status).to.equal(202);
        expect(res2.body).to.deep.equal(expectedRes);
        expect(data2.length).to.equal(1);
      });

      it('takes correctly populated body but no userID in headers and should return a json with saved band data with null userId', async () => {
        const res = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .send(MOCK_POST_BODY);
        expect(res.status).to.equal(200);
        const { activeDate, _id } = res.body;
        const MOCK_RETURN_BAND = MOCK_BAND_DATA(_id, activeDate);
        MOCK_RETURN_BAND.userId = null;
        expect(res.body).to.deep.equal(MOCK_RETURN_BAND);
      });
      it('takes userID in headers and populated body without total sessions and should return a json with saved band data,', async () => {
        const mockBody = { ...MOCK_POST_BODY };
        delete mockBody.totalPaidSessions;
        const res = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .send(mockBody)
          .set('x-auth-userid', slId);
        expect(res.status).to.equal(200);
        const { activeDate, _id } = res.body;
        const MOCK_RETURN_BAND = MOCK_BAND_DATA(_id, activeDate);
        MOCK_RETURN_BAND.totalPaidSessions = 0;
        expect(res.body).to.deep.equal(MOCK_RETURN_BAND);
      });
    });
    describe('invalid request', () => {
      it('should return 400 where RFID is not defined', async () => {
        const mockBody = { ...MOCK_POST_BODY };
        delete mockBody.rfid;
        const res = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .send(mockBody)
          .set('x-auth-userid', slId);
        expect(res.status).to.equal(400);
        expect(res.body.name).to.equal('APIError');
        expect(res.body.message).to.equal(
          'Invalid Parameters: Cannot create a band object without both a band RFID and surfparkID',
        );
      });
      it('should return 400 where surfparkId is not defined', async () => {
        const mockBody = { ...MOCK_POST_BODY };
        delete mockBody.surfparkId;
        const res = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .send(mockBody)
          .set('x-auth-userid', slId);
        expect(res.status).to.equal(400);
        expect(res.body.name).to.equal('APIError');
        expect(res.body.message).to.equal(
          'Invalid Parameters: Cannot create a band object without both a band RFID and surfparkID',
        );
      });
      it('takes empty body and should return a 400 as surfparkId or RFID are not sent in req body', async () => {
        const res = await chai
          .request(thisServer)
          .post(`/surfparks/sessions/band`)
          .set('x-auth-userid', slId);
        expect(res.status).to.equal(400);
        expect(res.body.name).to.equal('APIError');
        expect(res.body.message).to.equal(
          'Invalid Parameters: Cannot create a band object without both a band RFID and surfparkID',
        );
      });
    });
  });
});
