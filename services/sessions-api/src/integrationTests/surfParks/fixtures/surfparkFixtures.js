export const slId = '5842041f4e65fad6a7708959';
export const dummyData = {
  associated: {
    timezone: [
      {
        key: 'America/Los_Angeles',
        abbr: 'PST',
        offset: -7,
        id: 1,
        path: 'associated.timezone',
      },
    ],
  },
  sessions: [
    {
      id: '620b77dabeeaa4188d2a8fea',
      timezone: {
        id: 1,
        path: 'associated.timezone',
      },
      surfpark: {
        name: 'Lower Trestles',
        websiteUrl: '',
        logo: {
          url:
            'https://sl-cam-thumbnails-high-freq-sandbox/583499c4e411dc743a5d5296/2021/02/08/583499c4e411dc743a5d5296.20210208T212408.jpg',
        },
        location: {
          type: 'POINT',
          coordinates: [33, -110],
        },
      },
      spot: {
        id: '584204204e65abc5a775520b',
        name: 'Lower Trestles',
        location: {
          type: 'POINT',
          coordinates: [33, -110],
        },
      },
      stats: {
        waveCount: 0,
      },
      clips: {
        total: 0,
        available: 0,
      },
      waves: [],
    },
    {
      id: '620b77df561e77ebb71c1706',
      timezone: {
        id: 1,
        path: 'associated.timezone',
      },
      surfpark: {
        name: 'Lower Trestles',
        websiteUrl: '',
        logo: {
          url:
            'https://sl-cam-thumbnails-high-freq-sandbox/583499c4e411dc743a5d5296/2021/02/08/583499c4e411dc743a5d5296.20210208T212408.jpg',
        },
        location: {
          type: 'POINT',
          coordinates: [33, -110],
        },
      },
      spot: {
        id: '584204204e65abc5a775520b',
        name: 'Lower Trestles',
        location: {
          type: 'POINT',
          coordinates: [33, -110],
        },
      },
      stats: {
        waveCount: 1,
      },
      clips: {
        total: 1,
        available: 1,
      },
      waves: [
        {
          id: '620b77e42ed827b294d0a8ad',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b796ccd690c24fccfffb2',
            },
          ],
        },
      ],
      startTimestamp: 1488878277,
      endTimestamp: 1488878277,
      preview: {
        thumbnails: [
          {
            url:
              'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
            sizes: ['small'],
          },
        ],
        clip: {
          clipUrl:
            'https://sl-live-cam-archive-staging.s3.us-west-1.amazonaws.com/live/uk-gwithian.stream.20210818T125651445.mp4',
        },
      },
    },
    {
      id: '284204254e65abc5a775520b',
      timezone: {
        id: 1,
        path: 'associated.timezone',
      },
      surfpark: {
        name: 'Lower Trestles',
        websiteUrl: '',
        logo: {
          url:
            'https://sl-cam-thumbnails-high-freq-sandbox/583499c4e411dc743a5d5296/2021/02/08/583499c4e411dc743a5d5296.20210208T212408.jpg',
        },
        location: {
          type: 'POINT',
          coordinates: [33, -110],
        },
      },
      spot: {
        id: '584204204e65abc5a775520b',
        name: 'Lower Trestles',
        location: {
          type: 'POINT',
          coordinates: [33, -110],
        },
      },
      stats: {
        waveCount: 10,
      },
      clips: {
        total: 10,
        available: 10,
      },
      waves: [
        {
          id: '620b77e981f5164b9ecc02d0',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b77ef1393277f01b6995e',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b77f6af9f314392d246ad',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78002d1e254b7ee11d06',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b7807929b201c351deed3',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b7814ef313367fcddff5d',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b781ba141a6f8b27b7da5',
            },
          ],
        },
        {
          id: '620b7823c14ce572a5a496e0',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b782c30602302e8c2620d',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78352d50dd33dc2d7121',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78421705c860c039d283',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78496d3910acd897927b',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78540825d52a5f5a8c72',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b789079c21e8b605a7fdf',
            },
          ],
        },
        {
          id: '620b78963da07d5e2ec97c58',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b789d9119df25bb348170',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78a2982c4ab2e36b9467',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78a6dd049dc591df464e',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78aaab8acab70c569463',
            },
          ],
        },
        {
          id: '234056789e65abc5a775509c',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78af71e826174fbec9ef',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78b3bacddd19ce16bfc0',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78b7d45bfe85617fdbc1',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78c25fca8acf91b637e4',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78c943a721c7c939dcd8',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78cdf3d9f731ff8a026c',
            },
          ],
        },
        {
          id: '234056789e65abc5a775566y',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78d1bdeba2b91fe8e9d0',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78d52827586aa116ea45',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78dc499041562a763d35',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78e361c0b4ec682fda1f',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78e79a934b0ba1ee774f',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78eb9ff3199ac2355c08',
            },
          ],
        },
        {
          id: '620b790d2c1522946e932c43',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78f033ca5b414bd9b69e',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78f4cded0fe57442662f',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78f70daeddaf0b9bfae0',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b78fd63117331c4e78ffa',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b7904528e3bf6dd3ccfc7',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b7908906f0aeb713e33b7',
            },
          ],
        },
        {
          id: '620b79135b11889b1662f3c3',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b791a2d42a0cf3b1cbc84',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b791ea59c4eb80c86a7b0',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b792314ed2c43201c2e8a',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b79267b6ad35d1852df0a',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b792afdc52bb9961aa42c',
            },
          ],
        },
        {
          id: '234056789e65abc5a987984n',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b792de311a98aa68864b2',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b793121c7672e86722685',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b793530d691ea1e03c89b',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b793985b2a0b9be7425ab',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b793c029364981419aa21',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b793fc5ea950bcccfe55c',
            },
          ],
        },
        {
          id: '620b79430d06b64bab2c34d3',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b794a1da6713c9e9a02a3',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b79562158683e87bdb7d1',
            },
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b795ac1990fbcbebfeb61',
            },
          ],
        },
        {
          id: '620b795f9cf55c0058f8bc8e',
          startTimestamp: 1488878277,
          clips: [
            {
              clipUrl:
                'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
              startTimestamp: 1488878277,
              preview: {
                thumbnails: [
                  {
                    url:
                      'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
                    sizes: ['small'],
                  },
                ],
                clip: {
                  clipUrl:
                    'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4',
                },
              },
              clipId: '620b7962ea2014fabc9427d6',
            },
          ],
        },
      ],
      startTimestamp: 1488878277,
      endTimestamp: 1488878277,
      preview: {
        thumbnails: [
          {
            url:
              'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
            sizes: ['small'],
          },
        ],
        clip: {
          clipUrl:
            'https://sl-live-cam-archive-staging.s3.us-west-1.amazonaws.com/live/uk-gwithian.stream.20210818T125651445.mp4',
        },
      },
    },
  ],
};

const RFID = '1234abcd';
const MOCK_PARK_ID = '5842041f4e65fad6a7708956';
const MOCK_PAID_SESSIONS = 1;

export const MOCK_POST_BODY = {
  surfparkId: MOCK_PARK_ID,
  totalPaidSessions: MOCK_PAID_SESSIONS,
  rfid: RFID,
};

export const MOCK_BAND_DATA = (_id, date) => ({
  __v: 0,
  _id,
  rfid: RFID,
  surfparkId: MOCK_PARK_ID,
  activeDate: date,
  createdDate: date,
  updatedDate: date,
  totalPaidSessions: MOCK_PAID_SESSIONS,
  userId: slId,
});
