import mongoose from 'mongoose';
import garminParserOutputOriginal from './fixtures/expectedGarminParserOutput.json';

export const garminParserOutput = garminParserOutputOriginal;

export const S3Input = id => ({
  Body: [
    {
      startTimestamp: '2020-03-18T17:23:56.000Z',
      endTimestamp: '2020-03-18T17:24:02.000Z',
      type: 'WAVE',
      distance: 49.55,
      speedAverage: 1,
      speedMax: 1.101,
      positions: [
        {
          timestamp: '2020-03-18T17:23:56.000Z',
          location: { type: 'Point', coordinates: [-118.00564521923661, 33.6542122811079] },
        },
        {
          timestamp: '2020-03-18T17:23:57.000Z',
          location: { type: 'Point', coordinates: [-118.00561512820423, 33.65435108542442] },
        },
        {
          timestamp: '2020-03-18T17:23:58.000Z',
          location: { type: 'Point', coordinates: [-118.00563323311508, 33.65431856364012] },
        },
        {
          timestamp: '2020-03-18T17:23:59.000Z',
          location: { type: 'Point', coordinates: [-118.005654187873, 33.65425813011825] },
        },
        {
          timestamp: '2020-03-18T17:24:00.000Z',
          location: { type: 'Point', coordinates: [-118.00567908212543, 33.65423114039004] },
        },
        {
          timestamp: '2020-03-18T17:24:01.000Z',
          location: { type: 'Point', coordinates: [-118.00566826947033, 33.654229547828436] },
        },
        {
          timestamp: '2020-03-18T17:24:02.000Z',
          location: { type: 'Point', coordinates: [-118.00565745681524, 33.654229464009404] },
        },
      ],
      _id: '6033854b84236d6ecdae1aa3',
      outcome: 'CAUGHT',
    },
  ],
  Bucket: 'sl-sessions-api-sessions-events-sandbox-fake',
  Key: `${id}.json`,
  ContentType: 'application/json',
  ContentEncoding: 'utf8',
});

export const expectedSession = (id, createdAt, updatedAt) => ({
  __v: 0,
  startTimestamp: new Date('2020-03-18T17:23:56.000Z').toString(),
  endTimestamp: new Date('2020-03-18T17:24:02.000Z').toString(),
  visibility: 'PUBLIC',
  waveCount: 1,
  speedMax: 1.101,
  spot: {
    location: {
      coordinates: [],
    },
  },
  distancePaddled: 0,
  deviceType: 'vivoactive3',
  distanceLongestWave: 49.55,
  distanceWaves: 49.55,
  user: mongoose.Types.ObjectId('5aabfbc0ffd66500117e554d'),
  createdAt: new Date(createdAt).toString(),
  updatedAt: new Date(updatedAt).toString(),
  partner: {
    stats: {
      startTimestamp: new Date('2020-03-18T17:23:56.000Z').toString(),
      endTimestamp: new Date('2020-03-18T17:24:02.000Z').toString(),
      waveCount: 1,
      speedMax: 1.101,
      longestWave: { distance: 49.55, seconds: null },
      distancePaddled: 0,
      distanceWaves: 49.55,
    },
  },
  client: mongoose.Types.ObjectId('5af1ce73b5acf7c6dd2592ee'),
  name: 'Morning Shift',
  notified: false,
  status: 'PENDING',
  gpsCenter: { type: 'Point', coordinates: [-118.00565036812925, 33.65426145893282] },
  _id: mongoose.Types.ObjectId(id),
  startLatLon: '33.6542122811079,-118.00564521923661',
  appVersion: 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173',
});

export const SQSInput = (id, createdAt, updatedAt) => ({
  QueueUrl:
    'https://sqs.us-west-1.amazonaws.com/665294954271/sl-sessions-api-sessions-enricher-queue-sandbox-fake',
  MessageBody: {
    method: 'enrich',
    session: {
      appVersion: 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173',
      startTimestamp: '2020-03-18T17:23:56.000Z',
      endTimestamp: '2020-03-18T17:24:02.000Z',
      visibility: 'PUBLIC',
      waveCount: 1,
      speedMax: 1.101,
      distancePaddled: 0,
      deviceType: 'vivoactive3',
      distanceLongestWave: 49.55,
      distanceWaves: 49.55,
      user: '5aabfbc0ffd66500117e554d',
      clientId: '5af1ce73b5acf7c6dd2592ee', // Garmin parser attaches this to the object but we use the one from the headers.
      createdAt,
      updatedAt,
      partner: {
        stats: {
          startTimestamp: '2020-03-18T17:23:56.000Z',
          endTimestamp: '2020-03-18T17:24:02.000Z',
          waveCount: 1,
          speedMax: 1.101,
          longestWave: { distance: 49.55, seconds: null },
          distancePaddled: 0,
          distanceWaves: 49.55,
        },
      },
      client: '5af1ce73b5acf7c6dd2592ee',
      name: 'Morning Shift',
      status: 'PENDING',
      startLatLon: '33.6542122811079,-118.00564521923661',
      gpsCenter: { type: 'Point', coordinates: [-118.00565036812925, 33.65426145893282] },
      _id: id,
    },
  },
});

export const S3GpsInput = id => ({
  Body: [
    {
      distance: 1.4010909645705965,
      endTimestamp: '2014-08-30T13:22:50.000Z',
      positions: [
        {
          timestamp: '2014-08-30T13:22:34.000Z',
          location: {
            type: 'Point',
            coordinates: [-4.083272, 50.317242],
          },
        },
        {
          timestamp: '2014-08-30T13:22:35.000Z',
          location: {
            type: 'Point',
            coordinates: [-4.083271, 50.317243],
          },
        },
      ],
      speedAverage: 0.15553270090428506,
      speedMax: 0.3411536715030103,
      startTimestamp: '2014-08-30T13:22:34.000Z',
      type: 'PADDLE',
      _id: '6035132c172a0887289b0f69',
      outcome: 'CAUGHT',
    },
    {
      distance: 31.680635286120584,
      endTimestamp: '2014-08-30T13:23:48.000Z',
      positions: [
        {
          timestamp: '2014-08-30T13:23:33.000Z',
          location: {
            type: 'Point',
            coordinates: [-4.083267, 50.31737],
          },
        },
        {
          timestamp: '2014-08-30T13:23:34.000Z',
          location: {
            type: 'Point',
            coordinates: [-4.083282, 50.317392],
          },
        },
      ],
      speedAverage: 2.4292571065997453,
      speedMax: 2.668825938621285,
      startTimestamp: '2014-08-30T13:23:33.000Z',
      type: 'WAVE',
      _id: '6035132c172a0887289b0f6a',
      outcome: 'CAUGHT',
    },
  ],
  Bucket: 'sl-sessions-api-sessions-events-sandbox-fake',
  Key: `${id}.json`,
  ContentType: 'application/json',
  ContentEncoding: 'utf8',
});

export const expectedGpsSession = (id, createdAt, updatedAt) => ({
  _id: mongoose.Types.ObjectId(id),
  spot: {
    location: {
      coordinates: [],
    },
  },
  gpsCenter: {
    type: 'Point',
    coordinates: [-118.00565036812925, 33.65426145893282],
  },
  appVersion: 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173',
  distanceLongestWave: 31.680635286120584,
  notified: false,
  deviceType: 'Surfline Apple Watch',
  distancePaddled: 126.90374544124955,
  distanceWaves: 63.03542294500738,
  endTimestamp: new Date('2014-08-30T13:27:46.000Z').toString(),
  speedMax: 4.5439977640772,
  startTimestamp: new Date('2014-08-30T13:22:34.000Z').toString(),
  visibility: 'PRIVATE',
  waveCount: 2,
  createdAt: new Date(createdAt).toString(),
  updatedAt: new Date(updatedAt).toString(),
  partner: {
    stats: {
      waveCount: 2,
      startTimestamp: new Date('2014-08-30T13:22:34.000Z').toString(),
      endTimestamp: new Date('2014-08-30T13:27:46.000Z').toString(),
      speedMax: 4.5439977640772,
      longestWave: {
        distance: 31.680635286120584,
        seconds: null,
      },
      distancePaddled: 126.90374544124955,
      distanceWaves: 63.03542294500738,
    },
  },
  user: mongoose.Types.ObjectId('5aabfbc0ffd66500117e554d'),
  client: mongoose.Types.ObjectId('5af1ce73b5acf7c6dd2592ee'),
  name: 'Afternoon Session',
  status: 'PENDING',
  startLatLon: '50.317242,-4.083272',
  __v: 0,
});

export const SQSGpsInput = (id, createdAt, updatedAt) => ({
  QueueUrl:
    'https://sqs.us-west-1.amazonaws.com/665294954271/sl-sessions-api-sessions-enricher-queue-sandbox-fake',
  MessageBody: {
    method: 'enrich',
    session: {
      appVersion: 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173',
      deviceType: 'Surfline Apple Watch',
      distanceLongestWave: 31.680635286120584,
      distancePaddled: 126.90374544124955,
      distanceWaves: 63.03542294500738,
      endTimestamp: '2014-08-30T13:27:46.000Z',
      speedMax: 4.5439977640772,
      startTimestamp: '2014-08-30T13:22:34.000Z',
      visibility: 'PRIVATE',
      waveCount: 2,
      createdAt,
      updatedAt,
      partner: {
        stats: {
          startTimestamp: '2014-08-30T13:22:34.000Z',
          endTimestamp: '2014-08-30T13:27:46.000Z',
          waveCount: 2,
          speedMax: 4.5439977640772,
          longestWave: {
            distance: 31.680635286120584,
            seconds: null,
          },
          distancePaddled: 126.90374544124955,
          distanceWaves: 63.03542294500738,
        },
      },
      user: '5aabfbc0ffd66500117e554d',
      client: '5af1ce73b5acf7c6dd2592ee',
      name: 'Afternoon Session',
      status: 'PENDING',
      startLatLon: '50.317242,-4.083272',
      gpsCenter: {
        type: 'Point',
        coordinates: [-118.00565036812925, 33.65426145893282],
      },
      _id: id,
    },
  },
});

export const gpsSessionData = {
  positions: [
    {
      timestamp: 1584552236000,
      latitude: 33.6542122811079,
      longitude: -118.00564521923661,
    },
    {
      timestamp: 1584552237000,
      latitude: 33.65435108542442,
      longitude: -118.00561512820423,
    },
    {
      timestamp: 1584552238000,
      latitude: 33.65431856364012,
      longitude: -118.00563323311508,
    },
    {
      timestamp: 1584552239000,
      latitude: 33.65425813011825,
      longitude: -118.005654187873,
    },
    {
      timestamp: 1584552240000,
      latitude: 33.65423114039004,
      longitude: -118.00567908212543,
    },
    {
      timestamp: 1584552241000,
      latitude: 33.654229547828436,
      longitude: -118.00566826947033,
    },
    {
      timestamp: 1584552242000,
      latitude: 33.654229464009404,
      longitude: -118.00565745681524,
    },
  ],
};
