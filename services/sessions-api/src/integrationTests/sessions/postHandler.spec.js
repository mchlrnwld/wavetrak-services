import chai, { expect } from 'chai';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import newrelic from 'newrelic';
import sinon from 'sinon';
import * as AWS from 'aws-sdk';
import nock from 'nock';
import * as fetch from 'node-fetch';
import startApp from '../../server/server';
import log from '../../common/logger';
import * as awsWrapper from '../../external/aws';
import Session from '../../models/Session';
import {
  expectedSession,
  S3Input,
  SQSInput,
  garminParserOutput,
  gpsSessionData,
  S3GpsInput,
  SQSGpsInput,
  expectedGpsSession,
} from './postHandlerExpectedData';
import analyzedSession from './fixtures/analyzedSession.json';

import config from '../../config';
import * as calculateSessionGPSCenter from '../../utils/calculateSessionGPSCenter';

let mongoServer;
let thisServer;
const { SESSION_ANALYZER, SESSION_ENRICHER_QUEUE, APP_VERSION } = config;
const { SESSIONS_EVENTS_BUCKET } = process.env;

describe('POST', () => {
  before(() => {
    sinon.stub(AWS.SQS.prototype, 'constructor');
    sinon.stub(AWS.S3.prototype, 'constructor');
    process.env.SESSIONS_EVENTS_BUCKET = 'sl-sessions-api-sessions-events-sandbox-fake';
    config.SESSION_ENRICHER_QUEUE =
      'https://sqs.us-west-1.amazonaws.com/665294954271/sl-sessions-api-sessions-enricher-queue-sandbox-fake';
    config.APP_VERSION = 'a0f5e56e6fe39c2e6c8eff3641e28cd2bdc42173';
  });

  after(() => {
    AWS.S3.prototype.constructor.restore();
    AWS.SQS.prototype.constructor.restore();
    process.env.SESSIONS_EVENTS_BUCKET = SESSIONS_EVENTS_BUCKET;
    config.SESSION_ENRICHER_QUEUE = SESSION_ENRICHER_QUEUE;
    config.APP_VERSION = APP_VERSION;
  });

  describe('POST sessions/', () => {
    beforeEach(async () => {
      mongoServer = await MongoMemoryServer.create();

      const mongoUri = await mongoServer.getUri();
      await mongoose.connect(
        mongoUri,
        { useNewUrlParser: true, useUnifiedTopology: true },
        err => err,
      );
      const { server } = await startApp(log);
      thisServer = server;
      sinon.spy(awsWrapper, 'uploadSessionEvents');
      sinon.spy(newrelic, 'recordCustomEvent');
    });

    afterEach(async () => {
      mongoServer = null;
      mongoose.disconnect();
      awsWrapper.uploadSessionEvents.restore();
      newrelic.recordCustomEvent.restore();
      nock.cleanAll();
    });

    it('should return 401 if no auth headers are provided', done => {
      chai
        .request(thisServer)
        .post('/sessions/')
        .end((err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
    it('should return 400 if positions are empty', done => {
      chai
        .request(thisServer)
        .post('/sessions/')
        .set('x-api-key', 'testApiKey')
        .set('X-Auth-UserId', 'testUserId')
        .send({ events: [{ positions: [] }] })
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
    it('should return 400 if positions are invalid', done => {
      chai
        .request(thisServer)
        .post('/sessions/')
        .set('x-api-key', 'testApiKey')
        .set('X-Auth-UserId', 'testUserId')
        .send({ events: [{ positions: [1, 2, 3] }] })
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
    it('should return 200 if positions are valid and if duplicate session exists.', done => {
      const S3Fake = sinon.fake.returns({
        promise: sinon.fake.resolves(''),
      });
      const SQSFake = sinon.fake.returns({
        promise: sinon.fake.resolves(''),
      });
      AWS.S3.prototype.constructor.returns({ putObject: S3Fake });
      AWS.SQS.prototype.constructor.returns({ sendMessage: SQSFake });
      chai
        .request(thisServer)
        .post('/sessions/')
        .set('x-api-key', '5af1ce73b5acf7c6dd2592ee')
        .set('X-Auth-UserId', '5aabfbc0ffd66500117e554d')
        .send(garminParserOutput)
        .catch(err => {
          done(err);
        })
        .then(async res => {
          expect(res).to.have.status(200);

          const sessionId = awsWrapper.uploadSessionEvents.getCall(0).args[0];

          const { createdAt, updatedAt } = JSON.parse(
            SQSFake.getCall(0).args[0].MessageBody,
          ).session;

          const S3Call = S3Fake.getCall(0).args[0];
          S3Call.Body = JSON.parse(S3Call.Body);
          expect(S3Call).to.eql(S3Input(sessionId.toString()));

          const SQSCall = SQSFake.getCall(0).args[0];
          SQSCall.MessageBody = JSON.parse(SQSCall.MessageBody);
          expect(SQSCall).to.eql(SQSInput(sessionId.toString(), createdAt, updatedAt));

          const session = await Session.findById(sessionId).lean();

          // iterate over keys to prevent incorrect value assignment
          ['endTimestamp', 'createdAt', 'startTimestamp', 'updatedAt'].forEach(key => {
            session[key] = session[key].toString();
          });

          ['endTimestamp', 'startTimestamp'].forEach(key => {
            session.partner.stats[key] = session.partner.stats[key].toString();
          });

          expect(session).to.eql(expectedSession(sessionId, createdAt, updatedAt));
          chai
            .request(thisServer)
            .post('/sessions/')
            .set('x-api-key', '5af1ce73b5acf7c6dd2592ee')
            .set('X-Auth-UserId', '5aabfbc0ffd66500117e554d')
            .send(garminParserOutput)
            .catch(err => {
              done(err);
            })
            .then(async res2 => {
              expect(res2).to.have.status(200);
              const nrObjects = newrelic.recordCustomEvent.getCall(2).args[1];
              expect(nrObjects).to.eql({
                event: 'Duplicate Session',
                userId: '5aabfbc0ffd66500117e554d',
                startTimestamp: 1584552236000,
                endTimestamp: 1584552242000,
                exisitingSession: sessionId.toString(),
              });
              done();
            })
            .catch(err => {
              done(err);
            });
        })
        .catch(err => {
          done(err);
        });
    });
  });
  describe('POST sessions/gps/', () => {
    beforeEach(async () => {
      mongoServer = await MongoMemoryServer.create();
      const mongoUri = await mongoServer.getUri();
      await mongoose.connect(
        mongoUri,
        { useNewUrlParser: true, useUnifiedTopology: true },
        err => err,
      );

      const { server } = startApp(log);
      thisServer = server;
      config.SESSION_ANALYZER = 'http://testAnalyserUrl.com';

      sinon.spy(awsWrapper, 'uploadSessionEvents');
      sinon.spy(newrelic, 'recordCustomEvent');
    });

    afterEach(() => {
      mongoServer = null;
      mongoose.disconnect();
      config.SESSION_ANALYZER = SESSION_ANALYZER;

      awsWrapper.uploadSessionEvents.restore();
      newrelic.recordCustomEvent.restore();
      nock.cleanAll();
    });

    it('should return 401 if no auth headers are provided', done => {
      chai
        .request(thisServer)
        .post('/sessions/gps/')
        .end((err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
    it('should return 400 if positions are empty', done => {
      chai
        .request(thisServer)
        .post('/sessions/gps/')
        .set('x-api-key', 'testApiKey')
        .set('X-Auth-UserId', 'testUserId')
        .set('content-length', 2)
        .send({ positions: [] })
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
    it('should return 200 if positions are valid or if duplicate session is detected.', done => {
      sinon.spy(fetch, 'default');
      sinon.spy(calculateSessionGPSCenter, 'calculateSessionGPSCenter');

      nock(`${config.SESSION_ANALYZER}`)
        .defaultReplyHeaders({
          'Content-Type': 'application/json',
          'Content-Length': '2',
        })
        .post('/analyze')
        .reply(200, analyzedSession)
        .post('/analyze')
        .reply(200, analyzedSession);

      const S3Fake = sinon.fake.returns({
        promise: sinon.fake.resolves(true),
      });
      const SQSFake = sinon.fake.returns({
        promise: sinon.fake.resolves(true),
      });

      AWS.S3.prototype.constructor.returns({ putObject: S3Fake });
      AWS.SQS.prototype.constructor.returns({ sendMessage: SQSFake });

      chai
        .request(thisServer)
        .post('/sessions/gps/')
        .set('x-api-key', '5af1ce73b5acf7c6dd2592ee')
        .set('X-Auth-UserId', '5aabfbc0ffd66500117e554d')
        .send(gpsSessionData)
        .catch(err => {
          done(err);
        })
        .then(async res => {
          expect(res).to.have.status(200);

          const gpsCenter = calculateSessionGPSCenter.calculateSessionGPSCenter.getCall(0)
            .returnValue;

          expect(fetch.default).to.have.been.calledWith(`${config.SESSION_ANALYZER}/analyze`, {
            method: 'POST',
            body: JSON.stringify({ ...gpsSessionData, gpsCenter }),
            headers: { 'Content-Type': 'application/json' },
          });

          const sessionId = awsWrapper.uploadSessionEvents.getCall(0).args[0];

          const { createdAt, updatedAt } = JSON.parse(
            SQSFake.getCall(0).args[0].MessageBody,
          ).session;

          const S3Call = S3Fake.getCall(0).args[0];
          S3Call.Body = JSON.parse(S3Call.Body);

          expect(S3Call).to.eql(S3GpsInput(sessionId.toString()));

          const SQSCall = SQSFake.getCall(0).args[0];
          SQSCall.MessageBody = JSON.parse(SQSCall.MessageBody);
          expect(SQSCall).to.eql(SQSGpsInput(sessionId.toString(), createdAt, updatedAt));

          const session = await Session.findById(sessionId).lean();

          // iterate over keys to prevent incorrect value assignment
          ['endTimestamp', 'createdAt', 'startTimestamp', 'updatedAt'].forEach(key => {
            session[key] = session[key].toString();
          });

          ['endTimestamp', 'startTimestamp'].forEach(key => {
            session.partner.stats[key] = session.partner.stats[key].toString();
          });

          expect(session).to.eql(expectedGpsSession(sessionId, createdAt, updatedAt));
          chai
            .request(thisServer)
            .post('/sessions/gps/')
            .set('x-api-key', '5af1ce73b5acf7c6dd2592ee')
            .set('X-Auth-UserId', '5aabfbc0ffd66500117e554d')
            .send(gpsSessionData)
            .catch(err => {
              done(err);
            })
            .then(async res2 => {
              expect(res2).to.have.status(200);
              const nrObjects = newrelic.recordCustomEvent.getCall(2).args[1];
              expect(nrObjects).to.eql({
                event: 'Duplicate Session',
                userId: '5aabfbc0ffd66500117e554d',
                startTimestamp: 1409404954000,
                endTimestamp: 1409405266000,
                exisitingSession: sessionId.toString(),
              });
              done();
            })
            .catch(err => {
              done(err);
            });
        })
        .catch(err => {
          done(err);
        });
    });
  });
});
