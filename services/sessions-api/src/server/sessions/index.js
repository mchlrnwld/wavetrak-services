import { Router } from 'express';
import { authenticateRequest, wrapErrors, wrapErrorsToNewRelic } from '@surfline/services-common';
import {
  getSessionHandler,
  postSessionHandler,
  deleteSessionHandler,
  putSessionHandler,
  putSessionDetailsHandler,
  patchSessionDetailsHandler,
  patchSessionWaveHandler,
  getSessionDetailsHandler,
  getSessionFeedHandler,
  getSessionsSummaryHandler,
  putSessionCamVisibility,
  getSessionClipHandler,
  postGpsHandler,
} from './sessions';
import { failed, sessionUsers } from './handlers';
import trackRequests from '../../common/trackRequests';
import getSessionMiddleware from '../../helpers/getSessionMiddleware';
import webhooks from '../webhooks';

const sessions = log => {
  const api = Router();
  const {
    getSessionUsers,
    getSessionUser,
    createSessionUser,
    updateSessionUser,
    patchSessionUser,
    deleteSessionUser,
  } = sessionUsers;

  api.use('*', trackRequests(log));

  api.get(
    '/',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(getSessionHandler),
  );

  api.post(
    '/',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrorsToNewRelic(postSessionHandler),
  );

  api.put(
    '/:sessionId',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(putSessionHandler),
  );

  api.delete(
    '/:sessionId',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(deleteSessionHandler),
  );

  api.put(
    '/:sessionId/details',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(putSessionDetailsHandler),
  );

  api.put(
    '/:sessionId/cams/:cameraId',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(putSessionCamVisibility),
  );

  api.patch(
    '/:sessionId/details',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(getSessionMiddleware),
    wrapErrors(patchSessionDetailsHandler),
  );

  api.get(
    '/:sessionId/details',
    authenticateRequest(),
    wrapErrors(getSessionMiddleware),
    wrapErrors(getSessionDetailsHandler),
  );

  api.patch(
    '/:sessionId/waves/:waveId',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(getSessionMiddleware),
    wrapErrors(patchSessionWaveHandler),
  );

  api.get(
    '/feed',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(getSessionFeedHandler),
  );

  api.get(
    '/summary',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrors(getSessionsSummaryHandler),
  );

  api.get(
    '/clips/:clipId',
    authenticateRequest({ userIdRequired: false, clientIdRequired: false }),
    wrapErrors(getSessionClipHandler),
  );

  api.post(
    '/gps',
    authenticateRequest({ userIdRequired: true, clientIdRequired: true }),
    wrapErrorsToNewRelic(postGpsHandler),
  );

  api.use('/webhooks', webhooks(log));

  api.get('/failed', wrapErrors(failed.getFailedSessions));

  // Special Session Users
  api.get('/users', wrapErrors(getSessionUsers));
  api.get('/users/:sessionUserId', wrapErrors(getSessionUser));
  api.post('/users', wrapErrors(createSessionUser));
  api.put('/users/:sessionUserId', wrapErrors(updateSessionUser));
  api.patch('/users/:sessionUserId', wrapErrors(patchSessionUser));
  api.delete('/users/:sessionUserId', wrapErrors(deleteSessionUser));

  return api;
};

export default sessions;
