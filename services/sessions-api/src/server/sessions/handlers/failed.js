import { getSessionsByStatus, getStalePendingSessions } from '../../../models';

/* eslint-disable import/prefer-default-export */
export const getFailedSessions = async (req, res) => {
  const failedSessions = await getSessionsByStatus('FAILED', false);
  const stalePendingSession = await getStalePendingSessions();

  const sessions = [...failedSessions, ...stalePendingSession].map(session => ({
    id: session._id,
    user: session.user,
    client: session.client,
  }));

  return res.send(sessions);
};
