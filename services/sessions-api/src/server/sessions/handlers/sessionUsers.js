import mongoose from 'mongoose';
import { APIError } from '@surfline/services-common';
import SessionUser, { insertSessionUser } from '../../../models/SessionUser';
import { getSessionUser as getSessionUserFromDB } from '../../../models';

const { ObjectId } = mongoose.Types;

/* eslint-disable import/prefer-default-export */
export const getSessionUsers = async (req, res) => {
  const users = await SessionUser.find({ status: { $ne: 'DELETED' } }).lean();
  return res.send(users);
};

export const getSessionUser = async (req, res) => {
  const { sessionUserId } = req.params;
  const sessionUser = await getSessionUserFromDB(sessionUserId);
  return res.send(sessionUser.length > 0 ? sessionUser[0] : {});
};

export const createSessionUser = async (req, res) => {
  const { user, groups, nickname } = req.body;

  if (!user) throw new APIError('Cannot create user without a valid user ID');

  if (!groups || groups?.length === 0) {
    throw new APIError('Cannot create user without a valid user group');
  }

  if (!nickname) throw new APIError('Cannot create user without a valid user nickname');

  if (!ObjectId.isValid(user)) throw new APIError('Cannot create user with an invalid user ID');

  const duplicateEntryCheck = await SessionUser.findOne({
    status: 'ACTIVE',
    user: new ObjectId(user),
  });

  if (duplicateEntryCheck) throw new APIError('Cannot create duplicate session user');

  const sessionUser = {
    user: new ObjectId(user),
    groups,
    nickname,
    status: 'ACTIVE',
  };

  const { insertedId } = await insertSessionUser(sessionUser);

  return res.send({
    _id: insertedId,
    ...sessionUser,
  });
};

export const updateSessionUser = async (req, res) => {
  const { sessionUserId } = req.params;
  const { user, groups, status, nickname } = req.body;
  if (!sessionUserId) throw new APIError('Cannot update session user with invalid session user ID');
  if (!user) throw new APIError('Cannot update session user without a user ID');
  if (!groups || groups?.length === 0) {
    throw new APIError('Cannot update session user without a user group');
  }
  if (!status) throw new APIError('Cannot update session user without a user status');
  if (!nickname) throw new APIError('Cannot update session user without a user nickname');
  let sessionUser = {};
  try {
    sessionUser = await SessionUser.findOneAndUpdate(
      {
        _id: ObjectId(sessionUserId),
      },
      {
        groups,
        user: new ObjectId(user),
        status,
      },
      { runValidators: true },
    ).lean();
  } catch (error) {
    throw new APIError(error.message);
  }
  return res.send({ ...sessionUser, groups, user, status, nickname });
};

export const patchSessionUser = async (req, res) => {
  const { sessionUserId } = req.params;
  const { user, groups, status, nickname } = req.body;
  if (!sessionUserId)
    throw new APIError('Cannot update session user with an invalid session user ID');
  if (groups?.length === 0) {
    throw new APIError('Cannot update session user without user group');
  }
  const setPatch = {};
  if (user) setPatch.user = new ObjectId(user);
  if (groups) setPatch.groups = groups;
  if (status) setPatch.status = status;
  if (nickname) setPatch.nickname = nickname;
  let sessionUser = {};
  try {
    sessionUser = await SessionUser.findOneAndUpdate(
      {
        _id: ObjectId(sessionUserId),
      },
      setPatch,
      { runValidators: true },
    ).lean();
  } catch (error) {
    throw new APIError(error.message);
  }
  return res.send({ ...sessionUser, ...setPatch });
};

export const deleteSessionUser = async (req, res) => {
  const { sessionUserId } = req.params;
  if (!sessionUserId) throw new APIError('Cannot delete with invalid session user ID');
  let sessionUser = {};
  try {
    sessionUser = await SessionUser.findOneAndUpdate(
      {
        _id: ObjectId(sessionUserId),
      },
      { status: 'DELETED' },
      { runValidators: true },
    ).lean();
  } catch (error) {
    throw new APIError(error.message);
  }
  return res.send({ ...sessionUser, status: 'DELETED' });
};
