import AWS from 'aws-sdk';
import newrelic from 'newrelic';
import config from '../../config';
import markSessionFailed from '../../helpers/markSessionFailed';

/**
 * @param {Session | EnrichedSession} session
 * @param {boolean} reEnrich
 * @param {boolean} deleteClips
 */
const queueSessionForEnrichment = async (session, reEnrich, deleteClips) => {
  try {
    const sqs = new AWS.SQS({ apiVersion: '2012-11-05', region: 'us-west-1' });

    const response = await sqs
      .sendMessage({
        QueueUrl: config.SESSION_ENRICHER_QUEUE,
        MessageBody: JSON.stringify({
          method: 'enrich',
          session,
          reEnrich,
          deleteClips,
        }),
      })
      .promise();
    return response;
  } catch (error) {
    newrelic.recordCustomEvent('Sessions', {
      event: 'queueSessionForEnrichment failed',
      sessionId: session?._id,
      userId: session?.user,
      error: error.message,
    });
    newrelic.noticeError(error);
    await markSessionFailed(session._id, session.user);
    return null;
  }
};

export default queueSessionForEnrichment;
