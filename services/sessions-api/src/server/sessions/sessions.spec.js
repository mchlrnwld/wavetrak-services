import { expect } from 'chai';
import mongoose from 'mongoose';
import sinon from 'sinon';
import {
  patchSessionDetailsHandler,
  patchSessionWaveHandler,
  getSessionClipHandler,
  postGpsHandler,
  deleteSessionHandler,
  postSessionHandler,
} from './sessions';
import * as Session from '../../models/Session';
import * as SessionModel from '../../models';
import * as queueSessionForEnrichment from './enrich';
import * as userApi from '../../external/user';
import * as aws from '../../external/aws';
import * as clips from '../../external/clips';
import * as convertUnits from '../../utils/convertUnits';
import * as analyzer from '../../external/analyzer';
import gpsSessionData from '../../fixtures/gpsSessionData.json';
import analyzedSession from '../../fixtures/analyzedSession.json';
import * as existsDuplicateSession from '../../helpers/duplicateSession';

const CLIP_ID = mongoose.Types.ObjectId('5e011bc25b891e33b3915966').toString();

const USER_SETTINGS_FIXTURE = {
  units: {
    temperature: 'F',
    windSpeed: 'KTS',
    tideHeight: 'FT',
    swellHeight: 'FT',
    surfHeight: 'FT',
    distance: 'FT',
    speed: 'MPH',
  },
};

const USER_NAME_FIXTURE = { firstName: 'Jeremy', lastName: 'Monson' };

const GET_SESSION_CLIP_FIXTURE = {
  _id: '5ba4749a0cd32de9632c9955',
  spot: {
    id: '5842041f4e65fad6a77088ea',
    name: 'Goldenwest',
    location: { type: 'Point', coordinates: [1, 1] },
  },
  user: '58f00c0fb016012247d2ceb5',
  spotReportView: {
    someData: true,
  },
  clips: [
    {
      _id: '5e011bc25b891e33b3915966',
      eventId: '5ceee4b8bb15a161671bbe7e',
    },
  ],
  cams: [
    {
      id: '58235706302ec90c4108c3e4',
      title: 'UK - Perranporth Overview',
      alias: 'uk-perranporthov',
      stillUrl: 'https://camstills.cdn-surfline.com/uk-perranporthov/latest_small.jpg',
      isPremium: false,
      visible: true,
    },
  ],
};

const SESSION_EVENT_FIXTURE = {
  _id: '5ceee4b8bb15a161671bbe7e',
  startTimestamp: 1553554939870,
  endTimestamp: 1553554939870,
  distance: 1,
  speedMax: 1,
};

const GET_CLIP_FIXTURE = [
  {
    clipUrl: `https://camclips.sandbox.surfline.com/58235706302ec90c4108c3e4/${CLIP_ID}.mp4`,
    cameraId: '58235706302ec90c4108c3e4',
    startTimestamp: 1553554909870,
    endTimestamp: 1553554981786,
    thumbnail: {
      url: `https://camclips.sandbox.surfline.com/58235706302ec90c4108c3e4/${CLIP_ID}_{size}.jpg`,
      sizes: [100, 300, 600],
    },
    status: 'CLIP_AVAILABLE',
    clipId: CLIP_ID,
  },
];

const POST_SESSIONS_FIXTURE = {
  startTimestamp: 1584552221000,
  endTimestamp: 1584552242000,
  visibility: 'PUBLIC',
  waveCount: 1,
  speedMax: 1.101,
  distancePaddled: 0,
  deviceType: 'vivoactive3',
  distanceLongestWave: 49.55,
  distanceWaves: 49.55,
  events: [
    {
      startTimestamp: 1584552221000,
      endTimestamp: 1584552242000,
      type: 'WAVE',
      distance: 49.55,
      speedAverage: 1,
      speedMax: 1.101,
      positions: [
        {
          timestamp: 1584552221000,
          latitude: 33.6542122811079,
          longitude: -118.00564521923661,
        },
        {
          timestamp: 1584552242000,
          latitude: 33.6542122811079,
          longitude: -118.00564521923661,
        },
      ],
    },
  ],
  user: '5aabfbc0ffd66500117e554d',
  clientId: '5af1ce73b5acf7c6dd2592ee',
};

describe('patchSessionDetailsHandler', () => {
  beforeEach(async () => {
    sinon.stub(SessionModel, 'patchSessionDetails');
    sinon.stub(Session, 'getEnrichedSession');
    sinon.stub(Session, 'updateSessionNotification');
    sinon.stub(queueSessionForEnrichment, 'default');
  });

  afterEach(() => {
    SessionModel.patchSessionDetails.restore();
    Session.getEnrichedSession.restore();
    Session.updateSessionNotification.restore();
    queueSessionForEnrichment.default.restore();
  });

  it('updates whitelisted fields only', async () => {
    const req = {
      body: {
        name: 'A new name',
        rating: 4,
        mysterious_paramter: 'should be ignored!',
      },
      authenticatedUserId: 'mongoId1',
      params: { sessionId: 'mongoId2' },
      session: {
        response: 'this is the session',
        spot: { id: '1' },
      },
    };
    Session.getEnrichedSession.resolves({
      response: 'this is the session',
      spot: { id: '1' },
    });
    SessionModel.patchSessionDetails.resolves({ response: 'this is the session' });
    queueSessionForEnrichment.default.resolves({ response: 'this is the enriched session' });

    await patchSessionDetailsHandler(req, { send: () => null });
    expect(SessionModel.patchSessionDetails).to.have.been.calledOnce();
    expect(SessionModel.patchSessionDetails).to.have.been.calledWithExactly(
      'mongoId1',
      'mongoId2',
      {
        name: 'A new name',
        rating: 4,
      },
      undefined,
    );
  });

  it('updates notified flag for a Session when user changes the spot', async () => {
    const req = {
      body: {
        name: 'A new name',
        rating: 4,
        mysterious_paramter: 'should be ignored!',
        userSpotId: '1',
      },
      authenticatedUserId: 'mongoId1',
      params: { sessionId: 'mongoId2' },
      session: {
        response: 'this is the session',
        spot: { id: '2' },
      },
    };
    Session.getEnrichedSession.resolves({
      response: 'this is the session',
      spot: { id: '2' },
    });
    SessionModel.patchSessionDetails.resolves({ response: 'this is the session' });
    queueSessionForEnrichment.default.resolves({ response: 'this is the enriched session' });

    await patchSessionDetailsHandler(req, { send: () => null });
    expect(SessionModel.patchSessionDetails).to.have.been.calledOnce();
    expect(SessionModel.patchSessionDetails).to.have.been.calledWithExactly(
      'mongoId1',
      'mongoId2',
      {
        name: 'A new name',
        rating: 4,
        userSpotId: '1',
        notified: false,
      },
      true,
    );
  });
});

describe('patchSessionWaveHandler', () => {
  beforeEach(async () => {
    sinon.stub(SessionModel, 'patchSessionWaveDetails');
    sinon.stub(Session, 'getEnrichedSession');
    sinon.stub(queueSessionForEnrichment, 'default');
  });

  afterEach(() => {
    SessionModel.patchSessionWaveDetails.restore();
    Session.getEnrichedSession.restore();
    queueSessionForEnrichment.default.restore();
  });

  it('updates whitelisted fields only', async () => {
    const waveId = mongoose.Types.ObjectId();
    const sessionId = mongoose.Types.ObjectId();
    const userId = mongoose.Types.ObjectId();
    const req = {
      body: {
        outcome: 'ATTEMPTED',
        favorite: true,
        mysterious_paramter: 'should be ignored!',
      },
      authenticatedUserId: userId,
      params: { sessionId, waveId },
      session: {
        response: 'this is the session',
        spot: { id: '1' },
      },
    };
    Session.getEnrichedSession.resolves({
      events: [{ _id: waveId, outcome: 'CAUGHT', favorite: false }],
    });
    SessionModel.patchSessionWaveDetails.resolves({ response: 'this is the session' });
    queueSessionForEnrichment.default.resolves({ response: 'this is the enriched session' });

    await patchSessionWaveHandler(req, { send: () => null });
    expect(SessionModel.patchSessionWaveDetails).to.have.been.calledOnce();
    expect(SessionModel.patchSessionWaveDetails).to.have.been.calledWithExactly(
      req.authenticatedUserId,
      req.session,
      waveId,
      {
        favorite: true,
        outcome: 'ATTEMPTED',
      },
    );
  });
});

describe('getSessionClipHandler', () => {
  beforeEach(async () => {
    sinon.stub(userApi, 'getUserName');
    sinon.stub(userApi, 'getUserSettings');
    sinon.stub(clips, 'getClips');
    sinon.stub(SessionModel, 'getSessionByClipId');
    sinon.stub(aws, 'getSessionEvents');
    sinon.stub(convertUnits, 'convertSpeed');
    sinon.stub(convertUnits, 'convertLength');
  });

  afterEach(() => {
    userApi.getUserName.restore();
    userApi.getUserSettings.restore();
    aws.getSessionEvents.restore();
    SessionModel.getSessionByClipId.restore();
    clips.getClips.restore();
    convertUnits.convertSpeed.restore();
    convertUnits.convertLength.restore();
  });

  it('fetches a session clip and returns it with unit conversions for the requesting user', async () => {
    const sendStub = sinon.stub();
    const req = {
      params: { clipId: CLIP_ID },
      geoCountryIso: 'US',
      authenticatedUserId: 'mongoId1',
    };

    const { spot, spotReportView } = GET_SESSION_CLIP_FIXTURE;
    const { speedMax, distance } = SESSION_EVENT_FIXTURE;
    const { clipUrl, thumbnail } = GET_CLIP_FIXTURE[0];
    const { units } = USER_SETTINGS_FIXTURE;

    userApi.getUserName.resolves(USER_NAME_FIXTURE);
    userApi.getUserSettings.resolves(USER_SETTINGS_FIXTURE);
    SessionModel.getSessionByClipId.resolves(GET_SESSION_CLIP_FIXTURE);
    aws.getSessionEvents.resolves([SESSION_EVENT_FIXTURE]);
    clips.getClips.resolves(GET_CLIP_FIXTURE);
    convertUnits.convertSpeed.returns(2);
    convertUnits.convertLength.returns(2);

    await getSessionClipHandler(req, { send: sendStub });

    expect(userApi.getUserName).to.have.been.calledOnce();
    expect(userApi.getUserName).to.have.been.calledWithExactly(GET_SESSION_CLIP_FIXTURE.user);

    expect(userApi.getUserSettings).to.have.been.calledOnce();
    expect(userApi.getUserSettings).to.have.been.calledWithExactly('mongoId1', 'US');

    expect(clips.getClips).to.have.been.calledOnce();
    expect(clips.getClips).to.have.been.calledWithExactly([CLIP_ID]);

    expect(convertUnits.convertSpeed).to.have.been.calledOnce();
    expect(convertUnits.convertSpeed).to.have.been.calledWithExactly(speedMax, units.speed);

    expect(convertUnits.convertLength).to.have.been.calledOnce();
    expect(convertUnits.convertLength).to.have.been.calledWithExactly(distance, units.distance);

    expect(sendStub).to.have.been.calledOnce();
    expect(sendStub).to.have.been.calledWithExactly({
      associated: {
        units,
        timezoneAbbr: 'GMT',
        utcOffset: 0,
      },
      spot,
      spotReportView,
      camera: GET_SESSION_CLIP_FIXTURE.cams[0],
      user: USER_NAME_FIXTURE,
      wave: {
        clip: {
          clipUrl,
          thumbnail,
        },
        startTimestamp: 1553554939870,
        endTimestamp: 1553554939870,
        distance: 2,
        speedMax: 2,
      },
    });
  });
});

describe('postGpsHandler', () => {
  /** @type {sinon.SinonStub} */
  let enrichedSessionFindOneStub;
  let objectIdStub;
  const gpsCenter = {
    latitude: 50.317337558825514,
    longitude: -4.084309524596326,
  };

  const res = {};

  beforeEach(async () => {
    sinon.stub(analyzer, 'default');
    sinon.stub(Session, 'insertSession').resolves({
      _id: '123',
    });
    sinon.stub(queueSessionForEnrichment, 'default');
    sinon.stub(aws, 'uploadFileToS3');
    sinon.spy(SessionModel, 'addSession');
    objectIdStub = sinon.stub(mongoose.Types, 'ObjectId');
    enrichedSessionFindOneStub = sinon.stub(Session.EnrichedSession, 'findOne');
    res.status = sinon.fake.returns(res);
    res.send = sinon.fake();
  });

  afterEach(() => {
    analyzer.default.restore();
    Session.insertSession.restore();
    queueSessionForEnrichment.default.restore();
    aws.uploadFileToS3.restore();
    SessionModel.addSession.restore();
    enrichedSessionFindOneStub.restore();
    objectIdStub.restore();
  });

  it('should successfully analyze a session on the gps endpoint', async () => {
    const req = {
      body: gpsSessionData,
      get: sinon.stub(),
      authenticatedUserId: mongoose.Types.ObjectId(),
      authenticatedClientId: mongoose.Types.ObjectId(),
      headers: {
        'User-Agent':
          'Tracking Extension/7.0.0 (com.surfline.surfapp.watchkitapp.watchkitextension; watchOS 7.2.0)',
      },
    };

    // Resolve stubs
    analyzer.default.resolves({ body: analyzedSession });
    objectIdStub.returns('123');
    enrichedSessionFindOneStub.returns({
      select: () => ({
        lean: () => null,
      }),
    });
    aws.uploadFileToS3.resolves({
      ETag: '234rds23456765432wdf654e',
      ServerSideEncryption: 'AES256',
      VersionId: '4RDSWEDFGT43EDDWR..FEFS2345',
    });
    queueSessionForEnrichment.default.resolves({ response: 'this is the enriched session' });

    await postGpsHandler(req, res);
    const millisecondStartTimestamp = new Date(analyzedSession.startTimestamp * 1000);
    const { startTimestamp } = await SessionModel.addSession.returnValues[0];
    expect(analyzer.default).to.have.been.calledWithExactly(gpsSessionData, gpsCenter);
    expect(startTimestamp.toString()).to.be.equal(millisecondStartTimestamp.toString());
    expect(res.send).to.have.been.called();
    expect(res.send).to.have.been.calledWithExactly({
      success: true,
      message: 'Session added successfully.',
      id: '123',
    });
  });

  it('should return the _id of an existing duplicate if a duplicate session exists, and throw a 200 status', async () => {
    const user = mongoose.Types.ObjectId();
    const req = {
      body: gpsSessionData,
      get: sinon.stub(),
      authenticatedUserId: user,
      authenticatedClientId: mongoose.Types.ObjectId(),
    };

    // Resolve stubs
    analyzer.default.resolves({ body: analyzedSession });
    enrichedSessionFindOneStub.returns({
      select: () => ({
        lean: () => ({
          _id: 'some_duplicate_id',
        }),
      }),
    });
    aws.uploadFileToS3.resolves({
      ETag: '234rds23456765432wdf654e',
      ServerSideEncryption: 'AES256',
      VersionId: '4RDSWEDFGT43EDDWR..FEFS2345',
    });
    queueSessionForEnrichment.default.resolves({ response: 'this is the enriched session' });

    await postGpsHandler(req, res);
    const startTimestamp = new Date(analyzedSession.startTimestamp * 1000);
    const endTimestamp = new Date(analyzedSession.endTimestamp * 1000);
    const { waveCount } = analyzedSession;

    expect(analyzer.default).to.have.been.calledOnceWithExactly(gpsSessionData, gpsCenter);
    expect(enrichedSessionFindOneStub).to.have.been.calledOnceWithExactly({
      startTimestamp,
      endTimestamp,
      user,
      waveCount,
      gpsCenter: {
        type: 'Point',
        coordinates: [gpsCenter.longitude, gpsCenter.latitude],
      },
    });
    expect(res.send).to.have.been.called();
    expect(res.status).to.have.been.calledWithExactly(200);
    expect(res.send).to.have.been.calledWithExactly({
      success: false,
      message: 'Duplicate session detected.',
      id: 'some_duplicate_id',
    });
  });

  it('should throw if validation fails on an analyzed session', async () => {
    const sendStub = sinon.stub();
    const req = {
      body: gpsSessionData,
      get: sinon.stub(),
      authenticatedUserId: mongoose.Types.ObjectId(),
      authenticatedClientId: mongoose.Types.ObjectId(),
    };

    // Resolve stubs
    analyzedSession.waveCount = 'SOME STRING THAT IS NOT VALID AS PROPERTY';
    analyzer.default.resolves({ body: analyzedSession });
    queueSessionForEnrichment.default.resolves({ response: 'this is the enriched session' });

    let error;
    try {
      await postGpsHandler(req, { send: sendStub });
    } catch (err) {
      error = err;
    }
    expect(error).to.exist();
    expect(error.name).to.be.equal('ValidationError');
  });
});

describe('deleteSessionHandler', () => {
  const res = {
    send: () => {},
    status: () => res,
  };

  beforeEach(async () => {
    sinon.stub(SessionModel, 'getSession');
    sinon.stub(SessionModel, 'deleteSession');
    sinon.spy(res, 'status');
    sinon.spy(res, 'send');
  });

  afterEach(() => {
    res.status.restore();
    res.send.restore();
    SessionModel.getSession.restore();
    SessionModel.deleteSession.restore();
  });

  it('throws 404 when supplied an invalid session ID', async () => {
    const sessionId = '';
    const userId = '';
    const req = {
      authenticatedUserId: userId,
      params: { sessionId },
    };
    SessionModel.deleteSession.resolves(null);

    await deleteSessionHandler(req, res);
    expect(res.status).to.have.been.calledWithExactly(404);
  });

  it('should call getSession and deleteSession', async () => {
    const sessionId = '';
    const userId = '';
    const req = {
      authenticatedUserId: userId,
      params: { sessionId },
    };

    SessionModel.deleteSession.resolves();

    await deleteSessionHandler(req, res);
    expect(SessionModel.deleteSession).to.have.been.calledOnce();
    expect(SessionModel.deleteSession).to.have.been.calledWithExactly(userId, sessionId);
  });
});

describe('postSessionHandler', () => {
  afterEach(() => {
    sinon.restore();
  });
  it('should return an error if there are no events with positions', async () => {
    const noPositionsBody = {
      ...POST_SESSIONS_FIXTURE,
      events: undefined,
    };
    try {
      await postSessionHandler({ body: noPositionsBody }, {});
    } catch (error) {
      expect(error.message).to.equal(
        'Invalid Parameters: Valid events are required to add a session',
      );
    }

    noPositionsBody.events = [
      {
        ...POST_SESSIONS_FIXTURE.events[0],
        positions: [],
      },
      {
        ...POST_SESSIONS_FIXTURE.events[0],
        positions: undefined,
      },
    ];
    try {
      await postSessionHandler({ body: noPositionsBody }, {});
    } catch (error) {
      expect(error.message).to.equal(
        'Invalid Parameters: Valid events are required to add a session',
      );
    }
  });

  it('should not return an error if there are events with positions', async () => {
    const positionsBody = {
      ...POST_SESSIONS_FIXTURE,
      events: [
        {
          ...POST_SESSIONS_FIXTURE.events[0],
          positions: [],
        },
        {
          ...POST_SESSIONS_FIXTURE.events[0],
          positions: undefined,
        },
        POST_SESSIONS_FIXTURE.events[0],
      ],
    };
    sinon.stub(existsDuplicateSession, 'default');

    existsDuplicateSession.default.resolves({ _id: '123' });

    const sendSpy = sinon.spy();

    await postSessionHandler(
      { body: positionsBody },
      { status: sinon.stub().returns({ send: sendSpy }) },
    );

    expect(sendSpy).to.have.been.calledWithExactly({
      success: false,
      id: '123',
      message: 'Duplicate session detected.',
    });
  });
});
