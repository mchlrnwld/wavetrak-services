import moment from 'moment-timezone';
import newrelic from 'newrelic';
import tzlookup from 'tz-lookup';
import { isNumber } from 'lodash';
import { APIError } from '@surfline/services-common';
import {
  getSessions,
  addSession,
  deleteSession,
  updateSessionDetails,
  getSessionFeed,
  getSessionsSummary,
  updateSessionCamVisibility,
  patchSessionDetails,
  patchSessionWaveDetails,
  getSessionUserByUserId,
  getSessionByClipId,
} from '../../models';
import queueSessionForEnrichment from './enrich';
import { compileSessionDetails, compileStats } from '../../helpers/compileSessionDetails';
import { getClips } from '../../external/clips';
import {
  timezoneData,
  getTimezoneArray,
  getFeedTimezoneArray,
  getTimezoneRefByOffset,
  toUnixTimestamp,
  utcOffsetFromTimezone,
} from '../../utils/datetime';
import { getSpot } from '../../external/spots';
import { getUserSettings, getUserName } from '../../external/user';
import { convertSpeed, convertLength } from '../../utils/convertUnits';
import analyzeSessionGps from '../../external/analyzer';
import convertSessionTimestamps from '../../helpers/convertSessionTimestamps';
import validatePatchWaveFields from '../../helpers/validatePatchWaveFields';
import postToSlack from '../../external/slack';
import config from '../../config';
import { getSessionEvents, uploadSessionGpsData } from '../../external/aws';
import hydrateEvents, { hydrateEventOverrides } from '../../helpers/hydrateEvents';
import {
  calculateSessionGPSCenter,
  convertEventsToPositions,
} from '../../utils/calculateSessionGPSCenter';
import buildSession from '../../helpers/buildSession';
import validateSession from '../../helpers/validateSession';
import getWatchDetails from '../../utils/getWatchDetails';
import existsDuplicateSession from '../../helpers/duplicateSession';

export const getSessionHandler = async (req, res) => {
  const sessions = await getSessions(req.authenticatedUserId);
  return res.send(sessions);
};

/**
 * Endpoint where 3rd Party App data gets injested having been pre-processed in the garmin parser lambda
 * @param {object} req express request object
 * @param {object} res express response object
 */
export const postSessionHandler = async (req, res) => {
  const userId = req.authenticatedUserId;

  newrelic.recordCustomEvent('Sessions', {
    event: 'POST Session',
    userId,
    clientId: req.authenticatedClientId,
    startTimestamp: req.body.startTimestamp,
    endTimestamp: req.body.endTimestamp,
    waveCount: req.body.waveCount,
    deviceType: req.body.deviceType,
  });

  req.body.events = req.body.events?.filter(event => !!event.positions?.length);
  if (!req.body.events?.length) {
    newrelic.recordCustomEvent('Session Missing Positions', {
      userId,
      clientId: req.authenticatedClientId,
    });
    throw new APIError('Valid events are required to add a session');
  }

  const validatedSession = buildSession(validateSession(req.body));

  const sessionPositions = newrelic.startSegment('convertEventsToPositions', true, () =>
    convertEventsToPositions(validatedSession.events),
  );
  const gpsCenter = newrelic.startSegment('calculateSessionGeoCenter', true, () =>
    calculateSessionGPSCenter(sessionPositions),
  );

  const duplicateSession = await existsDuplicateSession(gpsCenter, validatedSession, userId);

  // If there is a duplicate session we are sending a 200 for the time being.
  if (duplicateSession) {
    return res
      .status(200)
      .send({ success: false, id: duplicateSession._id, message: 'Duplicate session detected.' });
  }

  const session = await addSession(
    userId,
    req.authenticatedClientId,
    validatedSession,
    gpsCenter,
    getWatchDetails(req),
  );

  await queueSessionForEnrichment(session);

  return res.send({ success: true, id: session._id, message: 'Session added successfully.' });
};

/**
 * @deprecated - Temporarily disabling due to unintended consequences (losing clips)
 * discussing how to revamp endpoint
 */
export const putSessionHandler = async (_, res) =>
  res.send({ success: true, message: 'Session updated successfully.' });

export const deleteSessionHandler = async (req, res) => {
  const deletedSession = await deleteSession(req.authenticatedUserId, req.params.sessionId);

  if (!deletedSession) {
    return res.status(404).send({
      success: false,
      message: 'Session not found for user',
    });
  }

  return res.send({
    success: true,
    message: 'Session deleted successfully.',
    id: req.params.sessionId,
  });
};

export const getSessionsSummaryHandler = async (req, res) => {
  const { start, end } = req.query;
  const sessionsSummary = await getSessionsSummary(req.authenticatedUserId, start, end);

  if (!sessionsSummary) {
    return res.status(404).send({
      success: false,
      message: 'Session not found for user',
    });
  }

  const sessionCount = sessionsSummary.length;
  const allEvents = sessionsSummary.reduce((accum, { events }) => [...accum, ...events], []);
  const waves = allEvents.filter(
    event => event.type === 'WAVE' && event.outcome?.toUpperCase() !== 'ATTEMPTED',
  );
  const timeSpentSurfing = waves.reduce((totalTime, wave) => {
    const newTotalTime =
      totalTime + (new Date(wave.endTimestamp) - new Date(wave.startTimestamp)) / 1000;
    return newTotalTime;
  }, 0);

  const waveCount = waves.length;

  return res.send({
    sessionCount,
    waveCount,
    timeSpentSurfing,
  });
};

export const getSessionDetailsHandler = async (req, res) => {
  const { session } = req;

  const user = await getUserName(session.user);
  const userSettings = await getUserSettings(req.authenticatedUserId, req.geoCountryIso);
  const userUnits = userSettings.units;

  const clipIds = session.clips?.map(clip => clip._id) || [];
  const clips = clipIds.length ? await getClips(clipIds) : [];

  const units = {
    ...userUnits,
    distance: userUnits.surfHeight,
    speed: userUnits.surfHeight === 'M' ? 'KPH' : 'MPH',
  };

  // Sessions Pending enrichment will not have a spot
  if (session.spot?.id) {
    const { relivableRating } = await getSpot(session.spot.id);
    session.spot.relivableRating = relivableRating;
  }

  const stats = await compileStats(session);
  const sessionDetailsObject = compileSessionDetails({
    session,
    stats,
    clips,
    units,
    showAttempted: true,
  });

  const { startTimestamp, endTimestamp } = sessionDetailsObject;
  const sessionCoords = session.events[0].positions[0].location.coordinates;
  const lat = sessionCoords[1];
  const lon = sessionCoords[0];
  const associatedTimezone = getTimezoneArray(lat, lon, startTimestamp, endTimestamp);
  const timezone = getTimezoneRefByOffset(
    associatedTimezone,
    timezoneData(lat, lon, startTimestamp).offset,
  );

  return res.send({
    ...sessionDetailsObject,
    timezone,
    associated: {
      units,
      timezone: associatedTimezone,
    },
    ...(user && { user }),
  });
};

export const getSessionFeedHandler = async (req, res) => {
  const limit = req.query.limit ? parseInt(req.query.limit, 10) : 20;
  const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0;

  const sessionFeed = await getSessionFeed(req.authenticatedUserId, limit, offset);

  if (!sessionFeed) {
    return res.status(404).send({
      success: false,
      message: 'Session feed not found for user',
    });
  }

  const userSettings = await getUserSettings(req.authenticatedUserId, req.geoCountryIso);
  const userUnits = userSettings.units;

  const units = {
    ...userUnits,
    distance: userUnits.surfHeight,
    speed: userUnits.surfHeight === 'M' ? 'KPH' : 'MPH',
  };

  const associatedTimezone = sessionFeed.length > 0 ? getFeedTimezoneArray(sessionFeed) : null;

  const associated = {
    units,
    timezone: associatedTimezone,
  };

  const sessionFeedArray = await Promise.all(
    sessionFeed.map(async session => {
      const stats = await compileStats(session);

      const clipIds = session.clips?.map(clip => clip._id) || [];
      const clips = clipIds.length ? await getClips(clipIds) : [];

      let relivableRating = false;
      // Pending Enrichment sessions will not have a spot
      if (session.spot?.id) {
        const { rating } = await getSpot(session.spot.id);
        relivableRating = rating;
      }

      const sessionDetailsObject = compileSessionDetails({
        session: {
          ...session,
          spot: {
            ...session.spot,
            // Only add relivableRating is it is a number
            ...(isNumber(relivableRating) && { relivableRating }),
          },
        },
        stats,
        clips,
        units,
      });

      const { startTimestamp } = sessionDetailsObject;
      const sessionCoords = session.events[0].positions[0].location.coordinates;
      const lat = sessionCoords[1];
      const lon = sessionCoords[0];
      const timezone = getTimezoneRefByOffset(
        associatedTimezone,
        timezoneData(lat, lon, startTimestamp).offset,
      );

      return {
        ...sessionDetailsObject,
        timezone,
      };
    }),
  );

  return res.send({
    associated,
    sessions: sessionFeedArray,
  });
};

export const putSessionDetailsHandler = async (req, res) => {
  newrelic.recordCustomEvent('Sessions', {
    event: 'PUT Session',
    userId: req.authenticatedUserId,
    clientId: req.authenticatedClientId,
    startTimestamp: req.body.startTimestamp,
    endTimestamp: req.body.endTimestamp,
    waveCount: req.body.waveCount,
    deviceType: req.body.deviceType,
  });

  // Note: Not adding overrides and clips through this route, should use patch instead
  const updatedSession = await updateSessionDetails(
    req.authenticatedUserId,
    req.params.sessionId,
    req.body,
  );
  const sessionUser = await getSessionUserByUserId(req.authenticatedUserId);
  if (sessionUser?.groups) {
    const { firstName, lastName } = await getUserName(req.authenticatedUserId);
    const sessionGroupsToChannel = {
      SL_TEAM: ['#sessions_tool_influencers'],
      UAG: ['#sessions_tool_influencers', '#sessions_tool_uag'],
      SESSION_INFLUENCER: ['#sessions_tool_influencers'],
      RIPCURL: ['#sessions_tool_influencers', '#sessions_tool_rip_curl'],
    };
    sessionUser.groups.forEach(group => {
      sessionGroupsToChannel[group].forEach(async channel => {
        const text = `A session has been logged for ${firstName} ${lastName}.
        Session: ${config.LINK_URL}/sessions/${req.params.sessionId}`;
        const payload = {
          text,
          channel,
          username: 'sessionbot',
          icon_emoji: ':surfline:',
        };
        await postToSlack(payload);
      });
    });
  } else {
    const { firstName, lastName } = await getUserName(req.authenticatedUserId);
    const text = `A session has been logged for ${firstName} ${lastName}: ${config.LINK_URL}/sessions/${req.params.sessionId}`;
    const payload = {
      text,
      channel: '#sessions_tool',
      username: 'sessionbot',
      icon_emoji: ':surfline:',
    };
    await postToSlack(payload);
  }

  if (!updatedSession) {
    return res.status(404).send({
      success: false,
      message: 'Session not found for user',
    });
  }

  return res.send({ success: true, message: 'Session details updated successfully.' });
};

export const patchSessionDetailsHandler = async (req, res) => {
  const { session } = req;
  const { deleteClips, reEnrich } = req.body;

  newrelic.recordCustomEvent('Sessions', {
    event: 'PATCH Session',
    userId: req.authenticatedUserId,
    clientId: req.authenticatedClientId,
    startTimestamp: req.body.startTimestamp,
    endTimestamp: req.body.endTimestamp,
    waveCount: req.body.waveCount,
    deviceType: req.body.deviceType,
  });

  newrelic.addCustomAttributes({ ...req.body });

  const WHITE_LIST = ['name', 'rating', 'userSpotId', 'status'];

  const changedFields = WHITE_LIST.reduce((fields, key) => {
    if (req.body[key]) {
      /* eslint-disable-next-line no-param-reassign */
      fields[key] = req.body[key];
    }
    return fields;
  }, {});

  const currentSpotId = session.userSpotId || session.spot?.id || '';
  const userSpotIdChanged =
    changedFields.userSpotId && changedFields.userSpotId !== currentSpotId.toString();

  if (userSpotIdChanged) {
    changedFields.notified = false;
  }

  const needsReEnrichment = userSpotIdChanged || reEnrich;
  let updatedSession;
  try {
    updatedSession = await patchSessionDetails(
      req.authenticatedUserId,
      req.params.sessionId,
      changedFields,
      needsReEnrichment,
    );
  } catch (err) {
    newrelic.noticeError(err);
    throw new Error('Something went wrong updating the session');
  }

  // In order to not exceeded the max payload size for SQS, remove the SRV
  delete updatedSession.spotReportView;

  if (needsReEnrichment) {
    await queueSessionForEnrichment(updatedSession, reEnrich, deleteClips);
  }

  return res.send({ success: true, message: 'Session details patched successfully.' });
};

export const patchSessionWaveHandler = async (req, res) => {
  const {
    session,
    params: { waveId },
  } = req;

  const changedFields = validatePatchWaveFields(req.body);

  await patchSessionWaveDetails(req.authenticatedUserId, session, waveId, changedFields);

  return res.send({ success: true, message: 'Session wave details patched successfully.' });
};

export const putSessionCamVisibility = async (req, res) => {
  const { visible } = req.body;
  const { sessionId, cameraId } = req.params;
  const userId = req.authenticatedUserId;

  try {
    await updateSessionCamVisibility(userId, sessionId, cameraId, visible);
    return res.send({ success: true, message: 'Camera visibility successfully updated' });
  } catch (err) {
    throw new Error('Something went wrong updating the camera visibility');
  }
};

export const getSessionClipHandler = async (req, res) => {
  const { clipId } = req.params;

  const session = await getSessionByClipId(clipId);

  if (!session) {
    return res.status(404).send({
      success: false,
      message: 'Clip not found',
    });
  }

  const clipEvent = session.clips.find(clip => clip._id.toString() === clipId);
  const rawEvents = await getSessionEvents(session._id);

  const sessionEvents = hydrateEvents(
    rawEvents,
    session.clips,
    hydrateEventOverrides(session.eventOverrides),
  );
  const event = sessionEvents.find(evt => evt._id === clipEvent.eventId.toString());

  const sessionUser = session.user;
  const { spot, spotReportView, cams } = session;
  const { startTimestamp, endTimestamp, distance, speedMax } = event;

  const [clip] = await getClips([clipId]);
  const { cameraId } = clip;
  const user = await getUserName(sessionUser);

  const userSettings = await getUserSettings(req.authenticatedUserId, req.geoCountryIso);
  const userUnits = userSettings.units;

  const camera = cams.find(cam => cam.id.toString() === cameraId);

  const units = {
    ...userUnits,
    distance: userUnits.surfHeight,
    speed: userUnits.surfHeight === 'M' ? 'KPH' : 'MPH',
  };

  const [lon, lat] = spot.location.coordinates;
  const timezone = spotReportView?.timezone || tzlookup(lat, lon);

  const utcOffset = utcOffsetFromTimezone(timezone);
  const timezoneAbbr = moment.tz(timezone).zoneAbbr();

  const associated = {
    units,
    utcOffset,
    timezoneAbbr,
  };

  return res.send({
    associated,
    spot,
    spotReportView,
    camera,
    user,
    wave: {
      startTimestamp: toUnixTimestamp(startTimestamp),
      endTimestamp: toUnixTimestamp(endTimestamp),
      distance: convertLength(distance, units.distance),
      speedMax: convertSpeed(speedMax, units.speed),
      clip: {
        clipUrl: clip?.clipUrl,
        thumbnail: clip?.thumbnail,
      },
    },
  });
};

/**
 * Endpoint that the Surfline App posts too with sessions recorded from an apple watch
 * @param {object} req express request object
 * @param {object} res express response object
 */
export const postGpsHandler = async (req, res) => {
  const gpsData = req.body;
  const userId = req.authenticatedUserId;
  const clientId = req.authenticatedClientId;
  const inputBodySize = req.get('content-length') || 0;

  newrelic.recordCustomEvent('Sessions', {
    event: 'POST gps Session',
    userId: req.authenticatedUserId,
    clientId: req.authenticatedClientId,
    startTimestamp: req.body.startTimestamp,
    endTimestamp: req.body.endTimestamp,
    waveCount: req.body.waveCount,
    deviceType: req.body.deviceType,
  });

  const gpsCenter = calculateSessionGPSCenter(req.body.positions);
  const { body: analyzedSession, bodySize: responseBodySize } = await analyzeSessionGps(
    gpsData,
    gpsCenter,
  );

  const hasSizes = inputBodySize > 0 && responseBodySize > 0;
  if (hasSizes && responseBodySize / inputBodySize > 5) {
    newrelic.addCustomAttributes({ inputBodySize, responseBodySize });
    newrelic.noticeError('Sessions GPS body size inflated over 5x');
  }

  if (analyzedSession) {
    const positions = analyzedSession.events?.[0]?.positions;
    if (!positions || !positions.length) {
      throw new APIError('Valid events are required to add a session');
    }

    const validatedSession = buildSession(
      validateSession(convertSessionTimestamps(analyzedSession)),
    );

    const duplicateSession = await existsDuplicateSession(gpsCenter, validatedSession, userId);

    // If there is a duplicate session we are sending back a 200 for now with the existing session's
    // _id in order to prevent the apple watch from going into an endless syncing loop
    if (duplicateSession) {
      return res
        .status(200)
        .send({ success: false, id: duplicateSession._id, message: 'Duplicate session detected.' });
    }

    const session = await addSession(
      userId,
      clientId,
      validatedSession,
      gpsCenter,
      getWatchDetails(req),
    );
    await queueSessionForEnrichment(session);
    await uploadSessionGpsData(session._id, gpsData);

    return res.send({ success: true, id: session._id, message: 'Session added successfully.' });
  }

  return res.status(500).send('Something went wrong analyzing the session');
};
