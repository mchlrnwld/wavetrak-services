import { setupExpress } from '@surfline/services-common';
import trackRequests from '../common/trackRequests';
import sessions from './sessions';
import config from '../config';
import admin from './admin';
import surfparks from './surfparkSessions';

/**
 * @description this should handle CORS and/or preflight requests.
 * See https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request for further reading.
 * @param {req, res, next}
 */
const accessControlMiddleware = (req, res, next) => {
  let allowedOrigin = req.header('Origin') || '*';
  // eslint-disable-next-line no-param-reassign
  res.locals.scopes = req.header('X-Auth-AccessScope')?.split(',') || null;
  if (allowedOrigin === 'null' || allowedOrigin === '*') {
    const referer = req.header('referer');
    if (referer) {
      const refererParts = referer.split('/');
      allowedOrigin = `${refererParts[0]}//${refererParts[2]}`;
    }
  }
  res.header('Access-Control-Allow-Origin', allowedOrigin);
  res.header('Access-Control-Request-Headers', 'Origin');
  res.header(
    'Access-Control-Allow-Headers',
    // eslint-disable-next-line max-len
    'Origin, X-Requested-With, Content-Type, Accept, Credentials, X-Auth-ClientId, X-Auth-AnonymousId',
  );
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT, DELETE, PATCH');
  next();
};

const startApp = log =>
  setupExpress({
    port: config.EXPRESS_PORT || 8081,
    name: 'sessions-api',
    log,
    handlers: [
      accessControlMiddleware,
      ['*', trackRequests(log)],
      ['/sessions', sessions(log)],
      ['/admin', admin(log)],
      ['/surfparks/sessions', accessControlMiddleware, surfparks(log)],
    ],
    allowedMethods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    bodySizeLimit: '10mb',
  });

export default startApp;
