const clip = (clipId, thumbailUrl, mainClipUrl, previewClipUrl, startTimestamp = 1488878277) => ({
  clipUrl: mainClipUrl,
  startTimestamp,
  preview: {
    thumbnails: [
      {
        url: thumbailUrl,
        sizes: ['small'],
      },
    ],
    clip: {
      clipUrl: previewClipUrl,
    },
  },
  clipId,
});

const createWave = (id, clips, startTimestamp = 1488878277) => ({
  id,
  startTimestamp,
  clips,
});

const createBaseSession = id => ({
  id,
  timezone: {
    id: 1,
    path: 'associated.timezone',
  },
  surfpark: {
    name: 'Lower Trestles',
    websiteUrl: '',
    logo: {
      url:
        'https://sl-cam-thumbnails-high-freq-sandbox/583499c4e411dc743a5d5296/2021/02/08/583499c4e411dc743a5d5296.20210208T212408.jpg',
    },
    location: {
      type: 'POINT',
      coordinates: [33, -110],
    },
  },
  spot: {
    id: '584204204e65abc5a775520b',
    name: 'Lower Trestles',
    location: {
      type: 'POINT',
      coordinates: [33, -110],
    },
  },
  stats: {
    waveCount: 0,
  },
  clips: {
    total: 0,
    available: 0,
  },
  waves: [],
});

const createSession = (
  id,
  waves = [],
  sessionPreviewClip = 'https://sl-live-cam-archive-staging.s3.us-west-1.amazonaws.com/live/uk-gwithian.stream.20210818T125651445.mp4',
  sessionPreviewThumb = 'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg',
) => ({
  ...createBaseSession(id),
  startTimestamp: 1488878277,
  endTimestamp: 1488878277,
  clips: {
    total: waves.length,
    available: waves.length,
  },
  stats: {
    waveCount: waves.length,
  },
  preview: {
    thumbnails: [
      {
        url: sessionPreviewThumb,
        sizes: ['small'],
      },
    ],
    clip: {
      clipUrl: sessionPreviewClip,
    },
  },
  waves,
});

const mainClipUrl =
  'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4';
const previewClipUrl =
  'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426.mp4';
const thumbailUrl =
  'https://surf-park-sessions-clips.staging.surfline.com/park-id/2022/02/14/band-id/5bbba560b52a3b7b61500426_{size}.jpg';

export const dummyData = {
  associated: {
    timezone: [
      {
        key: 'America/Los_Angeles',
        abbr: 'PST',
        offset: -7,
        id: 1,
        path: 'associated.timezone',
      },
    ],
  },
  sessions: [
    // Session prior to waves
    createBaseSession('620b77dabeeaa4188d2a8fea'),
    // Sesssion after band mapping with a a single wave
    createSession('620b77df561e77ebb71c1706', [
      createWave('620b77e42ed827b294d0a8ad', [
        clip('620b796ccd690c24fccfffb2', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
    ]),
    // Sesssion after band mapping with a a multiple waves
    createSession('284204254e65abc5a775520b', [
      createWave('620b77e981f5164b9ecc02d0', [
        clip('620b77ef1393277f01b6995e', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b77f6af9f314392d246ad', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78002d1e254b7ee11d06', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b7807929b201c351deed3', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b7814ef313367fcddff5d', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b781ba141a6f8b27b7da5', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('620b7823c14ce572a5a496e0', [
        clip('620b782c30602302e8c2620d', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78352d50dd33dc2d7121', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78421705c860c039d283', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78496d3910acd897927b', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78540825d52a5f5a8c72', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b789079c21e8b605a7fdf', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('620b78963da07d5e2ec97c58', [
        clip('620b789d9119df25bb348170', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78a2982c4ab2e36b9467', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78a6dd049dc591df464e', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78aaab8acab70c569463', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('234056789e65abc5a775509c', [
        clip('620b78af71e826174fbec9ef', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78b3bacddd19ce16bfc0', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78b7d45bfe85617fdbc1', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78c25fca8acf91b637e4', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78c943a721c7c939dcd8', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78cdf3d9f731ff8a026c', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('234056789e65abc5a775566y', [
        clip('620b78d1bdeba2b91fe8e9d0', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78d52827586aa116ea45', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78dc499041562a763d35', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78e361c0b4ec682fda1f', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78e79a934b0ba1ee774f', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78eb9ff3199ac2355c08', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('620b790d2c1522946e932c43', [
        clip('620b78f033ca5b414bd9b69e', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78f4cded0fe57442662f', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78f70daeddaf0b9bfae0', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b78fd63117331c4e78ffa', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b7904528e3bf6dd3ccfc7', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b7908906f0aeb713e33b7', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('620b79135b11889b1662f3c3', [
        clip('620b791a2d42a0cf3b1cbc84', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b791ea59c4eb80c86a7b0', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b792314ed2c43201c2e8a', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b79267b6ad35d1852df0a', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b792afdc52bb9961aa42c', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('234056789e65abc5a987984n', [
        clip('620b792de311a98aa68864b2', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b793121c7672e86722685', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b793530d691ea1e03c89b', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b793985b2a0b9be7425ab', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b793c029364981419aa21', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b793fc5ea950bcccfe55c', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('620b79430d06b64bab2c34d3', [
        clip('620b794a1da6713c9e9a02a3', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b79562158683e87bdb7d1', thumbailUrl, mainClipUrl, previewClipUrl),
        clip('620b795ac1990fbcbebfeb61', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
      createWave('620b795f9cf55c0058f8bc8e', [
        clip('620b7962ea2014fabc9427d6', thumbailUrl, mainClipUrl, previewClipUrl),
      ]),
    ]),
  ],
};
