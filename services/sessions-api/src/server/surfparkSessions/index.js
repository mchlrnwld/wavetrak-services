import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { surfParkSessions } from './handlers';

const surfparks = () => {
  const api = Router();
  const { getSurfParkSessionsFeed, postSurfParkSessionBand } = surfParkSessions;
  api.get('/feed', wrapErrors(getSurfParkSessionsFeed));
  api.post('/band', wrapErrors(postSurfParkSessionBand));

  return api;
};

export default surfparks;
