import { APIError } from '@surfline/services-common';
import { dummyData } from '../sampleSurfparkSessionsData/sampleResponse';
import SurfparkSessionBandModel, {
  createSurfparkSessionBand,
} from '../../../models/surfparks/SurfparkSessionBands';

export const getSurfParkSessionsFeed = async ({ headers }, res) => {
  if (!headers || !headers['x-auth-userid']) {
    return res.status(400).send({
      message: 'Cannot get surfpark session user without header info.',
    });
  }

  return res.send({
    ...dummyData,
  });
};

export const postSurfParkSessionBand = async (req, res) => {
  const surflineUserId = req.headers['x-auth-userid'];
  const { surfparkId, totalPaidSessions, rfid } = req.body;
  if (!rfid || !surfparkId) {
    throw new APIError('Cannot create a band object without both a band RFID and surfparkID', 400);
  }
  const createBandObject = {
    rfid,
    surfparkId,
  };
  if (totalPaidSessions) createBandObject.totalPaidSessions = totalPaidSessions;
  if (surflineUserId) createBandObject.userId = surflineUserId;
  let insertedBand;
  try {
    insertedBand = await createSurfparkSessionBand(createBandObject);
    res.json(insertedBand);
    res.status(200);
  } catch (error) {
    // check data for duplication
    const nowDate = new Date();
    const todysDate = `${nowDate.getFullYear()}/${nowDate.getMonth() + 1}/${nowDate.getDate()}`;
    if (error.code === 11000) {
      const existingBand = await SurfparkSessionBandModel.findOne({
        activeDate: todysDate,
        rfid,
      });
      res.status(202);
      res.json(existingBand);
    } else {
      res.status(500);
      res.json({ message: 'Something went wrong' });
    }
  }
  return res;
};
