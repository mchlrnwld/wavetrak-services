import chai, { expect } from 'chai';
import express from 'express';
import sinon from 'sinon';
import mongoose from 'mongoose';
import admin from './index';
import * as aws from '../../external/aws';
import Session, { EnrichedSession } from '../../models/Session';

describe('/admin', () => {
  let request;

  before(async () => {
    const app = express();
    app.use(
      admin({ error: sinon.stub(), info: sinon.stub(), log: sinon.stub(), trace: sinon.stub() }),
    );
    request = chai.request(app).keepOpen();
  });

  after(() => {
    request.close();
  });

  beforeEach(() => {
    sinon.stub(aws, 'getS3EventsUrl');
    sinon.stub(aws, 'headS3Events');
    sinon.stub(aws, 'deleteSessionEvents');
    sinon.stub(Session, 'deleteOne');
    sinon.stub(EnrichedSession, 'find');
  });

  afterEach(() => {
    aws.getS3EventsUrl.restore();
    aws.headS3Events.restore();
    aws.deleteSessionEvents.restore();
    Session.deleteOne.restore();
    EnrichedSession.find.restore();
  });

  it('GET /admin - Should retrieve a users sessions', async () => {
    const userId = '5733b498de23de02271f1f70';
    const sessionsFixture = [
      {
        _id: '123',
        user: userId,
      },
    ];
    const selectStub = sinon.stub().returns({ lean: () => sessionsFixture });
    EnrichedSession.find.returns({ select: selectStub });
    aws.headS3Events.resolves({ ContentLength: 1048576 });
    aws.getS3EventsUrl.returns('url');

    const res = await request.get('/').set('x-auth-userId', userId);

    expect(res).to.have.status(200);
    expect(res.body).to.be.ok();
    expect(res.body).to.deep.equal([{ _id: '123', user: userId, eventsSize: 1, url: 'url' }]);
    expect(EnrichedSession.find).to.have.been.calledOnceWithExactly({ user: userId });
    expect(selectStub).to.have.been.calledOnce();
    expect(aws.getS3EventsUrl).to.have.been.calledOnceWithExactly('123');
    expect(aws.headS3Events).to.have.been.calledOnceWithExactly('123');
  });

  it('DELETE /admin - should delete a session and its events', async () => {
    const userId = '5733b498de23de02271f1f70';
    const sessionId = '5733b498de23de02271f1f71';

    aws.deleteSessionEvents.resolves();
    Session.deleteOne.resolves();

    const res = await request
      .delete('/')
      .set('x-auth-userId', userId)
      .query({ id: sessionId });

    expect(res).to.have.status(200);
    expect(res.body).to.be.ok();
    expect(aws.deleteSessionEvents).to.have.been.calledOnceWithExactly(sessionId);
    expect(Session.deleteOne).to.have.been.calledOnceWithExactly({
      user: mongoose.Types.ObjectId(userId),
      _id: mongoose.Types.ObjectId(sessionId),
    });
  });
});
