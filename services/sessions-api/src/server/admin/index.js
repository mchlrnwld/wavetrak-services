import { Router } from 'express';
import { authenticateRequest, wrapErrors } from '@surfline/services-common';
import trackRequests from '../../common/trackRequests';
import { adminGetSessionsByUser, deleteUserSession } from './admin';

const admin = log => {
  const api = Router();

  api.use('*', trackRequests(log));
  api.use('*', authenticateRequest({ userIdRequired: true }));

  api.get('/', wrapErrors(adminGetSessionsByUser));
  api.delete('/', wrapErrors(deleteUserSession));

  return api;
};

export default admin;
