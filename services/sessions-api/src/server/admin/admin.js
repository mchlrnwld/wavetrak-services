import { getUserSessions, removeSession } from '../../models/Session';
import { getS3EventsUrl, headS3Events, deleteSessionEvents } from '../../external/aws';

const bytesToMegaBytes = bytes => bytes / (1024 * 1024);

/**
 * Admin endpoint to retrieve a user's sessions.
 *
 * @function adminGetSessionsByUser
 * @param {import('express').Request} req
 * @param {import("express").Response} res
 * @return {Promise<void>}
 */
export const adminGetSessionsByUser = async (req, res) => {
  const { authenticatedUserId: userId } = req;

  const sessions = await getUserSessions(userId);

  const sessionsWithEventsInfo = await Promise.all(
    sessions.map(async session => {
      const sessionId = session._id;

      const url = getS3EventsUrl(sessionId);
      const sessionEvents = await headS3Events(sessionId);
      const eventsSize = bytesToMegaBytes(sessionEvents.ContentLength);
      return {
        ...session,
        eventsSize,
        url,
      };
    }),
  );

  res.send(sessionsWithEventsInfo);
};

/**
 * Admin endpoint to retrieve a user's sessions.
 *
 * @function adminGetSessionsByUser
 * @param {import('express').Request} req
 * @param {import("express").Response} res
 * @return {Promise<void>}
 */
export const deleteUserSession = async (req, res) => {
  const { authenticatedUserId: userId } = req;
  const sessionId = req.query.id;

  await deleteSessionEvents(sessionId);
  await removeSession(sessionId, userId);

  res.sendStatus(200);
};
