import { expect } from 'chai';
import mongoose from 'mongoose';
import sinon from 'sinon';
import { generateClipHandler, garminActivityFileHandler } from './handlers';
import * as sessionsModel from '../../models/Session';
import * as clips from '../../external/clips';
import * as awsExternal from '../../external/aws';
import * as aws from '../../common/aws';
import * as redis from '../../external/redis';
import * as getCaughtWaveCount from '../../helpers/getCaughtWaveCount';
import * as sessionEvents from '../../helpers/sessionEvents';

describe('/webhooks', () => {
  describe('generateClipHandler', () => {
    const { SESSION_UPDATED_TOPIC_ARN } = process.env;

    const clipId = '5cbdf59b6d2730634eafc451';
    const clip1 = mongoose.Types.ObjectId();
    const clip2 = mongoose.Types.ObjectId();
    const clip3 = mongoose.Types.ObjectId();

    const sendStub = sinon.stub();

    const req = {
      get: () => 'Notification',
      body: {
        Message: `{"clipId":"${clipId}"}`,
      },
    };
    const res = { send: sendStub };

    beforeEach(() => {
      sinon.stub(getCaughtWaveCount, 'default');
      sinon.stub(sessionEvents, 'hydrateSession');
      sinon.stub(sessionsModel, 'getSessionByClipId');
      sinon.stub(sessionsModel, 'updateSessionNotification');
      sinon.stub(clips, 'getClips');
      sinon.stub(aws, 'publishMessage');
    });

    afterEach(() => {
      sendStub.reset();
      getCaughtWaveCount.default.restore();
      sessionEvents.hydrateSession.restore();
      sessionsModel.getSessionByClipId.restore();
      sessionsModel.updateSessionNotification.restore();
      clips.getClips.restore();
      aws.publishMessage.restore();
    });

    it('should not publish a message to SNS if clips are still pending', async () => {
      const user = mongoose.Types.ObjectId();
      const sessionId = mongoose.Types.ObjectId();

      sessionsModel.getSessionByClipId.resolves({
        user,
        clips: [
          {
            _id: clipId,
          },
          {
            _id: clip1,
          },
          {
            _id: clip2,
          },
          {
            _id: clip3,
          },
        ],
        _id: sessionId,
      });

      clips.getClips.resolves([
        {
          status: 'CLIP_AVAILABLE',
          clipId,
        },
        {
          status: 'CLIP_AVAILABLE',
          clipId: clip1,
        },
        {
          status: 'CLIP_AVAILABLE',
          clipId: clip2,
        },
        {
          status: 'CLIP_PENDING',
          clipId: clip3,
        },
      ]);

      await generateClipHandler(req, res);

      const clipsArray = [clipId, clip1.toString(), clip2.toString(), clip3.toString()];

      expect(clips.getClips).to.have.been.calledOnce();
      expect(clips.getClips).to.have.been.calledWithExactly(clipsArray);

      expect(sessionsModel.getSessionByClipId).to.have.been.calledOnce();
      expect(sessionsModel.getSessionByClipId).to.have.been.calledWithExactly(clipId);

      expect(sendStub).to.have.been.calledOnce();
      expect(sendStub).to.have.been.calledWithExactly({
        notify: false,
        message: 'Session still has pending clips.',
      });

      expect(aws.publishMessage).to.not.have.been.called();
    });

    it('publishes a message to SNS if all clips are completed', async () => {
      const user = mongoose.Types.ObjectId();
      const sessionId = mongoose.Types.ObjectId();
      const spot = {
        name: 'HB Southside',
      };
      const waveCount = 4;

      sessionsModel.getSessionByClipId.resolves({
        user,
        _id: sessionId,
        spot,
        clips: [
          {
            _id: clipId,
          },
          {
            _id: clip1,
          },
          {
            _id: clip2,
          },
          {
            _id: clip3,
          },
        ],
        waveCount,
      });

      clips.getClips.resolves([
        {
          status: 'CLIP_AVAILABLE',
          clipId,
        },
        {
          status: 'CLIP_OUT_OF_RANGE',
          clipId: clip1,
        },
        {
          status: 'CLIP_AVAILABLE',
          clipId: clip2,
        },
        {
          status: 'CLIP_NOT_AVAILABLE',
          clipId: clip3,
        },
      ]);

      sessionEvents.hydrateSession.resolves({ events: [] });
      getCaughtWaveCount.default.resolves(1);
      sessionsModel.updateSessionNotification.resolves({ _id: sessionId, notified: true });

      aws.publishMessage.resolves();

      await generateClipHandler(req, res);

      const clipsArray = [clipId, clip1.toString(), clip2.toString(), clip3.toString()];

      expect(clips.getClips).to.have.been.calledOnce();
      expect(clips.getClips).to.have.been.calledWithExactly(clipsArray);

      expect(sessionsModel.getSessionByClipId).to.have.been.calledOnce();
      expect(sessionsModel.getSessionByClipId).to.have.been.calledWithExactly(clipId);

      expect(sessionsModel.updateSessionNotification).to.have.been.calledOnceWithExactly(
        sessionId,
        true,
      );

      expect(sendStub).to.have.been.calledOnce();
      expect(sendStub).to.have.been.calledWithExactly({
        notify: true,
        message: 'Session clips have finished generating.',
      });

      expect(aws.publishMessage).to.have.been.calledOnce();
      expect(aws.publishMessage).to.have.been.calledWithExactly(SESSION_UPDATED_TOPIC_ARN, {
        sessionId,
        user,
        waveCount: 1,
        spot,
      });
    });

    it('should not publish a message to SNS if the session is already notified ', async () => {
      const user = mongoose.Types.ObjectId();
      const sessionId = mongoose.Types.ObjectId();
      const spot = {
        name: 'HB Southside',
      };
      const waveCount = 4;

      sessionsModel.getSessionByClipId.resolves({
        user,
        _id: sessionId,
        notified: true,
        spot,
        clips: [
          {
            _id: clipId,
          },
          {
            _id: clip1,
          },
          {
            _id: clip2,
          },
          {
            _id: clip3,
          },
        ],
        waveCount,
      });

      clips.getClips.resolves([
        {
          status: 'CLIP_AVAILABLE',
          clipId,
        },
        {
          status: 'CLIP_OUT_OF_RANGE',
          clipId: clip1,
        },
        {
          status: 'CLIP_AVAILABLE',
          clipId: clip2,
        },
        {
          status: 'CLIP_NOT_AVAILABLE',
          clipId: clip3,
        },
      ]);

      sessionEvents.hydrateSession.resolves({ events: [] });
      getCaughtWaveCount.default.resolves(1);

      await generateClipHandler(req, res);

      const clipsArray = [clipId, clip1.toString(), clip2.toString(), clip3.toString()];

      expect(clips.getClips).to.have.been.calledOnce();
      expect(clips.getClips).to.have.been.calledWithExactly(clipsArray);

      expect(sessionsModel.getSessionByClipId).to.have.been.calledOnce();
      expect(sessionsModel.getSessionByClipId).to.have.been.calledWithExactly(clipId);

      expect(sessionsModel.updateSessionNotification).to.have.not.been.called();
      expect(aws.publishMessage).to.have.not.been.called();

      expect(sendStub).to.have.been.calledOnce();
      expect(sendStub).to.have.been.calledWithExactly({
        notify: true,
        message: 'Session clips have finished generating.',
      });
    });
  });

  describe('garminActivityFileHandler', () => {
    /** @type {sinon.SinonStub} */
    let invokeLambdaAsyncStub;
    /** @type {sinon.SinonStub} */
    let getFitFileTTLStub;
    /** @type {sinon.SinonStub} */
    let setFitFileKeyStub;

    beforeEach(() => {
      getFitFileTTLStub = sinon.stub(redis, 'getFitFileTTL');
      setFitFileKeyStub = sinon.stub(redis, 'setFitFileKey');
      invokeLambdaAsyncStub = sinon.stub(awsExternal, 'invokeLambdaAsync');
    });

    afterEach(() => {
      getFitFileTTLStub.restore();
      setFitFileKeyStub.restore();
      invokeLambdaAsyncStub.restore();
    });

    it('should return a 200 if no activityFiles are passed', async () => {
      const req = { body: {} };
      const sendStub = sinon.stub();
      const res = { sendStatus: sendStub };

      await garminActivityFileHandler(req, res);

      expect(sendStub).to.have.been.calledOnceWithExactly(200);
      expect(invokeLambdaAsyncStub).to.not.have.been.called();
      expect(getFitFileTTLStub).to.not.have.been.called();
      expect(setFitFileKeyStub).to.not.have.been.called();
    });

    it('should properly process activityFiles', async () => {
      getFitFileTTLStub.resolves(-2);
      const req = {
        body: {
          activityFiles: [
            { fileType: 'FIT', manual: false, userAccessToken: '1', callbackURL: '1' },
            { fileType: 'GPX', manual: false, userAccessToken: '2', callbackURL: '2' },
            { fileType: 'FIT', manual: true, userAccessToken: '3', callbackURL: '3' },
          ],
        },
      };

      const sendStub = sinon.stub();
      const res = { sendStatus: sendStub };

      await garminActivityFileHandler(req, res);

      expect(sendStub).to.have.been.calledOnceWithExactly(200);
      expect(invokeLambdaAsyncStub).to.have.been.calledOnceWithExactly('fit-file-lambda', {
        userAccessToken: '1',
        callbackURL: '1',
      });
      expect(setFitFileKeyStub).to.have.been.calledOnceWithExactly('1', '1');
    });

    it('should skip activity files that have already been synced', async () => {
      getFitFileTTLStub.onFirstCall().resolves(1000);
      getFitFileTTLStub.onSecondCall().resolves(-2);
      const req = {
        body: {
          activityFiles: [
            { fileType: 'FIT', manual: false, userAccessToken: '1', callbackURL: '1' },
            { fileType: 'FIT', manual: false, userAccessToken: '2', callbackURL: '2' },
          ],
        },
      };

      const sendStub = sinon.stub();
      const res = { sendStatus: sendStub };

      await garminActivityFileHandler(req, res);

      expect(sendStub).to.have.been.calledOnceWithExactly(200);
      expect(invokeLambdaAsyncStub).to.have.been.calledOnceWithExactly('fit-file-lambda', {
        userAccessToken: '2',
        callbackURL: '2',
      });
      expect(setFitFileKeyStub).to.have.been.calledOnceWithExactly('2', '2');
    });

    it('should gracefully fail setting a redis key to prevent garmin re-pings', async () => {
      getFitFileTTLStub.resolves(-2);
      setFitFileKeyStub.throws();
      const req = {
        body: {
          activityFiles: [
            { fileType: 'FIT', manual: false, userAccessToken: '1', callbackURL: '1' },
            { fileType: 'FIT', manual: false, userAccessToken: '2', callbackURL: '2' },
          ],
        },
      };

      const sendStub = sinon.stub();
      const res = { sendStatus: sendStub };

      await garminActivityFileHandler(req, res);

      expect(sendStub).to.have.been.calledOnceWithExactly(200);

      expect(invokeLambdaAsyncStub).to.have.been.calledTwice();
      expect(invokeLambdaAsyncStub.getCall(0)).to.have.been.calledWithExactly('fit-file-lambda', {
        userAccessToken: '1',
        callbackURL: '1',
      });
      expect(invokeLambdaAsyncStub.getCall(1)).to.have.been.calledWithExactly('fit-file-lambda', {
        userAccessToken: '2',
        callbackURL: '2',
      });

      expect(setFitFileKeyStub).to.have.been.calledTwice();
      expect(setFitFileKeyStub.getCall(0)).to.have.been.calledWithExactly('1', '1');
      expect(setFitFileKeyStub.getCall(1)).to.have.been.calledWithExactly('2', '2');
    });

    it('should handle an error invoking the lambda', async () => {
      const req = {
        body: {
          activityFiles: [
            { fileType: 'FIT', manual: false, userAccessToken: '1', callbackURL: '1' },
          ],
        },
      };

      const sendStub = sinon.stub();
      const res = { sendStatus: sendStub };

      invokeLambdaAsyncStub.throws();
      let error;
      try {
        await garminActivityFileHandler(req, res);
      } catch (err) {
        error = err;
      }
      expect(invokeLambdaAsyncStub).to.have.been.calledOnceWithExactly('fit-file-lambda', {
        userAccessToken: '1',
        callbackURL: '1',
      });
      expect(error.message).to.equal('Something went wrong processing the webhook');
    });
  });
});
