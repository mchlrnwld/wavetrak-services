import newrelic from 'newrelic';
import { ServerError } from '@surfline/services-common';
import { confirmSubscription, publishMessage } from '../../common/aws';
import { getSessionByClipId, updateSessionNotification } from '../../models/Session';
import { getClips } from '../../external/clips';
import config from '../../config';
import getCaughtWaveCount from '../../helpers/getCaughtWaveCount';
import { hydrateSession } from '../../helpers/sessionEvents';
import { invokeLambdaAsync } from '../../external/aws';
import { getFitFileTTL, setFitFileKey } from '../../external/redis';

const { SESSION_UPDATED_TOPIC_ARN } = config;
/**
 * @description Handler function for a `POST` message from the AWS SNS
 * subscription on the `sl-cameras-service-generate-clip-sns_{env}` sns
 * topic. This topic receives a message when a clip is completed (success/failure).
 *
 * There are two different payloads to handle, the first being a `SubscriptionConfirmation`
 * and the second being a `Notification`.
 *
 * When we receive a `SubscriptionConfirmation` we want to validate and confirm it.
 *
 * When we receive a `Notification` we want to process it, validate it, and then perform
 * actions to send a notification if all of a users clips have been generated.
 * @param {Object} req
 * @param {Object} req.body
 * @param {Object} res
 * @param {Function} res.send
 */
export const generateClipHandler = async (req, res) => {
  const messageType = req.get('x-amz-sns-message-type');

  if (messageType === 'SubscriptionConfirmation') {
    const { Token: token } = req.body;
    const topicArn = req.get('x-amz-sns-topic-arn');
    await confirmSubscription(token, topicArn);

    return res.send({
      message: 'Subscription confirmed successfully',
    });
  }

  if (messageType === 'Notification') {
    const message = JSON.parse(req.body.Message);
    const { clipId } = message;

    // Add to newrelic to help with debugging
    newrelic.addCustomAttribute('clipId', clipId);

    const session = await getSessionByClipId(clipId);

    if (!session) {
      // Clip no longer tied to a session, return early.
      // User may have changed their spot or deleted the session
      return res.send({
        notify: false,
        message: 'No session found for clip.',
      });
    }

    const clips = session.clips.map(clip => clip._id.toString());

    const clipStatuses = await getClips(clips);

    const pendingClips = clipStatuses.filter(clip => clip.status === 'CLIP_PENDING');
    const availableClips = clipStatuses.filter(clip => clip.status === 'CLIP_AVAILABLE');

    if (pendingClips?.length) {
      return res.send({
        notify: false,
        message: 'Session still has pending clips.',
      });
    }

    if (!availableClips?.length) {
      return res.send({
        notify: false,
        message: 'Session has no available clips',
      });
    }

    const { _id: sessionId, user, waveCount, spot, notified = false } = session;
    let notificationWaveCount = waveCount;
    // TODO: Remove this after discussing with Dawn Patrol on their wave count
    try {
      // We want to try to make the wave count reflect the caught waves, but if it fails
      // it's okay to fall back to the `waveCount` property
      const { events: hydratedEvents } = await hydrateSession({ session });
      notificationWaveCount = await getCaughtWaveCount(hydratedEvents);
    } catch (error) {
      newrelic.noticeError(error);
    }
    try {
      if (!notified) {
        await publishMessage(SESSION_UPDATED_TOPIC_ARN, {
          sessionId,
          user,
          waveCount: notificationWaveCount,
          spot,
        });
        await updateSessionNotification(sessionId, true);
      }
      return res.send({
        notify: true,
        message: 'Session clips have finished generating.',
      });
    } catch (error) {
      newrelic.noticeError(error);
      throw error;
    }
  }

  return res.status(400).send({
    message: 'Invalid Message Type',
  });
};

/**
 * @typedef {{fileType: string, manual: boolean, userAccessToken: string, callbackURL: string}} ActivityFile
 * @param {import('express').Request<any, void, {activityFiles: ActivityFile[]}>} req
 * @param {import('express').Response<void>} res
 */
export const garminActivityFileHandler = async (req, res) => {
  const { activityFiles } = req.body;

  if (!activityFiles) return res.sendStatus(200);

  try {
    const payload = activityFiles
      .filter(activityFile => activityFile.fileType === 'FIT' && !activityFile.manual)
      .map(({ userAccessToken, callbackURL }) => ({ userAccessToken, callbackURL }));

    // We disable the rule here since we want to await each step to ensure we
    // can set a redis key after each successful lambda invocation, so that we
    // don't end up re-processing that specific fit file.
    /* eslint-disable no-await-in-loop */
    for (let i = 0; i < payload.length; i += 1) {
      const item = payload[i];
      const { userAccessToken, callbackURL } = item;
      const fitFileTTL = await getFitFileTTL(userAccessToken, callbackURL);
      const hasFitFileBeenProcessed = fitFileTTL > 0;

      // Ensure the fit file has not already been processed so that we don't get
      // duplicate sessions.
      if (!hasFitFileBeenProcessed) {
        await invokeLambdaAsync(config.FIT_FILE_PARSER_LAMBDA, item);
        try {
          await setFitFileKey(userAccessToken, callbackURL);
        } catch (_) {
          // We want to let this error into the void so that it doesn't cause the endpoint
          // to throw and cause garmin to re-send the ping without us
        }
      }
    }
    /* eslint-enable no-await-in-loop */

    return res.sendStatus(200);
  } catch (err) {
    console.log(err);
    newrelic.noticeError(err);
    throw new ServerError({ message: 'Something went wrong processing the webhook' });
  }
};
