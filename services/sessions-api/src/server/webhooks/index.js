import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import trackRequests from '../../common/trackRequests';
import { generateClipHandler, garminActivityFileHandler } from './handlers';

const webhooks = log => {
  const api = Router();

  api.use('*', json({ type: ['application/json', 'text/plain'] }), trackRequests(log));

  api.post('/generateClip', wrapErrors(generateClipHandler));
  api.post('/garmin/activity-files', wrapErrors(garminActivityFileHandler));
  return api;
};

export default webhooks;
