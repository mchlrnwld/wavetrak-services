// import testconfig from './testconfig';

// const defaultUrl = port => `http://localhost:${port}`;

export default {
  REDIS_IP: process.env.REDIS_IP,
  REDIS_PORT: process.env.REDIS_PORT,
  MONGO_CONNECTION_STRING_SESSIONS: process.env.MONGO_CONNECTION_STRING_SESSIONS,
  EXPRESS_PORT: process.env.EXPRESS_PORT || 8081,
  LOGSENE_KEY: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63',
  CONSOLE_LOG_LEVEL: process.env.CONSOLE_LOG_LEVEL || 'debug',
  LOGSENE_LEVEL: process.env.LOGSENE_LEVEL || 'debug',
  SPOTS_API: process.env.SPOTS_API || 'https://services.sandbox.surfline.com',
  CAMERAS_API: process.env.CAMERAS_API || 'https://services.sandbox.surfline.com',
  USER_SERVICE: process.env.USER_SERVICE,
  AUTH_SERVICE: process.env.AUTH_SERVICE,
  SESSION_ANALYZER: process.env.SESSION_ANALYZER,
  SESSION_ENRICHER_FUNCTION:
    process.env.SESSION_ENRICHER_FUNCTION || 'sl-sessions-api-enrich-session-sandbox',
  SESSION_ENRICHER_QUEUE: process.env.SESSION_ENRICHER_QUEUE,
  SESSION_UPDATED_TOPIC_ARN: process.env.SESSION_UPDATED_TOPIC_ARN,
  mapTileUrl: 'https://api.maptiler.com/maps/062c0d04-1842-4a45-8181-c5bec3bf2214',
  mapTileKey: '3tFgnOQBQixe61aigsBT',
  SNS_API_VERSION: '2010-03-31',
  SLACK_HOOK_URL: `https://hooks.slack.com/services/${process.env.SLACK_HOOK_KEY}`,
  LINK_URL: process.env.LINK_URL,
  FIT_FILE_PARSER_LAMBDA: process.env.FIT_FILE_PARSER_LAMBDA,
  APP_VERSION: process.env.APP_VERSION || 'unknown',
};
