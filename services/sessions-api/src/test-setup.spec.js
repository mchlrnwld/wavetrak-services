import chai from 'chai';
import sinon from 'sinon';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import * as servicesCommon from '@surfline/services-common';
import mongoose from 'mongoose';

require('dotenv').config({ silent: true, path: '.env.test' });

chai.use(chaiHttp);
chai.use(dirtyChai);
chai.use(sinonChai);

sinon.stub(servicesCommon, 'setupLogsene');
sinon.stub(servicesCommon, 'createLogger').returns({
  trace: () => null,
  debug: () => null,
  info: () => null,
  error: () => null,
  warn: () => null,
  setMaxListeners: () => null,
});

chai.config.truncateThreshold = 0;

mongoose.Promise = global.Promise;
