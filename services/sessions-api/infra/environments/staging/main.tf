provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/staging/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/staging/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "sessions-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "sessions_api" {
  source = "../../"

  company     = "sl"
  application = "sessions-api"
  environment = "staging"

  ecs_cluster      = "sl-core-svc-staging"
  default_vpc      = "vpc-981887fd"
  dns_name         = "sessions-api.staging.surfline.com"
  dns_zone_id      = "Z3JHKQ8ELQG5UE"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "sessions-api.staging.surfline.com"
    },
  ]

  alb_listener_arn           = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  garmin_parser_arn          = "arn:aws:lambda:us-west-1:665294954271:function:wt-sessions-api-garmin-parser-staging"
  generate_clip_topic_arn    = "arn:aws:sns:us-west-1:665294954271:sl-cameras-service-generate-clip-sns_staging"
  iam_role_arn               = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn          = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"
  session_enricher_arn       = "arn:aws:lambda:us-west-1:665294954271:function:sl-sessions-api-enrich-session-staging"
  sessions_events_bucket_arn = "arn:aws:s3:::sl-sessions-api-sessions-events-staging"

  sessions_api_sns_http_endpoint = "http://services.staging.surfline.com/sessions/webhooks/generateClip"

  auto_scaling_enabled = false
}
