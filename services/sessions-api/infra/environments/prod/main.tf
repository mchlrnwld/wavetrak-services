provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "sessions-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "sessions_api" {
  source = "../../"

  company     = "sl"
  application = "sessions-api"
  environment = "prod"

  ecs_cluster      = "sl-core-svc-prod"
  default_vpc      = "vpc-116fdb74"
  dns_name         = "sessions-api.prod.surfline.com"
  dns_zone_id      = "Z3LLOZIY0ZZQDE"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "sessions-api.prod.surfline.com"
    },
  ]

  alb_listener_arn           = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  garmin_parser_arn          = "arn:aws:lambda:us-west-1:833713747344:function:wt-sessions-api-garmin-parser-prod"
  generate_clip_topic_arn    = "arn:aws:sns:us-west-1:833713747344:sl-cameras-service-generate-clip-sns_prod"
  iam_role_arn               = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn          = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"
  session_enricher_arn       = "arn:aws:lambda:us-west-1:833713747344:function:sl-sessions-api-enrich-session-prod"
  sessions_events_bucket_arn = "arn:aws:s3:::sl-sessions-api-sessions-events-prod"

  sessions_api_sns_http_endpoint = "http://services.surfline.com/sessions/webhooks/generateClip"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 100
  auto_scaling_min_size     = 40
  auto_scaling_max_size     = 5000
}
