provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/sandbox/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/sandbox/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "sessions-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "sessions_api" {
  source = "../../"

  company     = "sl"
  application = "sessions-api"
  environment = "sandbox"

  ecs_cluster      = "sl-core-svc-sandbox"
  default_vpc      = "vpc-981887fd"
  dns_name         = "sessions-api.sandbox.surfline.com"
  dns_zone_id      = "Z3DM6R3JR1RYXV"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "sessions-api.sandbox.surfline.com"
    },
  ]

  alb_listener_arn           = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  garmin_parser_arn          = "arn:aws:lambda:us-west-1:665294954271:function:wt-sessions-api-garmin-parser-sandbox"
  generate_clip_topic_arn    = "arn:aws:sns:us-west-1:665294954271:sl-cameras-service-generate-clip-sns_sandbox"
  iam_role_arn               = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn          = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"
  session_enricher_arn       = "arn:aws:lambda:us-west-1:665294954271:function:sl-sessions-api-enrich-session-sandbox"
  sessions_events_bucket_arn = "arn:aws:s3:::sl-sessions-api-sessions-events-sandbox"

  sessions_api_sns_http_endpoint = "http://services.sandbox.surfline.com/sessions/webhooks/generateClip"

  auto_scaling_enabled = false
}
