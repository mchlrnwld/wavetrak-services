module "sessions_api" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-${var.application}"
  service_td_count            = var.service_td_count
  service_td_container_name   = var.application
  service_port                = 8080
  service_alb_priority        = 150

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id
  default_vpc = var.default_vpc
  ecs_cluster = var.ecs_cluster

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_up_metric      = var.auto_scaling_up_metric
  auto_scaling_down_metric    = var.auto_scaling_down_metric
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
  auto_scaling_up_count       = var.auto_scaling_up_count
  auto_scaling_down_count     = var.auto_scaling_down_count
}

# Create Session Updated SNS topic
module "sns_topic" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"

  environment = var.environment
  topic_name  = "${var.company}-${var.application}-sessions-updated-sns"
}

data "aws_sns_topic" "generate_clip_topic" {
  name = "sl-cameras-service-generate-clip-sns_${var.environment}"
}

# Permit Sessions API to publish to SNS topic
resource "aws_iam_role_policy_attachment" "sns_topic_publish_policy" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = module.sns_topic.topic_policy
}

# Permit Sessions API access to sessions events S3 bucket
resource "aws_iam_policy" "sessions_events_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-sessions-events-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/session-events-policy.json", {
    resource_arn = var.sessions_events_bucket_arn
  })
}

resource "aws_iam_role_policy_attachment" "sessions_events_bucket_mgmt_policy_attachement" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sessions_events_bucket_mgmt_policy.arn
}

resource "aws_s3_bucket" "sessions_full_gps_bucket" {
  bucket = "${var.company}-${var.application}-sessions-gps-tracks-${var.environment}"
  acl    = "private"
}
resource "aws_iam_policy" "sessions_gps_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-sessions-gps-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/session-events-policy.json", {
    resource_arn = aws_s3_bucket.sessions_full_gps_bucket.arn
  })
}

resource "aws_iam_role_policy_attachment" "sessions_gps_bucket_mgmt_policy_attachement" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sessions_gps_bucket_mgmt_policy.arn
}

resource "aws_ssm_parameter" "sessions_full_gps_bucket_name" {
  name  = "/${var.environment}/common/SESSIONS_GPS_BUCKET"
  type  = "String"
  description = "Bucket containing full gps tracks for sessions"
  value = aws_s3_bucket.sessions_full_gps_bucket.bucket
}

resource "aws_iam_policy" "sessions-queue-mgmt-policy" {
  name        = "${var.company}-${var.application}-sessions-queue-mgmt-policy-${var.environment}"
  description = "policy to send messages to sessions queue"
  policy = templatefile("${path.module}/resources/sessions-queue-policy.json", {
    resource_arn = "arn:aws:sqs:*:*:${var.company}-${var.application}-sessions-enricher-queue-${var.environment}"
  })
}

resource "aws_iam_role_policy_attachment" "sessions_queue_mgmt_policy_attachement" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sessions-queue-mgmt-policy.arn
}

resource "aws_sns_topic_subscription" "generate_clip_sns_subscription" {
  topic_arn              = data.aws_sns_topic.generate_clip_topic.arn
  protocol               = "http"
  endpoint               = var.sessions_api_sns_http_endpoint
  endpoint_auto_confirms = true
  delivery_policy        = file("${path.module}/resources/sns-subscription-policy.json")
}

resource "aws_iam_policy" "sessions_api_session_enricher_policy" {
  name        = "${var.company}-${var.application}-sessions-api-session-enricher-policy-${var.environment}"
  description = "policy to invoke session enricher from sessions api"
  policy = templatefile("${path.module}/resources/sessions-api-enricher-policy.json", {
    resource_arn = var.session_enricher_arn
  })
}

resource "aws_iam_role_policy_attachment" "sessions_api_session_enricher_attachment" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sessions_api_session_enricher_policy.arn
}

resource "aws_iam_policy" "sns_subscription_management_policy" {
  name        = "sns_policy_subscription_management_sessions_api_${var.environment}"
  description = "policy to manage subscriptions to topics for the sessions api"
  policy = templatefile("${path.module}/resources/sessions-api-topics-policy.json", {
    resource_arn = var.generate_clip_topic_arn
  })
}

resource "aws_iam_role_policy_attachment" "notifications_service_sns_read_write_attachment" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sns_subscription_management_policy.arn
}

resource "aws_iam_policy" "sessions_api_garmin_parser_policy" {
  name        = "${var.company}-${var.application}-sessions-api-garmin-parser-policy-${var.environment}"
  description = "policy to invoke garmin parser from sessions api"
  policy = templatefile("${path.module}/resources/sessions-api-garmin-parser-policy.json", {
    resource_arn = var.garmin_parser_arn
  })
}

resource "aws_iam_role_policy_attachment" "sessions_api_garmin_parser_attachment" {
  role       = "sessions_api_task_role_${var.environment}"
  policy_arn = aws_iam_policy.sessions_api_garmin_parser_policy.arn
}
