variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "default_vpc" {
}

variable "ecs_cluster" {
}

variable "alb_listener_arn" {
}

variable "service_td_count" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "sessions_events_bucket_arn" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_response_latency"
}

variable "auto_scaling_min_size" {
  default = 1
}

variable "auto_scaling_max_size" {
  default = 1
}

variable "auto_scaling_up_metric" {
  default = 10
}

variable "auto_scaling_down_metric" {
  default = 1
}

variable "auto_scaling_up_count" {
  default = 1
}

variable "auto_scaling_down_count" {
  default = -1
}

variable "auto_scaling_target_value" {
  default = ""
}

variable "sessions_api_sns_http_endpoint" {
}

variable "session_enricher_arn" {
}

variable "generate_clip_topic_arn" {
}

variable "garmin_parser_arn" {
}
