import fs from 'fs';
import path from 'path';
import Mocha from 'mocha';
import chai from 'chai';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';

chai.should();
chai.use(sinonChai);
chai.use(dirtyChai);

const findTestFiles = dir => {
  const contents = fs.readdirSync(dir);
  const filePaths = [];

  contents.forEach(element => {
    const elementPath = path.join(dir, element);
    const stats = fs.statSync(elementPath);

    if (stats.isDirectory()) {
      Array.prototype.push.apply(filePaths, findTestFiles(elementPath));
    } else if (element.substr(-8) === '.spec.js') {
      filePaths.push(elementPath);
    }
  });

  return filePaths;
};

const runMocha = dir =>
  new Promise((resolve, reject) => {
    try {
      const mocha = new Mocha();
      const files = findTestFiles(dir);
      files.forEach(file => {
        mocha.addFile(file);
      });
      mocha.run(resolve);
    } catch (err) {
      reject(err);
    }
  });

const tests = async () => {
  const failures = await runMocha(path.join(__dirname, 'src'));
  if (failures) process.exit(failures);
};

tests()
  .then(process.exit)
  .catch(err => {
    console.error(err);
    process.exit(-1);
  });
