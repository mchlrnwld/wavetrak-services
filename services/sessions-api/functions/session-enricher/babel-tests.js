require('dotenv').config({ path: '.env.test' });
require('@babel/register')({
  /**
   * Allows stubbing of ES6 methods that are used within the same file for example:
   *
   * export const foo = () => true
   *
   * export const bar = () => foo()
   *
   * With rewire we are able to stub foo properly.
   * Docs: https://github.com/speedskater/babel-plugin-rewire
   */
  plugins: [],
});
require('./tests');
