# Session Enricher Lambda Function

## Table of contents <!-- omit in toc -->

- [Summary](#summary)
- [Development](#development)
  - [Installation](#installation)
  - [Unit tests and linting](#unit-tests-and-linting)
  - [Integration test](#integration-test)
    - [Request](#request)
    - [Response](#response)
- [Jenkins deployment](#jenkins-deployment)

## Summary

Triggered from the PATCH endpoint used to edit the session's rating, name, or user defined spot of the session. If the request contains a `userSpotId`, the `sessions-api` service will re-invoke this lambda function in order to populate the session with new cam clips/spot details.

More information [here](https://github.com/Surfline/wavetrak-services/blob/master/services/sessions-api/docs/sessions.md#update-a-session).

## Development

### Installation

```bash
cp .env.sample .env
npm install
```

### Unit tests and linting

```bash
npm run test
npm run lint
```

### Integration test

The next call to the lambda function should be enough to test if we receive a successful response.

#### Request

```bash
export AWS_PROFILE=surfline-dev
aws lambda invoke \
    --function-name sl-sessions-api-enrich-session-sandbox \
    --payload "$(cat ./event.json | base64)" \
    ../output.json
```

#### Response

```json
{
    "StatusCode": 200,
    "ExecutedVersion": "$LATEST"
}
```

The output file generated should contain a `null` string. If not, this particular function outputs the error handled.

A good approach would be to browse the [CloudWatch logs for the lambda function](https://us-west-1.console.aws.amazon.com/cloudwatch/home?region=us-west-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fsl-sessions-api-enrich-session-sandbox) (sandbox) and read the complete log responses.

## Jenkins deployment

To deploy this application as a lambda function, launch a [Jenkins job](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-sessions-api/), in the desired environment.

Once the job is completed, you can check the `lambda-functions/sessions-api-enrich-session/sl-sessions-api-enrich-session.zip` file under the following S3 buckets:

| staging              | sandbox          | prod              |
| -------------------- | ---------------- | ----------------- |
| sl-artifacts-staging | sl-artifacts-dev | sl-artifacts-prod |
