import newrelic from 'newrelic';
import '@newrelic/aws-sdk';
import matchHandler from './handlers/matchHandler';

const lambdaHandler = async (event) => {
  try {
    const result = await matchHandler(event);
    return result;
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export default newrelic.setLambdaHandler(lambdaHandler);
