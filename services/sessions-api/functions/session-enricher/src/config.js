export default {
  sessionsApi: process.env.SESSIONS_API,
  spotsAPI: process.env.SPOTS_API,
  kbygProductApi: process.env.KBYG_PRODUCT_API,
  camerasAPI: process.env.CAMERAS_API,
  maxSaveFailures: process.env.MAX_SAVE_FAILURES,
  maxSpotDistance: process.env.MAX_SPOT_DISTANCE,
  clipBufferTime: process.env.CLIP_BUFFER_TIME,
  segmentWriteKey: process.env.SEGMENT_WRITE_KEY,
  sessionEventsBucket: process.env.SESSIONS_EVENTS_BUCKET,
  clientData: {
    sandbox: {
      '5af1ce73b5acf7c6dd2592ee': {
        name: 'Rip Curl',
      },
    },
    staging: {
      '5b0cc082b5acfa365f447b6a': {
        name: 'Rip Curl',
      },
      '5b76ffb3b41cae367d6609d1': {
        name: 'Dawn Patrol',
      },
    },
    production: {
      '5b0cc082b5acfa365f447b6a': {
        name: 'Rip Curl',
      },
      '5b764335b41cae367d6609cb': {
        name: 'Dawn Patrol',
      },
      '5b4684340daa62c91127de4c': {
        name: 'Amazon Alexa',
      },
    },
  },
};
