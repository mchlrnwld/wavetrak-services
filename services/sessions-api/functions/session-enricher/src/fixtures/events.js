export default [
  {
    _id: '5b806f6c888db745c12d1234',
    startTimestamp: '2018-08-24T20:45:31.000+0000',
    endTimestamp: '2018-08-24T20:45:31.999+0000',
    type: 'PADDLE',
    distance: 544310,
    speedMax: 8.56111796,
    speedAverage: 0.9972230200000001,
    positions: [
      {
        timestamp: '2017-03-07T09:17:07.000+0000',
        location: {
          coordinates: [-117.998803, 33.659484],
          type: 'Point',
        },
      },
      {
        timestamp: '2017-03-07T09:17:08.000+0000',
        location: {
          coordinates: [-117.998803, 33.659484],
          type: 'Point',
        },
      },
    ],
  },
  {
    _id: '5b806f6c888bc745cc4d1234',
    startTimestamp: '2018-08-24T20:45:31.000+0000',
    endTimestamp: '2018-08-24T20:45:31.999+0000',
    type: 'WAVE',
    distance: 54410,
    speedMax: 8.561796,
    speedAverage: 0.9972230200000001,
    positions: [
      {
        timestamp: '2017-03-07T09:17:07.000+0000',
        location: {
          coordinates: [-117.998803, 33.659484],
          type: 'Point',
        },
      },
      {
        timestamp: '2017-03-07T09:17:08.000+0000',
        location: {
          coordinates: [-117.998803, 33.659484],
          type: 'Point',
        },
      },
    ],
  },
];
