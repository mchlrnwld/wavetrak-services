import axios from 'axios';
import config from '../config';

const getCamera = async (cameraId) => {
  const { data: camera } = await axios.get(`${config.camerasAPI}/cameras/${cameraId}`);
  return camera;
};

export default getCamera;
