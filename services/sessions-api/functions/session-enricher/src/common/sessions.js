/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import config from '../config';

export const updateSession = (session) => {
  const headers = {
    'x-auth-userid': session.user,
    'x-api-key': session.client,
  };
  return axios
    .put(`${config.sessionsApi}/sessions/${session._id}/details`, { ...session }, { headers })
    .then((response) => response.data);
};

export const setSessionStatus = (user, client, sessionId, status) => {
  const headers = {
    'x-auth-userid': user,
    'x-api-key': client,
  };
  return axios
    .patch(`${config.sessionsApi}/sessions/${sessionId}/details`, { status }, { headers })
    .then((response) => response.data);
};
