import Analytics from 'analytics-node';
import config from '../config';

const slAnalytics = new Analytics(config.segmentWriteKey || 'NoWrite', {
  flushAt: 1,
});

export const track = (params, callback = null) => {
  slAnalytics.track(params, callback);
};

export default track;
