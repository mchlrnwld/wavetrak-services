import axios from 'axios';
import config from '../config';

/**
 * Gets the spot nearest to the given session's coordinates. If a spot with a cam is available
 * it will return the nearest spot with a cam. If a cam is not available it will return the
 * nearest spot without a cam.
 * @param {{ lon: number, lat: number }} coordinates
 * @param {number} distance - max distance to search from given coords
 */
export const getNearbySpot = async (coordinates, distance) => {
  const { lon, lat } = coordinates;
  const nearbySpots = await axios
    .get(
      `${config.spotsAPI}/admin/spots/?filter=nearby&distance=${distance}&coordinates=${lon}&coordinates=${lat}&limit=10&select=id,cams,name,status,location`,
    )
    .then((res) => res.data);

  return nearbySpots[0];
};

export const getSpot = async (spotId) => {
  const spot = await axios.get(`${config.spotsAPI}/admin/spots/${spotId}`);

  return spot.data;
};

/**
 * Queries the Spots API for the historical spot report view near the given timestamp.
 * @param {string} spotId
 * @param {number} timstamp - unix timestamp
 */
export const getHistoricalSpotReportView = async (spotId, timestamp) => {
  const spotReportView = await axios.get(
    `${config.kbygProductApi}/spots/archivedreports?spotId=${spotId}&startTimestamp=${timestamp}`,
  );

  return spotReportView.data;
};
