import AWS from 'aws-sdk';
import config from '../config';

const bucket = config.sessionEventsBucket;
// eslint-disable-next-line import/prefer-default-export
export const getSessionEvents = async (sessionId) => {
  const s3 = new AWS.S3();

  const s3Params = {
    Bucket: bucket,
    Key: `${sessionId}.json`,
  };

  try {
    const response = await s3.getObject(s3Params).promise();
    return JSON.parse(response.Body);
  } catch (err) {
    console.log(err);
    throw new Error(`Could not retrieve session events for session ${sessionId} from S3`);
  }
};
