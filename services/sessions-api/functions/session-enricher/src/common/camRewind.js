/* eslint-disable no-unused-vars */
import axios from 'axios';
import config from '../config';

const postClip = async (startTimestampInMs, endTimestampInMs, cameraId) => {
  const response = await axios.post(`${config.camerasAPI}/clips`, {
    cameraId,
    startTimestampInMs,
    endTimestampInMs,
  });

  return response.data.clipId;
};

export default postClip;
