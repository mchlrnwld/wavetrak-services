import { expect } from 'chai';
import { addClipBuffers } from './addClipBuffers';

describe('addClipBuffers', () => {
  it('should add the buffer time to the start and end of the clips', async () => {
    const startTimestamp = '2017-03-07T12:00:30.000+0000';
    const endTimestamp = '2017-03-07T12:00:30.000+0000';
    const result = addClipBuffers(startTimestamp, endTimestamp);

    expect(result).to.deep.equal([1488888000000, 1488888060000]);
  });
});
