const round = (value, decimals) => {
  const rounded = Math.round(`${value}e${decimals}`);
  return Number(`${rounded}e-${decimals}`);
};

export default (session) => {
  const { events, partner, startTimestamp, endTimestamp } = session;
  const waves = events.filter((event) => event.type === 'WAVE');
  const caughtWaves = waves.filter(
    (wave) => !wave.outcome || wave.outcome.toUpperCase() !== 'ATTEMPTED',
  );
  const paddles = events.filter((event) => event.type === 'PADDLE');

  // cannot assume both waves and paddles
  const distanceWaves = caughtWaves.length
    ? caughtWaves.map((wave) => wave.distance).reduce((a, b) => a + b)
    : null;
  const speedMax = caughtWaves.length
    ? caughtWaves.map((wave) => wave.speedMax).reduce((a, b) => (a > b ? a : b))
    : null;
  const distancePaddled = paddles.length
    ? paddles.map((paddle) => paddle.distance).reduce((a, b) => a + b)
    : null;

  let waveDistance = 0;
  let waveSeconds = 0;
  caughtWaves.forEach((wave) => {
    if (wave.distance > waveDistance) {
      waveDistance = wave.distance;
      // With new Date you can subtract to get milliseconds, then convert to seconds.
      const etsCompare = new Date(wave.endTimestamp);
      const stsCompare = new Date(wave.startTimestamp);
      waveSeconds = (etsCompare - stsCompare) / 1000;
    }
  });

  // longestWave data
  const longestWave = {
    distance: waveDistance || null,
    seconds: waveSeconds || null,
  };

  // gather wavesPerHour based on start end timestamps
  const etsSessionCompare = new Date(endTimestamp);
  const stsSessionCompare = new Date(startTimestamp);
  const sessionHours = (etsSessionCompare - stsSessionCompare) / 1000 / 60 / 60;
  const wavesPerHour = caughtWaves.length ? round(caughtWaves.length / sessionHours, 2) : null;

  const calories = partner?.stats?.calories;

  const stats = {
    waveCount: caughtWaves.length >= 0 ? caughtWaves.length : 0,
    startTimestamp,
    endTimestamp,
    speedMax,
    longestWave,
    distancePaddled,
    distanceWaves,
    wavesPerHour,
    calories,
  };

  return stats;
};
