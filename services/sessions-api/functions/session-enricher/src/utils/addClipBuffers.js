import moment from 'moment';
import config from '../config';

/**
 * @description When a user has a `WAVE` event and we want to generate a clip, we want to
 * start the clip slightly before and end it slightly after the wave so that they can see
 * the entirety of the start and end of the wave.
 * We use an environment variable `CLIP_BUFFER_TIME` to make this easily configurable without
 * code change.
 * @param {String | Date} startTimestamp - The start timestamp of the WAVE event
 * @param {String | Date} endTimestamp - The end timestamp of the WAVE event
 */
export const addClipBuffers = (startTimestamp, endTimestamp) => {
  const bufferTime = config.clipBufferTime;
  const bufferedStart = moment(startTimestamp).subtract(bufferTime, 's').valueOf();
  const bufferedEnd = moment(endTimestamp).add(bufferTime, 's').valueOf();

  return [bufferedStart, bufferedEnd];
};
