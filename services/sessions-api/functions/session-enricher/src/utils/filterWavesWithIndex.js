import mongoose from 'mongoose';

export default (waveEvents, event, waveIndex) => {
  // Some legacy session don't have event ID's. We can instantiate them here by mutating the event
  // this will get picked up by S3 when it hits the PUT details endpoint
  if (!event._id) {
    // eslint-disable-next-line no-param-reassign
    event._id = mongoose.Types.ObjectId();
  }
  if (event.type === 'WAVE') {
    const wave = { ...event, waveIndex };
    return [...waveEvents, wave];
  }
  return waveEvents;
};
