import * as analytics from '../common/analytics';

/**
 * @param {{}} enrichedSession
 * @returns {void}
 */
export default (enrichedSession) => {
  const { spotReportView } = enrichedSession;
  const trackData = {
    event: 'Enriched Session',
    userId: enrichedSession.user,
    properties: {
      sessionId: enrichedSession._id,
      client: enrichedSession.client || 'unknown',
      waveCount: enrichedSession.waveCount,
      maxSpeed: enrichedSession.speedMax,
      maxDistance: enrichedSession.distanceLongestWave,
      startLatLon: enrichedSession.startLatLon,
      startTimestamp: enrichedSession.startTimestamp,
      stopTimestamp: enrichedSession.endTimestamp,
      spotReliveRating: enrichedSession.spot?.relivableRating,
      spotId: enrichedSession.spot?.id,
      spotName: enrichedSession.spot?.name,
      sessionLength:
        new Date(enrichedSession.endTimestamp) - new Date(enrichedSession.startTimestamp),
      windSpeed: spotReportView?.wind?.speed,
      windDirection: spotReportView?.wind?.direction,
      currentTideHeight: spotReportView?.tide?.current?.height,
      nextTideHeight: spotReportView?.tide?.next?.height,
      waveHeightMin: spotReportView?.waveHeight?.min,
      waveHeightMax: spotReportView?.waveHeight?.max,
      condition: spotReportView?.conditions?.value || 'lola',
    },
  };
  analytics.track(trackData);
};
