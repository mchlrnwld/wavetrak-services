import { expect } from 'chai';
import sinon from 'sinon';
import * as enrichHandler from './index';
import handler from './matchHandler';
import sessionFixture from '../fixtures/session';
import record from '../fixtures/record';
import events from '../fixtures/events';

import * as sessionsApi from '../common/sessions';
import * as spotsApi from '../common/spots';
import * as sessionEvents from '../common/sessionEvents';

describe('enrichHandler', () => {
  beforeEach(() => {
    sinon.stub(sessionsApi, 'updateSession');
    sinon.stub(sessionEvents, 'getSessionEvents');
    sinon.stub(spotsApi, 'getHistoricalSpotReportView');
    sinon.stub(spotsApi, 'getSpot');
    sinon.stub(sessionsApi, 'setSessionStatus');
  });

  afterEach(() => {
    sessionsApi.updateSession.restore();
    sessionEvents.getSessionEvents.restore();
    spotsApi.getHistoricalSpotReportView.restore();
    spotsApi.getSpot.restore();
    sessionsApi.setSessionStatus.restore();
  });

  it('should trigger then enrichHandler with method "enrich"', async () => {
    sinon.stub(enrichHandler, 'default');
    const event = record;

    enrichHandler.default.resolves({ success: true });

    const response = await handler(event);
    expect(response[0].success).to.be.true();

    enrichHandler.default.restore();
  });

  it("should use the user's given spot if they have a userSpotId", async () => {
    spotsApi.getSpot.resolves({});
    sessionEvents.getSessionEvents.resolves(events);
    spotsApi.getHistoricalSpotReportView.resolves({});
    sessionsApi.updateSession.resolves({ success: true });

    const response = await enrichHandler.default({
      session: {
        ...sessionFixture,
        userSpotId: '5977abb3b38c2300127471ec',
      },
    });

    expect(response.success).to.be.true();
    expect(spotsApi.getSpot).to.have.been.calledOnceWith('5977abb3b38c2300127471ec');
    expect(sessionsApi.updateSession).to.have.been.calledOnce();
  });
});
