import { expect } from 'chai';
import sinon from 'sinon';
import matchHandler from './matchHandler';
import record from '../fixtures/record';
import session from '../fixtures/session';
import events from '../fixtures/events';

import * as sessionsApi from '../common/sessions';
import * as spotsApi from '../common/spots';
import * as sessionEvents from '../common/sessionEvents';

describe('matchHandler', () => {
  beforeEach(() => {
    sinon.stub(sessionsApi, 'updateSession');
    sinon.stub(sessionEvents, 'getSessionEvents');
    sinon.stub(spotsApi, 'getHistoricalSpotReportView');
    sinon.stub(spotsApi, 'getSpot');
    sinon.stub(spotsApi, 'getNearbySpot');
    sinon.stub(sessionsApi, 'setSessionStatus');
  });

  afterEach(() => {
    sessionsApi.updateSession.restore();
    sessionEvents.getSessionEvents.restore();
    spotsApi.getHistoricalSpotReportView.restore();
    spotsApi.getSpot.restore();
    sessionsApi.setSessionStatus.restore();
  });

  it('should set the session status to FAILED if the enrichHandler throws', async () => {
    spotsApi.getNearbySpot.resolves({});
    sessionEvents.getSessionEvents.resolves(events);
    spotsApi.getHistoricalSpotReportView.resolves({});
    sessionsApi.updateSession.throws();
    sessionsApi.setSessionStatus.resolves({ success: true });

    let err;
    try {
      await matchHandler(record);
    } catch (error) {
      err = error;
    }

    expect(err).to.exist();
    expect(err.userId).to.be.equal(session.user);
    expect(err.sessionId).to.be.equal(session._id);
    expect(err.clientId).to.be.equal(session.client);

    expect(sessionsApi.updateSession).to.have.been.calledOnce();
    expect(sessionsApi.setSessionStatus).to.have.been.calledOnce();
  });
});
