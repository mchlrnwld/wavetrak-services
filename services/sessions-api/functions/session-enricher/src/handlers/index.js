import mongoose from 'mongoose';
import _round from 'lodash/round';
import postClip from '../common/camRewind';
import config from '../config';
import compileSurflineStats from '../utils/compileSurflineStats';
import { updateSession } from '../common/sessions';
import { getSpot, getNearbySpot, getHistoricalSpotReportView } from '../common/spots';
import getCamera from '../common/cameras';
import trackSessionEnriched from '../utils/trackSessionEnriched';
import { getSessionEvents } from '../common/sessionEvents';
import { addClipBuffers } from '../utils/addClipBuffers';
import filterWavesWithIndex from '../utils/filterWavesWithIndex';

/**
 * Lambda handler for enriching a user's session. This function is responsible for
 * retrieving a spot for a session and making a POST call to the cam service
 * to generate clip id's if the spot has a cam.
 * @param {Object} event
 * @param {Object} event.session
 */
export default async (event) => {
  const { session, reEnrich, deleteClips } = event;
  const { originalSpotId, startTimestamp, gpsCenter, userSpotId } = session;
  const { maxSpotDistance } = config;
  const [lon, lat] = gpsCenter.coordinates;
  const sessionDate = new Date(startTimestamp);
  const sessionStartTimestamp = sessionDate.getTime();

  const enrichDataOnly = reEnrich && reEnrich === 'dataOnly';

  try {
    const events = await getSessionEvents(session._id);
    session.events = events;

    const spot = userSpotId
      ? await getSpot(userSpotId)
      : await getNearbySpot({ lon, lat }, maxSpotDistance);

    let spotReportView;
    if (spot) {
      try {
        spotReportView = await getHistoricalSpotReportView(spot._id, sessionStartTimestamp);
      } catch (error) {
        if (error?.response?.status !== 400) {
          console.log(
            'There was an error retrieving the historical SRV, this will not prevent enrichment',
          );
          console.log(error);
        }
      }
    }

    const hasCams = spot?.cams?.length > 0;
    const stats = compileSurflineStats(session);

    const enrichedSession = {
      ...session,
      surfline: {
        stats,
      },
      status: 'ENRICHED',
      spotReportView,
    };

    if (spot) {
      enrichedSession.spot = {
        id: spot._id,
        name: spot.name,
        location: spot.location,
      };
      if (!originalSpotId) {
        enrichedSession.originalSpotId = spot._id;
      }
    } else {
      enrichedSession.spot = {
        name: `${_round(lat, 2)},${_round(lon, 2)}`,
        id: null,
        location: {
          type: 'Point',
          coordinates: [lon, lat],
        },
      };
    }

    /**
     * We want to delete the current clips before creating any new clips.
     * If the user passes `deleteClips: true` and the new spot has cams,
     * having this block after the next block would make the new clips
     * get deleted as well.
     */
    if (deleteClips || !hasCams) {
      enrichedSession.cams = [];
      enrichedSession.clips = [];
    }

    if (hasCams && !enrichDataOnly) {
      // filter the events for waves and save the index so we can populate the clips
      const waves = events.reduce(filterWavesWithIndex, []);

      // grab useful cam data for each spot.cams id value
      const camResponse = await Promise.all(
        spot.cams.map(async (cameraId) => {
          try {
            const cameraData = await getCamera(cameraId);
            const { _id: id, title, alias, isPremium, stillUrl } = cameraData;
            return {
              id,
              title,
              alias,
              isPremium,
              stillUrl,
              visible: true,
            };
          } catch (e) {
            return undefined;
          }
        }),
      );

      // filter out any bad cams
      const cams = camResponse.filter((cam) => !!cam);
      // add resulting cams array to enriched session
      enrichedSession.cams = cams;

      const postClips = Promise.all(
        waves.map(({ startTimestamp: startTime, endTimestamp: endTime }) =>
          Promise.all(cams.map(({ id }) => postClip(...addClipBuffers(startTime, endTime), id))),
        ),
      );

      const clips = await postClips;
      enrichedSession.clips = [];

      // Put the clips into the session tied to their corresponding wave event
      waves.forEach((wave, index) => {
        const waveClips = clips[index];
        const formattedClips = waveClips.map((clipId) => ({
          _id: mongoose.Types.ObjectId(clipId),
          eventId: mongoose.Types.ObjectId(enrichedSession.events[wave.waveIndex]._id),
        }));
        enrichedSession.clips = enrichedSession.clips.concat(formattedClips || []);
      });
    }

    delete enrichedSession.events;
    const updatedSession = await updateSession(enrichedSession);
    // track enriched session segment event
    const shouldTrack = !userSpotId && !reEnrich;

    if (shouldTrack) {
      trackSessionEnriched(enrichedSession);
    }
    return updatedSession;
  } catch (error) {
    error.userId = session.user;
    error.sessionId = session._id;
    error.clientId = session.client;
    throw error;
  }
};
