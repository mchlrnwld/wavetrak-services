import enrichHandler from '.';
import { setSessionStatus } from '../common/sessions';
/**
 * Responsible for parsing the event object passed into the lambda function, and
 * returning the appropriate handler for the given parameters.
 *
 * @param {Object} event event object passed into the lambda handler
 */
const matchHandler = async (event) => {
  const records = await Promise.all(
    event?.Records.map(async (record) => {
      const { body } = record;
      const parsedBody = JSON.parse(body);

      console.log(`Processing session: ${parsedBody?.session?._id}`);

      if (parsedBody?.method === 'enrich') {
        try {
          const response = await enrichHandler(parsedBody);
          return response;
        } catch (error) {
          // Pluck the metadata off the error
          const { userId, sessionId, clientId } = error;

          if (userId && sessionId && clientId) {
            await setSessionStatus(userId, clientId, sessionId, 'FAILED');
          }

          throw error;
        }
      }
      return { success: true };
    }) || [],
  );
  return records;
};

export default matchHandler;
