# General variables
variable "application" {
  description = "The application name"
  default     = ""
}

variable "company" {
  description = "The company (sl, bw, ft, wt, msw)"
  default     = ""
}

variable "environment" {
  description = "The environment (sbox, staging, prod)"
  default     = ""
}

# Function variables
variable "artifacts_bucket" {
  description = "The artifacts S3 bucket"
  default     = ""
}

variable "function_name" {
  description = "The function name"
  default     = ""
}

variable "runtime" {
  description = "The runtime that the Lambda function should use"
  default     = "nodejs8.10"
}

variable "service_handler" {
  description = "The service handler that the Lambda function should use"
  default     = "service.handler"
}

variable "memory_size" {
  description = "The memory size of the Lambda function"
  default     = 128
}

variable "timeout" {
  description = "The timeout of the Lambda function"
  default     = 30
}

variable "tracing_config_mode" {
  description = "The tracing config mode of the Lambda function (Active, PassThrough)"
  default     = "PassThrough"
}

# Network variables
variable "instance_subnets" {
  description = "The subnets that the function will execute in"
  type        = list(string)
  default     = []
}

variable "internal_sg_group" {
  description = "The internal servers security group"
  default     = ""
}

variable "sg_all_servers" {
  description = "The all-servers security group"
  default     = ""
}

