module "lambda" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/lambda"

  company     = var.company
  application = var.application
  environment = var.environment

  artifacts_bucket    = var.artifacts_bucket
  function_name       = var.function_name
  runtime             = var.runtime
  service_handler     = var.service_handler
  memory_size         = var.memory_size
  timeout             = var.timeout
  tracing_config_mode = var.tracing_config_mode

  internal_sg_group = var.internal_sg_group
  sg_all_servers    = var.sg_all_servers
  instance_subnets  = var.instance_subnets

  concurrent_executions = 5
}

# S3 bucket for content
resource "aws_s3_bucket" "sessions_events_bucket" {
  bucket = "${var.company}-${var.application}-sessions-events-${var.environment}"
  acl    = "private"

  tags = {
    Name        = "${var.company}-${var.application}-sessions-events-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
  }
}

# Permit Lambda function access to sessions events S3 bucket
resource "aws_iam_policy" "sessions_events_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-sessions-events-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"

  policy = templatefile("${path.module}/resources/sessions-events-policy.json", {
    resource_arn = aws_s3_bucket.sessions_events_bucket.arn
  })
}

resource "aws_iam_role_policy_attachment" "sessions_events_bucket_mgmt_policy_attachement" {
  role       = module.lambda.lambda_role_name
  policy_arn = aws_iam_policy.sessions_events_bucket_mgmt_policy.arn
}

resource "aws_sqs_queue" "queue_deadletter" {
  name = "${var.company}-${var.application}-sessions-enricher-queue_deadletter_${var.environment}"
}

resource "aws_sqs_queue" "sessions_enricher_queue" {
  name                       = "${var.company}-${var.application}-sessions-enricher-queue-${var.environment}"
  visibility_timeout_seconds = 180

  redrive_policy = templatefile("${path.module}/resources/sessions-enricher-queue.json", {
    resource_arn = aws_sqs_queue.queue_deadletter.arn
  })

  tags = {
    Name        = "${var.company}-${var.application}-sessions-enricher-queue-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
  }
}

# Grant permissions to the Lambda function to receive messages from SQS
resource "aws_iam_policy" "lambda_sqs_policy" {
  name        = "${var.company}-${var.application}-lambda-sqs-policy-${var.environment}"
  description = "Policy to receive SQS message from Lambda function"

  policy = templatefile("${path.module}/resources/lambda-sqs-policy.json", {
    resource_arn = aws_sqs_queue.sessions_enricher_queue.arn
  })
}

resource "aws_iam_role_policy_attachment" "lambda_sqs_policy" {
  role       = module.lambda.lambda_role_name
  policy_arn = aws_iam_policy.lambda_sqs_policy.arn
}

resource "aws_lambda_event_source_mapping" "invoke_enricher_lambda" {
  event_source_arn = aws_sqs_queue.sessions_enricher_queue.arn
  function_name    = module.lambda.lambda_function_arn
  batch_size       = 1
}
