provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "session-enricher/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "session-enricher" {
  source = "../../"

  company     = "sl"
  application = "sessions-api"
  environment = "prod"

  artifacts_bucket    = "sl-artifacts-prod"
  function_name       = "enrich-session"
  runtime             = "nodejs12.x"
  service_handler     = "dist/index.default"
  tracing_config_mode = "Active"
  timeout             = 120
  memory_size         = 512

  internal_sg_group = "sg-a5a0e5c0"
  instance_subnets  = ["subnet-d1bb6988", "subnet-85ab36e0"]
  sg_all_servers    = "sg-a4a0e5c1"
}
