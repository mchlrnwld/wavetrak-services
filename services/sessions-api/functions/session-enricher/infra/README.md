# Session Enricher Infrastructure

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.28`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments are `sbox`, `staging`, `prod`.
