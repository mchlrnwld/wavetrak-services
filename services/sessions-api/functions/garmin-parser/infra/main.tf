module "lambda" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/lambda"

  company     = var.company
  application = var.application
  environment = var.environment

  artifacts_bucket    = var.artifacts_bucket
  function_name       = var.function_name
  runtime             = var.runtime
  service_handler     = var.service_handler
  memory_size         = var.memory_size
  timeout             = var.timeout
  tracing_config_mode = var.tracing_config_mode

  internal_sg_group   = var.internal_sg_group
  sg_all_servers      = var.sg_all_servers
  instance_subnets    = var.instance_subnets
}
