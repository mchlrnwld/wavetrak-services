provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "garmin-parser/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "garmin-parser" {
  source = "../../"

  company     = "wt"
  application = "sessions-api"
  environment = "prod"

  artifacts_bucket    = "sl-artifacts-prod"
  function_name       = "garmin-parser"
  runtime             = "nodejs12.x"
  service_handler     = "dist/index.default"
  memory_size         = 128
  timeout             = 200
  tracing_config_mode = "Active"

  internal_sg_group = "sg-a5a0e5c0"
  instance_subnets  = ["subnet-d1bb6988", "subnet-85ab36e0"]
  sg_all_servers    = "sg-a4a0e5c1"
}
