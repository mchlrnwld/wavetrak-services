provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "garmin-parser/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "garmin-parser" {
  source = "../../"

  company     = "wt"
  application = "sessions-api"
  environment = "sandbox"

  artifacts_bucket    = "sl-artifacts-dev"
  function_name       = "garmin-parser"
  runtime             = "nodejs12.x"
  service_handler     = "dist/index.default"
  memory_size         = 128
  timeout             = 30
  tracing_config_mode = "Active"

  internal_sg_group = "sg-90aeaef5"
  instance_subnets  = ["subnet-0909466c", "subnet-f2d458ab"]
  sg_all_servers    = "sg-91aeaef4"
}
