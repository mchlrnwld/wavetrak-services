import newrelic from 'newrelic';
import '@newrelic/aws-sdk';
import processFitFileHandler from './handlers';

const lambdaHandler = async (event) => {
  try {
    const result = await processFitFileHandler(event);
    return result;
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export default newrelic.setLambdaHandler(lambdaHandler);
