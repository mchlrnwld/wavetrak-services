/* eslint-disable no-unused-expressions */
import { expect } from 'chai';
import sinon from 'sinon';
import newrelic from 'newrelic';
import '@newrelic/aws-sdk';
import * as processFitFileHandler from '.';
import * as fitFileHelper from '../common/fitfile';
import * as userApi from '../common/user';
import * as sessionsApi from '../common/sessions';
import * as oauth from '../utils/oauth';
import event from '../fixtures/events';
import fitFileParsedOutput from '../fixtures/output';
import userOutput from '../fixtures/user';
import sessionsPayload, { emptySplitsSession } from '../fixtures/sessions';

describe('handler', () => {
  const fitFileByteContent = null;
  const error = { code: 500, message: 'Something went wrong' };
  beforeEach(() => {
    sinon.stub(console, 'log');
    sinon.stub(console, 'info');
    sinon.stub(fitFileHelper, 'getFitFile');
    sinon.stub(fitFileHelper, 'parseFitFile');
    sinon.stub(userApi, 'getUserByClientAccessToken');
    sinon.spy(fitFileHelper, 'queryFitFileOutput');
    sinon.stub(sessionsApi, 'postSession');
    sinon.spy(newrelic, 'noticeError');
    sinon.spy(newrelic, 'recordCustomEvent');
    sinon.stub(oauth, 'createOauthHeaders');
    fitFileHelper.getFitFile.resolves(fitFileByteContent);
    fitFileHelper.parseFitFile.resolves(fitFileParsedOutput);
    userApi.getUserByClientAccessToken.resolves(userOutput);
    sessionsApi.postSession.resolves({ message: 'success' });
    oauth.createOauthHeaders.returns({ Authorization: 'Authorization' });
  });
  afterEach(() => {
    console.log.restore();
    console.info.restore();
    fitFileHelper.getFitFile.restore();
    fitFileHelper.parseFitFile.restore();
    fitFileHelper.queryFitFileOutput.restore();
    userApi.getUserByClientAccessToken.restore();
    sessionsApi.postSession.restore();
    newrelic.noticeError.restore();
    newrelic.recordCustomEvent.restore();
    oauth.createOauthHeaders.restore();
  });

  it('should parse the Garmin FIT file and transform to Sessions API format and POST to /sessions', async () => {
    const { records } = fitFileParsedOutput;
    const queryRecordCondition = `$..[?(@.timestamp>=${1584552221000} && @.timestamp<=${1584552242000})]`;
    const response = await processFitFileHandler.default(event);
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWith(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWith('testToken', undefined);
    expect(fitFileHelper.queryFitFileOutput).to.have.been.calledWith(records, queryRecordCondition);
    expect(sessionsApi.postSession).to.have.been.calledWith(sessionsPayload);
    expect(response).to.deep.equal({ message: 'success' });
  });

  it('should not proceed if there are no positions', async () => {
    fitFileHelper.parseFitFile.resolves({
      ...fitFileParsedOutput,
      records: [
        {
          timestamp: 1584552236000,
          elapsed_time: 590,
          timer_time: 590,
          distance: 801.32,
          heart_rate: 106,
          temperature: 19,
        },
      ],
    });
    const response = await processFitFileHandler.default(event);
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(sessionsApi.postSession).to.have.not.been.called();
    expect(response).to.equal(true);
  });

  it('should handle a session with no splits', async () => {
    const { records } = fitFileParsedOutput;
    const queryRecordCondition = `$..[?(@.timestamp>=${1584552236000} && @.timestamp<=${1584554176280})]`;
    fitFileHelper.parseFitFile.resolves({
      ...fitFileParsedOutput,
      splits: [],
      file_id: {},
    });
    const response = await processFitFileHandler.default(event);
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWith(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWith('testToken', undefined);
    expect(fitFileHelper.queryFitFileOutput).to.have.been.calledWith(records, queryRecordCondition);
    expect(sessionsApi.postSession).to.have.been.calledWith({
      ...emptySplitsSession,
      deviceType: 'Garmin',
    });
    expect(response).to.deep.equal({ message: 'success' });
  });

  it('should handle a session endTimestamp that is smaller than the last event timestamp', async () => {
    fitFileHelper.parseFitFile.resolves({
      ...fitFileParsedOutput,
      sessions: [{ ...fitFileParsedOutput.sessions[0], total_elapsed_time: -999999999 }],
    });
    const { records } = fitFileParsedOutput;
    const queryRecordCondition = `$..[?(@.timestamp>=${1584552221000} && @.timestamp<=${1584552242000})]`;
    const response = await processFitFileHandler.default(event);
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWith(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWith('testToken', undefined);
    expect(fitFileHelper.queryFitFileOutput).to.have.been.calledWith(records, queryRecordCondition);
    expect(sessionsApi.postSession).to.have.been.calledWith({
      ...sessionsPayload,
      endTimestamp: 1584552242000,
    });
    expect(response).to.deep.equal({ message: 'success' });
  });

  it('should throw an error if the user access token does not exist', async () => {
    let err;
    try {
      await processFitFileHandler.default({ ...event, userAccessToken: undefined });
    } catch (handlerError) {
      err = handlerError;
    }
    expect(err.message).to.equal('userAccessToken property is required on event.');
    expect(fitFileHelper.parseFitFile).to.have.not.been.called();
    expect(userApi.getUserByClientAccessToken).to.have.not.been.called();
  });

  it('should throw an error if the callback URL does not exist', async () => {
    let err;
    try {
      await processFitFileHandler.default({ ...event, callbackURL: undefined });
    } catch (handlerError) {
      err = handlerError;
    }
    expect(err.message).to.equal('callbackURL property is required on event.');
    expect(fitFileHelper.parseFitFile).to.have.not.been.called();
    expect(userApi.getUserByClientAccessToken).to.have.not.been.called();
  });

  it('should not process if there is an error getting FIT file', async () => {
    fitFileHelper.getFitFile.rejects(error);
    let err;
    try {
      await processFitFileHandler.default(event);
    } catch (handlerError) {
      err = handlerError;
    }
    expect(err).to.deep.equal(error);
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.notCalled;
    expect(userApi.getUserByClientAccessToken).to.have.been.notCalled;
  });

  it('should gracefully handle a FIT file for a non-surfing sport', async () => {
    fitFileHelper.parseFitFile.resolves({ sport: { sport: 'yoga' } });
    const result = await processFitFileHandler.default(event);
    expect(result).to.be.null();
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWithExactly(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWithExactly(
      event.userAccessToken,
      undefined,
    );
  });

  it('should not process if there is an error parsing FIT file', async () => {
    fitFileHelper.parseFitFile.rejects(error);
    let err;
    try {
      await processFitFileHandler.default(event);
    } catch (handlerError) {
      err = handlerError;
    }
    expect(err).to.deep.equal(error);

    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWith(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.notCalled;
  });

  it('should not process if there is an invalid user Token', async () => {
    userApi.getUserByClientAccessToken.rejects(error);
    let err;
    try {
      await processFitFileHandler.default(event);
    } catch (handlerError) {
      err = handlerError;
    }
    expect(err).to.deep.equal(error);
    expect(fitFileHelper.getFitFile).to.have.not.been.called();
    expect(fitFileHelper.parseFitFile).to.have.not.been.called();
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWith('testToken');
    expect(fitFileHelper.queryFitFileOutput).to.have.been.notCalled;
  });

  it('should log a NewRelic error if there is an error POSTing to /sessions', async () => {
    sessionsApi.postSession.rejects({ message: 'error' });
    const { records } = fitFileParsedOutput;

    const queryRecordCondition = `$..[?(@.timestamp>=${1584552221000} && @.timestamp<=${1584552242000})]`;
    let err;
    try {
      await processFitFileHandler.default(event);
    } catch (handlerError) {
      err = handlerError;
    }

    expect(err).to.deep.equal({ message: 'error' });
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('garminCallbackURL', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWith(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWith('testToken', undefined);
    expect(fitFileHelper.queryFitFileOutput).to.have.been.calledWith(records, queryRecordCondition);
    expect(sessionsApi.postSession).to.have.been.calledWith(sessionsPayload);
    expect(newrelic.noticeError).to.have.been.calledOnce();
  });

  it('should process a dry run when the flag is specified', async () => {
    userApi.getUserByClientAccessToken.restore();
    sinon.spy(userApi, 'getUserByClientAccessToken');
    const { records } = fitFileParsedOutput;
    const queryRecordCondition = `$..[?(@.timestamp>=${1584552221000} && @.timestamp<=${1584552242000})]`;
    const response = await processFitFileHandler.default({
      callbackURL: 'dryRunUrl',
      dryRun: true,
    });
    expect(fitFileHelper.getFitFile).to.have.been.calledOnceWith('dryRunUrl', {
      Authorization: 'Authorization',
    });
    expect(fitFileHelper.parseFitFile).to.have.been.calledOnceWith(fitFileByteContent);
    expect(userApi.getUserByClientAccessToken).to.have.been.calledOnceWith('testDryRunToken', true);
    expect(fitFileHelper.queryFitFileOutput).to.have.been.calledWith(records, queryRecordCondition);
    expect(sessionsApi.postSession).to.have.not.been.called();
    sessionsPayload.clientId = '5c0ffa88ffda1e500b1bedc7';
    expect(response).to.deep.equal(sessionsPayload);
  });
});
