import newrelic from 'newrelic';
import '@newrelic/aws-sdk';
import fs from 'fs';
import { getUserByClientAccessToken } from '../common/user';
import { postSession } from '../common/sessions';
import { getFitFile, parseFitFile, queryFitFileOutput } from '../common/fitfile';
import { createOauthHeaders } from '../utils/oauth';

const SURFING = 'surfing';
const WAVE = 'WAVE';
const PADDLE = 'PADDLE';

/**
 * Lambda handler for processing a Garmin FIT file.
 * @param {{ userAccessToken: string, callbackURL: string, debug?: boolean }} event
 */
const processFitFileHandler = async (event) => {
  // Log out event for the time being so we can debug
  console.log(event);

  const { callbackURL, dryRun, debug } = event;

  const userAccessToken = dryRun ? 'testDryRunToken' : event.userAccessToken;

  if (!userAccessToken) {
    throw new Error('userAccessToken property is required on event.');
  }

  if (!callbackURL) {
    throw new Error('callbackURL property is required on event.');
  }

  // Validate the user accessToken and get tokenSecret
  const { _id: user, linkedClients } = await getUserByClientAccessToken(userAccessToken, dryRun);
  const { clientId, tokenSecret } = linkedClients.find(({ token }) => userAccessToken === token);

  const oauthHeaders = createOauthHeaders(userAccessToken, tokenSecret, callbackURL);

  // Pull the FIT file from Garmin
  const content = await getFitFile(callbackURL, oauthHeaders, debug, dryRun);

  // Parse the FIT file
  const fitFile = await parseFitFile(content);
  const watchVersion = fitFile.file_id?.product;

  /* istanbul ignore next */
  if (debug) {
    fs.writeFileSync('./fit-file.json', JSON.stringify(fitFile, null, 2));
  }

  // Stop processing the file if sport is not surfing
  const sport = fitFile?.sport?.sport;
  if (sport !== SURFING) {
    console.info('FIT File is not a surf file.');
    return null;
  }

  // Start Transformation
  const { records, sessions } = fitFile;
  let { splits } = fitFile;

  // Find the matching session. This is an array so it's safer to use find than assume it will only
  // have one entry.
  const sessionField = sessions.find((session) => session.sport === SURFING);
  const sessionStartTimestamp = sessionField.start_time;
  const sessionEndTimestamp = sessionStartTimestamp + sessionField.total_elapsed_time * 1000;

  const hasSplits = splits?.length;

  // If there are no splits/waves send a new relic event and create a dummy split
  // so that we can process all the records as part of a paddle event
  if (!hasSplits) {
    console.info('Session has no splits');
    newrelic.recordCustomEvent('GarminParser', {
      event: 'No splits found',
      userAccessToken,
      watchVersion: watchVersion || 'Garmin',
    });
    splits = [
      {
        start_time: sessionStartTimestamp,
        total_elapsed_time: sessionField.total_elapsed_time,
      },
    ];
  }

  // Process each split item and store it in an events array
  const events = splits
    .map((split) => {
      const type = hasSplits ? WAVE : PADDLE;
      const {
        start_time: startTimestamp,
        total_elapsed_time: totalElapsedTime,
        total_distance: totalDistance,
      } = split;
      const totalElapsedTimeinMs = totalElapsedTime * 1000;
      const endTimestamp = startTimestamp + totalElapsedTimeinMs;

      // Query the records object to get positions that match the condition
      const queryRecordCondition = `$..[?(@.timestamp>=${startTimestamp} && @.timestamp<=${endTimestamp})]`;
      const matchedPositions = queryFitFileOutput(records, queryRecordCondition);
      const numMatchedPositions = matchedPositions.length;

      let hasGpsPositions = false;
      let totalSpeed = 0;
      let speedMax = 0;
      for (let positionIndex = 0; positionIndex < numMatchedPositions; positionIndex += 1) {
        const pos = matchedPositions[positionIndex];
        const {
          enhanced_speed: enhancedSpeed = 0,
          position_lat: positionLat,
          position_long: positionLong,
        } = pos;

        // Accum speeds to get the total
        totalSpeed += enhancedSpeed;

        // Found larger speed, update speedMax
        if (enhancedSpeed > speedMax) {
          speedMax = enhancedSpeed;
        }

        // Found matching positions, toggle flag
        if (positionLat && positionLong) {
          hasGpsPositions = true;
        }
      }

      if (!hasGpsPositions) return false;

      const speedAverage = Math.round(totalSpeed / matchedPositions.length) || 0;

      // Map positions array to be sessions api compatible structure
      // or default to a basic positions array with the session start as the position
      const positions = matchedPositions
        .map(({ timestamp, position_lat: latitude, position_long: longitude }) => {
          /* istanbul ignore next */
          if (!longitude || !latitude) return null;
          return {
            timestamp,
            latitude,
            longitude,
          };
        })
        .filter(Boolean);

      // Generate and return the event object
      return {
        startTimestamp,
        endTimestamp,
        type,
        distance: totalDistance || 0,
        speedAverage,
        speedMax,
        positions,
      };
    })
    .filter(Boolean);

  if (!events.length) {
    newrelic.recordCustomEvent('Session Missing Positions', {
      userId: user,
      clientId,
    });
    return true;
  }
  const [lastEvent] = events.slice(-1) || [];

  // Ensure that the end Timestamp is greater than the last event timestamp
  const endTimestamp = Math.max(sessionEndTimestamp, lastEvent?.endTimestamp || 0);

  /* istanbul ignore next */
  // Get the max of speedMax from events
  const { speedMax: speedMaxSession } = events.reduce((prev, current) =>
    prev.speedMax > current.speedMax ? prev : current,
  );

  /* istanbul ignore next */
  // Get the max of distance from events
  const { distance: distanceLongestWave } = events.reduce((prev, current) =>
    prev.distance > current.distance ? prev : current,
  );

  // Get the total of distance from events
  const distanceWaves = events.reduce((accum, current) => accum + current.distance, 0);

  const session = {
    startTimestamp: sessionStartTimestamp,
    endTimestamp,
    visibility: 'PUBLIC',
    waveCount: hasSplits ? events.length : 0,
    speedMax: speedMaxSession,
    distancePaddled: 0,
    deviceType: watchVersion || 'Garmin',
    distanceLongestWave,
    distanceWaves: distanceWaves || 0,
    events,
    user,
    clientId,
  };

  // If we are debugging, then don't actually post it to the DB
  /* istanbul ignore next */
  if (debug || dryRun) {
    console.log(session);
    return session;
  }

  try {
    const result = await postSession(session);
    return result;
  } catch (error) {
    console.log('There was an error posting the session:');
    console.log(error);
    const { message } = error;
    newrelic.noticeError(error, {
      message,
      session,
    });
    throw error;
  }
};

export default processFitFileHandler;
