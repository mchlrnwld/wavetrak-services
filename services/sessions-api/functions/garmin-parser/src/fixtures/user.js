export default {
  _id: '5aabfbc0ffd66500117e554d',
  firstName: 'firstName',
  lastName: 'lastName',
  email: 'test@surfline.com',
  usID: 766886,
  locale: 'en-US',
  brand: 'sl',
  modifiedDate: '2020-05-19T23:57:16.755Z',
  isActive: true,
  __v: 0,
  linkedClients: [
    {
      createdAt: '2019-05-20T20:31:10.391Z',
      clientId: '5c0ffa88ffda1e500b1bedc7',
      token: 'testDryRunToken',
    },
    {
      createdAt: '2019-08-07T22:53:59.644Z',
      clientId: '5af1ce73b5acf7c6dd2592ee',
      token: 'testToken',
    },
  ],
  linkedAccount: { facebook: { id: 'xxx' } },
  timezone: 'America/Los_Angeles',
};
