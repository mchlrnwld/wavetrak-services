export default {
  userApi: process.env.USER_SERVICE,
  sessionsApi: process.env.SESSIONS_API,
  GARMIN_CONSUMER_KEY: process.env.GARMIN_CONSUMER_KEY,
  GARMIN_CONSUMER_SECRET: process.env.GARMIN_CONSUMER_SECRET,
};
