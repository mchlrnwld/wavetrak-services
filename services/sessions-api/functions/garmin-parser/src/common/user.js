/* istanbul ignore file */

import axios from 'axios';
import config from '../config';
import userFixture from '../fixtures/user';
/**
 * @param {string} accessToken
 * @returns {object} user
 * @description This function fetches the user details for an accessToken
 */
export const getUserByClientAccessToken = async (accessToken, dryRun) => {
  try {
    if (dryRun) return userFixture;
    const { data } = await axios.get(`${config.userApi}/user?clientToken=${accessToken}`);
    return data;
  } catch (err) {
    console.log(`err in getUserByClientAccessToken for ${accessToken}`, err);
    throw err;
  }
};
