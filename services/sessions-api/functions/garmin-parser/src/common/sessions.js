/* istanbul ignore file */
import axios from 'axios';
import config from '../config';

export const postSession = async (session) => {
  const headers = {
    'x-auth-userid': session.user,
    'x-api-key': session.clientId,
  };
  const { data } = await axios.post(`${config.sessionsApi}/sessions`, { ...session }, { headers });
  return data;
};
