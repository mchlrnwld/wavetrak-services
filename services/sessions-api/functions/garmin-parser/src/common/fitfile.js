/* istanbul ignore file */

import axios from 'axios';
import fs from 'fs';
import newrelic from 'newrelic';
import jp from 'jsonpath';
import FitParser from '@surfline/fit-parser';

/**
 * @param {string} callbackURL
 * @param {{Authorization: string}} oauthHeaders
 * @param {boolean} [debug=false]
 * @returns Fit File output from Garmin
 * @description - This function pulls the Garmin FIT file from the callbackURL specified
 */
export const getFitFile = async (callbackURL, oauthHeaders, debug = false, dryRun = false) => {
  try {
    const options = {
      method: 'get',
      responseType: 'arraybuffer',
      headers: {
        ...(dryRun ? undefined : oauthHeaders),
      },
    };
    const { data } = await axios.get(callbackURL, options);
    if (debug && !dryRun) {
      fs.writeFileSync('./fit-file.fit', data);
    }
    return data;
  } catch (err) {
    console.log(`err in getFitFile for ${callbackURL}`, err);
    throw err;
  }
};

/**
 * @param Fit File content
 * @returns Parsed JSON FIT file output
 * @description - This function parses the data using FitParser package and returns the JSON data
 */
export const parseFitFile = async (content) => {
  try {
    const fitFileOptions = {
      force: true,
      speedUnit: 'm/s',
      lengthUnit: 'm',
      temperatureUnit: 'celsius',
      elapsedRecordField: true,
      mode: 'list',
    };
    const fitParser = new FitParser(fitFileOptions);
    const result = fitParser.parse(content);
    return result;
  } catch (err) {
    console.log('Error parsing fit file:', err);
    newrelic.noticeError(err);
    throw err;
  }
};

/**
 * @param {object} object
 * @param {string} query
 * @returns {object} matches
 * @description - This function queries the root {object} using jsonpath package to find matching results based on the query provided.
 */
export const queryFitFileOutput = (object, query) => {
  try {
    const matches = jp.query(object, query);
    return matches;
  } catch (err) {
    console.log('err in queryFitFileOutput', err);
    throw err;
  }
};
