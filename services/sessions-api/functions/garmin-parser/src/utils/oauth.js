/* istanbul ignore file */

import crypto from 'crypto';
import OAuth from 'oauth-1.0a';
import config from '../config';

const oauth = OAuth({
  consumer: { key: config.GARMIN_CONSUMER_KEY, secret: config.GARMIN_CONSUMER_SECRET },
  signature_method: 'HMAC-SHA1',
  hash_function(baseString, key) {
    return crypto.createHmac('sha1', key).update(baseString).digest('base64');
  },
});

/**
 * @description Util function to create the OAuth 1.0a request signing header.
 * @param {string} userAccessToken
 * @param {string} tokenSecret
 * @param {string} callbackURL
 * @returns {{Authorization: string}}
 */
export const createOauthHeaders = (userAccessToken, tokenSecret, callbackURL) => {
  const token = {
    key: userAccessToken,
    secret: tokenSecret,
  };

  const requestData = {
    url: callbackURL,
    method: 'GET',
  };

  return oauth.toHeader(oauth.authorize(requestData, token));
};
