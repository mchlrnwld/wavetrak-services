# Garmin Parser Lambda Function

## Table of contents <!-- omit in toc -->

- [Summary](#summary)
- [Development](#development)
  - [Installation](#installation)
  - [Unit tests and linting](#unit-tests-and-linting)
  - [Integration test](#integration-test)
    - [Request](#request)
    - [Response](#response)
- [Monitoring](#monitoring)
  - [Configuration](#configuration)
  - [Triggering the Alert](#triggering-the-alert)
- [Jenkins deployment](#jenkins-deployment)

## Summary

A function which will parse the Garmin FIT files and transforming them into Surfline Sessions compatible format.

This lambda function does the following:

- Pulls the FIT file from Garmin.
- Parses it to JSON output using `fit-parser` library.
- Transforms the data to Sessions API compatible contract.
- POSTs the sessions to /sessions API.

## Development

### Installation
you can do a dry run using a sample fit file we have stored on s3 by copying the dry run event.json to event.json

```bash
cp dryRunEvent.sample.json event.json
```

otherwise when you run `startlocal` it will create an event.json where you can put in your own details for the lambda payload

```bash
cp .env.sample .env
npm install
npm run startlocal
```

### Unit tests and linting

```bash
npm run test
npm run lint
```

### Integration test

The next call to the lambda function should be enough to test if we receive a successful response.

#### Request

```bash
export AWS_PROFILE=surfline-dev
aws lambda invoke \
    --function-name wt-sessions-api-garmin-parser-sandbox \
    --payload "{}" \
    ../output.json
```
#### DryRun

```bash
bash dryRun.sh
```
#### Response

```json
{
    "StatusCode": 200,
    "ExecutedVersion": "$LATEST"
}
```

The output file generated should contain a `null` string, even if the execution is successful or not. So a good approach would be to browse the [CloudWatch logs for the lambda function](https://us-west-1.console.aws.amazon.com/cloudwatch/home?region=us-west-1#logStream:group=%252Faws%252Flambda%252Fwt-sessions-api-garmin-parser-sandbox) (sandbox) and read the log responses.

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMjQ4MzI5IiwiY29uZGl0aW9uSWQiOiIxNTkyODYzNCJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ==&platform[accountId]=356245) that
monitors the Lambda invocation error count.

### Configuration

Use your judgment when adjusting the thresholds.

**Note:** Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.

### Triggering the Alert

If you need to trigger the alert for testing purposes, the fastest way is to temporarily substitute a bad value in one of the mission-critical Lambda environment variables. **Please do this with caution, and communicate your actions to the code owners.** To do this, you'll have to:

1. Update the variable's value in AWS Parameter Store.
2. Redeploy the Lambda.
3. Use the AWS console to configure a Lambda test event that contains any valid json object. Invoke that event repeatedly using the "Test" button until you reach the error threshold configured in the policy. After the offset window has elapsed, New Relic will trigger the alert.
4. After you are done, be sure to change the variable back to its correct value.

## Jenkins deployment

To deploy this application as a lambda function, launch a [Jenkins job](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-sessions-api-lambda-functions-parameter-store/), in the desired environment.

Once the job is completed, you can check the `lambda-functions/sessions-api-garmin-parser/wt-sessions-api-garmin-parser.zip` file under the following S3 buckets:

| staging              | sandbox          | prod              |
| -------------------- | ---------------- | ----------------- |
| sl-artifacts-staging | sl-artifacts-dev | sl-artifacts-prod |
