#!/bin/bash
export AWS_PROFILE=surfline-dev
for i in {1..5}
do
    aws lambda invoke \
        --function-name wt-sessions-api-garmin-parser-staging \
        --cli-binary-format raw-in-base64-out \
        --payload '{"dryRun": true, "callbackURL": "https://wt-dev-fixtures.s3.us-west-1.amazonaws.com/fit-file.fit"}' \
        response.json >> ./success.json
done
