# API Endpoints

## Root URLs
* Service URL: http://sessions-service.prod.surfline.com
* Public URL: https://services.surfline.com/sessions

## Create A Session

Path: `POST /sessions`

Authentication:  Endpoint requires authentication via `X-Auth-AccessToken` header. 
Api Key: Endpoint requires an `X-API-Key` header to validate that the client sending the request.

Endpoint used to create a session for a given user  (identified by bearer token).

#### Request Parameters

| Property            | Type    | Required | Description                                                                                 |
| ------------------- | ------- | -------- | ------------------------------------------------------------------------------------------- |
| startTimestamp      | Integer | Yes      | Unix timestamp, session start time.                                                         |
| endTimestamp        | Integer | Yes      | Unix timestamp, session end time.                                                           |
| visibility          | Enum    | Yes      | Denotes whether this session should be publically accessible or not. `PUBLIC` or `PRIVATE`. |
| waveCount           | Integer | Yes      | Number of waves caught.                                                                     |
| speedMax            | Float   | Yes      | Maximum speed on waves (meters per second).                                                 |
| distancePaddled     | Float   | Yes      | Distance paddled (meters).                                                                  |
| distanceWaves       | Float   | Yes      | Distance surfed (meters).                                                                   |
| distanceLongestWave | Float   | Yes      | Distance of longest wave surfed (meters).                                                   |
| deviceType          | Enum    | Yes      | Identifier for this device make/model, no set standard by us but should be consistent for                                                 all devices of the same make/model.                                                         |
| calories            | Integer | No       | Calories burned during the session.                                                         |
| events              | Array   | Yes      | Array of Event objects (see Linked Objects).                                                |

### Response Object Attributes

| Property         | Type    | Description                                                |
| ---------------- | ------- | ---------------------------------------------------------- |
| success          | Bool    | true/false. See Error Handling.                            |
| _id              | Integer | ID of session created on Surfline's side.                  |
| message          | Integer | Message related to status of api response                  |

#### Example Response Object

```json
{
  "success": true,
  "_id": "588112ea54b96f4c450766b2",
  "message": "Session added successfully"
}
```

## Create A Session From The GPS Endpoint

Path: `POST /sessions/gps`

Authentication:  Endpoint requires authentication via `X-Auth-AccessToken` header. 

Endpoint used to create a session for a given user from the 1st party watch app (identified by bearer token).

#### Request Body

| Property            | Type    | Required | Description                                      |
| ------------------- | ------- | -------- | ------------------------------------------------ |
| positions           | Array   | Yes      | Array of positions objects (see Linked Objects). |                                    

### Response Object Attributes

| Property         | Type    | Description                                                |
| ---------------- | ------- | ---------------------------------------------------------- |
| success          | Bool    | true/false. See Error Handling.                            |
| _id              | Integer | ID of session created on Surfline's side.                  |
| message          | Integer | Message related to status of api response                  |

#### Example Response Object

```json
{
  "success": true,
  "_id": "588112ea54b96f4c450766b2",
  "message": "Session added successfully"
}
```

## Session Summary
Path: `GET /sessions/summary/?start=1534268699&end=1534268699`

Summary endpoint will provide high level stats for all sessions between the two timestamps. This can be used for to see a users history over the X date range. Like number of waves in July.

#### Request Parameters
| Property            | Type             | Required | Description                                              |
|---------------------|------------------|----------|----------------------------------------------------------|
| start               | Integer          | Yes       | Unix timestamp, first session to include in the summary |
| end                 | Integer          | Yes       | Unix timestamp, last session to include in the summary  |

### Response Object Attributes
| Property            | Type             | Nullable | Description                                               |
|---------------------|------------------|----------|-----------------------------------------------------------|
| sessionCount        | Integer          | No       | The number of sessions in the timespan                    |
| timeSpentSurfing    | Integer          | No       | The amount of time logged in surf sessions in the timespan|
| waveCount           | Integer          | No       | The number of waves caught in the timespan                |

#### Example Response Object
```json
{
  "sessionCount": 4,
  "timeSpentSurfing": 5443100,
  "waveCount": 100
}
```

## Sessions Feed for a User

Path: `GET /sessions/feed?limit=50&offset=101`

Response returns list of session objects for the logged in user order by date (desc). This endpoint should return no session objects if there is no access token passed i.e. there's no public sessions. It's possible sessions will be returned that are not yet enrinched with KBYG and rewind data.

User authentication happens like it does for other services i.e. requesting client must pass the `X-Auth-AccessToken` header.

_Note: If you are using the entitlements service from the services proxy, these query params are populated automatically via the user's access token. Access tokens can be passed either via the header value `X-Auth-AccessToken` or query param `accesstoken`._

### Request Parameters
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| limit               | Integer          | No       | Limit the max number of objects to be returned. Omitting results in the a default of 50. |
| offset              | Integer          | No       | Start position parameter for the next page.  If it not passed it is assumed to be 0 and indicates the first page should be returned. |

### Response Object Attributes
#### Associated Units Object Attributes
| Property            | Type             | Nullable | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| surfHeight      | String          | No      | Wave height unit i.e. FT, M  |
| windSpeed      | String          | No      | Wind speed unit i.e. KTS, MPH, KPH |
| tideHeight      | String          | No      | Tide height unit i.e. FT, M  |
| distance  | String | No | Distance unit that can be used for wave length i.e. FT |
| speed | String | No | Speed unit that can be used for wave speed i.e. MPH, KPH |

#### Associated Timezone Array Object Attributes
| Property | Type  | Nullable | Description |
|----------|-------|----------|-------------|
| key      | String          | No      | Long-form timezone string (ex: America/Los_Angeles)                |
| abbr      | String          | No      | 3-Char abbreviation of the timezone (ex PST)              |
| offset      | Integer          | No      | Numeric hour offset from GMT            |
| id      | Integer          | No      | Incremented number indicating the lookup associated tiemzone lookup value                |
| path      | String          | No      | associated.timezone |

#### Limit & Offset
* Limit and offset passed in will be returned on the top level of the response for reference.

#### Session Object Attributes

Conditions object is determined using the nearest forecasted data to the start of the session time.

| Property            | Type             | Nullable | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| id | String | No | Session object ID. |
| name | String | No | Name of the session. |
| startTimestamp | Integer | No | Unix timestamp, session start timestamp. |
| endTimestamp | Integer | No | Unix timestamp, session end timestamp. |
|status|String/Enum|No|Status of the session can be `NEW` or `ENRICHED`. If `NEW` the sessions only just been recieved, a detail view and no KBYG data is available yet. If `ENRICHED` the session detail endpoint will not return a 404 and we might have `conditions.*` data if the `spot.*` isn't `null`.|
| clips | Object | No | Object containing clip metadata. (Should this be nullable?  Or just put 0 for values when no clips?) |
| clips.total | Integer | No | Total clips expected to be created. 0 if the user didn't surf in front of a cam. |
| clips.available | Integer | No | Clips we have ready for the user to watch. 0 if the user didn't surf infront of a cam. |
| stats | Object | No | Contains high level stats about the session. |
| stats.waveCount | Integer | No | Number of waves caught in the session, defaults to 0 |
| stats.speedMax | Float | Yes | Max speed attained in wave, will be null if no waves surfed. |
| stats.distancePaddled | Float | Yes | Total distance paddled. Can be null if integration things the user hasn't paddled anywhere or that integration doesn't report this.|
| stats.distanceWaves | Float | Yes | Total distance surfed (sum of all wave distances). Null if no waves surfed.|
| stats.calories | Integer | Yes | Calories burned whilst surfing. Can be null if device doesn't provide this.|
| stats.wavesPerHour | Integer | Yes |Average waves caught per hour surfed. Can be null if no waves caught.|
| stats.longestWave.distance | Float | Yes | Distance of the longest was caught in the session. Null if no waves caught. |
| stats.longestWave.seconds | Integer | Yes | Time of the longest wave in seconds. Null if no waves caught. |
| rating | Integer | Yes | Users rating of the session. null means the user hasn't rated it yet. |
| thumbnail | Object | No | Object describing thumbnail from the cam based off the first wave with a clip. If no waves with thumbnail URLs then should return a URL to the tracks image (satellite map with rides). This image represents the session. |
| thumbnail.url | String | No | Path to thumbnail file with `{size}` template param included, replace this with one of the sizes in the array to access the the image. |
| thumbnail.sizes | Array | No | List of sizes the thumbnail has available, to replace `{size}` in `thumbnail.url`. |
| waves | Array | No | Array of wave objects, will be empty if no waves caught. |
| waves[n].id| String |No| Unique wave id (Mongo) |
| waves[n].startTimestamp|Integer|No|Start timestamp (UNIX) of the wave event.|
| waves[n].endTimestamp|Integer|No|End timestamp (UNIX) of the wave event.|
| waves[n].favorite | Boolean | No | Default false |
| waves[n].outcome | String/Enum | No | Either one of 'ATTEMPTED' or 'CAUGHT' |
| waves[n].distance | Float | No | Distance of the wave event. |
| waves[n].speedMax | Float | No | Maximum speed obtained during wave event. |
| waves[n].trackImage | Object | No | Info related to image map of where the wave took place.|
| waves[n].trackImage.url|String|No|Image map of where the wave took place.|
| waves[n].trackImage.sizes|String|No| List of sizes the track image has available. |
| waves[n].clips|Array|Yes|Will contain objects with details of the clips available for this wave event. Will be null if no cam is available to create the clip i.e. user surfed at spot with no cam.|
| waves[n].clips[m].clipUrl|String|No|Path to .mp4 clip file.|
| waves[n].clips[m].cameraId|String|No|The camera angle ID used to generate this clip.|
| waves[n].clips[m].startTimestamp|Integer|No|Start timestamp in ms (UNIX) of the clip.|
| waves[n].clips[m].endTimestamp|Integer|No|End timestamp in ms (UNIX) of the clip.|
| waves[n].clips[m].clipId|String|No|The clip ID used to reference this clip.|
| waves[n].clips[m].thumbnail|Object|Yes|Object detailing a URL to the thumbnail image that represents this clip to this clip file. Will be null if no clip is available yet.|
| waves[n].clips[m].thumbnail.url|String|No|Path to thumbnail file with `{size}` template param included, replace this with one of the sizes in the array to access the the image.|
| waves[n].clips[m].thumbnail.sizes|Array|No|List of sizes the thumbnail has available.|
| waves[n].clips[m].status|String/Enum|No|Availablity status of the clip, see Clip Status Enum below.|
| spot | Object | Yes | This contains the attributes about the spot based on either the `originalSpotId` if available and no `userSpotId` else `userSpotId` if not null. |
| spot.id|String|Yes|ID of the spot for this session, null if not found.|
| spot.name | String | No | Name of the spot. |
| spot.location | Object | No | Object containing the location info for the spot. |
| spot.location.type | String | No | The type of spot location (Eg: POINT). |
| spot.location.coordinates | Array[<Number>] | No | Coordinate array ([lon, lat]) |
| cams | Array | No | Array of cameras that are part of the session. |
| cams[n].title|String|No|Camera name.|
| cams[n].alias|String|No|Camera alias.|
| cams[n].stillUrl|String|No|Url of latest camera still.|
| cams[n].isPremium|Boolen|No|Indicates if this is a Premium only cam.|
| cams[n].visible|Boolen|No|Indicates if cam is currently visible.|
| cams[n].id|String|No|id of the camera in MongoDB.|
| conditions | Object | Yes | Will be null if no conditions exist for this spot (or there's no spot). |
| conditions.waveHeight.min | Float | No | Minimum wave height for the session. |
| conditions.waveHeight.max | Float | No | Maximum wave height for the session. |
| conditions.wind.speed | Float | No | Wind speed for the session. |
| conditions.wind.directionCardinal | String | No | Cardinal wind direction i.e. SSE. |
| conditions.conditions.human | Bool | No | Is the reported wave heigth and conditions value from a human report? |
| conditions.conditions.value | Enum | No | Conditions enum i.e. POOR_TO_FAIR |
| conditions.tide.direction | Enum | Yes | Tide direction enum RISING or FALLING to denote whether the tide is going from low to high (RISING) or high to low (FALLING). Not all spots have tide, if no tide then null. |
| conditions.tide.height | Enum | Yes | Tide height at the session start time. Not all spots have tide, if no tide then null. |
| notified | Bool | Yes | Flag to indicate if the Session notification has been sent or not. Default to false. |

### Example Response Object
```json
{
  "associated": {
    "units" : {
      "surfHeight": "FT",
      "windSpeed": "KTS",
      "tideHeight": "FT",
      "distance": "FT",
      "speed": "MPH"
    },
    "timezone": [
      {
        "key": "America/Los_Angeles",
        "abbr": "PST",
        "offset": -7,
        "id": 1,
        "path": "associated.timezone"
      }
    ]
  },
  "limit": 50,
  "offset": 101,
  "sessions": [{
    "id": "584204204e65abc5a770772a",
    "name": "Morning Session",
    "timezone": {
      "id": 1,
      "path" : "associated.timezone"
    },
    "startTimestamp": 1488878277,
    "endTimestamp": 1488909066,
    "STATUS": "ENRICHED",
    "clips": {
      "total": 32,
      "available": 15
    },
    "stats": {
      "waveCount": 8,
      "speedMax": 21.0,
      "distancePaddled": 1.2,
      "distanceWaves": 4.5,
      "calories": 365,
      "wavesPerHour": 4,
      "longestWave": {
        "distance": 301,
        "seconds": 17000
      }
    },
    "rating": 4,
    "thumbnail": {
      "url": "https://maps.tilehosting.com/c/surfline/styles/surfline/static/-117.5884311499704,33.38143999849023,14/{size}.png?key=surflinePrivateTilehostingKey",
      "sizes": ["375x210"]
    },
    "waves": [
      {
        "id": "584772704e65abc5a770772a",
        "startTimestamp": 1488878277,
        "endTimestamp": 1488878822,
        "distance": 544310,
        "speedMax": 8.56,
        "outcome": "CAUGHT",
        "favorite": false,
        "trackImage": {
          "url": "https://maps.tilehosting.com/c/surfline/styles/surfline/static/-117.5884311499704,33.38143999849023,14/{size}.png?key=surflinePrivateTilehostingKey",
          "sizes": [
            "375x210"
          ]
        },
        "clips": [
          {
            "clipUrl": "https://camclips.staging.surfline.com/5978cb2518fd6daf0da062ae/5bbe75038f8452607846e3d7.mp4",
            "cameraId": "5978cb2518fd6daf0da062ae",
            "startTimestamp": 1539096907000,
            "endTimestamp": 1539097027000,
            "thumbnail": {
              "url": "https://camclips.staging.surfline.com/5978cb2518fd6daf0da062ae/5bbe75038f8452607846e3d7_{size}.jpg",
              "sizes": [
                "300",
                "640",
                "1500",
                "3000"
              ]
            },
            "status": "CLIP_AVAILABLE",
            "clipId": "5bbe75038f8452607846e3d7"
          }
        ]
      }
    ],
    "spot": {
      "id": "584204204e65abc5a775520b",
      "name": "Lower Trestles",
      "location": {
        "type": "POINT",
        "coordinates": [33, -110]
      }
    },
    "cams": [
      {
        "title": "SOCAL - 17th Street",
        "alias": "wc-seventeenthst",
        "stillUrl": "https://camstills.cdn-surfline.com/wc-seventeenthst/latest_small.jpg",
        "isPremium": false,
        "visible": true,
        "id": "58a37701c9d273fd4f581bf0"
      }
    ],
    "conditions": {
      "waveHeight": {
        "min": 8.0,
        "max": 10.0
      },
      "wind": {
        "speed": 2.09,
        "directionCardinal": "SSE"
      },
      "conditions": {
        "human": true,
        "value": "POOR"
      },
      "tide": {
        "direction": "RISING",
        "height": 3.6
      }
    },
  }]
}
```

## Get All Detailed Session Object

Path: `GET /sessions/:id/details`

Returns all the data required to render a detailed view of the session. Endpoint requires authentication via `X-Auth-AccessToken` header and will verify this user owns this session object or will return an authentication error if the access token is invalid and forbidden if the access token isn't allowed to access this object.

Will return a 404 for sessions without enriched data.

### Request Parameters
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| id              | String          | Yes       |ID of the session object to get the data for.|

### Response Object Attributes

Conditions object is determined using the nearest forecasted data to the start of the session time.

| Property | Type | Nullable | Description |
|----------|------|----------|-------------|
| associated | Object | No | Object containing units and timezone related information |
| associated.timezone| Array |No| Array of objects indicating all applicable timezones for the current response (see Associated Timezone Array Object Attributes) |
| associated.timezone[n].key | String | No | Long-form timezone string (ex: America/Los_Angeles) |
| associated.timezone[n].abbr | String | No | 3-Char abbreviation of the timezone (ex PST) |
| associated.timezone[n].offset | Integer | No | Numeric hour offset from GMT |
| associated.timezone[n].id | Integer | No | Incremented number indicating the lookup associated tiemzone lookup value |
| associated.timezone[n].path | String | No | associated.timezone |
| associated.units | Object | No | Object containing all relevant unit values for current response |
| associated.units.surfHeight | String | No | Wave height unit i.e. FT, m |
| associated.units.windSpeed | String | No | Wind speed unit i.e. KTS, MPH |
| associated.units.tideHeight | String | No | Tide height unit i.e. FT, m |
| associated.units.distance | String | No | Distance unit that can be used for wave length i.e. FT |
| associated.units.speed | String | No | Speed unit that can be used for wave speed i.e. MPH |
| associated.units.tide | String | No | Unit used for tide height i.e. ft, m |
| id | String | No | Session object ID. |
| name | String | No | Name of the session. |
| startTimestamp | Integer | No | Unix timestamp, session start timestamp. |
| endTimestamp | Integer | No | Unix timestamp, session end timestamp. |
|status|String/Enum|No|Status of the session can be `NEW` or `ENRICHED`. If `NEW` the sessions only just been recieved, a detail view and no KBYG data is available yet. If `ENRICHED` the session detail endpoint will not return a 404 and we might have `conditions.*` data if the `spot.*` isn't `null`.|
| clips | Object | No | Object containing clip metadata. (Should this be nullable?  Or just put 0 for values when no clips?) |
| clips.total | Integer | No | Total clips expected to be created. 0 if the user didn't surf in front of a cam. |
| clips.available | Integer | No | Clips we have ready for the user to watch. 0 if the user didn't surf infront of a cam. |
| stats | Object | No | Contains high level stats about the session. |
| stats.waveCount | Integer | No | Number of waves caught in the session, defaults to 0 |
| stats.speedMax | Float | Yes | Max speed attained in wave, will be null if no waves surfed. |
| stats.distancePaddled | Float | Yes | Total distance paddled. Can be null if integration things the user hasn't paddled anywhere or that integration doesn't report this.|
| stats.distanceWaves | Float | Yes | Total distance surfed (sum of all wave distances). Null if no waves surfed.|
| stats.calories | Integer | Yes | Calories burned whilst surfing. Can be null if device doesn't provide this.|
| stats.wavesPerHour | Integer | Yes | Average waves caught per hour surfed. Can be null if no waves caught.|
| stats.longestWave.distance | Float | Yes | Distance of the longest was caught in the session. Null if no waves caught. |
| stats.longestWave.seconds | Integer | Yes | Time of the longest wave in seconds. Null if no waves caught. |
| rating | Integer | Yes | Users rating of the session. null means the user hasn't rated it yet. |
| thumbnail | Object | No | Object describing thumbnail from the cam based off the first wave with a clip. If no waves with thumbnail URLs then should return a URL to the tracks image (satellite map with rides). This image represents the session. |
| thumbnail.url | String | No | Path to thumbnail file with `{size}` template param included, replace this with one of the sizes in the array to access the the image. |
| thumbnail.sizes | Array | No | List of sizes the thumbnail has available. |
| waves | Array | No | Array of wave objects, will be empty if no waves caught. |
| waves[n].id | ObjectId | Yes | The Object ID used for querying the specific event. |
| waves[n].startTimestamp|Integer|No|Start timestamp (UNIX) of the wave event.|
| waves[n].endTimestamp|Integer|No|End timestamp (UNIX) of the wave event.|
| waves[n].favorite | Boolean | No | Default false |
| waves[n].outcome | String/Enum | No | Either one of 'ATTEMPTED' or 'CAUGHT' |
| waves[n].distance | Float | No | Distance of the wave event. |
| waves[n].speedMax | Float | No | Maximum speed obtained during wave event. |
| waves[n].trackImage | Object | No | Info related to image map of where the wave took place.|
| waves[n].trackImage.url|String|No|Image map of where the wave took place.|
| waves[n].trackImage.sizes|String|No| List of sizes the track image has available. |
| waves[n].clips|Array|Yes|Will contain objects with details of the clips available for this wave event. Will be null if no cam is available to create the clip i.e. user surfed at spot with no cam.|
| waves[n].clips[m].clipUrl|String|No|Path to .mp4 clip file.|
| waves[n].clips[m].cameraId|String|No|The camera angle ID used to generate this clip.|
| waves[n].clips[m].startTimestamp|Integer|No|Start timestamp in ms (UNIX) of the clip.|
| waves[n].clips[m].endTimestamp|Integer|No|End timestamp in ms (UNIX) of the clip.|
| waves[n].clips[m].clipId|String|No|The clip ID used to reference this clip.|
| waves[n].clips[m].thumbnail|Object|Yes|Object detailing a URL to the thumbnail image that represents this clip to this clip file. Will be null if no clip is available yet.|
| waves[n].clips[m].thumbnail.url|String|No|Path to thumbnail file with `{size}` template param included, replace this with one of the sizes in the array to access the the image.|
| waves[n].clips[m].thumbnail.sizes|Array|No|List of sizes the thumbnail has available.|
| waves[n].clips[m].status|String/Enum|No|Availablity status of the clip, see Clip Status Enum below.|
| spot | Object | Yes | This contains the attributes about the spot based on either the `originalSpotId` if available and no `userSpotId` else `userSpotId` if not null. |
| spot.id|String|Yes|ID of the spot for this session, null if not available.|
| spot.name | String | No | Name of the spot. |
| spot.location | Object | No | Object containing the location info for the spot. |
| spot.location.type | String | No | The type of spot location (Eg: POINT). |
| spot.location.coordinates | Array[<Number>] | No | Coordinate array ([lon, lat]) |
| cams | Array | No | Array of cameras that are part of the session. |
| cams[n].title|String|No|Camera name.|
| cams[n].alias|String|No|Camera alias.|
| cams[n].stillUrl|String|No|Url of latest camera still.|
| cams[n].isPremium|Boolen|No|Indicates if this is a Premium only cam.|
| cams[n].visible|Boolen|No|Indicates if cam is currently visible.|
| cams[n].id|String|No|id of the camera in MongoDB.|
| conditions | Object | Yes | Will be null if no conditions exist for this spot (or there's no spot). |
| conditions.waveHeight.min | Float | No | Minimum wave height for the session. |
| conditions.waveHeight.max | Float | No | Maximum wave height for the session. |
| conditions.wind.speed | Float | No | Wind speed for the session. |
| conditions.wind.directionCardinal | String | No | Cardinal wind direction i.e. SSE. |
| conditions.conditions.human | Bool | No | Is the reported wave heigth and conditions value from a human report? |
| conditions.conditions.value | Enum | No | Conditions enum i.e. POOR_TO_FAIR |
| conditions.tide.direction | Enum | Yes | Tide direction enum RISING or FALLING to denote whether the tide is going from low to high (RISING) or high to low (FALLING). Not all spots have tide, if no tide then null. |
| conditions.tide.height | Enum | Yes | Tide height at the session start time. Not all spots have tide, if no tide then null. |
| notified | Bool | Yes | Flag to indicate if the Session notification has been sent or not. Default to false. |


#### Clip Status Enum
Enum specifies the availability of a rewind clip.

1. `CLIP_NOT_AVAILABLE` - Clip not available, spot with cam, but cam is down.
1. `CLIP_OUT_OF_RANGE` - Session sync'd too late and no rewind source clips were available to generate this clip.
1. `CLIP_PENDING` - Clip pending, spot with cam.
1. `CLIP_TOO_LONG` - Requested clip is longer than 5 minutes.
1. `CLIP_AVAILABLE` - Rewind available.
1. `CLIP_RETRY_TIMEOUT` - Timed out trying to regenerate the clip
1. `FATAL_ERROR` - There was an unrecoverable error during clip processing.

### Example Response Object
```json
{
  "associated": {
    "units": {
      "surfHeight": "FT",
      "windSpeed": "KTS",
      "tideHeight": "FT",
      "distance": "FT",
      "speed": "MPH"
    },
    "timezone": [
      {
        "key": "America/Los_Angeles",
        "abbr": "PST",
        "offset": -7,
        "id": 1,
        "path": "associated.timezone"
      }
    ]
  },
  "id": "584204204e65abc5a770772a",
  "name": "Morning Session",
  "timezone": {
    "id": 1,
    "path": "associated.timezone"
  },
  "startTimestamp": 1488878277,
  "endTimestamp": 1488909066,
  "STATUS": "ENRICHED",
  "clips": {
    "total": 32,
    "available": 15
  },
  "stats": {
    "waveCount": 8,
    "speedMax": 21,
    "distancePaddled": 1.2,
    "distanceWaves": 4.5,
    "calories": 365,
    "wavesPerHour": 4,
    "longestWave": {
      "distance": 301,
      "seconds": 17000
    }
  },
  "rating": 4,
  "thumbnail": {
    "url": "https://maps.tilehosting.com/c/surfline/styles/surfline/static/-117.5884311499704,33.38143999849023,14/{size}.png?key=surflinePrivateTilehostingKey",
    "sizes": [
      "375x210"
    ]
  },
  "waves": [
    {
      "id": "584772704e65abc5a770772a",
      "startTimestamp": 1488878277,
      "endTimestamp": 1488878822,
      "distance": 544310,
      "speedMax": 8.56,
      "outcome": "CAUGHT",
      "favorite": false,
      "trackImage": {
        "url": "https://maps.tilehosting.com/c/surfline/styles/surfline/static/-117.5884311499704,33.38143999849023,14/{size}.png?key=surflinePrivateTilehostingKey",
        "sizes": [
          "375x210"
        ]
      },
      "clips": [
        {
          "clipUrl": "https://camclips.staging.surfline.com/5978cb2518fd6daf0da062ae/5bbe75038f8452607846e3d7.mp4",
          "cameraId": "5978cb2518fd6daf0da062ae",
          "startTimestamp": 1539096907000,
          "endTimestamp": 1539097027000,
          "thumbnail": {
            "url": "https://camclips.staging.surfline.com/5978cb2518fd6daf0da062ae/5bbe75038f8452607846e3d7_{size}.jpg",
            "sizes": [
              "300",
              "640",
              "1500",
              "3000"
            ]
          },
          "status": "CLIP_AVAILABLE",
          "clipId": "5bbe75038f8452607846e3d7"
        }
      ]
    }
  ],
  "spot": {
    "id": "5842041f4e65fad6a77088eb",
    "name": "17th St.",
    "location": {
      "coordinates": [
        -118.0143556189923,
        33.66315613358258
      ],
      "type": "Point"
    }
  },
  "cams": [
    {
      "title": "SOCAL - 17th Street",
      "alias": "wc-seventeenthst",
      "stillUrl": "https://camstills.cdn-surfline.com/wc-seventeenthst/latest_small.jpg",
      "isPremium": false,
      "visible": true,
      "id": "58a37701c9d273fd4f581bf0"
    }
  ],
  "conditions": {
    "waveHeight": {
      "min": 8,
      "max": 10
    },
    "wind": {
      "speed": 2.09,
      "directionCardinal": "SSE"
    },
    "conditions": {
      "human": true,
      "value": "POOR"
    },
    "tide": {
      "direction": "RISING",
      "height": 3.6
    }
  }
}
```

## Update a Session

Endpoint provide support for the client app to update the session object. Only pass the properties required for update. Changing the `userSpotId` will result in generating new cam clips (should the spot have a cam), this is an async process and the update will return before those clips are available.
Can also use this endpoint to append a spot report view to a session.

Note: `originalSpotId` cannot be updated in the `PUT`, it's only set once in the `POST` at initial creation.

Path: `PUT /sessions/:id/details`

### Request Parameters
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| id              | String          | Yes       |ID of the session object to get the data for.|

### Request Object
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
|id|String|Yes|ID of the session object to update the data for.|
| startTimestamp      | Integer          | No      | Unix timestamp, session start timestamp.                |
| endTimestamp        | Integer          | No      | Unix timestamp, session end timestamp.                  |
| visibility        | String/Enum          | No      | `PUBLIC` or `PRIVATE`                  |
| waveCount               | Integer | No      | Number of waves surfed in the session. If no waves 0.        |
| name | String | No | Name of the session. |
| userSpotId | String | No | Surfline spot ID, that the user is selecting over the current spot ID. |
| surfline| Object | No | Object to store Surfline internally calculated information |
| surfline.stats | Object | No | Object to store Surfline internally calculated stats information |
| surfline.stats.speedMax | Float | Yes | Maximum speed achieved in all waves surfed in preferred units. Null if no waves caught in session.         |
| surfline.stats.distancePaddled | Float | Yes | Total distance paddled. Can be null if integration things the user hasn't paddled anywhere or that integration doesn't report this. |
| surfline.stats.distanceWaves | Float | Yes | Total distance surfed (sum of all wave distances). Null if no waves surfed.|
| surfline.stats.longestWave | Object | Yes | Object representing longest waves distance and seconds. Null if no waves caught. |
| surfline.stats.longestWave.distance | Float | Yes | Distance of the longest was caught in the session.. Null if no waves caught. |
| surfline.stats.longestWave.seconds | Object | Yes | Number of seconds of the longest was caught in the session.. Null if no waves caught. |
|events | Array | No | `null` if no events are tracked. |
| events[n]._id | ObjectId | Yes | The Object ID used for querying the specific event. |
| events[n].startTimestamp | Integer | No | UTC timestamp when the event started. |
| events[n].endTimestamp | Integer | No | UTC timestamp when the event ended. |
| events[n].type | String/Enum | No | Key to detonate the event type: `PADDLE` - Used when the user is paddling. `WAVE` - Used when the user is actively surfing a wave. |
| events[n].distance | Float | No | Distance in *meters* of this event (sum of distance between each point). |
| events[n].speedMax | Float | No | Maximum speed the user traveled during this event in *meters per second*. |
| events[n].speedAverage | Float | No | Average speed the user traveled during this event in *meters per second*. |
| events[n].positions | Array | No | Array of Position objects that are included as part of this event. Position objects should be at a frequency no higher than 1 per second. |
| events[n].positions[m].timestamp | Integer | No | Unix timestamp in milliseconds when this position object occurred. |
| events[n].positions[m].latitude | Float | No | Coordinate latitude, degrees, decimal format. |
| events[n].positions[m].longitude | Float | No | Coordinate longitude, degrees, decimal format. |
| spotReportView | Object | No | The closest spot report view to the session time and location |
| spotReportView.name | String | Yes | Name of the spot |
| spotReportView.lat | Float | Yes | Coordinate latitude, degrees, decimal format. |
| spotReportView.lon | Float | Yes | Coordinate longitude, degrees, decimal format. |
| spotReportView.thumbnail | String | Yes | Thumbnail saved on the spot report view |
| spotReportView.timezone | String | Yes | timezone of the spot |
| spotReportView.offshoreDirection | Float | No | Offshore direction in degrees |
| spotReportView.legacyRegionId | Integer | No | Legacy Region Id |
| spotReportView.subregionId | ObjectId | No | The subregion Id for the spot |
| spotReportView.rank | Integer | No | The spot's rank with respect to other spots in the subregion |
| spotReportView.legacyId | Integer | No | CF id for the spot |
| spotReportView.waveHeight | Object | No | Spot Report View Wave Height |
| spotReportView.waveHeight.human | Float | No | Coorindate longitude, degrees, decimal format. |
| spotReportView.waveHeight.min | Float | No | Coorindate longitude, degrees, decimal format. |
| spotReportView.waveHeight.max | Float | No | Coorindate longitude, degrees, decimal format. |
| spotReportView.waveHeight.occasional | Float | No | Coorindate longitude, degrees, decimal format. |
| spotReportView.waveHeight.humanRelation | Float | No | Coorindate longitude, degrees, decimal format. |
| spotReportView.waveHeight.plus | Float | No | Coorindate longitude, degrees, decimal format. |
| spotReportView.swells | Array | No | Array of swell objects |
| spotReportView.swells[n].height | Float | Yes | Height of swell |
| spotReportView.swells[n].direction | Float | Yes | Direction of swell |
| spotReportView.swells[n].period | Float | Yes | Period of swell |
| spotReportView.weather | Object | No | Weather details |
| spotReportView.weather.temperature | Float | No | Air temperature |
| spotReportView.weather.condition | String | No | Weather conditions (precipitation) |
| spotReportView.wind | Object | No | Wind details |
| spotReportView.wind.speed | Float | Yes | Wind speed |
| spotReportView.wind.direction | Integer | Yes | Wind direction |
| spotReportView.tide | Object | No | Tide Object containing previous, current, and next tides |
| spotReportView.tide.previous | Object | No | Previous tide object |
| spotReportView.tide.previous.type | String | Yes | Type of previous tide, typically 'HIGH' or 'LOW' |
| spotReportView.tide.previous.height | Float | Yes | Height of previous tide |
| spotReportView.tide.previous.timestamp | Integer | Yes | Timestamp of previous tide |
| spotReportView.tide.previous.utcOffset | Integer | Yes | UTC offset of previous tide |
| spotReportView.tide.current | Object | No | Previous tide object |
| spotReportView.tide.current.type | String | Yes | Type of current tide, typically 'NORMAL' |
| spotReportView.tide.current.height | Float | Yes | Height of current tide |
| spotReportView.tide.current.timestamp | Integer | Yes | Timestamp of current tide |
| spotReportView.tide.current.utcOffset | Integer | Yes | UTC offset of current tide |
| spotReportView.tide.next | Object | No | Previous tide object |
| spotReportView.tide.next.type | String | Yes | Type of next tide, typically 'HIGH' or 'LOW' |
| spotReportView.tide.next.height | Float | Yes | Height of next tide |
| spotReportView.tide.next.timestamp | Integer | Yes | Timestamp of next tide |
| spotReportView.tide.next.utcOffset | Integer | Yes | UTC offset of next tide |
| spotReportView.note | String | No | Optional note from spot report |
| spotReportView.conditions | Object | No | Conditions Object |
| spotReportView.conditions.human | Boolean | No | |
| spotReportView.conditions.value | String | No | |
| spotReportView.conditions.sortableCondition | Integer | No | |
| spotReportView.waterTemp | Object | No | Water temperature |
| spotReportView.waterTemp.min | Float | Yes |  |
| spotReportView.waterTemp.max | Float | Yes |  |
| spotReportView.parentTaxonomy | Array of ObjectIds | Yes |  |



Path: `PATCH /sessions/:id/details`

#### Note: 

Patch endpoint is used to edit the rating, name, or user defined spot of the session. If the request contains a `userSpotId`, this service will re-invoke the session-enricher lambda-function in order to populate the session with new cam clips/spot details.

#### Re-Enriching Sessions

This endpoint is also used to initiate a reenrichment of the session.
The variables passsed in the body will not be stored with the session data (not in the white list of valid variables), but is simply used to trigger a re-enrichment of the session.  There are currently two values for this 'all' and 'dataOnly'.  'dataOnly' is used to re enrich only the stats and other data values, leaving the clips alone so that clips do not get lost when they become out of range (example: 5 days after session date). 'all' will force the entire session to be re-enriched (could be used for bad clip issues within the clip availability window) and will also update all data and stats for the session.

### Request Parameters
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| id              | String          | Yes       |ID of the session object to get the data for.|

### Request Object
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| id | String | Yes | ID of the session object to update the data for.|
| rating | Integer | No      | The user's rating of the session       |
| name | String | No | Name of the session. |
| userSpotId | String | No | Surfline spot ID, that the user is selecting over the current spot ID. |
| reEnrich | String | No | One of (all or dataOnly) |

On success, this endpoint returns a success message as detailed below.

### Response Object Attributes
| Property            | Type             |
|---------------------|------------------|
| success    | Boolean          |
| message         | String          |

#### Example Response Object
```json
{
  "success": true,
  "message": "Session details patched successfully."
}
```

## Change Cam Visibility for a session

Endpoint provide support for the user to set a camera a `visible: true` or `visible: false` for a given session, in case they do or do not want to see the clips from that cam for a given session

Path: `PUT /sessions/:sessionId/cams/:cameraId`

### Request Parameters
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| sessionId           | String           | Yes      |ID of the session object to get the data for.|
| cameraId            | String           | Yes      |ID of the camera object to switch visibility for.|

### Request Body
| Property            | Type             | Required | Description                                       |
|---------------------|------------------|----------|---------------------------------------------------|
| visible             | Boolean          | No       | Boolean for the cam visiblity (Default: false)    |


On success, this endpoint returns a success message as detailed below.

### Response Object Attributes
| Property            | Type             |
|---------------------|------------------|
| success             | Boolean          |
| message             | String          |

#### Example Response Object
```json
{
  "success": true,
  "message": "Camera visibility successfully updated"
}
```

## Update Wave details

This endpoint provides support for the client app to update specifics of a particular event object within a session. Only pass the properties required for update. 

Path: `PATCH /sessions/:sessionId/waves/:waveId`

#### Note: 

Patch endpoint is used to edit the favorite and outcome of a particular wave in a session. 

### Request Parameters
| Property     | Type    | Required | Description                                  |
|--------------|---------|----------|----------------------------------------------|
| sessionId    | String  | Yes      |   ID of the session object                   |
| waveId       | String  | Yes      |   ID of the wave object within this session  |

### Request Object
| Property            | Type             | Required | Description                         |
|---------------------|------------------|----------|-------------------------------------|
| outcome             | String (Enum)    | No       | CAUGHT/ATTEMPTED                    |
| favorite            | Boolean          | No       | Is this event (wave) a favorite?    |
| userVerified        | Boolean          | No       | The user has confirmed the outcome  |


On success, this endpoint returns a success message as detailed below.

### Response Object Attributes
| Property   | Type             |
|------------|------------------|
| success    | Boolean          |
| message    | String           |

#### Example Response Object
```json
{
  "success": true,
  "message": "Wave details patched successfully."
}
```

## Get Session Clip

This endpoint allows users to share their clips with friends. Given a clip ID the endpoint can look up a session by clip and return the related clip information and a link to the clip video.

Path: `GET /sessions/clips/:clipId`

#### Authentication

User authentication happens like it does for other services i.e. requesting client must pass the `X-Auth-AccessToken` header.

_Note: If you are using the entitlements service from the services proxy, these query params are populated automatically via the user's access token. Access tokens can be passed either via the header value `X-Auth-AccessToken` or query param `accesstoken`._

### Request Parameters

| Property | Type   | Required | Description            |
| -------- | ------ | -------- | ---------------------- |
| clipId   | String | Yes      | ID of the desired clip |

On success, this endpoint returns a success message as detailed below.

### Response Object Attributes

| Property                                    | Type               |
| ------------------------------------------- | ------------------ |
| associated                                  | Object             |
| associated.units                            | Object             |
| associated.units.temperature                | String             |
| associated.units.windSpeed                  | String             |
| associated.units.tideHeight                 | String             |
| associated.units.swellHeight                | String             |
| associated.units.surfHeight                 | String             |
| associated.units.distance                   | String             |
| associated.units.speed                      | String             |
| spot                                        | Object             |
| spot.id                                     | String             |
| spot.name                                   | String             |
| spot.location                               | Object             |
| spot.location.type                          | String             |
| spot.location.coordinates                   | Point              |
| spotReportView                              | Object             |
| spotReportView.name                         | String             |
| spotReportView.lat                          | Float              |
| spotReportView.lon                          | Float              |
| spotReportView.thumbnail                    | String             |
| spotReportView.timezone                     | String             |
| spotReportView.offshoreDirection            | Float              |
| spotReportView.legacyRegionId               | Integer            |
| spotReportView.subregionId                  | ObjectId           |
| spotReportView.rank                         | Integer            |
| spotReportView.legacyId                     | Integer            |
| spotReportView.waveHeight                   | Object             |
| spotReportView.waveHeight.human             | Float              |
| spotReportView.waveHeight.min               | Float              |
| spotReportView.waveHeight.max               | Float              |
| spotReportView.waveHeight.occasional        | Float              |
| spotReportView.waveHeight.humanRelation     | Float              |
| spotReportView.waveHeight.plus              | Float              |
| spotReportView.swells                       | Array              |
| spotReportView.swells[n].height             | Float              |
| spotReportView.swells[n].direction          | Float              |
| spotReportView.swells[n].period             | Float              |
| spotReportView.weather                      | Object             |
| spotReportView.weather.temperature          | Float              |
| spotReportView.weather.condition            | String             |
| spotReportView.wind                         | Object             |
| spotReportView.wind.speed                   | Float              |
| spotReportView.wind.direction               | Integer            |
| spotReportView.tide                         | Object             |
| spotReportView.tide.previous                | Object             |
| spotReportView.tide.previous.type           | String             |
| spotReportView.tide.previous.height         | Float              |
| spotReportView.tide.previous.timestamp      | Integer            |
| spotReportView.tide.previous.utcOffset      | Integer            |
| spotReportView.tide.current                 | Object             |
| spotReportView.tide.current.type            | String             |
| spotReportView.tide.current.height          | Float              |
| spotReportView.tide.current.timestamp       | Integer            |
| spotReportView.tide.current.utcOffset       | Integer            |
| spotReportView.tide.next                    | Object             |
| spotReportView.tide.next.type               | String             |
| spotReportView.tide.next.height             | Float              |
| spotReportView.tide.next.timestamp          | Integer            |
| spotReportView.tide.next.utcOffset          | Integer            |
| spotReportView.note                         | String             |
| spotReportView.conditions                   | Object             |
| spotReportView.conditions.human             | Boolean            |
| spotReportView.conditions.value             | String             |
| spotReportView.conditions.sortableCondition | Integer            |
| spotReportView.waterTemp                    | Object             |
| spotReportView.waterTemp.min                | Float              |
| spotReportView.waterTemp.max                | Float              |
| spotReportView.parentTaxonomy               | Array of ObjectIds |
| user                                        | Object             |
| user.firstName                              | String             |
| user.lastName                               | String             |
| wave                                        | Object             |
| wave.startTimestamp                         | Number             |
| wave.endTimestamp                           | Number             |
| wave.distance                               | Float              |
| wave.speedMax                               | Float              |
| wave.clip                                   | Object             |
| wave.clipUrl                                | String             |
| wave.thumbnail                              | Object             |
| wave.thumbnail.url                          | String             |
| wave.thumbnail.sizes                        | Array<String>      |

#### Example Response Object

```json
{
  "associated": {
    "units": {
      "temperature": "F",
      "windSpeed": "KTS",
      "tideHeight": "FT",
      "swellHeight": "FT",
      "surfHeight": "FT",
      "distance": "FT",
      "speed": "MPH"
    }
  },
  "spot": {
    "id": "5842041f4e65fad6a77088ea",
    "name": "Goldenwest",
    "location": {
      "type": "Point",
      "coordinates": [-118.0192012569068, 33.66704400178788]
    }
  },
  "spotReportView": {
    "waveHeight": {
      "human": false,
      "min": 4.2,
      "max": 4.99,
      "occasional": null,
      "humanRelation": null,
      "plus": false
    },
    "weather": {
      "temperature": 67.5,
      "condition": "CLEAR_NO_RAIN"
    },
    "wind": {
      "speed": 10.55,
      "direction": 255.27
    },
    "conditions": {
      "human": false,
      "value": null
    },
    "waterTemp": {
      "min": 59,
      "max": 60.8
    },
    "offshoreDirection": 219,
    "legacyRegionId": 2143,
    "legacyId": 4870,
    "note": null,
    "parentTaxonomy": [
      "58f7ed51dadb30820bb38782",
      "58f7ed51dadb30820bb38791",
      "58f7ed51dadb30820bb3879c",
      "5908c46bdadb30820b23c1f3",
      "58f7ed5ddadb30820bb39651",
      "58f7ed51dadb30820bb387a6",
      "58f7ed5ddadb30820bb39689",
      "58f7ed5fdadb30820bb3987c"
    ],
    "thumbnail": "https://spot-thumbnails.staging.surfline.com/spots/5842041f4e65fad6a77088ea/5842041f4e65fad6a77088ea_1500.jpg",
    "rank": 8,
    "subregionId": "58581a836630e24c44878fd6",
    "lat": 33.66704400178788,
    "lon": -118.0192012569068,
    "name": "Goldenwest",
    "tide": {
      "previous": {
        "type": "HIGH",
        "height": 3.22,
        "timestamp": 1553547180,
        "utcOffset": -7
      },
      "current": {
        "type": "NORMAL",
        "height": 3.053211111111111,
        "timestamp": 1553551132,
        "utcOffset": -7
      },
      "next": {
        "type": "LOW",
        "height": 1.87,
        "timestamp": 1553564700,
        "utcOffset": -7
      }
    },
    "cameras": [
      {
        "status": {
          "isDown": false,
          "message": ""
        },
        "_id": "58235706302ec90c4108c3e4",
        "title": "OC - Goldenwest",
        "streamUrl": "https://cams.cdn-surfline.com/cdn-wc/wc-goldenwest/playlist.m3u8",
        "stillUrl": "https://camstills.cdn-surfline.com/wc-goldenwest/latest_small.jpg",
        "pixelatedStillUrl": "https://camstills.cdn-surfline.com/wc-goldenwest/latest_small_pixelated.png",
        "rewindBaseUrl": "https://camrewinds.cdn-surfline.com/wc-goldenwest/wc-goldenwest",
        "isPremium": false
      }
    ],
    "timezone": "America/Los_Angeles",
    "swells": [
      {
        "_id": "5c994f20e9ea9002a6b10d63",
        "height": 0.52,
        "direction": 191.25,
        "period": 18
      },
      {
        "_id": "5c994f20e9ea90847bb10d62",
        "height": 1.94,
        "direction": 264.38,
        "period": 14
      },
      {
        "_id": "5c994f20e9ea90a598b10d61",
        "height": 1.97,
        "direction": 271.41,
        "period": 9
      },
      {
        "_id": "5c994f20e9ea909f9bb10d60",
        "height": 0.66,
        "direction": 195.47,
        "period": 11
      },
      {
        "_id": "5c994f20e9ea90d8fdb10d5f",
        "height": 0,
        "direction": 0,
        "period": 0
      },
      {
        "_id": "5c994f20e9ea90947db10d5e",
        "height": 0,
        "direction": 0,
        "period": 0
      }
    ]
  },
  "user": {
    "firstName": "drew",
    "lastName": "newberry"
  },
  "wave": {
    "startTimestamp": 1553554939870,
    "endTimestamp": 1553554951786,
    "distance": 7365.13,
    "speedMax": 36.95,
    "clip": {
      "clipUrl": "https://camclips.sandbox.surfline.com/58235706302ec90c4108c3e4/5c9d0ddfcc38890c7ac9ebb0.mp4",
      "thumbnail": {
        "url": "https://camclips.sandbox.surfline.com/58235706302ec90c4108c3e4/5c9d0ddfcc38890c7ac9ebb0_{size}.jpg",
        "sizes": ["300", "640", "1500", "3000"]
      }
    }
  }
}
```

## Linked Objects

### Event

Object is used to describe an event during the user's session.

#### Object Properties

| Parameter      | Type    | Required | Description                                                                                      |
| -------------- | ------- | -------- | ------------------------------------------------------------------------------------------------ |
| startTimestamp | Integer | Yes      | UTC timestamp when the event started.                                                            |
| endTimestamp   | Integer | Yes      | UTC timestamp when the event ended.                                                              |
| type           | Enum    | Yes      | Key to detonate the event type: `PADDLE` - Used when the user is paddling. `WAVE` - Used when the                                         user is actively surfing a wave.                                                                 |
| outcome        | Enum    | No       | Key to detonate the outcome for this event: `ATTEMPTED` - This wave event was attempted only.                                               `CAUGHT` - This wave event was considered as having been caught. (default is `CAUGHT`)         |
| endTimestamp   | Integer | Yes      | UTC timestamp when the event ended.                                                              |
| speedMax       | Float   | Yes      | Maximum speed the user traveled during this event in *meters per second*.                        |
| speedAverage   | Float   | Yes      | Average speed the user traveled during this event in *meters per second*.                        |
| positions      | Array   | No       | Array of Position objects that are included as part of this event. Position objects should be at                                          a frequency no higher than 1 per second.                                                         |

### Position

Object is used to describe a discrete position in a user's session.

#### Object Properties

| Parameter      | Type    | Required | Description                                                                                      |
| -------------- | ------- | -------- | ------------------------------------------------------------------------------------------------ |
| timestamp      | Integer | Yes      | Unix timestamp in milliseconds when this position object occurred.                               |
| latitude       | Float   | Yes      | Coorindate latitude, degrees, decimal format.                                                    |
| longitude      | Float   | Yes      | Coorindate longitude, degrees, decimal format.                                                   |
| horizontalAccuracy  | Float   | No      | The latitude and longitude identify the center of a circle, and this value indicates the radius of that circle. A negative value indicates that the latitude and longitude are invalid. This is only used by requests sent to the `/sessions/gps` endpoint. |


# Questions?
- How do we determine timezone for the user, is it local to the spot or to the user? i.e. for the summary if I've surfed in two timezones what appears in the July summary?
-- Likely answer will be to use the clients timezone i.e. Timezone of the iOS device.
- What do we display when there's no spot for a session? Mock doesn't show this right now.
- Should conditions.conditions.value be null? if not a human report or does it return lola? Will be null if human = false
- Are we copying forecast data into the session object? *Yes*
-- If yes will we want to store more data than we're showing today?
-- Might be easier to do it now with no data than to have to backport it if we add a weather icon. Or we just add that data for sessions moving forward, but that just seems a bit shit from a users POV.
-- *We should store the entire sport report view.*
- Can `trackImage.url`s be null? If using lambda to create then yes, should we prepare for this today, even though we expect never null given the url can be worked out easily progomatically. *no will always return a basic tile.*
- Should we have seperate endpoints for compatible api vs product changes.
- Should we use camel case or underscores. *It should be camel case.*
- Where do we determine if the user has an integration, what API response is this in `/user`? *Drew to answer.*
- Confirm we're not doing multiple angles until we've combined camera angles on spots in the CMS. Where are we in discovery for that? Is there a story in the KBYG backlog for that? *Confirmed.*
