import { check } from 'k6';
import http from 'k6/http';
import { Rate, Trend } from 'k6/metrics';
import session from './session.js';

export const rateErrors = new Rate('errors');
export const trendDuration = new Trend('duration');

export const options = {
  thresholds: {
    errors: [{ threshold: 'rate<0.01', abortOnFail: true }],
    duration: [{ threshold: 'avg<5000', abortOnFail: true }],
  },
  duration: '1m',
  vus: 100,
  rps: 10,
};

export default () => {
  const res = http.post(`http://${__ENV.HOST}/sessions`, JSON.stringify(session), {
    headers: {
      'Content-Type': 'application/json',
      'x-auth-userid': __ENV.USER_ID,
      'x-api-key': __ENV.CLIENT_ID,
    },
  });

  trendDuration.add(res.timings.duration);

  const status200 = check(res, {
    'status is 200': r => r.status === 200,
  });

  if (!status200) {
    rateErrors.add(1);
  }
};
