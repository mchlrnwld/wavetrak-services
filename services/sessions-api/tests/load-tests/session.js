export default {
  startTimestamp: 1537738482000,
  endTimestamp: 1537738925000,
  visibility: 'PUBLIC',
  waveCount: 4,
  speedMax: 17.5,
  distancePaddled: 1026.83,
  distanceWaves: 839.19,
  distanceLongestWave: 544.63,
  deviceType: 'iOS',
  events: [
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
    {
      startTimestamp: 1537774540000,
      endTimestamp: 1537774553000,
      type: 'WAVE',
      distance: 75.7,
      speedMax: 25.31,
      speedAverage: 19.47,
      positions: [
        {
          timestamp: 1537774540000,
          latitude: -33.798788,
          longitude: 151.177443,
        },
        {
          timestamp: 1537774541000,
          latitude: -33.798791,
          longitude: 151.177491,
        },
        {
          timestamp: 1537774542000,
          latitude: -33.798794,
          longitude: 151.177548,
        },
        {
          timestamp: 1537774543000,
          latitude: -33.798798,
          longitude: 151.177615,
        },
        {
          timestamp: 1537774544000,
          latitude: -33.798803,
          longitude: 151.177688,
        },
        {
          timestamp: 1537774545000,
          latitude: -33.798807,
          longitude: 151.177763,
        },
        {
          timestamp: 1537774546000,
          latitude: -33.798807,
          longitude: 151.177839,
        },
        {
          timestamp: 1537774547000,
          latitude: -33.798805,
          longitude: 151.177914,
        },
        {
          timestamp: 1537774548000,
          latitude: -33.798804,
          longitude: 151.177986,
        },
        {
          timestamp: 1537774549000,
          latitude: -33.798803,
          longitude: 151.178051,
        },
        {
          timestamp: 1537774550000,
          latitude: -33.798802,
          longitude: 151.178107,
        },
        {
          timestamp: 1537774551000,
          latitude: -33.7988,
          longitude: 151.178156,
        },
        {
          timestamp: 1537774552000,
          latitude: -33.798798,
          longitude: 151.178195,
        },
        {
          timestamp: 1537774553000,
          latitude: -33.798795,
          longitude: 151.178225,
        },
      ],
    },
    {
      startTimestamp: 1537774654000,
      endTimestamp: 1537774668000,
      type: 'WAVE',
      distance: 73.9,
      speedMax: 23.69,
      speedAverage: 17.74,
      positions: [
        {
          timestamp: 1537774654000,
          latitude: -33.798799,
          longitude: 151.178355,
        },
        {
          timestamp: 1537774655000,
          latitude: -33.798797,
          longitude: 151.178397,
        },
        {
          timestamp: 1537774656000,
          latitude: -33.798794,
          longitude: 151.178447,
        },
        {
          timestamp: 1537774657000,
          latitude: -33.798791,
          longitude: 151.178505,
        },
        {
          timestamp: 1537774658000,
          latitude: -33.79879,
          longitude: 151.178572,
        },
        {
          timestamp: 1537774659000,
          latitude: -33.798794,
          longitude: 151.178643,
        },
        {
          timestamp: 1537774660000,
          latitude: -33.798803,
          longitude: 151.17871,
        },
        {
          timestamp: 1537774661000,
          latitude: -33.798821,
          longitude: 151.178769,
        },
        {
          timestamp: 1537774662000,
          latitude: -33.798846,
          longitude: 151.178819,
        },
        {
          timestamp: 1537774663000,
          latitude: -33.798876,
          longitude: 151.178863,
        },
        {
          timestamp: 1537774664000,
          latitude: -33.798912,
          longitude: 151.178899,
        },
        {
          timestamp: 1537774665000,
          latitude: -33.79895,
          longitude: 151.178926,
        },
        {
          timestamp: 1537774666000,
          latitude: -33.798987,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774667000,
          latitude: -33.799019,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774668000,
          latitude: -33.799047,
          longitude: 151.178963,
        },
      ],
    },
    {
      startTimestamp: 1537774694000,
      endTimestamp: 1537774712000,
      type: 'WAVE',
      distance: 144.96,
      speedMax: 46.19,
      speedAverage: 27.47,
      positions: [
        {
          timestamp: 1537774694000,
          latitude: -33.799073,
          longitude: 151.17901,
        },
        {
          timestamp: 1537774695000,
          latitude: -33.799098,
          longitude: 151.179032,
        },
        {
          timestamp: 1537774696000,
          latitude: -33.799124,
          longitude: 151.179059,
        },
        {
          timestamp: 1537774697000,
          latitude: -33.799149,
          longitude: 151.179089,
        },
        {
          timestamp: 1537774698000,
          latitude: -33.799172,
          longitude: 151.179124,
        },
        {
          timestamp: 1537774699000,
          latitude: -33.799187,
          longitude: 151.179159,
        },
        {
          timestamp: 1537774700000,
          latitude: -33.799192,
          longitude: 151.179198,
        },
        {
          timestamp: 1537774701000,
          latitude: -33.799187,
          longitude: 151.179245,
        },
        {
          timestamp: 1537774702000,
          latitude: -33.799175,
          longitude: 151.179298,
        },
        {
          timestamp: 1537774703000,
          latitude: -33.799136875,
          longitude: 151.179429,
        },
        {
          timestamp: 1537774704000,
          latitude: -33.79909875,
          longitude: 151.17956,
        },
        {
          timestamp: 1537774705000,
          latitude: -33.799060625,
          longitude: 151.179691,
        },
        {
          timestamp: 1537774706000,
          latitude: -33.7990225,
          longitude: 151.179822,
        },
        {
          timestamp: 1537774707000,
          latitude: -33.798984375,
          longitude: 151.179953,
        },
        {
          timestamp: 1537774708000,
          latitude: -33.79894625,
          longitude: 151.180084,
        },
        {
          timestamp: 1537774709000,
          latitude: -33.798908125,
          longitude: 151.180215,
        },
        {
          timestamp: 1537774710000,
          latitude: -33.79887,
          longitude: 151.180346,
        },
        {
          timestamp: 1537774711000,
          latitude: -33.798844,
          longitude: 151.180378,
        },
        {
          timestamp: 1537774712000,
          latitude: -33.798828,
          longitude: 151.180404,
        },
      ],
    },
    {
      startTimestamp: 1537774733000,
      endTimestamp: 1537774835000,
      type: 'WAVE',
      distance: 544.63,
      speedMax: 43.1,
      speedAverage: 19.05,
      positions: [
        {
          timestamp: 1537774733000,
          latitude: -33.798904,
          longitude: 151.180551,
        },
        {
          timestamp: 1537774734000,
          latitude: -33.798882,
          longitude: 151.180588,
        },
        {
          timestamp: 1537774735000,
          latitude: -33.79883599999999,
          longitude: 151.1806775,
        },
        {
          timestamp: 1537774736000,
          latitude: -33.79879,
          longitude: 151.180767,
        },
        {
          timestamp: 1537774737000,
          latitude: -33.798763,
          longitude: 151.1808,
        },
        {
          timestamp: 1537774738000,
          latitude: -33.798738,
          longitude: 151.180838,
        },
        {
          timestamp: 1537774739000,
          latitude: -33.798708,
          longitude: 151.180887,
        },
        {
          timestamp: 1537774740000,
          latitude: -33.798681,
          longitude: 151.180946,
        },
        {
          timestamp: 1537774741000,
          latitude: -33.798656,
          longitude: 151.18101,
        },
        {
          timestamp: 1537774742000,
          latitude: -33.798633,
          longitude: 151.181073,
        },
        {
          timestamp: 1537774743000,
          latitude: -33.798614,
          longitude: 151.181135,
        },
        {
          timestamp: 1537774744000,
          latitude: -33.798578,
          longitude: 151.181236,
        },
        {
          timestamp: 1537774745000,
          latitude: -33.798539,
          longitude: 151.181355,
        },
        {
          timestamp: 1537774746000,
          latitude: -33.798503,
          longitude: 151.181477,
        },
        {
          timestamp: 1537774747000,
          latitude: -33.79848815714286,
          longitude: 151.1815273571429,
        },
        {
          timestamp: 1537774748000,
          latitude: -33.79847331428571,
          longitude: 151.1815777142857,
        },
        {
          timestamp: 1537774749000,
          latitude: -33.79845847142857,
          longitude: 151.1816280714286,
        },
        {
          timestamp: 1537774750000,
          latitude: -33.79844362857143,
          longitude: 151.1816784285714,
        },
        {
          timestamp: 1537774751000,
          latitude: -33.79842878571428,
          longitude: 151.1817287857143,
        },
        {
          timestamp: 1537774752000,
          latitude: -33.79841394285714,
          longitude: 151.1817791428572,
        },
        {
          timestamp: 1537774753000,
          latitude: -33.7983991,
          longitude: 151.1818295,
        },
        {
          timestamp: 1537774754000,
          latitude: -33.79838425714286,
          longitude: 151.1818798571429,
        },
        {
          timestamp: 1537774755000,
          latitude: -33.79836941428571,
          longitude: 151.1819302142857,
        },
        {
          timestamp: 1537774756000,
          latitude: -33.79835457142857,
          longitude: 151.1819805714286,
        },
        {
          timestamp: 1537774757000,
          latitude: -33.79833972857143,
          longitude: 151.1820309285714,
        },
        {
          timestamp: 1537774758000,
          latitude: -33.79832488571428,
          longitude: 151.1820812857143,
        },
        {
          timestamp: 1537774759000,
          latitude: -33.79831004285714,
          longitude: 151.1821316428571,
        },
        {
          timestamp: 1537774760000,
          latitude: -33.7982952,
          longitude: 151.182182,
        },
        {
          timestamp: 1537774761000,
          latitude: -33.79828035714286,
          longitude: 151.1822323571429,
        },
        {
          timestamp: 1537774762000,
          latitude: -33.79826551428571,
          longitude: 151.1822827142857,
        },
        {
          timestamp: 1537774763000,
          latitude: -33.79825067142857,
          longitude: 151.1823330714286,
        },
        {
          timestamp: 1537774764000,
          latitude: -33.79823582857143,
          longitude: 151.1823834285714,
        },
        {
          timestamp: 1537774765000,
          latitude: -33.79822098571428,
          longitude: 151.1824337857143,
        },
        {
          timestamp: 1537774766000,
          latitude: -33.79820614285714,
          longitude: 151.1824841428571,
        },
        {
          timestamp: 1537774767000,
          latitude: -33.7981913,
          longitude: 151.1825345,
        },
        {
          timestamp: 1537774768000,
          latitude: -33.79817645714285,
          longitude: 151.1825848571429,
        },
        {
          timestamp: 1537774769000,
          latitude: -33.79816161428571,
          longitude: 151.1826352142857,
        },
        {
          timestamp: 1537774770000,
          latitude: -33.79814677142857,
          longitude: 151.1826855714286,
        },
        {
          timestamp: 1537774771000,
          latitude: -33.79813192857142,
          longitude: 151.1827359285714,
        },
        {
          timestamp: 1537774772000,
          latitude: -33.79811708571428,
          longitude: 151.1827862857143,
        },
        {
          timestamp: 1537774773000,
          latitude: -33.79810224285714,
          longitude: 151.1828366428572,
        },
        {
          timestamp: 1537774774000,
          latitude: -33.7980874,
          longitude: 151.182887,
        },
        {
          timestamp: 1537774775000,
          latitude: -33.79807255714285,
          longitude: 151.1829373571429,
        },
        {
          timestamp: 1537774776000,
          latitude: -33.79805771428571,
          longitude: 151.1829877142857,
        },
        {
          timestamp: 1537774777000,
          latitude: -33.79804287142857,
          longitude: 151.1830380714286,
        },
        {
          timestamp: 1537774778000,
          latitude: -33.79802802857142,
          longitude: 151.1830884285714,
        },
        {
          timestamp: 1537774779000,
          latitude: -33.79801318571428,
          longitude: 151.1831387857143,
        },
        {
          timestamp: 1537774780000,
          latitude: -33.79799834285714,
          longitude: 151.1831891428571,
        },
        {
          timestamp: 1537774781000,
          latitude: -33.7979835,
          longitude: 151.1832395,
        },
        {
          timestamp: 1537774782000,
          latitude: -33.79796865714285,
          longitude: 151.1832898571429,
        },
        {
          timestamp: 1537774783000,
          latitude: -33.79795381428571,
          longitude: 151.1833402142857,
        },
        {
          timestamp: 1537774784000,
          latitude: -33.79793897142857,
          longitude: 151.1833905714286,
        },
        {
          timestamp: 1537774785000,
          latitude: -33.79792412857142,
          longitude: 151.1834409285714,
        },
        {
          timestamp: 1537774786000,
          latitude: -33.79790928571428,
          longitude: 151.1834912857143,
        },
        {
          timestamp: 1537774787000,
          latitude: -33.79789444285714,
          longitude: 151.1835416428571,
        },
        {
          timestamp: 1537774788000,
          latitude: -33.79787959999999,
          longitude: 151.183592,
        },
        {
          timestamp: 1537774789000,
          latitude: -33.79786475714285,
          longitude: 151.1836423571428,
        },
        {
          timestamp: 1537774790000,
          latitude: -33.79784991428571,
          longitude: 151.1836927142857,
        },
        {
          timestamp: 1537774791000,
          latitude: -33.79783507142857,
          longitude: 151.1837430714286,
        },
        {
          timestamp: 1537774792000,
          latitude: -33.79782022857142,
          longitude: 151.1837934285714,
        },
        {
          timestamp: 1537774793000,
          latitude: -33.79780538571428,
          longitude: 151.1838437857143,
        },
        {
          timestamp: 1537774794000,
          latitude: -33.79779054285714,
          longitude: 151.1838941428571,
        },
        {
          timestamp: 1537774795000,
          latitude: -33.7977757,
          longitude: 151.1839445,
        },
        {
          timestamp: 1537774796000,
          latitude: -33.79776085714285,
          longitude: 151.1839948571429,
        },
        {
          timestamp: 1537774797000,
          latitude: -33.79774601428571,
          longitude: 151.1840452142857,
        },
        {
          timestamp: 1537774798000,
          latitude: -33.79773117142857,
          longitude: 151.1840955714286,
        },
        {
          timestamp: 1537774799000,
          latitude: -33.79771632857143,
          longitude: 151.1841459285714,
        },
        {
          timestamp: 1537774800000,
          latitude: -33.79770148571428,
          longitude: 151.1841962857143,
        },
        {
          timestamp: 1537774801000,
          latitude: -33.79768664285714,
          longitude: 151.1842466428571,
        },
        {
          timestamp: 1537774802000,
          latitude: -33.7976718,
          longitude: 151.184297,
        },
        {
          timestamp: 1537774803000,
          latitude: -33.79765695714286,
          longitude: 151.1843473571429,
        },
        {
          timestamp: 1537774804000,
          latitude: -33.79764211428571,
          longitude: 151.1843977142857,
        },
        {
          timestamp: 1537774805000,
          latitude: -33.79762727142857,
          longitude: 151.1844480714286,
        },
        {
          timestamp: 1537774806000,
          latitude: -33.79761242857143,
          longitude: 151.1844984285714,
        },
        {
          timestamp: 1537774807000,
          latitude: -33.79759758571429,
          longitude: 151.1845487857143,
        },
        {
          timestamp: 1537774808000,
          latitude: -33.79758274285714,
          longitude: 151.1845991428571,
        },
        {
          timestamp: 1537774809000,
          latitude: -33.7975679,
          longitude: 151.1846495,
        },
        {
          timestamp: 1537774810000,
          latitude: -33.79755305714286,
          longitude: 151.1846998571428,
        },
        {
          timestamp: 1537774811000,
          latitude: -33.79753821428571,
          longitude: 151.1847502142857,
        },
        {
          timestamp: 1537774812000,
          latitude: -33.79752337142857,
          longitude: 151.1848005714286,
        },
        {
          timestamp: 1537774813000,
          latitude: -33.79750852857143,
          longitude: 151.1848509285714,
        },
        {
          timestamp: 1537774814000,
          latitude: -33.79749368571429,
          longitude: 151.1849012857143,
        },
        {
          timestamp: 1537774815000,
          latitude: -33.79747884285714,
          longitude: 151.1849516428571,
        },
        {
          timestamp: 1537774816000,
          latitude: -33.797464,
          longitude: 151.185002,
        },
        {
          timestamp: 1537774817000,
          latitude: -33.797376,
          longitude: 151.184995,
        },
        {
          timestamp: 1537774818000,
          latitude: -33.797324,
          longitude: 151.184987,
        },
        {
          timestamp: 1537774819000,
          latitude: -33.797285,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774820000,
          latitude: -33.79725,
          longitude: 151.184966,
        },
        {
          timestamp: 1537774821000,
          latitude: -33.797216,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774822000,
          latitude: -33.797181,
          longitude: 151.184942,
        },
        {
          timestamp: 1537774823000,
          latitude: -33.797147,
          longitude: 151.184928,
        },
        {
          timestamp: 1537774824000,
          latitude: -33.797092,
          longitude: 151.184909,
        },
        {
          timestamp: 1537774825000,
          latitude: -33.797025,
          longitude: 151.184884,
        },
        {
          timestamp: 1537774826000,
          latitude: -33.796962,
          longitude: 151.184861,
        },
        {
          timestamp: 1537774827000,
          latitude: -33.7969,
          longitude: 151.184841,
        },
        {
          timestamp: 1537774828000,
          latitude: -33.796841,
          longitude: 151.184825,
        },
        {
          timestamp: 1537774829000,
          latitude: -33.796788,
          longitude: 151.184813,
        },
        {
          timestamp: 1537774830000,
          latitude: -33.796741,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774831000,
          latitude: -33.7967,
          longitude: 151.184805,
        },
        {
          timestamp: 1537774832000,
          latitude: -33.796664,
          longitude: 151.184808,
        },
        {
          timestamp: 1537774833000,
          latitude: -33.796633,
          longitude: 151.184814,
        },
        {
          timestamp: 1537774834000,
          latitude: -33.796605,
          longitude: 151.184819,
        },
        {
          timestamp: 1537774835000,
          latitude: -33.796581,
          longitude: 151.184823,
        },
      ],
    },
    {
      startTimestamp: 1537774482000,
      endTimestamp: 1537774539000,
      type: 'PADDLE',
      distance: 42.47,
      speedMax: 8.75,
      speedAverage: 2.64,
      positions: [
        {
          timestamp: 1537774482000,
          latitude: -33.798535,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774483000,
          latitude: -33.798534,
          longitude: 151.17736,
        },
        {
          timestamp: 1537774484000,
          latitude: -33.798534,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774485000,
          latitude: -33.798538,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774486000,
          latitude: -33.798543,
          longitude: 151.177355,
        },
        {
          timestamp: 1537774487000,
          latitude: -33.798548,
          longitude: 151.177357,
        },
        {
          timestamp: 1537774488000,
          latitude: -33.798553,
          longitude: 151.177359,
        },
        {
          timestamp: 1537774489000,
          latitude: -33.798558,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774490000,
          latitude: -33.798564,
          longitude: 151.177363,
        },
        {
          timestamp: 1537774491000,
          latitude: -33.798575,
          longitude: 151.177366,
        },
        {
          timestamp: 1537774492000,
          latitude: -33.798588,
          longitude: 151.17737,
        },
        {
          timestamp: 1537774493000,
          latitude: -33.798602,
          longitude: 151.177374,
        },
        {
          timestamp: 1537774494000,
          latitude: -33.798616,
          longitude: 151.177376,
        },
        {
          timestamp: 1537774495000,
          latitude: -33.79863,
          longitude: 151.177375,
        },
        {
          timestamp: 1537774496000,
          latitude: -33.798644,
          longitude: 151.177373,
        },
        {
          timestamp: 1537774497000,
          latitude: -33.798656,
          longitude: 151.177368,
        },
        {
          timestamp: 1537774498000,
          latitude: -33.798667,
          longitude: 151.177361,
        },
        {
          timestamp: 1537774499000,
          latitude: -33.798678,
          longitude: 151.177352,
        },
        {
          timestamp: 1537774500000,
          latitude: -33.798686,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774501000,
          latitude: -33.798693,
          longitude: 151.177333,
        },
        {
          timestamp: 1537774502000,
          latitude: -33.7987,
          longitude: 151.177324,
        },
        {
          timestamp: 1537774503000,
          latitude: -33.798706,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774504000,
          latitude: -33.798712,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774505000,
          latitude: -33.798719,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774506000,
          latitude: -33.798726,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774507000,
          latitude: -33.798734,
          longitude: 151.177307,
        },
        {
          timestamp: 1537774508000,
          latitude: -33.798742,
          longitude: 151.177309,
        },
        {
          timestamp: 1537774509000,
          latitude: -33.798749,
          longitude: 151.177311,
        },
        {
          timestamp: 1537774510000,
          latitude: -33.798756,
          longitude: 151.177314,
        },
        {
          timestamp: 1537774511000,
          latitude: -33.798763,
          longitude: 151.177317,
        },
        {
          timestamp: 1537774512000,
          latitude: -33.798769,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774513000,
          latitude: -33.798774,
          longitude: 151.177321,
        },
        {
          timestamp: 1537774514000,
          latitude: -33.798778,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774515000,
          latitude: -33.798781,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774516000,
          latitude: -33.798784,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774517000,
          latitude: -33.798785,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774518000,
          latitude: -33.798788,
          longitude: 151.17732,
        },
        {
          timestamp: 1537774519000,
          latitude: -33.798789,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774520000,
          latitude: -33.798791,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774521000,
          latitude: -33.798792,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774522000,
          latitude: -33.798794,
          longitude: 151.177315,
        },
        {
          timestamp: 1537774523000,
          latitude: -33.798795,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774524000,
          latitude: -33.798796,
          longitude: 151.177316,
        },
        {
          timestamp: 1537774525000,
          latitude: -33.798797,
          longitude: 151.177319,
        },
        {
          timestamp: 1537774526000,
          latitude: -33.798796,
          longitude: 151.177322,
        },
        {
          timestamp: 1537774527000,
          latitude: -33.798795,
          longitude: 151.177326,
        },
        {
          timestamp: 1537774528000,
          latitude: -33.798794,
          longitude: 151.177331,
        },
        {
          timestamp: 1537774529000,
          latitude: -33.798793,
          longitude: 151.177336,
        },
        {
          timestamp: 1537774530000,
          latitude: -33.798791,
          longitude: 151.17734,
        },
        {
          timestamp: 1537774531000,
          latitude: -33.798789,
          longitude: 151.177343,
        },
        {
          timestamp: 1537774532000,
          latitude: -33.798786,
          longitude: 151.177345,
        },
        {
          timestamp: 1537774533000,
          latitude: -33.798785,
          longitude: 151.177347,
        },
        {
          timestamp: 1537774534000,
          latitude: -33.798784,
          longitude: 151.177348,
        },
        {
          timestamp: 1537774535000,
          latitude: -33.798781,
          longitude: 151.17735,
        },
        {
          timestamp: 1537774536000,
          latitude: -33.798779,
          longitude: 151.177354,
        },
        {
          timestamp: 1537774537000,
          latitude: -33.79878,
          longitude: 151.177364,
        },
        {
          timestamp: 1537774538000,
          latitude: -33.798782,
          longitude: 151.177382,
        },
        {
          timestamp: 1537774539000,
          latitude: -33.798785,
          longitude: 151.177408,
        },
      ],
    },
    {
      startTimestamp: 1537774554000,
      endTimestamp: 1537774653000,
      type: 'PADDLE',
      distance: 21.15,
      speedMax: 7.38,
      speedAverage: 0.77,
      positions: [
        {
          timestamp: 1537774554000,
          latitude: -33.798795,
          longitude: 151.178246,
        },
        {
          timestamp: 1537774555000,
          latitude: -33.798796,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774556000,
          latitude: -33.798799,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774557000,
          latitude: -33.798802,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774558000,
          latitude: -33.798804,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774559000,
          latitude: -33.798807,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774560000,
          latitude: -33.79881,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774561000,
          latitude: -33.798814,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774562000,
          latitude: -33.798817,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774563000,
          latitude: -33.798818,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774564000,
          latitude: -33.79882,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774565000,
          latitude: -33.798821,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774566000,
          latitude: -33.798823,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774567000,
          latitude: -33.798824,
          longitude: 151.17826,
        },
        {
          timestamp: 1537774568000,
          latitude: -33.798825,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774569000,
          latitude: -33.798826,
          longitude: 151.178261,
        },
        {
          timestamp: 1537774570000,
          latitude: -33.798828,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774571000,
          latitude: -33.798829,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774572000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774573000,
          latitude: -33.79883,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774574000,
          latitude: -33.798832,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774575000,
          latitude: -33.798833,
          longitude: 151.178262,
        },
        {
          timestamp: 1537774576000,
          latitude: -33.798834,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774577000,
          latitude: -33.798836,
          longitude: 151.178263,
        },
        {
          timestamp: 1537774578000,
          latitude: -33.798836,
          longitude: 151.178264,
        },
        {
          timestamp: 1537774579000,
          latitude: -33.798835,
          longitude: 151.178265,
        },
        {
          timestamp: 1537774580000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774581000,
          latitude: -33.798834,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774582000,
          latitude: -33.798833,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774583000,
          latitude: -33.79883,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774584000,
          latitude: -33.798829,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774585000,
          latitude: -33.798827,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774586000,
          latitude: -33.798826,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774587000,
          latitude: -33.798825,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774588000,
          latitude: -33.798823,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774589000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774590000,
          latitude: -33.798822,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774591000,
          latitude: -33.798821,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774592000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774593000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774594000,
          latitude: -33.79882,
          longitude: 151.178267,
        },
        {
          timestamp: 1537774595000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774596000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774597000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774598000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774599000,
          latitude: -33.798819,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774600000,
          latitude: -33.798817,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774601000,
          latitude: -33.798816,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774602000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774603000,
          latitude: -33.798815,
          longitude: 151.178266,
        },
        {
          timestamp: 1537774604000,
          latitude: -33.798813,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774605000,
          latitude: -33.798811,
          longitude: 151.178268,
        },
        {
          timestamp: 1537774606000,
          latitude: -33.79881,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774607000,
          latitude: -33.798809,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774608000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774609000,
          latitude: -33.798808,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774610000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774611000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774612000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774613000,
          latitude: -33.798808,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774614000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774615000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774616000,
          latitude: -33.798807,
          longitude: 151.178273,
        },
        {
          timestamp: 1537774617000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774618000,
          latitude: -33.798806,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774619000,
          latitude: -33.798804,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774620000,
          latitude: -33.798803,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774621000,
          latitude: -33.798802,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774622000,
          latitude: -33.798801,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774623000,
          latitude: -33.7988,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774624000,
          latitude: -33.798798,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774625000,
          latitude: -33.798797,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774626000,
          latitude: -33.798796,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774627000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774628000,
          latitude: -33.798794,
          longitude: 151.178269,
        },
        {
          timestamp: 1537774629000,
          latitude: -33.798793,
          longitude: 151.17827,
        },
        {
          timestamp: 1537774630000,
          latitude: -33.798792,
          longitude: 151.178271,
        },
        {
          timestamp: 1537774631000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774632000,
          latitude: -33.798792,
          longitude: 151.178272,
        },
        {
          timestamp: 1537774633000,
          latitude: -33.798792,
          longitude: 151.178274,
        },
        {
          timestamp: 1537774634000,
          latitude: -33.798793,
          longitude: 151.178275,
        },
        {
          timestamp: 1537774635000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774636000,
          latitude: -33.798793,
          longitude: 151.178276,
        },
        {
          timestamp: 1537774637000,
          latitude: -33.798794,
          longitude: 151.178278,
        },
        {
          timestamp: 1537774638000,
          latitude: -33.798794,
          longitude: 151.178279,
        },
        {
          timestamp: 1537774639000,
          latitude: -33.798795,
          longitude: 151.178281,
        },
        {
          timestamp: 1537774640000,
          latitude: -33.798796,
          longitude: 151.178282,
        },
        {
          timestamp: 1537774641000,
          latitude: -33.798798,
          longitude: 151.178283,
        },
        {
          timestamp: 1537774642000,
          latitude: -33.7988,
          longitude: 151.178284,
        },
        {
          timestamp: 1537774643000,
          latitude: -33.798801,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774644000,
          latitude: -33.798802,
          longitude: 151.178285,
        },
        {
          timestamp: 1537774645000,
          latitude: -33.798803,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774646000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774647000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774648000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774649000,
          latitude: -33.798805,
          longitude: 151.178286,
        },
        {
          timestamp: 1537774650000,
          latitude: -33.798805,
          longitude: 151.178288,
        },
        {
          timestamp: 1537774651000,
          latitude: -33.798804,
          longitude: 151.178289,
        },
        {
          timestamp: 1537774652000,
          latitude: -33.798802,
          longitude: 151.178301,
        },
        {
          timestamp: 1537774653000,
          latitude: -33.7988,
          longitude: 151.178323,
        },
      ],
    },
    {
      startTimestamp: 1537774669000,
      endTimestamp: 1537774693000,
      type: 'PADDLE',
      distance: 22.8,
      speedMax: 9.29,
      speedAverage: 3.29,
      positions: [
        {
          timestamp: 1537774669000,
          latitude: -33.799067,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774670000,
          latitude: -33.799081,
          longitude: 151.178966,
        },
        {
          timestamp: 1537774671000,
          latitude: -33.799088,
          longitude: 151.178964,
        },
        {
          timestamp: 1537774672000,
          latitude: -33.79909,
          longitude: 151.178961,
        },
        {
          timestamp: 1537774673000,
          latitude: -33.799089,
          longitude: 151.178957,
        },
        {
          timestamp: 1537774674000,
          latitude: -33.799087,
          longitude: 151.178953,
        },
        {
          timestamp: 1537774675000,
          latitude: -33.799082,
          longitude: 151.178949,
        },
        {
          timestamp: 1537774676000,
          latitude: -33.799076,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774677000,
          latitude: -33.799067,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774678000,
          latitude: -33.799058,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774679000,
          latitude: -33.799049,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774680000,
          latitude: -33.799042,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774681000,
          latitude: -33.799037,
          longitude: 151.178945,
        },
        {
          timestamp: 1537774682000,
          latitude: -33.79903,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774683000,
          latitude: -33.799024,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774684000,
          latitude: -33.79902,
          longitude: 151.178946,
        },
        {
          timestamp: 1537774685000,
          latitude: -33.799017,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774686000,
          latitude: -33.799015,
          longitude: 151.178947,
        },
        {
          timestamp: 1537774687000,
          latitude: -33.799014,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774688000,
          latitude: -33.799013,
          longitude: 151.178948,
        },
        {
          timestamp: 1537774689000,
          latitude: -33.79901,
          longitude: 151.178951,
        },
        {
          timestamp: 1537774690000,
          latitude: -33.799011,
          longitude: 151.178956,
        },
        {
          timestamp: 1537774691000,
          latitude: -33.799017,
          longitude: 151.178963,
        },
        {
          timestamp: 1537774692000,
          latitude: -33.799031,
          longitude: 151.178975,
        },
        {
          timestamp: 1537774693000,
          latitude: -33.79905,
          longitude: 151.178991,
        },
      ],
    },
    {
      startTimestamp: 1537774713000,
      endTimestamp: 1537774732000,
      type: 'PADDLE',
      distance: 23.05,
      speedMax: 9.26,
      speedAverage: 4.16,
      positions: [
        {
          timestamp: 1537774713000,
          latitude: -33.798814,
          longitude: 151.180426,
        },
        {
          timestamp: 1537774714000,
          latitude: -33.798802,
          longitude: 151.180439,
        },
        {
          timestamp: 1537774715000,
          latitude: -33.798793,
          longitude: 151.180445,
        },
        {
          timestamp: 1537774716000,
          latitude: -33.79879916666667,
          longitude: 151.1804508888889,
        },
        {
          timestamp: 1537774717000,
          latitude: -33.79880533333333,
          longitude: 151.1804567777778,
        },
        {
          timestamp: 1537774718000,
          latitude: -33.7988115,
          longitude: 151.1804626666667,
        },
        {
          timestamp: 1537774719000,
          latitude: -33.79881766666667,
          longitude: 151.1804685555556,
        },
        {
          timestamp: 1537774720000,
          latitude: -33.79882383333334,
          longitude: 151.1804744444444,
        },
        {
          timestamp: 1537774721000,
          latitude: -33.79883,
          longitude: 151.1804803333333,
        },
        {
          timestamp: 1537774722000,
          latitude: -33.79883616666667,
          longitude: 151.1804862222222,
        },
        {
          timestamp: 1537774723000,
          latitude: -33.79884233333333,
          longitude: 151.1804921111111,
        },
        {
          timestamp: 1537774724000,
          latitude: -33.79884850000001,
          longitude: 151.180498,
        },
        {
          timestamp: 1537774725000,
          latitude: -33.79885466666667,
          longitude: 151.1805038888889,
        },
        {
          timestamp: 1537774726000,
          latitude: -33.79886083333334,
          longitude: 151.1805097777778,
        },
        {
          timestamp: 1537774727000,
          latitude: -33.798867,
          longitude: 151.1805156666667,
        },
        {
          timestamp: 1537774728000,
          latitude: -33.79887316666667,
          longitude: 151.1805215555556,
        },
        {
          timestamp: 1537774729000,
          latitude: -33.79887933333333,
          longitude: 151.1805274444444,
        },
        {
          timestamp: 1537774730000,
          latitude: -33.7988855,
          longitude: 151.1805333333333,
        },
        {
          timestamp: 1537774731000,
          latitude: -33.79889166666667,
          longitude: 151.1805392222222,
        },
        {
          timestamp: 1537774732000,
          latitude: -33.79889783333333,
          longitude: 151.1805451111111,
        },
      ],
    },
    {
      startTimestamp: 1537774836000,
      endTimestamp: 1537774925000,
      type: 'PADDLE',
      distance: 87.55,
      speedMax: 9.33,
      speedAverage: 3.51,
      positions: [
        {
          timestamp: 1537774836000,
          latitude: -33.796558,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774837000,
          latitude: -33.796537,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774838000,
          latitude: -33.796517,
          longitude: 151.184829,
        },
        {
          timestamp: 1537774839000,
          latitude: -33.796498,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774840000,
          latitude: -33.796479,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774841000,
          latitude: -33.796463,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774842000,
          latitude: -33.79645,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774843000,
          latitude: -33.796439,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774844000,
          latitude: -33.796432,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774845000,
          latitude: -33.796427,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774846000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774847000,
          latitude: -33.796425,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774848000,
          latitude: -33.796426,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774849000,
          latitude: -33.796429,
          longitude: 151.184827,
        },
        {
          timestamp: 1537774850000,
          latitude: -33.796433,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774851000,
          latitude: -33.796437,
          longitude: 151.184824,
        },
        {
          timestamp: 1537774852000,
          latitude: -33.796441,
          longitude: 151.184826,
        },
        {
          timestamp: 1537774853000,
          latitude: -33.796443,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774854000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774855000,
          latitude: -33.796444,
          longitude: 151.184828,
        },
        {
          timestamp: 1537774856000,
          latitude: -33.796444,
          longitude: 151.18483,
        },
        {
          timestamp: 1537774857000,
          latitude: -33.796443,
          longitude: 151.184831,
        },
        {
          timestamp: 1537774858000,
          latitude: -33.796441,
          longitude: 151.184835,
        },
        {
          timestamp: 1537774859000,
          latitude: -33.796438,
          longitude: 151.184842,
        },
        {
          timestamp: 1537774860000,
          latitude: -33.796435,
          longitude: 151.184851,
        },
        {
          timestamp: 1537774861000,
          latitude: -33.796429,
          longitude: 151.184866,
        },
        {
          timestamp: 1537774862000,
          latitude: -33.796423,
          longitude: 151.184883,
        },
        {
          timestamp: 1537774863000,
          latitude: -33.796417,
          longitude: 151.184902,
        },
        {
          timestamp: 1537774864000,
          latitude: -33.79640777777777,
          longitude: 151.1849244444444,
        },
        {
          timestamp: 1537774865000,
          latitude: -33.79639855555556,
          longitude: 151.1849468888889,
        },
        {
          timestamp: 1537774866000,
          latitude: -33.79638933333333,
          longitude: 151.1849693333333,
        },
        {
          timestamp: 1537774867000,
          latitude: -33.79638011111111,
          longitude: 151.1849917777778,
        },
        {
          timestamp: 1537774868000,
          latitude: -33.79637088888889,
          longitude: 151.1850142222222,
        },
        {
          timestamp: 1537774869000,
          latitude: -33.79636166666667,
          longitude: 151.1850366666667,
        },
        {
          timestamp: 1537774870000,
          latitude: -33.79635244444444,
          longitude: 151.1850591111111,
        },
        {
          timestamp: 1537774871000,
          latitude: -33.79634322222223,
          longitude: 151.1850815555555,
        },
        {
          timestamp: 1537774872000,
          latitude: -33.796334,
          longitude: 151.185104,
        },
        {
          timestamp: 1537774873000,
          latitude: -33.796347,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774874000,
          latitude: -33.796354,
          longitude: 151.185091,
        },
        {
          timestamp: 1537774875000,
          latitude: -33.796361,
          longitude: 151.185086,
        },
        {
          timestamp: 1537774876000,
          latitude: -33.796368,
          longitude: 151.185077,
        },
        {
          timestamp: 1537774877000,
          latitude: -33.796373,
          longitude: 151.185071,
        },
        {
          timestamp: 1537774878000,
          latitude: -33.796375,
          longitude: 151.185068,
        },
        {
          timestamp: 1537774879000,
          latitude: -33.796378,
          longitude: 151.185065,
        },
        {
          timestamp: 1537774880000,
          latitude: -33.796382,
          longitude: 151.185059,
        },
        {
          timestamp: 1537774881000,
          latitude: -33.796388,
          longitude: 151.185053,
        },
        {
          timestamp: 1537774882000,
          latitude: -33.796393,
          longitude: 151.185045,
        },
        {
          timestamp: 1537774883000,
          latitude: -33.796397,
          longitude: 151.185036,
        },
        {
          timestamp: 1537774884000,
          latitude: -33.796402,
          longitude: 151.185029,
        },
        {
          timestamp: 1537774885000,
          latitude: -33.796406,
          longitude: 151.185022,
        },
        {
          timestamp: 1537774886000,
          latitude: -33.796411,
          longitude: 151.185016,
        },
        {
          timestamp: 1537774887000,
          latitude: -33.796416,
          longitude: 151.185008,
        },
        {
          timestamp: 1537774888000,
          latitude: -33.79641775,
          longitude: 151.1849945,
        },
        {
          timestamp: 1537774889000,
          latitude: -33.7964195,
          longitude: 151.184981,
        },
        {
          timestamp: 1537774890000,
          latitude: -33.79642125,
          longitude: 151.1849675,
        },
        {
          timestamp: 1537774891000,
          latitude: -33.796423,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774892000,
          latitude: -33.79642475,
          longitude: 151.1849405,
        },
        {
          timestamp: 1537774893000,
          latitude: -33.7964265,
          longitude: 151.184927,
        },
        {
          timestamp: 1537774894000,
          latitude: -33.79642825,
          longitude: 151.1849135,
        },
        {
          timestamp: 1537774895000,
          latitude: -33.79643,
          longitude: 151.1849,
        },
        {
          timestamp: 1537774896000,
          latitude: -33.796429,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774897000,
          latitude: -33.796428,
          longitude: 151.184896,
        },
        {
          timestamp: 1537774898000,
          latitude: -33.796425,
          longitude: 151.184898,
        },
        {
          timestamp: 1537774899000,
          latitude: -33.796423,
          longitude: 151.184905,
        },
        {
          timestamp: 1537774900000,
          latitude: -33.796423,
          longitude: 151.18491,
        },
        {
          timestamp: 1537774901000,
          latitude: -33.796423,
          longitude: 151.184915,
        },
        {
          timestamp: 1537774902000,
          latitude: -33.796423,
          longitude: 151.184919,
        },
        {
          timestamp: 1537774903000,
          latitude: -33.796423,
          longitude: 151.184924,
        },
        {
          timestamp: 1537774904000,
          latitude: -33.796424,
          longitude: 151.184932,
        },
        {
          timestamp: 1537774905000,
          latitude: -33.796427,
          longitude: 151.18494,
        },
        {
          timestamp: 1537774906000,
          latitude: -33.796431,
          longitude: 151.184945,
        },
        {
          timestamp: 1537774907000,
          latitude: -33.796435,
          longitude: 151.184948,
        },
        {
          timestamp: 1537774908000,
          latitude: -33.79644,
          longitude: 151.18495,
        },
        {
          timestamp: 1537774909000,
          latitude: -33.796445,
          longitude: 151.184952,
        },
        {
          timestamp: 1537774910000,
          latitude: -33.79645,
          longitude: 151.184954,
        },
        {
          timestamp: 1537774911000,
          latitude: -33.796455,
          longitude: 151.184955,
        },
        {
          timestamp: 1537774912000,
          latitude: -33.796458,
          longitude: 151.184956,
        },
        {
          timestamp: 1537774913000,
          latitude: -33.796461,
          longitude: 151.184957,
        },
        {
          timestamp: 1537774914000,
          latitude: -33.796464,
          longitude: 151.184959,
        },
        {
          timestamp: 1537774915000,
          latitude: -33.796467,
          longitude: 151.184961,
        },
        {
          timestamp: 1537774916000,
          latitude: -33.796469,
          longitude: 151.184964,
        },
        {
          timestamp: 1537774917000,
          latitude: -33.796471,
          longitude: 151.184967,
        },
        {
          timestamp: 1537774918000,
          latitude: -33.796471,
          longitude: 151.184969,
        },
        {
          timestamp: 1537774919000,
          latitude: -33.79647,
          longitude: 151.184972,
        },
        {
          timestamp: 1537774920000,
          latitude: -33.796468,
          longitude: 151.184975,
        },
        {
          timestamp: 1537774921000,
          latitude: -33.796466,
          longitude: 151.184977,
        },
        {
          timestamp: 1537774922000,
          latitude: -33.796464,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774923000,
          latitude: -33.796461,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774924000,
          latitude: -33.796457,
          longitude: 151.184978,
        },
        {
          timestamp: 1537774925000,
          latitude: -33.796451,
          longitude: 151.18497,
        },
      ],
    },
  ],
};
