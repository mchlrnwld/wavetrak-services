# Sessions API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Developing](#developing)
  - [Posting Data](#posting-data)
- [Deploying and Acessing Endpoints](#deploying-and-acessing-endpoints)
- [Conventions](#conventions)
  - [Best Practices & Conventions](#best-practices--conventions)
- [Data Sources and External Services](#data-sources-and-external-services)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


The Sessions API supports user submitted session details from 3rd party integration partners and Surfline products.

## Developing

To start developing

* copy `.env.sample` to `.env` and adjust environment variables to your local computer
* run `npm run startlocal` to start the development server
* `npm run test` will run unit and pact contract tests
* `sh integration_tests.sh` will run integration tests

### Posting Data

You need some data to post, fortunately there's an example json file in the `session-analyzer` services, so copy that:

```bash
cp ../session-analyzer/fixtures/data_test.json ./data.json
```

With the access token you can post the below:

```bash
curl -d "@data.json" -H "Content-Type: application/json" -H 'X-Auth-UserId: 5a60dd3872c573001155a535' -H 'x-api-key: foobar' -X POST http://localhost:8081/sessions/gps
```

## Deploying and Acessing Endpoints

The [deploy-service-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/) can be used to deploy the sessions api. Select `sessions-api` under the dropdown.

From inside the vpn the service can be accessed via `http://sessions-api.[env].surfline.com`. It's also mounted on the platform proxy at `https://platform.surfline.com`.

## Conventions

The following headers are injected into each request when made from the platform proxy.

| Header          | Description                                                                       |
| --------------- | --------------------------------------------------------------------------------- |
| `X-Auth-UserId` | The userId of the user initiating request. All requests will container a user id. |
| `X-Auth-Scopes` | A comma separated list of scopes that the request is allowed to execute.          |

### Best Practices & Conventions

* When making database operations use the request's user id to ensure only document belonging to the user are modified.
* Pull environment variable through `src/config.js` if you need them in code.
* Use `node-fetch` for making http requests. http requests to internal microservices should go direct to the service, not through the service proxy.

## Data Sources and External Services

* The Sessions API stores data in the `Sessions` Mongo database


## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `sessions-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TURFNE5qSXdNemcifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE1ERTROakl3TXpnIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the following endpoint. 

**Note:** An `X-Auth-UserId` must be passed in the header to perform the following `curl` request. This value corresponds to the `_id` value in the Mongo `userInfo` collection.

An `X-API-Key` value is also required. This corresponds to the `_id` value in the Mongo `UserDB.ClientIds`  collection. (Multiple client types will work, such as the `Surfline iOS Native App`).

**Ex:**

Test `GET` requests against the `/sessions/feed` endpoint:

```
curl \
	-X GET \
   -H 'X-Auth-UserId: 5cb8c278a9680b000ffc68be' \
   -H 'x-api-key: foobar' \
    http://sessions-api.prod.surfline.com/sessions/feed
```

**Expected response:** The response takes the form:

```
{
  "associated": {
    "units": {
      "surfHeight": "M",
      "swellHeight": "M",
      "tideHeight": "M",
      "windSpeed": "KPH",
      "temperature": "C",
      "distance": "M",
      "speed": "KPH"
    },
    "timezone": null
  },
  "sessions": []
}
``` 