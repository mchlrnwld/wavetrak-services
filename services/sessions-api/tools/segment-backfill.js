import fs from 'fs';
import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import Session from '../src/models/Session';
import * as analytics from '../src/common/analytics';
import wait from '../src/utils/wait';
import { getSessionEvents } from '../src/external/aws';

const fireAddedSessionEvent = session => {
  let startLatLon = '';
  const positions = session?.events?.[0]?.positions;
  if (positions.length) {
    const latLonArr = [positions[0].location.coordinates[0], positions[0].location.coordinates[1]];
    startLatLon = latLonArr.join(',');
  }
  analytics.track({
    event: 'Added Session',
    userId: session.user.toString(),
    properties: {
      sessionId: session._id.toString(),
      client: session.client.toString(),
      waveCount: session.waveCount,
      maxSpeed: session.speedMax,
      maxDistance: session.distanceLongestWave,
      startLatLon,
      startTimestamp: session.startTimestamp,
      stopTimestamp: session.endTimestamp,
    },
  });
};

const sendAddedSessionEvents = async () => {
  const allSessions = await Session.find()
    .select('_id')
    .lean()
    .exec();
  const sessionIds = allSessions.map(({ _id }) => _id);
  console.log('writing to sessionIds file');
  fs.writeFileSync(`./sessionIds.json`, JSON.stringify(sessionIds));

  const sessions = JSON.parse(fs.readFileSync(`./sessionIds.json`));

  while (sessions.length > 0) {
    try {
      /*  eslint-disable no-await-in-loop */
      const sessionId = sessions[0];
      const rawSession = await Session.findById(sessionId).lean();
      const rawEvents = await getSessionEvents(sessionId);
      console.log(sessions.length, sessionId);

      fireAddedSessionEvent({
        ...rawSession,
        events: rawEvents,
      });
      const index = sessions.indexOf(sessionId);
      if (index >= 0) {
        sessions.splice(index, 1);
      }
      fs.writeFileSync(`./sessionIds.json`, JSON.stringify(sessions));
      await wait(50);
      /*  eslint-enable no-await-in-loop */
    } catch (err) {
      console.log(err);
    }
  }
  return true;
};

const runScript = async () => {
  try {
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');
    await sendAddedSessionEvents();

    console.log('DONE SENDING ADDED SESSION FOR ALL EVENTS');
    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

runScript();
