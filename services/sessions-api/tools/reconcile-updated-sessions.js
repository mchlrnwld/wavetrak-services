/* eslint-disable no-await-in-loop */
import fs from 'fs';
import AWS from 'aws-sdk';
import mongoose from 'mongoose';
import { ServerError, initMongo } from '@surfline/services-common';
import Session, { EnrichedSession } from '../src/models/Session';
import wait from '../src/utils/wait';

const uploadSessionEvents = async (insertedId, events) => {
  const s3 = new AWS.S3({
    accessKeyId: process.env.ACCESS_KEY,
    secretAccessKey: process.env.SECRET_KEY,
  });

  const s3Params = {
    Body: JSON.stringify(events),
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${insertedId}.json`,
    ContentType: 'application/json',
    ContentEncoding: 'utf8',
  };

  try {
    return await s3.putObject(s3Params).promise();
  } catch (err) {
    throw new ServerError({
      message: `Could not upload session events for session ${insertedId} to S3`,
    });
  }
};
const errors = [];

const writeErrorsToFile = () => {
  fs.writeFileSync('./reconcile-script-errors.json', JSON.stringify(errors));
};

/**
 * This script is responsible for migrating session events from mongo to s3
 * @param {} errors
 */
const migrateEventsToS3 = async sessionIds => {
  // eslint-disable-next-line
  for (const sessionId of sessionIds) {
    const session = await EnrichedSession.findOne({ _id: sessionId }).lean();
    let { events } = session;
    try {
      const clips = [];
      const eventOverrides = new Map();
      events = events.map(event => {
        if (event.clips) {
          event.clips.forEach(clip => {
            clips.push({ _id: clip, eventId: event._id });
          });
        }
        const { favorite } = event;
        if (favorite) eventOverrides.set(event._id.toString(), { favorite });
        // Remove favorite
        delete event.favorite;
        delete event.clips;
        return event;
      });

      // Insert events to S3 with sessionId as filename
      await uploadSessionEvents(session._id, events);

      // Remove events from session doc
      // delete session.events;

      // We want to make the updatedAt date less than migratedAt in case we have to restart
      // the script
      const updatedAt = new Date(Date.now() - 1000 * 120);
      const migratedAt = new Date(Date.now());
      await EnrichedSession.updateOne(
        {
          _id: session._id,
        },
        {
          $set: {
            clips,
            eventOverrides,
            updatedAt,
            migratedAt,
          },
        },
        {
          timestamps: false,
        },
      ).exec();

      console.log('Processed session: ', session._id);
    } catch (error) {
      console.error(error);
      errors.push(session._id);
    }

    // Wait in milliseconds
    await wait(1500);
  }
  return true;
};

const getSessionsToUpdate = async () => {
  const sessionsToUpdate = await Session.aggregate([
    {
      $addFields: {
        isUpdated: { $cmp: ['$migratedAt', '$updatedAt'] },
      },
    },
    { $match: { isUpdated: 1 } },
    { $project: { isUpdated: 1 } },
  ]);
  console.log(`migrating ${sessionsToUpdate.length} sessions`);
  const sessionIds = sessionsToUpdate.map(({ _id }) => _id);
  await migrateEventsToS3(sessionIds, errors);
};

const runScript = async () => {
  try {
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');
    await getSessionsToUpdate();

    console.log('DONE WITH ALL BATCHES');
    writeErrorsToFile();
    process.exit(0);
  } catch (error) {
    console.log(error);
    writeErrorsToFile();
    process.exit(1);
  }
};

runScript();
