/* eslint-disable import/no-extraneous-dependencies */
require('@babel/register');
require('dotenv').config({ path: '.env.script' });

const run = (fn, options) => {
  const start = new Date();
  return fn(options).then(() =>
    console.log(`\nFinished after ${new Date().getTime() - start.getTime()} ms`),
  );
};

// Ensure the number of imported modules is only 2
// to prevent this importing any dangerous production code or anything
// unexpected
if (process.mainModule.children.length === 2 && process.argv.length > 2) {
  delete require.cache[__filename];
  const module = require(`./${process.argv[2]}.js`).default;
  run(module)
    .catch(err => console.error(err.stack))
    .then(() => process.exit(0));
}
