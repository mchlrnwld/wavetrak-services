/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
/* eslint-disable import/no-extraneous-dependencies */
import fs from 'fs';
import ProgressBar from 'progress';
import mongoose from 'mongoose';
import { isEmpty } from 'lodash';
import { initMongo, setupLogsene } from '@surfline/services-common';
import { EnrichedSession } from '../src/models/Session';
/** @type {Duplicates[]} */
import DuplicateSessions from './DuplicateSessions.json';

/**
 * @typedef {object} Session
 * @property {object?} eventOverrides
 * @property {string?} userSpotId
 * @property {string} name
 * @property {rating?} number
 * @property {string} _id
 * @property {string} createdAt
 */

/**
 * @typedef {object} Duplicates
 * @property {string} userId
 * @property {string[]} uniqueIds
 * @property {number} count
 * @property {string} duplicateId
 */

const DUPLICATES_TO_PROCESS = process.env.DUPLICATES_TO_PROCESS
  ? parseInt(process.env.DUPLICATES_TO_PROCESS, 10)
  : 1;

const DEFAULT_SESSION_NAMES = [
  'Surf Session',
  'Dawn Patrol',
  'Morning Shift',
  'Lunch Break',
  'Afternoon Session',
  'Evening Surf',
];

/**
 * @param {ProgressBar} bar
 * @param {string} userId
 * @param {mongoose.ObjectId[]} sessionIds
 */
const flagDuplicateSession = async (bar, userId, sessionIds) => {
  bar.interrupt(`Processing ${sessionIds.length} duplicate sessions for ${userId}`);

  /** @type {Session[]} */
  const sessions = await EnrichedSession.find({ _id: { $in: sessionIds } })
    .select({ eventOverrides: 1, userSpotId: 1, name: 1, rating: 1, _id: 1, createdAt: 1 })
    .sort({ createdAt: 1 })
    .lean();

  const hasEventOverrides = session => session.eventOverrides && !isEmpty(session.eventOverrides);
  const hasChangedSpot = session => session.userSpotId;
  const hasRating = session => session.rating;
  const hasChangedStatus = session => session.status !== 'ENRICHED';
  const hasChangedName = session => !DEFAULT_SESSION_NAMES.includes(session.name);

  const isSessionModified = session => {
    const isModified = [
      hasChangedSpot,
      hasEventOverrides,
      hasRating,
      hasChangedName,
      hasChangedStatus,
    ]
      .map(func => func(session))
      .filter(Boolean);
    return isModified.length >= 1;
  };

  const hasModifiedSessions = sessions.some(isSessionModified);

  let sessionsToUpdate;

  // check if list of duplicated sessions has any sessions with
  //  - eventOverrides
  //  - userSpotId
  //  - rating
  //  - A status that is not `ENRICHED`
  //  - name that is different than default
  //
  //  if it does filter those sessions and then update all the rest to be DELETED with added flag
  if (hasModifiedSessions) {
    sessionsToUpdate = sessions
      .filter(session => !isSessionModified(session))
      .map(session => mongoose.Types.ObjectId(session._id));
  } else {
    //  If there are no sessions that have been modified (see above), find the oldest `createdAt`
    //  (first element since we sorted the query output with ascending createdAt) and filter
    //  it out, update all the others to have `status: DELETED` and duplicate flag
    sessionsToUpdate = sessions.slice(1).map(session => mongoose.Types.ObjectId(session._id));
  }

  bar.interrupt(`Flagging ${sessionsToUpdate.length} sessions for deletion`);

  for (const _id of sessionsToUpdate) {
    try {
      await EnrichedSession.updateOne(
        { _id },
        {
          $set: {
            status: 'DELETED',
            isDuplicateSession: true,
          },
        },
        { strict: false },
      );
      fs.appendFileSync('./soft-deleted-sessions.txt', `${_id}\n`);
    } catch (error) {
      bar.interrupt(error);
      fs.appendFileSync('./failed-duplicates.txt', `${_id} - ${error.message}\n`);
    }
  }
};

const runScript = async () => {
  const processedDuplicatesFilePath = './tools/ProcessedDuplicates.txt';
  const fileExists = fs.existsSync(processedDuplicatesFilePath);

  const alreadyProcessedDuplicates = fileExists
    ? fs.readFileSync(processedDuplicatesFilePath, { encoding: 'UTF-8' }).split(/\r?\n/)
    : [];

  let processed = 0;
  const bar = new ProgressBar('[:bar] :current :percent :etas', {
    total:
      DUPLICATES_TO_PROCESS > DuplicateSessions.length
        ? DuplicateSessions.length
        : DUPLICATES_TO_PROCESS,
  });

  try {
    await setupLogsene('fakekey');
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');

    for (const duplicate of DuplicateSessions) {
      if (processed >= DUPLICATES_TO_PROCESS) {
        break;
      }
      const { duplicateId, userId } = duplicate;

      if (alreadyProcessedDuplicates.includes(duplicateId)) {
        bar.interrupt(`Skipping already processed duplicate: ${duplicateId}`);
      } else {
        bar.interrupt(`Processing duplicate: ${duplicateId}`);
        const sessionIds = duplicate.uniqueIds;

        await flagDuplicateSession(
          bar,
          userId,
          sessionIds.map(_id => mongoose.Types.ObjectId(_id)),
        );

        fs.appendFileSync(processedDuplicatesFilePath, `${duplicateId}\n`);
        bar.tick();
        processed += 1;
      }
    }

    bar.interrupt('script done');
  } catch (err) {
    bar.interrupt(err);
    process.exit(1);
  }
};

export default runScript;
