/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
import fs from 'fs';
import AWS from 'aws-sdk';
import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import Session from '../src/models/Session';
import {
  convertEventsToPositions,
  calculateSessionGPSCenter,
} from '../src/utils/calculateSessionGPSCenter';

const s3 = new AWS.S3();
const bytesToMegaBytes = bytes => bytes / (1024 * 1024);

const getSessionEvents = async sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    const response = await s3.getObject(s3Params).promise();
    return { events: JSON.parse(response.Body), size: response.ContentLength };
  } catch (err) {
    throw new Error("Couldn't retrieve events");
  }
};

const backfillGPSCenter = async () => {
  const matchedSessions = await Session.find({ 'gpsCenter.coordinates.0': { $exists: false } })
    .select('_id')
    .lean()
    .exec();

  console.log(`Processing ${matchedSessions.length} sessions`);
  const sessionIds = matchedSessions.map(({ _id }) => _id);

  for (let i = 0; i < sessionIds.length; i += 1) {
    const sessionId = sessionIds[i];

    try {
      let hasLargeEventsPayload = false;
      console.log(`Processing session: ${sessionId}`);

      const { size, events } = await getSessionEvents(sessionId);

      console.log(`events size: ${bytesToMegaBytes(size)}`);

      if (bytesToMegaBytes(size) > 10) {
        hasLargeEventsPayload = true;
      }

      const sessionPositions = convertEventsToPositions(events);

      const gpsCenter = calculateSessionGPSCenter(sessionPositions);

      await Session.collection.updateOne(
        { _id: mongoose.Types.ObjectId(sessionId) },
        {
          $set: {
            gpsCenter: {
              type: 'Point',
              coordinates: [gpsCenter.longitude, gpsCenter.latitude],
            },
            // Flag sessions with large events payloads for easier querying
            ...(hasLargeEventsPayload && { largeEventsPayload: true }),
          },
        },
      );
    } catch (error) {
      console.log(error);
      fs.appendFileSync('failed-sessions.txt', `${sessionId}\n`);
    }
  }
  return true;
};

const runScript = async () => {
  try {
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');
    await backfillGPSCenter();

    console.log('script done');
    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

runScript();
