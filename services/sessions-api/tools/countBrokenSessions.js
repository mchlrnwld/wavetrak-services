/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
import fs from 'fs';
import AWS from 'aws-sdk';
import ProgressBar from 'progress';
import mongoose from 'mongoose';
import { initMongo, setupLogsene } from '@surfline/services-common';
import Session from '../src/models/Session';

const s3 = new AWS.S3();

const getEventsExists = async (bar, sessionId) => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    bar.interrupt(`Processing session: ${sessionId}`);
    const result = await s3.headObject(s3Params).promise();
    bar.interrupt(`Found with size: ${result.ContentLength}`);
  } catch (err) {
    throw new Error("Couldn't retrieve events");
  }
};

const countBrokenSessions = async () => {
  const numDocs = await Session.collection.countDocuments();
  const bar = new ProgressBar('[:bar] :current :percent :etas', {
    total: numDocs,
  });
  bar.interrupt(`processing ${numDocs}`);
  await Session.find()
    .select('_id')
    .lean()
    .cursor()
    .eachAsync(async session => {
      bar.tick();
      try {
        await getEventsExists(bar, session._id);
      } catch (error) {
        bar.interrupt('No events found for session:', session._id);
        fs.appendFileSync('./missingevents.txt', `${session._id}\n`);
      }
    });
};

const runScript = async () => {
  try {
    setupLogsene('dummykey');
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');
    await countBrokenSessions();

    console.log('script done');
    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

runScript();
