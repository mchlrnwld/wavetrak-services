/* eslint-disable no-await-in-loop */
import fs from 'fs';
import AWS from 'aws-sdk';
import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import Session, { EnrichedSession } from '../src/models/Session';
import wait from '../src/utils/wait';

const s3 = new AWS.S3({
  accessKeyId: process.env.ACCESS_KEY,
  secretAccessKey: process.env.SECRET_KEY,
});

const uploadSessionEvents = async (insertedId, events) => {
  const s3Params = {
    Body: JSON.stringify(events),
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${insertedId}.json`,
    ContentType: 'application/json',
    ContentEncoding: 'utf8',
  };
  const res = await s3.putObject(s3Params).promise();
  return res;
};

/**
 * This script is responsible for migrating session events from mongo to s3
 * @param {} errors
 */
const migrateEventsToS3 = async (limit, errors) => {
  const sessions = await Session.find({
    migratedAt: { $exists: false },
  })
    .limit(limit)
    .lean();

  // eslint-disable-next-line
  for (let session of sessions) {
    let { events } = session;
    try {
      const clips = [];
      const eventOverrides = new Map();
      events = events.map(event => {
        if (event.clips) {
          event.clips.forEach(clip => {
            clips.push({ _id: clip, eventId: event._id });
          });
        }
        const { favorite } = event;
        if (favorite) eventOverrides.set(event._id.toString(), { favorite });
        // Remove favorite
        delete event.favorite;
        delete event.clips;
        return event;
      });

      // Insert events to S3 with sessionId as filename
      await uploadSessionEvents(session._id, events);

      // Remove events from session doc
      // delete session.events;

      await EnrichedSession.findOneAndUpdate(
        {
          _id: session._id,
        },
        {
          $set: {
            clips,
            eventOverrides,
            updatedAt: new Date(),
            migratedAt: new Date(),
          },
        },
      ).exec();

      console.log('Processed session: ', session._id);
    } catch (error) {
      console.error(error);
      errors.push(session._id);
    }

    // Wait in milliseconds
    await wait(25);
  }
  await wait(5000);
  return true;
};

const batchMigrateEventsToS3 = async limit => {
  const totalSessions = await Session.find({
    migratedAt: { $exists: false },
  }).countDocuments();
  const migrationRuns = Math.ceil(totalSessions / limit);
  console.log({ totalSessions, migrationRuns });
  const runMigration = run => async cb => {
    if (run > 0) {
      await cb();
      console.log(`Completed run ${run}`);
      await runMigration(run - 1)(cb);
    } else {
      console.log('DONE MIGRATING');
      process.exit(0);
    }
  };
  await runMigration(migrationRuns)(async () => {
    const errors = [];
    await migrateEventsToS3(limit, errors);
    if (errors) {
      fs.appendFileSync(`./prod-migration-script.json`, JSON.stringify(errors));
    }
    return true;
  });
};

const runScript = async () => {
  try {
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');
    await batchMigrateEventsToS3(process.env.SESSIONS_BATCH_SIZE);

    console.log('DONE WITH ALL BATCHES');
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runScript();
