import fs from 'fs';
import mongoose from 'mongoose';
import { initMongo, setupLogsene } from '@surfline/services-common';
import Session from '../src/models/Session';

const aggregationQuery = [
  {
    $group: {
      _id: {
        startTimestamp: '$startTimestamp',
        endTimestamp: '$endTimestamp',
        user: '$user',
        waveCount: '$waveCount',
        gpsCenter: '$gpsCenter',
        client: '$client',
      },
      uniqueIds: {
        $addToSet: '$_id',
      },
      count: {
        $sum: 1.0,
      },
    },
  },
  {
    $match: {
      count: {
        $gt: 1.0,
      },
    },
  },
  {
    $sort: {
      count: -1.0,
    },
  },
];

const runScript = async () => {
  try {
    await setupLogsene('fakekey');
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');

    const duplicateSessions = await Session.aggregate(aggregationQuery).allowDiskUse(true);

    const duplicateSessionsWithUniqueIds = duplicateSessions.map(duplicate => ({
      ...duplicate,
      duplicateId: mongoose.Types.ObjectId(),
    }));

    fs.writeFileSync(
      './tools/DuplicateSessions.json',
      JSON.stringify(duplicateSessionsWithUniqueIds, null, 2),
    );
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

export default runScript;
