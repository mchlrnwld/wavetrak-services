import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import { EnrichedSession } from '../src/models/Session';

/**
 * This script is responsible for finding any sessions that were affected by a bug
 * in the PUT /sessions endpoint which was stripping clips arrays from the mongo document.
 * In all these sessions we run them through the EnrichedSession model and update the date,
 * which will also add back all of the model properties to the document.
 * @param {*} errors
 */
const reAddClipsArray = async errors => {
  const sessions = await EnrichedSession.find({
    'events.clips': { $exists: false },
    status: 'ENRICHED',
    cams: { $exists: true },
    'events.type': { $in: ['WAVE'] },
  })
    .lean()
    .select('-updatedAt');

  // sessions = sessions.slice(sessions.length - 2);
  // eslint-disable-next-line
  for (let session of sessions) {
    try {
      // eslint-disable-next-line
      const updatedSession = await EnrichedSession.findOneAndUpdate(
        {
          _id: session._id,
          user: mongoose.Types.ObjectId(session.user),
        },
        {
          ...session,
          updatedAt: new Date(),
        },
      ).exec();

      console.log('Processed session: ', session._id);
    } catch (error) {
      console.error(error);
      errors.push(session._id);
    }
  }
};

const runScript = async () => {
  try {
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');
    const errors = [];
    await reAddClipsArray(errors);

    console.log(errors);
    console.log('DONE');
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runScript();
