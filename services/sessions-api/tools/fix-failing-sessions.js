/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
import fs from 'fs';
import AWS from 'aws-sdk';
import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import Session from '../src/models/Session';

const s3 = new AWS.S3();
const bytesToMegaBytes = bytes => bytes / (1024 * 1024);

const getSessionEvents = async sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    const response = await s3.getObject(s3Params).promise();
    return response.ContentLength;
  } catch (err) {
    throw new Error("Couldn't retrieve events");
  }
};

const deleteSessionEvents = async sessionId => {
  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    const response = await s3.deleteObject(s3Params).promise();
    return response;
  } catch (err) {
    throw new Error('Could not delete session events for: ', sessionId);
  }
};

const fixFailingSessions = async () => {
  const failedSessions = await Session.find({ status: 'FAILED' })
    .select('_id')
    .lean()
    .exec();

  const sessionIds = failedSessions.map(({ _id }) => _id);

  console.log(`Processing ${sessionIds.length} sessions`);

  for (let i = 0; i < sessionIds.length; i += 1) {
    const sessionId = sessionIds[i];

    try {
      console.log(`Processing session: ${sessionId}`);

      const eventsSize = await getSessionEvents(sessionId);

      console.log(`events size: ${bytesToMegaBytes(eventsSize)}`);

      if (bytesToMegaBytes(eventsSize) > 10) {
        await deleteSessionEvents(sessionId);
        await Session.findOneAndRemove({ _id: sessionId }).exec();
        fs.appendFileSync('removed-sessions.txt', `${sessionId}\n`);
      }
    } catch (error) {
      console.log(error);
      fs.appendFileSync('failed-sessions.txt', `${sessionId}\n`);
    }
  }
};

const runScript = async () => {
  try {
    await initMongo(mongoose, process.env.MONGO_CONNECTION_STRING_SESSIONS, 'sessions-api');

    await fixFailingSessions();

    console.log('script done');
    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

runScript();
