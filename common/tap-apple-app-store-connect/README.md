# tap-apple-app-store-connect

Author: Greg Clunies

A [Singer.io](http://singer.io) tap that extracts [sales and finance reports](https://developer.apple.com/documentation/appstoreconnectapi/sales_and_finance_reports) data for any App on the Apple App store and produces JSON-formatted data following the [Singer spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md#schema-message).

## Known Apple 🍎 Data Issues
There is a known data issue where the subscriptions report data recieved from the App Store Connect API will "split" what should be a single row of data into two rows. This seems to be related to extra whitespace (' ') in the`Proceeds Currency` column. See the [`Apple_data_issue.csv`](./docs/Apple_data_issue.csv) file included in this repo to better understand this issue. This has downstream effects on identifying primary keys for each report type and how the tap prevents duplicate data from reaching Stitch destinations (e.g. `target-stitch`). See the next section for more details.

## Preventing Duplicate Data & Primary Keys
`tap-apple-app-store-connect` uses `key_properties` to indicate the primary key for a stream. Currently, `tap-apple-app-store-connect` builds a single primary key using an `md5()` hash of ___ALL___ the columns included in a given report. This has the following consequences:
1. Duplicate data is never sent by Stitch to downstream destinations.
1. UPSERT functionality is __NOT__ supported by this tap. It is _extremely_ rare (1-2 times in 5 years) for Apple to update any previously reported data, so this functionality is not a priority. _Preventing duplicate data is a proiority_.

Additionally, the only way to know if Apple has updated the data, or that we have received "bad" data from Apple is:
    - Apple tells us (you) that there is bad data that they will/have update(d).
    - The error in data is so obvious that it is visually apparent in downstream dashboards/charts/tools and we can plainly see that the data should be removed and retrieved again.

## Compatibility with Singer Targets
A summary of targets likely to be used with `tap-apple-app-store-connect` is provided below. Testing of each target is ongoing. 

| Target             | Can UPSERT? | Tested | 
|--------------------|:-----------:|:------:|
| `target-stitch`    | ❌          | ✅     |
| `target-postgres`  | ❌          | ❌     |
| `target-redshift`  | ❌          | ❌     |
| `target-snowflake` | ❌          | ❌     |
| `target-snowflake` | ❌          | ❌     |
| `target-csv`       | ❌          | ❌     |
| `target-gsheet`    | ❌          | ❌     |

The default replication method for `tap-apple-app-store-connect` is `INCREMENTAL` (only sends new data from App Store Connect API). `FULL_TABLE` replication is not supported.

### Supported App Store Connect Report Types:
A complete list of available report types is provided by Apple:
- [Finance reports](https://developer.apple.com/documentation/appstoreconnectapi/download_finance_reports)
    - FINANCIAL
- [Sales and Trend reports](https://developer.apple.com/documentation/appstoreconnectapi/download_sales_and_trends_reports) (e.g., subscriptions data)
    - SALES
    - SUBSCRIBER
    - SUBSCRIPTION
    - SUBSCRIPTION_EVENT

### Untested App Store Connect Report Types and Streams
The following report types and their streams *have not* been tested (since they do not exist for Surfline/Wavetrak apps):
- PRE_ORDER report
- NEWSSTAND Report

## Datetime Format & Time Zone Conventions
All datetime stamps use [RFC3339](https://www.ietf.org/rfc/rfc3339.txt) formatted datetime strings. We use the [`pendulum`](https://pendulum.eustace.io/) Python package to handle the intricacies of datetimes and timezones.

### `config.json`, `STATE` messages, `state.json`
All Apple reports are based on Pacific Standard Time (PST/PDT). As a result, all datetime stamps specified in the configuration file (see [`example_tap_config.json`](./example_tap_config.json)), in `STATE` messages, and `state.json` should be specified and are written in Pacific Standard Time (PST/PDT). For example:
```
2020-02-01T00:00:00.000000-0800  # Pacific Time not during daylight saving time (PST)
2020-02-01T00:00:00.000000-0700  # Pacific Time during daylight saving time (PDT)
```

### `RECORD` messages
Most data sources and data warehouses use Coordinated Universal Time (UTC) so that datetime data from different sources can be compared easily. To conform to this best practice, all datetime stamps in `RECORD` messages are _converted_ to UTC. For example:
```
"begin_date": "2020-03-18T07:00:00.000000Z", "end_date": "2020-03-18T07:00:00.000000Z",
```
___Notice that:<br>___
`2020-03-18T07:00:00.000000Z` (time in UTC) = `2020-03-18T07:00:00.000000-7000` (time in PDT)


## Quick Start

### `tap-apple-app-store-connect` Setup

#### 1. Install
To run locally, clone this repo. Then once inside the repo directory, run:

```bash
pip install .
```

### 2. Get authentication credentials for App Store Connect API

See the [Apple Documentation on creating API keys for App Store Connect API](https://developer.apple.com/documentation/appstoreconnectapi/creating_api_keys_for_app_store_connect_api). 

If you want to be able to stream data for all report types, you will need to create 2 API keys; one with a Finance Role for [Finance reports](https://developer.apple.com/documentation/appstoreconnectapi/download_finance_reports), and the other with a Reports Role for [Sales and Trend reports](https://developer.apple.com/documentation/appstoreconnectapi/download_sales_and_trends_reports).

If you only need finance report data, then only create a API key with a Finance Role. Same approach if you only needs sales and trends reports data.

Download and save the Private Keys as outlined in the [Apple docs](https://developer.apple.com/documentation/appstoreconnectapi/creating_api_keys_for_app_store_connect_api). Save the Private key(s) in file(s) with extenion `.p8`. An example [Finance role key file](./example_app_store_connect_key_file_finance_role.p8) and [Reports role key file](example_app_store_connect_key_file_reports_role.p8) are included in this repository.

#### 3. Create the tap config file
There is a template in this repo [`example_tap_config.json`](./example_tap_config.json) that you can use. Just copy it into a `tap_config.json` file you create in the repo root and set the values for each property.

You can view your `vendor_number` at any time in App Store Connect. Vendor Numbers are needed for downloading reports using the App Store Connect API.
1. From the homepage, click Payments and Financial Reports.
1. Your Vendor Number appears in the top left hand corner under your Legal Entity Name.
![](./docs/vendor_number.png)

To get your `issuer_id`, log in to App Store Connect and:
1. Select Users and Access, then Select the API Keys tab.
1. The issuer ID appears near the top of the page. To copy the issuer ID, click Copy next to the ID.
<br>
<br>

### Running `tap-apple-app-store-connect`

#### 1. Run `tap-apple-app-store-connect` in DISCOVERY mode to generate a catalog
```
tap-apple-app-store-connect --config tap_config.json --discover > catalog.json
```

Once the catalog is generated, you will need to select the streams and their corresponding fields you would like the tap to process and stream. See the [Singer docs](https://github.com/singer-io/getting-started/blob/master/docs/DISCOVERY_MODE.md#metadata) on selecting streams and fields from a catalog using `"selected": true`. 

We highly recommend using [singer-discover](https://github.com/chrisgoddard/singer-discover) to speed up this process.

An [`example_catalog.json`](./example_catalog.json) without the selected streams/fields can be found in this repo.

#### 2. Run `tap-apple-app-store-connect` in SYNC mode

**1st tap run**<br>
Send data to your target (this example uses `target-stitch`) with the command below, save the final state emitted by the target for future runs.
```
tap-apple-app-store-connect --config tap_config.json --catalog catalog.json | target-stitch --config target_stitch_config.json > state.json

tail -1 state.json > state.json.tmp && mv state.json.tmp state.json
```

**Subsequent tap runs**<br>
Use `state.json` from 1st/previous run as an argument to the tap, which tells the tap where to begin streaming data from. An [`example_state.json`](./example_state.json) is included in this repo to help clarify what data this file provides to the tap.
```
tap-apple-app-store-connect --config tap_config.json --catalog catalog.json --state state.json | target-stitch --config target_config.json > state.json

tail -1 state.json > state.json.tmp && mv state.json.tmp state.json
```

## Replacing Data: running the tap for a single day 
If you become aware of a data issue and need replace bad data for a given date, you will need to:
1. Remove the rows of bad data from Redshift or your destination.
2. Retreive the single day of data using the `download_single_day` and `download_single_day_date_pst` fields in your [`tap_config.json`](example_tap_config.json) and run the tap _without_ passing a `state.json`. Do not save the `state.json` that the target emits when running the tap for a single day.
```bash
tap-apple-app-store-connect --config tap_config.json --catalog catalog.json | target-stitch --config target_stitch_config.json
```

## Development
If you wish to contribute to the development of this tap, this repo also includes:

1. [`environment.yml`](./environment.yml) that defines a `tap-apple-app-store-connect-dev` conda environment. After cloning the repo and navigating inside the repo, run the commands below to build and activate the environment.
```bash
conda env create --file environment.yml
conda activate tap-apple-app-store-connect-dev
pip install -e .
```
2. A [`Makefile`](./Makefile) with commands that you may find helpful during development. This will reduce having to type out commands to the shell each time. 

---
Copyright &copy; 2020 Surfline/Wavetrak, Inc.
