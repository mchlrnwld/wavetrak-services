ENV ?= dev

.PHONY: all
all: format lint type-check run-tap

.PHONY: format
format:
	@autoflake --remove-all-unused-imports --remove-unused-variables --ignore-init-module-imports --recursive --in-place .
	@isort -rc .
	@black .

.PHONY: lint
lint:
	@flake8 .
	@black --check .

.PHONY: type-check
type-check:
	@mypy .

# You may need to change the path below to tap-google-play-console depending on
# whether you have conda or anaconda3 as your package manager.
.PHONY: catalog
catalog:
	python tap_apple_app_store_connect/__init__.py --config wavetrak_tap_config.json --discover > example_catalog.json

# You may need to change the path below to tap-google-play-console depending on
# whether you have conda or anaconda3 as your package manager.
.PHONY: run-tap
run-tap:
	python tap_apple_app_store_connect/__init__.py --config wavetrak_tap_config.json --catalog catalog.json

.PHONY: run-tap-state
run-tap-state:
	python tap_apple_app_store_connect/__init__.py --config wavetrak_tap_config.json --state state.json --catalog catalog.json

.PHONY: deploy
deploy:
	@docker build -t tap-apple-app-store-connect .
	@docker run --rm -t -v ~/.pypirc:/root/.pypirc tap-apple-app-store-connect \
		python setup.py bdist_wheel upload -r "${ENV}"
