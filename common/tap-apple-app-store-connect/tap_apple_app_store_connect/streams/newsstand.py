from typing import List

import singer  # type: ignore

from tap_apple_app_store_connect.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class NewsstandStream(BaseStream):
    KEY_PROPERTIES: List[str] = ['id']
    REPLICATION_KEYS: List[str] = ['apple_report_datetime']
    REPLICATION_METHOD: str = 'INCREMENTAL'
    # API_METHOD: str = 'GET'  # Not needed. We use appstoreconnect wrapper
    TABLE: str = 'newsstand'
    APP_STORE_CONNECT_DATE_FORMAT: str = 'YYYY-MM-DD'  # Pendulum formatting
    REPORT_TYPE: str = 'NEWSSTAND'
    REPORT_SUBTYPE: str = 'DETAILED'
    FREQUENCY: str = 'DAILY'
    VERSION: str = '1_0'
