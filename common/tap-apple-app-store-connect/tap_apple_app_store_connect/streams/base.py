import hashlib
import json

import pendulum  # type: ignore
import singer  # type: ignore
import singer.metrics  # type: ignore
import singer.utils  # type: ignore
from appstoreconnect.api import APIError  # type: ignore
from tap_framework.streams import BaseStream as base  # type: ignore

from tap_apple_app_store_connect import metadata
from tap_apple_app_store_connect.client import AppStoreConnectClient
from tap_apple_app_store_connect.config import (
    check_config_for_single_day_download,
    get_config_single_day_download_date,
    get_config_start_date,
    get_config_vendor_number,
)
from tap_apple_app_store_connect.state import (
    get_last_report_date_value_for_table,
    save_state,
)
from tap_apple_app_store_connect.utils import (
    clean_finance_report_response,
    convert_date_strings_to_rfc3339,
    gen_dates,
    response_to_list_of_dicts,
)

LOGGER = singer.get_logger()
BOOKMARK_DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSSSSZZ'  # Pendulum formatting
TIMEZONE = 'America/Los_Angeles'

REPORT_NOT_AVAILABLE_ERROR = (
    'Report is not available yet. Daily reports for the Americas are available'
    ' by 5 am Pacific Time; Japan, Australia, and New Zealand by 5 am Japan'
    ' Standard Time; and 5 am Central European Time for all other territories.'
)
NO_DATA_DATE_SPECIFIED_ERROR = 'There were no sales for the date specified.'


class BaseStream(base):
    """ Base Stream Class for all streams defined in this tap. Each individual
    stream (e.g. SubscriptionStream) is a child of this BaseStream
    class.

    The following class variables are assigned at the child class level:
        - KEY_PROPERTIES
        - REPLICATION_KEYS
        - REPLICATION_METHOD
        - APP_STORE_CONNECT_DATE_FORMAT
        - REPORT_TYPE
        - REPORT_SUBTYPE
        - FREQUENCY
        - VERSION

    See child class 'SubscriptionStream' defined in subscription.py as an
    example.
    """

    # This method is modified from its definition in tap-framework so that
    # it includes a 'bookmark_properties' value in SCHEMA messages. See:
    # https://github.com/singer-io/getting-started/blob/bbb53c62e727b436e6952211b0761b76c0ccc41b/docs/SPEC.md#schema-message
    def write_schema(self):
        singer.write_schema(
            self.catalog.stream,
            self.catalog.schema.to_dict(),
            key_properties=self.catalog.key_properties,
            bookmark_properties=self.REPLICATION_KEYS,
        )

    # Redefine generate_catalog() stream class method (from tap-framework)
    # so that it writes more descriptive metadata for stream.
    def generate_catalog(self):
        schema = self.get_schema()
        mdata = singer.metadata.new()
        mdata = metadata.get_standard_metadata(
            schema=schema,
            key_properties=self.KEY_PROPERTIES,
            valid_replication_keys=self.REPLICATION_KEYS,
            replication_method=self.REPLICATION_METHOD,
        )
        mdata = metadata.to_map(mdata)
        mdata = singer.metadata.write(mdata, (), 'inclusion', 'available')

        for field_name, field_schema in schema.get('properties').items():
            mdata = singer.metadata.write(
                mdata,
                ('properties', field_name),
                'inclusion',
                'automatic'
                if field_name in self.KEY_PROPERTIES
                else 'available',
            )

        return [
            {
                'tap_stream_id': self.TABLE,
                'stream': self.TABLE,
                'key_properties': self.KEY_PROPERTIES,
                'schema': self.get_schema(),
                'metadata': singer.metadata.to_list(mdata),
            }
        ]

    def sync_data(self):
        LOGGER.info('Syncing data for entity {0}'.format(self.TABLE))

        # Check config to see if user specified a single day download.
        # If yes, then dates_to_fetch is just that single day.
        if check_config_for_single_day_download(self.config):
            dates_to_fetch_str = get_config_single_day_download_date(
                self.config
            ).format(self.APP_STORE_CONNECT_DATE_FORMAT)

            dates_to_fetch = [dates_to_fetch_str]
            filter_first_date = False

        # Otherwise, dates_to_fetch is all the days since last STATE
        else:
            # Get the STATE from state.json (if it exists)
            last_report_date = get_last_report_date_value_for_table(
                self.state, self.TABLE
            )
            filter_first_date = True

            # If no state.json, get start date from tap config file
            if last_report_date is None:
                last_report_date = get_config_start_date(self.config)
                filter_first_date = False

            # Generate the list of report dates to use in requests
            dates_to_fetch = gen_dates(
                last_report_date,
                self.APP_STORE_CONNECT_DATE_FORMAT,  # 'YYYY-MM-DD' unless financial, then 'YYYY-MM'  # noqa: E501
                time_zone='America/Los_Angeles',
            )

        if self.REPORT_TYPE == 'FINANCIAL':
            # Setup args object from CLI arguments for 2nd client used for
            # finance report requests
            args = singer.utils.parse_args(
                required_config_keys=[
                    'vendor_number',
                    'issuer_id',
                    'key_id_reports',
                    'path_to_key_file_reports',
                    'key_id_finance',
                    'path_to_key_file_finance',
                    'start_date_pst',
                ]
            )

            # Initialize a 2nd client using finance role key_id and key file
            self.client2 = AppStoreConnectClient(
                args.config['key_id_finance'],
                args.config['path_to_key_file_finance'],
                args.config['issuer_id'],
                submit_stats=False,  # Don't share any usage data
            )

            # Only need unique start of each month for financial requests
            dates_to_fetch = list(
                sorted(set(dates_to_fetch), key=dates_to_fetch.index)
            )

        # Remove 1st date from list if STATE is from state.json to avoid
        # duplicating data
        if filter_first_date:
            dates_to_fetch = dates_to_fetch[1:]

        # Get vendor number from config file for requests to App Store Connect
        vendor_number = str(get_config_vendor_number(self.config))

        for report_date in dates_to_fetch:
            LOGGER.info('Starting sync for {0}'.format(report_date))

            try:
                # If on 'FINANCIAL' report type, use download_finance_reports()
                if self.REPORT_TYPE == 'FINANCIAL':
                    response_dirty = self.client2.download_finance_reports(
                        filters={
                            'vendorNumber': vendor_number,
                            'reportType': self.REPORT_TYPE,
                            'regionCode': self.REGION_CODE,
                            'reportDate': report_date,
                        }
                    )

                    response = clean_finance_report_response(response_dirty)

                    # Convert report_date to RFC3339 datetime
                    report_datetime = pendulum.from_format(
                        report_date,
                        self.APP_STORE_CONNECT_DATE_FORMAT,
                        tz=TIMEZONE,
                    ).format(BOOKMARK_DATE_FORMAT)

                else:
                    # Other report types,
                    # use download_sales_and_trends_reports()
                    response = self.client.download_sales_and_trends_reports(
                        filters={
                            'vendorNumber': vendor_number,
                            'reportType': self.REPORT_TYPE,
                            'reportSubType': self.REPORT_SUBTYPE,
                            'frequency': self.FREQUENCY,
                            'version': self.VERSION,
                            'reportDate': report_date,
                        }
                    )

                    # Convert report_date to RFC3339 datetime
                    report_datetime = pendulum.from_format(
                        report_date,
                        self.APP_STORE_CONNECT_DATE_FORMAT,
                        tz=TIMEZONE,
                    ).format(BOOKMARK_DATE_FORMAT)

                # Convert API response into list of dicts to be looped through
                report_list_of_dicts, _ = response_to_list_of_dicts(response)

                # Write individual rows of data as RECORD messages to stdout
                for _, data_dict in enumerate(report_list_of_dicts):
                    # Convert date strings in data to RFC3339 datetime
                    data_dict_transformed = convert_date_strings_to_rfc3339(
                        data_dict, BOOKMARK_DATE_FORMAT, time_zone=TIMEZONE
                    )

                    # Add `apple_report_datetime` field to RECORD message, this
                    # logs what report date the RECORD message is associated
                    # with. Formatted as RFC3339 datetime
                    data_dict_transformed.update(
                        {'apple_report_datetime': report_datetime}
                    )

                    # Create a unique id
                    data_md5 = hashlib.md5(
                        json.dumps(
                            data_dict_transformed, sort_keys=True
                        ).encode()
                    ).hexdigest()
                    data_dict_transformed['id'] = data_md5

                    # .transform_record() is from tap-framework skeleton code.
                    # The resultant Transformer object handles any needed data
                    # type conversions (according to the defined schema for the
                    # stream) before writing them to stdout with
                    # singer.write_record()
                    data_dict_singer_spec = self.transform_record(
                        data_dict_transformed
                    )

                    singer.write_record(
                        self.TABLE,
                        data_dict_singer_spec,
                        time_extracted=pendulum.now(TIMEZONE),
                    )

                singer.write_bookmark(
                    self.state,
                    self.TABLE,
                    'last_report_date_pst',
                    report_datetime,
                )

                save_state(self.state)  # Similar to singer.write_state()

            except APIError as e:
                # Only except certain errors, see docs for error codes:
                # https://developer.apple.com/documentation/appstoreconnectapi/interpreting_and_handling_errors/about_the_http_status_code  # noqa
                # TODO: update to error codes once fixed in appstoreconnect
                if (
                    str(e) == REPORT_NOT_AVAILABLE_ERROR
                    or str(e) == NO_DATA_DATE_SPECIFIED_ERROR
                ):
                    LOGGER.warning('{0}'.format(e))
                else:
                    raise e
