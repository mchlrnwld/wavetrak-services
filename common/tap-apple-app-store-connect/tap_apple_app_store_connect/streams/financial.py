from typing import List

import singer  # type: ignore

from tap_apple_app_store_connect.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class FinancialStream(BaseStream):
    KEY_PROPERTIES: List[str] = ['id']
    REPLICATION_KEYS: List[str] = ['apple_report_datetime']
    REPLICATION_METHOD: str = 'INCREMENTAL'
    # API_METHOD: str = 'GET'  # Not needed. We use appstoreconnect wrapper
    TABLE: str = 'financial'
    APP_STORE_CONNECT_DATE_FORMAT: str = 'YYYY-MM'  # Pendulum formatting
    REPORT_TYPE: str = 'FINANCIAL'
    REGION_CODE: str = 'ZZ'
