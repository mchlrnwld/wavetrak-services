from tap_apple_app_store_connect.streams.financial import FinancialStream
from tap_apple_app_store_connect.streams.newsstand import NewsstandStream
from tap_apple_app_store_connect.streams.pre_order import PreOrderStream
from tap_apple_app_store_connect.streams.sales import SalesStream
from tap_apple_app_store_connect.streams.subscriber import SubscriberStream
from tap_apple_app_store_connect.streams.subscription import SubscriptionStream
from tap_apple_app_store_connect.streams.subscription_event import (
    SubscriptionEventStream,
)

AVAILABLE_STREAMS = [
    FinancialStream,
    NewsstandStream,
    PreOrderStream,
    SalesStream,
    SubscriberStream,
    SubscriptionEventStream,
    SubscriptionStream,
]

__all__ = [
    'FinancialStream',
    'NewsstandStream',
    'PreOrderStream',
    'SalesStream',
    'SubscriberStream',
    'SubscriptionEventStream',
    'SubscriptionStream',
]
