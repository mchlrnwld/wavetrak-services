from typing import List

import singer  # type: ignore

from tap_apple_app_store_connect.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class SubscriptionStream(BaseStream):
    KEY_PROPERTIES: List[str] = ['id']
    REPLICATION_KEYS: List[str] = ['apple_report_datetime']
    REPLICATION_METHOD: str = 'INCREMENTAL'
    # API_METHOD: str = 'GET'  # Not needed. We use appstoreconnect wrapper
    TABLE: str = 'subscription'
    APP_STORE_CONNECT_DATE_FORMAT: str = 'YYYY-MM-DD'  # Pendulum formatting
    REPORT_TYPE: str = 'SUBSCRIPTION'
    REPORT_SUBTYPE: str = 'SUMMARY'
    FREQUENCY: str = 'DAILY'
    VERSION: str = '1_2'
