"""This module contains a small collection of helper functions."""
import csv
from typing import Dict, List

import pendulum  # type: ignore


def gen_dates(
    current_state: str,
    app_store_connect_date_format: str,
    time_zone: str = 'America/Los_Angeles',
) -> List[str]:
    """
    Generates a list of dates.
    :param current_state: current state in %Y-%m-%d format
    :param time_zone: timezone; string
    :param app_store_connect_date_format: date format to be used when making request to App Store Connect; string  # noqa: E501
    :return: list of dates in format specified by app_store_connect_date_format
    """

    today_ = pendulum.today(tz=time_zone)
    last_day = pendulum.parse(current_state, tz=time_zone)
    delta = today_ - last_day
    d_days = delta.days

    dates = []

    for i in range(0, d_days + 1):
        dates.append(
            last_day.add(days=i).format(  # type: ignore
                app_store_connect_date_format
            )
        )

    # Ensure that today's date is not in the generated list of dates
    dates = [
        date
        for date in dates
        if date
        != today_.format(  # type: ignore
            app_store_connect_date_format
        )
    ]

    return dates


def convert_date_strings_to_rfc3339(
    data_dict: dict,
    bookmark_date_format: str,
    time_zone: str = 'America/Los_Angeles',
) -> Dict:
    rfc3339_dict = {
        key: pendulum.parse(  # type: ignore
            item, tz=time_zone, strict=False
        ).format(  # type: ignore
            bookmark_date_format
        )
        if 'date' in key.lower() and item
        else item
        for key, item in data_dict.items()
    }

    return rfc3339_dict


def response_to_list_of_dicts(response):
    lines = response.split('\n')
    header = [
        s.lower()
        .replace('(', '')
        .replace(')', '')
        .replace('/', '_')
        .replace(' ', '_')
        .replace('-', '_')
        for s in lines[0].split('\t')
    ]

    data = []
    for row in csv.reader(lines[1:], delimiter='\t'):
        if len(row) == 0:
            continue
        line_obj = {}
        for i, column in enumerate(header):
            if i < len(row):
                line_obj[column] = (
                    row[i].strip() if row[i].strip() != '' else None
                )
        data.append(line_obj)

    return data, header


def clean_finance_report_response(response):
    clean_response = response.split('Total_Rows')[0]
    return clean_response
