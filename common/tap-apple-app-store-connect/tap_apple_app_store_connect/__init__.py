#!/usr/bin/env python3

import singer  # type: ignore
import tap_framework  # type: ignore

from tap_apple_app_store_connect.client import AppStoreConnectClient
from tap_apple_app_store_connect.streams import AVAILABLE_STREAMS

LOGGER = singer.get_logger()  # noqa


class AppleAppStoreConnectRunner(tap_framework.Runner):
    pass


@singer.utils.handle_top_exception(LOGGER)
def main():
    args = singer.utils.parse_args(
        required_config_keys=[
            'vendor_number',
            'issuer_id',
            'key_id_reports',
            'path_to_key_file_reports',
            'key_id_finance',
            'path_to_key_file_finance',
            'start_date_pst',
        ]
    )

    # Setup the client using reports role key_id and key file as default.
    # Logic in ./streams/base.py handles initializing 2nd client using the
    # finance role key_id and key file to accesss finance reports (if
    # FinancialStream stream is selected).
    client = AppStoreConnectClient(
        args.config['key_id_reports'],
        args.config['path_to_key_file_reports'],
        args.config['issuer_id'],
        submit_stats=False,  # Don't share any usage data
    )

    runner = AppleAppStoreConnectRunner(args, client, AVAILABLE_STREAMS)

    if args.discover:
        runner.do_discover()

    else:
        runner.do_sync()


if __name__ == '__main__':
    main()
