import pendulum  # type: ignore
import singer  # type: ignore

LOGGER = singer.get_logger()  # noqa


def get_config_vendor_number(config):
    return config.get('vendor_number')


def get_config_start_date(config, tzinfo='America/Los_Angeles'):
    return config.get('start_date_pst')


def check_config_for_single_day_download(config):
    return config.get('download_single_day')


def get_config_single_day_download_date(config, tzinfo='America/Los_Angeles'):
    single_day_download_date = pendulum.parse(
        config.get('download_single_day_date_pst'), tz=tzinfo
    )
    return single_day_download_date
