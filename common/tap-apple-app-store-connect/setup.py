#!/usr/bin/env python

from setuptools import find_packages, setup  # type: ignore

setup(
    name="tap-apple-app-store-connect",
    version="0.0.14",
    description="Singer.io tap for extracting Apple App Store Connect data.",
    author="Greg Clunies",
    author_email="gclunies@surfline.com",
    url="https://github.com/Surfline/wavetrak-services/tree/master/common/tap-apple-app-store-connect",  # noqa : E501
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["tap_apple_app_store_connect"],
    install_requires=[
        "appstoreconnect==0.8.0",
        "pendulum>=2.0",
        "tap-framework==0.1.1",  # This handles singer-python dependency
    ],
    entry_points="""
          [console_scripts]
          tap-apple-app-store-connect=tap_apple_app_store_connect:main
      """,
    packages=find_packages(),
    package_data={"tap_apple_app_store_connect": ["schemas/*.json"]},
)
