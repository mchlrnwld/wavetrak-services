# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.14] - 2020-11-17
### Changed
- Changes the log level for `APIError` exceptions from `ERROR` to `WARNING`.

## [0.0.13] - 2020-11-16
### Changed
- Handle [`appstoreconnect`](https://github.com/Ponytech/appstoreconnectapi) `APIError`s so that they don't break Apple subscription ETL pipelines.
   - In our case @Surfline. Airflow jobs for [`apple-app-store-connect-etl-pipeline`](https://github.com/Surfline/wavetrak-services/tree/master/jobs/apple-app-store-connect-etl-pipeline) were failing due to an `APIError` encountered in `tap-apple-app-store-connect` when the App Store Connect API returns a response saying that no data was found for a requested report type on a given date. This was causing a premature exit during our ETL jobs.

## [0.0.12] - 2020-09-02
### Changed
- Fixed parsing of empty string in API response.
- Fixed data-type errors in stream schemas.

## [0.0.11] - 2020-08-28
### Changed
- Code cleanup.

## [0.0.10] - 2020-08-27
### Changed
- Remove `print()` and `exit()` that was being used for debugging (sigh).

## [0.0.9] - 2020-08-27
### Changed
- Update `gen_dates()` logic so it never includes today in the list of generated dates used for API requests. Apple reports are only avialable for completed calendar days.

## [0.0.8] - 2020-08-26
### Changed
- Fixed date generation logic used when making App Store Connect API requests.

## [0.0.7] - 2020-08-26
### Changed
- Changed `key_properties` approach:
    - Removed all the report columns from each stream's `key_properties`; sometimes these column values are `null` which can break a Singer ETL pipeline since `key_properties` cannot contain `null` values when composing a composite primary key. 
    - Created a unique `id` column for every `RECORD` message streamed by the tap and set it as the `key_properties` for each stream. This `id` column is an `md5()` hash of all of the columns and their data in a record. 
- Updated [README.md](./README.md).
- For columns with "date" in the column name: dates are now returned as a RFC3339 timestamp in UTC timezone.
- Config `black` so it enforces double quotes "". Was mixed single & double quotes before.

## [0.0.6] - 2020-08-25
### Changed
- fix bad `install_requires` in `setup.py`
- Bump package version

## [0.0.5] - 2020-08-25
### Added
- Pendulum dependency included in `setup.py` 

### Changed
- Updated pendulum version used in package (must be `pendulum>=2.0`).
- Change formatting [tokens](https://pendulum.eustace.io/docs/#tokens) used for `BOOKMARK_DATE_FORMAT` and `APP_STORE_CONNECT_DATE_FORMAT` so they are compatible with `pendulum>=2.0`
- [README.md](./README.md) typo fixes.

## [0.0.4] - 2020-08-24
### Added
- Missing primary keys for report streams.

### Changed
- Updated [example_catalog.json](./example_catalog.json)

## [0.0.3] - 2020-08-21
### Added
- [`Apple_data_issue.csv`](./docs/Apple_data_issue.csv) showing Apple data issue discussed in README.

### Changed
- Adds primary keys for all report types in `tap-apple-app-store-connect` Singer Tap. Prevents duplicate rows
- Updates schemas and `utils.response_to_list_of_dicts()` function so that `-`'s are replaced with `_` in column names
- Bump package version
- Updates README to better describe Apple data issue.

## [0.0.2] - 2020-08-18
### Added
- This CHANGELOG.md file, based on conventions established in [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed
- Set `REPORT_SUBTYPE` to `DETAILED` for `NewsstandStream()` class.
- Fixed an error where  `report_date_str` was not set for all the streams except `FinancialStream()`. This was causing a variable call before assignment error.

## [0.0.1] - 2020-08-12
### Added
- Added the `tap-apple-app-store-connect` Singer tap to [wavetrak-services/common](https://github.com/Surfline/wavetrak-services/tree/master/common).
