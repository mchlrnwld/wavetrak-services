import fs from 'fs';
import FitParser from '../src';

const file = process.argv[2];

const content = fs.readFileSync(file);

const fitParser = new FitParser({
  force: true,
  speedUnit: 'km/h',
  lengthUnit: 'm',
  temperatureUnit: 'celsius',
  elapsedRecordField: true,
  mode: 'list',
});

const result = fitParser.parse(content);

fs.writeFileSync('./result.json', JSON.stringify(result, null, 2));
