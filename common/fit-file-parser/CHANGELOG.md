# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>1.1.1 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## 1.1.0 (2020-09-10)

* feat: Add Garmin Product Information ([183e01d](https://github.com/Surfline/wavetrak-services/commit/183e01d))





## <small>1.0.1 (2020-09-10)</small>

* fix: Add back Developer Data ID message ([be22c48](https://github.com/Surfline/wavetrak-services/commit/be22c48))
* fix: Fix Garmin Parser not handling Developer Fields (#3650) ([46c9a69](https://github.com/Surfline/wavetrak-services/commit/46c9a69)), closes [#3650](https://github.com/Surfline/wavetrak-services/issues/3650)





## 1.0.0 (2020-07-06)

* chore: add new example fit files ([3b59ff6](https://github.com/Surfline/wavetrak-services/commit/3b59ff6))
* chore: update packages ([97c4549](https://github.com/Surfline/wavetrak-services/commit/97c4549))
* fix!: change latitude longitude fields naming conventions ([d18f467](https://github.com/Surfline/wavetrak-services/commit/d18f467))
* feat: add new fields to split messages ([1f962d8](https://github.com/Surfline/wavetrak-services/commit/1f962d8))


### BREAKING CHANGE

* changed "end_position_lat to "end_lat"
* changed "end_position_long" to "end_lon"
* changed "start_position_lat to "start_lat"
* changed "start_position_long" to "start_lon"




## <small>0.1.2 (2020-07-02)</small>

**Note:** Version bump only for package @surfline/fit-parser





## <small>0.1.1 (2020-06-29)</small>

* chore: add extra fit files for testing ([1cedaa3](https://github.com/Surfline/wavetrak-services/commit/1cedaa3))
* fix: add start_time field to splits ([fbd49d0](https://github.com/Surfline/wavetrak-services/commit/fbd49d0))





## 0.1.0 (2020-06-19)

* chore: update dependencies ([b053086](https://github.com/Surfline/wavetrak-services/commit/b053086))
* feat: add `unix_time_ms` to output data types ([77e17c0](https://github.com/Surfline/wavetrak-services/commit/77e17c0))





## <small>0.0.2 (2020-06-16)</small>

* fix: change timestamps to unix timestamps ([0c76904](https://github.com/Surfline/wavetrak-services/commit/0c76904))
* fix: fix unix_time not properly converted ([6ab6cc8](https://github.com/Surfline/wavetrak-services/commit/6ab6cc8))
