import { getMessageName, getFieldObject } from './fit';

export const getFitMessage = (messageNum) => {
  return {
    name: getMessageName(messageNum),
    getAttributes: (fieldNum) => getFieldObject(fieldNum, messageNum),
  };
};
