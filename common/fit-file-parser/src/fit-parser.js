/* eslint-disable no-bitwise */
import { getArrayBuffer, calculateCRC, readRecord } from './binary';

export default class FitParser {
  constructor(options = {}) {
    this.options = {
      force: options.force != null ? options.force : true,
      speedUnit: options.speedUnit || 'm/s',
      lengthUnit: options.lengthUnit || 'm',
      temperatureUnit: options.temperatureUnit || 'celsius',
      elapsedRecordField: options.elapsedRecordField || false,
    };
  }

  parse = (content) => {
    const blob = new Uint8Array(getArrayBuffer(content));

    if (blob.length < 12) {
      if (!this.options.force) {
        throw new Error('File to small to be a FIT file');
      }
    }

    const headerLength = blob[0];
    if (headerLength !== 14 && headerLength !== 12) {
      if (!this.options.force) {
        throw new Error('Incorrect header size');
      }
    }

    let fileTypeString = '';
    for (let i = 8; i < 12; i += 1) {
      fileTypeString += String.fromCharCode(blob[i]);
    }
    if (fileTypeString !== '.FIT') {
      if (!this.options.force) {
        throw new Error("Missing '.FIT' in header");
      }
    }

    if (headerLength === 14) {
      const crcHeader = blob[12] + (blob[13] << 8);
      const crcHeaderCalc = calculateCRC(blob, 0, 12);
      if (crcHeader !== crcHeaderCalc) {
        // TODO: fix Header CRC check
        if (!this.options.force) {
          throw new Error('CRC mismatch');
        }
      }
    }
    const dataLength = blob[4] + (blob[5] << 8) + (blob[6] << 16) + (blob[7] << 24);
    const crcStart = dataLength + headerLength;
    const crcFile = blob[crcStart] + (blob[crcStart + 1] << 8);
    const crcFileCalc = calculateCRC(blob, headerLength === 12 ? 0 : headerLength, crcStart);

    if (crcFile !== crcFileCalc) {
      // TODO: fix File CRC check
      if (!this.options.force) {
        throw new Error('CRC mismatch');
      }
    }

    const splits = [];
    const fitObj = {};
    const sessions = [];
    const laps = [];
    const records = [];
    const developerFields = [];

    let loopIndex = headerLength;
    const messageTypes = [];

    let startDate;
    const pausedTime = 0;

    while (loopIndex < crcStart) {
      const { nextIndex, messageType, message } = readRecord(
        blob,
        messageTypes,
        developerFields,
        loopIndex,
        this.options,
        startDate,
        pausedTime,
      );
      loopIndex = nextIndex;

      switch (messageType) {
        case 'lap':
          laps.push(message);
          break;
        case 'session':
          sessions.push(message);
          break;
        case 'record':
          if (!startDate) {
            startDate = message.timestamp;
            message.elapsed_time = 0;
            message.timer_time = 0;
          }
          records.push(message);
          break;
        case 'split':
          splits.push(message);
          break;
        default:
          if (messageType !== '') {
            fitObj[messageType] = message;
          }
          break;
      }
    }

    fitObj.splits = splits;
    fitObj.sessions = sessions;
    fitObj.laps = laps;
    fitObj.records = records;

    return fitObj;
  };
}
