/* eslint-disable no-bitwise */
import { Buffer } from 'buffer';
import { FIT } from './fit';
import { getFitMessage } from './messages';

const COMPRESSED_HEADER_MASK = 0x80;
const COMPRESSED_TIME_MASK = 31;
const COMPRESSED_LOCAL_MESG_NUM_MSK = 0x60;
const GARMIN_TIME_OFFSET = 631065600000;
let MONITORING_TIMESTAMP = 0;
let TIMESTAMP = 0;
let LAST_TIME_OFFSET = 0;

export const addEndian = (littleEndian, bytes) => {
  let result = 0;
  if (!littleEndian) bytes.reverse();
  for (let i = 0; i < bytes.length; i += 1) {
    result += (bytes[i] << (i << 3)) >>> 0;
  }

  return result;
};

export const readData = (blob, fDef, startIndex, options) => {
  if (fDef.endianAbility === true) {
    const temp = [];
    for (let i = 0; i < fDef.size; i += 1) {
      temp.push(blob[startIndex + i]);
    }

    const { buffer } = new Uint8Array(temp);
    const dataView = new DataView(buffer);

    const array16 = [];
    const array32 = [];
    try {
      // eslint-disable-next-line default-case
      switch (fDef.type) {
        case 'sint16':
          return dataView.getInt16(0, fDef.littleEndian);
        case 'uint16':
        case 'uint16z':
          return dataView.getUint16(0, fDef.littleEndian);
        case 'sint32':
          return dataView.getInt32(0, fDef.littleEndian);
        case 'uint32':
        case 'uint32z':
          return dataView.getUint32(0, fDef.littleEndian);
        case 'float32':
          return dataView.getFloat32(0, fDef.littleEndian);
        case 'float64':
          return dataView.getFloat64(0, fDef.littleEndian);
        case 'uint32_array':
          for (let i = 0; i < fDef.size; i += 4) {
            array32.push(dataView.getUint32(i, fDef.littleEndian));
          }
          return array32;
        case 'uint16_array':
          for (let i = 0; i < fDef.size; i += 2) {
            array16.push(dataView.getUint16(i, fDef.littleEndian));
          }
          return array16;
      }
    } catch (e) {
      if (!options.force) {
        throw e;
      }
    }

    return addEndian(fDef.littleEndian, temp);
  }

  if (fDef.type === 'string') {
    const temp = [];
    for (let i = 0; i < fDef.size; i += 1) {
      if (blob[startIndex + i]) {
        temp.push(blob[startIndex + i]);
      }
    }
    return Buffer.from(temp).toString('utf-8');
  }

  if (fDef.type === 'byte_array') {
    const temp = [];
    for (let i = 0; i < fDef.size; i += 1) {
      temp.push(blob[startIndex + i]);
    }
    return temp;
  }

  return blob[startIndex];
};

const formatByType = (data, type, scale, offset) => {
  const values = [];
  const dataItem = {};
  switch (type) {
    case 'date_time':
    case 'local_date_time':
      return new Date(data * 1000 + 631065600000);
    case 'unix_time':
      return data + 631065600;
    case 'unix_time_ms':
      return (data + 631065600) * 1000;
    case 'sint32':
      return data * FIT.scConst;
    case 'uint8':
    case 'sint16':
    case 'uint32':
    case 'uint16':
      return scale ? data / scale + offset : data;
    case 'uint32_array':
    case 'uint16_array':
      return data.map((datum) => (scale ? datum / scale + offset : datum));
    default:
      if (!FIT.types[type]) {
        return data;
      }
      // Quick check for a mask
      Object.keys(FIT.types[type]).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(FIT.types[type], key)) {
          values.push(FIT.types[type][key]);
        }
      });
      if (values.indexOf('mask') === -1) {
        return FIT.types[type][data];
      }
      Object.keys(FIT.types[type]).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(FIT.types[type], key)) {
          if (FIT.types[type][key] === 'mask') {
            dataItem.value = data & key;
          } else {
            // Not sure if we need the >> 7 and casting to boolean but from all the masked
            // props of fields so far this seems to be the case
            dataItem[FIT.types[type][key]] = !!((data & key) >> 7);
          }
        }
      });

      return dataItem;
  }
};

const isInvalidValue = (data, type) => {
  switch (type) {
    case 'enum':
      return data === 0xff;
    case 'sint8':
      return data === 0x7f;
    case 'uint8':
      return data === 0xff;
    case 'sint16':
      return data === 0x7fff;
    case 'uint16':
      return data === 0xffff;
    case 'sint32':
      return data === 0x7fffffff;
    case 'uint32':
      return data === 0xffffffff;
    case 'string':
      return data === 0x00;
    case 'float32':
      return data === 0xffffffff;
    case 'float64':
      return data === 0xffffffffffffffff;
    case 'uint8z':
      return data === 0x00;
    case 'uint16z':
      return data === 0x0000;
    case 'uint32z':
      return data === 0x000000;
    case 'byte':
      return data === 0xff;
    case 'sint64':
      return data === 0x7fffffffffffffff;
    case 'uint64':
      return data === 0xffffffffffffffff;
    case 'uint64z':
      return data === 0x0000000000000000;
    default:
      return false;
  }
};

const convertTo = (data, unitsList, speedUnit) => {
  const unitObj = FIT.options[unitsList][speedUnit];
  return unitObj ? data * unitObj.multiplier + unitObj.offset : data;
};

const applyOptions = (data, field, options) => {
  switch (field) {
    case 'speed':
    case 'enhanced_speed':
    case 'vertical_speed':
    case 'avg_speed':
    case 'max_speed':
    case 'speed_1s':
    case 'enhanced_avg_speed':
    case 'enhanced_max_speed':
    case 'avg_pos_vertical_speed':
    case 'max_pos_vertical_speed':
    case 'avg_neg_vertical_speed':
    case 'max_neg_vertical_speed':
      return convertTo(data, 'speedUnits', options.speedUnit);
    case 'distance':
    case 'total_distance':
    case 'enhanced_avg_altitude':
    case 'enhanced_min_altitude':
    case 'enhanced_max_altitude':
    case 'enhanced_altitude':
    case 'height':
    case 'odometer':
    case 'avg_stroke_distance':
    case 'min_altitude':
    case 'avg_altitude':
    case 'max_altitude':
    case 'total_ascent':
    case 'total_descent':
    case 'altitude':
    case 'cycle_length':
    case 'auto_wheelsize':
    case 'custom_wheelsize':
    case 'gps_accuracy':
      return convertTo(data, 'lengthUnits', options.lengthUnit);
    case 'temperature':
    case 'avg_temperature':
    case 'max_temperature':
      return convertTo(data, 'temperatureUnits', options.temperatureUnit);
    default:
      return data;
  }
};

export const readRecord = (
  blob,
  messageTypes,
  developerFields,
  startIndex,
  options,
  startDate,
  pausedTime,
) => {
  const recordHeader = blob[startIndex];
  let localMessageType = recordHeader & 15;

  if ((recordHeader & COMPRESSED_HEADER_MASK) === COMPRESSED_HEADER_MASK) {
    // compressed timestampz
    const timeoffset = recordHeader & COMPRESSED_TIME_MASK;

    // This value is not currently used, but might be used in the future.
    // See https://github.com/jimmykane/fit-parser/blob/master/src/binary.js#L234
    // to see if it has been updated.
    // eslint-disable-next-line no-unused-vars
    TIMESTAMP += (timeoffset - LAST_TIME_OFFSET) & COMPRESSED_TIME_MASK;
    LAST_TIME_OFFSET = timeoffset;

    localMessageType = (recordHeader & COMPRESSED_LOCAL_MESG_NUM_MSK) >> 5;
  } else if ((recordHeader & 64) === 64) {
    // is definition message
    // startIndex + 1 is reserved

    const hasDeveloperData = (recordHeader & 32) === 32;
    const lEnd = blob[startIndex + 2] === 0;
    const numberOfFields = blob[startIndex + 5];
    const numberOfDeveloperDataFields = hasDeveloperData
      ? blob[startIndex + 5 + numberOfFields * 3 + 1]
      : 0;

    const mTypeDef = {
      littleEndian: lEnd,
      globalMessageNumber: addEndian(lEnd, [blob[startIndex + 3], blob[startIndex + 4]]),
      numberOfFields: numberOfFields + numberOfDeveloperDataFields,
      fieldDefs: [],
    };

    const message = getFitMessage(mTypeDef.globalMessageNumber);

    for (let i = 0; i < numberOfFields; i += 1) {
      const fDefIndex = startIndex + 6 + i * 3;
      const baseType = blob[fDefIndex + 2];
      const { field, type } = message.getAttributes(blob[fDefIndex]);
      const fDef = {
        type,
        fDefNo: blob[fDefIndex],
        size: blob[fDefIndex + 1],
        endianAbility: (baseType & 128) === 128,
        littleEndian: lEnd,
        baseTypeNo: baseType & 15,
        name: field,
        dataType: baseType & 15,
      };

      mTypeDef.fieldDefs.push(fDef);
    }

    // numberOfDeveloperDataFields = 0 so it wont crash here and wont loop
    for (let i = 0; i < numberOfDeveloperDataFields; i += 1) {
      // If we fail to parse then try catch
      try {
        const fDefIndex = startIndex + 6 + numberOfFields * 3 + 1 + i * 3;

        const fieldNum = blob[fDefIndex];
        const size = blob[fDefIndex + 1];
        const devDataIndex = blob[fDefIndex + 2];

        const devDef = developerFields[devDataIndex][fieldNum];

        const baseType = devDef.fit_base_type_id;

        const fDef = {
          type: FIT.types.fit_base_type[baseType],
          fDefNo: fieldNum,
          size,
          endianAbility: (baseType & 128) === 128,
          littleEndian: lEnd,
          baseTypeNo: baseType & 15,
          name: devDef.field_name,
          dataType: baseType & 15,
          scale: devDef.scale || 1,
          offset: devDef.offset || 0,
          developerDataIndex: devDataIndex,
          isDeveloperField: true,
        };

        mTypeDef.fieldDefs.push(fDef);
      } catch (e) {
        if (!options.force) {
          throw e;
        }
      }
    }

    // eslint-disable-next-line no-param-reassign
    messageTypes[localMessageType] = mTypeDef;

    const nextIndex = startIndex + 6 + mTypeDef.numberOfFields * 3;
    const nextIndexWithDeveloperData = nextIndex + 1;

    return {
      messageType: 'definition',
      nextIndex: hasDeveloperData ? nextIndexWithDeveloperData : nextIndex,
    };
  }

  const messageType = messageTypes[localMessageType] || messageTypes[0];

  // uncompressed header
  let messageSize = 0;
  let readDataFromIndex = startIndex + 1;
  const fields = {};
  const message = getFitMessage(messageType.globalMessageNumber);

  for (let i = 0; i < messageType.fieldDefs.length; i += 1) {
    const fDef = messageType.fieldDefs[i];
    const data = readData(blob, fDef, readDataFromIndex, options);

    if (!isInvalidValue(data, fDef.type)) {
      if (fDef.isDeveloperField) {
        const field = fDef.name;
        const { type } = fDef;
        const { scale } = fDef;
        const { offset } = fDef;

        fields[fDef.name] = applyOptions(formatByType(data, type, scale, offset), field, options);
      } else {
        const { field, type, scale, offset } = message.getAttributes(fDef.fDefNo);

        if (field !== 'unknown' && field !== '' && field !== undefined) {
          fields[field] = applyOptions(formatByType(data, type, scale, offset), field, options);
        }
      }

      if (message.name === 'record' && options.elapsedRecordField) {
        fields.elapsed_time = (fields.timestamp - startDate) / 1000;
        fields.timer_time = fields.elapsed_time - pausedTime;
      }
    }

    readDataFromIndex += fDef.size;
    messageSize += fDef.size;
  }

  if (message.name === 'field_description') {
    // eslint-disable-next-line no-param-reassign
    developerFields[fields.developer_data_index] =
      developerFields[fields.developer_data_index] || [];
    // eslint-disable-next-line no-param-reassign
    developerFields[fields.developer_data_index][fields.field_definition_number] = fields;
  }

  if (message.name === 'monitoring') {
    // we need to keep the raw timestamp value so we can calculate subsequent timestamp16 fields
    if (fields.timestamp) {
      // eslint-disable-next-line no-undef
      MONITORING_TIMESTAMP = fields.timestamp;
      fields.timestamp = new Date(fields.timestamp * 1000 + GARMIN_TIME_OFFSET);
    }
    if (fields.timestamp16 && !fields.timestamp) {
      MONITORING_TIMESTAMP += (fields.timestamp16 - (MONITORING_TIMESTAMP & 0xffff)) & 0xffff;
      // fields.timestamp = monitoring_timestamp;
      fields.timestamp = new Date(MONITORING_TIMESTAMP * 1000 + GARMIN_TIME_OFFSET);
    }
  }

  const result = {
    messageType: message.name,
    nextIndex: startIndex + messageSize + 1,
    message: fields,
  };

  return result;
};

export const getArrayBuffer = (buffer) => {
  if (buffer instanceof ArrayBuffer) {
    return buffer;
  }
  const ab = new ArrayBuffer(buffer.length);
  const view = new Uint8Array(ab);
  for (let i = 0; i < buffer.length; i += 1) {
    view[i] = buffer[i];
  }
  return ab;
};

export const calculateCRC = (blob, start, end) => {
  const crcTable = [
    0x0000,
    0xcc01,
    0xd801,
    0x1400,
    0xf001,
    0x3c00,
    0x2800,
    0xe401,
    0xa001,
    0x6c00,
    0x7800,
    0xb401,
    0x5000,
    0x9c01,
    0x8801,
    0x4400,
  ];

  let crc = 0;
  for (let i = start; i < end; i += 1) {
    const byte = blob[i];
    let tmp = crcTable[crc & 0xf];
    crc = (crc >> 4) & 0x0fff;
    crc = crc ^ tmp ^ crcTable[byte & 0xf];
    tmp = crcTable[crc & 0xf];
    crc = (crc >> 4) & 0x0fff;
    crc = crc ^ tmp ^ crcTable[(byte >> 4) & 0xf];
  }

  return crc;
};
