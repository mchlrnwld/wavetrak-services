import { expect } from 'chai';
import fs from 'fs';
import path from 'path';
import FitParser from '.';

const content = fs.readFileSync(path.join(process.cwd(), 'test.fit'));
const withDevData = fs.readFileSync(path.join(process.cwd(), 'WithDeveloperData.fit'));

describe('fit parser tests', () => {
  it('expects to retrieve a FIT object', () => {
    const fitParser = new FitParser({ force: true });
    const result = fitParser.parse(content);
    expect(result).to.be.a('object');
    expect(result).to.have.property('sessions');
  });

  it('expects longitude to be in the range -180 to +180', () => {
    const fitParser = new FitParser({ force: true });
    const result = fitParser.parse(content);
    expect(result).to.have.property('records');
    expect(
      result.records.map((r) => r.position_long).filter((l) => l > 180 || l < -180),
    ).to.be.empty();
  });

  it('should handle fit files with developer data', () => {
    const fitParser = new FitParser({ force: true });
    const result = fitParser.parse(withDevData);
    expect(result).to.have.property('records');
    expect(result).to.have.property('splits');
    expect(result.splits).to.have.length(36);
    expect(result.records).to.have.length(3815);
  });

  it('should have device information', () => {
    const fitParser = new FitParser({ force: true });
    const result = fitParser.parse(content);
    expect(result).to.have.property('file_id');
    expect(result.file_id).to.have.property('product');
    expect(result.file_id.product).to.be.equal('fenix6');
  });
});
