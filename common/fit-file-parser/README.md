# @surfline/fit-file-parser

## Install

```
$ npm install --save @surfline/fit-file-parser
```

## How to use

See in [examples](./examples) folder:

```javascript
import FitParser from '@surfline/fit-file-parser'
import fs from 'fs'

const file = process.argv[2];

const content = fs.readFileSync(file);

const fitParser = new FitParser({
  force: true,
  speedUnit: 'km/h',
  lengthUnit: 'm',
  temperatureUnit: 'celsius',
  elapsedRecordField: true,
  mode: 'list',
});

const result = fitParser.parse(content);
```

## API Documentation

### new FitParser(Object _options_)

Needed to create a new instance. _options_ is optional, and is used to customize the returned object.

Allowed properties :
- `lengthUnit`: String
  - `m`: Lengths are in meters (**default**)
  - `km`: Lengths are in kilometers
  - `mi`: Lengths are in miles
- `temperatureUnit`: String
  - `celsius`:Temperatures are in °C (**default**)
  - `kelvin`: Temperatures are in °K
  - `fahrenheit`: Temperatures are in °F
- `speedUnit`: String
  - `m/s`: Speeds are in meters per seconds (**default**)
  - `km/h`: Speeds are in kilometers per hour
  - `mph`: Speeds are in miles per hour
- `force`: Boolean
  - `true`: Continues even if they are errors (**default for now**)
  - `false`: Stops if an error occurs
- `elapsedRecordField`: Boolean
  - `true`: Includes `elapsed_time`, containing the elapsed time in seconds since the first record, and `timer_time`, containing the time shown on the device, inside each `record` field
  - `false` (**default**)

### fitParser.parse(Buffer _file_)

Function used to initiate the parsing of the buffer for the FIT file.

