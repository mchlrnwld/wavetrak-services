# tap-google-play-console

Author: Greg Clunies

A [Singer.io](http://singer.io) tap that extracts Google Play Console reports data and produces JSON-formatted data following the [Singer spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).

## Compatibility with Singer Targets

`tap-google-play-console` uses `key_properties` to identify records that should be UPSERT (aka de-duplication) in database-like targets. All other targets should assume that data will be appended. A summary of targets likely to be used with `tap-google-play-console` is provided below.

| Target             | Can UPSERT? | Tested | 
|--------------------|:-----------:|:------:|
| `target-stitch`    | ✅          | ✅     |
| `target-postgres`  | ✅          | ❌     |
| `target-redshift`  | ✅          | ❌     |
| `target-snowflake` | ✅          | ❌     |
| `target-snowflake` | ✅          | ❌     |
| `target-csv`       | ❌          | ❌     |
| `target-gsheet`    | ❌          | ❌     |

Testing of each target is ongoing, but UPSERT functionality _should_ be functional for all database targets due to the use of `key_properties`.

The default replication method for `tap-google-play-console` is `INCREMENTAL` (only sends data from updated Google Play Console Reports). `FULL_TABLE` replication is not supported at this time.

Before using the tap, please see the [known issues](#known_issues) section below as it may effect your use case.

### Known issues with Google Play Console data structure <a name="known_issues"></a>
Due to the structure of the data in Google Play Console Reports, _you will encounter re-streaming of RECORDS that may not have been updated_. This is a Google issue and cannot be solved by the tap. Details on this issue are provided [here](./docs/tap_compatibility.md).

## Google Play Console report types:
A complete list of [available report types](https://support.google.com/googleplay/android-developer/answer/6135870?hl=en) is provided by Google.

The following report types and their streams have not been tested:
- Cancellation survey responses (Stream: `cancellation_survey_responses`)
- Korean Play balance funded (Stream: `korean_play_balance`)
- Rewarded products (Stream: `rewarded_products`)

## Quick Start

### `tap-google-play-console` Setup

#### 1. Install
To run locally, clone this repo. Then once inside the repo directory, run:

```bash
pip install .
```

#### 2. Get authentication credentials from Google Play Console

- Follow the instructions under the section **Download reports using a client
library & service account** found on this
[Google Play Console Help](https://support.google.com/googleplay/android-developer/answer/6135870?p=financial_export&rd=1#export)
page.
    - Ignore **Step 3: Fetch reports using an API call**.

- You will get a JSON file with your service account credentials (see [`example_service_account_keyfile.json`](./example_service_account_keyfile.json)). You will use this in your `tap_config.json` file.

#### 3. Create the tap config file
There is a template in this repo [`example_tap_config.json`](./example_tap_config.json) that you can use. Just copy it into a `tap_config.json` file you create in the repo root and set the values for each property. Be sure you specify you service account keyfile path (see step 2 above).
<br>
<br>

### Running `tap-google-play-console`

#### 1. Run `tap-google-play-console` in DISCOVERY mode to generate a catalog
```
tap-google-play-console --config tap_config.json --discover > catalog.json
```

Once the catalog is generated, you will need to select the streams and their corresponding fields you would like the tap to process and stream. See the [Singer docs](https://github.com/singer-io/getting-started/blob/master/docs/DISCOVERY_MODE.md#metadata) on selecting streams and fields from a catalog using `"selected": true`. 

We highly recommend using [singer-discover](https://github.com/chrisgoddard/singer-discover) to speed up this process.

An [`example_catalog.json`](./example_catalog.json) without the selected streams/fields can be found in this repo.

#### 2. Run `tap-google-play-console` in SYNC mode

**1st tap run**<br>
Send data to your target (this example uses `target-stitch`) with the command below, save the final state emitted by the target for future runs.
```
tap-google-play-console --config tap_config.json --catalog catalog.json | target-stitch --config target_stitch_config.json > state.json

tail -1 state.json > state.json.tmp && mv state.json.tmp state.json
```

**Subsequent tap runs**<br>
Use `state.json` from 1st/previous run as an argument to the tap, which tells the tap where to begin streaming data from. An [`example_state.json`](./example_state.json) is included in this repo to help clarify what data this file provides to the tap.
```
tap-google-play-console --config tap_config.json --catalog catalog.json --state state.json | target-stitch --config target_config.json > state.json

tail -1 state.json > state.json.tmp && mv state.json.tmp state.json
```

## Developing
If you wish to contribute to the development of this tap, this repo also includes:

1. [`environment.yml`](./environment.yml) that defines a `tap-google-play-console-conda-dev` conda environment. After cloning the repo and navigating inside the repo, run the commands below to build and activate the environment.
```bash
conda env create --file environment.yml
conda activate tap-google-play-console-dev
pip install -e .
```
2. A [`Makefile`](./Makefile) with commands that you may find helpful during development. This will reduce having to type out commands to the shell each time. You may need to change the path to your conda enviroment in the `Makefile`based on whether or not you use [miniconda](https://docs.conda.io/en/latest/miniconda.html) or [anaconda](https://docs.anaconda.com/anaconda/) as your package manager.

---
Copyright &copy; 2020 Surfline/Wavetrak, Inc.
