import json

import singer  # type: ignore
from dateutil.parser import parse

LOGGER = singer.get_logger()


def get_last_report_updated_value_for_table(state, table):
    last_value = (
        state.get('bookmarks', {}).get(table, {}).get('last_report_updated')
    )

    return parse(last_value) if last_value else last_value


def incorporate(state, table, field, value):
    if value is None:
        return state

    new_state = state.copy()

    parsed = parse(value).strftime("%Y-%m-%dT%H:%M:%SZ")

    if 'bookmarks' not in new_state:
        new_state['bookmarks'] = {}

    if (
        new_state['bookmarks'].get(table, {}).get('last_record') is None
        or new_state['bookmarks'].get(table, {}).get('last_record') < value
    ):
        new_state['bookmarks'][table] = {'field': field, 'last_record': parsed}

    return new_state


def save_state(state):
    if not state:
        return

    LOGGER.info('Updating state.')

    singer.write_state(state)


def load_state(filename):
    if filename is None:
        return {}

    try:
        with open(filename) as handle:
            return json.load(handle)
    except:  # noqa: E722 (ignores Flake8 error)
        LOGGER.fatal("Failed to decode state file. Is it valid json?")
        raise
