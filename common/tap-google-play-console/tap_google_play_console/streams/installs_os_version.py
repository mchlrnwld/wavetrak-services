import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class InstallsOSVersionStream(BaseStream):
    KEY_PROPERTIES = ['date', 'package_name', 'android_os_version']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'installs_os_version'
    PREFIX = 'stats/installs/'
    DIMENSION = 'os_version'
