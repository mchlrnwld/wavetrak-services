import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class RetainedInstallersCountryStream(BaseStream):
    KEY_PROPERTIES = ['date', 'package_name', 'country']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'retained_installers_country'
    PREFIX = 'acquisition/retained_installers/'
    DIMENSION = 'country'
