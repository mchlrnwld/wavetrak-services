import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class Buyers7dChannelStream(BaseStream):
    KEY_PROPERTIES = ['date', 'package_name', 'acquisition_channel']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'buyers_7d_channel'
    PREFIX = 'acquisition/buyers_7d/'
    DIMENSION = 'channel'
