import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class InstallsCountryStream(BaseStream):
    KEY_PROPERTIES = ['date', 'package_name', 'country']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'installs_country'
    PREFIX = 'stats/installs/'
    DIMENSION = 'country'
