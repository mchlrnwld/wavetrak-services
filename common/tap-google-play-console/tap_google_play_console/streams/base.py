import codecs
import io
from zipfile import ZipFile

import google.cloud  # type: ignore
import singer  # type: ignore
import singer.metrics  # type: ignore
import singer.utils  # type: ignore
from tap_framework.streams import BaseStream as base  # type: ignore

from tap_google_play_console import metadata
from tap_google_play_console.config import get_config_start_date
from tap_google_play_console.state import (
    get_last_report_updated_value_for_table,
    save_state,
)
from tap_google_play_console.utils import csv_to_list

LOGGER = singer.get_logger()
BOOKMARK_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'
REPORT_DATE_FORMAT = '%Y%m'


class BaseStream(base):
    """ Base Stream Class for all streams defined in this tap. Each individual
    stream (e.g. SubscriptionsCountryStream) is a child of this BaseStream
    class.

    Class variables (e.g. self.TABLE, self.BRAND, etc.) are assigned at the
    child class level. See subscriptions_country.py as an example.
    """

    # This method is modified from its definition in tap-framework so that
    # it includes a 'bookmark_properties' value in SCHEMA messages. See:
    # https://github.com/singer-io/getting-started/blob/bbb53c62e727b436e6952211b0761b76c0ccc41b/docs/SPEC.md#schema-message
    def write_schema(self):
        singer.write_schema(
            self.catalog.stream,
            self.catalog.schema.to_dict(),
            key_properties=self.catalog.key_properties,
            bookmark_properties=self.REPLICATION_KEYS,
        )

    # Redefine generate_catalog() stream class method (from tap-framework)
    # so that it writes more descriptive metadata for stream.
    def generate_catalog(self):
        schema = self.get_schema()
        mdata = singer.metadata.new()
        mdata = metadata.get_standard_metadata(
            schema=schema,
            key_properties=self.KEY_PROPERTIES,
            valid_replication_keys=self.REPLICATION_KEYS,
            replication_method=self.REPLICATION_METHOD,
        )
        mdata = metadata.to_map(mdata)
        mdata = singer.metadata.write(mdata, (), 'inclusion', 'available')

        for field_name, field_schema in schema.get('properties').items():
            mdata = singer.metadata.write(
                mdata,
                ('properties', field_name),
                'inclusion',
                'automatic'
                if field_name in self.KEY_PROPERTIES
                else 'available',
            )

        return [
            {
                'tap_stream_id': self.TABLE,
                'stream': self.TABLE,
                'key_properties': self.KEY_PROPERTIES,
                'schema': self.get_schema(),
                'metadata': singer.metadata.to_list(mdata),
            }
        ]

    def sync_data(self):
        LOGGER.info('Syncing data for entity {}'.format(self.TABLE))

        last_report_updated = get_last_report_updated_value_for_table(
            self.state, self.TABLE
        )

        if last_report_updated is None:
            last_report_updated = get_config_start_date(self.config)

        # Connect to the Google Cloud Bucket that stores all Google Play
        # console reports. For list of available reports see:
        # https://support.google.com/googleplay/android-developer/answer/6135870?dark=1
        try:
            bucket = self.client.get_bucket(self.config['developer_bucket_id'])
        except google.cloud.exceptions.NotFound:
            LOGGER.info(
                'Sorry, bucket {} does not exist!'.format(
                    self.config['developer_bucket_id']
                )
            )
            raise

        # Get all of the reports from Google Cloud Platform (GCP) that match
        # the defined PREFIX for the stream. Then get a 2-tuple list of report
        # name and when the report was last updated on GCP.
        reports_info = [
            (blob.name, blob.updated)
            for blob in bucket.list_blobs(prefix=self.PREFIX)
        ]

        # Filter for reports that match the Stream's DIMENSION
        if self.DIMENSION is None:
            stream_reports_info = reports_info
        else:
            stream_reports_info = [
                (report_name, report_updated)
                for report_name, report_updated in reports_info
                if self.DIMENSION in report_name
            ]
            # When dimension is 'country', make sure not to include reports
            # with 'play_country' in report name
            if self.DIMENSION == 'country':
                stream_reports_info = [
                    (report_name, report_updated)
                    for report_name, report_updated in stream_reports_info
                    if 'play_country' not in report_name
                ]

        # Filter for reports that are new/updated since last tap run
        new_stream_reports_info = [
            (report_name, report_updated)
            for report_name, report_updated in stream_reports_info
            if report_updated >= last_report_updated
        ]

        # Loop through all new/updated report .csv files (sorted by updated
        # value), read data line by line in each report, write a record for
        # each line to stdout.
        for report_name, report_updated in sorted(
            new_stream_reports_info, key=lambda tup: tup[1]
        ):
            try:
                report_data = bucket.get_blob(report_name).download_as_string()
            except google.cloud.exceptions.NotFound:
                LOGGER.info(f"Sorry, {report_name} not found in bucket!")
                # raise

            # If report in a .zip containing the .csv report, unzip and decode
            if (
                'zip' in report_name
                or self.TABLE == 'cancellation_survey_responses'
                # Cancellation survey reports have .csv report_name in Google
                # Cloud Platform, but download as .zip (...sick Google!)
            ):
                zipbytes = io.BytesIO(report_data)

                with ZipFile(zipbytes, 'r') as myzip:
                    for csv_file in myzip.namelist():
                        report_data_from_csv_in_zip = myzip.read(csv_file)
                        report_csv = report_data_from_csv_in_zip.decode()

            # If report in a .csv, decode
            else:
                bom = codecs.BOM_UTF16_LE  # Sets 'byte order mark' to look for
                if report_data.startswith(bom):
                    slicer = len(bom)
                    report_data = report_data[slicer:]
                    report_csv = report_data.decode('utf-16le')

            # Take decoded data from .csv and write to list of dicts
            # One dict per RECORD
            report_list, fields = csv_to_list(report_csv)

            # Write the individuals rows from .csv (now dicts in list) as
            # RECORD messages
            for index, line in enumerate(report_list, start=1):
                data = line

                # Add `report_updated` field to RECORD message (from .csv file)
                data.update(
                    {
                        'report_updated': report_updated.strftime(
                            BOOKMARK_DATE_FORMAT
                        )
                    }
                )

                # .transform_record() is from tap-framework skeleton code.
                # The resultant Transformer object handles any needed data
                # type conversions (according to the defined schema for the
                # stream) before writing them to stdout with
                # singer.write_record()
                data_transformed = self.transform_record(data)

                singer.write_record(
                    self.TABLE,
                    data_transformed,
                    time_extracted=singer.utils.now(),
                )

            singer.write_bookmark(
                self.state,
                self.TABLE,
                'last_report_updated',
                report_updated.strftime(BOOKMARK_DATE_FORMAT),
            )

            save_state(self.state)  # Similar to singer.write_state()
