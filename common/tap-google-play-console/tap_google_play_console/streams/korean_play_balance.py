import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class KoreanPlayBalanceStream(BaseStream):
    KEY_PROPERTIES = ['order_number']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'korean_play_balance'
    PREFIX = 'play_balance_krw/'
    DIMENSION = None
