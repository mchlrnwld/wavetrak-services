import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class CancellationSurveyResponsesStream(BaseStream):
    KEY_PROPERTIES = ['cancellation_date', 'sku_id', 'country', 'response']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'cancellation_survey_responses'
    PREFIX = 'subscriptions/cancellations/'
    DIMENSION = None
