import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class SubscriptionsDeviceStream(BaseStream):
    KEY_PROPERTIES = ['date', 'package_name', 'product_id', 'device']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'subscriptions_device'
    PREFIX = 'financial-stats/subscriptions/'
    DIMENSION = 'device'
