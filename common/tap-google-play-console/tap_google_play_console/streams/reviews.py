import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class ReviewsStream(BaseStream):
    KEY_PROPERTIES = ['review_link']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'reviews'
    PREFIX = 'reviews/'
    DIMENSION = None
