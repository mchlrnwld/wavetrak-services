from tap_google_play_console.streams.buyers_7d_channel import (
    Buyers7dChannelStream,
)
from tap_google_play_console.streams.buyers_7d_country import (
    Buyers7dCountryStream,
)
from tap_google_play_console.streams.buyers_7d_play_country import (
    Buyers7dPlayCountryStream,
)
from tap_google_play_console.streams.buyers_7d_play_search import (
    Buyers7dPlaySearchStream,
)
from tap_google_play_console.streams.buyers_7d_utm_tagged import (
    Buyers7dUTMTaggedStream,
)
from tap_google_play_console.streams.cancellation_survey_reponses import (
    CancellationSurveyResponsesStream,
)
from tap_google_play_console.streams.crashes_app_version import (
    CrashesAppVersionStream,
)
from tap_google_play_console.streams.crashes_device import CrashesDeviceStream
from tap_google_play_console.streams.crashes_os_version import (
    CrashesOSVersionStream,
)
from tap_google_play_console.streams.crashes_overview import (
    CrashesOverviewStream,
)
from tap_google_play_console.streams.earnings import EarningsStream
from tap_google_play_console.streams.installs_app_version import (
    InstallsAppVersionStream,
)
from tap_google_play_console.streams.installs_carrier import (
    InstallsCarrierStream,
)
from tap_google_play_console.streams.installs_country import (
    InstallsCountryStream,
)
from tap_google_play_console.streams.installs_device import (
    InstallsDeviceStream,
)
from tap_google_play_console.streams.installs_language import (
    InstallsLanguageStream,
)
from tap_google_play_console.streams.installs_os_version import (
    InstallsOSVersionStream,
)
from tap_google_play_console.streams.installs_overview import (
    InstallsOverviewStream,
)
from tap_google_play_console.streams.korean_play_balance import (
    KoreanPlayBalanceStream,
)
from tap_google_play_console.streams.ratings_app_version import (
    RatingsAppVersionStream,
)
from tap_google_play_console.streams.ratings_carrier import (
    RatingsCarrierStream,
)
from tap_google_play_console.streams.ratings_country import (
    RatingsCountryStream,
)
from tap_google_play_console.streams.ratings_device import RatingsDeviceStream
from tap_google_play_console.streams.ratings_language import (
    RatingsLanguageStream,
)
from tap_google_play_console.streams.ratings_os_version import (
    RatingsOSVersionStream,
)
from tap_google_play_console.streams.ratings_overview import (
    RatingsOverviewStream,
)
from tap_google_play_console.streams.retained_installers_channel import (
    RetainedInstallersChannelStream,
)
from tap_google_play_console.streams.retained_installers_country import (
    RetainedInstallersCountryStream,
)
from tap_google_play_console.streams.retained_installers_play_country import (
    RetainedInstallersPlayCountryStream,
)
from tap_google_play_console.streams.retained_installers_play_search import (
    RetainedInstallersPlaySearchStream,
)
from tap_google_play_console.streams.retained_installers_utm_tagged import (
    RetainedInstallersUTMTaggedStream,
)
from tap_google_play_console.streams.reviews import ReviewsStream
from tap_google_play_console.streams.rewarded_products import (
    RewardedProductsStream,
)
from tap_google_play_console.streams.sales import SalesStream
from tap_google_play_console.streams.subscriptions_country import (
    SubscriptionsCountryStream,
)
from tap_google_play_console.streams.subscriptions_device import (
    SubscriptionsDeviceStream,
)

AVAILABLE_STREAMS = [
    Buyers7dChannelStream,
    Buyers7dCountryStream,
    Buyers7dPlayCountryStream,
    Buyers7dPlaySearchStream,
    Buyers7dUTMTaggedStream,
    CancellationSurveyResponsesStream,
    CrashesAppVersionStream,
    CrashesDeviceStream,
    CrashesOSVersionStream,
    CrashesOverviewStream,
    EarningsStream,
    InstallsAppVersionStream,
    InstallsCarrierStream,
    InstallsCountryStream,
    InstallsDeviceStream,
    InstallsLanguageStream,
    InstallsOSVersionStream,
    InstallsOverviewStream,
    KoreanPlayBalanceStream,
    RatingsAppVersionStream,
    RatingsCarrierStream,
    RatingsCountryStream,
    RatingsDeviceStream,
    RatingsLanguageStream,
    RatingsOSVersionStream,
    RatingsOverviewStream,
    RetainedInstallersChannelStream,
    RetainedInstallersCountryStream,
    RetainedInstallersPlayCountryStream,
    RetainedInstallersPlaySearchStream,
    RetainedInstallersUTMTaggedStream,
    ReviewsStream,
    RewardedProductsStream,
    SalesStream,
    SubscriptionsCountryStream,
    SubscriptionsDeviceStream,
]

__all__ = [
    'Buyers7dChannelStream',
    'Buyers7dCountryStream',
    'Buyers7dPlayCountryStream',
    'Buyers7dPlaySearchStream',
    'Buyers7dUTMTaggedStream',
    'CancellationSurveyResponsesStream',
    'CrashesAppVersionStream',
    'CrashesDeviceStream',
    'CrashesOSVersionStream',
    'CrashesOverviewStream',
    'EarningsStream',
    'InstallsAppVersionStream',
    'InstallsCarrierStream',
    'InstallsCountryStream',
    'InstallsDeviceStream',
    'InstallsLanguageStream',
    'InstallsOSVersionStream',
    'InstallsOverviewStream',
    'KoreanPlayBalanceStream',
    'RatingsAppVersionStream',
    'RatingsCarrierStream',
    'RatingsCountryStream',
    'RatingsDeviceStream',
    'RatingsLanguageStream',
    'RatingsOSVersionStream',
    'RatingsOverviewStream',
    'RetainedInstallersChannelStream',
    'RetainedInstallersCountryStream',
    'RetainedInstallersPlayCountryStream',
    'RetainedInstallersPlaySearchStream',
    'RetainedInstallersUTMTaggedStream',
    'ReviewsStream',
    'RewardedProductsStream',
    'SalesStream',
    'SubscriptionsCountryStream',
    'SubscriptionsDeviceStream',
]
