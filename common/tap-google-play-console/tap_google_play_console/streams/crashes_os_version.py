import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class CrashesOSVersionStream(BaseStream):
    KEY_PROPERTIES = ['date', 'package_name', 'android_os_version']
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'crashes_os_version'
    PREFIX = 'stats/crashes/'
    DIMENSION = 'os_version'
