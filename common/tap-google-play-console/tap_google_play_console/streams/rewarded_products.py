import singer  # type: ignore

from tap_google_play_console.streams.base import BaseStream

LOGGER = singer.get_logger()  # noqa


class RewardedProductsStream(BaseStream):
    # Key properties may be over constrained due to inexperience with this
    # report type
    KEY_PROPERTIES = [
        'date',
        'app',
        'product_title',
        'product_id',
        'merchant_currency',
    ]
    REPLICATION_KEYS = ['report_updated']
    REPLICATION_METHOD = 'INCREMENTAL'
    # API_METHOD = 'GET'  # Not needed since we are using Google Python Client
    TABLE = 'rewarded_products'
    PREFIX = 'rewarded_product/'
    DIMENSION = None
