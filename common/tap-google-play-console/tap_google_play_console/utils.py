"""This module contains helper functions."""
import csv


def csv_to_list(content):
    lines = content.split('\n')
    header = [
        s.lower()
        .replace('(', '')
        .replace(')', '')
        .replace('/', '_')
        .replace(' ', '_')
        for s in lines[0].split(',')
    ]

    data = []
    for row in csv.reader(lines[1:]):
        if len(row) == 0:
            continue
        line_obj = {}
        for i, column in enumerate(header):
            if i < len(row):
                line_obj[column] = row[i].strip()
        data.append(line_obj)

    return data, header
