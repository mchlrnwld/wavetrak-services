#!/usr/bin/env python3

import singer  # type: ignore
import tap_framework  # type: ignore

from tap_google_play_console.client import GooglePlayConsoleClient
from tap_google_play_console.streams import AVAILABLE_STREAMS

LOGGER = singer.get_logger()  # noqa


class GooglePlayConsoleRunner(tap_framework.Runner):
    pass


@singer.utils.handle_top_exception(LOGGER)
def main():
    args = singer.utils.parse_args(
        required_config_keys=['key_file', 'start_date', 'developer_bucket_id']
    )

    try:
        client = GooglePlayConsoleClient.from_service_account_json(
            args.config['key_file']
        )

    except TypeError:
        LOGGER.info(
            "When calling .from_service_account_json(), 'credentials' must"
            " not be in keyword arguments"
        )
        raise

    runner = GooglePlayConsoleRunner(args, client, AVAILABLE_STREAMS)

    if args.discover:
        runner.do_discover()

    else:
        runner.do_sync()


if __name__ == '__main__':
    main()
