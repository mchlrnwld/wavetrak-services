# Instead of writing our own client from scratch, we leverage the Google Cloud
# Platform Python Client and its storage.Client class to access our data. We
# create a subclass GooglePlayConsoleClient (see below) which inherits all of
# the attributes and methods from the storage.Client class via class
# inheritance.
#
# We could explicitly call the Google Cloud storage.Client class in
# ./tap_google_play_console/__init__.py, but opt to keep all dealings with
# client setup in a separate module (even though simple in this case). This
# follows the framework used by Fishtown Analytics. See:
# https://github.com/fishtown-analytics/tap-framework/tree/9f75d6ef57b540f7a5eb5a6048b7d12cf30bdf97/tap_framework

from google.cloud import storage  # type: ignore


class GooglePlayConsoleClient(storage.Client):
    pass
