#!/usr/bin/env python

from setuptools import find_packages, setup  # type: ignore

setup(
    name='tap-google-play-console',
    version='0.0.1',
    description='Singer.io tap for extracting Google Play Console data.',
    author='Greg Clunies',
    author_email='gclunies@surfline.com',
    url='https://github.com/Surfline/wavetrak-services/tree/master/common/tap-google-play-console',  # noqa : E501
    classifiers=['Programming Language :: Python :: 3 :: Only'],
    py_modules=['tap_google_play_console'],
    install_requires=[
        'tap-framework==0.1.1',  # This handles singer-python dependency
        'google-cloud-storage==1.28.1',
    ],
    entry_points='''
          [console_scripts]
          tap-google-play-console=tap_google_play_console:main
      ''',
    packages=find_packages(),
    package_data={'tap_google_play_console': ['schemas/*.json']},
)
