# Google Play Console data structure
Due to the structure of the data in Google Play Console Reports, _you will encounter re-streaming of RECORDS that may not have been updated_. A description of the data structure is provided below (sensitive data has been covered/removed):

## Data structure problem
Google Play Console report data are stored in `.csv` files in a Google Cloud Platform (GCP) bucket connected to your app. Each `.csv` file in the bucket contains the data for an entire month.

![](GCP_data_structure.png)

Each row in a `.csv` file represents a single `RECORD` of data (e.g. data for subscriptions, reviews, etc) that can be streamed by the tap. For some reports data is broken out into further dimensions (e.g. by country for subscriptions data).

![](csv_structure.png)

Each of these rows in the `.csv` will have a corresponding `RECORD` message written to `stdout` by the tap when it is run. 

The `.csv` files are updated by Google in the following manner:
- Google appends data to the current month's `.csv` report each day.
- Google _ALSO_ corrects data by updating rows in `.csv` files. This can happen for any report, sometimes even reports that are months/years old.

Google does not include a timestamp to indicate when a row in a `.csv` file was added (or updated if a correction is made). Instead:
  - Google changes the `updated` metadata value (the `last_modified` timestamp in the first image above) for the ***entire*** `.csv` file in stored in GCP.

Using the data provided in the columns of the `.csv` files alone, there is no way to tell if a individual row in a `.csv` file has been streamed previously by the tap, we can only tell if a `.csv` file is new or updated based on its `updated` metadata value.

## Solution for poor data structure
The data structure for Google Play Console reports forces the tap to employ a less than ideal method to ensure that all new _and_ updated rows in a `.csv` file are streamed to `target-stitch`. This is acheived by:
- Setting `key_properties` value(s) for each stream that uniquely identifies each row in the `.csv` report. `key_properties` can be a single field or a combination of multiple fields to provide a unique id.
  - For example, the `key_properties` for the `subscriptions_country` stream are: `date`, `package_name`, `product_id`, and `country`. 
- Append each `RECORD` message with a `report_updated` field which is equal to the `updated` metadata value from the `.csv` file. 

A `RECORD` message from the tap then looks like (example using `subscriptions_country` stream):

*Note: Every* `RECORD` *message must be on its own line to stdout, but the example here uses multiple lines for readability.*
```
{
    "type": "RECORD",
    "stream": "subscriptions_country", 
    "record": {
        "date": "2017-05-31T00:00:00.000000Z",
        "package_name": "packageABC", 
        "product_id": "product123", 
        "country": "AU", 
        "new_subscriptions": 9.0, 
        "cancelled_subscriptions": 9.0, 
        "active_subscriptions": 190.0, 
        "report_updated": "2017-10-15T21:18:33.158467Z"
        }, 
    "time_extracted": "2020-05-27T21:18:33.539135Z"
}
```
The tap determines if a `RECORD` message should be streamed to a target by comparing the `report_updated` value to the `last_report_updated` value in the [`state.json`](../example_state.json) file (or `start_date` in [`tap_config.json`](../example_config_tap.json)) which can be passed as an agrument to the tap when it is run at the command line.

While this approach ensures no data is left behind (e.g. when Google makes a data correction), it also means that rows of data in a `.csv` may be re-streamed unnecessarily since any change to a single row changes the `report_updated` in the corresponding `RECORD` messages for ***all rows in the*** `.csv` ***file***. This causes them to be interpreted by the tap as new data even if the underlying data in some of the rows has not changed.
