import setuptools  # type: ignore

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='wavetrak-cam-stills-helper',
    version='0.0.1',
    author='Wavetrak',
    author_email='nerdery@surfline.com',
    description=(
        'A library of common functions used to process, parse, and download'
        'various cam still files.'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url=(
        'https://github.com/Surfline/wavetrak-services/common/'
        'cam-stills-helper'
    ),
    packages=setuptools.find_packages(),
    package_data={'': ['py.typed']},
    install_requires=[
        'aiohttp>=3.6.2',
        'boto3>=1.14.13',
        'Pillow>=7.1.2',
        'requests>=2.24.0',
        'numpy>=1.19.0',
    ],
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
)
