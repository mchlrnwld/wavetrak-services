# Cam Stills Helper

A library of common functions used to interact with the collection of camera stills stored in S3.

Current functionality supports:
+ Validating camera existence using camera alias.
+ Querying and downloading still data within a user defined time range.

## Setup

See https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/763002972/Artifactory+Development+Workflow for instructions on getting setup to download/upload packages to Artifactory.

## Usage

After setting up pip with the instructions above, you can then install the package via pip:

```sh
$ pip install wavetrak-cam-stills-helper
```

Setup local `~/.aws/credentials` file to use prod access/secret keys as the default.

If using default host connect to the prod AWS VPN.

## Development

### Setup Environment

```sh
$ conda env create -f environment.yml
$ conda activate wavetrak-cam-stills-helper
```

Setup local `~/.aws/credentials` file to use prod access/secret keys as the default.

If using default host connect to the prod AWS VPN.

### Install Local Package

```sh
$ pip install -e /path/to/cam-stills-helper/
```

### Deploy

```sh
$ ENV=dev make deploy
```

## How To Use

### Introduction

Example below shows how you can use the cam_stills_helper package to
query stills within a specified time range:

```python
import cam_stills_helper
from datetime import datetime

# Initialize CameraStillsUtil using prod cameras-service host
cam_stills_util = cam_stills_helper.CameraStillsUtil()

# To query stills from wc-lowers for a given time range
cams = ['58a37530c9d273fd4f581beb']
for lowers_cam in cam_stills_util.get_cameras(cams):
  for batch in lowers_cam.get_still_batches(datetime(2020, 3, 16, 0, 0), datetime(2020, 3, 16, 0, 50), 2):
    print(batch)

    # Get individual stills one at a time
    for still in batch.stills:
      print(still)
      print(still.url)
      print(still.timestamp)
      print(still.get_data())

    # Or get the full batch of stills' data & timestamps
    batch_data = batch.get_batch()
    model.predict(batch_data['data'])  # for example to run inference directly
    timestamps = batch_data['timestamps']    
```

### Important Functions

#### Validate Camera Alias using CameraStillsUtil

```python
cam_stills_util = cam_stills_helper.CameraStillsUtil()
cam_stills_util.validate_camera_alias('wc-lowers')
```

| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| alias     | str  | cam alias to validate

| Returns       | Type   | Description
| ------------- | ------ | ----------------------------
|               | str    | camera ID for passed alias, None if camera doesn't exists

#### Fetch Camera objects

```python
cams = cam_stills_util.get_cameras(['58a37530c9d273fd4f581beb', '5834a1003421b20545c4b58e'])
lowers_cam = cams[0]
sunset_cam = cams[1]
```

| Parameter | Type       | Description
| --------- | ---------- | -----------------------------------------------
| cameras   | List[str]  | list of camera IDs

| Returns       | Type         | Description
| ------------- | ------------ | ----------------------------
|               | List[Camera] | list of Camera objects

#### Query Batches of stills from a Camera object

```python
lowers_batches = lowers_cam.get_still_batches(datetime(2020, 3, 16, 0, 0), datetime(2020, 3, 16, 0, 50), 2)
lowers_batch_1 = next(lowers_batches)
```

| Parameter  | Type              | Description
| ---------- | ----------------- | ----------------------------------------
| start_time | datetime.datetime | start of the time range to query stills
| end_time   | datetime.datetime | end of the time range to query stills
| batch_size | int               | number of stills to be included in a batch

Function returns a Generator that can then be used to fetch each batch individually.

| Returns     | Type  | Description
| ----------- | ----- | ----------------------------
| indiv batch | Batch | individual Batch object

#### Retrieve Individual Still objects

```python
lowers_batch_stills = lowers_batch_1.stills

still = lowers_batch_stills[0]
```

To get individual stills from a batch, just loop over the batch's property: `stills`.

| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| None      |      |

| Returns   | Type  | Description
| --------- | ----- | ----------------------------
|           | Still | Still object representing an individual still


#### Still Interaction & Download Data Function

```python
print(still)
print(still.url)
print(still.timestamp)
print(still.bucket_name)
print(still.object_key)
print(still.get_data())
```

get_data() function downloads a still and returns it as an np.ndarray

| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| None      |      |

| Returns   | Type       | Description
| --------- | ---------- | ----------------------------
|           | np.ndarray | numpy array of still data

#### Fetch dict of lists containing stills' timestamps & data from a batch

```python
batch_dict = lowers_batch_1.get_batch()

data = batch_dict['data']
timestamps = batch_dict['timestamps']
```
Dictionary structure: `{'data': List[np.ndarray],  'timestamps': List[datetime.datetime]}`

Still data is downloaded asynchronously, so be aware of the batch size that you use. Recommended to use size of 10.

| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| None      |      |

| Returns       | Type       | Description
| ------------- | ---------- | ----------------------------
|               | Dict[list] | dict of lists containing 'data' and 'timestamps' keys with full batch of data
