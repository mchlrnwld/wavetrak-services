import unittest.mock as mock
from datetime import datetime

import numpy as np  # type: ignore
import pytest  # type: ignore
import requests

from cam_stills_helper.util.batch import Batch
from cam_stills_helper.util.camera_stills_util import CameraStillsUtil
from cam_stills_helper.util.still import Still


@pytest.fixture
def stills_util():
    return CameraStillsUtil()


@pytest.fixture
def lowers_camera(stills_util):
    return stills_util.get_cameras(['58a37530c9d273fd4f581beb'])[0]


def mock_batch_generator():
    still_1 = Still(
        (
            's3://sl-cam-thumbnails-prod/wc-lowers/2020/02/16/'
            'feed_wc-lowers_1935.2020-02-16_full.jpg'
        ),
        datetime(2020, 2, 16, 19, 35),
    )

    still_2 = Still(
        (
            's3://sl-cam-thumbnails-prod/wc-lowers/2020/02/16/'
            'wc-lowers.20200216T194020352_full.jpg'
        ),
        datetime(2020, 2, 16, 19, 40, 20, 352000),
    )

    still_3 = Still(
        (
            's3://sl-cam-thumbnails-prod/wc-lowers/2020/05/05/'
            'wc-lowers.20200505T000556065_full.jpg'
        ),
        datetime(2020, 5, 5, 0, 5, 56, 65000),
    )

    batches = [Batch([still_1, still_2]), Batch([still_3])]

    for batch in batches:
        yield batch


def test_validate_alias(stills_util):
    """ test that we get the correct id back for wc-lowers """
    assert (
        stills_util.validate_camera_alias('wc-lowers')
        == '58a37530c9d273fd4f581beb'
    )


def test_non_existent_camera(stills_util):
    """ test that an exception is thrown for a camera id that doesn't exist """
    with pytest.raises(requests.HTTPError):
        stills_util.get_cameras(['393829292jdjsk3289'])


@mock.patch('cam_stills_helper.util.camera.Camera.get_still_batches')
def test_single_still_extraction(lowers_camera):
    """ test that we query the correct still for given timeframe """
    lowers_camera.get_still_batches.return_value = mock_batch_generator()

    batches = lowers_camera.get_still_batches(
        datetime(2020, 5, 5, 0, 0), datetime(2020, 5, 5, 0, 15), 2
    )

    next(batches)
    batch = next(batches)

    still = batch.stills[0]

    lowers_camera.get_still_batches.assert_called_with(
        datetime(2020, 5, 5, 0, 0), datetime(2020, 5, 5, 0, 15), 2
    )

    assert still.url == (
        'https://camstills.cdn-surfline.com/wc-lowers/2020'
        '/05/05/wc-lowers.20200505T000556065_full.jpg'
    )
    assert still.timestamp == datetime(2020, 5, 5, 0, 5, 56, 65000)
    assert still.bucket_name == 'sl-cam-thumbnails-prod'
    assert still.object_key == (
        'wc-lowers/2020/05/05/wc-lowers.20200505T000556065_full.jpg'
    )
    assert isinstance(still.get_data(), np.ndarray)


@mock.patch('cam_stills_helper.util.camera.Camera.get_still_batches')
def test_multiple_stills_extraction(lowers_camera):
    """ test that we query multiple stills for given timeframe """
    lowers_camera.get_still_batches.return_value = mock_batch_generator()

    batches = lowers_camera.get_still_batches(
        datetime(2020, 2, 16, 19, 35), datetime(2020, 2, 16, 19, 41), 2
    )

    batch = next(batches)

    still_1 = batch.stills[0]
    still_2 = batch.stills[1]

    lowers_camera.get_still_batches.assert_called_with(
        datetime(2020, 2, 16, 19, 35), datetime(2020, 2, 16, 19, 41), 2
    )

    assert still_1.url == (
        'https://camstills.cdn-surfline.com/wc-lowers/2020'
        '/02/16/feed_wc-lowers_1935.2020-02-16_full.jpg'
    )
    assert still_1.timestamp == datetime(2020, 2, 16, 19, 35)
    assert still_1.bucket_name == 'sl-cam-thumbnails-prod'
    assert still_1.object_key == (
        'wc-lowers/2020/02/16/feed_wc-lowers_1935.2020-02-16_full.jpg'
    )
    assert isinstance(still_1.get_data(), np.ndarray)

    assert still_2.url == (
        'https://camstills.cdn-surfline.com/wc-lowers/2020'
        '/02/16/wc-lowers.20200216T194020352_full.jpg'
    )
    assert still_2.timestamp == datetime(2020, 2, 16, 19, 40, 20, 352000)
    assert still_2.bucket_name == 'sl-cam-thumbnails-prod'
    assert still_2.object_key == (
        'wc-lowers/2020/02/16/wc-lowers.20200216T194020352_full.jpg'
    )
    assert isinstance(still_2.get_data(), np.ndarray)


@mock.patch('cam_stills_helper.util.camera.Camera.get_still_batches')
def test_multiple_stills_get_batch_function(lowers_camera):
    """ test that we query multiple stills within a batch for timeframe """
    lowers_camera.get_still_batches.return_value = mock_batch_generator()
    batched_stills_gen = lowers_camera.get_still_batches(
        datetime(2020, 2, 16, 19, 35), datetime(2020, 2, 16, 19, 41), 2
    )
    batch_of_stills = next(batched_stills_gen)

    batch = batch_of_stills.get_batch()

    lowers_camera.get_still_batches.assert_called_with(
        datetime(2020, 2, 16, 19, 35), datetime(2020, 2, 16, 19, 41), 2
    )

    assert isinstance(batch['data'][0], np.ndarray)
    assert batch['timestamps'][0] == datetime(2020, 2, 16, 19, 35)

    assert isinstance(batch['data'][1], np.ndarray)
    assert batch['timestamps'][1] == datetime(2020, 2, 16, 19, 40, 20, 352000)
