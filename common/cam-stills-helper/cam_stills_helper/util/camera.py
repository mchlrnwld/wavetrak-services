import logging
import time
from datetime import datetime, timezone
from typing import Generator, List, Tuple

import boto3  # type: ignore
import requests

from cam_stills_helper.util.batch import Batch
from cam_stills_helper.util.still import Still

logger = logging.getLogger(__name__)


class Camera:
    """
    Camera class is used to interact with individual cameras.

    Contains function to fetch stills for a specific date, by pulling stills
    from Athena and the cameras-service.

    Attributes:
        id: cameraId for the camera
        host_url: the host url to be used for the cameras-service
    """

    def __init__(self, id: str, host_url: str) -> None:
        self.id = id
        self.host_url = host_url

    def __str__(self) -> str:
        return f'Camera with cameraId: {self.id}'

    def get_still_batches(
        self, start_time: datetime, end_time: datetime, batch_size: int
    ) -> Generator[Batch, None, None]:
        """
        Can be used by the user to query stills in batches for a specific
        date range.

        Args:
            start_time: start of the time range to query stills
            end_time: end of the time range to query stills
            batch_size: number of stills to be included in a batch
        Returns:
            yields a Batch of stills one at a time
        """
        stills_list = self._build_stills_list(start_time, end_time)

        batch = []
        for still in stills_list:
            batch.append(still)
            if len(batch) >= batch_size:
                yield Batch(batch)
                batch = []

        if len(batch) > 0:
            yield Batch(batch)

    def _build_stills_list(
        self, start_time: datetime, end_time: datetime
    ) -> List[Still]:
        """
        Builds list of Still object with stills pulled from athena and
        the cameras-service.

        Args:
            start_time: start of the time range to query stills
            end_time: end of the time range to query stills
        Returns:
            Generator used to yield each still
        """
        still_data = self._pull_still_info_from_athena(start_time, end_time)

        still_data.extend(
            self._pull_still_info_from_cameras_service(start_time, end_time)
        )

        return [
            Still(
                still[0], datetime.strptime(still[1], '%Y-%m-%d %H:%M:%S.%f')
            )
            for still in still_data
        ]

    def _pull_still_info_from_athena(
        self, start_time: datetime, end_time: datetime
    ) -> List[Tuple[str, str, str]]:
        """
        Pulls stills data from athena using boto3.

        Stills are returned in Tuples: (path, takenDate, cameraId)

        Args:
            start_time: start of the time range to query stills
            end_time: end of the time range to query stills
        Returns:
            List of Tuples containing query results
        """
        athena_client = boto3.client('athena')

        query = (
            f'SELECT * FROM "wt-camera-stills-archive-records-db-prod".'
            f'"wt_camera_stills_archive_records_prod" WHERE id ='
            f' \'{self.id}\' AND taken_date BETWEEN TIMESTAMP'
            f' \'{start_time}\' AND TIMESTAMP \'{end_time}\' ORDER BY '
            'taken_date;'
        )

        try:
            query_exec_id = athena_client.start_query_execution(
                QueryString=query,
                ResultConfiguration={
                    'OutputLocation': (
                        's3://wt-camera-stills-archive-records-query-results-'
                        'prod/results/'
                    )
                },
            )['QueryExecutionId']
        except BaseException:
            logger.error(
                'Error occurred starting athena query execution for camera '
                f'{self.id}.'
            )
            raise

        # fetch query results
        query_status = None
        while (
            query_status == 'QUEUED'
            or query_status == 'RUNNING'
            or query_status is None
        ):
            query_status = athena_client.get_query_execution(
                QueryExecutionId=query_exec_id
            )['QueryExecution']['Status']['State']

            if query_status == 'FAILED' or query_status == 'CANCELLED':
                raise Exception(
                    f'Athena query for id: {self.id}, start_time: '
                    f'{start_time}, and end_time: {end_time} '
                    f'was {query_status}'
                )
            time.sleep(5)

        results_paginator = athena_client.get_paginator('get_query_results')
        results_iter = results_paginator.paginate(
            QueryExecutionId=query_exec_id
        )

        # Parse query results
        data_list = [
            row['Data']
            for results_page in results_iter
            for row in results_page['ResultSet']['Rows']
        ]

        results = [
            [x['VarCharValue'] if 'VarCharValue' in x else '' for x in datum]
            for datum in data_list[1:]
        ]

        return [tuple(x) for x in results]

    def _pull_still_info_from_cameras_service(
        self, start_time: datetime, end_time: datetime
    ) -> List[Tuple[str, str, str]]:
        """
        Pulls stills data from the cameras-service.

        Stills are returned as Tuples: (url, takenDate, cameraId)

        Args:
            start_time: start of the time range to query stills
            end_time: end of the time range to query stills
        Returns:
            List of Tuples containing query results
        """
        cameras_service_url = (
            f'{self.host_url}/recording/id/{self.id}?startDate='
            f'{start_time.replace(tzinfo=timezone.utc).timestamp() * 1000}'
            '&endDate='
            f'{end_time.replace(tzinfo=timezone.utc).timestamp() * 1000}'
            '&includeExpired=true&allowPartialMatch=true'
        )

        try:
            response = requests.get(cameras_service_url).json()
        except BaseException:
            logger.error(
                'Error occurred sending request to cameras-service for '
                f'camera: {self.id}'
            )
            raise

        stills_data = []
        for doc in response:
            # startDate in mongo doesn't have the milliseconds
            # so parse takenDate from the url
            cam_info = doc['thumbLargeUrl'].split('/')[-1].split('.')
            taken_date = datetime.strptime(
                cam_info[1].split('_')[0], '%Y%m%dT%H%M%S%f'
            )

            # check that still is within time range, this prevents the
            # one additional still we get from using allowPartialMatch
            if not (start_time <= taken_date <= end_time):
                continue

            still_record = (
                doc['thumbLargeUrl'],
                taken_date.strftime('%Y-%m-%d %H:%M:%S.%f'),
                doc['cameraId'],
            )
            stills_data.append(still_record)

        # Reserve stills_data so that stills are in ascending order
        stills_data.reverse()

        return stills_data
