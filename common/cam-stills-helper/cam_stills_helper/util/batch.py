import asyncio
import logging
from collections import defaultdict
from io import BytesIO
from typing import DefaultDict, List

import aiohttp
import async_timeout
import numpy as np  # type: ignore
from PIL import Image  # type: ignore

from cam_stills_helper.util.still import Still

logger = logging.getLogger(__name__)


class Batch:
    """
    The Batch class is used to store indivdual Stills.

    Can use get_stills() to interact with individual Still objects, or can
    fetch all stills' timestamps and data using get_batch().

    Attributes:
        stills: list of the stills contained in the Batch
        batch_size: number of stills in the batch
    """

    def __init__(self, stills: List[Still]):
        self.stills = stills
        self.batch_size = len(stills)

    def __str__(self) -> str:
        return f'Batch of {self.batch_size} stills'

    def get_batch(self) -> DefaultDict[str, list]:
        """
        Retrieves a batch of stills in dictionary form.

        Returns:
            defaultdict contains stills' data and timestamps
        """
        batch: DefaultDict[str, list] = defaultdict(list)

        stills_data = asyncio.run(self._download_all())
        stills_timestamps = [still.timestamp for still in self.stills]

        for data, timestamp in zip(stills_data, stills_timestamps):
            if data is not np.ndarray([]):
                batch['data'].append(data)
                batch['timestamps'].append(timestamp)
        return batch

    async def _download_all(self) -> List[np.ndarray]:
        """
        Gathers the async tasks, and starts the async batch download tasks.

        Returns:
            list of data downloaded for each still passed in
        """
        async with aiohttp.ClientSession() as session:
            tasks = [
                self._download_still(still, session) for still in self.stills
            ]
            results = await asyncio.gather(*tasks)

        return results

    async def _download_still(
        self, still: Still, session: aiohttp.ClientSession
    ) -> np.ndarray:
        """
        Downloads an indiviudal still's data. Will timeout if request takes
        longer than 10 seconds.

        Args:
            still: still to download data for
            session: ClientSession used to download data
        Returns:
            np.ndarray data for passed Still object
        """
        try:
            with async_timeout.timeout(10):
                async with session.get(
                    still.url, raise_for_status=True
                ) as response:
                    raw_still = await response.content.read()
                await response.release()
        except aiohttp.ClientResponseError:
            logger.error(
                f'ClientResponseError occurred while downloading still with '
                f'url: {still.url}'
            )
            return np.array([])

        try:
            image_array = np.array(Image.open(BytesIO(raw_still)))
        except TypeError:
            logger.error(
                'TypeError occurred converting image byte data to np array for'
                f' still with url: {still.url}'
            )
            return np.array([])

        return image_array
