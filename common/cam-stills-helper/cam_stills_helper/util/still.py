import asyncio
import logging
from datetime import datetime
from io import BytesIO

import aiohttp
import async_timeout
import numpy as np  # type: ignore
from PIL import Image  # type: ignore

logger = logging.getLogger(__name__)


class Still:
    """
    The still class is used to represent an indivdual still stored in S3.

    Contains the url and taken date for the still, and contains a function
    get_data() to return np.ndarray data for the still.

    Attributes:
        url: url for where to download/view the still
        timestamp: datetime object representing when the still was taken
        bucket_name: name of the bucket where still is stored in S3
        object_key: name of the object key for still in S3
    """

    def __init__(self, path: str, taken_date: datetime):
        if 's3://sl-cam-thumbnails-prod/' in path:
            self.bucket_name = 'sl-cam-thumbnails-prod'
            self.object_key = path.replace('s3://sl-cam-thumbnails-prod/', '')
            self.url = f'https://camstills.cdn-surfline.com/{self.object_key}'
        elif 's3://sl-camera-stills/' in path:
            self.bucket_name = 'sl-camera-stills'
            self.object_key = path.replace('s3://sl-camera-stills/', '')
            self.url = (
                f'https://d2lktm5re9lz5j.cloudfront.net/{self.object_key}'
            )
        else:
            self.bucket_name = 'sl-cam-thumbnails-prod'
            self.object_key = path.replace(
                'https://camstills.cdn-surfline.com/', ''
            )
            self.url = path

        self.timestamp = taken_date

    def __str__(self) -> str:
        return f'Cam Still with url: {self.url} and taken at {self.timestamp}'

    def get_data(self) -> np.ndarray:
        """
        Runs the _download_still coroutine, and returns it's result.

        Returns:
            np.ndarray data for the downloaded still
        """
        return asyncio.run(self._download_still())

    async def _download_still(self) -> np.ndarray:
        """
        Uses aiohttp ClientSession to download the data for
        individual still using it's url.

        Returns:
            np.ndarray data for the downloaded still
        """
        try:
            async with aiohttp.ClientSession() as session:
                with async_timeout.timeout(10):
                    async with session.get(
                        self.url, raise_for_status=True
                    ) as response:
                        raw_still = await response.content.read()
                    await response.release()
        except aiohttp.ClientResponseError:
            logger.error(
                f'ClientResponseError occurred while downloading still with '
                f'url: {self.url}'
            )
            return np.array([])

        try:
            image_array = np.array(Image.open(BytesIO(raw_still)))
        except TypeError:
            logger.error(
                'TypeError occurred converting image byte data to np array for'
                f' still with url: {self.url}'
            )
            return np.array([])

        return image_array
