import logging
from typing import List, Optional

import requests

from cam_stills_helper.util.camera import Camera

logger = logging.getLogger(__name__)


class CameraStillsUtil:
    """
    CameraStillsUtil is the entrypoint to the stills helper interface.

    Intiliazed by the user, using the host to pull stills from: sandbox,
    staging, or prod.

    Attributes:
        cameras_service_host: the host url to be used for the cameras-service
    """

    def __init__(
        self,
        cameras_service_host: str = 'http://cameras-service.prod.surfline.com',
    ) -> None:
        try:
            response = requests.get(cameras_service_host + '/health')
            response.raise_for_status()
        except BaseException:
            logger.error('Unable to connect to cameras-service host.')
            raise

        self.host_url = cameras_service_host + '/cameras'

    def __str__(self) -> str:
        return f'CameraStillsUtil using cameras-service: {self.host_url}'

    def validate_camera_alias(self, alias: str) -> Optional[str]:
        """
        Validates that the camera exists, and returns camera ID for specified
        camera.

        Args:
            alias: alias name for the camera
        Returns:
            cameraId for the camera if it exists
        Raises:
            HTTPError: exception will be thrown if the camera for the alias
                       doesn't exist
        """
        cameras_service_url = f'{self.host_url}/ValidateAlias?alias={alias}'

        try:
            response = requests.get(cameras_service_url)
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 400:
                logger.error('Camera does not exist for alias.')
                return None

            logger.error(f'HTTPError was thrown validating alias: {e}')
            raise

        return response.json()['_id']

    def get_cameras(self, cameras: List[str]) -> List[Camera]:
        """
        Returns a list of Camera objects for each camera ID passed in.

        Args:
            cameras: list of strings of cameraIds for cameras to query stills
                     from
        Returns:
            Camera object that is used to query stills
        Raises:
            HTTPError: exception will be thrown if any of the cameraIds given
            don't have an existing camera in the cameras-service.
        """
        # Check if camera exists for each given camera_id
        for camera_id in cameras:
            try:
                response = requests.get(f'{self.host_url}/{camera_id}')
                response.raise_for_status()
            except BaseException:
                logger.error(f'Error occurred validating camera {camera_id}.')
                raise

        return [Camera(cam_id, self.host_url) for cam_id in cameras]
