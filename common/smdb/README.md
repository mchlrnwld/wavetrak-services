# Common - Science Models DB

A collection of packages and tools to support Science Models DB development. Directory structure follows the [Standard Go Project Layout](https://github.com/golang-standards/project-layout) convention.

- [Swells Writer](docs/swells-writer.md)
- [Swells Ensemble Writer](docs/swells-ensemble-writer.md)
- [Weather Writer](docs/weather-writer.md)
