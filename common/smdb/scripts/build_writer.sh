#!/usr/bin/env bash
set -e

PLATFORMS=("darwin/amd64" "linux/amd64")

for platform in "${PLATFORMS[@]}"; do
  platform_split=(${platform//\// })
  os="${platform_split[0]}"
  arch="${platform_split[1]}"
  filename="${1}-${os}-${arch}"
  GOOS="${os}" GOARCH="${arch}" go build -o "${OUTPUT}/${filename}" "./cmd/${1}"
done
