package schemas

// SwellsRecord schema
type SwellsRecord struct {
	Height              float32
	Period              float32
	Direction           float32
	SwellWave1Height    float32
	SwellWave1Period    float32
	SwellWave1Direction float32
	SwellWave2Height    float32
	SwellWave2Period    float32
	SwellWave2Direction float32
	SwellWave3Height    float32
	SwellWave3Period    float32
	SwellWave3Direction float32
	SwellWave4Height    float32
	SwellWave4Period    float32
	SwellWave4Direction float32
	SwellWave5Height    float32
	SwellWave5Period    float32
	SwellWave5Direction float32
	WindWaveHeight      float32
	WindWavePeriod      float32
	WindWaveDirection   float32
}
