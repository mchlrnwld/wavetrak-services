package schemas

// SwellsEnsembleRecord schema
type SwellsEnsembleRecord struct {
	WindDirection        float32
	WindSpeed            float32
	WindWaveDirection    float32
	WindWaveHeight       float32
	WindWavePeriod       float32
	WindAndSwellHeight   float32
	PrimaryWaveDirection float32
	PrimaryWavePeriod    float32
	SwellWave1Direction  float32
	SwellWave1Height     float32
	SwellWave1Period     float32
	SwellWave2Direction  float32
	SwellWave2Height     float32
	SwellWave2Period     float32
}
