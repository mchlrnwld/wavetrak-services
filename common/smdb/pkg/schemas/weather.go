package schemas

// PrecipitationType enumerated
type PrecipitationType uint8

var precipitationTypeStrings = [...]string{
	"NONE",
	"RAIN",
	"ICE_PELLETS",
	"FREEZING_RAIN",
	"SNOW",
}

var maxPrecipitationType = PrecipitationType(len(precipitationTypeStrings) - 1)

func (pt PrecipitationType) String() string {
	if pt < 0 || pt > maxPrecipitationType {
		return ""
	}
	return precipitationTypeStrings[pt]
}

const (
	PTNone PrecipitationType = iota + 1
	PTRain
	PTIcePellets
	PTFreezingRain
	PTSnow
)

// WeatherConditions enumerated
type WeatherConditions uint8

var weatherConditionStrings = [...]string{
	"CLEAR",
	"MOSTLY_CLEAR",
	"MOSTLY_CLOUDY",
	"CLOUDY",
	"OVERCAST",
	"MIST",
	"FOG",
	"DRIZZLE",
	"BRIEF_SHOWERS_POSSIBLE",
	"BRIEF_SHOWERS",
	"LIGHT_SHOWERS_POSSIBLE",
	"LIGHT_SHOWERS",
	"SHOWERS",
	"HEAVY_SHOWERS_POSSIBLE",
	"HEAVY_SHOWERS",
	"LIGHT_RAIN",
	"LIGHT_RAIN_AND_FOG",
	"RAIN",
	"RAIN_AND_FOG",
	"HEAVY_RAIN",
	"TORRENTIAL_RAIN",
	"ICE_PELLETS",
	"BRIEF_ICE_PELLETS_SHOWERS",
	"ICE_PELLETS_SHOWERS",
	"FREEZING_RAIN",
	"BRIEF_FREEZING_RAIN_SHOWERS",
	"FREEZING_RAIN_SHOWERS",
	"LIGHT_SNOW",
	"SNOW",
	"HEAVY_SNOW",
	"BLIZZARD",
	"SEVERE_BLIZZARD",
	"BRIEF_SNOW_FLURRIES",
	"BRIEF_SNOW_SHOWERS",
	"LIGHT_SNOW_SHOWERS",
	"SNOW_SHOWERS",
	"BRIEF_HEAVY_SNOW_SHOWERS",
	"HEAVY_SNOW_SHOWERS",
	"SNOW_SQUALLS",
	"SEVERE_SNOW_SQUALLS",
	"THUNDER_SHOWERS",
	"HEAVY_THUNDER_SHOWERS",
	"THUNDER_STORMS",
	"HEAVY_THUNDER_STORMS",
	"SEVERE_THUNDER_STORMS",
}

var maxWeatherConditions = WeatherConditions(len(weatherConditionStrings) - 1)

func (wc WeatherConditions) String() string {
	if wc < 0 || wc > maxWeatherConditions {
		return ""
	}
	return weatherConditionStrings[wc]
}

const (
	WCClear WeatherConditions = iota + 1
	WCMostlyClear
	WCMostlyCloudy
	WCCloudy
	WCOvercast
	WCMist
	WCFog
	WCDrizzle
	WCBriefShowersPossible
	WCBriefShowers
	WCLightShowersPossible
	WCLightShowers
	WCShowers
	WCHeavyShowersPossible
	WCHeavyShowers
	WCLightRain
	WCLightRainAndFog
	WCRain
	WCRainAndFog
	WCHeavyRain
	WCTorrentialRain
	WCIcePellets
	WCBriefIcePelletsShowers
	WCIcePelletsShowers
	WCFreezingRain
	WCBriefFreezingRainShowers
	WCFreezingRainShowers
	WCLightSnow
	WCSnow
	WCHeavySnow
	WCBlizzard
	WCSevereBlizzard
	WCBriefSnowFlurries
	WCBriefSnowShowers
	WCLightSnowShowers
	WCSnowShowers
	WCBriefHeavySnowShowers
	WCHeavySnowShowers
	WCSnowSqualls
	WCSevereSnowSqualls
	WCThunderShowers
	WCHeavyThunderShowers
	WCThunderStorms
	WCHeavyThunderStorms
	WCSevereThunderStorms
)

// WeatherRecord schema
type WeatherRecord struct {
	WindU               float32
	WindV               float32
	WindGust            float32
	Pressure            float32
	Temperature         float32
	Visibility          float32
	Dewpoint            float32
	Humidity            float32
	PrecipitationType   PrecipitationType
	PrecipitationVolume float32
	WeatherConditions   WeatherConditions
}
