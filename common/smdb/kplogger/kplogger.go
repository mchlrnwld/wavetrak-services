package kplogger

import (
	"fmt"
	"log"
	"strings"

	producer "github.com/Surfline/kinesis-producer"
)

// KPLogger implements the Logger interface using the standard library logger,
// with a Debug field included that can be used during development
type KPLogger struct {
	Logger *log.Logger
	Debug  bool
}

// Info prints log message only if the Debug field is set to true
func (l *KPLogger) Info(msg string, values ...producer.LogValue) {
	if l.Debug {
		l.Logger.Print(msg, l.valuesToString(values...))
	}
}

// Error prints log message
func (l *KPLogger) Error(msg string, err error, values ...producer.LogValue) {
	l.Logger.Print(msg, l.valuesToString(values...), err)
}

func (l *KPLogger) valuesToString(values ...producer.LogValue) string {
	parts := make([]string, len(values))
	for i, v := range values {
		parts[i] = fmt.Sprint(v)
	}
	return strings.Join(parts, ", ")
}
