package writer

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	producer "github.com/Surfline/kinesis-producer"
	"github.com/Surfline/wavetrak-services/common/smdb/internal/keyvalue"
	"github.com/Surfline/wavetrak-services/common/smdb/kplogger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
)

// WriteStream takes stream records and writes them to an AWS Kinesis Data Stream.
func WriteStream(lineToKeyValue func(string) (*keyvalue.KeyValue, error)) {
	log := log.New(os.Stderr, "kinesis-producer: ", 0)

	agency, ok := os.LookupEnv("AGENCY")
	if !ok {
		// TODO: remove support for MODEL_AGENCY once GFS has migrated
		// to the new naming conventions
		agency, ok = os.LookupEnv("MODEL_AGENCY")
		if !ok {
			log.Fatal(errors.New("AGENCY or MODEL_AGENCY required"))
		}
	}

	model, ok := os.LookupEnv("MODEL")
	if !ok {
		// TODO: remove support for MODEL_NAME once GFS has migrated
		// to the new naming conventions
		model, ok = os.LookupEnv("MODEL_NAME")
		if !ok {
			log.Fatal(errors.New("MODEL or MODEL_NAME required"))
		}
	}

	run, ok := os.LookupEnv("RUN")
	if !ok {
		// TODO: remove support for MODEL_RUN once GFS has migrated
		// to the new naming conventions
		run, ok = os.LookupEnv("MODEL_RUN")
		if !ok {
			log.Fatal(errors.New("RUN or MODEL_RUN required"))
		}
	}

	streamName, ok := os.LookupEnv("STREAM_NAME")
	if !ok {
		log.Fatal(errors.New("STREAM_NAME required"))
	}

	debugFlag := flag.Bool("debug", false, "a bool")

	flag.Parse()

	client := kinesis.New(session.New(aws.NewConfig()))
	kp := producer.New(&producer.Config{
		StreamName: streamName,
		Client:     client,
		Logger: &kplogger.KPLogger{
			Logger: log,
			Debug:  *debugFlag,
		},
	})
	kp.Start()
	defer kp.Stop()

	go func() {
		for failureInfo := range kp.NotifyFailures() {
			if strings.Contains(failureInfo.Error(), "RequestError") {
				if pErr := kp.Put(failureInfo.Data, failureInfo.PartitionKey); pErr != nil {
					log.Fatal(pErr)
				}
				continue
			}

			log.Fatal(fmt.Errorf("Producer Failure: %v", failureInfo.Error()))
		}
	}()

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		kv, kvErr := lineToKeyValue(scanner.Text())
		if kvErr != nil {
			log.Fatal(kvErr)
		}

		key := fmt.Sprintf("%s,%s,%s,%s", agency, model, run, kv.Key)
		if pErr := kp.Put(kv.Value, key); pErr != nil {
			log.Fatal(pErr)
		}
	}

	if sErr := scanner.Err(); sErr != nil {
		log.Fatal(sErr)
	}
}
