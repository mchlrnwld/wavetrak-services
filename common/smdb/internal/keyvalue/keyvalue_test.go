package keyvalue

import (
	"bytes"
	"encoding/binary"
	"testing"

	"github.com/stretchr/testify/require"
)

type sample struct {
	Field1 uint8
	Field2 float32
}

func TestParseKeys(t *testing.T) {
	keys := [5]string{"0p25", "-0.25", "0.0", "2019-04-30T06:00:00Z"}
	keyBytes, err := ParseKeysToBytes(keys)
	require.NoError(t, err)
	require.Equal(t, "0p25,-0.25,0,2019-04-30T06:00:00Z", string(keyBytes))
	ensembleKeysSingleDigit := [5]string{"glo_30m", "80.0", "-40.0", "2019-04-30T06:00:00Z", "1"}
	ensembleKeyBytesSingleDigit, err := ParseKeysToBytes(ensembleKeysSingleDigit)
	require.NoError(t, err)
	require.Equal(t, "glo_30m,80,-40,2019-04-30T06:00:00Z,01", string(ensembleKeyBytesSingleDigit))
	ensembleKeysDoubleDigit := [5]string{"glo_30m", "80.0", "-40.0", "2019-04-30T06:00:00Z", "15"}
	ensembleKeyBytesDoubleDigit, err := ParseKeysToBytes(ensembleKeysDoubleDigit)
	require.NoError(t, err)
	require.Equal(t, "glo_30m,80,-40,2019-04-30T06:00:00Z,15", string(ensembleKeyBytesDoubleDigit))
}

func TestParseValue(t *testing.T) {
	value := sample{12, 3.4}
	valueBytes, pErr := ParseValueToBytes(value)
	require.NoError(t, pErr)

	var result sample
	buf := bytes.NewReader(valueBytes)
	rErr := binary.Read(buf, binary.BigEndian, &result)
	require.NoError(t, rErr)
	require.Equal(t, value, result)
}
