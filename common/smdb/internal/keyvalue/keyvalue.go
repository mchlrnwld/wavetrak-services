package keyvalue

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"strconv"
)

// KeyValue struct contains byte arrays to be translated into a record.
type KeyValue struct {
	Key   []byte
	Value []byte
}

// ParseKeysToBytes parses a 5 string array of grid, latitude, longitude, and timestamp and optional memberID into a bytes array that represents a key.
func ParseKeysToBytes(keys [5]string) ([]byte, error) {
	grid := keys[0]
	latitude, err := strconv.ParseFloat(keys[1], 32)
	if err != nil {
		return nil, err
	}
	longitude, err := strconv.ParseFloat(keys[2], 32)
	if err != nil {
		return nil, err
	}
	timestamp := keys[3]
	var key string
	if keys[4] == "" {
		key = fmt.Sprintf("%v,%v,%v,%v", grid, float32(latitude), float32(longitude), timestamp)
	} else {
		memberID, err := strconv.ParseInt(keys[4], 10, 32)
		if err != nil {
			return nil, err
		}
		key = fmt.Sprintf("%v,%v,%v,%v,%02d", grid, float32(latitude), float32(longitude), timestamp, int32(memberID))
	}
	return []byte(key), nil
}

// ParseValueToBytes parses a value into bytes. The value must be a type with fixed size (i.e. no strings or slices).
func ParseValueToBytes(value interface{}) ([]byte, error) {
	buf := new(bytes.Buffer)
	if err := binary.Write(buf, binary.BigEndian, value); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
