# Weather Writer

A CLI tool for streaming weather records from stdin into a Badger database and an AWS Kinesis Stream.

## Quickstart

```sh
$ for i in {1..10}; do for j in {1..10}; do echo "GLOB_30m,${i},${j},2018-10-26T00:00:00Z,0,0,0,0,0,0,0,0,0,0,0"; done; done | STREAM_NAME=wt-science-models-db-sandbox AWS_REGION=us-west-1 ./bin/weather-writer-darwin-amd64
```
## Development/Debugging

For development and debugging, there is an optional `-debug` flag for the kinesis-producer package. To do so, specify the `-debug`
flag after the executable like so:
```
./bin/weather-writer-darwin-amd64 -debug
```
This will turn on additional `kinesis-producer` logging, for example:
```
kinesis-producer: starting producer stream=wt-science-models-db-sandbox
kinesis-producer: stopping producer backlog=%!s(int=0)
kinesis-producer: backlog drained
kinesis-producer: flushing records reason=drain,  records=%!s(int=1)
kinesis-producer: stopped producer
```
