module github.com/Surfline/wavetrak-services/common/smdb

go 1.12

require (
	github.com/Surfline/kinesis-producer v0.2.1-0.20200109173925-b51f8deaaf72
	github.com/aws/aws-sdk-go v1.29.12
	github.com/stretchr/testify v1.4.0
)
