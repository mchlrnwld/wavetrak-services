package main

import (
	"strconv"
	"strings"

	"github.com/Surfline/wavetrak-services/common/smdb/internal/keyvalue"
	"github.com/Surfline/wavetrak-services/common/smdb/internal/writer"
	"github.com/Surfline/wavetrak-services/common/smdb/pkg/schemas"
)

func parseWeatherRecord(values [11]string) (*schemas.WeatherRecord, error) {
	windU, err := strconv.ParseFloat(values[0], 32)
	if err != nil {
		return nil, err
	}
	windV, err := strconv.ParseFloat(values[1], 32)
	if err != nil {
		return nil, err
	}
	windGust, err := strconv.ParseFloat(values[2], 32)
	if err != nil {
		return nil, err
	}
	pressure, err := strconv.ParseFloat(values[3], 32)
	if err != nil {
		return nil, err
	}
	temperature, err := strconv.ParseFloat(values[4], 32)
	if err != nil {
		return nil, err
	}
	visibility, err := strconv.ParseFloat(values[5], 32)
	if err != nil {
		return nil, err
	}
	dewpoint, err := strconv.ParseFloat(values[6], 32)
	if err != nil {
		return nil, err
	}
	humidity, err := strconv.ParseFloat(values[7], 32)
	if err != nil {
		return nil, err
	}
	precipitationType, err := strconv.ParseUint(values[8], 10, 8)
	if err != nil {
		return nil, err
	}
	precipitationVolume, err := strconv.ParseFloat(values[9], 32)
	if err != nil {
		return nil, err
	}
	weatherConditions, err := strconv.ParseUint(values[10], 10, 8)
	if err != nil {
		return nil, err
	}

	return &schemas.WeatherRecord{
		WindU:               float32(windU),
		WindV:               float32(windV),
		WindGust:            float32(windGust),
		Pressure:            float32(pressure),
		Temperature:         float32(temperature),
		Visibility:          float32(visibility),
		Dewpoint:            float32(dewpoint),
		Humidity:            float32(humidity),
		PrecipitationType:   schemas.PrecipitationType(precipitationType),
		PrecipitationVolume: float32(precipitationVolume),
		WeatherConditions:   schemas.WeatherConditions(weatherConditions),
	}, nil
}

func csvToWeatherRecordKV(csv string) (*keyvalue.KeyValue, error) {
	kv := strings.Split(csv, ",")

	var keys [5]string
	copy(keys[:], kv[:4])
	keyBytes, err := keyvalue.ParseKeysToBytes(keys)
	if err != nil {
		return nil, err
	}

	var values [11]string
	copy(values[:], kv[4:])
	value, err := parseWeatherRecord(values)
	if err != nil {
		return nil, err
	}
	valueBytes, err := keyvalue.ParseValueToBytes(value)
	if err != nil {
		return nil, err
	}

	return &keyvalue.KeyValue{
		Key:   keyBytes,
		Value: valueBytes,
	}, nil
}

func main() {
	writer.WriteStream(csvToWeatherRecordKV)
}
