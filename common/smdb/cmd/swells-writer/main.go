package main

import (
	"strconv"
	"strings"

	"github.com/Surfline/wavetrak-services/common/smdb/internal/keyvalue"
	"github.com/Surfline/wavetrak-services/common/smdb/internal/writer"
	"github.com/Surfline/wavetrak-services/common/smdb/pkg/schemas"
)

func parseSwellsRecord(values [21]string) (*schemas.SwellsRecord, error) {
	height, err := strconv.ParseFloat(values[0], 32)
	if err != nil {
		return nil, err
	}
	period, err := strconv.ParseFloat(values[1], 32)
	if err != nil {
		return nil, err
	}
	direction, err := strconv.ParseFloat(values[2], 32)
	if err != nil {
		return nil, err
	}
	swellWave1Height, err := strconv.ParseFloat(values[3], 32)
	if err != nil {
		return nil, err
	}
	swellWave1Period, err := strconv.ParseFloat(values[4], 32)
	if err != nil {
		return nil, err
	}
	swellWave1Direction, err := strconv.ParseFloat(values[5], 32)
	if err != nil {
		return nil, err
	}
	swellWave2Height, err := strconv.ParseFloat(values[6], 32)
	if err != nil {
		return nil, err
	}
	swellWave2Period, err := strconv.ParseFloat(values[7], 32)
	if err != nil {
		return nil, err
	}
	swellWave2Direction, err := strconv.ParseFloat(values[8], 32)
	if err != nil {
		return nil, err
	}
	swellWave3Height, err := strconv.ParseFloat(values[9], 32)
	if err != nil {
		return nil, err
	}
	swellWave3Period, err := strconv.ParseFloat(values[10], 32)
	if err != nil {
		return nil, err
	}
	swellWave3Direction, err := strconv.ParseFloat(values[11], 32)
	if err != nil {
		return nil, err
	}
	swellWave4Height, err := strconv.ParseFloat(values[12], 32)
	if err != nil {
		return nil, err
	}
	swellWave4Period, err := strconv.ParseFloat(values[13], 32)
	if err != nil {
		return nil, err
	}
	swellWave4Direction, err := strconv.ParseFloat(values[14], 32)
	if err != nil {
		return nil, err
	}
	swellWave5Height, err := strconv.ParseFloat(values[15], 32)
	if err != nil {
		return nil, err
	}
	swellWave5Period, err := strconv.ParseFloat(values[16], 32)
	if err != nil {
		return nil, err
	}
	swellWave5Direction, err := strconv.ParseFloat(values[17], 32)
	if err != nil {
		return nil, err
	}
	windWaveHeight, err := strconv.ParseFloat(values[18], 32)
	if err != nil {
		return nil, err
	}
	windWavePeriod, err := strconv.ParseFloat(values[19], 32)
	if err != nil {
		return nil, err
	}
	windWaveDirection, err := strconv.ParseFloat(values[20], 32)
	if err != nil {
		return nil, err
	}
	return &schemas.SwellsRecord{
		Height:              float32(height),
		Period:              float32(period),
		Direction:           float32(direction),
		SwellWave1Height:    float32(swellWave1Height),
		SwellWave1Period:    float32(swellWave1Period),
		SwellWave1Direction: float32(swellWave1Direction),
		SwellWave2Height:    float32(swellWave2Height),
		SwellWave2Period:    float32(swellWave2Period),
		SwellWave2Direction: float32(swellWave2Direction),
		SwellWave3Height:    float32(swellWave3Height),
		SwellWave3Period:    float32(swellWave3Period),
		SwellWave3Direction: float32(swellWave3Direction),
		SwellWave4Height:    float32(swellWave4Height),
		SwellWave4Period:    float32(swellWave4Period),
		SwellWave4Direction: float32(swellWave4Direction),
		SwellWave5Height:    float32(swellWave5Height),
		SwellWave5Period:    float32(swellWave5Period),
		SwellWave5Direction: float32(swellWave5Direction),
		WindWaveHeight:      float32(windWaveHeight),
		WindWavePeriod:      float32(windWavePeriod),
		WindWaveDirection:   float32(windWaveDirection),
	}, nil
}

func csvToSwellsRecordKV(csv string) (*keyvalue.KeyValue, error) {
	kv := strings.Split(csv, ",")

	var keys [5]string
	copy(keys[:], kv[:4])
	keyBytes, err := keyvalue.ParseKeysToBytes(keys)
	if err != nil {
		return nil, err
	}

	var values [21]string
	copy(values[:], kv[4:])
	value, err := parseSwellsRecord(values)
	if err != nil {
		return nil, err
	}
	valueBytes, err := keyvalue.ParseValueToBytes(value)
	if err != nil {
		return nil, err
	}

	return &keyvalue.KeyValue{
		Key:   keyBytes,
		Value: valueBytes,
	}, nil
}

func main() {
	writer.WriteStream(csvToSwellsRecordKV)
}
