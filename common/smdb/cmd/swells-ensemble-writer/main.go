package main

import (
	"strconv"
	"strings"

	"github.com/Surfline/wavetrak-services/common/smdb/internal/keyvalue"
	"github.com/Surfline/wavetrak-services/common/smdb/internal/writer"
	"github.com/Surfline/wavetrak-services/common/smdb/pkg/schemas"
)

func parseSwellsEnsembleRecord(values [14]string) (*schemas.SwellsEnsembleRecord, error) {
	windDirection, err := strconv.ParseFloat(values[0], 32)
	if err != nil {
		return nil, err
	}
	windSpeed, err := strconv.ParseFloat(values[1], 32)
	if err != nil {
		return nil, err
	}
	windWaveDirection, err := strconv.ParseFloat(values[2], 32)
	if err != nil {
		return nil, err
	}
	windWaveHeight, err := strconv.ParseFloat(values[3], 32)
	if err != nil {
		return nil, err
	}
	windWavePeriod, err := strconv.ParseFloat(values[4], 32)
	if err != nil {
		return nil, err
	}
	windAndSwellHeight, err := strconv.ParseFloat(values[5], 32)
	if err != nil {
		return nil, err
	}
	primaryWaveDirection, err := strconv.ParseFloat(values[6], 32)
	if err != nil {
		return nil, err
	}
	primaryWavePeriod, err := strconv.ParseFloat(values[7], 32)
	if err != nil {
		return nil, err
	}
	swellWave1Direction, err := strconv.ParseFloat(values[8], 32)
	if err != nil {
		return nil, err
	}
	swellWave1Height, err := strconv.ParseFloat(values[9], 32)
	if err != nil {
		return nil, err
	}
	swellWave1Period, err := strconv.ParseFloat(values[10], 32)
	if err != nil {
		return nil, err
	}
	swellWave2Direction, err := strconv.ParseFloat(values[11], 32)
	if err != nil {
		return nil, err
	}
	swellWave2Height, err := strconv.ParseFloat(values[12], 32)
	if err != nil {
		return nil, err
	}
	swellWave2Period, err := strconv.ParseFloat(values[13], 32)
	if err != nil {
		return nil, err
	}
	return &schemas.SwellsEnsembleRecord{
		WindDirection:        float32(windDirection),
		WindSpeed:            float32(windSpeed),
		WindWaveDirection:    float32(windWaveDirection),
		WindWaveHeight:       float32(windWaveHeight),
		WindWavePeriod:       float32(windWavePeriod),
		WindAndSwellHeight:   float32(windAndSwellHeight),
		PrimaryWaveDirection: float32(primaryWaveDirection),
		PrimaryWavePeriod:    float32(primaryWavePeriod),
		SwellWave1Direction:  float32(swellWave1Direction),
		SwellWave1Height:     float32(swellWave1Height),
		SwellWave1Period:     float32(swellWave1Period),
		SwellWave2Direction:  float32(swellWave2Direction),
		SwellWave2Height:     float32(swellWave2Height),
		SwellWave2Period:     float32(swellWave2Period),
	}, nil
}

func csvToSwellsRecordKV(csv string) (*keyvalue.KeyValue, error) {
	kv := strings.Split(csv, ",")

	var keys [5]string
	copy(keys[:], kv[:5])
	keyBytes, err := keyvalue.ParseKeysToBytes(keys)
	if err != nil {
		return nil, err
	}

	var values [14]string
	copy(values[:], kv[5:])
	value, err := parseSwellsEnsembleRecord(values)
	if err != nil {
		return nil, err
	}
	valueBytes, err := keyvalue.ParseValueToBytes(value)
	if err != nil {
		return nil, err
	}

	return &keyvalue.KeyValue{
		Key:   keyBytes,
		Value: valueBytes,
	}, nil
}

func main() {
	writer.WriteStream(csvToSwellsRecordKV)
}
