# Video Helpers

A library of common functions used to process, parse, and download rewind video files.

## Setup

See https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/73334794/PyPI+Package+Server for instructions on getting setup to download/upload packages to Artifactory.

Sample `$HOME/.pypirc` file:

```
[distutils]
index-servers =
  dev
  prod

[dev]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local-dev
username:
password:

[prod]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local
username:
password:
```

## Use

Install with `pip`, but before doing so, follow these steps:

1. [Install instructions for packages necessary for video-helpers to work correctly]

2. Then run following command: [based on setting up pip to work with artifactory]

3. Lastly, install with `pip`.

```sh
$ pip install wavetrak-video-helpers
```

## Development

### Setup Environment

Use [conda](https://docs.conda.io/en/latest/) to setup a development environment.

```sh
$ conda env create -f environment.yml
$ conda activate wavetrak-video-helpers
```

### Install Local Package

To test changes in a local application install as an "editable" package.

```sh
$ pip install -e /path/to/video-helpers/
```

### Deploy

Deploy to `dev` repository to test for local development.

```sh
$ ENV=dev make deploy
```

## How To Use

### Introduction
In it's simplest form the package can be used to extract frames 1 by 1 from
various cam alias's like so:

```python
# Initiate LocalRewindExtractor w/ path to rewind storage
storage = video_helpers.LocalRewindExtractor('/example/rewind-storage/rewinds/')

# To extract frames from all rewind videos for given aliases
for cam in storage.get_cameras(cams):
  print(f'Get batches for cam: {cam.alias}')

  for batch in cam.get_frame_batches(batch_size=20, interval=30, fps=2, start_time=start):

    print(batch)

    # Get single (timestamp, frame) tuple.
    for timestamp, frame in batch.get_frames():
      print(timestamp)
      print(frame)

   # Or get the full batch of frames & timestamps
   batch_data = batch.get_batch()
   model.predict(batch_data['frames'])  # for example to run inference directly
   timestamps = batch_data['timestamps']
```

### Important Functions

#### Check storage for cam alias's that have rewind data

```python
storage.get_supported_cam_list()
```

| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| None      |      |

| Returns       | Type                   | Description
| ------------- | ---------------------- | ----------------------------
|               | List[str]              | cam aliases

#### Fetch LocalRewindCamera objects

```python
cams = storage.get_cameras(aliases=['au-kirra', 'hi-pipeline', 'wc-lowers'])
kirra_cam = cams[0]
pipe_cam = cams[1]
lowers_cam = cams[2]
```

| Parameter     | Type        | Description
| ------------- |------------ | ------------------------------------------
| aliases       | List[str]   | list of cam aliases

| Returns       | Type                    | Description
| ------------- | ----------------------- | ----------------------------
|               | List[LocalRewindCamera] | list of LocalRewindCamera objects

#### Extract Batches From a LocalRewindCamera object

```python
kirra_batches = kirra_cam.get_frame_batches(batch_size=20, interval=30, fps=2, start_time=datetime(1, 1, 1), end_time=datetime(9, 9, 9))

kirra_batch_1 = next(kirra_batches)
```

| Parameter               | Type              | Description
| ----------------------- |------------------ | ---------------------------
| batch_size              | int               | max frames to return in each batch
| interval                | int               | seconds between batches
| fps (*Optional*)        | float             | frame rate of frame extaction, if omitted uses native fps
| start_time (*Optional*) | datetime.datetime | datetime object for time interval when to start extraction
| end_time (*Optional*)   | datetime.datetime | datetime object for time interval when to end extraction

Function returns a Generator that can then be used to fetch each batch
individually.

| Returns     | Type             | Description
| ----------- | ---------------- | -------------------------
| indiv batch | LocalRewindBatch | individual LocalRewindBatch object

#### Fetch Individual Frames & Timestamps from LocalRewindBatch

```python
kirra_batch_frames = kirra_batch_1.get_frames()

timestamp, frame = next(kirra_batch_frames)
```

Function returns a Generator that can then be used to fetch each timestamp & frame individually.

| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| None      |      |

| Returns   | Type              | Description
| --------- | ----------------- | ----------------------------
| timestamp | datetime.datetime | datetime indicating UTC time frame happened
| frame     | numpy.ndarray     | RGB numpy array of frame data

#### Dump all frames from a batch to local disk

```python
kirra_batch_1.dump(file_path="./frame_dump.mp4")
kirra_batch_1.dump(file_path="./frame_dump_jpgs/")
```
Pass a file name and location to save as MP4, or pass a directory location if using option JPG.


| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| file_path | str  | string representing the location to save frames

| Returns       | Type | Description
| ------------- | ---- | ----------------------------
|               | bool | signals if write was successful or not

#### Fetch dict of lists containing timestamps & frames from a full batch

```python
batch_dict = kirra_batch_1.get_batch()

timestamps = batch_dict['timestamps']
frames = batch_dict['frames']
```
Dictionary structure: `{'frames': List[frames],  'timestamps': List[timestamps]}`


| Parameter | Type | Description
| --------- | ---- | -----------------------------------------------
| None      |      |

| Returns       | Type       | Description
| ------------- | ---------- | ----------------------------
|               | Dict[list] | dict of lists containing 'frames' and 'timestamps' keys with full batch of data

#### Extract frames from a single rewind

```python
batches = storage.extract_frames_from_rewind(file="/location_to_single_rewind_file.mp4", batch_size=10, interval=20, fps=2)
```

| Parameter     | Type  | Description
| ------------- |------ |-------------------------------------------
| file          | str   | path for video file to extract frames from
| batch_size    | int   | max frames to return in each batch
| interval      | int   | seconds between batches
| fps (*Optional*) |      | float | frame rate of frame extraction, *Optional*: if omitted uses native fps

| Returns       | Type                   | Description
| ------------- | ---------------------- | ----------------------------
|               | List[LocalRewindBatch] | list containing LocalRewindBatch objects

### Tutorial

Examples of different things you can do with the package

```python
# First initiate the LocalRewindExtractor by passing in the path to where rewinds are stored locally
storage = video_helpers.LocalRewindExtractor('/example/rewind-storage/rewinds/')

# To get batches for a single video
test_batch = storage.extract_frames_from_rewind("/location_to_single_rewind_file", batch_size, interval, fps)

# To see what cameras you have rewinds for
print(storage.get_supported_cam_list())

# To fetch cameras, pass in desired aliases
cams = storage.get_cameras(['au-kirra', 'wc-lowers', 'hi-pipeline'])

# Then you can grab one cam at a time like so
kirra_cam = cams[0]
lowers_cam = cams[1]
pipe_cam = cams[2]

# Get alias for camera
print(kirra_cam.alias)

# Extract batches from a camera, this returns a Generator
kirra_batches = kirra_cam.get_frame_batches(batch_size=20, interval=30, fps=2, start_time=datetime(1, 1, 1), end_time=datetime(9, 9, 9))

# To Grab next batch from Generator
kirra_batch_1 = next(kirra_batches)

# Fetch a Generator that returns a frame & timestamp one at a time with next()
kirra_frame_gen = kirra_batch_1.get_frames()
timestamp, frame = next(kirra_frame_gen)

# Dump a batch of frames to local disk in .mp4 or .jpg
kirra_batch_1.dump("/example/test/dump/kirra.mp4")
kirra_batch_1.dump("/example/test/dump/")

# Fetch a list of dicts containing a single frame & timestamp from batch
kirra_batch_dict = kirra_batch_1.get_batch()
timestamps = kirra_batch_dict['timestamps']
frames = kirra_batch_dict['frames']

# To cycle through all frames in every batch for every camera alias
start = datetime(2020, 2, 25, 17, 57, 0)
end = datetime(2020, 2, 25, 18, 0, 0)

for cam in storage.get_cameras(cams):
 print(f'Get batches for cam: {cam.alias}')

 for index, batch in enumerate(cam.get_frame_batches(batch_size=20, interval=30, fps=2, start_time=start, end_time=end)):
   print(f'Process batch: {index}')

   for timestamp, frame in batch.get_frames():
     if frame is not None:
       print(f'Has frame for: {timestamp}')
     else:
       print(f'No frame for: {timestamp}')
```
