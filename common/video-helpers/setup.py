import setuptools  # type: ignore

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='wavetrak-video-helpers',
    version='0.0.0',
    author='Wavetrak',
    author_email='nerdery@surfline.com',
    description=(
        'A library of common functions used to process, parse, and download'
        'various video files.'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/Surfline/wavetrak-services/common/video-helpers',
    packages=setuptools.find_packages(),
    package_data={'': ['py.typed']},
    install_requires=[''],
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
)
