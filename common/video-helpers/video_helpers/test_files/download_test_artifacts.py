import os

import boto3  # type: ignore

s3 = boto3.client('s3')

operation_parameters = {
    'Bucket': 'wt-science-rd-tooling-files',
    'Prefix': 'test_files/frames/',
}

resp = s3.list_objects_v2(**operation_parameters)

print(__file__)

# Make local directories if they don't exist.
cwd = os.path.dirname(os.path.realpath(__file__))
frames_dir = os.path.join(cwd, 'frames')
if not os.path.isdir(frames_dir):
    os.mkdir(frames_dir)

for obj in resp['Contents']:
    key = obj['Key']
    file_name = key.split('/')[-1]
    target_file = os.path.join(frames_dir, file_name)
    s3.download_file(
        'wt-science-rd-tooling-files', f"{obj['Key']}", target_file
    )

operation_parameters = {
    'Bucket': 'wt-science-rd-tooling-files',
    'Prefix': 'test_files/rewinds/hi-pipeline/2020/02/13/',
}
resp = s3.list_objects_v2(**operation_parameters)

# Make 'rewinds' parent dirs
rewinds_dir = os.path.join(cwd, 'rewinds/disk/hi-pipeline/2020/02/13/')
if not os.path.isdir(rewinds_dir):
    os.makedirs(rewinds_dir)

for obj in resp['Contents']:
    key = obj['Key']
    file_name = key.split('/')[-1]
    target_file = os.path.join(rewinds_dir, file_name)
    s3.download_file(
        'wt-science-rd-tooling-files', f"{obj['Key']}", target_file
    )
