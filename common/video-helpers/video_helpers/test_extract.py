import os
from datetime import datetime

import numpy as np  # type: ignore
import pytest  # type: ignore

from video_helpers.extract.extractor import LocalRewindExtractor

TEST_DIR = './video_helpers/test_files/frames/'


@pytest.fixture
def storage():
    return LocalRewindExtractor('./video_helpers/test_files/rewinds')


@pytest.fixture
def camera(storage):
    return storage.get_cameras(['hi-pipeline'])[0]


# Test we get hi-pipeline back in supported alias list
def test_supported_cam_alias_list(storage):
    assert storage.get_supported_cam_list() == ['hi-pipeline']


def test_target_size(camera):
    """ test target_size keyword in get_frame_batches method """
    batch_size = 5
    interval = 10
    fps = 0.4
    start_time = datetime(2020, 2, 13, 0, 26, 1, 488000)
    end_time = datetime(2020, 2, 13, 0, 36, 1, 738000)
    target_size = (360, 640)

    batches = camera.get_frame_batches(
        batch_size, interval, fps, start_time, end_time, target_size
    )
    batch = next(batches)
    batch_frames = batch.get_batch()['frames']
    assert np.array(batch_frames).shape == (batch_size, *target_size, 3)

    # Try again with incorrect target_size
    incorrect_target_size = 5
    with pytest.raises(TypeError):
        batches = camera.get_frame_batches(
            batch_size,
            interval,
            fps,
            start_time,
            end_time,
            incorrect_target_size,
        )


def test_single_video_extraction(camera):
    batch_size = 5
    interval = 10
    fps = 0.4
    start_time = datetime(2020, 2, 13, 0, 26, 1, 488000)
    end_time = datetime(2020, 2, 13, 0, 36, 1, 738000)

    batches = camera.get_frame_batches(
        batch_size, interval, fps, start_time, end_time
    )
    batch = next(batches)

    # Test batch 1's first and last frame
    saved_frame_1 = np.load(os.path.join(TEST_DIR, 'frame_1_at_0s.npy'))
    saved_frame_5 = np.load(os.path.join(TEST_DIR, 'frame_5_at_10s.npy'))

    batch_frame_gen = batch.get_frames()
    _, frame = next(batch_frame_gen)
    assert frame.tolist() == saved_frame_1.tolist()

    for i in range(4):
        _, frame = next(batch_frame_gen)

    assert frame.tolist() == saved_frame_5.tolist()


def test_multiple_video_extraction(camera):
    batch_size = 5
    interval = 60
    fps = 0.05
    start_time = datetime(2020, 2, 13, 0, 26, 1, 488000)
    end_time = datetime(2020, 2, 13, 0, 46, 1, 979000)

    # Test batch 5, 1st frame should be from vid1 ane frame 5 from second vid
    batches = camera.get_frame_batches(
        batch_size, interval, fps, start_time, end_time
    )

    for i in range(5):
        batch = next(batches)

    saved_frame_1 = np.load(
        os.path.join(TEST_DIR, 'first_vid_frame_9min_20secs_in.npy')
    )
    saved_frame_5 = np.load(
        os.path.join(TEST_DIR, 'second_vid_frame_39s_in.npy')
    )

    batch_frame_gen = batch.get_frames()
    _, frame = next(batch_frame_gen)
    assert frame.tolist() == saved_frame_1.tolist()

    for i in range(4):
        _, frame = next(batch_frame_gen)
    assert frame.tolist() == saved_frame_5.tolist()


def test_extraction_after_video_time_frames(camera):
    batch_size = 5
    interval = 360
    fps = 0.05
    start_time = datetime(2020, 2, 13, 0, 45, 0, 0)
    end_time = datetime(2020, 2, 13, 0, 56, 1, 979000)

    # 4th frame should be from vid and 5th frame should be none
    batches = camera.get_frame_batches(
        batch_size, interval, fps, start_time, end_time
    )
    batch = next(batches)

    saved_frame = np.load(
        os.path.join(TEST_DIR, 'single_frame_empty_end_test.npy')
    )

    batch_frame_gen = batch.get_frames()

    for i in range(4):
        _, frame = next(batch_frame_gen)
    assert frame.tolist() == saved_frame.tolist()

    _, frame = next(batch_frame_gen)
    assert not frame


def test_extraction_before_video_time_frames(camera):
    batch_size = 5
    interval = 60
    fps = 0.05
    start_time = datetime(2020, 2, 13, 0, 25, 0, 0)
    end_time = datetime(2020, 2, 13, 0, 27, 0, 0)

    # Test empty first 4 frames and then frame from vid on the 5th
    batches = camera.get_frame_batches(
        batch_size, interval, fps, start_time, end_time
    )
    batch = next(batches)

    saved_frame = np.load(
        os.path.join(TEST_DIR, 'single_frame_empty_begin_test.npy')
    )

    batch_frame_gen = batch.get_frames()

    for i in range(4):
        _, frame = next(batch_frame_gen)
        assert not frame

    _, frame = next(batch_frame_gen)
    assert frame.tolist() == saved_frame.tolist()


def test_no_video_for_extraction(storage):
    assert not storage.get_cameras(['camera-does-not-exist'])
