import os
from collections import defaultdict
from datetime import datetime
from typing import DefaultDict, Generator, List, Tuple

import cv2  # type: ignore
import numpy  # type: ignore


class LocalRewindBatch:
    """
    The LocalRewindBatch class contains frames and timestamps extracted from a
    rewind vid.

    Has functions to allow users to work with the frames extracted from
    local rewind videos.

    Attributes:
        file: location of video the frames are extracted from
        frames: list that contains each frame in batch
        timestamps: list of timestamps for location corresponding to when frame
                    happened in UTC
        fps: rate the for the frames/timestamps
    """

    def __init__(
        self,
        frames: List[numpy.ndarray],
        timestamps: List[datetime],
        fps: float,
    ) -> None:

        self.frames = frames
        self.timestamps = timestamps
        self.fps = fps

        if len(frames) != len(timestamps):
            raise ValueError(
                'Frames & timestamps lists are of different sizes.'
            )

    def __str__(self) -> str:
        return (
            f'LocalRewindBatch extracted with '
            f'{len(self.frames)} frames, {self.fps}fps, and timestamps '
            f'{self.timestamps[0]} to {self.timestamps[-1]}'
        )

    def dump(self, file_path: str) -> bool:
        """
        Dumps frames to local disk.

        Doesn't dump empty frames.

        Options include: MP4, JPG
        Pass a file name and location to save as MP4 (/example.mp4)
        Pass a directory location if using option JPG(/example/)

        Args:
            file_path: A string representing the location to save frames

        Returns:
            bool to signal if frames were successfully saved or not

        Raises:
            NotImplementedError: extension to dump frames as hasn't been
                                 implemented yet
        """
        if os.path.isdir(file_path):
            flag = self._save_frames_to_jpg(file_path)
        elif file_path.endswith('.mp4'):
            flag = self._save_frames_to_mp4(file_path)
        else:
            raise NotImplementedError(
                f'Saving frames in {os.path.splitext(file_path)[1]} '
                'file extension is not supported.'
            )

        return flag

    def get_batch(self) -> DefaultDict[str, list]:
        """
        Retrieves a batch of frames in dictionary form.

        Returns frames and timestamps within a List

        Args:
            None
        Returns:
            A list of dicts containing a single frame and timestamp.
        """
        batch: DefaultDict[str, list] = defaultdict(list)
        for frame, timestamp in zip(self.frames, self.timestamps):
            if frame is not None:
                batch['frames'].append(frame)
                batch['timestamps'].append(timestamp)
        return batch

    def get_frames(
        self,
    ) -> Generator[Tuple[datetime, numpy.ndarray], None, None]:
        """
        Returns a Generator to grab frame/timestamp 1 at a time.

        Attributes:
            None
        Returns:
            Tuple containing (timestamp, frame)
            timestamp: datetime indicating UTC time in video frame was pulled
            frame: RGB numpy array of frame data
        Raises:
            StopIteration: exception will be thrown if next() is called when
                           there are no more frames to yield
        """
        for frame, timestamp in zip(self.frames, self.timestamps):
            yield timestamp, frame

    def _save_frames_to_mp4(self, file_path: str) -> bool:
        """
        Private function that handles the local mp4 write operation.
        """
        fourcc = cv2.VideoWriter_fourcc(*'H264')

        non_empty_frames = [
            frame for frame in self.frames if frame is not None
        ]
        if not non_empty_frames:
            return False

        height, width, _ = non_empty_frames[0].shape

        with VideoWriterWrapper(
            file_path, fourcc, self.fps, (width, height)
        ) as out:
            for frame in non_empty_frames:
                out.vid_writer.write(frame)

        return True

    def _save_frames_to_jpg(self, dir_path: str) -> bool:
        """
        Private function that handles the local jpg write operation.
        """
        non_empty_frames_with_numbers = [
            (num, frame)
            for num, frame in enumerate(self.frames, start=1)
            if frame is not None
        ]
        if not non_empty_frames_with_numbers:
            return False

        for num, frame in non_empty_frames_with_numbers:
            file_path = os.path.join(dir_path, f'frame_{num}.jpg')
            cv2.imwrite(file_path, frame)

        return True


class VideoWriterWrapper(object):
    """
    Wrapper to allow us to use a context manager when writing frames to a
    a single file.
    """

    def __init__(self, *args, **kwargs):
        self.vid_writer = cv2.VideoWriter(*args, **kwargs)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.vid_writer.release()
