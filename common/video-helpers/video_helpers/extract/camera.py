import glob
import math
import os
from datetime import datetime, timedelta
from typing import Generator, List, Optional, Tuple

import cv2  # type: ignore
import numpy as np  # type: ignore

from video_helpers.extract.batch import LocalRewindBatch


# TODO: Should probably improve logging messages so users know what's going on
#       during frame extraction process.
class LocalRewindCamera:
    """
    The LocalRewindCamera class is used to represent the location of local
    rewind video files.

    Contains functions to extract frames from videos, and build
    LocalRewindBatch objects.

    Attributes:
        alias: string representing name for a camera
        files: list containing locations of each rewind video file
    """

    def __init__(self, alias: str, dir_path: str) -> None:
        self.alias = alias
        self.rewind_location = dir_path

    def __str__(self) -> str:
        return f'LocalRewindCamera with alias: {self.alias}'

    def get_frame_batches(
        self,
        batch_size: int,
        interval: float,
        fps: Optional[float] = None,
        start_time: Optional[datetime] = None,
        end_time: Optional[datetime] = None,
        target_size: Tuple[int, int] = (720, 1280),
    ) -> Generator[LocalRewindBatch, None, None]:
        """
        Used to retrieve a list of LocalRewindBatch objects.

        Args:
            batch_size: max frames to return in this batch
            interval: seconds between batches
            fps: frame rate of frame extraction, Optional if omitted uses
                 native fps
            start_time: datetime object for when to start extraction
            end_time: datetime object for when to end extraction
            target_size: target frames size as (h, w)
        Returns:
            list of LocalRewindBatch objects
        Raises:
        StopIteration: exception will be thrown if next() is called when
                       there are no more batches to yield
        """
        self.target_size = target_size
        if (not isinstance(self.target_size, (list, tuple, np.ndarray))) or (
            len(target_size) != 2
        ):
            raise TypeError(
                'Input target size should be iterable '
                'of length 2, as (height, width)'
            )

        start = start_time or self._calculate_min_possible_time()
        end = end_time or self._calculate_max_possible_time()

        rewind_files = self._fetch_rewind_files(start, end)

        fps = self._get_native_fps(rewind_files[0]) if fps is None else fps

        # If user inputs zero interval - no time between batches -
        # increase to 1/fps so frames aren't repeated between batches
        interval = max(interval, 1.0 / fps)

        # build generator for timestamp locations where frame will be pulled
        time_loc_gen = self._build_timestamp_gen(
            batch_size, interval, fps, start, end
        )

        batches = self._get_batches(
            batch_size, time_loc_gen, fps, rewind_files
        )

        return batches

    def get_batches_from_video(
        self, file: str, batch_size: int, interval: int, fps: Optional[float]
    ) -> List[LocalRewindBatch]:
        """
        Extracts frames from a single video file.

        Args:
            file: path for video file to extract frames from
            batch_size: max frames to return in this batch
            interval: seconds between batches
            fps: frame rate of frame extraction, Optional if omitted uses
                 native fps
        Returns:
            A Generator that returns indiv LocalRewindBatch objects
        """
        batches_from_video = []

        current_timestamp = 0
        video_utc_time = self._get_start_time_from_rewind(file)

        vid_length = self._get_video_time_length(file)

        if vid_length == 0:
            raise ValueError(f'Video length for file: {file} is 0.')

        fps = self._get_native_fps(file) if fps is None else fps

        intervals = [
            i * interval for i in range(math.ceil(vid_length / interval))
        ]
        batches_from_video = [
            self._build_frame_batch(
                file,
                current_timestamp + i,
                batch_size,
                fps,
                video_utc_time + timedelta(seconds=i),
            )
            for i in intervals
        ]

        return batches_from_video

    def _get_batches(
        self,
        batch_size: int,
        time_loc_gen: Generator[datetime, None, None],
        fps: float,
        rewind_files: List[str],
    ) -> Generator[LocalRewindBatch, None, None]:
        """
        Private function that does all the work for building batches.
        """
        file_pos = 0
        frame_list: List[np.ndarray] = []
        timestamp_list: List[datetime] = []

        if rewind_files:
            current_file = rewind_files[file_pos]
        else:
            current_file = ''

        for timestamp in time_loc_gen:
            if not current_file:
                frame_list.append(None)
                timestamp_list.append(timestamp)
                continue

            if timestamp >= self._get_end_time_from_rewind(current_file):
                file_pos = self._cycle_video_file(
                    timestamp, file_pos, rewind_files
                )
                try:
                    current_file = rewind_files[file_pos]
                except IndexError:
                    current_file = ''

            time_in_vid = self._convert_timestamp_to_vid_time_secs(
                current_file, timestamp
            )

            if time_in_vid is None:
                frame = None
            else:
                frame = self._grab_frame(current_file, time_in_vid)
                if frame is not None:
                    frame = self._bgr2rgb(frame)

                    if frame.shape[:2] != self.target_size:
                        frame = cv2.resize(
                            frame,
                            self.target_size[::-1],
                            interpolation=cv2.INTER_LINEAR,
                        )

            frame_list.append(frame)
            timestamp_list.append(timestamp)

            # Check if batch is filled and ready to be yielded
            if len(frame_list) == batch_size:
                yield LocalRewindBatch(frame_list, timestamp_list, fps)
                frame_list = []
                timestamp_list = []

        if frame_list and timestamp_list:
            last_batch = LocalRewindBatch(frame_list, timestamp_list, fps)
            yield last_batch

    @staticmethod
    def _grab_frame(
        file: str, time_location_secs: float
    ) -> Optional[np.ndarray]:
        """
        Private function that does the actual frame extraction.
        """
        cap = cv2.VideoCapture(file)
        cap.set(cv2.CAP_PROP_POS_MSEC, (time_location_secs * 1000.0))

        flag, frame = cap.read()

        # return empty frame if there was no frame at time location
        if flag is False:
            return None

        return frame

    def _build_frame_batch(
        self,
        file: str,
        start_time: int,
        batch_size: int,
        fps: float,
        utc_start_time: datetime,
    ) -> LocalRewindBatch:
        """
        Private function that builds a single batch of frames.
        """
        frames: List[np.ndarray] = []
        timestamps: List[np.ndarray] = []

        # get time increment to move frame pointer by every frame pull
        time_inc = float(1 / fps)

        cap = cv2.VideoCapture(file)
        cap.set(cv2.CAP_PROP_POS_MSEC, (start_time * 1000.0))

        for i in range(batch_size):
            flag, frame = cap.read()

            if not flag:
                break

            vid_timestamp = cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0

            if frame is None:
                frames.append(frame)
            else:
                frames.append(self._bgr2rgb(frame))

            timestamps.append(
                utc_start_time + timedelta(seconds=vid_timestamp)
            )

            cap.set(cv2.CAP_PROP_POS_MSEC, (vid_timestamp + time_inc) * 1000.0)

        cap.release()
        batch = LocalRewindBatch(frames, timestamps, fps)

        return batch

    def _fetch_rewind_files(
        self, start_time: datetime, end_time: datetime
    ) -> List[str]:
        """
        Private function to fetch the paths for all of the cam's rewinds.
        """
        date_list = [
            start_time.date() + timedelta(days=dt)
            for dt in range((end_time.date() - start_time.date()).days + 1)
        ]

        file_list = [
            file
            for date in date_list
            for file in glob.glob(
                os.path.join(
                    self.rewind_location,
                    '*',
                    self.alias,
                    f"{date.strftime('%Y')}",
                    f"{date.strftime('%m')}",
                    f"{date.strftime('%d')}",
                    '*',
                )
            )
            if file.endswith('.mp4')
        ]

        file_list.sort()

        return file_list

    def _calculate_min_possible_time(self) -> datetime:
        """
        Private function to time for earliest archived rewind.
        """
        # Finds the earliest archived rewind and parses the timestamp
        cam_path = self.rewind_location
        date_list = glob.glob(os.path.join(cam_path, '*', '*', '*'))
        date_list.sort()

        return datetime.strptime(
            glob.glob(os.path.join(date_list[0], '*'))[0].split('.')[-2],
            '%Y%m%dT%H%M%S%f',
        )

    def _calculate_max_possible_time(self) -> datetime:
        """
        Private function to calculate time for last archived rewind.
        """
        # Finds the last archived rewind and parses the timestamp
        cam_path = self.rewind_location
        date_list = glob.glob(os.path.join(cam_path, '*', '*', '*'))
        date_list.sort(reverse=True)

        return datetime.strptime(
            glob.glob(os.path.join(date_list[0], '*'))[0].split('.')[-2],
            '%Y%m%dT%H%M%S%f',
        )

    def _cycle_video_file(
        self, timestamp: datetime, file_pos: int, rewind_files: List[str]
    ) -> int:
        """
        Private function that cycles through video files until finds the one
        we are intersted in. Then returns the index of that file.
        """
        while file_pos < len(rewind_files):
            if timestamp >= self._get_end_time_from_rewind(
                rewind_files[file_pos]
            ):
                file_pos += 1
            else:
                return file_pos

        return file_pos

    def _get_end_time_from_rewind(self, file: str) -> datetime:
        """
        Private function that gets end time in UTC for a video file.
        """
        start_time = self._get_start_time_from_rewind(file)

        return start_time + timedelta(
            seconds=self._get_video_time_length(file)
        )

    def _convert_timestamp_to_vid_time_secs(
        self, file: Optional[str], timestamp: datetime
    ) -> Optional[float]:
        """
        Private function that converts a datetime time into seconds within
        video.
        """
        if not file:
            return None

        start_time = self._get_start_time_from_rewind(file)
        end_time = self._get_end_time_from_rewind(file)

        if start_time <= timestamp < end_time:
            return (timestamp - start_time).total_seconds()

        return None

    @staticmethod
    def _build_timestamp_gen(
        batch_size: int,
        interval: float,
        fps: float,
        start_time: datetime,
        end_time: datetime,
    ) -> Generator[datetime, None, None]:
        """
        Private function that builds timestamp list for expected location of
        frame extraction.
        """
        current_time = start_time
        time_inc = float(1 / fps)
        batch_count = 0

        while current_time <= end_time:
            yield current_time
            batch_count += 1

            if batch_count == batch_size:
                current_time = current_time + timedelta(seconds=interval)
                batch_count = 0
                continue

            current_time = current_time + timedelta(seconds=time_inc)

    @staticmethod
    def _get_video_time_length(input_video: str) -> float:
        """
        Private function that returns the length of a video in seconds.
        """
        cap = cv2.VideoCapture(input_video)
        fps = cap.get(cv2.CAP_PROP_FPS)
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        duration = float(frame_count) / float(fps)
        cap.release()

        return duration

    @staticmethod
    def _get_native_fps(input_video: str) -> float:
        """
        Private function that returns the native fps for a video.
        """
        return cv2.VideoCapture(input_video).get(cv2.CAP_PROP_FPS)

    @staticmethod
    def _bgr2rgb(frame: np.ndarray) -> np.ndarray:
        """
        Private function that rolls a frame's color axis from BGR to RGB.
        """
        return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    @staticmethod
    def _rgb2bgr(frame: np.ndarray) -> np.ndarray:
        """
        Private function that rolls a frame's color axis from RGB to BGR.
        """
        return cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

    @staticmethod
    def _get_start_time_from_rewind(file: str) -> datetime:
        """
        Private function that gets start time in UTC for a video file
        """
        rewind_info = file.split('/')[-1].split('.')
        rewind_start_time = datetime.strptime(
            rewind_info[2], '%Y%m%dT%H%M%S%f'
        )

        return rewind_start_time
