import logging
import os
from typing import List

from video_helpers.extract.batch import LocalRewindBatch
from video_helpers.extract.camera import LocalRewindCamera

logger = logging.getLogger(__name__)


class LocalRewindExtractor:
    """
    The LocalRewindExtractor class is used to represent the storage location
    of local rewinds, and is used as the center point for all frame extraction.

    Contains a function to retrieve cameras with rewinds locally stored.

    Attributes:
        path_to_rewind_archive: path to rewind archive where rewinds are stored
                                by camera alias.
    """

    def __init__(self, path_to_rewind_archive: str) -> None:
        if not os.path.isdir(path_to_rewind_archive):
            raise ValueError(
                f'Given directory: {path_to_rewind_archive} does not exist.'
            )

        self.rewind_path = path_to_rewind_archive

        # build list of cam objects
        cam_aliases = [
            cam
            for item in os.listdir(self.rewind_path)
            if os.path.isdir(os.path.join(self.rewind_path, item))
            for cam in os.listdir(os.path.join(self.rewind_path, item))
            if os.path.isdir(os.path.join(self.rewind_path, item, cam))
        ]
        self.cam_list = self._build_local_rewind_cams(cam_aliases)

        if len(self.cam_list) == 0:
            raise ValueError(
                f'Given dir: {path_to_rewind_archive} contains no camera data.'
            )

    def __str__(self) -> str:
        return (
            f"LocalRewindExtractor with rewinds located in: {self.rewind_path}"
        )

    def extract_frames_from_rewind(
        self, file: str, batch_size: int, interval: int, fps: float
    ) -> List[LocalRewindBatch]:
        """
        Extracts frames from a single video file.

        Args:
            file: path for video file to extract frames from
            batch_size: max frames to return in this batch
            interval: seconds between batches
            fps: frame rate of frame extraction, Optional if omitted uses
                 native fps
        Returns:
            A List containing LocalRewindBatches
        """
        indiv_cam = LocalRewindCamera('indiv-rewind', file)
        batches = indiv_cam.get_batches_from_video(
            file, batch_size, interval, fps
        )

        return batches

    def get_supported_cam_list(self) -> List[str]:
        """
        Returns a list of supported cams where rewinds are present.

        Args:
            None
        Returns:
            list of camera aliases we have data for
        """
        supported_cam_list = [cam.alias for cam in self.cam_list]

        return supported_cam_list

    def get_cameras(self, aliases: List[str]) -> List[LocalRewindCamera]:
        """
        Retrieves interested cameras that are stored in local rewind archive.

        Cameras then can be used to retrieve batches/frames from rewind videos.

        Args:
            aliases: list of strings representing the alias of a camera
        Returns:
            list of LocalRewindCamera objects
        """
        filtered_cam_list = [
            cam
            for alias in aliases
            for cam in self.cam_list
            if cam.alias == alias
        ]

        for alias in aliases:
            if any(alias == cam.alias for cam in filtered_cam_list):
                continue
            else:
                logger.warning(
                    f'Desired cam: {alias} is not present in Extractor. '
                    'Please confirm video files exists for cam. '
                )

        return filtered_cam_list

    def _build_local_rewind_cams(
        self, cam_alias_list: List[str]
    ) -> List[LocalRewindCamera]:
        """
        Private function to build the LocalRewindCamera objects
        """
        local_cam_list = [
            LocalRewindCamera(cam_alias, self.rewind_path)
            for cam_alias in cam_alias_list
        ]

        return local_cam_list
