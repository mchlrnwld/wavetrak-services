import argparse
import json
import yaml


def build_ecs_params_file(ci_file_path, ecs_params_file_path, environment):
    """
    Used by deploy-service-to-ecs.sh to create an ecs-params.yml file that
    embeds references to AWS Parameter Store values. These values will be
    created in the service container's runtime.

    The below arguments are passed by the deploy-service-to-ecs.sh script.

    Arguments:
        --ci_file_path: Path to the ci.json file.
        --ecs_params_file_path: Path to the service's ecs-params.yml file.
        --environment: Deployment environment (sandbox, staging or prod).

    Returns:
        Prints yaml-formatted file to stdout.
    """

    # Read values from ci.json file
    try:
        with open(args.ci_file_path, 'r') as ci_file:
            ci_data = json.load(ci_file)
    except IOError as error:
        exit(error)

    # Check ci.json object format
    if ('secrets' not in ci_data['secrets']['common']) or (
        'secrets' not in ci_data['secrets']['service']
    ):
        raise KeyError('ci.json node must contain \'secrets\' key.')

    # Note: Converting to string in numerous places
    # to prevent PyYAML serialization type hints
    service_name = str(ci_data['compose'])
    task_role_arn = str(ci_data['taskRoleArn'][args.environment])

    # Read values from ecs-params.yml file and use as a base object
    with open(args.ecs_params_file_path) as initial_ecs_params_file:
        ecs_params_data = yaml.safe_load(initial_ecs_params_file)

    # Add a service node if one doesn't exist
    if 'services' not in ecs_params_data['task_definition']:
        ecs_params_data['task_definition']['services'] = {}
        ecs_params_data['task_definition']['services'][service_name] = {}

    # Generate secrets object
    secrets = [
        {
            'name': str(key),
            'value_from': str(ci_data['secrets'][scope]['prefix']).format(
                env=environment
            )
            + str(key),
        }
        for scope in ['common', 'service']
        for key in ci_data['secrets'][scope]['secrets']
    ]

    # Build out the rest of the yaml from ecs_params_data base object
    ecs_params_data['task_definition']['services'][service_name][
        'secrets'
    ] = secrets

    # Add task_execution_role and task_role_arn_nodes
    ecs_params_data['task_definition']['task_execution_role'] = task_role_arn
    ecs_params_data['task_definition']['task_role_arn'] = task_role_arn

    try:
        with open('ecs-params.yml', 'w') as final_ecs_params_file:
            final_ecs_params_file.write(yaml.dump(ecs_params_data))
    except IOError as error:
        exit(error)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate ecs-params.yml file.'
    )
    parser.add_argument('--ci_file_path')
    parser.add_argument('--ecs_params_file_path')
    parser.add_argument('--environment')

    args = parser.parse_args()

    build_ecs_params_file(
        args.ci_file_path, args.ecs_params_file_path, args.environment
    )
