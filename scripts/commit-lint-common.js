const fs = require("fs");
const intersection = require("lodash/intersection");
const chalk = require("chalk");
const { exec } = require("child_process");
const load = require("@commitlint/load").default;
const lint = require("@commitlint/lint").default;
const lernaConfig = require("../lerna.json");

const log = console.log;
const error = chalk.red.bold;
const warning = chalk.yellow;
const success = chalk.green;
const message = chalk.blue;
const bold = chalk.bold;

log(message.bold("Linting commit message..."));

const PACKAGES = lernaConfig.packages;
const GIT_GET_CHANGED_FOLDERS_COMMAND =
  "git diff --name-only --cached | xargs -L1 dirname | uniq";

const getChangedFolderMatches = (commandResult, err) => {
  if (err) {
    log(error(`Error: ${err.message || err}`));
    log(warning("Skipping commit validation."));

    // If a fatal error occurs just skip validation
    return process.exit(0);
  }

  const changedFolders = commandResult.split("\n");
  const matchedChangedFolders = intersection(changedFolders, PACKAGES);
  return matchedChangedFolders;
};

const getAndLintCommitMessage = (tasks) => {
  const commitMessage = fs.readFileSync(process.env.HUSKY_GIT_PARAMS, {
    encoding: "utf-8",
  });
  const [{ rules, parserPreset }] = tasks;
  return lint(
    commitMessage,
    rules,
    parserPreset ? { parserOpts: parserPreset.parserOpts } : {}
  );
};

const verifyLintResult = (report) => {
  if (report.valid) {
    log(success("Commit successfully linted."));
    return process.exit(0);
  }
  log(error("Commit lint failed"));
  log(`Commit message: ${message(report.input)}`);
  report.errors.forEach((err) =>
    log(warning(bold(`${err.name}: `) + err.message))
  );
  return process.exit(1);
};

// Retrieve folder names with changed files
exec(GIT_GET_CHANGED_FOLDERS_COMMAND, (commandError, stdout, stderr) => {
  const changedFolderMatches = getChangedFolderMatches(
    stdout,
    commandError || stderr
  );

  if (changedFolderMatches.length > 0) {
    Promise.all([load({}, { file: "commitlint.config.js" })])
      .then(getAndLintCommitMessage)
      .then(verifyLintResult)
      .catch((err) => {
        log(error(err));
        process.exit(1);
      });
  } else {
    log(success("No common packages changed, skipping commit validation."));
    return process.exit(0);
  }
});
