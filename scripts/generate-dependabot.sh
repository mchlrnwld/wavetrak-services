#!/bin/bash

cat > .github/dependabot.yml <<EOF
version: 2
registries:
  npm-artifactory:
    type: npm-registry
    url: https://surfline.jfrog.io/surfline/api/npm/npm-local/
    token: \${{secrets.NODE_AUTH_TOKEN}}
updates:
  # github actions
  - package-ecosystem: github-actions
    directory: /
    open-pull-requests-limit: 10
    schedule:
      interval: monthly
EOF

for dir in $(
  find * \
    -name '*package.json*' \
    -type f -not -path '*node_modules/*' \
    | xargs -n1 dirname | sort
)
do
cat >> .github/dependabot.yml <<EOF
  # npm
  - package-ecosystem: npm
    commit-message:
      prefix: chore
      include: scope
    directory: /${dir}
    labels:
      - dependencies
    open-pull-requests-limit: 5
    registries: "*"
    schedule:
      interval: monthly
    versioning-strategy: increase
EOF
done

for dir in $(
  find * \
    -name '*environment.yml*' -o -name 'environment.devenv.yml' \
    -type f -not -path '*vendor/*' \
    | xargs -n1 dirname | sort | uniq
)
do
cat >> .github/dependabot.yml <<EOF
  # python
  - package-ecosystem: pip
    commit-message:
      prefix: chore
      include: scope
    directory: /${dir}
    labels:
      - dependencies
    open-pull-requests-limit: 5
    schedule:
      interval: monthly
EOF
done

for dir in $(
  find * \
    -name '*go.mod*' \
    -type f -not -path '*vendor/*' \
    | xargs -n1 dirname | sort
)
do
cat >> .github/dependabot.yml <<EOF
  # go
  - package-ecosystem: gomod
    commit-message:
      prefix: chore
      include: scope
    directory: /${dir}
    labels:
      - dependencies
    open-pull-requests-limit: 5
    schedule:
      interval: monthly
EOF
done

for dir in $(
  find * \
    -name '*composer.json*' \
    -type f -not -path '*vendor/*' \
    -type f -not -path '*node_modules/*' \
    | xargs -n1 dirname | sort
)
do
cat >> .github/dependabot.yml <<EOF
  # php
  - package-ecosystem: composer
    commit-message:
      prefix: chore
      include: scope
    directory: /${dir}
    labels:
      - dependencies
    open-pull-requests-limit: 5
    schedule:
      interval: monthly
EOF
done
