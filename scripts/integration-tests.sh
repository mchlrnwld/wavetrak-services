#!/usr/bin/env bash

# [working directory]/[nanoseconds from epoch] as project name for reasonably unique container names
COMPOSE_PROJECT_NAME="${PWD##*/}-$(date +%s%N)"
INTEGRATION=docker-compose.integration.yml
TEST=docker-compose.test.yml
INTEGRATION_TEST=docker-compose.integration-test.yml

# Clean up containers, volumes, and network
function cleanup {
	if test -f "$INTEGRATION" && test -f "$TEST"; then
		docker-compose \
			-p "${COMPOSE_PROJECT_NAME}" \
			-f docker-compose.test.yml \
			-f docker-compose.integration.yml \
			rm \
			-v \
			--stop \
			--force
	elif test -f "$INTEGRATION_TEST"; then
		docker-compose \
			-p "${COMPOSE_PROJECT_NAME}" \
			-f docker-compose.integration-test.yml \
			rm \
			-v \
			--stop \
			--force
	fi

  docker network ls --filter "name=${COMPOSE_PROJECT_NAME}" -q | xargs docker network rm
}

trap cleanup EXIT

set -xe

if test -f "$INTEGRATION" && test -f "$TEST"; then
	docker-compose \
		-p "${COMPOSE_PROJECT_NAME}" \
		-f docker-compose.test.yml \
		-f docker-compose.integration.yml \
		up \
		--build \
		--force-recreate \
		--renew-anon-volumes \
		--no-color \
		--exit-code-from integration-tests
elif test -f "$INTEGRATION_TEST"; then
	# Build images, start containers, and run tests
	docker-compose \
		-p "${COMPOSE_PROJECT_NAME}" \
		-f docker-compose.integration-test.yml \
		up \
		--build \
		--force-recreate \
		--renew-anon-volumes \
		--no-color \
		--exit-code-from integration-tests
fi
