#!/usr/bin/env bash

# Outputs commits between the currently deployed commit SHA and the desired SHA for a service.
# Currently deployed commit SHA is determined based on the currently deployed task definition.
# A ci.json file is required in the service directory to determine the ECS service to query for a task definition.
#
# $1 - Environment (sandbox, staging, prod).
# $2 - Desired commit SHA, branch, or tag.
# $3 - Path to service directory containing ci.json file. Defaults to current directory.
#
# Examples
#
#   ./deploy-git-log.sh sandbox 4fbb8f500d12cc60b47ec88b117f68287df3133d ../services/science-data-service
#   AWS_PROFILE=prod ./deploy-git-log.sh prod PS-757-add-grid-point-mutations .

ENV="${1}"
COMMIT_SHA="${2}"
SERVICE_PATH="${3-.}"

SERVICE_NAME="$(jq -r '.serviceName' "${SERVICE_PATH}/ci.json")-${ENV}"
TASK_DEFINITION=$(aws ecs describe-services --cluster="sl-core-svc-${ENV}" --services="${SERVICE_NAME}" | jq -r '.services[0].taskDefinition')
DEPLOYED_IMAGE=$(aws ecs describe-task-definition --task-definition="${TASK_DEFINITION}" | jq -r '.taskDefinition.containerDefinitions[0].image')
DEPLOYED_SHA=$(echo "${DEPLOYED_IMAGE}" | cut -d':' -f2)

set -x
git --no-pager log --oneline "${DEPLOYED_SHA}..${COMMIT_SHA}" "${SERVICE_PATH}"
