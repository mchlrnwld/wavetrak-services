import argparse
import json
import logging

import boto3
import botocore


def verify_ci_params(ci_file_path, environment):
    """
    Used by ECS service deployment script to verify the existence of the
    variables listed in the ci.json files in AWS Parameter Store.

    Note: deployment job must have necessary permissions to access
    and decrypt the SSM parameters in ci.json.

    Arguments:
        --ci_file_path: Path to the ci.json file.
        --environment: Deployment environment (sandbox, staging or prod).

    Returns:
        If all variables are found, return success message. Else return
        a message with a list of all the variables that errored.
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    # Read the values from ci.json file
    try:
        with open(args.ci_file_path, 'r') as ci_file:
            ci_data = json.load(ci_file)
    except IOError as error:
        exit('Problem reading ci.json file:\n{}'.format(error))

    # Generate a list of ci.json's variables
    ssm_parameters = [
        ci_data['secrets'][scope]['prefix'].format(env=environment) + key
        for scope in ['common', 'service']
        for key in ci_data['secrets'][scope]['secrets']
    ]

    # Attempt boto3 lookup on params and keep track of params that fail
    ssm_client = boto3.client('ssm', region_name='us-west-1')

    missing_params = []
    access_denied_params = []
    other_error_params = []

    for parameter in ssm_parameters:
        try:
            response = ssm_client.get_parameter(
                Name=parameter, WithDecryption=False
            )

        except botocore.exceptions.ClientError as error:
            if error.response['Error']['Code'] == 'ParameterNotFound':
                missing_params.append(parameter)

            elif error.response['Error']['Code'] == 'AccessDeniedException':
                access_denied_params.append(parameter)

            else:
                other_error_params.append(error)

    # Report on errors
    logger.info('*' * 20 + '\nVerification results:\n' + '*' * 20)
    if (
        not missing_params
        and not access_denied_params
        and not other_error_params
    ):
        logger.info('All parameter lookups passed!')

    else:
        if missing_params:
            for param in missing_params:
                logger.error(
                    '- Can\'t find {} in Parameter Store'.format(param)
                )

        if access_denied_params:
            for param in access_denied_params:
                logger.error(
                    '- User does not have sufficient permissions to '
                    'view parameter {}'.format(param)
                )

        if other_error_params:
            for param in other_error_params:
                logger.error('- {}'.format(error))

        logger.info('-' * 20)
        exit(
            'Unable to successfully look up all the parameters in '
            'ci.json file; see script output for more information.'
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Verify ci.json params exist.'
    )
    parser.add_argument('--ci_file_path')
    parser.add_argument('--environment')

    args = parser.parse_args()

    verify_ci_params(args.ci_file_path, args.environment)
