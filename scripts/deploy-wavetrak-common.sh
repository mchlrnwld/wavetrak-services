#!/bin/bash

set -xe

function info {
  echo "[INFO] ${*}"
}

function error {
  echo "[ERROR] ${*}" >&2
  exit 1
}

cd "common/${MODULE}"

ENV="${ENVIRONMENT}" make deploy

info "build description:common/${MODULE}|${ENVIRONMENT}|${VERSION}|"
