#!/bin/bash

set -euxo pipefail

function info {
  echo "[INFO] ${*}"
}

function error {
  echo "[ERROR] ${*}" >&2
  exit 1
}

# add to path for assume-role
export PATH="${PATH}:/usr/local/bin"

REGISTRY_HOST="833713747344.dkr.ecr.us-west-1.amazonaws.com"
CONTAINER_PATH="services/${SERVICE}"

info "build description:${CONTAINER_PATH}|${ENVIRONMENT}|${VERSION}|"

# trap script errors
trap catch_errors ERR;
function catch_errors() {
  echo "script aborted, because of errors";
  exit 1;
}

SERVICE_URL="$(jq -r ".serviceUrls.${ENVIRONMENT}" "${CONTAINER_PATH}/ci.json")"
SMOKETEST_COMMAND="$(jq -er '.tests.smoke // empty' "${CONTAINER_PATH}/ci.json" || echo '')"

function run_smoketest() {
  local service_url
  local smoketest_command
  local container_path
  service_url="$1"
  smoketest_command="$2"
  container_path="$3"

  if [[ -n "${smoketest_command}" ]]; then
    cd "${container_path}"
    info "Running smoketests."
    info "Once completed, Jenkins deploy job will continue."
    SERVICE_URL="${service_url}" "${smoketest_command}"
    SMOKETEST_RESULT="${?}"
    cd "${WORKSPACE}"
  fi
}

# determine AWS profile
if [[ "${ENVIRONMENT}" == "sandbox" ]]; then
  PROFILE="dev"
else
  PROFILE="${ENVIRONMENT}"
fi

# Assume proper role
eval "$(assume-role "${ENVIRONMENT}")"

# Verify ci.json params are in AWS Parameter Store
if [[ -f ${WORKSPACE}/services/${SERVICE}/ci.json ]]; then
  python scripts/verify_variables_in_parameter_store.py \
    --ci_file_path="${WORKSPACE}/services/${SERVICE}/ci.json" \
    --environment="${ENVIRONMENT}"
else
  error "No ci.json file found. Aborting script."
fi

# Prepare environment
cd "${CONTAINER_PATH}"
IMAGE_NAME="$(jq -r ".imageName" ci.json)"
IMAGE_TAG="${GIT_COMMIT}"

# Create .npmrc file
eval "$(assume-role "${ENVIRONMENT}")"
echo "Getting secret from Secrets Manager:"
aws secretsmanager get-secret-value --secret-id "${ENVIRONMENT}"/deployment/npm/npmrc --region us-west-1 | jq .SecretString -r > .npmrc
aws secretsmanager get-secret-value --secret-id "${ENVIRONMENT}"/deployment/pip/pip.conf --region us-west-1 | jq .SecretString -r > pip.conf

# Using production role for ECR image pull/push
eval "$(assume-role prod)"

eval "$(aws ecr get-login --no-include-email --region us-west-1)"

APP_ENV="${ENVIRONMENT}"
if [[ "${ENVIRONMENT}" == "prod" ]]; then
  APP_ENV="production"
fi

ECR_IMAGE_TAG=$(aws ecr describe-images --region us-west-1 --repository-name "${IMAGE_NAME}" \
  | jq -r '.imageDetails[].imageTags[]?' \
  | grep "${IMAGE_TAG}" \
  | grep -v "sandbox\|staging\|prod" \
  || echo "")
if [[ "" == "${ECR_IMAGE_TAG}" || "true" == "${FORCE_REBUILD}" ]]; then

  # Check to run any pre docker build action
  PRE_DOCKER_BUILD_ACTIONS="${WORKSPACE}/${CONTAINER_PATH}/infra/scripts/pre-docker-build-actions.sh"
  if [[ -e "${PRE_DOCKER_BUILD_ACTIONS}" ]]; then
    "${PRE_DOCKER_BUILD_ACTIONS} ${PROFILE} ${WORKSPACE} ${CONTAINER_PATH}"
  fi

  # build image (tests execute as part of the build)
  docker build \
    --no-cache="${FORCE_REBUILD}" \
    --build-arg "APP_ENV=${APP_ENV}" \
    --build-arg "APP_VERSION=${GIT_COMMIT}" \
    --build-arg "NPMRC=.npmrc" \
    --tag "${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}" \
    --tag "${REGISTRY_HOST}/${IMAGE_NAME}:build-cache" \
      .
  cd "${WORKSPACE}"

  # allow for a test.command to be set in ci.json to run tests outside of docker builds
  TEST_COMMAND="$(jq -r '.test.command' "${CONTAINER_PATH}/ci.json")"

  if [[ "${TEST_COMMAND}" != "" && "${TEST_COMMAND}" != "null" ]]; then
    # cd to the container path, then run the test command
    cd "${CONTAINER_PATH}"
    info "RUNNING A TEST FROM ci.json test.command"
    info "Once completed, Jenkins deploy job will continue"
    eval "${TEST_COMMAND}"

    # cd back into workspace to continue
    cd "${WORKSPACE}"
  fi

  info "Pushing Docker image to ECR"
  docker push "${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}"
  docker push "${REGISTRY_HOST}/${IMAGE_NAME}:build-cache"
else
  # make sure in workspace
  cd "${WORKSPACE}"
  eval "$(aws ecr get-login --no-include-email --region us-west-1)"
  docker pull "${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}"
fi

COMPOSE="$(jq -r .compose "${CONTAINER_PATH}/ci.json")"
BASE_SERVICE="$(jq -r .serviceName "${CONTAINER_PATH}/ci.json")"
BASE_CLUSTER="$(jq -r .cluster "${CONTAINER_PATH}/ci.json")"

# allow for a projectBase to be set in ci.json - default BASE_CLUSTER
PROJECT_BASE="$(jq -r '.projectBase' "${CONTAINER_PATH}/ci.json")"

if [[ "${PROJECT_BASE}" != "" && "${PROJECT_BASE}" != "null" ]]; then
  info "projectBase in ci.json - use for PROJECT_PREFIX"
else
  info "projectBase not in ci.json - use BASE_CLUSTER for PROJECT_PREFIX"
  PROJECT_BASE="${BASE_CLUSTER}"
fi

info "Loaded Environment: ${ENVIRONMENT}"
info "Preparing to release '${COMPOSE}' task definition"

# get aws task specific metadata
SERVICE_NAME=${SERVICE}
SERVICE="${BASE_SERVICE}-${ENVIRONMENT}"
CLUSTER="${BASE_CLUSTER}-${ENVIRONMENT}"
PROJECT_PREFIX="${PROJECT_BASE}-${ENVIRONMENT}-"
FAMILY="${PROJECT_PREFIX}${COMPOSE}"

eval "$(assume-role "${ENVIRONMENT}")"
PREVIOUSLY_DEPLOYED_TASK_DEFINITION=$(
  aws ecs describe-services \
    --services "${SERVICE}" \
    --cluster "${CLUSTER}" \
    --region us-west-1 \
  | jq -r ".services[0].taskDefinition" \
  | awk -F / '{print $NF}'
)

info "Old task definition: ${PREVIOUSLY_DEPLOYED_TASK_DEFINITION}"

COMPOSE_FILE="./docker/${COMPOSE}.yml"
ECS_PARAMS_FILE="./docker/${COMPOSE}.ecs-params.yml"

# Create YAML file
sed -e "s/\${REPOSITORY_HOST}/${REGISTRY_HOST}/g" \
    -e "s/\${IMAGE_TAG}/${IMAGE_TAG}/g" \
    -e "s/\${ENVIRONMENT}/${ENVIRONMENT}/g" \
    "${COMPOSE_FILE}" > docker-compose.yml

info "docker-compose.yml after configuration"
cat docker-compose.yml

# Create ECS params file if it doesn't exist, then configure it with secret references.
if [[ ! -f "${ECS_PARAMS_FILE}" ]]; then
  echo -e "version: 1\ntask_definition: {}" > "${ECS_PARAMS_FILE}"
fi

if [[ -f ${WORKSPACE}/services/${SERVICE_NAME}/ci.json ]]; then
  python scripts/create_ecs_params.py \
    --ci_file_path="${WORKSPACE}/services/${SERVICE_NAME}/ci.json" \
    --ecs_params_file_path="${WORKSPACE}/docker/${COMPOSE}.ecs-params.yml" \
    --environment="${ENVIRONMENT}"
else
  error "No ci.json file found. Aborting script."
fi

# Validate yaml file from previous step
python -c 'import yaml, sys; yaml.safe_load(sys.stdin)' < ecs-params.yml || error "Script-generated ecs-params.yml file is invalid."

info "ecs-params.yml after configuration"
cat ecs-params.yml

# Assume the environment role for non-ECR activities
eval "$(assume-role "${ENVIRONMENT}")"

# Register task definition and update service
info "Registering task definition with AWS"
NEWLY_DEPLOYED_TASK_DEFINITION=$(
  ecs-cli compose \
    --file docker-compose.yml \
    --ecs-params ecs-params.yml \
    --project-name "${FAMILY}" \
    create \
    --region "us-west-1" \
    --cluster "${CLUSTER}" \
    --launch-type EC2 \
    --tags "Company=wt,Service=ecs,Application=${SERVICE_NAME},Environment=${ENVIRONMENT}" \
  | grep TaskDefinition \
  | sed 's/.*TaskDefinition="\(.*\)"/\1/'
)

if [[ -z "${NEWLY_DEPLOYED_TASK_DEFINITION}" ]]; then
  error "Failed to receive a task definition from ecs-cli command."
fi

info "Updating service to use new task definition ${NEWLY_DEPLOYED_TASK_DEFINITION}"
aws ecs update-service \
  --region us-west-1 \
  --cluster="${CLUSTER}" \
  --service "${SERVICE}" \
  --task-definition "${NEWLY_DEPLOYED_TASK_DEFINITION}" \
  --force-new-deployment

info "Waiting for service to become stable"
aws ecs wait services-stable \
  --region us-west-1 \
  --cluster "${CLUSTER}" \
  --services "${SERVICE}"

# Run smoketest and revert build if it fails
SMOKETEST_RESULT=""
if [[ "${SKIP_SMOKETEST}" != true ]]; then
  set +euo pipefail
  run_smoketest "${SERVICE_URL}" "${SMOKETEST_COMMAND}" "${CONTAINER_PATH}"
  set -euo pipefail
fi

if [[ "${SMOKETEST_RESULT}" != 0 ]] && [[ -n "${SMOKETEST_RESULT}" ]]; then
  aws ecs update-service \
    --service "${SERVICE}" \
    --cluster="${CLUSTER}" \
    --region us-west-1 \
    --task-definition "${PREVIOUSLY_DEPLOYED_TASK_DEFINITION}" \
    --force-new-deployment && \
  error 'Smoketest failed. Deployment reverted.'
fi

# Send deployment information to NewRelic.
cd "${WORKSPACE}/${CONTAINER_PATH}"
NEWRELIC_PARAMETER_NAME="/${ENVIRONMENT}/common/NEWRELIC_API_KEY"
NEWRELIC_APPLICATION_ID=$(jq -er ".newRelic.applicationId.${ENVIRONMENT} // empty" ci.json || echo "")
BUILD_USER="${BUILD_USER:-Jenkins}"
BUILD_CHANGELOG=$(git --no-pager log --oneline "${GIT_COMMIT}" | head -15 || echo "")

if [[ -n "${NEWRELIC_APPLICATION_ID}" && "${ENVIRONMENT}" == "prod" ]]; then
    info "Retrieving NewRelic API Key from AWS Parameter Store..."
    NEWRELIC_API_KEY=$(
        aws ssm \
            get-parameter \
            --name "${NEWRELIC_PARAMETER_NAME}" \
            --region "us-west-1" \
            --with-decryption | \
        jq -r '.Parameter.Value' \
        || echo ""
    )
    if [[ -n "${NEWRELIC_API_KEY}" ]]; then
        # Don't fail build if deployment couldn't be saved.
        set +x
        info "Logging deploy information into NewRelic..."
        NEWRELIC_DEPLOY_INFO=$(jq -n \
            --arg rev "${IMAGE_TAG}" \
            --arg usr "${BUILD_USER}" \
            --arg log "${BUILD_CHANGELOG}" \
            --arg txt "Jenkins test deployment #${BUILD_NUMBER} (${ENVIRONMENT}" \
            '{deployment: {revision: $rev, user: $usr, changelog: $log, description: $txt}}'
        )
        # Ref: https://rpm.newrelic.com/api/explore/application_deployments/create
        curl -X POST \
            "https://api.newrelic.com/v2/applications/${NEWRELIC_APPLICATION_ID}/deployments.json" \
            -H "Content-Type: application/json" \
            -H "X-Api-Key: ${NEWRELIC_API_KEY}" \
            -d "${NEWRELIC_DEPLOY_INFO}"
        set -x
    fi
fi

info "Deployed ${NEWLY_DEPLOYED_TASK_DEFINITION} OK"
