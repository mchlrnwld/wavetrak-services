#!/usr/bin/env bash

SERVICE_NAME=${PWD##*/}
PARENT_DIRECTORY="$(basename "$(dirname "${PWD}")")"

TESTS_E2E_SCRIPT_NAME=${0##*/}

# Storybook port used during testing
PORT_STORYBOOK=6006

# Applicaiton port when running Cypress tests against local app
PORT_APP=8080

export APP_ENV=testing
export NODE_ENV=production

if [[ -z "${GITHUB_RUN_ID}" ]]; then
  CI_BUILD_ID="testing"
else
  CI_BUILD_ID="${GITHUB_RUN_ID}"
fi

# If test environment file exists, copy it so app can recognize it on npm start
if [ -e "${PWD}/.env.test" ]; then
  cp "${PWD}/.env.test" "${PWD}/.env"
fi

echo "Starting e2e tests for ${PARENT_DIRECTORY}/${SERVICE_NAME}..."

# set cypress and percy env vars
eval "$(aws ssm get-parameters \
    --names "/test/${PARENT_DIRECTORY}/${SERVICE_NAME}/CYPRESS_RECORD_KEY" "/test/${PARENT_DIRECTORY}/${SERVICE_NAME}/PERCY_TOKEN" \
    --region us-west-1 --with-decryption --query "Parameters" | \
  jq -r 'map("\(.Name | sub("'/test/"${PARENT_DIRECTORY}"/"${SERVICE_NAME}"/'";""))=\(.Value)") | join("\n")' | \
  sed -e 's/^/export /')"

# outputs script usage instructions
usage() {
    cat << USAGE >&2
Usage:
    ${TESTS_E2E_SCRIPT_NAME} [--e2e] [--e2e-storybook] [--diff-storybook]
    --e2e               Run and record e2e tests against the app with Cypress
    --e2e-storybook     Run and record e2e tests against Storybook with Cypress
    --diff-storybook    Run visual difference against Storybook stories with Percy
USAGE
    exit 1
}

# storybook automated visual diff on all story bookmarks with percy
test_storybook_diff() {
  start-server-and-test "start-storybook -p ${PORT_STORYBOOK} --ci --quiet" "${PORT_STORYBOOK}" "percy storybook http://localhost:${PORT_STORYBOOK}"
}

# e2e and visual diff testing in storybook with cypress and percy
test_storybook_e2e() {
  # TODO return percy back to command when resolved
  # percy exec --
  start-server-and-test "start-storybook -p ${PORT_STORYBOOK} --ci --quiet" "${PORT_STORYBOOK}" "cypress run --record --headless --config-file tests/cypress.storybook.json --ci-build-id ${CI_BUILD_ID} --parallel"
}

# runs cypress and percy and records to configured projects
test_e2e() {
  # TODO return percy back to command when resolved
  # percy exec --
  start-server-and-test "npm run build && npm start" "${PORT_APP}" "cypress run --record --config-file tests/cypress.app.json"
}

# process arguments
while [ $# -gt 0 ]
do
    case "$1" in
        --e2e)
        TESTS_E2E=1
        shift 1
        ;;
        --e2e-storybook)
        TESTS_E2E_STORYBOOK=1
        shift 1
        ;;
        --diff-storybook)
        TESTS_E2E_DIFF_STORYBOOK=1
        shift 1
        ;;
        --help)
        usage
        ;;
        *)
        echoerr "Unknown argument: $1"
        usage
        ;;
    esac
done

# turn all off by default
TESTS_E2E=${TESTS_E2E:-0}
TESTS_E2E_STORYBOOK=${TESTS_E2E_STORYBOOK:-0}
TESTS_E2E_DIFF_STORYBOOK=${TESTS_E2E_DIFF_STORYBOOK:-0}

# tests against storybook
if [ ${TESTS_E2E_STORYBOOK} -eq 1 ] && [ ${TESTS_E2E_DIFF_STORYBOOK} -eq 1 ]; then
  echo "Running e2e and visual diff tests against Storybook"
  test_storybook_diff
  test_storybook_e2e
elif [ ${TESTS_E2E_STORYBOOK} -eq 1 ]; then
  echo "Running e2e tests against Storybook"
  test_storybook_e2e
elif [ ${TESTS_E2E_DIFF_STORYBOOK} -eq 1 ]; then
  echo "Running visual diff tests against Storybook"
  test_storybook_diff
else
  echo "Skipping Storybook e2e and visual diff tests";
fi

# test execution against local application
if [ ${TESTS_E2E} -eq 1 ]; then
  echo "Running e2e tests against app"
  test_e2e
  exit;
else
  echo "Skipping e2e app tests";
fi
