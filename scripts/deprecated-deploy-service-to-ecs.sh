#!/bin/bash

set -euxo pipefail

function info {
  echo "[INFO] ${*}"
}

function error {
  echo "[ERROR] ${*}" >&2
  exit 1
}

# add to path for assume-role
export PATH="${PATH}:/usr/local/bin"

REGISTRY_HOST="833713747344.dkr.ecr.us-west-1.amazonaws.com"
CONTAINER_PATH="services/${SERVICE}"

info "build description:${CONTAINER_PATH}|${ENVIRONMENT}|${VERSION}|"

# trap script errors
trap catch_errors ERR;
function catch_errors() {
  echo "script aborted, because of errors";
  exit 1;
}

# determine AWS profile
if [[ "${ENVIRONMENT}" == "sandbox" ]]; then
  PROFILE="dev"
else
  PROFILE="${ENVIRONMENT}"
fi

# Prepare environment
cd "${CONTAINER_PATH}"
IMAGE_NAME="$(jq -r ".imageName" ci.json)"
IMAGE_TAG="${GIT_COMMIT}"

# Create .npmrc file
eval "$(assume-role "${ENVIRONMENT}")"
echo "Getting secret from Secrets Manager:"
aws secretsmanager get-secret-value --secret-id ${ENVIRONMENT}/deployment/npm/npmrc --region us-west-1 | jq .SecretString -r > .npmrc

# Using production role for ECR image pull/push
eval "$(assume-role prod)"

eval "$(aws ecr get-login --no-include-email --region us-west-1)"

APP_ENV="${ENVIRONMENT}"
if [[ "${ENVIRONMENT}" == "prod" ]]; then
  APP_ENV="production"
fi

ECR_IMAGE_TAG=$(aws ecr describe-images --region us-west-1 --repository-name "${IMAGE_NAME}" \
  | jq -r '.imageDetails[].imageTags[]?' \
  | grep "${IMAGE_TAG}" \
  | grep -v "sandbox\|staging\|prod" \
  || echo "")
if [[ "" == "${ECR_IMAGE_TAG}" || "true" == "${FORCE_REBUILD}" ]]; then

  # Check to run any pre docker build action
  PRE_DOCKER_BUILD_ACTIONS="${WORKSPACE}/${CONTAINER_PATH}/infra/scripts/pre-docker-build-actions.sh"
  if [[ -e "${PRE_DOCKER_BUILD_ACTIONS}" ]]; then
    "${PRE_DOCKER_BUILD_ACTIONS} ${PROFILE} ${WORKSPACE} ${CONTAINER_PATH}"
  fi

  # build image (tests execute as part of the build)
  docker build \
    --no-cache="${FORCE_REBUILD}" \
    --build-arg "APP_ENV=${APP_ENV}" \
    --build-arg "APP_VERSION=${GIT_COMMIT}" \
    --build-arg "NPMRC=.npmrc" \
    --tag "${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}" \
    --tag "${REGISTRY_HOST}/${IMAGE_NAME}:build-cache" \
      .
  cd "${WORKSPACE}"

  # allow for a test.command to be set in ci.json to run tests outside of docker builds
  TEST_COMMAND="$(jq -r '.test.command' "${CONTAINER_PATH}/ci.json")"

  if [[ "${TEST_COMMAND}" != "" && "${TEST_COMMAND}" != "null" ]]; then
    # cd to the container path, then run the test command
    cd "${CONTAINER_PATH}"
    info "RUNNING A TEST FROM ci.json test.command"
    info "Once completed, Jenkins deploy job will continue"
    eval "${TEST_COMMAND}"

    # cd back into workspace to continue
    cd "${WORKSPACE}"
  fi

  info "Pushing Docker image to ECR"
  docker push "${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}"
  docker push "${REGISTRY_HOST}/${IMAGE_NAME}:build-cache"
else
  # make sure in workspace
  cd "${WORKSPACE}"
  eval "$(aws ecr get-login --no-include-email --region us-west-1)"
  docker pull "${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}"
fi

COMPOSE="$(jq -r .compose "${CONTAINER_PATH}/ci.json")"
TASK_ROLE_ARN="$(jq -r ".taskRoleArn.${ENVIRONMENT}" "${CONTAINER_PATH}/ci.json")"
BASE_SERVICE="$(jq -r .serviceName "${CONTAINER_PATH}/ci.json")"
BASE_CLUSTER="$(jq -r .cluster "${CONTAINER_PATH}/ci.json")"

# allow for a projectBase to be set in ci.json - default BASE_CLUSTER
PROJECT_BASE="$(jq -r '.projectBase' "${CONTAINER_PATH}/ci.json")"

if [[ "${PROJECT_BASE}" != "" && "${PROJECT_BASE}" != "null" ]]; then
  info "projectBase in ci.json - use for PROJECT_PREFIX"
else
  info "projectBase not in ci.json - use BASE_CLUSTER for PROJECT_PREFIX"
  PROJECT_BASE="${BASE_CLUSTER}"
fi

info "Loaded Environment: ${ENVIRONMENT}"
info "Preparing to release '${COMPOSE}' task definition"

# get aws task specific metadata
SERVICE="${BASE_SERVICE}-${ENVIRONMENT}"
CLUSTER="${BASE_CLUSTER}-${ENVIRONMENT}"
PROJECT_PREFIX="${PROJECT_BASE}-${ENVIRONMENT}-"
FAMILY="${PROJECT_PREFIX}${COMPOSE}"

COMPOSE_FILE="./docker/${COMPOSE}.yml"
ECS_PARAMS_FILE="./docker/${COMPOSE}.ecs-params.yml"

# Create YAML file containing environment vars
cp "${ENVIRONMENT}.env" build_env.json
jq -r '.[] | "\(.name)=\(.value)"' build_env.json > build_env.yml

# Configure docker-compose file for service and environment
python -c "import json, sys, yaml ; \
           y = yaml.safe_load(sys.stdin.read()) ; \
           y['services']['${COMPOSE}']['env_file'] = './build_env.yml' ; \
           yaml.dump(y, sys.stdout)" < "${COMPOSE_FILE}" \
  | sed -e "s/\${REPOSITORY_HOST}/${REGISTRY_HOST}/g" \
  | sed -e "s/\${IMAGE_TAG}/${IMAGE_TAG}/g" \
  | sed -e "s/\${ENVIRONMENT}/${ENVIRONMENT}/g" \
  > docker-compose.yml

info "docker-compose.yml after configuration"
cat docker-compose.yml

# Configure ECS params file for environment
# Create it if it doesn't exist
if [[ ! -f "${ECS_PARAMS_FILE}" ]]; then
  echo -e "version: 1\ntask_definition: {}" > "${ECS_PARAMS_FILE}"
fi

if [[ "${TASK_ROLE_ARN}" != "" && "${TASK_ROLE_ARN}" != "null" ]]; then
  python -c "import json, sys, yaml ; \
             y = yaml.safe_load(sys.stdin.read()) ; \
             y['task_definition']['task_role_arn'] = '${TASK_ROLE_ARN}' ; \
             yaml.dump(y, sys.stdout)" < "${ECS_PARAMS_FILE}" \
    > ecs-params.yml
else
  cat "${ECS_PARAMS_FILE}" > ecs-params.yml
fi

info "ecs-params.yml after configuration"
cat ecs-params.yml

# Assume the environment role for non-ECR activities
eval "$(assume-role "${ENVIRONMENT}")"

# Register task definition and update service
info "Registering task definition with AWS"
TASK=$(
  ecs-cli compose \
    --file docker-compose.yml \
    --ecs-params ecs-params.yml \
    --project-name "${FAMILY}" \
    create \
    --region "us-west-1" \
    --cluster "${CLUSTER}" \
    --launch-type EC2 \
  | grep TaskDefinition \
  | sed 's/.*TaskDefinition="\(.*\)"/\1/'
)

info "Updating service to use new task definition ${TASK}"
aws ecs update-service \
  --region us-west-1 \
  --cluster="${CLUSTER}" \
  --service "${SERVICE}" \
  --task-definition "${TASK}"

info "Waiting for service to become stable"
aws ecs wait services-stable \
  --region us-west-1 \
  --cluster "${CLUSTER}" \
  --services "${SERVICE}"

info "Deployed ${TASK} OK"
