#!/usr/bin/env bash

# This script is used by packages in order to automate annotated git tagging
# which Lerna uses in order to track version that are published/versioned whenever
# we run `lerna version` and `lerna publish`

PACKAGE_VERSION=$(< package.json grep \"version\" \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[:space:]')

PACKAGE_NAME=$(< package.json grep \"name\" \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[:space:]')


TAG="${PACKAGE_NAME}@${PACKAGE_VERSION}"
TAG_MESSAGE="chore: release ${TAG}"

echo "Tagging ${TAG} with message: \"${TAG_MESSAGE}\""
git tag -a "${TAG}" -m "${TAG_MESSAGE}"
git push --tags
