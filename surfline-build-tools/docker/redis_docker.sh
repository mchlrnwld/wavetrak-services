#!/usr/bin/env bash
eval "$(docker-machine env default)"
docker pull redis:2.8
docker run -p 6379:6379 --name local-redis -d redis:2.8
