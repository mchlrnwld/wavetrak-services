# Surfline Build Tools
Build tools for local development environment
[Code Standardization](https://wavetrak.atlassian.net/wiki/display/SUBR/Git+and+Code+Base+Standardization)

# Local Nginx reverse-proxy
We use Nginx to route traffic between microservices. The bundled dockerfile and config allows you to host a collection of services and access them all through one 'proxy' service. If you have user-service hosted locally on :8080 and auth-service hosted locally on :8081, you can configure nginx to route /user to user-service and /auth to auth-service. You can even mix and match local and remote services if your data sources are the same.

You can start a local Nginx with:
```
$ cd nginx 
$ docker build -t nginxlocal1 .
$ docker run --name mynginx -p 80:80 -d nginxlocal1
```
Check out the `nginx/conf/default.conf` to change the IPs to your desired service locations.

# OSX
You can ignore this section if you already have brew/node/docker installed. If you are on Linux/Windows edit this wiki and add some instructions
## Install homebrew, node, and docker
  * [Homebrew](http://brew.sh/)
    * After install: `brew update`
    * `brew install node nvm mongodb`
  * [Docker](https://www.docker.com/)
    * You should also install their GUI, Kitematic for managing your local dockers
  
# Setup local MongoDB and Redis instances
  * In terminal from surfline-build-tools dir: `npm run deploy`
    * This will download and install our two main databases locally
    * Fire up Kitematic to see local-mongo & local-redis configuration

## Interface with MongoDB and Redis
  * [Robomongo](https://robomongo.org/) for MongoDB
  * [Redis Desktop Manager](http://redisdesktop.com/) for Redis
  
# Importing data into database
## Mongo Backup/Restore
 
  * Dump collection from server `mongodump --host 192.168.99.100:27017 --db SurflineDev --collection MyUsers --gzip --archive surflineMongoDump.tar.gz`
  * Restore collection dump to server `mongorestore --host 192.168.99.100:27017 --gzip --archive surflineMongoDump.tar.gz`
  
## Redis Backup/Restore 
  
  * Snapshotting, DB dump to file and reload from disk [StackOverflow](http://stackoverflow.com/questions/6004915/how-do-i-move-a-redis-database-from-one-server-to-another)
    * Save a snapshot of DB `redis-cli bgsave`
    * Shutdown target DB
    * Copy this dump.rdb to the other redis server you want to migrate to. When redis starts up, it looks for this file to initialize the database from.
      * `cp /var/lib/redis/dump.rdb /backup/redis/dump.$(date +%Y%m%d%H%M).rdb`
    * Start target redis instance
      
  * Live migration, Copy from one live instance to another live instance [Redis Copy](https://github.com/yaauie/redis-copy)
    * `redis-copy --no-prompt old.redis.host/9 new.redis.host:6380/3`
    * You could use this from one dev to another dev, for copying data down from production, or from one redis docker to another docker

# Roadmap
  * Create seed data for development and testing
    * Save data dumps in `data/` directory and easily restore with `npm run seed mongodb user-sample`
  * Micro service communication during development
    * Create environment JSON for storing variables for service URLs, update variables to point to localhost/remote services
  * Deployment finalization
    * Jenkins builds from git commit triggers on master branch, when pull request merges
    * Jenkins tests and pushes through Integration/QA/Production environments
    * Goal is continuous deployment
