apps = [
    // SERVICES
    [
        type:          "services",
        containerPath: "favorites-service",
        version:       "services_favorites_service_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "feed-service",
        version:       "services_feed_service_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "kbyg-product-api",
        version:       "services_kbyg_product_api_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "location-view-api",
        version:       "services_location_view_api_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "location-view-service",
        version:       "services_location_view_service_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "onboarding-service",
        version:       "services_onboarding_service_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "reports-api",
        version:       "services_reports_api_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "search-service",
        version:       "services_search_service_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "spots-api",
        version:       "services_spots_api_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],
    [
        type:          "services",
        containerPath: "user-service",
        version:       "services_user_service_version",
        build:         "/surfline/Web/deploy-service-to-ecs"
    ],

    // ADMIN-TOOLS
    [
        type:          "admin-tools",
        containerPath: "modules/cameras",
        version:       "admin_tools_modules_cameras_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "modules/forecasts",
        version:       "admin_tools_modules_forecasts_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "modules/login",
        version:       "admin_tools_modules_login_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "modules/reports",
        version:       "admin_tools_modules_reports_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "modules/spots",
        version:       "admin_tools_modules_spots_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "modules/users",
        version:       "admin_tools_modules_users_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "services/auth-service",
        version:       "admin_tools_services_auth_service_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "services/cache-service",
        version:       "admin_tools_services_cache_service_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "services/forecasts-api",
        version:       "admin_tools_services_forecasts_api_version",
        build:         "/admin/deploy-container"
    ],
    [
        type:          "admin-tools",
        containerPath: "services/services-proxy",
        version:       "admin_tools_services_services_proxy_version",
        build:         "/admin/deploy-container"
    ],

    // EDITORIAL
    [
        type:          "editorial",
        containerPath: "wp-admin",
        version:       "editorial_wp_admin_version",
        build:         "/surfline/Web/deploy-editorial-to-ecs"
    ],
    [
        type:          "editorial",
        containerPath: "wordpress",
        version:       "editorial_wordpress_version",
        build:         "/surfline/Web/deploy-editorial-to-ecs"
    ],
    [
        type:          "editorial",
        containerPath: "varnish",
        version:       "editorial_varnish_version",
        build:         "/surfline/Web/deploy-editorial-to-ecs"
    ],

    // WEB
    [
        type:          "web",
        containerPath: "homepage",
        version:       "web_homepage_version",
        build:         "/surfline/Web/deploy-frontend-to-ecs"
    ],
    [
        type:          "web",
        containerPath: "kbyg",
        version:       "web_kbyg_version",
        build:         "/surfline/Web/deploy-frontend-to-ecs"
    ],
    [
        type:          "web",
        containerPath: "onboarding",
        version:       "web_onboarding_version",
        build:         "/surfline/Web/deploy-frontend-to-ecs"
    ],
    [
        type:          "web",
        containerPath: "travel",
        version:       "web_travel_version",
        build:         "/surfline/Web/deploy-frontend-to-ecs"
    ]

    // SUBSCRIPTIONS
    // FIXME: Add these
    // "web/subscription-mvp",
    // "web/subscriptions",
]

node {
    if (ENVIRONMENT == '') {
        println("Error: You must specify an ENVIRONMENT")
        currentBuild.result = "FAILURE"

    } else {
        // Iterate over apps and if the include a version, deploy them
        for (HashMap app : apps) {
            if (params[app.version] != '') {
                // TODO: Check to see if specified version is different from currently deployed version

                // Do build
                try {
                    stage "${app.type}/${app.containerPath}"

                    build job: app.build, \
                        parameters: [string(name: 'CONTAINER_PATH', value: app.containerPath), \
                                     string(name: 'ENVIRONMENT',    value: ENVIRONMENT), \
                                     string(name: 'VERSION',        value: params[app.version])]

                } catch(any) {
                    println("Exception deploying ${app.name}")
                    currentBuild.result = "FAILURE"
                }
            }
        }
    }
}
