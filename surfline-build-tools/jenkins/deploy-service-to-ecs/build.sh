#!/bin/bash

set -x

REGISTRY_HOST="833713747344.dkr.ecr.us-west-1.amazonaws.com"
CONTAINER_PATH="services/${SERVICE}"

# trap script errors
trap catch_errors ERR;
function catch_errors() {
   echo "script aborted, because of errors";
   exit 1;
 }

# determine AWS profile
if [ "$ENVIRONMENT" == "sandbox" ]
then
  PROFILE="dev"
else
  PROFILE="$ENVIRONMENT"
fi

# Prepare environment
cd $CONTAINER_PATH
IMAGE_NAME="$(cat ci.json | jq -r ".imageName")"
IMAGE_TAG="${ENVIRONMENT}-${GIT_COMMIT}"


$(assume-role prod aws ecr get-login --no-include-email --region us-west-1)
# `|| true` added so we can continue gracefull on no build-cache image present.
# For example: First builds won't have a build cache.
docker pull ${REGISTRY_HOST}/${IMAGE_NAME}:build-cache || true

APP_ENV="$ENVIRONMENT"
if [ "$ENVIRONMENT" == "prod" ]
then
    APP_ENV="production"
fi

assume-role prod aws s3 cp s3://sl-artifactory-prod/config/.npmrc .npmrc

# Check to run any pre docker build action
PRE_DOCKER_BUILD_ACTIONS="${WORKSPACE}/${CONTAINER_PATH}/infra/scripts/pre-docker-build-actions.sh"
if [[ -e ${PRE_DOCKER_BUILD_ACTIONS} ]]; then
  ${PRE_DOCKER_BUILD_ACTIONS} ${PROFILE} ${WORKSPACE} ${CONTAINER_PATH}
fi

# build image (tests execute as part of the build)
docker build \
    --cache-from ${REGISTRY_HOST}/${IMAGE_NAME}:build-cache \
    --build-arg APP_ENV="$APP_ENV" \
    --build-arg APP_VERSION="$GIT_COMMIT" \
    --build-arg NPMRC=.npmrc \
    --tag ${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG} \
    --tag ${REGISTRY_HOST}/${IMAGE_NAME}:build-cache \
    .

cd $WORKSPACE

echo "Pushing Docker image to ECR"
docker push ${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_TAG}
docker push ${REGISTRY_HOST}/${IMAGE_NAME}:build-cache

COMPOSE="$(cat ${CONTAINER_PATH}/ci.json | jq -r '.compose')"
TASK_ROLE_ARN=$(cat ${CONTAINER_PATH}/ci.json | jq -r ".taskRoleArn.${ENVIRONMENT}")
BASE_SERVICE="$(cat ${CONTAINER_PATH}/ci.json | jq -r '.serviceName')"
BASE_CLUSTER="$(cat ${CONTAINER_PATH}/ci.json | jq -r '.cluster')"


echo "Loaded Environment: ${ENVIRONMENT}"
echo "Preparing to release '${COMPOSE}' task definition"

# get aws task specific metadata
SERVICE="${BASE_SERVICE}-${ENVIRONMENT}"
CLUSTER="${BASE_CLUSTER}-${ENVIRONMENT}"

COMPOSE_FILE="./docker/${COMPOSE}.yml"
PROJECT_PREFIX="${BASE_CLUSTER}-${ENVIRONMENT}-"
FAMILY="${PROJECT_PREFIX}${COMPOSE}"

cp "${ENVIRONMENT}.env" build_env.json

# Build task definition from compose
docker pull micahhausler/container-transform:latest
cat "${COMPOSE_FILE}" | docker run --rm -i micahhausler/container-transform | \
    jq ".containerDefinitions[].environment = $(cat build_env.json)" | \
    sed -e "s/\${REPOSITORY_HOST}/${REGISTRY_HOST}/g" | \
    sed -e "s/\${IMAGE_TAG}/${IMAGE_TAG}/g" | \
    sed -e "s/\${ENVIRONMENT}/${ENVIRONMENT}/g" \
    > def.json

# If compose file contains memory limit and reservation, use them
MEM_RESERVATION=$(grep mem_reservation ${COMPOSE_FILE} | sed -e 's/.*mem_reservation: \([0-9]*\)m$/\1/')
if [[ "${MEM_RESERVATION}" != "" ]]; then
    jq ".containerDefinitions[].memoryReservation = ${MEM_RESERVATION}" def.json > def.json.tmp
    mv def.json.tmp def.json
fi

MEM_LIMIT=$(grep mem_limit ${COMPOSE_FILE} | sed -e 's/.*mem_limit: \([0-9]*\)m$/\1/')
if [[ "${MEM_LIMIT}" != "" ]]; then
    jq ".containerDefinitions[].memory = ${MEM_LIMIT}" def.json > def.json.tmp
    mv def.json.tmp def.json
fi

echo "def.json contains for ${ENVIRONMENT}"
cat def.json

eval $(assume-role $ENVIRONMENT)

# Apply task defintion role ARN
if [ $TASK_ROLE_ARN != "" ] && [ $TASK_ROLE_ARN != "null" ]
then
  aws ecs register-task-definition \
    --cli-input-json "file://$(pwd)/def.json" \
    --task-role-arn ${TASK_ROLE_ARN} \
    --region "us-west-1" \
    --family ${FAMILY}
else

  aws ecs register-task-definition \
    --cli-input-json "file://$(pwd)/def.json" \
    --region "us-west-1" \
    --family ${FAMILY}
fi


TASK="$(aws ecs list-task-definitions --region us-west-1 --family-prefix ${FAMILY} | jq -r '.taskDefinitionArns[-1:][]')"

aws ecs update-service --region us-west-1 --cluster="${CLUSTER}" --service "${SERVICE}" --task-definition "${TASK}"

echo "Deploying $TASK"
aws ecs wait services-stable --region us-west-1 --cluster "${CLUSTER}" --services "${SERVICE}"
echo "Deployed $TASK OK"

echo "build description:${CONTAINER_PATH}|${ENVIRONMENT}|${VERSION}|"
