var proxy = require('express-http-proxy');
var http = require('http');
var app = require('express')();

app.server = http.createServer(app);

app.use('/user', proxy('localhost:8001', {
  forwardPath: function (req, res) {
    console.log(req.originalUrl);
    return '/user' + require('url').parse(req.url).path;
  }
}));

app.use('/auth', proxy('localhost:8002', {
  forwardPath: function (req, res) {
    console.log(req.originalUrl);
    return '/auth' + require('url').parse(req.url).path;
  }
}));
app.use('/password-reset', proxy('localhost:8000', {
  forwardPath: function (req, res) {
    console.log(req.originalUrl);
    return '/password-reset' + require('url').parse(req.url).path;
  }
}));
app.use('/mobile', proxy('localhost:8003', {
  forwardPath: function (req, res) {
    console.log(req.originalUrl);
    return '/' + require('url').parse(req.url).path;
  }
}));
app.use('/subscriptions', proxy('localhost:8004', {
  forwardPath: function (req, res) {
    console.log(req.originalUrl);
    return '/subscriptions' + require('url').parse(req.url).path;
  }
}));
app.use('/subs', proxy('https://sandbox.surfline.com', {
  forwardPath: function (req, res) {
    console.log(req.originalUrl);
    return '/subs' + require('url').parse(req.url).path;
  }
}));

app.server.listen(8080);

console.log('Started on port ' + app.server.address().port);
