# Surfline Documents Markdown Style Guide

A common guideline for documentation files in Surfline projects.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [TBD](#tbd)
- [Create a Table of Contents](#create-a-table-of-contents)
  - [Generators](#generators)
  - [Using `doctoc`](#using-doctoc)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## TBD

*Markdown styleguide coming soon*

## Create a Table of Contents

When you're crafting a markdown file, you should add a **table of contents** at the top of the document between a brief **summary** section explaining the purpose of the application/function/job/module and the actual **content** of the file, having a list of the headings in the content with an anchor link to each of them:

```text
> title
> summary
> table of contents
> content
```

### Generators

There are several tools and editor extensions to automatically generate a ToC:

- Run the `doctoc` target from a `Makefile` before committing the document **(recommended)**:

  ```shell
  make doctoc
  ```

- Install the `doctoc` package globally and the run the command:

  ```shell
  npm install -g doctoc
  doctoc .
  ```

- Use a standard markdown extension like [**Markdown All in One**](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) for VSCode and a other editors. When you save the file, it will update the ToC automatically.

### Using `doctoc`

First, make sure you have the `doctoc` target configured in the `Makefile` as the following:

```makefile
.PHONY: doctoc
doctoc:
  @docker run \
    --entrypoint doctoc \
    --rm -i -t -v "$(shell pwd)":/usr/src jorgeandrada/doctoc README.md \
    --title "## Table of Contents <!-- omit in toc -->" \
    --maxlevel 5
```

If you use `doctoc` from the `npm` package, take into account that the recommended approach, takes care of a "Table of Contents" title which wouldn't be present in the ToC list and a headings depth set up to 5 levels.

If you're working in a brand new document, add two comment blocks in the `README.md` file right after the **summary** and the **content**:

```markdown
<!-- START doctoc -->
<!-- END doctoc -->
```

After updating the markdown file, when you run `make doctoc` or `doctoc .` depending on your preferred configuration, a **Table of Contents** will be automatically generated in the location you just placed the comment blocks above.

```shell
make doctoc
```

```console
DocToccing single file "README.md" for github.com.

==================

"README.md" will be updated

Everything is OK.
```

Now you should see that the file was updated:

```markdown
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

...
- [TBD](#tbd)
- [Create a Table of Contents](#create-a-table-of-contents)
  - [Generators](#generators)
  - [Using `doctoc`](#using-doctoc)
...

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
```
