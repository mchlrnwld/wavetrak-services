# Terraform Style Guide

Terraform is an infrastructure-as-code tool for building, changing, and managing infrastructure in a safe, repeatable way. It is used at Wavetrak to manage all new (and most old) infrastructure.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Quick Start](#quick-start)
- [Credentials](#credentials)
  - [AWS Credentials](#aws-credentials)
    - [Credentials Via Shared Credentials File](#credentials-via-shared-credentials-file)
      - [Example `~/.aws/credentials`](#example-awscredentials)
      - [Example `~/.aws/config`](#example-awsconfig)
      - [Using Credentials Via Shared Credentials File](#using-credentials-via-shared-credentials-file)
    - [Credentials Via Environment Variables](#credentials-via-environment-variables)
      - [Using Credentials Via Environment Variables](#using-credentials-via-environment-variables)
  - [New Relic Credentials](#new-relic-credentials)
    - [Creating a New Relic API Key](#creating-a-new-relic-api-key)
    - [Storing New Relic API Key](#storing-new-relic-api-key)
    - [Using Terraform-managed New Relic Alerts in Lower Tiers](#using-terraform-managed-new-relic-alerts-in-lower-tiers)
- [Makefiles](#makefiles)
- [Project Locations](#project-locations)
  - [ECS Services](#ecs-services)
  - [Lambda Functions](#lambda-functions)
  - [AWS Batch Jobs](#aws-batch-jobs)
    - [Compute Environments and Job Queues](#compute-environments-and-job-queues)
    - [Job Descriptions](#job-descriptions)
  - [Shared Modules](#shared-modules)
  - [Most Everything Else](#most-everything-else)
- [Project Structures](#project-structures)
  - [Applications / Services](#applications--services)
    - [Service Example](#service-example)
    - [Service Structure](#service-structure)
  - [Modules](#modules)
    - [Shared Module Example](#shared-module-example)
    - [Shared Module Structure](#shared-module-structure)
    - [Module Compositing](#module-compositing)
- [Environments](#environments)
- [State Management](#state-management)
  - [Example `terraform {}` Block](#example-terraform--block)
- [Deployment](#deployment)
- [Formatting](#formatting)
- [Linting](#linting)
- [Naming](#naming)
- [Variable Types](#variable-types)
  - [Primitive Types](#primitive-types)
  - [Complex Types](#complex-types)
- [Validation](#validation)
- [Updating](#updating)
  - [Terraform CLI](#terraform-cli)
  - [Providers](#providers)
- [IAM Policy Documents](#iam-policy-documents)
- [Resource Tagging](#resource-tagging)
- [Redundant Resource Types](#redundant-resource-types)
- [Configuring New Relic Alerts](#configuring-new-relic-alerts)
  - [Example New Relic alert configuration](#example-new-relic-alert-configuration)
- [More Information](#more-information)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Quick Start

These quick start instructions assume that you're using macOS and have [Homebrew](https://brew.sh/) installed. These commands should translate to Linux and Windows pretty easily.

1. Install [tfenv](https://github.com/tfutils/tfenv)

    This will allow you to manage different versions of Terraform on a single machine.

    ```bash
    brew install tfenv
    ```

1. Install the latest version of Terraform

    ```bash
    tfenv install latest
    ```

    Note: If you've already installed Terraform without using `tfenv`, you'll need to remove or rename the Terraform binary to avoid conflicts with `tfenv`.

1. Install `make`

    ```bash
    brew install make
    ```

1. Install AWS CLI

    ```bash
    brew install awscli
    ```

1. Configure AWS credentials

    ```bash
    export AWS_ACCESS_KEY_ID=AKIAXXXXXXXXXXXXXX
    export AWS_SECRET_ACCESS_KEY=WlRBZ01EUWdaR01nTlRFZ1l6TWdabVVnTlRjZ01H
    export AWS_DEFAULT_REGION=us-west-1
    ```

    This quick start uses environment variables which are the fastest (but not best) way to get up and running. The [Credentials](#credentials) section below goes into more detail on how to use AWS credentials with Terraform.

1. Create a Terraform project

1. Format Terraform code

    ```bash
    make format
    ```

1. Show which infrastructure will be created

    ```bash
    ENV=sandbox make plan
    ```

1. Create the infrastructure

    ```bash
    ENV=sandbox make apply
    ```

1. Remove any previous environment cached files

    ```bash
    ENV=sandbox make reset
    ```

## Credentials

### AWS Credentials

AWS credentials should either be stored in a shared credentials file or set as environment variables. Never reference credentials directly in Terraform code.

#### Credentials Via Shared Credentials File

[AWS Credential Files](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) are the preferred way to manage AWS credentials for use with Terraform.

1. Store AWS credentials in `~/.aws/credentials` and config settings in `~/.aws/config` (you need both)
2. Set `AWS_PROFILE` to the profile that you wish to use from your shared credentials file

##### Example `~/.aws/credentials`

```ini
[default]
aws_access_key_id=AKIAIOSFODNN7EXAMPLE
aws_secret_access_key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

[surfline-prod]
aws_access_key_id=AKIAIOSFODNN7EXAMPLE
aws_secret_access_key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

[surfline-dev]
aws_access_key_id=AKIAXXXXXXXXEXAMPLE
aws_secret_access_key=WlRBZ01EUWdaR01nTlRFZ1l6TWdabVVEXAMPLEKEY
```

##### Example `~/.aws/config`

```ini
[default]
region=us-west-1
output=json

[profile surfline-prod]
region=us-west-1
output=json

[profile surfline-dev]
region=us-west-1
output=json
```

##### Using Credentials Via Shared Credentials File

```bash
export AWS_PROFILE=surfline-dev

ENV=dev make plan
```

#### Credentials Via Environment Variables

The following environment variables must be set:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_DEFAULT_REGION`

##### Using Credentials Via Environment Variables

```bash
export AWS_ACCESS_KEY_ID=AKIAXXXXXXXXEXAMPLE
export AWS_SECRET_ACCESS_KEY=WlRBZ01EUWdaR01nTlRFZ1l6TWdabVVEXAMPLEKEY
export AWS_DEFAULT_REGION=us-west-1

ENV=dev make plan
```

### New Relic Credentials

New Relic has a Terraform provider that we use to provision alert conditions alongside the infrastructure it monitors. The New Relic provider uses personal API credentials to authenticate Terraform's API calls. This requires each developer to create a NR API key.

#### Creating a New Relic API Key

To create a New Relic API key:

1. Navigate to the New Relic dashboard as a logged-in user.
2. From the account dropdown in the top right, select `Account settings`.
3. Select `API keys` along the left sidebar.
4. Click `Create key`.

This will create an API key. For more information, you can refer to [New Relic Documentation](https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys).

> Note: If you need a New Relic account, request this from a system administrator.

#### Storing New Relic API Key

Before invoking the New Relic Terraform configuration, you'll need to set the API key as an environment variable called `NEW_RELIC_API_KEY`. This variable must be set each time you execute a terraform configuration that uses the New Relic provider. You should also set the `NEW_RELIC_ACCOUNT_ID`, which can be found on the [New Relic API keys page](https://one.newrelic.com/launcher/api-keys-ui.launcher) next to your API keys.

```bash
export NEW_RELIC_ACCOUNT_ID=356245
export NEW_RELIC_API_KEY="<your New Relic Personal API key>"
```

To set the environment variable once-and-for-all, you can add the above line to your shell profile (`.zshrc`, `.bash_profile`, etc), so that it loads whenever you start your terminal.

#### Using Terraform-managed New Relic Alerts in Lower Tiers

A single New Relic account is used for both lower tiers and `prod`. Authentication for this New Relic account is handled via OneLogin. (Contact a system administrator if you need access to one or both of them).

The process of configuring alerts in the lower tiers is identical to configuring them in `prod`. Simply configure your shell for the proper environment, then invoke the desired Makefile target. Alerts can be toggled on/off on an environment level via the Parameter Store variable `/{env}/common/NEW_RELIC_ENABLED`. However, note that this environment variable is shared amongst all the services. If you desire to toggle an alert off for a specific *service*, then you will have to create a service-scoped version of `NEW_RELIC_ENABLED`, update the service `ci.json` file accordingly, then redeploy the service. (Note that this will have to be done in all the environments.) 

## Makefiles

We use Makefiles to manage Terraform projects. This is to simplify commands, particularly where commands must be run within subdirectories or when commands are chained. Other times, we use Makefile targets for convenience or consistency.

These are the frequently-used Makefile targets:

**`ENV={environment} make plan`**

Run Terraform plan in the specified environment.

1. Changes working directory to specified environment directory

   1. Common environments are `prod`, `staging`, and `sandbox`

2. Runs `terraform plan -refresh=true`

**`ENV={environment} make apply`**

Run Terraform apply in the specified environment.

1. Changes working directory to specified environment directory
2. Runs `terraform apply -refresh=true`

**`make format`**

Format all Terraform code in the project.

1. Runs `terraform fmt -recursive`

**`make format-check`**

Check the format of all Terraform code in the project.

1. Runs `terraform fmt -check -recursive`

**`ENV={environment} make validate`**

Validates for correctness code in the specified directory.

1. Changes working directory to specified environment directory
2. Runs `terraform apply -refresh=true`

## Project Locations

We follow the philosophy that infrastructure code should be stored as close to the applications that use it as possible. For this reason, we have Terraform projects scattered across several repos. This is intentional. These are some of the most common locations for Terraform code:

### ECS Services

Infrastructure code for services is typically found in the `/infra` directory in the service parent directory. Services are typically located in the following places:

| Repo                | Path                             |
| :------------------ | :------------------------------- |
| `wavetrak-services` | `/services/{service-name}/infra` |
| `surfline-web`      | `/{service-name}/infra`          |
| `surfline-admin`    | `/services/{service-name}/infra` |
| `surfline-admin`    | `/modules/{service-name}/infra`  |

### Lambda Functions

Similar to services, infrastructure code for lambda functions is typically found in the `/infra` directory in the lambda function parent directory. Lambda functions are typically located in the following places:

| Repo                | Path                                                     |
| :------------------ | :------------------------------------------------------- |
| `wavetrak-services` | `/tools/{lambda-name}/infra`                             |
| `wavetrak-services` | `/services/{service-name}/functions/{lambda-name}/infra` |
| `surfline-admin`    | `/functions/{lambda-name}/infra`                         |

### AWS Batch Jobs

#### Compute Environments and Job Queues

| Repo                | Path                 |
| :------------------ | :------------------- |
| `wavetrak-services` | `/jobs/common/infra` |

#### Job Descriptions

| Repo                | Path                     |
| :------------------ | :----------------------- |
| `wavetrak-services` | `/jobs/{job-name}/infra` |

### Shared Modules

Terraform modules shared by multiple projects are stored here:

| Repo                      | Path                     |
| :------------------------ | :----------------------- |
| `wavetrak-infrastructure` | `/terraform/modules/aws` |

### Most Everything Else

Most Terraform not already mentioned is stored in the following location:

| Repo                      | Path         |
| :------------------------ | :----------- |
| `wavetrak-infrastructure` | `/terraform` |

This contains AWS account bootstrapping, standalone CDNs, VPC infrastructure, ECS clusters, etc. It's a bit of a "junk drawer" of mostly older infrastructure without a better place to be stored.

## Project Structures

The goals for project structure are simplicity, minimal repetition, and separate environments. Additionally, we should avoid large Terraform projects covering many domains. Our Terraform projects should cover a single domain.

### Applications / Services

Application and service Terraform projects have many similarities and are grouped together here.

Service Terraform code should be stored in an `/infra` directory under the service route. In most cases, this would be in `git://wavetrak-services/services/{service-name}/infra`.

Terraform has a lot of boilerplate code and repetition. This project structure seeks to minimize repetition and promote simplicity.

#### Service Example

This repo contains a [service example](./examples/service/).

#### Service Structure

In summary, services and applications have one or more environment directories, a root-level module, and an optional `modules` directory. The environment directories define shared state and local variables and call the root-level module. The root-level module creates resources directly or instantiates other shared modules.

```bash
├── environments/
│   ├── prod/
│   │   └── main.tf
│   ├── sandbox/
│   │   └── main.tf
│   └── staging/
│       └── main.tf
├── modules/
├── resources/
├── .gitignore
├── .terraform-version
├── Makefile
├── README.md
├── main.tf
├── outputs.tf
├── variables.tf
└── versions.tf
```

**`environments/(prod|staging|sandbox)/main.tf`**

Environment directories should each contain a single `main.tf` file that contains a `terraform {}` block and an instantiation of the root-level module with environment-specific parameters. Since these files are repeated for each environment, the goal is to keep them as small and simple as possible.

```hcl
# ./environments/staging/main.tf
module "surfline_module" {
  source = "../../"

  company     = "sl"
  environment = "staging"
}
```

**`modules/` (optional)**

Contains modules used by the project. This is optional and only used for projects that create several resources. Most projects will only use a root-level module.

In the case where multiple modules are used, we should create a top-level module in the root of this directory which calls the lower-level modules. This will reduce repetition in environment files by allowing us to only instantiate a single module which will then instantiate the rest of the modules.

**`modules/{module-name}/*`**

See [Shared Modules](#shared-modules) below for details on how modules are structured.

**`resources/` (optional)**

Contains resource files used by the root-level module. This would typically be IAM role policy JSON documents, EC2 user data scripts, S3 bucket policy JSON documents, and other similar files.

**`.gitignore`**

Contains patterns that `git` should ignore. Local `.terraform` directories and `*.tfstate` files are typically ignored.

**`.terraform-version`**

Contains the Terraform version that the project supports.

**`README.md`**

Contains a description of the project.

**`main.tf`**

The root-level module of the project. Contains a `provider` block, local interpolated variables, resources, and modules. For most projects, all resources will be defined in the root-level module.

**`outputs.tf`**

Contains the return values of the root-level module. A child module can use outputs to expose a subset of its resource attributes to a parent module. A root module can use outputs to print certain values in the CLI output after running terraform apply.

**`variables.tf`**

Variables for the root-level module. Project variables that are not specific to the environment should be set with a default here.

**`versions.tf`**

Defines required Terraform and provider versions for the project.

```hcl
terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = "~> 2.70"
    template = "~> 2.1"
  }
}
```

### Modules

Shared modules are stored in `git://wavetrak-infrastructure/terraform/modules/aws/{module-name}`.

They are referenced in code using their `git` address:

```hcl
module "example_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"
}
```

#### Shared Module Example

This repo contains a [shared module example](./examples/module/).

#### Shared Module Structure

```bash
├── resources/
├── README.md
├── main.tf
├── outputs.tf
├── variables.tf
└── versions.tf
```

**`resources/` (optional)**

Contains resource files used by the module. This would typically be IAM role policy JSON documents, EC2 user data scripts, S3 bucket policy JSON documents, and other similar files.

**`README.md`**

Contains a description of the module and an example of how to use the module.

**`main.tf`**

Resources, or infrastructure objects, created by the module are defined in `main.tf`.

In the past, we've separated resources in a module into separate files grouped by domain. For example, S3 buckets and related IAM policies might go in `s3.tf`, SQS queues and related IAM policies might go in `sqs.tf`, etc. Going forward, we should avoid this and store all resources in `main.tf`. If a module is large enough that it would be confusing to specify all resources in `main.tf`, we should consider breaking it into smaller submodules.

**`outputs.tf`**

Contains the return values of a Terraform module. A child module can use outputs to expose a subset of its resource attributes to a parent module. A root module can use outputs to print certain values in the CLI output after running terraform apply.

**`variables.tf`**

Contains the input parameters to the module. Grouped by functional domain, not necessarily alphabetically.

**`versions.tf`**

Contains the Terraform versions that the module supports.

#### Module Compositing

We occasionally combine multiple modules in a single modules. For example, the shared module `lambda-with-sns` references the `lambda` and `sns` shared modules. This combined module represents a common pattern at Wavetrak: A Lambda function triggered by messages published to an SNS topic. They're combined in a single module rather than called individually because they require several additional Terraform resources to subscribe the Lambda function to the SNS topic and to grant SNS permissions to invoke the Lambda function.

## Environments

The most common environments that are managed by Terraform are the following:

- `prod`
- `staging`
- `sandbox`

Because of inconsistent naming in the past, you might see the following names used occasionally:

- `sbox` (same as `sandbox`)
- `dev` (same as `sandbox`)
- `integration` (no longer used)

## State Management

Terraform state is managed in S3.

| AWS Account     | Environment | S3 Bucket             | S3 Key                                     |
| :-------------- | :---------- | :-------------------- | :----------------------------------------- |
| `surfline-prod` | `prod`      | `sl-tf-state-prod`    | `{service-name}/prod/terraform.tfstate`    |
| `surfline-dev`  | `staging`   | `sl-tf-state-staging` | `{service-name}/staging/terraform.tfstate` |
| `surfline-dev`  | `sandbox`   | `sl-tf-state-sbox`    | `{service-name}/sbox/terraform.tfstate`    |

State backend is defined in `terraform {}` blocks in environment `main.tf` files.

### Example `terraform {}` Block

```hcl
terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "example-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}
```

## Deployment

Terraform code is applied to AWS directly from developer machines. Typically a developer would create their Terraform code in the `sandbox` environment, iteratively applying it to AWS as they develop. Then they would open a PR and as the PR is reviewed, apply the Terraform to `staging`. Then after the PR is approved and merged, they would apply the Terraform code to `prod`.

## Formatting

Just use `make format`. :smile:

All code should be formatted before being committed.

## Linting

Ensure that Terraform input is formatted with the following command:

```bash
terraform fmt -check -recursive
```

Alternately, this `Makefile` target may be used to check that the Terraform input is formatted:

```bash
make format-check
```

## Naming

Resources should use _snake_case_ in their references within Terraform.

```hcl
# Good
resource "aws_iam_policy" "wt_common_kms_key" { }

# Bad
resource "aws_iam_policy" "wt-common-kms-key" { }
```

Tags and names that will appear as resource references within AWS should use _kebab-case_ in their naming.

```hcl
# Good
tags = {
  Name = "${var.company}-${var.application}-${var.environment}"
}

# Good
resource "aws_iam_policy" "wt_common_kms_key" {
  name = "wt-common-kms-key"
}

# Bad
tags = {
  Name = "${var.company}_${var.application}_${var.environment}"
}
```

More information on `_snake_case_` and `_kebab-case_`: [Case Styles: Camel, Pascal, Snake, and Kebab Case](https://medium.com/better-programming/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841)

## Variable Types

Each input variable accepted by a module must be declared using a `variable` block, and the types for these variables are split in two groups:

### Primitive Types

- `string`: a sequence of Unicode characters representing some text.
- `number`: a numeric value, representing both whole numbers and fractional values.
- `bool`: a boolean value, either true or false. that can be used in conditional logic.
- `null`: a value that represents absence or omission, behaving as though you had completely omitted it if no default value.

```hcl
variable "ttl" {
  description = "Lifetime of the cache data to be persisted"
  type        = number
  default     = 3600
}

variable "enable_autoscaling" {
  description = "Allow the service to scale"
  type        = bool
  default     = false
}

variable "application" {
  default = "my-app"
}
```

If not set, Terraform *assumes* the variable type for scalar values on runtime, so not setting the variable type for the primitive types `string`, `number` and `bool`, is considered a good practice. For the sake of readability you may want to add the `type` for scalar variables anyway, but they're not needed.

The `description` tag can be omitted if the variable name is descriptive enough.

### Complex Types

- `list (or tuple)`: a sequence of values.
- `map (or object)`: a group of values identified by named labels.

For complex types, we should explicitly declare the variable type and take advantage of the [Terraform Type Constraints](https://www.terraform.io/docs/language/expressions/type-constraints.html) built-in module. This validates and restricts input variables and resource arguments to properly-typed values.

Since newer Terraform versions, you may need to also declare the type for the primitive values inside a complex structure.

```hcl
variable "availability_zones" {
  type    = list(string)
  default = [
    "us-west-1a",
    "us-west-1b",
  ]
}

variable "backoff_strategy" {
  type    = list(number)
  default = [
    10,
    30,
    60,
  ]
}

variable "acl" {
  type = map(list(string))
  default = {
    "developer" = [
      "read",
      "write",
    ],
    "admin" = [
      "read",
      "write",
      "create",
      "delete",
    ]
  }
}

variable "instance" {
  type = object({
    type      = string
    min_count = number
    max_count = number
    metrics   = bool
    subnets   = list(string)
  })
  default = {
    type      = "g4dn.xlarge"
    min_count = 1
    max_count = 10
    metrics   = true
    subnets = [
      "subnet-0001",
      "subnet-0002",
      "subnet-0003",
    ]
  }
}
```

## Validation

Ensure that Terraform input is validated with the following command:

```bash
terraform validate
```

Alternately, this `Makefile` target may be used to check that the Terraform input is formatted:

```bash
ENV=prod make validate
```

## Updating

### Terraform CLI

Terraform uses semver for release versioning. Since there hasn't yet been a `1.0.0` version, we refer to Terraform by minor version. At the time of this writing, the latest Terraform release is `0.12.28`.

Always use the most recent patch version for the minor version of Terraform that we're on. Use `.terraform-version` files in application infrastructure or module directories to inform which Terraform version should be used.

```txt
latest:^0.12
```

[tfenv](https://github.com/tfutils/tfenv) is a good Terraform version manager that supports the use of `.terraform-version` files.

### Providers

Always use the most recent provider version. I use [terraform-providers/terraform-provider-aws/CHANGELOG.md](https://github.com/terraform-providers/terraform-provider-aws/blob/master/CHANGELOG.md) to determine the latest version of the AWS provider. This is specified in the `require_providers` block in the `versions.tf` file:

```hcl
terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = "~> 2.70"
    template = "~> 2.1"
  }
}
```

## IAM Policy Documents

Hashicorp recommends the usage of `aws_iam_policy_document` for specifying IAM policies. We should use this method going forward.

Historically we've used a combination of HEREDOC-style inline IAM policies and interpolated template files.

More information on the various ways of specifying IAM policies is available at <https://learn.hashicorp.com/terraform/aws/iam-policy>.

## Resource Tagging

For all resources that support tagging, the following tags should be included at a minimum:

```hcl
tags = {
  Name        = "${var.company}_${var.application}_${var.environment}"
  Company     = var.company
  Application = var.application
  Environment = var.environment
  Terraform   = "true"
}
```

## Redundant Resource Types

Terraform occasionally has alternate names for identical resource types. In these cases, we should strive to consistently use the same variant.

- `aws_alb_listener` vs `aws_lb_listener`: Use `aws_alb_listener`. More info [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener).
- `aws_alb` vs `aws_lb`: Use `aws_alb`: More info [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb).

## Configuring New Relic Alerts

In order to promote robust application monitoring, we should use Terraform to provision New Relic alerts alongside the infrastructure it provisions. We do this by using the [New Relic provider](https://registry.terraform.io/providers/newrelic/newrelic/latest/docs) to create policy conditions in service infrastructure code.

The Terraform resource that provisions these conditions (`newrelic_nrql_alert_condition`) is configured in the respective resource module at [wavetrak-infra/terraform/modules](https://github.com/Surfline/wavetrak-infrastructure/tree/master/terraform/modules/aws). They are invoked in the service infrastructure code as part of its provisioning lifecycle. The actual condition values are configured in the `infra` directory of the respective service infrastructure code.

**Important:** Services have varying error acceptability levels, and their alert thresholds should be tuned accordingly. **We've set the alert conditions to sane-but-conservative defaults.** So you will need to experiment with these threshold values to arrive at the optimum threshold level for the alert. The recommended way to do this is by looking at historic error rates in New Relic / AWS Cloudwatch, and choosing a threshold that is representative of a degraded performance state.

Optionally, you can bypass this alert creation by setting the `newrelic_create_alert` variable to false in the respective service terraform configuration.

### Example New Relic alert configuration

The following example is taken from a wavetrak lambda function's infrastructure code. Note the `newrelic`-scoped variables in the bottom half of the module:

```hcl
module "notification-processor" {
  source = "../../"

  company     = "sl"
  application = "notifications-service"
  environment = "sandbox"

...

# Note the newrelic variables defined below

  newrelic_create_alert = true
  newrelic_policy_name = "Squad - Nerdery"
  newrelic_condition_type = "static"
  newrelic_condition_name = "Terraform - Notification Processor Lambda Function Error"
  newrelic_condition_description = "Alert when NRQL error threshold is reached."
  newrelic_condition_enabled = true
  newrelic_condition_value_function = "sum"
  newrelic_condition_violation_time_limit = "ONE_HOUR"
  newrelic_condition_query = templatefile("${path.module}/../../resources/nrql-query.tmpl", {})
  newrelic_condition_evaluation_offset = "20"
  newrelic_condition_operator = "above"
  newrelic_condition_threshold = "25"
  newrelic_condition_threshold_duration = "3600"
  newrelic_condition_threshold_occurrences = "all"
}
```

## More Information

- [HashiCorp Learn - Terraform](https://learn.hashicorp.com/terraform)
- [Terraform AWS Provider Authentication](https://www.terraform.io/docs/providers/aws/index.html)
- [AWS Credential Files](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)
- [AWS Credential Environment Variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)
- [Top 3 Terraform Testing Strategies for Ultra-Reliable Infrastructure-as-Code](https://www.contino.io/insights/top-3-terraform-testing-strategies-for-ultra-reliable-infrastructure-as-code)
