provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/example-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "example_service" {
  source = "../../"

  environment       = "staging"
  default_vpc       = "vpc-981887fd"
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
  service_td_count  = "1"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-staging/26ee81426b4723db"
  ecs_cluster       = "sl-core-svc-staging"

  auto_scaling_enabled = false
}
