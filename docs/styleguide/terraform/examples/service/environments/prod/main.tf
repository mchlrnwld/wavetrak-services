provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "example-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "example_service" {
  source = "../../"

  environment       = "prod"
  default_vpc       = "vpc-116fdb74"
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  service_td_count  = "1"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"
  ecs_cluster       = "sl-core-svc-prod"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 800
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}
