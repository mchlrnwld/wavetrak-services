# SQS Module

This module creates an SQS queue and a policy granting access to send to
the queue.

## Usage

```hcl
module "sqs_queue" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs"
  company     = "sl"
  application = "cameras-service"
  environment = "sandbox"
  queue_name  = "sl-cameras-service-clip-gen-sqs"
}
```
