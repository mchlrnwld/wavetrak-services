from lib.calculator import Calculator  # type: ignore


def greeting(name: str, first_num: float, second_num: float):
    print(f'Hello! My name is: {name}. ')
    print(
        (
            f'My favorite numbers are: {first_num} and {second_num}. '
            f'Added together they equal: '
            f'{Calculator().add(first_num, second_num)}.'
        )
    )


def main():
    name = 'Jeff'
    first_num = 3.0
    second_num = 2.0
    greeting(name, first_num, second_num)


if __name__ == '__main__':
    main()
