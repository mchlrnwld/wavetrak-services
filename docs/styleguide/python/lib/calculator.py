class Calculator:
    """
    A simple calculator with the basic arithmetic
    functions.
    """

    def add(self, first_num: float, second_num: float) -> float:
        """
        Adds two numbers together.

        Args:
            first_num: The first number to include.
            second_num: The second number to include.

        Returns:
            The sum of first_num and second_num.
        """
        return first_num + second_num

    def subtract(self, first_num: float, second_num: float) -> float:
        """
        Subtracts two numbers.

        Args:
            first_num: The first number.
            second_num: The number to subtract by.

        Returns:
            The difference between first_num and second_num.
        """
        return first_num - second_num

    def multiply(self, first_num: float, second_num: float) -> float:
        """
        Multiplies two numbers.

        Args:
            first_num: The first number to include.
            second_num: The second number to include.

        Returns:
            The product of first_num and second_num.
        """
        return first_num * second_num

    def divide(self, first_num: float, second_num: float) -> float:
        """
        Divides two numbers.

        Args:
            first_num: The dividend.
            second_num: The divisor.

        Returns:
            The quotient of first_num divided by second_num.
        """
        return first_num / second_num
