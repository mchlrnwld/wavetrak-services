from lib.calculator import Calculator


def test_calculator():
    first_num = 6.0
    second_num = 5.0
    calculator = Calculator()
    assert calculator.add(first_num, second_num) == 11.0
    assert calculator.multiply(first_num, second_num) == 30.0
    assert calculator.divide(first_num, second_num) == 1.20
    assert calculator.subtract(first_num, second_num) == 1.0
