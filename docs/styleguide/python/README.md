# Python Style Guide

## Summary

This guide gives a comprehensive look at writing, structuring, and testing Python code in our applications and scripts. In particular, it is based on [PEP 0008](https://www.python.org/dev/peps/pep-0008/) and the practice guide of [Google Python style guide](https://google.github.io/styleguide/pyguide.html).

More detailed information on programming recommendations, syntax, linting, formatting, and testing can be found in later sections of the guide.

In overall, _readability_ and _consistency_ are the keys for Python developers, in coding style.

## Table of Contents <!-- omit in toc -->

- [Summary](#summary)
- [Setup](#setup)
  - [Install Conda](#install-conda)
  - [Install devenv](#install-devenv)
  - [Install direnv (optional)](#install-direnv-optional)
- [Configuration](#configuration)
  - [The environment file](#the-environment-file)
  - [The requirements file](#the-requirements-file)
  - [The Makefile file](#the-makefile-file)
  - [The README file](#the-readme-file)
- [Development](#development)
  - [Creating your environment](#creating-your-environment)
  - [Writing your code](#writing-your-code)
  - [Running your code](#running-your-code)
- [Conventions](#conventions)
  - [Imports](#imports)
  - [Naming](#naming)
  - [Semicolons](#semicolons)
  - [Line Length](#line-length)
  - [Indentation](#indentation)
  - [Blank Lines](#blank-lines)
  - [Whitespace](#whitespace)
  - [Comments](#comments)
  - [Strings](#strings)
    - [F-strings](#f-strings)
  - [File Encoding](#file-encoding)
- [Programming Recommendations](#programming-recommendations)
- [Formatting](#formatting)
  - [black](#black)
  - [isort](#isort)
  - [autoflake](#autoflake)
- [Linting](#linting)
  - [flake8](#flake8)
  - [black](#black-1)
- [Type Hints and Type Checking](#type-hints-and-type-checking)
  - [Type Hints](#type-hints)
  - [Type Checking](#type-checking)
- [Testing](#testing)

## Setup

This section describes how to write and run your Python code within common standards we decided to adopt at Wavetrak.

### Install Conda

Install and configure an environment manager. [`conda`](https://docs.conda.io/en/latest/) is our preferred environment manager tool due to its easy access to data and science libraries.

- To install `conda` on Mac:

  ```sh
  brew cask install miniconda
  ```

  Please refer to the [installation guide on Mac](https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html) for more details.

- To install `conda` on Linux, you'll need to download one of [the installers in this page](https://docs.conda.io/en/latest/miniconda.html#linux-installers).

  Then execute the `bash` command with the corresponding downloaded file:

  ```sh
  bash Miniconda3-latest-Linux-x86_64.sh
  ```

  Please refer to the [installation guide on Linux](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html) for more details.

### Install devenv

With [`conda-devenv`](https://github.com/ESSS/conda-devenv) you can manage your project dependencies across Linux, Windows, and macOS operating systems using a single conda dependency file `environment.devenv.yml` instead of multiple `environment.yml` files.

So, not only software dependencies for your code, but also operative system packages needed on your computer for the application to run properly.

- After installing `conda`, run the next command to install `conda-devenv`:

  ```sh
  conda install -c conda-forge conda-devenv
  ```

  Please refer to the [environment.devenv.yml](./environment.devenv.yml) section for more details on setting up the conda environment with the base dependencies.

### Install direnv (optional)

With [`direnv`](https://direnv.net/) you can make use of existing shells with a new feature that can load and unload environment variables depending on the current directory.

- To seamlessly activate/deactivate conda environments on command line, install `direnv`:

  ```sh
  brew install direnv
  ```

- Then add the following to the end of your `~/.bashrc ` file:

  ```sh
  eval "$(direnv hook bash)"
  ```

  To setup for other shells, please refer to [direnv official documentation](https://direnv.net/docs/hook.html).

- For `direnv` to work, a `.envrc` file in the root directory needs to be included. A sample [.envrc](.envrc) file is included in this repo to automatically activate/deactivate the `sample-conda-env` environment on command line.

- If you prefer NOT to install `direnv`, you can avoid the use of this `.envrc` file and activate/deactivate your `conda` environment manually with the following commands:

  ```sh
  # Activate
  conda activate sample-conda-env

  # Deactivate
  conda deactivate
  ```

**[⬆ back to top](#summary)**

## Configuration

### The environment file

At Wavetrak, [environment.devenv.yml](environment.devenv.yml) are often created manually with only direct dependencies listed.

- We use [conda-devenv](https://conda-devenv.readthedocs.io) to manage conda environments in order to address limitations of [environment.yml](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file) such as OS-specific packages. The first part of an `environment.devenv.yml` consists of the following:

  ```yml
  name: sample-conda-env
  channels:
    - conda-forge
    - defaults
  ```

  - The `name` for the environment follows `kebab-case` conventions.
  - The `conda-forge` channel is preferred over the default because packages are better and more frequently maintained.

  Next, the base dependencies for a Wavetrak python project are listed:

  ```yml
  dependencies:
    - autoflake=1.3
    - black=19.3b0
    - boto3=1.9.71
    - flake8=3.7.9
    - isort=4.3.21
    - make=4.2.1
    - mypy=0.711
    - pytest=5.3.0
    - python=3.7.3
  ```

  Most of these base dependencies are destined for testing, linting and formatting purposes.

  If a package is not available through `conda`, you may need to install it from `pip` instead. If so, add the following line under `dependencies`:

  ```yml
  ...
  - pip:
    - pyaml==20.4.0
  ```

- You may also create `environment.devenv.yml` in the following manner (assuming the `conda` environment has **already** been created):

  ```sh
  conda env export --from-history > environment.devenv.yml
  ```

  Please note that `conda env export` exports **all** dependencies that are installed. This includes direct and indirect dependencies.

- To install and add a single dependency to `environment.devenv.yml`:

  ```sh
  conda install -c conda-forge pytest && conda env export | grep pytest >> environment.devenv.yml
  ```

  Please be aware that you may have to clean up the `environment.devenv.yml` after using the above command.

### The requirements file

If you're upgrading your project of adapting the code to a `conda` environment, you may find a `requirements.txt` file is present in your codebase.

- You can make use of this `requirements.txt` file combined with the `pip` package manager if present in `environment.devenv.yml`.

  ```yml
  ...
  - pip=21.2.4
  - pytest=6.2.4
  - python=3.9.5
  - pyyaml=5.4.1
  - pip:
    - -r requirements.txt
  ```

### The Makefile file

All Python projects should be accompanied with a `Makefile` file, containing a set of commands to help with the formatting, linting and type-check processes, that you can call by executing `make {command-name}`.

The purpose of this file is also avoid the use of extra configuration files, since most of these commands can be set to be executed with the proper arguments, getting the same results.

For example, the [`pyproject.toml`](https://python-poetry.org/docs/pyproject/) is a common configuration file for Python projects, but as applications gain complexity and more steps are needed to build or to run your code, we start to lose track of the changes in files like this one or get confused about where these configurations need to be present and which of the configuration files handles the setting we need.

- To let the Python extensions to scan and format our code, we only need this snippet on our `Makefile` file:

  ```makefile
  # Makefile (preferred)
  ...

  .PHONY: format
  format:
    @black --line-length 79 --skip-string-normalization .
    @isort --trailing-comma --multi-line=3 .
    @autoflake \
      --remove-all-unused-imports --remove-unused-variables \
      --ignore-init-module-imports --recursive --in-place .

  ...
  ```

  All of these commands are being executed calling the `format` target:

  ```sh
  make format
  ```

- If we want to accomplish the same results using configuration files for the `black`, `isort` and `autoflake` extensions, we'll need of the `pyproject.toml`, `.isort.cfg` and `setup.cfg` files respectively.

  ```ini
  # pyproject.toml (discouraged)
  [tool.black]
  line-length = 79
  skip-string-normalization = true
  ```

  ```ini
  # .isort.cfg (discouraged)
  [settings]
  atomic=True
  trailing_comma=True
  multi_line_output=3
  ```

  ```ini
  # setup.cfg (discouraged)
  [autoflake]
  remove_all_unused_imports=True
  remove_unused_variables=True
  ignore_init_module_imports=True
  recursive=True
  in_place=True
  ```

> You can modify or add all of the commands you need (targets) to this `Makefile` file.

### The README file

On any project, job or simple script, please add a markdown-formatted file named `README.md`.

- The basic content of this file should have a Title, a Summary a Table of Contents and the proper Content.

  ```md
  # Project title

  ## Summary

  This project works better when documented.

  ## Table of Contents <!-- omit in toc -->

  ─ [Summary](#a-summary)
  ─ [Quick start](#quick-start)
  ...

  ## Quick start

  The proper content of the project.
  ```

- The `Makefile` has a `doctoc` target that may help you updating the Table of Contents as long as you modify the sections of the file, recreating the ToC just calling:

  ```sh
  make doctoc
  ```

**[⬆ back to top](#summary)**

## Development

### Creating your environment

- To create the `conda` environment, run:

  ```sh
  conda devenv
  ```

- According to your selected configuration, activate it with the `conda` command or using `direnv`:

  ```sh
  conda activate sample-conda-env
  ```

### Writing your code

- To get more familiar with python code, check out the [sample module](main.py) included in this repo. Additionally, tutorials by [Real Python](https://realpython.com/python-first-steps/) are highly recommended.

The next section will explain how to run the [sample module](main.py) and other python code.

### Running your code

- To execute python code, simply run:

  ```sh
  python module_name.py
  ```

  Running this command on the [example module](main.py):

  ```sh
  python main.py
  ```

  Result:

  ```console
  Hello! My name is: Jeff.
  My favorite numbers are: 3.0 and 2.0. Added together they equal: 5.0.
  ```

- Python also has a feature called the `interpreter`, that can be accessed very simply by running:

  ```sh
  python
  ```

  Which will open a interactive interpreter that looks like this:

  ```sh
  Python 3.7.3 (default, Mar 27 2019, 16:54:48)
  [Clang 4.0.1 (tags/RELEASE_401/final)] :: Anaconda, Inc. on darwin
  Type "help", "copyright", "credits" or "license" for more information.
  >>>
  ```

  The interactive interpreter is a excellent way to quickly run and test code.

**[⬆ back to top](#summary)**

## Conventions

Here we hand a list of programming best practices for our Python code.

### Imports

- Imports should be on separate lines and are always put at the top of the file, just after any module comments and doc strings and before module globals and constants. Imports should be grouped in the order shown below, with a blank line between each group of imports:

  - standard library imports
  - related third-party imports
  - local application-specific imports

  `isort` is used to automatically sort and group imports by the order specified above. For more information, please refer to the [isort](#isort) sub-section of the style guide.

- Always use absolute imports, as they are usually more readable and tend to be better behaved (or at least give better error messages):

  ```python
  import wavetrak.weather
  from wavetrak.weather import wind
  ```

- Never use relative imports, as it is difficult to tell where they are being imported from:

  ```python
  import .weather
  from .weather import wind
  ```

- Avoid using wildcard imports:

  ```python
  from wavetrak.weather import *
  ```

- Within each grouping, imports should be sorted lexicographically, ignoring case, according to each module's full package path.

  ```python
  import foo
  from foo import bar
  from foo.bar import baz
  from foo.bar import Quux
  from Foob import ar
  ```

- In case your IDE is not directly linked with the python environment, it might complain about dependencies not found in your code. By adding `# type: ignore` at the end of the custom or third party package, it will validate the dependency.

  ```python
  import boto3  # type: ignore
  import botocore  # type: ignore
  ```

**[⬆ back to top](#summary)**

### Naming

- Overall Naming Convention:

  - Prepending a single underscore `(_)` has some support for protecting module variables and functions (not included with import \* from). In other words, it is to indicate to other programmers that the attribute or method is intended to be protected and should not be accessed publicly. This is similar to "protected member" in Java.
  - Prepending a double underscore `(__)` to an instance variable or method effectively serves to make the variable or method private to its class. It is to indicate that nobody should be able to access it from outside the class.
  - Place related classes and top-level functions together in a module.
  - Use CapWords for class names, but lower_with_under.py for module filenames.

- Naming Summary (guidelines from Guido's recommendations):

  | Type                       | Public             | Internal                                                             |
  | -------------------------- | ------------------ | -------------------------------------------------------------------- |
  | Packages                   | lower_with_under   |                                                                      |
  | Modules                    | lower_with_under   | \_lower_with_under                                                   |
  | Classes                    | CapWords           | \_CapWords                                                           |
  | Exceptions                 | CapWords           |                                                                      |
  | Functions                  | lower_with_under() | \_lower_with_under()                                                 |
  | Global/Class Constants     | CAPS_WITH_UNDER    | \_CAPS_WITH_UNDER                                                    |
  | Global/Class Variables     | lower_with_under   | \_lower_with_under                                                   |
  | Instance Variables         | lower_with_under   | \_lower_with_under (protected) or \_\_lower_with_under (private)     |
  | Method Names               | lower_with_under() | \_lower_with_under() (protected) or \_\_lower_with_under() (private) |
  | Function/Method Parameters | lower_with_under   |                                                                      |
  | Local Variables            | lower_with_under   |                                                                      |

  Note: "Internal" means internal to a module or protected or private within a class.

**[⬆ back to top](#summary)**

### Semicolons

- Do not terminate your lines with semicolons and do not use semicolons to put two or more commands on the same line. However, you may put the result of a test on the same line as the test only if the entire statement fits on one line.

**[⬆ back to top](#summary)**

### Line Length

- Maximum line length is 79 characters. This is enforced by [flake8](#flake8) and [black](#black).

- Exceptions:
  - Long import statements
  - URLs in comments

- Avoid using backslash line continuation. The preferred way of wrapping long lines is by using Python's implied line continuation inside parentheses, brackets and braces.

- Long lines can be broken over multiple lines by wrapping expressions in parentheses.

  ```python
  # Examples of implicit line join inside parentheses, brackets and braces.
  foo = function_name(var_one, var_two,
                      var_three, var_four)

  if (temperature == 78 and city == 'LA' and
      wind == 10):

  my_str = ('This will create a very long long '
            'long long long long long long long string')
  ```

**[⬆ back to top](#summary)**

### Indentation

- Use 4 spaces per indentation level. Never use tabs or mix tabs and spaces. In cases of implied line continuation, you should align wrapped elements either vertically, or using a hanging indent of 4 spaces, in which case there should be no argument on the first line.

  ```python
  # Aligned vertically with opening delimiter
  foo = function_name(var_one, var_two,
                      var_three, var_four)

  # Aligned with 4-space hanging indent; nothing on first line
  foo = function_name(
      var_one, var_two, var_three,
      var_four)
  ```

**[⬆ back to top](#summary)**

### Blank Lines

- Surround top-level function and class definitions with two blank lines.
  - Method definitions inside a class are surrounded by a single blank line.
  - Use blank lines in functions, sparingly, to indicate logical sections.

**[⬆ back to top](#summary)**

### Whitespace

- Follow standard typographic rules for the use of spaces.

  - No whitespace inside parentheses, brackets or braces.

    ```python
    # Good
    spam(ham[1], {eggs: 2}, [])

    # Bad
    spam( ham[ 1 ], { eggs: 2 }, [ ] )
    ```

  - No whitespace before a comma, semicolon, or colon.

    ```python
    # Good
    if x == 6:
        print x, y

    # Bad
    if x == 6:
        print x , y
    ```

  - No whitespace before the open parent/bracket that starts an argument list, indexing or slicing.

    ```python
    # Good
    spam(1)
    dict['key'] = list[2]

    # Bad
    spam (1)
    dict['key'] = list [2]
    ```

  - Surround binary operators with a single space on either side for assignment (=), comparisons (==, <, >, !=, <>, <=, >=, in not in, is, is not), and Booleans (and, or, not).

    ```python
    # Good
    x == 1

    # Bad
    x==1
    ```

  - Don't use spaces around the '=' sign when used to indicate a keyword argument or a default parameter value.

    ```python
    # Good
    def func_name(real, imag=0.0): return another_func(r=real, i=imag)

    # Bad
    def func_name(real, imag = 0.0): return another_func(r = real, i = imag)
    ```

  - Avoid trailing whitespace anywhere.

**[⬆ back to top](#summary)**

### Comments

Comments should be complete sentences. If a comment is a phrase or sentence, its first word should be capitalized, unless it is an identifier that begins with a lower case letter. If a comment is short, the period at the end can be omitted. Block comments generally consist of one or more paragraphs built out of complete sentences, and each sentence should end in a period.

- Inline Comments: An inline comment is a comment on the same line as a statement. Inline comments should be separated by at least two spaces from the statement. They should start with a # and a single space.

- Block Comments: Block comments generally apply to some (or all) code that follows them, and are indented to the same level as that code. Each line of a block comment starts with a # and a single space (unless it is indented text inside the comment). Paragraphs inside a block comment are separated by a line containing a single # .

- Documentation Strings (a.k.a. "docstrings"): Write docstrings for all public modules, functions, classes, and methods. Docstrings are not necessary for non-public methods, but you should have a comment that describes what the method does. This comment should appear after the def line. Use the three double-quote """ format for doc strings.

  ```python
  # Example for functions and methods
  def fetch_db_rows(table_name, keys):
      """
      Fetches rows from a table.

      Retrieves rows pertaining to the given keys from the table instance

      Args:
          table_name: A table instance.
          keys: A string representing the key of each table row to fetch.

      Returns:
          A dict mapping keys to the corresponding table row data
          fetched. Each row is represented as a tuple of strings. For
          example:

          {'temperature': ('celsius', 'fahrenheit'),
          'wind': ('km', 'mile')}

          If a key from the keys argument is missing from the dictionary,
          then that row was not found in the table.

      Raises:
          IOError: An error occurred accessing the table object.
      """
      pass
  ```

  ```python
  # Example for classes
  class Weather(object):
      """
      Summary of Weather class here.

      Additional class information....
      Additional class information....

      Attributes:
          temperature: A numeric value indicating current weather temperature.
      """

      def __init__(self, temperature):
          """Inits Weather with blah."""
          self.temperature = temperature

      def public_method_get_temperature(self):
          """Performs operation blah."""
  ```

- For more information on docstrings, please refer to the [Google style guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings).

- TODO Comments: Use TODO comments for code that is temporary, a short-term, good-enough, but not perfect solution. TODOs should include the string TODO in all caps, followed by the e-mail address (in parentheses), or other identifier of the person who can provide context about the problem referenced by the TODO. A comment explaining what there is to do is required. The main TODO purpose is to have a consistent format that can be searched to find the person who can provide more details upon request. Note a TODO is not a commitment that the person referenced will fix the problem.

  ```python
  # TODO(xyz@wavetrak.com): Improve this algorithm using advanced sorting approach.
  ```

**[⬆ back to top](#summary)**

### Strings

- Single-quoted strings and double-quoted strings are the same. PEP 8 does not make a recommendation for this. However, in general, you should apply the following rules:

  - Use double quotes for docstrings only. The exception to this rule is that if the string itself contains single quotes, then use double quotes.
  - Use single quotes for everything else.

- When a string contains single or double quote characters, use the other one to avoid backslashes in the string. It improves readability.

- In Python 2, use the format method or the `%` operator for formatting strings. Use your best judgement to decide between `+` and `%` (or format). In particular, `+` should only be used for combining string variables, not for formatting strings.

  ```python
  # Good
  x = a + b
  x = '%s, %s!' % (message1, message2)
  x = '{}, {}!'.format(message1, message2)
  x = 'name: %s; score: %d' % (name, n)
  x = 'name: {}; score: {}'.format(name, n)

  # Bad
  x = 'name: ' + name + '; score: ' + str(n)
  ```

#### F-strings

- In Python 3, always use `f-strings` for string interpolation. `f-strings` are simple to use, and can contain almost any python expression:

  Example with variable:

  ```python
  name = 'dude'
  n = 2
  x = f'name: {name}; score: {n}'
  ```

  Result:

  ```python
  'name: dude score: 2'
  ```

  Example with expression:

  ```python
  import math

  x = f'2 squared is: {math.pow(2,2)}'
  ```

  Result:

  ```python
  '2 squared is: 4'
  ```

  A multiline `f-string` can be written like so:

  ```python
  x = (
      f'Hey there dude! '
      f'You been surfin lately?'
  )
  ```

  Result:

  ```python
  'Hey there dude! You been surfin lately?'
  ```

  An important thing to note that is that newline characters are **not** appended to each line.

  For more information, check out this excellent article: [f-strings](https://realpython.com/python-f-strings/)

### File Encoding

- Code in the core Python distribution should always use UTF-8.

  ```python
  # In source header you can declare encoding as below.
  # -*- coding: utf-8 -*-
  ```

**[⬆ back to top](#summary)**

## Programming Recommendations

The following recommendations includes a subset from [PEP 0008](https://www.python.org/dev/peps/pep-0008/#programming-recommendations) and [Google Python style guide](https://google.github.io/styleguide/pyguide.html#Python_Language_Rules).

- Comparisons to singletons like `None` should always be done with `is` or `is not` , never the equality operators.
- Use `is not` operator rather than `not ... is` .
- Always use a `def` statement instead of an assignment statement that binds a `lambda` expression directly to an identifier.
- Derive exceptions from `Exception` rather than `BaseException`.
- Use exception chaining appropriately.
- When raising an exception in Python 2, use `raise ValueError('message')` instead of the older form `raise ValueError, 'message'` .
- When catching exceptions, mention specific exceptions whenever possible instead of using a bare `except:` clause.
- When a resource is local to a particular section of code, use a `with` statement to ensure it is cleaned up promptly and reliably after use. A `try/finally` statement is also acceptable.
- Use `''.startswith()` and `''.endswith()` instead of string slicing to check for prefixes or suffixes.
- Don't write string literals that rely on significant trailing whitespace.
- Object type comparisons should always use isinstance() instead of comparing types directly.

  ```python
  # Good
  if isinstance(obj, int):

  # Bad
  if type(obj) is type(1):
  ```

- Don't compare boolean values to True or False using == .

  ```python
  # Good
  if greeting:

  # Bad
  if greeting == True:
  ```

- Avoid global variables.

- List and dictionary comprehensions are preferred over `for/while` loops wherever possible:

  List comprehension:

  ```python
  nums = [2,4,6,8]
  str_nums = [str(num) for num in nums]
  ```

  Dictionary comprehension:

  ```python
  prices = [1.0, 2.0, 3.0]
  vegetables = ['tomato', 'broccoli', 'eggplant']
  veg_prices = {veg: price for veg, price in zip(vegetables, prices)}
  ```

**[⬆ back to top](#summary)**

## Formatting

This section explain how `black`, `isort` and `autoflake` can be configured to work with your code, but you can check the formatting of your project by calling the `make` command:

```makefile
# Makefile
...

.PHONY: format
format:
  @black --line-length 79 --skip-string-normalization .
  @isort --trailing-comma --multi-line=3 .
  @autoflake \
    --remove-all-unused-imports --remove-unused-variables \
    --ignore-init-module-imports --recursive --in-place .

...
```

To run all the formatting commands you just execute:

```sh
make format
```

### black

- `black` is used for all python modules to format the code in a consistent manner. To start:

  Install with `conda`:

  ```sh
  conda install -c conda-forge black
  ```

  Add to `environment.devenv.yml`:

  ```sh
  conda env export | grep black >> environment.devenv.yml
  ```

  Black can then be used to format an python module or package by running the command:

  ```sh
  black {source_file_or_directory}
  ```

- An important thing to note about `black` is that it does not follow `PEP8` conventions strictly, but rather is a superset of `PEP8`.

  It deviates from certain `PEP8` conventions, such as preferred a line length of `88` rather than the standard `79`.

  > While `black` contains minimal configurations in an effort to maintain simplicity, an optional `pyproject.toml` file can be added to keep more in line with `PEP8` conventions, but it's encouraged to make use and update the `format` target in `Makefile` at convenience:

    ```makefile
    # Makefile (preferred)
    ...

    .PHONY: format
    format:
      ...
      @black --line-length 79 --skip-string-normalization  .
      ...

    ...
    ```

    ```ini
    # pyproject.toml (discouraged)
    [tool.black]
    line-length = 79
    skip-string-normalization = true
    ```

- See `black` documentation for more information: [Black Documentation](https://black.readthedocs.io/en/stable/index.html)

### isort

- `isort` is used to sort and group imports. To start:

  Install with `conda`:

  ```sh
  conda install -c conda-forge isort
  ```

  Add to `environment.devenv.yml`:

  ```sh
  conda env export | grep isort >> environment.devenv.yml
  ```

- By default, `isort` will automatically sort and group imports in the following order:

  1. Standard library imports
  2. Third-party imports
  3. Local application-specific imports

  > While an optional `.isort.cfg` config file can be added to the project, it's encouraged to make use and update the `format` target in `Makefile` at convenience.

    ```makefile
    # Makefile (preferred)
    ...

    .PHONY: format
    format:
      ...
      @isort --atomic --trailing-comma --multi-line=3 .
      ...

    ...
    ```

    ```ini
    # .isort.cfg (discouraged)
    [settings]
    atomic=True
    trailing_comma=True
    multi_line_output=3
    ```

  The parameters used are explained below:

  - `atomic=True` - isort will only change a file in place if the resulting file has correct Python syntax. This also requires that the `Python` version of the code being sorted must be the same `Python` version as `isort`.

  - `trailing_comma` - Includes a trailing comma on multi line imports that include parentheses.

  - `multi_line_output=3` - An integer that represents how you want imports to be displayed if they're long enough to span multiple lines. `3` represents vertical hanging indent, and more options can be seen on the github repo [here](https://github.com/timothycrosley/isort#multi-line-output-modes).

    An example of `3 - Vertical Hanging Indent` is shown below:

    ```py
    from third_party import (
        lib1,
        lib2,
        lib3,
        lib4,
    )
    ```

- For further information, please refer to the `isort` [github repository](https://github.com/timothycrosley/isort).

### autoflake

- `autoflake` automatically removes unused imports and unused variables. To start:

  Install with `conda`:

  ```sh
  conda install -c conda-forge autoflake
  ```

  Add to `environment.devenv.yml`:

  ```sh
  conda env export | grep autoflake >> environment.devenv.yml
  ```

- `autoflake` should be used with the following flags, already configured in `Makefile`:

  ```sh
  autoflake \
    --remove-all-unused-imports --remove-unused-variables \
    --ignore-init-module-imports --recursive --in-place .
  ```

- **IMPORTANT:** In an automated CI/CD pipeline, if an unused variable contains an expression, the variable assignment will be removed but the expression will remain. The remaining expression will have to be manually removed.

  Example:

  Before `autoflake`:

  ```py
  def main():
      num = 1 * 2 # Unused variable with expression.

  if __name__ == "__main__":
      main()
  ```

  After `autoflake`:

  ```py
  def main():
      1 * 2 # Resulting expression.

  if __name__ == "__main__":
      main()
  ```

- For further information, please refer to the [autoflake official documentation](https://github.com/myint/autoflake)

**[⬆ back to top](#summary)**

## Linting

This section explain how `flake8` and `black` can be configured to work with your code, but you can check the linting of your project by calling the `make` command:

```makefile
# Makefile
...

.PHONY: lint
lint:
  @flake8 .
  @black --check --diff --line-length 79 --skip-string-normalization  .

...
```

To run all the linting commands you just execute:

```sh
make lint
```

### flake8

- At Wavetrak, we use `flake8` to provide linting. `flake8` checks for syntax errors, unused variables/imports, and adherence to `PEP 0008` conventions. To start:

  Install with `conda`:

  ```sh
  conda install -c conda-forge flake8
  ```

  Add to `environment.devenv.yml`:

  ```sh
  conda env export | grep flake8 >> environment.devenv.yml
  ```

- `flake8` is run during the `docker build` phase of our CI/CD configuration. From the example [Dockerfile](Dockerfile) included in this repo:

  ```Docker
  RUN source activate sample-conda-env && \
      make lint && \
      make type-check && \
      make test
  ```

  > While an optional `.flake8` config file can be added to the project, it's encouraged to make use and update the `lint` target in `Makefile` at convenience, or to set specific rules or exceptions as `noqa` in the code itself:

    ```python
    # project_file.py (preferred)
    import os
    import sys
    from helpers import (  # noqa E402
        get_app_name,
        get_file_contents,
        put_file_contents,
        set_output,
    )
    ```

    ```ini
    # .flake8 (discouraged)
    [flake8]
    ignore = E402
    ```

  Read more about [the flake8 rules](https://www.flake8rules.com/).

- For further information on `flake8`, please refer to the [official documentation](http://flake8.pycqa.org/en/latest/index.html#).

### black

- In addition to formatting, `black` is also used for linting. To do so, run `black` with the `--check` flag to check code that hasn't been formatted yet.

- `black --check` is run during the `docker build` phase of our CI/CD configuration. From the example [Dockerfile](Dockerfile) included in this repo:

  ```Docker
  RUN source activate sample-conda-env && \
      make lint && \
      make type-check && \
      make test
  ```

  > While `black` contains minimal configurations in an effort to maintain simplicity, an optional `pyproject.toml` file can be added to keep more in line with `PEP8` conventions, but it's encouraged to make use and update the `lint` target in `Makefile` at convenience:

    ```makefile
    # Makefile (preferred)
    ...

    .PHONY: lint
    lint:
      ...
      @black --check --diff --line-length 79 --skip-string-normalization  .

    ...
    ```

    ```ini
    # pyproject.toml (discouraged)
    [tool.black]
    line-length = 79
    skip-string-normalization = true
    ```

- See `black` documentation for more information: [Black Documentation](https://black.readthedocs.io/en/stable/index.html)

**[⬆ back to top](#summary)**

## Type Hints and Type Checking

### Type Hints

- While Python at its core is a dynamic, interpreted language, there are many benefits to be gained by type checking. This can be achieved, in part, by using Python `type hints`, which were introduced in Python `3.6`. In practice, type hints are used in the following manner:

  ```python
  def greeting(name: str) -> str:
      return f'Hello {name}'
  ```

  Type hints support nearly all types, including `composite` types such as `list, dict, tuple, set`. When using type hints for `composite` types, there are 2 different options available. The first option is shown below:

  ```python
  def greeting(names: dict) -> dict:
      return {name: float(age) for name, age in names.items()}
  ```

  In the example above, we can see that a dictionary type is being passed in, and then a dictionary comprehension is being performed. The `age` value is being cast to type `float`, but the original type of the `age` value is unknown.

  For `composite` types such as `list, dict, tuple, set,`, there are special types that can be imported from the `typing` library that removes this uncertainty. An example of this is shown below:

  ```python
  from typing import Dict

  def greeting(names: Dict[str, int]) -> Dict[str, float]:
      return {name:float(age) for name,age in names.items()}
  ```

  Now it can be seen that the `names` dictionary being passed in is of type `[str, int]`, then a dictionary comprehension is being performed where the `int` is being cast to `float`.

  The returned dictionary is then of type `[str, float]`.

  More examples for the special types are shown below:

  ```python
  from typing import List

  def greeting(names: List[str]) -> List[str]:
      return [f'Hello {name}' for name in names]
  ```

  ```python
  from typing import Set

  def greeting(names: Set[str]) -> List[str]:
      return [name for name in names]
  ```

  ```python
  from typing import Tuple

  def greeting(person: Tuple[str, int]) -> str:
      name, age = person
      return f'Hello {name} your age is: {age}'
  ```

  When things get difficult, for example with complex Dictionary types, you can opt to use the `mypy_extensions` library.

  This is an additional library that must be installed via pip:

  ```sh
  pip install mypy-extensions
  ```

  Which then gives access to types such as `TypedDict`. An example of this is shown below:

  ```python
  PointOfInterestDict = TypedDict(
    'PointOfInterestDict',
    {
        'pointOfInterestId': str,
        'agency': str,
        'model': str,
        'grid': str,
        'lat': Union[int, float],
        'lon': Union[int, float],
    },
  )
  ```

  A deeper look into all the types offered by the typing library can be seen here: [Python Type Hints](https://docs.python.org/3/library/typing.html)

### Type Checking

- Type hints in Python are an excellent way to create code that is more readable and maintainable. However, since Python is a dynamic, interpreted language, the actual type hints by themselves are nothing more than annotations. In the example below, the type hint is wrong but Python will not complain when the code is executed:

  ```python
  def greeting(name: str) -> int:
      return f'Hello {name}'
  ```

  `mypy` is a static type checking tool that checks the type hints in Python code and determines whether or
  not they are valid. To start:

  Install with `conda`:

  ```sh
  conda install -c conda-forge mypy
  ```

  Add to `environment.devenv.yml`:

  ```sh
  conda env export | grep mypy >> environment.devenv.yml
  ```

  Running `mypy` on the example function above will result in the following error:

  ```console
  2: error: Incompatible return value type (got "str", expected "int")
  Found 1 error in 1 file (checked 1 source file)
  ```

- If a function that does **not** have type hints, for example a `main()` function, then calls a function **with** type hints,
  then the function **without** type hints (`main()`) will not be checked. An example of this is shown below:

  ```python
  def greeting(name: str) -> str:
      return f'Hello {name}'

  def main():
      greeting(1)

  main()
  ```

  The result of running `mypy` will be:

  ```console
  Success: no issues found in 1 source file
  ```

  As can be seen, `mypy` did not catch the fact that `main()` is passing an `int` to the `greeting(name: str)` function, when it should actually be passing in a `str`.

  So, instead of running `mypy`, you can execute `make type-check`, that it will show this error:

  ```console
  error: Argument 1 to "greeting" has incompatible type "int"; expected "str"
  Found 1 error in 1 file (checked 1 source file
  ```

  Which is the expected result. When using `mypy`, always include the `--check-untyped-defs` flag.

  > While a config file `mypy.ini` might be added to the project folder with the necessary configurations, it's encouraged to avoid any extra configuration file in our repos that can be substituted by improved command line parameters from the `Makefile` file.

    ```makefile
    # Makefile (preferred)
    ...

    .PHONY: type-check
    type-check:
      @mypy . --check-untyped-defs
    ...

    ```

    ```ini
    # mypy.ini (discouraged)
    [mypy]
    check_untyped_defs = True
    ```

- `mypy` is run during the `docker build` phase of our CI/CD configuration. From the example [Dockerfile](Dockerfile) included in this repo:

  ```Docker
  RUN source activate sample-conda-env && \
      make lint && \
      make type-check && \
      make test
  ```

- A more in-depth look at `mypy` can be seen in the official documentation: [mypy Documentation](https://mypy.readthedocs.io/en/stable/introduction.html)

**[⬆ back to top](#summary)**

## Testing

At Wavetrak, we use the 3rd party library `pytest` for all unit testing.

- To get started with `pytest`:

  Install with `conda`:

  ```sh
  conda install -c conda-forge pytest
  ```

  Add to `environment.devenv.yml`:

  ```sh
  conda env export | grep pytest >> environment.devenv.yml
  ```

- You can check for [example configurations](https://docs.pytest.org/en/6.2.x/example/simple.html) to improve your tests.

  > While a config file `pytest.ini` can be added to the project with specific `pytest` parameters, it's encouraged to make use and updated the `test` target configured in the `Makefile` file.

    ```makefile
    # Makefile (preferred)
    ...

    .PHONY: test
    test:
      @pytest . -vv -p no:warnings

    ...
    ```

    ```ini
    # pytest.ini (discouraged)
    [pytest]
    addopts = -vv -p no:warnings
    ```

- `pytest` is run during the `docker build` phase of our CI/CD configuration. From the example [Dockerfile](Dockerfile) included in this repo:

  ```Docker
  RUN source activate sample-conda-env && \
      make lint && \
      make type-check && \
      make test
  ```

  > You should not include the `lint`, `type-check` and `test` commands in the Dockerfile since they will be called from the CI/CD Pipeline when deploying using Wavetrak's GitHub Actions, but we leave the reference in this document in case you want to test locally.

- Please refer to the [pytest official documentation](https://docs.pytest.org/en/latest/contents.html) to get started writing and running unit tests. A sample unit test file [lib/test_calculator.py](lib/test_calculator.py) is included in this repo.

**[⬆ back to top](#summary)**
