# Wavetrak Coding Style Guides

## Introduction

The following guides show the standardized styles and conventions that various teams and projects adhere to at Wavetrak.

## Languages

- [Bash](./bash/README.md)
- [Javascript](./javascript/README.md)
- [Markdown](./markdown/README.md)
- [Python](./python/README.md)
- [Swift](./swift/README.markdown)
- [Terraform](./terraform/README.md)
