# Bash Style Guide

## Introduction
Bash is a shell scripting language used at Surfline. This style guide is a list of dos and
don'ts for Bash shell scripts. It is based on the following:

- [Google Shell Scripting Style Guide](https://google.github.io/styleguide/shell.xml)
- [Bash Style Guide](https://github.com/bahamas10/bash-style-guide)
- [Bash Reference Manual](https://www.gnu.org/software/bash/manual/bashref.html)
- [The Bash Hacker's Wiki](http://wiki.bash-hackers.org/doku.php)
- [Bash Pitfalls](http://mywiki.wooledge.org/BashPitfalls)
- [Defensive Bash Programming](http://www.kfirlavi.com/blog/2012/11/14/defensive-bash-programming/)
- [Writing Robust Bash Scripts](http://www.davidpashley.com/articles/writing-robust-shell-scripts/)

Overall, *readability* and *consistency* are the keys for Bash developers, in coding style.

## Table of Contents

1. [Which Shell To Use](#which-shell-to-use)
1. [When to use Shell](#when-to-use-shell)
1. [File Extensions](#file-extensions)
1. [STDOUT vs STDERR](#stdout-vs-stderr)
1. [File Header](#file-header)
1. [Script Flags](#script-flags)
1. [Comments](#comments)
1. [Indentation](#indentation)
1. [Line Length](#line-length)
1. [Pipelines](#pipelines)
1. [Loops](#loops)
1. [Case Statement](#case-statement)
1. [Variable Expansion](#variable-expansion)
1. [Quoting](#quoting)
1. [Sequences](#sequences)
1. [Command Substitution](#command-substitution)
1. [Conditional Testing](#conditional-testing)
1. [Wildcard Expansion of Filenames](#wildcard-expansion-of-filenames)
1. [Naming](#naming)
1. [Functions](#functions)
1. [Return Values](#return-values)
1. [Tools](#tools)

## Which Shell To Use

- `Bash` is the only shell scripting language permitted for executables.
- Executables must start with `#!/bin/bash` and a minimum number of flags.

**[⬆ back to top](#table-of-contents)**

## When to use Shell

- Shell should only be used for small utilities or simple wrapper scripts

**[⬆ back to top](#table-of-contents)**

## File Extensions

- Executables that would be deployed to `/usr/local/bin` on a system should have no extension.
- All other scripts, executable or otherwise, should have a `.sh` extension.
- Libraries must have a `.sh` extension and should not be executable.

**[⬆ back to top](#table-of-contents)**

## STDOUT vs STDERR

- All error messages should go to STDERR. This makes it easier to separate normal status from actual issues.
- A function to print out error messages along with other status information is recommended.

    ```bash
    err() {
      echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
    }

    if ! do_something; then
      err "Unable to do_something"
      exit "${E_DID_NOTHING}"
    fi
    ```

**[⬆ back to top](#table-of-contents)**

## File Header

- Start each file with a description of its contents.

    ```bash
    #!/bin/bash
    #
    # Perform hot backups of Oracle databases.
    ```

**[⬆ back to top](#table-of-contents)**

## Script Flags

Beneath the file header, use the following flags to set bash to "strict mode".

```
set -euxo pipefail
```

This helps avoid silent bugs and increases verbosity. The respective flags do the following:

- `-e`: Instructs bash to exit immediately if any command returns a non-zero exit code.
- `-u`: Throws error upon reference to undefined variables (except for `$*` and `$@`).
- `-x`: Prints commands and comments to `stdout` prior to execution.
- `-o pipefail`: Causes pipeline to return the exit status of the last non-zero return value. (Helps prevent errors in piped commands from being masked.)

## Comments

- Any function that is not both obvious and short must be commented. Any function in a library must be commented regardless of length or complexity.
- Comment tricky, non-obvious, interesting or important parts of your code.
- Use `TODO` comments for code that is temporary, a short-term solution, or good-enough but not perfect.


**[⬆ back to top](#table-of-contents)**

## Indentation

- Indent 2 spaces. No tabs.
- For existing files, stay faithful to the existing indentation.

**[⬆ back to top](#table-of-contents)**

## Line Length

- Maximum line length is 80 characters.
- If you have to write strings that are longer than 80 characters, this should be done with a here document or an embedded newline if possible.
- Literal strings that have to be longer than 80 chars and can't sensibly be split are ok, but it's strongly preferred to find a way to make it shorter.

    ```bash
    # DO use 'here document's
    cat <<END;
    I am an exceptionally long
    string.
    END

    # Embedded newlines are ok too
    long_string="I am an exceptionally
      long string."
    ```

**[⬆ back to top](#table-of-contents)**

## Pipelines

- Pipelines should be split one per line if they don't all fit on one line.
- If a pipeline all fits on one line, it should be on one line.

    ```bash
    # All fits on one line
    command1 | command2

    # Long commands
    command1 \
      | command2 \
      | command3 \
      | command4
    ```

**[⬆ back to top](#table-of-contents)**

## Loops

- Put `; do` and `; then` on the same line as the `while`, `for` or `if`.
- `else` should be on its own line
- Closing statements should be on their own line vertically aligned with the opening statement.

    ```bash
    for dir in ${dirs_to_cleanup}; do
      if [[ -d "${dir}/${ORACLE_SID}" ]]; then
        log_date "Cleaning up old files in ${dir}/${ORACLE_SID}"
        rm "${dir}/${ORACLE_SID}/"*
        if [[ "$?" -ne 0 ]]; then
          error_message
        fi
      else
        mkdir -p "${dir}/${ORACLE_SID}"
        if [[ "$?" -ne 0 ]]; then
          error_message
        fi
      fi
    done
    ```

**[⬆ back to top](#table-of-contents)**

## Case Statement

- Indent alternatives by 2 spaces.
- A one-line alternative needs a space after the close parenthesis of the pattern and before the `;;`.
- Long or multi-command alternatives should be split over multiple lines with the pattern, actions, and `;;` on separate lines.

    ```bash
    case "${expression}" in
      a)
        variable="..."
        some_command "${variable}" "${other_expr}" ...
        ;;
      absolute)
        actions="relative"
        another_command "${actions}" "${other_expr}" ...
        ;;
      *)
        error "Unexpected expression '${expression}'"
        ;;
    esac
    ```

- Simple commands may be put on the same line as the pattern and ;; as long as the expression remains readable.

    ```bash
    verbose='false'
    aflag=''
    bflag=''
    files=''
    while getopts 'abf:v' flag; do
      case "${flag}" in
        a) aflag='true' ;;
        b) bflag='true' ;;
        f) files="${OPTARG}" ;;
        v) verbose='true' ;;
        *) error "Unexpected option ${flag}" ;;
      esac
    done
    ```

**[⬆ back to top](#table-of-contents)**

## Variable Expansion

- Stay consistent with what you find
- Quote your variables
- Prefer `"${var}"` over `"$var"`, but see details.

    ```bash
    # Preferred style for 'special' variables:
    echo "Positional: $1" "$5" "$3"
    echo "Specials: !=$!, -=$-, _=$_. ?=$?, #=$# *=$* @=$@ \$=$$ ..."

    # Braces necessary:
    echo "many parameters: ${10}"

    # Braces avoiding confusion:
    # Output is "a0b0c0"
    set -- a b c
    echo "${1}0${2}0${3}0"

    # Preferred style for other variables:
    echo "PATH=${PATH}, PWD=${PWD}, mine=${some_var}"
    while read f; do
      echo "file=${f}"
    done < <(ls -l /tmp)
    ```

**[⬆ back to top](#table-of-contents)**

## Quoting

- Always quote strings containing variables, command substitutions, spaces or shell meta characters, unless careful unquoted expansion is required.
- Prefer quoting strings that are "words" (as opposed to command options or path names).
- 'Single' quotes indicate that no substitution is desired.
- "Double" quotes indicate that substitution is required/tolerated.

**[⬆ back to top](#table-of-contents)**

## Sequences

- Use bash builtins for generating sequences.

	```bash
    # GOOD
    for f in {1..5}; do
      ...
    done
    
    # GOOD
    for ((i = 0; i < n; i++)); do
      ...
    done
    
    # BAD
    for f in $(seq 1 5); do
      ...
    done
    ```

**[⬆ back to top](#table-of-contents)**

## Command Substitution

- Use `$(command)` instead of backticks.

    ```bash
    # GOOD
    var="$(command "$(command1)")"

    # BAD
    var="`command \`command1\``"
    ```

**[⬆ back to top](#table-of-contents)**

## Conditional Testing

- `[[ ... ]]` is preferred over `[`, `test` and `/usr/bin/[`.

    ```bash
    # GOOD
    if [[ "filename" == "f*" ]]; then
      echo "Match"
    fi

    # BAD
    if [ "filename" == f* ]; then
      echo "Match"
    fi
    ```

- Use quotes rather than filler characters where possible.
- To avoid confusion about what you're testing for, explicitly use `-z` or `-n`.

    ```bash
    # GOOD
    if [[ "${my_var}" = "some_string" ]]; then
      do_something
    fi

    # GOOD
    # -z (string length is zero) and -n (string length is not zero) are
    # preferred over testing for an empty string
    if [[ -z "${my_var}" ]]; then
      do_something
    fi

    # OKAY
    # This is OK (ensure quotes on the empty side), but not preferred:
    if [[ "${my_var}" = "" ]]; then
      do_something
    fi

    # BAD
    if [[ "${my_var}X" = "some_stringX" ]]; then
      do_something
    fi
    ```

**[⬆ back to top](#table-of-contents)**

## Wildcard Expansion of Filenames

- Use an explicit path when doing wildcard expansion of filenames.
- Test variables before using them in bulk `rm`

    ```bash
    # The variable WORKING_DIR is a directory, e.g., /ocean
    if [[ ! -z "${WORKING_DIR}" ]]; then
      rm -rf ${WORKING_DIR}/*
    fi    
    ```

**[⬆ back to top](#table-of-contents)**

## Naming

- Variable and function names should be lowercase with underscores to separate words (aka snake-case)

    ```bash
    # GOOD
    my_func() {
      ...
    }

    # GOOD
    name = "foo"

    # BAD
    myFunc() {
      ...
    }
    ```

- Libraries should use `::` to separate the name of the package

    ```bash
    # GOOD
    mypackage::my_func() {
      ...
    }
    ```

- Variables for loops should be similarly named to the variable you're looping through

    ```bash
    for zone in ${zones}; do
      something_with "${zone}"
    done
    ```

- Constants should be all caps, separated with underscores, declared at the top of the file.
- Anything exported to the environment should be capitalized.

    ```bash
    # Constant
    readonly PATH_TO_FILES='/some/path'

    # Both constant and environment
    declare -xr ORACLE_SID='PROD'
    ```

- Filenames should be lowercase with underscores to separate words if desired

    ```bash
    # GOOD
    maketemplate
    make_template

    # BAD
    make-template
    ```

- Use `readonly` or `declare -r` for read-only variables

- Declare function-specific variables with `local`. Declaration and assignment should be on different lines.

    ```bash
    my_func2() {
      # Separate lines for declaration and assignment:
      local my_var
      my_var="$(my_func)" || return
    }
    ```

**[⬆ back to top](#table-of-contents)**

## Functions

- Put all functions together in the file just below constants.
- Don't hide executable code between functions.
- A function called main is required for scripts long enough to contain at least one other function.
- In order to easily find the start of the program, put the main program in a function called `main` as the bottom-most function.
- The last non-comment line in the file should be a call to main: `main "$@"`
- Obviously, for short scripts where it's just a linear flow, `main` is overkill and is not required.

**[⬆ back to top](#table-of-contents)**

## Return Values

- Always check return values and give informative return values.
- Summary of our customized return codes (Note: avoid to conflict with [system exit codes](http://tldp.org/LDP/abs/html/exitcodes.html))
  
	Return value/code|Meaning
	---| ---
	20| Data download hung
	21| Script is running
	22| No available data to run
	23| Has already been completed for the latest data
	24| Full set of files not found
        25| makesurf.sh.FIXED is empty
        50| High resolution (small) dataset
	51| No available glz model to run

**[⬆ back to top](#table-of-contents)**

## Tools

- [bashbeautify](https://github.com/hermanbergwerf/bashbeautify): Help to "beautify" the existing bash scripts, especially to the indentation (note it's not fully compilant to this guideline).
- [shellcheck](https://github.com/koalaman/shellcheck): Help to check your bash scripts (note it's not fully compilant to this guideline).

**[⬆ back to top](#table-of-contents)**

