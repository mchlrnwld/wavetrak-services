---
title: Surfline API Reference
---

# Introduction

This document describes Surfline APIs. The APIs are modeled around REST, should be requested over SSL and return JSON.

# Making Requests

All documented endpoints can be access from the following base URLs.

Environment | Endpoint
----------- | --------
Production | [https://services.surfline.com/](https://services.surfline.com/)
Staging | [https://services.staging.surfline.com/](https://services.staging.surfline.com/)

# Conventions

## Headers

Header | Description
------ | -----------
`X-Auth-ClientId` | Used to identify the client making requests. This value is provided by Surfline, and should be sent with all requests.

## URL Parameters

Query Param | Description
----------- | -----------
`access_token` | An access token to authenticate the request with. See [Authentication](#authentication).

## Timestamps

Dates and times are represented as a unix timestamp. Time is always in UTC and a timezone offsets will be provided for the spot being requested.

## Units

> Sample `associated.units` object.

```json
"associated": {
    "units": {
        "temperature": "F",
        "tideHeight": "FT",
        "waveHeight": "FT",
        "windSpeed": "KTS"
    }
}
```


Units are returned in either imperial or metric. Users with registered accounts may change units via API settings while anonymous requests have units geolocated based the request's IP address. Most requests will return an `associated.units` object detailing units returned in the request.


## Errors

> Error Response

```json
{
    "message": "Unauthorized"
}
```

Requests containing errors will return a `4xx` or `5xx` status code with a `message` property describing the error.


# Authentication

Surfline implements two-legged OAuth. A username and password can be exchanged for an access and refresh token.

## Obtaining an Access Token

**`POST /auth/token`**

A user name and password can be posted to `/auth/token` in exchange for an access and refresh token. The access token should be sent along with all subsequent requests using the `access_token` query param. See [URL Parameters](#url-parameters) for more information.

> Sample authorization request

```http
POST /auth/token HTTP/1.1
Host: services.surfline.com
Content-Type: application/x-www-form-urlencoded
Cache-Control: no-cache

grant_type=password&username=user@example.com&password=password123
```

> A succesful response will look like

```json
{
    "token_type": "bearer",
    "access_token": "6d465b46426bb44fa4d417dc627dd469197cbc33",
    "expires_in": "2592000",
    "refresh_token": "90f606b27f80e7bce17336813e429af4a2501cfd"
}
```

### Request Parameters

Parameter | Default Value | Description
--------- | ------- | -----------
`grant_type` | (required) | `password` is the only supported grant type for this endpoint.
`username` | (required) | Should be an email address.
`password` | (required) | The user's password.

### Response Properties

Property | Description
-------- | -----------
`token_type` | Type of token based on `grant_type` request parameter.
`access_token` | Access token used to authenticate requests.
`expires_in` | Time in ms until the access token expires. The `refresh_token` should be used to get a new access token.
`refresh_token` | Used to fetch a new access token.



## Validating the access token

```http
GET /auth/authorise/ HTTP/1.1
Host: services.surfline.com
access_token=tGzv3JOkF0XG5Qx2TlKWIA
```

> A valid response will return

```json
{
    "id": "56faeb5b19b74318a73cb89f"
}
```

> An invalid authorization request will return

```json
{
    "code": 401,
    "error": "invalid_token",
    "error_description": "The access token provided is invalid."
}
```

**`GET /auth/authorise/`**

Access tokens can be validated using the authorise endpoint.


### HTTP Parameters

Parameter | Default | Description
--------- | ------- | -----------
`access_token` | (required) | The access token to validate

## Getting a new access token

```http
POST /auth/token/ HTTP/1.1
Host: services.surfline.com
Content-Type: application/x-www-form-urlencoded
grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA
```

**`POST /auth/token/`**

Access tokens expire after a given period of time. The default expiration for an access token is 2592000 ms. A new access token can be obtained via a refresh token.


## Logging out

**`POST /auth/accessToken/:access_token`**<br />
**`POST /auth/refreshToken/:refresh_token`**

To logout the access and refresh token should be deleted via a `DELETE` request to `/auth/accessToken/:access_token` and `/auth/refreshToken/:refresh_token` respectively.


> Delete an access token

```http
DELETE /auth/accessToken/6d465b46426bb44fa4d417dc627dd469197cbc33 HTTP/1.1
Host: services.surfline.com
```

> Delete a refresh token

```http
DELETE /auth/refreshToken/90f606b27f80e7bce17336813e429af4a2501cfd HTTP/1.1
Host: services.surfline.com
```

A `200` response will be returned if the token was succesfully deleted.


# Surf Reports

## Get Spot Report

**`GET /kbyg/spots/reports/`**

```json
{
  "spot": {
    "name": "Windansea",
    "lat": 32.829151647032,
    "lon": -117.2822713851
  },
  "forecast": {
    "conditions": {
      "human": true,
      "value": "POOR"
    },
    "wind": {
      "speed": 8,
      "direction": 278.76
    },
    "waveHeight": {
      "human": true,
      "min": 1,
      "max": 2,
      "occasional": null,
      "humanRelation": "knee to thigh high",
      "plus": false
    },
    "tide": {
      "previous": {
        "type": "HIGH",
        "height": 4.6,
        "timestamp": 1502391254,
        "utcOffset": -7
      },
      "current": {
        "type": "NORMAL",
        "height": 4,
        "timestamp": 1502396706,
        "utcOffset": -7
      },
      "next": {
        "type": "LOW",
        "height": 1.8,
        "timestamp": 1502411012,
        "utcOffset": -7
      }
    },
    "waterTemp": {
      "min": 65,
      "max": 68
    },
    "weather": {
      "temperature": 75,
      "condition": "CLEAR_NO_RAIN"
    }
  },
  "report": {
    "timestamp": 1502396705,
    "forecaster": {
      "name": "Chris Borg",
      "iconUrl": "https://www.gravatar.com/avatar/732a7c19e81048b6e1f7d9d654c450c7?d=mm",
      "title": "Senior Forecaster"
    },
    "body": "<p><strong>Afternoon Report for South SD:</strong> Small scale blend of long period new SSW/S swell and persistent NW windswell this afternoon. Surf is mainly running knee/thigh high range, while top spots hit waist high. Winds are light+ westerly at report time for textured+ conditions.</p>"
  }
}
```

Retrieves a spot's surf report. A surf report consists of data points for the spot's current conditions.

**Example Endpoint**

[https://services.surfline.com/kbyg/spots/reports/?spotId=5842041f4e65fad6a770883c](https://services.surfline.com/kbyg/spots/reports/?spotId=5842041f4e65fad6a770883c)

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
`spotId` | (required) | Spot id to return report data for

### Response Properties

Property | Type | Description
-------- | ---- | -----------
`spot.name` | String | The name of the spot
`spot.lat` | Number | The latitude of the spot
`spot.lon` | Number | The longitude of the spot
`spot.forecast.conditions` | Object | Spot forecast conditions
`spot.forecast.conditions.value` | String | See [Surf Rating Values](#surf-ratings)
`spot.forecast.wind.speed` | Number | Wind speed in the user's units
`spot.forecast.wind.direction` | Number | The current wind direction
`spot.forecast.waveHeight` | Object | An object containing current wave height details
`spot.forecast.waveHeight.min` | Number | Max surf height
`spot.forecast.waveHeight.max` | Number | Min surf height
`spot.forecast.waveHeight.occasional` | Number | (nullable) Occasional surf height
`spot.forecast.waveHeight.humanRelation` | String | (nullable) Human relation to the wave height.
`spot.forecast.waveHeight.plus` | Boolean |
`spot.forecast.tide` | Object | `previous`, `current` and `next` tides
`spot.forecast.waterTemp` | Object |
`spot.forecast.waterTemp.min` | Number | Minumum water temperature
`spot.forecast.waterTemp.max` | Number | Maximum water temperature
`spot.forecast.weather` | Object |
`spot.forecast.weather.temperature` | Number | Current air temperature
`spot.forecast.weather.condition` | String | See [Surf Ratings](#surf-ratings) for a list of possible values
`spot.report` | Object | (nullable) Written report details. A `null` value indicates no human report exists.
`spot.report.timestamp` | Timestamp | Unix timestamp
`spot.report.body` | String | An html string containing the timestamp's writen spot report.


# User Favorites

## List Favorites with Spot Reports


> Sample Favorites Response

```json
{
    "data": {
        "favorites": [
            {
                "_id": "5842041f4e65fad6a77088b5",
                "name": "Pipes",
                "conditions": {
                    "human": true,
                    "value": "POOR"
                },
                "wind": {
                    "speed": 6,
                    "direction": 278.68
                },
                "waveHeight": {
                    "human": true,
                    "min": 1,
                    "max": 2,
                    "occasional": null,
                    "humanRelation": "knee to thigh high",
                    "plus": false
                },
                "tide": {
                    "previous": {
                        "type": "LOW",
                        "height": 1.8,
                        "timestamp": 1502411012,
                        "utcOffset": -7
                    },
                    "current": {
                        "type": "NORMAL",
                        "height": 4.9,
                        "timestamp": 1502428025,
                        "utcOffset": -7
                    },
                    "next": {
                        "type": "HIGH",
                        "height": 5.3,
                        "timestamp": 1502432550,
                        "utcOffset": -7
                    }
                }
            }
        ]
    }
}
```

**`GET /kbyg/favorites`**

Returns the authenticated user's favorites and spot reports for those favorites.

### Response Properties

Property | Type | Description
-------- | ---- | -----------
`favorites[]` | Array | An array of favorites and corresponding spot reports
`favorites[]._id` | String | The id of the spot. The id can be used as the `spotId` in the [Surf Report](#surf-reports) endpoint
`favorites[].name` | String | The name of the spot
`favorites[].conditions` | String | Current surf conditions for the spot
`favorites[].wind` | Object | Current wind conditions for the spot
`favorites[].waveHeight` | Object | Current wave conditions for the spot
`favorites[].tide` | Object | `previous`, `current` and `next` tides

<aside class="warning">Requests made without an access token will return a <code>401</code> unauthorized error.</aside>

# Enumerated Values

## Weather Conditions

The following values are valid weather condition strings

**Daytime**

* `CLEAR_NO_RAIN`
* `CLEAR_RAIN`
* `CLEAR_FREEZING_RAIN`
* `CLEAR_HAIL`
* `CLEAR_SNOW`
* `MOSTLY_CLEAR_NO_RAIN`
* `MOSTLY_CLEAR_RAIN`
* `MOSTLY_CLEAR_FREEZING_RAIN`
* `MOSTLY_CLEAR_HAIL`
* `MOSTLY_CLEAR_SNOW`
* `PARTLY_CLOUDY_NO_RAIN`
* `PARTLY_CLOUDY_RAIN`
* `PARTLY_CLOUDY_FREEZING_RAIN`
* `PARTLY_CLOUDY_HAIL`
* `PARTLY_CLOUDY_SNOW`
* `MOSTLY_CLOUDY_NO_RAIN`
* `MOSTLY_CLOUDY_RAIN`
* `MOSTLY_CLOUDY_FREEZING_RAIN`
* `MOSTLY_CLOUDY_HAIL`
* `MOSTLY_CLOUDY_SNOW`
* `OVERCAST_NO_RAIN`
* `OVERCAST_RAIN`
* `OVERCAST_FREEZING_RAIN`
* `OVERCAST_HAIL`
* `OVERCAST_SNOW`

**Nighttime**

* `NIGHT_CLEAR_NO_RAIN`
* `NIGHT_CLEAR_RAIN`
* `NIGHT_CLEAR_FREEZING_RAIN`
* `NIGHT_CLEAR_HAIL`
* `NIGHT_CLEAR_SNOW`
* `NIGHT_MOSTLY_CLEAR_NO_RAIN`
* `NIGHT_MOSTLY_CLEAR_RAIN`
* `NIGHT_MOSTLY_CLEAR_FREEZING_RAIN`
* `NIGHT_MOSTLY_CLEAR_HAIL`
* `NIGHT_MOSTLY_CLEAR_SNOW`
* `NIGHT_PARTLY_CLOUDY_NO_RAIN`
* `NIGHT_PARTLY_CLOUDY_RAIN`
* `NIGHT_PARTLY_CLOUDY_FREEZING_RAIN`
* `NIGHT_PARTLY_CLOUDY_HAIL`
* `NIGHT_PARTLY_CLOUDY_SNOW`
* `NIGHT_MOSTLY_CLOUDY_NO_RAIN`
* `NIGHT_MOSTLY_CLOUDY_RAIN`
* `NIGHT_MOSTLY_CLOUDY_FREEZING_RAIN`
* `NIGHT_MOSTLY_CLOUDY_HAIL`
* `NIGHT_MOSTLY_CLOUDY_SNOW`
* `NIGHT_OVERCAST_NO_RAIN`
* `NIGHT_OVERCAST_RAIN`
* `NIGHT_OVERCAST_FREEZING_RAIN`
* `NIGHT_OVERCAST_HAIL`
* `NIGHT_OVERCAST_SNOW`

## Surf Ratings

The following values are valid surf rating strings

* `NONE`
* `EPIC`
* `GOOD_TO_EPIC`
* `VERY_GOOD`
* `GOOD`
* `FAIR_TO_GOOD`
* `FAIR`
* `POOR_TO_FAIR`
* `POOR`
* `VERY_POOR`
* `FLAT`
