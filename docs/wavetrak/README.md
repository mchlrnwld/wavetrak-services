# Wavetrak Forecast API

http://sl-docs.s3-us-west-1.amazonaws.com/wavetrak/index.html

## Building and Publishing Documentation

### Developing

These documents are built using https://github.com/lord/slate. Documentation source is stored in `source/index.html.md`. To automatically generate docs as you make changes run

```bash
docker-compose up
```

### Publishing

To publish the documentation to S3 run

```
make publish
```

(Your AWS credentials will need to be set to use `dev` credentials)
