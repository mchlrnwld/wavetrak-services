---
title: Wavetrak Forecast API
---

# Introduction
The Wavetrak Forecast API provides forecasted data for a given coordinate. This does not include observation or tide data.

The response provides the best data for the attribute from the available predictions. You can override the source data for an attribute by specifying a model parameter.

The API is modeled around REST, should be requested over SSL and return JSON.

## Definitions

General definitions mostly for the descriptive text.

Phrase | Definition
------ | ----------
timestep | Start point in time that the forecast is for. The forecast is a mean of that metric from this start point until the next timestep.
run | Complete set of forecast/timestep objects. i.e. a forecast run.
data bucket | Group of attributes within a given data object, like wind, wave etc.
data object | Forecast data for a given timestep.


# Making Requests

## Base URLs

All documented endpoints can be access from the following base URLs.

Environment | Endpoint
----------- | --------
Production  | <http://science-data-service.prod.surfline.com/>
Sandbox  | <http://science-data-service.sandbox.surfline.com/>

## Query Parameters

Query parameters common to all API endpoints.

Parameter | Type | Default | Description | Example
--------- | ---- | ------- | ----------- | -------
`start` | Number | 00am of current day (local time). | Unix timestamp of start time for forecast. Bear in mind the value is a UTC timestamp, but you'll be requesting for a location that won't be on the same timezone. Passing timestamps in the past will return an historic forecast. | `1445988876`
`end` | Number | Maximum forecasted date available. | Unix timestamp of end time for forecast. | `1445988876`
`interval` | Number | `10800` | Specify in seconds the timesteps between forecast data objects. Data may be interpolated. | `3600`
`units[*]` | Enum | Default Unit | Units option. See [units](#units). | `units[waveHeight]=m`


# Conventions

## Root object

> **_Do we want to enforce data exists and throw 4xx error if empty?_**

Root response is always an object with either error if there was an error with the response or data if the response was considered OK (doesn't mean there was results). The existence of the error object implies that an error occurred.

Attribute | Type | Description
--------- | ---- | -----------
`data` | Array | Will be empty if no results. Array will contain most of the data for your query.
`associated` | Object | Optional. Will contain resolved ref objects. Ref objects are explained below. Since ref objects could be referenced multiple times within a given data set the ref objects are returned in the `associated` parameter on the root object to avoid massive duplication and large response sizes.
`error` | Object | Optional. If the API returns a non 200 HTTP response code this object will exist. It contains `code` (number) and `message` (string).

## Timestamps
Dates and times are represented as a unix timestamp. Time is always in UTC and a timezone offsets will be provided for the spot being requested.

To prevent potential timezone inconsistencies (i.e. daylight savings) that might change between a forecast's timesteps, we include an array of timezones in the `associated` response, and an `id` in each forecast timestep mapping to the correct timezone and offset for that timestamp.

## Models

> Sample model object.

```JSON
{
    "agency": "NOAA",
    "model": "GFS",
    "run": 2019012100,
    "grid": {
        "key": "0p25",
        "lat": 33.6046872,
        "lon": -117.8868619
    }
}
```

Model object defines the data model used for the data returned.

Property | Type | Description
-------- | ---- | -----------
`agency` | Enum | Key identifying the agency providing the model.
`model` | Enum | Name of the model.
`run` | Number | UNIX timestamp for model initialization time.
`grid.key` | Enum | Key identifying grid used for the model data.
`grid.lat` | Number | Latitude of grid coordinate for model data.
`grid.lon` | Number | Longitude of grid coordinate for model data.

## Timezones

Timezones for forecast dates used. These will be referenced by each timestep in the forecast. A list is provided to account for timezones changing during the forecasted time range (e.g. daylight savings time or political timezone changes).

> Sample timezone object.

```JSON
{
    "refId": 0,
    "name": "Europe/London",
    "abbr": "BST",
    "offset": 3600
}
```

Property | Type | Description
-------- | ---- | -----------
`refId` | Number | ID to reference this timezone in `data`.
`name` | String | IANA timezone identifier.
`abbr` | String | Abbreviation of the timezone.
`offset` | Number | Offset from UTC in seconds.

## Units

> Sample units object.

```JSON
{
    "waveHeight": "ft",
    "tideHeight": "m",
    "windSpeed": "mi/h",
    "temperature": "C",
    "pressure": "mb",
    "visibility": "m",
    "precipitation": "in"
}
```

Units are returned in either imperial or metric. Optional `unit` param is able to specify units for each corresponding data object. Anonymous requests determine units based on geolocation of the request's IP address.

Requests will return an `associated.units` object detailing units returned in the request.

### Unit types

Type | Options | Default
---- | ------- | -----------------
`waveHeight` | `m` &#124; `ft` | `m`
`tideHeight` | `m` &#124; `ft` | `m`
`windSpeed` | `mi/h` &#124; `knot` &#124; `km/h` | `km/h`
`temperature` | `C` &#124; `F` | `C`
`pressure` | `mb` | `mb`
`visibility` | `m` &#124; `ft` | `m`
`precipitation` | `mm` &#124; `in` | `mm`

## Errors
> **_How do we handle partial errors, as in when the wind lookup fails but the wave lookup succeeds? Do we fail the entire request and throw an error, or do send back all successful data with a Multi-Status 207 status code?_**

> **_GraphQL's error collection may provide cleaner error handling and the returning of successful data alongside any errors._**

> Error Response

```JSON
{
    "code": 401,
    "message": "Unauthorized"
}
```

Requests containing errors will return a `4xx` or `5xx` status code with a `message` property describing the error.


# Swells

**`GET /forecasts/swells`**

Retrieve swell forecast data for a specific coordinate derived from `lat`/`lon` params.

> Get forecasts

```http
GET /forecasts/swells?model=GFS%20FProteus&grid=GLOB_30m&lat=33.654207&lon=-118.004142&start=1445725452&end=1445811840&interval=24 HTTP/1.1
Host: science-data-service.prod.surfline.com
```
> A valid response will return

```JSON
{
    "associated": {
        "model": {
            "agency": "Proteus",
            "model": "WW3",
            "run": 2019012100,
            "grid": {
                "key": "GLOB_30m",
                "lat": 33.6046872,
                "lon": -117.8868619
            }
        },
        "timezones": [{
            "refId": 0,
            "name": "Europe/London",
            "abbr": "BST",
            "offset": 3600
        }],
        "units": {
            "waveHeight": "FT"
        }
    },
    "data": [{
        "timestamp": 1445725452,
        "timezoneRefId": 0,
        "swells": {
           "combined": {
               "height": 9.19,
               "direction": {
                   "max": 270,
                   "mean": 267.19,
                   "min": 264.38
               },
               "period": {
                   "mean": 8,
                   "peak": 10
               }
           },
            "components": [{
                "height": 2.45,
                "direction": {
                    "min": 220.02,
                    "max": 250.56,
                    "mean": 210.23
                },
                "period": {
                  "mean": 4.32,
                  "peak": 4.32
                }
            }, {
                "height": 2.45,
                "direction": {
                    "min": 220.02,
                    "max": 250.56,
                    "mean": 210.23
                },
                "period": {
                  "mean": 4.32,
                  "peak": 4.32
                }
            }]
        }
    }]
}
```

## Query Parameters

Query parameters in addition to those defined [here](#query-parameters).

Parameter | Type | Default | Description | Example
--------- | ---- | ------- | ----------- | -------
`lat` | Float | (required) | Latitude, coordinate point of forecast. Max resolution of four decimal places. | `50.3`
`lon` | Float | (required) | Longitude, coordinate point of forecast. Max resolution of four decimal places. | `-5.12`
`agency` | Enum | (required) | Specify a model key. | `Proteus`
`model` | Enum | (required) | Specify a model key. | `WW3`
`run` | Number | Latest online model run. | Specify a model run. | `2019012100`
`grid` | Enum | (required) | Specify a model grid. If a requested grid is not available with the agency in use, this will return an error. | `UK_4m`

## Response Properties

Successful responses return both `associated` and `data` objects, as described below.

### Swells Forecast Data Object

Property | Type | Description
-------- | ---- | -----------
`timestamp` | Number | Start timestamp of the forecasted timestamp.
`timezoneRefId` | Number | `refId` for the timezone at this timestep.
`swells.combined` | Object | Nested swells object.
`swells.components` | Object | Nested swells object.

### Swells Object
> **_Swells should be ordered by height/energy to match MSW's primary/secondary/tertiary contract_**

Property | Type | Description
-------- | ---- | -----------
`height` | Float | Height of swell.
`direction.min` | Float | Min component swell direction.
`direction.max` | Float | Max component swell direction.
`direction.mean` | Float | Mean component swell direction. If you're displaying 2ft at 12s WNW you'd calculate the cardinal direction from this angle.
`period.peak` | Float | Peak period of partitioned swell component.
`period.mean` | Float | Mean period of swell component.

### Associated Data Object

Property | Type | Description
-------- | ---- | -----------
`model` | Object | [Model Object](#models).
`timezones[]` | Object | [Timezone Object](#timezones).
`units` | Object | [Units Object](#units).


# Swells Ensemble

**`GET /forecasts/swells-ensemble`**

Retrieve swell ensemble forecast data for a specific coordinate derived from `lat`/`lon` params.

> Get forecasts

```http
GET /forecasts/swells-ensemble?agency=noaa&model=ww3-ensemble&grid=glo_30m&lat=33.5&lon=118.0&start=1565740800&end=1565762400&interval=43200 HTTP/1.1
Host: science-data-service.prod.surfline.com
```

> A valid response will return

```JSON
{
    "associated": {
        "model": {
            "agency": "NOAA",
            "model": "WW3-Ensemble",
            "run": 2019081500,
            "grid": {
                "key": "glo_30m",
                "lat": 33.5,
                "lon": 118.0
            }
        },
        "timezones": [{
            "refId": 0,
            "name": "Europe/London",
            "abbr": "BST",
            "offset": 3600
        }],
        "units": {
            "waveHeight": "m",
            "windSpeed": "km/h"
        }
    },
    "data": [{
        "timestamp": 1566000000,
        "timezoneRefId": 0,
        "members": [{
            "wind": {
                "direction": 175.24,
                "speed": 7.23
            },
            "swells": {
                "combined": {
                    "direction": 204.86,
                    "height": 2.32,
                    "period": 18.19
                },
                "components": [{
                    "direction": 203.25,
                    "height": 1.59,
                    "period": 18.19
                }, {
                    "direction": 203.46,
                    "height": 0.92,
                    "period": 12.99
                },
                {
                    "direction": 172.03,
                    "height": 1.41,
                    "period": 9.25
                }]
            }
        }, {
            "wind": {
                "direction": 123.57,
                "speed": 8.01
            },
            "swells": {
                "combined": {
                    "direction": 199.68,
                    "height": 3.49,
                    "period": 20.05
                },
                "components": [{
                    "direction": 215.30,
                    "height": 1.80,
                    "period": 15.20
                }, {
                    "direction": 175.40,
                    "height": 0.98,
                    "period": 13.00
                },
                {
                    "direction": 175.03,
                    "height": 1.78,
                    "period": 10.23
                }]
            }
        }]
    },{
        "timestamp": 1566216000,
        "timezoneRefId": 0,
        "members": [{
            "wind": {
                "direction": 175.24,
                "speed": 7.23
            },
            "swells": {
                "combined": {
                    "direction": 204.86,
                    "height": 2.32,
                    "period": 18.19
                },
                "components": [{
                    "direction": 203.25,
                    "height": 1.59,
                    "period": 18.19
                }, {
                    "direction": 203.46,
                    "height": 0.92,
                    "period": 12.99
                },
                {
                    "direction": 172.03,
                    "height": 1.41,
                    "period": 9.25
                }]
            }
        }, {
            "wind": {
                "direction": 123.57,
                "speed": 8.01
            },
            "swells": {
                "combined": {
                    "direction": 199.68,
                    "height": 3.49,
                    "period": 20.05
                },
                "components": [{
                    "direction": 215.30,
                    "height": 1.80,
                    "period": 15.20
                }, {
                    "direction": 175.40,
                    "height": 0.98,
                    "period": 13.00
                },
                {
                    "direction": 175.03,
                    "height": 1.78,
                    "period": 10.23
                }]
            }
        }]
    }]
}
```

## Query Parameters

Query parameters in addition to those defined [here](#query-parameters).

Parameter | Type | Default | Description | Example
--------- | ---- | ------- | ----------- | -------
`agency` | Enum | (required) | Specify an agency key. | `NOAA`
`model` | Enum | (required) | Specify a model key. | `WW3-Ensemble`
`run` | Number | (required) Latest online model run. | Specify a model run. | `2019081500`
`grid` | Enum | (required) | Specify a model grid. If a requested grid is not available with the agency in use, this will return an error. | `glo_30m`
`lat` | Float | (required) | Latitude, coordinate point of forecast. Max resolution of four decimal places. | `37.5`
`lon` | Float | (required) | Longitude, coordinate point of forecast. Max resolution of four decimal places. | `-5.0`

## Response Properties

Successful responses return both `associated` and `data` objects, as described below.

### Swells Ensemble Forecast Data Object

Property | Type | Description
-------- | ---- | -----------
`timestamp` | Number | Start timestamp of the forecasted timestamp.
`timezoneRefId` | Number | `refId` for the timezone at this timestep.
`members` | Object | Nested members object representing the different outcomes for the given model run. There are 21 outcomes in total. Each member contains wind information and swell data objects.
`wind.direction` | Float | Wind direction.
`wind.speed` | Float | Wind speed.
`swells.combined` | Object | Nested swells object.
`swells.components` | Object | Nested swells object.

### Swells Object
Property | Type | Description
-------- | ---- | -----------
`direction` | Float | Swell direction.
`height` | Float | Height of swell.
`period` | Float | Mean period of swell.


### Associated Data Object

Property | Type | Description
-------- | ---- | -----------
`model` | Object | [Model Object](#models).
`timezones[]` | Object | [Timezone Object](#timezones).
`units` | Object | [Units Object](#units).

# Wind

**`GET /forecasts/wind`**

Retrieve wind forecast data for a specific coordinate derived from `lat`/`lon` params.

> Get forecasts

```http
GET /forecasts/wind?model=GFS&grid=GLOB_15m&lat=33.654207&lon=-118.004142&start=1445725452&end=1445811840&interval=24 HTTP/1.1
Host: science-data-service.prod.surfline.com
```
> A valid response will return

```JSON
{
    "associated": {
        "model": {
            "agency": "NOAA",
            "model": "GFS",
            "run": 2019012100,
            "grid": {
                "key": "0p25",
                "lat": 33.6046872,
                "lon": -117.8868619
            }
        },
        "timezones": [{
            "refId": 0,
            "abbr": "BST",
            "offset": 3600,
            "name": "Europe/London"
        }],
        "units": {
            "windSpeed": "KPH"
        }
    },
    "data": [{
        "timestamp": 1445725452,
        "timezoneRefId": 0,
        "wind": {
            "speed": 12.23,
            "direction": 345.23,
            "gusts": 15.66
        }
    }]
}
```

## Query Parameters

Query parameters in addition to those defined [here](#query-parameters).

Parameter | Type | Default | Description | Example
--------- | ---- | ------- | ----------- | -------
`lat` | Float | (required) | Latitude, coordinate point of forecast. Max resolution of four decimal places. | `50.3`
`lon` | Float | (required) | Longitude, coordinate point of forecast. Max resolution of four decimal places. | `-5.12`
`agency` | Enum | (required) | Specify a model key. | `NOAA`
`model` | Enum | (required) | Specify a model key. | `GFS`
`run` | Number | Latest online model run. | Specify a model run. | `2019012100`
`grid` | Enum | (required) | Specify a model grid. If a requested grid is not available with the agency in use, this will return an error. | `0p25`

## Response Properties

Successful responses return both `associated` and `data` objects, as described below.

### Wind Forecast Data Object

Property | Type | Description
-------- | ---- | -----------
`timestamp` | Number | Start timestamp of the forecasted timestamp.
`timezoneRefId` | Number | `refId` for the timezone at this timestep.
`wind.speed` | Float | Average wind speed.
`wind.direction` | Float | Average wind direction.
`wind.gusts` | Float | Average wind gusts.

### Associated Data Object

Property | Type | Description
-------- | ---- | -----------
`model` | Object | [Model Object](#models).
`timezones[]` | Object | [Timezone Object](#timezones).
`units` | Object | [Units Object](#units).


# Weather

**`GET /forecasts/weather`**

Retrieve wind/weather forecast data for a specific coordinate derived from `lat`/`lon` params.

> Get forecasts

```http
GET /forecasts/weather?agency=NOAA&model=GFS&grid=0p25&lat=33.654207&lon=-118.004142&start=1445725452&end=1445811840&interval=10800 HTTP/1.1
Host: science-data-service.prod.surfline.com
```
> A valid response will return

```JSON
{
    "associated": {
        "model": {
            "agency": "NOAA",
            "model": "GFS",
            "run": 2019030518,
            "grid": {
                "key": "0p25",
                "lat": 33.75,
                "lon": 242
            }
        },
        "timezones": [{
            "refId": 0,
            "name": "America/Los_Angeles",
            "abbr": "PST",
            "offset": -28800
        }],
        "units": {
            "temperature": "C",
            "pressure": "mb",
            "visibility": "m",
            "precipitation": "in"
        }
    },
    "data": [{
        "timestamp": 1445725452,
        "timezoneRefId": 0,
        "weather": {
            "wind": {
                "speed": 12.23,
                "direction": 345.23,
                "gusts": 15.66
            },
            "pressure": 1024,
            "temperature": 12,
            "visibility": 24100,
            "dewpoint": 23,
            "humidity": 90,
            "precipitation": {
                "type": "RAIN",
                "volume": 12.23
            },
            "conditions": "CLEAR_INTERVALS"
        }
    }]
}
```

## Query Parameters

Query parameters in addition to those defined [here](#query-parameters).

Parameter | Type | Default | Description | Example
--------- | ---- | ------- | ----------- | -------
`lat` | Float | (required) | Latitude, coordinate point of forecast. Max resolution of four decimal places. | `50.3`
`lon` | Float | (required) | Longitude, coordinate point of forecast. Max resolution of four decimal places. | `-5.12`
`agency` | Enum | (required) | Specify a model key. | `NOAA`
`model` | Enum | (required) | Specify a model key. | `GFS`
`run` | Number | Latest online model run. | Specify a model run. | `2019012100`
`grid` | Enum | (required) | Specify a model grid. If a requested grid is not available with the agency in use, this will return an error. | `0p25`

## Response Properties

Successful responses return both `associated` and `data` objects, as described below.

### Weather Forecast Data Object

Property | Type | Description
-------- | ---- | -----------
`timestamp` | Number | Start timestamp of the forecasted timestamp.
`timezoneRefId` | Number | `refId` for the timezone at this timestep.
`weather.wind.speed` | Float | Average wind speed.
`weather.wind.direction` | Float | Average wind direction.
`weather.wind.gusts` | Float | Average wind gusts.
`weather.pressure` | Float | Mean sea level pressure.
`weather.temperature` | Float | Air temperature.
`weather.visibility` | Float | Visibility distance.
`weather.dewpoint` | Float | Dewpoint is the temperature to which a given parcel of air must be cooled, at constant barometric pressure, for water vapor to condense into water.
`weather.humidity` | Float | The amount of water vapor in the air.
`weather.precipitation.type` | Enum | Precipitation type. Possible values: `NONE`, `RAIN`, `ICE_PELLETS`, `FREEZING_RAIN`, `SNOW`.
`weather.precipitation.volume` | Float | Volume of precipitation type over the forecasted time period. Pretty sure it's the maximum predicted volume.
`weather.conditions` | Enum | Used to identify the weather type/condition, useful to tie weather icons. Possible values: `CLEAR`, `CLEAR_INTERVALS`, `CLOUDY`, `OVERCAST`, `MIST`, `FOG`, `DRIZZLE`, `BRIEF_SHOWERS`, `SHOWERS`, `HEAVY_SHOWERS`, `RAIN`, `HEAVY_RAIN`, `TORRENTIAL_RAIN`, `ICE_PELLETS`, `ICE_PELLETS_SHOWERS`, `FREEZING_RAIN`, `FREEZING_RAIN_SHOWERS`, `LIGHT_SNOW`, `SNOW`, `HEAVY_SNOW`, `LIGHT_SNOW_SHOWERS`, `SNOW_SHOWERS`, `HEAVY_SNOW_SHOWERS`, `THUNDERY_SHOWERS`, `THUNDER_STORMS`.

### Associated Data Object

Property | Type | Description
-------- | ---- | -----------
`model` | Object | [Model Object](#models).
`timezones[]` | Object | [Timezone Object](#timezones).
`units` | Object | [Units Object](#units).


# Surf Spot

**`GET /forecasts/surfspot`**

Retrieve surf spot forecast data derived from `surfSpotId` param.

> Get forecasts

```http
GET /forecasts/surfspot?models[swells]=GFS%20FProteus&models[weather]=GFS&grids[swells]=GLOB_30m&grids[weather]=GLOB_15m&surfSpotId=5842041f4e65fad6a77088ed&start=1445725452&end=1445811840&interval=24 HTTP/1.1
Host: science-data-service.prod.surfline.com
```
> A valid response will return

```JSON
{
    "associated": {
        "models": {
            "weather": {
                "agency": "NOAA",
                "model": "GFS",
                "run": 1477267200,
                "grid": {
                    "key": "GLOB_15m",
                    "lat": 33.6046872,
                    "lon": -117.8868619
                }
            },
            "swells": {
                "agency": "Proteus",
                "model": "WW3",
                "run": 1477267200,
                "grid": {
                    "key": "GLOB_30m",
                    "lat": 33.6046872,
                    "lon": -117.8868619
                }
            }
        },
        "timezones": [{
            "refId": 0,
            "name": "Europe/London",
            "abbr": "BST",
            "offset": 3600,
        }],
        "units": {
            "waveHeight": "FT",
            "windSpeed": "KPH",
            "temperature": "C",
            "pressure": "MB",
            "visibility": "KM",
            "precipitation": "IN"
        }
    },
    "data": [{
        "timestamp": 1445725452,
        "timezoneRefId": 0,
        "swells": {
           "combined": {
               "height": 9.19,
               "direction": {
                   "max": 270,
                   "mean": 267.19,
                   "min": 264.38
               },
               "period": {
                   "mean": 8,
                   "peak": 10
               }
           },
            "components": [{
                "height": 2.45,
                "direction": {
                    "min": 220.02,
                    "max": 250.56,
                    "mean": 210.23
                },
                "period": {
                  "mean": 4.32,
                  "peak": 4.32
                }
            }, {
                "height": 2.45,
                "direction": {
                    "min": 220.02,
                    "max": 250.56,
                    "mean": 210.23
                },
                "period": {
                  "mean": 4.32,
                  "peak": 4.32
                }
            }],
            "probability": 0.8,
            "spectra": {
              "period": [2, 3, 4, ... , 28],
              "energy": [0, 0, 0.0002, 0.00043, ... , 0],
              "direction": [0, 0, 249.6, ... , 0],
              "directionSpread": [0, 0, 6.2, 5.4, ..., 0]
            }
        },
        "wind": {
            "speed": 12.23,
            "direction": 345.23,
            "gusts": 15.66
        },
        "weather": {
            "pressure": 1024,
            "temperature": 12,
            "visibility": 15,
            "dewpoint": 23,
            "humidity": 90,
            "precipitation": {
                "type": "RAIN",
                "volume": 12.23
            },
            "condition": "MOSTLY_CLEAR_NO_RAIN"
        },
        "surf": {
            "min": 2.34,
            "max": 5.43
        },
        "rating": 5,
    }]
}
```

## Query Parameters

Query parameters in addition to those defined [here](#query-parameters).

Parameter | Type | Default | Description | Example
--------- | ---- | ------- | ----------- | -------
`surfSpotId` | String | (required) | Unique identifier used by the API to identify the spot lat/lon, model name/grid, transformation matrix and identifier for ML model. | `1234`
`models[swells]` | Enum | (required) | Specify a swells model key. | GFS/Proteus
`models[weather]` | Enum | (required) | Specify a weather model key. | GFS
`grids[swells]` | Enum | (required) | Specify a swells model grid key. | GLOB_30m
`grids[weather]` | Enum | (required) | Specify a weather model grid key. | GLOB_15m

## Response Properties

Successful responses return both `associated` and `data` objects, as described below.

### Surf Forecast Data Object

Property | Type | Description
-------- | ---- | -----------
`timestamp` | Number | Start timestamp of the forecasted timestamp.
`timezoneRefId` | Number | `refId` to the timezone at this timestep.
`swells` | Object | [Swell Forecast Data Object](#swells).
`swells.spectra` | Object | Properties `period`, `direction`, `speed` and `directionSpeed` are all array of values being the same length. Units are: seconds for period, m^2s for energy, and degrees for direction and direction spread.
`swells.probability` | Float | Probability of swell forecast accuracy.
`wind` | Object | [Wind Forecast Data Object](#wind).
`weather` | Object | [Weather Forecast Data Object](#weather).
`surf.min` | Float | Minimum breaking surf height.
`surf.max` | Float | Maximum breaking surf height.
`rating` | Int | Rating derived from ML models. Possible values are in the range 0 - 100.

### Associated Data Object

Property | Type | Description
-------- | ---- | -----------
`models.swells` | Object | [Model Object](#models) for swell data.
`models.weather` | Object | [Model Object](#models) for weather data.
`timezones[]` | Object | [Timezone Object](#timezones).
`units` | Object | [Units Object](#units).


# GraphQL

**`GET /forecasts/graph`**

Retrieve any or all components of forecast data across swell/surf/wind/weather/sst data types. Query structure determines what is included in response.

Details TBD
