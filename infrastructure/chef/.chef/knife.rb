$home_dir = ENV['HOME']
current_dir = File.dirname(__FILE__)
load "#{$home_dir}/.chef/knife-config.rb"
load "#{current_dir}/knife-config.rb"
log_level                       :info
log_location                    STDOUT
node_name                       "#{$chef_username}"
client_key                      "#{$home_dir}/.chef/#{$chef_username}.pem"
syntax_check_cache_path         "#{$home_dir}/.chef/syntax_check_cache"
trusted_certs_dir               "#{$home_dir}/.chef/trusted_certs"
chef_server_url                 "https://chef.sturdynetworks.com/organizations/#{$chef_orgname}"
cookbook_path                   [ "#{current_dir}/../cookbooks", "#{current_dir}/../vendor/cookbooks" ]
