log_level              :info
log_location           "/var/log/chef/client.log"
ssl_verify_mode        :verify_none
validation_client_name "surfline-validator"
validation_key         "/etc/chef/validation.pem"
client_key             "/etc/chef/client.pem"
chef_server_url        "https://chef.sturdynetworks.com/organizations/surfline"
file_cache_path        "/var/cache/chef"
file_backup_path       "/var/backups/chef"
pid_file               "/var/run/chef/client.pid"
Chef::Log::Formatter.show_time = true
