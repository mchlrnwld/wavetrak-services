#
# Cookbook Name:: bw-wordpress
# Recipe:: content
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# deploy wp-content artifact from S3
s3_file "#{node['bw-wordpress']['temp_dir']}" + '/' + "#{node['bw-wordpress']['artifact']}" do
  bucket        "#{node['bw-wordpress']['bucket']}"
  remote_path   '/wordpress/' + "#{node['bw-wordpress']['artifact']}"
  notifies :run, 'execute[extract-archive]', :immediately
end

# extract into web root
execute 'extract-archive' do
  command "
    tar -xzf #{node['bw-wordpress']['temp_dir']}/#{node['bw-wordpress']['artifact']} -C /var/www/html/news/wp-content ;
    chown -R apache:apache /var/www/html/news/wp-content/uploads ;
    chmod -R 775 /var/www/html/news/wp-content/uploads
  "
  action :nothing
end
