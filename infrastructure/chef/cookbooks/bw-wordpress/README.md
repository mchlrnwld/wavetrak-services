bw-wordpress Cookbook
=====================
Deploys Buoyweather Wordpress

Requirements
------------

#### cookbooks
- `s3_file` - bw-proxy needs s3_file to grab artifacts from S3

Attributes
----------

#### bw-wordpress::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['bw-wordpress']['artifact']</tt></td>
    <td>String</td>
    <td>name of artifact</td>
    <td><tt>wp-content.tar.gz</tt></td>
  </tr>
  <tr>
    <td><tt>['bw-wordpress']['bucket']</tt></td>
    <td>String</td>
    <td>S3 bucket containing artifact</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['bw-wordpress']['temp_dir']</tt></td>
    <td>String</td>
    <td>temporary directory</td>
    <td><tt>/tmp</tt></td>
  </tr>
</table>

Usage
-----
#### bw-wordpress::default

Just include `bw-wordpress` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[bw-wordpress]"
  ]
}
```
