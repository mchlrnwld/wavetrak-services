# name of artifact
default['bw-wordpress']['artifact']          = 'wp-content.tar.gz'

# S3 bucket containing artifact
default['bw-wordpress']['bucket']            = ''

# temporary directory
default['bw-wordpress']['temp_dir']      = '/tmp'
