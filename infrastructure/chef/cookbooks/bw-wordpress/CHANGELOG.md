bw-wordpress CHANGELOG
======================

1.1.0
-----
- [Brandon Pierce] - Update paths used for extracting WP artifact

1.0.1
-----
- [Brandon Pierce] - Resolves SDO-216

1.0.0
-----
- [Brandon Pierce] - Initial release of bw-wordpress
