default['aws_parameter_store']['region'] = 'us-west-1'

# Surfline
default['surfline-coldfusion']['server_type']                       = ''
default['surfline-coldfusion']['service'] = ''
default['surfline-coldfusion']['server_type'] = ''
default['surfline-coldfusion']['healthcheck_type'] = ''

# Apache
default['apache']['ext_status']                                     = true
default['apache']['timeout']                                        = '120'
default['apache']['keepaliverequests']                              = '150'
default['apache']['access_log']                                     = 'access_log'
default['apache']['error_log']                                      = 'error_log'
default['apache']['serversignature']                                = 'off'
default['apache']['version']                                        = '2.2'
default['apache']['package']                                        = 'httpd'
default['apache']['default_modules']                                = %w(
  status alias auth_basic authn_core authn_file authz_core authz_groupfile
  authz_host authz_user env mime negotiation log_config mod_logio
)

default['surfline-coldfusion']['apache']['config'] = {}
default['surfline-coldfusion']['apache']['sites'] = {}

# Apache - prefork
default['apache']['prefork']['startservers']                        = '8'
default['apache']['prefork']['minspareservers']                     = '5'
default['apache']['prefork']['maxspareservers']                     = '20'
default['apache']['prefork']['serverlimit']                         = '256'
default['apache']['prefork']['maxclients']                          = '256'
default['apache']['prefork']['maxrequestsperchild']                 = '4000'

# Apache - worker
default['apache']['worker']['startservers']                         = '2'
default['apache']['worker']['maxclients']                           = '150'
default['apache']['worker']['minsparethreads']                      = '25'
default['apache']['worker']['maxsparethreads']                      = '75'
default['apache']['worker']['threadsperchild']                      = '25'
default['apache']['worker']['maxrequestsperchild']                  = '0'

# Surfline IAM SSH Cookbook Attributes
default['aws-ec2-ssh']['iam_ssh_group'] = case node.chef_environment
                                          when 'prod'
                                            'sl-all-ssh,sl-cfdevs-ssh'
                                          else
                                            'sl-all-ssh,sl-cfdevs-ssh'
                                          end

# ColdFusion 8
# installation directory
default['surfline-coldfusion']['coldfusion8']['installation_dir']   = '/opt/coldfusion8'

# name of installer artifact
default['surfline-coldfusion']['coldfusion8']['installer']          = 'coldfusion8.zip'

# S3 bucket containing artifact
default['surfline-coldfusion']['coldfusion8']['bucket']  = case node.chef_environment
                                                           when 'dev'
                                                             'sl-artifacts-dev'
                                                           when 'prod'
                                                             'sl-artifacts-prod'
                                                           else
                                                             'sl-artifacts-staging'
                                                           end
default['surfline-science']['bucket']  = case node.chef_environment
                                                          when 'dev'
                                                            'sl-artifacts-dev'
                                                          when 'prod'
                                                            'sl-artifacts-prod'
                                                          else
                                                            'sl-artifacts-staging'
                                                          end


# coldfusion location_flag
default['surfline-coldfusion']['coldfusion8']['location_flag']  = case node.chef_environment
                                                                  when 'dev'
                                                                    'development'
                                                                  when 'prod'
                                                                    'production'
                                                                  else
                                                                    'staging'
                                                                  end

# temporary directory
default['surfline-coldfusion']['coldfusion8']['temp_dir']           = '/tmp/coldfusion8'

# ColdFusion 8 runtime settings
# attribute values must NOT contain any / character or anything else that breaks sed!!!
default['surfline-coldfusion']['coldfusion8']['missing_template']        = '404.html'
default['surfline-coldfusion']['coldfusion8']['sitewide_error_template'] = 'error.cfm'
default['surfline-coldfusion']['coldfusion8']['request_timeout']         = '60.0'
default['surfline-coldfusion']['coldfusion8']['queue_timeout']           = '60.0'
default['surfline-coldfusion']['coldfusion8']['post_size_limit']         = '100.0'
default['surfline-coldfusion']['coldfusion8']['request_limit']           = '30.0'
default['surfline-coldfusion']['coldfusion8']['post_parameters_limit']   = '1000.0'
default['surfline-coldfusion']['coldfusion8']['session_timeout_days']    = '0'
default['surfline-coldfusion']['coldfusion8']['session_timeout_hours']   = '0'
default['surfline-coldfusion']['coldfusion8']['session_timeout_minutes'] = '3'
default['surfline-coldfusion']['coldfusion8']['session_timeout_seconds'] = '0'

# Subscription Service(s)
default['surfline-coldfusion']['subs']['url'] = case node.chef_environment
                                                when 'prod'
                                                  'http://web-account.prod.surfline.com'
                                                else
                                                  'http://web-account.staging.surfline.com'
                                                end

default['surfline-coldfusion']['subs']['fishtrack_url'] = case node.chef_environment
                                                          when 'prod'
                                                            'http://ft-subs-service-fe-prod.us-west-1.elasticbeanstalk.com'
                                                          else
                                                            'http://ft-subs-svc-fe-staging.us-west-1.elasticbeanstalk.com'
                                                          end

# CF Static bucket
default['surfline-coldfusion']['static-bucket']['url']              = 'https://s3-us-west-1.amazonaws.com/sl-coldfusion-static-prod'

# Java
default['java']['install_flavor']                                   = 'oracle'
default['java']['jdk_version']                                      = '8'
default['java']['jdk']['8']['x86_64']['url']                        = 'https://s3-us-west-1.amazonaws.com/sl-artifacts-prod/coldfusion/jdk-8u221-linux-x64.tar.gz'
default['java']['jdk']['8']['x86_64']['checksum']                   = nil
default['java']['oracle']['accept_oracle_download_terms']           = true
default['java']['oracle']['jce']['8']['checksum']                   = 'f3020a3922efd6626c2fff45695d527f34a8020e938a49292561f18ad1320b59'
default['java']['oracle']['jce']['8']['url']                        = 'https://sl-artifacts-prod.s3-us-west-1.amazonaws.com/coldfusion/jce_policy-8.zip'
default['java']['oracle']['jce']['enabled']                         = false
default['java']['set_etc_environment']                              = true

# JVM
default['surfline-coldfusion']['jvm']['memory']['xms']              = '2048m'
default['surfline-coldfusion']['jvm']['memory']['xmx']              = '2048m'

# rsyncd - global config
default['rsyncd']['globals']['use chroot']                          = 'false'
default['rsyncd']['globals']['strict modes']                        = 'false'
default['rsyncd']['globals']['hosts allow']                         = '*'
default['rsyncd']['globals']['log file']                            = '/var/log/rsyncd'

# rsyncd - serves
default['surfline-coldfusion']['rsyncd']['serve']['surfline']                = '/var/www/sites/public-prod'
default['surfline-coldfusion']['rsyncd']['serve']['surflineApi']             = '/var/www/sites/public-api'
default['surfline-coldfusion']['rsyncd']['serve']['surflineDeveloper']       = '/var/www/sites/public-developer'
default['surfline-coldfusion']['rsyncd']['serve']['lolatips']                = '/var/www/sites/public-prod/surfline/lolatips/tipimages'
default['surfline-coldfusion']['rsyncd']['serve']['surflineAdmin']           = '/var/www/sites/admin-prod'
default['surfline-coldfusion']['rsyncd']['serve']['scripts']                 = '/var/www/scripts'
default['surfline-coldfusion']['rsyncd']['serve']['bwApiScripts']            = '/var/www/data/scripts'
default['surfline-coldfusion']['rsyncd']['serve']['surflineCFC']             = '/opt/coldfusion8/CustomTags'
default['surfline-coldfusion']['rsyncd']['serve']['surflineJava']            = '/opt/coldfusion8/java'
default['surfline-coldfusion']['rsyncd']['serve']['temp']                    = '/tmp/rsync'
default['surfline-coldfusion']['rsyncd']['serve']['surflineFullRepo']        = '/var/www/git/surfline-coldfusion'

# users / permissions
default['authorization']['sudo']['groups'] = ['sl.sudoers.all']

# New Relic
default['newrelic']['enabled'] = case node.chef_environment
                                 when 'prod'
                                   'true'
                                 else
                                   'false'
                                 end

default['newrelic']['enabled'] = case node.chef_environment
                                 when 'prod'
                                   'true'
                                 else
                                   'false'
                                 end

default['newrelic']['log_level'] = 'info'
default['newrelic']['log_limit_in_kbytes'] = '100000'
default['newrelic']['license'] = case node.chef_environment
                                 when 'prod'
                                   'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                 else
                                   'd88d91400d512eba67a0faeb922435db811b5728'
                                 end

default['newrelic-infra']['license_key'] = case node.chef_environment
                                           when 'prod'
                                             'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                           else
                                             'd88d91400d512eba67a0faeb922435db811b5728'
                                           end


default['newrelic_meetme_plugin']['license'] = case node.chef_environment
                                               when 'prod'
                                                 'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                               else
                                                 'd88d91400d512eba67a0faeb922435db811b5728'
                                               end
default['newrelic_meetme_plugin']['prefix'] = '/usr/local/bin'

default['surfline-common']['route53']['domain_name'] = 'aws.surfline.com'
default['surfline-common']['route53']['zone_id'] = 'ZWONYSN4O2R94'

# AWS / Route 53
default['route53']['domain_name'] = 'aws.surfline.com'
default['route53']['zone_id'] = 'ZWONYSN4O2R94'
