surfline-coldfusion CHANGELOG
=============================

2.20.8
-----
- [Paul Oginni] - Remove references to deprecated Beachlive infrastructure.

2.20.7
-----
- [Matt Walker] - Duplicate surfline-varnish:hostfile to circumvent deps issue

2.20.6
-----
- [Jon Price] - Updated marineapi to use Chef Zero

2.20.5
-----
- [Matt Walker] - Add newrelic-infra recipe

2.20.4
-----
- [Matt Sollie] - changed to vendored local sturdy packages

2.20.3
-----
- [Matt Sollie] - Removed retired MongoDB references

2.20.2
-----
- [Brandon Pierce] - Fix missing tile attributes and recipes
- [Brandon Pierce] - Add role-specific recipes for each ColdFusion role

2.20.1
-----
- [Matt Walker] - Fix cookbook issues from MongoDB not running in staging

2.20.0
-----
- [Matt Walker] - Add MongoDB connection string

2.19.6
-----
- [Jerry Warren] - Update override_sciencejobs with corrected mounts and recipe

2.19.5
-----
- [Jon Price] - Added fix for QA lola URLs

2.19.4
-----
- [Michael Malchak] - Replace front-end subscription EB urls, with the new ECS urls

2.19.3
-----
- [Matt Walker] - Add Fishtrack http=>https redirects

2.19.2
-----
- [Jerry Warren] - SNS-4715 - Remove cfstat from rollchecker.sh script

2.19.0
-----
- [Jon Price] - Switch to Chef Zero (Dropped data bags, node search via aws api)

2.18.1
-----
- [Ajith Manmadhan] - Add tile-origin.surfline.com as an accepted domain

2.18.0
-----
- [Brandon Pierce] - Switch to Oracle JDK

2.17.6
-----
- [Brandon Pierce] - Make New Relic logging parameters configurable

2.17.5
-----
- [Matt Walker] - Remove New Relic from non-prod CF environments

2.17.4
-----
- [Mike Shea] - No redirect for myaccount/logout.cfm (and query params)

2.17.3
-----
- [Brandon Pierce] - Remove `/buoyweather` redirect

2.17.2
-----
- [Brandon Pierce] - Switch to proper JRE package

2.17.1
-----
- [Mike Shea] - Remove old funnel related rewrite rules

2.16.2
-----
- [Brandon Pierce] - Add additional links to ocean in `tile` recipe

2.16.1
-----
- [Gavin Cooper] - Added state-parks URL redirect to caexplorer.surfline.com.

2.16.0
-----
- [Brandon Pierce] - Bump version dependency of `surfline-common`
- [Brandon Pierce] - Switch from `surfline-common::critsend` to `surfline-common::ses` recipe

2.15.1
-----
- [Mike Shea] - Add config entry `for my-staging.fishtrack.com`

2.15.0
-----
- [Bryan Anderson] - Updated `rollchecker.sh` to include a force option and detect last run time

2.14.0
-----
- [Brandon Pierce] - Add dynamically generated per-environment rewrite rules for FishTrack related domains
- [Brandon Pierce] - Add subscription service frontend related RewriteRules for FishTrack

2.13.0
- [Brandon Pierce] - Add `*.buoyweather.com` VirtualHost alias to point to same site as `my.buoyweather.com`
- [Mike Shea] - add location_flag value to cf config and local_settings erb

2.12.0
-----
- [Brandon Pierce] - Switch `build_datasource_conf.sh` to use `|` instead of `/` in `sed`

2.11.1
-----
- [Matt Walker] - Make account rewrite regex less inclusive in `www.conf.erb` template for public site

2.11.0
-----
- [Brandon Pierce] - Add `api-int` New Relic templates to support new internal API server pool

2.10.1
-----
- [Brandon Pierce] - Add sandbox CF server to domain rewrite whitelist
- [Glen Thompson] - BW-1550 Add tracking parameter to trial vanity URL

2.10.0
-----
- [Bryan Anderson] - Add cancel, account, and upgrade routes to apache configuration

2.9.9
-----
- [Brandon Pierce] - Add missing sandbox related recipe and configs

2.9.8
-----
- [Bryan Anderson] - Add `monitor@surfline.com` email to roll script
- [Brandon Pierce] - Add custom Apache logs to `logrotate` recipe

2.9.7
-----
- [Brandon Pierce] - Add missing symlink in `qa` recipe

2.9.6
-----
- [Bryan Anderson] - Add forecaster email notification for roll script

2.9.5
-----
- [Matt Walker] - Configure `neo-runtime.xml` using node attributes

2.9.4
-----
- [Brandon Pierce] - Add `makecoast.sh` script to `tile` recipe
- [Brandon Pierce] - Add missing symlink in `tile` recipe
- [Matt Walker] - Fix static asset rewrite for tile

2.9.3
-----
- [Brandon Pierce] - Merge individual Fishtrack sites into single Apache configuration template
- [Brandon Pierce] - Cleanup Fishtrack site configuration templates
- [Brandon Pierce] - Fix logging for Fishtrack
- [Brandon Pierce] - Add missing symlink for Beachlive

2.9.2
-----
- [Brandon Pierce] - Set Surfline public site as default Apache Virtual Host for tile server

2.9.1
-----
- [Brandon Pierce] - Add missing links and data directories for the `marineapi` recipe
- [Brandon Pierce] - Add missing Marine API scripts to the `marineapi` recipe
- [Brandon Pierce] - Add missing climateData link in the `qa` recipe
- [Brandon Pierce] - Install 32-bit GLIBC in the `marineapi` recipe to support older tools

2.9.0
-----
- [Brandon Pierce] - Resolve numerous naming issues with ColdFusion apps in New Relic

2.8.1
-----
- [Brandon Pierce] - Sort `mongo_servers` in `coldfusion8` recipe to prevent excessive ColdFusion restarts
- [Brandon Pierce] - Set Surfline public site as default Apache Virtual Host for jobs server
- [Brandon Pierce] - Add jobs server ELB name to domain exclusion for mod_rewrite rules

2.8.0
-----
- [Brandon Pierce] - Fix logging for admin site
- [Brandon Pierce] - Add additional listener for admin site

2.7.1
-----
- [Matt Walker] - Add QA CF servers to domain whitelist
- [Brandon Pierce] - Add `neo-cron-sync` recipe for syncing neo-cron.xml to S3
- [Brandon Pierce] - Add new attribute `['surfline-coldfusion']['service']`
- [Brandon Pierce] - Exclude `neo-cron.xml` from ColdFusion extraction/installation
- [Brandon Pierce] - Add missing links for `qa` recipe

2.7.0
-----
- [Brandon Pierce] - Deploy API local_settings to all `server_type`
- [Brandon Pierce] - Add CFIDE site

2.6.1
-----
- [Brandon Pierce] - Add missing `/var/www/auth/htpasswd` entry in `httpd` recipe
- [Matt Walker] - Add Apache ServerAlias entries for qa and vindiciatest

2.6.0
-----
- [Brandon Pierce] - Add per-environment ServerAlias for `www` Apache site template
- [Bryan Anderson] - Add per-environment .surfline.com exclusion to mod rewrite config

2.5.2
-----
- [Brandon Pierce] - Include `surfline-common::hostfile` recipe for adding data center servers to /etc/hosts

2.5.1
-----
- [Matt Walker] - Update `rollchecker.sh` script to work with all CF instance types
- [Matt Walker] - Remove `rollchecker.sh-ap[i|p]` scripts

2.5.0
-----
- [Brandon Pierce] - Add `homepage` recipe for homepage generation instance
- [Brandon Pierce] - Add `qa` recipe for QA
- [Brandon Pierce] - Add `vindiciatest` recipe for vindiciatest
- [Brandon Pierce] - Add `fishtrack` recipe for FishTrack
- [Brandon Pierce] - Add `marineapi` recipe for Marine API
- [Brandon Pierce] - Add `tile` recipe for tile instances
- [Brandon Pierce] - Add `beachlive` recipe for Beachlive

2.4.0
-----
- [Brandon Pierce] - Include surfline-varnish::hostfile recipe to workaround hard-coded Varnish hostnames in CF code
- [Brandon Pierce] - Add `curl` output to the roll checker script

2.3.1
-----
- [Brandon Pierce] - Fix several script issues referencing incorrect jstack / jmap
- [Brandon Pierce] - Include surfline-common::critsend recipe for proper email delivery
- [Brandon Pierce] - Ensure correct permissions on log files in /opt/coldfusion8/logs

2.3.0
-----
- [Matt Walker] - Split my.buoyweather.com into separate VirtualHost

2.2.2
-----
- [Oren Maor] - Redirects HTTP to HTTPS for user account related endpoints

2.2.1
-----
- [Brandon Pierce] - Fix timezone
- [Brandon Pierce] - Fix rewrite rule for tile server

2.2.0
-----
- [Brandon Pierce] - Add mod_rewrite rule to serve files from AWS that don't exist locally
- [Brandon Pierce] - Include resolver recipe to allow for DNS lookups to data center

2.1.0
-----
- [Brandon Pierce] - Fallback to Prod MongoDB servers if none exist in the current environment
- [Brandon Pierce] - Make New Relic application names environment-specific
- [Brandon Pierce] - Add revised mod_rewrite rules provided by Matt Walker

2.0.0
-----
- [Brandon Pierce] - Drop management of CF code base in favor of further local_settings.cfm usage
- [Brandon Pierce] - Drop adding MongoDB servers to /etc/hosts
- [Brandon Pierce] - Rebuild neo datasource on zip extraction
- [Brandon Pierce] - Cleanup formatting of misc CF scripts
- [Brandon Pierce] - Fix rollchecker to use per-role roll scripts
- [Brandon Pierce] - Add cron job for rollchecker
- [Brandon Pierce] - Refresh neo-datasource template from Matt Walker's post-AWS change

1.18.1
-----
- [Brandon Pierce] - Add missing proxy related modules for Apache

1.18.0
-----
- [Brandon Pierce] - redirect user account related endpoints to AWS Subscription Service

1.17.0
-----
- [Brandon Pierce] - Switch to using Oracle JRE

1.16.0
-----
- [Brandon Pierce] - Add rollchecker, rollmx, and other CF scripts

1.15.2
-----
- [Brandon Pierce] - Add missing directory for java.policy
- [Brandon Pierce] - Properly re-order include_recipe statements in default recipe
- [Brandon Pierce] - Add missing Apache modules
- [Brandon Pierce] - Conditionally create templates based on role
- [Brandon Pierce] - Foodcritic cleanups

1.15.1
-----
- [Brandon Pierce] - Add missing host.cfm

1.15.0
-----
- [Brandon Pierce] - Replace neo-datasource.xml generate with Matt Walker's template builder

1.14.0
-----
- [Brandon Pierce] - Add license file management

1.13.1
-----
- [Brandon Pierce] - Fix issue where ColdFusion artifact was being downloaded every Chef run
- [Brandon Pierce] - Change permissions on neo-datasource.xml template to prevent ColdFusion restart every Chef run
- [Brandon Pierce] - Override ['apache']['default_modules'] to prevent conflict with custom module configurations
- [Brandon Pierce] - Update README.md to include missing cookbook dependencies

1.13.0
-----
- [Brandon Pierce] - Add logrotate support

1.12.1
-----
- [Brandon Pierce] - Fix incorrect / excessive enablement / starts of ColdFusion service

1.12.0
-----
- [Brandon Pierce] - Add MongoDB connection settings

1.11.0
-----
- [Brandon Pierce] - Add additional Redis connection settings

1.10.0
-----
- [Brandon Pierce] - Add additional 'local settings' files

1.9.0
-----
- [Brandon Pierce] - Add additional datasource configurations

1.8.3
-----
- [Brandon Pierce] - Fix incorrect use of PASS vs PASS_ENC in datasources

1.8.2
-----
- [Brandon Pierce] - Fix New Relic yml template for application name

1.8.1
-----
- [Brandon Pierce] - Fix issue with java.policy templates

1.8.0
-----
- [Brandon Pierce] - Add neo-datasource.xml configuration from encrypted data bag

1.7.0
-----
- [Brandon Pierce] - Add ColdFusion 8 local_settings.cfm for API server types

1.6.0
-----
- [Brandon Pierce] - Add New Relic recipe and matching JVM permissions

1.5.0
-----
- [Brandon Pierce] - Add new node attribute for determining server "type"

1.4.1
-----
- [Brandon Pierce] - Add missing rsyncd serve and permission creation

1.4.0
-----
- [Brandon Pierce] - Add extensive Apache configurations

1.3.0
-----
- [Brandon Pierce] - Add rsyncd cookbook dependency and configure for Jenkins

1.2.0
-----
- [Brandon Pierce] - Add Java cookbook dependency and configure Java environment

1.1.0
-----
- [Brandon Pierce] - Add basic ColdFusion 8 deployment recipe

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-coldfusion
