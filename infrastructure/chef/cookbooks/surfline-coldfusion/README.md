surfline-coldfusion Cookbook
============================
Deploys ColdFusion and dependencies with specific roles for ColdFusion
implementation types (API, App, etc)

Requirements
------------

#### cookbooks
- `apache2` - surfline-coldfusion needs apache2 to install and manage Apache 2
- `java` - surfline-coldfusion needs Java for ColdFusion
- `logrotate` - surfline-coldfusion needs logrotate for managing log files
- `newrelic` - surfline-coldfusion needs newrelic for monitoring
- `rsync` - surfline-coldfusion needs rsyncd for deployments from Jenkins
- `s3_file` - surfline-coldfusion needs S3 for downloading artifacts from S3
- `sed` - surfline-coldfusion needs sed for ad-hoc find-and-replace in various files
- `surfline-varnish` - surfline-coldfusion needs surfline-varnish::hostfile for updating Varnish entries in /etc/hosts

Attributes
----------

#### surfline-coldfusion::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['installation_dir']</tt></td>
    <td>String</td>
    <td>installation directory</td>
    <td><tt>/opt/coldfusion8</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['installer']</tt></td>
    <td>String</td>
    <td>name of installer</td>
    <td><tt>coldfusion8.zip</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['bucket']</tt></td>
    <td>String</td>
    <td>S3 bucket containing artifact</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['temp_dir']</tt></td>
    <td>String</td>
    <td>temporary directory</td>
    <td><tt>/tmp/coldfusion8</tt></td>
  </tr>
  <tr>
    <td><tt>['java']['jdk_version'] </tt></td>
    <td>String</td>
    <td>OpenJDK version</td>
    <td><tt>6</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['jvm']['memory']['xms']</tt></td>
    <td>String</td>
    <td>JVM Xms</td>
    <td><tt>2048m</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['healthcheck_type']</tt></td>
    <td>String</td>
    <td>healthcheck type</td>
    <td><tt>jrun</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['coldfusion8']['server_type']</tt></td>
    <td>String</td>
    <td>server type</td>
    <td><tt>app</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['service']</tt></td>
    <td>String</td>
    <td>server type</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['rsyncd']['globals']</tt></td>
    <td>Hash</td>
    <td>Global Rsyncd options</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-coldfusion']['rsyncd']['serve']</tt></td>
    <td>Hash</td>
    <td>Rsyncd serves</td>
    <td><tt></tt></td>
  </tr>
</table>

Usage
-----
#### surfline-coldfusion::default

Just include `surfline-coldfusion` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-coldfusion]"
  ]
}
```


Cookbook Deployment
-----

### Testing

#### List "suites" or roles for ColdFusion

```shell
$ kitchen list
Instance                  Driver  Provisioner  Verifier  Transport  Last Action    Last Error
app-amazon-linux          Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
api-amazon-linux          Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
api-int-amazon-linux      Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
admin-amazon-linux        Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
fishtrack-amazon-linux    Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
jobs-amazon-linux         Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
marineapi-amazon-linux    Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
tile-amazon-linux         Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
qa-amazon-linux           Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
sciencejobs-amazon-linux  Ec2     ChefZero     Inspec    Rsync      <Not Created>  <None>
```


#### Spin up a Test Kitchen instance

_Note: due to a race condition in userdata execution vs. Chef run these steps must be run independently_

```shell
kitchen create <some suite>
kitchen exec <some suite> -c "tail -F /var/log/cloud-init-output.log"
```

Once you see the line _Retrieving ColdFusion XML_, even if it is followed by a _fatal error_, testing may continue.

```shell
kitchen verify <some suite>
```


### Packaging

```shell
export BERKS_PACKAGE=$(basename $(berks package | awk ' { print $4 } '))
```


### Uploading

_Example for fishtrack in staging_

```shell
export CHEF_ENV="staging"
export APPLICATION="coldfusion"
export SERVICE="fishtrack"
aws s3 cp ${BERKS_PACKAGE} s3://sl-chef-${CHEF_ENV}/${APPLICATION}-${SERVICE}/config/
rm cookbooks-*.tar.gz
````

### Encrypting DSN Passwords

Use the utility at
<https://github.com/Surfline/wavetrak-infrastructure/tree/master/scripts/cf-dsn-password-encryptor/>
to encrypt DSN passwords. Then store them in AWS Parameter Store.
