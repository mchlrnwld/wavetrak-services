#
# Cookbook Name:: surfline-coldfusion
# Recipe:: scripts
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# mail utility
package 'mailx'

# roll script
template '/usr/local/bin/rollmx.sh' do
  source    'scripts/rollmx.sh.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# roll checker script
template '/usr/local/bin/rollchecker.sh' do
  source    'scripts/rollchecker.sh.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# cfheapdump script
template '/usr/local/bin/cfheapdump' do
  source    'scripts/cfheapdump.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# cfheapinfo script
template '/usr/local/bin/cfheapinfo' do
  source    'scripts/cfheapinfo.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# cfjstack script
template '/usr/local/bin/cfjstack' do
  source    'scripts/cfjstack.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# cfjstackfull script
template '/usr/local/bin/cfjstackfull' do
  source    'scripts/cfjstackfull.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# cfjstackshort script
template '/usr/local/bin/cfjstackshort' do
  source    'scripts/cfjstackshort.erb'
  owner     'root'
  group     'root'
  mode      0755
end

# colorcfstat script
template '/usr/local/bin/colorcfstat' do
  source    'scripts/colorcfstat.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
end

# rollchecker
cron_d 'rollchecker' do
  minute    '*/10'
  command   '/usr/local/bin/rollchecker.sh'
end
