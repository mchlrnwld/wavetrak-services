#
# Cookbook Name:: surfline-coldfusion
# Recipe:: newrelic
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# configure New Relic
template "#{node['surfline-coldfusion']['coldfusion8']['installation_dir']}" + '/java/newrelic.yml' do
  source    'newrelic/newrelic-' + "#{node['surfline-coldfusion']['service']}"+ '.yml.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :enabled => node['newrelic']['enabled'],
    :log_level => node['newrelic']['log_level'],
    :log_limit_in_kbytes => node['newrelic']['log_limit_in_kbytes'],
    :license => node['newrelic']['license']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
end
