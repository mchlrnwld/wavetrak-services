#
# Cookbook Name:: surfline-coldfusion
# Recipe:: logrotate
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'logrotate'

# apache log rotation
logrotate_app 'httpd-surfline' do
  path          '/var/log/httpd/access-*'
  options       [ 'missingok', 'notifempty', 'sharedscripts', 'delaycompress' ]
  postrotate    [ '/sbin/service httpd reload > /dev/null 2>/dev/null || true' ]
  rotate        0
end

# coldfusion log rotation
logrotate_app 'coldfusion8' do
  path          '/opt/coldfusion8/logs/*.log'
  frequency     'daily'
  options       ['missingok', 'copytruncate']
  rotate        0
  size          '50m'
end

# rsyncd log rotation
logrotate_app 'rsyncd' do
  path          "#{node['rsyncd']['globals']['log file']}"
  options       ['missingok', 'copytruncate']
  rotate        0
  size          '50m'
end
