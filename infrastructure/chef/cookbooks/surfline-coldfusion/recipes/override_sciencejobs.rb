#
# Cookbook Name:: surfline-coldfusion
# Recipe:: override_jobs
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe "#{cookbook_name}::jobs"

# NOTE: This is not the normal way o override attributes - this is just a way
#       we are replicating the chef_role functionality (overrides)
node.default['surfline-coldfusion']['service'] = 'jobs'
node.default['surfline-coldfusion']['server_type'] = 'app'
node.default['surfline-coldfusion']['healthcheck_type'] = 'jrun'

node.default['surfline-coldfusion']['apache']['config'] = {
  'app' => 'true',
  'domain_mod_rewrite' => 'true',
  'ft_mod_rewrite' => 'true',
  'mod_rewrite' => 'true',
  'sl_regional_mod_rewrite' =>  'true',
  'sladmin' => 'true'
}

node.default['surfline-coldfusion']['apache']['sites'] = {
  'backup-admin' => 'true',
  'brasil' => 'true',
  'cfide' => 'true',
  'fishtrack' => 'true',
  'mobile' => 'true',
  'my.buoyweather' => 'true',
  'sladmin' => 'true',
  'www' => 'true'
}

node.default['newrelic_meetme_plugin']['services']['apache_httpd'] = {
  'scheme' => 'http',
  'host' => 'localhost',
  'port' => '80',
  'path' => '/server-status'
}

node.default['surfline-coldfusion']['coldfusion8']['post_size_limit'] = '40000.0'
node.default['surfline-coldfusion']['coldfusion8']['request_limit'] = '40.0'
node.default['surfline-coldfusion']['coldfusion8']['post_parameters_limit'] = '10000.0'

node.default['surfline-nfs']['client4']['mounts'] = case node.chef_environment
                                                    when 'prod'
                                                      {
                                                        'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean2_fs',
                                                        'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata2'
                                                      }
                                                    else
                                                      {
                                                        'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs',
                                                        'dev-nfs-primary-1.aws.surfline.com:/sciencedata_aws_dev' => '/sciencedata2'}
                                                    end

node.default['surfline-nfs']['client4']['options'] = 'rw'

include_recipe "surfline-nfs::client4"
