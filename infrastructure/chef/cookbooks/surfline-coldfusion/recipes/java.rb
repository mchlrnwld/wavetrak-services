#
# Cookbook Name:: surfline-coldfusion
# Recipe:: java
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# install Java
include_recipe 'java'

# extend runtime permissions for JVM
template "#{node['java']['java_home']}/jre/lib/security/java.policy" do
  source 'java/java.policy.erb'
end

directory "#{node['surfline-coldfusion']['coldfusion8']['installation_dir']}/runtime/jre/lib/security" do
  recursive true
end

template "#{node['surfline-coldfusion']['coldfusion8']['installation_dir']}/runtime/jre/lib/security/java.policy" do
  source 'java/java.policy.erb'
end

# configure java security files
remote_file '/usr/lib/jvm/jdk1.8.0_221/jre/lib/security/java.policy' do
  source 'https://sl-artifacts-prod.s3-us-west-1.amazonaws.com/coldfusion/jdk1.8.0_221-java.policy'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

remote_file '/usr/lib/jvm/jdk1.8.0_221/jre/lib/security/java.security' do
  source 'https://sl-artifacts-prod.s3-us-west-1.amazonaws.com/coldfusion/jdk1.8.0_221-java.security'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end
