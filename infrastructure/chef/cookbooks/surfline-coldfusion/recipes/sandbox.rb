#
# Cookbook Name:: surfline-coldfusion
# Recipe:: sandbox
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# set default Apache site
link '/etc/httpd/sites-enabled/00default.conf' do
  to  '/etc/httpd/sites-available/www.conf'
end
