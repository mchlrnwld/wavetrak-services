#
# Cookbook Name:: surfline-coldfusion
# Recipe:: neo-cron-sync
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# neo-cron-sync script
template '/usr/local/bin/neo-cron-sync' do
  source 'neo-cron-sync/neo-cron-sync.erb'
  mode    0755
  owner  'root'
  group  'root'
end

# neo-cron-sync cron job
cron_d 'neo-cron-sync' do
  minute    '*/30'
  command   '/usr/local/bin/neo-cron-sync'
end
