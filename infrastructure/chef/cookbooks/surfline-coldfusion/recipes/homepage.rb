#
# Cookbook Name:: surfline-coldfusion
# Recipe:: homepage
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# homepage generate script
template '/usr/local/bin/gen_and_deploy_sl_homepage_aws' do
  source      'homepage/gen_and_deploy_sl_homepage_aws.erb'
  owner       'root'
  group       'root'
  mode         0755
  sensitive    true
  variables ({
    :environment => "#{node.chef_environment}"
  })
end

# homepage generate cron job
# cron_d 'gen_and_deploy_sl_homepage_aws' do
#   minute    '*/5'
#   command   '/usr/local/bin/gen_and_deploy_sl_homepage_aws &> /var/log/job-gen_and_deploy_sl_homepage_aws'
# end
