#
# Cookbook Name:: surfline-coldfusion
# Recipe:: override_marineapi
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe "#{cookbook_name}::marineapi"
include_recipe "surfline-science::grads"
include_recipe "surfline-science::coastwatch"

# NOTE: This is not the normal way o override attributes - this is just a way
#       we are replicating the chef_role functionality (overrides)
node.default['surfline-coldfusion']['service'] = 'marineapi'
node.default['surfline-coldfusion']['server_type'] = 'api'
node.default['surfline-coldfusion']['healthcheck_type'] = 'apisettings'
node.default['surfline-coldfusion']['apache']['config'] = {
  'api' => 'true',
  'api_mod_rewrite' => 'true',
  'domain_mod_rewrite' => 'true',
  'mod_rewrite' => 'true',
  'sl_regional_mod_rewrite' =>  'true'
}
node.default['surfline-coldfusion']['apache']['sites'] = {
  'api' => 'true'
}

node.default['surfline-nfs']['client4']['mounts'] = case node.chef_environment
                                                    when 'prod'
                                                      {
                                                        'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean2_fs',
                                                        'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata2'
                                                      }
                                                    else
                                                      { 'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs' }
                                                    end

node.default['surfline-nfs']['client4']['options'] = 'rw'


node.default['surfline-coldfusion']['coldfusion8']['request_timeout'] = '60.0'
node.default['surfline-coldfusion']['coldfusion8']['queue_timeout'] = '60.0'
node.default['surfline-coldfusion']['coldfusion8']['missing_template'] = '/404.html'
node.default['surfline-coldfusion']['coldfusion8']['sitewide_error_template'] = '/error.cfm'
node.default['surfline-coldfusion']['coldfusion8']['post_size_limit'] = '40000.0'
node.default['surfline-coldfusion']['coldfusion8']['request_limit'] = '30.0'
node.default['surfline-coldfusion']['coldfusion8']['post_parameters_limit'] = '1000.0'

include_recipe "surfline-nfs::client4"
