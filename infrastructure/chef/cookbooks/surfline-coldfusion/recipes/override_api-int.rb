#
# Cookbook Name:: surfline-coldfusion
# Recipe:: override_api-int
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# NOTE: This is not the normal way o override attributes - this is just a way
#       we are replicating the chef_role functionality (overrides)
node.default['surfline-coldfusion']['service'] = 'api-int'
node.default['surfline-coldfusion']['server_type'] = 'api'
node.default['surfline-coldfusion']['healthcheck_type'] = 'apisettings'
node.default['surfline-coldfusion']['apache']['config'] = {
  'api' => 'true',
  'api_mod_rewrite' => 'true',
  'domain_mod_rewrite' => 'true',
  'mod_rewrite' => 'true',
  'sl_regional_mod_rewrite' =>  'true'
}
node.default['surfline-coldfusion']['apache']['sites'] = {
  'api' => 'true'
}
# node.default['surfline-coldfusion']['coldfusion8']['request_timeout'] = '120.0'
# node.default['surfline-coldfusion']['coldfusion8']['queue_timeout'] = '120.0'
node.default['surfline-coldfusion']['coldfusion8']['post_size_limit'] = '40000.0'
node.default['surfline-coldfusion']['coldfusion8']['request_limit'] = '30.0'
node.default['surfline-coldfusion']['coldfusion8']['post_parameters_limit'] = '10000.0'
