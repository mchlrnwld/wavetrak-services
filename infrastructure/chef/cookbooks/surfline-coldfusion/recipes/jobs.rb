#
# Cookbook Name:: surfline-coldfusion
# Recipe:: jobs
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-coldfusion::neo-cron-sync'

# NOTE: Having this enabled was causing a conflict. With it disabled we are not having issues.
# set default Apache site
# link '/etc/httpd/sites-enabled/00default.conf' do
#   to  '/etc/httpd/sites-available/www.conf'
# end
