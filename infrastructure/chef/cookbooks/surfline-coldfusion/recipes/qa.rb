#
# Cookbook Name:: surfline-coldfusion
# Recipe:: qa
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-science::scripts'

directory '/var/www/data/shared' do
  recursive true
end

directory '/var/www/sites/public-prod/lola/climateCharts' do
  recursive true
  mode '0777'
end

# setup links to various data
links = {
  '/ocean/climate/' => '/var/www/sites/public-prod/lola/climateData',
  '/ocean/live/chloro' => '/var/www/data/shared/chloro',
  '/ocean/live/cwatchAU' => '/var/www/data/shared/cwatchAU',
  '/ocean/live/cwatchfull' => '/var/www/data/shared/cwatchfull',
  '/ocean/live/cwatchtn' => '/var/www/data/shared/cwatchtn',
  '/ocean/live/cwatchtnbw5' => '/var/www/data/shared/cwatchtn',
  '/ocean/live/jplft' => '/var/www/data/shared/jplft',
  '/ocean/live/modisfthdf' => '/var/www/data/shared/modisfthdf',
  '/ocean/live/modisftnc' => '/var/www/data/shared/modisftnc',
  '/ocean/live/rtofsft' => '/var/www/data/shared/rtofsft',
  '/ocean' => '/ocean.snarf2'
}

links.each do |source, target|
  link target do
    to source
  end
end
