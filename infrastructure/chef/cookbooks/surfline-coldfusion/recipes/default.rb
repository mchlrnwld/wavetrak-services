#
# Cookbook Name:: surfline-coldfusion
# Recipe:: default
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-iam-ssh'
include_recipe 'timezone-ii'
include_recipe 'surfline-coldfusion::java'
include_recipe 'surfline-coldfusion::rsyncd'
include_recipe 'surfline-coldfusion::coldfusion8'
include_recipe 'surfline-coldfusion::hostfile'
include_recipe 'surfline-coldfusion::httpd'
include_recipe 'surfline-coldfusion::newrelic'
include_recipe 'surfline-coldfusion::logrotate'
include_recipe 'surfline-coldfusion::scripts'
include_recipe 'surfline-common::record_analytics'
include_recipe 'surfline-common::hostfile'
include_recipe 'surfline-common::ses'
include_recipe 'sturdy_platform'

include_recipe 'newrelic' unless ENV['TEST_KITCHEN']
include_recipe 'newrelic-infra' unless ENV['TEST_KITCHEN']
# include_recipe 'newrelic_meetme_plugin' unless ENV['TEST_KITCHEN']
