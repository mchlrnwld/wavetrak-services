#
# Cookbook Name:: surfline-coldfusion
# Recipe:: rsyncd
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

params = {}

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

params = aws_parameter_store('us-west-1')["/#{environment}/coldfusion/*"]

# install rsync server
include_recipe 'rsync::server'

# configure rsyncd secrets
template '/etc/rsyncd.secrets' do
  source            'rsyncd/rsyncd.secrets.erb'
  owner             'root'
  group             'root'
  mode              0750
  sensitive         true
  variables ({
    :username => params["/staging/coldfusion/rsync_user"],
    :password => params["/staging/coldfusion/rsync_pass"]
  })
end

# configure rsyncd serves
node['surfline-coldfusion']['rsyncd']['serve'].each do |name, path|
  # create serve directory
  directory name do
    path              path
    owner             'nobody'
    group             'nobody'
    mode              0755
    recursive         true
  end
  # create rsyncd.conf serve entry
  rsync_serve name do
    path              path
    read_only         false
    list              false
    secrets_file      '/etc/rsyncd.secrets'
  end
end
