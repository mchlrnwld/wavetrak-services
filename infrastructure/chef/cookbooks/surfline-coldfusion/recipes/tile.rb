#
# Cookbook Name:: surfline-coldfusion
# Recipe:: tile
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-nfs::client4'

include_recipe 'surfline-science::gmt'
include_recipe 'surfline-science::grads'
include_recipe 'surfline-science::imagemagick'

# set default Apache site
link '/etc/httpd/sites-enabled/00default.conf' do
  to  '/etc/httpd/sites-available/www.conf'
end

# makecoast.sh script
template '/usr/local/bin/makecoast.sh' do
  source    'tile/makecoast.sh.erb'
  owner     'root'
  group     'root'
  mode      0755
end

link '/var/www/surfline_htdocs' do
  to '/var/www/sites/public-prod'
end

link '/var/www/sites/public-prod/coastline/charts' do
  to '/ocean2_fs/tile/coastline/charts'
end

link '/var/www/sites/public-prod/dashboard/dashboards' do
  to '/ocean2_fs/tile/dashboard/dashboards'
end

link '/var/www/sites/public-prod/dashboard/rose' do
  to '/ocean2_fs/tile/dashboard/rose'
end

link '/var/www/sites/public-prod/dashboard/buoys' do
  to '/ocean2_fs/tile/dashboard/buoys'
end

link '/var/www/sites/public-prod/dashboard/swells' do
  to '/ocean2_fs/tile/dashboard/swells'
end
