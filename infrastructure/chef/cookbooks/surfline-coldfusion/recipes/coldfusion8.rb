#
# Cookbook Name:: surfline-coldfusion
# Recipe:: coldfusion8
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

# register the cfsearch8 service in Chef
service 'cfsearch8' do
  supports :status => true, :start => true
  action :nothing
end

# create and enable the cfsearch8 service
template '/etc/init.d/cfsearch8' do
  source    'init.d/cfsearch8.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
  notifies  :enable, 'service[cfsearch8]', :delayed
  notifies  :start, 'service[cfsearch8]', :delayed
end

# register the coldfusion_8 service in Chef
service 'coldfusion_8' do
  supports :status => true, :start => true, :restart => true
  action :nothing
end

# create and enable the coldfusion_8 service
template '/etc/init.d/coldfusion_8' do
  source    'init.d/coldfusion_8.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir']
  })
  notifies  :enable, 'service[coldfusion_8]', :delayed
  notifies  :start, 'service[coldfusion_8]', :delayed
end

include_recipe 's3_file'

directory "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}"

# download artifact from S3
s3_file "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + "/" + "#{node['surfline-coldfusion']['coldfusion8']['installer']}" do
  bucket        "#{node['surfline-coldfusion']['coldfusion8']['bucket'] }"
  remote_path   '/coldfusion/' + "#{node['surfline-coldfusion']['coldfusion8']['installer']}"
  notifies :run, 'execute[extract-zip]', :immediately
end

# extract into web root
execute 'extract-zip' do
  command "
    set -x
    unzip -o #{node['surfline-coldfusion']['coldfusion8']['temp_dir']}/#{node['surfline-coldfusion']['coldfusion8']['installer']} -d #{node['surfline-coldfusion']['coldfusion8']['installation_dir']} -x '*neo-cron.xml' ;
    chown -R nobody:nobody #{node['surfline-coldfusion']['coldfusion8']['installation_dir']} ;
    chmod -R g+r #{node['surfline-coldfusion']['coldfusion8']['installation_dir']}
  "
  notifies :run, 'execute[build-datasource-conf]', :delayed
  notifies :run, 'execute[build-runtime-conf]', :delayed
end

# configure JVM
template "#{node['surfline-coldfusion']['coldfusion8']['installation_dir']}" + "/" + 'runtime/bin/jvm.config' do
  source    'coldfusion/jvm.config.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :javahome => node['java']['java_home'],
    :xms => node['surfline-coldfusion']['jvm']['memory']['xms'],
    :xmx => node['surfline-coldfusion']['jvm']['memory']['xmx']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
end

# configure license
template "#{node['surfline-coldfusion']['coldfusion8']['installation_dir']}" + '/lib/license.properties' do
  source    'coldfusion/license.properties.erb'
  owner     'nobody'
  group     'nobody'
  mode      0755
  notifies  :restart, 'service[coldfusion_8]', :delayed
  not_if "wc -l /opt/coldfusion8/lib/license.properties | grep 12"
end

directory "#{node['surfline-coldfusion']['rsyncd']['serve']['surfline']}"

# configure common local settings
template "#{node['surfline-coldfusion']['rsyncd']['serve']['surfline']}" + '/localSettings.cfm' do
  source    source    'coldfusion/local_settings.cfm.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :hostname => node['hostname'],
    :PROD_CALENDAR_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_user"],
    :PROD_CALENDAR_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_pass"],
    :PROD_SESSION_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_user"],
    :PROD_SESSION_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_pass"],
    :PROD_SURFCOMPANIES_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_user"],
    :PROD_SURFCOMPANIES_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_pass"],
    :PROD_SURFLINE_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_user"],
    :PROD_SURFLINE_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_pass"],
    :RedisHost => aws_parameter_store["/#{environment}/coldfusion/redis_host"],
    :MongoConnectionString => aws_parameter_store["/#{environment}/coldfusion/mongo_connection_string"],
    :LOCATION_FLAG => node['surfline-coldfusion']['coldfusion8']['location_flag']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
end

template "#{node['surfline-coldfusion']['rsyncd']['serve']['surfline']}" + '/util/host.cfm' do
  source    source    'coldfusion/host.cfm.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :hostname => node['hostname']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
  only_if { node['surfline-coldfusion']['server_type'] == 'app' }
end

template "#{node['surfline-coldfusion']['rsyncd']['serve']['surfline']}" + '/util/hostname.cfm' do
  source    source    'coldfusion/hostname.cfm.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :hostname => node['hostname']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
  only_if { node['surfline-coldfusion']['server_type'] == 'app' }
end

directory "#{node['surfline-coldfusion']['rsyncd']['serve']['surflineAdmin']}"

template "#{node['surfline-coldfusion']['rsyncd']['serve']['surflineAdmin']}" + '/localSettings.cfm' do
  source    source    'coldfusion/local_settings.cfm.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :hostname => node['hostname'],
    :PROD_CALENDAR_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_user"],
    :PROD_CALENDAR_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_pass"],
    :PROD_SESSION_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_user"],
    :PROD_SESSION_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_pass"],
    :PROD_SURFCOMPANIES_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_user"],
    :PROD_SURFCOMPANIES_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_pass"],
    :PROD_SURFLINE_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_user"],
    :PROD_SURFLINE_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_pass"],
    :RedisHost => aws_parameter_store["/#{environment}/coldfusion/redis_host"],
    :MongoConnectionString => aws_parameter_store["/#{environment}/coldfusion/mongo_connection_string"],
    :LOCATION_FLAG => node['surfline-coldfusion']['coldfusion8']['location_flag']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
end

directory "#{node['surfline-coldfusion']['rsyncd']['serve']['surflineApi']}/include"

# configure local settings for API
template "#{node['surfline-coldfusion']['rsyncd']['serve']['surflineApi']}" + '/include/local_settings.cfm' do
  source    'coldfusion/local_settings.cfm.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :hostname => node['hostname'],
    :PROD_CALENDAR_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_user"],
    :PROD_CALENDAR_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_pass"],
    :PROD_SESSION_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_user"],
    :PROD_SESSION_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_pass"],
    :PROD_SURFCOMPANIES_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_user"],
    :PROD_SURFCOMPANIES_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_pass"],
    :PROD_SURFLINE_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_user"],
    :PROD_SURFLINE_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_pass"],
    :RedisHost => aws_parameter_store["/#{environment}/coldfusion/redis_host"],
    :MongoConnectionString => aws_parameter_store["/#{environment}/coldfusion/mongo_connection_string"],
    :LOCATION_FLAG => node['surfline-coldfusion']['coldfusion8']['location_flag']
  })
  notifies  :restart, 'service[coldfusion_8]', :delayed
end

# configure env.sh for use in generating neo datasources and runtime
template "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + '/env.sh' do
  source    'coldfusion/env.sh.erb'
  owner     'root'
  group     'root'
  mode      0640
  sensitive true
  variables ({
    :PROD_CALENDAR_DB_DATABASE => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_name"],
    :PROD_CALENDAR_DB_DSN => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_dsn"],
    :PROD_CALENDAR_DB_HOST => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_host"],
    :PROD_CALENDAR_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_pass"],
    :PROD_CALENDAR_DB_PASS_ENC => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_pass_enc"],
    :PROD_CALENDAR_DB_PORT => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_port"],
    :PROD_CALENDAR_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_cal_db_user"],

    :PROD_SESSION_DB_DATABASE => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_name"],
    :PROD_SESSION_DB_DSN => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_dsn"],
    :PROD_SESSION_DB_HOST => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_host"],
    :PROD_SESSION_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_pass"],
    :PROD_SESSION_DB_PASS_ENC => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_pass_enc"],
    :PROD_SESSION_DB_PORT => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_port"],
    :PROD_SESSION_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_session_db_user"],

    :PROD_SURFCOMPANIES_DB_DATABASE => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_name"],
    :PROD_SURFCOMPANIES_DB_DSN => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_dsn"],
    :PROD_SURFCOMPANIES_DB_HOST => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_host"],
    :PROD_SURFCOMPANIES_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_pass"],
    :PROD_SURFCOMPANIES_DB_PASS_ENC => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_pass_enc"],
    :PROD_SURFCOMPANIES_DB_PORT => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_port"],
    :PROD_SURFCOMPANIES_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surf_comp_db_user"],

    :PROD_SURFLINE_DB_DATABASE => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_name"],
    :PROD_SURFLINE_DB_DSN => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_dsn"],
    :PROD_SURFLINE_DB_HOST => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_host"],
    :PROD_SURFLINE_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_pass"],
    :PROD_SURFLINE_DB_PASS_ENC => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_pass_enc"],
    :PROD_SURFLINE_DB_PORT => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_port"],
    :PROD_SURFLINE_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_surfline_db_user"],

    :PROD_SURVEY_DB_DATABASE => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_name"],
    :PROD_SURVEY_DB_DSN => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_dsn"],
    :PROD_SURVEY_DB_HOST => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_host"],
    :PROD_SURVEY_DB_PASS => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_pass"],
    :PROD_SURVEY_DB_PASS_ENC => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_pass_enc"],
    :PROD_SURVEY_DB_PORT => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_port"],
    :PROD_SURVEY_DB_USER => aws_parameter_store["/#{environment}/coldfusion/prod_survey_db_user"],

    :CF_MISSING_TEMPLATE => node['surfline-coldfusion']['coldfusion8']['missing_template'],
    :CF_SITEWIDE_ERROR_TEMPLATE => node['surfline-coldfusion']['coldfusion8']['sitewide_error_template'],
    :CF_REQUEST_TIMEOUT => node['surfline-coldfusion']['coldfusion8']['request_timeout'],
    :CF_QUEUE_TIMEOUT => node['surfline-coldfusion']['coldfusion8']['queue_timeout'],
    :CF_POST_SIZE_LIMIT => node['surfline-coldfusion']['coldfusion8']['post_size_limit'],
    :CF_REQUEST_LIMIT => node['surfline-coldfusion']['coldfusion8']['request_limit'],
    :CF_POST_PARAMETERS_LIMIT => node['surfline-coldfusion']['coldfusion8']['post_parameters_limit'],
    :CF_SESSION_TIMEOUT_DAYS => node['surfline-coldfusion']['coldfusion8']['session_timeout_days'],
    :CF_SESSION_TIMEOUT_HOURS => node['surfline-coldfusion']['coldfusion8']['session_timeout_hours'],
    :CF_SESSION_TIMEOUT_MINUTES => node['surfline-coldfusion']['coldfusion8']['session_timeout_minutes'],
    :CF_SESSION_TIMEOUT_SECONDS => node['surfline-coldfusion']['coldfusion8']['session_timeout_seconds']
  })
  notifies :run, 'execute[build-datasource-conf]', :delayed
  notifies :run, 'execute[build-runtime-conf]', :delayed
end

# configure neo datasource builder script
template "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + '/build_datasource_conf.sh' do
  source    'coldfusion/build_datasource_conf.sh.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir'],
    :cf8tmp => node['surfline-coldfusion']['coldfusion8']['temp_dir']
  })
  notifies :run, 'execute[build-datasource-conf]', :delayed
end

# configure neo datasource builder template
template "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + '/neo-datasource.template.xml' do
  source    'coldfusion/neo-datasource.template.xml.erb'
  owner     'root'
  group     'root'
  mode      0755
  notifies :run, 'execute[build-datasource-conf]', :delayed
end

# build neo datasource xml
execute 'build-datasource-conf' do
  command "
    #{node['surfline-coldfusion']['coldfusion8']['temp_dir']}/build_datasource_conf.sh
  "
  action :nothing
  notifies  :restart, 'service[coldfusion_8]', :delayed
end

# configure neo runtime builder script
template "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + '/build_runtime_conf.sh' do
  source    'coldfusion/build_runtime_conf.sh.erb'
  owner     'root'
  group     'root'
  mode      0755
  variables ({
    :cf8dir => node['surfline-coldfusion']['coldfusion8']['installation_dir'],
    :cf8tmp => node['surfline-coldfusion']['coldfusion8']['temp_dir']
  })
  notifies :run, 'execute[build-runtime-conf]', :delayed
end

# configure neo runtime builder template
template "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + '/neo-runtime.template.xml' do
  source    'coldfusion/neo-runtime.template.xml.erb'
  owner     'root'
  group     'root'
  mode      0755
  notifies :run, 'execute[build-runtime-conf]', :delayed
end

# build neo runtime xml
execute 'build-runtime-conf' do
  command "
    #{node['surfline-coldfusion']['coldfusion8']['temp_dir']}/build_runtime_conf.sh
  "
  action :nothing
  notifies  :restart, 'service[coldfusion_8]', :delayed
end
