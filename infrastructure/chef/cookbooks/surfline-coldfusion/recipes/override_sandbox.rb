#
# Cookbook Name:: surfline-coldfusion
# Recipe:: override_fishtrack
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# NOTE: This is not the normal way o override attributes - this is just a way
#       we are replicating the chef_role functionality (overrides)
node.default['surfline-coldfusion']['service'] = 'sandbox'
node.default['surfline-coldfusion']['server_type'] = 'app'
node.default['surfline-coldfusion']['healthcheck_type'] = 'homepage'
node.default['surfline-coldfusion']['apache']['config'] = {
  'app' => 'true',
  'domain_mod_rewrite' => 'true',
  'ft_mod_rewrite' => 'true',
  'mod_rewrite' => 'true',
  'sl_regional_mod_rewrite' =>  'true',
  'sladmin' => 'true'
}

node.default['surfline-coldfusion']['apache']['sites'] = {
  'backup-admin' => 'true',
  'brasil' => 'true',
  'fishtrack' => 'true',
  'mobile' => 'true',
  'my.buoyweather' => 'true',
  'sladmin' => 'true',
  'www' => 'true'
}
node.default['surfline-coldfusion']['coldfusion8']['post_size_limit'] = '100.0'
node.default['surfline-coldfusion']['coldfusion8']['request_limit'] = '30.0'
node.default['surfline-coldfusion']['coldfusion8']['post_parameters_limit'] = '1000.0'
