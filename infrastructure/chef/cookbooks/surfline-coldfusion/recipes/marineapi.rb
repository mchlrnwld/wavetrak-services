#
# Cookbook Name:: surfline-coldfusion
# Recipe:: marineapi
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# install 32-bit GLIBC to support older tools
package 'glibc.i686'

directory '/var/www/data/shared' do
  recursive true
  mode      0777
  owner     'nobody'
  group     'nobody'
end

directory '/var/www/data/grads' do
  recursive true
  mode      0777
  owner     'nobody'
  group     'nobody'
end

directory '/var/www/data/output' do
  recursive true
  mode      0777
  owner     'nobody'
  group     'nobody'
end

# setup links to various data
links = {
  '/ocean/live/chloro' => '/var/www/data/shared/chloro',
  '/ocean/live/cwatchAU' => '/var/www/data/shared/cwatchAU',
  '/ocean/live/cwatchtn' => '/var/www/data/shared/cwatchtnnew',
  '/ocean/live/cwatchtnbw5' => '/var/www/data/shared/cwatchtn',
  '/ocean/live/jplft' => '/var/www/data/shared/jplft',
  '/ocean/live/modisfthdf' => '/var/www/data/shared/modisfthdf',
  '/ocean/live/modisftnc' => '/var/www/data/shared/modisftnc',
  '/ocean/live/rtofsft' => '/var/www/data/shared/rtofsft',
  '/ocean/bathy' => '/usr/share/otf/bathy'
}

directory '/usr/share/otf'

links.each do |source, target|
  link target do
    to source
  end
end

include_recipe 's3_file'

directory "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}"

# download artifact from S3
s3_file "#{node['surfline-coldfusion']['coldfusion8']['temp_dir']}" + '/marineapi-scripts.zip' do
  bucket        "#{node['surfline-coldfusion']['coldfusion8']['bucket'] }"
  remote_path   '/coldfusion/marineapi-scripts.zip'
  notifies :run, 'execute[extract-marineapi-zip]', :immediately
end

directory '/var/www/data/scripts/'

# extract into web root
execute 'extract-marineapi-zip' do
  command "
    set -x
    unzip -o #{node['surfline-coldfusion']['coldfusion8']['temp_dir']}/marineapi-scripts.zip -d /var/www/data/scripts/
    chown -R nobody:nobody /var/www/data/scripts/ ;
    chmod -R g+r /var/www/data/scripts/
  "
  action :nothing
end
