#
# Cookbook Name:: surfline-coldfusion
# Recipe:: httpd
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

# base Apache installation
include_recipe 'apache2'

include_recipe 'apache2::mod_status'

# setup apache config
node['surfline-coldfusion']['apache']['config'].each do |name, status|
  apache_conf name do
    source    'httpd/configs/' + name + '.erb'
    enable    status
  end
end

# autoindex module
apache_module 'autoindex' do
  conf            true
  enable          true
end

# dir module
apache_module 'dir' do
  conf            true
  enable          true
end

# expires module
apache_module 'expires' do
  enable          true
end

# include module
apache_module 'include' do
  conf            true
  enable          true
end

# JRun module
link '/usr/lib64/httpd/modules/mod_jrun22.so' do
  to        '/opt/coldfusion8/runtime/lib/wsconfig/1/mod_jrun22.so'
end

apache_module 'jrun' do
  identifier      'jrun_module'
  filename        'mod_jrun22.so'
  conf            true
  enable          true
end

file '/opt/coldfusion8/runtime/lib/wsconfig/1/jrunserver.store' do
  owner           "#{node['apache']['user']}"
  group           "#{node['apache']['group']}"
end

# negotiation module
apache_module 'negotiation' do
  enable          true
end

# SetEnvIf module
apache_module 'setenvif' do
  conf            true
  enable          true
end

# proxy modules
proxy_modules = [ 'proxy', 'proxy_http']
proxy_modules.each do |m|
  apache_module m do
    enable          true
    only_if { node['surfline-coldfusion']['server_type'] == 'app' }
  end
end

# setup apache sites
node['surfline-coldfusion']['apache']['sites'].each do |name, status|
  web_app name do
    template        'httpd/sites/' + name + '.conf.erb'
    enable          status
  end
end

# admin site authentication
directory '/var/www/auth' do
  mode            0750
  owner           node['apache']['user']
  group           node['apache']['group']
end

http_pass = aws_parameter_store["/#{environment}/coldfusion/http_pass"]

if ENV['TEST_KITCHEN']
  http_pass = 'floater'
end

htpasswd '/var/www/auth/htpasswd' do
  user        aws_parameter_store["/#{environment}/coldfusion/http_user"]
  password    http_pass
end

template '/var/www/auth/htgroups' do
  source          'httpd/htgroups.erb'
  mode            0750
  owner           node['apache']['user']
  group           node['apache']['group']
end

append_if_no_line 'NameVirtualHost Line' do
  path '/etc/httpd/ports.conf'
  line 'NameVirtualHost *:80'
end
