################################################################################
# Check for services

%w[
  coldfusion_8
  postfix
  httpd
].each do |s|
  describe service(s) do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

################################################################################
# Check for Ports listening

# Check httpd
%w[
  80
  7013
].each do |p|
  describe port(p) do
    it { should be_listening }
    its('processes') { should include 'httpd' }
  end
end

# Check rsync
describe port(873) do
  it { should be_listening }
  its('processes') { should include 'rsync' }
end

# Check coldfusion8
%w[
  1099
  7999
  51_800
].each do |p|
  describe port(p) do
    it { should be_listening }
    its('processes') { should include 'coldfusion8' }
  end
end

################################################################################
# URL Tests

http_creds = 'surfline:floater'
http_host = 'www.fishtrack.com'

describe bash("curl --user #{http_creds} \
              http://127.0.0.1/healthcheck/jrun.cfm") do
  its('stdout') { should include 'functional' }
end

describe bash("curl --user #{http_creds} \
               -H \'Host:#{http_host}\' \
               http://127.0.0.1/") do
  its('stdout') { should include 'FISHTRACK.COM' }
end

describe bash("curl --user #{http_creds} \
              http://127.0.0.1/robots.txt") do
  its('stdout') { should include 'Disallow: /' }
end

describe bash("curl --user #{http_creds} \
              http://127.0.0.1/healthcheck/jrun.cfm") do
  its('stdout') { should include 'functional' }
end

# Confirm that subscription redirects work properly
%w[
  sign-in
  create-account
  forgot-password
  subs/
  cancel
  account
  upgrade
].each do |p|
  describe bash("curl -ksv --user #{http_creds} \
                 -H \'Host:#{http_host}\' \
                 -H \'X-Forwarded-Proto:http\' \
                 http://127.0.0.1/#{p}") do
    its('stderr') { should include "Location: https://#{http_host}/#{p}" }
  end
end
