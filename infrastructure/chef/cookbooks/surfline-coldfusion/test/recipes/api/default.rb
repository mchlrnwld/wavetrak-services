################################################################################
# Check for services

%w[
  coldfusion_8
  postfix
  httpd
].each do |s|
  describe service(s) do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

################################################################################
# Check for Ports listening

# Check httpd
describe port(80) do
  it { should be_listening }
  its('processes') { should include 'httpd' }
end

# Check rsync
describe port(873) do
  it { should be_listening }
  its('processes') { should include 'rsync' }
end

# Check coldfusion8
%w[
  1099
  7999
  51_800
].each do |p|
  describe port(p) do
    it { should be_listening }
    its('processes') { should include 'coldfusion8' }
  end
end

################################################################################
# URL Tests

http_creds = 'surfline:floater'
http_host = 'api.staging.surfline.com'

describe bash('curl 127.0.0.1') do
  its('stdout') { should include '<h1>api.surfline.com</h1>' }
end

describe bash("curl --user #{http_creds} \
              -H \'Host:#{http_host}\' \
              http://127.0.0.1/v1/testmongo") do
  its('stdout') { should include 'LASTACCESS' }
end

describe bash("curl --user #{http_creds} \
              -H \'Host:#{http_host}\' \
              http://127.0.0.1/v1/testredis") do
  its('stdout') { should include 'application.redisScripts' }
end

describe bash("curl --user #{http_creds} \
              -H \'Host:#{http_host}\' \
              http://127.0.0.1/v1/testerror") do
  its('stdout') { should include 'Error processing API request' }
end
