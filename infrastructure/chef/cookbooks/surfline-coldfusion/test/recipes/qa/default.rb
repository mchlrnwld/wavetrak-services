################################################################################
# Check for services

%w[
  coldfusion_8
  postfix
  httpd
].each do |s|
  describe service(s) do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

################################################################################
# Check for Ports listening

# Check httpd
%w[
  80
  7013
].each do |p|
  describe port(p) do
    it { should be_listening }
    its('processes') { should include 'httpd' }
  end
end

# Check rsync
describe port(873) do
  it { should be_listening }
  its('processes') { should include 'rsync' }
end

# Check coldfusion8
%w[
  1099
  7999
  51_800
].each do |p|
  describe port(p) do
    it { should be_listening }
    its('processes') { should include 'coldfusion8' }
  end
end

################################################################################
# URL Tests

http_creds = 'surfline:floater'
http_host = 'www.surfline.com'

# describe bash('curl 127.0.0.1') do
#   its('stdout') { should include '401 Authorization Required' }
# end

describe bash("curl --user #{http_creds} \
               -H \'Host:#{http_host}\' \
               http://127.0.0.1/healthcheck/jrun.cfm") do
  its('stdout') { should include 'functional' }
end

describe bash("curl --user #{http_creds} \
               -H \'Host:#{http_host}\' \
               http://127.0.0.1/home/index.cfm?live=true") do
  its('stdout') { should include '<title>SURFLINE.COM | Global Surf Reports, Surf Forecasts, Live Surf Cams and Coastal Weather</title>' }
end

describe bash("curl --user #{http_creds} \
               -H \'Host:#{http_host}\' \
               http://127.0.0.1/surfology/schoolsearch.cfm") do
  its('stdout') { should include 'BILLABONG ALOHA BEACH CAMP - SAN CLEMENTE' }
end

describe bash("curl --user #{http_creds} \
  -H \'Host:#{http_host}\' \
  http://127.0.0.1/robots.txt") do
  its('stdout') { should include 'Disallow: /' }
end

describe bash("curl --user #{http_creds} \
  -H \'Host:www.surfline.com\' \
  \'http://127.0.0.1/lola/mapit.cfm?readFrom=%2Focean%2Ftransient%2Fvalidation%2Fbuoy%2Flola_v_buoy_mapit_v2.txt&latColumn=1&lonColumn=2&labelColumn=&urlColumn=3&delimiter=%2C&grep=&action=doit\'") do
  its('stdout') { should include '<title>Lola Generic Mapper</title>' }
end
