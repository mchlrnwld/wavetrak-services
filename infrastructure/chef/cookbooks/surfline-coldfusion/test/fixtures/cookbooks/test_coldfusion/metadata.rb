name             'test_coldfusion'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Testing support for application'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.0'
