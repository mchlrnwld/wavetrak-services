#
# Cookbook Name:: test_coldfusion
# Recipe:: mysql
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Postfix fails to start w/o the hostname resolving
hostsfile_entry '127.0.0.1' do
  hostname    node['hostname']
  comment     'Created by Chef'
  ignore_failure true
end
