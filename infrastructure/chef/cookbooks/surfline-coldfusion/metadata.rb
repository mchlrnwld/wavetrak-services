name             'surfline-coldfusion'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-coldfusion'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.20.7'

depends 'apache2', '~> 3.1'
depends 'aws_parameter_store', '~> 2.1.2'
depends 'cron', '~> 1.6'
depends 'htpasswd', '~> 0.3'
depends 'java', '~> 1.50.0'
depends 'line', '>= 2.0.0'
depends 'logrotate', '~> 1.9'
depends 'newrelic', '~> 2.17'
depends 'newrelic-infra', '~> 0.0.1'
depends 'rsync', '~> 0.8'
depends 's3_file', '~> 2.7'
depends 'surfline-common', '~> 1.10.5'
depends 'surfline-nfs', '~> 1.3.1'
depends 'surfline-users', '~> 1.4'
depends 'surfline-science', '~> 1.22.4'
depends 'surfline-iam-ssh', '~> 1.0.0'
depends 'timezone-ii', '~> 0.2'
depends 'sturdy_platform', '~> 2.0'
depends 'newrelic_meetme_plugin'

gem 'aws-sdk-ec2', '~> 1.84.0'

gem 'htauth' if respond_to?(:gem)

supports 'amazon'
