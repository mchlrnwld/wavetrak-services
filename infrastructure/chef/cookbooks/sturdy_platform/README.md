# sturdy_platform

## Prerequisites

1. An instance role with permissions equivalent to the following IAM policy document:
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "cloudwatch:PutMetricData"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ssm:DescribeAssociation",
        "ssm:GetDeployablePatchSnapshotForInstance",
        "ssm:GetDocument",
        "ssm:GetParameters",
        "ssm:ListAssociations",
        "ssm:ListInstanceAssociations",
        "ssm:PutInventory",
        "ssm:UpdateAssociationStatus",
        "ssm:UpdateInstanceAssociationStatus",
        "ssm:UpdateInstanceInformation"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2messages:AcknowledgeMessage",
        "ec2messages:DeleteMessage",
        "ec2messages:FailMessage",
        "ec2messages:GetEndpoint",
        "ec2messages:GetMessages",
        "ec2messages:SendReply"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstanceStatus"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ds:CreateComputer",
        "ds:DescribeDirectories"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": "arn:aws:s3:::amazon-ssm-packages-*"
    }
  ]
}
```
_This is a combination of the SSM agent managed policy (without the global S3 permissions), cloudwatch metrics permissions (taken from collectd cloudwatch plugin documentation), and the awscli logs plugin documentation._

2. Verify syslog is recording log entries in the same timezone as the system timezone (i.e. if the system timezone has been changed since last boot then execute `sudo service rsyslog restart`)

## Configuration

### Logsforwarder

Add additional configuration files to /opt/sturdy/etc/logsforwarder/config to configure log forwarding.

Documentation on the file format is available at https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/AgentReference.html

## Testing

## Prerequisites

* [ChefDK](https://downloads.chef.io/chefdk)
* `kitchen-ec2` driver (e.g. `chef gem install kitchen-ec2`)
* `kitchen-sync` driver (e.g. `chef gem install kitchen-sync`)

## Use

Set your `AWS_SSH_KEY_ID` environment variable to the AWS key pair name with which instances should be associated (e.g. `export AWS_SSH_KEY_ID=sturdylab-uswest2`). Then set the `TESTKITCHEN_SSH_KEY` environment variable to the full local path of the associated private key (e.g. `export TESTKITCHEN_SSH_KEY=~/.ssh/sturdylab-uswest2`)

## Cookbook Resources

### sturdy_platform_eni

Configures a persistent IP address by way of invoking a lambda function that attaches it to the instance (i.e. as setup by the sturdy-platform-infrastructure asg module). E.g.:
```
sturdy_platform_eni 'eth1' do
  lambda_function 'arn:aws:lambda:us-west-2:123456789012:function:ENIAttach-123456789012'
end
```

### sturdy_platform_letsencrypt

Registers a TLS certificate and configures automatic renewal. The default Route53 authenticator requires the invoking instance to have [IAM permissions for modifying route53](https://github.com/certbot/certbot/blob/v0.19.0/certbot-dns-route53/examples/sample-aws-policy.json). E.g.:
```
sturdy_platform_letsencrypt 'mysite.example.com' do
  email 'devops@example.com'
  deploy_hook 'test -x /usr/sbin/nginx && nginx -t && systemctl reload nginx' # optional
end
```

### sturdy_platform_volume

Configures a persistent volume (i.e. one created by the sturdy-platform-infrastructure asg module) & its associated mountpoints. E.g.:
```
sturdy_platform_volume 'vol-049df61146c4d7901' do
  mountpoints ['/var/lib/jenkins']
end
```
