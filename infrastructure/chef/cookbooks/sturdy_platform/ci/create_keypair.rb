#!/usr/bin/env ruby

require 'aws-sdk-ec2'
require 'json'
require 'net/http'

instance_identity = Net::HTTP.get(
  URI.parse(
    'http://169.254.169.254/latest/dynamic/instance-identity/document'
  )
)
region = JSON.parse(instance_identity)['availabilityZone'][0...-1]
instance_id = JSON.parse(instance_identity)['instanceId']

key_name = case ENV['TKPLATFORM'].nil?
           when true
             instance_id
           else
             "#{instance_id}-#{ENV['TKPLATFORM']}"
           end

ec2 = Aws::EC2::Client.new(region: region)

begin
  current_key_pairs = ec2.describe_key_pairs(key_names: [key_name]).key_pairs
  ec2.delete_key_pair(key_name: key_name) if current_key_pairs.count != 0
rescue Aws::EC2::Errors::InvalidKeyPairNotFound
end

Dir.mkdir("#{ENV['HOME']}/.ssh") unless File.directory? "#{ENV['HOME']}/.ssh"
resp = ec2.create_key_pair(key_name: key_name)
File.open("#{ENV['HOME']}/.ssh/#{key_name}", 'w') { |file| file.write(resp.key_material) }
File.chmod(0600, "#{ENV['HOME']}/.ssh/#{key_name}")

# Return key name for us in environment variables
puts key_name
