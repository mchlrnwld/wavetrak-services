property :volume_id, String, name_property: true
property :mountpoints, Array, default: []
property :volume_dev_name, String, default: node['sturdy']['platform']['volume_resource']['dev_name']
property :volume_fs_label, String, default: 'PersistentVol'
property :volume_mountpoint, [String, NilClass], default: node['sturdy']['platform']['volume_resource']['mountpoint']

action :attach do
  if node['os'] == 'windows'
    Chef::Application.fatal!('Windows Server 2016 or greater required for the volume resource') unless
      ::File.exist?('c:/ProgramData/Amazon/EC2-Windows/Launch/Scripts/InitializeDisks.ps1')
    Chef::Application.fatal!('Drive letter must be automatically assigned') unless
      new_resource.volume_mountpoint.nil?
  end

  aws_ebs_volume 'attach_persistent_volume' do
    volume_id new_resource.volume_id
    device new_resource.volume_dev_name
    action :attach
    if node['os'] == 'linux'
      notifies :run, 'ruby_block[verify_persistent_vol_attachment]', :immediate
    elsif node['os'] == 'windows'
      notifies :run, 'powershell_script[online_disks]', :immediate
    end
  end
  ruby_block 'verify_persistent_vol_attachment' do
    block do
      vol_attached = false
      until vol_attached do
        vol_attached = ::File.exist?(new_resource.volume_dev_name)
        sleep 3
      end
    end
    action :nothing
  end

  case node['os']
  when 'windows'
    # After the EBS volume is attached, if it was previously formatted it will be
    # offline -- mark it online here to have it claim its previous drive letter
    powershell_script 'online_disks' do
      code <<-EOH
foreach ($Disk in $(Get-Disk)) {
  If (-not ($Disk.PartitionStyle -eq 'RAW')) {
    If (($Disk | Get-Disk).IsOffline) {
      $Disk | Set-Disk -IsOffline $False
    }
    If (($Disk | Get-Disk).IsReadOnly) {
      $Disk | Set-Disk -IsReadOnly $False
    }
  }
}
EOH
      action :nothing
      notifies :run, 'powershell_script[initialize_disks]', :immediate
    end
    # For a new disk, this will format it and assign it the next available drive
    # letter
    powershell_script 'initialize_disks' do
      code '. c:\ProgramData\Amazon\EC2-Windows\Launch\Scripts\InitializeDisks.ps1'
      action :nothing
    end

  when 'linux'
    ruby_block 'format_persistent_volume' do
      block do
        chkcmd = Mixlib::ShellOut.new(
          "btrfs check #{new_resource.volume_dev_name}",
          returns: [0, 1, 251]
        )
        chkcmd.run_command
        if [1, 251].include?(chkcmd.exitstatus) && chkcmd.stderr.include?('No valid Btrfs found on')
          Chef::Log.info("No filesystem found on dev #{new_resource.volume_dev_name}; creating it...")
          mkfscmd = Mixlib::ShellOut.new("mkfs.btrfs -L #{new_resource.volume_fs_label} #{new_resource.volume_dev_name}")
          mkfscmd.run_command
        else
          Chef::Log.info('Persistent volume formatting called, but filesystem appears to be present. '\
                         "'btrfs check' exited code #{chkcmd.exitstatus} with stdout #{chkcmd.stdout} & stderr #{chkcmd.stderr}")
        end
      end
      not_if { node['filesystem']['by_device'][new_resource.volume_dev_name] && node['filesystem']['by_device'][new_resource.volume_dev_name]['mounts'] && !node['filesystem']['by_device'][new_resource.volume_dev_name]['mounts'].empty? }
      not_if "btrfs filesystem show #{new_resource.volume_dev_name}"
    end

    # Mount point for persistent volume
    directory "ensure_#{new_resource.volume_dev_name}_mountpoint_exists" do
      path new_resource.volume_mountpoint
      not_if { ::File.directory? new_resource.volume_mountpoint }
    end
    mount new_resource.volume_mountpoint do
      device new_resource.volume_dev_name
      fstype 'btrfs'
      action [:enable, :mount]
    end

    # Mount persistent data
    new_resource.mountpoints.each do |i|
      mountpoint, subvol = case i.respond_to?(:has_key?)
                           when true
                             [i[:mountpoint], i[:subvol]]
                           when false
                             [i, i.tr('/', '_')]
                           end
      subvol_dir = ::File.join(new_resource.volume_mountpoint, subvol)

      execute "btrfs subvolume create #{subvol_dir}" do
        not_if { ::File.directory?(subvol_dir) }
      end
      # ensure directory exists prior to mount
      directory "ensure_#{subvol}_mountpoint_exists" do
        path mountpoint
        not_if { ::File.directory?(mountpoint) }
      end
      mount mountpoint do
        device new_resource.volume_dev_name
        fstype 'btrfs'
        options "defaults,subvol=#{subvol}"
        action [:enable, :mount]
      end
    end
  end
end
