property :domain, String, name_property: true
property :additional_domains, [NilClass, Array], default: nil
property :email, [NilClass, String], default: nil
property :register_cert, [TrueClass, FalseClass], default: true
property :authenticator, String, default: 'dns-route53'
property :deploy_hook, [NilClass, String], default: nil
property :environment, [NilClass, Hash], default: nil
property :use_letsencrypt_staging, [TrueClass, FalseClass], default: false
property :use_sni, [TrueClass, FalseClass], default: node['sturdy']['platform']['letsencrypt_resource']['use_sni']
# Windows specific properties
property :update_winrm_cert, [TrueClass, FalseClass], default: false
property :route53_zone_id, [NilClass, String], default: nil
property :iis_site, [NilClass, String], default: nil
property :iis_port, [Integer, String], default: '443'
property :acmesharp_profile_name, String, default: 'acmesharp'
property :acmesharp_profile_path, String, default: 'c:\acmesharp'

action :create do
  case node['platform_family'] == 'windows'
  when true
    if new_resource.authenticator == 'dns-route53' && new_resource.route53_zone_id.nil?
      Chef::Application.fatal!('Route53 HostedZoneId must be provided to the letsencrypt resource (property route53_zone_id)')
    end
    if new_resource.authenticator != 'dns-route53'
      Chef::Application.fatal!('Non-Route53 authentication not currently supported on Windows')
    end
    unless new_resource.additional_domains.nil?
      Chef::Application.fatal!('Additional domains not currently supported on Windows by the letsencrypt resource')
    end
    if new_resource.email.nil?
      Chef::Application.fatal!('Email property must be set for the letsencrypt resource on Windows')
    end

    include_recipe 'sturdy_platform::pstools'

    powershell_script 'install_acmesharp' do
      code <<-EOH
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

Set-PSRepository -Name PSGallery -InstallationPolicy Trusted

Install-Module -Name ACMESharp.Providers.AWS -RequiredVersion #{node['sturdy']['platform']['letsencrypt_resource']['acmesharp_version']}
Install-Module -Name ACMESharp.Providers.IIS -RequiredVersion #{node['sturdy']['platform']['letsencrypt_resource']['acmesharp_version']}

Import-Module ACMESharp
Enable-ACMEExtensionModule ACMESharp.Providers.AWS
Enable-ACMEExtensionModule ACMESharp.Providers.IIS
      EOH
      guard_interpreter :powershell_script
      # https://stackoverflow.com/a/28740512
      only_if "if (Get-Module -ListAvailable -Name ACMESharp.Providers.AWS) {exit 1}"
    end

    set_acmesharp_vault_profile_cmd =
      "Set-ACMEVaultProfile -ProfileName #{new_resource.acmesharp_profile_name} -Provider local -VaultParameters @{ RootPath = '#{new_resource.acmesharp_profile_path}'; CreatePath = $true; BypassEFS = $true }"

    powershell_script 'set_acmesharp_vault_profile' do
      code <<-EOH
Import-Module ACMESharp
#{set_acmesharp_vault_profile_cmd}
EOH
      notifies :run, 'ruby_block[set_system_acmesharp_vault_profile]', :immediate
      guard_interpreter :powershell_script
      only_if "Import-Module ACMESharp\r\n"\
              "if ((Get-ACMEVaultProfile -ListProfiles).Contains(\"#{new_resource.acmesharp_profile_name}\")) { exit 1 }\r\n"
    end

    # Duplicate the vault profile setup for the SYSTEM user (for running scheduled tasks)
    ruby_block 'set_system_acmesharp_vault_profile' do
      block do
        cmd = Mixlib::ShellOut.new(
          "#{node['sturdy']['platform']['pstools']['path']}\\PsExec.exe -accepteula -i -s powershell.exe -ExecutionPolicy ByPass -Command \"& {Import-Module ACMESharp; #{set_acmesharp_vault_profile_cmd}}\""
        ).run_command
        unless cmd.exitstatus == 0
          Chef::Application.fatal!('SYSTEM user Set-ACMEVaultProfile failed')
        end
      end
      action :nothing
    end

    powershell_script 'initialize_acmesharp_vault_profile' do
      code <<-EOH
Import-Module ACMESharp
Initialize-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}#{new_resource.use_letsencrypt_staging ? ' -BaseService LetsEncrypt-STAGING' : ''}
EOH
      not_if { ::File.directory?(new_resource.acmesharp_profile_path) }
    end

    powershell_script 'register_letsencrypt_account' do
      code <<-EOH
Import-Module ACMESharp
New-ACMERegistration -Contacts mailto:#{new_resource.email} -VaultProfile #{new_resource.acmesharp_profile_name} -AcceptTos
EOH
      guard_interpreter :powershell_script
      not_if "Import-Module ACMESharp\r\n"\
              "Get-ACMERegistration -VaultProfile #{new_resource.acmesharp_profile_name}\r\n"\
              "if (-not ($?)) { exit 1 }\r\n"
    end

    # ACMESharp update script
    # Sourced from https://github.com/whereisaaron/acmesharp-update-certificate
    # (MIT License)
    template ::File.join('c:', 'chef', 'Update-Certificate.ps1') do
      cookbook 'sturdy_platform'
    end

    # Customized update script for this domain
    update_cert_cmd_suffix =
      "-VaultProfile \"#{new_resource.acmesharp_profile_name}\" -certPath \"\\LocalMachine\\My\" -ChallengeType \"dns-01\" -ChallengeHandler \"aws-route53\" -ChallengeParameters @{HostedZoneId=\"#{new_resource.route53_zone_id}\";Region=\"#{node['sturdy']['platform']['letsencrypt_resource']['route53_region']}\";AwsIamRole=\"*\"}"
    update_cert_cmd =
      case new_resource.iis_site.nil?
      when true
        "Update-Certificate -alias \"#{new_resource.domain}$suffix\" -domain #{new_resource.domain} -notIIS #{update_cert_cmd_suffix}"
      else
        "Update-Certificate -alias \"#{new_resource.domain}$suffix\" -domain #{new_resource.domain}#{new_resource.use_sni ? '' : ' -notSNI'} -websiteName \"#{new_resource.iis_site}\" #{update_cert_cmd_suffix}"
      end

    acmesharp_update_script_basename =
      "acmesharp_update_#{new_resource.domain.tr('.', '_')}_cert"
    acmesharp_update_script_path =
      ::File.join('c:', 'chef', "#{acmesharp_update_script_basename}.ps1")
    file acmesharp_update_script_path do # ~FC005
      content <<-EOH
Import-Module ACMESharp
. .\\Update-Certificate.ps1

$newSuffix = "-{0:D2}-{1:D2}-{2:D2}-{3:D2}-{4:D2}" -f [int](Get-Date).year,[int](Get-Date).month,[int](Get-Date).day,[int](Get-Date).hour,[int](Get-Date).minute

$certList = (Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates
if ([string]::IsNullOrEmpty($certList)) {
  $suffix = $newSuffix
  #{update_cert_cmd}
} else {
  $latestCert = ((Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates | Sort-Object Alias -Descending)[0]
  $pemFile = gci -Path "#{new_resource.acmesharp_profile_path}" -recurse -filter "$($latestCert.CrtPemFile)" -File -ErrorAction SilentlyContinue
  $pemPath = "$($pemFile.DirectoryName)\\$($pemFile.Name)"
  $certInMem = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
  $certInMem.Import($pemPath)
  $cutoffDate = (Get-Date).adddays(45)
  if ($certInMem.NotAfter -lt $cutoffDate) {
    $suffix = $newSuffix
    #{update_cert_cmd}
  } else {
    exit 0
  }
}
EOH
      only_if { new_resource.register_cert }
    end

    acmesharp_install_latest_path =
      ::File.join('c:', 'chef', "acmesharp_install_latest_existing_#{new_resource.domain.tr('.', '_')}_cert.ps1")
    file acmesharp_install_latest_path do
      content <<-EOH
Import-Module ACMESharp
. .\\Update-Certificate.ps1
$latestCert = ((Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates | Sort-Object Alias -Descending)[0]
$latestCert.Alias -match '-[\\d]*-[\\d]*-[\\d]*-[\\d]*-[\\d]*$'
$suffix = $matches[0]
#{update_cert_cmd} -useExistingCert
EOH
      only_if { new_resource.register_cert }
    end

    # If a valid cert if in the vault, install it; this will keep a new cert
    # from being generated when a server is terminated
    powershell_script 'install_previously_created_cert_if_still_valid' do
      code ". #{acmesharp_install_latest_path}\r\n"
      cwd ::File.join('c:', 'chef')
      not_if { new_resource.iis_site.nil? }
      only_if { new_resource.register_cert }
      guard_interpreter :powershell_script
      only_if <<-EOH
Import-Module ACMESharp
Import-Module WebAdministration
$certList = (Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates
if ([string]::IsNullOrEmpty($certList)) {
  exit 1
} else {
  $latestCert = ((Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates | Sort-Object Alias -Descending)[0]
  $pemFile = gci -Path "#{new_resource.acmesharp_profile_path}" -recurse -filter "$($latestCert.CrtPemFile)" -File -ErrorAction SilentlyContinue
  $pemPath = "$($pemFile.DirectoryName)\\$($pemFile.Name)"
  $certInMem = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
  $certInMem.Import($pemPath)
  $cutoffDate = (Get-Date).adddays(1)
  # If the ACMESharp cert has already or will shortly expire, exit now so it won't be used
  if ($certInMem.NotAfter -lt $cutoffDate) {
    exit 1
  } else {
    $checkIisSni = #{new_resource.use_sni ? '$true' : '$false'}
    if ($checkIisSni) {
      # TODO: add condition here so this resource doesn't execute on every chef-client run
      # This may the same (identical?) to the non-sni check below
      exit 0
    } else {
      $iisCert = Get-ChildItem IIS:SSLBindings | ? { $_.Sites.Value -eq '#{new_resource.iis_site}' } | % { $_.Thumbprint }
      if ([string]::IsNullOrEmpty($iisCert)) {
        exit 0
      } else {
        if ($latestCert.Thumbprint -eq $iisCert) {
          exit 1
        } else {
          exit 0
        }
      }
    }
  }
}
EOH
    end

    # Run the update script now if a new/updated cert is needed
    powershell_script 'update_cert_if_required' do
      code ". #{acmesharp_update_script_path}\r\n"
      cwd ::File.join('c:', 'chef')
      not_if { new_resource.iis_site.nil? }
      only_if { new_resource.register_cert }
      guard_interpreter :powershell_script
      only_if <<-EOH
Import-Module ACMESharp
$certList = (Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates
if ([string]::IsNullOrEmpty($certList)) {
  exit 0
} else {
  $latestCert = ((Get-ACMEVault -VaultProfile #{new_resource.acmesharp_profile_name}).Certificates | Sort-Object Alias -Descending)[0]
  $pemFile = gci -Path "#{new_resource.acmesharp_profile_path}" -recurse -filter "$($latestCert.CrtPemFile)" -File -ErrorAction SilentlyContinue
  $pemPath = "$($pemFile.DirectoryName)\\$($pemFile.Name)"
  $certInMem = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
  $certInMem.Import($pemPath)
  $cutoffDate = (Get-Date).adddays(1)
  if ($certInMem.NotAfter -lt $cutoffDate) {
    exit 0
  } else {
    exit 1
  }
}
EOH
    end

    # This cmd script to invoke the powershell update script will be called by
    # the scheduled task
    acmesharp_update_cmd_path =
      ::File.join('c:', 'chef', "acmesharp_scheduledtask_#{new_resource.domain.tr('.', '_')}.cmd")

    file acmesharp_update_cmd_path do
      content "PowerShell.exe -ExecutionPolicy ByPass .\\#{acmesharp_update_script_basename}.ps1 > c:\\chef\\logs\\acmesharp_log_#{new_resource.domain}.log 2>&1\r\n"
    end

    # This will show as updating on every chef-client run w/ chef v13+
    # regardless if the execution_time_limit is set or not
    # https://github.com/chef/chef/issues/6638
    windows_task "acmesharp-#{new_resource.domain}" do
      cwd 'c:\chef'
      command acmesharp_update_cmd_path
      run_level :highest
      frequency :weekly
      if Chef::Resource::WindowsTask.properties[:execution_time_limit] # Chef 13+
        execution_time_limit (12*60).to_s # Any value over 2 minutes should be fine
      end
    end
  else # linux
    if new_resource.use_letsencrypt_staging
      # TODO: test staging deployment with certbot
      Chef::Application.fatal!('Let\'s Encrypt Staging not currently supported on Linux')
    end
    if new_resource.use_sni
      Chef::Application.fatal!('SNI certificate deployment not currently supported on Linux')
    end

    # install certbot "let's encrypt" client
    apt_repository 'certbot' do
      uri 'ppa:certbot/certbot'
      distribution node['lsb']['codename']
      only_if { %w(trusty xenial zesty).include?(node['lsb']['codename']) } # certbot v0.17+ included in universe as of 17.10
    end
    package %w(certbot ssl-cert)
    package 'install_pip_for_dns-route53' do
      package_name 'python-pip'
      only_if { new_resource.authenticator == 'dns-route53' }
    end
    python_package 'certbot-dns-route53' do
      only_if { new_resource.authenticator == 'dns-route53' }
    end

    # letsencrypt account registration
    execute 'register_letsencrypt_account' do
      command "certbot register #{new_resource.email ? '-m "' + new_resource.email + '" --no-eff-email': '--register-unsafely-without-email'} --agree-tos -n"
      environment new_resource.environment if new_resource.environment
      not_if { ::File.directory?('/etc/letsencrypt/accounts') }
    end

    # register cert
    execute 'generate_certificate' do
      command "certbot certonly -n --#{new_resource.authenticator} -d #{new_resource.domain}#{new_resource.additional_domains ? ',' + new_resource.additional_domains.join(',') : ''}#{new_resource.deploy_hook ? ' --deploy-hook \'' + new_resource.deploy_hook + '\'' : ''}"
      environment new_resource.environment if new_resource.environment
      only_if { new_resource.register_cert }
      not_if { ::File.exist?("/etc/letsencrypt/live/#{new_resource.domain}/cert.pem") }
    end
  end
end
