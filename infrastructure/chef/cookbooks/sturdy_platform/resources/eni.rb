property :adapter_name, String, name_property: true
property :lambda_function, String

action :attach do
  ruby_block 'attach_persistent_address' do
    block do
      require 'aws-sdk-lambda'
      require 'json'

      Aws::Lambda::Client.new(region: node['ec2']['region']).invoke(
        function_name: new_resource.lambda_function,
        payload: {'InstanceId' => node['ec2']['instance_id']}.to_json
      )
    end
    not_if { node['network']['interfaces'][new_resource.adapter_name] } # skip if adapter has already been attached
    notifies :run, 'ruby_block[wait_for_persistent_address_attachment]', :immediate
  end
  ruby_block 'wait_for_persistent_address_attachment' do
    block do
      require 'socket'
      nic_attached = false
      until nic_attached do
        nic_attached = true if Socket.getifaddrs.any? { |h| h.name == new_resource.adapter_name }
        sleep 3
      end
    end
    action :nothing
  end
  # This route table will be used to route traffic received via eth0 back out
  # eth0
  # Adapted from https://unix.stackexchange.com/a/23345
  execute 'add_ephemeral_interface_route_table' do
    command 'echo 200 ephemtbl >> /etc/iproute2/rt_tables'
    not_if 'grep ephemtbl /etc/iproute2/rt_tables'
  end
  default_route_cmd =
    "ip route change to default dev #{new_resource.adapter_name} via #{node['network']['default_gateway']}"
  network_interface new_resource.adapter_name do
    bootproto 'dhcp'
    post_up default_route_cmd # TODO: is there a better way to persist this default route change?
  end
  execute "change_default_interface_to_#{new_resource.adapter_name}" do
    command default_route_cmd
    not_if { node['network']['default_interface'] == new_resource.adapter_name }
    notifies :reload, 'ohai[network]', :immediate
  end
  ohai 'network' do
    plugin 'network'
    action :nothing
  end
  file '/usr/local/sbin/fix_eth0_return_traffic' do
    content "#!/bin/sh\n"\
            "ip rule add from $(ip addr|awk '/eth0/ && /inet/ {gsub(/\\/[0-9][0-9]/,\"\"); print $2}') table ephemtbl\n"\
            "ip route add default via #{node['network']['default_gateway']} dev eth0 table ephemtbl\n"
    mode 0755
  end
  network_interface 'eth0' do
    bootproto 'dhcp'
    post_up '/usr/local/sbin/fix_eth0_return_traffic'
  end
  # TODO: Why doesn't the routing work properly until a network restart?
  ruby_block 'restart_eth0_at_boot' do
    block do
      if ::File.exist?('/etc/rc.local')
        open('/etc/rc.local', 'a') { |f| f.puts "ifdown eth0 && ifup eth0\n" }
      else
        open('/etc/rc.local', 'w') do |f|
          f.puts "#!/bin/sh\n"
          f.puts "ifdown eth0 && ifup eth0\n"
        end
        ::File.chmod(0755, '/etc/rc.local')
      end
    end
    not_if 'grep \'ifdown eth0\' /etc/rc.local'
  end
end
