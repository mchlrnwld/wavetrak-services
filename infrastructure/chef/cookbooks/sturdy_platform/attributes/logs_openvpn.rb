default['sturdy']['platform']['logs']['openvpn'].tap do |config|
  config['datetime_format'] = '%a %b %d %H:%M:%S %Y'
  config['package_names'] = %w(openvpn)
  config['log_file'] = '/var/log/openvpn.log'
end
