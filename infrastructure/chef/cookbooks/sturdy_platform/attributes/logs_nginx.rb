default['sturdy']['platform']['logs']['nginx']['access'].tap do |config|
  config['datetime_format'] = '%a %b %d %H:%M:%S %Y'
  config['package_names'] = %w(nginx)
  config['log_file'] = '/var/log/nginx/access.log'
end

default['sturdy']['platform']['logs']['nginx']['error'].tap do |config|
  config['datetime_format'] = '%a %b %d %H:%M:%S %Y'
  config['package_names'] = %w(nginx)
  config['log_file'] = '/var/log/nginx/error.log'
end
