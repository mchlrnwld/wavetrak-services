default['sturdy']['platform']['logs']['chef_client'].tap do |config|
  config['datetime_format'] = '[%Y/%m/%dT%H:%M:%S%:z]' # [2017-04-28T03:23:36+00:00]
  config['package_names'] = %w(chef-client chef)
  config['log_file'] = '/var/log/chef/client.log'
  config['log_group_name'] = '/var/log/chef/client.log'
end
