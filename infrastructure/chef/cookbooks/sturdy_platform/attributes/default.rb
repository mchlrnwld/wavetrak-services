if ENV['CI']
  extension = case node['platform_family']
              when 'debian'
                '.deb'
              when 'rhel'
                '.rpm'
              end
  installer_path =
    run_context.cookbook_collection['sturdy_platform'].file_filenames.find { |i| i =~ /#{extension}$/ }
  default['sturdy']['platform']['installer']['url'] =
    "file://#{installer_path}"
else
  release_folder = 'https://s3-us-west-2.amazonaws.com/sturdylab-sandboxbucket/sturdy-platform-releases/'
  default['sturdy']['platform']['installer']['version'] = '1.1.0'
  default['sturdy']['platform']['installer']['epoch'] = '1'
  case node['platform_family']
  when 'debian'
    default['sturdy']['platform']['installer']['url'] =
      case %w(16.04 14.04).include?(node['platform_version']) && node['platform'] == 'ubuntu'
      when true
        release_folder + "ubuntu_#{node['platform_version'].tr('.', '')}/" + "sturdy-platform_#{node['sturdy']['platform']['installer']['version']}_amd64.deb"
      else
        release_folder + "sturdy-platform_#{node['sturdy']['platform']['installer']['version']}_amd64.deb"
      end
  when 'rhel'
    default['sturdy']['platform']['installer']['url'] =
      case %w(2017.03 2016.09 2016.03 2015.09).include?(node['platform_version']) && node['platform'] == 'amazon'
      when true
        release_folder + "amazon_#{node['platform_version'].tr('.', '')}/" + "sturdy-platform-#{node['sturdy']['platform']['installer']['version']}-#{node['sturdy']['platform']['installer']['epoch']}.x86_64.rpm"
      else
        release_folder + "sturdy-platform-#{node['sturdy']['platform']['installer']['version']}-#{node['sturdy']['platform']['installer']['epoch']}.x86_64.rpm"
      end
  end
end
