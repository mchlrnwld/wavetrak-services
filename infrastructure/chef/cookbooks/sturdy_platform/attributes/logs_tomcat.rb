default['sturdy']['platform']['logs']['tomcat'].tap do |config|
  config['datetime_format'] = '%d/%b/%Y:%H:%M:%S %z' # 08/Apr/2017:08:00:06 +0000
  case node['platform_family']
  when 'rhel'
    config['package_names'] = %w(tomcat7)
    config['access_logs'] = '/opt/tomcat_server/logs/*_access_log.*.txt'
    config['access_logs_group_name'] = '/opt/tomcat_server/logs/access_log.txt'
    config['access_logs_datetime'] = '%d/%b/%Y:%H:%M:%S %z' # 08/Apr/2017:08:00:06 +0000
    config['catalina_out_log'] = '/opt/tomcat_server/logs/catalina.out'
    config['catalina_out_datetime'] = '%d-%b-%Y %H:%M:%S' # 26-Apr-2017 16:37:30.078
  when 'debian'
    config['package_names'] = %w(tomcat7)
    config['access_logs'] = '/opt/tomcat_server/logs/*_access_log.*.txt'
    config['access_logs_group_name'] = '/opt/tomcat_server/logs/access_log.txt'
    config['access_logs_datetime'] = '%d/%b/%Y:%H:%M:%S %z' # 08/Apr/2017:08:00:06 +0000
    config['catalina_out_log'] = '/opt/tomcat_server/logs/catalina.out'
    config['catalina_out_datetime'] = '%d-%b-%Y %H:%M:%S' # 26-Apr-2017 16:37:30.078
  end
end
