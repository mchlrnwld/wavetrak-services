
if node['ec2'] && node['ec2']['region'] # Chef 13+
  default['sturdy']['platform']['letsencrypt_resource']['route53_region'] = node['ec2']['region']
elsif node['ec2'] && node['ec2']['placement_availability_zone'] # Chef 12
  default['sturdy']['platform']['letsencrypt_resource']['route53_region'] = node['ec2']['placement_availability_zone'][0...-1]
else
  default['sturdy']['platform']['letsencrypt_resource']['route53_region'] = 'us-east-1'
end

default['sturdy']['platform']['letsencrypt_resource']['acmesharp_version'] =
  '0.9.1.326'

default['sturdy']['platform']['letsencrypt_resource']['use_sni'] = false
