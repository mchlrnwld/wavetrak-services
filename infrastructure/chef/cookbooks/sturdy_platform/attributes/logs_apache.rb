default['sturdy']['platform']['logs']['apache'].tap do |config|
  config['datetime_format'] = '%d/%b/%Y:%H:%M:%S'
  case node['platform_family']
  when 'rhel'
    config['package_names'] = %w(httpd httpd24)
    config['access_log'] = '/var/log/httpd/access_log'
  when 'debian'
    config['package_names'] = %w(apache2)
    config['access_log'] = '/var/log/apache2/access.log'
  end
end
