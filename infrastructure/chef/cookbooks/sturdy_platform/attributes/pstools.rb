default['sturdy']['platform']['pstools']['source'] = 'https://download.sysinternals.com/files/PSTools.zip'
default['sturdy']['platform']['pstools']['checksum'] = '91c36d9794f031f9756c4b2c2dbfd315c83e05be13fd3932cba878794b4e828e'
default['sturdy']['platform']['pstools']['path'] = 'C:\SysinternalsSuite'
