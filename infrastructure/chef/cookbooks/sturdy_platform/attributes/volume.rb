case node['os']
when 'linux'
  default['sturdy']['platform']['volume_resource']['dev_name'] = '/dev/xvdn'
  default['sturdy']['platform']['volume_resource']['mountpoint'] = '/mnt/persistent_vol'
when 'windows'
  default['sturdy']['platform']['volume_resource']['dev_name'] = 'xvdb'
  default['sturdy']['platform']['volume_resource']['mountpoint'] = nil # drive letter will be automatically assigned
end
