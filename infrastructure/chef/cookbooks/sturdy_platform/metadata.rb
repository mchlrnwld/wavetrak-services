name 'sturdy_platform'
maintainer 'Sturdy Networks'
maintainer_email 'devops@sturdy.cloud'
license 'all_rights'
description 'Deploys the Sturdy Platform'
long_description 'Deploys the Sturdy Platform'
source_url 'https://bitbucket.org/nbdev/sturdy_platform-cookbook'
version '2.7.3'

chef_version '>= 12.15'

%w(ubuntu centos redhat amazon).each do |i|
  supports i
end

gem 'aws-sdk-ec2', '~> 1.84.0'
gem 'aws-sdk-lambda', '~> 1.18.0'

depends 'aws', '> 2'
depends 'poise-python', '~> 1.6'
depends 'sturdy_ssm_agent', '~> 1.0'
depends 'windows', '~> 3.3'
