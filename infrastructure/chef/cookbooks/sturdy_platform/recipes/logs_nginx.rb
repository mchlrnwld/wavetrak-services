#
# Cookbook Name:: sturdy_platform
# Recipe:: logs_nginx
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

%w(
  access
  error
).each do |l|
  template "/opt/sturdy/etc/logsforwarder/config/nginx_#{l}_log" do
    source 'generic_logsforwarder.conf.erb'
    variables(
      monitored_file: node['sturdy']['platform']['logs']['nginx'][l]['log_file'],
      log_group_name: node['sturdy']['platform']['logs']['nginx'][l]['log_file'],
      datetime_format: node['sturdy']['platform']['logs']['nginx'][l]['datetime_format']
    )
    only_if { ::File.exist? node['sturdy']['platform']['logs']['nginx'][l]['log_file'] }
    node['sturdy']['platform']['logs']['nginx']["#{l}"]['package_names'].each do |p|
      subscribes :create, "package[#{p}]", :delayed
    end
    notifies :restart, 'service[sturdy-platform]', :delayed
  end
end
