#
# Cookbook Name:: sturdy_platform
# Recipe:: logs_openvpn
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

template '/opt/sturdy/etc/logsforwarder/config/openvpn_log' do
  source 'generic_logsforwarder.conf.erb'
  variables(
    monitored_file: node['sturdy']['platform']['logs']['openvpn']['log_file'],
    log_group_name: node['sturdy']['platform']['logs']['openvpn']['log_file'],
    datetime_format: node['sturdy']['platform']['logs']['openvpn']['datetime_format']
  )
  only_if { ::File.exist? node['sturdy']['platform']['logs']['openvpn']['log_file'] }
  node['sturdy']['platform']['logs']['openvpn']['package_names'].each do |p|
    subscribes :create, "package[#{p}]", :delayed
  end
  notifies :restart, 'service[sturdy-platform]', :delayed
end
