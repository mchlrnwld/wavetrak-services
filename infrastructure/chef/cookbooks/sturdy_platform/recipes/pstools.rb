#
# Cookbook Name:: sturdy_platform
# Recipe:: pstools
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

# https://docs.microsoft.com/en-us/sysinternals/downloads/pstools

windows_zipfile 'sturdy_platform_install_pstools' do
  path node['sturdy']['platform']['pstools']['path']
  source node['sturdy']['platform']['pstools']['source']
  checksum node['sturdy']['platform']['pstools']['checksum'] unless node['sturdy']['platform']['pstools']['checksum'].nil?
  action :unzip
  not_if { ::File.directory?(node['sturdy']['platform']['pstools']['path']) }
end
