#
# Cookbook Name:: sturdy_platform
# Recipe:: logs_chef_client
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

template '/opt/sturdy/etc/logsforwarder/config/chef_client_logs' do
  source 'generic_logsforwarder.conf.erb'
  variables(
    monitored_file: node['sturdy']['platform']['logs']['chef_client']['log_file'],
    log_group_name: node['sturdy']['platform']['logs']['chef_client']['log_group'],
    datetime_format: node['sturdy']['platform']['logs']['apache']['datetime_format']
  )
  only_if { ::File.exist? node['sturdy']['platform']['logs']['chef_client']['log_file'] }
  node['sturdy']['platform']['logs']['chef_client']['package_names'].each do |p|
    subscribes :create, "package[#{p}]", :delayed
  end
  notifies :restart, 'service[sturdy-platform]', :delayed
end
