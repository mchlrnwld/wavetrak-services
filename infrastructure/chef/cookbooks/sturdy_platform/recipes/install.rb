#
# Cookbook Name:: sturdy_platform
# Recipe:: install
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

require 'digest'

# SSM agent install
include_recipe 'sturdy_ssm_agent::default' unless
  node['platform_family'].include? 'windows'

# Platform dependencies
package 'sturdy-platform-dependencies' do
  case node['platform']
  when 'debian', 'ubuntu'
    package_name 'python-minimal'
  when 'amazon'
    package_name 'python27'
  else
    action :nothing
  end
end

filename = node['sturdy']['platform']['installer']['url'].split('/').last
local_path = "#{Chef::Config[:file_cache_path]}/#{filename}"

remote_file local_path do
  source node['sturdy']['platform']['installer']['url']
  mode '0755'
end

package 'sturdy-platform' do
  case node['platform_family']
  when 'debian'
    provider Chef::Provider::Package::Dpkg
  when 'rhel'
    provider Chef::Provider::Package::Rpm
  end
  source local_path
  action :install
end

ruby_block 'upgrade_platform_if_out_of_date' do
  block do
    # Using only_if condition here as trigger for upgrade
    true
  end
  only_if { node['packages']['sturdy-platform'] && (::Gem::Version.new(node['packages']['sturdy-platform']['version']) < ::Gem::Version.new(node['sturdy']['platform']['installer']['version'])) }
  notifies :upgrade, 'package[sturdy-platform]', :immediately
end

# Backport platform features
# platform < v1.1.1 did not support newer cloudwatch handler features; until it
# is verified/released, patch the file directly here
# This adds the `collect_without_dimension` option
platform_cw_handler =
  '/opt/sturdy/var/python/lib/python2.7/site-packages/diamond/handler/cloudwatch.py'
remote_file 'update_platform_cloudwatch_handler' do
  path platform_cw_handler
  source 'https://raw.githubusercontent.com/python-diamond/Diamond/a87022edf33553064afa81ad02dec8788209a3de/src/diamond/handler/cloudwatch.py'
  mode 0644
  only_if { ::Digest::SHA256.hexdigest(File.read(platform_cw_handler)) == '3a3129ed1a191e02a3371dd83a4759f42ca4de2598df214d24fe61c1baf16c2f' }
  notifies :restart, 'service[sturdy-platform]', :delayed if
    ::File.exist?(platform_cw_handler)
end

# platform < v1.1.2 did not support service restarts properly
execute 'fix_platform_init' do
  command 'sed -i "s/for i in {1..60}/for i in \$(seq 1 60)/" /etc/init.d/sturdy-platform'
  only_if 'grep "for i in {1..60}" /etc/init.d/sturdy-platform'
end

service 'sturdy-platform' do
  provider Chef::Provider::Service::Init
  action [:start]
end
