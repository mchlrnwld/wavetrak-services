#
# Cookbook Name:: sturdy_platform
# Recipe:: default
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe "#{cookbook_name}::install"
include_recipe "#{cookbook_name}::logs_apache"
include_recipe "#{cookbook_name}::logs_nginx"
include_recipe "#{cookbook_name}::logs_chef_client"
include_recipe "#{cookbook_name}::logs_openvpn"
include_recipe "#{cookbook_name}::logs_tomcat"
