#
# Cookbook Name:: sturdy_platform
# Recipe:: logs_apache
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

template '/opt/sturdy/etc/logsforwarder/config/apache_access_log' do
  source 'generic_logsforwarder.conf.erb'
  variables(
    monitored_file: node['sturdy']['platform']['logs']['apache']['access_log'],
    log_group_name: node['sturdy']['platform']['logs']['apache']['access_log'],
    datetime_format: node['sturdy']['platform']['logs']['apache']['datetime_format']
  )
  only_if { ::File.exist? node['sturdy']['platform']['logs']['apache']['access_log'] }
  node['sturdy']['platform']['logs']['apache']['package_names'].each do |p|
    subscribes :create, "package[#{p}]", :delayed
  end
  notifies :restart, 'service[sturdy-platform]', :delayed
end
