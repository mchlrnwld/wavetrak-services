#
# Cookbook Name:: sturdy_platform
# Recipe:: logs_tomcat
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

template '/opt/sturdy/etc/logsforwarder/config/tomcat_catalina_out' do
  source 'generic_logsforwarder.conf.erb'
  variables(
    monitored_file: node['sturdy']['platform']['logs']['tomcat']['catalina_out_log'],
    log_group_name: node['sturdy']['platform']['logs']['tomcat']['catalina_out_log'],
    datetime_format: node['sturdy']['platform']['logs']['tomcat']['catalina_out_datetime']
  )
  only_if { ::File.exist? node['sturdy']['platform']['logs']['tomcat']['catalina_out_log'] }
  node['sturdy']['platform']['logs']['tomcat']['package_names'].each do |p|
    subscribes :create, "package[#{p}]", :delayed
  end
  notifies :restart, 'service[sturdy-platform]', :delayed
end

template '/opt/sturdy/etc/logsforwarder/config/tomcat_access_logs' do
  source 'generic_logsforwarder.conf.erb'
  variables(
    monitored_file: node['sturdy']['platform']['logs']['tomcat']['access_logs'],
    log_group_name: node['sturdy']['platform']['logs']['tomcat']['access_logs_group_name'],
    datetime_format: node['sturdy']['platform']['logs']['tomcat']['access_logs_datetime']
  )
  # Checking the catalina.out file here since the access logs may be a multifile pattern
  only_if { ::File.exist? node['sturdy']['platform']['logs']['tomcat']['access_logs'] }
  node['sturdy']['platform']['logs']['tomcat']['package_names'].each do |p|
    subscribes :create, "package[#{p}]", :delayed
  end
  notifies :restart, 'service[sturdy-platform]', :delayed
end
