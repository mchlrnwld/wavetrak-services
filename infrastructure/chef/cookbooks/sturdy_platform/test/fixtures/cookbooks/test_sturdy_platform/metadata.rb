name             'test_sturdy_platform' # ~FC064, ~FC065
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Corrects inspec TTY issues'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'sudo', '~> 3.3'
