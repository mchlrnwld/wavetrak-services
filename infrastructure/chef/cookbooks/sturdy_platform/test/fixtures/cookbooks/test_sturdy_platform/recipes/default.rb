#
# Cookbook Name:: test_sturdy_platform
# Recipe:: default
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'test_sturdy_platform::_fix_tty' if node['platform'] == 'centos'
