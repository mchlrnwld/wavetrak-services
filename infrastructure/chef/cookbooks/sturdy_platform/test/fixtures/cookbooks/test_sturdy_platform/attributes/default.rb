default['authorization']['sudo']['include_sudoers_d'] = true

default['inspec_user'] = case node['platform']
                         when 'centos'
                           'centos'
                         when 'amazon'
                           'ec2-user'
                         when 'ubuntu'
                           'ubuntu'
                         end
