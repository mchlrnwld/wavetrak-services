# # encoding: utf-8

# Inspec test for recipe sturdy_platform::default

describe package('sturdy-platform') do
  it { should be_installed }
end

describe file('/etc/init.d/sturdy-platform') do
  it { should be_file }
  it { should be_executable }
end

%w(metricsforwarder logsforwarder).each do |f|
  describe file(::File.join('/opt/sturdy/etc/init.d', f)) do
    it { should be_file }
    it { should be_executable }
  end

  # Are the appropriate daemons running?
  describe command("ps aux | grep #{f.gsub('der', '[der]')}") do
    its('exit_status') { should eq 0 }
  end
end
