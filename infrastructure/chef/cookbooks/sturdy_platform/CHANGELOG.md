# sturdy_platform Cookbook CHANGELOG

This file is used to list changes made in each version of the sturdy_platform cookbook.

## 2.7.3 (2018-01-17)

- Backport v3.0.1 sturdy_platform_volume fix to v2.7

## 2.7.2 (2017-12-08)

- Update Windows `letsencrypt` renewal task to support maximum time duration where possible

## 2.7.1 (2017-12-08)

- Fix `letsencrypt` automatic renewal on Windows

## 2.7.0 (2017-12-07)

- Add Windows support to letsencrypt & volume resources

## 2.6.0 (2017-11-21)

- Change `ssm_agent` dependency to `sturdy_ssm_agent`

## 2.5.1 (2017-11-20)

- Fixed tomcat `access_logs` path to match value of `access_logs`

## 2.5.0 (2017-11-03)

- Added nginx logs support

## 2.4.0 (2017-10-23)

- Add `environment` property to `letsencrypt` resource

## 2.3.1 (2017-10-23)

- Incorporate ssm_agent attribute fix from PR 6 (eliminates errors when ec2 ohai plugin doesn't run)

## 2.3.0 (2017-10-06)

- Add `letsencrypt` resource

## 2.2.0 (2017-10-04)

- Add `eni` & `volume` resources

## 2.1.1 (2017-06-29)

- Backport service restart fix

## 2.1.0 (2017-06-27)

- Deploy updated CloudWatch metrics handler

## 2.0.2 (2017-04-27)

- Fix ssm cookbook use with chef-client 13

## 2.0.1 (2017-04-26)

- Update Tomcat date/time format to match `tomcat` cookbook format


## 2.0.0 (2017-04-26)

- Major version bump to account for new attribute format for 'monitored' packages. This is only a breaking change if your wrapper cookbook overrode the one of the old attributes (e.g. `default['sturdy']['platform']['logs']['apache']['package_name'] = 'mycustompkgname'`).
