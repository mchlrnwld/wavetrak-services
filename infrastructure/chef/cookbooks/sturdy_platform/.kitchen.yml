---
driver:
  name: ec2
  instance_type: m3.medium
  aws_ssh_key_id: <%= ENV['AWS_SSH_KEY_ID'] ? ENV['AWS_SSH_KEY_ID'] : 'sturdylab-uswest2' %>
  spot_price: "0.10"
  tags:
    Name: kitchen-sturdy-platform-dev
    created-by: test-kitchen
  region: <%= ENV['AWS_DEFAULT_REGION'] ? ENV['AWS_DEFAULT_REGION'] : 'us-west-2' %>
  iam_profile_name: <%= ENV['TESTKITCHEN_IAM_PROFILE'] ? ENV['TESTKITCHEN_IAM_PROFILE'] : 'SturdyLab-Core-InstanceProfile-13D0267ZW1217' %>
  security_group_ids: <%= ENV['TESTKITCHEN_EC2_SEC_GROUP_IDS'] ? ENV['TESTKITCHEN_EC2_SEC_GROUP_IDS'] : 'sg-bda64fda' %>
  subnet_id: <%= ENV['TESTKITCHEN_EC2_SUBNET_ID'] ? ENV['TESTKITCHEN_EC2_SUBNET_ID'] : 'subnet-b51928c2' %>

# can't use default location as /var/log/chef doesn't exist at start of the run
chef_client_log_file: &chef_client_log_file
  log_file: "/var/log/chef-client.log"

provisioner:
  name: chef_zero
  always_update_cookbooks: <%= !ENV['CI'] %>
  <<: *chef_client_log_file
  log_level: info
  require_chef_omnibus: 12.19.36

transport:
  name: rsync
  # We want to set this so that test-kitchen doesn't fall back to ssh password
  # when the instances first spin up and don't have the key enabled
  ssh_key: <%= ENV['TESTKITCHEN_SSH_KEY'] ? ENV['TESTKITCHEN_SSH_KEY'] : '~/.ssh/sturdylab-uswest2' %>

verifier:
  name: inspec

platforms:
  - name: ubuntu-14.04
    run_list:
      - recipe[apt::default]
  - name: ubuntu-16.04
    run_list:
      - recipe[apt::default]
  - name: centos-7
    driver:
      block_device_mappings:
        - device_name: /dev/sda1
          ebs:
            delete_on_termination: true
    run_list:
      - recipe[test_sturdy_platform::default]
  - name: amazon-2015.09
    driver:
      # 2015.09.2 in us-east-1 if specified otherwise falling back to us-west-2
      image_id: <%= (ENV['AWS_DEFAULT_REGION'] && ENV['AWS_DEFAULT_REGION'] == 'us-east-1') ? 'ami-8fcee4e5' : 'ami-63b25203' %>
      user_data: "#cloud-config\nrepo_releasever: 2015.09\n"
    run_list:
      - recipe[test_sturdy_platform::default]
    transport:
      username: ec2-user
  - name: amazon-2016.03
    driver:
      # 2016.03.3 in us-east-1 if specified otherwise falling back to us-west-2
      image_id: <%= (ENV['AWS_DEFAULT_REGION'] && ENV['AWS_DEFAULT_REGION'] == 'us-east-1') ? 'ami-6869aa05' : 'ami-7172b611' %>
      user_data: "#cloud-config\nrepo_releasever: 2016.03\n"
    run_list:
      - recipe[test_sturdy_platform::default]
    transport:
      username: ec2-user
  - name: amazon-2016.09
    driver:
      # 2016.09.1 in us-east-1 if specified otherwise falling back to us-west-2
      image_id: <%= (ENV['AWS_DEFAULT_REGION'] && ENV['AWS_DEFAULT_REGION'] == 'us-east-1') ? 'ami-9be6f38c' : 'ami-1e299d7e' %>
      user_data: "#cloud-config\nrepo_releasever: 2016.09\n"
    run_list:
      - recipe[test_sturdy_platform::default]
    transport:
      username: ec2-user
  - name: amazon-2017.03
    driver:
      # 2017.03.0 in us-east-1 if specified otherwise falling back to us-west-2
      image_id: <%= (ENV['AWS_DEFAULT_REGION'] && ENV['AWS_DEFAULT_REGION'] == 'us-east-1') ? 'ami-22ce4934' : 'ami-8ca83fec' %>
      user_data: "#cloud-config\nrepo_releasever: 2017.03\n"
    run_list:
      - recipe[test_sturdy_platform::default]
    transport:
      username: ec2-user

suites:
  - name: default
    run_list:
      - recipe[sturdy_platform::default]
    verifier:
      inspec_tests:
        - test/recipes
    attributes:
      sturdy:
        platform:
          logs:
            chef_client:
              <<: *chef_client_log_file
