sturdy-auto-snapshot-tool Cookbook
==================================

This cookbook configures automated snapshot creation and deletion.

Requirements
------------

#### AWS IAM policy
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "SturdyAutoSnapshotToolPermissions",
            "Effect": "Allow",
            "Action": [
                "ec2:CreateSnapshot",
                "ec2:DescribeSnapshots",
                "ec2:DeleteSnapshot",
                "ec2:DescribeVolumes",
                "ec2:ModifySnapshotAttribute",
                "ec2:CreateTags"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```

For instances created without this IAM policy it is also possible to create a '.aws/credentials' file in the executing user's home directory with the following information:

```
[default]
region =
aws_access_key_id =
aws_secret_access_key =
```

Please note that this is not the recommended solution.


#### cookbooks
- `cron` - sturdy-auto-snapshot-tool needs cron

Attributes
----------
TODO: List your cookbook attributes here.

e.g.
#### sturdy-auto-snapshot-tool::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['sturdy-auto-snapshot-tool']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### sturdy-auto-snapshot-tool::default

Just include `sturdy-auto-snapshot-tool` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[sturdy-auto-snapshot-tool]"
  ]
}
```

License and Authors
-------------------
Authors: Brandon Pierce (<brandon.pierce@sturdynetworks.com>)
