#
# Cookbook Name:: sturdy-auto-snapshot-tool
# Recipe:: default
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# include cron cookbook for creating cron jobs
include_recipe 'cron'

# create the script directory
directory node['sturdy-auto-snapshot-tool']['script_dir'] do
  owner   'root'
  group   'root'
  mode    0755
end

# install Python package manager
case node['platform_family']
when 'debian'
  package 'python-pip'
end

# install package pre-reqs
case node['platform_family']
when 'debian'
  %w{awscli boto}.each do |package|
    python_pip package do
      action  :install
    end
  end
end

# create the snapshot script
template node['sturdy-auto-snapshot-tool']['script_dir'] + '/manage_volume_snapshots.py' do
  source    'manage_volume_snapshots.py.erb'
  owner     'root'
  group     'root'
  mode      '0755'
end

# setup cron job to create snapshots
cron_d 'create-snapshots' do
  minute    0
  hour      0
  command   node['sturdy-auto-snapshot-tool']['script_dir'] + '/manage_volume_snapshots.py -a create'
  user      node['sturdy-auto-snapshot-tool']['user']
  # cleanup this 1.x cron job
  action    :delete
end

# setup cron job to create snapshots
%w(daily weekly monthly).each do |v|
  cron_d 'create-snapshots-' + v do
    predefined_value  '@' + v
    command   node['sturdy-auto-snapshot-tool']['script_dir'] +
      '/manage_volume_snapshots.py -a create --retention-policy ' + v
    user      node['sturdy-auto-snapshot-tool']['user']
  end
end

# setup cron job to purge snapshots
cron_d 'purge-snapshots' do
  minute    0
  hour      2
  command   node['sturdy-auto-snapshot-tool']['script_dir'] + '/manage_volume_snapshots.py -a purge -d ' + node['sturdy-auto-snapshot-tool']['days']
  user      node['sturdy-auto-snapshot-tool']['user']
  # cleanup this 1.x cron job
  action    :delete
end

# setup cron job to purge snapshots
%w(daily weekly monthly).each do |v|
  cron_d 'purge-snapshots-' + v do
    predefined_value  '@' + v
    command   node['sturdy-auto-snapshot-tool']['script_dir'] + \
      '/manage_volume_snapshots.py -a purge --retention-policy ' + v + \
      ' --retain ' + node['sturdy-auto-snapshot-tool']['retention'][v]
    user      node['sturdy-auto-snapshot-tool']['user']
  end
end
