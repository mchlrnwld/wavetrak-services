# platform specific attributes
case node['platform']
when 'ubuntu'
  default['sturdy-auto-snapshot-tool']['user'] = 'ubuntu'
  default['sturdy-auto-snapshot-tool']['group'] = 'ubuntu'
when 'amazon'
  default['sturdy-auto-snapshot-tool']['user'] = 'ec2-user'
  default['sturdy-auto-snapshot-tool']['group'] = 'ec2-user'
end

# common attributes
default['sturdy-auto-snapshot-tool']['script_dir'] = '/opt/sturdy-auto-snapshot-tool'

# retention policy attributes
default['sturdy-auto-snapshot-tool']['days'] = '30'
default['sturdy-auto-snapshot-tool']['retention']['daily'] = '7'
default['sturdy-auto-snapshot-tool']['retention']['weekly'] = '1'
default['sturdy-auto-snapshot-tool']['retention']['monthly'] = '3'
