sturdy-auto-snapshot-tool CHANGELOG
===================================

2.0.0
-----
- Brandon Pierce - Support more fine grained retention policy

1.0.1
-----
- Brandon Pierce - Only install the `python-pip` package on Ubuntu

1.0.0
-----
- Brandon Pierce - Initial release of sturdy-auto-snapshot-tool
