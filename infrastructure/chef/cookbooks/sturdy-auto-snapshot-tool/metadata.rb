name             'sturdy-auto-snapshot-tool'
maintainer       'Sturdy Networks'
maintainer_email 'brandon.pierce@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures sturdy-auto-snapshot-tool'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.0.3'

depends 'cron',    '~> 1.6'
depends 'poise-python',  '~> 1.6.0'

%w(amazon ubuntu).each do |os|
  supports os
end
