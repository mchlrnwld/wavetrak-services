# # encoding: utf-8

# Inspec test for recipe surfline-highwinds-ftp::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe port(2121) do
  it { should be_listening }
end

describe pip('s3ftpd') do
  it { should be_installed }
end

describe service('s3ftpd') do
  it { should be_running }
end
