name 'surfline-highwinds-ftp'
maintainer 'Sturdy Networks, LLC'
maintainer_email 'devops@sturdynetworks.com'
license 'All Rights Reserved'
description 'Installs/Configures surfline-highwinds-ftp'
long_description 'Installs/Configures surfline-highwinds-ftp'
version '0.1.2'
chef_version '>= 12.1' if respond_to?(:chef_version)

depends 'aws_parameter_store', '~> 2.1.1'
depends 's3_presigned_url', '~> 1.1'
depends 'poise-python', '~> 1.6'
depends 'openssl'
depends 'aws', '~> 7.1'
depends 'build-essential', '~> 8.0'
depends 'iptables', '~> 4.2'
depends 'sysctl', '~> 0.10'

gem 'aws-sdk-ec2', '~> 1.84.0'
