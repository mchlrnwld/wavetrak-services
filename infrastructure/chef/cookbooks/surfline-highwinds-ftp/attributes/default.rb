default['company'] = 'surfline'
default['environment'] = 'dev'
default['application']['name'] = 'highwinds'
default['application']['directory'] = '/etc/s3ftpd'
default['application']['port'] = '2121' # port the process runs (actually listens on 21)
default['application']['pasv_min_port'] = 4242
default['application']['pasv_max_port'] = 4257
default['application']['permit_foreign_addresses'] = false
default['application']['user'] = 's3ftpd'
default['aws_parameter_store']['region'] = 'us-west-1'
default['s3_presigned_url']['region'] = 'us-west-1'
environment = node.chef_environment != "_default" ? node.chef_environment : node['environment']

# S3FTPD
default['s3ftpd']['path'] = "#{environment}/#{node['application']['name']}"
default['s3ftpd']['version'] = "0.2.5"

default['route53']['name'] = "internal-hwupload.sandbox.surfline.com"
default['route53']['ttl'] = 60
default['route53']['zone_id'] = "Z3DM6R3JR1RYXV" # sandbox.surfline.com

# Self Signed Certificate
default['cert']['common_name'] = '*.surfline.com'
default['cert']['org'] = 'Surfline Wavetrak Inc'
default['cert']['org_unit'] = 'Highwinds'
default['cert']['country'] = 'US'
default['cert']['expire'] = 365 # Days until certificate expires

default['yum_install']['packages'] = ['gcc', 'libffi-devel', 'python27-pip',
                                      'openssl-devel']
