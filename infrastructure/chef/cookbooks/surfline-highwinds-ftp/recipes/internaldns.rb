#
# Cookbook:: surfline-highwinds-ftp
# Recipe:: internaldns
#
# Copyright:: 2017, Sturdy Networks, LLC, All Rights Reserved.

aws_route53_record "internal dns entry" do
  name node['route53']['name']
  value node['ipaddress']
  type "A"
  ttl node['route53']['ttl']
  zone_id node['route53']['zone_id']
  fail_on_error true
end
