#
# Cookbook:: surfline-highwinds-ftp
# Recipe:: install
#
# Copyright:: 2017, Sturdy Networks, LLC, All Rights Reserved.
environment = node.chef_environment != "_default" ? node.chef_environment : node['environment']
my_params = aws_parameter_store["#{environment}.#{node['company']}.#{node['application']['name']}.ftp*"]
# my_params = aws_parameter_store[%W(#{environment}.#{node['company']}.#{node['application']['name']}.ftpuser
#   #{environment}.#{node['company']}.#{node['application']['name']}.ftppass)]

require 'yaml'
include_recipe 'iptables'

node['yum_install']['packages'].each do |package|
  yum_package package
end

user node['application']['user'] do
  comment 'User for running s3ftpd'
  shell '/sbin/nologin'
end

directory node['application']['directory'] do
  owner node['application']['user']
  group node['application']['user']
  mode '0755'
  action :create
end

config = {
  'bucket' => node['application']['bucketname'],
  'port' => node['application']['port'],
  'pasv_min_port' => node['application']['pasv_min_port'],
  'pasv_max_port' => node['application']['pasv_max_port'],
  'permit_foreign_addresses' => node['application']['permit_foreign_addresses'],
  'users' => [
    {'username' => my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpuser"],
    'password' => my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftppass"]}
  ]
}

text = my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpcert"] + " " + my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpkey"]
keycert = ''
text.tr("\n", ' ').split(/\- | \-/).each do |i|
  keycert += (/^-/ =~ i) ? "\n"+i.tr_s('-', '-').gsub('-', '-----')+"\n" : i.split(' ').join("\n")
end

file "#{node['application']['directory']}/keycert.pem" do
  content keycert
  owner node['application']['user']
  group node['application']['user']
  mode '0400'
  sensitive true
  not_if {my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpcert"] == nil ||
          my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpkey"] == nil }
  notifies :restart, 'service[s3ftpd]', :delayed if ::File.exist?('/etc/init.d/s3ftpd')
end

# self signed certificate
openssl_x509 "#{node['application']['directory']}/cert.pem" do
  common_name node['cert']['common_name']
  org node['cert']['org']
  org_unit node['cert']['org_unit']
  country node['cert']['country']
  expire node['cert']['expire']
  key_file "#{node['application']['directory']}/key.pem"
  notifies :run, 'execute[Join certs and key]', :immediately
  only_if {my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpcert"] == nil ||
           my_params["#{environment}.#{node['company']}.#{node['application']['name']}.ftpkey"] == nil }
end

execute 'Join certs and key' do
  command <<-EOF
    cat #{node['application']['directory']}/cert.pem #{node['application']['directory']}/key.pem > #{node['application']['directory']}/keycert.pem
    chown #{node['application']['user']}:#{node['application']['user']} #{node['application']['directory']}/keycert.pem
    chmod 0400 #{node['application']['directory']}/keycert.pem
EOF
  action :nothing
  notifies :restart, 'service[s3ftpd]', :delayed if ::File.exist?('/etc/init.d/s3ftpd')
end

s3ftpd_path = "#{node['s3ftpd']['path']}/s3ftpd-#{node['s3ftpd']['version']}.tar.gz"

python_execute "-m pip install \"#{s3_presigned_url[s3ftpd_path]}\"" do
  notifies :restart, 'service[s3ftpd]', :delayed if ::File.exist?('/etc/init.d/s3ftpd')
  not_if "pip show --disable-pip-version-check -V s3ftpd | grep \"^Version\" | grep #{node['s3ftpd']['version']}"
end

file "#{node['application']['directory']}/s3ftpd.yml" do
  content config.to_yaml
  owner node['application']['user']
  group node['application']['user']
  mode '0400'
  sensitive true
  notifies :restart, 'service[s3ftpd]', :delayed if ::File.exist?('/etc/init.d/s3ftpd')
end

directory '/var/log/s3ftpd' do
  owner node['application']['user']
  group node['application']['user']
  action :create
end

directory '/var/run/s3ftpd' do
  owner 'root'
  group 'root'
  action :create
end

template '/etc/init.d/s3ftpd' do
  source 'init.erb'
  mode '0755'
  variables({
    :user => node['application']['user']
    })
end

sysctl_param 'net.ipv4.ip_forward' do
  value 1
end

iptables_rule 'ftp_21' do
  lines "-A PREROUTING -i eth0 -p tcp --dport 21 -j REDIRECT --to-port #{node['application']['port']}"
  table :nat
end

service 's3ftpd' do
  action :start
end
