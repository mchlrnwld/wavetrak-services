#
# Cookbook:: surfline-highwinds-ftp
# Recipe:: default
#
# Copyright:: 2017, Sturdy Networks, LLC, All Rights Reserved.

include_recipe 'build-essential::default'

# Attach instance EIP
# Would likely be better to call the elastic_ip resource in the aws cookbook,
# but this sidesteps any potential issues with the library load time intallation
# of the aws-sdk gem.
ruby_block 'attach_eip' do # ~FC014
  block do
    require 'aws-sdk-ec2'

    region = node['ec2']['placement_availability_zone'][0...-1]
    client = Aws::EC2::Client.new(region: region)
    resource = Aws::EC2::Resource.new(client: client)

    instance = resource.instance(node['ec2']['instance_id'])
    eip_address = instance.tags.detect {|t| t['key'] == 'eip_address'}['value']
    if !node['cloud_v2']['public_ipv4'] ||
       node['cloud_v2']['public_ipv4'] != eip_address

      eip_id =
        client.describe_addresses(public_ips: [eip_address]).addresses[0][:allocation_id]
      client.associate_address({
        allocation_id: eip_id,
        instance_id: node['ec2']['instance_id'],
      })
      node.normal['aws']['elastic_ip'][eip_address]['ip'] = eip_address # matching style of aws cookbook
    end
  end
  not_if { ENV['TEST_KITCHEN'] }
  not_if { node['aws'] && node['aws']['elastic_ip'] }
end

include_recipe 'surfline-highwinds-ftp::install'
include_recipe 'surfline-highwinds-ftp::internaldns' unless ENV['TEST_KITCHEN']
