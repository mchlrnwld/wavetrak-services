#AWS Parameter Store Configuration
default['aws_parameter_store']['region'] = 'us-west-1'

#SSM
default['aws_ssm']['env'] = if node.chef_environment != "_default"
                              node.chef_environment
                            else
                              'dev'
                            end

default['aws_ssm']['base_key'] = "#{node['aws_ssm']['env']}/airflow-celery"

# Surfline Configuration
default['surfline-airflow']['version'] = '1.7.1.3'
default['surfline-airflow']['artifactory']['host']                    = 'surfline.jfrog.io'
default['surfline-airflow']['artifactory']['path']                    = case node.chef_environment
                                                                            when 'prod'
                                                                                '/surfline/api/pypi/pypi/simple'
                                                                            when 'dev'
                                                                                '/surfline/api/pypi/pypi-dev/simple'
                                                                            else
                                                                                '/surfline/api/pypi/pypi-dev/simple'
                                                                            end
default['surfline-airflow']['script_path'] = '/ocean/code'

default['airflow']['installer'] = case node.chef_environment
                                      when 'prod'
                                          'https://s3-us-west-1.amazonaws.com/sl-artifacts-prod/airflow/apache-airflow-1.7.1.3-0-g209bf9c.tar.gz'
                                      else
                                          'https://s3-us-west-1.amazonaws.com/sl-artifacts-dev/airflow/apache-airflow-1.7.1.3-0-g209bf9c.tar.gz'
                                      end

# User configuration
default['airflow']['user'] = 'airflow'
default['airflow']['group'] = 'airflow'
default['airflow']['user_uid'] = 500
default['airflow']['group_gid'] = 500
default['airflow']['user_home_directory'] = "/home/#{node['airflow']['user']}"
default['airflow']['shell'] = '/bin/bash'

# General config
default['airflow']['directories_mode'] = '0775'
default['airflow']['config_file_mode'] = '0644'
default['airflow']['log_path'] = "/home/#{node['airflow']['user']}/airflow/service-log"

# airflow.cfg configurations. The required entries listed below, you can add more sections and configs.
# The structure default['airflow']['config']['CONFIG_SECTION']['CONFIG_ENTRY']

# Required core airflow.cfg settings
default['airflow']['config']['core']['airflow_home'] = "/home/#{node['airflow']['user']}/airflow"
default['airflow']['config']['core']['dags_folder'] = "/ocean/static/airflow/dags"
default['airflow']['config']['core']['plugins_folder'] = "/ocean/static/airflow/plugins"
default['airflow']['config']['core']['base_log_folder'] = "/ocean/static/airflow/logs"
default['airflow']['config']['core']['sql_alchemy_conn'] = "sqlite:///#{node['airflow']['config']['core']['airflow_home']}/airflow.db"
default['airflow']['config']['core']['sql_alchemy_pool_size'] = 5
default['airflow']['config']['core']['sql_alchemy_pool_recycle'] = 3600
default['airflow']['config']['core']['executor'] = 'CeleryExecutor'
default['airflow']['config']['core']['parallelism'] = 128
default['airflow']['config']['core']['dag_concurrency'] = 16
default['airflow']['config']['core']['dags_are_paused_at_creation'] = 'False'
default['airflow']['config']['core']['max_active_runs_per_dag'] = 16
default['airflow']['config']['core']['load_examples'] = 'False'
default['airflow']['config']['core']['fernet_key'] = 'cryptography_not_found_storing_passwords_in_plain_text'
default['airflow']['config']['core']['donot_pickle'] = 'False'
default['airflow']['config']['core']['dagbag_import_timeout'] = 30

# Required webserver airflow.cfg settings
# FIXME: host IP address
default['airflow']['config']['webserver']['base_url'] = "http://#{node['hostname']}.aws.surfline.com:8080"
default['airflow']['config']['webserver']['web_server_host'] = '0.0.0.0'
default['airflow']['config']['webserver']['web_server_port'] = 8080
default['airflow']['config']['webserver']['secret_key'] = 'temporary_key'
default['airflow']['config']['webserver']['workers'] = 8
default['airflow']['config']['webserver']['worker_class'] = 'sync'
default['airflow']['config']['webserver']['expose_config'] = true
default['airflow']['config']['webserver']['authenticate'] = 'False'
default['airflow']['config']['webserver']['filter_by_owner'] = 'False'

# Required scheduler airflow.cfg settings
default['airflow']['config']['scheduler']['job_heartbeat_sec'] = 5
default['airflow']['config']['scheduler']['scheduler_heartbeat_sec'] = 5

# Required celery airflow.cfg settings
default['airflow']['config']['celery']['celery_app_name'] = 'airflow.executors.celery_executor'
default['airflow']['config']['celery']['celeryd_concurrency'] = 8
default['airflow']['config']['celery']['worker_log_server_port'] = 8793
default['airflow']['config']['celery']['broker_url'] = "redis://localhost:6379/15"
default['airflow']['config']['celery']['celery_result_backend'] = "redis://localhost:6379/15"

default['airflow']['config']['celery']['flower_port'] = 5555
default['airflow']['config']['celery']['default_queue'] = 'default'

# Required email airflow.cfg settings
default['airflow']['config']['email']['email_backend'] = 'airflow.utils.email.send_email_smtp'

# Required smtp airflow.cfg settings
default['airflow']['config']['smtp']['smtp_host'] = 'email-smtp.us-west-2.amazonaws.com'
default['airflow']['config']['smtp']['smtp_starttls'] = 'True'
default['airflow']['config']['smtp']['smtp_ssl'] = 'False'
default['airflow']['config']['smtp']['smtp_user'] = ''
default['airflow']['config']['smtp']['smtp_port'] = 25
default['airflow']['config']['smtp']['smtp_password'] = ''
default['airflow']['config']['smtp']['smtp_mail_from'] = 'workflow@surfline.com'

#airflow logs
default['sturdy']['platform']['logs']['airflow'].tap do |config|
  config['airflow-flower'] = '/home/airflow/airflow/service-log/airflow-flower.log'
  config['airflow-scheduler'] = '/home/airflow/airflow/service-log/airflow-scheduler.log'
  config['airflow-webserver'] = '/home/airflow/airflow/service-log/airflow-webserver.log'
  config['airflow-worker'] = '/home/airflow/airflow/service-log/airflow-worker.log'
end

#Sudoers
default['authorization']['sudo']['groups'] = ['sl.sudoers.all','sl.sudoers.science','airflow']
default['authorization']['sudo']['passwordless'] = true

# NFS mounts
default['surfline-nfs']['client4']['mounts'] = case node.chef_environment
                                               when 'prod'
                                                 {
                                                   'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean2_fs',
                                                   'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata2',
                                                   'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean_aws_dev'
                                                 }
                                               when 'dev'
                                                 {
                                                   'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs',
                                                   'dev-nfs-primary-1.aws.surfline.com:/sciencedata_aws_dev' => '/sciencedata2',
                                                   'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean_aws_prod',
                                                   'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata_aws_prod',
                                                 }
                                               else
                                                 {
                                                   'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs',
                                                   'dev-nfs-primary-1.aws.surfline.com:/sciencedata_aws_dev' => '/sciencedata2',
                                                 }
                                               end

# New Relic - Enable in all environments for Airflow
default['newrelic']['enabled'] = 'true'
default['newrelic']['log_level'] = 'info'
default['newrelic']['log_limit_in_kbytes'] = '100000'
default['newrelic']['license'] = case node.chef_environment
                                  when 'prod'
                                    'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                  else
                                    'd88d91400d512eba67a0faeb922435db811b5728'
                                  end

default['newrelic']['app_name'] = case node.chef_environment
                                  when 'prod'
                                    'Airflow'
                                  else
                                    'Airflow (dev)'
                                  end

default['newrelic-infra']['license_key'] = case node.chef_environment
                                           when 'prod'
                                             'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                           else
                                             'd88d91400d512eba67a0faeb922435db811b5728'
                                           end

default['newrelic_meetme_plugin']['license'] = case node.chef_environment
                                               when 'prod'
                                                 'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                               else
                                                 'd88d91400d512eba67a0faeb922435db811b5728'
                                               end

default['newrelic_meetme_plugin']['prefix'] = '/usr/local/bin'
