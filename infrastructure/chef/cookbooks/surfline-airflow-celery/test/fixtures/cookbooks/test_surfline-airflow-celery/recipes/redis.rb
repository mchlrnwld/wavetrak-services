#
# Cookbook Name:: test_surfline-airflow-celery
# Recipe:: redis
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# install redis (use the same version as AWS ElastiCache)
redisio_install "redis-installation" do
  version '2.8.24'
  download_url 'http://download.redis.io/releases/redis-2.8.24.tar.gz'
  safe_install false
  install_dir '/usr/local/'
end

# launch Redis as a daemon
# FIXME: tried to use redisio service to run redis, but not working
# Due to its local use for kitchen purpose, may not plan to spend more time
execute 'redis-server' do
  command '/usr/local/bin/redis-server --daemonize yes --dbfilename dump_jobs.rdb'
end
