#
# Cookbook Name:: test_surfline-airflow-celery
# Recipe:: mysql
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#


# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('airflow-celery', "#{node.chef_environment}", databag_secret)

# install mysqld
package 'mysql-server'

# register the service in Chef
service 'mysqld' do
  supports :restart => true, :start => true, :stop => true
  action :start
end

# install the MySQL gem since it isn't included in the 'database', '~> 6.0'
# cookbook by default: https://github.com/chef-cookbooks/database/issues/207
gem_package 'mysql2' do
  gem_binary RbConfig::CONFIG['bindir'] + '/gem'
  action :install
end

# externalize connection info in a Ruby hash
mysql_connection_info = {
  :host => secrets['mysql']['host'],
  :username => secrets['mysql']['user'],
  :password => secrets['mysql']['password']
}

# create the Airflow database
mysql_database 'airflowDB' do
  connection mysql_connection_info
  action :create
end

# initialize the Airflow database
execute 'initdb' do
  command '/usr/bin/airflow initdb'
  user node['airflow']['user']
  environment ({'AIRFLOW_HOME' => "#{node['airflow']['config']['core']['airflow_home']}"})
end
