name             'test_surfline-airflow-celery'  # ~FC064, ~FC065
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Testing support for surfline-airflow cookbook'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.0'

depends 'redisio', '~> 2.5.0'
depends 'database', '~> 6.0'
depends 'surfline-airflow-celery'
