# # encoding: utf-8

# Inspec test for recipe surfline-airflow::worker

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

describe upstart_service('airflow-worker', '/sbin/initctl') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
