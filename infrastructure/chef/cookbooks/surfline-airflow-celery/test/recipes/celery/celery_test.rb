# # encoding: utf-8

# Inspec test for recipe surfline-airflow::default

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

describe port(8793) do
  it { should be_listening }
end

describe port(5555) do
  it { should_not be_listening }
end

describe upstart_service('airflow-scheduler', '/sbin/initctl') do
  it { should_not be_installed }
  it { should_not be_enabled }
  it { should_not be_running }
end

describe upstart_service('airflow-webserver', '/sbin/initctl') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe upstart_service('airflow-worker', '/sbin/initctl') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe upstart_service('airflow-flower', '/sbin/initctl') do
  it { should_not be_installed }
  it { should_not be_enabled }
  it { should_not be_running }
end

# Verify that pypd is installed and can be imported into python
describe command('python -c \'import pypd\'') do
  its(:exit_status) {should eq (0)}
end
