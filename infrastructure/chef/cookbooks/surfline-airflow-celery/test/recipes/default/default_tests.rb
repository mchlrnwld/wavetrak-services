# # encoding: utf-8

# Inspec test for recipe surfline-airflow::default

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

describe user('hadoop') do
  it { should exist }
end

# Verify that docker-credential-ecr-login is installed
describe command('docker-credential-ecr-login') do
  its('stdout') { should include 'Usage: docker-credential-ecr-login' }
end

describe user('airflow') do
  it { should exist }
end

describe port(8080) do
  it { should be_listening }
end

# Verify correct version of docker python package is installed
describe command('/usr/bin/python -c \'import pkg_resources; print pkg_resources.get_distribution("docker").version\'') do
  its('stdout') { should include '3.5.0' }
end

# Verify correct version of kombu python package is installed
describe command('/usr/bin/python -c \'import pkg_resources; print pkg_resources.get_distribution("kombu").version\'') do
  its('stdout') { should include '4.6.3' }
end

# Verify correct version of pypd python package is installed
describe command('/usr/bin/python -c \'import pkg_resources; print pkg_resources.get_distribution("pypd").version\'') do
  its('stdout') { should include '1.1.0' }
end

# Verify npmrc file exists
describe file('/home/airflow/.npmrc') do
  its('size') { should > 0 }
end

# Verify that parameter store variables have been interpolated into npmrc file
describe file('/home/airflow/.npmrc') do
  its('content') { should match(%r{^\/\/surfline.jfrog.io/surfline/api/npm/npm-local/:_auth=[A-Za-z1-9]+}) }
end

describe file('/home/airflow/.npmrc') do
  its('content') { should match(%r{^\/\/surfline.jfrog.io/surfline/api/npm/npm-local/:username=[A-Za-z1-9]+}) }
end

# Verify that wavetrak-airflow-helpers package is installed
describe pip('wavetrak-airflow-helpers') do
  it { should be_installed }
end
