#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: logrotate
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'logrotate'

# airflow log rotation
logrotate_app 'airflow' do
  path [
        "#{node["airflow"]["config"]["core"]["airflow_home"]}/service-log/airflow-webserver.log",
        "#{node["airflow"]["config"]["core"]["airflow_home"]}/service-log/airflow-scheduler.log",
        "#{node["airflow"]["config"]["core"]["airflow_home"]}/service-log/airflow-flower.log",
        "#{node["airflow"]["config"]["core"]["airflow_home"]}/service-log/airflow-worker.log"
       ]
  dateformat '%Y%m%d-%s'
  frequency 'daily'
  options [
            'missingok',
            'copytruncate',
            'compress',
            'dateext',
            'delaycompress'
          ]
  rotate 14
end
