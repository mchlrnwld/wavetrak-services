#
# Cookbook Name:: surfline-airflow
# Recipe:: ecr-credential-helper
#
# Copyright 2019, Surfline/Wavetrack Inc
#
# All rights reserved - Do Not Redistribute
#

directory '/home/airflow/.docker' do
  owner 'airflow'
  group 'airflow'
  mode '0755'
  action :create
end

template '/home/airflow/.docker/config.json' do
  source 'ecr-credential-helper/ecr-credential-helper.erb'
  owner 'airflow'
  group 'airflow'
  mode '0400'
end

remote_file '/usr/local/bin/docker-credential-ecr-login' do
  source 'https://sl-artifacts-dev.s3-us-west-1.amazonaws.com/airflow/docker-credential-ecr-login.linux-amd64.b4a1707'
  owner 'airflow'
  group 'airflow'
  mode '0755'
  action :create
end
