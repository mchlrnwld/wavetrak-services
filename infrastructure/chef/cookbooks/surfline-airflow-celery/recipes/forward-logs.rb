#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: forward-logs
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

%w(airflow-flower airflow-scheduler airflow-webserver airflow-worker).each do |logs|
  template "/opt/sturdy/etc/logsforwarder/config/#{logs}" do
    cookbook 'sturdy_platform'
    source 'generic_logsforwarder.conf.erb'
    variables(
      monitored_file: node['sturdy']['platform']['logs']['airflow']["#{logs}"],
      log_group_name: "#{node.chef_environment}" + "-#{node['sturdy']['platform']['logs']['airflow']["#{logs}"]}",
      datetime_format: '%Y-%b-%d %H:%M:%S'
    )
    only_if { ::File.exist? node['sturdy']['platform']['logs']['airflow']["#{logs}"] }
    notifies :restart, 'service[sturdy-platform]', :delayed
  end
end
