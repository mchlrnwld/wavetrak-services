#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: artifactory
#
# Copyright 2021, Surfline/Wavetrack Inc
#
# All rights reserved - Do Not Redistribute
#

params = aws_parameter_store('us-west-1')["/#{node['aws_ssm']['base_key']}/*"]

template '/home/airflow/.npmrc' do
  source 'artifactory/npmrc.erb'
  owner 'airflow'
  group 'airflow'
  mode '0600'
  variables({
    :username => params["/#{node['aws_ssm']['base_key']}/npmrc/username"],
    :auth => params["/#{node['aws_ssm']['base_key']}/npmrc/auth"]
  })
end
