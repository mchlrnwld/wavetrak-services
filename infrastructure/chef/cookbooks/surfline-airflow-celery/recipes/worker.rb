#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: worker
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

params = aws_parameter_store('us-west-1')["/#{node['aws_ssm']['base_key']}/*"]

# install git
git_client 'default'

# setup deploy key
file '/home/airflow/.ssh/id_rsa' do
  content params["/#{node['aws_ssm']['base_key']}/git_deploykey"]
  mode 0600
  user 'airflow'
  group 'airflow'
  sensitive true
end

# add SSH known_hosts entry for github.com
ssh_known_hosts_entry 'github.com'

# set AWS_SCIENCE_BASE variable
magic_shell_environment 'AWS_SCIENCE_BASE' do
  value "#{node['surfline-airflow']['script_path']}/science-scripts"
end

# set AWS_SCIENCE_S3 variable
magic_shell_environment 'AWS_SCIENCE_S3' do
  value "s3://surfline-science-s3-#{node.chef_environment}"
end

# setup key to allow airflow worker instance SSH to emr master
template '/home/airflow/.ssh/hadoop-emr' do
  source    'sshkeys/key.erb'
  variables({
    :key => params["/#{node['aws_ssm']['base_key']}/ssh-keys_hadoop-emr"]
  })
  owner 'airflow'
  group 'airflow'
  mode      '0600'
  sensitive true
end

# setup key to allow airflow worker instance SSH to one another
template '/home/airflow/.ssh/celery_key' do
  source    'sshkeys/key.erb'
  variables({
    :key => params["/#{node['aws_ssm']['base_key']}/ssh-keys_celery_key"]
  })
  owner 'airflow'
  group 'airflow'
  mode      '0600'
  sensitive true
end

# prod NFS should be read-only when mounted outside of prod
mount '/ocean_aws_prod' do
  device 'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod'
  fstype 'nfs4'
  options 'ro'
  action [:remount]
  not_if { node.chef_environment == 'prod' }
end

mount '/sciencedata_aws_prod' do
  device 'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod'
  fstype 'nfs4'
  options 'ro'
  action [:remount]
  not_if { node.chef_environment == 'prod' }
end
