#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: scheduler
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# register the scheduler service in Chef
service 'airflow-scheduler' do
  provider Chef::Provider::Service::Upstart
  supports :restart => true, :start => true, :status => true, :stop => true
  action :nothing
  only_if {node.name.end_with?('-1')}
end

template '/etc/init/airflow-scheduler.conf' do
  source 'scheduler/airflow-scheduler-upstart.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
    :config => node['airflow']['config'],
    :log_path => node['airflow']['log_path']
  })
  notifies  :enable, 'service[airflow-scheduler]', :delayed
  notifies  :start, 'service[airflow-scheduler]', :delayed
  only_if {node.name.end_with?('-1')}
end

# register the airflow worker service in Chef
# scheduler service will invoke worker service to get task from queue
service 'airflow-worker' do
  provider Chef::Provider::Service::Upstart
  supports :restart => true, :start => true, :status => true, :stop => true
  action :nothing
  not_if {node.name.end_with?('-1')}
end

template '/etc/init/airflow-worker.conf' do
  source 'worker/airflow-worker-upstart.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
    :config => node['airflow']['config'],
    :log_path => node['airflow']['log_path']
  })
  notifies  :enable, 'service[airflow-worker]', :delayed
  notifies  :start, 'service[airflow-worker]', :delayed
  not_if {node.name.end_with?('-1')}
end

# register the airflow flower service in Chef
# flower service is related to worker service for queue/task status
service 'airflow-flower' do
  provider Chef::Provider::Service::Upstart
  supports :restart => true, :start => true, :status => true, :stop => true
  action :nothing
  only_if {node.name.end_with?('-1')}
end

template '/etc/init/airflow-flower.conf' do
  source 'flower/airflow-flower-upstart.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
    :config => node['airflow']['config'],
    :log_path => node['airflow']['log_path']
  })
  notifies  :enable, 'service[airflow-flower]', :delayed
  notifies  :start, 'service[airflow-flower]', :delayed
  only_if {node.name.end_with?('-1')}
end
