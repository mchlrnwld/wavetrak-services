#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: default
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#


params = aws_parameter_store('us-west-1')["/#{node['aws_ssm']['base_key']}/*"]

if ENV['TEST_KITCHEN']
  # load the encrypted data bag secret key
  files = Chef::DataBagItem.load('chef','files')
  secret_file = files['secret_file']

  # load secrets from encrypted data bag
  databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
  secrets = Chef::EncryptedDataBagItem.load('airflow-celery', "#{node.chef_environment}", databag_secret)
end

include_recipe 'surfline-users'
include_recipe 'surfline-users::science'
include_recipe 'airflow::directories'
include_recipe 'poise-python'

# Install csh, used by scripts in /ocean/code/science-scripts/scripts
package 'tcsh'

# install dependencies
[
  'gcc',
  'gcc-c++',
  'mysql',
  'mysql-devel',
  'python-devel'
].each do |p|
  package p
end

# Note: Certain airflow packages were removed from PyPi, so we now install Airflow via a tarball
include_recipe 'tar'

# install python packages

directory '/opt/python' do
  owner     'root'
  group     'root'
  recursive true
end

template '/opt/python/surfline-airflow-celery-requirements.txt' do
  source 'surfline-airflow-celery-requirements.txt'
  owner node['airflow']['user']
  group node['airflow']['group']
  mode node['airflow']['config_file_mode']
  notifies :install, 'pip_requirements[/opt/python/surfline-airflow-celery-requirements.txt]', :immediately
end

pip_requirements '/opt/python/surfline-airflow-celery-requirements.txt' do
  notifies :extract, "tar_extract[#{node['airflow']['installer']}]", :immediately
end

# Install Airflow
tar_extract node['airflow']['installer'] do
  target_dir '/opt/airflow-pip-package'
  notifies :run, 'python_execute[install airflow]', :immediately
end

python_execute 'install airflow' do
  action :nothing
  command '-m pip install -e .[mysql,celery]'
  cwd '/opt/airflow-pip-package/apache-airflow-209bf9c'
end

# install symlinks from /usr/bin/airflow <=> /usr/local/bin/airflow
link '/usr/local/bin/airflow' do
  to '/usr/bin/airflow'
  only_if { ::File.exist?('/usr/bin/airflow') }
  not_if { ::File.exist?('/usr/local/bin/airflow') }
end

link '/usr/bin/airflow' do
  to '/usr/local/bin/airflow'
  only_if { ::File.exist?('/usr/local/bin/airflow') }
  not_if { ::File.exist?('/usr/bin/airflow') }
end

# install Surfline Surflow
python_package 'surfline-workflow' do
  options "--extra-index-url https://#{params["/#{node['aws_ssm']['base_key']}/pip/username"]}:#{params["/#{node['aws_ssm']['base_key']}/pip/password"]}@#{node['surfline-airflow']['artifactory']['host']}#{node['surfline-airflow']['artifactory']['path']}"
    if node['surfline-airflow'] &&
       node['surfline-airflow']['surfline-workflow'] &&
       node['surfline-airflow']['surfline-workflow']['version']
      version node['surfline-airflow']['surfline-workflow']['version']
    end
  action :upgrade
end

# install Surfline Data Extraction
python_package 'surfline-data-extraction' do
  options "--extra-index-url https://#{params["/#{node['aws_ssm']['base_key']}/pip/username"]}:#{params["/#{node['aws_ssm']['base_key']}/pip/password"]}@#{node['surfline-airflow']['artifactory']['host']}#{node['surfline-airflow']['artifactory']['path']}"
    if node['surfline-airflow'] &&
       node['surfline-airflow']['surfline-data-extraction'] &&
       node['surfline-airflow']['surfline-data-extraction']['version']
      version node['surfline-airflow']['surfline-data-extraction']['version']
    end
  action :upgrade
end

# install Wavetrak Airflow Helpers
python_package 'wavetrak-airflow-helpers' do
  options "--extra-index-url https://#{params["/#{node['aws_ssm']['base_key']}/pip/username"]}:#{params["/#{node['aws_ssm']['base_key']}/pip/password"]}@#{node['surfline-airflow']['artifactory']['host']}#{node['surfline-airflow']['artifactory']['path']}"
  action :upgrade
end

# set the MySQL connection string from encrypted data bag
node.set['airflow']['config']['core']['sql_alchemy_conn'] = if ENV['TEST_KITCHEN']
                                                              "mysql://#{secrets['mysql']['user']}:#{secrets['mysql']['password']}@#{secrets['mysql']['host']}:3306/airflowDB"
                                                            else
                                                              "mysql://#{params["/#{node['aws_ssm']['base_key']}/mysql_user"]}:#{params["/#{node['aws_ssm']['base_key']}/mysql_password"]}@#{params["/#{node['aws_ssm']['base_key']}/mysql_host"]}:3306/airflowDB"
                                                            end

# set the SMTP user/password from encrypted data bag
node.set['airflow']['config']['smtp']['smtp_user'] = params["/#{node['aws_ssm']['base_key']}/smtp_user"]
node.set['airflow']['config']['smtp']['smtp_password'] =  params["/#{node['aws_ssm']['base_key']}/smtp_password"]

#Set broker info
node.set['airflow']['config']['celery']['broker_url'] = params["/#{node['aws_ssm']['base_key']}/broker_url"]
node.set['airflow']['config']['celery']['celery_result_backend'] = params["/#{node['aws_ssm']['base_key']}/result_backend"]

# configure Airflow
template "#{node['airflow']['config']['core']['airflow_home']}/airflow.cfg" do
  source 'airflow.cfg.erb'
  owner node['airflow']['user']
  group node['airflow']['group']
  mode node['airflow']['config_file_mode']
  variables({
  	:config => node['airflow']['config']
  })
  notifies  :restart, 'service[airflow-webserver]', :delayed
  notifies  :restart, 'service[airflow-scheduler]', :delayed
  sensitive true
end

# create the logging directory
directory "#{node["airflow"]["config"]["core"]["airflow_home"]}/service-log"

include_recipe 'surfline-airflow-celery::artifactory'
include_recipe 'surfline-airflow-celery::newrelic'
include_recipe 'surfline-airflow-celery::scheduler'
include_recipe 'surfline-airflow-celery::webserver'
include_recipe 'surfline-airflow-celery::sshkeys'
include_recipe 'surfline-airflow-celery::logrotate'
include_recipe 'surfline-airflow-celery::logsync'

# cleanup-airflow-task-logs cron job
cron_d 'cleanup-airflow-task-logs' do
  hour      '00'
  minute    '10'
  command   "/usr/bin/find #{node["airflow"]["config"]["core"]["airflow_home"]}/service-log -mtime +30 -name '*00' -delete"
end

include_recipe 'sturdy_platform::install'
include_recipe 'surfline-airflow-celery::forward-logs'
include_recipe 'surfline-airflow-celery::ecr-credential-helper'
