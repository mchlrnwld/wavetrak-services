#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: newrelic
#

# configure New Relic
template '/etc/newrelic.ini' do
  source    'newrelic/newrelic.ini.erb'
  owner     'root'
  group     'root'
  mode      '0755'
  variables({
    :enabled => node['newrelic']['enabled'],
    :log_level => node['newrelic']['log_level'],
    :license => node['newrelic']['license'],
    :app_name => node['newrelic']['app_name']
  })
end

python_package 'newrelic'

include_recipe 'newrelic' unless ENV['TEST_KITCHEN']
include_recipe 'newrelic-infra' unless ENV['TEST_KITCHEN']
