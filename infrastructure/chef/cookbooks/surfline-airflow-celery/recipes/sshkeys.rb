#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: sshkeys
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

params = aws_parameter_store('us-west-1')["/#{node['aws_ssm']['base_key']}/ssh-keys*"]

# setup SSH private keys
params.each do |param_key, key|
  user =  param_key[/ssh-keys_(.*)/,1]
  template node['airflow']['user_home_directory'] + '/.ssh/' + user do
    source    'sshkeys/key.erb'
    variables({
      :key => key
    })
    owner     node['airflow']['user']
    group     node['airflow']['group']
    mode      '0600'
    sensitive true
  end
end
