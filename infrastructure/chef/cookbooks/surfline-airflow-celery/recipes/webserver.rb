#
# Cookbook Name:: surfline-airflow-celery
# Recipe:: webserver
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# register the service in Chef
service 'airflow-webserver' do
  provider Chef::Provider::Service::Upstart
  supports :restart => true, :start => true, :status => true, :stop => true
  action :nothing
end

template '/etc/init/airflow-webserver.conf' do
  source 'webserver/airflow-webserver-upstart.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
    :config => node['airflow']['config'],
    :log_path => node['airflow']['log_path']
  })
  notifies  :enable, 'service[airflow-webserver]', :delayed
  notifies  :start, 'service[airflow-webserver]', :delayed
end

# setup key to allow webserver instance SSH to emr master
template "/home/#{node['airflow']['user']}/.ssh/hadoop-emr" do
  source    'sshkeys/key.erb'
  variables({
    :key => aws_parameter_store('us-west-1')["/#{node['aws_ssm']['base_key']}/ssh-keys_hadoop-emr"]
  })
  owner node['airflow']['user']
  group node['airflow']['group']
  mode      '0600'
  sensitive true
end

# Jenkins deploy key
# TODO: refactor this to use per-environment key, not tied to node['airflow']['user']
#cookbook_file "/home/#{node['airflow']['user']}/.ssh/authorized_keys" do
#  source 'surfline-science-dev.pub'
#  owner node['airflow']['user']
#  group node['airflow']['group']
#  mode 00644
#end
