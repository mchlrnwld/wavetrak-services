surfline-airflow-celery Cookbook
=========================

This cookbook installs and configures the Airflow workflow software

Requirements
------------

#### cookbooks
- `airflow` - surfline-airflow-celery needs the airflow cookbook to manage Airflow
- `cron` - surfline-airflow-celery needs the cron cookbook to manage cron jobs
- `logrotate` - surfline-airflow-celery needs logrotate for managing log files
- `python` - surfline-airflow-celery needs the python cookbook to manage Python and modules via pip
- `magic_shell` - surfline-airflow-celery needs to the magic_shell cookbook to manage environment variables on the host


Attributes
----------

#### surfline-airflow-celery::default

This cookbook overrides attributes provided by the `airflow` cookbook. At this time it doesn't have any attributes in the `surfline-airflow-celery` namespace.

Usage
-----
#### surfline-airflow-celery::default

Just include `surfline-airflow-celery` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-airflow-celery]"
  ]
}
```
