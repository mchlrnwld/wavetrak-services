surfline-airflow-celery CHANGELOG
==========================

=======

2.8.0
-----
- [Matt Walker] - Add boto3 to Airflow Python requirements

2.7.4
-----
- [Jason Wang] - Add airflow user to docker group
- [Jason Wang] - Export ENV variable

2.7.3
-----
- [Matt Walker] - Symlink /usr/bin/airflow <=> /usr/local/bin/airflow

2.7.2
-----
- [Matt Walker] - Bump aws-sdk version

2.7.1
-----
- [Matt Sollie] - changed to vendored local sturdy packages

2.7.0
-----
- [Matt Sollie] - Chef Zero Support
- [Matt Sollie] - Data bags moved to parameter store
- [Matt Sollie] - surfline-common::route53 forked locally due to KMS key access
- [Matt Sollie] - Changed airflow bin pathing to support packer base image

2.6.3
-----
- [Matt Sollie] - updated airflow bin pathing to support packer base image

2.6.2
-----
- [Matt Sollie] - Adding data-extraction package from Artifactory

2.6.1
-----
- [Jerry Warren] - Adding tcsh package install needed by some scripts

2.6.0
-----
- [Jon Price] - Swapped `airflow` data_bag with `airflow-celery`

2.5.0
-----
- [Brandon Pierce] - Add `dev` specific default attributes for NFS

2.4.0
-----
- [Andrew Slosarczyk] - Changed the cookbook logic to only run the airflow scheduler and flower on the first server

2.3.0
-----
- [Brandon Pierce] - add NFS sciencedata access

2.2.0
-----
- [Dominick Nguyen] - push airflow-celery-worker log to CloudWatch and rotate

2.1.1
-----
- [Dominick Nguyen] - add key pair for SSH access among celery workers

2.1.0
-----
- [Brandon Pierce] - initial release of `surfline-airflow-celery`

2.0.0
-----
- [Brandon Pierce] - update default attribute to `airflow` from `ec2-user`
- [Brandon Pierce] - replace hard-coded `ec2-user` references with `user` and `group` attributes
- [Brandon Pierce] - drop upstream `airflow::users` recipe since we pull those in via our own recipe
- [Brandon Pierce] - switch to using ocean for DAGs

1.7.0
-----
- [Jason Chen] - Remove `science-scripts` installation from Chef

1.6.7
-----
- [Jason Chen] - Add EMR/Hadoop key to `worker` recipe

1.6.6
-----
- [Brandon Pierce] - Add missing `git` and `ssh_known_hosts` dependencies

1.6.5
-----
- [Jason Chen] - Use renamed science repo and deploy location

1.6.4
-----
- [Brandon Pierce] - Add missing `surfline-users` dependency
- [Brandon Pierce] - Add missing `cython` dependency for the `pandas` Python library
- [Brandon Pierce] - Add missing Airflow logging directory
- [Brandon Pierce] - Properly restart services on configuration changes (fixes SDO-948)
- [Brandon Pierce] - Add Jenkins deploy key to webserver
- [Brandon Pierce] - Replace `python` with `poise-python` - for real this time (fixes SDO-1014)
- [Brandon Pierce] - Make various template resources as `sensitive true` to prevent secrets from leaking via Chef output

1.6.3
-----
- [Jason Chen] - Assign proper info for SMTP server in airflow config

1.6.2
-----
- [Brandon Pierce] - Add undocumented `timeout` parameter to `airflow` installation
- [Matt Walker] - Increase number of gunicorn workers for the webserver
- [Brandon Pierce] - Drop installation of `airflow` Python module to work around [poise-python #14][3c9f1145]

1.6.1
-----
- [Brandon Pierce] - Add missing domain to `base_url` configuration parameter (fixes SDO-935)

1.6.0
-----
- [Bryan Anderson] - Add installation of surfline emr keys under hadoop user for webserver instance

1.5.0
-----
- [Brandon Pierce] - Switch from `python` to `poise-python` Chef cookbook

1.4.0
-----
- [Bryan Anderson] - Set `AWS_SCIENCE_S3` environment variable in worker recipe

1.3.0
-----
- [Bryan Anderson] - Add `magic_shell` Cookbook from Supermarket
- [Bryan Anderson] - Set `AWS_SCIENCE_BASE` environment variable in worker recipe

1.2.0
-----
- [Brandon Pierce] - Add `surfline-workflow` Python package from Artifactory

1.1.0
-----
- [Brandon Pierce] - Add `sshkeys` recipe for automated installation of SSH private keys
- [Brandon Pierce] - Add `worker` recipe for configuration of worker instances

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-airflow-celery

[3c9f1145]: https://github.com/poise/poise-python/issues/14 "poise-python #14"
