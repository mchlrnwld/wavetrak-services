surfline-users Cookbook
=======================

Sets up users and various related permissions

Requirements
------------

#### cookbooks
- `sudo` - surfline-users needs sudo to manage sudoers
- `users` - surfline-users needs users to manage users


Usage
-----
#### surfline-users::default

Just include `surfline-users` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-users]"
  ]
}
```
