#
# Cookbook Name:: surfline-users
# Recipe:: science
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# setup sudo
include_recipe 'sudo'

# create the users in surfline science group
users_manage 'sl.science' do
  group_id 20010
end

# create the sudoers in surfline science group
users_manage 'sl.sudoers.science' do
  group_id 20011
end

# create system users
users_manage 'sl.science.system' do
  group_id 30000
end
