#
# Cookbook Name:: surfline-users
# Recipe:: cfdevs
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# setup sudo
include_recipe 'sudo'

# create the users in surfline science group
users_manage 'sl.cfdevs' do
  group_id 20014
end

# create the sudoers in surfline science group
users_manage 'sl.sudoers.cfdevs' do
  group_id 20024
end
