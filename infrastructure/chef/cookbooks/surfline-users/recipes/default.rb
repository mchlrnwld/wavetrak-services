#
# Cookbook Name:: surfline-users
# Recipe:: default
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# setup sudo
include_recipe 'sudo'

# create the users in surfline group
users_manage 'sl.all' do
  group_id 20000
end

# create the sudoers in surfline group
users_manage 'sl.sudoers.all' do
  group_id 20001
end
