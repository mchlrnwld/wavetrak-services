#
# Cookbook Name:: surfline-users
# Recipe:: jenkins
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# setup sudo
include_recipe 'sudo'

# create the users in surfline science group
users_manage 'sl.jenkins-admins' do
  group_id 20015
end

# create the sudoers in surfline science group
users_manage 'sl.sudoers.jenkins-admins' do
  group_id 20016
end
