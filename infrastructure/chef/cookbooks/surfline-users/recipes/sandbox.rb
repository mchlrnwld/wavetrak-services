#
# Cookbook Name:: surfline-users
# Recipe:: sandbox
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# setup sudo
include_recipe 'sudo'

# create the users in surfline science group
users_manage 'sl.sandbox' do
  group_id 20012
end

# create the sudoers in surfline science group
users_manage 'sl.sudoers.sandbox' do
  group_id 20013
end
