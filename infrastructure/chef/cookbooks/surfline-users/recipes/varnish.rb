#
# Cookbook Name:: surfline-users
# Recipe:: varnish
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# setup sudo
include_recipe 'sudo'

# create the users in surfline science group
users_manage 'sl.varnish' do
  group_id 20025
end

# create the sudoers in surfline science group
users_manage 'sl.sudoers.varnish' do
  group_id 20026
end
