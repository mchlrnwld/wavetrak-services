# sudoers
default['authorization']['sudo']['groups']                = [ "sl.sudoers.all" ]
default['authorization']['sudo']['passwordless']          = true
default['authorization']['sudo']['include_sudoers_d']     = true
