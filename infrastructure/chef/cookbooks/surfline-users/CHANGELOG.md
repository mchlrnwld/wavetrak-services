surfline-users CHANGELOG
========================

1.4.0
- [Dominick Nguyen] - Add varnish user groups in `varnish` recipe

1.3.1
-----
- [Dominick Nguyen] - Add sudo group in `cfdevs`

1.3.0
-----
- [Brandon Pierce] - Add `jenkins` recipe for Jenkins users

1.2.0
-----
- [Brandon Pierce] - Add `sandbox` recipe for sandbox users
- [Brandon Pierce] - Add `cfdevs` recipe for CF developers

1.1.0
-----
- [Brandon Pierce] - Add `science` recipe for members of the science squad

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-users
