name             'surfline-users'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-users'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.4.0'

depends 'sudo', '~> 2.9'
depends 'users', '~> 2.0'
