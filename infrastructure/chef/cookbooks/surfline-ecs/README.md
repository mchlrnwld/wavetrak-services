# surfline-ecs

## Description

This cookbook installs some baseline tools and monitoring for ECS hosts in AWS.

## Tools Included
* byobu - Light-weight, configurable window manager built upon GNU screen
* htop - Interactive process viewer
* screen - A screen manager that supports multiple logins on one terminal
* sysdig - a system-level exploration and troubleshooting tool


## Usage

Include `surfline-ecs::default` in the `run_list`. This is commonly found in
`roles` in Chef or in the `recipes` tag in EC2.


## Testing

Tests exist in `test/recipes/"${suite}"_test.rb`. There is currently only a
"default" suite though that could expand to application/service-specific suites
matching potential individual ECS cluster needs for admin, Jenkins, services,
etc.


### Pre-reqs

```shell
brew cask install chefdk
chef gem install kitchen-ec2 kitchen-sync
```


### Executing tests

```shell
kitchen test
```


### Sample results

```
User newrelic
   ✔  should exist
   ✔  groups should eq ["newrelic", "docker"]
Service newrelic-sysmond
   ✔  should be installed
   ✔  should be enabled
   ✔  should be running

Test Summary: 5 successful, 0 failures, 0 skipped
     Finished verifying <default-amazon-linux> (0m1.17s).
-----> Destroying <default-amazon-linux>...
     EC2 instance <i-0cb953180899a1d62> destroyed.
     Finished destroying <default-amazon-linux> (0m0.54s).
     Finished testing <default-amazon-linux> (2m26.65s).
```


Cookbook Deployment
-----

### Packaging

```shell
export BERKS_PACKAGE=$(basename $(berks package | awk ' { print $4 } '))
export BERKS_PACKAGE_VERSION=$(echo ${BERKS_PACKAGE} | grep -oE '[0-9]' | tr -d '\n')
gunzip -d ${BERKS_PACKAGE}
tar -C ../../ -uvf cookbooks-${BERKS_PACKAGE_VERSION}.tar data_bags/
gzip cookbooks-${BERKS_PACKAGE_VERSION}.tar
```


### Uploading

_Example for ecs in dev_

```shell
export CHEF_ENV="sbox"
export APPLICATION="ecs"
aws s3 cp cookbooks-${BERKS_PACKAGE_VERSION}.tar.gz s3://sl-chef-${CHEF_ENV}/${APPLICATION}/config/
rm cookbooks-*.tar.gz
````
