surfline-ecs CHANGELOG
========================

1.4.2
-----
- [Matt Walker] - remove route53 recipe

1.4.1
-----
- [Matt Sollie] - changed to vendored local sturdy packages

1.4.0
-----
- [Matt Walker] - Update Test Kitchen AMI, include meltdown vulnerability test

1.3.0
-----
- [Jon Price] - Added surfline-iam-ssh cookbook to runlist

1.2.1
-----
- [Jerry Warren] - Add sturdy_platform to surfline-ecs cookbook

1.2.0
-----
- [Jerry Warren] - Add cronjobs recipe that includes a cron to remove old
  images except those tagged as build-cache

1.1.1
-----
- [Matt Walker] - Change non-prod environments to use surfline-lite New Relic
  infrastructure key

1.1.0
-----
- [Sturdy Networks] - Initial revision
