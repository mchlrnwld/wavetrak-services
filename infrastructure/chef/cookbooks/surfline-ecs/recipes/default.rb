#
# Cookbook Name:: surfline-ecs
# Recipe:: default
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe 'surfline-ecs::dnsmasq'
include_recipe 'surfline-ecs::monitoring'
include_recipe 'surfline-ecs::tools'
include_recipe 'surfline-iam-ssh'
