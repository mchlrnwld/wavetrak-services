#
# Cookbook Name:: surfline-ecs
# Recipe:: jenkins-slaves
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe 'cron'

cron_d 'daily-remove-images-except-build-cache-images' do
  command           "docker rmi $(sudo docker images | sed '1d' | grep -v build-cache | awk '{print $3}')"
  predefined_value  '@daily'
end
