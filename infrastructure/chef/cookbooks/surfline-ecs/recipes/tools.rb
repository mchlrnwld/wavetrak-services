#
# Cookbook Name:: surfline-ecs
# Recipe:: tools
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe 'yum'
include_recipe 'yum-epel'

# Repo for Sysdig
# http://www.sysdig.org/install/
yum_repository 'draios' do
  description 'Draios'
  baseurl 'http://download.draios.com/stable/rpm/$basearch'
  gpgkey 'https://s3.amazonaws.com/download.draios.com/DRAIOS-GPG-KEY.public'
end

%w(bind-utils dkms htop kernel-devel sysdig tcpdump tmux vim).each do |p|
  package p
end
