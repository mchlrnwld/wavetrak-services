#
# Cookbook Name:: surfline-ecs
# Recipe:: newrelic
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe 'newrelic'
include_recipe 'newrelic-infra'

service 'newrelic-sysmond' do
  action :enable
end

# New Relic LSM needs `docker` group membership to report container stats
group 'docker' do
  action :modify
  members 'newrelic'
  append true
  notifies :restart, 'service[newrelic-sysmond]', :delayed
end

template '/etc/newrelic-infra/agent.yaml' do
  source    'newrelic-infra/agent.yaml.erb'
  owner     'root'
  group     'root'
  mode      '0644'
  variables({
    :license_key => node['newrelic_infra']['license_key']
  })
end
