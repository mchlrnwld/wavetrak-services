#
# Cookbook Name:: surfline-ecs
# Recipe:: dnsmasq
#
# Copyright (c) 2018 Surfline/Wavetrak, All Rights Reserved.

# Don't use dnsmasq cookbook, they don't install cleanly for dns caching
#
# This follows the dnsmasq caching config described here:
# https://aws.amazon.com/premiumsupport/knowledge-center/dns-resolution-failures-ec2-linux/

# Define services to allow Chef to interact with them
service 'dnsmasq' do
  supports status: true, start: true, restart: true
  action   :nothing
  notifies :restart, 'service[docker]', :delayed
end

service 'docker' do
  supports restart: true
  action   :nothing
end

execute 'dhclient' do
  action  :nothing
  command 'pkill -9 dhclient && dhclient'
end

# Set up dnsmasq users
user 'dnsmasq' do
  action   :create
  notifies :create, 'group[dnsmasq]', :immediately
end

group 'dnsmasq' do
  members  'dnsmasq'
  action   :create
  notifies :install, 'package[dnsmasq]', :immediately
end

# Install dnsmasq
package 'dnsmasq' do
  action   :install
  notifies :create, 'template[/etc/dnsmasq.conf]', :immediately
  notifies :enable, 'service[dnsmasq]', :immediately
end

# Create config files
template '/etc/resolv.dnsmasq' do
  source    'dnsmasq/resolv.dnsmasq.erb'
  owner     'root'
  group     'root'
  mode      0o0644
  variables ns: node['dnsmasq']['nameserver']
  notifies  :restart, 'service[dnsmasq]', :immediately
end

template '/etc/resolv.conf' do
  source   'dnsmasq/resolv.conf.erb'
  owner    'root'
  group    'root'
  mode     0o0644
  notifies :restart, 'service[dnsmasq]', :immediately
end

template '/etc/dnsmasq.conf' do
  source   'dnsmasq/dnsmasq.conf.erb'
  owner    'root'
  group    'root'
  mode     0o0644
  notifies :restart, 'service[dnsmasq]', :immediately
end

template '/etc/dhcp/dhclient.conf' do
  source    'dnsmasq/dhclient.conf.erb'
  owner     'root'
  group     'root'
  mode      0o0644
  variables ns: node['dnsmasq']['nameserver']
  notifies  :run, 'execute[dhclient]', :immediately
end

template '/etc/docker/daemon.json' do
  source    'docker/daemon.json.erb'
  owner     'root'
  group     'root'
  mode      0o0644
  notifies  :restart, 'service[docker]', :immediately
end
