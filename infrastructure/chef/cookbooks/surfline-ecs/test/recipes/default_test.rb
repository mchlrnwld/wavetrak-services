# Inspec test for recipe surfline-ecs::default

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

# The `newrelic` user must be a member of the `docker` group for container
# metrics to be published to New Relic
describe user('newrelic') do
  it { should exist }
  its('groups') { should eq ['newrelic', 'docker']}
end

describe service('newrelic-sysmond') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe upstart_service('newrelic-infra', '/sbin/initctl') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# Make sure kernel is patched for meltdown vulnerability
describe kernel_parameter('kernel.osrelease') do
  its('value') { should cmp >= '4.14' }
end

# Test dnsmasq
describe service('dnsmasq') do
  it { should be_enabled }
  it { should be_running }
end

describe port(53) do
  it { should be_listening }
end

describe file('/etc/resolv.dnsmasq') do
  its(:content) {
    should match(/^nameserver [0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/)
  }
end

describe command('host google.com') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/^google.com has address/) }
end

# Test ECS agent version
describe command('curl -s 127.0.0.1:51678/v1/metadata | python -mjson.tool | grep Agent | awk \'{print $6}\'') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should cmp >= 'v1.43.0' }
end
