name 'surfline-ecs'
maintainer 'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license 'all_rights'
description 'Installs/Configures surfline-ecs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.4.2'

depends 'cron', '~> 4.1.3'
depends 'newrelic', '~> 2.24'
depends 'newrelic-infra', '~> 0.11.0'
depends 'route53', '~> 0.4'
depends 'surfline-base', '~> 1.0.0'
depends 'surfline-common', '~> 1.10.0'
depends 'yum', '~> 4.1'
depends 'yum-epel', '~> 2.1'
depends 'surfline-iam-ssh', '~> 1.0.0'
# Note: Newer version of seven_zip are incompatible with chef 12.19.36 (which is our current version of chef as of 12/07/2018).
depends 'seven_zip', '~> 2.0.2'

supports 'amazon'

gem 'aws-sdk-ec2', '~> 1.84.0'
