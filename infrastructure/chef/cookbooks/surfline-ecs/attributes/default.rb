default['authorization']['sudo']['groups'] = ['sl.sudoers.all']

# default AWS VPC nameserver
default['dnsmasq']['nameserver'] = '169.254.169.253'
default['newrelic_infra']['license_key'] = 'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
default['newrelic']['license'] = default['newrelic_infra']['license_key']
