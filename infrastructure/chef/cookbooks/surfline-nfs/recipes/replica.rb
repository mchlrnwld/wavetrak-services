#
# Cookbook Name:: surfline-nfs
# Recipe:: replica
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-nfs::tuning'
include_recipe 'nfs::server4'
include_recipe 'surfline-nfs::science-mounts'
include_recipe 'surfline-nfs::utils'
