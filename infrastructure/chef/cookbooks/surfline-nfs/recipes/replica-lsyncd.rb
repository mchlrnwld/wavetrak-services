#
# Cookbook Name:: surfline-nfs
# Recipe:: replica-lsyncd
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'openssh'

# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load rsyncd secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('nfs', "#{node.chef_environment}", databag_secret)

# setup lsyncd key
ssh_authorize_key 'surfline-chef-lsyncd' do
  key secrets['lsyncd']['publickey']
  user 'root'
end
