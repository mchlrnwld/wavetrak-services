#
# Cookbook Name:: surfline-nfs
# Recipe:: primary-lsyncd
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'lsyncd'

# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load rsyncd secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('nfs', "#{node.chef_environment}", databag_secret)

# setup lsyncd key
file '/root/.ssh/id_rsa' do
  content secrets['lsyncd']['privatekey']
  mode 0600
end

# lookup members of the replica role in this environment and store ssh host public key
replica_servers = search(:node, "role:sl-nfs-replica AND chef_environment:#{node.chef_environment}").each do |server|
  ssh_known_hosts_entry server['hostname'] + '.aws.surfline.com'
end

# setup replication from local to replicas
replica_servers.each do |server|
  # setup ocean sync
  lsyncd_target 'ocean-' + server['hostname'] do
    host server['hostname'] + '.aws.surfline.com'
    source "/ocean_aws_#{node.chef_environment}"
    target "/ocean_aws_#{node.chef_environment}"
    notifies :restart, 'service[lsyncd]', :delayed
  end

  # setup sciencedata sync
  lsyncd_target 'sciencedata-' + server['hostname'] do
    host server['hostname'] + '.aws.surfline.com'
    source "/sciencedata_aws_#{node.chef_environment}"
    target "/sciencedata_aws_#{node.chef_environment}"
    notifies :restart, 'service[lsyncd]', :delayed
  end
end

# log rotation
logrotate_app 'lsyncd' do
  path          '/var/log/lsyncd.log'
  frequency     'daily'
  options       [ 'missingok', 'copytruncate' ]
  rotate        2
  size          '100m'
end

logrotate_app 'lsyncd-status' do
  path          '/var/log/lsyncd-status.log'
  frequency     'daily'
  options       [ 'missingok', 'copytruncate' ]
  rotate        2
  size          '100m'
end
