#
# Cookbook Name:: surfline-nfs
# Recipe:: client4
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'nfs::client4'

# setup client mounts
node['surfline-nfs']['client4']['mounts'].each do |name, path|
  # create mountpoint
  directory path do
    path              path
    mode              0777
    recursive         true
  end
  # add to fstab and mount
  mount path do
    device name
    fstype 'nfs4'
    options node['surfline-nfs']['client4']['options']
    action [:mount, :enable]
  end
end

# forklift patchwork of symlinks for various ocean drive variants
[
  '/ocean',
  '/ocean2',
  '/ocean2.earth2'
].each do |l|
  link l do
    to '/ocean2_fs'
  end
end
