#
# Cookbook Name:: surfline-nfs
# Recipe:: tuning
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'sysctl::default'

# OS receive buffer size - default
sysctl_param 'net.core.rmem_default' do
  value '12582912'
end

# OS receive buffer size - max
sysctl_param 'net.core.rmem_max' do
  value '12582912'
end

# OS send buffer size - default
sysctl_param 'net.core.wmem_default' do
  value '12582912'
end

# OS send buffer size - max
sysctl_param 'net.core.wmem_max' do
  value '12582912'
end
