#
# Cookbook Name:: surfline-nfs
# Recipe:: replica-client4
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'nfs::client4'

# lookup members of the replica role in this environment and AZ
replica_servers = search(:node, "role:sl-nfs-replica AND chef_environment:#{node.chef_environment}").each do |server|
  ssh_known_hosts_entry server['hostname'] + '.aws.surfline.com'
end



# setup client mounts
node['surfline-nfs']['client4']['mounts'].each do |name, path|
  # create mountpoint
  directory path do
    path              path
    mode              0777
    recursive         true
  end
  # add to fstab and mount
  mount path do
    device name
    fstype 'nfs4'
    options node['surfline-nfs']['client4']['options']
    action [:mount, :enable]
  end
end
