#
# Cookbook Name:: surfline-nfs
# Recipe:: science-mounts
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# ocean
directory "/ocean_aws_#{node.chef_environment}" do
  mode 0777
end

mount "/ocean_aws_#{node.chef_environment}" do
  device '/dev/xvdf'
  fstype 'ext4'
  action [:mount, :enable]
end

nfs_export "/ocean_aws_#{node.chef_environment}" do
  network '*'
  writeable true
  sync false
  options ['no_root_squash']
end

# science data
directory "/sciencedata_aws_#{node.chef_environment}" do
  mode 0777
end

mount "/sciencedata_aws_#{node.chef_environment}" do
  device '/dev/xvdg'
  fstype 'ext4'
  action [:mount, :enable]
end

nfs_export "/sciencedata_aws_#{node.chef_environment}" do
  network '*'
  writeable true
  sync false
  options ['no_root_squash']
end
