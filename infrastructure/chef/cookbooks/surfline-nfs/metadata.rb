name             'surfline-nfs'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-nfs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.3.1'

depends 'cron', '~> 1.6'
depends 'logrotate', '~> 1.9'
depends 'lsyncd', '~> 0.2'
depends 'nfs', '= 2.6.0'
depends 'openssh', '~> 2.0'
depends 'ssh_authorized_keys', '~> 0.4'
depends 'ssh_known_hosts', '~> 2.0'
depends 'sturdy-auto-snapshot-tool', '~> 2.0.0'
depends 'sysctl', '~> 1.0.5'

supports 'amazon'
