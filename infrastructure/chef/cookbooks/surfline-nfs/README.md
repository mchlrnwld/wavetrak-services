surfline-nfs Cookbook
=====================

Installs and manages NFS server and client

Requirements
------------

#### cookbooks
- `cron` - surfline-nfs needs the cron cookbook to manage cron jobs
- `logrotate` - surfline-coldfusion needs logrotate for managing log files
- `lsyncd` - surfline-nfs needs the lsyncd cookbook to install and manage replication from primaries to replicas
- `nfs` - surfline-nfs needs the nfs cookbook to install and manage NFS server and client
- `openssh` - surfline-nfs needs the openssh cookbook to manage SSHD settings
- `ssh_authorized_keys` - surfline-nfs needs the ssh_authorized_keys cookbook to manage SSH keys used by lsyncd
- `ssh_known_hosts` - surfline-nfs needs the ssh_known_hosts cookbook to maintain the ssh host keys of replicas
- `sturdy-auto-snapshot-tool` - surfline-nfs needs the sturdy-auto-snapshot-tool for managing automatic EBS snapshots
- `sysctl` - surfline-nfs needs the sysctl cookbook for tuning various kernel parameters


Usage
-----
#### surfline-nfs::default

Just include `surfline-nfs` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-nfs]"
  ]
}
```
