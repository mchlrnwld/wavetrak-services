surfline-nfs CHANGELOG
======================

1.3.1
-----
- [Matt Walker] Bump aws-sdk version

1.3.0
-----
- [Brandon Pierce] - Bump the version of `sturdy-auto-snapshot-tool` included to allow for more granularity options in backups

1.2.0
-----
- [Brandon Pierce] - Add `replica` recipe for supporting "read replica" NFS servers
- [Brandon Pierce] - Add `lsyncd-primary` and `lsyncd-replica` recipes for enabling replication from primaries to replicas
- [Brandon Pierce] - Add missing symlinks for ocean drive in `client4` recipe

1.1.0
-----
- [Brandon Pierce] - `client4` - load mounts from attributes

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-nfs
