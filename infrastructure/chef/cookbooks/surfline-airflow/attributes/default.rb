# Surfline Configuration
default['surfline-airflow']['version'] = '1.7.1.3'
default['surfline-airflow']['artifactory']['username']                = 'pypi-uploader'
default['surfline-airflow']['artifactory']['password']                = 'r7zYeHRc6GzaMr'
default['surfline-airflow']['artifactory']['host']                    = 'surfline.jfrog.io'
default['surfline-airflow']['artifactory']['path']                    = '/surfline/api/pypi/pypi/simple'
default['surfline-airflow']['script_path'] = '/opt/surfline'

# User configuration
default['airflow']['user'] = 'ec2-user'
default['airflow']['group'] = 'ec2-user'
default['airflow']['user_uid'] = 500
default['airflow']['group_gid'] = 500
default['airflow']['user_home_directory'] = "/home/#{node['airflow']['user']}"
default['airflow']['shell'] = '/bin/bash'

# General config
default['airflow']['directories_mode'] = '0775'
default['airflow']['config_file_mode'] = '0644'
default['airflow']['log_path'] = "/home/#{node['airflow']['user']}/airflow/service-log"

# airflow.cfg configurations. The required entries listed below, you can add more sections and configs.
# The structure default['airflow']['config']['CONFIG_SECTION']['CONFIG_ENTRY']

# Required core airflow.cfg settings
default['airflow']['config']['core']['airflow_home'] = "/home/#{node['airflow']['user']}/airflow"
default['airflow']['config']['core']['dags_folder'] = "#{node['airflow']['config']['core']['airflow_home']}/dags"
default['airflow']['config']['core']['plugins_folder'] = "#{node['airflow']['config']['core']['airflow_home']}/plugins"
default['airflow']['config']['core']['base_log_folder'] = "#{node['airflow']['config']['core']['airflow_home']}/logs"
default['airflow']['config']['core']['sql_alchemy_conn'] = "sqlite:///#{node['airflow']['config']['core']['airflow_home']}/airflow.db"
default['airflow']['config']['core']['sql_alchemy_pool_size'] = 5
default['airflow']['config']['core']['sql_alchemy_pool_recycle'] = 3600
default['airflow']['config']['core']['executor'] = 'LocalExecutor'
default['airflow']['config']['core']['parallelism'] = 32
default['airflow']['config']['core']['dag_concurrency'] = 16
default['airflow']['config']['core']['dags_are_paused_at_creation'] = 'False'
default['airflow']['config']['core']['max_active_runs_per_dag'] = 16
default['airflow']['config']['core']['load_examples'] = 'False'
default['airflow']['config']['core']['fernet_key'] = 'cryptography_not_found_storing_passwords_in_plain_text'
default['airflow']['config']['core']['donot_pickle'] = 'False'
default['airflow']['config']['core']['dagbag_import_timeout'] = 30

# Required webserver airflow.cfg settings
# FIXME: host IP address
default['airflow']['config']['webserver']['web_server_host'] = '0.0.0.0'
default['airflow']['config']['webserver']['web_server_port'] = 8080
default['airflow']['config']['webserver']['base_url'] = "http://#{node['fqdn']}.#{node['surfline-common']['route53']['domain_name']}:#{node['airflow']['config']['webserver']['web_server_port']}"
default['airflow']['config']['webserver']['secret_key'] = 'temporary_key'
default['airflow']['config']['webserver']['workers'] = 8
default['airflow']['config']['webserver']['worker_class'] = 'sync'
default['airflow']['config']['webserver']['expose_config'] = true
default['airflow']['config']['webserver']['authenticate'] = 'False'
default['airflow']['config']['webserver']['filter_by_owner'] = 'False'

# Required scheduler airflow.cfg settings
default['airflow']['config']['scheduler']['job_heartbeat_sec'] = 5
default['airflow']['config']['scheduler']['scheduler_heartbeat_sec'] = 5

# Required celery airflow.cfg settings
default['airflow']['config']['celery']['celery_app_name'] = 'airflow.executors.celery_executor'
default['airflow']['config']['celery']['celeryd_concurrency'] = 16
default['airflow']['config']['celery']['worker_log_server_port'] = 8793
default['airflow']['config']['celery']['broker_url'] = 'sqla+mysql://root:Zm1GmCmwsBYs2aUCAxLMh7WtsaOluwaR@sl-airflow-dev.cvpipfqer4dd.us-west-1.rds.amazonaws.com:3306/airflow'
default['airflow']['config']['celery']['celery_result_backend'] = 'db+mysql://root:Zm1GmCmwsBYs2aUCAxLMh7WtsaOluwaR@sl-airflow-dev.cvpipfqer4dd.us-west-1.rds.amazonaws.com:3306/airflow'
default['airflow']['config']['celery']['flower_port'] = 5555
default['airflow']['config']['celery']['default_queue'] = 'default'

# Required email airflow.cfg settings
default['airflow']['config']['email']['email_backend'] = 'airflow.utils.email.send_email_smtp'

# Required smtp airflow.cfg settings
default['airflow']['config']['smtp']['smtp_host'] = 'email-smtp.us-west-2.amazonaws.com'
default['airflow']['config']['smtp']['smtp_starttls'] = 'True'
default['airflow']['config']['smtp']['smtp_ssl'] = 'False'
default['airflow']['config']['smtp']['smtp_user'] = ''
default['airflow']['config']['smtp']['smtp_port'] = 25
default['airflow']['config']['smtp']['smtp_password'] = ''
default['airflow']['config']['smtp']['smtp_mail_from'] = 'workflow@surfline.com'

# NFS mounts
default['surfline-nfs']['client4']['mounts'] = case node.chef_environment
                                               when 'prod'
                                                 {
                                                   'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean2_fs',
                                                   'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata2',
                                                 }
                                               else
                                                 {
                                                   'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs',
                                                   'dev-nfs-primary-1.aws.surfline.com:/sciencedata_aws_dev' => '/sciencedata2',
                                                 }
                                               end
