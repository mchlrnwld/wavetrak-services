name             'surfline-airflow'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-airflow'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.8.0'

depends 'airflow', '~> 1.0'
depends 'cron', '~> 1.6'
depends 'git', '~> 4.4'
depends 'logrotate', '~> 1.9'
depends 'poise-python', '~> 1.6.0'
depends 'magic_shell', '~> 1.0'
depends 'ssh_known_hosts', '~> 2.0'
depends 'surfline-users', '~> 1.3'

supports 'amazon'
