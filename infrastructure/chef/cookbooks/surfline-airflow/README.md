surfline-airflow Cookbook
=========================

This cookbook installs and configures the Airflow workflow software

Requirements
------------

#### cookbooks
- `airflow` - surfline-airflow needs the airflow cookbook to manage Airflow
- `cron` - surfline-airflow needs the cron cookbook to manage cron jobs
- `logrotate` - surfline-airflow needs logrotate for managing log files
- `python` - surfline-airflow needs the python cookbook to manage Python and modules via pip
- `magic_shell` - surfline-airflow needs to the magic_shell cookbook to manage environment variables on the host


Attributes
----------

#### surfline-airflow::default

This cookbook overrides attributes provided by the `airflow` cookbook. At this time it doesn't have any attributes in the `surfline-airflow` namespace.

Usage
-----
#### surfline-airflow::default

Just include `surfline-airflow` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-airflow]"
  ]
}
```
