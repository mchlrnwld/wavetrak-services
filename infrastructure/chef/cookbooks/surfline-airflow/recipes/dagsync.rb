#
# Cookbook Name:: surfline-airflow
# Recipe:: dagsync
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# dag sync script
template '/usr/local/bin/airflow-dag-sync' do
  source 'dagsync/airflow-dag-sync.erb'
  owner 'root'
  group 'root'
  mode '0755'
end

# dag sync cron job
cron_d 'airflow-dag-sync' do
  minute  '*/10'
  command   '/usr/local/bin/airflow-dag-sync'
end
