#
# Cookbook Name:: surfline-airflow
# Recipe:: sshkeys
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('airflow', "#{node.chef_environment}", databag_secret)


# setup SSH private keys
secrets['ssh-keys'].each do |user, key|
  template node['airflow']['user_home_directory'] + '/.ssh/' + user do
    source    'sshkeys/key.erb'
    variables({
      :key => key
    })
    owner     node['airflow']['user']
    group     node['airflow']['group']
    mode      '0600'
    sensitive true
  end
end
