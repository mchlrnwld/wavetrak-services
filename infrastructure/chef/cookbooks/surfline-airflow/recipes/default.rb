#
# Cookbook Name:: surfline-airflow
# Recipe:: default
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#


# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('airflow', "#{node.chef_environment}", databag_secret)

include_recipe 'surfline-users'
include_recipe 'surfline-users::science'
include_recipe 'airflow::user'
include_recipe 'airflow::directories'
include_recipe 'poise-python'

# install dependencies
[
  'gcc',
  'gcc-c++',
  'mysql',
  'mysql-devel',
  'python-devel'
].each do |p|
  package p
end

python_package 'cython'

# install Airflow
[
  'airflow[mysql]'
].each do |p|
  python_package p do
    version node['surfline-airflow']['version']
    timeout 3600
  end
end

python_package 'surfline-workflow' do
  options "--extra-index-url https://#{node['surfline-airflow']['artifactory']['username']}:#{node['surfline-airflow']['artifactory']['password']}@#{node['surfline-airflow']['artifactory']['host']}#{node['surfline-airflow']['artifactory']['path']}"
    if node['surfline-airflow'] &&
       node['surfline-airflow']['surfline-workflow'] &&
       node['surfline-airflow']['surfline-workflow']['version']
      version node['surfline-airflow']['surfline-workflow']['version']
    end
  action :upgrade
end

# set the MySQL connection string from encrypted data bag
node.set['airflow']['config']['core']['sql_alchemy_conn'] = "mysql://#{secrets['mysql']['user']}:#{secrets['mysql']['password']}@#{secrets['mysql']['host']}:3306/airflow"

# set the SMTP user/password from encrypted data bag
node.set['airflow']['config']['smtp']['smtp_user'] = secrets['smtp']['user']
node.set['airflow']['config']['smtp']['smtp_password'] = secrets['smtp']['password']

# configure Airflow
template "#{node['airflow']['config']['core']['airflow_home']}/airflow.cfg" do
  source 'airflow.cfg.erb'
  owner node['airflow']['user']
  group node['airflow']['group']
  mode node['airflow']['config_file_mode']
  variables({
  	:config => node['airflow']['config']
  })
  notifies  :restart, 'service[airflow-webserver]', :delayed
  notifies  :restart, 'service[airflow-scheduler]', :delayed
  sensitive true
end

# create the logging directory
directory "#{node["airflow"]["config"]["core"]["airflow_home"]}/service-log"

include_recipe 'surfline-airflow::scheduler'
include_recipe 'surfline-airflow::webserver'
include_recipe 'surfline-airflow::sshkeys'
include_recipe 'surfline-airflow::logrotate'
include_recipe 'surfline-airflow::logsync'
include_recipe 'surfline-airflow::dagsync'

# cleanup-airflow-task-logs cron job
cron_d 'cleanup-airflow-task-logs' do
  hour      '00'
  minute    '10'
  command   "/usr/bin/find #{node["airflow"]["config"]["core"]["airflow_home"]}/service-log -mtime +7 -name '*00' -delete"
end
