#
# Cookbook Name:: surfline-airflow
# Recipe:: logsync
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# log sync script
template '/usr/local/bin/airflow-log-sync' do
  source 'logsync/airflow-log-sync.erb'
  owner 'root'
  group 'root'
  mode '0755'
end

# log sync cron job
cron_d 'airflow-log-sync' do
  minute  '0'
  hour    '1'
  command   '/usr/local/bin/airflow-log-sync'
end
