#
# Cookbook Name:: surfline-airflow
# Recipe:: scheduler
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# register the service in Chef
service 'airflow-scheduler' do
  provider Chef::Provider::Service::Upstart
  supports :restart => true, :start => true, :status => true, :stop => true
  action :nothing
end

template '/etc/init/airflow-scheduler.conf' do
  source 'scheduler/airflow-scheduler-upstart.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
    :config => node['airflow']['config'],
    :log_path => node['airflow']['log_path']
  })
  notifies  :enable, 'service[airflow-scheduler]', :delayed
  notifies  :start, 'service[airflow-scheduler]', :delayed
end
