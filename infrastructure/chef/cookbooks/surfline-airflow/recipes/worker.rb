#
# Cookbook Name:: surfline-airflow
# Recipe:: worker
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load rsyncd secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('airflow', "#{node.chef_environment}", databag_secret)

# install git
git_client 'default'

# setup deploy key
file '/home/airflow/.ssh/id_rsa' do
  content secrets['git']['deploykey']
  mode 0600
  user 'airflow'
  group 'airflow'
  sensitive true
end

# add SSH known_hosts entry for github.com
ssh_known_hosts_entry 'github.com'

# create the repo directory
directory "#{node['surfline-airflow']['script_path']}" do
  owner 'airflow'
  group 'airflow'
  mode 0755
  recursive true
end

# set AWS_SCIENCE_BASE variable
magic_shell_environment 'AWS_SCIENCE_BASE' do
  value "#{node['surfline-airflow']['script_path']}/science-scripts"
end

# set AWS_SCIENCE_S3 variable
magic_shell_environment 'AWS_SCIENCE_S3' do
  value "s3://surfline-science-s3-#{node.chef_environment}"
end

# setup key to allow airflow worker instance SSH to emr master
template '/home/airflow/.ssh/hadoop-emr' do
  source    'sshkeys/key.erb'
  variables({
    :key => secrets['ssh-keys']['hadoop-emr']
  })
  owner 'airflow'
  group 'airflow'
  mode      '0600'
  sensitive true
end
