#
# Cookbook Name:: surfline-airflow
# Recipe:: webserver
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef','files')
secret_file = files['secret_file']

# load rsyncd secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('airflow', "#{node.chef_environment}", databag_secret)

# register the service in Chef
service 'airflow-webserver' do
  provider Chef::Provider::Service::Upstart
  supports :restart => true, :start => true, :status => true, :stop => true
  action :nothing
end

template '/etc/init/airflow-webserver.conf' do
  source 'webserver/airflow-webserver-upstart.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
    :config => node['airflow']['config'],
    :log_path => node['airflow']['log_path']
  })
  notifies  :enable, 'service[airflow-webserver]', :delayed
  notifies  :start, 'service[airflow-webserver]', :delayed
end

# setup key to allow webserver instance SSH to emr master
template '/home/ec2-user/.ssh/hadoop-emr' do
  source    'sshkeys/key.erb'
  variables({
    :key => secrets['ssh-keys']['hadoop-emr']
  })
  owner node['airflow']['user']
  group node['airflow']['group']
  mode      '0600'
  sensitive true
end

# Jenkins deploy key
# TODO: refactor this to use per-environment key, not tied to ec2-user
cookbook_file "/home/#{node['airflow']['user']}/.ssh/authorized_keys" do
  source 'surfline-science-dev.pub'
  owner node['airflow']['user']
  group node['airflow']['group']
  mode 00644
end
