surfline-airflow CHANGELOG
==========================

1.8.0
-----
- [Brandon Pierce] - migrate NFS attributes to cookbook

1.7.0
-----
- [Jason Chen] - Remove `science-scripts` installation from Chef

1.6.7
-----
- [Jason Chen] - Add EMR/Hadoop key to `worker` recipe

1.6.6
-----
- [Brandon Pierce] - Add missing `git` and `ssh_known_hosts` dependencies

1.6.5
-----
- [Jason Chen] - Use renamed science repo and deploy location

1.6.4
-----
- [Brandon Pierce] - Add missing `surfline-users` dependency
- [Brandon Pierce] - Add missing `cython` dependency for the `pandas` Python library
- [Brandon Pierce] - Add missing Airflow logging directory
- [Brandon Pierce] - Properly restart services on configuration changes (fixes SDO-948)
- [Brandon Pierce] - Add Jenkins deploy key to webserver
- [Brandon Pierce] - Replace `python` with `poise-python` - for real this time (fixes SDO-1014)
- [Brandon Pierce] - Make various template resources as `sensitive true` to prevent secrets from leaking via Chef output

1.6.3
-----
- [Jason Chen] - Assign proper info for SMTP server in airflow config

1.6.2
-----
- [Brandon Pierce] - Add undocumented `timeout` parameter to `airflow` installation
- [Matt Walker] - Increase number of gunicorn workers for the webserver
- [Brandon Pierce] - Drop installation of `airflow` Python module to work around [poise-python #14][3c9f1145]

1.6.1
-----
- [Brandon Pierce] - Add missing domain to `base_url` configuration parameter (fixes SDO-935)

1.6.0
-----
- [Bryan Anderson] - Add installation of surfline emr keys under hadoop user for webserver instance

1.5.0
-----
- [Brandon Pierce] - Switch from `python` to `poise-python` Chef cookbook

1.4.0
-----
- [Bryan Anderson] - Set `AWS_SCIENCE_S3` environment variable in worker recipe

1.3.0
-----
- [Bryan Anderson] - Add `magic_shell` Cookbook from Supermarket
- [Bryan Anderson] - Set `AWS_SCIENCE_BASE` environment variable in worker recipe

1.2.0
-----
- [Brandon Pierce] - Add `surfline-workflow` Python package from Artifactory

1.1.0
-----
- [Brandon Pierce] - Add `sshkeys` recipe for automated installation of SSH private keys
- [Brandon Pierce] - Add `worker` recipe for configuration of worker instances

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-airflow

[3c9f1145]: https://github.com/poise/poise-python/issues/14 "poise-python #14"
