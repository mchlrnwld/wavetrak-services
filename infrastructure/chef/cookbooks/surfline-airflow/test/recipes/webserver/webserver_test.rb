# # encoding: utf-8

# Inspec test for recipe surfline-airflow::default

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html


describe user('hadoop') do
  it { should exist }
end

describe port(8080) do
  it { should be_listening }
end

describe upstart_service('airflow-scheduler', '/sbin/initctl') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe upstart_service('airflow-webserver', '/sbin/initctl') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
