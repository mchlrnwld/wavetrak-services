# # encoding: utf-8

# Inspec test for recipe surfline-airflow::worker

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html


describe user('airflow') do
  it { should exist }
end

describe user('hadoop') do
  it { should exist }
end
