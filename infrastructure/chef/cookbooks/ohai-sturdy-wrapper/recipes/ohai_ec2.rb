
ohai 'reload_ec2' do
  plugin 'ec2'
  action :nothing
end

template "#{node['ohai']['plugin_path']}/ec2.rb" do
  source 'plugins/ec2.rb.erb'
  owner  'root'
  group  node['root_group']
  mode   '0755'
  notifies :reload, 'ohai[reload_ec2]', :immediately
end

include_recipe 'ohai::default'
