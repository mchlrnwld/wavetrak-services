
ohai 'reload_publicip' do
  plugin 'publicip'
  action :nothing
end

template "#{node['ohai']['plugin_path']}/publicip.rb" do
  source 'plugins/publicip.rb.erb'
  owner  'root'
  group  node['root_group']
  mode   '0755'
  notifies :reload, 'ohai[reload_publicip]', :immediately
end

include_recipe 'ohai::default'
