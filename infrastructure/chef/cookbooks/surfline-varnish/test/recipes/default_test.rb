describe package('varnish') do
  it { should be_installed }
end

# extend tests with metadata
control 'Service' do
  impact 0.7
  title 'Verify varnish service'
  desc 'Ensures varnish service is up and running'
  describe service('varnish') do
    it { should be_enabled }
    it { should be_installed }

    # Temporarily remove this test. It's failing but the service is running.
    # If the service fails to run, the `curl` command below will fail.
    # it { should be_running }
  end
end

describe port(80) do
  it { should be_listening }
end

describe port(6082) do
  it { should be_listening }
end

describe command('curl -ksv http://localhost/varnishcheck 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end
