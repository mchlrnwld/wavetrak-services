surfline-varnish CHANGELOG
==========================

1.11.2
-----
- [Matt Walker] Bump aws-sdk version

1.11.1
-----
- [Matt Walker] - Change backend check to restart varnish rather than alert

1.10.0
-----
- [Jon Price] - Added route53 recipe runlist
- [Jerry Warren] - Fixed gem dependency issue

1.9.0
-----
- [Dan Heneise] - Updated Varnish repo to point to packagecloud.io

1.8.2
-----
- [Dan Heneise] - Added a library to pull servers by a filter; retrieves IP addresses for Varnish servers.

1.8.0
-----
- [Jon Price] - Added varnish_check_backends.py to varnish-cookbook

1.7.0
-----
- [Brandon Pierce] - Add ability to load GitHub deploy key from node attribute given certain conditions
- [Brandon Pierce] - Set GitHub deploy key file creation to sensitive
- [Brandon Pierce] - Add default attributes for backends to allow for cleaner automated testing
- [Brandon Pierce] - Switch Varnish service notifications to `reload`

1.6.0
-----
- [Brandon Pierce] - Add API internal dynamic backend generator

1.5.0
-----
- [Brandon Pierce] - Add Fishtrack dynamic backend generator
- [Brandon Pierce] - Add MarineAPI dynamic backend generator

1.4.1
-----
- [Brandon Pierce] - Include `surfline-common::hostfile` recipe for adding data center servers to /etc/hosts

1.4.0
-----
- [Brandon Pierce] - Create hostfile recipe for creating /etc/hosts entries for legacy servers mapped to AWS instances

1.3.1
-----
- [Brandon Pierce] - Create errorpage recipe to add missing error page and directory tree

1.3.0
-----
- [Brandon Pierce] - Enable EPEL
- [Brandon Pierce] - Add repo recipe for enabling the varnish-cache.org Yum repository

1.2.0
-----
- [Brandon Pierce] - Pull backend DNS names from attribute

1.1.0
-----
- [Brandon Pierce] - Add support for pulling VCL from GitHub
- [Brandon Pierce] - Add support for dynamically generated backends based on ELB hostname
- [Brandon Pierce] - Include surfline-common::resolver recipe to lookup Data Center hostnames used in VCL
- [Brandon Pierce] - Add support for loading Varnish secret from encrypted data bag

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-varnish
