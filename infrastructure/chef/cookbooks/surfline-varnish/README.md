surfline-varnish Cookbook
=========================

Deploys and manages Varnish

Requirements
------------

#### cookbooks
- `varnish` - surfline-varnish needs varnish to be useful
- `git` - surfline-varnish needs git to grab configuration from GitHub
- `hostsfile` - surfline-varnish needs the hostsfile cookbook for managing /etc/hosts entries
- `yum-epel` - surfline-varnish needs the EPEL repository to install recent versions of Varnish

#### packages
- `varnish` - surfline-varnish needs varnish to be useful

Attributes
----------

#### surfline-varnish::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['varnish']['listen_port']</tt></td>
    <td>Number</td>
    <td>Port to listen on</td>
    <td><tt>80</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-varnish']['backend']['api']</tt></td>
    <td>String</td>
    <td>DNS name of API backend</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-varnish']['backend']['api_int']</tt></td>
    <td>String</td>
    <td>DNS name of API backend</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-varnish']['backend']['www']</tt></td>
    <td>String</td>
    <td>DNS name of WWW backend</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-varnish']['threads']['min']</tt></td>
    <td>String</td>
    <td>Minimum number of threads to run</td>
    <td><tt>50</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-varnish']['threads']['max']</tt></td>
    <td>String</td>
    <td>Maximum number of threads to run</td>
    <td><tt>1000</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-varnish']['threads']['timeout']</tt></td>
    <td>String</td>
    <td>Timeout for threads</td>
    <td><tt>120</tt></td>
  </tr>
  <tr>
    <td><tt>['varnish']['parameters']['vcc_err_unref']</tt></td>
    <td>String</td>
    <td>Whether or not to error on unused backends</td>
    <td><tt>off</tt></td>
  </tr>
  <tr>
    <td><tt>['varnish']['storage_size']</tt></td>
    <td>String</td>
    <td>Size of cache</td>
    <td><tt>4G</tt></td>
  </tr>
  <tr>
    <td><tt>['varnish']['storage_file']</tt></td>
    <td>String</td>
    <td>Cache storage file</td>
    <td><tt>/var/lib/varnish/varnish_storage.bin</tt></td>
  </tr>
</table>

Usage
-----
#### surfline-varnish::default

Just include `surfline-varnish` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-varnish]"
  ]
}
```

test kitchen
-----
test kitchen requires the SSH key for github access.
To pass it in, you need to pass in the SSH key as an ENV variable:

```
VARNISH_DEPLOYKEY=$(cat <location of private key>) kitchen converge
```
