name             'surfline-varnish'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-varnish'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.11.2'

depends 'aws_parameter_store', '~> 2.1.1'
depends 'cron', '~> 1.6'
depends 'git', '~> 4.4'
depends 'hostsfile', '~> 2.4'
depends 'newrelic', '~> 2.17'
depends 'newrelic-infra'
depends 'seven_zip', '~> 2.0.2'
depends 'ssh_known_hosts', '~> 2.0'
depends 'surfline-common', '~> 1.10.4'
depends 'surfline-users', '~> 1.4'
depends 'varnish', '~> 2.3'
depends 'yum-epel', '~> 0.6.4'

gem 'aws-sdk-s3', '~> 1.30.0'

supports 'amazon'
