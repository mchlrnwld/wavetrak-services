#
# Cookbook Name:: surfline-varnish
# Recipe:: vcl
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

params = aws_parameter_store('us-west-1')["/#{environment}/varnish/*"]

# install git
git_client 'default'

# setup deploy key
file '/root/.ssh/id_rsa' do
  content params["/#{environment}/varnish/git_deploykey"]
  mode 0600
  sensitive true
end

# add SSH known_hosts entry for github.com
ssh_known_hosts_entry 'github.com'

# create the temp directory
directory "#{node['surfline-varnish']['temp_dir']}"

# grab the code from GitHub
git "#{node['surfline-varnish']['temp_dir']}" do
  action                :sync
  checkout_branch       "#{node['surfline-varnish']['git']['branch']}"
  enable_checkout       node['surfline-varnish']['git']['enable_checkout']
  repository            "#{node['surfline-varnish']['git']['repository']}"
  retries               5
  revision              "#{node['surfline-varnish']['git']['revision']}"
  user                  'root'
end

# put the VLC in place
file "/etc/varnish/default.vcl" do
  owner 'root'
  group 'root'
  mode 0644
  content lazy {::File.open("#{node['surfline-varnish']['temp_dir']}/varnish/etc/varnish/default.vcl").read}
  notifies  :reload, 'service[varnish]', :delayed
end

# put the dynamic backend generator script in place
file "/usr/local/bin/backend_generator.py" do
  owner 'root'
  group 'root'
  mode 0755
  content lazy {::File.open("#{node['surfline-varnish']['temp_dir']}/scripts/backend_generator.py").read}
  notifies  :reload, 'service[varnish]', :delayed
end

# backend_generator - api
execute 'backend_generator_api' do
  command   "/usr/local/bin/backend_generator.py \
    --hostname=#{params["/#{environment}/varnish/backend_api"]} \
    --verbose --dest=/etc/varnish/elb_api_backend.vcl \
    --reload-varnish \
    --director-name=api"
  sensitive true
end

cron_d 'backend_generator_api_cron' do
  minute    '*/1'
  mailto    '""'
  command   "/usr/local/bin/backend_generator.py --hostname=#{params["/#{environment}/varnish/backend_api"]} --verbose --dest=/etc/varnish/elb_api_backend.vcl --reload-varnish --director-name=api >/dev/null 2>&1"
end

# backend_generator - api_int
execute 'backend_generator_api_int' do
  command   "/usr/local/bin/backend_generator.py \
    --hostname=#{params["/#{environment}/varnish/backend_api_int"]} \
    --verbose --dest=/etc/varnish/elb_api_int_backend.vcl \
    --reload-varnish \
    --director-name=api_int"
  sensitive true
end

cron_d 'backend_generator_api_int_cron' do
  minute    '*/1'
  mailto    '""'
  command   "/usr/local/bin/backend_generator.py --hostname=#{params["/#{environment}/varnish/backend_api_int"]} --verbose --dest=/etc/varnish/elb_api_int_backend.vcl --reload-varnish --director-name=api_int >/dev/null 2>&1"
end

# backend_generator - www
execute 'backend_generator_www' do
  command   "/usr/local/bin/backend_generator.py \
    --hostname=#{params["/#{environment}/varnish/backend_www"]} \
    --verbose --dest=/etc/varnish/elb_app_backend.vcl \
    --reload-varnish \
    --director-name=www"
  sensitive true
end

cron_d 'backend_generator_www_cron' do
  minute    '*/1'
  mailto    '""'
  command   "/usr/local/bin/backend_generator.py --hostname=#{params["/#{environment}/varnish/backend_www"]} --reload-varnish --verbose --dest=/etc/varnish/elb_app_backend.vcl --director-name=www >/dev/null 2>&1"
end

# backend_generator - fishtrack
execute 'backend_generator_fishtrack' do
  command   "/usr/local/bin/backend_generator.py \
    --hostname=#{params["/#{environment}/varnish/backend_fishtrack"]} \
    --verbose --dest=/etc/varnish/elb_fishtrack_backend.vcl \
    --reload-varnish \
    --director-name=fishtrack"
  sensitive true
end

cron_d 'backend_generator_fishtrack_cron' do
  minute    '*/1'
  mailto    '""'
  command   "/usr/local/bin/backend_generator.py --hostname=#{params["/#{environment}/varnish/backend_fishtrack"]} --reload-varnish --verbose --dest=/etc/varnish/elb_fishtrack_backend.vcl --director-name=fishtrack >/dev/null 2>&1"
end

# backend_generator - marineapi
execute 'backend_generator_marineapi' do
  command   "/usr/local/bin/backend_generator.py \
    --hostname=#{params["/#{environment}/varnish/backend_marineapi"]} \
    --verbose --dest=/etc/varnish/elb_marineapi_backend.vcl \
    --reload-varnish \
    --director-name=marineapi"
  sensitive true
end

cron_d 'backend_generator_marineapi_cron' do
  minute    '*/1'
  mailto    '""'
  command   "/usr/local/bin/backend_generator.py --hostname=#{params["/#{environment}/varnish/backend_marineapi"]} --reload-varnish --verbose --dest=/etc/varnish/elb_marineapi_backend.vcl --director-name=marineapi >/dev/null 2>&1"
end
