#
# Cookbook Name:: surfline-varnish
# Recipe:: hostfile
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

Chef::Resource::User.send(:include, AWSSDK)
varnish_servers = AWSSDK.servers_by_filter('us-west-1', [{ name: 'tag:Name', values: ["#{environment}-varnish-cache-*"] }, { name: 'instance-state-name', values: ['running']}])

# NOTE: This is not the best way to detect the odd number of varnish nodes per
# environment. This sets the slcache01 & slcache03 host entries to both equal
# that of the ip found - this keeps both hosts resolvable for coldfusion

if varnish_servers.count == 1
  hostsfile_entry varnish_servers[0].private_ip_address do
    hostname    'slcache01'
    aliases     [
      'slcache01.surflinedc.tld',
      'slcache01.api.surfline.com',
      'slcache03',
      'slcache03.surflinedc.tld',
      'slcache03.api.surfline.com'
    ]
    comment     'Created by Chef'
  end
elsif varnish_servers.count >= 2
  hostsfile_entry varnish_servers[0].private_ip_address do
    hostname    'slcache01'
    aliases     ['slcache01.surflinedc.tld', 'slcache01.api.surfline.com']
    comment     'Created by Chef'
  end
  hostsfile_entry varnish_servers[1].private_ip_address do
    hostname    'slcache03'
    aliases     ['slcache03.surflinedc.tld', 'slcache03.api.surfline.com']
    comment     'Created by Chef'
  end
end
