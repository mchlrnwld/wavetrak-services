#
# Cookbook Name:: surfline-varnish
# Recipe:: default
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

# enable EPEL repo
include_recipe 'yum-epel'

# enable Varnish repo
include_recipe 'surfline-varnish::repo'

# install Varnish from the built-in repository
varnish_install 'default' do
  package_name 'varnish'
  vendor_repo false
end

# register the varnish service in Chef
service 'varnish' do
  supports :start => true,
    :stop => true,
    :status => true,
    :restart => true,
    :condrestart => true,
    :'try-restart' => true,
    :reload => true,
    :'force-reload' => true
  action :enable
end

# setup a config
template '/etc/sysconfig/varnish' do
  source 'varnish/default.erb'
  notifies  :restart, 'service[varnish]', :delayed
end

# secret
file '/etc/varnish/secret' do
  content aws_parameter_store('us-west-1')["/#{environment}/varnish/varnish_secret"]
  user 'root'
  group 'varnish'
  mode 0640
  sensitive true
end

# Data Center resolver support
include_recipe 'surfline-common::resolver'
include_recipe 'surfline-common::hostfile'

#Users
#include_recipe 'surfline-users'
include_recipe 'surfline-users::varnish'

# Added route53 recipe
include_recipe 'surfline-common::route53' unless ENV['TEST_KITCHEN']

# configure vcl
include_recipe 'surfline-varnish::vcl'

# error page
include_recipe 'surfline-varnish::errorpage'

# scripts
include_recipe 'surfline-varnish::scripts'

# varnish cache hosts
include_recipe 'surfline-varnish::hostfile'

include_recipe 'newrelic' unless ENV['TEST_KITCHEN']
include_recipe 'newrelic-infra' unless ENV['TEST_KITCHEN']

