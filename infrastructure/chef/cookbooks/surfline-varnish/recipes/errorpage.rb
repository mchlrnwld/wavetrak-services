# Cookbook Name:: surfline-varnish
# Recipe:: errorpage
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# create the error page directory tree
directory '/var/www/html/www.surfline.com' do
  action :create
  group 'root'
  mode '0755'
  owner 'root'
  recursive true
end

# setup the error page
template '/var/www/html/www.surfline.com/service_temporarily_unavailable.html' do
  source 'varnish/service_temporarily_unavailable.html.erb'
end
