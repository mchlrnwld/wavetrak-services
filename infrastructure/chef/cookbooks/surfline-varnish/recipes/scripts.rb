#
# Cookbook Name:: surfline-varnish
# Recipe:: scripts
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# load the encrypted data bag secret key

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

params = {}
params = aws_parameter_store('us-west-1')["/#{environment}/varnish/*"]

template '/usr/bin/varnish_backend_check.py' do
  source 'varnish/varnish_backend_check.py.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables ({
    pd_api_key: params["/#{environment}/varnish/pd_api_key"],
    pd_svc_api_key: params["/#{environment}/varnish/pd_svc_api_key"]
  })
end

%w[
  api
  api_int
  fishtrack
  marineapi
  www
].each do |k|
  cron_d "varnish_backend_check_py_#{k}" do
    minute    '*/5'
    mailto    '""'
    command   "/usr/bin/varnish_backend_check.py --restart-varnish --backendname #{k} --backenddns #{params["/#{environment}/varnish/backend_#{k}"]} >/dev/null 2>&1"
  end
end
