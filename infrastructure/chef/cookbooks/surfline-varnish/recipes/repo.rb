#
# Cookbook Name:: surfline-varnish
# Recipe:: repo
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

yum_repository 'varnish' do
  description "Varnish #{node['varnish']['version']} repo 6 - $basearch)"
  gpgcheck false
  gpgkey 'https://packagecloud.io/varnishcache/varnish30/gpgkey'
  url "https://packagecloud.io/varnishcache/varnish30/el/6/$basearch"
  priority '9'
  action :create
end
