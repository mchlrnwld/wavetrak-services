# Varnish
default['varnish']['listen_port']                                         = 80
default['varnish']['vcl_generated']                                              = false
default['varnish']['version']                                             = '3.0'

# Varnish threads
default['varnish']['threads']['min']                       = '50'
default['varnish']['threads']['max']                       = '1000'
default['varnish']['threads']['timeout']                   = '120'

# Varnish parameters
default['varnish']['parameters']['vcc_err_unref']                         = 'off'

# Varnish storage
default['varnish']['storage_size']                                        = '4G'
default['varnish']['storage_file']                                        = '/var/lib/varnish/varnish_storage.bin'

# Varnish - surfline
#default['surfline-varnish']['backend']['api']                             = 'sl-api-coldfusion-staging.aws.surfline.com'
#default['surfline-varnish']['backend']['api_int']                         = 'sl-api-coldfusion-staging.aws.surfline.com'
#default['surfline-varnish']['backend']['fishtrack']                       = 'sl-app-coldfusion-staging.aws.surfline.com'
#default['surfline-varnish']['backend']['marineapi']                       = 'sl-app-coldfusion-staging.aws.surfline.com'
#default['surfline-varnish']['backend']['www']                             = 'sl-app-coldfusion-staging.aws.surfline.com'
default['surfline-varnish']['temp_dir']                                   = '/root/surfline-varnish'
default['surfline-varnish']['git']['repository']                          = 'git@github.com:Surfline/surfline-varnish.git'
default['surfline-varnish']['git']['branch']                              = 'SDO-138-surfline-aws-accelerator'
default['surfline-varnish']['git']['revision']                              = 'SDO-138-surfline-aws-accelerator'
default['surfline-varnish']['git']['enable_checkout']                     = true
default['surfline-varnish']['git']['deploykey'] = 'bar'

# AWS / Route 53
default['surfline-common']['route53']['domain_name'] = 'aws.surfline.com'
default['surfline-common']['route53']['zone_id'] = 'ZWONYSN4O2R94'

# users / permissions
default['authorization']['sudo']['groups'] = ['sl.sudoers.all']

# New Relic
default['newrelic']['enabled'] = case node.chef_environment
when 'prod'
  'true'
else
  'false'
end

default['newrelic']['log_level'] = 'info'
default['newrelic']['log_limit_in_kbytes'] = '100000'
default['newrelic']['license'] = case node.chef_environment
                                    when 'prod'
                                      'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                    else
                                      'd88d91400d512eba67a0faeb922435db811b5728'
                                    end

default['newrelic-infra']['license_key'] = case node.chef_environment
                                              when 'prod'
                                                'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                              else
                                                'd88d91400d512eba67a0faeb922435db811b5728'
                                              end


default['newrelic_meetme_plugin']['license'] = case node.chef_environment
                                                  when 'prod'
                                                    'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                                  else
                                                    'd88d91400d512eba67a0faeb922435db811b5728'
                                                  end
default['newrelic_meetme_plugin']['prefix'] = '/usr/local/bin'

