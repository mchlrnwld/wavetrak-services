surfline-nagios CHANGELOG
=========================

1.0.1
-----
- [Brandon Pierce] - Actually install NRPE and enable it at boot... (derp)


1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-nagios
