#server config

package "nrpe" do
  action :install
end

package "nagios-plugins-all" do
  action :install
end

package "nagios-plugins-nrpe" do
  action :install
end

service "nrpe" do
  supports :status => true, :restart => true, :stop => true, :start => true, :reload => true
  action :enable
end

nagios_nodes = search(:node, "*:*")

template "/etc/nagios/nrpe.cfg" do
  source "nrpe-server.cfg.erb"
  owner "root"
  group "root"
  mode 0644
  variables ({
      :nagios_nodes => nagios_nodes
  })
  notifies :restart, "service[nrpe]", :delayed
end
