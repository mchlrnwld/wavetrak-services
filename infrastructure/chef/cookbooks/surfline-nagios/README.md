surfline-nagios Cookbook
========================

Basic monitoring cookbook

Requirements
------------

#### packages

### cookbooks

Usage
-----
#### surfline-nagios::default

Just include `surfline-nagios::server` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-nagios::server]"
  ]
}
```

License and Authors
-------------------
Authors: Brandon Pierce <brandon.pierce@sturdynetworks.com>
