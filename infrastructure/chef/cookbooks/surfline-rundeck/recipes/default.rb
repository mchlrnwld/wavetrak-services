include_recipe "rundeck::client"
  
template "/var/lib/rundeck/.ssh/authorized_keys" do
  source "client_keys/id_rsa.pub"
  owner "rundeck"
  group "rundeck"
  mode 0600
end
EOF
