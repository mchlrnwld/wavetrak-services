# s3_presigned_url

Chef recipe helper to get a download URL for S3 objects. Useful for use with Chef attributes/resources that only support HTTP downloads.

## Use

1. Upload file to s3 (e.g. mybucket/mypath/myfile.tar.gz)
1. Ensure instance has an IAM role with access to the file
1. Use the s3_presigned_url method in place of an http string; e.g.:
```
remote_file '/tmp/myfile.tar.gz' do
  source s3_presigned_url('mybucket', 'us-east-1')['mypath/myfile.tar.gz']
end
```

### Bucket Attributes

The bucket options can also be set via attributes, e.g.:
```
node.default['s3_presigned_url']['bucket'] = 'mybucket'
node.default['s3_presigned_url']['region'] = 'us-east-1'
remote_file '/tmp/myfile.tar.gz' do
  source s3_presigned_url['mypath/myfile.tar.gz']
end
```

### Expiry Time

The expiry time for the URL can also be set inline or via the `['s3_presigned_url']['expiry']` attribute, e.g.:
```
remote_file '/tmp/myfile.tar.gz' do
  source s3_presigned_url('mybucket', 'us-east-1', 1800)['mypath/myfile.tar.gz']
end

or

node.default['s3_presigned_url']['expiry'] = 1800
remote_file '/tmp/myfile.tar.gz' do
  source s3_presigned_url['mypath/myfile.tar.gz']
end
```

## Credits

Heavy inspiration drawn from the [Citadel cookbook](https://github.com/poise/citadel).
