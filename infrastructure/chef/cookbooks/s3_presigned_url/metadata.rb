name 's3_presigned_url'
maintainer 'Sturdy Networks'
maintainer_email 'devops@sturdy.cloud'
license 'all_rights'
description 'Provides the s3_presigned_url method'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://bitbucket.org/nbdev/s3_presigned_url-cookbook'
version '1.1.1'

# Should theoretically work on any platform supported by aws-sdk
%w(amazon rhel centos ubuntu windows).each do |plat|
  supports plat
end

chef_version '>= 12.8' # when gem support was added to metadata.rb

gem 'aws-sdk-s3', '~> 1.30.0'
