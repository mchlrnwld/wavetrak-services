# s3_presigned_url Cookbook CHANGELOG

This file is used to list changes made in each version of the s3_presigned_url cookbook.

## 1.1.1 (2018-09-26)

- Bump aws-sdk version

## 1.1.0 (2017-06-26)

- Update aws-sdk gem dependency from `~> 2.0` to `~> 2` to better match other community cookbooks. This should allow this cookbook to better coincide in run-lists with cookbooks like `aws`.
