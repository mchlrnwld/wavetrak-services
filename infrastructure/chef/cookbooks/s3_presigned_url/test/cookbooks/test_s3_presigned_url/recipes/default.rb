#
# Cookbook Name:: test_s3_presigned_url
# Recipe:: default
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'test_s3_presigned_url::_fix_tty' if node['platform'] == 'centos'
