name             'test_s3_presigned_url' # ~FC064, ~FC065
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdy.cloud'
license          'All rights reserved'
description      'Test cookbook for s3_presigned_url'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 's3_presigned_url'
