#
# Copyright 2013-2016, Balanced, Inc.
# Copyright 2016, Noah Kantrowitz
# Copyright 2017, Sturdy Networks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'aws-sdk-s3'

class S3PresignedUrl
  attr_reader :bucket, :region, :expiry

  def initialize(node, bucket = nil, region = nil, expiry = nil)
    @node = node
    @bucket = bucket || node['s3_presigned_url']['bucket']
    @region = region || node['s3_presigned_url']['region']
    @expiry = expiry || node['s3_presigned_url']['expiry']
  end

  def [](key)
    Chef::Log.debug("s3_presigned_url: Retrieving URL for #{@bucket}/#{key} via #{@region} endpoint, expiring in #{@expiry} seconds")
    obj = Aws::S3::Object.new(@bucket, key, region: @region)
    url = obj.presigned_url(:get, expires_in: @expiry)
    Chef::Log.debug("s3_presigned_url: generated url is #{url}")
    url
  end

  # Helper module for the DSL extension.
  #
  # @since 1.0.0
  # @api private
  module ChefDSL
    def s3_presigned_url(bucket = nil, region = nil, expiry = nil)
      S3PresignedUrl.new(node, bucket, region, expiry)
    end
  end
end

# Patch our DSL extension into Chef.
# @api private
class Chef
  class Recipe
    include S3PresignedUrl::ChefDSL
  end

  class Resource
    include S3PresignedUrl::ChefDSL
  end

  class Provider
    include S3PresignedUrl::ChefDSL
  end
end
