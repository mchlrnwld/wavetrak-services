name             'surfline-base'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-base'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.1'

depends 'base', '= 0.4.3'
