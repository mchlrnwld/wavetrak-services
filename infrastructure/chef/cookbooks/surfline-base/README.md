surfline-base Cookbook
========================
Base cookbook for Model Match

Usage
-----
#### surfline-base::default

Just include `surfline-base` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-base]"
  ]
}
```

License and Authors
-------------------
Authors: Brandon Pierce (<brandon.pierce@sturdynetworks.com>)
