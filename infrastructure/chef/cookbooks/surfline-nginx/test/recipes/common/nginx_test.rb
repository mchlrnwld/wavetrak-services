describe package('nginx') do
  it { should be_installed }
end

# extend tests with metadata
control 'Service' do
  impact 0.7
  title 'Verify nginx service'
  desc 'Ensures nginx service is up and running'
  describe service('nginx') do
    it { should be_enabled }
    it { should be_installed }
    it { should be_running }
  end
end

describe port(80) do
  it { should be_listening }
end

# Healthcheck should return 200 status code
describe command('curl -ksv http://localhost/health_nginx 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end

# Report page with cookie should redirect to MVP
describe command('curl -ksi http://localhost/surf-report/foo_5084/ -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://services.staging.surfline.com/kbyg/redirect/mvp/spot/5084\r\n" }
end

# Report page with cookie should redirect to MVP
describe command('curl -ksi http://localhost/surf-report-v/foo_5084/ -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://services.staging.surfline.com/kbyg/redirect/mvp/spot/5084\r\n" }
end

# Homepage with cookie should redirect to MVP
describe command('curl -ksi http://localhost/home/index.cfm -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.staging.surfline.com/\r\n" }
end

# Homepage with cookie should redirect to MVP
describe command('curl -ksi http://localhost/home/index.cfm -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.staging.surfline.com/\r\n" }
end

# Homepage with cookie should redirect to MVP
describe command('curl -ksi http://localhost/ -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.staging.surfline.com/\r\n" }
end

# Homepage without cookie should not redirect to MVP
describe command('curl -ksi http://localhost/ -H "Host:www.surfline.com" -H "Cookie:" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: http://www.surfline.com/home/index.cfm\r\n" }
end

# Surf news index with cookie should redirect to MVP
describe command('curl -ksi http://localhost/surf-news/ -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.staging.surfline.com/surf-news\r\n" }
end

# Page without corresponding MVP page should not redirect, even with cookie
describe command('curl -ksi "http://localhost/surfdata/video-rewind/video_rewind.cfm?id=4874" -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq '' }
end

# Page without corresponding MVP page but cookie and query string param should redirect to MVP homepage
describe command('curl -ksi "http://localhost/surfdata/video-rewind/video_rewind.cfm?id=4874&initial_mvp_opt_in=true" -H "Host:www.surfline.com" -H "Cookie:new_surfline=" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.staging.surfline.com/\r\n" }
end

# Page without corresponding MVP page but query string param should redirect to MVP homepage, regardless of cookie
describe command('curl -ksi "http://localhost/surfdata/video-rewind/video_rewind.cfm?id=4874&initial_mvp_opt_in=true" -H "Host:www.surfline.com" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.staging.surfline.com/\r\n" }
end

# BAN method should not be allowed
describe command('curl -ksi -X BAN -H "X-Ban-Url: /varnish/surf-news.*" "http://localhost/home/index.cfm" -H "Host:www.surfline.com" 2>&1 | grep "405"') do
  its('stdout') { should match /405/ }
end

# PURGE method should not be allowed
describe command('curl -ksi -X PURGE -H "X-Ban-Url: /varnish/surf-news.*" "http://localhost/home/index.cfm" -H "Host:www.surfline.com" 2>&1 | grep "405"') do
  its('stdout') { should match /405/ }
end
