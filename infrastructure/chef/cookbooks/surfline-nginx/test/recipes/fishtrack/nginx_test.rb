describe package('nginx') do
  it { should be_installed }
end

# extend tests with metadata
control 'Service' do
  impact 0.7
  title 'Verify nginx service'
  desc 'Ensures nginx service is up and running'
  describe service('nginx') do
    it { should be_enabled }
    it { should be_installed }
    it { should be_running }
  end
end

describe port(80) do
  it { should be_listening }
end

describe command('curl -ksv http://localhost/health_nginx 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end
