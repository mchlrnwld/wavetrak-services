describe package('nginx') do
  it { should be_installed }
end

# extend tests with metadata
control 'Service' do
  impact 0.7
  title 'Verify nginx service'
  desc 'Ensures nginx service is up and running'
  describe service('nginx') do
    it { should be_enabled }
    it { should be_installed }
    it { should be_running }
  end
end

describe port(80) do
  it { should be_listening }
end

# Healthcheck should return 200 status code
describe command('curl -ksv http://localhost/health_nginx 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end

# http pages should redirect to https when matching X-Forwarded-Proto
describe command('curl -ksI http://localhost/ -H "Host:new.surfline.com" -H "X-Forwarded-Proto:http" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.surfline.com/\r\n" }
end

# http pages should redirect to https with query params when matching X-Forwarded-Proto
describe command('curl -ksI http://localhost/surf-news -H "Host:new.surfline.com" -H "X-Forwarded-Proto:http" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.surfline.com/surf-news\r\n" }
end

# surf news requests should include query params
describe command('curl -ksI http://localhost/surf-news?native=true -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://new.surfline.com/surf-news/?native=true\r\n" }
end

# Pages not matching an MVP route should redirect back to legacy
describe command('curl -ksI "http://localhost/surfdata/video-rewind/video_rewind.cfm?id=4874" -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: http://staging.surfline.com/\r\n" }
end

# Pages that don't exist should return a 404 error page loaded from Cloudfront
describe command('curl -ksI http://localhost/surf-news/idontexist -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep "X-Cache: Hit from cloudfront"') do
  its('stdout') { should eq "X-Cache: Hit from cloudfront\r\n" }
end

# BAN method should not be allowed
describe command('curl -ksi -X BAN -H "X-Ban-Url: /varnish/surf-news.*" "http://localhost/home/index.cfm" -H "Host:www.surfline.com" 2>&1 | grep "405"') do
  its('stdout') { should match /405/ }
end

# PURGE method should not be allowed
describe command('curl -ksi -X PURGE -H "X-Ban-Url: /varnish/surf-news.*" "http://localhost/home/index.cfm" -H "Host:www.surfline.com" 2>&1 | grep "405"') do
  its('stdout') { should match /405/ }
end

# /favicon.ico should return 200 status code
describe command('curl -ksv http://localhost/favicon.ico -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end

# /undefined/favicon.png should return 200 status code
describe command('curl -ksv http://localhost/undefined/favicon.png -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end

# /readymag.js should return 200 status code
describe command('curl -ksv http://localhost/readymag.js -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end

# /manifest.json should return 200 status code
describe command('curl -ksv http://localhost/manifest.json -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep \'200 OK\'') do
  its('stdout') { should match /200 OK/ }
end

# /sitemaps/index.xml should return 200 status code
describe command('curl -ksv http://localhost/sitemaps/index.xml -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" 2>&1 | grep \'sitemapindex\'') do
  its('stdout') { should match /sitemapindex/ }
end

# WP-Content plugins should route through Editorial CDN
describe command('curl -ksi http://localhost/wp-content/plugins/bwp-minify/cache/minify-b1-jq-41feeaaa709c21e2050536e53b94337e.js -H "Host:d2pn5sjh0l9yc4.cloudfront.net" -H "Cookie:" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/plugins/bwp-minify/cache/minify-b1-jq-41feeaaa709c21e2050536e53b94337e.js\r\n" }
end

# /logout should return 302 status code
describe command('curl -ksv http://localhost/logout -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" -H "Cookie:access_token=123;refresh_token=321" 2>&1 | grep \'302\'') do
  its('stdout') { should match /302/ }
end

# /logout should redirect back to host location
describe command('curl -ksI http://localhost/logout -H "Host:new.surfline.com" -H "X-Forwarded-Proto:https" -H "Cookie:access_token=123;refresh_token=321" 2>&1 | grep "Location:"') do
  its('stdout') { should eq "Location: /\r\n" }
end
