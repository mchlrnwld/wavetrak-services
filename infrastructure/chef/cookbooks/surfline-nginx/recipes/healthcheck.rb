#
# Cookbook Name:: surfline-nginx
# Recipe:: healthcheck
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site '00healthcheck' do
  template  'sites/healthcheck.erb'
  enable    true
end
