#
# Cookbook Name:: surfline-nginx
# Recipe:: f5proxy
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'f5proxy' do
  template  'sites/f5proxy.erb'
  enable    true
end
