#
# Cookbook Name:: surfline-nginx
# Recipe:: micro-services-proxy
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

#base Nginx installation
include_recipe 'surfline-nginx'

nginx_site 'micro-services-proxy' do
  template  'sites/micro-services-proxy.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end
