#
# Cookbook Name:: surfline-nginx
# Recipe:: common-proxy
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-nginx'
include_recipe 'surfline-nginx::surfline-api'
include_recipe 'surfline-nginx::surfline-public'
include_recipe 'surfline-nginx::surfline-public-redesign'
include_recipe 'surfline-nginx::my-buoyweather'
include_recipe 'surfline-nginx::marineapi'
# This should me moved to the 'surfline-nginx::default' once all nginx stacks
# are converted to chef-zero
include_recipe 'sturdy_platform'
