#
# Cookbook Name:: surfline-nginx
# Recipe:: marineapi
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'marineapi' do
  template  'sites/marineapi.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end
