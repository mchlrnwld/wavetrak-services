#
# Cookbook Name:: surfline-ecs
# Recipe:: newrelic
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe 'newrelic'
include_recipe 'newrelic-infra'

service 'newrelic-sysmond' do
  action :enable
end
