#
# Cookbook Name:: surfline-nginx
# Recipe:: surfline-api
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'surfline-api' do
  template  'sites/surfline-api.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end
