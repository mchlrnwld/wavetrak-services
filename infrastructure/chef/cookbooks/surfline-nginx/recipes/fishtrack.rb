#
# Cookbook Name:: surfline-nginx
# Recipe:: fishtrack
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'fishtrack' do
  template  'sites/fishtrack.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end

# replace the default hello world page
template '/usr/share/nginx/html/index.html' do
  source 'content/fishtrack-redirect.html.erb'
  notifies :reload, 'service[nginx]', :delayed
end
