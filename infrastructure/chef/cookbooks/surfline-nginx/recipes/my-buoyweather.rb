#
# Cookbook Name:: surfline-nginx
# Recipe:: my-buoyweather
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'my-buoyweather' do
  template  'sites/my-buoyweather.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end
