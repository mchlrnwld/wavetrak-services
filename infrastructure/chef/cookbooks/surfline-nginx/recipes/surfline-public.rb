#
# Cookbook Name:: surfline-nginx
# Recipe:: surfline-public
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'surfline-public' do
  template  'sites/surfline-public.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end

# mvp redirect param include file
template "#{node['nginx']['dir']}/surfline-public-mvp-redirect-param.inc" do
  source 'sites/surfline-public-mvp-redirect-param.inc.erb'
  notifies :reload, 'service[nginx]', :delayed
end

# replace the default hello world page
template '/usr/share/nginx/html/index.html' do
  source 'content/www-redirect.html.erb'
  notifies :reload, 'service[nginx]', :delayed
end
