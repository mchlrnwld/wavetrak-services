#
# Cookbook Name:: surfline-nginx
# Recipe:: default
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# delete Amazon bundled Nginx config
file '/etc/nginx/nginx.conf.default' do
  action  :delete
end

# base Nginx installation
include_recipe 'nginx'

include_recipe 'surfline-nginx::monitoring'
include_recipe 'surfline-users' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-nginx::route53' unless ENV['TEST_KITCHEN']

# health check
include_recipe 'surfline-nginx::healthcheck'
# This will be enabled once all nginx-stacks are converted to chef-zero
#include_recipe 'sturdy_platform'
