#
# Cookbook Name:: surfline-nginx
# Recipe:: surfline-public
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'surfline-public-redesign' do
  template  'sites/surfline-public-redesign.erb'
  enable    true
  notifies :reload, 'service[nginx]', :delayed
end

# general redesign include file
template "#{node['nginx']['dir']}/surfline-public-redesign-all-envs.inc" do
  source 'sites/surfline-public-redesign-all-envs.inc.erb'
  notifies :reload, 'service[nginx]', :delayed
end

# replace the default hello world page
template '/usr/share/nginx/html/index.html' do
  source 'content/www-redirect.html.erb'
  notifies :reload, 'service[nginx]', :delayed
end

# add htpasswd file for basic auth
template "#{node['nginx']['dir']}/htpasswd" do
  source 'auth/htpasswd.erb'
  notifies :reload, 'service[nginx]', :delayed
end

# serve the local version of robots.txt specific for MVP
template '/usr/share/nginx/html/restrictive-robots.txt' do
  source 'content/restrictive-robots.txt.erb'
  notifies :reload, 'service[nginx]', :delayed
end
