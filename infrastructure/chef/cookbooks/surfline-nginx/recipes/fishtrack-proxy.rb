#
# Cookbook Name:: surfline-nginx
# Recipe:: fishtrack-proxy
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-nginx'
include_recipe 'surfline-nginx::fishtrack'
# This should me moved to the 'surfline-nginx::default' once all nginx stacks
# are converted to chef-zero
include_recipe 'sturdy_platform'
