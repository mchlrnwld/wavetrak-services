#
# Cookbook Name:: surfline-nginx
# Recipe:: f5proxy-3scale
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'f5proxy-3scale' do
  template  'sites/f5proxy-3scale.erb'
  enable    true
end
