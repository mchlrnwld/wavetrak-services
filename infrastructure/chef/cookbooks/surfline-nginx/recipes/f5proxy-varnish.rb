#
# Cookbook Name:: surfline-nginx
# Recipe:: f5proxy-varnish
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

nginx_site 'f5proxy-varnish' do
  template  'sites/f5proxy-varnish.erb'
  enable    true
end
