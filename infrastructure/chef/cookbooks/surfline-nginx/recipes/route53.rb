#
# Cookbook Name:: surfline-ecs
# Recipe:: route53
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

include_recipe 'route53'

route53_record node['hostname'] do
  name                      node['hostname'] + '.' + node['route53']['domain_name']
  value                     node['ipaddress']
  type                      'A'
  ttl                       60
  zone_id                   node['route53']['zone_id']
  aws_access_key_id         node['route53']['access_key']
  aws_secret_access_key     node['route53']['secret_key']
  overwrite                 true
  ignore_failure            true
  action                    :create
end
