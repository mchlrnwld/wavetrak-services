# Nginx
default['nginx']['conf_cookbook']                   = 'surfline-nginx'
default['nginx']['server_tokens']                   = 'off'
default['nginx']['default_site_enabled']            = false
default['nginx']['client_max_body_size']            = '5m'

# Nginx - http_realip_module
default['nginx']['realip']['header']                = 'X-Forwarded-For'

# Nginx - downstream host
default['surfline-nginx']['downstream'] = case node.chef_environment
                                          when 'dev'
                                            {
                                              'mvp' => 'new.sandbox.surfline.com',
                                              'www' => 'sandbox.surfline.com',
                                              'editorial-cdn' => 'd1542niisbwnmf.cloudfront.net'
                                            }
                                          when 'integration'
                                            {
                                              'mvp' => 'new.integration.surfline.com',
                                              'www' => 'sandbox.surfline.com',
                                              'editorial-cdn' => 'd3l8fvzh6g5k9s.cloudfront.net'
                                            }
                                          when 'prod'
                                            {
                                              'mvp' => 'new.surfline.com',
                                              'www' => 'www.surfline.com',
                                              'editorial-cdn' => 'd14fqx6aetz9ka.cloudfront.net'
                                            }
                                          when 'staging'
                                            {
                                              'mvp' => 'new.staging.surfline.com',
                                              'www' => 'staging.surfline.com',
                                              'editorial-cdn' => 'd2pn5sjh0l9yc4.cloudfront.net'
                                            }
                                          else
                                            {
                                              'mvp' => 'new.staging.surfline.com',
                                              'www' => 'staging.surfline.com',
                                              'editorial-cdn' => 'd2pn5sjh0l9yc4.cloudfront.net'
                                            }
                                          end

# Nginx - upstream endpoints
default['surfline-nginx']['upstream'] = case node.chef_environment
                                        when 'dev'
                                          {
                                            'fishtrack' => 'sl-fishtrack-coldfusion-prod.aws.surfline.com',
                                            'oauth2' => 'sl-oauth2-prod.aws.surfline.com',
                                            'varnish' => 'internal-sl-cache-varnish-prod-1584820028.us-west-1.elb.amazonaws.com',
                                            'editorial' => 'int-edit-cms.sandbox.surfline.com',
                                            'homepage' => 'internal-homepage.sandbox.surfline.com',
                                            'kbyg' => 'internal-kbyg.sandbox.surfline.com',
                                            'travel' => 'internal-travel.sandbox.surfline.com',
                                            'onboarding' => 'internal-onboarding.sandbox.surfline.com',
                                            'subscription' => 'sl-subs-svc-sbox.us-west-1.elasticbeanstalk.com',
                                            'subscription-redesign' => 'sl-subs-svc-sbox.us-west-1.elasticbeanstalk.com',
                                            'www' => 'sl-app-coldfusion-sandbox.aws.surfline.com',
                                            'micro-services-proxy' => {
                                              'auth_service' => 'auth-service.sandbox.surfline.com',
                                              'user_service' => 'user-service.sandbox.surfline.com',
                                              'password_reset_service' => 'password-reset-service.sandbox.surfline.com',
                                              'geo_target_service' => 'geo-target-service.sandbox.surfline.com',
                                              'entitlements_service' => 'sl-entitle-svc-sbox.us-west-1.elasticbeanstalk.com',
                                              'subscription_service_backend' => 'sl-subs-svc-bckd-sbox.us-west-1.elasticbeanstalk.com',
                                              'openresty' => 'services.sandbox.surfline.com'
                                            },
                                            'proto' => {
                                              'editorial' => 'https'
                                            }
                                          }
                                        when 'integration'
                                          {
                                            'fishtrack' => 'sl-app-coldfusion-dev.aws.surfline.com',
                                            'oauth2' => 'sl-oauth2-staging.aws.surfline.com',
                                            'varnish' => 'internal-sl-cache-varnish-staging-1207571358.us-west-1.elb.amazonaws.com',
                                            'editorial' => 'int-edit-cms.integration.surfline.com',
                                            'homepage' => 'internal-homepage.integration.surfline.com',
                                            'kbyg' => 'internal-kbyg.integration.surfline.com',
                                            'travel' => 'internal-travel.integration.surfline.com',
                                            'onboarding' => 'internal-onboarding.integration.surfline.com',
                                            'subscription' => 'sl-subs-svc-sbox.us-west-1.elasticbeanstalk.com',
                                            'subscription-redesign' => 'sl-subs-svc-sbox.us-west-1.elasticbeanstalk.com',
                                            'www' => 'sl-app-coldfusion-dev.aws.surfline.com',
                                            'micro-services-proxy' => {
                                              'auth_service' => 'auth-service.sandbox.surfline.com',
                                              'user_service' => 'user-service.sandbox.surfline.com',
                                              'password_reset_service' => 'password-reset-service.sandbox.surfline.com',
                                              'geo_target_service' => 'geo-target-service.sandbox.surfline.com',
                                              'entitlements_service' => 'sl-entitle-svc-sbox.us-west-1.elasticbeanstalk.com',
                                              'subscription_service_backend' => 'sl-subs-svc-bckd-sbox.us-west-1.elasticbeanstalk.com',
                                              'openresty' => 'services.sandbox.surfline.com'
                                            },
                                            'proto' => {
                                              'editorial' => 'https'
                                            }
                                          }
                                        when 'prod'
                                          {
                                            'fishtrack' => 'sl-fishtrack-coldfusion-prod.aws.surfline.com',
                                            'oauth2' => 'sl-oauth2-prod.aws.surfline.com',
                                            'varnish' => 'internal-sl-cache-varnish-prod-1584820028.us-west-1.elb.amazonaws.com',
                                            'editorial' => 'int-edit-cms.surfline.com',
                                            'homepage' => 'internal-homepage.surfline.com',
                                            'kbyg' => 'internal-kbyg.surfline.com',
                                            'travel' => 'internal-travel.surfline.com',
                                            'onboarding' => 'internal-onboarding.surfline.com',
                                            'subscription' => 'subscription-svc-prod.us-west-1.elasticbeanstalk.com',
                                            'subscription-redesign' => 'subscript-svc-new-prod.us-west-1.elasticbeanstalk.com',
                                            'www' => 'sl-app-coldfusion-prod.aws.surfline.com',
                                            'micro-services-proxy' => {
                                              'auth_service' => 'auth-service-prod.us-west-1.elasticbeanstalk.com',
                                              'user_service' => 'user-service-prod.us-west-1.elasticbeanstalk.com',
                                              'password_reset_service' => 'password-reset-svc-prod.us-west-1.elasticbeanstalk.com',
                                              'geo_target_service' => 'geo-target-svc-prod.us-west-1.elasticbeanstalk.com',
                                              'entitlements_service' => 'entitlements-svc-prod.us-west-1.elasticbeanstalk.com',
                                              'subscription_service_backend' => 'subscr-backend-svc-prod.us-west-1.elasticbeanstalk.com',
                                              'openresty' => 'services.surfline.com'
                                            },
                                            'proto' => {
                                              'editorial' => 'https'
                                            }
                                          }
                                        when 'staging'
                                          {
                                            'fishtrack' => 'sl-app-coldfusion-staging.aws.surfline.com',
                                            'oauth2' => 'sl-oauth2-staging.aws.surfline.com',
                                            'varnish' => 'internal-sl-cache-varnish-staging-1207571358.us-west-1.elb.amazonaws.com',
                                            'editorial' => 'int-edit-cms.staging.surfline.com',
                                            'homepage' => 'internal-homepage.staging.surfline.com',
                                            'kbyg' => 'internal-kbyg.staging.surfline.com',
                                            'travel' => 'internal-travel.staging.surfline.com',
                                            'onboarding' => 'internal-onboarding.staging.surfline.com',
                                            'subscription' => 'subscri-service-staging.us-west-1.elasticbeanstalk.com',
                                            'subscription-redesign' => 'subscri-service-staging.us-west-1.elasticbeanstalk.com',
                                            'www' => 'sl-app-coldfusion-staging.aws.surfline.com',
                                            'micro-services-proxy' => {
                                              'auth_service' => 'auth-service.staging.surfline.com',
                                              'user_service' => 'user-service.staging.surfline.com',
                                              'password_reset_service' => 'password-reset-service.staging.surfline.com',
                                              'geo_target_service' => 'geo-target-service.staging.surfline.com',
                                              'entitlements_service' => 'entitle-service-staging.us-west-1.elasticbeanstalk.com',
                                              'subscription_service_backend' => 'subsc-servi-backe-stagi.us-west-1.elasticbeanstalk.com',
                                              'openresty' => 'services.staging.surfline.com'
                                            },
                                            'proto' => {
                                              'editorial' => 'https'
                                            }
                                          }
                                        else
                                          {
                                            'fishtrack' => 'sl-app-coldfusion-staging.aws.surfline.com',
                                            'oauth2' => 'sl-oauth2-staging.aws.surfline.com',
                                            'varnish' => 'internal-sl-cache-varnish-staging-1207571358.us-west-1.elb.amazonaws.com',
                                            'editorial' => 'int-edit-cms.staging.surfline.com',
                                            'homepage' => 'internal-homepage.staging.surfline.com',
                                            'kbyg' => 'internal-kbyg.staging.surfline.com',
                                            'travel' => 'internal-travel.staging.surfline.com',
                                            'onboarding' => 'internal-onboarding.staging.surfline.com',
                                            'subscription' => 'subscri-service-staging.us-west-1.elasticbeanstalk.com',
                                            'subscription-redesign' => 'subscri-service-staging.us-west-1.elasticbeanstalk.com',
                                            'www' => 'sl-app-coldfusion-staging.aws.surfline.com',
                                            'micro-services-proxy' => {
                                              'auth_service' => 'auth-service.staging.surfline.com',
                                              'user_service' => 'user-service.staging.surfline.com',
                                              'password_reset_service' => 'password-reset-service.staging.surfline.com',
                                              'geo_target_service' => 'geo-target-service.staging.surfline.com',
                                              'entitlements_service' => 'entitle-service-staging.us-west-1.elasticbeanstalk.com',
                                              'subscription_service_backend' => 'subsc-servi-backe-stagi.us-west-1.elasticbeanstalk.com',
                                              'openresty' => 'services.staging.surfline.com'
                                            },
                                            'proto' => {
                                              'editorial' => 'https'
                                            }
                                          }
                                        end

# Nginx - CDNs
default['surfline-nginx']['cdn'] = case node.chef_environment
                                        when 'dev'
                                          {
                                            'product-services' => 'product-cdn.sandbox.surfline.com',
                                            'quiver-assets' => 'wa.cdn-surfline.com'
                                          }
                                        when 'integration'
                                          {
                                            'product-services' => 'product-cdn.integration.surfline.com',
                                            'quiver-assets' => 'wa.cdn-surfline.com'
                                          }
                                        when 'prod'
                                          {
                                            'product-services' => 'wa.cdn-surfline.com',
                                            'quiver-assets' => 'wa.cdn-surfline.com'
                                          }
                                        when 'staging'
                                          {
                                            'product-services' => 'product-cdn.staging.surfline.com',
                                            'quiver-assets' => 'wa.cdn-surfline.com'
                                          }
                                        else
                                          {
                                            'product-services' => 'product-cdn.staging.surfline.com',
                                            'quiver-assets' => 'wa.cdn-surfline.com'
                                          }
                                        end

# Sitemaps
default['surfline-nginx']['sitemaps'] = case node.chef_environment
                                        when 'dev'
                                          'sl-sitemaps-sandbox.s3-us-west-1.amazonaws.com'
                                        when 'staging'
                                          'sl-sitemaps-staging.s3-us-west-1.amazonaws.com'
                                        when 'prod'
                                          'sl-sitemaps-prod.s3-us-west-1.amazonaws.com'
                                        else
                                          'sl-sitemaps-staging.s3-us-west-1.amazonaws.com'
                                        end

# users / permissions
default['authorization']['sudo']['groups'] = ['sl.sudoers.all']

# New Relic
default['newrelic']['license'] = 'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
default['newrelic-infra'] = case node.chef_environment
                            when 'prod'
                              {
                                'license_key' => 'a26ab469b2d2b56766af51d1c8c436e736ba72bf',
                              }
                            else
                              {
                                'license_key' => 'd88d91400d512eba67a0faeb922435db811b5728',
                              }
                            end

# AWS / Route 53
default['route53']['domain_name'] = 'aws.surfline.com'
default['route53']['zone_id'] = 'ZWONYSN4O2R94'
default['route53']['access_key'] = 'AKIAIMDNWOCNTFF4OACA'
default['route53']['secret_key'] = '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'
