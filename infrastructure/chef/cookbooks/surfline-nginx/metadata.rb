name             'surfline-nginx'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-nginx'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.2.10'

depends 'newrelic', '~> 2.24.0'
depends 'newrelic_meetme_plugin', '~> 0.5.0'
depends 'newrelic-infra', '~> 0.0.1'
depends 'nginx', '~> 2.7.6'
depends 'route53', '~> 0.4'
depends 'surfline-users', '~> 1.4.0'
depends 'sturdy_platform', '~> 2.0'

supports 'amazon'
