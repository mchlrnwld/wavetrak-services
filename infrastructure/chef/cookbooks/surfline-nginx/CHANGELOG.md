surfline-nginx CHANGELOG
========================

2.2.10
-----
- [Matt Sollie] - changed to vendored local sturdy packages

2.2.9
------
- [Jon Price] - Add new upstreams for auth, user, password-reset, geo-target

2.2.8
------
- [Patrick Ranspach] - Add cloudfront origin to editorial for CDN routing

2.2.7
-----
- [Matt Walker] - Update nginx cookbook to use minimart.sturdynetworks.com

2.2.6
-----
- [Matt Walker] - Change non-prod environments to use surfline-lite New Relic
infrastructure key

2.2.5
-----
- [Jon Price] - Add sturdy_platform to nginx cookbooks

2.2.4
-----
- [Michael Malchak] - Add logout path to redesign proxy.

2.2.1 - 2.2.3
-----
- [Michael Malchak] - Static versions inadvertently bumped during development.

2.2.0
------
- [Matt Walker] - Remove 3scale from `surfline-api` routing

2.1.4
------
- [Patrick Ranspach] - Expand scope query param `initial_mvp_opt_in` to redirect for SEO testing

2.1.3
------
- [Jason Wang] - Add sitemaps path to redesign proxy

2.1.2
------
- [Patrick Ranspach] - Pass query params to editorial app
- [Patrick Ranspach] - Proxy manifest.json requests to quiver cdn

2.1.1
------
- [Jon Price] - Changed order of operation - Moved health_check recipes to end of run

2.1.0
------
- [Brandon Pierce] - Add new recipes to further deprecate roles

2.0.0
------
- [Brandon Pierce] - Move per-environment attributes into cookbook
- [Brandon Pierce] - Add additional cookbooks from deprecated roles

1.14.3
------
- [Scott Diorio] - Added nginx route for travel/beaches to cookbook

1.14.2
------
- [Scott Diorio] - Added nginx route for surf-reports-forecasts-cams-map to cookbook

1.14.1
------
- [Matt Walker] - Add new.integration.surfline.com to redesign proxy

1.14.0
------
- [Drew Newberry] - Add surf-charts path to redesign proxy

1.13.2
-----
- [Matt Walker] - Add MVP `initial_mvp_opt_in` query string redirect

1.13.1
-----
- [Matt Walker] - Change WP login route to pass through admin

1.13.0
-----
- [Matt Walker] - Change MVP editorial conf to route through Varnish

1.12.0
-----
- [Matt Walker] - Abstract Surfline redesign conf into include file to enable env-specific conf (like basic auth)
- [Matt Walker] - Remove basic auth from new.surfline.com
- [Matt Walker] - Add MVP/OpenResty hosts to nginx vars
- [Matt Walker] - Add MVP redirects and tests
- [Matt Walker] - Add MVP HTTP->HTTPS redirect and tests

1.11.9
-----
- [Ajith Manmadhan] - Add restrictive robots.txt to support new.surfline.com

1.11.8
-----
- [Mike Shea] - Add subscription-redesign upstream to support new.surfline.com subs frontend

1.11.7
-----
- [Drew Newberry] - Add kbyg to regex for KBYG routes

1.11.6
-----
- [Jason Wang] - Remove non-SEO-friendly KBYG routes
- [Mike Shea] - Include subdirs of `/account` in redesign proxy subscription routes

1.11.5
-----
- [Spencer Miller] - Add `favorites` path to Surfline redesign KBYG proxy
- [Matt Walker] - Set default `client_max_body_size` to `5m` to support WordPress uploads

1.11.4
-----
- [Matt Walker] - Use `https` protocol for editorial upstream

1.11.2
-----
- [Matt Walker] - Simplify WP host header in Surfline redesign

1.11.1
-----
- [Matt Walker] - Update Surfline redesign proxy routes, update upstreams

1.11.0
-----
- [Matt Walker] - Add `new(\.staging|\.sandbox)?.surfline.com` proxies for Surfline redesign

1.10.1
-----
- [Mike Shea] - redirect all my.fishtrack type traffic to the new funnel on www.fishtrack

1.10.0
-----
- [Dominick Nguyen] - Add micro-services-proxy recipe, template, and attributes
- [Brandon Pierce] - Add missing apex host name for `fishtrack.com`

1.9.1
-----
- [Matt Walker] - Route process-recents.cfm to Varnish

1.9.0
-----
- [Brandon Pierce] - Add staging variants of various FishTrack domains

1.8.0
-----
- [Matt Walker] - Add brasil domain variants to `surfline-public.erb` template

1.7.0
-----
- [Brandon Pierce] - Add staging variants of various Surfline and Buoyweather domains

1.6.0
-----
- [Brandon Pierce] - Switch oauth2 upstream to use attribute in `surfline-api.erb` template

1.5.2
-----
- [Brandon Pierce] - Add missing Nixon name from `surfline-api.erb` template

1.5.1
-----
- [Brandon Pierce] - Fix `/myaccount` path issues for secure site in `fishtrack`

1.5.0
-----
- [Brandon Pierce] - Add `fishtrack` recipe to service `my.fishtrack.com` and `www.fishtrack.com`
- [Brandon Pierce] - Add `marineapi` recipe to service `api.buoyweather.com`

1.4.0
-----
- [Brandon Pierce] - Add `my-buoyweather` recipe to service `my.buoyweather.com`
- [Brandon Pierce] - Add index.html redirect page to `surfline-public` recipe

1.3.0
-----
- [Brandon Pierce] - Finalize surfline.com public and API site rules

1.2.0
-----
- [Brandon Pierce] - Add f5proxy-3scale recipe to handle proxying request from F5 to 3scale

1.1.0
-----
- [Brandon Pierce] - Add f5proxy-varnish recipe to handle proxying request from F5 to Varnish

1.0.1
-----
- [Matt Walker] - Add all missing F5 handled domains in f5proxy recipe
- [Matt Walker] - Fix http->https redirect loop in f5proxy recipe


1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-nginx
