surfline-nginx Cookbook
=======================

This cookbook installs and configures Nginx as a proxy for various Surfline internals

Requirements
------------

#### cookbooks
- `nginx` - surfline-nginx needs nginx to install and configure Nginx

Attributes
----------

#### surfline-nginx::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['surfline-nginx']['upstream']['varnish']</tt></td>
    <td>String</td>
    <td>DNS name of Varnish upstream</td>
    <td><tt></tt></td>
  </tr>
</table>

Usage
-----
#### surfline-nginx::default

Just include `surfline-nginx` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-nginx]"
  ]
}
```

Cookbook Deployment
-----

### Packaging

```shell
export BERKS_PACKAGE=$(basename $(berks package | awk ' { print $4 } '))
export BERKS_PACKAGE_VERSION=$(echo ${BERKS_PACKAGE} | grep -oE '[0-9]' | tr -d '\n')
gunzip -d ${BERKS_PACKAGE}
tar -C ../../ -uvf cookbooks-${BERKS_PACKAGE_VERSION}.tar data_bags/
gzip cookbooks-${BERKS_PACKAGE_VERSION}.tar
```


### Uploading

_Example for common-proxy in dev_

```shell
export CHEF_ENV="dev"
export PROXY_TYPE="common-proxy"
aws s3 cp cookbooks-${BERKS_PACKAGE_VERSION}.tar.gz s3://sl-chef-${CHEF_ENV}/${PROXY_TYPE}/config/
rm cookbooks-*.tar.gz
````

### Re-deploy config

_Example for common-proxy in dev_

The best way to trigger this is to ssh in and run the following commands.
```shell
sudo aws --region us-west-1 s3 sync s3://sl-chef-dev/common-proxy/config /etc/chef/
sudo /tmp/execute_first_chef_client_run.sh
```
