# Static ldap auth strings
default['sturdy']['openvpn']['ldap_hostname'] =
  'ldap.us.onelogin.com'
default['sturdy']['openvpn']['ldap_url'] =
  "ldaps://#{node['sturdy']['openvpn']['ldap_hostname']}"
default['sturdy']['openvpn']['users_base_dn'] =
  'ou=users,dc=surfline,dc=onelogin,dc=com'
default['sturdy']['openvpn']['groups_base_dn'] =
  'ou=roles,dc=surfline,dc=onelogin,dc=com'
default['sturdy']['openvpn']['ldap_auth_search_string'] =
  '(&(email=${username})(memberOf=cn=${vpngroup},${groupdn}))'

# Configure automatic updates
default['sturdy']['openvpn']['auto_update'] = true

# Set Surfline OpenVPN Groups Per Env
default['sturdy']['openvpn']['vpn_group'] = case node.chef_environment
                                            when 'dev'
                                              'Surfline_Dev_VPN'
                                            when 'prod'
                                              'Surfline_Prod_VPN'
                                            when 'legacy'
                                              'Surfline_Legacy_VPN'
                                            end
