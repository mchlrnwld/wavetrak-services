#
# Cookbook Name:: vpn
# Recipe:: default
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Read in environment attributes and set openvpn config settings if our standard
# attributes are set

params = aws_parameter_store('us-west-1')["/#{node.chef_environment}/surfline_vpn/*"]

sturdy_openvpn_update_attributes cookbook_name.chomp('_vpn') do
  action :nothing
end.run_action(:update)

# Install OpenVPN
include_recipe 'sturdy_openvpn::install'

# Create config/ssl files on S3 and setup LDAP auth
include_recipe 'sturdy_openvpn::config'

# Configure and start OpenVPN server
include_recipe 'sturdy_openvpn::server'

# Logging/metrics/SSM management
include_recipe 'sturdy_openvpn::logging'

edit_resource!(:template, '/usr/local/bin/ldap_auth.sh') do
  source 'ldap_auth.sh.erb'
  cookbook 'surfline_vpn'
  variables(
    vpn_group: node['sturdy']['openvpn']['vpn_group'],
    bindname: params["/#{node.chef_environment}/surfline_vpn/username"],
    bindpw: params["/#{node.chef_environment}/surfline_vpn/password"]
  )
end

template '/opt/sturdy/etc/logsforwarder/config/auth_log' do
  cookbook 'sturdy_platform'
  source 'generic_logsforwarder.conf.erb'
  variables(
    monitored_file: '/var/log/auth.log',
    log_group_name: "#{node.chef_environment}" + '-/var/log/auth.log',
    datetime_format: '%d-%b-%Y %H:%M:%S'
  )
  only_if { ::File.exist? '/var/log/auth.log' }
  notifies :restart, 'service[sturdy-platform]', :delayed
end
