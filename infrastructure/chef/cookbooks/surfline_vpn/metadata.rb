name 'surfline_vpn'
maintainer 'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license 'All rights reserved'
description 'Installs/Configures the VPC VPN'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.1'

chef_version '>= 12.15'
supports 'ubuntu', '>= 16.04'

depends 'aws_parameter_store', '~> 2.1.1'
depends 'sturdy_openvpn', '~> 2.0'
depends 'sturdy_platform', '~> 2.0'
