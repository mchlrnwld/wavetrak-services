name 'surfline-iam-ssh'
maintainer 'Sturdy Networks'
maintainer_email 'devops@sturdy.cloud'
license 'all_rights'
description 'Deploys the Surfline IAM SSH Cookbook'
long_description 'Deploys the Surfline IAM SSH Cookbook'
version '1.0.0'

chef_version '>= 12.19'

depends 'line', '>= 2.0.0'
