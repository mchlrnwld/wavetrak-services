# Surfline IAM SSH Cookbook attributes
default['aws-ec2-ssh']['iam_ssh_group'] = case node.chef_environment
                                          when 'prod'
                                            'sl-all-ssh'
                                          else
                                            'sl-all-ssh,sl-sandbox-ssh'
                                          end

default['aws-ec2-ssh']['sudoers'] = '##ALL##'

# attributes controlling aws-ec2-ssh.conf
default['aws-ec2-ssh']['local_marker_groups'] = 'iam-synced-users'

# Enable cron hourly auto sync
default['aws-ec2-ssh']['enable_cron'] = true

# Files & Scripts
default['aws-ec2-ssh']['main_config'] = '/etc/aws-ec2-ssh.conf'
default['aws-ec2-ssh']['auth_keys_cmd'] = '/opt/authorized_keys_command.sh'
default['aws-ec2-ssh']['import_usr_cmd'] = '/opt/import_users.sh'
default['aws-ec2-ssh']['sshd_config'] = '/etc/ssh/sshd_config'
