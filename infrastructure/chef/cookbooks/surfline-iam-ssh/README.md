# surfline-ssh-cookbook

## Prerequisites

1. An instance role with permissions equivalent to the following IAM policy document:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:GetSSHPublicKey",
                "iam:ListSSHPublicKeys"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```

## Configuration

`default['aws-ec2-ssh']['iam_groups']` is the attribute that should match an IAM
group with users that have ssh keys uploaded.
`default['aws-ec2-ssh']['sudoers']` is the attribute used to control who should
have sudo access; by default everyone who has SSH - has sudo.

## Use

TBD

## Cookbook Recipes

`default` - Sets up iam-ssh-login using default attributes.
