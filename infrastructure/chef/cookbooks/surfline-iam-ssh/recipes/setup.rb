#
# Cookbook Name:: surfline-iam-ssh
# Recipe:: setup
#
# Copyright (c) 2017 Sturdy Networks, All Rights Reserved.

service 'sshd' do
  supports status: true, restart: true, reload: true
  action %i[enable start]
end

# Setup authorized_keys_command
template node['aws-ec2-ssh']['auth_keys_cmd'] do
  source 'authorized_keys_command.sh.erb'
  mode 0755
end

# setup import_usr_cmd
template node['aws-ec2-ssh']['import_usr_cmd'] do
  source 'import_users.sh.erb'
  mode 0755
end

# setup aws-ec2-ssh.conf
template node['aws-ec2-ssh']['main_config'] do
  source 'aws-ec2-ssh.conf.erb'
  variables({
              local_marker_groups: node['aws-ec2-ssh']['local_marker_groups'],
              iam_ssh_group: node['aws-ec2-ssh']['iam_ssh_group'],
              sudoers: node['aws-ec2-ssh']['sudoers']
  })
  mode 0755
end

replace_or_add '#AuthorizedKeysCommand none' do
  path node['aws-ec2-ssh']['sshd_config']
  pattern '#AuthorizedKeysCommand.*'
  line "AuthorizedKeysCommand #{node['aws-ec2-ssh']['auth_keys_cmd']}"
end

replace_or_add 'AuthorizedKeysCommandUser nobody' do
  path node['aws-ec2-ssh']['sshd_config']
  pattern 'AuthorizedKeysCommandUser.*'
  line 'AuthorizedKeysCommandUser nobody'
  notifies :restart, 'service[sshd]', :delayed
end

cron 'iam_ssh_import_users' do
  action node['aws-ec2-ssh']['enable_cron'] ? :create : :delete
  minute '0'
  hour '*'
  weekday '*'
  user 'root'
  command node['aws-ec2-ssh']['import_usr_cmd']
end

execute 'sync_users' do
  command node['aws-ec2-ssh']['import_usr_cmd']
  notifies :restart, 'service[sshd]', :delayed
end
