surfline-science CHANGELOG
==========================

1.22.5
-----
- [Paul Oginni] Update Artifactory credentials with dynamic call to AWS Parameter Store.

1.22.4
-----
- [Matt Walker] Bump aws-sdk version

1.22.3
-----
- [Matt Sollie] - Add pyarrow

1.22.2
-----
- [Matt Walker] - Add newrelic-infra recipe

1.22.1
-----
- [Matt Sollie] Added grib2json recipe

1.22.0
-----
- [Jon Price] - Added gem dependency for aws-sdk
- [Jerry Warren] - Updating parallel recipe to not fail if cookbook already installed and uses rpm file as a rpm_package

1.21.1
-----
- [Jon Price] - Added ffmpeg to all-tools.rb recipe

1.21.0
-----
- [Brandon Pierce] - Add Docker
- [Brandon Pierce] - Refresh and better constrain versions of dependencies

1.20.4
-----
- [Brandon Pierce] - Fix broken install logic for Fortran and pre-reqs

1.20.3
-----
- [Brandon Pierce] - Ensure GEOS is installed before GDAL is compiled

1.20.2
-----
- [Jon Price] - Added awscli to list of python packages to be installed.

1.20.1
-----
- [Andrew Slosarczyk] - Changed the way matplotlib gets installed. Added the freetype-devel package because it was a dependency.

1.20.0
-----
- [Andrew Slosarczyk] - Add matplotlib installation

1.19.2
-----
- [Dominick Nguyen] - Update seadas.rb remove cloning git repos

1.19.1
-----
- [Dominick Nguyen] - Add install resource for additional package dependencies

1.19.0
-----
- [Dominick Nguyen] - Add surfline-data-extraction python package installer recipe.

1.17.0
-----
- [Jon Price] - Ensured wgrib2 via yum is removed before new version installed

1.16.0
-----
- [Jon Price] - Updated wgrib2 to v0.2.0.6c 2/2017

1.15.1
-----
- [Dominick Nguyen] - Update SEADAS URLs to `https`

1.15.0
-----
- [Brandon Pierce] - Add OCSSW to SEADAS

1.14.0
-----
- [Andrew Slosarczyk] - Add missing Python module for GDAL
- [Andrew Slosarczyk] - Add automated test for GDAL Python module import
- [Brandon Pierce] - Switch to using parallel `make` build for GDAL and CDO

1.13.0
-----
- [Brandon Pierce] - Bump version dependency of `surfline-common`
- [Brandon Pierce] - Switch from `surfline-common::critsend` to `surfline-common::ses` recipe

1.12.0
-----
- [Matt Walker] - Add `brat` recipe to install Broadview Radar Altimetry Toolbox (BRAT)
- [Matt Walker] - Add `parallel` recipe to install GNU parallel
- [Matt Walker] - Update `gdal` recipe to install GDAL version 2.1.1 from source

1.11.0
-----
- [Brandon Pierce] - Bump the version of `sturdy-auto-snapshot-tool` included to allow for more granularity options in backups

1.10.0
-----
- [Brandon Pierce] - Add `python` cookbook dependency to enable use of Python related resources
- [Brandon Pierce] - Add `supervisor` recipe for installing Supervisor
- [Brandon Pierce] - Include `supervisor` recipe in charts related recipes

1.9.2
-----
- [Brandon Pierce] - Add `record_analytics` script to the `scripts` recipe

1.9.1
-----
- [Brandon Pierce] - Add missing EPEL dependency in `devtools` recipe
- [Brandon Pierce] - Add missing `netcdf` dependency in `cdo` recipe

1.9.0
-----
- [Brandon Pierce] - Add new `scripts` recipe for deploying misc scripts
- [Brandon Pierce] - Add `findID.sh` script to the `scripts` recipe

1.8.1
-----
- [Brandon Pierce] - Add missing IDL license to `idl` recipe
- [Brandon Pierce] - Add logrotate support for the license manager for IDL
- [Brandon Pierce] - Add `ftncheck` to `fortran` recipe

1.8.0
-----
- [Brandon Pierce] - Add `surfline-common::critsend` to `default` recipe

1.7.6
-----
- [Brandon Pierce] - Add missing symlinks in `lolabw2` recipe

1.7.5
-----
- [Brandon Pierce] - Add missing symlinks in `lolabw3` recipe
- [Brandon Pierce] - Add more missing symlinks in `grads` recipe

1.7.4
-----
- [Brandon Pierce] - Add PHP to `slcharts02` recipe

1.7.3
-----
- [Brandon Pierce] - Fix incorrect symlinks for `lolabw5` recipe
- [Brandon Pierce] - Fix duplicate links for `/var/www/html.new/cwatchtn` in `lolabw5` recipe

1.7.2
-----
- [Brandon Pierce] - Include `yum-epel` cookbook in `netcdf` recipe
- [Brandon Pierce] - Add additional grads symlinks

1.7.1
-----
- [Brandon Pierce] - Add missing compilation options to `cdo` recipe
- [Brandon Pierce] - Add missing symlink for `gradsdods` --> `grads`
- [Brandon Pierce] - Add missing symlinks for `coastwatch` recipe

1.7.0
-----
- [Brandon Pierce] - Revert to packaged GMT version 4.x

1.6.1
-----
- [Brandon Pierce] - Add missing package for `idl` recipe

1.6.0
-----
- [Brandon Pierce] - Install multiple grads versions side-by-side

1.5.1
-----
- [Brandon Pierce] - Add missing MatLab license
- [Brandon Pierce] - Add Java to various BW server recipes
- [Brandon Pierce] - Add `xvfb` recipe to fix certain MatLab crashes

1.5.0
-----
- [Brandon Pierce] - Remove NFS from `lolabwdev` recipe
- [Brandon Pierce] - Add various symlinks to `lolabwdev` recipe

1.4.0
-----
- [Brandon Pierce] - Use attributes to determine which version of Grads to install

1.3.2
-----
- [Brandon Pierce] - Add missing dependencies for `matlab` recipe

1.3.1
-----
- [Brandon Pierce] - Include `surfline-common::hostfile` recipe for adding data center servers to /etc/hosts
- [Brandon Pierce] - Include `surfline-common::resolver` recipe for managing /etc/resolv.conf

1.3.0
-----
- [Brandon Pierce] - Add NFS exports to `lolabwdev` recipe
- [Brandon Pierce] - Add additional missing libraries for the `fortran` recipe

1.2.0
-----
- [Brandon Pierce] - Add additional packages to the `utils` recipe per Science Squad's requests
- [Brandon Pierce] - Move `s3cmd` installation to its own recipe to allow for customization

1.1.0
-----
- [Brandon Pierce] - Add `backup` recipe for creating automatic snapshots of prod instance

1.0.0
-----
- [Brandon Pierce] - Initial release of surfline-science
