#AWS Parameter Store Configuration
default['aws_parameter_store']['region'] = 'us-west-1'
default['aws_ssm']['env'] = node.chef_environment
default['aws_ssm']['base_key'] = "#{node['aws_ssm']['env']}/airflow-celery"
default['aws_ssm']['base_key'] = "prod/airflow-celery"

default['surfline-science']['bucket']     = ''
default['surfline-science']['temp_dir']   = '/tmp'
default['surfline-science']['ocssw']['root'] = '/var/ocssw'
default['surfline-science']['ocssw']['version'] = 'v7.1'

default['surfline-science']['artifactory']['host']              = 'surfline.jfrog.io'
default['surfline-science']['artifactory']['path']              = '/surfline/api/pypi'
default['surfline-science']['artifactory']['package_directory'] = '/pypi-dev/simple'
default['surfline-science']['surfline-data-extraction']         = 'surfline-data-extraction'

default['newrelic']['enabled'] = case node.chef_environment
                                  when 'prod'
                                    'true'
                                  else
                                    'false'
                                  end

default['newrelic']['log_level'] = 'info'
default['newrelic']['log_limit_in_kbytes'] = '100000'
default['newrelic']['license'] = case node.chef_environment
                                    when 'prod'
                                      'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                    else
                                      'd88d91400d512eba67a0faeb922435db811b5728'
                                    end

default['newrelic-infra']['license_key'] = case node.chef_environment
                                            when 'prod'
                                              'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                            else
                                              'd88d91400d512eba67a0faeb922435db811b5728'
                                            end

default['newrelic_meetme_plugin']['license'] = case node.chef_environment
                                                when 'prod'
                                                  'a26ab469b2d2b56766af51d1c8c436e736ba72bf'
                                                else
                                                  'd88d91400d512eba67a0faeb922435db811b5728'
                                                end
default['newrelic_meetme_plugin']['prefix'] = '/usr/local/bin'

default['surfline-common']['route53']['domain_name'] = 'aws.surfline.com'
default['surfline-common']['route53']['zone_id'] = 'ZWONYSN4O2R94'

# AWS / Route 53
default['route53']['domain_name'] = 'aws.surfline.com'
default['route53']['zone_id'] = 'ZWONYSN4O2R94'

# NFS mounts
default['surfline-nfs']['client4']['options'] = 'rw'
default['surfline-nfs']['client4']['mounts'] = case node.chef_environment
                                                when 'prod'
                                                  {
                                                    'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean2_fs',
                                                    'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata2',
                                                  }
                                                when 'dev'
                                                  {
                                                    'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs',
                                                    'dev-nfs-primary-1.aws.surfline.com:/sciencedata_aws_dev' => '/sciencedata2',
                                                    'prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod' => '/ocean_aws_prod',
                                                    'prod-nfs-primary-1.aws.surfline.com:/sciencedata_aws_prod' => '/sciencedata_aws_prod',
                                                  }
                                                else
                                                  {
                                                    'dev-nfs-primary-1.aws.surfline.com:/ocean_aws_dev' => '/ocean2_fs',
                                                    'dev-nfs-primary-1.aws.surfline.com:/sciencedata_aws_dev' => '/sciencedata2',
                                                  }
                                                end

# Java
default['java']['install_flavor']                                   = 'oracle'
default['java']['jdk_version']                                      = '6'
default['java']['jdk']['6']['x86_64']['url']                        = 'https://s3-us-west-1.amazonaws.com/sl-artifacts-prod/coldfusion/jdk-6u45-linux-x64.bin'
default['java']['jdk']['6']['x86_64']['checksum']                   = nil
default['java']['oracle']['accept_oracle_download_terms']           = true
default['java']['oracle']['jce']['6']['checksum']                   = 'd0c2258c3364120b4dbf7dd1655c967eee7057ac6ae6334b5ea8ceb8bafb9262'
default['java']['oracle']['jce']['6']['url']                        = 'https://s3-us-west-1.amazonaws.com/sl-artifacts-prod/coldfusion/jce_policy-6.zip'
default['java']['oracle']['jce']['enabled']                         = false
default['java']['set_etc_environment']                              = true

# Anaconda installation defaults

default['anaconda']['version'] = '5.0.1'
default['anaconda']['python'] = 'python2'
default['anaconda']['flavor'] = nil
default['anaconda']['install_type'] = 'anaconda'
default['anaconda']['system_path'] = true

default['anaconda']['installer_info'] = {
  'anaconda' => {
    '2.2.0' => {
      'python2' => {
        'uri_prefix' => 'https://repo.continuum.io',
        'x86' => '6437d5b08a19c3501f2f5dc3ae1ae16f91adf6bed0f067ef0806a9911b1bef15',
        'x86_64' => 'ca2582cb2188073b0f348ad42207211a2b85c10b244265b5b27bab04481b88a2',
      },
      'python3' => {
        'uri_prefix' => 'https://repo.continuum.io',
        'x86' => '223655cd256aa912dfc83ab24570e47bb3808bc3b0c6bd21b5db0fcf2750883e',
        'x86_64' => '4aac68743e7706adb93f042f970373a6e7e087dbf4b02ac467c94ca4ce33d2d1',
      },
    },
    '2.3.0' => {
      'python2' => {
        'uri_prefix' => 'https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com',
        'x86' => '73fdbbb3e38207ed18e5059f71676d18d48fdccbc455a1272eb45a60376cd818',
        'x86_64' => '7c02499e9511c127d225992cfe1cd815e88fd46cd8a5b3cdf764f3fb4d8d4576',
      },
      'python3' => {
        'uri_prefix' => 'https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com',
        'x86' => '4cc10d65c303191004ada2b6d75562c8ed84e42bf9871af06440dd956077b555',
        'x86_64' => '3be5410b2d9db45882c7de07c554cf4f1034becc274ec9074b23fd37a5c87a6f',
      },
    },
    '4.4.0' => {
      'python2' => {
        'uri_prefix' => 'https://repo.continuum.io/archive',
        'x86' => nil,
        'x86_64' => nil,
      },
      'python3' => {
        'uri_prefix' => 'https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com',
        'x86' => '4cc10d65c303191004ada2b6d75562c8ed84e42bf9871af06440dd956077b555',
        'x86_64' => '3be5410b2d9db45882c7de07c554cf4f1034becc274ec9074b23fd37a5c87a6f',
      },
    },
    # '5.0.1' => {
    #   'python2' => {
    #     'uri_prefix' => 'https://repo.xcontinuum.io/archive',
    #     'x86' => '88c8d698fff16af15862daca10e94a0a46380dcffda45f8d89f5fe03f6bd2528',
    #     'x86_64' => '23c676510bc87c95184ecaeb327c0b2c88007278e0d698622e2dd8fb14d9faa4',
    #   },
    #   'python3' => {
    #     'uri_prefix' => 'https://repo.continuum.io/archive',
    #     'x86' => '991a4b656fcb0236864fbb27ff03bb7f3d98579205829b76b66f65cfa6734240',
    #     'x86_64' => '55e4db1919f49c92d5abbf27a4be5986ae157f074bf9f8238963cd4582a4068a',
    #   },
    # },
    '5.0.1' => {
      'python2' => {
        'uri_prefix' => 'https://repo.continuum.io/archive',
        'x86' => '88c8d698fff16af15862daca10e94a0a46380dcffda45f8d89f5fe03f6bd2528',
        'x86_64' => '23c676510bc87c95184ecaeb327c0b2c88007278e0d698622e2dd8fb14d9faa4',
      },
      'python3' => {
        'uri_prefix' => 'https://repo.continuum.io/archive',
        'x86' => '991a4b656fcb0236864fbb27ff03bb7f3d98579205829b76b66f65cfa6734240',
        'x86_64' => '',
      },
    },
  },
  'miniconda' => {
    'latest' => {
      'python2' => {
        'uri_prefix' => 'https://repo.continuum.io/miniconda',
        'x86' => nil,
        'x86_64' => nil,
      },
      'python3' => {
        'uri_prefix' => 'https://repo.continuum.io/miniconda',
        'x86' => nil,
        'x86_64' => nil,
      },
    },
  },
}

default['anaconda']['install_root'] = '/opt/anaconda'
default['anaconda']['accept_license'] = 'yes'
default['anaconda']['package_logfile'] = nil

default['anaconda']['owner'] = 'anaconda'
default['anaconda']['group'] = 'anaconda'
default['anaconda']['home'] = "/home/#{node['anaconda']['owner']}"

default['anaconda']['notebook'] = {
  # by default, listens on all interfaces; there will be a warning since
  # security is disabled
  'ip' => '*',
  'port' => 8888,
  'owner' => node['anaconda']['owner'],
  'group' => node['anaconda']['group'],
  'install_dir' => '/opt/jupyter/server',
  # the default is to NOT set the security token, to ensure that a secure key
  # is chosen and set
  'use_provided_token' => false,
  'token' => '',
}

# Add anaconda to the system path
default['anaconda']['system_path'] = true
