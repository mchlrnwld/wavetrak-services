surfline-science Cookbook
=========================

Installs and configures various science related tools

Requirements
------------

#### cookbooks
- `cron` - surfline-science needs cron to install and manage cron daemons
- `git` - surfline-science needs git to pull in remote repositories
- `java` - surfline-coldfusion needs Java for various BW instances
- `nfs` - surfline-science needs the nfs cookbook to install and manage NFS server and client
- `python` - surfline-science needs the python cookbook to manage Python and modules via pip
- `s3_file` - surfline-science needs s3_file to grab artifacts from S3
- `ssh_known_hosts` - surfline-science needs ssh_known_hosts to manage remote SSH host keys
- `sturdy-auto-snapshot-tool` - surfline-nfs needs the sturdy-auto-snapshot-tool for managing automatic EBS snapshots
- `yum-epel` - surfline-science needs yum-epel to setup the EPEL repository

Attributes
----------

#### surfline-science::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['surfline-science']['bucket']</tt></td>
    <td>String</td>
    <td>S3 bucket from which to download artifacts</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-science']['temp_dir']</tt></td>
    <td>String</td>
    <td>Local directory in which to download artifacts</td>
    <td><tt>/tmp</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-science']['grads']['artifact']</tt></td>
    <td>String</td>
    <td>Name of artifact to download</td>
    <td><tt>grads-2.1.a3-bin-CentOS5.11-x86_64.tar.gz</tt></td>
  </tr>
  <tr>
    <td><tt>['surfline-science']['grads']['version']</tt></td>
    <td>String</td>
    <td>Version number of Grads</td>
    <td><tt>2.1.a3</tt></td>
  </tr>
</table>

Usage
-----
#### surfline-science::default

Just include `surfline-science` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-science]"
  ]
}
```
