#
# Cookbook Name:: surfline-science
# Recipe:: ffmpeg
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Taken from https://gist.github.com/mindworker/b670903c3d8b8977afaa4440b50f48e1

s3_file "#{node['surfline-science']['temp_dir']}/ffmpeg-release-64bit-static.tar.xz" do
  bucket        "#{node['surfline-science']['bucket']}"
  remote_path   '/science/ffmpeg/ffmpeg-release-64bit-static.tar.xz'
  mode          '0755'
  not_if { ::File.exist?("#{node['surfline-science']['temp_dir']}/ffmpeg-release-64bit-static.tar.xz") }
end

execute 'setup_ffmpeg' do
  command "
  tar -xf ffmpeg-release-64bit-static.tar.xz
  cp ffmpeg-3.4-64bit-static/ffmpeg /usr/local/bin/ffmpeg
  "
  not_if { ::File.exist?('/usr/local/bin/ffmpeg') }
  cwd "#{node['surfline-science']['temp_dir']}/"
end
