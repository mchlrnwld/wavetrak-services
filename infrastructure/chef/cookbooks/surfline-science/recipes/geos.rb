#
# Cookbook Name:: surfline-science
# Recipe:: geos
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

[ 'geos',
  'geos-devel',
  'geos-python',
  'geos-python27'
].each do |p|
  package p
end
