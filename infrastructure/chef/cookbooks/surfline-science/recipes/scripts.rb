#
# Cookbook Name:: surfline-science
# Recipe:: scripts
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# findID.sh
template '/usr/local/bin/findID.sh' do
  source 'scripts/findID.sh.erb'
  mode    0755
end

# record_analytics
template '/usr/local/bin/record_analytics' do
  source 'scripts/record_analytics.erb'
  mode    0755
end
