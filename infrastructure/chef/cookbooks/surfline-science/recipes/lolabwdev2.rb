#
# Cookbook Name:: surfline-science
# Recipe:: lolabwdev2
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-science::all-tools'
include_recipe 'surfline-science::apache'

# setup links to various data
links = {
  '/ocean/live/modisftnc' => '/var/www/html.new/modisftnc',
  '/ocean/live/viirs' => '/var/www/html.new/viirs'
}

links.each do |source, target|
  link target do
    to source
  end
end
