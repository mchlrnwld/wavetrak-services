#
# Cookbook Name:: surfline-science
# Recipe:: s3cmd
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

package 's3cmd'

link '/usr/local/bin/s3cmd' do
  to '/usr/bin/s3cmd'
end
