#
# Cookbook Name:: surfline-science
# Recipe:: mpfr
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

package 'mpfr'
package 'mpfr-devel'
