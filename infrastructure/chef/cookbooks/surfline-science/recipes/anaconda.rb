#
# Cookbook Name:: surfline-science
# Recipe:: anaconda + anaconda-sourced packages
#
# Copyright 2018, Surfline / Wavetrack Inc
#
# All rights reserved - Do Not Redistribute
#

#include_recipe recipe[anaconda::python_workaround]
include_recipe 'anaconda::default'

# SciPy (this is how NumPy -- which is required for pygrib -- is being installed)
anaconda_package '-c anaconda scipy=1.2.1' do
  action :install
end

# PyArrow
anaconda_package '-c conda-forge pyarrow=0.11.0' do
  action :install
end

# PyAthena
anaconda_package '-c conda-forge pyathena=1.6.1' do
  action :install
end

# Pygrib- using conda-forge/label/gcc7 due to issue with latest conda-forge packages
anaconda_package '-c conda-forge/label/gcc7 pygrib=2.0.3' do
  action :install
end

# netCDF4- using conda-forge/label/gcc7 due to issue with latest conda-forge packages
anaconda_package '-c conda-forge/label/gcc7 netcdf4=1.4.2' do
  action :install
end
