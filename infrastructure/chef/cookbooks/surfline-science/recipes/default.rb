#
# Cookbook Name:: surfline-science
# Recipe:: default
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-common::resolver'
include_recipe 'surfline-common::hostfile'
include_recipe 'surfline-common::ses'
