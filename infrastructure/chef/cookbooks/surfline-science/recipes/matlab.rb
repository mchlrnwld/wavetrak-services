#
# Cookbook Name:: surfline-science
# Recipe:: matlab
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# matlabl prereqs
[ 'libXmu',
  'libXp',
  'libXmu.i686',
  'libXp.i686',
  'netpbm',
  'netpbm-devel',
  'netpbm-progs',
  'netpbm.i686',
].each do |p|
  package p
end

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'matlabR2010b.tar.gz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'matlabR2010b.tar.gz'
  notifies :run, 'execute[extract-matlab]', :immediately
end

# extract
execute 'extract-matlab' do
  command "
    tar zxf #{node['surfline-science']['temp_dir']}/matlabR2010b.tar.gz \
      -C /usr/local/
  "
  action :nothing
end

# license
template '/usr/local/matlabR2010b/licenses/license.lic' do
  source 'matlab/license.lic.erb'
end
