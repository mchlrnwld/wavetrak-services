#
# Cookbook Name:: surfline-science
# Recipe:: seadas
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# install git
git_client 'default'

# install OCSSW build prereqs
# http://seadas.gsfc.nasa.gov/build_ocssw.html
%w(cmake libX11-devel zlib-devel).each do |p|
  package p
end

# create the OCSSW directory structure
ocsswroot="#{node['surfline-science']['ocssw']['root']}"

# link to /ocean drive
link '/var/ocssw' do
  to '/ocean/ocssw'
end

# set OCSSW environment variables
magic_shell_environment 'OCDATAROOT' do
  value "#{ocsswroot}/run/data"
end

magic_shell_environment 'OCVARROOT' do
  value '/ocean/static/modis/occsw/var'
end

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'seadas_7.3.1_linux64_installer.sh' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'seadas_7.3.1_linux64_installer.sh'
  notifies :run, 'execute[install-seadas]', :immediately
end

# install seadas
execute 'install-seadas' do
  command "
    echo -e '#{"\n" * 32}1\n' | \
      bash #{node['surfline-science']['temp_dir']}/seadas_7.3.1_linux64_installer.sh || \
        true
  "
  action :nothing
end
