#
# Cookbook Name:: surfline-science
# Recipe:: hal2
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

node.default["authorization"]["sudo"]["groups"] = ['sl.sudoers.all', 'sl.sudoers.science']

# Override NR license key so that dev-science-dev ends up in standard NR
node.default['newrelic-infra']['license_key'] = 'a26ab469b2d2b56766af51d1c8c436e736ba72bf'

include_recipe 'newrelic' unless ENV['TEST_KITCHEN']
include_recipe 'newrelic-infra' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-nfs::client4'
include_recipe 'surfline-users'
include_recipe 'surfline-users::science'
include_recipe 'surfline-common::route53' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-common::hostfile'
include_recipe 'surfline-common::resolver'
include_recipe 'surfline-science::backup'
include_recipe 'surfline-common::ses'
