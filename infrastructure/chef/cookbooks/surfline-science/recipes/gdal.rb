#
# Cookbook Name:: surfline-science
# Recipe:: gdal
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'
include_recipe 'surfline-science::devtools'
include_recipe 'surfline-science::geos'

#set env variables
magic_shell_environment 'LD_LIBRARY_PATH' do
  value '/usr/local/lib'
end

magic_shell_environment 'PYTHONPATH' do
  value '/usr/local/lib64/python2.7/dist-packages'
end

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'gdal-2.1.1.tar.gz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'gdal-2.1.1.tar.gz'
  notifies :run, 'execute[extract-gdal]', :immediately
end

# extract
execute 'extract-gdal' do
  command "
    tar xf #{node['surfline-science']['temp_dir']}/gdal-2.1.1.tar.gz
  "
  action :nothing
  notifies :run, 'execute[install-gdal]', :immediately
end

# install
execute 'install-gdal' do
  environment(INSTALL_LAYOUT: 'amzn',
              LD_LIBRARY_PATH: '/usr/local/lib',
              PYTHONPATH: '/usr/local/lib64/python2.7/dist-packages')
  command "
    set -x ; \
    pushd gdal-2.1.1 ; \
    touch config.rpath ; \
    ./autogen.sh ; \
    ./configure \
        --with-python \
        --with-geos=yes && \
      make -j 4 && \
      make install; \
  "
  action :nothing
end


# gdal requires libproj.so to exist
link '/usr/lib64/libproj.so' do
  to '/usr/lib64/libproj.so.0'
end
