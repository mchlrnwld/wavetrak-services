#
# Cookbook Name:: surfline-science
# Recipe:: grads
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# install git
git_client 'default'
include_recipe 'docker'
include_recipe 's3cmd'

git "#{node['surfline-science']['temp_dir']}/grib2json" do
  repository 'git://github.com/cambecc/grib2json.git'
  revision 'master'
  action :sync
end

execute 'build_grib2json' do
  command "
  docker run -it --rm --name maven-builder -v ""$PWD"":/usr/src/app -w /usr/src/app maven:slim mvn package
  docker rmi maven:slim
  mv target/grib2json*.tar.gz target/grib2json-pkg.tar.gz
  s3cmd put target/grib2json-pkg.tar.gz s3://#{node['surfline-science']['bucket']}/science/
  "
  cwd "#{node['surfline-science']['temp_dir']}/grib2json"
end

