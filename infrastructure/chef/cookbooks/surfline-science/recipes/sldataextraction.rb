#
# Cookbook Name:: surfline-science
# Recipe:: sldataextraction
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

#AWS Parameter Store Configuration
params = aws_parameter_store('us-west-1')["/#{node['aws_ssm']['base_key']}/*"]

#installs Surfline-data-extraction
python_package 'surfline-data-extraction' do
  options "--extra-index-url https://#{params["/#{node['aws_ssm']['base_key']}/pip/username"]}:#{params["/#{node['aws_ssm']['base_key']}/pip/password"]}@#{node['surfline-science']['artifactory']['host']}#{node['surfline-science']['artifactory']['path']}#{node['surfline-science']['artifactory']['package_directory']}"
    if node['surfline-science'] &&
       node['surfline-science']['surfline-data-extraction'] &&
       node['surfline-science']['surfline-data-extraction']['version']
      version node['surfline-science']['surfline-data-extraction']['version']
    end
  action :upgrade
end

python_package 'numpy'
python_package 'redis'
