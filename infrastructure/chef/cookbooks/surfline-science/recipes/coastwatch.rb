#
# Cookbook Name:: surfline-science
# Recipe:: coastwatch
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'cwf-linux-x86_64.tar.gz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'cwf-linux-x86_64.tar.gz'
  notifies :run, 'execute[extract-coastwatch]', :immediately
end

# extract
execute 'extract-coastwatch' do
  command "
    tar zxf #{node['surfline-science']['temp_dir']}/cwf-linux-x86_64.tar.gz \
      -C /opt \
      --xform 's/cwf_3.3.1_1748/cwf/'
  "
  action :nothing
end

# setup symlinks
[
  'cwangles',
  'cwautonav',
  'cwcomposite',
  'cwcoverage',
  'cwdownload',
  'cwexport',
  'cwgraphics',
  'cwimport',
  'cwinfo',
  'cwmaster',
  'cwmath',
  'cwnavigate',
  'cwregister',
  'cwrender',
  'cwsample',
  'cwstats',
  'cwstatus'
].each do |b|
  link '/usr/local/bin/' + b do
    to  '/opt/cwf/bin/' + b
  end
end
