#
# Cookbook Name:: surfline-science
# Recipe:: parallel
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

parallel_pkg_path = "#{node['surfline-science']['temp_dir']}/parallel-20130122-2.1.noarch.rpm"

# declare rpm file as a package and install later
# this is so chef can keep state and recipe won't fail
declare_resource(:rpm_package, "#{parallel_pkg_path}") do
  action :nothing
end

# download artifact from S3
s3_file "#{parallel_pkg_path}" do
  not_if        "rpm -qa | grep 'parallel-20160722-3.1.amzn1.noarch'"
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'parallel-20130122-2.1.noarch.rpm'
  notifies :install, "rpm_package[#{parallel_pkg_path}]", :immediately
end
