#
# Cookbook Name:: surfline-science
# Recipe:: supervisor
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

package 'supervisor'

[
  'supervisor',
  'superlance'
].each do |p|
  python_pip p
end

directory '/var/log/supervisor'

# register the varnish service in Chef
service 'supervisord' do
  supports :start => true,
    :stop => true,
    :status => true,
    :restart => true,
    :condrestart => true,
    :reload => true,
    :'force-reload' => true
  action :enable
end
