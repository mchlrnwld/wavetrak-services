#
# Cookbook Name:: surfline-science
# Recipe:: idl
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'logrotate'
include_recipe 's3_file'

package 'redhat-lsb-core'

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'idl-82_83.tar.gz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'idl-82_83.tar.gz'
  notifies :run, 'execute[extract-idl]', :immediately
end

# extract
execute 'extract-idl' do
  command "
    tar zxf #{node['surfline-science']['temp_dir']}/idl-82_83.tar.gz \
      -C /usr/local/
  "
  action :nothing
end

# license
template '/usr/local/exelis/license/license.dat' do
  source 'idl/license.dat.erb'
end

# lmgrd log rotation
logrotate_app 'lmgrd' do
  path          '/usr/local/exelis/license/lmgrd_log.txt'
  frequency     'daily'
  options       [ 'missingok', 'copytruncate' ]
  rotate        0
  size          '50m'
end
