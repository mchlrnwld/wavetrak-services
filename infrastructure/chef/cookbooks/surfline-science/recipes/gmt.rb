#
# Cookbook Name:: surfline-science
# Recipe:: gmt
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'
include_recipe 'surfline-science::netcdf'
include_recipe 'surfline-science::gdal'

# install GMT
[ 'GMT',
  'GMT-coastlines-full',
  'GMT-coastlines-high',
  'GMT-devel',
  'GMT-doc'
].each do |p|
  package p
end
