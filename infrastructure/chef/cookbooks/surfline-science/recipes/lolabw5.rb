#
# Cookbook Name:: surfline-science
# Recipe:: lolabw5
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

node.default["authorization"]["sudo"]["groups"] = ['sl.sudoers.all', 'sl.sudoers.science']

include_recipe 'newrelic' unless ENV['TEST_KITCHEN']
include_recipe 'newrelic-infra' unless ENV['TEST_KITCHEN']
#include_recipe 'newrelic_meetme_plugin' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-nfs::client4'
include_recipe 'surfline-users'
include_recipe 'surfline-users::science'
include_recipe 'surfline-common::route53' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-common::hostfile'
include_recipe 'surfline-common::resolver'
include_recipe 'surfline-science::all-tools'
include_recipe 'surfline-science::apache'
include_recipe 'java'
include_recipe 'surfline-science::backup'
include_recipe 'surfline-common::ses'

# setup links to various data
links = {
  '/ocean/live/cwatchtnbw5' => '/var/www/html.new/cwatchtn',
  '/ocean/live/modisftnc' => '/var/www/html.new/modisftnc',
  '/ocean/live/viirs' => '/var/www/html.new/viirs',
  '/ocean/live/chloro' => '/home/buoyweather/html/chloro',
  '/ocean/live/cwatchAU' => '/var/www/html.new/cwatchAU',
  '/ocean/live/cwatchfull' => '/var/www/html.new/cwatchfull',
  '/ocean/live/jplft' => '/var/www/html.new/jplft',
  '/ocean/live/rtofsft' => '/var/www/html.new/rtofsft',
  '/ocean/bathy' => '/usr/share/otf/bathy'
}

directory '/usr/share/otf'

links.each do |source, target|
  link target do
    to source
  end
end
