#
# Cookbook Name:: surfline-science
# Recipe:: grads
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# grads versions
versions = {
  'grads-2.0.a8-bin-CentOS5.4-x86_64.tar.gz' => '2.0.a8',
  'grads-2.1.a3-bin-CentOS5.11-x86_64.tar.gz' => '2.1.a3'
}

# download artifacts from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/grads-2.0.a8-bin-CentOS5.4-x86_64.tar.gz" do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/grads-2.0.a8-bin-CentOS5.4-x86_64.tar.gz'
  notifies :run, 'execute[extract-grads-2.0]', :immediately
end

s3_file "#{node['surfline-science']['temp_dir']}" + "/grads-2.1.a3-bin-CentOS5.11-x86_64.tar.gz" do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/grads-2.1.a3-bin-CentOS5.11-x86_64.tar.gz'
  notifies :run, 'execute[extract-grads-2.1]', :immediately
end


# extract
execute 'extract-grads-2.0' do
  command "
    tar zxf #{node['surfline-science']['temp_dir']}/grads-2.0.a8-bin-CentOS5.4-x86_64.tar.gz \
      --exclude=INSTALL \
      --exclude=COPYRIGHT \
      -C /usr/local
  "
  action :nothing
end
execute 'extract-grads-2.1' do
  command "
    tar zxf #{node['surfline-science']['temp_dir']}/grads-2.1.a3-bin-CentOS5.11-x86_64.tar.gz \
      --exclude=INSTALL \
      --exclude=COPYRIGHT \
      -C /usr/local
  "
  action :nothing
end

# link each version
versions.each do |artifact, version|
  link '/usr/local/bin/grads-' + version do
    to  '/usr/local/grads-' + version + '/bin/grads'
  end
end

# link default version
[
  'bufrscan',
  'grads',
  'grib2scan',
  'gribmap',
  'gribscan',
  'gxeps',
  'gxps',
  'gxtran',
  'stnmap',
  'wgrib'
].each do |b|
  link '/usr/local/bin/' + b do
    to  '/usr/local/grads-2.0.a8/bin/' + b
  end
end

# misc symlinks
[
  'gradsc',
  'gradsdods',
  'gradsdods_bwdev'
].each do |l|
  link '/usr/local/bin/' + l do
    to '/usr/local/bin/grads'
  end
  link '/usr/bin/' + l do
    to '/usr/local/bin/' + l
  end
end

link '/usr/local/grads-2.1.a1' do
  to '/usr/local/grads-2.1.a3'
end

directory '/usr/local/lib/grads'

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'data2.tar.gz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'data2.tar.gz'
  notifies :run, 'execute[extract-data]', :immediately
end

# extract
execute 'extract-data' do
  command "
    tar zxf #{node['surfline-science']['temp_dir']}/data2.tar.gz \
      -C /usr/local/lib/grads
  "
  action :nothing
end
