#
# Cookbook Name:: surfline-science
# Recipe:: cdo
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'
include_recipe 'surfline-science::devtools'
include_recipe 'surfline-science::netcdf'

# CDO prereqs
package 'jasper-devel'

[
  'grib_api-1.12.3-5.el6.x86_64.rpm',
  'grib_api-devel-1.12.3-5.el6.x86_64.rpm'
].each do |p|
  # download package from S3
  s3_file "#{node['surfline-science']['temp_dir']}/" + p do
    bucket        "#{node['surfline-science']['bucket'] }"
    remote_path   '/science/' + p
  end
  # install package from local cache
  rpm_package "#{node['surfline-science']['temp_dir']}/" + p do
      action :install
      options '--nodeps'
  end
end

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + '/cdo-current.tar.gz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'cdo-current.tar.gz'
  notifies :run, 'execute[extract-cdo]', :immediately
end

# extract
execute 'extract-cdo' do
  command "
    tar xf #{node['surfline-science']['temp_dir']}/cdo-current.tar.gz
  "
  action :nothing
  notifies :run, 'execute[install-cdo]', :immediately
end

# install
execute 'install-cdo' do
  command "
    pushd cdo-1.7.2rc3 ; \
    ./configure \
        CFLAGS=-fPIC \
        --with-netcdf \
        --with-jasper \
        --with-hdf5 \
        --with-grib_api && \
      make -j 4 && \
      make install
  "
  action :nothing
end
