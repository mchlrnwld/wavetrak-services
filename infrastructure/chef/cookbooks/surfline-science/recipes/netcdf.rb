#
# Cookbook Name:: surfline-science
# Recipe:: netcdf
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'yum-epel'

[ 'netcdf',
  'netcdf-devel'
].each do |p|
  package p
end
