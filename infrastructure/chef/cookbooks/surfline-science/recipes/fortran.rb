#
# Cookbook Name:: surfline-science
# Recipe:: fortran
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# EPEL
include_recipe 'yum-epel'

include_recipe 's3_file'

# prereqs
[
  'glibc.i686',
  'g2clib-devel'
].each do |p|
  package p
end

s3_file "#{node['surfline-science']['temp_dir']}/compat-libf2c-34-3.4.6-19.el6.i686.rpm" do
  bucket        node['surfline-science']['bucket']
  remote_path   '/science/compat-libf2c-34-3.4.6-19.el6.i686.rpm'
end

s3_file "#{node['surfline-science']['temp_dir']}/compat-libf2c-34-3.4.6-19.el6.x86_64.rpm" do
  bucket        node['surfline-science']['bucket']
  remote_path   '/science/compat-libf2c-34-3.4.6-19.el6.x86_64.rpm'
end

execute 'install-fortran-prereqs-32-bit' do
  command "rpm -Uvh #{node['surfline-science']['temp_dir']}/compat-libf2c-34-3.4.6-19.el6.i686.rpm"
  not_if 'rpm --quiet -q compat-libf2c-34-3.4.6-19.el6.i686'
end

execute 'install-fortran-prereqs-64-bit' do
  command "rpm -Uvh #{node['surfline-science']['temp_dir']}/compat-libf2c-34-3.4.6-19.el6.x86_64.rpm"
  not_if 'rpm --quiet -q compat-libf2c-34-3.4.6-19.el6.x86_64'
end

# gfortran
package 'gcc44-gfortran'

# f77
[
  'f2c.x86_64',
  'ftnchek'
].each do |p|
  package p
end


template '/usr/local/bin/f77' do
  source  'fortran/f77.erb'
  mode 0755
end
