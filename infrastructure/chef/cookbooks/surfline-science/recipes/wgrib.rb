#
# Cookbook Name:: surfline-science
# Recipe:: wgrib
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#
package 'wgrib2' do
  action :remove
end

cookbook_file '/usr/bin/wgrib2' do
  source 'wgrib2'
  mode 0755
end
