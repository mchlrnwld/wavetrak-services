#
# Cookbook Name:: surfline-science
# Recipe:: brat
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

package 'expect'

# download artifacts from S3
[
  'brat-3.3.0-x86_64-installer.run',
  'brat-3.3.0-x86_64-installer.exp'
].each do |p|
  s3_file "#{node['surfline-science']['temp_dir']}" + "/" + p do
    bucket        "#{node['surfline-science']['bucket'] }"
    remote_path   '/science/' + p
    mode          '0755'
    notifies :run, 'execute[install-brat]', :delayed
  end
end

# install
execute 'install-brat' do
  command "
  #{node['surfline-science']['temp_dir']}/brat-3.3.0-x86_64-installer.exp
  "
  action :nothing
  cwd "#{node['surfline-science']['temp_dir']}"
end

# symlink ssl/curl libs
# FIXME: refactor to satisfy these requirements more cleanly
link '/usr/lib64/libssl.so.1.0.0' do
  to '/usr/lib64/libssl.so.10'
end

link '/usr/lib64/libcurl-nss.so.4' do
  to '/usr/lib64/libcurl.so.4'
end
