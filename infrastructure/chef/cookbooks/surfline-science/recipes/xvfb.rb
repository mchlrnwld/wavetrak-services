#
# Cookbook Name:: surfline-science
# Recipe:: xvfb
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Install Xvfb
package 'xorg-x11-server-Xvfb'

# register the xvfb service in Chef
service 'xvfb' do
  supports :start => true
  action :nothing
end

# create and enable the xvfb service
template '/etc/init.d/xvfb' do
  source    'xvfb/xvfb.erb'
  owner     'root'
  group     'root'
  mode      0755
  notifies  :enable, 'service[xvfb]', :delayed
  notifies  :start, 'service[xvfb]', :delayed
end
