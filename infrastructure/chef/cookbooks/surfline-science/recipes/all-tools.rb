#
# Cookbook Name:: surfline-science
# Recipe:: all-tools
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

[ 'anaconda',
  'brat',
  'cdo',
  'cnvgrib',
  'coastwatch',
  'devtools',
  'docker',
  'fortran',
  'gdal',
  'geos',
  'gmt',
  'grads',
  'grib2json',
  'ffmpeg',
  'idl',
  'imagemagick',
  'matlab',
  'mpfr',
  'mpich',
  'netcdf',
  'octave',
  'parallel',
  's3cmd',
  'scripts',
  'seadas',
  'sldataextraction',
  'utils',
  'wgrib' ].each do |r|
    include_recipe 'surfline-science::' + r
end

package 'freetype-devel'

# install python packages

directory '/opt/python' do
  owner     'root'
  group     'root'
  recursive true
end

template '/opt/python/surfline-science-requirements.txt' do
  source    'surfline-science-requirements.txt'
  owner     'root'
  group     'root'
  mode      '0644'
end

pip_requirements '/opt/python/surfline-science-requirements.txt'
