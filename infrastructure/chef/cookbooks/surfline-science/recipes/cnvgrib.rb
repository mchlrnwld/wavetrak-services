#
# Cookbook Name:: surfline-science
# Recipe:: cnvgrib
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}" + "/" + 'gribmaster5.tgz' do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/' + 'gribmaster5.tgz'
  notifies :run, 'execute[extract-cnvgrib]', :immediately
end

# extract
execute 'extract-cnvgrib' do
  command "
    tar xf #{node['surfline-science']['temp_dir']}/gribmaster5.tgz \
      -C /usr/local/bin/ \
      --xform 's/gribmaster5\\\/bin//' \
      gribmaster5/bin/cnvgrib
  "
  action :nothing
end
