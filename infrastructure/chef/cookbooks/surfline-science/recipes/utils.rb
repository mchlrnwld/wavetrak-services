#
# Cookbook Name:: surfline-science
# Recipe:: utils
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# EPEL
include_recipe 'yum-epel'

[ 'dstat',
  'gnuplot',
  'htop',
  'jq',
  'lynx',
  'mlocate',
  'ncdu',
  'tcsh'
].each do |p|
  package p
end
