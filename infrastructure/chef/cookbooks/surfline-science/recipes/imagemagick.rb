#
# Cookbook Name:: surfline-science
# Recipe:: imagemagick
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

package 'ImageMagick'
