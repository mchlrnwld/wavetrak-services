#
# Cookbook Name:: surfline-science
# Recipe:: docker
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

docker_installation_package 'default' do
  case node['platform_version']
  when '2016.03'
    package_name 'docker'
    package_version '1.11.2-1.6.amzn1'
  else
    action :create
  end
end

service 'docker' do
  action [ :enable, :start ]
end
