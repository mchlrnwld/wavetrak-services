#
# Cookbook Name:: surfline-science
# Recipe:: lolabw3
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-science::all-tools'
include_recipe 'surfline-science::apache'
include_recipe 'java'

# setup links to various data
links = {
  '/ocean/live/cwatchtnbw5' => '/var/www/html.new/cwatchtn',
  '/ocean/live/modisftnc' => '/var/www/html.new/modisftnc',
  '/ocean/live/viirs' => '/var/www/html.new/viirs',
  '/ocean/live/chloro' => '/home/buoyweather/html/chloro',
  '/ocean/live/cwatchAU' => '/var/www/html.new/cwatchAU',
  '/ocean/live/cwatchfull' => '/var/www/html.new/cwatchfull',
  '/ocean/live/jplft' => '/var/www/html.new/jplft',
  '/ocean/live/rtofsft' => '/var/www/html.new/rtofsft',
  '/ocean/bathy' => '/usr/share/otf/bathy'
}

directory '/usr/share/otf'

links.each do |source, target|
  link target do
    to source
  end
end
