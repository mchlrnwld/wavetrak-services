#
# Cookbook Name:: surfline-science
# Recipe:: grads
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# download artifact from S3
s3_file "#{node['surfline-science']['temp_dir']}/grib2json-pkg.tar.gz" do
  bucket        "#{node['surfline-science']['bucket'] }"
  remote_path   '/science/grib2json-pkg.tar.gz'
  notifies :run, 'execute[extract-grib2json]', :immediately
end

# extract
execute 'extract-grib2json' do
  command "
    tar xf grib2json-pkg.tar.gz
    cd grib2json*
    cp lib/* /usr/local/lib/
    cp bin/grib2json /usr/local/bin/
  "
  action :nothing
  cwd "#{node['surfline-science']['temp_dir']}"
end
