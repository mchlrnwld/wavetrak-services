#
# Cookbook Name:: surfline-science
# Recipe:: backup
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'sturdy-auto-snapshot-tool' if "#{node.chef_environment}" == 'prod'
