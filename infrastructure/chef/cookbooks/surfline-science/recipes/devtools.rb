#
# Cookbook Name:: surfline-science
# Recipe:: devtools
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'yum-epel'

bash 'yum groupinstall Development tools' do
  user 'root'
  group 'root'
  code <<-EOC
    yum groupinstall "Development tools" -y
  EOC
  not_if "yum grouplist installed | grep 'Development tools'"
end

# misc dev libraries
[ 'hdf5-devel'
].each do |p|
  package p
end
