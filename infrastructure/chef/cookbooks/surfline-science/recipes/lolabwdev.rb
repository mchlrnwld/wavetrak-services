#
# Cookbook Name:: surfline-science
# Recipe:: lolabwdev
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'surfline-science::all-tools'
include_recipe 'surfline-science::apache'
include_recipe 'java'

# setup links to various data
links = {
  '/ocean/live/viirs' => '/var/www/html.new/viirs',
  '/ocean/live/modisftnc' => '/var/www/html.new/modisftnc',
  '/ocean/live/viirssst' => '/var/www/html.new/viirssst',
  '/ocean/live/chloro' => '/home/buoyweather/html/chloro',
  '/ocean/live/cwatchAU' => '/var/www/html.new/cwatchAU',
  '/ocean/live/cwatchfull' => '/var/www/html.new/cwatchfull',
  '/ocean/live/cwatchtn' => '/var/www/html.new/cwatchtn',
  '/ocean/live/jplft' => '/var/www/html.new/jplft',
  '/ocean/live/modisfthdf' => '/var/www/html.new/modisfthdf',
  '/ocean/live/rtofsft' => '/var/www/html.new/rtofsft'
}

links.each do |source, target|
  link target do
    to source
  end
end
