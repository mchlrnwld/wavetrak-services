#
# Cookbook Name:: surfline-science
# Recipe:: zen
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#
# NFS mounts

node.default['newrelic_meetme_plugin']['services']['apache_httpd'] = {
  'scheme' => 'http',
  'host' => 'localhost',
  'port' => '80',
  'path' => '/server-status'
}

node.default["authorization"]["sudo"]["groups"] = ['sl.sudoers.all', 'sl.sudoers.science']

include_recipe 'newrelic' unless ENV['TEST_KITCHEN']
include_recipe 'newrelic-infra' unless ENV['TEST_KITCHEN']
#include_recipe 'newrelic_meetme_plugin' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-nfs::client4'
include_recipe 'surfline-users'
include_recipe 'surfline-users::science'
include_recipe 'surfline-common::route53' unless ENV['TEST_KITCHEN']
include_recipe 'surfline-science'
include_recipe 'surfline-science::all-tools'
include_recipe 'surfline-science::apache'
include_recipe 'surfline-science::backup'
include_recipe 'surfline-common::ses'
