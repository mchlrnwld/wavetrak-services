#
# Cookbook Name:: test_surfline-science
# Recipe:: all-tools
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# inclues all-tools sample data files

include_recipe 'test_surfline-science::wgrib2'
include_recipe 'test_surfline-science::gdal'
