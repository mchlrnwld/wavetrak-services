#
# Cookbook Name:: test_surfline-science
# Recipe:: gdal
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# create a sample data file input for gdal
cookbook_file '/tmp/JPL_0_latlon.tiff' do
  source 'JPL_0_latlon.tiff'
end

#Check if output file exists
file '/tmp/JPL_0_mercator.tiff' do
    only_if { ::File.exist?('/tmp/JPL_0_mercator.tiff') }
    action :delete
end

# generate the output file
execute 'generate-gdal-output' do
  command '
    /usr/local/bin/gdalwarp -s_srs EPSG:4326 -t_srs EPSG:3857 -ts 1100 880 \
      /tmp/JPL_0_latlon.tiff \
      /tmp/JPL_0_mercator.tiff
  '
end
