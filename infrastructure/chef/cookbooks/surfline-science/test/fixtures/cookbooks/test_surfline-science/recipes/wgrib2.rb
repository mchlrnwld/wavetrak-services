#
# Cookbook Name:: test_surfline-science
# Recipe:: wgrib2
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# create a sample data file input for wgrib2
cookbook_file '/tmp/nam.t12z.conusnest.hiresf03.tm00' do
  source 'nam.t12z.conusnest.hiresf03.tm00'
end

# create a sample data file2 input for wgrib2
cookbook_file '/tmp/nam.t12z.conusnest.hiresf03.tm00.grb2' do
  source 'nam.t12z.conusnest.hiresf03.tm00.grb2'
end
