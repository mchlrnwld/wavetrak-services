# # encoding: utf-8

# Inspec test for recipe surfline-science::all-tools

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

require 'digest'

# BRAT
describe command('/usr/local/bin/BratExportAscii') do
  its('stderr') { should match /^Where commandFileName/ }
end

# GNU parallel
describe command('/usr/bin/parallel --version') do
  its('stdout') { should match /^GNU parallel 20130122/ }
end

# GDAL
describe command('/usr/local/bin/gdalinfo --version') do
  its('stdout') { should match /^GDAL 2.1.1/ }
end

# GDAL - JPL data test (input)
describe file('/tmp/JPL_0_latlon.tiff') do
  its('md5sum') { should eq 'b399a9946caa6852ad6cc2c929f40e58' }
end

# GDAL - JPL data test (output)
describe file('/tmp/JPL_0_mercator.tiff') do
  its(:md5sum) { should eq 'ffdd649ff7487014d90ba889f206ff8d' }
end

#Verify that GDAL can be imported into python
describe command('LD_LIBRARY_PATH=/usr/local/lib PYTHONPATH=/usr/local/lib64/python2.7/dist-packages /usr/bin/python -c \'import gdal\'') do
  its(:exit_status) {should eq (0)}
end

#Verify that Seadas has been installed correctly
describe command('/opt/seadas-7.3.1/bin/seadas-cli.sh') do
  its(:exit_status) {should eq 0 }
  its('stdout') { should match /^Welcome to the SeaDAS command-line interface!/ }
end

# OCSSW - smigen
describe command('/var/ocssw/run/bin/linux/smigen') do
  its('stdout') { should match /smigen ifile ofile prod-name/ }
end

# OCSSW - l2bin
describe command('/var/ocssw/run/bin/linux/l2bin') do
  its('stdout') { should match /l2bin parfile=parfile or/ }
end

# Verify that wgrib2 has been installed correctly
describe command('/usr/bin/wgrib2') do
  its(:exit_status) { should eq 8 }
  its('stdout') { should match /^wgrib2 v0.2.0.6c 2\/2017/ }
end

[ '1:107:d=2017032912:PRMSL:mean sea level:3 hour fcst:',
  '1:107:d=2017032912:PRMSL:mean sea level:3 hour fcst:',
  '2:2483680:d=2017032912:TMP:surface:3 hour fcst:',
  '3:4711537:d=2017032912:TMP:2 m above ground:3 hour fcst:',
  '4:6740115:d=2017032912:RH:2 m above ground:3 hour fcst:',
  '5.1:8614512:d=2017032912:UGRD:10 m above ground:3 hour fcst:',
  '5.2:8614512:d=2017032912:VGRD:10 m above ground:3 hour fcst:',
  '6:12136151:d=2017032912:CSNOW:surface:3 hour fcst:',
  '7:12408986:d=2017032912:CICEP:surface:3 hour fcst:',
  '8:12647493:d=2017032912:CFRZR:surface:3 hour fcst:',
  '9:12889510:d=2017032912:CAPE:surface:3 hour fcst:',
  '10:13900697:d=2017032912:PWAT:entire atmosphere (considered as a single layer):3 hour fcst:',
  '11:15025668:d=2017032912:4LFTX:180-0 mb above ground:3 hour fcst:',
  '12:16238908:d=2017032912:CAPE:180-0 mb above ground:3 hour fcst:',
  '13:17224612:d=2017032912:APCP:surface:0-3 hour acc fcst:',
  '14:17759899:d=2017032912:TCDC:entire atmosphere (considered as a single layer):3 hour fcst:' ].each do |line|
    describe command('/usr/bin/wgrib2 /tmp/nam.t12z.conusnest.hiresf03.tm00 -grib /tmp/nam.t12z.conusnest.hiresf03.tm00.grb2') do
      its(:exit_status) {should eq (0)}
      its('stdout') { should include(line) }
    end
end

# Verify that awscli is 1.11.115
describe command('aws --version') do
  its(:exit_status) { should eq 0 }
  its('stderr') { should include 'aws-cli/1.11.115'}
end

# Docker
describe service('docker') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# Verify that ffmpeg is version 3.4-static
describe command('/usr/local/bin/ffmpeg') do
  its(:exit_status) { should eq 1 }
  its('stderr') { should include 'ffmpeg version 3.4-static'}
end

# Verify that Anaconda is version 1.6.5
describe command('/opt/anaconda/5.0.1/bin/anaconda --version') do
  its(:exit_status) {should eq (0)}
  its('stderr') { should include 'anaconda Command line client (version 1.6.5)' }
end

# Verify that NumPy is version 1.15.4
describe command('/opt/anaconda/5.0.1/bin/python2.7 -c \'import numpy; print numpy.version.version\'') do
  its(:exit_status) {should eq (0)}
  its('stdout') { should include '1.15.4' }
end

# Verify that PyArrow is installed and can be imported into python
describe command('/opt/anaconda/5.0.1/bin/python2.7 -c \'import pyarrow\'') do
  its(:exit_status) {should eq (0)}
end

# Verify that PyAthena is installed and can be imported into python
describe command('/opt/anaconda/5.0.1/bin/python2.7 -c \'import pyathena\'') do
  its(:exit_status) {should eq (0)}
end

# Verify that Pygrib is installed and can be imported into python
describe command('/opt/anaconda/5.0.1/bin/python2.7 -c \'import pygrib\'') do
  its(:exit_status) {should eq (0)}
end

# Verify that netCDF4 is installed and can be imported
describe command('/opt/anaconda/5.0.1/bin/python2.7 -c \'import netcdf4\'') do
  its(:exit_status) {should eq (0)}
end

# Verify correct version of awscli python package is installed
describe command('/usr/bin/python -c \'import pkg_resources; print pkg_resources.get_distribution("awscli").version\'') do
  its('stdout') { should include '1.11.115' }
end

# Verify correct version of folium python package is installed
describe command('/usr/bin/python -c \'import pkg_resources; print pkg_resources.get_distribution("folium").version\'') do
  its('stdout') { should include '0.2.1' }
end

# Verify correct version of s3transfer package is installed
describe command('/usr/bin/python -c \'import pkg_resources; print pkg_resources.get_distribution("s3transfer").version\'') do
  its('stdout') { should include '0.1.13' }
end
