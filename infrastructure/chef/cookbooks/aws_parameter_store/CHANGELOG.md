# aws_parameter_store Cookbook CHANGELOG

This file is used to list changes made in each version of the aws_parameter_store cookbook.

## 2.1.2 (2018-09-26)

- Bump aws-sdk version

## 2.1.1 (2018-4-24)

- Added paginated wildcard searches to resolve a max of 10 parameters

## 2.1.0 (2017-10-06)

- Return `nil` when attempting to lookup a single parameter that doesn't exist.

## 2.0.0 (2017-08-16)

Breaking changes:
- The default region for parameter lookup has been changed to the running instance's region (if applicable).

## 1.1.2 (2017-08-14)

- Fix wildcard lookups.

## 1.1.1 (2017-07-13)

- Add retries (with exponential backoff) to SSM API calls. This should reduce issues with multiple instances access parameters simultaneously.

## 1.1.0 (2017-06-26)

- Update aws-sdk gem dependency from `~> 2.0` to `~> 2` to better match other community cookbooks. This should allow this cookbook to better coincide in run-lists with cookbooks like `aws`.
