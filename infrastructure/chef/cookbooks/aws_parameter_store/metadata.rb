name 'aws_parameter_store'
maintainer 'Sturdy Networks'
maintainer_email 'devops@sturdy.cloud'
license 'All rights reserved'
description 'Provides the aws_parameter_store method'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://bitbucket.org/nbdev/aws_parameter_store-cookbook'
version '2.1.2'

# Should theoretically work on any platform supported by aws-sdk
%w(amazon redhat centos ubuntu windows).each do |plat|
  supports plat
end

chef_version '>= 12.8' # when gem support was added to metadata.rb

gem 'aws-sdk-ssm', '~> 1.46.0'
gem 'retries', '~> 0.0'
