# name of artifact
default['bw-proxy']['artifact']    = 'bw-nginx-aws.conf'

# S3 bucket containing artifact
default['bw-proxy']['bucket']      = ''
