name             'bw-proxy'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures bw-proxy'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.0.1'

depends 's3_file'

supports 'amazon'
