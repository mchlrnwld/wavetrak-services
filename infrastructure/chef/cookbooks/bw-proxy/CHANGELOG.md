bw-proxy CHANGELOG
==================

2.0.1
-----
- [Brandon Pierce] - Fix default Nginx artifact name

2.0.0
-----
- [Brandon Pierce] - Overwrite default Nginx config file

1.0.0
-----
- [Brandon Pierce] - Initial release of bw-proxy
