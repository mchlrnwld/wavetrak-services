#
# Cookbook Name:: bw-proxy
# Recipe:: config
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 's3_file'

# deploy Nginx configuration from S3
s3_file '/etc/nginx/nginx.conf' do
  bucket        "#{node['bw-proxy']['bucket']}"
  remote_path   '/nginx/' + "#{node['bw-proxy']['artifact']}"
  notifies :restart, 'service[nginx]', :delayed
end
