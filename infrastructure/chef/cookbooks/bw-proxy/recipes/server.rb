#
# Cookbook Name:: bw-proxy
# Recipe:: server
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# install Nginx
package 'nginx'

# enable Nginx service and set supported actions
service 'nginx' do
  supports :status => true, :restart => true, :stop => true, :start => true, :reload => true
  action :enable
end
