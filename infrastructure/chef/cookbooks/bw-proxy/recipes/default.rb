#
# Cookbook Name:: bw-proxy
# Recipe:: default
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'bw-proxy::server'
include_recipe 'bw-proxy::config'
