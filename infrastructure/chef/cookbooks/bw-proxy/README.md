bw-proxy Cookbook
=================
Deploys the Buoyweather proxy

Requirements
------------

#### cookbooks
- `s3_file` - bw-proxy needs s3_file to grab artifacts from S3

Attributes
----------

#### bw-proxy::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['bw-proxy']['artifact']</tt></td>
    <td>String</td>
    <td>name of artifact</td>
    <td><tt>bw-nginx.conf</tt></td>
  </tr>
  <tr>
    <td><tt>['bw-proxy']['bucket']</tt></td>
    <td>String</td>
    <td>S3 bucket containing artifact</td>
    <td><tt></tt></td>
  </tr>
</table>

Usage
-----
#### bw-proxy::default

Just include `bw-proxy` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[bw-proxy]"
  ]
}
```
