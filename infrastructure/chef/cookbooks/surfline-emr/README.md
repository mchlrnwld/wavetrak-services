surfline-emr Cookbook
=====================

This cookbook install various science related tools in order to support BestWinds and other EMR powered Surfline science products

Attributes
----------

#### surfline-emr::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td>['surfline-emr']['surfline-data-extraction']['version']</tt></td>
    <td>String</td>
    <td>Version of the surfline-data-extraction Python module to install. Nil class will default to latest./td>
    <td><tt></tt></td>
  </tr>
</table>

Usage
-----
#### surfline-emr::default

For an EMR master:

```json
{
  "name":"my_emr_master",
  "run_list": [
    "recipe[surfline-emr::master]"
  ]
}
```

For an EMR core/slave:

```json
{
  "name":"my_emr_core",
  "run_list": [
    "recipe[surfline-emr::core]"
  ]
}
```
