#
# Cookbook Name:: surfline-emr
# Recipe:: bestwinds
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# All of this is untested, I just wrote it out

include_recipe 'python::pip'
include_recipe 'surfline-science::fortran'
include_recipe 'surfline-science::devtools'

[
  'gcc-gfortran',
  'gcc44-gfortran',
  'gcc48-gfortran',
  'wget'
].each do |p|
  package p
end

[
  'redis',
  'pyproj',
  'numpy',
  'pymongo',
  'geojson',
  'redis_protocol',
  'newrelic'
].each do |p|
  python_pip p
end

python_pip 'surfline-data-extraction' do
  options "--extra-index-url https://#{node['surfline-emr']['artifactory']['username']}:#{node['surfline-emr']['artifactory']['password']}@#{node['surfline-emr']['artifactory']['host']}#{node['surfline-emr']['artifactory']['path']}"
    if node['surfline-emr'] &&
       node['surfline-emr']['surfline-data-extraction'] &&
       node['surfline-emr']['surfline-data-extraction']['version']
      version node['surfline-emr']['surfline-data-extraction']['version']
    end
end

remote_file '/tmp/redis-stable.tar.gz' do
  source 'http://download.redis.io/redis-stable.tar.gz'
  action :create_if_missing
  notifies :run, 'execute[untar-redis]', :immediately
end

execute 'untar-redis' do
  command 'tar zxvf redis-stable.tar.gz'
  cwd '/tmp/'
  action :nothing
  notifies :run, 'execute[build-redis]', :immediately
end

execute 'build-redis' do
  command 'make install'
  cwd '/tmp/redis-stable/'
  action :nothing
end
