name             'surfline-api'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-api'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.1'

depends 'chef-3scale', '~> 0.3.0'
depends 'openresty', '~> 0.4.0'

supports 'amazon'
