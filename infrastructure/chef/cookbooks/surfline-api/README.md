surfline-api Cookbook
=====================

This cookbook installs and configures the various components related to API

Requirements
------------

#### cookbook
- `chef-3scale` - surfline-api needs the chef-3scale cookbook to install and configure 3scale

Usage
-----
#### surfline-api::default

Just include `surfline-api` in your node's `run_list`:
