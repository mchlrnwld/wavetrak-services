#
# Cookbook Name:: surfline-api
# Recipe:: 3scale
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

directory '/usr/share/nginx/logs' do
  recursive true
end

include_recipe 'chef-3scale'
