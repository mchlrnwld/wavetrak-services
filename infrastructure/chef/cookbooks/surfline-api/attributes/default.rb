# 3scale
default['3scale']['config-source'] = 'url'
default['3scale']['provider-key'] = '3ba8f00b9c609a9578d4fec286179ba0'
default['3scale']['admin-domain'] = 'developer-surfline'
default['3scale']['config-version'] = nil
default['3scale']['config-url'] = 'https://s3-us-west-1.amazonaws.com/sl-artifacts-prod/3scale/bundle.zip'

# OpenResty
default['openresty']['source']['default_configure_flags'] = [
  "--prefix=#{node['openresty']['source']['prefix']}",
  "--conf-path=#{node['openresty']['source']['conf_path']}",
  "--sbin-path=#{node['openresty']['binary']}",
  "--error-log-path=#{node['openresty']['log_dir']}/error.log",
  "--http-log-path=#{node['openresty']['log_dir']}/access.log",
  "--pid-path=#{node['openresty']['pid']}",
  "--lock-path=#{node['openresty']['run_dir']}/nginx.lock",
  "--http-client-body-temp-path=#{node['openresty']['cache_dir']}/client_temp",
  "--http-proxy-temp-path=#{node['openresty']['cache_dir']}/proxy_temp",
  "--http-fastcgi-temp-path=#{node['openresty']['cache_dir']}/fastcgi_temp",
  "--http-uwsgi-temp-path=#{node['openresty']['cache_dir']}/uwsgi_temp",
  "--http-scgi-temp-path=#{node['openresty']['cache_dir']}/scgi_temp",
  '--with-ipv6',
  '--with-md5-asm',
  '--with-sha1-asm',
  '--without-http_ssi_module',
  '--without-mail_smtp_module',
  '--without-mail_imap_module',
  '--without-mail_pop3_module',
  '--with-luajit',
  '-j2',
  '--with-http_iconv_module'
]

# backporting https://github.com/priestjim/chef-openresty/commit/367bdbb903f905abe6ba5f6e25186b757451505a
default['openresty']['pcre']['version']  = '8.38'
default['openresty']['pcre']['url']      = "https://sourceforge.net/projects/pcre/files/pcre/#{node['openresty']['pcre']['version']}/pcre-#{node['openresty']['pcre']['version']}.tar.bz2/download"
default['openresty']['pcre']['checksum'] = 'b9e02d36e23024d6c02a2e5b25204b3a4fa6ade43e0a5f869f254f49535079df'
