surfline-common Cookbook
==================
Deploys various common tools, script, overrides, etc

Requirements
------------

#### cookbooks
- `newrelic` - surfline-common needs newrelic to install the base agent
- `postfix` - surfline-common needs postfix to install and manage Postfix

Attributes
----------

#### surfline-common::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['surfline-common']['newrelic']['app']</tt></td>
    <td>String</td>
    <td>path to newrelic-plugin-agent script</td>
    <td><tt>/usr/local/bin/newrelic-plugin-agent</tt></td>
  </tr>
</table>

Usage
-----
#### surfline-common::default

Just include `surfline-common` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[surfline-common]"
  ]
}
```
