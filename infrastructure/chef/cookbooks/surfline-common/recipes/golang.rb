#
# Cookbook Name:: surfline-common
# Recipe:: golang
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

package 'golang'
directory '/usr/local/share/golang' do
  mode 0755
end
file '/etc/profile.d/golang-global-bin.sh' do
  content "export PATH=${PATH}${PATH:+:}/usr/local/share/golang/bin\n"
  mode 0644
end

# CLI for assuming AWS roles
# https://github.com/remind101/assume-role
execute 'go get github.com/remind101/assume-role' do
  environment('GOPATH' => '/usr/local/share/golang')
  not_if { ::File.exist? '/usr/local/share/golang/bin/assume-role' }
end
