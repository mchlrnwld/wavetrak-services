#
# Cookbook Name:: surfline-common
# Recipe:: ses
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

params = {}

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

# %w[
#   ses_user
#   ses_pass
# ].each do |p|
#   params[p] = aws_parameter_store('us-west-1')["/#{environment}/coldfusion/#{p}"]
# end

ses_user = aws_parameter_store('us-west-1')["/#{environment}/coldfusion/ses_user"]
ses_pass = aws_parameter_store('us-west-1')["/#{environment}/coldfusion/ses_pass"]

include_recipe 'postfix'

# postmap for the SASL password
execute 'postmap-sasl_passwd' do
  command 'postmap /etc/postfix/sasl_passwd'
  action :nothing
end

# set the SASL password
template '/etc/postfix/sasl_passwd' do
  sensitive true
  source 'postfix/sasl_passwd.erb'
  owner 'root'
  group 'root'
  mode 0400
  variables({
    :smtp_sasl_passwd => ses_pass,
    :smtp_sasl_user_name => ses_user})
  notifies :run, 'execute[postmap-sasl_passwd]', :immediately
  notifies  :restart, 'service[postfix]', :delayed
end
