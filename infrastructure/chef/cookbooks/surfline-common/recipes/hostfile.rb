#
# Cookbook Name:: surfline-common
# Recipe:: hostfile
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

##############################################################################
# ColdFusion

ips = {}

%w[
  prod-coldfusion-admin-1
  prod-science-zen-1
  prod-coldfusion-qa-1
  prod-coldfusion-jobs-1
  prod-coldfusion-sciencejobs-1
  prod-coldfusion-tile-1
  prod-science-lolabw5-1
  prod-science-snarf2-1
  prod-science-zen-1
].each do |h|
  ips[h] = Resolv.getaddress "#{h}.aws.surfline.com"
end

hostsfile_entry ips['prod-coldfusion-admin-1'] do
  hostname    'sl01'
  aliases     ['sl01.surflinedc.tld', 'admin.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# jobs
hostsfile_entry ips['prod-coldfusion-jobs-1'] do
  hostname    'sljobs01'
  aliases     ['sljobs01.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# qa
hostsfile_entry ips['prod-coldfusion-qa-1'] do
  hostname    'cf3'
  aliases     ['cf3.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# sandbox
hostsfile_entry '192.168.147.15' do
  hostname    'sandbox.surflinedc.tld'
  aliases     ['sandbox4.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# science jobs
hostsfile_entry ips['prod-coldfusion-sciencejobs-1'] do
  hostname    'sciencejobs01'
  aliases     ['sciencejobs01.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# tile
hostsfile_entry ips['prod-coldfusion-tile-1'] do
  hostname    'sl20'
  aliases     ['sl20.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

##############################################################################
# Science

# lolabw5
hostsfile_entry ips['prod-science-lolabw5-1'] do
  hostname    'lolabw5'
  aliases     ['lolabw5.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# snarf2
hostsfile_entry ips['prod-science-snarf2-1'] do
  hostname    'snarf2'
  aliases     ['snarf2.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end

# tides
hostsfile_entry ips['prod-science-zen-1'] do
  hostname    'tides'
  aliases     ['tides.surflinedc.tld', 'zen.surflinedc.tld']
  comment     'Created by Chef'
  ignore_failure true
end
