#
# Cookbook Name:: surfline-common
# Recipe:: newrelic-httpd
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# register the httpd service
service 'httpd' do
  supports :status => true, :restart => true, :stop => true, :start => true
  action :enable
end

# setup the server-status endpoint
template '/etc/httpd/conf.d/server-status.conf' do
  source    'httpd/server-status.erb'
  owner     'root'
  group     'root'
  mode      0644
  notifies  :restart, 'service[httpd]', :delayed
end
