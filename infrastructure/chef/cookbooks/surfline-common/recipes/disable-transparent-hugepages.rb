#
# Cookbook Name:: surfline-common
# Recipe:: disable-transparent-hugepages
#
# Copyright 2016, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# register the disable-transparent-hugepages service in Chef
service 'disable-transparent-hugepages' do
  supports :status => true, :start => true
  action :nothing
end

# create and enable the disable-transparent-hugepages service
template '/etc/init.d/disable-transparent-hugepages' do
  source    'linux/disable-transparent-hugepages.erb'
  owner     'root'
  group     'root'
  mode      0755
  notifies  :enable, 'service[disable-transparent-hugepages]', :delayed
  notifies  :start, 'service[disable-transparent-hugepages]', :delayed
end
