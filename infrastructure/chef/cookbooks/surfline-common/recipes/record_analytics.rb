#
# Cookbook Name:: surfline-coldfusion
# Recipe:: record_analytics
#
# Copyright 2019, Surfline / Wavetrack Inc
#
# All rights reserved - Do Not Redistribute
#
#

# record_analytics
template '/usr/local/bin/record_analytics.sh' do
  source 'scripts/record_analytics.erb'
  mode    0755
end
