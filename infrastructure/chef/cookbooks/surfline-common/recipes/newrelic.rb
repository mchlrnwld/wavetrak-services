#
# Cookbook Name:: surfline-common
# Recipe:: newrelic-plugin-agent
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# register the newrelic-plugin-agent service
service 'newrelic-plugin-agent' do
  supports :status => true, :restart => true, :stop => true, :start => true
  action :enable
end

# overrides for init script defaults
template '/etc/sysconfig/newrelic-plugin-agent' do
  source    'sysconfig/newrelic-plugin-agent.erb'
  owner     'root'
  group     'root'
  mode      0644
  variables ({
    :app => node['surfline-common']['newrelic']['app']
  })
  notifies  :restart, 'service[newrelic-plugin-agent]', :delayed
end
