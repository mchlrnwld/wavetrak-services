#
# Cookbook Name:: surfline-common
# Recipe:: route53
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

environment = if node.chef_environment == 'prod'
                node.chef_environment
              elsif node.chef_environment == 'staging'
                node.chef_environment
              else
                'staging'
              end

%w[
  route53_key
  route53_secret
].each do |p|
  params[p] = aws_parameter_store('us-west-1')["/#{environment}/common/#{p}"]
end

include_recipe 'route53'

route53_record node['hostname'] do
  name                      node['hostname'] + "." + node['surfline-common']['route53']['domain_name']
  value                     node['ipaddress']
  type                      "A"
  ttl                       60
  zone_id                   node['surfline-common']['route53']['zone_id']
  aws_access_key_id         params['route53_key']
  aws_secret_access_key     params['route53_secret']
  overwrite                 true
  ignore_failure            true
  action                    :create
end
