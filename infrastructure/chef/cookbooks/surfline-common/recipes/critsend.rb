#
# Cookbook Name:: surfline-common
# Recipe:: critsend
#
# Copyright 2015, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# load the encrypted data bag secret key
files = Chef::DataBagItem.load('chef', 'files')
secret_file = files['secret_file']

# load rsyncd secrets from encrypted data bag
databag_secret = Chef::EncryptedDataBagItem.load_secret(secret_file)
secrets = Chef::EncryptedDataBagItem.load('postfix', "#{node.chef_environment}", databag_secret)

include_recipe 'postfix'

# postmap for the SASL password
execute 'postmap-sasl_passwd' do
  command 'postmap /etc/postfix/sasl_passwd'
  action :nothing
end

# set the SASL password
template '/etc/postfix/sasl_passwd' do
  sensitive true
  source 'postfix/sasl_passwd.erb'
  owner 'root'
  group 'root'
  mode 0400
  variables(
    :smtp_sasl_passwd => secrets['critsend']['password'],
    :smtp_sasl_user_name => secrets['critsend']['username'])
  notifies :run, 'execute[postmap-sasl_passwd]', :immediately
  notifies  :restart, 'service[postfix]', :delayed
end
