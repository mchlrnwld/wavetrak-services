# path to newrelic-plugin-agent script
default['surfline-common']['newrelic']['app']           = '/usr/local/bin/newrelic-plugin-agent'

# resolver
default['resolver']['search']                           = 'aws.surfline.com'
default['resolver']['nameservers']                      = [ '8.8.8.8', '4.2.2.2' ]

# postfix
default['postfix']['main']['mydomain']                          = 'aws.surfline.com'
default['postfix']['main']['myorigin']                          = 'aws.surfline.com'
default['postfix']['main']['relayhost']                         = '[email-smtp.us-west-2.amazonaws.com]:587'
default['postfix']['main']['smtp_sasl_auth_enable']             = 'yes'
default['postfix']['use_transport_maps']                        = true
default['postfix']['transports']                                = ['surfline.com :','surflineadmin.com :','aws.surfline.com :','* discard:']
