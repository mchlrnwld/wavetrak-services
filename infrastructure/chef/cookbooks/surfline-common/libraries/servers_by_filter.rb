#
# Cookbook Name:: surfline-common
# Library:: server_by_filter
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

require 'aws-sdk-ec2'

# Filter syntax: filter = [{ name: 'tag:name', values: ['value-string'] }]
module AWSSDK
  def self.servers_by_filter(region, filter)
    ec2 = Aws::EC2::Resource.new(region: region)
    servers = ec2.instances(filters: [filter]).to_a || nil

    servers
  end
end
