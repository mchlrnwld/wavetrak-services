name             'surfline-common'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Installs/Configures surfline-common'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.10.5'

depends 'aws_parameter_store', '~> 2.1.1'
depends 'hostsfile', '~> 2.4'
depends 'newrelic'
depends 'postfix', '~> 3.8'
depends 'resolver', '~> 1.3'
depends 'route53', '~> 0.4'

gem 'aws-sdk-ec2', '~> 1.84.0' if respond_to?(:gem)

supports 'amazon'
