#Recipe to create hostnames
#Adding recipe

hostname = Chef::Config[:node_name]

rabbitmq_servers = search(:node, 'role:rabbitmq-cluster')

execute "change_hostname" do
  command "hostname #{hostname}"
  action :nothing
end

template "/etc/hosts" do
  source "hosts.erb"
  owner "root"
  group "root"
  mode 0644
  variables(
    :hostname => Chef::Config[:node_name],
    :rabbitmq_servers => rabbitmq_servers
  )
end

case node["platform"]

when "debian", "ubuntu"
  template "/etc/hostname" do
    source "hostname.erb"
    owner "root"
    group "root"
    mode 0644
    variables(
      :hostname => Chef::Config[:node_name]
    )
    notifies :run, "execute[change_hostname]", :immediately
  end

when "amazon", "redhat", "centos", "fedora"
  template "/etc/sysconfig/network" do
    source "network.erb"
    owner "root"
    group "root"
    mode 0644
    variables(
      :hostname => Chef::Config[:node_name]
    )
    notifies :run, "execute[change_hostname]", :immediately
  end

end

