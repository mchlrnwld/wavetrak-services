# sturdy_openvpn

## Setting Up a New Server

* Create an openvpn cookbook for the client, using the sample_ recipes as a template for the client cookbook's default recipe. The cookbook should be named `SHORTNAME_vpn`, where `SHORTNAME` is the short customer named passed as the `CustomerName` core stack parameter.
  * Make sure to copy this cookbook's Berksfile to the wrapper cookbook (the integration entries can optionally be omitted)

## Testing

**TODO:** Troubleshoot OpenLDAP deployment (in test_sturdy_openldap cookbook) on Amazon Linux; test instructions below will only work with CentOS.

The test-kitchen `server` suite will add an OpenLDAP server to the OpenVPN instance and configure the OpenVPN server to authenticate against. To test the connection, use a simple OpenVPN client configuration like the following:
```
client
dev tun
proto udp
remote REPLATEWITHPUBLICIP 1194
cipher AES-256-CBC
comp-lzo
resolv-retry infinite
nobind
persist-key
persist-tun
ca REPLACEWITHOPENLDAPCOOKBOOKPATH/files/default/ldap.example.com.pem
auth-user-pass
auth-nocache
reneg-sec 43200
verb 3
```
Replacing the public ip and cookbook path (e.g. `/Users/yourhomedir/.berkshelf/cookbooks/openldap-test-81454d06a96637122b41f5bf7cd4b5f56dd75c17`) and starting with `openvpn -config FILENAME`.

You will be prompted for authentication:
* Username: `jcdenton`
* Password: `bionicman` (generated via `slappasswd -h {SSHA}`)
