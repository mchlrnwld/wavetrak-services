#!/bin/bash
# Check for running OpenVPN service

METRICNAME="openvpn.server.service.status"

if pgrep openvpn > /dev/null
then
  echo "$METRICNAME 0"
else
  echo "$METRICNAME 2"
fi
