property :vpn_url, kind_of: String, name_property: true
property :port, kind_of: String, default: '1194'
property :proto, kind_of: String, default: 'udp'
property :reneg_sec, kind_of: String, default: '43200'
property :bucket, kind_of: String
property :folder, kind_of: String, default: 'common/vpnservers'
property :region, kind_of: String, default: 'us-east-1'

default_action :create

action :create do
  ruby_block 'create_openvpn_config' do
    block do
      require 'aws-sdk-s3'
      s3 = Aws::S3::Client.new(region: new_resource.region)

      ca_cert = s3.get_object(bucket: new_resource.bucket,
                              key: "#{new_resource.folder}/ssl/ca.crt").body.read
      client_config =
        "client\n"\
        "dev tun\n"\
        "proto #{new_resource.proto}\n"\
        "remote #{new_resource.vpn_url} #{new_resource.port}\n"\
        "comp-lzo\n"\
        "resolv-retry infinite\n"\
        "nobind\n"\
        "persist-key\n"\
        "persist-tun\n"\
        "auth-user-pass\n"\
        "auth-nocache\n"\
        "reneg-sec #{new_resource.reneg_sec}\n"\
        "verb 3\n"\
        "cipher AES-256-CBC\n"\
        "<ca>\n#{ca_cert}</ca>\n"

      s3.put_object(bucket: new_resource.bucket,
                    key: "#{new_resource.folder}/client.ovpn",
                    body: client_config,
                    server_side_encryption: 'AES256')
    end
  end
end
