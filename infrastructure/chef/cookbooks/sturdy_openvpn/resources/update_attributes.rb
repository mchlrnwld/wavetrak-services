property :customer_name, kind_of: String, name_property: true

default_action :update

require_relative '../libraries/sturdycidrtranslation'

action :update do
  if node['sturdy']['openvpn']['vpn_subnet_cidr']
    vpn_subnet = node['sturdy']['openvpn']['vpn_subnet_cidr'].split('/').first
    vpn_netmask = SturdyCIDRTranslation.cidr_to_netmask(
      node['sturdy']['openvpn']['vpn_subnet_cidr'].split('/').last.to_i
    )
    node.default['openvpn']['subnet'] = vpn_subnet
    node.default['openvpn']['config']['server'] = "#{vpn_subnet} #{vpn_netmask}"
  end
  if node['sturdy']['openvpn']['chef_data_bucket_region'].nil?
    node.default['sturdy']['openvpn']['chef_data_bucket_region'] =
      node['ec2']['placement_availability_zone'][0...-1] # e.g. 'us-east-1'
  end
  if node['sturdy']['openvpn']['vpn_group'].nil?
    node.default['sturdy']['openvpn']['vpn_group'] =
      "sturdy-vpn-#{new_resource.customer_name}"
  end
  if node['sturdy']['openvpn']['vpn_url'].nil?
    node.default['sturdy']['openvpn']['vpn_url'] =
      "vpn.#{new_resource.customer_name}.int.sturdynetworks.com"
  end
  # URL above doesn't need to resolve; client connections are made to this address
  if node['sturdy']['openvpn']['vpn_address'].nil?
    node.default['sturdy']['openvpn']['vpn_address'] =
      case node['sturdy']['openvpn']['vpn_elastic_ip'].nil?
      when true
        node['cloud_v2']['public_ipv4']
      else
        node['sturdy']['openvpn']['vpn_elastic_ip']
      end
  end
end
