property :vpn_url, kind_of: String, name_property: true
property :bucket, kind_of: String
property :folder, kind_of: String, default: 'common/vpnservers'
property :region, kind_of: String, default: 'us-east-1'

default_action :create_if_missing

action :create_if_missing do
  # Setup key directory
  directory node['openvpn']['key_dir'] do
    owner node['openvpn']['config']['user']
    group node['openvpn']['config']['group']
    mode 0750
  end

  ruby_block 'create_openvpn_ssl' do # ~FC005
    block do
      require 'aws-sdk-s3'
      s3 = Aws::S3::Client.new(region: new_resource.region)
      objects = s3.list_objects_v2(bucket: new_resource.bucket,
                                   prefix: "#{new_resource.folder}/")

      # no-op if the server cert already is on S3
      unless objects.contents.detect { |o| o.key.start_with? "#{new_resource.folder}/ssl/server.crt" }
        require 'certificate_authority'
        fifteen_years_out = Time.now + 60 * 60 * 24 * 365 * 15
        ca_signing_profile = {
          'extensions' => {
            'keyUsage' => {
              'usage' => %w(critical keyCertSign)
            }
          }
        }
        cert_signing_profile = {
          'extensions' => {
            'basicConstraints' => { 'ca' => false },
            'subjectKeyIdentifier' => {},
            'authorityKeyIdentifier' => {},
            'keyUsage' => { 'usage' => %w(digitalSignature nonRepudiation) },
            'extendedKeyUsage' => { 'usage' => %w(serverAuth clientAuth) }
          }
        }

        root = CertificateAuthority::Certificate.new
        root.subject.common_name = "ca-#{new_resource.vpn_url}"
        root.serial_number.number = 1
        root.not_after = fifteen_years_out
        root.key_material.generate_key
        root.signing_entity = true
        root.sign!(ca_signing_profile)

        plain_cert = CertificateAuthority::Certificate.new
        plain_cert.subject.common_name = new_resource.vpn_url
        plain_cert.serial_number.number = 2
        plain_cert.not_after = fifteen_years_out
        plain_cert.key_material.generate_key
        plain_cert.parent = root
        plain_cert.sign!
        plain_cert.sign!(cert_signing_profile)

        {
          'ca.crt' => root.to_pem,
          'ca.key' => root.key_material.private_key.to_pem,
          'server.crt' => plain_cert.to_pem,
          'server.key' => plain_cert.key_material.private_key.to_pem
        }.each do |k, v|
          s3.put_object(bucket: new_resource.bucket,
                        key: "#{new_resource.folder}/ssl/#{k}",
                        body: v,
                        server_side_encryption: 'AES256')
        end
      end
    end
    not_if { ::File.exist? "#{node['openvpn']['key_dir']}/server.key" }
  end

  ruby_block 'create_dh_if_missing' do
    block do
      require 'aws-sdk-s3'
      s3 = Aws::S3::Client.new(region: new_resource.region)
      objects = s3.list_objects_v2(bucket: new_resource.bucket,
                                   prefix: "#{new_resource.folder}/")
      # no-op if the file already is on S3
      unless objects.contents.detect { |o| o.key.start_with? "#{new_resource.folder}/ssl/dh#{node['openvpn']['key']['size']}.pem" }
        dh = OpenSSL::PKey::DH.new(node['openvpn']['key']['size'].to_i)
        # dh generation can take a long time; simpler here to reinstantiate the
        # S3 client rather than checking for active status
        s3client = Aws::S3::Client.new(region: new_resource.region)
        s3client.put_object(bucket: new_resource.bucket,
                            key: "#{new_resource.folder}/ssl/dh#{node['openvpn']['key']['size']}.pem",
                            body: dh.public_key.to_pem,
                            server_side_encryption: 'AES256')
      end
    end
    not_if { ::File.exist? "#{node['openvpn']['key_dir']}/dh#{node['openvpn']['key']['size']}.pem" }
  end

  # Deploy SSL keypair
  node.run_state['sturdy_openvpn_citadel'] = {}
  # Until https://github.com/poise/citadel/issues/28 is resolved, work around
  # it by reading in the values in our own ruby_block
  ruby_block 'load_openvpn_ssl_secrets_into_run_state' do
    block do
      require 'aws-sdk-s3'
      s3 = Aws::S3::Client.new(region: new_resource.region)
      [
        'ca.crt',
        'server.crt',
        'server.key',
        "dh#{node['openvpn']['key']['size']}.pem"
      ].each do |f|
        data = s3.get_object(bucket: new_resource.bucket,
                             key: "#{new_resource.folder}/ssl/#{f}").body.read
        node.run_state['sturdy_openvpn_citadel'][f] = data
      end
    end
  end
  [
    'ca.crt',
    'server.crt',
    'server.key',
    "dh#{node['openvpn']['key']['size']}.pem"
  ].each do |f|
    file "#{node['openvpn']['key_dir']}/#{f}" do
      owner node['openvpn']['config']['user']
      group node['openvpn']['config']['group']
      mode f.start_with?('dh') ? '640' : '600'
      # content lazy { citadel(new_resource.bucket, new_resource.region)["#{new_resource.folder}/ssl/#{f}"] }
      content lazy { node.run_state['sturdy_openvpn_citadel'][f] }
      notifies :restart, 'service[openvpn]' if
        ::File.exist? "#{node['openvpn']['key_dir']}/server.crt" # don't restart on the first run
    end
  end
end
