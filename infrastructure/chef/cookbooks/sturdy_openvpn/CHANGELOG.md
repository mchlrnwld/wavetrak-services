# sturdy_openvpn Cookbook CHANGELOG

This file is used to list changes made in each version of the sturdy_openvpn cookbook.

## 2.3.3 (2018-09-26)

- Bump aws-sdk version

## 2.3.2 (2017-08-07)

- Add `config` recipe to reduce boilerplate in consuming cookbooks

## 2.3.1 (2017-07-21)

- Fix client config generation to use elastic IP if provided

## 2.3.0 (2017-07-21)

- Add optional elastic ip association

## 2.2.4 (2017-07-14)

- Make CloudWatch metrics (published by the logging recipe) names configurable via attributes

## 2.2.3 (2017-06-29)

- Fix metrics publishing to match namespacing set in v2.2.1

## 2.2.2 (2017-06-29)

- Fix service restart notification
    - This was incorrectly introduced in v2.2.1

## 2.2.1 (2017-06-28)

- Updated suggested OpenVPN cookbook commit to support [proper service name on Ubuntu](https://github.com/sous-chefs/openvpn/commit/34b2723a7539c8550d6b42e9dd09fe285c158b9c#diff-60bfb5066c3341ede8cc371d12094986)
    - This allowed the hacky initial deployment restart code to be dropped
- Dropped status server config option from v2.1.0 - this is done automatically by the systemd unit file under /run
- Moved service restart handling to systemd

## 2.2.0 (2017-06-27)

- Add monitor script for starting OpenVPN service if it stops unexpectedly.
- Add logging recipe to publish metrics to CloudWatch

## 2.1.1 (2017-06-26)

- Update aws-sdk gem dependency from `~> 2.6` to `~> 2` to better match other community cookbooks. This should allow this cookbook to better coincide in run-lists with cookbooks like `aws`.

## 2.1.0 (2017-06-26)

- Add `status` [server config option](https://github.com/OpenVPN/openvpn/blob/v2.4.3/sample/sample-config-files/server.conf#L284-L287)

## 2.0.0 (2017-06-02)

Breaking changes:
- `sturdy_openvpn_create_ssl` & `sturdy_openvpn_create_client_config` resources now support a configurable S3 key prefix AKA folder. This was previously hardcoded to `vpnservers` and now defaults to `common/vpnservers`.

Other changes:
- The server recipe will now ensure /var/log/openvpn.log exists
    - This is a temporary workaround for the Debian-family service startup issue's impact on any log ingestion setup during the initial chef-client run
- Removed the remaining components referencing the aws logs forwarder installation by this cookbook

## 1.1.0 (2017-05-17)

- Move remaining hard-coded ldap_auth.sh values into node attributes.

## 1.0.0 (2017-05-17)

- Move components related to the SATAN cert into their own recipe, for easier exclusion if desired in cookbook's wrapping this. This will require an additional recipe inclusion for most consumers of this cookbook (add `include_recipe 'sturdy_openvpn::satan_cert'` just before `template '/usr/local/bin/ldap_auth.sh'`)
