name             'sturdy_openvpn'
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Configures OpenVPN server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url       'https://bitbucket.org/nbdev/sturdy_openvpn-cookbook'
version          '2.3.3'

chef_version '>= 12.15'

depends 'aws', '~> 7.1'
depends 'citadel', '~> 1.1'
depends 'firewall', '~> 2.6'
depends 'line', '~> 0.6'
depends 'openvpn', '~> 3.0'
depends 'sturdy_platform'

gem 'aws-sdk-s3', '~> 1.30.0'
gem 'certificate_authority', '~> 0.1'

supports 'amazon', '>= 2016.09'
supports 'centos', '>= 7'
supports 'ubuntu', '>= 16.04'
