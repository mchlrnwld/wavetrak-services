#
# Cookbook Name:: sturdy_openvpn
# Recipe:: satan_cert
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Pull in our freeipa cert
cert_dir, cert_cmd = case node['platform_family']
                     when 'debian'
                       ['/usr/local/share/ca-certificates', 'update-ca-certificates']
                     else # RHEL/Amazon
                       ['/etc/pki/ca-trust/source/anchors', 'update-ca-trust']
                     end
remote_file ::File.join(cert_dir, 'freeipa.crt') do
  source 'https://s3.amazonaws.com/misc.sturdynetworks.com/public/freeipa.crt'
  mode 0644
  notifies :run, 'execute[load_new_freeipa_cert]', :immediately
end
execute 'load_new_freeipa_cert' do
  command cert_cmd
  action :nothing
end

# TODO: determine proper SSL check satan.sturdynetworks.com
# (cert CN doesn't match ELB domain name)
append_if_no_line 'update_ldap_conf' do
  path "/etc/#{node['platform_family'] == 'debian' ? '' : 'open'}ldap/ldap.conf"
  line 'TLS_REQCERT never'
end
