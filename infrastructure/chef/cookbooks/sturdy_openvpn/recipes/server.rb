#
# Cookbook Name:: sturdy_openvpn
# Recipe:: server
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'openvpn::enable_ip_forwarding'

# Deploy the server configuration
openvpn_conf 'server' do
  notifies :restart, 'service[openvpn]', :delayed if
    ::File.exist? '/etc/openvpn/server.conf'
end

# Start the server service
include_recipe 'sturdy_openvpn::service'

# Configure automatic updates
include_recipe 'sturdy_openvpn::auto_update' if node['sturdy']['openvpn']['auto_update']
