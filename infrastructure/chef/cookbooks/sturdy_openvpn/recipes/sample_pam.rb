#
# Cookbook Name:: vpn
# Recipe:: default
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Manually allocated on
# https://nbdevs.atlassian.net/wiki/display/SG/Infrastructure+Subnets
node.default['openvpn']['push_routes'] =
  ['XX.XXX.XXX.0 255.255.248.0']
node.default['openvpn']['subnet'] = 'XX.XXX.XX.0'
node.default['openvpn']['config']['server'] =
  "#{node['openvpn']['subnet']} #{node['openvpn']['netmask']}"

# This doesn't have to really resolve; used for SSL cert CN
vpn_fqdn = 'vpn.CUSTOMERNAME.int.sturdynetworks.com'

chef_data_bucket = 'DATABUCKETNAME'
chef_data_bucket_folder = 'common/vpnservers'
chef_data_bucket_region = 'us-east-1'

# General settings defined, now start the install / config

include_recipe 'sturdy_openvpn::install'

sturdy_openvpn_create_ssl vpn_fqdn do
  bucket chef_data_bucket
  folder chef_data_bucket_folder
  region chef_data_bucket_region
end
sturdy_openvpn_create_client_config node['cloud_v2']['public_ipv4'] do
  bucket chef_data_bucket
  folder chef_data_bucket_folder
  region chef_data_bucket_region
  reneg_sec node['openvpn']['config']['reneg-sec']
end

pam_plugin = case node['platform_family']
             when 'debian'
               '/usr/lib/openvpn/openvpn-plugin-auth-pam.so'
             else # RHEL / amazon
               '/usr/lib64/openvpn/plugins/openvpn-plugin-auth-pam.so'
             end
node.default['openvpn']['config']['plugin'] = "#{pam_plugin} openvpn"
node.default['openvpn']['config']['auth-user-pass-verify'] = nil
node.default['openvpn']['config']['script-security'] = nil

file '/etc/pam.d/openvpn' do
  content "auth    required        pam_unix.so    shadow    nodelay\n"\
          "account required        pam_unix.so\n"
  mode 0644
end

# Deploy users here (e.g. user resource, users cookbook ,etc)
user 'EXAMPLE USER - REPLACE ME' do
  manage_home true # syntax here for chef >= 12.14.60
  comment 'JC Denton'
  uid '1234'
  home '/home/jc'
  shell '/bin/bash'
  # The hash to use here can be generated via "mkpasswd -m sha-512" on linux
  # systems or by using the python passlib module (from pypi) on any system:
  # python -c "from passlib.hash import sha512_crypt; print sha512_crypt.encrypt(raw_input('clear-text password: '))"
  password 'PWHASH_OR_CITADEL_CALL_HERE'
end

# Configure and start OpenVPN server
include_recipe 'sturdy_openvpn::server'
# Logging/metrics/SSM management
include_recipe 'sturdy_openvpn::logging'
