#
# Cookbook Name:: vpn
# Recipe:: default
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Manually allocated on
# https://nbdevs.atlassian.net/wiki/display/SG/Infrastructure+Subnets
node.default['openvpn']['push_routes'] =
  ['10.XXX.XXX.0 255.255.248.0']
node.default['openvpn']['subnet'] = '10.XXX.XX.0'
node.default['openvpn']['config']['server'] =
  "#{node['openvpn']['subnet']} #{node['openvpn']['netmask']}"

vpn_fqdn = 'vpn.CUSTOMERNAME.int.sturdynetworks.com' # not required to resolve
vpn_group = 'sturdy-vpn-CUSTOMERNAME'
chef_data_bucket = 'BUCKETNAME'
chef_data_bucket_folder = 'common/vpnservers'
chef_data_bucket_region = 'REGION' # e.g. 'us-east-1'

# General settings defined, now start the install / config

include_recipe 'sturdy_openvpn::install'

sturdy_openvpn_create_ssl vpn_fqdn do
  bucket chef_data_bucket
  folder chef_data_bucket_folder
  region chef_data_bucket_region
end
sturdy_openvpn_create_client_config node['cloud_v2']['public_ipv4'] do
  bucket chef_data_bucket
  folder chef_data_bucket_folder
  region chef_data_bucket_region
  reneg_sec node['openvpn']['config']['reneg-sec']
end

# Deploy LDAP auth script
template '/usr/local/bin/ldap_auth.sh' do
  cookbook 'sturdy_openvpn'
  mode 0755
  variables(vpn_group: vpn_group)
end

# Configure and start OpenVPN server
include_recipe 'sturdy_openvpn::server'
# Logging/metrics/SSM management
include_recipe 'sturdy_openvpn::logging'
