#
# Cookbook Name:: sturdy_vpn
# Recipe:: logging
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'sturdy_platform::default'

custom_cloudwatch_metrics = ''

# Enable UserScripts collector
# https://diamond.readthedocs.io/en/latest/collectors/UserScriptsCollector/
userscripts_path = '/opt/sturdy/etc/metricsforwarder/userscripts'
directory userscripts_path
file '/opt/sturdy/etc/metricsforwarder/collectors/UserScriptsCollector.conf' do
  content "enabled = True\n"\
          "scripts_path = #{userscripts_path}\n"
  mode 0644
  notifies :restart, 'service[sturdy-platform]', :delayed if
    ::File.exist?('/etc/init.d/sturdy-platform')
end
# Add openvpn process status check
cookbook_file '/opt/sturdy/etc/metricsforwarder/userscripts/openvpnstatus.sh' do
  mode 0755
  notifies :restart, 'service[sturdy-platform]', :delayed if
    ::File.exist?('/etc/init.d/sturdy-platform')
end
custom_cloudwatch_metrics += "[OpenVPNServiceStatus]\n"\
                             "collector = openvpn\n"\
                             "metric = server.service.status\n"\
                             "namespace = OpenVPN\n"\
                             "name = #{node['sturdy']['openvpn']['service_status_metric_name']}\n"\
                             "unit = None\n"\
                             "collect_without_dimension = True\n\n"

# Enable OpenVPN statistics collector
# https://diamond.readthedocs.io/en/latest/collectors/OpenVPNCollector/
openvpn_status_file = case node['openvpn']['config']['status'].nil?
                      when true
                        # Specified by debian-family systemd service file
                        "/run/openvpn/#{node['openvpn']['type']}.status"
                      else
                        if node['openvpn']['config']['status'].include?('/')
                          node['openvpn']['config']['status']
                        else
                          "/etc/openvpn/#{node['openvpn']['config']['status']}"
                        end
                      end
file '/opt/sturdy/etc/metricsforwarder/collectors/OpenVPNCollector.conf' do
  content "enabled = True\n"\
          "instances = file://#{openvpn_status_file}\n"
  mode 0644
end
custom_cloudwatch_metrics += "[OpenVPNClientsConnected]\n"\
                             "collector = openvpn\n"\
                             "metric = server.clients.connected\n"\
                             "namespace = OpenVPN\n"\
                             "name = #{node['sturdy']['openvpn']['clients_connected_metric_name']}\n"\
                             "unit = None\n"\
                             "collect_without_dimension = True\n\n"

# Publish custom metrics to cloudwatch via custom cloudwatch handler settings
file '/opt/sturdy/etc/metricsforwarder/handlers/cloudwatchHandler.conf' do
  content custom_cloudwatch_metrics
  mode 0644
  notifies :restart, 'service[sturdy-platform]', :delayed if
    ::File.exist?('/etc/init.d/sturdy-platform')
end
