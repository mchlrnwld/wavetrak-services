#
# Cookbook Name:: vpn
# Recipe:: config
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# Create/deploy OpenVPN SSL keypair
sturdy_openvpn_create_ssl node['sturdy']['openvpn']['vpn_url'] do
  bucket node['sturdy']['openvpn']['chef_data_bucket_name']
  folder node['sturdy']['openvpn']['chef_data_bucket_folder']
  region node['sturdy']['openvpn']['chef_data_bucket_region']
end
# Update client .ovpn connection file on S3
sturdy_openvpn_create_client_config node['sturdy']['openvpn']['vpn_address'] do
  bucket node['sturdy']['openvpn']['chef_data_bucket_name']
  folder node['sturdy']['openvpn']['chef_data_bucket_folder']
  region node['sturdy']['openvpn']['chef_data_bucket_region']
  reneg_sec node['openvpn']['config']['reneg-sec']
end

# Deploy LDAP auth script
include_recipe 'sturdy_openvpn::satan_cert'
template '/usr/local/bin/ldap_auth.sh' do
  cookbook 'sturdy_openvpn'
  mode 0755
  variables(vpn_group: node['sturdy']['openvpn']['vpn_group'])
end
