#
# Cookbook Name:: sturdy_openvpn
# Recipe:: auto_update
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

case node['platform_family']
when 'amazon', 'rhel' # ~FC024 we'll address this after https://github.com/chef/chef-rfc/pull/252
  # yum-cron install will fail if yum < 3.4.3-140.el7
  # https://bugzilla.redhat.com/show_bug.cgi?id=1293513
  package 'update_yum_for_yum-cron' do
    package_name 'yum'
    action :upgrade
    only_if { node['platform_version'] < '8' }
    not_if 'rpm -q --changelog yum | grep 3.4.3-140'
  end

  package 'yum-cron'

  # Enable yum 'minimal' security updates
  template '/etc/yum/yum-cron.conf'

  service 'yum-cron' do
    action [:enable, :start]
  end
when 'debian'
  package 'unattended-upgrades'
end
