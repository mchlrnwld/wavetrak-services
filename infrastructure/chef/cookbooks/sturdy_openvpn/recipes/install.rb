#
# Cookbook Name:: sturdy_openvpn
# Recipe:: install
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

if node['sturdy']['openvpn']['vpn_elastic_ip']
  aws_elastic_ip node['sturdy']['openvpn']['vpn_elastic_ip'] do
    action :associate
  end
else
  log 'No ElasticIP found; skipping association...'
end

# Install the server and related utils
include_recipe 'openvpn::install'
additional_openvpn_packages = %w(easy-rsa openvpn-auth-ldap)
additional_openvpn_packages << case node['platform_family']
                               when 'debian'
                                 'ldap-utils'
                               else
                                 'openldap-clients'
                               end
package additional_openvpn_packages

include_recipe 'firewall::default'
firewall_rule 'openvpn' do
  port 1194
  protocol :udp
  command  :allow
end
firewall_rule 'forward_new_tun0_traffic' do
  raw '-I FORWARD -i tun0 -m conntrack --ctstate NEW -j ACCEPT'
end
firewall_rule 'forward_return_traffic' do
  raw '-I FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT'
end
