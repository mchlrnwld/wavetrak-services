#
# Cookbook Name:: sturdy_openvpn
# Recipe:: service
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

# If running via systemd, ensure automatic service restarting is enabled
service_file = '/lib/systemd/system/openvpn@.service'
ruby_block 'auto_restart_openvpn_service' do
  block do
    file_content = ::File.read(service_file).sub(/\[Service\]\n/,
                                                 "[Service]\n"\
                                                 "Restart=always\n")
    ::File.write(service_file, file_content)
  end
  only_if { ::File.exist?(service_file) }
  not_if "grep 'Restart' '#{service_file}'"
  notifies :run, 'execute[reload_openvpn_systemd_config]', :immediate
end
execute 'reload_openvpn_systemd_config' do
  command 'systemctl daemon-reload'
  action :nothing
end

# Start the server service
include_recipe 'openvpn::service'
