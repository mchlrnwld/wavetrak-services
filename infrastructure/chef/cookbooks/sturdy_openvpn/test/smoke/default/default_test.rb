# # encoding: utf-8

# Test the embedded cookbook
ldapsearch = 'ldapsearch -x -LLL -D "cn=Manager,dc=example,dc=com" '\
             '-w secretsauce -b"dc=example,dc=com"'
ldapbaseoutput = /dn: dc=example,dc=com\nobjectClass: top\nobjectClass: dcObject/
describe command(ldapsearch) do
  its('stdout') { should match ldapbaseoutput }
end

membersearch = ldapsearch +
               ' \'(memberof=cn=unatco,ou=Group,dc=example,dc=com)\''
ldapmembersearchoutput =
  /dn: uid=jcdenton,ou=People,dc=example,dc=com/
describe command(membersearch) do
  its('stdout') { should match ldapmembersearchoutput }
end

# Now test the server install
%w(openvpn openvpn-auth-ldap).each do |pkg|
  describe package(pkg) do
    it { should be_installed }
  end
end

vpn_service = 'openvpn'
vpn_service << '@server' if os.redhat? # includes centos but not amazon linux
describe service(vpn_service) do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
