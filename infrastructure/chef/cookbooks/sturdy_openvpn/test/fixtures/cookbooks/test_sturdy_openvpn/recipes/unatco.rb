#
# Cookbook Name:: test_sturdy_openvpn
# Recipe:: unatco
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'sturdy_openvpn::install'

ruby_block 'create_dh_if_missing' do
  block do
      dh = OpenSSL::PKey::DH.new(node['openvpn']['key']['size'].to_i)
      ::File.open("#{node['openvpn']['key_dir']}/dh#{node['openvpn']['key']['size']}.pem", 'w') { |file| file.write(dh.public_key.to_pem) }
  end
  not_if { ::File.exist? "#{node['openvpn']['key_dir']}/dh#{node['openvpn']['key']['size']}.pem" }
end

# Setup LDAP auth
ldap_opts = { ldap_url: 'ldap://',
              bind_dn: "cn=Manager,dc=example,dc=com",
              bind_pw: 'secretsauce',
              base_dn: 'ou=People,dc=example,dc=com',
              search_filter: '(memberof=cn=unatco,ou=Group,dc=example,dc=com)' }
template '/etc/openvpn/auth/ldap.conf' do
  cookbook 'sturdy_openvpn'
  mode 0600
  variables ldap_opts
end

# Don't need updates to run during testing
node.override['sturdy']['openvpn']['auto_update'] = false
include_recipe 'sturdy_openvpn::server'
include_recipe 'sturdy_openvpn::cloudwatch'
