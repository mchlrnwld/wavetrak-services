name             'test_sturdy_openvpn'  # ~FC064, ~FC065
maintainer       'Sturdy Networks'
maintainer_email 'devops@sturdynetworks.com'
license          'All rights reserved'
description      'Provides a test customer recipe'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'sturdy_openvpn'
