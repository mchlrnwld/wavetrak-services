#
# Cookbook Name:: test_sturdy_openldap
# Recipe:: default
#
# Copyright 2017, Sturdy Networks
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'test_sturdy_openldap::_fix_tty' if node['platform'] == 'centos'

config_path = '/etc/openldap/slapd.d/cn=config'

package %w(openldap-servers openldap-clients)

unless ::File.exist?('/var/lib/ldap/DB_CONFIG')
  execute 'cp /usr/share/openldap-servers/DB_CONFIG.example '\
          '/var/lib/ldap/DB_CONFIG'
  execute 'chown ldap. /var/lib/ldap/DB_CONFIG'
end

service 'slapd' do
  action [:enable, :start]
end

unless ::File.exist?(::File.join(config_path, 'olcDatabase={0}config.ldif'))
  file "#{Chef::Config[:file_cache_path]}/password.ldif" do
    content "dn: olcDatabase={0}config,cn=config\n"\
            "changetype: modify\n"\
            "replace: olcRootPW\n"\
            "olcRootPW: #{node['openldap']['rootpw']}\n"
    owner 'root'
    group 'root'
    mode 00640
  end
  execute 'update_root_ldap_pass' do
    command 'ldapmodify -Y EXTERNAL -H ldapi:/// '\
            "-f #{Chef::Config[:file_cache_path]}/password.ldif"
  end
end

# Import basic Schemas
# (appears to have already been configured on Amazon Linux)
if node['platform'] == 'centos' &&
   !::File.exist?("#{config_path}/cn=schema/cn={1}cosine.ldif")
  %w(cosine nis inetorgperson).each do |v|
    execute "install_#{v}_schema" do
      command 'ldapadd -Y EXTERNAL -H ldapi:/// '\
              "-f /etc/openldap/schema/#{v}.ldif"
    end
  end
end

# Enable memberOf
# FIXME: fix Amazon Linux support (this shouldn't be excluding Amazon Linux)
# This is currently generating the following error output:
# SASL/EXTERNAL authentication started
# SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
# SASL SSF: 0
# ldap_add: No such object (32)
#      matched DN: cn=config
if node['platform'] == 'centos' &&
   !::File.exist?(::File.join(config_path,
                              'olcDatabase={2}hdb',
                              'olcOverlay={0}memberof.ldif'))
  file "#{Chef::Config[:file_cache_path]}/memberof_config.ldif" do
    content "dn: cn=module,cn=config\n"\
            "cn: module\n"\
            "objectClass: olcModuleList\n"\
            "objectclass: top\n"\
            "olcModuleLoad: memberof.la\n"\
            "olcModulePath: /usr/lib64/openldap\n"\
            "\n"\
            "dn: olcOverlay=memberof,olcDatabase={2}hdb,cn=config\n"\
            "objectclass: olcconfig\n"\
            "objectclass: olcMemberOf\n"\
            "objectclass: olcoverlayconfig\n"\
            "objectclass: top\n"\
            "olcoverlay: memberof\n"\
            "olcMemberOfDangling: ignore\n"\
            "olcMemberOfRefInt: TRUE\n"\
            "olcMemberOfGroupOC: groupOfNames\n"\
            "olcMemberOfMemberAD: member\n"\
            "olcMemberOfMemberOfAD: memberOf\n"
    owner 'root'
    group 'root'
    mode 00640
  end
  execute 'deploy_memberof_overlay' do
    command 'ldapadd -Y EXTERNAL -H ldapi:/// '\
            "-f #{Chef::Config[:file_cache_path]}/memberof_config.ldif"
  end
  file "#{Chef::Config[:file_cache_path]}/refint.ldif" do
    content "dn: cn=module,cn=config\n"\
            "cn: module\n"\
            "objectClass: olcModuleList\n"\
            "objectclass: top\n"\
            "olcModuleLoad: refint.la\n"\
            "olcModulePath: /usr/lib64/openldap\n"\
            "\n"\
            "dn: olcOverlay=refint,olcDatabase={2}hdb,cn=config\n"\
            "objectclass: olcconfig\n"\
            "objectclass: olcRefintConfig\n"\
            "objectclass: olcoverlayconfig\n"\
            "objectclass: top\n"\
            "olcoverlay: refint\n"\
            "olcRefintAttribute: memberof member manager owner\n"
    owner 'root'
    group 'root'
    mode 00640
  end
  execute 'deploy_refint_overlay' do
    command 'ldapadd -Y EXTERNAL -H ldapi:/// '\
            "-f #{Chef::Config[:file_cache_path]}/refint.ldif"
  end
end

# Setup domain
# FIXME: fix Amazon Linux support (this shouldn't be excluding Amazon Linux)
# After previous statements ^ broke the following haven't been tested
if node['platform'] == 'centos' &&
   !::File.exist?("#{config_path}/olcDatabase={2}hdb.ldif")
  template "#{Chef::Config[:file_cache_path]}/chdomain.ldif" do
    mode 0640
    variables(
      root_pw: node['openldap']['rootpw'],
      base_dn: node['openldap']['basedn']
    )
  end
  execute 'update_root_dn' do
    command 'ldapmodify -Y EXTERNAL -H ldapi:/// '\
            "-f #{Chef::Config[:file_cache_path]}/chdomain.ldif"
  end

  template "#{Chef::Config[:file_cache_path]}/basedomain.ldif" do
    mode 0640
    # dc will generate 'example' from 'dc=example,dc=com'
    variables(
      dc: node['openldap']['basedn'].split(',')[0].split('=').last,
      base_dn: node['openldap']['basedn']
    )
  end
  execute 'add_base_domain' do
    command "ldapadd -x -D cn=Manager,#{node['openldap']['basedn']} "\
            '-w secretsauce '\
            "-f #{Chef::Config[:file_cache_path]}/basedomain.ldif"
  end

  # Add 'unatco' group and 'jcdenton' member
  %w(user_jcdenton group_unatco).each do |t|
    template "#{Chef::Config[:file_cache_path]}/#{t}.ldif" do
      mode 0640
      variables(base_dn: node['openldap']['basedn'])
    end
  end
  # user pass is bionicman
  execute 'add_jcdenton_user' do
    command "ldapadd -x -D cn=Manager,#{node['openldap']['basedn']} "\
            '-w secretsauce '\
            "-f #{Chef::Config[:file_cache_path]}/user_jcdenton.ldif"
  end
  execute 'add_unatco_group' do
    command "ldapadd -x -D cn=Manager,#{node['openldap']['basedn']} "\
            '-w secretsauce '\
            "-f #{Chef::Config[:file_cache_path]}/group_unatco.ldif"
  end
end
