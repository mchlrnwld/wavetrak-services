require 'ipaddr'

# https://gist.github.com/nevans/1006716
class SturdyCIDRTranslation
  def self.unpack_ip(packed_ip)
    octets = []
    4.times do
      octet = packed_ip & 0xFF
      octets.unshift(octet.to_s)
      packed_ip = packed_ip >> 8
    end
    ip = octets.join('.')
  end

  def self.cidr_to_packed_ip(cidr)
    (("1"*cidr)+("0"*(32-cidr))).to_i(2)
  end

  def self.cidr_to_netmask(cidr)
    unpack_ip(cidr_to_packed_ip(Integer(cidr)))
  end

  def self.network_addr(ip_addr, cidr)
    packed_ip   = IPAddr.new(ip_addr).to_i
    packed_cidr = cidr_to_packed_ip(cidr)
    unpack_ip(packed_cidr & packed_ip)
  end
end
