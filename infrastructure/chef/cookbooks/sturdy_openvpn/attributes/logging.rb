default['sturdy']['openvpn']['service_status_metric_name'] =
  'service.status'
default['sturdy']['openvpn']['clients_connected_metric_name'] =
  'clients.connected'
