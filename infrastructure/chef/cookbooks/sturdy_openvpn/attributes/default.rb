# Static ldap auth strings
default['sturdy']['openvpn']['ldap_hostname'] =
  'satan.sturdynetworks.com'
default['sturdy']['openvpn']['ldap_url'] =
  "ldaps://#{node['sturdy']['openvpn']['ldap_hostname']}"
default['sturdy']['openvpn']['users_base_dn'] =
  'cn=users,cn=accounts,dc=us-east-1,dc=cloud,dc=sturdynetworks,dc=com'
default['sturdy']['openvpn']['groups_base_dn'] =
  'cn=groups,cn=accounts,dc=us-east-1,dc=cloud,dc=sturdynetworks,dc=com'
default['sturdy']['openvpn']['ldap_auth_search_string'] =
  '(&(uid=${username})(memberOf=cn=${vpngroup},${groupdn}))'

# Because it's 2X 1024, which has to be at least 8 times as good
default['openvpn']['key']['size'] = 2048
default['openvpn']['config']['dh'] =
  "#{node['openvpn']['key_dir']}/dh#{node['openvpn']['key']['size']}.pem"

# Not using custom network startup script
default['openvpn']['config']['up'] = nil

# Wait 12 hours before forcing client reconnection
default['openvpn']['config']['reneg-sec'] = '43200'

# This user is created automatically by the openvpn package on rhel systems
default['openvpn']['config']['user'] = 'openvpn' if %w(rhel amazon).include?(node['platform_family'])

# Our default subnet size
default['openvpn']['netmask'] = '255.255.255.0'

# Not authing with client certs
default['openvpn']['config']['crl-verify'] = nil
# Instead checking with ldap
unless ENV['TEST_KITCHEN']
  default['openvpn']['config']['auth-user-pass-verify'] = '/usr/local/bin/ldap_auth.sh via-env'
  default['openvpn']['config']['script-security'] = '3'
end
default['openvpn']['config']['client-cert-not-required'] = ''
default['openvpn']['config']['username-as-common-name'] = ''

# Use AES instead of the default insecure 64bit blowfish
default['openvpn']['config']['cipher'] = 'AES-256-CBC'

# Configure automatic updates
default['sturdy']['openvpn']['auto_update'] = true

# Allow inbound SSH and icmp through firewall (even if security group rules don't permit it)
default['firewall']['allow_icmp'] = true
default['firewall']['allow_ssh'] = true

# conditional forward rules don't work with ufw
default['firewall']['ubuntu_iptables'] = true
