# Stacks

## ElastiCache Parameter Group

### Dev

Hand created `redis-with-more-dbs`

### Prod

`sl-bestmodels-cache-parametergroup-prod` created from template surfline-bestmodels-elasticache-parameter-group.json with parameters:
* ApplicationName => bestmodels
* CompanyName => sl
* EnvironmentName	=> prod
* CacheParameterGroupFamily => redis2.8
* Databases => 24

## ElastiCache Replication Group

### Dev

`sl-bestmodels-redis-cache-dev` created from template surfline-elasticache-replication-group.json with parameters:
* ApplicationName => bestmodels
* AutomaticFailoverEnabled => false
* AutoMinorVersionUpgrade => true
* CacheEngineType => redis
* CacheNodeType => cache.m3.large
* CacheParameterGroupName => redis-with-more-dbs
* CacheSubnetGroup => rds-dbsubnet-group-sl-bestmodels-dev
* CompanyName => sl
* EnvironmentName	=> dev
* NumCacheClusters => 1
* PreferredMaintenanceWindow => mon:00:00-mon:01:00
* SnapshotArns => ""
* SnapshotRetentionLimit => 7
* SnapshotWindow	=> 02:00-03:00
* VpcId => vpc-981887fd

### Prod

`sl-bestmodels-redis-cache-prod` created from template surfline-elasticache-replication-group.json with parameters:
* AdditionalIngressSecurityGroup1 => ""
* ApplicationName => bestmodels
* AutomaticFailoverEnabled => true
* AutoMinorVersionUpgrade => true
* CacheEngineType => redis
* CacheNodeType => cache.m3.large
* CacheParameterGroupName => sl-be-cache-tprzuo6y4tuo (CacheParameterGroup output from `sl-bestmodels-cache-parametergroup-prod`)
* CacheSubnetGroup => sl-co-cache-jl0epcjm2il0
* CompanyName => sl
* EnvironmentName	=> prod
* NumCacheClusters => 2
* PreferredMaintenanceWindow => mon:00:00-mon:01:00
* SnapshotArns => ""
* SnapshotRetentionLimit => 7
* SnapshotWindow	=> 02:00-03:00
* VpcId => vpc-116fdb74
