# Surfline CMS Updater SNS Subscription

The template `surfline-cms-updater-sns-subscription.yml` is used to deploy the `sl-cms-updater-sns-subscription-prod` stack. This stack consists of a IAM Role, SNS Subscription, and a Lambda Permission to allow the SNS Subscription to execute the supplied lambda function.

### Prod

`sl-cms-updater-iam-role-prod` is created from template `surfline-cms-updater-iam-role.yml` with parameters:

Parameter       | Value
|:--------------|:------------------------
CustomerName    | sl
ApplicationName | cms-updater
EnvironmentName | prod
Bucketname      | surfline-science-s3-prod



`sl-cms-updater-sns-subscription-prod` created from template `surfline-cms-updater-sns-subscription.yml` with parameters:

Parameter     | Value
|:------------|:-----
SubscribedTopic | spot_details_updated_prod
LambdaFunction | python_get_best_wx
