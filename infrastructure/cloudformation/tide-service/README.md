# Tide Service

This template creates the AWS infrastructure required to support the Tide
Service Lambda function.


## Resources

* DynamoDB tables
* API Gateway in front of defined Lambda function
* IAM role for DynamoDB usage


## Parameters
|  Name                   | Description                                        | Type                        |
| :-------------          | :-------------                                     | :-------------              |
| EnvironmentName         | Name of environment in which resources will belong | String, AllowedValues       |
| LoggingLevel            | API Gateway to CloudWatch logs logging level (all methods) | String, AllowedValues |
| TableName               | Name of DynamoDB table to be create                | String                      |
| TableReadCapacityUnits  | Number of read capacity units to provision         | Number, MinValue / MaxValue |
| TableWriteCapacityUnits | Number of write capacity units to provision        | Number, MinValue / MaxValue |


## Outputs

| Name                      | Description                                    |
| :-------------            | :-------------                                 |
| ApiGatewayInvokeUrl       | URL to use for API Gateway function invocation |
| DynamoDBTableName         | Name of created DynamoDB table                 |
| MetadataDynamoDBTableName | Name of created DynamoDB metadata table        |
| IAMRoleARN                | IAM role ARN to use for DynamoDB table access  |


## Usage

The following example will launch the AWS infrastructure necessary for the
Tide Service in the logical environment "dev" using default values:

```shell
export ENV="dev"
aws cloudformation create-stack \
  --stack-name "sl-tide-service-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-tide-service.yaml' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}"
```


This example launches "prod" resources specifying additional read/write capacity
parameters:

```shell
export ENV="prod"
aws cloudformation create-stack \
  --stack-name "sl-tide-service-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-tide-service.yaml' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}" \
    "ParameterKey=TableReadCapacityUnits,ParameterValue=10" \
    "ParameterKey=TableWriteCapacityUnits,ParameterValue=10"

```


Here we will update the "prod" DynamoDB table to further increase read/write
capacity:

```shell
export ENV="prod"
aws cloudformation update-stack \
  --stack-name "sl-tide-service-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-tide-service.yaml' \
  --parameters \
    "ParameterKey=TableReadCapacityUnits,ParameterValue=20" \
    "ParameterKey=TableWriteCapacityUnits,ParameterValue=20"
```
