{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "RDS Infrastructure (2.3.4)",
  "Metadata": {
    "AWS::CloudFormation::Interface": {
      "ParameterGroups": [
        {
          "Label": {
            "default": "Application"
          },
          "Parameters": [
            "ApplicationName",
            "CompanyName",
            "EnvironmentName"
          ]
        },
        {
          "Label": {
            "default": "DB Admin Settings"
          },
          "Parameters": [
            "AllowMajorVersionUpgrade",
            "AutoMinorVersionUpgrade",
            "BackupRetentionDays",
            "DBPassword"
          ]
        },
        {
          "Label": {
            "default": "DB Instance Settings"
          },
          "Parameters": [
            "EnablePerformanceInsights",
            "RdsAllocatedStorage",
            "MaxAllocatedStorage",
            "RdsEngineType",
            "RdsInstanceClass",
            "RdsMultiAZ",
            "RdsParameterGroupName",
            "RdsStorageEncrypted"
          ]
        },
        {
          "Label": {
            "default": "Networking"
          },
          "Parameters": [
            "DBSubnetGroup",
            "VpcId"
          ]
        }
      ]
    }
  },
  "Parameters": {
    "AlertEmail": {
      "Description": "Emails address receiving notifications. (OPTIONAL: Adding email will create a Cloudwatch alarm.)",
      "Type": "String",
      "Default": ""
    },
    "AllowMajorVersionUpgrade": {
      "Description": "Enable automatic major version upgrades  - true or false",
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "true",
        "false"
      ]
    },
    "ApplicationName": {
      "Description": "Name of application database will be used for",
      "Type": "String"
    },
    "AutoMinorVersionUpgrade": {
      "Description": "Enable automatic minor version upgrades  - true or false",
      "Type": "String",
      "Default": "true",
      "AllowedValues": [
        "true",
        "false"
      ]
    },
    "BackupRetentionDays": {
      "Description": "Number of days to retain backups (1-35)",
      "Type": "Number",
      "Default": "7",
      "MinValue": "1",
      "MaxValue": "35"
    },
    "CompanyName": {
      "Description": "Company for the bucket (Surfline, Buoy Weather)",
      "Type": "String",
      "Default": "sl",
      "AllowedValues": [
        "bw",
        "ft",
        "sl"
      ]
    },
    "DBPassword": {
      "Description": "Password for the administrative user",
      "Type": "String",
      "NoEcho": "True"
    },
    "DBSubnetGroup": {
      "Description": "Physical ID of the DB Subnet Group",
      "Type": "String"
    },
    "EnablePerformanceInsights": {
      "Description": "Enable performance Insights for the DB instance",
      "Type": "String",
      "Default": "true",
      "AllowedValues": [
        "true",
        "false"
      ]
    },
    "EnvironmentName": {
      "Description": "Name of Environment",
      "Type": "String",
      "Default": "prod",
      "AllowedValues": [
        "common",
        "dev",
        "prod",
        "qa",
        "sbox",
        "staging",
        "testing"
      ]
    },
    "RdsAllocatedStorage": {
      "Description": "Storage to create for RDS databases (GB)",
      "Type": "Number",
      "Default": "90",
      "MinValue": "80",
      "MaxValue": "800"
    },
    "MaxAllocatedStorage": {
      "Description": "Upper limit to which Amazon RDS can automatically scale the storage of the DB instance.",
      "Type": "Number",
      "Default": "100",
      "MinValue": "80",
      "MaxValue": "800"
    },
    "RdsEngineType": {
      "Description": "Engine type to use for RDS instance",
      "Type": "String",
      "AllowedValues": [
        "mssql",
        "mysql",
        "postgres"
      ]
    },
    "RdsInstanceClass": {
      "Description": "Instance type to use for RDS instance",
      "Type": "String",
      "Default": "db.m3.large",
      "AllowedValues": [
        "db.m3.2xlarge",
        "db.m3.large",
        "db.m3.medium",
        "db.m3.xlarge",
        "db.m4.10xlarge",
        "db.m4.2xlarge",
        "db.m4.4xlarge",
        "db.m4.large",
        "db.m4.xlarge",
        "db.m5.xlarge",
        "db.m5.2xlarge",
        "db.r3.2xlarge",
        "db.r3.4xlarge",
        "db.r3.8xlarge",
        "db.r3.large",
        "db.r3.xlarge",
        "db.t2.large",
        "db.t2.medium",
        "db.t2.micro",
        "db.t2.small"
      ]
    },
    "RdsMultiAZ": {
      "Description": "MultiAZ - true or false",
      "Type": "String",
      "AllowedValues": [
        "true",
        "false"
      ]
    },
    "VpnSecurityGroup": {
      "Description": "Security group for vpn-servers",
      "Type": "String",
      "Default": "sg-82aeaee7"
    },
    "RdsParameterGroupName": {
      "Description": "Optional RDS parameter group name to use",
      "Type": "String"
    },
    "RdsStorageEncrypted": {
      "Description": "Encrypted Volume at rest - true or false",
      "Type": "String",
      "AllowedValues": [
        "true",
        "false"
      ]
    },
    "VpcId": {
      "Description": "VPC ID",
      "Type": "AWS::EC2::VPC::Id"
    },
    "CpuComparisonOperator": {
      "Description": "Comparison operator type",
      "Type": "String",
      "Default": "GreaterThanOrEqualToThreshold",
      "AllowedValues": [
        "GreaterThanOrEqualToThreshold",
        "GreaterThanThreshold",
        "LessThanThreshold",
        "LessThanOrEqualToThreshold"
      ]
    },
    "CpuEvaluationPeriods": {
      "Description": "Number of periods to evaluate with base threshold",
      "Type": "String",
      "Default": "2"
    },
    "CpuPeriod": {
      "Description": "Period of time to apply the statistics (in seconds)",
      "Type": "Number",
      "Default": "300",
      "AllowedValues": [
        "60",
        "300",
        "600",
        "900",
        "1800",
        "3600"
      ]
    },
    "CpuStatistic": {
      "Description": "Statistic type to apply to metric",
      "Type": "String",
      "Default": "Average",
      "AllowedValues": [
        "Average",
        "Minimum",
        "Maximum",
        "Sum",
        "SampleCount"
      ]
    },
    "CpuThreshold": {
      "Description": "Percentage threshold to alert",
      "Type": "Number",
      "Default": "95",
      "MinValue": "1",
      "MaxValue": "100"
    }
  },
  "Conditions": {
    "CustomParameterGroup": { "Fn::Not": [ { "Fn::Equals": [ { "Ref": "RdsParameterGroupName" }, "" ] } ] },
    "IsMicrosoftSqlServer": { "Fn::Equals": [ { "Ref": "RdsEngineType" }, "mssql" ] },
    "AlertEmail": { "Fn::Not" : [ { "Fn::Equals": [ { "Ref": "AlertEmail" }, "" ] } ] }
  },
  "Mappings": {
    "RdsMap": {
      "mssql": {
        "RdsEngine": "sqlserver-se",
        "RdsEngineVersion": "11.00.5058.0.v1",
        "RdsLicenseModel": "license-included",
        "RdsMasterUsername": "sa",
        "RdsParameterGroupName": "default.sqlserver-se-11.0",
        "RdsPort": "1433"
      },
      "mysql": {
        "RdsEngine": "mysql",
        "RdsEngineVersion": "5.7",
        "RdsLicenseModel": "general-public-license",
        "RdsMasterUsername": "root",
        "RdsParameterGroupName": "default.mysql5.7",
        "RdsPort": "3306"
      },
      "postgres": {
        "RdsEngine": "postgres",
        "RdsEngineVersion": "9.4",
        "RdsLicenseModel": "postgresql-license",
        "RdsMasterUsername": "postgres",
        "RdsParameterGroupName": "default.postgres9.4",
        "RdsPort": "5432"
      }
    }
  },
  "Resources": {
    "RdsClientSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": { "Fn::Join": [ "-", [ { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, "-RdsClientSecurityGroup", { "Ref": "EnvironmentName" } ] ] },
        "Tags": [
          {
            "Key": "Application",
            "Value": { "Ref": "ApplicationName" }
          },
          {
            "Key": "Company",
            "Value": { "Ref": "CompanyName" }
          },
          {
            "Key": "Environment",
            "Value": { "Ref": "EnvironmentName" }
          },
          {
            "Key": "Name",
            "Value": { "Fn::Join": [ "-", [ "rds-clients", { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, { "Ref": "EnvironmentName" } ] ] }
          }
        ],
        "VpcId": { "Ref": "VpcId" },
        "SecurityGroupEgress": [
          { "IpProtocol": "-1", "FromPort": "0", "ToPort": "65535", "CidrIp": "0.0.0.0/0" }
        ]
      }
    },
    "RdsServerSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": { "Fn::Join": [ "-", [ { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, "-RdsServerSecurityGroup", { "Ref": "EnvironmentName" } ] ] },
        "Tags": [
          {
            "Key": "Application",
            "Value": { "Ref": "ApplicationName" }
          },
          {
            "Key": "Company",
            "Value": { "Ref": "CompanyName" }
          },
          {
            "Key": "Environment",
            "Value": { "Ref": "EnvironmentName" }
          },
          {
            "Key": "Name",
            "Value": { "Fn::Join": [ "-", [ "rds-server", { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, { "Ref": "EnvironmentName" } ] ] }
          }
        ],
        "VpcId": { "Ref": "VpcId" },
        "SecurityGroupIngress": [
          { "IpProtocol": "tcp", "FromPort": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsPort" ] },  "ToPort": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsPort" ] },  "SourceSecurityGroupId": { "Ref": "RdsClientSecurityGroup" } },
          { "IpProtocol": "tcp", "FromPort": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsPort" ] },  "ToPort": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsPort" ] },  "SourceSecurityGroupId": { "Ref": "VpnSecurityGroup" } }
         ],
        "SecurityGroupEgress": [
          { "IpProtocol": "-1", "FromPort": "0", "ToPort": "65535", "CidrIp": "0.0.0.0/0" }
        ]
      },
      "DependsOn": [ "RdsClientSecurityGroup" ]
    },
    "RdsDatabaseInstance": {
      "Type": "AWS::RDS::DBInstance",
      "Properties": {
        "AllocatedStorage": { "Ref": "RdsAllocatedStorage" },
        "MaxAllocatedStorage": { "Ref": "MaxAllocatedStorage" },
        "AllowMajorVersionUpgrade": { "Ref": "AllowMajorVersionUpgrade" },
        "AutoMinorVersionUpgrade": { "Ref": "AutoMinorVersionUpgrade" },
        "DBInstanceClass": { "Ref": "RdsInstanceClass" },
        "DBInstanceIdentifier": { "Fn::Join": [ "-", [ { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, { "Ref": "EnvironmentName" } ] ] },
        "DBParameterGroupName": {
          "Fn::If": [
            "CustomParameterGroup",
            { "Ref": "RdsParameterGroupName" },
            { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsParameterGroupName" ] }
          ]
        },
        "BackupRetentionPeriod": { "Ref": "BackupRetentionDays" },
        "EnablePerformanceInsights": { "Ref": "EnablePerformanceInsights" },
        "Engine": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsEngine" ] },
        "Port": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsPort" ] },
        "LicenseModel": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsLicenseModel" ] },
        "EngineVersion": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsEngineVersion" ] },
        "DBSubnetGroupName": { "Ref": "DBSubnetGroup" },
        "VPCSecurityGroups": [ { "Ref": "RdsServerSecurityGroup" } ],
        "MasterUsername": { "Fn::FindInMap": [ "RdsMap", { "Ref": "RdsEngineType" }, "RdsMasterUsername" ] },
        "MasterUserPassword": { "Ref": "DBPassword" },
        "PreferredBackupWindow": "03:00-04:00",
        "PreferredMaintenanceWindow": "sat:06:00-sat:07:00",
        "MultiAZ": {
          "Fn::If": [
            "IsMicrosoftSqlServer",
            { "Ref": "AWS::NoValue" },
            { "Ref": "RdsMultiAZ" }
          ]
        },
        "StorageEncrypted": { "Ref": "RdsStorageEncrypted" },
        "StorageType": "gp2",
        "Tags": [
          {
            "Key": "Application",
            "Value": { "Ref": "ApplicationName" }
          },
          {
            "Key": "Company",
            "Value": { "Ref": "CompanyName" }
          },
          {
            "Key": "Environment",
            "Value": { "Ref": "EnvironmentName" }
          },
          {
            "Key": "Name",
            "Value": { "Fn::Join": [ "-", [ { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, { "Ref": "EnvironmentName" } ] ] }
          }
        ]
      },
      "DependsOn": [ "RdsServerSecurityGroup" ]
    },
    "SNSTopic": {
      "Type" : "AWS::SNS::Topic",
      "Condition": "AlertEmail",
      "Properties" : {
        "DisplayName" : { "Fn::Join" : [ "-", [ { "Ref": "CompanyName" }, { "Ref": "ApplicationName" }, { "Ref": "EnvironmentName" }, "CPUUtilization", "topic" ] ] },
        "Tags": [
          {
            "Key": "Company",
            "Value": { "Ref": "CompanyName" }
          },
          {
            "Key": "Application",
            "Value": { "Ref": "ApplicationName" }
          },
          {
            "Key": "Environment",
            "Value": { "Ref": "EnvironmentName" }
          }
        ],
        "TopicName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "CPUUtilization", "topic" ] ] }
      }
    },
    "HighCPUAlarm": {
      "Type": "AWS::CloudWatch::Alarm",
      "Condition": "AlertEmail",
      "Properties": {
        "ActionsEnabled": "true",
        "AlarmActions": [
          {
            "Ref": "SNSTopic"
          }
        ],
        "AlarmDescription": "CPUUtilization Alarm for RDS",
        "AlarmName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" },  "CPUUtilization", "alarm" ] ] },
        "ComparisonOperator": { "Ref": "CpuComparisonOperator" },
        "Dimensions": [
          {
            "Name": "DBInstanceIdentifier",
            "Value": { "Ref": "RdsDatabaseInstance" }
          }
        ],
        "EvaluationPeriods": { "Ref": "CpuEvaluationPeriods" },
        "MetricName": "CPUUtilization",
        "Namespace": "AWS/RDS",
        "Period": { "Ref": "CpuPeriod" },
        "Statistic": { "Ref": "CpuStatistic" },
        "Threshold": { "Ref": "CpuThreshold" }
      }
    }
  },
  "Outputs": {
    "RdsDatabaseInstance": {
      "Description": "DNS name of the RDS instance",
      "Value": { "Fn::GetAtt": [ "RdsDatabaseInstance", "Endpoint.Address" ] }
    },
    "RdsClientSecurityGroup": {
      "Description": "ID of the RDS client security group",
      "Value": { "Ref": "RdsClientSecurityGroup" }
    }
  }
}
