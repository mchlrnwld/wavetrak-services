# Best Lookup Redis Cluster

These templates create the AWS infrastructure required to support the Redis Best
Lookup Cache Cluster.

Elasticache bestlookup redis cluster `sl-bestlookup-redis-cache-prod` is deployed via
the `surfline-elasticache-replication-group-prod` stack from template
`surfline-elasticache-replication-group.json`. This stack requires 2x previous
Cloudformation templates to have been run.

- `./surfline-elasticache-subnet-group.json`
- `./bestlookup/surfline-elasticache-parameter-group.yaml`

----

## 1. Best Lookup Redis Parameter Group Template

### Parameters

|  Name                     | Description                                                  | Type                  |
| :------------------------ | :----------------------------------------------------------- | :-------------------- |
| ApplicationName           | Name of application cache will be used for                   | String                |
| CompanyName               | Company (Surfline, Buoy Weather)                             | String, AllowedValues |
| EnvironmentName           | Name of environment                                          | String, AllowedValues |
| CacheParameterGroupFamily | The name of the cache parameter group family                 | String, AllowedValues |
| Databases                 | The number of logical partitions the databases is split into | String, AllowedValues |


### Resources

* ElastiCache Parameter Group


## Outputs

| Name                      | Description                                     |
| :-------------            | :-------------                                  |
| CacheParameterGroup       | Physical ID of the ElastiCache Parameter Group  |


## Usage

The following example will launch the AWS infrastructure necessary for the
Best Lookup Elasticache Subnet Group in the logical environment "dev" using
default values:

```shell
export ENV="dev"
aws cloudformation create-stack \
  --stack-name "sl-bestlookup-cache-parametergroup-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-elasticache-parameter-group.yaml' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=ApplicationName,ParameterValue=bestlookup" \
    "ParameterKey=CompanyName,ParameterValue=sl" \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}"
```


This example launches "prod" using default values:

```shell
export ENV="prod"
aws cloudformation create-stack \
  --stack-name "sl-bestlookup-cache-parametergroup-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://bestlookup/surfline-elasticache-parameter-group.yaml' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=ApplicationName,ParameterValue=bestlookup" \
    "ParameterKey=CompanyName,ParameterValue=sl" \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}"
```

----

## 2. Best Lookup Redis Subnet Group Template

### Parameters

|  Name           | Description                                   | Type                  |
| :-------------- | :-------------------------------------------- | :-------------------- |
| ApplicationName | Name of application cache will be used for    | String                |
| CompanyName     | Company (Surfline, Buoy Weather)              | String, AllowedValues |
| EnvironmentName | Name of environment                           | String, AllowedValues |
| CacheSubnets    | ID of the subnets associated with the VPC     | String, AllowedValues |
| VpcId           | ID of the VPC associated with the environment | String, AllowedValues |


### Resources

* ElastiCache Subnet Group


## Outputs

| Name             | Description                                     |
| :--------------- | :---------------------------------------------- |
| CacheSubnetGroup | Physical ID of the ElastiCache Parameter Group  |



The following example will launch the AWS infrastructure necessary for the
Best Lookup Elasticache Subnet Group in the logical environment "dev" using
default values:

```shell
export ENV="dev"
export CACHE_SUBNET_GROUP="sg-XYZ" # Use the CacheSubnetGroup Output from above
export VPC_ID="vpc-XYZ" # Use the VPC-ID for the VPC this will be deployed into
aws cloudformation create-stack \
  --stack-name "sl-bestlookup-cache-subnetgroup-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-elasticache-subnet-group.json' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=ApplicationName,ParameterValue=bestlookup" \
    "ParameterKey=CompanyName,ParameterValue=sl" \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}" \
    "ParameterKey=CacheSubnets,ParameterValue=${CACHE_SUBNET_GROUP}" \
    "ParameterKey=VpcId,ParameterValue=${VPC_ID}"
```


This example launches "prod" using default values:

```shell
export ENV="prod"
export CACHE_SUBNET_GROUP="" # Use the CacheSubnetGroup Output from above
export VPC_ID="vpc-XYZ" # Use the VPC-ID for the VPC this will be deployed into
aws cloudformation create-stack \
  --stack-name "sl-bestlookup-cache-subnetgroup-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-elasticache-subnet-group.json' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=ApplicationName,ParameterValue=bestlookup" \
    "ParameterKey=CompanyName,ParameterValue=sl" \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}" \
    "ParameterKey=CacheSubnets,ParameterValue=${CACHE_SUBNET_GROUP}" \
    "ParameterKey=VpcId,ParameterValue=${VPC_ID}"
```




## 3. Best Lookup Redis Replication Group Template

### Parameters

|  Name                           | Description                                   | Type                  |
| :------------------------------ | :-------------------------------------------- | :-------------------- |
| AutomaticFailoverEnabled        | Enable automatic failover - true or false     |                       |
| AutoMinorVersionUpgrade         | Enable automatic minor version upgrades       |                       |
| CacheEngineType                 | Engine type to use for cache instance         |                       |
| CacheNodeType                   | Instance type to use for Redis instance       |                       |
| CacheParameterGroupName         | Optional Redis parameter group name to use    |                       |
| CacheSubnetGroup                | Physical ID of the Cache Subnet Group         |                       |
| NumCacheClusters                | Number of cache clusters in replication group |                       |
| PreferredMaintenanceWindow      | Weekly system maintenance window (24H UTC)    |                       |
| SnapshotArns                    | ARN of RDB file used to populate cluster      |                       |
| SnapshotRetentionLimit          | Number of days to retain snapshots (1-35)     |                       |
| SnapshotWindow                  | Daily snapshot window (24H UTC)               |                       |
| ApplicationName                 | Name of application cache will be used for    | String                |
| CompanyName                     | Company (Surfline, Buoy Weather)              | String, AllowedValues |
| EnvironmentName                 | Name of environment                           | String, AllowedValues |
| CacheSubnets                    | ID of the subnets associated with the VPC     | String, AllowedValues |
| VpcId                           | ID of the VPC associated with the environment | String, AllowedValues |


### Resources

* ElastiCache Security Groups
* ElastiCache Replication Group


## Outputs

| Name                               | Description                             |
| :--------------------------------- | :-------------------------------------- |
| ElastiCacheReplicationGroupPrimary | DNS name of the primary read-write node |
| CacheClientSecurityGroup           | ID of the cache client security group   |


The following example will launch the AWS infrastructure necessary for the
Best Lookup Elasticache Replicaiton Group in the logical environment "dev" using
default values:

```shell
export ENV="dev"
export CACHE_SUBNET_GROUP="sg-XYZ" # Use the CacheSubnetGroup Output from above
export VPC_ID="vpc-XYZ" # Use the VPC-ID for the VPC this will be deployed into
aws cloudformation create-stack \
  --stack-name "sl-bestlookup-redis-cache-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-elasticache-replication-group.json' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=ApplicationName,ParameterValue=bestlookup" \
    "ParameterKey=CompanyName,ParameterValue=sl" \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}" \
    "ParameterKey=CacheSubnets,ParameterValue=${CACHE_SUBNET_GROUP}" \
    "ParameterKey=VpcId,ParameterValue=${VPC_ID}"
```

The following example will launch the AWS infrastructure necessary for the
Best Lookup Elasticache Replicaiton Group in the logical environment "prod" using
default values:

```shell
export ENV="prod"
export CACHE_SUBNET_GROUP="" # Use the CacheSubnetGroup Output from above
export VPC_ID="vpc-XYZ" # Use the VPC-ID for the VPC this will be deployed into
aws cloudformation create-stack \
  --stack-name "sl-bestlookup-redis-cache-${ENV}" \
  --capabilities 'CAPABILITY_IAM' \
  --template-body 'file://surfline-elasticache-replication-group.json' \
  --tags "Key=EnvironmentName,Value=${ENV}" \
  --parameters \
    "ParameterKey=ApplicationName,ParameterValue=bestlookup" \
    "ParameterKey=CompanyName,ParameterValue=sl" \
    "ParameterKey=EnvironmentName,ParameterValue=${ENV}" \
    "ParameterKey=CacheSubnets,ParameterValue=${CACHE_SUBNET_GROUP}" \
    "ParameterKey=VpcId,ParameterValue=${VPC_ID}" \
```
