#!/usr/bin/env bash

# This script will remove a stack.

USAGE=$(cat <<-END
Usage:
  remove_stack.sh <type> <region> <environment>
Examples:
  remove_tack.sh rec wc test
END
)

# Is the user requesting help?
if [[ "$1" = "-h" ]]; then
  echo "${USAGE}"
  exit
fi

# Did the user specify the type
if [[ -n "$1" ]]; then
  SERVER_GROUP=$1
else
  echo '[ERROR] You must specify  the type: distribution, recorder, transcoder'
  echo
  echo "${USAGE}"
  exit 1
fi

# Did the user specify the region
if [[ -n "$2" ]]; then
  SERVER_GROUP=$2
else
  echo '[ERROR] You must specify the region (if rec/dist: wc, ec) or number (if trans:1-5) '
  echo
  echo "${USAGE}"
  exit 1
fi

# Did the user specify the environment
if [[ -n "$3" ]]; then
  SERVER_GROUP=$3
else
  echo '[ERROR] You must specify the environment: prod, staging, test'
  echo
  echo "${USAGE}"
  exit 1
fi


type=$1
reg=$2
env=$3

aws cloudformation delete-stack --stack-name "surfline-wowza-${type}-${instance}-${env}"
