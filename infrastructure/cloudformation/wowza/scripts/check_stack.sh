#!/usr/bin/env bash

# This script will update a stack.

USAGE=$(cat <<-END
Usage:
  update_stack.sh <type> <region> <environment>
Examples:
  update_tack.sh rec wc test
END
)

# Is the user requesting help?
if [[ "$1" = "-h" ]]; then
  echo "${USAGE}"
  exit
fi

# Did the user specify the type
if [[ -n "$1" ]]; then
  SERVER_GROUP=$1
else
  echo '[ERROR] You must specify  the type: distribution, recorder, transcoder'
  echo
  echo "${USAGE}"
  exit 1
fi

# Did the user specify the region
if [[ -n "$2" ]]; then
  SERVER_GROUP=$2
else
  echo '[ERROR] You must specify the region (if rec/dist: wc, ec) or number (if trans:1-5) '
  echo
  echo "${USAGE}"
  exit 1
fi

# Did the user specify the environment
if [[ -n "$3" ]]; then
  SERVER_GROUP=$3
else
  echo '[ERROR] You must specify the environment: prod, staging, test'
  echo
  echo "${USAGE}"
  exit 1
fi


type=$1
reg=$2
env=$3

aws cloudformation update-stack                                                          \
    --stack-name "surfline-wowza-${type}-${instance}-${env}"                          \
    --capabilities "CAPABILITY_NAMED_IAM"                                                \
    --template-body "file://../surfline-wowza-${type}.yaml"                              \
    --tags "Key=EnvironmentName,Value=legacy"                                            \
    --parameters                                                                         \
      "ParameterKey=AmiId,                         ParameterValue=ami-bff7e3df"          \
      "ParameterKey=InternalServerSecurityGroupId, ParameterValue=sg-6d0d070f"           \
      "ParameterKey=CFLBSecurityGroup,             ParameterValue=sg-aea9afc9"           \
      "ParameterKey=KeyName,                       ParameterValue=surfline-wowza-wc"     \
      "ParameterKey=PubSubnet1,                    ParameterValue=subnet-68a1af0a"       \
      "ParameterKey=PubSubnet2,                    ParameterValue=subnet-0e226848"       \
      "ParameterKey=ServiceInstance,               ParameterValue=${instance}"           \
      "ParameterKey=VpcId,                         ParameterValue=vpc-29f3f241"          \
      "ParameterKey=PreExistingS3BackupBucket,     ParameterValue=sl-wowza-${type}-${instance}-${env}-backup"



aws cloudformation describe-stack-events --stack-name "surfline-wowza-${type}-${instance}-${env}"

