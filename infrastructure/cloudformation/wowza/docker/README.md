# Wowza Docker Dev Tools

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Getting Started](#getting-started)
  - [Dependencies](#dependencies)
    - [`ffmpeg`](#ffmpeg)
    - [VLC](#vlc)
  - [Run Wowza](#run-wowza)
- [Configure Wowza](#configure-wowza)
  - [Conf Files](#conf-files)
  - [Web Interface](#web-interface)
  - [REST API](#rest-api)
    - [Documentation](#documentation)
    - [Sample Request](#sample-request)
      - [Get Stream Files](#get-stream-files)
- [Stream Local Files](#stream-local-files)
  - [Download Sample](#download-sample)
  - [Stream Sample To Wowza](#stream-sample-to-wowza)
  - [Probe Streams](#probe-streams)
    - [RTSP](#rtsp)
    - [HLS](#hls)
  - [View Streams](#view-streams)
    - [RTSP](#rtsp-1)
    - [HLS](#hls-1)
- [Restream Remote Streams](#restream-remote-streams)
  - [Add Remote Stream to Wowza](#add-remote-stream-to-wowza)
  - [Probe Streams](#probe-streams-1)
    - [RTSP](#rtsp-2)
    - [HLS](#hls-2)
  - [View Streams](#view-streams-1)
    - [RTSP](#rtsp-3)
    - [HLS](#hls-3)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Started

### Dependencies

#### `ffmpeg`

```sh
brew install ffmpeg
```

#### VLC

<https://www.videolan.org/vlc/download-macosx.html>

### Run Wowza

```sh
# leave this running
make start
```

## Configure Wowza

### Conf Files

Wowza config files are included in this repo in the `./conf` subdirectory. You may edit these files and restart Wowza for your changes to take effect.

### Web Interface

<http://localhost:8088/>
username: `w`
password: `t`

### REST API

#### Documentation

- [Wowza Streaming Engine REST API Examples](https://www.wowza.com/docs/wowza-streaming-engine-rest-api-query-examples-curl)
- [Stream Management Query Examples](https://www.wowza.com/docs/stream-management-query-examples)
- [Stream Recording Query Examples](https://www.wowza.com/docs/stream-recording-query-examples)
- [Stream Target Query Examples](https://www.wowza.com/docs/stream-targets-query-examples-push-publishing)

#### Sample Request

##### Get Stream Files

```bash
curl \
  --user w:t \
  --digest \
  -X GET \
  --header 'Accept:application/json; charset=utf-8' \
  --header 'Content-type:application/json; charset=utf-8' \
  http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/streamfiles

# =>

{
  "serverName": "_defaultServer_",
  "streamFiles": [
    {
      "id": "test-stream-from-wowza",
      "href": "/v2/servers/_defaultServer_/vhosts/_defaultVHost_/streamfiles/test-stream-from-wowza"
    }
  ]
}
```

## Stream Local Files

### Download Sample

This downloads a sample Surfline cam rewind that can be streamed into Wowza.

```sh
make get-sample
```

### Stream Sample To Wowza

This uses `ffmpeg` to create an RTSP stream which is ingested and restreamed by Wowza.

```sh
# leave this running, restart when it finishes
make stream-cam-sample-file-to-wowza

# => rtsp://localhost:554/live/sl-cam-rewind-sample
```

### Probe Streams

#### RTSP

```sh
make open-cam-rtsp-stream-in-probe
```

#### HLS

```sh
make open-cam-hls-stream-in-probe
```

### View Streams

#### RTSP

```sh
make open-cam-rtsp-stream-in-vlc
```

#### HLS

```sh
make open-cam-hls-stream-in-vlc
```

## Restream Remote Streams

### Add Remote Stream to Wowza

This adds a sample stream from Wowza's CDN to the local Wowza instance and connects to it.

```sh
# leave this running, restart when it finishes
make add-remote-rtsp-stream-to-wowza

# => rtsp://localhost:554/live/test-stream-from-wowza.stream
```

### Probe Streams

#### RTSP

```sh
make open-remote-rtsp-stream-in-probe
```

#### HLS

```sh
make open-remote-hls-stream-in-probe
```

### View Streams

#### RTSP

```sh
make open-remote-rtsp-stream-in-vlc
```

#### HLS

```sh
make open-remote-hls-stream-in-vlc
```
