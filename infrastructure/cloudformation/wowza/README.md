# Surfline Wowza Servers

This directory contains templates and scripts to deploy, update and remove the following Wowza servers at the AWS infrastructure:

* Wowza distribution
* Wowza transcoder
* Wowza recorder


### Launching a new  stack

To create a new stack, run:

```
create_stack.sh <type> <region> <environment>
```

Example:
```
create_stack.sh recorder wc prod
```


If the S3 backup bucket already exists, add the following parameter:

```
ParameterKey=PreExistingS3BackupBucket, ParameterValue=sl-wowza-transcoder-${instance}-prod-backup
```

or the stack creation will fail.


### Installing Wowza plugins

Distribution instances require the installation of the follow plugin:

* [ModuleRTPWrapper](https://github.com/Surfline/surfline-services/services/cameras-service/wowza-plugin/rtcp-encoder/).

Recorder instances require the installation of the follow plugin:

* [ModuleS3Upload](https://github.com/Surfline/surfline-services/services/cameras-service/wowza-plugin/rtcp-s3-recorder/).


### Checking the stack status

```
check_stack.sh <type> <region> <environment>
```

Example:
```
check_stack.sh transcoder 3 test
```


### Updating an existing stack

To update a stack with a modified cloudformation template, run:

```
update_stack.sh <type> <region> <environment>
```

Example:
```
update_stack.sh transcoder 1 prod
```


### Removing an existing stack

To remove stack, run:

```
remove_stack.sh  <type> <region> <environment>
```

Example:
```
remove_stack.sh distribution ec staging
```


Note: you will mostly not have to remove a stack (unless it is a testing stack), in general we want to just update it with the new changes in its templates.

