#!/usr/bin/env bash

# This script will launch a new encoder stack.

USAGE=$(cat <<-END
Usage:
  deploy.sh <type> <region> <environment>
Examples:
  deploy.sh recorder wc test
END
)

# Is the user requesting help?
if [[ "$1" = "-h" ]]; then
  echo "${USAGE}"
  exit
fi

# Did the user specify the type
if [[ -n "$1" ]]; then
  SERVER_GROUP=$1
else
  echo '[ERROR] You must specify the type: distribution, recorder, transcoder'
  echo
  echo "${USAGE}"
  exit 1
fi

# Did the user specify the region
if [[ -n "$2" ]]; then
  SERVER_GROUP=$2
else
  echo '[ERROR] You must specify the region (if rec/dist: wc, ec) or number (if trans:1-5) '
  echo
  echo "${USAGE}"
  exit 1
fi

# Did the user specify the environment
if [[ -n "$3" ]]; then
  SERVER_GROUP=$3
else
  echo '[ERROR] You must specify the environment: prod, staging, test'
  echo
  echo "${USAGE}"
  exit 1
fi

type=$1
reg=$2
env=$3

# Set the instance type based on the type and the env
# TODO: Allow this to be passed as an arg
if [[ "${type}" = "distribution" ]]; then
  if [[ "${env}" = "prod" ]]; then
    instancetype=i3.2xlarge
  else
    instancetype=t2.medium
  fi
fi

if [[ "${type}" = "recorder" ]]; then
  if [[ "${env}" = "prod" ]]; then
    instancetype=r3.xlarge
  else
    instancetype=t2.small
  fi
fi

if [[ "${type}" = "transcoder" ]]; then
  instancetype=g3.4xlarge
fi

# If you are spinning an instance that already contains a S3 bucket, add the following parameter:
# "ParameterKey=PreExistingS3BackupBucket, ParameterValue=sl-wowza-${type}-${reg}-${env}-backup"

aws cloudformation create-stack                                                          \
    --stack-name "surfline-wowza-${type}-${reg}-${env}"                                  \
    --capabilities "CAPABILITY_NAMED_IAM"                                                \
    --template-body "file://surfline-wowza-${type}.yaml"                                 \
    --tags "Key=EnvironmentName,Value=legacy"                                            \
    --parameters                                                                         \
      "ParameterKey=AmiId,                         ParameterValue=ami-bff7e3df"          \
      "ParameterKey=InternalServerSecurityGroupId, ParameterValue=sg-6d0d070f"           \
      "ParameterKey=KeyName,                       ParameterValue=surfline-wowza-wc"     \
      "ParameterKey=PubSubnet1,                    ParameterValue=subnet-68a1af0a"       \
      "ParameterKey=PubSubnet2,                    ParameterValue=subnet-0e226848"       \
      "ParameterKey=VpcId,                         ParameterValue=vpc-29f3f241"          \
      "ParameterKey=ServiceRegion,                 ParameterValue=${reg}"                \
      "ParameterKey=EnvironmentName,               ParameterValue=${env}"                \
      "ParameterKey=InstanceType,                  ParameterValue=${instancetype}"
