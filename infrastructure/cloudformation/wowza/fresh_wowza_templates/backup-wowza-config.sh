#!/bin/sh

# Backup Wowza configuration per the following instructions:
# https://www.wowza.com/docs/how-to-migrate-wowza-streaming-engine-to-a-different-server

set -ex

WOWZA_HOME=/usr/local/WowzaStreamingEngine
# S3_BUCKET=${S3BackupBucketRef}
S3_BUCKET=sl-wowza-recorder-int-prod-backup

# [install-dir]/applications: All application folders.
aws s3 sync "${WOWZA_HOME}/applications/" "s3://${S3_BUCKET}/applications/"

# [install-dir]/conf: Server configuration files.
# Important: Do not copy the [install-dir]/conf/jmxremote.access file from your backup.
aws s3 sync "${WOWZA_HOME}/conf/" "s3://${S3_BUCKET}/conf/" --exclude "jmxremote.access"

# [install-dir]/content: On-demand video files, caption files, stream files, and so on.
aws s3 sync "${WOWZA_HOME}/content/" "s3://${S3_BUCKET}/content/"

# [install-dir]/lib: Important: Copy only your customized module (.jar) files to the new instance.
aws s3 sync "${WOWZA_HOME}/lib/" "s3://${S3_BUCKET}/lib/" \
    --exclude "*" \
    --include "aws-java-sdk-1.11.438.jar" \
    --include "wowza-rtptime-segmenting-*.jar" \
    --include "wse-plugin-autorecord.jar" \
    --include "wse-plugin-s3upload.jar"

# [install-dir]/manager/conf: Wowza Streaming Engine Manager configuration files.
aws s3 sync "${WOWZA_HOME}/manager/conf/" "s3://${S3_BUCKET}/manager/conf/"

# [install-dir]/transcoder:
aws s3 sync "${WOWZA_HOME}/transcoder/" "s3://${S3_BUCKET}/transcoder/"
