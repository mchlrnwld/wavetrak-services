{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "Buoyweather MongoDB Stack (3.0.2)",
  "Parameters" : {
    "AlertSubscriptionEmail": {
      "Description": "Email to receive instance events",
      "Type": "String"
    },
    "ApplicationName" : {
      "Description" : "Name of application database will be used for",
      "Type" : "String"
    },
    "BackupBucketName" : {
      "Description" : "Name of bucket storing backup data",
      "Type" : "String"
    },
    "ChefBucketName" : {
      "Description" : "Name of bucket storing Chef configuration",
      "Type" : "String"
    },
    "CompanyName" : {
      "Description" : "Company (Buoyweather)",
      "Type" : "String",
      "Default" : "bw",
      "AllowedValues" : [
        "bw"
      ]
    },
    "DeregTopic": {
      "Description": "The ARN of the DeregTopic",
      "Type": "String",
      "Default": "arn:aws:sns:us-west-1:833713747344:Sturdy-SAND"
    },
    "EbsStorageCapacityData" : {
      "Description" : "EBS storage capacity (GB) - Data",
      "Type" : "Number",
      "Default" : "50",
      "MinValue" : "10",
      "MaxValue" : "1000"
    },
    "EbsStorageCapacityJournal" : {
      "Description" : "EBS storage capacity (GB) - Journal",
      "Type" : "Number",
      "Default" : "25",
      "MinValue" : "10",
      "MaxValue" : "1000"
    },
    "EbsStorageCapacityLog" : {
      "Description" : "EBS storage capacity (GB) - Log",
      "Type" : "Number",
      "Default" : "25",
      "MinValue" : "10",
      "MaxValue" : "1000"
    },
    "EbsStorageIopsData" : {
      "Description" : "EBS storage IOPS (GB * 30 minimum) - Data",
      "Type" : "Number",
      "Default" : "1500",
      "MinValue" : "300",
      "MaxValue" : "20000"
    },
    "EbsStorageIopsJournal" : {
      "Description" : "EBS storage capacity IOPS (GB * 30 minimum) - Journal",
      "Type" : "Number",
      "Default" : "750",
      "MinValue" : "300",
      "MaxValue" : "20000"
    },
    "EbsStorageIopsLog" : {
      "Description" : "EBS storage capacity IOPS (GB * 30 minimum) - Log",
      "Type" : "Number",
      "Default" : "750",
      "MinValue" : "300",
      "MaxValue" : "20000"
    },
    "EnvironmentName" : {
      "Description" : "Name of Environment",
      "Type" : "String",
      "Default" : "prod",
      "AllowedValues" : [
        "prod",
        "qa"
      ]
    },
    "InternalServerSecurityGroupId" : {
      "Description" : "The security group used for internal servers",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "InstanceType" : {
      "Description" : "Type of instance(s). Selection limited to EBS optimized types.",
      "Type" : "String",
      "Default" : "m4.large",
      "AllowedValues" : [
        "c3.xlarge",
        "c3.2xlarge",
        "c3.4xlarge",
        "c4.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "d2.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "i2.xlarge",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "m3.xlarge",
        "m3.2xlarge",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge",
        "r3.large",
        "r3.xlarge",
        "r3.2xlarge",
        "r3.4xlarge"
      ]
    },
    "KeyName" : {
      "Description" : "Name of an existing EC2-VPC KeyPair",
      "Type" : "AWS::EC2::KeyPair::KeyName"
    },
    "MongoVersion" : {
      "Description" : "Version of MongoDB to install",
      "Type": "String",
      "AllowedValues" : [
        "2.6",
        "3.0"
      ]
    },
    "PriSubnet1" : {
      "Description" : "Private subnet 1",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "PriSubnet2" : {
      "Description" : "Private subnet 2",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "ServiceName" : {
      "Description" : "Service name",
      "Type" : "String",
      "Default" : "mongodb"
    },
    "ServerCount" : {
      "Description" : "Number of MongoDB instances to start",
      "Type" : "Number",
      "Default" : "1",
      "MinValue" : "0",
      "MaxValue" : "6"
    },
    "VpcId" : {
      "Description" : "VPC ID",
      "Type" : "AWS::EC2::VPC::Id"
    }
  },
  "Mappings" : {
    "AWSAMI" : {
      "us-east-1" : { "AMI" : "ami-146e2a7c" },
      "us-west-2" : { "AMI" : "ami-e7527ed7" },
      "us-west-1" : { "AMI" : "ami-cd3aff89" }
    },
    "MongoMap" : {
      "2.6" : {
        "BaseUrl" : "https://repo.mongodb.org/yum/amazon/2013.03/mongodb-org/stable/x86_64/"
      },
      "3.0" : {
        "BaseUrl" : "https://repo.mongodb.org/yum/amazon/2013.03/mongodb-org/3.0/x86_64/"
      }
    },
    "CompanyNames" : {
      "bw" : {
        "CompanyFullName" : "buoyweather"
      }
    }
  },
  "Resources" : {
    "MongoClientSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "-MongoClientSecurityGroup", { "Ref" : "EnvironmentName" } ] ] },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "mongo-clients", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ],
        "VpcId" : { "Ref" : "VpcId" },
        "SecurityGroupEgress" : [
          { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" }
        ]
      }
    },
    "MongoServerSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "-MongoServerSecurityGroup", { "Ref" : "EnvironmentName" } ] ] },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "mongo-servers", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ],
        "VpcId" : { "Ref" : "VpcId" },
        "SecurityGroupIngress" : [
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "SourceSecurityGroupId" : { "Ref" : "MongoClientSecurityGroup" } },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.101.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.102.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.103.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.140.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.145.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.146.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.147.0/24" },
           { "IpProtocol" : "tcp", "FromPort" : "27017",  "ToPort" : "27017",  "CidrIp" : "192.168.255.0/24" }
         ],
        "SecurityGroupEgress" : [
          { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" }
        ]
      },
      "DependsOn" : [ "MongoClientSecurityGroup" ]
    },
    "MongoServerSecurityGroupIngress" : {
      "Type" : "AWS::EC2::SecurityGroupIngress",
      "Properties" : {
        "GroupId" : { "Ref" : "MongoServerSecurityGroup" },
        "IpProtocol" : "tcp",
        "FromPort" : "27017",
        "ToPort" : "27017",
        "SourceSecurityGroupId" : { "Ref" : "MongoServerSecurityGroup" }
      },
      "DependsOn" : "MongoServerSecurityGroup"
    },
    "InstanceRole" : {
      "Type" : "AWS::IAM::Role",
      "Properties" : {
        "AssumeRolePolicyDocument" : {
          "Version" : "2012-10-17",
            "Statement" : [
              {
                "Effect" : "Allow",
                "Principal" : { "Service" : [ "ec2.amazonaws.com" ]},
                "Action" : [ "sts:AssumeRole" ]
              }
            ]
        },
        "Path" : "/",
        "Policies" : [
          {
            "PolicyName" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "server-role", { "Ref" : "EnvironmentName" } ] ]
            },
            "PolicyDocument" : {
              "Version" : "2012-10-17",
              "Statement" : [
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:List*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ChefBucketName" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:Get*", "s3:List*" ],
                  "Resource": [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ChefBucketName" }, "/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:ListBucket" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "BackupBucketName" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:DeleteObject", "s3:Get*", "s3:Put*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "BackupBucketName" }, "/*" ] ] } ]
                },
                {
                  "Sid" : "SturdyAutoSnapshotTool",
                  "Effect" : "Allow",
                  "Action" : [
                      "ec2:CreateSnapshot",
                      "ec2:DescribeSnapshots",
                      "ec2:DeleteSnapshot",
                      "ec2:DescribeVolumes",
                      "ec2:ModifySnapshotAttribute",
                      "ec2:CreateTags"
                  ],
                  "Resource" : [
                      "*"
                  ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "ec2:CreateTags", "ec2:DescribeInstances", "ec2:DescribeTags" ],
                  "Resource" : [ "*" ]
                }
              ]
            }
          }
        ]
      }
    },
    "InstanceProfile" : {
      "Type" : "AWS::IAM::InstanceProfile",
      "Properties" : {
        "Path" : "/",
        "Roles" : [
          { "Ref" : "InstanceRole" }
        ]
      }
    },
    "LaunchServer" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "InstanceType" : { "Ref" : "InstanceType" },
        "EbsOptimized" : "true",
        "BlockDeviceMappings" : [
          {
            "DeviceName" : "/dev/sdf",
            "Ebs" : {
              "VolumeSize" : { "Ref" : "EbsStorageCapacityData"},
              "VolumeType" : "io1",
              "Iops" : { "Ref" : "EbsStorageIopsData" }
            }
          },
          {
            "DeviceName" : "/dev/sdg",
            "Ebs" : {
              "VolumeSize" : { "Ref" : "EbsStorageCapacityJournal"},
              "VolumeType" : "io1",
              "Iops" : { "Ref" : "EbsStorageIopsJournal" }
            }
          },
          {
            "DeviceName" : "/dev/sdh",
            "Ebs" : {
              "VolumeSize" : { "Ref" : "EbsStorageCapacityLog"},
              "VolumeType" : "io1",
              "Iops" : { "Ref" : "EbsStorageIopsLog" }
            }
          }
        ],
        "KeyName" : { "Ref" : "KeyName" },
        "ImageId" : { "Fn::FindInMap" : [ "AWSAMI", { "Ref" : "AWS::Region" }, "AMI" ] },
        "IamInstanceProfile" : { "Ref" : "InstanceProfile" },
        "SecurityGroups" : [
          { "Ref" : "InternalServerSecurityGroupId" },
          { "Ref" : "MongoServerSecurityGroup" }
        ],
        "UserData" : {
          "Fn::Base64" : {
            "Fn::Join" : [
              "",
              [
                "#cloud-config\n",
                "yum_repos:\n",
                "  mongodb:\n",
                "    baseurl: ", { "Fn::FindInMap" : [ "MongoMap", { "Ref" : "MongoVersion" }, "BaseUrl" ] }, "\n",
                "    enabled: true\n",
                "    gpgcheck: false\n",
                "    name: MongoDB Repository\n",
                "mounts:\n",
                "- [ xvdf, /var/lib/mongo, \"defaults,noatime\"]\n",
                "- [ xvdg, /var/lib/mongo/journal, \"defaults,noatime\"]\n",
                "- [ xvdh, /var/log/mongodb, \"defaults,noatime\"]\n",
                "repo_update: true\n",
                "repo_upgrade: all\n",
                "packages:\n",
                "- wget\n",
                "- python\n",
                "- python-pip\n",
                "- python-boto\n",
                "- \n",
                "write_files:\n",
                "  - path: /usr/local/bin/mongodb-sync\n",
                "    content: |\n",
                "      #!/bin/bash\n",
                "      mkdir -p /tmp/mongodump\n",
                "      mongodump -d buoyweather_cms_", { "Ref" : "EnvironmentName" }, " -o /tmp/mongodump\n",
                "      tar zcvf /tmp/mongodump.tar.gz /tmp/mongodump\n",
                "      aws s3 cp /tmp/mongodump.tar.gz s3://", { "Ref" : "BackupBucketName" }, "\n",
                "  - path: /etc/cron.d/mongodb-sync\n",
                "    content: |\n",
                "      */15 * * * * root bash /usr/local/bin/mongodb-sync > /var/log/mongodb-sync.log\n",
                "runcmd:\n",
                "- mkfs.ext4 /dev/xvdf\n",
                "- mkfs.ext4 /dev/xvdg\n",
                "- mkfs.ext4 /dev/xvdh\n",
                "- blockdev --setra 32 /dev/xvdf\n",
                "- blockdev --setra 32 /dev/xvdg\n",
                "- blockdev --setra 32 /dev/xvdh\n",
                "- echo 'ACTION==\"add\", KERNEL==\"xvdf\", ATTR{bdi/read_ahead_kb}=\"16\"' > /etc/udev/rules.d/85-ebs.rules\n",
                "- echo 'ACTION==\"add\", KERNEL==\"xvdg\", ATTR{bdi/read_ahead_kb}=\"16\"' >> /etc/udev/rules.d/85-ebs.rules\n",
                "- echo 'ACTION==\"add\", KERNEL==\"xvdh\", ATTR{bdi/read_ahead_kb}=\"16\"' >> /etc/udev/rules.d/85-ebs.rules\n",
                "- mount /var/lib/mongo\n",
                "- mkdir /var/lib/mongo/journal\n",
                "- mount /var/lib/mongo/journal\n",
                "- mount /var/log/mongodb\n",
                "- yum install -y mongodb-org\n",
                "- chown mongod /var/lib/mongo/journal\n",
                "- sed -i '/bindIp/s/^/#/' /etc/mongod.conf\n",
                "- chkconfig mongod on\n",
                "- service mongod restart\n",
                "- mkdir /tmp/mongorestore\n",
                "- aws s3 cp s3://", { "Ref" : "BackupBucketName" }, "/mongodump.tar.gz /tmp/mongorestore/mongodump.tar.gz\n",
                "- tar zxvf /tmp/mongorestore/mongodump.tar.gz -C /tmp/mongorestore\n",
                "- mongorestore -d buoyweather_cms_", { "Ref" : "EnvironmentName" }, " /tmp/mongorestore/tmp/mongodump/buoyweather_cms_", { "Ref" : "EnvironmentName" }, "\n",
                "- curl -L https://www.chef.io/chef/install.sh | bash -s -- -v 12.19.36\n",
                "- mkdir -p /var/log/chef /etc/chef\n",
                "- aws --region us-west-1 s3 sync s3://", { "Ref" : "ChefBucketName" }, "/config /etc/chef/\n",
                "- python /etc/chef/rename_instance\n"
              ]
            ]
          }
        }
      }
    },
    "ServerAutoScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "LaunchConfigurationName" : { "Ref" : "LaunchServer" },
        "MinSize" : "0",
        "MaxSize" : { "Ref" : "ServerCount" },
        "DesiredCapacity" : { "Ref" : "ServerCount" },
        "NotificationConfigurations" : [
          {
            "TopicARN" : { "Ref" : "AutoScalingNotification" },
            "NotificationTypes" : [
              "autoscaling:EC2_INSTANCE_LAUNCH",
              "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
              "autoscaling:EC2_INSTANCE_TERMINATE",
              "autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
            ]
          },
          {
            "TopicARN" : { "Ref" : "DeregTopic" },
            "NotificationTypes" : [
              "autoscaling:EC2_INSTANCE_TERMINATE"
            ]
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key": "recipes",
            "Value": { "Fn::Join": [ "", [ "role[base],", "role[", { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" } ] ] }, "]" ] ] },
            "PropagateAtLaunch": "true"
          },
          {
            "Key": "type",
            "Value": { "Fn::Join" : [ "-", [ { "Ref" : "ApplicationName" }, { "Ref" : "ServiceName" }] ] },
            "PropagateAtLaunch": "true"
          }
        ],
        "VPCZoneIdentifier" : [
          { "Ref" : "PriSubnet1" },
          { "Ref" : "PriSubnet2" }
        ],
        "MetricsCollection" : [
          {
            "Granularity" : "1Minute",
            "Metrics" : [
              "GroupMinSize",
              "GroupMaxSize"
            ]
          }
        ]
      }
    },
    "AutoScalingNotification": {
      "Type": "AWS::SNS::Topic",
      "Properties": {
        "DisplayName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "topic" ] ] },
        "Subscription": [
          {
            "Endpoint": { "Ref": "AlertSubscriptionEmail" },
            "Protocol": "email"
          }
        ],
        "TopicName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "topic" ] ] }
      }
    }
  }
}
