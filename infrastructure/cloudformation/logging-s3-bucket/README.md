# AWS (ELB/CF) Logging S3 Bucket

This template is used to deploy an S3 logging bucket

# Usage

## Parameters
* ApplicationName => redesign-proxy
* CompanyName => sl
* EnvironmentName => sbox

## Outputs
* BucketName

## CLI

### Create stack

```sh
export ENV="integration"
aws cloudformation create-stack                                          \
  --stack-name "sl-redesign-proxy-logging-${ENV}"                        \
  --capabilities "CAPABILITY_NAMED_IAM"                                  \
  --template-body "file://surfline-aws-logging-s3-bucket.json"           \
  --tags "Key=EnvironmentName,Value=${ENV}"                              \
  --parameters                                                           \
    "ParameterKey=ApplicationName,     ParameterValue=redesign-proxy"    \
    "ParameterKey=CompanyName,         ParameterValue=sl"                \
    "ParameterKey=EnvironmentName,     ParameterValue=${ENV}"

# =>

{
    "StackId": "arn:aws:cloudformation:us-west-1:665294954271:stack/sl-redesign-proxy-logging-integration/abece1f0-4002-11e7-a759-500cf8eeb88d"
}
```

### Check Status

```sh
# Check `create-stack` status
aws cloudformation describe-stack-events --stack-name "sl-redesign-proxy-logging-${ENV}" \
  | jq '.StackEvents[] | {Timestamp, ResourceStatus, ResourceType, LogicalResourceId, ResourceStatusReason}'
```
