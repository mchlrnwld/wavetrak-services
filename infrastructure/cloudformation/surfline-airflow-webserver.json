{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "Surfline Airflow Webserver (1.5.1)",
  "Parameters" : {
    "AcmCertificateArn": {
      "Description": "Arn of certificate to use for load balancer",
      "Type": "String"
    },
    "AlertSubscriptionEmail": {
      "Description": "Email to receive instance events",
      "Type": "String"
    },
    "AllServerSecurityGroupId" : {
      "Description" : "The security group used for all servers",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "ApplicationName" : {
      "Description" : "Name of application database will be used for",
      "Type" : "String"
      ,"Default" : "airflow"
    },
    "BackupBucketName" : {
      "Description" : "Name of bucket storing backups",
      "Type" : "String"
    },
    "ChefBucketName" : {
      "Description" : "Name of bucket storing Chef configuration",
      "Type" : "String"
    },
    "CompanyName" : {
      "Description" : "Company (Surfline, Buoyweather)",
      "Type" : "String",
      "Default" : "sl",
      "AllowedValues" : [
        "bw",
        "ft",
        "sl"
      ]
    },
    "DeregTopic": {
      "Description": "The ARN of the DeregTopic",
      "Type": "String"
    },
    "EbsStorageCapacityRoot" : {
      "Description" : "EBS storage capacity (GB) - /",
      "Type" : "Number",
      "Default" : "80",
      "MinValue" : "10",
      "MaxValue" : "16384"
    },
    "ElbLoggingBucketName" : {
      "Description" : "Name of bucket storing ELB logs",
      "Type" : "String"
    },
    "ElbLoggingBucketEnabled" : {
      "Description" : "Enable ELB bucket logging",
      "Type" : "String",
      "AllowedValues": [
        "false",
        "true"
      ]
    },
    "ElbLoggingBucketInterval" : {
      "Description" : "Enable ELB bucket logging interval (minutes)",
      "Type" : "String",
      "AllowedValues": [
        "5",
        "60"
      ]
    },
    "ElbScheme": {
      "Description": "Type of load balancer to create",
      "Type": "String",
      "AllowedValues": [
        "internal",
        "internet-facing"
      ],
      "Default": "internal"
    },
    "ElbSubnet1" : {
      "Description" : "ELB subnet 1",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "ElbSubnet2" : {
      "Description" : "ELB subnet 2",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "EnvironmentName" : {
      "Description" : "Name of Environment",
      "Type" : "String",
      "AllowedValues" : [
        "common",
        "dev",
        "prod",
        "qa",
        "sbox",
        "staging",
        "testing"
      ]
    },
    "HealthCheckGracePeriod": {
      "Description": "Legnth of health check grace period to use for AutoScaling Group (seconds)",
      "Type" : "Number",
      "Default" : "300",
      "MinValue" : "30",
      "MaxValue" : "900"
    },
    "HealthCheckInterval": {
      "Description": "Interval of health check to use for load balancer (5 - 300)",
      "Type" : "Number",
      "Default" : "10",
      "MinValue" : "5",
      "MaxValue" : "300"
    },
    "HealthCheckTimeout": {
      "Description": "Timeout of health check to use for load balancer (2 - 60)",
      "Type" : "Number",
      "Default" : "5",
      "MinValue" : "2",
      "MaxValue" : "60"
    },
    "HealthCheckType": {
      "Description": "Type of health check to use for AutoScaling Group (ELB / EC2)",
      "Type": "String",
      "AllowedValues": [
        "ELB",
        "EC2"
      ],
      "Default": "EC2"
    },
    "HealthCheckUnhealthyThreshold": {
      "Description": "Unhealthy threshold of health check to use for load balancer (2 - 10)",
      "Type" : "Number",
      "Default" : "10",
      "MinValue" : "2",
      "MaxValue" : "10"
    },
    "InternalServerSecurityGroupId" : {
      "Description" : "The security group used for internal servers",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "InstanceType" : {
      "Description" : "Type of instance(s). Selection limited to EBS optimized types.",
      "Type" : "String",
      "Default" : "m4.large",
      "AllowedValues" : [
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c3.large",
        "c3.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "c4.large",
        "c4.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "d2.xlarge",
        "g2.2xlarge",
        "g2.2xlarge ",
        "g2.8xlarge",
        "g2.8xlarge ",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "i2.xlarge",
        "m3.2xlarge",
        "m3.large",
        "m3.medium",
        "m3.xlarge",
        "m4.10xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.large",
        "m4.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "r3.large",
        "r3.xlarge",
        "t2.large",
        "t2.large ",
        "t2.medium",
        "t2.micro",
        "t2.small"
      ]
    },
    "InstanceSubnet1" : {
      "Description" : "Instance subnet 1",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "InstanceSubnet2" : {
      "Description" : "Instance subnet 2",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "JenkinsServerSecurityGroup": {
      "Description": "Security group of Jenkins servers",
      "Type": "String"
    },
    "KeyName" : {
      "Description" : "Name of an existing EC2-VPC KeyPair",
      "Type" : "AWS::EC2::KeyPair::KeyName"
    },
    "MaxServerCount" : {
      "Description" : "Max number of instances to start",
      "Type" : "Number",
      "Default" : "1",
      "MinValue" : "0",
      "MaxValue" : "10"
    },
    "MinServerCount" : {
      "Description" : "Min number of instances to start",
      "Type" : "Number",
      "Default" : "1",
      "MinValue" : "0",
      "MaxValue" : "10"
    },
    "RdsClientSecurityGroup" : {
      "Description" : "The security group used for database clients",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "RedisClientSecurityGroup": {
      "Description": "Redis client security group ID",
      "Type": "AWS::EC2::SecurityGroup::Id"
    },
    "ServiceName" : {
      "Description" : "Service name",
      "Type" : "String",
      "Default" : "webserver"
    },
    "VpcId" : {
      "Description" : "VPC ID",
      "Type" : "AWS::EC2::VPC::Id"
    }
  },
  "Mappings" : {
    "AWSAMI" : {
      "us-east-1" : { "AMI" : "ami-a4827dc9" },
      "us-west-1" : { "AMI" : "ami-0e113406c1d38fb8d" },
      "us-west-2" : { "AMI" : "ami-f303fb93" }
    },
    "CompanyNames" : {
      "bw" : {
        "CompanyFullName" : "buoyweather"
      },
      "ft" : {
        "CompanyFullName" : "fishtrack"
      },
      "sl" : {
        "CompanyFullName" : "surfline"
      }
    }
  },
  "Resources" : {
    "ElbSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "ElbSecurityGroup", { "Ref" : "EnvironmentName" } ] ] },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "elb", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ],
        "VpcId" : { "Ref" : "VpcId" },
        "SecurityGroupIngress" : [
          { "IpProtocol": "tcp", "FromPort": "443",  "ToPort": "443",  "CidrIp": "0.0.0.0/0" }
        ],
        "SecurityGroupEgress" : [
          { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" }
        ]
      }
    },
    "ServerSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "ServerSecurityGroup", { "Ref" : "EnvironmentName" } ] ] },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "servers", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ],
        "VpcId" : { "Ref" : "VpcId" },
        "SecurityGroupIngress" : [
           { "IpProtocol" : "tcp", "FromPort" : "8080",  "ToPort" : "8080",  "SourceSecurityGroupId" : { "Ref" : "ElbSecurityGroup" } },
           { "IpProtocol": "tcp", "FromPort": "22", "ToPort": "22", "SourceSecurityGroupId": { "Ref": "JenkinsServerSecurityGroup" } }
         ],
        "SecurityGroupEgress" : [
          { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" }
        ]
      },
      "DependsOn" : [ "ElbSecurityGroup" ]
    },
    "LoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "AccessLoggingPolicy": {
          "EmitInterval": { "Ref": "ElbLoggingBucketInterval"},
          "Enabled": { "Ref": "ElbLoggingBucketEnabled" },
          "S3BucketName": { "Ref": "ElbLoggingBucketName" }
        },
        "CrossZone" : "True",
        "LoadBalancerName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] },
        "Scheme": { "Ref": "ElbScheme" },
        "SecurityGroups" : [
          { "Ref": "ElbSecurityGroup" }
        ],
        "Instances" : [ ],
        "Subnets" : [
          { "Ref": "ElbSubnet1" },
          { "Ref": "ElbSubnet2" }
        ],
        "Listeners" : [
          { "LoadBalancerPort": "443", "InstancePort": "8080", "Protocol": "HTTPS", "InstanceProtocol": "HTTP", "SSLCertificateId": { "Ref": "AcmCertificateArn" }}
        ],
        "HealthCheck": {
          "Target": "HTTP:8080/admin/",
          "HealthyThreshold": "2",
          "UnhealthyThreshold":  { "Ref": "HealthCheckUnhealthyThreshold" },
          "Interval": { "Ref": "HealthCheckInterval" },
          "Timeout": { "Ref": "HealthCheckTimeout" }
        },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "servers", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ]
      }
    },
    "InstanceRole" : {
      "Type" : "AWS::IAM::Role",
      "Properties" : {
        "AssumeRolePolicyDocument" : {
          "Version" : "2012-10-17",
            "Statement" : [
              {
                "Effect" : "Allow",
                "Principal" : { "Service" : [ "ec2.amazonaws.com" ]},
                "Action" : [ "sts:AssumeRole" ]
              }
            ]
        },
        "Path" : "/",
        "Policies" : [
          {
            "PolicyName" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "server-role", { "Ref" : "EnvironmentName" } ] ]
            },
            "PolicyDocument" : {
              "Version" : "2012-10-17",
              "Statement" : [
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:List*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "BackupBucketName" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:Get*", "s3:List*", "s3:Put*" ],
                  "Resource": [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "BackupBucketName" }, "/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:List*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ChefBucketName" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:Get*", "s3:List*" ],
                  "Resource": [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ChefBucketName" }, "/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "ec2:CreateTags", "ec2:DescribeInstances", "ec2:DescribeTags", "elasticmapreduce:*", "iam:PassRole" ],
                  "Resource" : [ "*" ]
                }
              ]
            }
          }
        ]
      }
    },
    "InstanceProfile" : {
      "Type" : "AWS::IAM::InstanceProfile",
      "Properties" : {
        "Path" : "/",
        "Roles" : [
          { "Ref" : "InstanceRole" }
        ]
      }
    },
    "LaunchServer" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "BlockDeviceMappings" : [
          {
            "DeviceName" : "/dev/xvda",
            "Ebs" : {
              "VolumeSize" : { "Ref" : "EbsStorageCapacityRoot"},
              "VolumeType" : "gp2"
            }
          }
        ],
        "InstanceType" : { "Ref" : "InstanceType" },
        "KeyName" : { "Ref" : "KeyName" },
        "ImageId" : { "Fn::FindInMap" : [ "AWSAMI", { "Ref" : "AWS::Region" }, "AMI" ] },
        "IamInstanceProfile" : { "Ref" : "InstanceProfile" },
        "SecurityGroups" : [
          { "Ref" : "AllServerSecurityGroupId" },
          { "Ref" : "InternalServerSecurityGroupId" },
          { "Ref" : "RdsClientSecurityGroup" },
          { "Ref" : "RedisClientSecurityGroup" },
          { "Ref" : "ServerSecurityGroup" }
        ],
        "UserData" : {
          "Fn::Base64" : {
            "Fn::Join" : [
              "",
              [
                "#cloud-config\n",
                "repo_releasever: 2016.03\n",
                "repo_update: true\n",
                "repo_upgrade: all\n",
                "runcmd:\n",
                "- echo \"Restoring Airflow Dags\"\n",
                "- mkdir -p /home/ec2-user/airflow/dags\n",
                "- aws s3 sync s3://", { "Ref" : "BackupBucketName" }, "/dags /home/ec2-user/airflow/dags\n",
                "- echo \"Installing Chef\"\n",
                "- curl -L https://www.chef.io/chef/install.sh | bash -s -- -v 12.14\n",
                "- mkdir -p /var/log/chef /etc/chef\n",
                "- echo \"Configuring Chef\"\n",
                "- aws --region us-west-1 s3 sync s3://", { "Ref" : "ChefBucketName" }, "/config /etc/chef/\n",
                "- echo \"Checking into Chef\"\n",
                "- python /etc/chef/rename_instance\n"
              ]
            ]
          }
        }
      }
    },
    "ServerAutoScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "HealthCheckGracePeriod": { "Ref": "HealthCheckGracePeriod" },
        "HealthCheckType": { "Ref": "HealthCheckType" },
        "LaunchConfigurationName" : { "Ref" : "LaunchServer" },
        "MinSize" : { "Ref" : "MinServerCount" },
        "MaxSize" : { "Ref" : "MaxServerCount" },
        "DesiredCapacity" : { "Ref" : "MinServerCount" },
        "NotificationConfigurations" : [
          {
            "TopicARN" : { "Ref" : "AutoScalingNotification" },
            "NotificationTypes" : [
              "autoscaling:EC2_INSTANCE_LAUNCH",
              "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
              "autoscaling:EC2_INSTANCE_TERMINATE",
              "autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
            ]
          },
          {
            "TopicARN" : { "Ref" : "DeregTopic" },
            "NotificationTypes" : [
              "autoscaling:EC2_INSTANCE_TERMINATE"
            ]
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key": "recipes",
            "Value": { "Fn::Join": [ "", [ "role[base],", "role[", { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "ServiceName" } ] ] }, "]" ] ] },
            "PropagateAtLaunch": "true"
          },
          {
            "Key": "type",
            "Value": { "Fn::Join" : [ "-", [ { "Ref" : "ApplicationName" }, { "Ref" : "ServiceName" }] ] },
            "PropagateAtLaunch": "true"
          }
        ],
        "LoadBalancerNames" : [
          { "Ref": "LoadBalancer" }
        ],
        "VPCZoneIdentifier" : [
          { "Ref" : "InstanceSubnet1" },
          { "Ref" : "InstanceSubnet2" }
        ],
        "MetricsCollection" : [
          {
            "Granularity" : "1Minute",
            "Metrics" : [
              "GroupMinSize",
              "GroupMaxSize"
            ]
          }
        ]
      }
    },
    "ServerScaleUpPolicy": {
      "Type": "AWS::AutoScaling::ScalingPolicy",
      "Properties": {
        "AdjustmentType": "ChangeInCapacity",
        "AutoScalingGroupName": { "Ref": "ServerAutoScalingGroup" },
        "Cooldown": "900",
        "ScalingAdjustment": "1"
      }
    },
    "ServerScaleDownPolicy": {
      "Type": "AWS::AutoScaling::ScalingPolicy",
      "Properties": {
        "AdjustmentType": "ChangeInCapacity",
        "AutoScalingGroupName": { "Ref": "ServerAutoScalingGroup" },
        "Cooldown": "900",
        "ScalingAdjustment": "-1"
      }
    },
    "ServerCPUAlarmHigh": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "AlarmDescription": "Scale-up if CPU > 80% for 5 minutes",
        "MetricName": "CPUUtilization",
        "Namespace": "AWS/EC2",
        "Statistic": "Average",
        "Period": "300",
        "EvaluationPeriods": "1",
        "Threshold": "80",
        "AlarmActions": [ { "Ref": "ServerScaleUpPolicy" } ],
        "Dimensions": [
          {
            "Name": "AutoScalingGroupName",
            "Value": { "Ref": "ServerAutoScalingGroup" }
          }
        ],
        "ComparisonOperator": "GreaterThanThreshold"
      }
    },
    "ServerCPUAlarmLow": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "AlarmDescription": "Scale-down if CPU < 40% for 20 minutes",
        "MetricName": "CPUUtilization",
        "Namespace": "AWS/EC2",
        "Statistic": "Average",
        "Period": "300",
        "EvaluationPeriods": "4",
        "Threshold": "40",
        "AlarmActions": [ { "Ref": "ServerScaleDownPolicy" } ],
        "Dimensions": [
          {
            "Name": "AutoScalingGroupName",
            "Value": { "Ref": "ServerAutoScalingGroup" }
          }
        ],
        "ComparisonOperator": "LessThanThreshold"
      }
    },
    "AutoScalingNotification": {
      "Type": "AWS::SNS::Topic",
      "Properties": {
        "DisplayName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "topic" ] ] },
        "Subscription": [
          {
            "Endpoint": { "Ref": "AlertSubscriptionEmail" },
            "Protocol": "email"
          }
        ],
        "TopicName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "topic" ] ] }
      }
    }
  },
  "Outputs" : {
    "LoadBalancerCname" : {
      "Description" : "CNAME of the load balancer",
      "Value" : { "Fn::GetAtt" : [ "LoadBalancer", "DNSName" ] }
    }
  }
}
