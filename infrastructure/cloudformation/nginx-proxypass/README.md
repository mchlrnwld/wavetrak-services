# Frontend Web Server Stack - Nginx ProxyPass

This template is used to deploy an Nginx proxy server cluster

## Usage

### Parameters

|Parameter|Default|
|:--------|:------|
|`AlertSubscriptionEmail`|`monitor@surfline.com`|
|`AllServerSecurityGroupId`|`sg-91aeaef4`|
|`ApplicationName`|`redesign`|
|`ArtifactBucketName`|`sl-artifacts-${ENV}`|
|`ArtifactName`|`chef-managed`|
|`ChefBucketName`|`sl-chef-${ENV}`|
|`CompanyName`|`sl`|
|`DeregTopic`|`arn:aws:sns:us-west-1:665294954271:Sturdy-SAND`|
|`ElbLoggingBucketEnabled`|`true`|
|`ElbLoggingBucketInterval`|`5`|
|`ElbLoggingBucketName`|`sl-redesign-proxy-${ENV}`|
|`ElbScheme`|`internet-facing`|
|`ElbSubnet1`|`subnet-f4d458ad`|
|`ElbSubnet2`|`subnet-0909466c`|
|`EnvironmentName`|`${ENV}`|
|`IamCertificateArn`|`arn:aws:acm:us-west-1:665294954271:certificate/57f89e15-1cd5-4083-82e9-a282abcd0d64`|
|`IamCertificateName`||
|`InstanceSubnet1`|`subnet-f2d458ab`|
|`InstanceSubnet2`|`subnet-0909466c`|
|`InstanceType`|`t2.micro`|
|`InternalServerSecurityGroupId`|`sg-90aeaef5`|
|`KeyName`|`sturdy-surfline-dev`|
|`MaxServerCount`|`1`|
|`MinServerCount`|`1`|
|`ServiceName`|`proxy`|
|`VpcId`|`vpc-981887fd`|

### Outputs

* `LoadBalancerCname`

### CLI

#### Create stack

```sh
export ENV="integration"
aws cloudformation create-stack                          \
  --stack-name "sl-redesign-proxy-${ENV}"                \
  --capabilities "CAPABILITY_NAMED_IAM"                  \
  --template-body "file://surfline-nginx-proxypass.json" \
  --tags "Key=EnvironmentName,Value=${ENV}"              \
  --parameters                                           \
    "ParameterKey=AlertSubscriptionEmail,   ParameterValue=monitor@surfline.com"     \
    "ParameterKey=AllServerSecurityGroupId, ParameterValue=sg-91aeaef4"              \
    "ParameterKey=ApplicationName,          ParameterValue=redesign"                 \
    "ParameterKey=ArtifactBucketName,       ParameterValue=sl-artifacts-${ENV}"      \
    "ParameterKey=ArtifactName,             ParameterValue=chef-managed"             \
    "ParameterKey=ChefBucketName,           ParameterValue=sl-chef-${ENV}"           \
    "ParameterKey=CompanyName,              ParameterValue=sl"                       \
    "ParameterKey=DeregTopic,               ParameterValue=arn:aws:sns:us-west-1:665294954271:Sturdy-SAND" \
    "ParameterKey=ElbLoggingBucketEnabled,  ParameterValue=true"                     \
    "ParameterKey=ElbLoggingBucketInterval, ParameterValue=5"                        \
    "ParameterKey=ElbLoggingBucketName,     ParameterValue=sl-redesign-proxy-${ENV}" \
    "ParameterKey=ElbScheme,                ParameterValue=internet-facing"          \
    "ParameterKey=ElbSubnet1,               ParameterValue=subnet-f4d458ad"          \
    "ParameterKey=ElbSubnet2,               ParameterValue=subnet-0b09466e"          \
    "ParameterKey=EnvironmentName,          ParameterValue=${ENV}"                   \
    "ParameterKey=IamCertificateArn,        ParameterValue=arn:aws:acm:us-west-1:665294954271:certificate/57f89e15-1cd5-4083-82e9-a282abcd0d64" \
    "ParameterKey=IamCertificateName,       ParameterValue="                         \
    "ParameterKey=InstanceSubnet1,          ParameterValue=subnet-f2d458ab"          \
    "ParameterKey=InstanceSubnet2,          ParameterValue=subnet-0909466c"          \
    "ParameterKey=InstanceType,             ParameterValue=t2.micro"                 \
    "ParameterKey=InternalServerSecurityGroupId, ParameterValue=sg-90aeaef5"         \
    "ParameterKey=KeyName,                  ParameterValue=sturdy-surfline-dev"      \
    "ParameterKey=MaxServerCount,           ParameterValue=1"                        \
    "ParameterKey=MinServerCount,           ParameterValue=1"                        \
    "ParameterKey=ServiceName,              ParameterValue=proxy"                    \
    "ParameterKey=VpcId,                    ParameterValue=vpc-981887fd"

# =>

{
    "StackId": "arn:aws:cloudformation:us-west-1:665294954271:stack/sl-redesign-proxy-integration/b69e6780-4008-11e7-a759-500cf8eeb88d"
}
```

#### Check Status

```sh
# Check `create-stack` status
aws cloudformation describe-stack-events --stack-name "sl-redesign-proxy-${ENV}" \
  | jq '.StackEvents[] | {Timestamp, ResourceStatus, ResourceType, LogicalResourceId, ResourceStatusReason}'
```
