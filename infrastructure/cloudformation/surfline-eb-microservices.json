{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Surfline Beanstalk Microservice (1.2.0)",
  "Parameters": {
    "AcmCertificateArn": {
      "Description": "Arn of certificate to use for load balancer",
      "Type": "String"
    },
    "AppSecurityGroups": {
      "Description": "SecurityGroups for the application (comma delimited, no spaces)",
      "Type": "String"
    },
    "EC2KeyPair": {
      "Description": "Select the EC2 KeyPair for the instance created by eb",
      "Type": "AWS::EC2::KeyPair::KeyName"
    },
    "EnvironmentName": {
      "Description": "Name of Environment",
      "Type": "String",
      "Default": "prod",
      "AllowedValues": [
        "common",
        "dev",
        "prod",
        "qa",
        "sbox",
        "staging",
        "testing"
      ]
    },
    "BeanstalkEnvName": {
      "Description": "Name of Beanstalk Environment (Maximum Characters: 40)",
      "Type": "String",
      "MaxLength": "40"
    },
    "HealthcheckURL": {
      "Description": "Healthcheck URL, beginning with '/' (e.g. /health/check) or leave blank",
      "Type": "String",
      "Default": "/health"
    },
    "LogToS3": {
      "Description": "Copy application log files to S3?",
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "true",
        "false"
      ],
      "ConstraintDescription": "true or false"
    },
    "InstanceType" : {
      "Description" : "Type of instance to use",
      "Type" : "String",
      "Default" : "t2.small",
      "AllowedValues" : [
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c3.large",
        "c3.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "c4.large",
        "c4.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "d2.xlarge",
        "g2.2xlarge",
        "g2.2xlarge ",
        "g2.8xlarge",
        "g2.8xlarge ",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "i2.xlarge",
        "m3.2xlarge",
        "m3.large",
        "m3.medium",
        "m3.xlarge",
        "m4.10xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.large",
        "m4.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "r3.large",
        "r3.xlarge",
        "t2.large",
        "t2.large ",
        "t2.medium",
        "t2.micro",
        "t2.small"
      ]
    },
    "InstanceProfileName": {
      "Description": "Please enter the name of the IAM Instance Profile for the application server",
      "Type": "String",
      "Default": "aws-elasticbeanstalk-ec2-role"
    },
    "MicroserviceName": {
      "Description": "Name of the micro-service (e.g. password-reset-service)",
      "Type": "String"
    },
    "MinInstances": {
      "Description": "Minimum number of instances for auto scaling group (1-1000)",
      "Type": "Number",
      "Default": "2",
      "MaxValue": "1000",
      "MinValue": "1"
    },
    "MaxInstances": {
      "Description": "Maximum number of instances for auto scaling group (1-1000)",
      "Type": "Number",
      "Default": "4",
      "MaxValue": "1000",
      "MinValue": "1"
    },
    "NotificationEmail": {
      "Description": "E-mail address to send notifications to",
      "Type": "String",
      "Default" : "monitor@surfline.com"
    },
    "LBType": {
      "Description": "Please select load balancer type (internal or external)",
      "Type": "String",
      "Default": "external",
      "AllowedValues": [
        "internal",
        "external"
      ],
      "ConstraintDescription": "internal or external, not whatever you just put"
    },
    "CompanyName": {
      "Description": "Company abbreviation (sl=surfline, bw=buoyweather, ft=fishtrack)",
      "Type": "String",
      "Default": "sl",
      "AllowedValues": [
        "sl",
        "bw",
        "ft"
      ]
    },
    "NodeStackAMI": {
      "Description": "Select the Node.js stack / AMI version",
      "Type": "String",
      "Default": "64bit Amazon Linux 2015.09 v2.0.8 running Node.js",
      "AllowedValues": [
        "64bit Amazon Linux 2015.09 v2.0.8 running Node.js",
        "64bit Amazon Linux 2016.03 v2.1.0 running Node.js",
        "64bit Amazon Linux 2016.03 v2.1.1 running Node.js",
        "64bit Amazon Linux 2016.03 v2.1.3 running Node.js"
      ]
    },
    "ServiceRoleName": {
      "Description": "Enter the service role name for ElasticBeanstalk",
      "Type": "String",
      "Default": "aws-elasticbeanstalk-service-role"
    },
    "ELBSubnetIDs": {
      "Description": "Enter the subnet IDs for your application LB",
      "Type": "String"
    },
    "ELBInstanceProtocol": {
      "Description": "Enter the protocol for the ELB and Instance (HTTP or HTTPS)",
      "Type": "String",
      "Default": "HTTP",
      "AllowedValues": [
        "HTTP",
        "HTTPS"
      ]
    },
    "SubnetIDs": {
      "Description": "Enter the subnet IDs for your application",
      "Type": "String"
    },
    "VPCID": {
      "Description": "Select the VPC to place your application instances into",
      "Type": "AWS::EC2::VPC::Id"
    }
  },
  "Mappings": {
  },
  "Conditions": {
    "IsInternalLB": { "Fn::Equals": [{ "Ref": "LBType" }, "internal"]},
    "IsLBHTTPS": { "Fn::Equals": [{ "Ref": "ELBInstanceProtocol"}, "HTTPS"]},
    "NoHealthCheck": { "Fn::Equals": [{ "Ref": "HealthcheckURL"}, ""] }
  },
  "Metadata": {
    "AWS::CloudFormation::Interface": {
      "ParameterGroups": [
        {
          "Label": {
            "default": "Application Settings"
          },
          "Parameters": [
            "CompanyName",
            "BeanstalkEnvName",
            "EnvironmentName",
            "MicroserviceName",
            "ServiceRoleName",
            "NotificationEmail",
            "LogToS3"
          ]
        },
        {
          "Label": {
            "default": "Network Settings"
          },
          "Parameters": [
            "AppSecurityGroups",
            "ELBSubnetIDs",
            "LBType",
            "AcmCertificateArn",
            "ELBInstanceProtocol",
            "HealthcheckURL",
            "SubnetIDs",
            "VPCID"
          ]
        },
        {
          "Label": {
            "default": "EC2 Settings"
          },
          "Parameters": [
            "InstanceProfileName",
            "NodeStackAMI",
            "InstanceType",
            "MinInstances",
            "MaxInstances",
            "EC2KeyPair"
          ]
        }
      ]
    }
  },
  "Resources": {
    "BeanstalkApp": {
      "Type": "AWS::ElasticBeanstalk::Application",
      "Properties": {
        "ApplicationName": { "Fn::Join": [ "-", [{ "Ref": "CompanyName" }, { "Ref": "MicroserviceName" }, { "Ref": "EnvironmentName" }]]},
        "Description": { "Fn::Join": [ " ", [ "AWS Elastic Beanstalk Application:", { "Ref": "MicroserviceName" }]]}
      }
    },
    "BeanstalkEnv": {
      "Type": "AWS::ElasticBeanstalk::Environment",
      "Properties": {
        "ApplicationName": { "Fn::Join": [ "-", [{ "Ref": "CompanyName" }, { "Ref": "MicroserviceName" }, { "Ref": "EnvironmentName" }]]},
        "Description": { "Fn::Join": [ " ", [ "AWS Elastic Beanstalk Environment running", { "Ref": "MicroserviceName" }, "in", { "Ref": "EnvironmentName" }]]},
        "CNAMEPrefix": { "Ref": "BeanstalkEnvName" },
        "EnvironmentName":  { "Ref": "BeanstalkEnvName" },
        "SolutionStackName": { "Ref": "NodeStackAMI" },
        "OptionSettings": [
        {
          "Namespace": "aws:autoscaling:launchconfiguration",
          "OptionName": "EC2KeyName",
          "Value": { "Ref": "EC2KeyPair" }
        },
        {
          "Namespace": "aws:autoscaling:launchconfiguration",
          "OptionName": "InstanceType",
          "Value": { "Ref": "InstanceType" }
        },
        {
          "Namespace": "aws:autoscaling:launchconfiguration",
          "OptionName": "IamInstanceProfile",
          "Value": { "Ref": "InstanceProfileName" }
        },
        {
          "Namespace": "aws:autoscaling:asg",
          "OptionName": "MinSize",
          "Value": { "Ref": "MinInstances" }
        },
        {
          "Namespace": "aws:autoscaling:asg",
          "OptionName": "MaxSize",
          "Value": { "Ref": "MaxInstances" }
        },
        {
          "Namespace": "aws:elasticbeanstalk:environment",
          "OptionName": "ServiceRole",
          "Value": { "Ref": "ServiceRoleName" }
        },
        {
          "Namespace": "aws:elasticbeanstalk:healthreporting:system",
          "OptionName": "SystemType",
          "Value": "enhanced"
        },
        {
          "Namespace": "aws:elasticbeanstalk:sns:topics",
          "OptionName": "Notification Endpoint",
          "Value": { "Ref": "NotificationEmail" }
        },
        {
          "Namespace": "aws:elasticbeanstalk:sns:topics",
          "OptionName": "Notification Protocol",
          "Value": "email"
        },
        {
          "Namespace": "aws:elasticbeanstalk:hostmanager",
          "OptionName": "LogPublicationControl",
          "Value": { "Ref": "LogToS3" }
        },
        {
          "Namespace": "aws:ec2:vpc",
          "OptionName": "ELBScheme",
          "Value": { "Fn::If": [ "IsInternalLB", { "Ref": "LBType" }, { "Ref": "AWS::NoValue" }]}
        },
        {
          "Namespace": "aws:ec2:vpc",
          "OptionName": "VPCId",
          "Value": { "Ref": "VPCID" }
        },
        {
          "Namespace": "aws:ec2:vpc",
          "OptionName": "Subnets",
          "Value": { "Ref": "SubnetIDs" }
        },
        {
          "Namespace": "aws:ec2:vpc",
          "OptionName": "ELBSubnets",
          "Value": { "Ref": "ELBSubnetIDs" }
        },
        {
          "Namespace": "aws:elasticbeanstalk:application",
          "OptionName": "Application Healthcheck URL",
          "Value": { "Fn::If": [ "NoHealthCheck", { "Ref": "AWS::NoValue" }, { "Ref": "HealthcheckURL" }]}
        },
        {
          "Namespace": "aws:autoscaling:updatepolicy:rollingupdate",
          "OptionName": "RollingUpdateEnabled",
          "Value": "true"
        },
        {
          "Namespace": "aws:autoscaling:updatepolicy:rollingupdate",
          "OptionName": "RollingUpdateType",
          "Value": "Health"
        },
        {
          "Namespace": "aws:elb:loadbalancer",
          "OptionName": "CrossZone",
          "Value": "true"
        },
        {
          "Namespace": "aws:autoscaling:launchconfiguration",
          "OptionName": "SecurityGroups",
          "Value": { "Ref": "AppSecurityGroups" }
        },
        {
          "Namespace": "aws:autoscaling:launchconfiguration",
          "OptionName": "RootVolumeType",
          "Value": "gp2"
        },
        {
          "Namespace": "aws:elasticbeanstalk:command",
          "OptionName": "BatchSize",
          "Value": "30"
        },
        {
          "Namespace": "aws:elb:policies",
          "OptionName": "ConnectionDrainingEnabled",
          "Value": "true"
        },
        {
          "Namespace": "aws:elb:policies",
          "OptionName": "ConnectionDrainingTimeout",
          "Value": "20"
        },
        {
          "Namespace": { "Fn::Join": ["",["aws:elb:listener", { "Fn::If": [ "IsLBHTTPS", ":443", ":80" ]}]]},
          "OptionName": "ListenerProtocol",
          "Value": { "Ref": "ELBInstanceProtocol" }
        },
        {
          "Namespace": { "Fn::Join": ["",["aws:elb:listener", { "Fn::If": [ "IsLBHTTPS", ":443", ":80" ]}]]},
          "OptionName": "InstancePort",
          "Value": { "Fn::If": [ "IsLBHTTPS", "443", "80" ]}
        },
        {
          "Namespace": { "Fn::Join": ["",["aws:elb:listener", { "Fn::If": [ "IsLBHTTPS", ":443", ":80" ]}]]},
          "OptionName": "InstanceProtocol",
          "Value": { "Ref": "ELBInstanceProtocol" }
        },
        {
          "Namespace": { "Fn::Join": ["",["aws:elb:listener", { "Fn::If": [ "IsLBHTTPS", ":443", ":80" ]}]]},
          "OptionName": "SSLCertificateId",
          "Value":  { "Ref": "AcmCertificateArn" }
        }]
      },
      "DependsOn": "BeanstalkApp"
    }
  }
}
