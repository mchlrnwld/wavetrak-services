{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "Surfline KMS Key (1.0.0)",
  "Parameters" : {
    "KeyAlias": {
      "Default": "",
      "Description": "(Optional) alias to create for the key",
      "Type": "String"
    },
    "AdminRoleArn": {
      "Default": "",
      "Description": "(Optional) ARN of admin IAM role; specifying a role here will grant it full access to manage the key",
      "Type": "String"
    },
    "AppServerRoleArn": {
      "Default": "",
      "Description": "(Optional) ARN of server IAM role; specifying a role here will grant it access to the key",
      "Type": "String"
    },
    "ApplicationName" : {
      "Description" : "Name of application",
      "Type" : "String",
      "Default" : "coldfusion"
    },
    "CompanyName" : {
      "Description" : "Company (Buoyweather, FishTrack, Surfline)",
      "Type" : "String",
      "Default" : "sl",
      "AllowedValues" : [
        "bw",
        "ft",
        "sl"
      ]
    },
    "EnvironmentName" : {
      "Description" : "Name of Environment",
      "Type" : "String",
      "Default" : "prod",
      "AllowedValues" : [
        "common",
        "dev",
        "prod",
        "qa",
        "sandbox",
        "staging",
        "testing"
      ]
    }
  },
  "Conditions" :  {
    "AdminRoleArnSpecified": {
      "Fn::And": [
        {
          "Fn::Not": [
            {
              "Fn::Equals": [
                {
                  "Ref": "AdminRoleArn"
                },
                ""
              ]
            }
          ]
        },
        {
          "Fn::Not": [
            {
              "Fn::Equals": [
                {
                  "Ref": "AdminRoleArn"
                },
                "undefined"
              ]
            }
          ]
        }
      ]
    },
    "KeyAliasSpecified": {
      "Fn::And": [
        {
          "Fn::Not": [
            {
              "Fn::Equals": [
                {
                  "Ref": "KeyAlias"
                },
                ""
              ]
            }
          ]
        },
        {
          "Fn::Not": [
            {
              "Fn::Equals": [
                {
                  "Ref": "KeyAlias"
                },
                "undefined"
              ]
            }
          ]
        }
      ]
    },
    "AppServerRoleArnSpecified": {
      "Fn::And": [
        {
          "Fn::Not": [
            {
              "Fn::Equals": [
                {
                  "Ref": "AppServerRoleArn"
                },
                ""
              ]
            }
          ]
        },
        {
          "Fn::Not": [
            {
              "Fn::Equals": [
                {
                  "Ref": "AppServerRoleArn"
                },
                "undefined"
              ]
            }
          ]
        }
      ]
    }
  },
  "Resources" : {
    "KeyUsage" : {
      "Type" : "AWS::IAM::Role",
      "Properties" : {
        "AssumeRolePolicyDocument" : {
          "Version" : "2012-10-17",
            "Statement" : [
              {
                "Effect" : "Allow",
                "Principal" : { "Service" : [ "ec2.amazonaws.com" ]},
                "Action" : [ "sts:AssumeRole" ]
              }
            ]
        },
        "Path" : "/",
        "Policies" : [
          {
            "PolicyName" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, "kms-key-role", { "Ref" : "EnvironmentName" } ] ]
            },
            "PolicyDocument" : {
              "Version" : "2012-10-17",
              "Statement" : [
                {
                  "Effect": "Allow",
                  "Action": [ "ssm:DescribeParameters"],
                  "Resource": "*"
                },
                {
                  "Effect": "Allow",
                  "Action": [ "ssm:*"],
                  "Resource": [ { "Fn::Join" : ["", [ "arn:aws:ssm:", { "Ref" : "AWS::Region" }, ":", { "Ref" : "AWS::AccountId" }, ":parameter/", { "Ref" : "EnvironmentName" }, "/", { "Ref" : "ApplicationName" }, "/*" ] ] } ]
                },
                {
                  "Effect": "Allow",
                  "Action": [ "kms:Encrypt", "kms:Decrypt", "kms:ReEncrypt*", "kms:GenerateDataKey*", "kms:DescribeKey"],
                  "Resource": [ { "Fn::Join" : ["", [ "arn:aws:kms:", { "Ref" : "AWS::Region" }, ":", { "Ref" : "AWS::AccountId" }, ":key/", { "Ref" : "KmsEncryptionKey" } ] ] } ]
                }
              ]
            }
          }
        ]
      }
    },
    "KmsEncryptionKey": {
      "Type": "AWS::KMS::Key",
      "Properties": {
        "Description": {
          "Fn::Join": [
            "-",
            [
              {
                "Ref": "CompanyName"
              },
              {
                "Ref": "ApplicationName"
              },
              {
                "Ref": "EnvironmentName"
              }
            ]
          ]
        },
        "EnableKeyRotation": "false",
        "Enabled": "true",
        "KeyPolicy": {
          "Fn::If": [
            "AdminRoleArnSpecified",
            {
              "Fn::If": [
                "AppServerRoleArnSpecified",
                {
                  "Id": "key-consolepolicy-3",
                  "Statement": [
                    {
                      "Action": [
                        "kms:*"
                      ],
                      "Effect": "Allow",
                      "Principal": {
                        "AWS": {
                          "Fn::Join": [
                            "",
                            [
                              "arn:aws:iam::",
                              {
                                "Ref": "AWS::AccountId"
                              },
                              ":root"
                            ]
                          ]
                        }
                      },
                      "Resource": [
                        "*"
                      ],
                      "Sid": "Enable IAM User Permissions"
                    },
                    {
                      "Action": [
                        "kms:Create*",
                        "kms:Describe*",
                        "kms:Enable*",
                        "kms:List*",
                        "kms:Put*",
                        "kms:Update*",
                        "kms:Revoke*",
                        "kms:Disable*",
                        "kms:Get*",
                        "kms:Delete*",
                        "kms:TagResource",
                        "kms:UntagResource",
                        "kms:ScheduleKeyDeletion",
                        "kms:CancelKeyDeletion"
                      ],
                      "Effect": "Allow",
                      "Principal": {
                        "AWS": {
                          "Ref": "AdminRoleArn"
                        }
                      },
                      "Resource": [
                        "*"
                      ],
                      "Sid": "Allow access for Key Administrators"
                    }
                  ],
                  "Version": "2012-10-17"
                },
                {
                  "Id": "key-consolepolicy-3",
                  "Statement": [
                    {
                      "Action": [
                        "kms:*"
                      ],
                      "Effect": "Allow",
                      "Principal": {
                        "AWS": {
                          "Fn::Join": [
                            "",
                            [
                              "arn:aws:iam::",
                              {
                                "Ref": "AWS::AccountId"
                              },
                              ":root"
                            ]
                          ]
                        }
                      },
                      "Resource": [
                        "*"
                      ],
                      "Sid": "Enable IAM User Permissions"
                    },
                    {
                      "Action": [
                        "kms:Create*",
                        "kms:Describe*",
                        "kms:Enable*",
                        "kms:List*",
                        "kms:Put*",
                        "kms:Update*",
                        "kms:Revoke*",
                        "kms:Disable*",
                        "kms:Get*",
                        "kms:Delete*",
                        "kms:TagResource",
                        "kms:UntagResource",
                        "kms:ScheduleKeyDeletion",
                        "kms:CancelKeyDeletion"
                      ],
                      "Effect": "Allow",
                      "Principal": {
                        "AWS": {
                          "Ref": "AdminRoleArn"
                        }
                      },
                      "Resource": [
                        "*"
                      ],
                      "Sid": "Allow access for Key Administrators"
                    }
                  ],
                  "Version": "2012-10-17"
                }
              ]
            },
            {
              "Fn::If": [
                "AppServerRoleArnSpecified",
                {
                  "Id": "key-consolepolicy-3",
                  "Statement": [
                    {
                      "Action": [
                        "kms:*"
                      ],
                      "Effect": "Allow",
                      "Principal": {
                        "AWS": {
                          "Fn::Join": [
                            "",
                            [
                              "arn:aws:iam::",
                              {
                                "Ref": "AWS::AccountId"
                              },
                              ":root"
                            ]
                          ]
                        }
                      },
                      "Resource": [
                        "*"
                      ],
                      "Sid": "Enable IAM User Permissions"
                    }
                  ],
                  "Version": "2012-10-17"
                },
                {
                  "Id": "key-consolepolicy-3",
                  "Statement": [
                    {
                      "Action": [
                        "kms:*"
                      ],
                      "Effect": "Allow",
                      "Principal": {
                        "AWS": {
                          "Fn::Join": [
                            "",
                            [
                              "arn:aws:iam::",
                              {
                                "Ref": "AWS::AccountId"
                              },
                              ":root"
                            ]
                          ]
                        }
                      },
                      "Resource": [
                        "*"
                      ],
                      "Sid": "Enable IAM User Permissions"
                    }
                  ],
                  "Version": "2012-10-17"
                }
              ]
            }
          ]
        }
      }
    },
    "KmsKeyAlias":{
      "Type" : "AWS::KMS::Alias",
      "Condition":"KeyAliasSpecified",
      "Properties": {
        "AliasName": { "Fn::Join" : [ "", [ "alias/", { "Ref" : "KeyAlias" } ] ] },
        "TargetKeyId": { "Ref": "KmsEncryptionKey" }
      }
    },
    "KeyUsageManagedPolicy" : {
      "Type" : "AWS::IAM::ManagedPolicy",
      "Properties" : {
        "Description" : "Policy for allowing access to kms key usage",
        "Path" : "/",
        "PolicyDocument" :   {
          "Version":"2012-10-17",
          "Statement" : [{
            "Effect" : "Allow",
            "Action" : [
              "kms:Encrypt",
              "kms:Decrypt",
              "kms:ReEncrypt*",
              "kms:GenerateDataKey*",
              "kms:DescribeKey"
            ],
            "Resource" : ["*"],
          }]
        }
      }
    }
  },
  "Outputs" : {
    "KmsKeyId" : {
      "Description" : "Id of KMS Key created",
      "Value" : { "Ref": "KmsEncryptionKey" }
    },
    "KmsKeyUsageRole" : {
      "Description" : "Name of KMS Key Role that grants usge",
      "Value" : { "Ref": "KeyUsageManagedPolicy" }
    }
  }
}
