AWSTemplateFormatVersion: '2010-09-09'
Description: Create surfline.tv CloudFront distribution - 1.0.0
Parameters:
  AcmCertificateArn:
    Description: ACM Cert ARN for distribution
    Type: String
  BucketName:
    Description: S3 bucket name to serve as the distribution's origin
    Type: String
  CacheTTL:
    Description: Default cache TTL
    Default: 86400
    Type: Number
  Comment:
    Description: Optional distribution comment
    Default: "surfline.tv"
    Type: String
  DefaultRootObject:
    Description: Default distribution root object
    Default: index.html
    Type: String
  DomainAliases:
    Description: Domain aliases for CF distribution config
    Type: CommaDelimitedList
  LoggingBucket:
    Description: Optional S3 bucket name to enable access log storage
    Default: ""
    Type: String
  OriginAccessIdentity:
    Description: Optional origin access identity to associate with the distribution
    Default: ""
    Type: String
Conditions:
  CommentEntered:
    "Fn::Not":
      - "Fn::Equals":
          - Ref: Comment
          - ""
  DefaultRootObjectPresent:
    "Fn::Not":
      - "Fn::Equals":
          - Ref: DefaultRootObject
          - ""
  LoggingEnabled:
    "Fn::Not":
      - "Fn::Equals":
          - Ref: LoggingBucket
          - ""
  OriginAccessIdentityEntered:
    "Fn::Not":
      - "Fn::Equals":
          - Ref: OriginAccessIdentity
          - ""
Resources:
  CFDistribution:
    Type: AWS::CloudFront::Distribution
    Properties:
      DistributionConfig:
        Aliases:
          Ref: DomainAliases
        Comment:
          "Fn::If":
            - CommentEntered
            - Ref: Comment
            - Ref: AWS::NoValue
        DefaultCacheBehavior:
          AllowedMethods:
            - GET
            - HEAD
          Compress: 'false'
          DefaultTTL:
            Ref: CacheTTL
          ForwardedValues:
            Cookies:
              Forward: none
            QueryString: 'false'
          TargetOriginId: S3Origin
          ViewerProtocolPolicy: redirect-to-https
        DefaultRootObject:
          "Fn::If":
            - DefaultRootObjectPresent
            - Ref: DefaultRootObject
            - Ref: AWS::NoValue
        Enabled: 'true'
        HttpVersion: http2
        Logging:
          "Fn::If":
            - LoggingEnabled
            - Bucket:
                "Fn::Join":
                  - ""
                  -
                    - Ref: LoggingBucket
                    - .s3.amazonaws.com
              Prefix:
                Ref: BucketName
            - Ref: AWS::NoValue
        Origins:
          - Id: S3Origin
            DomainName:
              "Fn::Join":
                - ""
                -
                  - Ref: BucketName
                  - .s3.amazonaws.com
            S3OriginConfig:
              OriginAccessIdentity:
                "Fn::If":
                  - OriginAccessIdentityEntered
                  - Ref: OriginAccessIdentity
                  - Ref: AWS::NoValue
        ViewerCertificate:
          AcmCertificateArn:
            Ref: AcmCertificateArn
          SslSupportMethod: sni-only
Outputs:
  CFDistributionDomainName:
    Description: CloudFront distribution domain name
    Value:
      Fn::GetAtt:
        - CFDistribution
        - DomainName
