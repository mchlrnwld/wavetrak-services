# Stacks

## Prod

CloudFront logging bucket `sl-cloudfrontlogs-prod` (usable for all present and future distributions) deployed via `sl-cloudfront-bucket-prod` stack from template surfline-aws-logging-s3-bucket.json with parameters:
* ApplicationName => cloudfrontlogs
* CompanyName => sl
* EnvironmentName	=> prod

### surfline.tv

`sl-surfline-tv-prod` bucket created via `sl-surfline-tv-bucket-prod` stack created from template surfline-public-s3-bucket.json with parameters:
* ApplicationName => `surfline-tv`
* CompanyName => `sl`
* EnvironmentName	=> `prod`

`sl-surfline-tv-prod-full-access` IAM group created by `sl-surfline-tv-bucketgroup-prod` stack created from template surfline-bucket-management-iam-group.yaml with parameters:
* BucketName => `sl-surfline-tv-prod`

CF distribution in front of the bucket deployed in `sl-surfline-tv-distribution-prod` stack created from template surfline-tv-cloudfront.yaml with parameters:
* AcmCertificateArn => 		`arn:aws:acm:us-east-1:833713747344:certificate/d4f0c28d-ce81-42ab-b2c9-03d8277963cc`
* BucketName => `sl-surfline-tv-prod`
* CacheTTL => `86400`
* Comment	=> `surfline.tv`
* DefaultRootObject => `index.html`
* DomainAliases => `surfline.tv,*.surfline.tv`
* LoggingBucket => `sl-cloudfrontlogs-prod`
* OriginAccessIdentity (none)

### x1.surfline.tv

Same as `surfline.tv` above, but with the following stacks/parameter differences:

`sl-surfline-x1-tv-bucket-prod`:
* ApplicationName => `surfline-x1-tv`

`sl-surfline-x1-tv-bucketgroup-prod`:
* BucketName => `sl-surfline-x1-tv-prod`

`sl-surfline-x1-tv-distribution-prod`:
* BucketName => `sl-surfline-x1-tv-prod`
* Comment	=> `x1.surfline.tv`
* DomainAliases => `x1.surfline.tv`
