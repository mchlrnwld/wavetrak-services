{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "WordPress Server Stack (2.11.3)",
  "Parameters" : {
    "AcmCertificateArn": {
      "Description": "Arn of certificate to use for load balancer",
      "Type": "String"
    },
    "AlertSubscriptionEmail": {
      "Description": "Email to receive instance events",
      "Type": "String"
    },
    "AllServerSecurityGroupId" : {
      "Description" : "The security group used for all servers",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "ApplicationName" : {
      "Description" : "Name of application",
      "Type" : "String",
      "Default": "wordpress"
    },
    "ArtifactBucketName" : {
      "Description" : "Name of bucket storing deployment artifact",
      "Type" : "String"
    },
    "ArtifactName" : {
      "Description" : "Name of artifact to deploy",
      "Type" : "String",
      "Default": "wp-content.tar.gz"
    },
    "BackupBucketName" : {
      "Description" : "Name of bucket storing backup data",
      "Type" : "String"
    },
    "ChefBucketName" : {
      "Description" : "Name of bucket storing Chef configuration",
      "Type" : "String"
    },
    "CompanyName" : {
      "Description" : "Company (Surfline, Buoy Weather)",
      "Type" : "String",
      "Default" : "sl",
      "AllowedValues" : [
        "bw",
        "ft",
        "sl"
      ]
    },
    "DeregTopic": {
      "Description": "The ARN of the DeregTopic",
      "Type": "String",
      "Default": "arn:aws:sns:us-west-1:833713747344:Sturdy-SAND"
    },
    "EnvironmentName" : {
      "Description" : "Name of Environment",
      "Type" : "String",
      "Default" : "prod",
      "AllowedValues" : [
        "common",
        "prod",
        "qa",
        "staging",
        "testing"
      ]
    },
    "InternalServerSecurityGroupId" : {
      "Description" : "The security group used for internal servers",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "RdsClientSecurityGroup" : {
      "Description" : "The security group used for database clients",
      "Type" : "AWS::EC2::SecurityGroup::Id"
    },
    "InstanceType" : {
      "Description" : "Type of instance(s).",
      "Type" : "String",
      "Default" : "t2.micro",
      "AllowedValues" : [
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c3.large",
        "c3.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "c4.large",
        "c4.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "d2.xlarge",
        "g2.2xlarge",
        "g2.2xlarge ",
        "g2.8xlarge",
        "g2.8xlarge ",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "i2.xlarge",
        "m3.2xlarge",
        "m3.large",
        "m3.medium",
        "m3.xlarge",
        "m4.10xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.large",
        "m4.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "r3.large",
        "r3.xlarge",
        "t2.large",
        "t2.large ",
        "t2.medium",
        "t2.micro",
        "t2.small"
      ]
    },
    "KeyName" : {
      "Description" : "Name of an existing EC2-VPC KeyPair",
      "Type" : "AWS::EC2::KeyPair::KeyName"
    },
    "MongoDBName" : {
      "Description" : "Name of the Mongo database to connect to",
      "Type" : "String"
    },
    "MongoConnectionStr" : {
      "Description" : "Connection String of the Mongo database to connect to",
      "Type" : "String"
    },
    "MongoHost" : {
      "Description" : "Name of MongoDB host to connect to",
      "Type" : "String"
    },
    "MongoPort" : {
      "Description" : "Value for MongoDB port to connect to",
      "Type" : "Number",
      "Default" : "27017",
      "MaxValue" : "65535",
      "MinValue" : "1024"
    },
    "MySqlHost" : {
      "Description" : "Name of MySQL host to connect to",
      "Type" : "String"
    },
    "MySqlPassword" : {
      "Description" : "Password for MySQL connection",
      "Type" : "String",
      "NoEcho" : "true"
    },
    "PriSubnet1" : {
      "Description" : "Private subnet 1",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "PriSubnet2" : {
      "Description" : "Private subnet 2",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "ServiceName" : {
      "Description" : "Service name",
      "Type" : "String",
      "Default" : "wordpress"
    },
    "MaxServerCount" : {
      "Description" : "Max number of instances to start",
      "Type" : "Number",
      "Default" : "1",
      "MinValue" : "0",
      "MaxValue" : "10"
    },
    "MinServerCount" : {
      "Description" : "Min number of instances to start",
      "Type" : "Number",
      "Default" : "1",
      "MinValue" : "0",
      "MaxValue" : "10"
    },
    "StaticContentBucket" : {
      "Description" : "S3 bucket containing static content",
      "Type" : "String"
    },
    "VpcId" : {
      "Description" : "VPC ID",
      "Type" : "AWS::EC2::VPC::Id"
    },
    "WordPressVersion" : {
      "Description" : "Version of WordPress to deploy",
      "Type" : "String",
      "ConstraintDescription" : "Please enter a valid Semantic Versioning number",
      "AllowedPattern" : "[0-9].[0-9].[0-9]",
      "Default": "4.5.3"
    }
  },
  "Mappings" : {
    "AWSAMI" : {
      "us-east-1" : { "AMI" : "ami-6869aa05" },
      "us-west-1" : { "AMI" : "ami-31490d51" },
      "us-west-2" : { "AMI" : "ami-7172b611" }
    },
    "CompanyNames" : {
      "bw" : {
        "CompanyFullName" : "buoyweather"
      },
      "ft" : {
        "CompanyFullName" : "fishtrack"
      },
      "sl" : {
        "CompanyFullName" : "surfline"
      }
    }
  },
  "Resources" : {
    "ElbSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "ElbSecurityGroup", { "Ref" : "EnvironmentName" } ] ] },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "elb", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ],
        "VpcId" : { "Ref" : "VpcId" },
        "SecurityGroupIngress" : [
          { "IpProtocol": "tcp", "FromPort": "80",  "ToPort": "80",  "CidrIp": "0.0.0.0/0" },
          { "IpProtocol": "tcp", "FromPort": "443",  "ToPort": "443",  "CidrIp": "0.0.0.0/0" }
        ],
        "SecurityGroupEgress" : [
          { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" }
        ]
      }
    },
    "ServerSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "ServerSecurityGroup", { "Ref" : "EnvironmentName" } ] ] },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "servers", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ],
        "VpcId" : { "Ref" : "VpcId" },
        "SecurityGroupIngress" : [
           { "IpProtocol" : "tcp", "FromPort" : "80",  "ToPort" : "80",  "SourceSecurityGroupId" : { "Ref" : "ElbSecurityGroup" } }
         ],
        "SecurityGroupEgress" : [
          { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" }
        ]
      },
      "DependsOn" : [ "ElbSecurityGroup" ]
    },
    "LoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "LoadBalancerName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] },
        "Scheme": "internal",
        "SecurityGroups" : [
          { "Ref": "ElbSecurityGroup" }
        ],
        "Instances" : [ ],
        "Subnets" : [
          { "Ref": "PriSubnet1" },
          { "Ref": "PriSubnet2" }
        ],
        "Listeners" : [
          { "LoadBalancerPort": "80", "InstancePort": "80", "Protocol": "HTTP", "InstanceProtocol": "HTTP" },
          { "LoadBalancerPort": "443", "InstancePort": "80", "Protocol": "HTTPS", "InstanceProtocol": "HTTP", "SSLCertificateId": { "Ref": "AcmCertificateArn" }}
        ],
        "HealthCheck": {
          "Target": "HTTP:80/news/",
          "HealthyThreshold": "2",
          "UnhealthyThreshold": "10",
          "Interval": "10",
          "Timeout": "5"
        },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ "servers", { "Ref" : "CompanyName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] }
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" }
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] }
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" }
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" }
          }
        ]
      }
    },
    "InstanceRole" : {
      "Type" : "AWS::IAM::Role",
      "Properties" : {
        "AssumeRolePolicyDocument" : {
          "Version" : "2012-10-17",
            "Statement" : [
              {
                "Effect" : "Allow",
                "Principal" : { "Service" : [ "ec2.amazonaws.com" ]},
                "Action" : [ "sts:AssumeRole" ]
              }
            ]
        },
        "Path" : "/",
        "Policies" : [
          {
            "PolicyName" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, "server-role", { "Ref" : "EnvironmentName" } ] ]
            },
            "PolicyDocument" : {
              "Version" : "2012-10-17",
              "Statement" : [
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:List*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ArtifactBucketName" }, "/wordpress" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:Get*", "s3:List*" ],
                  "Resource": [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ArtifactBucketName" }, "/wordpress/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:List*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ChefBucketName" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:Get*", "s3:List*" ],
                  "Resource": [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "ChefBucketName" }, "/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:ListBucket" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "StaticContentBucket" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:DeleteObject", "s3:Get*", "s3:Put*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "StaticContentBucket" }, "/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:ListBucket" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "BackupBucketName" } ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "s3:DeleteObject", "s3:Get*", "s3:Put*" ],
                  "Resource" : [ { "Fn::Join" : [ "", [ "arn:aws:s3:::", { "Ref" : "BackupBucketName" }, "/*" ] ] } ]
                },
                {
                  "Effect" : "Allow",
                  "Action" : [ "ec2:CreateTags", "ec2:DescribeInstances", "ec2:DescribeTags" ],
                  "Resource" : [ "*" ]
                }
              ]
            }
          }
        ]
      }
    },
    "InstanceProfile" : {
      "Type" : "AWS::IAM::InstanceProfile",
      "Properties" : {
        "Path" : "/",
        "Roles" : [
          { "Ref" : "InstanceRole" }
        ]
      }
    },
    "LaunchServer" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "InstanceType" : { "Ref" : "InstanceType" },
        "KeyName" : { "Ref" : "KeyName" },
        "ImageId" : { "Fn::FindInMap" : [ "AWSAMI", { "Ref" : "AWS::Region" }, "AMI" ] },
        "IamInstanceProfile" : { "Ref" : "InstanceProfile" },
        "SecurityGroups" : [
          { "Ref" : "AllServerSecurityGroupId" },
          { "Ref" : "InternalServerSecurityGroupId" },
          { "Ref" : "ServerSecurityGroup" },
          { "Ref" : "RdsClientSecurityGroup" }
        ],
        "UserData" : {
          "Fn::Base64" : {
            "Fn::Join" : [
              "",
              [
                "#cloud-config\n",
                "repo_update: true\n",
                "repo_upgrade: all\n",
                "packages:\n",
                "- wget\n",
                "- python\n",
                "- python-pip\n",
                "- python-boto\n",
                "- awscli\n",
                "- httpd24\n",
                "- mysql\n",
                "- openssl-devel\n",
                "- php54\n",
                "- php54-devel\n",
                "- php54-gd\n",
                "- php54-mysql\n",
                "- php-pear\n",
                "write_files:\n",
                "  - path: /etc/httpd/conf.d/environment.conf\n",
                "    content: |\n",
                "      SetEnv MONGO_CONNECTION_STR \"", { "Ref" : "MongoConnectionStr" }, "\"\n",
                "      SetEnv MONGO_DB_NAME \"", { "Ref" : "MongoDBName" }, "\"\n",
                "      SetEnv MONGO_PORT_27017_TCP_ADDR \"", { "Ref" : "MongoHost" }, "\"\n",
                "      SetEnv MONGO_PORT_27017_TCP_PORT \"", { "Ref" : "MongoPort" }, "\"\n",
                "  - path: /etc/cron.d/wp-uploads-sync\n",
                "    content: |\n",
                "      */15 * * * * root aws s3 sync --delete /var/www/html/news/wp-content/uploads s3://", { "Ref" : "StaticContentBucket" }, "/uploads > /var/log/wp-uploads-sync.log\n",
                "  - path: /root/.my.cnf\n",
                "    content: |\n",
                "      [client]\n",
                "      user=root\n",
                "      password=", { "Ref" : "MySqlPassword" }, "\n",
                "  - path: /usr/local/bin/mysql-sync\n",
                "    content: |\n",
                "      #!/bin/bash\n",
                "      mkdir -p /tmp/mysqldump\n",
                "      mysqldump -h ", { "Ref" : "MySqlHost" }, " wordpress > /tmp/mysqldump/wordpress.sql\n",
                "      aws s3 cp /tmp/mysqldump/wordpress.sql s3://", { "Ref" : "BackupBucketName" }, "\n",
                "  - path: /etc/cron.d/mysql-sync\n",
                "    content: |\n",
                "      */15 * * * * root bash /usr/local/bin/mysql-sync > /var/log/mysql-sync.log\n",
                "  - path: /var/www/html/news/.htaccess\n",
                "    permissions: '0644'\n",
                "    content: |\n",
                "      # BEGIN WordPress\n",
                "      <IfModule mod_rewrite.c>\n",
                "      RewriteEngine On\n",
                "      RewriteBase /\n",
                "      RewriteRule ^index\\.php$ - [L]\n",
                "      RewriteCond %{REQUEST_FILENAME} !-f\n",
                "      RewriteCond %{REQUEST_FILENAME} !-d\n",
                "      RewriteRule . /news/index.php [L]\n",
                "      RewriteRule ^wp-admin/includes/ - [F,L]\n",
                "      RewriteRule !^wp-includes/ - [S=3]\n",
                "      RewriteRule ^wp-includes/[^/]+\\.php$ - [F,L]\n",
                "      RewriteRule ^wp-includes/js/tinymce/langs/.+\\.php - [F,L]\n",
                "      RewriteRule ^wp-includes/theme-compat/ - [F,L]\n",
                "      </IfModule>\n",
                "      \n",
                "      # END WordPress\n",
                "runcmd:\n",
                "- yum groupinstall -y \"Development tools\"\n",
                "- pecl install mongo\n",
                "- echo \"extension=mongo.so\" > /etc/php.d/docker-php-ext-mongo.ini\n",
                "- curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar\n",
                "- chmod +x wp-cli.phar\n",
                "- mv wp-cli.phar /usr/local/bin/wp\n",
                "- curl -o wordpress.tar.gz -SL https://wordpress.org/wordpress-", { "Ref" : "WordPressVersion" }, ".tar.gz\n",
                "- tar -xzf wordpress.tar.gz --xform 's/^wordpress\\///' -C /var/www/html/news\n",
                "- rm -rf /var/www/html/news/wp-content/plugins/akismet\n",
                "- rm -rf /var/www/html/news/wp-content/plugins/hello.php\n",
                "- mkdir /var/www/html/news/wp-content/uploads\n",
                "- time aws s3 sync s3://", { "Ref" : "StaticContentBucket" }, " /var/www/html/news/wp-content\n",
                "- aws --region us-west-1 s3 cp s3://", { "Ref" : "ArtifactBucketName" }, "/wordpress/", { "Ref" : "ArtifactName" }, " wp-content.tar.gz\n",
                "- tar -xzf wp-content.tar.gz -C /var/www/html/news/wp-content\n",
                "- chown -R apache:apache /var/www/html/news/wp-content/uploads\n",
                "- chmod -R 775 /var/www/html/news/wp-content/uploads\n",
                "- aws --region us-west-1 s3 cp s3://", { "Ref" : "ChefBucketName" }, "/wordpress/", { "Ref" : "CompanyName" }, "-wp-config-", { "Ref" : "EnvironmentName" }, ".php", " /var/www/html/news/wp-config.php\n",
                "- chown apache:apache /var/www/html/news/wp-config.php\n",
                "- chmod 600 /var/www/html/news/wp-config.php\n",
                "- aws --region us-west-1 s3 cp s3://", { "Ref" : "ChefBucketName" }, "/wordpress/php.ini /etc/php.ini\n",
                "- sed -i \"s/AllowOverride None/AllowOverride All/gI\" /etc/httpd/conf/httpd.conf\n",
                "- chkconfig httpd on\n",
                "- service httpd restart\n",
                "- curl -L https://www.chef.io/chef/install.sh | bash -s -- -v 12.19.36\n",
                "- mkdir -p /var/log/chef /etc/chef\n",
                "- aws --region us-west-1 s3 sync s3://", { "Ref" : "ChefBucketName" }, "/config /etc/chef/\n",
                "- python /etc/chef/rename_instance\n"
              ]
            ]
          }
        }
      }
    },
    "ServerAutoScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "LaunchConfigurationName" : { "Ref" : "LaunchServer" },
        "MinSize" : "0",
        "MaxSize" : { "Ref" : "MaxServerCount" },
        "DesiredCapacity" : { "Ref" : "MinServerCount" },
        "HealthCheckGracePeriod": "300",
        "HealthCheckType" : "ELB",
        "NotificationConfigurations" : [
          {
            "TopicARN" : { "Ref" : "AutoScalingNotification" },
            "NotificationTypes" : [
              "autoscaling:EC2_INSTANCE_LAUNCH",
              "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
              "autoscaling:EC2_INSTANCE_TERMINATE",
              "autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
            ]
          },
          {
            "TopicARN" : { "Ref" : "DeregTopic" },
            "NotificationTypes" : [
              "autoscaling:EC2_INSTANCE_TERMINATE"
            ]
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" } ] ] },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Environment",
            "Value" : { "Ref" : "EnvironmentName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Company",
            "Value" : { "Fn::FindInMap" : [ "CompanyNames", { "Ref" : "CompanyName" }, "CompanyFullName" ] },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Application",
            "Value" : { "Ref" : "ApplicationName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key" : "Service",
            "Value" : { "Ref" : "ServiceName" },
            "PropagateAtLaunch" : "true"
          },
          {
            "Key": "recipes",
            "Value": { "Fn::Join": [ "", [ "role[base],", "role[", { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }] ] }, "]" ] ] },
            "PropagateAtLaunch": "true"
          },
          {
            "Key": "type",
            "Value": { "Fn::Join" : [ "-", [ { "Ref" : "ApplicationName" }, { "Ref" : "ServiceName" }] ] },
            "PropagateAtLaunch": "true"
          }
        ],
        "LoadBalancerNames" : [
          { "Ref": "LoadBalancer" }
        ],
        "VPCZoneIdentifier" : [
          { "Ref" : "PriSubnet1" },
          { "Ref" : "PriSubnet2" }
        ],
        "MetricsCollection" : [
          {
            "Granularity" : "1Minute",
            "Metrics" : [
              "GroupMinSize",
              "GroupMaxSize"
            ]
          }
        ]
      }
    },
    "AutoScalingNotification": {
      "Type": "AWS::SNS::Topic",
      "Properties": {
        "DisplayName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "topic" ] ] },
        "Subscription": [
          {
            "Endpoint": { "Ref": "AlertSubscriptionEmail" },
            "Protocol": "email"
          }
        ],
        "TopicName": { "Fn::Join" : [ "-", [ { "Ref" : "CompanyName" }, { "Ref" : "ServiceName" }, { "Ref" : "ApplicationName" }, { "Ref" : "EnvironmentName" }, "topic" ] ] }
      }
    }
  },
  "Outputs" : {
    "LoadBalancerCname" : {
      "Description" : "CNAME of the load balancer",
      "Value" : { "Fn::GetAtt" : [ "LoadBalancer", "DNSName" ] }
    }
  }
}
