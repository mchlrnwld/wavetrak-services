# Surfline CMS Spec SNS Subscription

The template `surfline-cms-spec-lambda-iam-role.yml` is used to deploy the `sl-cms-spec-lambda-iam-role-prod` stack. This stack consists of an IAM role that has access to the S3 Bucket.


The template `surfline-cms-spec-sns-subscription.yml` is used to deploy the `sl-cms-spec-sns-subscription-prod` stack. This stack consists of a SNS Subscription, and a Lambda Permission to allow the SNS Subscription to execute the supplied lambda function.

## Prod

### IAM Role
`sl-cms-spec-lambda-iam-role-prod` is created from template `surfline-cms-spec-lambda-iam-role.yml` with parameters:

Parameter       | Value
|:--------------|:-----
CustomerName    | sl
ApplicationName | cms-spec
EnvironmentName | prod
BucketName      | surfline-science-s3-prod
BucketPath      | spot_cms

### Lambda Warmer Rule
`sl-cms-spec-lambda-rule-prod` is created from template `surfline-cms-spec-lambda-rule.yml` with parameters:

Parameter           | Value
|:------------------|:-----
CustomerName        | sl
ApplicationName     | cms-spec
EnvironmentName     | prod
LambdaFunction      | FIXME_HERE


### SNS Subscription
`sl-cms-spec-sns-subscription-prod` is created from template `surfline-cms-spec-sns-subscription.yml` with parameters:

Parameter     | Value
|:------------|:-----
SubscribedTopic | spot_details_updated_prod
LambdaFunction | python_get_best_wx
