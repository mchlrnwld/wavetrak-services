# ElasticSearch Templates

The following templates can be used to deploy both a single node and multi-node Elastic Search Clusters.

## No Dedicated Master Nodes
The `surfline-elasticsearch-no-master-cluster.yaml` template is used to deploy a ElasticSearch Cluster without a dedicated master node.

#### Parameters
* CompanyName => sl
* ApplicationName => es
* EnvironmentName => sbox
* ESVersion => 5.1
* ESSnapshotStartHour => 0
* ESVolumeSize => 15  (Size in GB)
* ESInstanceCount => 1
* ESInstanceType => t2.small.elasticsearch

#### Exports
* ElasticsearchDomainArn
* ElasticsearchIAMRoleArn `(This is the ARN to be used by any micro service that will talk to the ES Cluster)`
* ElasticsearchDomainEndpoint

#### Usage

The following example will launch the AWS infrastructure necessary for the
Elasticsearch cluster in the logical environment specified using
default values:

##### Environment: sandbox

```sh
export ENV="sbox"
aws cloudformation create-stack                                          \
  --stack-name "sl-common-es51-${ENV}"                                   \
  --capabilities 'CAPABILITY_NAMED_IAM'                                  \
  --template-body 'file://surfline-elasticsearch-no-master-cluster.yaml' \
  --tags "Key=EnvironmentName,Value=${ENV}"                              \
  --parameters                                                           \
    "ParameterKey=CompanyName,         ParameterValue=sl"                \
    "ParameterKey=ApplicationName,     ParameterValue=es"                \
    "ParameterKey=EnvironmentName,     ParameterValue=${ENV}"            \
    "ParameterKey=ESVersion,           ParameterValue=5.1"               \
    "ParameterKey=ESSnapshotStartHour, ParameterValue=0"                 \
    "ParameterKey=ESVolumeSize,        ParameterValue=15"                \
    "ParameterKey=ESInstanceCount,     ParameterValue=1"                 \
    "ParameterKey=ESInstanceType,      ParameterValue=t2.small.elasticsearch"

# Check `create-stack` status
aws cloudformation describe-stack-events --stack-name "sl-common-es51-${ENV}" \
  | jq '.StackEvents[] | {Timestamp, ResourceStatus, ResourceType, LogicalResourceId, ResourceStatusReason}'
```


## Dedicated Master Nodes
The `surfline-elasticsearch-dedicated-master-cluster.yaml` template is used to deploy a ElasticSearch Cluster with a dedicated master nodes. This stack requires that both `ESInstanceCount` and `ESMasterInstanceCount` be multiples of 2 if `ESZoneAwarenessEnabled = true`.

#### Parameters
* CompanyName => sl
* ApplicationName => es
* EnvironmentName => sbox
* ESVersion => 5.1
* ESSnapshotStartHour => 0
* ESVolumeSize => 15  `(Size in GB)`
* ESInstanceType => t2.small.elasticsearch
* ESInstanceCount => 2 `(If ESZoneAwarenessEnabled = true this parameter MUST be a multiple of 2)`
* ESMasterInstanceCount => 2 `(If ESZoneAwarenessEnabled = true this parameter MUST be a multiple of 2)`
* ESMasterInstanceType => t2.small.elasticsearch
* ESZoneAwarenessEnabled => true

#### Exports
* ElasticsearchDomainArn
* ElasticsearchIAMRoleArn `(This is the ARN to be used by any micro service that will talk to the ES Cluster)`
* ElasticsearchDomainEndpoint

#### Usage

The following example will launch the AWS infrastructure necessary for the
Elasticsearch cluster in the logical environment specified:

##### Environment: staging

```sh
export ENV="staging"
aws cloudformation create-stack                                                  \
  --stack-name "sl-common-es51-${ENV}"                                           \
  --capabilities 'CAPABILITY_NAMED_IAM'                                          \
  --template-body 'file://surfline-elasticsearch-dedicated-master-cluster.yaml'  \
  --tags "Key=EnvironmentName,Value=${ENV}"                                      \
  --parameters                                                                   \
    "ParameterKey=CompanyName,            ParameterValue=sl"                     \
    "ParameterKey=ApplicationName,        ParameterValue=es"                     \
    "ParameterKey=EnvironmentName,        ParameterValue=${ENV}"                 \
    "ParameterKey=ESVersion,              ParameterValue=5.1"                    \
    "ParameterKey=ESSnapshotStartHour,    ParameterValue=0"                      \
    "ParameterKey=ESVolumeSize,           ParameterValue=15"                     \
    "ParameterKey=ESInstanceType,         ParameterValue=t2.small.elasticsearch" \
    "ParameterKey=ESInstanceCount,        ParameterValue=1"                      \
    "ParameterKey=ESMasterInstanceType,   ParameterValue=t2.small.elasticsearch" \
    "ParameterKey=ESMasterInstanceCount,  ParameterValue=2"                      \
    "ParameterKey=ESZoneAwarenessEnabled, ParameterValue=false"

# Check `create-stack` status
aws cloudformation describe-stack-events --stack-name "sl-common-es51-${ENV}" \
  | jq '.StackEvents[] | {Timestamp, ResourceStatus, ResourceType, LogicalResourceId, ResourceStatusReason}'
```

##### Environment: production

```sh
export ENV="prod"
aws cloudformation create-stack                                                  \
  --stack-name "sl-common-es51-${ENV}"                                           \
  --capabilities 'CAPABILITY_NAMED_IAM'                                          \
  --template-body 'file://surfline-elasticsearch-dedicated-master-cluster.yaml'  \
  --tags "Key=EnvironmentName,Value=${ENV}"                                      \
  --parameters                                                                   \
    "ParameterKey=CompanyName,            ParameterValue=sl"                     \
    "ParameterKey=ApplicationName,        ParameterValue=es"                     \
    "ParameterKey=EnvironmentName,        ParameterValue=${ENV}"                 \
    "ParameterKey=ESVersion,              ParameterValue=5.1"                    \
    "ParameterKey=ESSnapshotStartHour,    ParameterValue=0"                      \
    "ParameterKey=ESVolumeSize,           ParameterValue=15"                     \
    "ParameterKey=ESInstanceType,         ParameterValue=t2.small.elasticsearch" \
    "ParameterKey=ESInstanceCount,        ParameterValue=2"                      \
    "ParameterKey=ESMasterInstanceType,   ParameterValue=t2.small.elasticsearch" \
    "ParameterKey=ESMasterInstanceCount,  ParameterValue=2"                      \
    "ParameterKey=ESZoneAwarenessEnabled, ParameterValue=true"

# Check `create-stack` status
aws cloudformation describe-stack-events --stack-name "sl-common-es51-${ENV}" \
  | jq '.StackEvents[] | {Timestamp, ResourceStatus, ResourceType, LogicalResourceId, ResourceStatusReason}'
```


## Gotchas

The following "gotchas" exist with Elasticsearch on AWS:

* When using dedicated masters, you must have at least `2` master nodes regardless of zone awareness.
* The `t2.micro.elasticsearch` instance type is not supported for ES 5.1.
* You must proxy Kibana through a service within AWS that can either sign requests or has IAM access to your ES domain. More info [here](https://aws.amazon.com/blogs/security/how-to-control-access-to-your-amazon-elasticsearch-service-domain/).



## Querying Elasticsearch

Elasticsearch on AWS requires that you sign requests. (You cannot directly query Elasticsearch) See this [Link](https://aws.amazon.com/blogs/security/how-to-control-access-to-your-amazon-elasticsearch-service-domain/) for more info on how to sign requests using the SDKs.
