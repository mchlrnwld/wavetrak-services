# jnlp-slave-docker

## Description

The `jnlp-slave-docker` image is an extension of `jenkinsci/jnlp-slave`
including additional tools required for building various Surfline projects.


## Tools Included

* awscli
* docker
* [inspec](https://github.com/chef/inspec)
* jq
* [assume-role](https://github.com/remind101/assume-role)
  * Depends on awscli installation


## Usage

It is intended to be used in conjunction with the [Amazon EC2 Container Service Plugin][df06bbce]

### assume-role

Containers can assume IAM roles by specifying a ~/.aws/roles yaml configuration file in the following form:
```
rolefriendlyname:
  role: arnhere
anotherrole:
  role: arnhere
```

and then executing a command like `eval $(assume-role rolefriendlyname)`


## Testing

A simple `test.sh` script is included to define a few simple checks of expected
tools.


### Pre-reqs

#### Tools

```shell
pip install awscli
brew cask install docker
```

#### AWS Login

```shell
export AWS_PROFILE="surfline-prod"
$(aws ecr get-login --region us-west-1)
```


### Executing tests

```shell
VERSION="1.0.0"
docker run --rm \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -i \
  -t 833713747344.dkr.ecr.us-west-1.amazonaws.com/sl-jenkins-builders/jnlp-slave-docker:${VERSION} \
  /test.sh
```


### Sample results

```
FIXME
```


## Building / Releasing

```shell
VERSION="1.0.0"
docker build -t "sl-jenkins-builders/jnlp-slave-docker" .
docker tag \
  "sl-jenkins-builders/jnlp-slave-docker" \
  "833713747344.dkr.ecr.us-west-1.amazonaws.com/sl-jenkins-builders/jnlp-slave-docker:${VERSION}"
docker push "833713747344.dkr.ecr.us-west-1.amazonaws.com/sl-jenkins-builders/jnlp-slave-docker:${VERSION}"
```


## TODO

* Move the build / deploy of this image into Jenkins
* Consider whether this is appropriate in `surfline-sturdy-infrastructure`, it's
  own repo, or something else

  [df06bbce]: https://wiki.jenkins-ci.org/display/JENKINS/Amazon+EC2+Container+Service+Plugin "Link to Amazon EC2 Container Service Plugin on Jenkins wiki"
