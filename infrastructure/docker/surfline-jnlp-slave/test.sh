set -e
echo "Running Tests in $(pwd)"
echo "Container user info: $(id)"
echo "Docker status: $(docker ps -qa 1>/dev/null && echo running)"
echo "Docker version: $(docker version 2>&1 | grep Version | awk ' { print $2 } ')"
echo "Number of other running containers: $(docker ps -qa 2>&1 | grep -v ^Cannot | wc -l)"
echo "AWS CLI version: $(aws --version 2>&1)"
echo "OK"
exit 0
