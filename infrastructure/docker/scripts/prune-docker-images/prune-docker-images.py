#!/usr/bin/env python2.7

import re
import subprocess as sp
import logging as log

# Identified constants
NR_SCRIPT = "/usr/local/bin/record_analytics.sh"
DOCKR_OUTPT_FILE = "/tmp/tmp_docker_output_file.txt"
LOG_FILE ="/tmp/docker_cron_log_file.txt"

# Set up logger
log.basicConfig(filename=LOG_FILE, level=log.ERROR)

# Define send_analytics function for later
def send_analytics(operation, name):
    try:
        sp.call([NR_SCRIPT, "-f", "-v", "DockerCleanupEvent", operation, name])
    except:
        log.error("Could not access {}, or script execution failed" \
        .format(NR_SCRIPT))
        exit(1)

if __name__ == "__main__":

    # Run docker prune and redirect the stdout to a tmp file
    try:
        with open(DOCKR_OUTPT_FILE, "w") as file:
            sp.call(["sudo", \
            "/usr/bin/docker", \
            "system", \
            "prune", \
            "-af"], \
            stdout=file)
    except:
        log.error("Could not execute docker prune, or could not write to {}" \
        .format(DOCKR_OUTPT_FILE))

    # Read output from the tmp file
    try:
        with open(DOCKR_OUTPT_FILE) as file:
            file_text = file.read()
    except FileNotFoundError as e:
        log.error("Could not access the output file. \
        \n Looking for file in {}: {}" \
        .format(DOCKR_OUTPT_FILE, e))

    # Begin parsing

    ## Container parsing section
    raw_cntnr_str = re.search(r"^Deleted\sContainers:.*Deleted\s", \
        file_text, \
        re.MULTILINE | re.DOTALL)
    if raw_cntnr_str:
        cntnr_section_str = raw_cntnr_str.group()
        try:
            cntnr_del_list = cntnr_section_str.split("\n")[1:-2]
        except IndexError as e:
            log.error("Could not extract value from deletion list {0}: {1}"
            .format(cntnr_section_str, e))

        ### Log deleted container messages to New Relic
        for cntnr_name in cntnr_del_list:
            send_analytics("operation", "deleted-container--{}" \
            .format(str(cntnr_name)))
    else:
        send_analytics("operation", "no-container-to-delete")

    ##  Image parsing section
    raw_img_list = re.findall(r"deleted:\s.*", file_text)
    if raw_img_list:
        img_del_list = raw_img_list
        strpd_img_del_list = [img.replace(" ", "_") for img in img_del_list]

        ### Log image container messages to New Relic
        for img_name in strpd_img_del_list:
            send_analytics("operation", "deleted-image--{}" \
            .format(str(img_name)))
    else:
        send_analytics("operation", "no-images-to-delete")

    ##  Reclaimed space section
    raw_rclmd_spce_str = re.search("Total\sreclaimed\sspace.*", file_text) \
        .group() \
        .replace(" ", "-")

    ### Log image reclaimed space messages to New Relic
    if raw_rclmd_spce_str:
        send_analytics("operation", "reclaimed-space--{}" \
        .format(str(raw_rclmd_spce_str)))
    else:
        send_analytics("operation", "empty-docker-output-file")
