provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-editorial-cms/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-editorial-cms" {
  source = "../../"

  company     = "sl"
  application = "editorial-cms"
  environment = "staging"

  ecs_cluster_name = "sl-ecs-editorial-cms-staging"
  default_vpc      = "vpc-981887fd"
  availability_zones = [
    "us-west-1b",
    "us-west-1c"
  ]
  internal_sg_group = "sg-90aeaef5"
  multiclient_sg    = "sg-057f870587bcb8e40"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  lb_subnets = [
    "subnet-f4d458ad",
    "subnet-0b09466e"
  ]

  ssl_certificate_arn = "arn:aws:acm:us-west-1:665294954271:certificate/a926595c-f282-4435-be22-3cfaf907b459"
  key_name            = "sturdy-surfline-dev"
  artifacts_bucket    = "sl-artifacts-staging"

  lb_ingress_cidr_block = [
    "0.0.0.0/0"
  ]

  instance_type             = "t2.medium"
  wordpress_td_count        = 2
  wordpress_td_count_max    = 2
  wpadmin_td_count          = 2
  varnish_td_count          = 2
  min_instance_count        = 2
  task_deregistration_delay = 3
  rds_performance_insights  = false
  rds_deletion_protection   = false

  # chef
  chef_bucket = "sl-chef-staging"
  dereg_topic = "arn:aws:sns:us-west-1:665294954271:Sturdy-SAND"

  internal_dns_zone_id    = "Z3JHKQ8ELQG5UE"
  internal_dns_name       = "internal-editorial-cms.staging.surfline.com"
  short_internal_dns_name = "int-edit-cms.staging.surfline.com"

  elasticsearch_arn = "arn:aws:es:us-west-1:665294954271:domain/sl-es-staging"

  environment_url = "staging.surfline.com"

  custom_cdn_authorization_header = "Basic c3VyZmxpbmU6ZmxvYXRlcg=="
}
