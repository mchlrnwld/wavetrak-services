provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-editorial-cms/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-editorial-cms" {
  source = "../../"

  company     = "sl"
  application = "editorial-cms"
  environment = "prod"

  ecs_cluster_name = "sl-ecs-editorial-cms-prod"
  default_vpc      = "vpc-116fdb74"
  availability_zones = [
    "us-west-1b",
    "us-west-1c"
  ]
  internal_sg_group = "sg-a5a0e5c0"
  vpn_sg_group      = "sg-cfa0e5aa"
  sg_all_servers    = "sg-a4a0e5c1"
  multiclient_sg    = "sg-006a1f199e16d61f9"

  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0"
  ]
  lb_subnets = [
    "subnet-debb6987",
    "subnet-baab36df"
  ]

  ssl_certificate_arn = "arn:aws:acm:us-west-1:833713747344:certificate/691e76fd-f62d-4380-92cd-ac22f36a8580"
  key_name            = "sturdy_surfline_id_rsa"
  artifacts_bucket    = "sl-artifacts-prod"

  lb_ingress_cidr_block = [
    "0.0.0.0/0"
  ]

  instance_type               = "m4.large"
  wordpress_td_count          = 4
  wordpress_td_count_max      = 8
  wpadmin_td_count            = 2
  varnish_td_count            = 2
  min_instance_count          = 2
  task_deregistration_delay   = 3
  rds_instance_class          = "db.m5.4xlarge"
  rds_allocated_storage       = 100
  rds_multi_az                = true
  rds_backup_retention_period = 35
  rds_performance_insights    = true
  rds_deletion_protection     = true

  # chef
  chef_bucket = "sl-chef-prod"
  dereg_topic = "arn:aws:sns:us-west-1:833713747344:Sturdy-SAND"

  internal_dns_zone_id    = "ZY7MYOQ65TY5X"
  internal_dns_name       = "internal-editorial-cms.surfline.com"
  short_internal_dns_name = "int-edit-cms.surfline.com"

  elasticsearch_arn = "arn:aws:es:us-west-1:833713747344:domain/sl-es-prod"

  environment_url = "www.surfline.com"

  custom_cdn_authorization_header = ""
}
