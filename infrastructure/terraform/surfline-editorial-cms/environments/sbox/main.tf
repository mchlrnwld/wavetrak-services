provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-editorial-cms/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-editorial-cms" {
  source = "../../"

  company     = "sl"
  application = "editorial-cms"
  environment = "sandbox"

  ecs_cluster_name = "sl-ecs-editorial-cms-sandbox"
  default_vpc      = "vpc-981887fd"
  availability_zones = [
    "us-west-1b",
    "us-west-1c"
  ]
  internal_sg_group = "sg-90aeaef5"
  multiclient_sg    = "sg-03888cba167954624"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  lb_subnets = [
    "subnet-f4d458ad",
    "subnet-0b09466e"
  ]

  ssl_certificate_arn = "arn:aws:acm:us-west-1:665294954271:certificate/b5d003ab-5256-4737-9333-c07e7f817a98"
  key_name            = "sturdy-surfline-dev"
  artifacts_bucket    = "sl-artifacts-dev"

  lb_ingress_cidr_block = [
    "0.0.0.0/0"
  ]

  instance_type             = "t2.medium"
  wordpress_td_count        = 1
  wordpress_td_count_max    = 1
  wpadmin_td_count          = 1
  varnish_td_count          = 1
  min_instance_count        = 1
  task_deregistration_delay = 3
  rds_performance_insights  = false
  rds_deletion_protection   = false

  # chef
  chef_bucket = "sl-chef-sbox"
  dereg_topic = "arn:aws:sns:us-west-1:665294954271:Sturdy-SAND"

  internal_dns_zone_id    = "Z3DM6R3JR1RYXV"
  internal_dns_name       = "internal-editorial-cms.sandbox.surfline.com"
  short_internal_dns_name = "int-edit-cms.sandbox.surfline.com"

  elasticsearch_arn = "arn:aws:es:us-west-1:665294954271:domain/sl-es-sbox"

  environment_url = "sandbox.surfline.com"

  custom_cdn_authorization_header = "Basic c3VyZmxpbmU6ZmxvYXRlcg=="
}
