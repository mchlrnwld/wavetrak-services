# Article Update SNS

# SNS topic receiving WP post update notifications
# Triggers Feed ETL and Cache Clearing lambda functions
module "article_update_sns" {
  source = "../modules/aws/sns"

  environment = var.environment
  topic_name  = "${var.company}-${var.application}-article-update-sns"
}

# Travel Page Update SNS

# SNS topic receiving WP post update notifications
# Triggers Feed ETL and Cache Clearing lambda functions
module "travel_page_update_sns" {
  source = "../modules/aws/sns"

  environment = var.environment
  topic_name  = "${var.company}-${var.application}-travel-page-update-sns"
}

# Travel Page ETL Lambda
module "lambda_travel_page_etl" {
  source = "../modules/aws/lambda-with-sns"

  company     = var.company
  application = var.application
  environment = var.environment

  function_name   = "travel-page-etl"
  service_handler = "service.handler"
  runtime         = "python2.7"
  memory_size     = 128
  timeout         = 30

  artifacts_bucket  = var.artifacts_bucket
  instance_subnets  = var.instance_subnets
  internal_sg_group = var.internal_sg_group
  sg_all_servers    = var.sg_all_servers
}

# Create policy allowing access to Elasticsearch
resource "aws_iam_policy" "travel_page_etl_es_policy" {
  name        = "${var.company}-${var.application}-travel-page-etl-es-policy-${var.environment}"
  description = "policy to access elasticsearch from travel page etl"
  policy = templatefile("${path.module}/resources/elasticsearch-policy.json", {
    elasticsearch = var.elasticsearch_arn
  })
}

# Apply policy to allow Travel Page ETL Lambda to access Elasticsearch
resource "aws_iam_role_policy_attachment" "travel_page_etl_es_policy" {
  role       = module.lambda_travel_page_etl.lambda_role_name
  policy_arn = aws_iam_policy.travel_page_etl_es_policy.arn
}

# Allow Travel Page ETL Lambda to be invoked by an SNS topic
resource "aws_lambda_permission" "travel_page_etl_article_update" {
  statement_id  = "AllowExecutionFromSNS-TravelPageETL"
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  function_name = module.lambda_travel_page_etl.lambda_function_name
  source_arn    = module.article_update_sns.topic
}

# Subscribe Travel Page ETL Lambda to SNS topic
resource "aws_sns_topic_subscription" "travel_page_etl_article_update" {
  protocol  = "lambda"
  endpoint  = module.lambda_travel_page_etl.lambda_function_arn
  topic_arn = module.article_update_sns.topic
}

# Feed ETL Lambda
module "lambda_feed_etl" {
  source = "../modules/aws/lambda-with-sns"

  company     = var.company
  application = var.application
  environment = var.environment

  function_name   = "feed-etl"
  service_handler = "service.handler"
  runtime         = "python2.7"
  memory_size     = 128
  timeout         = 30

  artifacts_bucket  = var.artifacts_bucket
  instance_subnets  = var.instance_subnets
  internal_sg_group = var.internal_sg_group
  sg_all_servers    = var.sg_all_servers
}

# Create policy allowing access to Elasticsearch
resource "aws_iam_policy" "feed_etl_es_policy" {
  name        = "${var.company}-${var.application}-feed-etl-es-policy-${var.environment}"
  description = "policy to access elasticsearch from feed etl"
  policy = templatefile("${path.module}/resources/elasticsearch-policy.json", {
    elasticsearch = var.elasticsearch_arn
  })
}

# Apply policy to allow Feed ETL Lambda to access Elasticsearch
resource "aws_iam_role_policy_attachment" "feed_etl_es_policy" {
  role       = module.lambda_feed_etl.lambda_role_name
  policy_arn = aws_iam_policy.feed_etl_es_policy.arn
}

# Allow Feed ETL Lambda to be invoked by an SNS topic
resource "aws_lambda_permission" "feed_etl_article_update" {
  statement_id  = "AllowExecutionFromSNS-FeedETL"
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  function_name = module.lambda_feed_etl.lambda_function_name
  source_arn    = module.article_update_sns.topic
}

# Subscribe Feed ETL Lambda to SNS topic
resource "aws_sns_topic_subscription" "feed_etl_article_update" {
  protocol  = "lambda"
  endpoint  = module.lambda_feed_etl.lambda_function_arn
  topic_arn = module.article_update_sns.topic
}

# Update Varnish Cache Lambda
module "lambda_update_varnish_cache" {
  source = "../modules/aws/lambda-with-sns"

  company     = var.company
  application = var.application
  environment = var.environment

  function_name   = "update-varnish-cache"
  service_handler = "service.handler"
  runtime         = "python2.7"
  memory_size     = 128
  timeout         = 30

  artifacts_bucket  = var.artifacts_bucket
  instance_subnets  = var.instance_subnets
  internal_sg_group = var.internal_sg_group
  sg_all_servers    = var.sg_all_servers
}

# Allow Update Varnish Cache Lambda to query for info about ECS and EC2 instances
# TODO: Determine whether it's possible to narrow the ec2 resource to only within this ECS cluster.
resource "aws_iam_policy" "update_varnish_cache_ecs_policy" {
  name        = "${var.company}-${var.application}-update-varnish-cache-es-policy-${var.environment}"
  description = "policy to access elasticsearch from feed etl"
  policy      = file("${path.module}/resources/ecs-varnish-cache-policy.json")
}

# Apply policy to allow Update Varnish Cache Lambda to query for info about ECS and EC2 instances
resource "aws_iam_role_policy_attachment" "update_varnish_cache_es_policy" {
  role       = module.lambda_update_varnish_cache.lambda_role_name
  policy_arn = aws_iam_policy.update_varnish_cache_ecs_policy.arn
}

# Allow Update Varnish Cache Lambda to be invoked by an SNS topic
resource "aws_lambda_permission" "update_varnish_cache_article_update" {
  statement_id  = "AllowExecutionFromSNS-UpdateVarnishCache"
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  function_name = module.lambda_update_varnish_cache.lambda_function_name
  source_arn    = module.article_update_sns.topic
}

# Subscribe Update Varnish Cache Lambda to SNS topic
resource "aws_sns_topic_subscription" "update_varnish_cache_article_update" {
  protocol  = "lambda"
  endpoint  = module.lambda_update_varnish_cache.lambda_function_arn
  topic_arn = module.article_update_sns.topic
}
