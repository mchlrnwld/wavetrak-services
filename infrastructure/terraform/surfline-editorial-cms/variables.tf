variable "company" {
  default = "sl"
}

variable "application" {
  default = "editorial-cms"
}

variable "environment" {
  default = "sbox"
}

variable "ecs_cluster_name" {
  default = "surfline-editorial-cms-sbox"
}

variable "artifacts_bucket" {
  default = "sl-artifacts-dev"
}

variable "default_vpc" {
  default = "vpc-981887fd"
}

variable "availability_zones" {
  default = ["us-west-1b", "us-west-1c"]
}

variable "internal_sg_group" {
  default = "sg-90aeaef5"
}

variable "vpn_sg_group" {
  default = "sg-82aeaee7"
}

variable "multiclient_sg" {
  default = "sg-03888cba167954624"
}

variable "sg_all_servers" {
  default = "sg-91aeaef4"
}

variable "instance_subnets" {
  default = ["subnet-0909466c", "subnet-f2d458ab"]
}

variable "lb_subnets" {
  default = ["subnet-f4d458ad", "subnet-0b09466e"]
}

variable "ssl_certificate_arn" {
  default = "arn:aws:acm:us-west-1:665294954271:certificate/b5d003ab-5256-4737-9333-c07e7f817a98"
}

variable "key_name" {
  default = "sturdy-surfline-dev"
}

variable "lb_ingress_cidr_block" {
  default = ["0.0.0.0/0"]
}

variable "instance_type" {
  default = "t2.medium"
}

variable "wordpress_td_count" {
  default = "2"
}

variable "wordpress_td_count_max" {
  default = "2"
}

variable "wpadmin_td_count" {
  default = "2"
}

variable "varnish_td_count" {
  default = "2"
}

variable "task_deregistration_delay" {
  default = "30"
}

variable "ami_id" {
  default = "ami-9df0f0fd"
}

variable "ecs_cluster_alarm_cooldown" {
  default = "2700"
}

# Cloudwatch cpu alarm variables
variable "ecs_cluster_alarm_cpu_util_high_eval_periods" {
  default = "5"
}

variable "ecs_cluster_alarm_cpu_util_high_period" {
  default = "60"
}

variable "ecs_cluster_alarm_cpu_util_high_threshold" {
  default = "80"
}

variable "ecs_cluster_alarm_cpu_util_low_eval_periods" {
  default = "30"
}

variable "ecs_cluster_alarm_cpu_util_low_period" {
  default = "60"
}

variable "ecs_cluster_alarm_cpu_util_low_threshold" {
  default = "0"
}

variable "ecs_cluster_alarm_cpu_res_high_eval_periods" {
  default = "5"
}

variable "ecs_cluster_alarm_cpu_res_high_period" {
  default = "60"
}

variable "ecs_cluster_alarm_cpu_res_high_threshold" {
  default = "95"
}

variable "ecs_cluster_alarm_cpu_res_low_eval_periods" {
  default = "30"
}

variable "ecs_cluster_alarm_cpu_res_low_period" {
  default = "60"
}

variable "ecs_cluster_alarm_cpu_res_low_threshold" {
  default = "1"
}

# Cloudwatch memory alarm variables
variable "ecs_cluster_alarm_memory_util_high_eval_periods" {
  default = "5"
}

variable "ecs_cluster_alarm_memory_util_high_period" {
  default = "60"
}

variable "ecs_cluster_alarm_memory_util_high_threshold" {
  default = "80"
}

variable "ecs_cluster_alarm_memory_util_low_eval_periods" {
  default = "30"
}

variable "ecs_cluster_alarm_memory_util_low_period" {
  default = "60"
}

variable "ecs_cluster_alarm_memory_util_low_threshold" {
  default = "1"
}

variable "ecs_cluster_alarm_memory_res_high_eval_periods" {
  default = "5"
}

variable "ecs_cluster_alarm_memory_res_high_period" {
  default = "60"
}

variable "ecs_cluster_alarm_memory_res_high_threshold" {
  default = "100"
}

variable "ecs_cluster_alarm_memory_res_low_eval_periods" {
  default = "30"
}

variable "ecs_cluster_alarm_memory_res_low_period" {
  default = "60"
}

variable "ecs_cluster_alarm_memory_res_low_threshold" {
  default = "1"
}

# Cloudwatch ALB alarm variables
variable "wordpress_alb_svc_target_resp_high_eval_periods" {
  default = "1"
}

variable "wordpress_alb_svc_target_resp_high_period" {
  default = "60"
}

variable "wordpress_alb_svc_target_resp_high_threshold" {
  default = ".5"
}

variable "wordpress_alb_svc_target_resp_low_eval_periods" {
  default = "1"
}

variable "wordpress_alb_svc_target_resp_low_period" {
  default = "60"
}

variable "wordpress_alb_svc_target_resp_low_threshold" {
  default = ".05"
}

variable "wordpress_td_scale_down_cooldown" {
  default = "2700"
}

# name of Chef bucket containing environment config
variable "chef_bucket" {
  default = "sl-chef-sbox"
}

# ARN for SAND
variable "dereg_topic" {
  default = "arn:aws:sns:us-west-1:665294954271:Sturdy-SAND"
}

# RDS settings
variable "rds_allocated_storage" {
  default = 20
}

variable "rds_instance_class" {
  default = "db.t2.micro"
}

variable "rds_password" {
  default = ""
}

variable "rds_backup_retention_period" {
  default = 7
}

variable "rds_multi_az" {
  default = false
}

variable "rds_storage_encrypted" {
  default = false
}

variable "rds_auto_minor_version_upgrade" {
  default = true
}

variable "rds_allow_major_version_upgrade" {
  default = false
}

variable "rds_performance_insights" {
  default = false
}

variable "rds_deletion_protection" {
  default = false
}

# terraform does not apply RDS changes that require a reboot immediately.
# Setting this to true will allow the update to take place now instead of
# during the next maint window.
variable "rds_apply_immediately" {
  default = false
}

variable "enabled_cloudwatch_logs_exports" {
  default = ["error", "slowquery"]
}

# ECS settings
variable "is_internal" {
  default = false
}

variable "min_instance_count" {
  default = 3
}

# Internal LB Route53 Zone ID
variable "internal_dns_zone_id" {
  default = "Z3JHKQ8ELQG5UE"
}

# Internal LB Route53 DNS Name
variable "internal_dns_name" {
  default = "internal-editorial-cms.sandbox.surfline.com"
}

# Internal LB Route53 DNS Name
variable "short_internal_dns_name" {
  default = "int-edit-cms.sandbox.surfline.com"
}

# Elasticsearch resource
variable "elasticsearch_arn" {
  default = "arn:aws:es:us-west-1:665294954271:domain/sl-es-sbox"
}

variable "environment_url" {
  default = "staging.surfline.com"
}

variable "custom_cdn_authorization_header" {
  default = ""
}
