module "wp_database" {
  source = "./modules/database/"

  # common module settings
  application = var.application
  company     = var.company
  environment = var.environment
  service     = "mysql"

  default_vpc                     = var.default_vpc
  instance_subnets                = var.instance_subnets
  rds_allocated_storage           = var.rds_allocated_storage
  rds_instance_class              = var.rds_instance_class
  rds_password                    = var.rds_password
  rds_backup_retention_period     = var.rds_backup_retention_period
  rds_multi_az                    = var.rds_multi_az
  rds_storage_encrypted           = var.rds_storage_encrypted
  rds_auto_minor_version_upgrade  = var.rds_auto_minor_version_upgrade
  rds_allow_major_version_upgrade = var.rds_allow_major_version_upgrade
  rds_apply_immediately           = var.rds_apply_immediately
  rds_vpn_ingress_sg              = var.vpn_sg_group
  multiclient_sg                  = var.multiclient_sg
  rds_performance_insights        = var.rds_performance_insights
  rds_deletion_protection         = var.rds_deletion_protection

  # We'll have to manually retrieve the logs from RDS until terraform upgrade;
  # The below line is commented out. See TAG-931.
  # enabled_cloudwatch_logs_exports = ["${var.enabled_cloudwatch_logs_exports}"]
}
