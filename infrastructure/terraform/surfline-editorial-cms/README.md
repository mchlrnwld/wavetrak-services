# Surfline Editorial CMS

- [Surfline Editorial CMS](#surfline-editorial-cms)
  - [Naming Conventions](#naming-conventions)
  - [Pre-requisites](#pre-requisites)
  - [Deploying to a AWS environment](#deploying-to-a-aws-environment)
  - [Future Plans](#future-plans)
  - [Logging](#logging)
    - [State Management](#state-management)
    - [Secret Management](#secret-management)
    - [Modules](#modules)
  - [Further Reading](#further-reading)

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Pre-requisites

Terraform version `0.12.28`.

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)
The environment variable `TF_VAR_rds_password` will need to be set when creating
the RDS instance.

Terraform does not apply RDS changes that require a reboot immediately. Set
`TF_VAR_rds_apply_immediately=true` in order to apply the updates now.

```shell
TF_VAR_rds_apply_immediately=true make apply
```

Task Definitions, even if just a placeholder, must exist before the `apply` will
work properly.

```shell
# sandbox
aws ecs register-task-definition \
  --family sl-editorial-cms-sandbox-varnish \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"varnish","portMappings":[{"containerPort":6081,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

aws ecs register-task-definition \
  --family sl-editorial-cms-sandbox-wordpress \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"wordpress","portMappings":[{"containerPort":80,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

aws ecs register-task-definition \
  --family sl-editorial-cms-sandbox-wp-admin \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"wp-admin","portMappings":[{"containerPort":80,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# staging
aws ecs register-task-definition \
  --family sl-editorial-cms-staging-varnish \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"varnish","portMappings":[{"containerPort":6081,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

aws ecs register-task-definition \
  --family sl-editorial-cms-staging-wordpress \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"wordpress","portMappings":[{"containerPort":80,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

aws ecs register-task-definition \
  --family sl-editorial-cms-staging-wp-admin \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"wp-admin","portMappings":[{"containerPort":80,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# production
aws ecs register-task-definition \
  --family sl-editorial-cms-prod-varnish \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"varnish","portMappings":[{"containerPort":6081,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

aws ecs register-task-definition \
  --family sl-editorial-cms-prod-wordpress \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"wordpress","portMappings":[{"containerPort":80,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

aws ecs register-task-definition \
  --family sl-editorial-cms-prod-wp-admin \
  --container-definitions \
  '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"wp-admin","portMappings":[{"containerPort":80,"hostPort":0,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'
```

```bash
make plan
make apply
```

## Future Plans

?

## Logging

Enabled log types are configured in the aws_db_parameter_group resource.

### State Management

For now keep each environment `.tfstate` in S3. Future plans could use other providers such as Artifactory. To pull state from a given environment run

```bash
make setup
```

### Secret Management

Vault?

### Modules

https://www.terraform.io/docs/modules/usage.html (caveat all resources are prefixed with `module.`)

## Further Reading

- https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/
- https://charity.wtf/2016/02/23/two-weeks-with-terraform/
- http://blog.mattiasgees.be/2015/07/29/terraform-remote-state/
- http://code.hootsuite.com/how-to-use-terraform-and-remote-state-with-s3/
