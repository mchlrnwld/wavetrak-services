data "aws_caller_identity" "current" {
}

# Create base IAM role for wp-admin task
resource "aws_iam_role" "wp-admin_task_arn" {
  name               = "wp-admin_task_role_${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-task-role-policy.json")
}

# Allow wp-admin task to perform basic tasks functions and access asset s3 bucket
resource "aws_iam_role_policy_attachment" "wp-admin_task_arn" {
  role       = aws_iam_role.wp-admin_task_arn.name
  policy_arn = module.wp_content.bucket_policy_arn
}

# Allow wp-admin task to publish to Feed ETL SNS topic
resource "aws_iam_role_policy_attachment" "wp-admin_sns_publish" {
  role       = aws_iam_role.wp-admin_task_arn.name
  policy_arn = module.article_update_sns.topic_policy
}

# Allow wp-admin task to publish to Travel Page ETL SNS topic
resource "aws_iam_role_policy_attachment" "wp-admin_sns_publish_travel" {
  role       = aws_iam_role.wp-admin_task_arn.name
  policy_arn = module.travel_page_update_sns.topic_policy
}

# Allow wp-admin task to write to the asset S3 bucket
resource "aws_iam_role_policy_attachment" "wp-admin_s3_access" {
  role       = aws_iam_role.wp-admin_task_arn.name
  policy_arn = module.wp_content.bucket_policy_arn
}
