module "wp_content" {
  source = "./modules/bucket-with-cdn/"

  # common module settings
  application = var.application
  company     = var.company
  environment = var.environment
  service     = "wp-content"

  # S3 specific settings
  versioning_enabled = true

  # CloudFront specific settings
  comment                         = "Wordpress Content Distribution"
  environment_url                 = var.environment_url
  custom_cdn_authorization_header = var.custom_cdn_authorization_header
}
