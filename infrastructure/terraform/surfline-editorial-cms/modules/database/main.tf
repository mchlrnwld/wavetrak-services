# DB Subnet Group
resource "aws_db_subnet_group" "subnet_group" {
  name       = "rds-dbsubnet-group-${var.company}-${var.application}-${var.environment}"
  subnet_ids = var.instance_subnets

  tags = {
    Name        = "rds-dbsubnet-group-${var.company}-${var.application}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# RDS Client Security Group
resource "aws_security_group" "rds_clients" {
  name        = "rds-clients-${var.company}-${var.application}-${var.environment}"
  description = "${var.company}-${var.application}-RdsClientSecurityGroup-${var.environment}"
  vpc_id      = var.default_vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "rds-clients-${var.company}-${var.application}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# RDS Server Security Group
resource "aws_security_group" "rds_server" {
  name        = "rds-server-${var.company}-${var.application}-${var.environment}"
  description = "${var.company}-${var.application}-RdsServerSecurityGroup-${var.environment}"
  vpc_id      = var.default_vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.rds_clients.id, var.rds_vpn_ingress_sg, var.multiclient_sg]
  }

  tags = {
    Name        = "rds-server-${var.company}-${var.application}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# RDS Instance
resource "aws_db_instance" "mysql" {
  engine         = "mysql"
  engine_version = "5.7"
  instance_class = var.rds_instance_class
  username       = "root"
  password       = var.rds_password

  db_subnet_group_name         = aws_db_subnet_group.subnet_group.id
  vpc_security_group_ids       = [aws_security_group.rds_server.id]
  allocated_storage            = var.rds_allocated_storage
  backup_retention_period      = var.rds_backup_retention_period
  storage_encrypted            = var.rds_storage_encrypted
  auto_minor_version_upgrade   = var.rds_auto_minor_version_upgrade
  allow_major_version_upgrade  = var.rds_allow_major_version_upgrade
  multi_az                     = var.rds_multi_az
  apply_immediately            = var.rds_apply_immediately
  performance_insights_enabled = var.rds_performance_insights
  deletion_protection          = var.rds_deletion_protection
  parameter_group_name         = "db-param-group-${var.company}-${var.application}-${var.environment}"
  skip_final_snapshot          = true

  # We'll have to manually retrieve the logs from RDS until terraform upgrade;
  # The below line is commented out. See TAG-931.
  # enabled_cloudwatch_logs_exports = ["${var.enabled_cloudwatch_logs_exports}"]
}

resource "aws_db_parameter_group" "db_parameter_group" {
  name   = "db-param-group-${var.company}-${var.application}-${var.environment}"
  family = "mysql5.7"

  parameter {
    name  = "innodb_print_all_deadlocks"
    value = "1"
  }

  parameter {
    name  = "net_read_timeout"
    value = "60"
  }

  parameter {
    name  = "net_write_timeout"
    value = "120"
  }

  parameter {
    name  = "max_allowed_packet"
    value = "8388608"
  }

  parameter {
    name  = "slow_query_log"
    value = "1"
  }
}
