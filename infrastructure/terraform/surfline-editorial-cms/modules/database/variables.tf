variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "service" {
}

variable "default_vpc" {
}

variable "instance_subnets" {
  type = list(string)
}

variable "rds_allocated_storage" {
}

variable "rds_instance_class" {
}

variable "rds_password" {
}

variable "rds_backup_retention_period" {
}

variable "rds_multi_az" {
}

variable "rds_storage_encrypted" {
}

variable "rds_auto_minor_version_upgrade" {
}

variable "rds_allow_major_version_upgrade" {
}

variable "rds_apply_immediately" {
}

variable "rds_vpn_ingress_sg" {
}

variable "multiclient_sg" {
}

variable "rds_performance_insights" {
}

variable "rds_deletion_protection" {
}

# variable "enabled_cloudwatch_logs_exports" {type = "list"}
