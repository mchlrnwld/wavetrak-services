# S3 bucket policy for CloudFront access
data "template_file" "policy" {
  template = templatefile("${path.module}/resources/s3-bucket-policy.tpl", {
    s3_bucket  = "arn:aws:s3:::${var.company}-${var.application}-${var.service}-${var.environment}"
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
}

# S3 bucket for content
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.company}-${var.application}-${var.service}-${var.environment}"
  acl    = "private"

  versioning {
    enabled = var.versioning_enabled
  }

  lifecycle_rule {
    prefix  = ""
    enabled = var.versioning_enabled

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  policy = data.template_file.policy.rendered

  tags = {
    Name        = "${var.company}-${var.application}-${var.service}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

data "template_file" "mgmt-policy" {
  template = templatefile("${path.module}/resources/s3-mgmt-policy.tpl", {
    s3_bucket = aws_s3_bucket.bucket.arn
  })
}

# Set a policy to manage content in bucket
resource "aws_iam_policy" "bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-${var.service}-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy      = data.template_file.mgmt-policy.rendered
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.comment
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  origin {
    domain_name = "${aws_s3_bucket.bucket.bucket}.s3.amazonaws.com"
    origin_id   = "S3-${aws_s3_bucket.bucket.bucket}"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = var.environment_url
    origin_id   = "editorial-cms-${var.environment}"
    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2"]
    }
    custom_header {
      name  = "Authorization"
      value = var.custom_cdn_authorization_header
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment

  # TODO: logging_config {}

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-${aws_s3_bucket.bucket.bucket}"
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  ordered_cache_behavior {
    allowed_methods = ["HEAD", "GET", "OPTIONS"]
    cached_methods  = ["HEAD", "GET"]
    default_ttl     = 86400
    forwarded_values {
      cookies {
        forward = "none"
      }
      query_string = true
    }
    max_ttl                = 31536000
    min_ttl                = 0
    path_pattern           = "wp-content/plugins/*"
    target_origin_id       = "editorial-cms-${var.environment}"
    viewer_protocol_policy = "allow-all"
  }

  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    cloudfront_default_certificate = true
    minimum_protocol_version       = "TLSv1"
  }
}
