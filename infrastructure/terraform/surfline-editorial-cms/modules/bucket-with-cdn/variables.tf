variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "service" {
}

variable "versioning_enabled" {
}

variable "comment" {
}

variable "environment_url" {
}

variable "custom_cdn_authorization_header" {
}
