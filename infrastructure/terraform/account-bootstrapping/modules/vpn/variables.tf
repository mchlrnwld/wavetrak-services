variable "company" {
  description = "The company that these resources are being created for"
}

variable "application" {
  description = "The application that these resources are being created for"
}

variable "environment" {
  description = "The environment that these resources are being created in"
}

variable "region" {
  description = "The region these resources are being created in"
}

variable "vpc_id" {
  description = "The ID generated from the 'vpc' module"
}

variable "public_subnets" {
  description = "The public subnets resulting from the 'vpc' module"
}

variable "vpn_subdomain" {
  description = "The domain name of the VPN DNS hosted zone"
}

variable "vpn_group" {
  description = "VPN group to be used with OneLogin"
}

variable "ssl_certificate_email" {
  description = "The email associated with the SSL cert to be generated"
}

variable "aws_access_key_id" {
  description = "Access key ID for the AWS service account with access to the prod hosted zone"
}

variable "aws_secret_access_key" {
  description = "Secret access key for the AWS service account with access to the prod hosted zone"
}

variable "instance_type" {
  description = "Instance type to be used for VPN server"
}

variable "image_id" {
  description = "AWS marketplace OpenVPN AS AMI ID"
}

variable "public_key" {
  description = "Public key generated for use with this module"
}
