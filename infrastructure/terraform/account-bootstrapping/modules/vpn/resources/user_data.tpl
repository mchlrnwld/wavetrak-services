#cloud-config
---
sources:
  certbot:
    source: "ppa:certbot/certbot"
packages:
  - awscli
  - certbot
  - easy-rsa
  - jq
  - ldap-utils
  - openvpn-auth-ldap
  - python3-pip
write_files:
  -
    path: /opt/openvpnas/client.ovpn.tpl
    permissions: "0644"
    owner: "openvpnas:openvpnas"
    content: |
        client
        dev tun
        proto udp
        remote <IP_ADDRESS> 1194
        comp-lzo
        resolv-retry infinite
        nobind
        persist-key
        persist-tun
        auth-user-pass
        auth-nocache
        reneg-sec 43200
        verb 3
        cipher AES-256-CBC
  -
    path: /opt/openvpnas/certbot_generate_ssl_cert.sh
    permissions: "0755"
    owner: "openvpnas:openvpnas"
    content: |
        sleep 30
        sudo certbot certonly \
          --standalone \
          --non-interactive \
          --agree-tos \
          --email ${ssl_certificate_email} \
          --domains ${vpn_domain_prefix_public}.${vpn_subdomain} \
          --pre-hook 'sudo service openvpnas stop' \
          --post-hook 'sudo service openvpnas start'
  -
    path: /opt/openvpnas/update_client_ovpn.sh
    permissions: "0755"
    owner: "openvpnas:openvpnas"
    content: |
        #!/usr/bin/env bash
        # This is the template file that will be used to render the final client.ovpn.

        ip_address=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)

        cat /opt/openvpnas/client.ovpn.tpl | sed "s/<IP_ADDRESS>/$ip_address/" > /opt/openvpnas/client.ovpn
        echo '<ca>' >> /opt/openvpnas/client.ovpn
        cat /etc/letsencrypt/live/${vpn_domain_prefix_public}.${vpn_subdomain}/cert.pem >> /opt/openvpnas/client.ovpn
        echo '</ca>' >> /opt/openvpnas/client.ovpn
  -
    path: /usr/local/bin/ldap_auth.sh
    permissions: "0755"
    owner: "root:root"
    content: |
        #!/bin/bash
        # Adapted from
        # https://github.com/waldner/openvpn-ldap

        # passed by openvpn (in the environment):
        #
        # $username
        # $password

        ourname=ldap_auth.sh
        facility=auth

        ldapserver=ldap.us.onelogin.com

        vpngroup=${vpn_group}
        basedn='ou=users,dc=surfline,dc=onelogin,dc=com'
        groupdn='cn=roles,dc=surfline,dc=onelogin,dc=com'

        bindname=$${username}
        declare -a query

          # writing the query this way is useful because it's easier to
          # include it in the log; using an array instead of a string is
          # safer, see http://mywiki.wooledge.org/BashFAQ/050

          query[0]='-L'
          query[1]='-s'
          query[2]='sub'
          query[3]='-x'
          query[4]='-w'
          query[5]="$${password}"
          query[6]='-D'
          query[7]="uid=$${bindname},$${basedn}"
          query[8]='-b'
          query[9]="$${basedn}"
          query[10]='-H'
          query[11]="ldaps://$${ldapserver}"
          query[12]="(&(uid=$${username})(memberOf=cn=$${vpngroup},$${groupdn}))"
          query[13]='dn'

          output=$(mktemp)
          error=$(mktemp)

          # clean temp files when we terminate
          trap "rm -f $${output} $${error}" EXIT

          logger -p "$${facility}.info" -t "$ourname" "Trying to authenticate user $${bindname} against LDAP"

          ldapsearch "$${query[@]}" 1>"$${output}" 2>"$${error}"

          # save exist status here, otherwise the following assignment resets $?
          status=$?

          query[5]='xxxxxxxxx'   # obfuscate password to put query in the logs

          if [ $status -ne 0 ]; then
            logger -p "$${facility}.err" -t "$${ourname}" "There was an error authenticating user $${username} ($${bindname}) against LDAP."
            logger -p "$${facility}.err" -t "$${ourname}" "The query was: ldapsearch $${query[*]}"
            logger -p "$${facility}.err" -t "$${ourname}" "The error was: $(tr '\n' ' ' < "$${error}" )"  # turn multiline into single line
            exit 1
          fi

          # look for the "numEntries" line in the output of ldapsearch
          numentries=$(awk '/numEntries:/{ne = $3} END{print ne + 0}' "$output")

          if [ $numentries -eq 1 ]; then
            logger -p "$${facility}.info" -t "{$ourname}" "User $${username} authenticated successfully"
            exit 0
          else
            logger -p "$${facility}.err" -t "$${ourname}" "User $${username} NOT authenticated (user not in group?)"
            logger -p "$${facility}.err" -t "$${ourname}" "The query was: ldapsearch $${query[*]}"
            exit 1
          fi
  -
    path: /home/openvpnas/.aws/credentials
    permissions: "0644"
    owner: "openvpnas:openvpnas"
    content: |
      [service-user]
      aws_access_key_id=${aws_access_key_id}
      aws_secret_access_key=${aws_secret_access_key}
  -
    path: /home/openvpnas/.aws/config
    permissions: "0644"
    owner: "openvpnas:openvpnas"
    content: |
      [service-user]
      output = json
      region = ${region}
runcmd:
  # Note: awscil is updated manually below due to version-pinning issues with cloud-config
  - pip3 install awscli --upgrade
  - set -x

  - echo "--------------Creating pubic/private Route 53 DNS record:--------------"
  - wget https://github.com/barnybug/cli53/releases/download/0.8.15/cli53-linux-amd64 -O /usr/local/bin/cli53
  - chmod 755 /usr/local/bin/cli53
  # Note: using openvpnas system user due to issues running aws commands as root
  - export private_ip_address=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
  - su openvpnas -c 'cli53 rrcreate ${vpn_subdomain} "${vpn_domain_prefix_private} 60 A $private_ip_address" --profile=service-user'
  - su openvpnas -c 'cli53 rrcreate ${vpn_subdomain} "${vpn_domain_prefix_public} 60 A ${public_eip}"  --profile=service-user'

  - echo "--------------Pushing cert to SSM:--------------"
  - aws ssm put-parameter --name "openvpnas_ssl_cert" --type "SecureString" --value "$(cat /usr/local/openvpn_as/etc/web-ssl/server.key)" --overwrite --region ${region}

  - echo "--------------Associating EIP with EC2 instance:--------------"
  - aws --region=${region} ec2 associate-address --public-ip ${public_eip} --instance-id "$(curl -ks http://169.254.169.254/latest/meta-data/instance-id)"
  - sleep 15 # Ensure that instance has completed the switch to its new EIP

  - echo "--------------Generating SSL cert:--------------"
  - service openvpnas stop
  - /opt/openvpnas/certbot_generate_ssl_cert.sh
  - ln -s -f /etc/letsencrypt/live/${vpn_domain_prefix_public}.${vpn_subdomain}/cert.pem /usr/local/openvpn_as/etc/web-ssl/server.crt
  - ln -s -f /etc/letsencrypt/live/${vpn_domain_prefix_public}.${vpn_subdomain}/privkey.pem /usr/local/openvpn_as/etc/web-ssl/server.key
  - sleep 5 && service openvpnas start

  - echo "--------------Pushing ovpn file to S3:--------------"
  - /opt/openvpnas/update_client_ovpn.sh
  - aws s3 cp /opt/openvpnas/client.ovpn s3://${bucket_name}/client.ovpn --region ${region}
