locals {
  bucket_name               = "${var.company}-${var.application}-artifacts-${var.environment}"
  vpn_domain_prefix_public  = "${var.company}-${var.environment}-${var.region}"
  vpn_domain_prefix_private = "${var.company}-${var.environment}-${var.region}"
}

data "aws_caller_identity" "vpn_module" {
}

resource "aws_kms_key" "parameter_store" {
  description             = "Parameter store kms master key"
  deletion_window_in_days = 10
  enable_key_rotation     = true
}

resource "aws_kms_alias" "kms_alias" {
  name          = "alias/kms_alias"
  target_key_id = aws_kms_key.parameter_store.id
}

#==== OPENVPN SETUP ====#
# Mostly from: https://loige.co/using-lets-encrypt-and-certbot-to-automate-the-creation-of-certificates-for-openvpn/

# NOTE: As detailed in the README, this module
# requires that you create an RSA key pair. The
# public key from this key pair should be used
# for the "key_name" value below.

resource "aws_key_pair" "openvpn_key_pair" {
  key_name   = "${var.company}-${var.application}-${var.environment}-keypair"
  public_key = var.public_key
}

resource "aws_iam_policy" "update_openvpn_policy" {
  name        = "${var.company}-vpc-${var.environment}-role"
  description = "policy to update OpenVPN EC2"
  policy = templatefile("${path.module}/resources/vpn-policy.json", {
    bucket_name = local.bucket_name
    region      = var.region
    account_id  = data.aws_caller_identity.vpn_module.account_id
  })
}

resource "aws_iam_role_policy_attachment" "update_policy_attach" {
  role       = aws_iam_role.openvpn_iam_role.name
  policy_arn = aws_iam_policy.update_openvpn_policy.arn
}

resource "aws_iam_instance_profile" "ovpn_instance_profile" {
  name = "ovpn-instance-profile"
  role = aws_iam_role.openvpn_iam_role.name
}

resource "aws_iam_role" "openvpn_iam_role" {
  name               = "${var.company}-${var.application}-${var.environment}-role"
  assume_role_policy = templatefile("${path.module}/resources/ec2-trust-policy.json", {})
  tags = {
    Name        = "${var.company}-${var.application}-${var.environment}"
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_security_group" "openvpn_security_group" {
  name        = "${var.company}-openvpn-${var.environment}-sg"
  description = "Allow traffic needed by openvpn"
  vpc_id      = var.vpc_id

  # ssh
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # https
  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # http
  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # open vpn tcp
  ingress {
    from_port   = "943"
    to_port     = "943"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # open vpn udp
  ingress {
    from_port   = "1194"
    to_port     = "1194"
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_autoscaling_group" "vpn_cluster" {
  name                 = "${var.company}-${var.application}-${var.environment}"
  min_size             = 1
  max_size             = 1
  desired_capacity     = 1
  default_cooldown     = 30
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.vpn_launch_configuration.name
  vpc_zone_identifier  = [var.public_subnets[0]]

  tag {
    key                 = "Name" # names the launched instances
    value               = "${var.company}-vpn-${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Company"
    value               = var.company
    propagate_at_launch = true
  }

  tag {
    key                 = "Service"
    value               = "vpn"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = true
    propagate_at_launch = true
  }
}

resource "aws_eip" "vpn_eip" {
  vpc = true
  tags = {
    Name = "${var.company}-vpn-eip"
  }
}

resource "aws_launch_configuration" "vpn_launch_configuration" {
  image_id                    = var.image_id
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.openvpn_security_group.id]
  iam_instance_profile        = aws_iam_instance_profile.ovpn_instance_profile.name
  key_name                    = aws_key_pair.openvpn_key_pair.key_name
  associate_public_ip_address = true
  user_data = templatefile("${path.module}/resources/user_data.tpl", {
    company                   = var.company
    region                    = var.region
    vpn_subdomain             = var.vpn_subdomain
    vpn_domain_prefix_public  = local.vpn_domain_prefix_public
    vpn_domain_prefix_private = local.vpn_domain_prefix_private
    ssl_certificate_email     = var.ssl_certificate_email
    aws_access_key_id         = var.aws_access_key_id
    aws_secret_access_key     = var.aws_secret_access_key
    bucket_name               = local.bucket_name
    vpn_group                 = var.vpn_group
    public_eip                = aws_eip.vpn_eip.public_ip
  })
}

resource "aws_s3_bucket" "vpn_artifacts" {
  bucket = "${var.company}-${var.application}-artifacts-${var.environment}"
  acl    = "private"
  tags = {
    Name        = "${var.company}-${var.application}-${var.environment}-artifacts"
    Environment = var.environment
    Terraform   = true
  }
}
