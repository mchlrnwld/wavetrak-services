variable "company" {
  description = "The company that these resources are being created for"
}

variable "application" {
  description = "The application that these resources are being created for"
}

variable "environment" {
  description = "The environment that these resources are being created in"
}

variable "region" {
  description = "The region these resources are being created in"
}

variable "azs" {
  description = "The availability zones that these resources are being created in"
}

# IMPORTANT: when using this module, be sure that the following "cidr", "public_subnets" and "private_subnets" values
# don't conflict with existing VPCs. BE SURE TO ACCOUNT FOR NETWORKS LOCATED IN OTHER ACCOUNTS AND REGIONS.
# If values conflict, you will not be able to create peered VPCs in the future.
variable "cidr" {
  description = "The CIDR block that will be used to generate the VPN"
}

variable "public_subnets" {
  description = "The public subnets that these resources are being created in"
}

variable "private_subnets" {
  description = "The private subnets that these resources are being created in"
}

variable "vpn_subdomain" {
  description = "The domain name of the VPN DNS hosted zone"
}

# Note: the below variable refers to the role name
# used to authenticate the user via LDAP. OneLogin is
# the identity provider, and contains the names/permissions of
# all the valid groups. Contact IT dept for more information.
variable "vpn_group" {
  description = "VPN group to be used with OneLogin."
}

variable "ssl_certificate_email" {
  description = "The email associated with the SSL cert to be generated"
}

variable "aws_access_key_id" {
  description = "Access key ID for the AWS service account with access to the prod hosted zone"
  default     = ""
}

variable "aws_secret_access_key" {
  description = "Secret access key for the AWS service account with access to the prod hosted zone"
  default     = ""
}

variable "instance_type" {
  description = "Instance type to be used for VPN server"
}

# Note: the following AMI ID differs from region to region. To find
# the AMI ID for your particular region, you can use the
# following query:
# aws --region us-east-2 ec2 describe-images \
#  --owners aws-marketplace \
#  --filters "Name=name,Values=OpenVPN Access Server 2.5.0*" \
#  --region us-east-2 \
#  --output table
# This AMI ID can be verified here:
# https://aws.amazon.com/marketplace/pp?sku={ProductCode}
variable "image_id" {
  description = "AWS marketplace OpenVPN AS AMI ID"
}

# IMPORTANT: As noted in the README, the following "public_key" should correspond to an SSH
# public key created for use with this module. See README for more instruction.
variable "public_key" {
  description = "Public key generated for use with this module."
}
