# Account Bootstrapping

- [Account Bootstrapping](#account-bootstrapping)
  - [Summary](#summary)
    - [Directory Structure](#directory-structure)
    - [Important Notes](#important-notes)
  - [Basic Usage](#basic-usage)
  - [RSA Key Pair](#rsa-key-pair)
    - [Generating an SSH key pair](#generating-an-ssh-key-pair)
  - [Route 53 Records](#route-53-records)
  - [Service Account User](#service-account-user)
  - [SSL Certificate Creation](#ssl-certificate-creation)
  - [Authentication via LDAP](#authentication-via-ldap)

## Summary

This Terraform module bootstraps basic AWS VPC resources into newly-created AWS accounts. Running this module in new accounts should decrease bootstrap time, and standardize basic resources common to VPCs at Wavetrak. This module should be executed manually via the Terraform CLI, and assumes that the user has the necessary AWS credentials.

The module requires certain prerequisites and environment variables, including: (1) the selection of networking values that don't conflict with any other values used in Wavetrak VPCs, (2) the creation of a new SSH public/private key pair, and (3) the configuration of certain environment variables that are obtained from 1Password. These requirements are elaborated below in this README.

The following AWS components are created by the module:

- VPC
- Public / private subnets
- AWS KMS key
- S3 bucket
- AWS Parameter Store secret
- NAT Gateway
- VPN Gateway
- S3 Endpoint
- IAM policies/roles
- EC2 Key Pair
- OpenVPN EC2 instance
- Elastic IP address for OpenVPN instance
- SSL certificate for OpenVPN instance
- Route 53 records for SSL certificate creation
- Autoscaling group to ensure OpenVPN instance is always up

### Directory Structure

```bash
account-bootstrapping
├── Makefile
├── README.md
├── sl-dev-us-east-2
│   ├── main.tf
│   ├── outputs.tf
|   └── variables.tf
└── modules
    └── vpn
        ├── user_data.tpl
        ├── main.tf
        ├── ec2-trust-policy.json
        └── vpn-policy.json
```

### Important Notes

When using this module, be sure that the "cidr", "public_subnets" and "private_subnets" values don't conflict with existing VPCs. These are set in the `{env}/main.tf` file.

BE SURE TO ACCOUNT FOR NETWORKS LOCATED IN OTHER ACCOUNTS AND REGIONS.

If values conflict, you will not be able to create peered VPCs in the future.

## Basic Usage

To run the terraform stack, run the following from the `account-bootstrapping` folder: `ENV=<environment directory> make apply` (i.e. `ENV=sl-dev-us-east-2 make apply`).

A template of an environment-specific directory has been created, named `sl-dev-us-east-2`. When using this module, you should duplicate this template directory, and rename it according to your own usage. The folder format should be: `{brand}-{env}-{region}` (i.e. `sl-dev-us-east-2`). Additionally, there are multiple values that you'll need to configure in the `variables.tf` file. (See below for more information.)

## RSA Key Pair

This module requires that you create an SSH key pair. This key pair will be used to generate the PEM file that you will use to SSH onto the instance. Steps on how to do that below:

### Generating an SSH key pair

**Note:** Be careful not to overwrite your existing keys when doing this. Create a new name for these keys (see step 4).

1. Open your terminal application.
2. Paste the following text, but substitute your own email address. This will create a new ssh key, using the provided email as a label:
```$ ssh-keygen -t rsa -b 4096 -C "your_email@example.com"```
3. **Important:** When you're prompted to "Enter a file in which to save the key," **don't** accept the default name/location, as doing so will overwrite your existing keys. Instead, enter a different name for the keys. As a suggestion, you could name them `~/.ssh/{brand}-{env}-{region}` (i.e. `~/.ssh/sl-dev-us-east-2`).
4. If you want additional security, type a secure passphrase when prompted. Otherwise type "Enter" to bypass. (If you enter a passphrase, you'll need to use this phrase when you SSH onto the instance, and you'll need to save this passphrase in 1Password.)
5. Once you generate the SSH key pair, you must update the module's `public_key` in the `{env}/main.tf` file. It should take the form `public_key = "ssh-rsa AAAAB3NzaC...`
6. **Be sure to store both the private key and the public key in the 1Password's infrastructure vault.** This way others will be able to log into the EC2 instances on their own machines. The 1Password name should be in the following format: `{company} - {environment} - {region} - SSH - VPN` (I.e., `Surfline - Dev - US-EAST-2 - SSH - VPN`). If there are already other keys that match this value in 1Password, you will need to append additional relevant information to make this value unique in 1Password.

Additionally, don't forget to update any relevant documentation that you may create for use with your VPC.

## Route 53 Records

This module creates two DNS records in the `vpn.wavetrak.com` Route 53 hosted zone:

1. A public IP record. The IP for this DNS record is the Elastic IP created in Terraform. The DNS prefix (the portion that prepends `vpn.wavetrak.com`) is auto-generated based on company, environment, and region. If you need to change this value, you can configure it in the `vpn_domain_prefix_public` variable, which is in the `locals` block in `{env}/main.tf`.
2. A private IP record. Similar to the public record, it is autogenerated but can be configured in the `vpn_domain_prefix_private` variable, which is in the `locals` block in `{env}/main.tf`.

**Note:** If multiple people in the same company/region/environment attempt to use this module simultaneously, the Route 53 records will conflict. If this happens, change these values to make them unique.

This name is configured automatically as a locals variable in `{env}/main.tf`. By default, it will use the following format: `${var.company}-${var.environment}-${var.region}-public`. This can be modified if desired.

The IAM credentials used to create these DNS records correspond to the user named `vpn-serviceacct` in the production AWS account.

*Note:* the below records are created in user data--*not* Terraform. So if you use `terraform destroy` (or `make destroy`) to tear down this stack, the following resources won't be destroyed:

- Route 53 records
- S3 records
- SSM parameter store records

YOU WILL NEED TO MANUALLY DESTROY THESE RECORDS as part of the cleanup process.

## Service Account User

In order for Terraform to create the Route 53 records, it needs the credentials of the previously discussed service account user (`vpn-serviceacct`). This user has access to Route 53 actions in the `vpn.wavetrak.com` hosted zone. These credentials are located in 1Password.

**You will need to set these credentials in your environment variables as follows:**

1. Find the credentials in 1Password. They are saved in the Infrastructure vault under the name `AWS - Surfline Prod - Route53 Service User`.
2. Add the `aws_access_key_id` and `aws_secret_access_key` to your environment variables. **You will need to prepend them with `TF_VAR_` in order for Terraform to recognize them.** I.e., on a Unix system, you would add the environment variables as follows:

```bash
export TF_VAR_aws_access_key_id=<value>
export TF_VAR_aws_secret_access_key=<value>
```

See [terraform documentation](https://www.terraform.io/docs/configuration/variables.html#environment-variables) for more information about passing values via environment variables.

## SSL Certificate Creation

Certbot will use the `vpn_domain_prefix`s that are autogenerated in `{env}/main.tf` to create an SSL cert. Unless otherwise noted, `vpn.wavetrak.com` is used as the hosted zone domain. This hosted zone domain is configured by default in `{env}/main.tf` under the `vpn_subdomain` variable. The email that corresponds to this hosted zone is `domains@surfline.com`. This email value is set by default in `{env}/main.tf` under the `ssl_certificate_email` variable.

## Authentication via LDAP

LDAP is the method we'll use for authenticating incoming users from a VPN client. We are using OneLogin as the identity provider, so this is where all the roles/permissions are stored. By default, this module is configured to use the `Surfline_Dev_VPN` group. This can be changed in the `{env}/variables.tf` file. For more information about the LDAP authentication, please contact the IT department.
