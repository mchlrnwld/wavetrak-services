provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "account-bootstrapping/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "open-vpn" {
  source = "../../"

  company               = "sl"
  application           = "vpn"
  environment           = "dev"
  region                = "us-east-2"
  azs                   = ["us-east-2a", "us-east-2b", "us-east-2c"]
  cidr                  = "10.10.0.0/16"
  public_subnets        = ["10.10.11.0/24", "10.10.12.0/24", "10.10.13.0/24"]
  private_subnets       = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
  vpn_subdomain         = "vpn.wavetrak.com"
  vpn_group             = "Surfline_Dev_VPN"
  ssl_certificate_email = "domains@surfline.com"
  instance_type         = "t2.micro"
  image_id              = "ami-0f93b5fd8f220e428"
  public_key            = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCk8cNqR0Qtik15F29SBvEk/qKhv9bpkXwb1j01tWNX7UKbglCsQ0oWfiqSGb2BGEQfj+U+xt5ahzOqlooCHXKrfCkKgHBCVAsR0r0T6bb1NbtYcULD/TI5vPN/CLn3YETeaYSCaPLW2Wuwn4XbijRBrkIX2APvEdWK9zopmlUAtSbb44xMMbnap4GSzv2ugEXYnmp1AvuEPdQSbutQ7OAQjWX95C7b63WQdFKXaUCDhpDhtPgy2upF872kJX0MmPQG7oZXNl8pbNm/72LXraCcOmyct29bZqHGC3jZ8TRQIzG38F7s7xnYb1jD3tkLOBTA3VffpAL1vx7caqroiyLj1RiFC0IZqBMqOMc2O3smFZnX2exBpI1sWVY6lOGq/n16215gwxLx7shFaNWMK/GBd5YLKn/0hZ2VXySLf4CU5duNKfAUdT7CUMKXmBM5dLlafodP480wSyyjs9ArspnCHIA/zK98+EaIUneRcotMW/ISv1dm3KvxpEyPLLcqHB5zwc7rCw8ELB9FzGGPxHChunwtYhVNdXLMjYFsE6/2av4+MKU7Ko2rgnYCI6pOTlSVE611SujPq8zNpz7TGV8LfO3kEbLVO/Q5CJMzBbpgbx0d+ootJFh7bgCh9leSduSqT9r5jwSGoDnNrqkhGru1zkvfmSu8zUGBJhd6TFHqyw== poginni@surfline.com"
}
