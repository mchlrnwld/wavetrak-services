module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.company}-${var.environment}-vpc"

  azs             = var.azs
  cidr            = var.cidr
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  # Note on NAT Gateways: there is a cost tradeoff around using
  # one NAT Gateway vs using multiple. There is a fee of
  # $0.045 per Gateway per hour, plus data processing & transfer
  # costs. Limiting the architecture to one Gateway keeps the
  # $0.045 per-gateway fee down. However, when using a single
  # NAT Gateway, we will incur cross-availability-zone data transfer
  # fees for public internet traffic originating in an AZ that doesn't
  # have a NAT Gateway. So in cases where the volume of such traffic is
  # considerable, it may be desireable to use multiple Gateways.

  single_nat_gateway = true
  enable_nat_gateway = true
  enable_vpn_gateway = false

  enable_s3_endpoint = true

  tags = {
    Environment = var.environment
    OpenVPN_VPC = true
    Terraform   = true
  }
}

module "open-vpn" {
  source = "./modules/vpn"

  company               = var.company
  application           = var.application
  environment           = var.environment
  region                = var.region
  vpc_id                = module.vpc.vpc_id
  public_subnets        = module.vpc.public_subnets
  vpn_subdomain         = var.vpn_subdomain
  vpn_group             = var.vpn_group
  ssl_certificate_email = var.ssl_certificate_email
  aws_access_key_id     = var.aws_access_key_id
  aws_secret_access_key = var.aws_secret_access_key
  instance_type         = var.instance_type
  image_id              = var.image_id
  public_key            = var.public_key
}
