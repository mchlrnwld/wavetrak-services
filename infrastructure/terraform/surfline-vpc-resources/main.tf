module "surfline_nat_gateway" {
  source = "./modules/surfline-nat-gateway"

  company         = var.company
  application     = var.application
  environment     = var.environment
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets

  # Optionally create NAT Gateway
  create_nat_gateway = var.create_nat_gateway
}

module "surfline_vpc_endpoint" {
  source = "./modules/surfline-vpc-endpoint"

  vpc_id          = var.vpc_id
  private_subnets = var.private_subnets
  service_map     = var.service_map

  # Optionally create S3 Enpoint Resources
  create_s3_endpoint = var.create_s3_endpoint
}
