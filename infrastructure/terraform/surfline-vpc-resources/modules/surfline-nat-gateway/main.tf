data "aws_subnet" "public_subnets" {
  count = length(var.public_subnets)
  id    = element(var.public_subnets, count.index)
}

data "aws_subnet" "private_subnets" {
  count = length(var.private_subnets)
  id    = element(var.private_subnets, count.index)
}

data "aws_route_table" "route_tables" {
  count     = length(var.private_subnets)
  subnet_id = element(var.private_subnets, count.index)
}

resource "aws_eip" "ip" {
  count = var.create_nat_gateway == true ? length(var.public_subnets) : 0
  vpc   = true
}

resource "aws_nat_gateway" "gw" {
  count         = var.create_nat_gateway == true ? length(var.public_subnets) : 0
  allocation_id = element(aws_eip.ip.*.id, count.index)
  subnet_id     = element(var.public_subnets, count.index)

  tags = {
    Name        = "${lookup(data.aws_subnet.public_subnets.*.tags[count.index], "Name")} NAT Gateway"
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}
