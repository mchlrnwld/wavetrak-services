output "nat_gateway_subnet_id" {
  value = aws_nat_gateway.gw.*.subnet_id
}

output "nat_gateway_network_interface_id" {
  value = aws_nat_gateway.gw.*.network_interface_id
}

output "nat_gateway_private_ip" {
  value = aws_nat_gateway.gw.*.private_ip
}

output "nat_gateway_public_ip" {
  value = aws_nat_gateway.gw.*.public_ip
}
