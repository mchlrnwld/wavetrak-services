output "gateway_endpoint_id" {
  value = aws_vpc_endpoint.vpc_gateway_endpoint_s3.*.id
}

output "interface_endpoint_ids" {
  value = {
    for endpoint in aws_vpc_endpoint.vpc_interface_endpoints:
    endpoint.service_name => endpoint.subnet_ids...
  }
}
