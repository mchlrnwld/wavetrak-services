data "aws_subnet" "private_subnets" {
  count = length(var.private_subnets)
  id    = element(var.private_subnets, count.index)
}

data "aws_route_table" "route_tables" {
  count     = length(var.private_subnets)
  subnet_id = element(var.private_subnets, count.index)
}

data "aws_vpc" "vpc_cidr_blocks" {
  id = var.vpc_id
}

resource "aws_security_group" "vpc_internal_traffic" {
  count       = length(var.service_map) > 0 ? 1 : 0
  name        = "vpc_internal_traffic"
  description = "Traffic originating from within VPC."
  vpc_id      = data.aws_subnet.private_subnets.0.vpc_id

  ingress {
    description = "All internally originated traffic."
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.vpc_cidr_blocks.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_vpc_endpoint" "vpc_gateway_endpoint_s3" {
  count               = var.create_s3_endpoint == true ? 1 : 0
  vpc_id              = data.aws_subnet.private_subnets.0.vpc_id
  service_name        = "com.amazonaws.us-west-1.s3"
  vpc_endpoint_type   = "Gateway"
  route_table_ids     = data.aws_route_table.route_tables.*.id
  private_dns_enabled = false
}

resource "aws_vpc_endpoint" "vpc_interface_endpoints" {
  for_each            = var.service_map
  vpc_id              = data.aws_subnet.private_subnets.0.vpc_id
  subnet_ids          = var.private_subnets
  service_name        = each.value
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.vpc_internal_traffic[0].id]
  private_dns_enabled = true
}
