provider "aws" {
  region  = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-nat-gateway/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-vpc-resources" {
  source = "../../"

  company            = "wt"
  application        = "surfline-vpc-resources"
  environment        = "prod"
  create_nat_gateway = true
  create_s3_endpoint = true
  vpc_id             = "vpc-116fdb74"
  public_subnets     = ["subnet-debb6987", "subnet-baab36df"]
  private_subnets    = ["subnet-d1bb6988", "subnet-85ab36e0"]
  service_map = {
    "cloudtrail"      = "com.amazonaws.us-west-1.cloudtrail",
    "cloudwatch"      = "com.amazonaws.us-west-1.monitoring",
    "cloudwatch_logs" = "com.amazonaws.us-west-1.logs",
    "ecs"             = "com.amazonaws.us-west-1.ecs",
    "kinesis"         = "com.amazonaws.us-west-1.kinesis-streams",
    "sns"             = "com.amazonaws.us-west-1.sns",
    "sqs"             = "com.amazonaws.us-west-1.sqs",
    "ssm"             = "com.amazonaws.us-west-1.ssm"
  }
}
