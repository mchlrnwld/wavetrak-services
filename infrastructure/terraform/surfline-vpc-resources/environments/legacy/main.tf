provider "aws" {
  region  = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-legacy"
    key    = "surfline-vpc-resources/legacy/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-vpc-resources" {
  source = "../../"

  company            = "wt"
  application        = "surfline-vpc-resources"
  environment        = "dev"
  create_nat_gateway = false
  create_s3_endpoint = true
  vpc_id             = "vpc-29f3f241"
  public_subnets     = ["subnet-0e226848", "	subnet-68a1af0a"]
  private_subnets    = ["subnet-00226846", "subnet-6aa1af08", "subnet-f2c5d290"]
  service_map        = {}
}
