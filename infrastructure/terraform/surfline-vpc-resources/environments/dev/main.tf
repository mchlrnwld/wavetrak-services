provider "aws" {
  region  = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-vpc-resources/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-vpc-resources" {
  source = "../../"

  company            = "wt"
  application        = "surfline-vpc-resources"
  environment        = "dev"
  create_nat_gateway = false
  create_s3_endpoint = true
  vpc_id             = "vpc-981887fd"
  public_subnets     = ["subnet-0b09466e", "subnet-f4d458ad"]
  private_subnets    = ["subnet-f2d458ab", "subnet-0909466c"]
  service_map        = {}
}
