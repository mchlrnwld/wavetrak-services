variable "company" {
  description = "The company that these resources are being created for"
  default     = ""
}

variable "application" {
  description = "The application that these resources are being created for"
  default     = ""
}

variable "environment" {
  description = "The environment that these resources are being created in"
  default     = ""
}

variable "public_subnets" {
  description = "The public subnets that these resources are being created in"
  type        = list(string)
  default     = []
}

variable "private_subnets" {
  description = "The private subnets that these resources are being created in"
  type        = list(string)
  default     = []
}

variable "create_nat_gateway" {
  description = "Specifies whether or not to create NAT gateway resources."
  default     = false
}

variable "create_s3_endpoint" {
  description = "Specifies whether or not to create an S3 endpoint resource."
  default     = false
}

variable "vpc_id" {
  description = "The id of the VPC in which to create the resources."
  default     = ""
}

variable "service_map" {
  description = "The map of services for which to create VPC interface endpoints."
  type        = map(string)
  default     = {}
}
