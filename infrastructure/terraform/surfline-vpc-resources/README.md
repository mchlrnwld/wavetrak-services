# surfline-vpc-resources Infrastructure

## Summary

This project provisions NAT-related VPC resources for Surfline accounts.

It optionally creates a NAT Gateway, an S3 VPC Gateway Endpoint, and Interface endpoints for specified services.

## Creating a NAT Gateway

To create a NAT Gateway, set the `create_nat_gateway` variable to `true`. For each NAT Gateway that is created, an elastic EIP will also created for the Gateway's use. This terraform stack will create one NAT Gateway per subnet specified.

**Important:** In order to use the NAT Gateway created by this terraform stack, you must add it to the route table of the desired subnet. This will point the internet traffic at the NAT Gateway. This link describes how to do this:

[NAT Gateways - Updating Your Route Table](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html#nat-gateway-create-route)

## Creating an S3 Gateway Endpoint

To create an S3 Gateway Endpoint, set the `create_s3_endpoint` variable to `true`. See [here](https://docs.aws.amazon.com/vpc/latest/userguide/vpce-gateway.html) for more information on S3 Gateway endpoints.

## Creating Selected Interface Endpoints

Interface endpoints can be created for various services. This module will create an interface endpoint for all the services specified in the `service_map` variable. This variable is a map in which the `key` should specifiy what you want to call the resource, and the `value` is the AWS-specified endpoint name for the service. In order to modify the resource types that will receive a VPC Interface endpoint, udpate the `service_map` variable accordingly.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.28`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments are `dev`, `legacy` and `prod`.
