output "bucket_arn" {
  value = aws_s3_bucket.bucket.arn
}

output "domain_name" {
  value = aws_cloudfront_distribution.cdn[0].domain_name
}

output "domain_alias" {
  value = aws_cloudfront_distribution.cdn[0].aliases
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.cdn[0].hosted_zone_id
}

output "origin_access_identity" {
  value = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
}
