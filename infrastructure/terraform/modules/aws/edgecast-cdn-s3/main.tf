data "aws_region" "current" {
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

# S3 bucket for content
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.company}-${var.application}-${var.service}-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD", "DELETE", "POST", "PUT"]
    allowed_origins = [var.allowed_origins]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = var.versioning_enabled
  }

  lifecycle_rule {
    prefix  = ""
    enabled = var.versioning_enabled

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  tags = {
    Name        = "${var.company}-${var.application}-${var.service}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# S3 bucket policy for CloudFront access
data "template_file" "policy" {
  template = templatefile("${path.module}/resources/s3-policy.json", {
    s3_bucket  = aws_s3_bucket.bucket.arn
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.template_file.policy.rendered
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.comment
}

data "aws_acm_certificate" "cdn_acm" {
  count    = var.cdn_acm_count
  provider = aws.us-east-1
  domain   = format("*.%s", element(var.cdn_acm_domains, count.index))
  statuses = ["ISSUED"]
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  count = var.cdn_acm_count
  origin {
    domain_name = aws_s3_bucket.bucket.bucket_domain_name
    origin_id   = "S3-${aws_s3_bucket.bucket.bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${aws_s3_bucket.bucket.bucket}"
    compress         = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment
  # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
  # force an interpolation expression to be interpreted as a list by wrapping it
  # in an extra set of list brackets. That form was supported for compatibility in
  # v0.11, but is no longer supported in Terraform v0.12.
  #
  # If the expression in the following list itself returns a list, remove the
  # brackets to avoid interpretation as a list of lists. If the expression
  # returns a single list item then leave it as-is and remove this TODO comment.
  aliases = var.cdn_fqdn[element(var.cdn_acm_domains, count.index)]

  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    acm_certificate_arn      = element(data.aws_acm_certificate.cdn_acm.*.arn, count.index)
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
