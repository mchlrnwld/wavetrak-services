variable "company" {
}

variable "environment" {
}

variable "application" {
}

variable "service" {
}

variable "cdn_acm_count" {
}

variable "cdn_acm_domains" {
  type = list(string)
}

variable "comment" {
}

variable "cdn_fqdn" {
  type = map(list(string))
}

variable "versioning_enabled" {
}

variable "allowed_origins" {
}

variable "default_ttl" {
  default = 3600
}

variable "max_ttl" {
  default = 86400
}
