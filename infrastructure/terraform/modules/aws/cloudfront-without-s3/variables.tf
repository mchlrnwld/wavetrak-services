variable "company" {
}

variable "environment" {
}

variable "application" {
}

variable "service" {
}

variable "s3_bucket_map" {
  type = list(string)
}

variable "cdn_s3_count" {
}

variable "comment" {
}

variable "versioning_enabled" {
}

variable "allowed_origins" {
}

variable "default_ttl" {
  default = "86400"
}

variable "max_ttl" {
  default = "604800"
}

