data "aws_region" "current" {
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

# S3 bucket policy for CloudFront access
data "template_file" "policy" {
  count = var.cdn_s3_count
  template = templatefile("${path.module}/resources/s3-policy.json", {
    s3_bucket  = "arn:aws:s3:::${element(split(",", element(var.s3_bucket_map, count.index)), "2")}"
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.comment
}

data "aws_acm_certificate" "cdn_acm" {
  count    = var.cdn_s3_count
  provider = aws.us-east-1
  domain   = element(split(",", element(var.s3_bucket_map, count.index)), "1")
  statuses = ["ISSUED"]
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  count = var.cdn_s3_count
  origin {
    domain_name = "${element(split(",", element(var.s3_bucket_map, count.index)), "2")}.s3.amazonaws.com"
    origin_id   = "S3-${element(split(",", element(var.s3_bucket_map, count.index)), "2")}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = "${element(split(",", element(var.s3_bucket_map, count.index)), "2")} ${var.comment}"
  aliases         = [element(split(",", element(var.s3_bucket_map, count.index)), "0")]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${element(split(",", element(var.s3_bucket_map, count.index)), "2")}"
    compress         = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = length(split(",", element(var.s3_bucket_map, count.index))) == 4 ? element(split(",", element(var.s3_bucket_map, count.index)), "3") : var.default_ttl
    max_ttl                = length(split(",", element(var.s3_bucket_map, count.index))) == 4 ? element(split(",", element(var.s3_bucket_map, count.index)), "3") : var.max_ttl
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    acm_certificate_arn      = element(data.aws_acm_certificate.cdn_acm.*.arn, count.index)
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
