output "lambda_function_arn" {
  value = module.lambda.lambda_function_arn
}

output "lambda_function_name" {
  value = module.lambda.lambda_function_name
}

output "lambda_role_arn" {
  value = module.lambda.lambda_role_arn
}

output "lambda_role_name" {
  value = module.lambda.lambda_role_name
}

output "topic" {
  value = module.sns_topic.topic
}

output "topic_policy" {
  value = module.sns_topic.topic_policy
}
