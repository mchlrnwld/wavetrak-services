# Create the Lambda function
module "lambda" {
  source = "../lambda"

  company     = var.company
  application = var.application
  environment = var.environment

  artifacts_bucket    = var.artifacts_bucket
  function_name       = var.function_name
  runtime             = var.runtime
  service_handler     = var.service_handler
  memory_size         = var.memory_size
  timeout             = var.timeout
  tracing_config_mode = var.tracing_config_mode
  internal_sg_group   = var.internal_sg_group
  sg_all_servers      = var.sg_all_servers
  instance_subnets    = var.instance_subnets

  newrelic_lambda_nrql_create_alert                              = var.newrelic_lambda_nrql_create_alert
  newrelic_policy_name                                           = var.newrelic_policy_name
  newrelic_lambda_nrql_alert_type                                = var.newrelic_lambda_nrql_alert_type
  newrelic_lambda_nrql_alert_name                                = var.newrelic_lambda_nrql_alert_name
  newrelic_lambda_nrql_alert_description                         = var.newrelic_lambda_nrql_alert_description
  newrelic_lambda_nrql_alert_enabled                             = var.newrelic_lambda_nrql_alert_enabled
  newrelic_lambda_nrql_alert_value_function                      = var.newrelic_lambda_nrql_alert_value_function
  newrelic_lambda_nrql_alert_violation_time_limit_seconds        = var.newrelic_lambda_nrql_alert_violation_time_limit_seconds
  newrelic_lambda_name                                           = var.newrelic_lambda_name
  newrelic_lambda_nrql_runbook_url                               = var.newrelic_lambda_nrql_runbook_url
  newrelic_lambda_nrql_alert_evaluation_offset                   = var.newrelic_lambda_nrql_alert_evaluation_offset
  newrelic_lambda_nrql_alert_operator                            = var.newrelic_lambda_nrql_alert_operator
  newrelic_lambda_nrql_alert_error_count_threshold               = var.newrelic_lambda_nrql_alert_error_count_threshold
  newrelic_lambda_nrql_alert_error_count_threshold_duration      = var.newrelic_lambda_nrql_alert_error_count_threshold_duration
  newrelic_lambda_nrql_alert_error_count_threshold_occurrences   = var.newrelic_lambda_nrql_alert_error_count_threshold_occurrences
  newrelic_lambda_nrql_warning_operator                          = var.newrelic_lambda_nrql_warning_operator
  newrelic_lambda_nrql_warning_error_count_threshold             = var.newrelic_lambda_nrql_warning_error_count_threshold
  newrelic_lambda_nrql_warning_error_count_threshold_duration    = var.newrelic_lambda_nrql_warning_error_count_threshold_duration
  newrelic_lambda_nrql_warning_error_count_threshold_occurrences = var.newrelic_lambda_nrql_warning_error_count_threshold_occurrences
}

# Create the SNS topic that will invoke the Lambda function
module "sns_topic" {
  source = "../sns"

  environment = var.environment
  topic_name  = "${var.company}-${var.application}-${var.function_name}-sns"
}

# Allow Lambda to be invoked by an SNS topic
resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  function_name = module.lambda.lambda_function_name
  source_arn    = module.sns_topic.topic
}

# Subscribe Lambda to SNS topic
resource "aws_sns_topic_subscription" "topic_subscription" {
  protocol  = "lambda"
  endpoint  = module.lambda.lambda_function_arn
  topic_arn = module.sns_topic.topic
}
