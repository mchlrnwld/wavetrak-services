data "aws_region" "current" {
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

# S3 bucket for content
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.company}-${var.application}-${var.service}-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD", "DELETE", "POST", "PUT"]
    allowed_origins = [var.allowed_origins]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = var.versioning_enabled
  }

  lifecycle_rule {
    prefix  = ""
    enabled = var.versioning_enabled

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  tags = {
    Name        = "${var.company}-${var.application}-${var.service}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# S3 bucket policy for CloudFront access
data "template_file" "policy" {
  template = templatefile("${path.module}/resources/s3-bucket-policy.tpl", {
    s3_bucket  = aws_s3_bucket.bucket.arn
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.template_file.policy.rendered
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.comment
}

data "aws_acm_certificate" "cdn_acm" {
  count       = var.cdn_acm_count
  provider    = aws.us-east-1
  domain      = format("*.%s", element(var.cdn_acm_domains, count.index))
  statuses    = ["PENDING_VALIDATION", "ISSUED"]
  most_recent = true
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  count = length(var.cdn_acm_domains)
  origin {
    domain_name = aws_s3_bucket.bucket.bucket_regional_domain_name
    origin_id   = "S3-${aws_s3_bucket.bucket.bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = var.comment
  aliases             = lookup(var.cdn_fqdn, element(var.cdn_acm_domains, 1))
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${aws_s3_bucket.bucket.bucket}"
    compress         = true

    forwarded_values {
      headers      = var.whitelist_headers
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl

    dynamic "lambda_function_association" {
      for_each = [for l in var.lambda_function_associations: {
        event_type    = l.event_type
        lambda_arn    = l.lambda_arn
        include_body  = l.include_body
      }]

      content {
        event_type   = lambda_function_association.value.event_type
        lambda_arn   = lambda_function_association.value.lambda_arn
        include_body = lambda_function_association.value.include_body
      }
    }
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    acm_certificate_arn      = element(data.aws_acm_certificate.cdn_acm.*.arn, count.index)
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
