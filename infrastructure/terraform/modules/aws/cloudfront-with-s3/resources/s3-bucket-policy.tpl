{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "allow CloudFront to get bucket objects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${cloudfront}"
      },
      "Action": "s3:GetObject",
      "Resource": "${s3_bucket}/*"
    }
  ]
}
