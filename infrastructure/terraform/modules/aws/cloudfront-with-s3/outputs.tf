output "bucket_arn" {
  value = aws_s3_bucket.bucket.arn
}

output "bucket_name" {
  value = aws_s3_bucket.bucket.id
}

output "domain_name" {
  value = aws_cloudfront_distribution.cdn.*.domain_name
}

output "domain_alias" {
  value = aws_cloudfront_distribution.cdn.*.aliases
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.cdn.*.hosted_zone_id
}
