# SQS with SNS Subscription Module

## Summary

This module creates an SQS queue along with an SNS subscription, while also granting SNS the permission to send to the queue.

## Usage

```hcl
module "sqs_with_sns_subscription" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure/terraform/modules/aws/sqs-with-sns-subscription"
  application = "${var.application}"
  environment = "${var.environment}"
  queue_name  = "${var.queue_name}"
  topic_name  = "${var.topic_name}"
}
```
