provider "aws" {
  alias = "topic_provider"
}

locals {
  topic_count = length(var.topic_arns)
}

resource "aws_sns_topic_subscription" "sns_topic_subscription" {
  count     = local.topic_count
  provider  = aws.topic_provider
  topic_arn = element(var.topic_arns, count.index)
  protocol  = "sqs"
  endpoint  = module.sqs.queue_arn
}

module "sqs" {
  source = "../sqs"

  environment                = var.environment
  queue_name                 = "${var.company}-${var.application}-${var.queue_name}"
  application                = var.application
  company                    = var.company
  delay_seconds              = var.delay_seconds
  visibility_timeout_seconds = var.visibility_timeout_seconds
  max_message_size           = var.max_message_size
  message_retention_seconds  = var.message_retention_seconds
  receive_wait_time_seconds  = var.receive_wait_time_seconds
}

resource "aws_sqs_queue_policy" "sqs_read_policy" {
  queue_url = module.sqs.queue_url
  policy = templatefile("${path.module}/resources/sqs-read-policy.json", {
    sqs_queue = module.sqs.queue_arn
    sns_topic = jsonencode(var.topic_arns)
  })
}
