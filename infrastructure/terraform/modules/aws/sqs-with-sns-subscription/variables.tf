variable "application" {
  description = "The application that this queue is being created for"
}

variable "environment" {
  description = "The environment that this SQS queue is being created in"
}

variable "company" {
  description = "The company that this infrastructure belongs to"
}

variable "queue_name" {
  description = "The name of the SQS queue being created"
}

variable "delay_seconds" {
  description = "The time in seconds that the delivery of all messages in the queue will be delayed."
  default     = 0
}

variable "visibility_timeout_seconds" {
  description = "The visibility timeout for the queue."
  default     = 30
}

variable "max_message_size" {
  description = "The limit of how many bytes a message can contain before Amazon SQS rejects it."
  default     = 262144
}

variable "message_retention_seconds" {
  description = "The number of seconds Amazon SQS retains a message."
  default     = 345600
}

variable "receive_wait_time_seconds" {
  description = "The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning."
  default     = 0
}

variable "topic_arns" {
  description = "The SNS topics that the SQS will subscribe to"
  type        = list(string)
  default     = []
}
