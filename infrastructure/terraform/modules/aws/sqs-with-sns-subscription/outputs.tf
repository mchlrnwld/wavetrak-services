output "queue_arn" {
  value = module.sqs.queue_arn
}

output "queue_policy" {
  value = module.sqs.queue_policy
}

output "queue_url" {
  value = module.sqs.queue_url
}
