provider "aws" {
  region = "us-east-1"
  alias  = "east"
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  origin {
    domain_name = var.wc_origin_fqdn
    origin_id   = "HTTP-${var.wc_origin_fqdn}"

    custom_origin_config {
      http_port                = 1935
      https_port               = 443
      origin_protocol_policy   = "http-only"
      origin_ssl_protocols     = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      origin_keepalive_timeout = 30
    }
  }

  origin {
    domain_name = var.ec_origin_fqdn
    origin_id   = "HTTP-${var.ec_origin_fqdn}"

    custom_origin_config {
      http_port                = 1935
      https_port               = 443
      origin_protocol_policy   = "http-only"
      origin_ssl_protocols     = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      origin_keepalive_timeout = 30
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment
  aliases         = [var.cdn_fqdn]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "HTTP-${var.wc_origin_fqdn}"
    compress         = true

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "HTTP-${var.wc_origin_fqdn}"
    compress         = true
    path_pattern     = "/${var.path_pattern_prefix}-west/*"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "HTTP-${var.ec_origin_fqdn}"
    compress         = true
    path_pattern     = "/${var.path_pattern_prefix}-east/*"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
    Name        = "${var.company}-${var.application}-${var.service}-${var.environment}"
  }

  viewer_certificate {
    acm_certificate_arn      = var.cdn_acm
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

resource "aws_sns_topic" "cdn_alarm_topic" {
  provider = aws.east
  name     = "sl-cam-cdn-alert-topic-${var.environment}"
}

resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  provider               = aws.east
  topic_arn              = aws_sns_topic.cdn_alarm_topic.arn
  protocol               = "https"
  endpoint               = var.pagerduty_hook
  endpoint_auto_confirms = true
  count                  = var.pagerduty_count
}

resource "aws_iam_policy" "cam_cdn_sns_policy" {
  name        = "sns_policy_${var.company}-cam-cdn_${var.environment}"
  description = "policy to publish to sns topic"
  policy = templatefile("${path.module}/resources/sns-policy.json", {
    sns_topic  = aws_sns_topic.cdn_alarm_topic.id
    region     = var.region
    account_id = var.account_id
  })
}

resource "aws_cloudwatch_metric_alarm" "cdn_500_alarm" {
  provider                  = aws.east
  alarm_name                = "${var.company}-${var.application}-${var.service}-${var.environment}-cloudfront-500_errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.cdn_500_alarm_period_count
  metric_name               = "5xxErrorRate"
  namespace                 = "AWS/CloudFront"
  period                    = var.cdn_500_alarm_period_len
  statistic                 = "Average"
  threshold                 = var.cdn_500_alarm_critical_pct
  alarm_description         = "This metric monitors cloudfront 5xx errors"
  insufficient_data_actions = [aws_sns_topic.cdn_alarm_topic.arn]
  alarm_actions             = [aws_sns_topic.cdn_alarm_topic.arn]

  dimensions = {
    DistributionId = aws_cloudfront_distribution.cdn.id
    Region         = "Global"
  }
}
