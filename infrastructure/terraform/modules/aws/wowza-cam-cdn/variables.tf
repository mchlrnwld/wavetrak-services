variable "account_id" {
}

variable "region" {
}

variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "service" {
}

variable "versioning_enabled" {
}

variable "comment" {
}

variable "cdn_fqdn" {
}

variable "cdn_acm" {
}

variable "cdn_500_alarm_critical_pct" {
  default = 10
}

variable "cdn_500_alarm_period_count" {
  default = 3
}

variable "cdn_500_alarm_period_len" {
  default = 60
}

variable "wc_origin_fqdn" {
}

variable "ec_origin_fqdn" {
}

variable "pagerduty_hook" {
}

variable "pagerduty_count" {
}

variable "path_pattern_prefix" {
}
