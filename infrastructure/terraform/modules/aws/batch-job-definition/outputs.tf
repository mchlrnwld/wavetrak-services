output "batch_job_definition_arn" {
  value = aws_batch_job_definition.batch.arn
}

output "batch_job_iam_role_name" {
  value = aws_iam_role.batch.name
}

output "batch_job_iam_role_arn" {
  value = aws_iam_role.batch.arn
}
