variable "company" {
  description = "Company prefix."
}

variable "application" {
  description = "Application name."
}

variable "environment" {
  description = "Environment name. Used for resource name suffixes and image tag."
}

variable "custom_policy" {
  description = "Custom IAM policy to attach to job role."
  default     = null
}

# Example:
# default = {
#   "image" : "test-image",
#   "memory" : 1024,
#   "vcpus" : 1,
#   "volumes" : [
#    {
#      "host" : {
#        "sourcePath" : "/tmp"
#      },
#      "name" : "tmp"
#    }
#  ],
#  "mountPoints" : [
#    {
#      "sourceVolume" : "tmp",
#      "containerPath" : "/tmp",
#      "readOnly" : false
#    }
#  ]
#}
variable "container_properties" {
  description = "An object of properties (memory, vCPUs, volumes, etc) used to define the docker container."
}
