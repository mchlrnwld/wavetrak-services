locals {
  container_iam_role = { "jobRoleArn" : aws_iam_role.batch.arn }
}

resource "aws_iam_role" "batch" {
  name               = "${var.company}-${var.application}-batch-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-task-policy.json")
}

resource "aws_iam_policy" "batch" {
  name   = "${var.company}-${var.application}-batch-${var.environment}"
  policy = file("${path.module}/resources/policy.json")
}

resource "aws_iam_policy" "custom" {
  name   = "${var.company}-${var.application}-batch-custom-${var.environment}"
  count  = var.custom_policy != null ? 1 : 0
  policy = var.custom_policy
}

resource "aws_iam_role_policy_attachment" "batch" {
  role       = aws_iam_role.batch.name
  policy_arn = aws_iam_policy.batch.arn
}

resource "aws_iam_role_policy_attachment" "s3" {
  count      = var.custom_policy != null ? 1 : 0
  role       = aws_iam_role.batch.name
  policy_arn = aws_iam_policy.custom[0].arn
}

# Environment variables may be overridden on job submit
#
# aws batch submit-job \
#   --job-name wt-jobs-download-data-model-batch-job-dev \
#   --job-queue wt-jobs-common-download-files-job-queue-dev \
#   --job-definition wt-jobs-download-data-model-batch-job-definition-dev \
#   --container-overrides '{"environment": [{"name": "DATE", "value": "2019-12-09"}]}'
#

resource "aws_batch_job_definition" "batch" {
  name = "${var.company}-${var.application}-${var.environment}"
  type = "container"

  container_properties = jsonencode(merge(
    local.container_iam_role,
    var.container_properties
  ))
}
