# AWS Batch Job Definition

This module creates an AWS Batch Job Definition.

## Simple Usage

```hcl
module "batch_download_job_definition" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company              = "wt"
  application          = "jobs-download-data-model"
  environment          = "sandbox"
  s3_bucket            = "surfline-science-s3-dev"
  s3_key_prefix        = "data/model"
  container_properties = {"image" : "test-image", "memory" : 1024, "vcpus" : 1}
}
```

## Advanced usage, with volume mount:

```hcl
variable "container_properties" {

   "image" : "test-image",
   "memory" : 1024,
   "vcpus" : 1,
   "volumes" : [
    {
      "host" : {
        "sourcePath" : "/tmp"
      },
      "name" : "tmp"
    }
  ],
  "mountPoints" : [
    {
      "sourceVolume" : "tmp",
      "containerPath" : "/tmp",
      "readOnly" : false
    }
  ]
}

module "batch_download_job_definition" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company              = "wt"
  application          = "jobs-download-data-model"
  environment          = "sandbox"
  s3_bucket            = "surfline-science-s3-dev"
  s3_key_prefix        = "data/model"
  container_properties = var.container_properties
}
```
