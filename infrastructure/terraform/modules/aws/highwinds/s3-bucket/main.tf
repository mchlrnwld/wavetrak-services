data "aws_region" "current" {
}

# S3 bucket for content
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.company}-${var.application}-${var.service}-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD", "DELETE", "POST", "PUT"]
    allowed_origins = [var.allowed_origins]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = var.versioning_enabled
  }

  lifecycle_rule {
    prefix  = ""
    enabled = var.versioning_enabled

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  tags = {
    Name        = "${var.company}-${var.application}-${var.service}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# S3 bucket policy for CloudFront access
data "template_file" "policy" {
  template = templatefile("${path.module}/resources/s3-policy.json", {
    s3_bucket  = aws_s3_bucket.bucket.arn
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.template_file.policy.rendered
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.comment
}
