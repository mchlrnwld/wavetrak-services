output "bucket_arn" {
  value = aws_s3_bucket.bucket.arn
}

output "bucket_name" {
  value = aws_s3_bucket.bucket.id
}

output "bucket_domain" {
  value = aws_s3_bucket.bucket.bucket_domain_name
}

output "origin_access_identity" {
  value = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
}

