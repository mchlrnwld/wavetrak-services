variable "company" {
}

variable "environment" {
}

variable "application" {
}

variable "service" {
}

variable "s3_bucket" {
}

variable "s3_bucket_domain" {
}

variable "origin_access_identity" {
}

variable "cdn_acm_domain" {
}

variable "cdn_aliases" {
  type = list(string)
}

variable "origin_path" {
  default = ""
}

variable "comment" {
}

variable "default_ttl" {
  default = "86400"
}

variable "max_ttl" {
  default = "604800"
}
