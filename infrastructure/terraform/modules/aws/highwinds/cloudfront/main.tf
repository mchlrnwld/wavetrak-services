provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

data "aws_region" "current" {
}

data "aws_acm_certificate" "cdn_acm" {
  provider = aws.us-east-1
  domain   = format("*.%s", var.cdn_acm_domain)
  statuses = ["ISSUED"]
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  origin {
    domain_name = var.s3_bucket_domain
    origin_id   = "S3-${var.s3_bucket}"
    origin_path = var.origin_path

    s3_origin_config {
      origin_access_identity = var.origin_access_identity
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment
  aliases         = var.cdn_aliases

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${var.s3_bucket}"
    compress         = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.cdn_acm.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
