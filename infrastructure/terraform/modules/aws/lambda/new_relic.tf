data "newrelic_alert_policy" "policy" {
  count = var.newrelic_lambda_nrql_create_alert ? 1 : 0
  name  = var.newrelic_policy_name
}

resource "newrelic_nrql_alert_condition" "lambda_alert" {
  count                        = var.newrelic_lambda_nrql_create_alert ? 1 : 0
  name                         = var.newrelic_lambda_nrql_alert_name
  policy_id                    = data.newrelic_alert_policy.policy[0].id
  type                         = var.newrelic_lambda_nrql_alert_type
  description                  = var.newrelic_lambda_nrql_alert_description
  enabled                      = var.newrelic_lambda_nrql_alert_enabled
  runbook_url                  = var.newrelic_lambda_nrql_runbook_url
  value_function               = var.newrelic_lambda_nrql_alert_value_function
  violation_time_limit_seconds = var.newrelic_lambda_nrql_alert_violation_time_limit_seconds

  nrql {
    query             = templatefile("${path.module}/resources/nrql-query.tmpl", { newrelic_lambda_name = var.newrelic_lambda_name })
    evaluation_offset = var.newrelic_lambda_nrql_alert_evaluation_offset
  }

  critical {
    operator              = var.newrelic_lambda_nrql_alert_operator
    threshold             = var.newrelic_lambda_nrql_alert_error_count_threshold
    threshold_duration    = var.newrelic_lambda_nrql_alert_error_count_threshold_duration
    threshold_occurrences = var.newrelic_lambda_nrql_alert_error_count_threshold_occurrences
  }

  warning {
    operator              = var.newrelic_lambda_nrql_warning_operator
    threshold             = var.newrelic_lambda_nrql_warning_error_count_threshold
    threshold_duration    = var.newrelic_lambda_nrql_warning_error_count_threshold_duration
    threshold_occurrences = var.newrelic_lambda_nrql_warning_error_count_threshold_occurrences
  }
}
