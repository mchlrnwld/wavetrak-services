# Lambda Module

## Summary

This module creates a Lambda function and policies granting the Lambda access
to resources.

## Usage

The following example shows invoking the `lambda` module. In
this particular example, an `update-cache` function is configured to
run for the `careers` application.

```hcl
module "lambda" {
  source              = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/lambda"
  application         = "careers"
  company             = "sl"
  environment         = "prod"
  artifacts_bucket    = "sl-artifacts-prod"
  function_name       = "update-cache"
  runtime             = "python2.7"
  service_handler     = "service.handler"
  memory_size         = "128"
  timeout             = "30"
  tracing_config_mode = "Active"
  internal_sg_group   = "sg-a5a0e5c0"
  sg_all_servers      = "sg-a4a0e5c1"

  instance_subnets    = [
    "subnet-d1bb6988",
    "subnet-85ab36e0",
  ]
}
```
