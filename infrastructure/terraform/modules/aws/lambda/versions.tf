terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = "~> 2.70"
    newrelic = {
      source  = "newrelic/newrelic",
      version = "~> 2.10"
    }
  }
}
