# General variables
variable "application" {
  description = "The application name"
  default     = ""
}

variable "company" {
  description = "The company (sl, bw, ft, wt, msw)"
  default     = ""
}

variable "environment" {
  description = "The environment (sbox, staging, prod)"
  default     = ""
}

# Function variables
variable "artifacts_bucket" {
  description = "The artifacts S3 bucket"
  default     = ""
}

variable "function_name" {
  description = "The function name"
  default     = ""
}

variable "runtime" {
  description = "The runtime that the Lambda function should use"
  default     = "python2.7"
}

variable "service_handler" {
  description = "The service handler that the Lambda function should use"
  default     = "service.handler"
}

variable "memory_size" {
  description = "The memory size of the Lambda function"
  default     = 128
}

variable "timeout" {
  description = "The timeout of the Lambda function"
  default     = 30
}

variable "tracing_config_mode" {
  description = "The tracing config mode of the Lambda function (Active, PassThrough)"
  default     = "PassThrough"
}

# Network variables
variable "instance_subnets" {
  description = "The subnets that the function will execute in"
  type        = list(string)
  default     = []
}

variable "internal_sg_group" {
  description = "The internal servers security group"
  default     = ""
}

variable "sg_all_servers" {
  description = "The all-servers security group"
  default     = ""
}

variable "concurrent_executions" {
  description = "Concurrent executions"
  default     = "-1"
}

# KMS
variable "key_description" {
  description = "Description of the resource's use"
  default     = "Function specific KMS key."
}

variable "key_usage" {
  description = "Intended key operations"
  default     = "ENCRYPT_DECRYPT"
}

variable "key_is_enabled" {
  description = "Specifies whether key is enabled"
  default     = true
}

variable "key_enable_rotation" {
  description = "Specifies whether key rotation is enabled"
  default     = false
}

variable "key_deletion_window_in_days" {
  description = "Number of days a secret is retained after being marked for deletion"
  default     = 30
}

# New Relic Lambda NRQL alert variables

variable "newrelic_lambda_nrql_create_alert" {
  description = "Create New Relic alert with lambda function infrastructure"
  default     = false
}

variable "newrelic_policy_name" {
  description = "Name of New Relic policy"
  default     = ""
}

variable "newrelic_lambda_nrql_alert_type" {
  description = "Type of the condition"
  default     = "static"
}

variable "newrelic_lambda_nrql_alert_name" {
  description = "Name of the New Relic condition to be created"
  default     = ""
}

variable "newrelic_lambda_nrql_alert_description" {
  description = "New Relic condition description"
  default     = "New Relic alert description. See conditions for more details."
}

variable "newrelic_lambda_nrql_alert_enabled" {
  description = "Enable New Relic condition alert"
  default     = true
}

variable "newrelic_lambda_nrql_alert_value_function" {
  description = "Value function; possible values are single_value, sum"
  default     = "single_value"
}

variable "newrelic_lambda_nrql_alert_violation_time_limit_seconds" {
  description = "Limit, in seconds, that will automatically force-close a long-lasting violation"
  default     = 86400
}

variable "newrelic_lambda_name" {
  description = "Name of the lambda function on which to alert"
  default     = ""
}

variable "newrelic_lambda_nrql_runbook_url" {
  description = "URL of runbook that explains how to resolve the issue"
  default     = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
}

variable "newrelic_lambda_nrql_alert_evaluation_offset" {
  description = "Offset of evaluation action in minutes"
  default     = 20
}

variable "newrelic_lambda_nrql_alert_operator" {
  description = "Condition operator for the alert"
  default     = "above"
}

variable "newrelic_lambda_nrql_alert_error_count_threshold" {
  description = "Condition violation threshold for the alert"
  default     = 1
}

variable "newrelic_lambda_nrql_alert_error_count_threshold_duration" {
  description = "Duration of time in seconds before an alert violation is created"
  default     = 300
}

variable "newrelic_lambda_nrql_alert_error_count_threshold_occurrences" {
  description = "Number of data points that must be in violation for the specified threshold duration in order to create an alert"
  default     = "all"
}

variable "newrelic_lambda_nrql_warning_operator" {
  description = "Condition operator for the warning"
  default     = "above"
}

variable "newrelic_lambda_nrql_warning_error_count_threshold" {
  description = "Condition violation threshold for the warning"
  default     = 0
}

variable "newrelic_lambda_nrql_warning_error_count_threshold_duration" {
  description = "Duration of time in seconds before a warning violation is created"
  default     = 600
}

variable "newrelic_lambda_nrql_warning_error_count_threshold_occurrences" {
  description = "Number of data points that must be in violation for the specified threshold duration in order to create a warning"
  default     = "all"
}

variable "layers" {
  description = "Layers for Lambda function"
  type        = list(string)
  default     = []
}

variable "environment_variables" {
  description = "Environment variables passed on to Lambda"
  type        = map(string)
  default     = null
}
