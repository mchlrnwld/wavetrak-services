# Create the Lambda function
resource "aws_lambda_function" "lambda_function" {
  function_name                  = "${var.company}-${var.application}-${var.function_name}-${var.environment}"
  runtime                        = var.runtime
  s3_bucket                      = var.artifacts_bucket
  s3_key                         = "lambda-functions/${var.application}-${var.function_name}/${var.company}-${var.application}-${var.function_name}.zip"
  handler                        = var.service_handler
  memory_size                    = var.memory_size
  timeout                        = var.timeout
  role                           = aws_iam_role.lambda_role.arn
  reserved_concurrent_executions = var.concurrent_executions

  vpc_config {
    subnet_ids = var.instance_subnets
    security_group_ids = [
      var.sg_all_servers,
      var.internal_sg_group
    ]
  }

  tracing_config {
    mode = var.tracing_config_mode
  }

  dead_letter_config {
    target_arn = module.sqs_queue.queue_arn
  }

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Service     = "lambda"
    Terraform   = true
  }

  layers = var.layers

  dynamic "environment" {
    for_each = var.environment_variables != null ? [var.environment_variables] : []
    content {
      variables = var.environment_variables
    }
  }
}

# Create base IAM role for Lambda function
resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-${var.application}-${var.function_name}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/lambda-policy.json")
}

# Allow Lambda to access resources in VPC
resource "aws_iam_role_policy_attachment" "lambda_execution_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

# Allow Lambda to write to Xray
resource "aws_iam_role_policy_attachment" "lambda_xray_write_access_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
}

# Create a dead letter queue for this Lambda function
module "sqs_queue" {
  source = "../sqs"

  environment = var.environment
  queue_name  = "${var.company}-${var.application}-${var.function_name}-sqs-dlq"
}

resource "aws_iam_role_policy_attachment" "lambda_sqs_dlq_write_access_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = module.sqs_queue.queue_policy
}

# Access to SSM and KMS keys.
data "aws_kms_key" "wt_common_kms_key" {
  key_id = "alias/${var.environment}/common"
}

resource "aws_iam_policy" "wt_common_kms_key" {
  name        = "wt-common-kms-key-${var.function_name}-${var.environment}"
  description = "Policy for access to KMS key that encrypts commonly shared service/lambda secrets."
  policy = templatefile("${path.module}/resources/ssm-kms-policy.json", {
    resource    = "${var.environment}/common"
    kms_key_arn = data.aws_kms_key.wt_common_kms_key.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_wt_common_kms_key" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.wt_common_kms_key.arn
}

resource "aws_kms_key" "wt_function_kms_key" {
  description             = var.key_description
  key_usage               = var.key_usage
  is_enabled              = var.key_is_enabled
  enable_key_rotation     = var.key_enable_rotation
  deletion_window_in_days = var.key_deletion_window_in_days

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Function    = var.function_name
    Service     = "lambda"
    Terraform   = true
  }
}

resource "aws_iam_policy" "wt_function_kms_key" {
  name        = "wt-function-kms-key-${var.application}-${var.function_name}-${var.environment}"
  description = "Policy for access to KMS key that encrypts lambda secrets."
  policy = templatefile("${path.module}/resources/ssm-kms-policy.json", {
    resource    = "${var.environment}/functions/${var.application}-${var.function_name}"
    kms_key_arn = aws_kms_key.wt_function_kms_key.arn
  })
}

resource "aws_kms_alias" "wt_function_kms_key_alias" {
  name          = "alias/${var.environment}/functions/${var.application}-${var.function_name}"
  target_key_id = aws_kms_key.wt_function_kms_key.key_id
}

resource "aws_iam_role_policy_attachment" "attach_wt_function_kms_key" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.wt_function_kms_key.arn
}
