variable "company" {
}

variable "company_full_name" {
  default = "surfline"
}

variable "environment" {
}

variable "application" {
}

variable "service" {
}

variable "region" {
}

variable "desired_instance_count" {
}

variable "instance_subnets" {
  type = list(string)
}

variable "s3_bucket_name" {
}

variable "chef_bucket_name" {
}

variable "chef_version" {
}

variable "image_id" {
}

variable "instance_type" {
}

variable "key_name" {
}

variable "default_vpc" {
}

variable "ftp_zone" {
}

variable "ftp_domain" {
}

variable "ftp_internal_domain" {
}
