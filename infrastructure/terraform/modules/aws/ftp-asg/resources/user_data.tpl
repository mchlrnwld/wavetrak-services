#cloud-config
# repo_releasever: 2016.09
# repo_update: true
# repo_upgrade: all
# packages:
#   - aws-cli
#   - python
#   - python-pip
#   - python-boto
#   - aws-cfn-bootstrap

write_files:
  - path: /etc/chef/sync_cookbooks.sh
    permissions: '0755'
    owner: 'root'
    group: 'root'
    content: |
      #!/bin/bash
      aws --region ${region} s3 sync s3://${chef_bucket_name}/${environment}/${application}/config /etc/chef/
      if compgen -G "/etc/chef/cookbooks-*.tar.gz" > /dev/null; then
          echo "Cookbook archive found."
          if [ -d "/etc/chef/cookbooks" ]; then
              echo "Removing previously extracted cookbooks."
              rm -r /etc/chef/cookbooks
          fi
          echo "Extracting highest numbered cookbook archive."
          cbarchives=(/etc/chef/cookbooks-*.tar.gz)
          tar -zxf "$${cbarchives[@]: -1}" -C /etc/chef
          chown -R root:root /etc/chef
      fi

  - path: /etc/chef/perform_chef_run.sh
    permissions: '0755'
    owner: 'root'
    group: 'root'
    content: |
      #!/bin/bash
      chef-client -z -r '${chef_runlist}' -c /etc/chef/client.rb -E ${environment}

  - path: /etc/chef/client.rb
    permissions: '0644'
    owner: 'root'
    group: 'root'
    content: |
      log_level :info
      log_location '/var/log/chef/client.log'
      ssl_verify_mode :verify_none
      cookbook_path '/etc/chef/cookbooks'
      node_path '/etc/chef/nodes'
      role_path '/etc/chef/roles'
      data_bag_path '/etc/chef/data_bags'
      environment_path '/etc/chef/environments'
      local_mode 'true'

  - path: /etc/chef/environments/${environment}.json
    permissions: '0644'
    owner: 'root'
    group: 'root'
    content: |
      {
        "name": "${environment}",
        "default_attributes": {
          "application": {
            "bucketname": "${app_bucket}"
          },
          "s3_presigned_url": {
            "bucket": "${chef_bucket_name}",
            "region": "${region}"
          },
          "s3ftpd": {
            "path": "${code_path}"
          },
          "route53": {
            "zone_id": "${zone_id}",
            "name": "${internal_domain}"
          }
        },
        "json_class": "Chef::Environment",
        "description": "${environment} environment",
        "chef_type": "environment"
      }


runcmd:
  - echo 'Installing Chef'
  - curl --max-time 10 --retry-delay 5 --retry 5 -L https://www.chef.io/chef/install.sh | bash -s -- -v ${chef_version}
  - echo 'Configuring Chef'
  - mkdir -p /var/log/chef /etc/chef/data_bags /etc/chef/nodes /etc/chef/roles
  - chmod 0755 /etc/chef
  - /etc/chef/sync_cookbooks.sh
  - /etc/chef/perform_chef_run.sh
