data "aws_caller_identity" "current" {
}

data "aws_region" "current" {
}

resource "aws_security_group" "asg" {
  name        = "${var.company}-sg-asg-${var.application}-${var.environment}"
  description = "Allows ftp traffic"
  vpc_id      = var.default_vpc

  ingress {
    from_port   = 21
    to_port     = 21
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 4242
    to_port     = 4257
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Company     = var.company
    Service     = var.service
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_eip" "ftp_ip" {
  vpc = true
}

data "aws_route53_zone" "surfline-com" {
  name = "${var.ftp_zone}."
}

resource "aws_route53_record" "highwinds_record" {
  zone_id = data.aws_route53_zone.surfline-com.zone_id
  name    = var.ftp_domain
  type    = "A"
  ttl     = "60"
  records = [aws_eip.ftp_ip.public_ip]
}

resource "aws_route53_zone" "internal_zone" {
  name = var.ftp_internal_domain
}

resource "aws_route53_record" "internal_ns_record" {
  zone_id = data.aws_route53_zone.surfline-com.zone_id
  name    = var.ftp_internal_domain
  type    = "NS"
  ttl     = "60"
  records = aws_route53_zone.internal_zone.name_servers
}

resource "aws_autoscaling_group" "as_group" {
  name                 = "${var.company}-${var.application}-asg-${var.environment}"
  min_size             = var.desired_instance_count
  max_size             = 6
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.asg_launch_config.name
  vpc_zone_identifier  = var.instance_subnets

  tag {
    key = "Name" # names the launched instances

    value               = "${var.company}-asg-${var.application}-${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Company"
    value               = var.company
    propagate_at_launch = true
  }

  tag {
    key                 = "Service"
    value               = var.service
    propagate_at_launch = true
  }

  tag {
    key                 = "Application"
    value               = var.application
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = true
    propagate_at_launch = true
  }

  tag {
    key                 = "eip_address"
    value               = aws_eip.ftp_ip.public_ip
    propagate_at_launch = true
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/resources/user_data.tpl")
  vars = {
    application      = var.application
    environment      = var.environment
    app_bucket       = var.s3_bucket_name
    region           = var.region
    chef_version     = var.chef_version
    chef_bucket_name = var.chef_bucket_name
    chef_runlist     = "surfline-highwinds-ftp"
    code_path        = "${var.environment}/${var.application}/s3ftpd"
    zone_id          = aws_route53_zone.internal_zone.zone_id
    internal_domain  = var.ftp_internal_domain
  }
}

resource "aws_launch_configuration" "asg_launch_config" {
  image_id                    = var.image_id
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.asg.id]
  iam_instance_profile        = aws_iam_instance_profile.asg_instance_profile.name
  key_name                    = var.key_name
  associate_public_ip_address = true
  user_data                   = data.template_file.user_data.rendered
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_role" "asg_role" {
  name               = "${var.company}-${var.application}-asg-role-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ec2-policy.json")
}

resource "aws_iam_role_policy" "s3-access" {
  name = "${var.company}-${var.application}-s3-access-${var.environment}"
  role = aws_iam_role.asg_role.name
  policy = templatefile("${path.module}/resources/ec2-policy.json", {
    s3_bucket_name    = var.s3_bucket_name
    chef_bucket_name  = var.chef_bucket_name
    region_name       = data.aws_region.current.name
    caller_account_id = data.aws_caller_identity.current.account_id
    application       = var.application
    environment       = var.environment
    company_full_name = var.company_full_name
    route53_zone_id   = aws_route53_zone.internal_zone.zone_id
  })
}

resource "aws_iam_instance_profile" "asg_instance_profile" {
  name = "${var.company}-${var.application}-asg-instanceprofile-${var.environment}"
  role = aws_iam_role.asg_role.name
}
