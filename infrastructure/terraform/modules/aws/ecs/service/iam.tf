data "aws_kms_key" "wt_common_kms_key" {
  key_id = "alias/${var.environment}/common"
}

resource "aws_iam_role" "service_task_role" {
  name               = "${replace(var.service_name, "-", "_")}_task_role_${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-policy.json")
}

resource "aws_iam_policy" "wt_common_kms_key" {
  name        = "wt-common-kms-key-${var.service_name}-${var.environment}"
  description = "Policy for access to KMS key that encrypts commonly shared service/lambda secrets."
  policy = templatefile("${path.module}/resources/ssm-kms-policy.json", {
    resource    = "${var.environment}/common"
    kms_key_arn = data.aws_kms_key.wt_common_kms_key.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_wt_common_kms_key" {
  role       = aws_iam_role.service_task_role.name
  policy_arn = aws_iam_policy.wt_common_kms_key.arn
}

resource "aws_iam_policy" "wt_service_kms_key" {
  name        = "wt-service-kms-key-${var.service_name}-${var.environment}"
  description = "Policy for access to KMS key that encrypts service/lambda secrets."
  policy = templatefile("${path.module}/resources/ssm-kms-policy.json", {
    resource    = "${var.environment}/services/${var.service_name}"
    kms_key_arn = aws_kms_key.wt_service_kms_key.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_wt_service_kms_key" {
  role       = aws_iam_role.service_task_role.name
  policy_arn = aws_iam_policy.wt_service_kms_key.arn
}

resource "aws_iam_role_policy_attachment" "attach_amazon_ecs_task_execution_role_policy" {
  role       = aws_iam_role.service_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
