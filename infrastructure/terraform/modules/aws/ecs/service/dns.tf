resource "aws_route53_record" "service_task_dns_record" {
  count = var.is_http_service && var.provision_route_53 ? 1 : 0

  zone_id = var.dns_zone_id
  name    = var.dns_name
  type    = "A"

  alias {
    name                   = data.aws_alb.service_alb.dns_name
    zone_id                = data.aws_alb.service_alb.zone_id
    evaluate_target_health = false
  }
}
