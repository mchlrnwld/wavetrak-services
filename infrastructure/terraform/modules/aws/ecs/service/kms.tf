resource "aws_kms_key" "wt_service_kms_key" {
  description             = var.description
  key_usage               = var.key_usage
  is_enabled              = var.is_enabled
  enable_key_rotation     = var.enable_key_rotation
  deletion_window_in_days = var.deletion_window_in_days

  tags = {
    Name        = "${var.company}-${var.application}-${var.environment}"
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Terraform   = true
    Source      = "Surfline/wavetrak-infrastructure/terraform/modules/aws/ecs/service"
  }
}

resource "aws_kms_alias" "wt_service_kms_key_alias" {
  name          = "alias/${var.environment}/services/${var.service_name}"
  target_key_id = aws_kms_key.wt_service_kms_key.key_id
}
