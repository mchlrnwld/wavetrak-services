# Common
variable "application" {
}

variable "company" {
}

variable "default_vpc" {
}

variable "environment" {
}

variable "service_name" {
}

# DNS
variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

# Feature flag for HTTP/HTTPS services.
variable "is_http_service" {
  type    = bool
  default = true
}

# Load Balancer
variable "alb_listener" {
}

variable "service_alb_priority" {
}

variable "service_lb_healthcheck_path" {
}

variable "service_lb_healthcheck_matcher" {
  default = 200
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "task_deregistration_delay" {
  default = 60
}

# Allow route 53 dsn.tf bypass
variable "provision_route_53" {
  default = true
}

# KMS
variable "description" {
  description = "Description of the resource's use"
  default     = "Service-specific KMS key."
}

variable "key_usage" {
  description = "Intended key operations"
  default     = "ENCRYPT_DECRYPT"
}

variable "is_enabled" {
  description = "Specifies whether key is enabled"
  default     = true
}

variable "enable_key_rotation" {
  description = "Specifies whether key rotation is enabled"
  default     = false
}

variable "deletion_window_in_days" {
  description = "Number of days a secret is retained after being marked for deletion"
  default     = 30
}

# Service
variable "ecs_cluster" {
}

variable "iam_role_arn" {
}

variable "propagate_tags" {
  description = "Specifies whether service should propagate tags to its tasks"
  default     = false
}

variable "service_port" {
}

variable "service_td_container_name" {
}

variable "service_td_count" {
}

variable "service_td_name" {
}

variable "service_placement_strategy" {
  type    = list(map(string))
  default = []
}

variable "health_check_grace_period_seconds" {
  default = 0
}

# Autoscaling.
variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_min_size" {
  default = 2
}

variable "auto_scaling_max_size" {
  default = 5
}

variable "auto_scaling_scale_by" {
  default     = "memory_utilization"
  description = "Metric to scale by (cpu_utilization|memory_utilization|alb_response_latency|schedule|alb_request_count|sqs_queue_size)"
}

# Used to override defaults
variable "auto_scaling_up_metric" {
  default = -1
}

variable "auto_scaling_down_metric" {
  default = -1
}

variable "auto_scaling_target_value" {
  default = -1
}

#Change number of tasks to add/remove
variable "auto_scaling_up_count" {
  default = 1
}

variable "auto_scaling_down_count" {
  default = -1
}

#Used to override scaling defaults
variable "auto_scaling_cooldown" {
  default = 60
}

variable "auto_scaling_evaluation_period" {
  default = 120
}

#required for use with ALB autoscaling
variable "auto_scaling_alb_arn_suffix" {
  default = ""
}

#required for use with scheduled scaling
variable "auto_scaling_schedule_scale_up" {
  description = "schedule in the format cron() or at()"
  default     = ""
}

variable "auto_scaling_schedule_scale_down" {
  description = "schedule in the format cron() or at() "
  default     = ""
}

variable "auto_scaling_queue_name" {
  description = "Used when the auto-scaling scale data is 'sqs_queue_size'"
  default     = ""
}

# When using "sqs_queue_size", the "target_value" will be:
# The average messages count / The seconds that takes to process a message.
variable "scale_data" {
  type = map(string)
  default = {
    "cpu_utilization.namespace"        = "ECS"
    "cpu_utilization.metric"           = "CPUUtilization"
    "cpu_utilization.down_metric"      = 2
    "cpu_utilization.up_metric"        = 70
    "memory_utilization.namespace"     = "ECS"
    "memory_utilization.metric"        = "MemoryUtilization"
    "memory_utilization.down_metric"   = 10
    "memory_utilization.up_metric"     = 60
    "alb_response_latency.namespace"   = "ApplicationELB"
    "alb_response_latency.metric"      = "TargetResponseTime"
    "alb_response_latency.down_metric" = 0.01
    "alb_response_latency.up_metric"   = 0.75
    "schedule.namespace"               = ""
    "schedule.metric"                  = ""
    "schedule.up_metric"               = ""
    "schedule.down_metric"             = ""
    "alb_request_count.namespace"      = ""
    "alb_request_count.target_value"   = ""
    "sqs_queue_size.namespace"         = "AWS/SQS"
    "sqs_queue_size.metric"            = "ApproximateNumberOfMessagesVisible"
    "sqs_queue_size.target_value"      = 50000
  }
}

variable "target_group_name_override" {
  description = "Optionally overrides service_name in constructing the alb_target_group name"
  default     = ""
}

# New Relic shared alert variables

variable "newrelic_app_name" {
  description = "Name of the APM service being monitored in New Relic"
  default     = ""
}

variable "newrelic_policy_name" {
  description = "Name of the New Relic alerting policy"
  default     = ""
}

# New Relic apdex alert variables

variable "newrelic_apdex_create_alert" {
  description = "Create New Relic apdex alert"
  default     = false
}

variable "newrelic_apdex_alert_name" {
  description = "Name of the New Relic alert to be created"
  default     = "Apdex (Low)"
}

variable "newrelic_apdex_alert_enabled" {
  description = "Enable New Relic apdex alert"
  default     = true
}

variable "newrelic_apdex_alert_type" {
  description = "Type of the alert (apm_app_metric, apm_jvm_metric, etc)"
  default     = "apm_app_metric"
}

variable "newrelic_apdex_alert_scope" {
  description = "Scope of analysis (application vs instance)"
  default     = "application"
}

variable "newrelic_apdex_alert_metric" {
  description = "New Relic metric type (apdex, error_percentage, etc)"
  default     = "apdex"
}

variable "newrelic_apdex_violation_close_timer" {
  description = "Number of hours before violation is automatically closed"
  default     = null
}

variable "newrelic_apdex_runbook_url" {
  description = "URL of runbook that explains how to resolve the issue"
  default     = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
}

variable "newrelic_apdex_alert_priority" {
  description = "Critical vs warning alert type"
  default     = "critical"
}

variable "newrelic_apdex_alert_threshold_duration" {
  description = "Duration of time in minutes before a violation is created"
  default     = 10
}

variable "newrelic_apdex_alert_operator" {
  description = "Alert condition operator (above, below, or equal)"
  default     = "below"
}

variable "newrelic_apdex_alert_threshold" {
  description = "Condition violation threshold for the alert"
  default     = ".75"
}

variable "newrelic_apdex_alert_time_function" {
  description = "Time function (possible values include all or any)"
  default     = "all"
}

variable "newrelic_apdex_warning_priority" {
  description = "Critical vs warning alert type"
  default     = "warning"
}

variable "newrelic_apdex_warning_threshold_duration" {
  description = "Duration of time in minutes before a warning violation is created"
  default     = 5
}

variable "newrelic_apdex_warning_operator" {
  description = "Warning condition operator (above, below, or equal) for the warning"
  default     = "below"
}

variable "newrelic_apdex_warning_threshold" {
  description = "Condition violation threshold for the warning"
  default     = ".85"
}

variable "newrelic_apdex_warning_time_function" {
  description = "Time function for the warning (possible values include all or any)"
  default     = "all"
}

# New Relic error rate alert variables

variable "newrelic_error_rate_create_alert" {
  description = "Create New Relic error rate alert"
  default     = false
}

variable "newrelic_error_rate_alert_name" {
  description = "Name of the New Relic alert to be created"
  default     = "Error percentage (High)"
}

variable "newrelic_error_rate_alert_enabled" {
  description = "Enable New Relic apdex alert"
  default     = true
}

variable "newrelic_error_rate_alert_type" {
  description = "Type of the alert (apm_app_metric, apm_jvm_metric, etc)"
  default     = "apm_app_metric"
}

variable "newrelic_error_rate_alert_scope" {
  description = "Scope of analysis (application vs instance)"
  default     = "application"
}

variable "newrelic_error_rate_alert_metric" {
  description = "New Relic metric type (apdex, error_percentage, etc)"
  default     = "error_percentage"
}

variable "newrelic_error_rate_violation_close_timer" {
  description = "Number of hours before violation is automatically closed"
  default     = null
}

variable "newrelic_error_rate_runbook_url" {
  description = "URL of runbook that explains how to resolve the issue"
  default     = "https://wavetrak.atlassian.net/wiki/spaces/IR/pages/1718255713/Runbooks"
}

variable "newrelic_error_rate_alert_priority" {
  description = "Critical vs warning alert type"
  default     = "critical"
}

variable "newrelic_error_rate_alert_threshold_duration" {
  description = "Duration of time in minutes before an alert violation is created"
  default     = 10
}

variable "newrelic_error_rate_alert_operator" {
  description = "Alert condition operator (above, below, or equal)"
  default     = "above"
}

variable "newrelic_error_rate_alert_threshold" {
  description = "Condition violation threshold for the alert (as a percentage)"
  default     = "0.05"
}

variable "newrelic_error_rate_alert_time_function" {
  description = "Time function for the alert (possible values include all or any)"
  default     = "all"
}

variable "newrelic_error_rate_warning_priority" {
  description = "Critical vs warning alert type"
  default     = "warning"
}

variable "newrelic_error_rate_warning_threshold_duration" {
  description = "Duration of time in minutes before a warning violation is created"
  default     = 5
}

variable "newrelic_error_rate_warning_operator" {
  description = "Warning condition operator (above, below, or equal) for the warning"
  default     = "above"
}

variable "newrelic_error_rate_warning_threshold" {
  description = "Condition violation threshold for the warning (as a percentage)"
  default     = "0.03"
}

variable "newrelic_error_rate_warning_time_function" {
  description = "Time function for the warning (possible values include all or any)"
  default     = "all"
}
