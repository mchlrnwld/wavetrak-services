terraform {
  required_version = ">= 0.12"
  required_providers {
    aws      = ">= 2.70"
    newrelic = "~> 2.10"
    random   = "~> 3.0"
  }
}
