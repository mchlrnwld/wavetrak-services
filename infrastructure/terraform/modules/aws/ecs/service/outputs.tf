output "name" {
  value = var.service_name
}

output "arn_suffix" {
  value = var.is_http_service ? aws_alb_target_group.service_target_group[0].arn_suffix : ""
}

output "target_group" {
  value = var.is_http_service ? aws_alb_target_group.service_target_group[0].id : ""
}

output "task_role_arn" {
  value = aws_iam_role.service_task_role.arn
}

output "task_role" {
  value = aws_iam_role.service_task_role.id
}

output "task_role_name" {
  value = aws_iam_role.service_task_role.name
}
