data "aws_ecs_task_definition" "service_latest_td" {
  task_definition = var.service_td_name
}

resource "aws_ecs_service" "service_ecs_service" {
  name                              = "${var.company}-${var.service_name}-${var.environment}"
  cluster                           = var.ecs_cluster
  task_definition                   = "${data.aws_ecs_task_definition.service_latest_td.family}:${data.aws_ecs_task_definition.service_latest_td.revision}"
  iam_role                          = var.is_http_service ? var.iam_role_arn : ""
  desired_count                     = var.service_td_count
  propagate_tags                    = var.propagate_tags ? "TASK_DEFINITION" : null
  health_check_grace_period_seconds = var.health_check_grace_period_seconds

  dynamic "load_balancer" {
    for_each = aws_alb_target_group.service_target_group
    content {
      target_group_arn = load_balancer.value.arn
      container_name   = var.service_td_container_name
      container_port   = var.service_port
    }
  }

  dynamic "ordered_placement_strategy" {
    for_each = var.service_placement_strategy
    content {
      # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
      # which keys might be set in maps assigned here, so it has
      # produced a comprehensive set here. Consider simplifying
      # this after confirming which keys can be set in practice.

      field = lookup(ordered_placement_strategy.value, "field", null)
      type  = ordered_placement_strategy.value.type
    }
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  depends_on = [
    aws_alb_target_group.service_target_group[0]
  ]
}
