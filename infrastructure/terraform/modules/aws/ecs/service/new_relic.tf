data "newrelic_alert_policy" "policy" {
  count = var.newrelic_apdex_create_alert ? 1 : 0
  name  = var.newrelic_policy_name
}

data "newrelic_entity" "apm_service_name" {
  count  = var.newrelic_apdex_create_alert ? 1 : 0
  name   = var.newrelic_app_name
  type   = "APPLICATION"
  domain = "APM"
}

resource "newrelic_alert_condition" "service_apdex_alert" {
  count                 = var.newrelic_apdex_create_alert ? 1 : 0
  name                  = var.newrelic_apdex_alert_name
  enabled               = var.newrelic_apdex_alert_enabled
  entities              = [data.newrelic_entity.apm_service_name[0].application_id]
  policy_id             = data.newrelic_alert_policy.policy[0].id
  type                  = var.newrelic_apdex_alert_type
  condition_scope       = var.newrelic_apdex_alert_scope
  metric                = var.newrelic_apdex_alert_metric
  violation_close_timer = var.newrelic_apdex_violation_close_timer
  runbook_url           = var.newrelic_apdex_runbook_url

  term {
    priority      = var.newrelic_apdex_alert_priority
    duration      = var.newrelic_apdex_alert_threshold_duration
    operator      = var.newrelic_apdex_alert_operator
    threshold     = var.newrelic_apdex_alert_threshold
    time_function = var.newrelic_apdex_alert_time_function
  }

  term {
    priority      = var.newrelic_apdex_warning_priority
    duration      = var.newrelic_apdex_warning_threshold_duration
    operator      = var.newrelic_apdex_warning_operator
    threshold     = var.newrelic_apdex_warning_threshold
    time_function = var.newrelic_apdex_warning_time_function
  }
}

resource "newrelic_alert_condition" "service_error_rate_alert" {
  count                 = var.newrelic_error_rate_create_alert ? 1 : 0
  name                  = var.newrelic_error_rate_alert_name
  enabled               = var.newrelic_error_rate_alert_enabled
  entities              = [data.newrelic_entity.apm_service_name[0].application_id]
  policy_id             = data.newrelic_alert_policy.policy[0].id
  type                  = var.newrelic_error_rate_alert_type
  condition_scope       = var.newrelic_error_rate_alert_scope
  metric                = var.newrelic_error_rate_alert_metric
  violation_close_timer = var.newrelic_error_rate_violation_close_timer
  runbook_url           = var.newrelic_error_rate_runbook_url

  term {
    priority      = var.newrelic_error_rate_alert_priority
    duration      = var.newrelic_error_rate_alert_threshold_duration
    operator      = var.newrelic_error_rate_alert_operator
    threshold     = var.newrelic_error_rate_alert_threshold * 100
    time_function = var.newrelic_error_rate_alert_time_function
  }

  term {
    priority      = var.newrelic_error_rate_warning_priority
    duration      = var.newrelic_error_rate_warning_threshold_duration
    operator      = var.newrelic_error_rate_warning_operator
    threshold     = var.newrelic_error_rate_warning_threshold * 100
    time_function = var.newrelic_error_rate_warning_time_function
  }
}
