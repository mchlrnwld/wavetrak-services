# Terraform ECS Service Module

This module is used to stand up new Surfline microservices in ECS.

## How to use this module

This module should be called from within a Terraform project in the `infra` directory of a Surfline microservice.

It should be included using its Github URL:

```hcl
source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"
```

This module assumes that the following resources have already been created:

- ECS Cluster
- Application Load Balancer (ALB)
- ALB Listener
- Service IAM Role

A good reference implementation is the Surfline [geotarget service](https://github.com/Surfline/surfline-services/tree/master/services/geo-target-service).

## Inputs

Property | Description | Example
-------- | ----------- | -------
`application` | Application name | `geo-target-service`
`company` | Company abbreviation | `sl`
`default_vpc` | VPC ID for AWS account | `vpc-981887fd`
`environment` | Environment | `sandbox`
`service_name` | Service name | `geo-target-service`
`dns_name` | Service FQDN | `geo-target-service.sandbox.surfline.com`
`dns_zone_id` | Zone ID of service FQDN | `Z3DM6R3JR1RYXV`
`load_balancer_arn` | ARN of ALB that the service is associated to | `arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b`
`alb_listener` | ARN of ALB listener to associate service to | `arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/2ac05abce55d3ec5`
`service_alb_priority` | Priority of ALB listener rules | `1000`
`service_lb_healthcheck_path` | service healthcheck path | `/health`
`service_lb_healthcheck_matcher` | service healthcheck matcher | `200`
`service_lb_rules` | List of ALB listener rules | `[{field = "path-pattern" value = "*"}]`
`task_deregistration_delay` | delay when deregistering tasks | `10`
`ecs_cluster` | ECS cluster to associate service to | `sl-core-svc-sandbox`
`iam_role_arn` | ARN of service IAM role | `arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox`
`service_port` | Port that the internal service listens on | `8080`
`service_td_container_name` | Service task definition container name | `geo-target-service`
`service_td_count` | Service task definition count | `1`
`service_td_name` | Service task definition name | `sl-core-svc-sandbox-geo-target-service`
`is_http_service` | Tell if it's an HTTP/HTTPS service or a worker without the need of an ALB Listener | `true`

### Autoscaling Inputs

Property | Autoscale Mode | Description | Example
-------- | -------------- | ----------- | -------
`auto_scaling_enabled` | All | Enable autoscaling on the service | `true`
`auto_scaling_scale_by`| All | Scaling mode | `memory_utilization`
`auto_scaling_min_size`| All | Minimum size of the service | `2`
`auto_scaling_max_size`| All | Maximum size of the service | `6`
`auto_scaling_target_value`| `alb_request_count`, `sqs_queue_size` | A limit value for step scaling or final count | `100`
`variable auto_scaling_up_count`| All | Containers to add at a time | `1`
`variable auto_scaling_down_count`| All | containers to remove at a time | `-1`
`auto_scaling_up_metric` | (optional) All Except `schedule` | override high threshold value of memory, cpu, or latency | `50`
`auto_scaling_down_metric` | (optional) All Except `schedule` | override default low threshold value of memory, cpu, or latency | `5`
`auto_scaling_schedule_scale_up` | `schedule` | Scale up schedule | `cron(0 5 * * ? *)`
`auto_scaling_schedule_scale_down` |`schedule` | Scale down schedule | `cron(0 15 * * ? *)`
`auto_scaling_alb_arn_suffix` | `alb_response_latency` | ALB ARN Suffix of the service's ALB | `app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57`
`variable auto_scaling_cooldown` | (optional) All  | Override the default cooldown value | `120`
`variable auto_scaling_evaluation_period` | (optional) All | Override the default evaluation period | `120`

For `auto_scaling_scale_by`, valid modes are:

- `cpu_utilization`
- `memory_utilization`
- `alb_response_latency`
- `alb_request_count`
- `schedule` and
- `sqs_queue_size`

### Example

```hcl
locals {
  service_lb_rules = [
    {
      field = "path-pattern"
      value = "*"
    },
  ]
}

module "geo_target_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company     = "sl"
  application = "geo-target-service"
  environment = "sandbox"

  default_vpc = "vpc-981887fd"
  dns_name    = "geo-target-service.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"

  ecs_cluster  = "sl-core-svc-sandbox"
  service_name = "geo-target-service"
  service_port = 8080

  service_lb_rules               = "${local.service_lb_rules}"
  service_lb_healthcheck_path    = "/health"
  service_lb_healthcheck_matcher = "200,401"
  service_alb_priority           = 400

  service_td_name           = "sl-core-svc-sandbox-geo-target-service"
  service_td_container_name = "geo-target-service"
  service_td_count          = 1

  is_http_service = true

  alb_listener                   = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/2ac05abce55d3ec5"
  iam_role_arn                   = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn              = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b"

  auto_scaling_enabled        = true
  auto_scaling_scale_by       = "alb_response_latency"
  auto_scaling_min_size       = 1
  auto_scaling_max_size       = 5
  auto_scaling_up_metric      = 0.5
  auto_scaling_down_metric    = 0.1
  auto_scaling_alb_arn_suffix = "app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"
}
```

### Using SQS queue size autoscaling

By adding the next example parameters to any of our services, we can make use of the SQS autoscaling:

```hcl
auto_scaling_enabled      = true
auto_scaling_scale_by     = "sqs_queue_size"
auto_scaling_min_size     = 1
auto_scaling_max_size     = 5
auto_scaling_target_value = 50000
```

The value for `auto_scaling_target_value` **IS NOT** the messages count allowed per task, it should be calculated between the average amount of messages being received and the time each of the messages takes to process it. This depends on the capacity of the server instance, so according to the process, a GPU instance should be a better fit than a CPU-optimized processing instance and, of course, the other way around:

```shellscript
average_messages_count / average_time_to_process_message = 5.000 messages / 0.1 sec = 50.000 target value
```

For this, the autoscaling policy will create 9 scaling steps from 0 to 400.000+:

```shellscript
(autoscaling_max_size - 1) * auto_scaling_target_value = (10 - 1) x 50.000
```

## Outputs

Property | Description
-------- | -----------
`arn_suffix` | The ARN suffix for use with CloudWatch Metrics
`name` | The service name
`target_group` | The ARN of the target group
`task_role_arn` | The Amazon Resource Name (ARN) specifying the role. Associate `aws_iam_role_policy` to this role to grant access to AWS resources from the tasks.

## Resources

This is a list of the resources that the module will create:

- `aws_alb_listener_rule`
- `aws_alb_target_group`
- `aws_ecs_service`
- `aws_iam_role`
- `aws_route53_record`

## Creating a non HTTP/HTTPS service

If a service doesn't need the use of an HTTP/HTTPS Listener, you can change the feature flag that creates the service Target Group, Route53 record and ALB Listener Rules, by setting `is_http_service` to **FALSE** when you call the ECS Module in any of the environments, so several resources wouldn't need to be created:

```hcl
module "geo_target_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company     = var.company
  application = var.application
  environment = var.environment
  ...

  is_http_service = false
}
```
