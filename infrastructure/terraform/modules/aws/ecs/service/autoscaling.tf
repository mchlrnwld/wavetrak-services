locals {
  service_scaling_enabled = (
    var.is_http_service &&
    var.auto_scaling_enabled &&
    var.auto_scaling_scale_by != "schedule" &&
    var.auto_scaling_scale_by != "alb_request_count" &&
    var.auto_scaling_scale_by != "sqs_queue_size"
  )
}

data "aws_caller_identity" "current" {
}

resource "aws_appautoscaling_target" "ecs_target" {
  count = var.auto_scaling_enabled ? 1 : 0

  max_capacity       = var.auto_scaling_max_size
  min_capacity       = var.auto_scaling_min_size
  resource_id        = "service/${var.ecs_cluster}/${var.company}-${var.service_name}-${var.environment}"
  role_arn           = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/ecs.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_ECSService"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  depends_on = [
    aws_ecs_service.service_ecs_service
  ]
}

resource "aws_appautoscaling_policy" "ecs_up" {
  count = local.service_scaling_enabled ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-scaling-up"
  service_namespace  = "ecs"
  resource_id        = "service/${var.ecs_cluster}/${var.company}-${var.service_name}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.auto_scaling_cooldown
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = var.auto_scaling_up_count
    }
  }

  depends_on = [
    aws_appautoscaling_target.ecs_target
  ]
}

resource "aws_appautoscaling_policy" "ecs_down" {
  count = local.service_scaling_enabled ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-scaling-down"
  service_namespace  = "ecs"
  resource_id        = "service/${var.ecs_cluster}/${var.company}-${var.service_name}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 3 * var.auto_scaling_cooldown
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = var.auto_scaling_down_count
    }
  }

  depends_on = [
    aws_appautoscaling_target.ecs_target
  ]
}

# ECS metric Alarms
resource "aws_cloudwatch_metric_alarm" "ecs_service_alarm_up" {
  count = local.service_scaling_enabled && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ECS" ? 1 : 0

  alarm_name          = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ECS"
  period              = var.auto_scaling_evaluation_period
  statistic           = "Average"
  threshold           = tonumber(var.auto_scaling_up_metric) == -1 ? var.scale_data["${var.auto_scaling_scale_by}.up_metric"] : var.auto_scaling_up_metric

  dimensions = {
    ClusterName = basename(var.ecs_cluster)
    ServiceName = "${var.company}-${var.service_name}-${var.environment}"
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_up[0].arn,
  ]
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_alarm_down" {
  count = local.service_scaling_enabled && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ECS" ? 1 : 0

  alarm_name          = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ECS"
  period              = var.auto_scaling_evaluation_period
  statistic           = "Average"
  threshold           = tonumber(var.auto_scaling_down_metric) == -1 ? var.scale_data["${var.auto_scaling_scale_by}.down_metric"] : var.auto_scaling_down_metric

  dimensions = {
    ClusterName = basename(var.ecs_cluster)
    ServiceName = "${var.company}-${var.service_name}-${var.environment}"
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_down[0].arn,
  ]
}

# ALB Alarms
resource "aws_cloudwatch_metric_alarm" "alb_alarm_up" {
  count = local.service_scaling_enabled && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ApplicationELB" ? 1 : 0

  alarm_name          = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ApplicationELB"
  period              = var.auto_scaling_evaluation_period
  statistic           = "Average"
  threshold           = var.auto_scaling_up_metric == "-1" ? var.scale_data["${var.auto_scaling_scale_by}.up_metric"] : var.auto_scaling_up_metric

  dimensions = {
    TargetGroup  = aws_alb_target_group.service_target_group[0].arn_suffix
    LoadBalancer = var.auto_scaling_alb_arn_suffix
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_up[0].arn,
  ]
}

resource "aws_cloudwatch_metric_alarm" "alb_alarm_down" {
  count = local.service_scaling_enabled && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ApplicationELB" ? 1 : 0

  alarm_name          = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ApplicationELB"
  period              = var.auto_scaling_evaluation_period
  statistic           = "Average"
  threshold           = tonumber(var.auto_scaling_down_metric) == -1 ? var.scale_data["${var.auto_scaling_scale_by}.down_metric"] : var.auto_scaling_down_metric

  dimensions = {
    TargetGroup  = aws_alb_target_group.service_target_group[0].arn_suffix
    LoadBalancer = var.auto_scaling_alb_arn_suffix
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_down[0].arn,
  ]
}

resource "aws_appautoscaling_scheduled_action" "scheduled_scale_up" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "schedule" ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}-scheduled-scale-up"
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension
  schedule           = var.auto_scaling_schedule_scale_up

  scalable_target_action {
    min_capacity = var.auto_scaling_max_size
    max_capacity = var.auto_scaling_max_size
  }
}

resource "aws_appautoscaling_scheduled_action" "scheduled_scale_down" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "schedule" ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}-scheduled-scale-down"
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension
  schedule           = var.auto_scaling_schedule_scale_down

  scalable_target_action {
    min_capacity = var.auto_scaling_min_size
    max_capacity = var.auto_scaling_min_size
  }

  depends_on = [aws_appautoscaling_scheduled_action.scheduled_scale_up]
}

resource "aws_appautoscaling_policy" "ecs_policy" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "alb_request_count" ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}-request-count-per-target-scaling"
  policy_type        = "TargetTrackingScaling"
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension

  target_tracking_scaling_policy_configuration {
    target_value       = var.auto_scaling_target_value == "-1" ? var.scale_data["${var.auto_scaling_scale_by}.target_value"] : var.auto_scaling_target_value
    scale_in_cooldown  = 3 * var.auto_scaling_cooldown
    scale_out_cooldown = var.auto_scaling_cooldown

    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label         = "${var.auto_scaling_alb_arn_suffix}/${aws_alb_target_group.service_target_group[0].arn_suffix}"
    }
  }

  depends_on = [
    aws_appautoscaling_target.ecs_target
  ]
}

# SQS size autoscaling.
resource "aws_appautoscaling_policy" "sqs_queue_size_scale_up" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "sqs_queue_size" ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-sqs-scaling-up"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ExactCapacity"
    metric_aggregation_type = "Average"

    dynamic "step_adjustment" {
      for_each = range(1, var.auto_scaling_max_size)
      content {
        metric_interval_lower_bound = tonumber(var.auto_scaling_target_value * step_adjustment.key)
        metric_interval_upper_bound = tonumber(var.auto_scaling_target_value * (step_adjustment.key + 1))
        scaling_adjustment          = tonumber(step_adjustment.key + 1)
      }
    }

    step_adjustment {
      metric_interval_lower_bound = tonumber(var.auto_scaling_target_value * (var.auto_scaling_max_size - 1))
      scaling_adjustment          = var.auto_scaling_max_size
    }
  }
}

resource "aws_appautoscaling_policy" "sqs_queue_size_scale_down" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "sqs_queue_size" ? 1 : 0

  name               = "${var.company}-${var.service_name}-${var.environment}-sqs-scaling-down"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ExactCapacity"
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = tonumber(-var.auto_scaling_target_value * (var.auto_scaling_max_size - 1))
      scaling_adjustment          = var.auto_scaling_max_size
    }

    dynamic "step_adjustment" {
      for_each = range(1, var.auto_scaling_max_size)
      content {
        metric_interval_lower_bound = tonumber(-var.auto_scaling_target_value * (step_adjustment.key + 1))
        metric_interval_upper_bound = tonumber(var.auto_scaling_target_value * step_adjustment.key != 0 ? -var.auto_scaling_target_value * step_adjustment.key : 0)
        scaling_adjustment          = tonumber(step_adjustment.key + 1)
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_queue_size_alarm_up" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "sqs_queue_size" ? 1 : 0

  alarm_name          = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}_up"
  alarm_description   = "Alert when SQS queue size exceeds the scaling step threshold"
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = var.scale_data["${var.auto_scaling_scale_by}.namespace"]
  period              = var.auto_scaling_evaluation_period
  evaluation_periods  = var.auto_scaling_evaluation_period > 60 ? var.auto_scaling_evaluation_period / 60 : 1
  comparison_operator = "GreaterThanOrEqualToThreshold"
  statistic           = "Average"
  treat_missing_data  = "notBreaching"

  dimensions = {
    QueueName = var.auto_scaling_queue_name
  }

  alarm_actions = [
    aws_appautoscaling_policy.sqs_queue_size_scale_up[0].arn,
  ]
}

resource "aws_cloudwatch_metric_alarm" "sqs_queue_size_alarm_down" {
  count = var.auto_scaling_enabled && var.auto_scaling_scale_by == "sqs_queue_size" ? 1 : 0

  alarm_name          = "${var.company}-${var.service_name}-${var.environment}-${var.auto_scaling_scale_by}_down"
  alarm_description   = "Alert when SQS queue size is below the scaling step threshold"
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = var.scale_data["${var.auto_scaling_scale_by}.namespace"]
  period              = var.auto_scaling_evaluation_period
  evaluation_periods  = var.auto_scaling_evaluation_period > 60 ? var.auto_scaling_evaluation_period / 60 : 1
  comparison_operator = "LessThanOrEqualToThreshold"
  statistic           = "Average"
  treat_missing_data  = "notBreaching"

  dimensions = {
    QueueName = var.auto_scaling_queue_name
  }

  alarm_actions = [
    aws_appautoscaling_policy.sqs_queue_size_scale_down[0].arn,
  ]
}
