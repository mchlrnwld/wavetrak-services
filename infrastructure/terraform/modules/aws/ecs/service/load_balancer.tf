data "aws_alb" "service_alb" {
  arn = var.load_balancer_arn
}

# random_id is being used to prevent name collisions when recreating aws_alb_target_group due to unique name constraint.
# See here for more info:  https://github.com/hashicorp/terraform-provider-aws/issues/636#issuecomment-637761075
resource "random_id" "target_group_name" {
  count = var.is_http_service ? 1 : 0

  keepers = {
    service_port                   = var.service_port
    default_vpc                    = var.default_vpc
    task_deregistration_delay      = var.task_deregistration_delay
    service_lb_healthcheck_path    = var.service_lb_healthcheck_path
    service_lb_healthcheck_matcher = var.service_lb_healthcheck_matcher
  }

  byte_length = 4

  depends_on = [
    data.aws_alb.service_alb
  ]
}

resource "aws_alb_target_group" "service_target_group" {
  count = var.is_http_service ? 1 : 0

  name                 = trimsuffix(substr("${var.company}-${coalesce(var.target_group_name_override, var.service_name)}-${var.environment}-${random_id.target_group_name[0].hex}", 0, 32), "-")
  port                 = var.service_port
  protocol             = "HTTP"
  vpc_id               = var.default_vpc
  deregistration_delay = var.task_deregistration_delay

  health_check {
    path    = var.service_lb_healthcheck_path
    matcher = var.service_lb_healthcheck_matcher
  }

  tags = {
    Company     = var.company
    Service     = "ecs"
    Application = var.application
    Environment = var.environment
    Terraform   = true
    Source      = "Surfline/wavetrak-infrastructure/terraform/modules/aws/ecs/service"
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [name]
  }

  depends_on = [
    data.aws_alb.service_alb
  ]
}

# This accepts a list of conditions and creates a rule for each condition:
#
# service_lb_rules = [
#   {
#     field = "path-pattern"
#     value = "*"
#   },
#   {
#     field = "host-header"
#     value = "${local.dns_name}"
#   }
# ]
resource "aws_alb_listener_rule" "service_alb_listener" {
  count = var.is_http_service ? length(var.service_lb_rules) : 0

  listener_arn = var.alb_listener
  priority     = count.index + tonumber(var.service_alb_priority)

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.service_target_group[0].arn
  }

  dynamic "condition" {
    for_each = [for i in [var.service_lb_rules[count.index]] : i if i.field == "host-header"]
    content {
      host_header {
        values = [condition.value.value]
      }
    }
  }

  dynamic "condition" {
    for_each = [for i in [var.service_lb_rules[count.index]] : i if i.field == "path-pattern"]
    content {
      path_pattern {
        values = [condition.value.value]
      }
    }
  }

  depends_on = [
    aws_alb_target_group.service_target_group[0]
  ]
}
