output "lambda_function_arn" {
  value = module.lambda.lambda_function_arn
}

output "lambda_function_name" {
  value = module.lambda.lambda_function_name
}

output "lambda_role_arn" {
  value = module.lambda.lambda_role_arn
}

output "lambda_role_name" {
  value = module.lambda.lambda_role_name
}

output "queue_arn" {
  value = module.sqs_queue.queue_arn
}

output "queue_policy" {
  value = module.sqs_queue.queue_policy
}
