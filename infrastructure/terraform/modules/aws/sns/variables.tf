variable "environment" {
  description = "The environment that this SNS topic is being created in"
  default     = ""
}

variable "topic_name" {
  description = "The name of the SNS topic being created"
  default     = ""
}
