provider "aws" {
  region = var.region
}

locals {
  tags = {
    Name = "${var.company}-${var.application}-${var.environment}"
  }
}

resource "aws_subnet" "main" {
  count             = length(var.subnet_availability_zones)
  vpc_id            = var.vpc_id
  cidr_block        = var.subnet_cidrs[count.index]
  availability_zone = var.subnet_availability_zones[count.index]
  tags              = local.tags
}

resource "aws_internet_gateway" "main" {
  vpc_id = var.vpc_id
}

resource "aws_security_group" "main" {
  vpc_id = var.vpc_id
  name   = "${var.company}-${var.application}-${var.environment}"
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  tags = local.tags
}
