variable "region" {
  default = "us-west-1"
}

variable "company" {
  default = "sl"
}

variable "application" {
  default = "es"
}

variable "environment" {
  default = "test"
}

variable "vpc_id" {
  default = "vpc-01b78defb2e4a043c"
}

variable "vpc_cidr" {
  default = "10.128.128.0/22"
}

variable "subnet_availability_zones" {
  type = list(string)
  default = [
    "us-west-1a",
    "us-west-1c"
  ]
}

variable "subnet_cidrs" {
  type = list(string)
  default = [
    "10.128.128.0/24",
    "10.128.129.0/24",
  ]
}
