resource "aws_sqs_queue" "queue" {
  name                       = "${var.queue_name}-${var.environment}"
  delay_seconds              = var.delay_seconds
  max_message_size           = var.max_message_size
  message_retention_seconds  = var.message_retention_seconds
  receive_wait_time_seconds  = var.receive_wait_time_seconds
  visibility_timeout_seconds = var.visibility_timeout_seconds

  tags = {
    Company     = var.company
    Service     = "sqs"
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_iam_policy" "policy" {
  name        = "sqs-policy-${var.queue_name}-${var.environment}"
  description = "policy to publish to sqs queue"
  policy = templatefile("${path.module}/resources/sqs-policy.json", {
    sqs_queue = aws_sqs_queue.queue.arn
  })
}
