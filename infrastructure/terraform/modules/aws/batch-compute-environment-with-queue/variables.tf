variable "company" {
  description = "The company (e.g. Wavetrak) that this infrastructure belongs to."
}

variable "application" {
  description = "The application that this compute environment and job queue is being created for."
}

variable "service" {
  description = "The service associated with this compute environment and job queue (ex. Airflow)."
}

variable "job_name" {
  description = "The job name asssociated with this compute environment and job queue."
}

variable "environment" {
  description = "The environment (e.g sandbox, prod) that this compute environment and job queue is being created in."
}

variable "desired_vcpus" {
  description = "The desired number of EC2 vCPUs that an environment should maintain. IMPORTANT: This should only be set if minVCPUs need to be greater than 0. Refer to the README for more information."
  default     = null
}

variable "min_on_demand_vcpus" {
  description = "The minimum number of on-demand EC2 vCPUs that an environment should maintain."
  default     = 0
}

variable "max_on_demand_vcpus" {
  description = "The maximum number of on-demand EC2 vCPUs that an environment can have."
}

variable "instance_type" {
  description = "A list of EC2 instance types that may be launched (e.g. [c5d.2xlarge, m5.large])."
  type        = list(string)
  default     = []
}

variable "ec2_key_pair" {
  description = "The EC2 key pair that is used to access every instance launched in the associated compute environment."
}

variable "vpc_security_groups" {
  description = "A list of EC2 security group that are associated with EC2 instances launched in the compute environment."
  type        = list(string)
  default     = []
}

variable "vpc_subnets" {
  description = "A list of VPC subnets into which the compute resources are launched."
  type        = list(string)
  default     = []
}

variable "user_data" {
  description = "User data that is utilized to launch each EC2 instance in the compute environment. The user data can be defined in a template file then passed into the module as string using the file() function."
}

variable "associate_public_ip_address" {
  description = "Boolean value specifyign whether or not to associate a public ip address with the network interface."
}

variable "root_volume_type" {
  description = "EBS volume type for the EC2 root volume. i.e. gp2, gp3, io2, etc."
  default     = null
}

variable "root_volume_size" {
  description = "Size in GB to allocate for the EC2 instance root volume."
  default     = null
}

variable "root_volume_iops" {
  description = "IOPS to provision for the EC2 instance root volume."
  default     = null
}

variable "root_volume_throughput" {
  description = "MiB/s throughput to provision for the EC2 instance root volume."
  default     = null
}
