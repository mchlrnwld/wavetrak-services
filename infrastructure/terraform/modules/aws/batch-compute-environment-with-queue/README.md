# Batch Compute Environment With Queue

- [Batch Compute Environment With Queue](#batch-compute-environment-with-queue)
  - [Summary](#summary)
  - [Spot Instances](#spot-instances)
  - [Usage](#usage)
    - [IMPORTANT](#important)
    - [Example Module Usage](#example-module-usage)
    - [user_data.tpl](#user_datatpl)

## Summary

This module sets up infrastructure to support creating compute environments and a connected job queue on AWS Batch.

## Spot Instances

TL;DR - DO NOT attempt to connect job queues to spot instance compute environments unless the jobs that will be queued up can reasonably be delayed up to an hour.

We've previously attempted to take advantage of spot instances for these compute environments by setting up 2 compute environments (spot and on-demand) connected to each job queue with spot prioritized. Unfortunately when spot requests failed or were delayed, jobs ended up delayed for up to an hour because the queue only spills jobs over to the second compute environment once vCPUs are maxed out in the first.

The use case we want is an open feature request with the Batch team but is not currently supported and is not the use case that having multiple connected compute environments solves. Because of this we removed spot compute environments in https://github.com/Surfline/wavetrak-infrastructure/pull/666. See https://console.aws.amazon.com/support/home#/case/?displayId=7697157631&language=en for more information.

## Usage

### IMPORTANT

There are issues when creating/updating compute environments in terraform. These issues are described below:

- When making changes to an existing compute environment, terraform executes the following actions:

  - Destroys the compute environment.
  - Recreates the compute environment.

  This is problematic due to the fact that a job queue must be associated with a compute environment. Therefore, Terraform will not be able to complete
  these changes if applied. To avoid this issue, it is better to destroy the infrastructure completely. Then the infrastructure can be recreated.

  This can be accomplished in 2 different ways:

  1. Use the `terraform destroy` command. This can be used where the `batch-job-definition` module is referenced. Using this command will then proceed to destroy
     all infrastructure associated with the `batch-job-definition` module. **WARNING:** Use `terraform destroy` with caution, as it will destroy all infrastructure in the current stack.
  2. An alternative to using `terraform destroy` is to simply comment out where the `batch-job-definition` module is used.
     Use this option when the `batch-compute-environment-and-queue` module exists in the same file or directory as other terraform modules.
     To use this option, follow these steps:

     1. Comment-out the `batch-compute-environment-and-queue` module.
     2. Run `terraform apply` - all of the infrastructure should be destroyed.
     3. Remove the comments from the `batch-compute-environment-and-queue` module.
     4. Run `terraform apply` again - all of the infrastructure should be available.

     An example of commenting-out the module is shown below:

     ```hcl
     /*
     module "batch_compute_environment_with_queue" {
       source = "../modules/batch-compute-environment-with-queue"

       company     = "wt"
       application = "jobs-common"
       environment = "sandbox"
       job_name    = "example_job"

       min_on_demand_vcpus = 0
       max_on_demand_vcpus = 32
       instance_type       = ["m5.large"]
       vpc_security_groups = ["sg-91aeaef4", "sg-90aeaef5"]
       vpc_subnets         = ["subnet-0b09466e", "subnet-f4d458ad"]
       user_data           = file("${path.module}/user_data.tpl")
     }
     */
     ```

- When creating a compute environment, there are 2 parameters that are _required_:

  - `min_vCPUs`: defines the minimum amount of `vCPUs` to maintain in the compute environment.
  - `max_vCPUs`: defines the maximum number of `vCPUs` the compute environment can have.

  In addition to `min_vCPUs` and `max_VCPUs`, there is another parameter that is _optional_:

  - `desired_vCPUs`: defines the desired number of `vCPUs` to maintain in the compute environment.
    This should be adjusted by the compute environment itself based on demand.

  However, there is currently a bug in the terraform `compute_environment` resource.
  This bug _requires_ that `desired_vCPUs` be manually set if `min_vCPUs` is greater than 0.

  If `min_vCPUs` must be greater than 0, then the following steps should be taken:

  1. Assign `desired_vCPUs` the same value as `min_vCPUs`.
  2. Pass in `desired_vCPUS` into the module when `terraform apply` is applied for the first time.
  3. Remove `desired_vCPUs` from the module reference. `desired_vCPUs` will then be adjusted by the compute environment based on demand.

  For more information, refer to these github issues:

  - <https://github.com/terraform-providers/terraform-provider-aws/issues/4671>
  - <https://github.com/terraform-providers/terraform-provider-aws/pull/4855>

### Example Module Usage

```hcl
module "batch_compute_environment_with_queue" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-compute-environment-with-queue"

  company     = "wt"
  application = "jobs-common"
  environment = "sandbox"
  job_name    = "example_job"

  min_on_demand_vcpus = 0
  max_on_demand_vcpus = 32
  instance_type       = ["m5.large"]
  vpc_security_groups = ["sg-91aeaef4", "sg-90aeaef5"]
  vpc_subnets         = ["subnet-0b09466e", "subnet-f4d458ad"]
  # The example user_data.tpl below is converted to string using the file() function.
  user_data           = file("${path.module}/user_data.tpl")
}
```

### user_data.tpl

```tpl
Content-Type: multipart/mixed; boundary="==MYBOUNDARY=="
MIME-Version: 1.0

--==MYBOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash
echo ECS_IMAGE_PULL_BEHAVIOR=always >> /etc/ecs/ecs.config

--==MYBOUNDARY==--
```
