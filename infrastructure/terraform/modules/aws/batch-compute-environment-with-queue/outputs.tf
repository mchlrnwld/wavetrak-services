output "batch_job_queue_arn" {
  value = aws_batch_job_queue.job_queue.arn
}

output "on_demand_compute_environment_arn" {
  value = aws_batch_compute_environment.on_demand.arn
}
