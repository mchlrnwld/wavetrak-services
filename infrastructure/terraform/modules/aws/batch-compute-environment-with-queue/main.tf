locals {
  common_tags = {
    Company     = var.company
    Service     = var.service
    Application = var.application
    Environment = var.environment
    Name        = "${var.company}-${var.application}-${var.job_name}-${var.environment}"
    Terraform   = true
  }
}

resource "aws_iam_role" "ecs_instance_role" {
  name               = "${var.company}-${var.application}-${var.job_name}-ecs-instance-role-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ec2-policy.json")
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_instance_role" {
  name = "${var.company}-${var.application}-${var.job_name}-ecs-instance-profile-${var.environment}"
  role = aws_iam_role.ecs_instance_role.name
}

resource "aws_iam_role" "aws_batch_service_role" {
  name               = "${var.company}-${var.application}-${var.job_name}-batch-service-role-${var.environment}"
  assume_role_policy = file("${path.module}/resources/batch-policy.json")
}

resource "aws_iam_role_policy_attachment" "aws_batch_service_role" {
  role       = aws_iam_role.aws_batch_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole"
}

# Minimal launch template, AWS Batch overrides most parameters.
# This is only needed to include user-data with ECS config params.
# AWS Batch user data must be MIME-encoded.
resource "aws_launch_template" "launch_template" {
  name      = "${var.company}-${var.application}-${var.job_name}-${var.environment}"
  user_data = base64encode(var.user_data)

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_type           = var.root_volume_type
      volume_size           = var.root_volume_size
      iops                  = var.root_volume_iops
      throughput            = var.root_volume_throughput
      delete_on_termination = true
    }
  }

  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = var.associate_public_ip_address
    delete_on_termination       = true
    security_groups             = var.vpc_security_groups
  }

  tags = local.common_tags

  tag_specifications {
    resource_type = "instance"
    tags          = local.common_tags
  }

  tag_specifications {
    resource_type = "volume"
    tags          = local.common_tags
  }
}

resource "random_id" "launch_template" {
  keepers = {
    launch_template_latest_version = aws_launch_template.launch_template.latest_version
    instance_type                  = join(", ", var.instance_type)
    max_on_demand_vcpus            = var.max_on_demand_vcpus
    min_on_demand_vcpus            = var.min_on_demand_vcpus
    desired_vcpus                  = var.desired_vcpus
  }

  byte_length = 4
}

resource "aws_batch_compute_environment" "on_demand" {
  compute_environment_name = "${var.company}-${var.application}-${var.job_name}-on-demand-${var.environment}-${random_id.launch_template.hex}"
  lifecycle {
    create_before_destroy = true
  }
  compute_resources {
    launch_template {
      launch_template_id = aws_launch_template.launch_template.id
      version            = "$Latest"
    }

    instance_role = aws_iam_instance_profile.ecs_instance_role.arn

    instance_type = var.instance_type
    ec2_key_pair  = var.ec2_key_pair

    max_vcpus = var.max_on_demand_vcpus
    min_vcpus = var.min_on_demand_vcpus

    # IMPORTANT:
    # This parameter should only be set if `min_vCPUs` must be greater than 0.
    # If that is the case, then the following steps should be taken:
    # 1. Assign `desired_vCPUs` the same value as `min_vCPUs`.
    # 2. Pass in `desired_vCPUS` into the module when `terraform apply` is applied for the first time.
    # 3. Remove `desired_vCPUs` from the module reference. `desired_vCPUs` will then be adjusted by the compute environment based on demand.
    desired_vcpus = var.desired_vcpus

    # Security groups are overridden in the launch template,
    # but this parameter is still required in the compute environment resource.
    security_group_ids = []
    subnets            = var.vpc_subnets

    tags = local.common_tags

    type = "EC2"
  }

  service_role = aws_iam_role.aws_batch_service_role.arn
  type         = "MANAGED"
  tags         = local.common_tags

  depends_on = [
    aws_iam_role.ecs_instance_role,
    aws_iam_role_policy_attachment.aws_batch_service_role,
  ]
}

resource "aws_batch_job_queue" "job_queue" {
  name     = "${var.company}-${var.application}-${var.job_name}-job-queue-${var.environment}"
  state    = "ENABLED"
  priority = 1
  compute_environments = [
    aws_batch_compute_environment.on_demand.arn,
  ]
  depends_on = [
    aws_batch_compute_environment.on_demand,
  ]
}
