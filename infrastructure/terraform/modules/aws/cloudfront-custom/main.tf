data "aws_region" "current" {
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

data "aws_acm_certificate" "cdn_acm" {
  count    = var.domain_count
  provider = aws.us-east-1
  domain   = element(split(",", element(var.domain_map, count.index)), "1")
  statuses = ["ISSUED"]
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  count = var.domain_count
  origin {
    domain_name = element(split(",", element(var.domain_map, count.index)), "2")
    origin_id   = "custom-${element(split(",", element(var.domain_map, count.index)), "2")}"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = "${element(split(",", element(var.domain_map, count.index)), "2")} ${var.comment}"
  aliases         = [element(split(",", element(var.domain_map, count.index)), "0")]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "custom-${element(split(",", element(var.domain_map, count.index)), "2")}"
    compress         = true

    forwarded_values {
      query_string = var.query_strings

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    acm_certificate_arn      = element(data.aws_acm_certificate.cdn_acm.*.arn, count.index)
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
