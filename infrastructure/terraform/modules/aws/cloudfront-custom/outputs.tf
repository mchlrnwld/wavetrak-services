output "domain_name" {
  value = aws_cloudfront_distribution.cdn[0].domain_name
}

output "domain_alias" {
  value = aws_cloudfront_distribution.cdn[0].aliases
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.cdn[0].hosted_zone_id
}
