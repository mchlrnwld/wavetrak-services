variable "company" {
}

variable "environment" {
}

variable "application" {
}

variable "service" {
}

variable "domain_map" {
  type = list(string)
}

variable "domain_count" {
  default = 1
}

variable "comment" {
}

variable "versioning_enabled" {
}

variable "allowed_origins" {
}

variable "query_strings" {
  default = false
}

variable "default_ttl" {
  default = 600
}

variable "max_ttl" {
  default = 600
}
