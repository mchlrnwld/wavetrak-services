provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "image-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "image-service" {
  source = "../../"

  environment = "sandbox"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  internal_sg_group = "sg-90aeaef5"
}
