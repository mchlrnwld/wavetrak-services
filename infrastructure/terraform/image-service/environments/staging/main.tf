provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "image-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "image-service" {
  source = "../../"

  environment = "staging"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  internal_sg_group = "sg-90aeaef5"
}
