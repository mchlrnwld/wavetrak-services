provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "image-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "image-service" {
  source = "../../"

  environment = "prod"
  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0"
  ]
  internal_sg_group = "sg-a5a0e5c0"
}
