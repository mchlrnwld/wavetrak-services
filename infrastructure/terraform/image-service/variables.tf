variable "company" {
  default = "sl"
}

variable "application" {
  default = "image-service"
}

variable "environment" {
}

variable "instance_subnets" {
}

variable "internal_sg_group" {
}
