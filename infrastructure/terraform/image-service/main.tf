resource "aws_lambda_function" "image-service" {
  function_name = "${var.company}-image-service-${var.environment}"
  runtime       = "nodejs12.x"
  s3_bucket     = "sl-admin-lambda-functions-${var.environment}"
  s3_key        = "${var.company}-image-service-lambda.zip"
  timeout       = 60
  handler       = "dist/index.default"
  role          = aws_iam_role.lambda_role.arn
  memory_size   = 1024
  vpc_config {
    subnet_ids         = var.instance_subnets
    security_group_ids = [var.internal_sg_group]
  }
}

resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-lambda-${var.application}-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/lambda-role-policy.json", {})
}

resource "aws_iam_role_policy_attachment" "lambda_execution_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_policy" "ecs-execute" {
  name        = "image-service-execute-${var.environment}"
  description = "ecs hosts can execute image service"
  policy = templatefile("${path.module}/resources/lambda-function-policy.json", {
    lambda_function_arn = aws_lambda_function.image-service.arn
  })
}
