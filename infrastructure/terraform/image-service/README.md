# Spot thumbnail manager

- [Spot thumbnail manager](#spot-thumbnail-manager)
  - [Setup](#setup)
  - [Deploying to a AWS environment](#deploying-to-a-aws-environment)
  - [State Management](#state-management)

## Setup

- `export ENV=staging` Configures your target environment choice
- `make setup` Configures your local terraform's state to match your target environment
- `make plan` Performs a dry run so you can verify changes
- `make apply` Applies your changes

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)

```bash
make plan
make apply
```

## State Management

For now keep each environment `.tfstate` in S3. Future plans could use other providers such as Artifactory. To pull state from a given environment run

```bash
make setup
```
