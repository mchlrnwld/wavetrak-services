#!/bin/bash
for d in *; do
  if [ -e "$d/.terraform-version" ]; then
    tf_version="$(tr -d '\n' <"$d/.terraform-version")"
  else
    tf_version="none"
  fi
  printf "%s\t %s\n" "$d" "${tf_version}"
done | column -t
