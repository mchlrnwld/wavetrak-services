# Braze CDN Service Infrastructure

This Terraform code tracks changes made for Braze CDN.

It was created manually using the following documents and imported into Terraform:

- <https://www.sparkpost.com/docs/tech-resources/enabling-https-engagement-tracking-on-sparkpost/#step-by-step-guide-with-aws-cloudfront>
- <https://www.braze.com/docs/user_guide/onboarding_with_braze/email_setup/ssl_clicktracking/>

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version specified in [.terraform-version](./.terraform-version)._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sandbox make plan
ENV=sandbox make apply
```

Valid environment is only `prod`.

#### State Management

State is uploaded to S3 after `make apply` is run.
