SHELL := /bin/bash

.PHONY: checkenv
checkenv:
	@if [ -z "environments/${ENV}" ]; then \
		echo "ENV was not set"; exit 10; \
	fi

.PHONY: init
init: checkenv
	@pushd ./environments/${ENV} && \
	terraform init && \
	popd

.PHONY: plan
plan: init
	@pushd ./environments/${ENV} && \
	terraform plan -refresh=true && \
	popd

.PHONY: apply
apply: init
	@pushd ./environments/${ENV} && \
	terraform apply -refresh=true && \
	popd

.PHONY: destroy
destroy: init
	@pushd ./environments/${ENV} && \
	terraform destroy -refresh=true && \
	popd

.PHONY: format
format:
	terraform fmt -recursive

.PHONY: format-check
format-check:
	terraform fmt -check -recursive

.PHONY: validate
validate: init
	@pushd ./environments/${ENV} && \
	terraform validate && \
	popd
