provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "braze-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

resource "aws_cloudfront_distribution" "distribution" {
  aliases = [
    "email.surfline.com",
    "link.surfline.com",
    "news.surfline.com",
    "notifications.surfline.com",
  ]
  comment             = "Braze / Sparkpost Email Tracking"
  enabled             = true
  http_version        = "http2"
  is_ipv6_enabled     = true
  price_class         = "PriceClass_All"
  retain_on_delete    = false
  tags                = {}
  wait_for_deployment = true

  default_cache_behavior {
    allowed_methods = [
      "GET",
      "HEAD",
    ]
    cached_methods = [
      "GET",
      "HEAD",
    ]
    compress               = false
    default_ttl            = 10
    max_ttl                = 31536000
    min_ttl                = 0
    smooth_streaming       = false
    target_origin_id       = "Custom-spgo.io"
    trusted_signers        = []
    viewer_protocol_policy = "allow-all"

    forwarded_values {
      headers = [
        "User-Agent",
      ]
      query_string            = true
      query_string_cache_keys = []

      cookies {
        forward           = "none"
        whitelisted_names = []
      }
    }
  }

  origin {
    domain_name = "spgo.io"
    origin_id   = "Custom-spgo.io"

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy   = "https-only"
      origin_read_timeout      = 30
      origin_ssl_protocols = [
        "TLSv1",
        "TLSv1.1",
        "TLSv1.2",
      ]
    }
  }

  restrictions {
    geo_restriction {
      locations        = []
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn            = "arn:aws:acm:us-east-1:833713747344:certificate/9c34df12-d074-4604-bc38-c890e3438e15"
    cloudfront_default_certificate = false
    minimum_protocol_version       = "TLSv1.2_2019"
    ssl_support_method             = "sni-only"
  }
}
