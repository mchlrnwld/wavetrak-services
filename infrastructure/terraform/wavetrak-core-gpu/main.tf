provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "ami" {
  name = "/aws/service/ecs/optimized-ami/amazon-linux-2/gpu/recommended"
}

locals {
  cluster_name  = "${var.company}-${var.application}-${var.environment}"
  resource_name = "${var.company}-${var.application}-%s-${var.environment}"
  default_cidr_blocks = [
    "0.0.0.0/0",
  ]
  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Service     = "ecs"
    Terraform   = "true"
    Source      = "Surfline/wavetrak-infrastructure/terraform/wavetrak-core-gpu"
  }
}

# The cluster.
resource "aws_ecs_cluster" "main" {
  name = local.cluster_name
  tags = local.tags
}

# Security groups.
resource "aws_security_group" "ecs" {
  name        = format(local.resource_name, "sg-ecs")
  description = "Allows traffic to/from ECS Cluster"

  vpc_id = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = local.default_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = local.default_cidr_blocks
  }

  tags = local.tags
}

resource "aws_security_group" "alb" {
  name        = format(local.resource_name, "sg-alb")
  description = "Allows web traffic from Application Load Balancer"

  vpc_id = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = local.default_cidr_blocks
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = local.default_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = local.default_cidr_blocks
  }

  tags = local.tags
}

resource "aws_security_group" "client" {
  name        = format(local.resource_name, "sg-client")
  description = "Allows traffic to external applications, like databases and cache services."

  vpc_id = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = local.default_cidr_blocks
  }

  tags = local.tags
}

# IAM.
resource "aws_iam_role" "ecs_instance" {
  name = format(local.resource_name, "iam-role-ecs-instance")

  assume_role_policy = file("${path.module}/resources/instance-role.json")
}

resource "aws_iam_role_policy" "ecs_instance" {
  name = format(local.resource_name, "iam-role-policy-ecs-instance")

  policy = file("${path.module}/resources/instance-policy.json")
  role   = aws_iam_role.ecs_instance.id
}

resource "aws_iam_role_policy_attachment" "ecs_instance" {
  role       = aws_iam_role.ecs_instance.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_instance" {
  name = format(local.resource_name, "iam-instance-profile-ecs-instance")

  path = "/"
  role = aws_iam_role.ecs_instance.name
}

resource "aws_iam_role" "ecs_service" {
  name = format(local.resource_name, "iam-role-ecs-service")

  assume_role_policy = file("${path.module}/resources/service-role.json")
}

resource "aws_iam_role_policy" "ecs_service" {
  name = format(local.resource_name, "iam-role-policy-ecs-service")

  policy = file("${path.module}/resources/service-policy.json")
  role   = aws_iam_role.ecs_service.id
}

# Autoscaling + CloudWatch alarms.
resource "aws_launch_configuration" "main" {
  name_prefix   = format(local.resource_name, "launch-configuration")
  image_id      = jsondecode(data.aws_ssm_parameter.ami.value)["image_id"]
  key_name      = var.instance.key_name
  instance_type = var.instance.type
  security_groups = [
    var.sg_internal,
    aws_security_group.ecs.id,
    aws_security_group.client.id,
  ]
  iam_instance_profile = aws_iam_instance_profile.ecs_instance.name
  user_data = templatefile("${path.module}/resources/user_data.sh", {
    cluster = local.cluster_name
  })
}

resource "aws_autoscaling_group" "main" {
  name = format(local.resource_name, "asg")

  min_size             = var.instance.min_count
  max_size             = var.instance.max_count
  desired_capacity     = var.instance.desired
  vpc_zone_identifier  = var.instance.subnets
  availability_zones   = var.instance.availability_zones
  launch_configuration = aws_launch_configuration.main.name
  health_check_type    = "EC2"

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = format(local.resource_name, "ecs")
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = local.tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

resource "aws_autoscaling_policy" "gpu_reservation_scale_up" {
  name = format(local.resource_name, "autoscaling-policy-gpu-reservation-scale-up")

  autoscaling_group_name = aws_autoscaling_group.main.name

  adjustment_type    = "ChangeInCapacity"
  policy_type        = "SimpleScaling"
  cooldown           = var.autoscaling.cooldown_period
  scaling_adjustment = 1
}

resource "aws_cloudwatch_metric_alarm" "gpu_reservation_scale_up" {
  alarm_name        = format(local.resource_name, "cw-gpu-reservation-scale-up")
  alarm_description = "Trigger ASG scale up policy when GPU reservation exceeds high threshold"

  comparison_operator = "GreaterThanOrEqualToThreshold"
  threshold           = var.autoscaling.threshold_high
  period              = var.autoscaling.interval_period
  evaluation_periods  = 2

  metric_name = "GPUReservation"
  namespace   = "AWS/ECS"
  statistic   = "Average"
  dimensions = {
    ClusterName = local.cluster_name
  }

  actions_enabled = true
  alarm_actions = [
    aws_autoscaling_policy.gpu_reservation_scale_up.arn
  ]

  tags = local.tags
}

resource "aws_autoscaling_policy" "gpu_reservation_scale_down" {
  name = format(local.resource_name, "autoscaling-policy-gpu-reservation-scale-down")

  autoscaling_group_name = aws_autoscaling_group.main.name

  adjustment_type    = "ChangeInCapacity"
  policy_type        = "SimpleScaling"
  cooldown           = var.autoscaling.cooldown_period
  scaling_adjustment = -1
}

resource "aws_cloudwatch_metric_alarm" "gpu_reservation_scale_down" {
  alarm_name        = format(local.resource_name, "cw-gpu-reservation-scale-down")
  alarm_description = "Trigger ASG scale down when GPU reservation recedes lower threshold"

  comparison_operator = "LessThanOrEqualToThreshold"
  threshold           = var.autoscaling.threshold_low
  period              = var.autoscaling.interval_period
  evaluation_periods  = 2

  metric_name = "GPUReservation"
  namespace   = "AWS/ECS"
  statistic   = "Average"
  dimensions = {
    ClusterName = local.cluster_name
  }

  actions_enabled = true
  alarm_actions = [
    aws_autoscaling_policy.gpu_reservation_scale_down.arn
  ]

  tags = local.tags
}

resource "aws_cloudwatch_log_group" "main" {
  name = format(local.resource_name, "logs")

  retention_in_days = 3
}

# Load balancer.
resource "aws_alb" "main" {
  name = format(local.resource_name, "alb")

  subnets = var.alb_subnets
  security_groups = [
    aws_security_group.alb.id,
  ]
  internal = true

  tags = local.tags
}

resource "aws_alb_listener" "main" {
  load_balancer_arn = aws_alb.main.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = 200
    }
  }
}
