# Wavetrak GPU Services

A complete Terraform configuration to create the needed infrastructure to run a GPU-Enabled Cluster in Amazon ECS, for services that could be benefited from GPU workloads.

## Table of contents <!-- omit in toc -->

- [Requirements](#requirements)
  - [Terraform](#terraform)
  - [AWS credentials](#aws-credentials)
  - [NewRelic information](#newrelic-information)
  - [Make](#make)
- [Structure](#structure)
- [Usage](#usage)
- [Deploy](#deploy)
- [Add a new GPU Service](#add-a-new-gpu-service)
  - [Deploy a testing service](#deploy-a-testing-service)

## Requirements

### Terraform

You need to have [Terraform] installed in your local environment. This particular configuration is version `0.12.28`.

An easy way to accomplish this is by installing [tfenv] directly and then setting this version as default:

```bash
tfenv install 0.12.28
tfenv use 0.12.28
```

### AWS credentials

Since the infrastructure is being installed in [AWS], in the **Credentials** section of our [Terraform Style Guide] you can check how to obtain and make use of your AWS credentials to run the Terraform commands.

### NewRelic information

The alarms created in [NewRelic] by the ECS module require the `account_id` and `api_key` configured in your local environment.

For this, you'll need to retrieve these values from AWS Parameter Store and export them as environment variables:

```bash
assume-role sandbox
export NEW_RELIC_ACCOUNT_ID=$(
  aws ssm get-parameters \
    --names "/sandbox/common/NEW_RELIC_ACCOUNT_ID" \
    --region "us-west-1" \
    --with-decrypt | \
  jq -r '.Parameters[].Value' \
)
export NEW_RELIC_API_KEY=$(
  aws ssm get-parameters \
    --names "/sandbox/common/NEW_RELIC_API_KEY" \
    --region "us-west-1" \
    --with-decrypt | \
  jq -r '.Parameters[].Value' \
)
```

> Note: The Account ID and API KEY values are the same for all of the environments, so you can execute this part one single time.

### Make

Provided in most of the development environments, `make` is needed to run the commands we defined in the `Makefile` file in the root of this folder. If you're running this from inside a container or a limited virtual machine, make sure you have `make` installed.

## Structure

The Terraform configuration is based on a `main.tf` file in the root of this folder, in charge of creating all of the resources the cluster needs, but the execution of the Terraform commands needs an environment with their specific settings where to deploy.

```bash
├── environments/
│   ├── prod/
│   │   └── main.tf
│   ├── sandbox/
│   │   └── main.tf
│   └── staging/
│       └── main.tf
├── .terraform-version
├── Makefile
├── README.md
├── main.tf
├── outputs.tf
├── variables.tf
└── versions.tf
```

All environment related settings will be stored in the `environments/{env}/main.tf` file.

## Usage

We created a curated list of Terraform and cleaning commands that you can check in the `Makefile` file:

- checkenv
- init
- plan
- apply
- refresh
- destroy
- format
- validate
- output
- pull
- reset

Most of these commands will need to be prefixed with the `ENV` variable and the proper environment name, so the format of the commands will be `ENV={env} make {command}`:

```bash
ENV=sandbox make plan
ENV=prod make pull
```

## Deploy

When you make changes to the configuration files, make sure you're running the commands using the right AWS credentials and the right environment you want to modify:

```bash
assume-role dev
ENV=sandbox make plan
ENV=sandbox make apply
```

The available environments for this configuration are `sandbox`, `staging` and `prod`.

## Add a new GPU Service

A GPU Service will have the same structure and information as any other deployed Core Service.

The main difference on their `ci.json` file will be the `cluster` name.

```text
{
  "imageName": "services/example-gpu-service",
  "serviceName": "example-gpu-service",
  "cluster": "wt-core-gpu",
  "secrets": {
    "common": {
      "prefix": "/{env}/common/",
      ...
```

The **task definitions** and **repositories** involved in a GPU Service still needs to be created the same as a Core Service, so refer to the documentation how to [add a new service], with the specific considerations for a GPU Service and its container configuration.

### Deploy a testing service

A simple service was created to run into the cluster and it will work as a verification that it's correctly configured and validated, since you should find a task running in the GPU-Enabled cluster within the `wt-simple-service-{env}` service and using the `wt-core-gpu-{env}-simple-service` task definition with minimal resources.

Searching for the task in the AWS Console, at the **Containers** section and expanding the carat of the `simple-service` container, if you browse to the **External Link** shown in the details, it should give you a JSON response similar to the above.

You can find the code to [deploy this simple service] in `wavetrak-services` repository.

[AWS]: https://aws.amazon.com/
[Terraform]: https://www.terraform.io/
[tfenv]: https://github.com/tfutils/tfenv
[Terraform Style Guide]: https://github.com/Surfline/styleguide/tree/master/terraform#credentials
[NewRelic]: https://one.newrelic.com/
[add a new service]: ../surfline-core-services/README.md#adding-a-new-service
[deploy this simple service]: https://github.com/Surfline/wavetrak-services/tree/master/services/simple-service
