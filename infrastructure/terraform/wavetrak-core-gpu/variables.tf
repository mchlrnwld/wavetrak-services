variable "company" {
  type = string
}

variable "application" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "alb_subnets" {
  type = list(string)
}

variable "sg_internal" {
  type = string
}

variable "instance" {
  type = object({
    type               = string
    min_count          = number
    max_count          = number
    desired            = number
    metrics            = bool
    key_name           = string
    availability_zones = list(string)
    subnets            = list(string)
  })
}

variable "autoscaling" {
  type = object({
    cooldown_period = number
    interval_period = number
    threshold_high  = number
    threshold_low   = number
  })
}
