terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "wavetrak-core-gpu/staging/ecs-cluster.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "core-gpu"
  environment = "staging"

  vpc_id   = "vpc-981887fd"
  alb_subnets = [
    "subnet-0b09466e",
    "subnet-f4d458ad",
  ]
  sg_internal = "sg-90aeaef5"

  instance = {
    type      = "g4dn.xlarge"
    key_name  = "sturdy-surfline-dev"
    min_count = 1
    max_count = 10
    desired   = 1
    metrics   = true
    availability_zones = [
      "us-west-1b",
      "us-west-1c",
    ]
    subnets = [
      "subnet-0909466c",
      "subnet-f2d458ab",
    ]
  }

  autoscaling = {
    cooldown_period = 900
    interval_period = 120
    threshold_high  = 80
    threshold_low   = 20
  }
}

module "core_gpu_cluster" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  vpc_id          = local.vpc_id
  alb_subnets     = local.alb_subnets
  sg_internal     = local.sg_internal
  instance        = local.instance
  autoscaling     = local.autoscaling
}
