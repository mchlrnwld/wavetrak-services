terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wavetrak-core-gpu/prod/ecs-cluster.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "core-gpu"
  environment = "prod"

  vpc_id = "vpc-116fdb74"
  alb_subnets = [
    "subnet-baab36df",
    "subnet-debb6987",
  ]
  sg_internal = "sg-a5a0e5c0"

  instance = {
    type      = "g4dn.xlarge"
    key_name  = "sturdy_surfline_id_rsa"
    min_count = 1
    max_count = 10
    desired   = 1
    metrics   = true
    availability_zones = [
      "us-west-1b",
      "us-west-1c",
    ]
    subnets = [
      "subnet-85ab36e0",
      "subnet-d1bb6988",
    ]
  }

  autoscaling = {
    cooldown_period = 900
    interval_period = 120
    threshold_high  = 80
    threshold_low   = 20
  }
}

module "core_gpu_cluster" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  vpc_id      = local.vpc_id
  alb_subnets = local.alb_subnets
  sg_internal = local.sg_internal
  instance    = local.instance
  autoscaling = local.autoscaling
}
