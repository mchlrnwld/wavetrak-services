data "aws_ssm_document" "foo" {
  name = "AWS-RunPatchBaseline"
}

resource "aws_ssm_maintenance_window" "patch_window" {
  name     = "${var.company}-${var.service_name}-maintenance-window-${var.environment}"
  schedule = "cron(0 5 * * ? *)"
  duration = 2
  cutoff   = 1
}

# Amazon Linux 1
data "aws_ssm_patch_baseline" "amzn_1_baseline" {
  owner            = "AWS"
  name_prefix      = "AWS-"
  default_baseline = true
  operating_system = "AMAZON_LINUX"
}

resource "aws_ssm_patch_group" "amzn_1_patchgroup" {
  baseline_id = data.aws_ssm_patch_baseline.amzn_1_baseline.id
  patch_group = "${var.environment}-patch-group-amnz-1"
}

resource "aws_ssm_maintenance_window_task" "amzn_1_task" {
  max_concurrency = 5
  max_errors      = 1
  priority        = 1
  task_arn        = "AWS-RunPatchBaseline"
  task_type       = "RUN_COMMAND"
  window_id       = aws_ssm_maintenance_window.patch_window.id

  targets {
    key    = "WindowTargetIds"
    values = [aws_ssm_maintenance_window_target.amzn_1_target.id]
  }

  task_invocation_parameters {
    run_command_parameters {
      timeout_seconds = 40

      parameter {
        name   = "Operation"
        values = ["Install"]
      }

      parameter {
        name   = "RebootOption"
        values = ["NoReboot"]
      }
    }
  }
}

resource "aws_ssm_maintenance_window_target" "amzn_1_target" {
  window_id     = aws_ssm_maintenance_window.patch_window.id
  name          = "amzn1-maintenance-window-target"
  description   = "This is a maintenance window target for Amazon Linux 1 instances"
  resource_type = "INSTANCE"

  targets {
    key    = "tag:Patch Group"
    values = ["${var.environment}-patch-group-amnz-1"]
  }
}

# Amazon Linux 2
data "aws_ssm_patch_baseline" "amzn_2_baseline" {
  owner            = "AWS"
  name_prefix      = "AWS-"
  default_baseline = true
  operating_system = "AMAZON_LINUX_2"
}

resource "aws_ssm_patch_group" "amzn_2_patchgroup" {
  baseline_id = data.aws_ssm_patch_baseline.amzn_2_baseline.id
  patch_group = "${var.environment}-patch-group-amzn-2"
}

resource "aws_ssm_maintenance_window_task" "amzn_2_task" {
  max_concurrency = 5
  max_errors      = 130
  priority        = 1
  task_arn        = "AWS-RunPatchBaseline"
  task_type       = "RUN_COMMAND"
  window_id       = aws_ssm_maintenance_window.patch_window.id

  targets {
    key    = "WindowTargetIds"
    values = [aws_ssm_maintenance_window_target.amzn_2_target.id]
  }

  task_invocation_parameters {
    run_command_parameters {
      timeout_seconds = 300

      parameter {
        name   = "Operation"
        values = ["Install"]
      }

      parameter {
        name   = "RebootOption"
        values = ["NoReboot"]
      }
    }
  }
}

resource "aws_ssm_maintenance_window_target" "amzn_2_target" {
  window_id     = aws_ssm_maintenance_window.patch_window.id
  name          = "amzn2-aintenance-window-target"
  description   = "This is a maintenance window target for Amazon Linux 2 instances"
  resource_type = "INSTANCE"

  targets {
    key    = "tag:Patch Group"
    values = ["${var.environment}-patch-group-amnz-2"]
  }
}

# Ubuntu
data "aws_ssm_patch_baseline" "ubuntu_baseline" {
  owner            = "AWS"
  name_prefix      = "AWS-"
  default_baseline = true
  operating_system = "UBUNTU"
}

resource "aws_ssm_patch_group" "ubuntu_patchgroup" {
  baseline_id = data.aws_ssm_patch_baseline.ubuntu_baseline.id
  patch_group = "${var.environment}-patch-group-ubuntu"
}

resource "aws_ssm_maintenance_window_task" "ubuntu_task" {
  max_concurrency = 5
  max_errors      = 1
  priority        = 1
  task_arn        = "AWS-RunPatchBaseline"
  task_type       = "RUN_COMMAND"
  window_id       = aws_ssm_maintenance_window.patch_window.id

  targets {
    key    = "WindowTargetIds"
    values = [aws_ssm_maintenance_window_target.ubuntu_target.id]
  }

  task_invocation_parameters {
    run_command_parameters {
      timeout_seconds = 300

      parameter {
        name   = "Operation"
        values = ["Install"]
      }

      parameter {
        name   = "RebootOption"
        values = ["NoReboot"]
      }
    }
  }
}

resource "aws_ssm_maintenance_window_target" "ubuntu_target" {
  window_id     = aws_ssm_maintenance_window.patch_window.id
  name          = "ubuntu-aintenance-window-target"
  description   = "This is a maintenance window target for Ubuntu instances"
  resource_type = "INSTANCE"

  targets {
    key    = "tag:Patch Group"
    values = ["${var.environment}-patch-group-ubuntu"]
  }
}
