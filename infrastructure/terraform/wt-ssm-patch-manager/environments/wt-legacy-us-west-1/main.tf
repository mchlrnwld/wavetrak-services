provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-legacy"
    key    = "ssm-patch-manager/wt-legacy-us-west-1/terraform.tfstate"
    region = "us-west-1"
  }
}

module "patch-manager" {
  source = "../../"

  company      = "wavetrak"
  service_name = "patch-manager"
  environment  = "legacy"
}
