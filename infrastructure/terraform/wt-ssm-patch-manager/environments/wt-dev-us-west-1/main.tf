provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "ssm-patch-manager/wt-dev-us-west-1/terraform.tfstate"
    region = "us-west-1"
  }
}

module "patch-manager" {
  source = "../../"

  company      = "wavetrak"
  service_name = "patch-manager"
  environment  = "dev"
}
