locals {
  role_name = "${var.company}-${var.application}-${var.environment}"
}

resource "aws_iam_role" "ad_creatives" {
  name               = local.role_name
  assume_role_policy = templatefile("${path.module}/resources/iam-role.json", {})
}

resource "aws_iam_role_policy" "ad_creatives" {
  name = local.role_name
  role = aws_iam_role.ad_creatives.id
  policy = templatefile("${path.module}/resources/iam-role-policy.json", {
    bucket_name = aws_s3_bucket.ad_creatives.bucket
  })
}
