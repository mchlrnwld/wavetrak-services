variable "company" {}
variable "application" {}
variable "service" {}
variable "environment" {}
variable "cdn_fqdn" {}
variable "cdn_acm" {}
variable "alias_hosted_zone_id" {}
variable "price_class" {}
