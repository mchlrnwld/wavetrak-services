provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "ad-creatives-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "ad_creatives_cdn" {
  source = "../../"

  company     = "sl"
  application = "ad-creatives"
  service     = "cdn"
  environment = "prod"

  cdn_fqdn             = "creatives.cdn-surfline.com"
  cdn_acm              = "arn:aws:acm:us-east-1:833713747344:certificate/522318ff-4e5b-4642-b217-a932bc4df082"
  alias_hosted_zone_id = "Z1OCTVEV5XMNUZ"

  price_class = "PriceClass_100"
}
