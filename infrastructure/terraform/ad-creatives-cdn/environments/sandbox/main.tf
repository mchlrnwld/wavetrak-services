provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "ad-creatives-cdn/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "ad_creatives_cdn" {
  source = "../../"

  company     = "sl"
  application = "ad-creatives"
  service     = "cdn"
  environment = "sandbox"

  cdn_fqdn             = "creatives-cdn.sandbox.surfline.com"
  cdn_acm              = "arn:aws:acm:us-east-1:665294954271:certificate/a48acac9-c364-4186-b33a-73543e5f72c0"
  alias_hosted_zone_id = "Z3DM6R3JR1RYXV"

  price_class = "PriceClass_All"
}
