provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "ad-creatives-cdn/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "ad_creatives_cdn" {
  source = "../../"

  company     = "sl"
  application = "ad-creatives"
  service     = "cdn"
  environment = "staging"

  cdn_fqdn             = "creatives-cdn.staging.surfline.com"
  cdn_acm              = "arn:aws:acm:us-east-1:665294954271:certificate/07c4d79f-1151-42ed-bb62-5ea7c0f93208"
  alias_hosted_zone_id = "Z3JHKQ8ELQG5UE"

  price_class = "PriceClass_All"
}
