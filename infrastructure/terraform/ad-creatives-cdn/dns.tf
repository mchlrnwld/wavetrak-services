resource "aws_route53_record" "ad_creatives_cdn" {
  zone_id = var.alias_hosted_zone_id
  name    = var.cdn_fqdn
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.ad_creatives_cdn.domain_name
    zone_id                = aws_cloudfront_distribution.ad_creatives_cdn.hosted_zone_id
    evaluate_target_health = false
  }
}
