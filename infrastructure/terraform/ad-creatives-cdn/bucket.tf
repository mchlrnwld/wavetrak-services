locals {
  bucket_name = "${var.company}-${var.application}-${var.service}-${var.environment}"
}

resource "aws_s3_bucket" "ad_creatives" {
  bucket = local.bucket_name
  policy = templatefile("${path.module}/resources/s3-policy.json", {
    bucket_name = local.bucket_name
    iam_arn     = aws_cloudfront_origin_access_identity.ad_creatives.iam_arn
  })

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["HEAD", "GET", "PUT", "POST", "DELETE"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  tags = {
    Name        = local.bucket_name
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}
