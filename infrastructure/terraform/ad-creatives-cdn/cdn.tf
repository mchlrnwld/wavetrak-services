resource "aws_cloudfront_distribution" "ad_creatives_cdn" {
  origin {
    domain_name = "${aws_s3_bucket.ad_creatives.bucket}.s3.amazonaws.com"
    origin_id   = "S3-${aws_s3_bucket.ad_creatives.bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.ad_creatives.cloudfront_access_identity_path
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  aliases         = [var.cdn_fqdn]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${aws_s3_bucket.ad_creatives.bucket}"
    compress         = true

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = var.price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }

  viewer_certificate {
    acm_certificate_arn      = var.cdn_acm
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

resource "aws_cloudfront_origin_access_identity" "ad_creatives" {
}
