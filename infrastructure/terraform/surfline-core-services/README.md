# Surfline Core Services

- [Surfline Core Services](#surfline-core-services)
  - [Naming Conventions](#naming-conventions)
  - [Deploying to a AWS environment](#deploying-to-a-aws-environment)
  - [Adding a New Service](#adding-a-new-service)
  - [Prerequisites](#prerequisites)
  - [Task Definitions](#task-definitions)
  - [Repositories](#repositories)
  - [Terraform Config to Update](#terraform-config-to-update)
  - [Service Config to Update](#service-config-to-update)
  - [Future Plans](#future-plans)
  - [State Management](#state-management)
  - [Modules](#modules)
  - [Further Reading](#further-reading)

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)

```bash
make plan
make apply
```

## Adding a New Service

## Prerequisites

- Terraform 0.12.28

## Task Definitions

Task definitions for all services must be manually created before
applying Terraform config. The following command may be used to
create task definitions:

```bash
# In surfline-dev aws account
aws ecs register-task-definition \
  --family sl-core-svc-sandbox-reports-api \
  --container-definitions \
 '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"reports-api","portMappings":[{"containerPort":8080,"hostPort":8080,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# In surfline-dev aws account
aws ecs register-task-definition \
  --family sl-core-svc-sandbox-spots-api \
  --container-definitions \
 '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"spots-api","portMappings":[{"containerPort":8080,"hostPort":8080,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# In surfline-dev aws account
aws ecs register-task-definition \
  --family sl-core-svc-staging-reports-api \
  --container-definitions \
 '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"reports-api","portMappings":[{"containerPort":8080,"hostPort":8080,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# In surfline-dev aws account
aws ecs register-task-definition \
  --family sl-core-svc-staging-spots-api \
  --container-definitions \
 '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"spots-api","portMappings":[{"containerPort":8080,"hostPort":8080,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# In surfline-prod aws account
aws ecs register-task-definition \
  --family sl-core-svc-prod-reports-api \
  --container-definitions \
 '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"reports-api","portMappings":[{"containerPort":8080,"hostPort":8080,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'

# In surfline-prod aws account
aws ecs register-task-definition \
  --family sl-core-svc-prod-spots-api \
  --container-definitions \
 '{"cpu":0,"disableNetworking":true,"essential":true,"image":"amazonlinux","memory":128,"memoryReservation":128,"name":"spots-api","portMappings":[{"containerPort":8080,"hostPort":8080,"protocol":"tcp"}],"privileged":false,"readonlyRootFilesystem":true}'
```

## Repositories

ECR repositories must be manually created before services can be
deployed. We currently store images for all environments in the
`surfline-prod` account and set policy to allow these images to be read
from `surfline-dev` and `surfline-legacy`.

```bash
# In surfline-prod aws account
export REPOSITORY_NAME=services/feed-api
aws ecr create-repository --repository-name ${REPOSITORY_NAME}

aws ecr set-repository-policy --repository-name  ${REPOSITORY_NAME} --policy-text \
  '{"Version":"2008-10-17","Statement":[{"Sid":"AllowPullFromSurflineDevAws","Effect":"Allow","Principal":{"AWS":"arn:aws:iam::665294954271:root"},"Action":["ecr:GetDownloadUrlForLayer","ecr:BatchGetImage","ecr:BatchCheckLayerAvailability"]},{"Sid":"AllowPullFromSurflineLegacyAws","Effect":"Allow","Principal":{"AWS":"arn:aws:iam::094422976157:root"},"Action":["ecr:GetDownloadUrlForLayer","ecr:BatchGetImage","ecr:BatchCheckLayerAvailability"]}]}'
```

## Terraform Config to Update

The following files must be updated to add a service:

TODO better documentation. See `modules/ecs/services/`.

## Service Config to Update

The following files within a service must be created to support deploying to this cluster.

TODO Add better documentation

```bash
docker/serviceMaps.json
docker/{service-name}.yml
services/{service-name}/ci.json
```

## Future Plans

?

## State Management

For now keep each environment `.tfstate` in S3. Future plans could use other providers such as Artifactory. To pull state from a given environment run

```bash
make setup
```

## Modules

https://www.terraform.io/docs/modules/usage.html (caveat all resources are prefixed with `module.`)

## Further Reading

- https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/
- https://charity.wtf/2016/02/23/two-weeks-with-terraform/
- http://blog.mattiasgees.be/2015/07/29/terraform-remote-state/
- http://code.hootsuite.com/how-to-use-terraform-and-remote-state-with-s3/
