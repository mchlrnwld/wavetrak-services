locals {
  tags = {
    Company     = var.company
    Service     = "ecs"
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

data "aws_caller_identity" "current" {
}

data "template_file" "ecs_instance_role_policy" {
  template = file("${path.module}/resources/ecs-instance-role-policy.json")
  vars = {
    chef_bucket            = var.chef_bucket
    travel_page_update_sns = var.travel_page_update_sns
    article_update_sns     = var.article_update_sns
    s3_bucket_arn          = var.s3_bucket_arn
  }
}

# Setup Security Group
resource "aws_security_group" "ecs" {
  name        = "${var.company}-sg-ecs-${var.application}-${var.environment}"
  description = "Allows all traffic"
  vpc_id      = var.default_vpc

  ingress {
    from_port   = 80
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}

resource "aws_security_group" "load_balancers" {
  name        = "${var.company}-sg-lb-${var.application}-${var.environment}"
  description = "Allows all traffic"
  vpc_id      = var.default_vpc

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = var.lb_ingress_cidr_block
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.lb_ingress_cidr_block
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}

resource "aws_security_group" "client_sg_group" {
  name        = "${var.company}-${var.application}-multiclient-${var.environment}"
  description = "Client security group for RDS, Redis and Editorial RDS applications."
  vpc_id      = var.default_vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}

# Admin Tools ECS Cluster Setup
resource "aws_ecs_cluster" "main" {
  name = "${var.company}-${var.application}-${var.environment}"
}

module "autoscaling" {
  source = "./modules/autoscaling"

  company          = var.company
  application      = var.application
  environment      = var.environment
  ami_id           = var.ami_id
  instance_type    = var.instance_type
  key_name         = var.key_name
  ecs_cluster_name = aws_ecs_cluster.main.name
  vpc_security_group_ids = concat(
    [aws_security_group.ecs.id],
    [var.internal_sg_group],
    [aws_security_group.client_sg_group.id]
  )
  iam_instance_profile                = aws_iam_instance_profile.ecs.name
  availability_zones                  = var.availability_zones
  min_instance_count                  = var.min_instance_count
  max_instance_count                  = var.max_instance_count
  instance_subnets                    = var.instance_subnets
  ecs_cluster_alarm_memory_res_buffer = var.ecs_cluster_alarm_memory_res_buffer
  scale_up_cooldown_period            = var.scale_up_cooldown_period
  scale_down_cooldown_period          = var.scale_down_cooldown_period
  scaling_adjustment_up               = var.scaling_adjustment_up
  scaling_adjustment_down             = var.scaling_adjustment_down
  scaling_evaluation_periods_up       = var.scaling_evaluation_periods_up
  scaling_evaluation_periods_down     = var.scaling_evaluation_periods_down
  period_up                           = var.period_up
  period_down                         = var.period_down
  threshold_up                        = var.threshold_up
  threshold_down                      = var.threshold_down
  enabled_metrics                     = var.enabled_metrics
  chef_bucket                         = var.chef_bucket
}

# Setup IAM Roles
resource "aws_iam_role" "ecs_host_role" {
  name               = "${var.company}-ecs-host-${var.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-role-policy.json")
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
  name   = "${var.company}-ecs-instance-${var.application}-${var.environment}"
  policy = data.template_file.ecs_instance_role_policy.rendered
  role   = aws_iam_role.ecs_host_role.id
}

resource "aws_iam_role" "ecs_service_role" {
  name               = "${var.company}-ecs-service-${var.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/ecs-role-policy.json")
}

resource "aws_iam_role_policy" "ecs_service_role_policy" {
  name   = "${var.company}-ecs-service-${var.application}-${var.environment}"
  policy = file("${path.module}/resources/ecs-service-role-policy.json")
  role   = aws_iam_role.ecs_service_role.id
}

resource "aws_iam_instance_profile" "ecs" {
  name = "${var.company}-ecs-${var.application}-${var.environment}"
  path = "/"
  role = aws_iam_role.ecs_host_role.name
}

# External web proxy load balancer
resource "aws_alb" "web_proxy" {
  name            = "${var.company}-ecs-web-proxy-int-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = false
  tags            = local.tags

  access_logs {
    bucket  = var.access_logs_bucket
    prefix  = "infrastructure"
    enabled = var.access_logs_proxy_active
  }
}

# External service proxy
resource "aws_alb" "services_proxy" {
  name            = "${var.company}-ecs-services-proxy-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = false
  tags            = local.tags

  access_logs {
    bucket  = "jason-log-temp-prod"
    enabled = "true"
  }
}

# Internal core services 1 ALB
resource "aws_alb" "internal_core_services_1" {
  name            = "${var.company}-int-core-srvs-1-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = true
  tags            = local.tags

  access_logs {
    bucket  = "jason-log-temp-prod"
    enabled = "true"
  }
}

# Internal core services 2 ALB
resource "aws_alb" "internal_core_services_2" {
  name            = "${var.company}-int-core-srvs-2-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = true
  tags            = local.tags

  access_logs {
    bucket  = "jason-log-temp-prod"
    enabled = "true"
  }
}

# Internal core services 3 ALB
resource "aws_alb" "internal_core_services_3" {
  name            = "${var.company}-int-core-srvs-3-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = true
  tags            = local.tags

  access_logs {
    bucket  = "jason-log-temp-prod"
    enabled = "true"
  }
}

# Internal core services 4 ALB
resource "aws_alb" "internal_core_services_4" {
  name            = "${var.company}-int-core-srvs-4-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = true
  tags            = local.tags

  access_logs {
    bucket  = "jason-log-temp-prod"
    enabled = "true"
  }
}

# Internal core services 5 ALB
resource "aws_alb" "internal_core_services_5" {
  name            = "${var.company}-int-core-srvs-5-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.load_balancers.id]
  internal        = true
  tags            = local.tags

  access_logs {
    bucket  = "jason-log-temp-prod"
    enabled = "true"
  }
}

# Science models DB security group
resource "aws_security_group" "science_models_db_lb" {
  count       = var.environment != "staging" ? 1 : 0
  name        = "wt-sg-lb-science-models-db-${var.environment}"
  description = "Allows all traffic"
  vpc_id      = var.default_vpc

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Company     = "wt"
    Service     = "ecs"
    Application = "science-models-db"
    Environment = var.environment
    Terraform   = true
  }
}

# Science models DB ALB
resource "aws_alb" "science_models_db" {
  count           = var.environment != "staging" ? 1 : 0
  name            = "wt-science-models-db-${var.environment}"
  subnets         = var.lb_subnets
  security_groups = [aws_security_group.science_models_db_lb[count.index].id]
  internal        = true

  tags = {
    Company     = "wt"
    Service     = "ecs"
    Application = "science-models-db"
    Environment = var.environment
    Terraform   = true
  }
}

# Science Models DB listener
resource "aws_alb_listener" "science_models_db" {
  count             = var.environment != "staging" ? 1 : 0
  load_balancer_arn = aws_alb.science_models_db[count.index].arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not Found"
      status_code  = 404
    }
  }
}

# Logs
resource "aws_cloudwatch_log_group" "main" {
  name              = "${var.company}-logs-${var.application}-${var.environment}"
  retention_in_days = "3"
}

resource "aws_route53_record" "internal_product_services" {
  zone_id = var.internal_dns_zone_id
  name    = var.internal_dns_zone_product
  type    = "A"

  alias {
    name                   = aws_alb.internal_core_services_1.dns_name
    zone_id                = aws_alb.internal_core_services_1.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "internal_search_service" {
  zone_id = var.internal_dns_zone_id
  name    = var.internal_dns_zone_search
  type    = "A"

  alias {
    name                   = aws_alb.internal_core_services_1.dns_name
    zone_id                = aws_alb.internal_core_services_1.zone_id
    evaluate_target_health = true
  }
}

# Tech debt from product-svc cluster removal
resource "aws_cloudwatch_log_group" "product_logs" {
  name              = "${var.company}-logs-product-svc-${var.environment}"
  retention_in_days = 3
}

# Core services ALB listeners
resource "aws_alb_listener" "internal_core_services_1_http" {
  load_balancer_arn = aws_alb.internal_core_services_1.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

resource "aws_alb_listener" "internal_core_services_2_http" {
  load_balancer_arn = aws_alb.internal_core_services_2.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

resource "aws_alb_listener" "internal_core_services_3_http" {
  load_balancer_arn = aws_alb.internal_core_services_3.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

resource "aws_alb_listener" "internal_core_services_3_https" {
  load_balancer_arn = aws_alb.internal_core_services_3.arn
  certificate_arn   = var.editorial_ssl_certificate_arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

resource "aws_alb_listener" "internal_core_services_4_http" {
  load_balancer_arn = aws_alb.internal_core_services_4.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

resource "aws_alb_listener" "internal_core_services_5_http" {
  load_balancer_arn = aws_alb.internal_core_services_5.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

# Services proxy HTTP listener
resource "aws_alb_listener" "services_proxy_http" {
  load_balancer_arn = aws_alb.services_proxy.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

# Services proxy HTTPS listener
resource "aws_alb_listener" "services_proxy_https" {
  load_balancer_arn = aws_alb.services_proxy.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = var.service_proxy_ssl_certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

# Web proxy HTTP ALB listener
resource "aws_alb_listener" "web_proxy_http" {
  load_balancer_arn = aws_alb.web_proxy.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

# Web proxy HTTPS ALB listener
resource "aws_alb_listener" "web_proxy_https" {
  load_balancer_arn = aws_alb.web_proxy.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = var.web_proxy_default_ssl_certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OK"
      status_code  = "200"
    }
  }
}

# Add additional SSL certs for web proxy
resource "aws_lb_listener_certificate" "surfline_admin_cert" {
  listener_arn    = aws_alb_listener.web_proxy_https.arn
  certificate_arn = var.surfline_admin_ssl_certificate_arn
}

resource "aws_lb_listener_certificate" "coastalwatch_cert" {
  listener_arn    = aws_alb_listener.web_proxy_https.arn
  certificate_arn = var.coastalwatch_ssl_certificate_arn
}

resource "aws_lb_listener_certificate" "surf2surf_cert" {
  listener_arn    = aws_alb_listener.web_proxy_https.arn
  certificate_arn = var.surf2surf_ssl_certificate_arn
}
