provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-core-svc/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-core-services" {
  source = "../../"

  company     = "sl"
  application = "core-svc"
  environment = "sandbox"

  ecs_cluster_name = "sl-ecs-core-svc-sandbox"

  chef_bucket            = "sl-chef-sbox"
  travel_page_update_sns = "arn:aws:sns:us-west-1:665294954271:sl-editorial-cms-travel-page-update-sns_sandbox"
  article_update_sns     = "arn:aws:sns:us-west-1:665294954271:sl-editorial-cms-article-update-sns_sandbox"
  s3_bucket_arn          = "arn:aws:s3:::sl-editorial-cms-wp-content-sandbox"

  default_vpc = "vpc-981887fd"
  availability_zones = [
    "us-west-1b",
    "us-west-1c",
  ]
  instance_type = "m5.4xlarge"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab",
  ]
  lb_subnets = [
    "subnet-f4d458ad",
    "subnet-0b09466e",
  ]
  lb_ingress_cidr_block = [
    "0.0.0.0/0"
  ]

  internal_sg_group = "sg-90aeaef5"

  # ssl_certificate_arn => *.sandbox.surfline.com
  ssl_certificate_arn                   = "arn:aws:acm:us-west-1:665294954271:certificate/5c0161f4-e15a-4ee8-8de3-bd19053d0e24"
  editorial_ssl_certificate_arn         = "arn:aws:acm:us-west-1:665294954271:certificate/b5d003ab-5256-4737-9333-c07e7f817a98"
  service_proxy_ssl_certificate_arn     = "arn:aws:acm:us-west-1:665294954271:certificate/5c0161f4-e15a-4ee8-8de3-bd19053d0e24"
  web_proxy_default_ssl_certificate_arn = "arn:aws:acm:us-west-1:665294954271:certificate/5c0161f4-e15a-4ee8-8de3-bd19053d0e24"
  coastalwatch_ssl_certificate_arn      = "arn:aws:acm:us-west-1:665294954271:certificate/9620832e-1484-492d-a2e2-a8249fed9825"
  surf2surf_ssl_certificate_arn         = "arn:aws:acm:us-west-1:665294954271:certificate/56fa647a-5d6a-43ee-ba03-55788abd9644"
  surfline_admin_ssl_certificate_arn    = "arn:aws:acm:us-west-1:665294954271:certificate/255fdbc4-4251-40b8-b1ff-e069dccc8a4d"

  td_count           = 2
  min_instance_count = 3
  max_instance_count = 8

  key_name                  = "sturdy-surfline-dev"
  dereg_topic               = "arn:aws:sns:us-west-1:665294954271:Sturdy-SAND"
  task_deregistration_delay = 3

  access_logs_bucket         = "wt-data-lake-dev"
  access_logs_proxy_active   = false
  access_logs_service_active = true

  # Route53 Zone ID - sandbox.surfline.com
  dns_zone_id               = "Z3DM6R3JR1RYXV"
  dns_name                  = "services.sandbox.surfline.com"
  internal_dns_zone_id      = "Z3DM6R3JR1RYXV"
  internal_dns_zone_name    = "sandbox.surfline.com"
  internal_dns_zone_product = "internal-product-svc.sandbox.surfline.com"
  internal_dns_zone_search  = "internal-search.sandbox.surfline.com"

  # Cloudwatch cpu alarm variables
  ecs_cluster_alarm_cpu_util_high_threshold = 20
  ecs_cluster_alarm_cpu_util_low_threshold  = ".01"
  ecs_cluster_alarm_cpu_res_high_threshold  = 75
  ecs_cluster_alarm_cpu_res_low_threshold   = 25

  # Cloudwatch memory alarm variables
  ecs_cluster_alarm_memory_util_high_threshold = 20
  ecs_cluster_alarm_memory_util_low_threshold  = 1
  ecs_cluster_alarm_memory_res_high_threshold  = 80
  ecs_cluster_alarm_memory_res_low_threshold   = 50

  scale_up_cooldown_period        = 900
  scale_down_cooldown_period      = 900
  scaling_adjustment_up           = 1
  scaling_adjustment_down         = "-1"
  scaling_evaluation_periods_up   = 2
  scaling_evaluation_periods_down = 2

  period_up      = 300
  period_down    = 300
  threshold_up   = 45
  threshold_down = 45

  # Cloudwatch enabled metrics
  enabled_metrics = []

  #percentage adjustment on host scaling algorithm: increase to reduce headroom before scaling
  #sbox/staging up to 0.1 to tighten up free space before scaling by 10%
  #prod 0.0 for safety
  ecs_cluster_alarm_memory_res_buffer = "-0.05"
}
