provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-core-svc/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-core-services" {
  source = "../../"

  company     = "sl"
  application = "core-svc"
  environment = "prod"

  ecs_cluster_name = "sl-ecs-core-svc-prod"

  chef_bucket            = "sl-chef-prod"
  travel_page_update_sns = "arn:aws:sns:us-west-1:833713747344:sl-editorial-cms-travel-page-update-sns_prod"
  article_update_sns     = "arn:aws:sns:us-west-1:833713747344:sl-editorial-cms-article-update-sns_prod"
  s3_bucket_arn          = "arn:aws:s3:::sl-editorial-cms-wp-content-prod"

  default_vpc = "vpc-116fdb74"
  availability_zones = [
    "us-west-1b",
    "us-west-1c",
  ]
  instance_type = "m5.4xlarge"
  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0",
  ]
  lb_subnets = [
    "subnet-debb6987",
    "subnet-baab36df",
  ]
  lb_ingress_cidr_block = [
    "0.0.0.0/0"
  ]

  internal_sg_group = "sg-a5a0e5c0"

  # using multibrand-toplevel-wildcard-cert
  # apex and wildcards for surfline.com, buoyweather.com and fishtrack.com
  ssl_certificate_arn                   = "arn:aws:acm:us-west-1:833713747344:certificate/f6268a81-8d22-4928-b244-fa589b0c3bca"
  editorial_ssl_certificate_arn         = "arn:aws:acm:us-west-1:833713747344:certificate/4c4df1b0-55b5-46e7-8cb6-9ac5d5e1bd2c"
  service_proxy_ssl_certificate_arn     = "arn:aws:acm:us-west-1:833713747344:certificate/f6268a81-8d22-4928-b244-fa589b0c3bca"
  web_proxy_default_ssl_certificate_arn = "arn:aws:acm:us-west-1:833713747344:certificate/f6268a81-8d22-4928-b244-fa589b0c3bca"
  coastalwatch_ssl_certificate_arn      = "arn:aws:acm:us-west-1:833713747344:certificate/0e1e36b0-e10e-4e4e-a1b4-2370f0ee5590"
  surf2surf_ssl_certificate_arn         = "arn:aws:acm:us-west-1:833713747344:certificate/671b047c-4b16-460f-8bf6-4afd25a63283"
  surfline_admin_ssl_certificate_arn    = "arn:aws:acm:us-west-1:833713747344:certificate/894896bb-9391-4040-9fd3-4455c3aba139"

  td_count           = 2
  min_instance_count = 5
  max_instance_count = 650

  key_name                  = "sturdy_surfline_id_rsa"
  dereg_topic               = "arn:aws:sns:us-west-1:833713747344:Sturdy-SAND"
  task_deregistration_delay = 3

  access_logs_bucket         = "wt-data-lake-prod"
  access_logs_proxy_active   = true
  access_logs_service_active = false

  # Route53 Zone ID - surfline.com
  # FIXME: Change to prod.surfline.com when reqs routed through common-proxy
  dns_zone_id               = "Z2TX51CNR2T618"
  dns_name                  = "services.surfline.com"
  internal_dns_zone_id      = "ZY7MYOQ65TY5X"
  internal_dns_zone_name    = "surfline.com"
  internal_dns_zone_product = "internal-product-svc.surfline.com"
  internal_dns_zone_search  = "internal-search.surfline.com"

  # Cloudwatch cpu alarm variables
  ecs_cluster_alarm_cpu_util_high_threshold = 20
  ecs_cluster_alarm_cpu_util_low_threshold  = ".1"
  ecs_cluster_alarm_cpu_res_high_threshold  = 75
  ecs_cluster_alarm_cpu_res_low_threshold   = 25

  # Cloudwatch memory alarm variables
  ecs_cluster_alarm_memory_util_high_threshold = 20
  ecs_cluster_alarm_memory_util_low_threshold  = 1
  ecs_cluster_alarm_memory_res_high_threshold  = 80
  ecs_cluster_alarm_memory_res_low_threshold   = 50

  scale_up_cooldown_period        = 60
  scale_down_cooldown_period      = 900
  scaling_adjustment_up           = 2
  scaling_adjustment_down         = "-1"
  scaling_evaluation_periods_up   = 1
  scaling_evaluation_periods_down = 2

  period_up      = 120
  period_down    = 300
  threshold_up   = 45
  threshold_down = 33

  # Cloudwatch enabled metrics
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances"
  ]

  #percentage adjustment on host scaling algorithm: increase to reduce headroom before scaling
  #sbox/staging up to 0.1 to tighten up headroom before scaling by 10%
  #prod 0.0 to -0.1 for extra headroom
  ecs_cluster_alarm_memory_res_buffer = "-0.5"
}

resource "aws_lb_listener_certificate" "surfline_beachlive_cert" {
  listener_arn    = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-ecs-web-proxy-int-prod/019f8b7d648a84af/3e78aa4f38b70e9b"
  certificate_arn = "arn:aws:acm:us-west-1:833713747344:certificate/849bc3ba-677e-41a2-9030-377832f5ed6f"
}
