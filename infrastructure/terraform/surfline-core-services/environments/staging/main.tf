provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-core-svc/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-core-services" {
  source = "../../"

  company     = "sl"
  application = "core-svc"
  environment = "staging"

  ecs_cluster_name = "sl-ecs-core-svc-staging"

  chef_bucket            = "sl-chef-staging"
  travel_page_update_sns = "arn:aws:sns:us-west-1:665294954271:sl-editorial-cms-travel-page-update-sns_staging"
  article_update_sns     = "arn:aws:sns:us-west-1:665294954271:sl-editorial-cms-article-update-sns_staging"
  s3_bucket_arn          = "arn:aws:s3:::sl-editorial-cms-wp-content-staging"

  default_vpc = "vpc-981887fd"
  availability_zones = [
    "us-west-1b",
    "us-west-1c",
  ]
  instance_type = "m5.4xlarge"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab",
  ]
  lb_subnets = [
    "subnet-f4d458ad",
    "subnet-0b09466e",
  ]
  lb_ingress_cidr_block = [
    "0.0.0.0/0"
  ]

  internal_sg_group = "sg-90aeaef5"

  # ssl_certificate_arn => *.staging.surfline.com
  ssl_certificate_arn                   = "arn:aws:acm:us-west-1:665294954271:certificate/a926595c-f282-4435-be22-3cfaf907b459"
  editorial_ssl_certificate_arn         = "arn:aws:acm:us-west-1:665294954271:certificate/a926595c-f282-4435-be22-3cfaf907b459"
  service_proxy_ssl_certificate_arn     = "arn:aws:acm:us-west-1:665294954271:certificate/a926595c-f282-4435-be22-3cfaf907b459"
  web_proxy_default_ssl_certificate_arn = "arn:aws:acm:us-west-1:665294954271:certificate/a926595c-f282-4435-be22-3cfaf907b459"
  coastalwatch_ssl_certificate_arn      = "arn:aws:acm:us-west-1:665294954271:certificate/9620832e-1484-492d-a2e2-a8249fed9825"
  surf2surf_ssl_certificate_arn         = "arn:aws:acm:us-west-1:665294954271:certificate/b5c85fc7-e938-40b4-8ba7-6c191f84de14"
  surfline_admin_ssl_certificate_arn    = "arn:aws:acm:us-west-1:665294954271:certificate/9aaed19c-da20-4923-a013-80eb485efc46"

  td_count           = 2
  min_instance_count = 2
  max_instance_count = 8

  key_name                  = "sturdy-surfline-dev"
  dereg_topic               = "arn:aws:sns:us-west-1:665294954271:Sturdy-SAND"
  task_deregistration_delay = 3

  access_logs_bucket         = "wt-data-lake-dev"
  access_logs_proxy_active   = false
  access_logs_service_active = true

  # Route53 Zone ID - staging.surfline.com
  dns_zone_id               = "Z3JHKQ8ELQG5UE"
  dns_name                  = "services.staging.surfline.com"
  internal_dns_zone_id      = "Z3JHKQ8ELQG5UE"
  internal_dns_zone_name    = "staging.surfline.com"
  internal_dns_zone_product = "internal-product-svc.staging.surfline.com"
  internal_dns_zone_search  = "internal-search.staging.surfline.com"

  # Cloudwatch cpu alarm variables
  ecs_cluster_alarm_cpu_util_high_threshold = 20
  ecs_cluster_alarm_cpu_util_low_threshold  = ".01"
  ecs_cluster_alarm_cpu_res_high_threshold  = 75
  ecs_cluster_alarm_cpu_res_low_threshold   = 25

  # Cloudwatch memory alarm variables
  ecs_cluster_alarm_memory_util_high_threshold = 20
  ecs_cluster_alarm_memory_util_low_threshold  = 1
  ecs_cluster_alarm_memory_res_high_threshold  = 80
  ecs_cluster_alarm_memory_res_low_threshold   = 50

  scale_up_cooldown_period        = 900
  scale_down_cooldown_period      = 900
  scaling_adjustment_up           = 1
  scaling_adjustment_down         = "-1"
  scaling_evaluation_periods_up   = 2
  scaling_evaluation_periods_down = 2

  period_up      = 300
  period_down    = 300
  threshold_up   = 45
  threshold_down = 45

  # Cloudwatch enabled metrics
  enabled_metrics = []

  #percentage adjustment on host scaling algorithm: increase to reduce headroom before scaling
  #sbox/staging up to 0.1 to tighten up free space before scaling by 10%
  #prod 0.0 for safety
  ecs_cluster_alarm_memory_res_buffer = "0.00"
}
