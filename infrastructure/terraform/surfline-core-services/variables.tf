variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "ecs_cluster_name" {
}

variable "ami_id" {
  default = "ami-0667a9cc6a93f50fe"
}

variable "chef_bucket" {
}

variable "travel_page_update_sns" {
}

variable "article_update_sns" {
}

variable "s3_bucket_arn" {
}

variable "default_vpc" {
}

variable "availability_zones" {
}

variable "internal_sg_group" {
}

variable "instance_subnets" {
}

variable "lb_subnets" {
}

variable "ssl_certificate_arn" {
}

variable "editorial_ssl_certificate_arn" {
}

variable "service_proxy_ssl_certificate_arn" {
}

variable "web_proxy_default_ssl_certificate_arn" {
}

variable "coastalwatch_ssl_certificate_arn" {
}

variable "surf2surf_ssl_certificate_arn" {
}

variable "surfline_admin_ssl_certificate_arn" {
}

variable "key_name" {
}

variable "access_logs_bucket" {
}

variable "access_logs_proxy_active" {
}

variable "access_logs_service_active" {
}

variable "lb_ingress_cidr_block" {
}

variable "instance_type" {
}

variable "min_instance_count" {
}

variable "max_instance_count" {
}

variable "td_count" {
}

variable "task_deregistration_delay" {
}

variable "scaling_evaluation_periods_up" {
}

variable "scaling_evaluation_periods_down" {
}

variable "period_up" {
}

variable "period_down" {
}

variable "threshold_up" {
}

variable "threshold_down" {
}

# ARN for SAND
variable "dereg_topic" {
}

# External LB Route53 Zone ID
variable "dns_zone_id" {
}

# External LB Route53 DNS Name
variable "dns_name" {
}

# Internal LB Route53 Zone ID
variable "internal_dns_zone_id" {
}

# Internal DNS Zone Name
variable "internal_dns_zone_name" {
}

variable "internal_dns_zone_product" {
}

variable "internal_dns_zone_search" {
}

variable "reports_api_dns_record" {
  default = "reports-api.staging.surfline.com"
}

variable "spots_api_dns_record" {
  default = "spots-api.staging.surfline.com"
}

variable "sns_spot_details_updated_policy" {
  default = "arn:aws:iam::665294954271:policy/sns_policy_spot_details_updated_staging"
}

variable "sns_spot_report_updated_policy" {
  default = "arn:aws:iam::665294954271:policy/sns_policy_spot_report_updated_staging"
}

variable "image_service_execute_policy" {
  default = "arn:aws:iam::665294954271:policy/image-service-execute-staging"
}

variable "spot_thumbnail_policy" {
  default = "arn:aws:iam::665294954271:policy/spot-thumbnail-policy-staging"
}

variable "reports_api_td_count" {
  default = 2
}

variable "reports_api_td_count_max" {
  default = 6
}

variable "spots_api_td_count" {
  default = 4
}

variable "spots_api_td_count_max" {
  default = 8
}

variable "ecs_cluster_alarm_cooldown" {
  default = 2700
}

# Cloudwatch cpu alarm variables
variable "ecs_cluster_alarm_cpu_util_high_eval_periods" {
  default = 5
}

variable "ecs_cluster_alarm_cpu_util_high_period" {
  default = 60
}

variable "ecs_cluster_alarm_cpu_util_high_threshold" {
  default = 80
}

variable "ecs_cluster_alarm_cpu_util_low_eval_periods" {
  default = 30
}

variable "ecs_cluster_alarm_cpu_util_low_period" {
  default = 60
}

variable "ecs_cluster_alarm_cpu_util_low_threshold" {
  default = 0
}

variable "ecs_cluster_alarm_cpu_res_high_eval_periods" {
  default = 5
}

variable "ecs_cluster_alarm_cpu_res_high_period" {
  default = 60
}

variable "ecs_cluster_alarm_cpu_res_high_threshold" {
  default = 95
}

variable "ecs_cluster_alarm_cpu_res_low_eval_periods" {
  default = 30
}

variable "ecs_cluster_alarm_cpu_res_low_period" {
  default = 60
}

variable "ecs_cluster_alarm_cpu_res_low_threshold" {
  default = 1
}

# Cloudwatch memory alarm variables
variable "ecs_cluster_alarm_memory_util_high_eval_periods" {
  default = 5
}

variable "ecs_cluster_alarm_memory_util_high_period" {
  default = 60
}

variable "ecs_cluster_alarm_memory_util_high_threshold" {
  default = 80
}

variable "ecs_cluster_alarm_memory_util_low_eval_periods" {
  default = 30
}

variable "ecs_cluster_alarm_memory_util_low_period" {
  default = 60
}

variable "ecs_cluster_alarm_memory_util_low_threshold" {
  default = 1
}

variable "ecs_cluster_alarm_memory_res_high_eval_periods" {
  default = 5
}

variable "ecs_cluster_alarm_memory_res_high_period" {
  default = 60
}

variable "ecs_cluster_alarm_memory_res_high_threshold" {
  default = 100
}

variable "ecs_cluster_alarm_memory_res_low_eval_periods" {
  default = 30
}

variable "ecs_cluster_alarm_memory_res_low_period" {
  default = 60
}

variable "ecs_cluster_alarm_memory_res_low_threshold" {
  default = 1
}

variable "enabled_metrics" {
}

# Reports API ALB alarm variables
variable "reports_api_alb_svc_target_resp_high_eval_periods" {
  default = 1
}

variable "reports_api_alb_svc_target_resp_high_period" {
  default = 60
}

variable "reports_api_alb_svc_target_resp_high_threshold" {
  default = ".1"
}

variable "reports_api_alb_svc_target_resp_low_eval_periods" {
  default = 1
}

variable "reports_api_alb_svc_target_resp_low_period" {
  default = 60
}

variable "reports_api_alb_svc_target_resp_low_threshold" {
  default = ".05"
}

variable "reports_api_td_scale_down_cooldown" {
  default = 2700
}

variable "reports_api_memory_util_scale_up_metric" {
  default = 80
}

variable "reports_api_memory_util_scale_down_metric" {
  default = 10
}

# Spots API ALB alarm variables
variable "spots_api_alb_svc_target_resp_high_eval_periods" {
  default = 1
}

variable "spots_api_alb_svc_target_resp_high_period" {
  default = 60
}

variable "spots_api_alb_svc_target_resp_high_threshold" {
  default = ".1"
}

variable "spots_api_alb_svc_target_resp_low_eval_periods" {
  default = 1
}

variable "spots_api_alb_svc_target_resp_low_period" {
  default = 60
}

variable "spots_api_alb_svc_target_resp_low_threshold" {
  default = ".01"
}

variable "spots_api_td_scale_down_cooldown" {
  default = 2700
}

variable "spots_api_memory_util_scale_up_metric" {
  default = 80
}

variable "spots_api_memory_util_scale_down_metric" {
  default = 10
}

variable "scale_up_cooldown_period" {
  default = 60
}

variable "scale_down_cooldown_period" {
  default = 900
}

variable "scaling_adjustment_up" {
  default = 2
}

variable "scaling_adjustment_down" {
  default = "-1"
}

#ECS Host Scaling overhead allowance

variable "ecs_cluster_alarm_memory_res_buffer" {
  default = 0
}
