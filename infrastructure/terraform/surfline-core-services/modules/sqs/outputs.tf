output "sqs_queue" {
  value = aws_sqs_queue.queue.id
}

output "sqs_queue_policy" {
  value = aws_iam_policy.policy.id
}
