variable "service_name" {
}

variable "service_lb_path" {
}

variable "service_lb_healthcheck_path" {
}

variable "service_port" {
}

variable "service_td_name" {
}

variable "service_td_container_name" {
}

variable "service_td_count" {
}

variable "alb_listener" {
}

variable "alb_target_healthy_threshold" {
  default = 5
}

variable "alb_target_unhealthy_threshold" {
  default = 2
}

variable "target_group_name" {
  default = ""
}

variable "service_alb_priority" {
}

# probably inherited
variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "default_vpc" {
}

variable "task_deregistration_delay" {
}

variable "iam_role_policy" {
}

variable "ecs_cluster" {
}

#Required
variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_min_size" {
  default = 2
}

variable "auto_scaling_max_size" {
  default = 5
}

variable "auto_scaling_scale_by" {
  default     = "memory_utilization"
  description = "Metric to scale by ( cpu_utilization, memory_utilization, alb_response_latency, schedule )"
}

#Used to override defaults
variable "auto_scaling_up_metric" {
  default = -1
}

variable "auto_scaling_down_metric" {
  default = -1
}

#Used to override scaling defaults
variable "auto_scaling_cooldown" {
  default = 120
}

variable "auto_scaling_evaluation_period" {
  default = 120
}

#required for use with ALB autoscaling
variable "auto_scaling_alb_arn_suffix" {
  default = ""
}

#required for use with scheduled scaling
variable "auto_scaling_schedule_scale_up" {
  description = "schedule in the format cron() or at()"
  default     = ""
}

variable "auto_scaling_schedule_scale_down" {
  description = "schedule in the format cron() or at() "
  default     = ""
}

variable "scale_data" {
  type = map(string)
  default = {
    "cpu_utilization.namespace"        = "ECS"
    "cpu_utilization.metric"           = "CPUUtilization"
    "cpu_utilization.down_metric"      = "2"
    "cpu_utilization.up_metric"        = "70"
    "memory_utilization.namespace"     = "ECS"
    "memory_utilization.metric"        = "MemoryUtilization"
    "memory_utilization.down_metric"   = "10"
    "memory_utilization.up_metric"     = "60"
    "alb_response_latency.namespace"   = "ApplicationELB"
    "alb_response_latency.metric"      = "TargetResponseTime"
    "alb_response_latency.down_metric" = "0.01"
    "alb_response_latency.up_metric"   = "0.75"
    "schedule.namespace"               = ""
    "schedule.metric"                  = ""
    "schedule.up_metric"               = ""
    "schedule.down_metric"             = ""
  }
}
