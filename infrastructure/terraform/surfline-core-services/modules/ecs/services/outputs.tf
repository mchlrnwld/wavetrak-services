output "target_group" {
  value = aws_alb_target_group.service-target-group.id
}

output "arn_suffix" {
  value = aws_alb_target_group.service-target-group.arn_suffix
}

output "name" {
  value = var.service_name
}
