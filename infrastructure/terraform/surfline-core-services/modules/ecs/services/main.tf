data "aws_ecs_task_definition" "service_latest_td" {
  task_definition = var.service_td_name
}

resource "aws_alb_target_group" "service-target-group" {
  name                 = "${var.company}-${coalesce(var.target_group_name, var.service_name)}-${var.application}-${var.environment}"
  port                 = var.service_port
  protocol             = "HTTP"
  vpc_id               = var.default_vpc
  deregistration_delay = var.task_deregistration_delay

  health_check {
    path                = var.service_lb_healthcheck_path
    healthy_threshold   = var.alb_target_healthy_threshold
    unhealthy_threshold = var.alb_target_unhealthy_threshold
  }

  tags = {
    Company     = var.company
    Service     = "ecs"
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_alb_listener_rule" "service-alb-listener" {
  listener_arn = var.alb_listener
  priority     = var.service_alb_priority

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.service-target-group.arn
  }

  condition {
    path_pattern {
      values = [var.service_lb_path]
    }
  }

  depends_on = [
    aws_alb_target_group.service-target-group
  ]
}

resource "aws_ecs_service" "service-ecs-service" {
  name            = "${var.company}-${var.service_name}-${var.application}-${var.environment}"
  cluster         = var.ecs_cluster
  task_definition = "${data.aws_ecs_task_definition.service_latest_td.family}:${data.aws_ecs_task_definition.service_latest_td.revision}"
  iam_role        = var.iam_role_policy
  desired_count   = var.service_td_count

  load_balancer {
    target_group_arn = aws_alb_target_group.service-target-group.arn
    container_name   = var.service_td_container_name
    container_port   = var.service_port
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  depends_on = [
    aws_alb_target_group.service-target-group
  ]
}
