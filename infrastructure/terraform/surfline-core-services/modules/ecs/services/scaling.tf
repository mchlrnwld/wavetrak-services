data "aws_caller_identity" "current" {
}

resource "aws_appautoscaling_target" "ecs_target" {
  count              = var.auto_scaling_enabled ? 1 : 0
  max_capacity       = var.auto_scaling_max_size
  min_capacity       = var.auto_scaling_min_size
  resource_id        = "service/${basename(var.ecs_cluster)}/${var.company}-${var.service_name}-${var.application}-${var.environment}"
  role_arn           = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/ecs.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_ECSService"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_up" {
  count              = var.auto_scaling_enabled && var.auto_scaling_scale_by != "schedule" ? 1 : 0
  name               = "${var.company}-${var.service_name}-${var.application}-${var.environment}-scaling-up"
  service_namespace  = "ecs"
  resource_id        = "service/${basename(var.ecs_cluster)}/${var.company}-${var.service_name}-${var.application}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.auto_scaling_cooldown
    metric_aggregation_type = "Average"
    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
  depends_on = [aws_appautoscaling_target.ecs_target]
}

resource "aws_appautoscaling_policy" "ecs_down" {
  count              = var.auto_scaling_enabled && var.auto_scaling_scale_by != "schedule" ? 1 : 0
  name               = "${var.company}-${var.service_name}-${var.application}-${var.environment}-scaling-down"
  service_namespace  = "ecs"
  resource_id        = "service/${basename(var.ecs_cluster)}/${var.company}-${var.service_name}-${var.application}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.auto_scaling_cooldown
    metric_aggregation_type = "Average"
    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
  depends_on = [aws_appautoscaling_target.ecs_target]
}

#ECS metric Alarms
resource "aws_cloudwatch_metric_alarm" "ecs_service_alarm_up" {
  count               = var.auto_scaling_enabled && var.auto_scaling_scale_by != "schedule" && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ECS" ? 1 : 0
  alarm_name          = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ECS"
  statistic           = "Average"
  period              = var.auto_scaling_evaluation_period
  threshold           = var.auto_scaling_up_metric == "-1" ? var.scale_data["${var.auto_scaling_scale_by}.up_metric"] : var.auto_scaling_up_metric

  dimensions = {
    ClusterName = basename(var.ecs_cluster)
    ServiceName = "${var.company}-${var.service_name}-${var.application}-${var.environment}"
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_up[0].arn
  ]
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_alarm_down" {
  count               = var.auto_scaling_enabled && var.auto_scaling_scale_by != "schedule" && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ECS" ? 1 : 0
  alarm_name          = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ECS"
  statistic           = "Average"
  period              = var.auto_scaling_evaluation_period
  threshold           = var.auto_scaling_down_metric == "-1" ? var.scale_data["${var.auto_scaling_scale_by}.down_metric"] : var.auto_scaling_down_metric

  dimensions = {
    ClusterName = basename(var.ecs_cluster)
    ServiceName = "${var.company}-${var.service_name}-${var.application}-${var.environment}"
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_down[0].arn,
  ]
}

#ALB Alarms
resource "aws_cloudwatch_metric_alarm" "alb_alarm_up" {
  count               = var.auto_scaling_enabled && var.auto_scaling_scale_by != "schedule" && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ApplicationELB" ? 1 : 0
  alarm_name          = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ApplicationELB"
  statistic           = "Average"
  period              = var.auto_scaling_evaluation_period
  threshold           = var.auto_scaling_up_metric == "-1" ? var.scale_data["${var.auto_scaling_scale_by}.up_metric"] : var.auto_scaling_up_metric

  dimensions = {
    TargetGroup  = aws_alb_target_group.service-target-group.arn_suffix
    LoadBalancer = var.auto_scaling_alb_arn_suffix
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_up[0].arn,
  ]
}

resource "aws_cloudwatch_metric_alarm" "alb_alarm_down" {
  count               = var.auto_scaling_enabled && var.auto_scaling_scale_by != "schedule" && var.scale_data["${var.auto_scaling_scale_by}.namespace"] == "ApplicationELB" ? 1 : 0
  alarm_name          = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = var.scale_data["${var.auto_scaling_scale_by}.metric"]
  namespace           = "AWS/ApplicationELB"
  statistic           = "Average"
  period              = var.auto_scaling_evaluation_period
  threshold           = var.auto_scaling_down_metric == "-1" ? var.scale_data["${var.auto_scaling_scale_by}.down_metric"] : var.auto_scaling_down_metric

  dimensions = {
    TargetGroup  = aws_alb_target_group.service-target-group.arn_suffix
    LoadBalancer = var.auto_scaling_alb_arn_suffix
  }

  alarm_actions = [
    aws_appautoscaling_policy.ecs_down[0].arn,
  ]
}

resource "aws_appautoscaling_scheduled_action" "scheduled_scale_up" {
  count              = var.auto_scaling_enabled && var.auto_scaling_scale_by == "schedule" ? 1 : 0
  name               = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}-scheduled-scale-up"
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension
  schedule           = var.auto_scaling_schedule_scale_up

  scalable_target_action {
    min_capacity = var.auto_scaling_max_size
    max_capacity = var.auto_scaling_max_size
  }
}

resource "aws_appautoscaling_scheduled_action" "scheduled_scale_down" {
  count              = var.auto_scaling_enabled && var.auto_scaling_scale_by == "schedule" ? 1 : 0
  name               = "${var.company}-${var.service_name}-${var.application}-${var.environment}-${var.auto_scaling_scale_by}-scheduled-scale-down"
  service_namespace  = aws_appautoscaling_target.ecs_target[0].service_namespace
  resource_id        = aws_appautoscaling_target.ecs_target[0].resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target[0].scalable_dimension
  schedule           = var.auto_scaling_schedule_scale_down

  scalable_target_action {
    min_capacity = var.auto_scaling_min_size
    max_capacity = var.auto_scaling_min_size
  }

  depends_on = [aws_appautoscaling_scheduled_action.scheduled_scale_up]
}
