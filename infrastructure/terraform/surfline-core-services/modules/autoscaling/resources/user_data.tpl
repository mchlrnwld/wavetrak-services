#cloud-config
repo_releasever: 2017.09
repo_update: true
repo_upgrade: security
packages:
  - aws-cli
  - jq
  - lsof
  - ncdu
  - python-boto3
write_files:
  - path: /etc/chef/rename_instance
    content: |
      #!/usr/bin/env python

      import json
      import subprocess

      import boto3
      import requests

      print "[INFO] Getting instance identity"
      metadata_url = ('http://169.254.169.254'
                      '/latest/dynamic/instance-identity/document')
      inst_data = requests.get(metadata_url).json()

      print "[INFO] Getting tags for current instance"
      ec2 = boto3.resource('ec2', region_name=inst_data['region'])
      inst = ec2.Instance(inst_data['instanceId'])

      tags = {tag['Key']: tag['Value'] for tag in inst.tags}

      inst_type = tags.get('type', 'core-svc-ecs-host')
      inst_environment = tags.get('Environment', 'dev')

      print "[INFO] Generating instance name"
      short_id_hash = inst_data['instanceId'][2:10]
      name = "{}-{}-{}".format(inst_environment, inst_type, short_id_hash)

      # Ensure the generated name with hash doesn't exceed 63 chars
      short_name = name[:63]

      print "[INFO] Renaming instance to {}".format(short_name)
      inst.create_tags(Tags=[{'Key': 'Name', 'Value': short_name}])
      poutput = subprocess.check_output('hostname ' + name, shell=True)
      sed_cmd = ("sed -i 's/HOSTNAME=.*/HOSTNAME={}/' "
                 "/etc/sysconfig/network").format(name)
      poutput = subprocess.check_output(sed_cmd, shell=True)

  - path: /etc/cron.d/cleanup
    content: |
      */5 * * * * docker rm $(docker ps -aq) >/dev/null 2>&1
      */5 * * * * docker rmi $(docker images -q) >/dev/null 2>&1
  - path: /etc/ecs/ecs.config
    content: |
      ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=5m
      ECS_IMAGE_CLEANUP_INTERVAL=10m
      ECS_IMAGE_MINIMUM_CLEANUP_AGE=15m
      ECS_NUM_IMAGES_DELETE_PER_CYCLE=20
      ECS_CLUSTER=${company}-${application}-${environment}
      ECS_AVAILABLE_LOGGING_DRIVERS=["json-file","awslogs"]
      ECS_IMAGE_PULL_BEHAVIOR=once
      ECS_ENABLE_SPOT_INSTANCE_DRAINING=true
  - path: /etc/chef/sync_cookbooks.sh
    permissions: '0755'
    owner: 'root'
    group: 'root'
    content: |
      #!/bin/bash
      aws --region us-west-1 s3 sync s3://${chef_bucket}/${application}/config /etc/chef/
      if compgen -G "/etc/chef/cookbooks-*.tar.gz" > /dev/null; then
          echo "Cookbook archive found."
          if [ -d "/etc/chef/cookbooks" ]; then
              echo "Removing previously extracted cookbooks."
              rm -r /etc/chef/cookbooks
          fi
          echo "Extracting highest numbered cookbook archive."
          cbarchives=(/etc/chef/cookbooks-*.tar.gz)
          tar -zxf "$${cbarchives[@]: -1}" -C /etc/chef
          chown -R root:root /etc/chef
      fi
  - path: /etc/chef/perform_chef_run.sh
    permissions: '0755'
    owner: 'root'
    group: 'root'
    content: |
      #!/bin/bash
      echo "Running chef-client"
      chef-client -z -F min -r 'recipe[surfline-ecs::default]' -c /etc/chef/client.rb -E ${environment}
  - path: /etc/chef/environments/${environment}.json
    permissions: '0644'
    owner: 'root'
    group: 'root'
    content: |
      {
        "name": "${environment}",
        "default_attributes": {
          "citadel": {
            "bucket": "${company}-${application}-${environment}",
            "region": "us-west-1"
          }
        },
        "json_class": "Chef::Environment",
        "description": "${environment} environment",
        "chef_type": "environment"
      }
  - path: /etc/chef/client.rb
    permissions: '0644'
    content: |
      log_level :info
      log_location '/var/log/chef/client.log'
      ssl_verify_mode        :verify_none
      cookbook_path '/etc/chef/cookbooks'
      node_path '/etc/chef/nodes'
      role_path '/etc/chef/roles'
      data_bag_path '/etc/chef/data_bags'
      environment_path '/etc/chef/environments'
      local_mode 'true'
  - path: /etc/chef/grep_for_bootstrapping_anomalies.sh
    permissions: '0755'
    content: |
      #!/bin/bash

      set -euxo pipefail

      readonly ENVIRONMENT=${environment}
      readonly HOST=$(hostname -s)
      readonly INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
      readonly LOGS='/var/log/chef/client.log'
      readonly NEW_RELIC_API_ENDPOINT='https://insights-collector.newrelic.com/v1/accounts/356245/events'
      readonly NEWRELIC_EVENTS_KEY=${newrelic_events_key}
      readonly TIME=$(date +"%Y-%m-%d %T")

      function error {
          echo "[ERROR]: $${*}" >&2
          exit 1
      }

      function info {
          echo "[INFO]: $${*}"
      }

      trap catch_errors ERR;
      function catch_errors() {
        info 'Script aborted because of errors';
        exit 1;
      }

      function send_nr_event {
          info 'Sending New Relic events:'
          PAYLOAD=$( \
                  jq -n \
                  --arg time "$${TIME}" \
                  --arg environment "$${ENVIRONMENT}" \
                  --arg instance_id "$${INSTANCE_ID}" \
                  --arg host "$${HOST}" \
                  '{time: $time, environment: $environment, eventType: "InfrastructureEvent", eventDetail: "ecsChefrunMonitor", instance_id: $instance_id, host: $host}' \
          )

          curl \
              -X POST \
              -H 'Content-Type: application/json' \
              -H "X-Insert-Key: $${NEWRELIC_EVENTS_KEY}" \
              -d "$${PAYLOAD}" \
              "$${NEW_RELIC_API_ENDPOINT}"
      }

      if [[ -n $(grep 'ERROR: Running exception handlers' "$${LOGS}" ) || ! -s "$${LOGS}" ]]; then
          info 'Errors found.'
          send_nr_event
      else
          info 'Chef ran successfully!'
      fi
runcmd:
  - echo "Installing Chef"
  - curl -L https://www.chef.io/chef/install.sh | bash -s -- -v 12.19.36
  - mkdir -p /var/log/chef /etc/chef
  - echo "Configuring Chef"
  - aws --region us-west-1 s3 sync s3://${chef_bucket}/${application}/config /etc/chef/
  - echo "Initial Chef run"
  - python /etc/chef/rename_instance
  - /etc/chef/sync_cookbooks.sh
  - /etc/chef/perform_chef_run.sh
  - /etc/chef/grep_for_bootstrapping_anomalies.sh
