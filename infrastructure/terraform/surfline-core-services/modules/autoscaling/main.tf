data aws_ssm_parameter "newrelic_events_key" {
  name = "/${var.environment}/infrastructure/wt-newrelic/NEWRELIC_EVENTS_KEY"
}

data "template_file" "user_data" {
  template = file("${path.module}/resources/user_data.tpl")
  vars = {
    company             = var.company
    application         = var.application
    environment         = var.environment
    chef_bucket         = var.chef_bucket
    newrelic_events_key = data.aws_ssm_parameter.newrelic_events_key.value
  }
}

resource "aws_launch_template" "ecs" {
  name                   = "${var.company}-${var.application}-ecs-${var.environment}"
  image_id               = var.ami_id # http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = var.vpc_security_group_ids
  user_data              = base64encode(data.template_file.user_data.rendered)
  update_default_version = true

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size           = 55
      volume_type           = "gp2"
      delete_on_termination = "true"
      encrypted             = "false"
    }
  }

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  tags = {
    Company     = var.company
    Service     = "ecs"
    Application = var.application
    Environment = var.environment
    Terraform   = "true"
  }
}

resource "aws_autoscaling_group" "ecs_cluster" {
  name                = "${var.company}-${var.application}-ecs-${var.environment}"
  availability_zones  = var.availability_zones
  min_size            = var.min_instance_count
  max_size            = var.max_instance_count
  health_check_type   = "EC2"
  vpc_zone_identifier = var.instance_subnets
  enabled_metrics     = var.enabled_metrics

  launch_template {
    id      = aws_launch_template.ecs.id
    version = "$Latest"
  }

  tag {
    key                 = "Name" # names the launched instances
    value               = "${var.company}-ecs-${var.application}-${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Company"
    value               = var.company
    propagate_at_launch = true
  }

  tag {
    key                 = "Service"
    value               = "ecs"
    propagate_at_launch = true
  }

  tag {
    key                 = "Application"
    value               = var.application
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = true
  }

  # type is used by the bootstrap script to locate other instances of same type
  tag {
    key                 = "type"
    value               = "${var.application}-ecs-host"
    propagate_at_launch = true
  }
}

module "sns_topic" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"
  environment = var.environment
  topic_name  = "${var.company}-${var.application}-ecs-host-terminating"
}

resource "aws_iam_role" "lifecycle_hook_role" {
  name = "${var.company}-${var.application}-${var.environment}-ecs-host-terminating-role"

  assume_role_policy = templatefile("${path.module}/resources/asg-assume-role-policy.json", {})
}

resource "aws_iam_role_policy_attachment" "lifecycle_hook_policy" {
  role       = aws_iam_role.lifecycle_hook_role.name
  policy_arn = module.sns_topic.topic_policy
}

resource "aws_autoscaling_lifecycle_hook" "ecs_host_terminating" {
  name                   = "${var.company}-${var.application}-${var.environment}-ecs-host-terminating"
  autoscaling_group_name = aws_autoscaling_group.ecs_cluster.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = 900
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"

  notification_target_arn = module.sns_topic.topic
  role_arn                = aws_iam_role.lifecycle_hook_role.arn
}

#ASG Scale on Memory Reservations
resource "aws_autoscaling_policy" "memory_scale_up" {
  name                   = "${var.company}-${var.application}-${var.environment}-memory-scale-up-policy"
  autoscaling_group_name = aws_autoscaling_group.ecs_cluster.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = var.scaling_adjustment_up
  cooldown               = var.scale_up_cooldown_period
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "ecs_memory_reservation_alarm_up" {
  alarm_name          = "${var.company}-${var.application}-${var.environment}-ecs-memory-reservation-alarm-up"
  alarm_description   = "High ECS memory reservation to trigger ASG scaling"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.scaling_evaluation_periods_up
  metric_name         = "MemoryReservation"
  namespace           = "AWS/ECS"
  period              = var.period_up
  statistic           = "Average"
  threshold           = var.threshold_up

  dimensions = {
    "ClusterName" = var.ecs_cluster_name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.memory_scale_up.arn]
}

#ASG Scale down on Memory Reservations
resource "aws_autoscaling_policy" "memory_scale_down" {
  name                   = "${var.company}-${var.application}-${var.environment}-memory-scale-down-policy"
  autoscaling_group_name = aws_autoscaling_group.ecs_cluster.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = var.scaling_adjustment_down
  cooldown               = var.scale_down_cooldown_period
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "ecs_memory_reservation_alarm_down" {
  alarm_name          = "${var.company}-${var.application}-${var.environment}-ecs-memory-reservation-alarm-down"
  alarm_description   = "High ECS memory reservation to trigger ASG scaling"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = var.scaling_evaluation_periods_down
  metric_name         = "MemoryReservation"
  namespace           = "AWS/ECS"
  period              = var.period_down
  statistic           = "Average"
  threshold           = var.threshold_down

  dimensions = {
    "ClusterName" = var.ecs_cluster_name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.memory_scale_down.arn]
}
