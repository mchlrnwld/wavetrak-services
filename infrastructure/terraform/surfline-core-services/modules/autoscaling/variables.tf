variable "company" {
  default = "sl"
}

variable "application" {
  default = "core-svc"
}

variable "environment" {
  default = ""
}

variable "ami_id" {
  default = "ami-0667a9cc6a93f50fe"
}

variable "instance_type" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "ecs_cluster_name" {
  default = ""
}

variable "availability_zones" {
  type    = list(string)
  default = []
}

variable "instance_subnets" {
  type    = list(string)
  default = []
}

variable "chef_bucket" {
  default = ""
}

variable "iam_instance_profile" {
  default = ""
}

variable "min_instance_count" {
  default = 0
}

variable "max_instance_count" {
  default = 0
}

variable "ecs_cluster_alarm_memory_res_buffer" {
  default = 0
}

variable "scale_up_cooldown_period" {
  default = 120
}

variable "scale_down_cooldown_period" {
  default = 120
}

variable "scaling_adjustment_up" {
  default = 120
}

variable "scaling_adjustment_down" {
  default = 300
}

variable "scaling_evaluation_periods_up" {
  default = 120
}

variable "scaling_evaluation_periods_down" {
  default = 300
}

variable "period_up" {
  default = 120
}

variable "period_down" {
  default = 300
}

variable "threshold_up" {
  default = 45
}

variable "threshold_down" {
  default = 33
}

variable "enabled_metrics" {
  default = []
}

variable "vpc_security_group_ids" {
  type    = list(string)
  default = []
}
