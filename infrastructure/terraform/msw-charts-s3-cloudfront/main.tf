module "msw_charts" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/cloudfront-with-s3"

  company     = var.company
  environment = var.environment
  application = var.application
  service     = "chart-image-cdn"

  cdn_acm_domains    = var.chart_cdn_acm_domains
  cdn_acm_count      = length(var.chart_cdn_acm_domains)
  cdn_fqdn           = var.chart_cdn_fqdn
  versioning_enabled = false
  comment            = "${var.environment} chart image CDN wth S3"
  allowed_origins    = "*"
}
