provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "msw-charts/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "msw-charts"
  environment = "sandbox"

  chart_cdn_acm_domains = ["sandbox.surfline.com"]
  chart_cdn_fqdn = {
    "sandbox.surfline.com" = ["msw-chart-image-cdn.sandbox.surfline.com"]
  }
}

module "msw_charts" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  chart_cdn_acm_domains = local.chart_cdn_acm_domains
  chart_cdn_fqdn        = local.chart_cdn_fqdn
}
