provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "msw-charts/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "msw-charts"
  environment = "staging"

  chart_cdn_acm_domains = ["staging.surfline.com"]
  chart_cdn_fqdn = {
    "staging.surfline.com" = ["msw-chart-image-cdn.staging.surfline.com"]
  }
}

module "msw_charts" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  chart_cdn_acm_domains = local.chart_cdn_acm_domains
  chart_cdn_fqdn        = local.chart_cdn_fqdn
}
