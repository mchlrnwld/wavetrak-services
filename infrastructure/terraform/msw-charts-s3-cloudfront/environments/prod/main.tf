provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "msw-charts/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "msw-charts"
  environment = "prod"

  chart_cdn_acm_domains = ["cdn-surfline.com"]
  chart_cdn_fqdn = {
    "cdn-surfline.com" = ["msw-chart-image-cdn.cdn-surfline.com"]
  }
}

module "msw_charts" {
  source = "../../"

  company     = local.company
  application = local.application
  environment = local.environment

  chart_cdn_acm_domains = local.chart_cdn_acm_domains
  chart_cdn_fqdn        = local.chart_cdn_fqdn
}
