# The command line shell application.
SHELL := /bin/bash

# The folder containing the list of available environments for the module.
ENVS := "environments"

# The actual folder with specific environment configurations.
# Example: "environments/staging/main.tf"
FOLDER := "${ENVS}/${ENV}"

# Checks for the existence of the ENV variable and its environment's folder.
checkenv:
	@echo -n "- Checking environment... ";\
	if [ -z ${ENV} ]; then\
		echo "  Error!";\
		echo "  ENV variable was not set";\
		echo "";\
		exit 10;\
	fi;\
	if [ ! -d "${FOLDER}" ]; then\
		echo "  Error!";\
		echo "  Folder '${FOLDER}' not found";\
		echo "";\
		exit 10;\
	fi;\
	echo "Done!"

# Runs the `terraform validate` command checking all resource configurations and
# child modules involved in the architecture of the current module.
validate:
	@pushd ./${FOLDER} > /dev/null;\
		echo "- Running 'terraform validate' command... ";\
		terraform validate;\
		echo "Done!";\
	popd > /dev/null

# Access environment's folder and executes the `terraform init` command.
init: checkenv
	@pushd ./${FOLDER} > /dev/null;\
		echo "- Running 'terraform init' command...";\
		terraform init -reconfigure;\
		if [ ! $$? -eq 0 ]; then\
			echo "";\
			echo "- Could not complete command!";\
			echo "  Credentials might be incorrect or remote storage doesn't exists.";\
			echo "";\
			exit 13;\
		fi;\
	popd > /dev/null

# Calls the `init` target and executes the `terraform plan` command after.
plan: init
	@pushd ./${FOLDER} > /dev/null;\
		echo "- Running 'terraform plan' command...";\
		terraform plan -refresh=true;\
	popd > /dev/null

# Calls the `init` target and executes the `terraform apply` command after.
apply: init
	@pushd ./${FOLDER} > /dev/null;\
		echo "- Running 'terraform apply' command...";\
		terraform apply -refresh=true;\
	popd > /dev/null

# Runs the `terraform refresh` command that will update the state file in the
# remote backend with the version of the local Terraform CLI that executes the
# process.
refresh: init
	@pushd ./${FOLDER} > /dev/null;\
		echo "- Running 'terraform refresh' command...";\
		terraform refresh;\
	popd > /dev/null

# Executes the destroying process of removing any cloud resource associated with
# the current module infrastructure.
destroy: init
	@pushd ./${FOLDER} > /dev/null;\
		echo "- Running 'terraform destroy' command...";\
		terraform destroy -refresh=true;\
	popd > /dev/null

# Runs a linter for Terraform configuration files.
# This target doesn't need the ENV variable to be present.
format:
	@echo -n "- Running 'terraform fmt' command recursively... ";\
	terraform fmt -recursive;\
	echo "Done!"

# Removes temporary folders created from a previous execution.
reset: checkenv
	@pushd ./${FOLDER} > /dev/null;\
		echo -n "- Cleaning up temporary configurations... ";\
		rm -rf .terraform terraform.tfstate terraform.tfplan;\
		echo "Done!";\
	popd > /dev/null

.PHONY: checkenv validate init plan apply refresh destroy format reset
