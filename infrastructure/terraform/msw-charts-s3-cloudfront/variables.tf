variable "company" {
  type    = string
  default = ""
}

variable "application" {
  type    = string
  default = ""
}

variable "environment" {
  type    = string
  default = ""
}

variable "chart_cdn_fqdn" {
  description = "List of domains associated with ACM certificate on the Cloudfront distribution"
  type        = map(list(string))
}

variable "chart_cdn_acm_domains" {
  description = "List of domains associated with Cloudfront distribution"
  type        = list(string)
}
