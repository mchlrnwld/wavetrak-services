variable "environment" {
}

variable "application" {
}

variable "company" {
}

variable "cf_domains" {
  type = list(string)
}

variable "cdn_acm_arn" {
}

variable "s3_bucket_name" {
}

variable "cf_ttl" {
}
