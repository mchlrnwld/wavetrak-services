provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-legacy"
    key    = "s3-auth-client-access/legacy/terraform.tfstate"
    region = "us-west-1"
  }
}

module "s3-auth-client-access" {
  source = "../../"

  company     = "sl"
  application = "s3-auth-client-access"
  environment = "legacy"

  cf_domains     = ["spacex.surfline.com", "ww3-spectral.cdn-surfline.com"]
  cdn_acm_arn    = "arn:aws:acm:us-east-1:094422976157:certificate/6762a866-8ff1-4444-b219-9a056dc3cb5a"
  s3_bucket_name = "ww3-spectral"
  cf_ttl         = "0"
}