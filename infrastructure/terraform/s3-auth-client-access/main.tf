provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

# Because the Cloudfront distribution needs to be associated to a Lambda function,
# we can't use an existing module. If we need to do more with Lambda@Edge, we should
# consider abstracting this to a module

resource "aws_s3_bucket_policy" "bucket_policy" {
  provider = aws.us-east-1
  bucket   = var.s3_bucket_name
  policy = templatefile("${path.module}/resources/s3-bucket-policy.json", {
    s3_bucket  = "arn:aws:s3:::${var.s3_bucket_name}"
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.environment} S3 Auth Client Access CDN without S3"
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  origin {
    domain_name = "${var.s3_bucket_name}.s3.amazonaws.com"
    origin_id   = "S3-${var.s3_bucket_name}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = "${var.s3_bucket_name} ${var.environment} S3 Auth Client Access CDN without S3"
  aliases         = var.cf_domains

  logging_config {
    include_cookies = false
    bucket          = "ww3-spectral-logs.s3.amazonaws.com"
    prefix          = "cf-logs/"
  }

  default_cache_behavior {
    allowed_methods        = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods         = ["HEAD", "GET", "OPTIONS"]
    target_origin_id       = "S3-${var.s3_bucket_name}"
    compress               = true
    viewer_protocol_policy = "redirect-to-https"

    lambda_function_association {
      event_type = "viewer-request"
      lambda_arn = aws_lambda_function.auth.qualified_arn
    }

    forwarded_values {
      query_string = false
      headers      = ["Authorization"]

      cookies {
        forward = "none"
      }
    }

    min_ttl     = 0
    default_ttl = var.cf_ttl
    max_ttl     = var.cf_ttl
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = "cdn"
  }

  viewer_certificate {
    acm_certificate_arn      = var.cdn_acm_arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

# Lambda resources

data "archive_file" "auth" {
  type        = "zip"
  output_path = "${path.module}/.zip/auth.zip"

  source {
    filename = "index.js"
    content  = file("${path.module}/resources/lambda/auth.js")
  }
}

resource "aws_lambda_function" "auth" {
  provider         = aws.us-east-1
  function_name    = "${var.company}-${var.application}-auth-${var.environment}"
  filename         = data.archive_file.auth.output_path
  source_code_hash = data.archive_file.auth.output_base64sha256
  role             = aws_iam_role.main.arn
  runtime          = "nodejs10.x"
  handler          = "index.handler"
  memory_size      = 128
  timeout          = 3
  publish          = true
}

resource "aws_iam_role" "main" {
  provider           = aws.us-east-1
  name_prefix        = var.application
  assume_role_policy = file("${path.module}/resources/edgelambda-policy.json")
}

resource "aws_iam_role_policy_attachment" "basic" {
  provider   = aws.us-east-1
  role       = aws_iam_role.main.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_policy" "get_secret" {
  name        = "${var.company}-${var.application}-auth-policy-${var.environment}"
  description = "Grant access to an AWS Secrets Manager secret"

  policy = templatefile("${path.module}/resources/secretsmanager-policy.json", {
    application = var.application
    company     = var.company
  })
}

resource "aws_iam_role_policy_attachment" "get_secret" {
  provider   = aws.us-east-1
  role       = aws_iam_role.main.name
  policy_arn = aws_iam_policy.get_secret.arn
}
