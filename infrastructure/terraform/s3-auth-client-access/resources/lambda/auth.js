/**
 * BASIC Authentication
 *
 * Simple authentication script intended to be run by Amazon Lambda to
 * provide Basic HTTP Authentication for a static website hosted in an
 * Amazon S3 bucket through Couldfront.
 *
 * https://hackernoon.com/serverless-password-protecting-a-static-website-in-an-aws-s3-bucket-bfaaa01b8666
 */

const AWS = require('aws-sdk');

const region = "us-east-1";
const secretName = "legacy/function/sl-s3-auth-client-access/AUTH";

// Create a Secrets Manager client
const client = new AWS.SecretsManager({
  region
});

exports.handler = (event, context, callback) => {

  // Get request and request headers
  const { request } = event.Records[0].cf;
  const { headers } = request;

  console.log(`[DEBUG] Headers: ${JSON.stringify({ headers })}`);

  // Get auth values
  client.getSecretValue({SecretId: secretName}, (err, data) => {
    if (err) {
      console.log('[ERROR] Error retrieving auth values, sending 500 error');

      const body = 'Internal Server Error';
      const response = {
        status: '500',
        statusDescription: 'Internal Server Error',
        body,
      };
      return callback(null, response);
    }

    const authValues = JSON.parse(data.SecretString);
    const isAuthenticated = authValues.some(authPair => {
      const authString = `Basic ${Buffer.from(authPair).toString('base64')}`;
      console.log(`[DEBUG] Auth string from Secrets Manager: ${authString}`);
      return typeof headers.authorization !== 'undefined' && headers.authorization[0].value === authString;
    });

    // Auth matched, allow request to continue
    if (isAuthenticated) {
      console.log('[DEBUG] Authorization succeeded');
      return callback(null, request);
    }

    // Auth didn't match, send 401 response
    console.log('[ERROR] Authorization failed');

    const body = 'Unauthorized';
    const response = {
      status: '401',
      statusDescription: 'Unauthorized',
      body,
      headers: {
        'www-authenticate': [{key: 'WWW-Authenticate', value:'Basic'}]
      },
    };
    return callback(null, response);
  });
};
