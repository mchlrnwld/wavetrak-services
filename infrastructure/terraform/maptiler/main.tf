locals {
  tiledb_bucket = "${var.company}-${var.application}-tiledb-${var.environment}"
}

resource "aws_s3_bucket" "maptiler_tiledb" {
  bucket = local.tiledb_bucket

  policy = templatefile(
    "${path.module}/resources/bucket-policy.tpl",
    {
      bucket = local.tiledb_bucket
    }
  )
}

resource "aws_sns_topic_subscription" "wavetrak" {
  for_each        = toset(var.maptiler_wavetrak_sns_topic_arns)
  topic_arn       = each.key
  protocol        = "https"
  endpoint        = "https://surfline-processing.maptiler.com/cgi-bin/webhook.py"
  delivery_policy = file("${path.module}/resources/sns-delivery-policy.json")
}
