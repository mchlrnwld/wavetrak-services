variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "maptiler"
}

variable "environment" {
  type = string
}

variable "maptiler_wavetrak_sns_topic_arns" {
  description = "Wavetrak SNS topic ARNs to subscribe Maptiler HTTP endpoint to."
  type        = list(string)
  default     = []
}
