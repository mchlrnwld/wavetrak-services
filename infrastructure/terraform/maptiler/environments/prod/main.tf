provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "maptiler/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "maptiler" {
  source = "../../"

  environment = "prod"
  maptiler_wavetrak_sns_topic_arns = [
    "arn:aws:sns:us-west-1:833713747344:surfline-science-s3-prod-lotus-upload-event",
    "arn:aws:sns:us-west-1:833713747344:surfline-science-s3-prod-nasa-mur-sst-upload-event",
    "arn:aws:sns:us-west-1:833713747344:surfline-science-s3-prod-noaa-nam-upload-event"
  ]
}
