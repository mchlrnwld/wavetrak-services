{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::665294954271:role/science_data_service_task_role_sandbox"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${bucket}"
        },
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::665294954271:role/science_data_service_task_role_sandbox"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${bucket}/*"
        }
    ]
}
