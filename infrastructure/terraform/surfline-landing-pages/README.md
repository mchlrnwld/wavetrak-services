# Example Service Infrastructure

This project adds the CNAME records for our landing pages managed with Instapage.

## Terraform

_Uses terraform version `1.0.5`.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sandbox make plan
ENV=sandbox make apply
```

Valid environments are `sandbox`, `staging`, and `prod`.

#### State Management

State is uploaded to S3 after `make apply` is run.
