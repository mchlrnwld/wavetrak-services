provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-landing-pages/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

resource "aws_route53_record" "instapage" {
  zone_id = "ZY7MYOQ65TY5X"
  name    = "go.surfline.com"
  type    = "CNAME"
  ttl     = 60
  records = ["secure.pageserve.co"]
}
