provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-landing-pages/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

resource "aws_route53_record" "instapage" {
  zone_id = "Z3DM6R3JR1RYXV"
  name    = "go.sandbox.surfline.com"
  type    = "CNAME"
  ttl     = 60
  records = ["secure.pageserve.co"]
}
