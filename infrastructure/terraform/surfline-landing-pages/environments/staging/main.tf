provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-landing-pages/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

resource "aws_route53_record" "instapage" {
  zone_id = "Z3JHKQ8ELQG5UE"
  name    = "go.staging.surfline.com"
  type    = "CNAME"
  ttl     = 60
  records = ["secure.pageserve.co"]
}
