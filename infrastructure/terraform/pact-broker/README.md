# Pact Broker Infrastructure

- [Pact Broker Infrastructure](#pact-broker-infrastructure)
  - [Summary](#summary)
  - [Naming Conventions](#naming-conventions)
  - [Terraform](#terraform)
    - [Using Terraform](#using-terraform)

## Summary

This project sets up infrastructure to run Pact Broker in ECS. The container is setup to run in the core services cluster and connects to a Postgres RDS instance.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.29`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=staging make plan
ENV=staging make apply
```

Valid environments are `staging` and `prod`.
