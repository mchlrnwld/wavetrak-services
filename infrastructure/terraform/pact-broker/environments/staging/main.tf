provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "pact-broker/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  environment = "staging"
  dns_name    = "pact-broker.${local.environment}.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "pact_broker" {
  source = "../../"

  environment  = local.environment
  iam_role_arn = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  default_vpc  = "vpc-981887fd"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  ecs_cluster       = "sl-core-svc-${local.environment}"
  service_td_count  = 2
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-staging/26ee81426b4723db"
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
  service_lb_rules  = local.service_lb_rules
  dns_name          = local.dns_name
  dns_zone_id       = "Z3JHKQ8ELQG5UE"
  db_instance_class = "db.t2.micro"
}
