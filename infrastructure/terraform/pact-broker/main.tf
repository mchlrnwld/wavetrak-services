data "aws_ssm_parameter" "db_username" {
  name = "/${var.environment}/pact_broker/db_user"
}

data "aws_ssm_parameter" "db_password" {
  name = "/${var.environment}/pact_broker/db_pass"
}

data "aws_security_group" "core_services" {
  name = "${var.company}-sg-ecs-core-svc-${var.environment}"
}

locals {
  db_name = "Pacts"
}

data "template_file" "container_definitions" {
  template = templatefile("${path.module}/resources/pact-broker.json", {
    name        = var.application
    db_host     = aws_db_instance.postgres.address
    db_adapter  = aws_db_instance.postgres.engine
    db_name     = local.db_name
    db_username = data.aws_ssm_parameter.db_username.value
    db_password = data.aws_ssm_parameter.db_password.value
    log_group   = "${var.company}-logs-core-svc-${var.environment}"
    log_region  = "us-west-1"
  })
}

resource "aws_ecs_task_definition" "pact_broker" {
  family                = "${var.company}-core-svc-${var.environment}-${var.application}"
  container_definitions = data.template_file.container_definitions.rendered
}

module "ecs_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company                        = var.company
  application                    = var.application
  environment                    = var.environment
  service_name                   = var.application
  service_lb_rules               = var.service_lb_rules
  service_lb_healthcheck_path    = "/diagnostic/status/heartbeat"
  service_lb_healthcheck_matcher = 200
  service_td_name                = "sl-core-svc-${var.environment}-${var.application}"
  service_td_count               = var.service_td_count
  service_port                   = 80
  service_td_container_name      = var.application
  service_alb_priority           = 1000
  iam_role_arn                   = var.iam_role_arn
  dns_name                       = var.dns_name
  dns_zone_id                    = var.dns_zone_id
  load_balancer_arn              = var.load_balancer_arn
  alb_listener                   = var.alb_listener_arn
  default_vpc                    = var.default_vpc
  ecs_cluster                    = var.ecs_cluster
}

resource "aws_db_subnet_group" "postgres" {
  name       = "${var.company}-${var.application}-rds-${var.environment}"
  subnet_ids = var.instance_subnets
}

resource "aws_security_group" "postgres" {
  name   = "${var.company}-${var.application}-rds-${var.environment}"
  vpc_id = var.default_vpc
}

resource "aws_security_group_rule" "ingress_allow_core_services" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = data.aws_security_group.core_services.id
  security_group_id        = aws_security_group.postgres.id
}

resource "aws_security_group_rule" "egress_allow_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.postgres.id
}

resource "aws_db_instance" "postgres" {
  identifier        = "${var.company}-${var.application}-${var.environment}"
  engine            = "postgres"
  engine_version    = "9.6.18"
  instance_class    = var.db_instance_class
  allocated_storage = 5

  name     = local.db_name
  username = data.aws_ssm_parameter.db_username.value
  password = data.aws_ssm_parameter.db_password.value

  db_subnet_group_name   = aws_db_subnet_group.postgres.id
  vpc_security_group_ids = [aws_security_group.postgres.id]

  maintenance_window        = "Sat:06:00-Sat:07:00"
  final_snapshot_identifier = "${var.company}-${var.application}-${var.environment}"
  apply_immediately         = true

  backup_retention_period = 7
  backup_window           = "03:00-04:00"

  depends_on = [
    aws_db_subnet_group.postgres,
    aws_security_group.postgres,
  ]
}
