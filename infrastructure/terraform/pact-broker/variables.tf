variable "company" {
  default = "sl"
}

variable "application" {
  default = "pact-broker"
}

variable "environment" {
}

variable "iam_role_arn" {
}

variable "ecs_cluster" {
}

variable "service_td_count" {
}

variable "load_balancer_arn" {
}

variable "alb_listener_arn" {
}

variable "service_lb_rules" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "default_vpc" {
}

variable "instance_subnets" {
}

variable "db_instance_class" {
}
