provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "msw-ms-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "msw_cdn" {
  source = "../../"

  environment = "prod"
}
