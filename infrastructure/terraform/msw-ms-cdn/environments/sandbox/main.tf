provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "msw-ms-cdn/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "msw_cdn" {
  source = "../../"

  environment = "sandbox"
}
