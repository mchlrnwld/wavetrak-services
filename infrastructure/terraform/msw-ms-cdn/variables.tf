variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "msw-theme-cdn"
}

variable "environment" {
  type    = string
  default = ""
}

variable "theme_origin_domain" {
  type    = string
  default = "images.magicseaweed.com"
}

variable "default_ttl" {
  default = 600
}

variable "max_ttl" {
  default = 600
}
