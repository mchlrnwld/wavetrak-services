variable "company" {}
variable "application" {}
variable "environment" {}

provider "aws" {
  region  = "<%= awsRegion %>"
  version = "~> <%= awsProviderVersion %>"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-<%= environment %>"
    key    = "<%= application %>/<%= environment %>/terraform.tfstate"
    region = "<%= awsRegion %>"
  }
}
