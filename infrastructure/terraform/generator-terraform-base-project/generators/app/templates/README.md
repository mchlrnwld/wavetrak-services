# <%= application %> Infrastructure

About this project. Whats it's architecture? What infrastructure does it provision?


## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `<%= tfVersion %>`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments are `sbox`, `integration`, `staging`, `prod`.

#### State Management

State is uploaded to S3 after `make setup` and `make apply` are run. State details can be found in each environment's `main.tf` file.


### Directory Structure

```
infra
├── Makefile
├── README.md
├── modules
├── prod
│   ├── main.tf
│   └── prod.tfvars
├── sbox
│   ├── main.tf
│   └── sbox.tfvars
└── staging
    ├── main.tf
    └── staging.tfvars
```
