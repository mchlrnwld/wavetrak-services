const Generator = require('yeoman-generator');
const yosay = require('yosay');

const DEFAULT_ENVS = ['sbox', 'staging', 'prod'];

module.exports = class extends Generator {
  prompting() {
    this.log(yosay('Setting up a terraform project!'));

    const prompts = [
      {
        type: 'input',
        name: 'tfVersion',
        message: 'Terraform Version',
        default: '0.11.8',
        store: true,
      },
      {
        type: 'input',
        name: 'awsRegion',
        message: 'AWS Region',
        default: 'us-west-1',
        store: true,
      },
      {
        type: 'input',
        name: 'awsProviderVersion',
        message: 'Terraform AWS Provider Version',
        default: '1.23.0',
        store: true,
      },
      {
        type: 'list',
        name: 'company',
        message: 'Company',
        choices: ['sl', 'ft', 'bw', 'wt'],
        store: true,
      },
      {
        type: 'input',
        name: 'application',
        message: 'Project name',
        store: true,
      },
    ];

    return this.prompt(prompts).then((answers) => {
      this.answers = answers;
    });
  }

  writing() {
    const globalTplAnswers = {
      tfVersion: this.answers.tfVersion,
      awsRegion: this.answers.awsRegion,
      awsProviderVersion: this.answers.awsProviderVersion,
      application: this.answers.application,
      company: this.answers.company,
    };

    this.fs.copy(
      this.templatePath('Makefile'),
      this.destinationPath('Makefile')
    );

    this.fs.copy(
      this.templatePath('gitignore'),
      this.destinationPath('.gitignore')
    );

    this.fs.copy(
      this.templatePath('modules/gitignore'),
      this.destinationPath('modules/.gitignore')
    );

    this.fs.copyTpl(
      this.templatePath('README.md'),
      this.destinationPath('README.md'),
      globalTplAnswers
    );

    this.fs.copyTpl(
      this.templatePath('terraform-version'),
      this.destinationPath('.terraform-version'),
      globalTplAnswers
    );

    DEFAULT_ENVS.forEach((env) => {
      this.fs.copyTpl(
        this.templatePath('env/main.tf'),
        this.destinationPath(`${env}/main.tf`),
        Object.assign(globalTplAnswers, { environment: env })
      );
      this.fs.copyTpl(
        this.templatePath('env/variables.tf'),
        this.destinationPath(`${env}/${env}.tfvars`),
        Object.assign(globalTplAnswers, { environment: env })
      );
    });
  }
};
