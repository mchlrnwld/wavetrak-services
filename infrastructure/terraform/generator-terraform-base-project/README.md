# Terraform Base Terraform Project

This project manages a base terraform project for setting up new project infrastructure. Yeoman is used to manage files and bootstrapping.

## Installation

### Prerequisites
* Node v6
* Yeoman
* Yarn

```bash
npm install -g yo
npm install -g @surfline/generator-terraform-base-project
```

Then generate your new project (from the project `infra/` directory):

```bash
yo @surfline/terraform-base-project
```

## Development

To make changes to the base project `npm link` can be run from the root of this project. Running `yo @surfline/terraform-base-project` will then use the linked generator.

Run `npm publish` to publish a new version of the generator project to Surfline's shared npm repository.

## About the Base Project

For more information see `generators/app/templates/README.md`.
