
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

describe('generator-terraform-base-project:app', () => {
  beforeAll(() => helpers.run(path.join(__dirname, '../generators/app'))
    .withPrompts({
      tfVersion: '0.11.8',
      awsRegion: 'us-west-1',
      awsProviderVersion: '1.23.0',
      company: 'sl',
      application: 'test-runner',
    }));

  it('creates files', () => {
    assert.file([
      'Makefile',
      '.terraform-version',
      '.gitignore',
      'modules/.gitignore',
      'README.md',
      'sbox/main.tf',
      'sbox/sbox.tfvars',
      'staging/main.tf',
      'staging/staging.tfvars',
      'prod/main.tf',
      'prod/prod.tfvars',
    ]);
  });

  it('sets the proper terraform version', () => {
    assert.fileContent('.terraform-version', '0.10.2');
  });

  it('creates environment terraform files with the proper provider', () => {
    assert.fileContent('sbox/main.tf', /bucket(.+)= "sl-tf-state-sbox"/);
    assert.fileContent('sbox/main.tf',
      /key(.+)= "test-runner\/sbox\/terraform.tfstate"/);
    assert.fileContent('sbox/main.tf', /region(.+)= "us-west-1"/);

    assert.fileContent('staging/main.tf', /bucket(.+)= "sl-tf-state-staging"/);
    assert.fileContent('staging/main.tf',
      /key(.+)= "test-runner\/staging\/terraform.tfstate"/);
    assert.fileContent('staging/main.tf', /region(.+)= "us-west-1"/);

    assert.fileContent('prod/main.tf', /bucket(.+)= "sl-tf-state-prod"/);
    assert.fileContent('prod/main.tf',
      /key(.+)= "test-runner\/prod\/terraform.tfstate"/);
    assert.fileContent('prod/main.tf', /region(.+)= "us-west-1"/);
  });
});
