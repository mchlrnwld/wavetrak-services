output "bucket_arn" {
  value = aws_s3_bucket.bucket.arn
}

output "bucket_policy_arn" {
  value = aws_iam_policy.bucket_mgmt_policy.arn
}

output "domain_name" {
  value = aws_cloudfront_distribution.cdn.domain_name
}

output "domain_alias" {
  value = aws_cloudfront_distribution.cdn.aliases
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.cdn.hosted_zone_id
}
