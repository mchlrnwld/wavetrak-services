# S3 bucket for content
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.company}-${var.application}-${var.service}-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD", "DELETE", "POST", "PUT"]
    allowed_origins = [var.allowed_origins]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = var.versioning_enabled
  }

  lifecycle_rule {
    prefix  = ""
    enabled = var.versioning_enabled

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  tags = {
    Name        = "${var.company}-${var.application}-${var.service}-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
}

# S3 bucket policy for CloudFront access
data "template_file" "policy" {
  template = templatefile("${path.module}/resources/cloudfront-policy.json", {
    cloudfront = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
    s3_bucket  = aws_s3_bucket.bucket.arn
  })
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.template_file.policy.rendered
}

# Set a policy to manage content in bucket
resource "aws_iam_policy" "bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-${var.service}-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/s3-policy.json", {
    s3_bucket = aws_s3_bucket.bucket.arn
  })
}

# CloudFront access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.comment
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  origin {
    domain_name = "${aws_s3_bucket.bucket.bucket}.s3.amazonaws.com"
    origin_id   = "S3-${aws_s3_bucket.bucket.bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment
  aliases         = [var.cdn_fqdn]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${aws_s3_bucket.bucket.bucket}"
    compress         = true

    forwarded_values {
      query_string = false
      headers      = ["Origin"]
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    max_ttl                = 604800
    default_ttl            = 86400
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }
  viewer_certificate {
    acm_certificate_arn      = var.cdn_acm
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
