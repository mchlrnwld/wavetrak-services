variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "service" {
}

variable "versioning_enabled" {
}

variable "comment" {
}

variable "allowed_origins" {
}

variable "cdn_fqdn" {
}

variable "cdn_acm" {
}
