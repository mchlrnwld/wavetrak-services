variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "service" {
}

variable "versioning_enabled" {
}

variable "comment" {
}

variable "cdn_fqdn" {
}

variable "alias_hosted_zone_id" {
}

variable "cdn_acm" {
}

variable "jenkins_push_user" {
}
