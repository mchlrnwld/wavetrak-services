module "bucket_cdn" {
  source = "./modules/bucket-with-cdn"

  application        = "product"
  company            = "sl"
  environment        = var.environment
  service            = "cdn"
  comment            = "surfline product cdn"
  versioning_enabled = false
  allowed_origins    = "*"

  cdn_acm  = var.cdn_acm
  cdn_fqdn = var.cdn_fqdn
}

resource "aws_route53_record" "product_cdn_domain" {
  zone_id = var.alias_hosted_zone_id
  name    = var.cdn_fqdn
  type    = "A"

  alias {
    name                   = module.bucket_cdn.domain_name
    zone_id                = module.bucket_cdn.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_iam_policy_attachment" "jenkins_push" {
  name       = "s3-readwrite-product-cdn-${var.environment}"
  roles      = [var.jenkins_push_user]
  policy_arn = module.bucket_cdn.bucket_policy_arn
}
