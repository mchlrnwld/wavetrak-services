provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-product-cdn/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "product-cdn" {
  source = "../../"

  application = "product"
  company     = "sl"
  environment = "staging"
  service     = "cdn"

  comment            = "staging surfline product cdn"
  versioning_enabled = false

  cdn_fqdn             = "product-cdn.staging.surfline.com"
  alias_hosted_zone_id = "Z3JHKQ8ELQG5UE"
  cdn_acm              = "arn:aws:acm:us-east-1:665294954271:certificate/07c4d79f-1151-42ed-bb62-5ea7c0f93208"

  jenkins_push_user = "sl-jenkins-cross-account"
}
