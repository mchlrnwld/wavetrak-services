provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-product-cdn/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "product-cdn" {
  source = "../../"

  application = "product"
  company     = "sl"
  environment = "sbox"
  service     = "cdn"

  comment            = "sandbox surfline product cdn"
  versioning_enabled = false

  cdn_fqdn             = "product-cdn.sandbox.surfline.com"
  alias_hosted_zone_id = "Z3DM6R3JR1RYXV"
  cdn_acm              = "arn:aws:acm:us-east-1:665294954271:certificate/a48acac9-c364-4186-b33a-73543e5f72c0"

  jenkins_push_user = "sl-jenkins-cross-account"
}
