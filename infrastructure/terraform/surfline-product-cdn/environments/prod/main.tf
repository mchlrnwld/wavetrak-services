provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-product-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "product-cdn" {
  source = "../../"

  application = "product"
  company     = "sl"
  environment = "prod"
  service     = "cdn"

  comment            = "prod surfline product cdn"
  versioning_enabled = false

  cdn_fqdn             = "wa.cdn-surfline.com"
  alias_hosted_zone_id = "Z1OCTVEV5XMNUZ"
  cdn_acm              = "arn:aws:acm:us-east-1:833713747344:certificate/522318ff-4e5b-4642-b217-a932bc4df082"

  jenkins_push_user = "sl-jenkins-cross-account"
}
