resource "aws_security_group" "ecs" {
  name        = "${var.company}-sg-ecs-${var.application}-${var.environment}"
  description = "${var.company}-sg-ecs-${var.application}-${var.environment}"
  vpc_id      = var.default_vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Company     = var.company
    Service     = "ecs"
    Application = var.application
    Environment = var.environment
    Terraform   = "true"
  }
}

resource "aws_ecs_cluster" "main" {
  name = "${var.company}-${var.application}-${var.environment}"
}

resource "aws_launch_configuration" "ecs" {
  image_id      = var.ami_id // http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
  instance_type = var.instance_type
  security_groups = [
    aws_security_group.ecs.id,
    var.internal_sg_group, var.sg_jenkins
  ]
  iam_instance_profile        = aws_iam_instance_profile.ecs.name
  key_name                    = var.key_name
  associate_public_ip_address = false

  user_data = templatefile("${path.module}/resources/user_data.tpl", {
    company     = var.company
    application = var.application
    environment = var.environment
    chef_bucket = var.chef_bucket
  })

  ebs_block_device {
    device_name = "/dev/xvdcz"
    volume_size = "100"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs_cluster" {
  name                 = "${var.company}-${var.application}-ecs-${var.environment}"
  availability_zones   = var.availability_zones
  min_size             = 2
  max_size             = 2
  desired_capacity     = 2
  default_cooldown     = 30
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.ecs.name
  vpc_zone_identifier  = var.instance_subnets

  tag {
    key                 = "Name" // names the launched instances
    value               = "${var.company}-ecs-${var.application}-${var.environment}"
    propagate_at_launch = true
  }
  tag {
    key                 = "Company"
    value               = var.company
    propagate_at_launch = true
  }
  tag {
    key                 = "Service"
    value               = "ecs"
    propagate_at_launch = true
  }
  tag {
    key                 = "Application"
    value               = var.application
    propagate_at_launch = true
  }
  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = true
  }
  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = true
  }
  // type is used by the bootstrap script to locate other instances of same type
  tag {
    key                 = "type"
    value               = "${var.application}-ecs-host"
    propagate_at_launch = true
  }
}

// notify SAND of scaling events
resource "aws_autoscaling_notification" "ecs_cluster_notify_sand" {
  group_names   = [aws_autoscaling_group.ecs_cluster.name]
  notifications = ["autoscaling:EC2_INSTANCE_TERMINATE"]
  topic_arn     = var.dereg_topic
}

resource "aws_autoscaling_schedule" "ecs_cluster_scale_down_schedule" {
  scheduled_action_name  = "ecs_cluster_scale_down_schedule"
  autoscaling_group_name = aws_autoscaling_group.ecs_cluster.name
  min_size               = 0
  max_size               = 1
  desired_capacity       = 1

  # Every week day at 1am UTC which is 5/6pm PST/PDT.
  recurrence = "0 1 * * MON-FRI"
}

resource "aws_autoscaling_schedule" "ecs_cluster_scale_up_schedule" {
  scheduled_action_name  = "ecs_cluster_scale_up_schedule"
  autoscaling_group_name = aws_autoscaling_group.ecs_cluster.name
  min_size               = 2
  max_size               = 2
  desired_capacity       = 2

  # Every week day at 2pm UTC which is 7/8am PST/PDT.
  recurrence = "0 14 * * MON-FRI"
}

resource "aws_iam_role" "ecs_host_role" {
  name               = "${var.company}-ecs-host-${var.application}-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/ecs-role.json", {})
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
  name = "${var.company}-ecs-instance-${var.application}-${var.environment}"
  role = aws_iam_role.ecs_host_role.id
  policy = templatefile("${path.module}/resources/ecs-instance-role-policy.json", {
    chef_bucket = var.chef_bucket
  })
}

resource "aws_iam_role" "ecs_service_role" {
  name               = "${var.company}-ecs-service-${var.application}-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/ecs-role.json", {})
}

resource "aws_iam_role_policy" "ecs_service_role_policy" {
  name   = "${var.company}-ecs-service-${var.application}-${var.environment}"
  policy = templatefile("${path.module}/resources/ecs-service-role-policy.json", {})
  role   = aws_iam_role.ecs_service_role.id
}

resource "aws_iam_instance_profile" "ecs" {
  name = "${var.company}-ecs-${var.application}-${var.environment}"
  path = "/"
  role = aws_iam_role.ecs_host_role.name
}

resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-lambda-${var.application}-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/lambda-role.json", {})
}

resource "aws_iam_role_policy_attachment" "lambda_execution_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_cloudwatch_log_group" "main" {
  name              = "${var.company}-logs-${var.application}-${var.environment}"
  retention_in_days = "7"
}

resource "aws_iam_role" "jenkins_lifecycle_role" {
  name               = "${var.company}-${var.application}-lifecycle-role-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/dlm-snapshot-assume-role-policy.json", {})
}

resource "aws_iam_role_policy" "jenkins_lifecycle" {
  name = "${var.company}-${var.application}-${var.environment}-lifecycle-policy"
  role = aws_iam_role.jenkins_lifecycle_role.id
  policy = templatefile("${path.module}/resources/dlm-snapshot-policy.json", {
    jenkins_ebs_volume_tag = var.jenkins_ebs_volume_tag
  })
}

resource "aws_dlm_lifecycle_policy" "jenkins_dlm_lifecycle_policy" {
  description        = "jenkins DLM lifecycle policy"
  execution_role_arn = aws_iam_role.jenkins_lifecycle_role.arn
  state              = "ENABLED"

  policy_details {
    resource_types = ["VOLUME"]
    schedule {
      name = "nightly snapshots"
      create_rule {
        interval      = 24
        interval_unit = "HOURS"
        times         = ["08:45"]
      }
      retain_rule {
        count = 7
      }
      tags_to_add = {
        Environment = var.environment
        Service     = "DLM"
        Company     = var.company
        Application = var.application
        Terraform   = "True"
      }
      copy_tags = true
    }
    target_tags = {
      Name = var.jenkins_ebs_volume_tag
    }
  }
}
