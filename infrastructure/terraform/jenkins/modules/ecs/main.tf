resource "aws_alb_target_group" "service-target-group" {
  name                 = "${var.company}-${coalesce(var.target_group_name, var.service_name)}-${var.application}-${var.environment}"
  port                 = var.service_port
  protocol             = "HTTP"
  vpc_id               = var.default_vpc
  deregistration_delay = var.task_deregistration_delay

  health_check {
    path = var.service_lb_healthcheck_path
  }

  tags = {
    Company     = var.company
    Service     = "ecs"
    Application = var.application
    Environment = var.environment
    Terraform   = "true"
  }
}

resource "aws_alb_listener_rule" "service-alb-listener" {
  listener_arn = var.alb_listener
  priority     = var.service_alb_priority

  depends_on = [aws_alb_target_group.service-target-group]

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.service-target-group.arn
  }

  condition {
    field  = "path-pattern"
    values = [var.service_lb_path]
  }
}

resource "aws_ecs_service" "service-ecs-service" {
  name            = "${var.company}-${var.service_name}-${var.application}-${var.environment}"
  cluster         = var.ecs_cluster
  task_definition = var.service_td_name
  iam_role        = var.iam_role_policy
  desired_count   = var.service_td_count

  load_balancer {
    target_group_arn = aws_alb_target_group.service-target-group.id
    container_name   = var.service_td_container_name
    container_port   = var.service_port
  }

  depends_on = [aws_alb_target_group.service-target-group]
}
