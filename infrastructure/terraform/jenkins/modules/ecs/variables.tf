variable "service_name" {
}

variable "service_lb_path" {
}

variable "service_lb_healthcheck_path" {
}

variable "service_port" {
}

variable "service_td_name" {
}

variable "service_td_container_name" {
}

variable "service_td_count" {
}

variable "alb_listener" {
}

variable "target_group_name" {
  default = ""
}

variable "service_alb_priority" {
}

# probably inherited
variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "default_vpc" {
}

variable "task_deregistration_delay" {
}

variable "iam_role_policy" {
}

variable "ecs_cluster" {
}
