resource "aws_sqs_queue" "queue_deadletter" {
  name = "${var.queue_name}_deadletter_${var.environment}"
}

resource "aws_sqs_queue" "queue" {
  name = "${var.queue_name}_${var.environment}"
  redrive_policy = templatefile("${path.module}/resources/sqs-queue.json", {
    target_arn = aws_sqs_queue.queue_deadletter.arn
  })
}

resource "aws_sns_topic_subscription" "queue_subscription" {
  topic_arn = var.sns_topic_arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.queue.arn
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = aws_sqs_queue.queue.id
  policy = templatefile("${path.module}/resources/sqs-policy.json", {
    queue_arn = aws_sqs_queue.queue.arn
    topic_arn = var.sns_topic_arn
  })
}
