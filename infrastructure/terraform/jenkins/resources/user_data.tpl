#cloud-config
repo_releasever: 2017.03
repo_update: true
repo_upgrade: all
packages:
  - aws-cli
  - python27-boto
write_files:
  - path: /etc/chef/rename_instance
    content: |
      #!/usr/bin/env python

      import json
      import subprocess

      import boto.ec2
      import requests

      print "[INFO] Getting instance identity"
      metadata_url = ('http://169.254.169.254'
                      '/latest/dynamic/instance-identity/document')
      inst_data = requests.get(metadata_url).json()

      conn = boto.ec2.connect_to_region(inst_data['region'])

      print "[INFO] Getting tags for current instance"
      curr_reservation = conn.get_all_instances(instance_ids=inst_data['instanceId'])
      curr_inst = [i for r in curr_reservation for i in r.instances]
      for inst in curr_inst:
          inst_type = inst.tags['type']
          inst_environment = inst.tags['Environment']

      print "[INFO] Generating instance name"
      short_id_hash = inst_data['instanceId'][2:10]
      name = "{}-{}-{}".format(inst_environment, inst_type, short_id_hash)

      # Ensure the generated name with hash doesn't exceed 63 chars
      short_name = name[:63]

      print "[INFO] Renaming instance to {}".format(short_name)
      for inst in curr_inst:
          inst.add_tag('Name', name)
          poutput = subprocess.check_output('hostname ' + name, shell=True)
          sed_cmd = ("sed -i 's/HOSTNAME=.*/HOSTNAME={}/' "
                     "/etc/sysconfig/network").format(name)
          poutput = subprocess.check_output(sed_cmd, shell=True)

  - path: /etc/cron.d/cleanup
    content: |
      */5 * * * * docker rm $(docker ps -aq) >/dev/null 2>&1
      */5 * * * * docker rmi $(docker images -q) >/dev/null 2>&1
  - path: /etc/ecs/ecs.config
    content: |
      ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=5m
      ECS_CLUSTER=${company}-${application}-${environment}
      ECS_AVAILABLE_LOGGING_DRIVERS=["json-file","awslogs"]
  - path: /etc/chef/sync_cookbooks.sh
    permissions: '0755'
    owner: 'root'
    group: 'root'
    content: |
      #!/bin/bash
      aws --region us-west-1 s3 sync s3://${chef_bucket}/${application}/config /etc/chef/
      if compgen -G "/etc/chef/cookbooks-*.tar.gz" > /dev/null; then
          echo "Cookbook archive found."
          if [ -d "/etc/chef/cookbooks" ]; then
              echo "Removing previously extracted cookbooks."
              rm -r /etc/chef/cookbooks
          fi
          echo "Extracting highest numbered cookbook archive."
          cbarchives=(/etc/chef/cookbooks-*.tar.gz)
          tar -zxf "$${cbarchives[@]: -1}" -C /etc/chef
          chown -R root:root /etc/chef
      fi
  - path: /etc/chef/perform_chef_run.sh
    permissions: '0755'
    owner: 'root'
    group: 'root'
    content: |
      #!/bin/bash
      echo \"Running chef-client\"
      chef-client -z -F min -r 'recipe[surfline-ecs::default],recipe[surfline-ecs::jenkins-slaves]' -c /etc/chef/client.rb -E ${environment} | sed 's/\[0m//g'
  - path: /etc/chef/environments/${environment}.json
    permissions: '0644'
    owner: 'root'
    group: 'root'
    content: |
      {
        "name": "${environment}",
        "default_attributes": {
          "citadel": {
            "bucket": "${company}-${application}-${environment}",
            "region": "us-west-1"
          }
        },
        "json_class": "Chef::Environment",
        "description": "${environment} environment",
        "chef_type": "environment"
      }
  - path: /etc/chef/client.rb
    permissions: '0644'
    content: |
      log_level :info
      log_location '/var/log/chef/client.log'
      ssl_verify_mode        :verify_none
      cookbook_path '/etc/chef/cookbooks'
      node_path '/etc/chef/nodes'
      role_path '/etc/chef/roles'
      data_bag_path '/etc/chef/data_bags'
      environment_path '/etc/chef/environments'
      local_mode 'true'
runcmd:
  - echo "Installing Chef"
  - curl -L https://www.chef.io/chef/install.sh | bash -s -- -v 12.19.36
  - mkdir -p /var/log/chef /etc/chef
  - echo "Configuring Chef"
  - aws --region us-west-1 s3 sync s3://${chef_bucket}/${application}/config /etc/chef/
  - echo "Initial Chef run"
  - python /etc/chef/rename_instance
  - /etc/chef/sync_cookbooks.sh
  - /etc/chef/perform_chef_run.sh
