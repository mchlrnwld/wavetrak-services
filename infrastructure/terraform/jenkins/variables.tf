variable "company" {
  default = "sl"
}

variable "application" {
  default = "jenkins"
}

variable "environment" {
}

variable "default_vpc" {
}

variable "availability_zones" {
}

variable "internal_sg_group" {
}

variable "instance_subnets" {
}

variable "state_bucket" {
}

variable "state_key" {
}

variable "key_name" {
}

variable "instance_type" {
}

variable "sg_jenkins" {
}

variable "chef_bucket" {
}

variable "dereg_topic" {
}

variable "ami_id" {
}

variable "jenkins_ebs_volume_tag" {
}
