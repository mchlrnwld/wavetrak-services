provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "jenkins/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "jenkins" {
  source = "../../"

  company     = "sl"
  application = "jenkins"
  environment = "staging"
  default_vpc = "vpc-981887fd"
  availability_zones = [
    "us-west-1b",
    "us-west-1c",
  ]
  internal_sg_group = "sg-90aeaef5"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab",
  ]
  state_bucket           = "sl-tf-state-staging"
  state_key              = "jenkins/staging/terraform.tfstate"
  key_name               = "sturdy-surfline-dev"
  instance_type          = "t2.medium"
  sg_jenkins             = "sg-3560ec51"
  chef_bucket            = "sl-chef-staging"
  dereg_topic            = "arn:aws:sns:us-west-1:665294954271:Sturdy-SAND"
  ami_id                 = "ami-b388b4d3"
  jenkins_ebs_volume_tag = "wt-jenkins-master-2-staging"
}
