provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jenkins/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "jenkins" {
  source = "../../"

  company     = "sl"
  application = "jenkins"
  environment = "prod"
  default_vpc = "vpc-116fdb74"
  availability_zones = [
    "us-west-1b",
    "us-west-1c",
  ]
  internal_sg_group = "sg-a5a0e5c0"
  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0",
  ]
  state_bucket           = "sl-tf-state-prod"
  state_key              = "jenkins/prod/terraform.tfstate"
  key_name               = "sturdy_surfline_id_rsa"
  instance_type          = "m4.large"
  sg_jenkins             = "sg-3560ec51"
  chef_bucket            = "sl-chef-prod"
  dereg_topic            = "arn:aws:sns:us-west-1:833713747344:Sturdy-SAND"
  ami_id                 = "ami-b388b4d3"
  jenkins_ebs_volume_tag = "wt-jenkins-master-2-prod"
}
