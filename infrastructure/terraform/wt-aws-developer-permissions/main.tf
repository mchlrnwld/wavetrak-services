# Common developer permissions
data "template_file" "developer_common" {
  template = templatefile("${path.module}/resources/developer-common.json", {})
}

# Developer boundary permissions
data "template_file" "developer_boundary" {
  template = templatefile("${path.module}/resources/developer-boundary.json", {})
}

resource "aws_iam_policy" "developer_boundary" {
  name        = "wt-developer-boundary"
  description = "Boundary permission set to limit developer actions."
  policy      = data.template_file.developer_boundary.rendered
}

resource "aws_iam_role" "developer_common_role" {
  name                 = "developer-common"
  assume_role_policy   = templatefile("${path.module}/resources/developer-common.json", {})
  permissions_boundary = aws_iam_policy.developer_boundary.arn

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Service     = "wt-aws-developer-permissions"
    Terraform   = "true"
    Source      = "Surfline/wavetrak-infrastructure/terraform/wt-aws-developer-permissions"
  }
}
