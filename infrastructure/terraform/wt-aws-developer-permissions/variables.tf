variable "environment" {
  default = "prod"
}

variable "application" {
  default = "wt-aws-developer-permissions"
}

variable "company" {
  default = "wt"
}
