provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wt-aws-developer-permissions/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "newrelic_events" {
  source = "../../"
}
