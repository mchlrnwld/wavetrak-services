<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Wavetrak permissions module](#wavetrak-permissions-module)
  - [Summary](#summary)
  - [Philosophy](#philosophy)
    - [Requesting additional permissions](#requesting-additional-permissions)
  - [Usage](#usage)
  - [Naming Conventions](#naming-conventions)
  - [Terraform](#terraform)
    - [Using Terraform](#using-terraform)
  - [State Management](#state-management)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Wavetrak permissions module

## Summary

This terraform module creates IAM permissions for developers in production.

## Philosophy 

Developer permissions in the `prod` AWS account should be broad enough to allow a frictionless developer workflow, while still blocking access to  services / critical resources that developers don't need to access. This limits our exposure to operational and security risk, and standardizes developers access.

Permissions will not be as restricted in the `dev` AWS account, where developers can have pseudo-admin access.       

### Requesting additional permissions

It is possible that developers may require expanded access to `prod` resources / services at some point. If so, they should contact the IT manager or System Administrator. 

## Usage

There are two policies: the permissions policy and the boundary policy.

The permissions policy explicitly blocks access to resources / services that fall outside typical developers scope. The boundary permission policy prevents privilege escalation.

When creating new users or roles, all developers with this permission set will need to add the boundary permission to new users / roles. The resource creation will fail otherwise.

## Naming Conventions

See [terraform styleguide](https://github.com/Surfline/styleguide/tree/master/terraform).

## Terraform

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=prod make plan
ENV=prod make apply
```

Valid environment is only `prod`.

## State Management

The `.tfstate` file is stored in S3. Future plans could use other providers such as Artifactory.