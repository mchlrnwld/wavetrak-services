terraform {
  required_providers {
    aws = "~> 2.70"
  }
  required_version = ">= 0.13"
}
