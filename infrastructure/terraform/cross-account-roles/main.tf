module "dns_management_cross_account_role" {
  source = "github.com/Surfline/terraform-aws-cross-account-role?ref=1.0.0"
  name   = "${var.company}-${var.application}-dns-mgmt-${var.environment}"

  principal_arns = [
    # surfline_legacy_account_id is used for creation of wowza route53 records
    var.surfline_legacy_account_id,

    # surfline_dev_account_id is used for creation of dev-airflow route53 records
    var.surfline_dev_account_id,

    # surfline_prod_account_id is used for creation of prod-airflow route53 records
    var.surfline_prod_account_id,
  ]

  policy_arns = [aws_iam_policy.dns_management_policy.arn]
}

data "aws_route53_zone" "aws_surfline_com" {
  name         = var.dns_zone_name
  private_zone = false
}

resource "aws_iam_policy" "dns_management_policy" {
  name        = "${var.company}-${var.application}-dns-mgmt-policy-${var.environment}"
  path        = "/"
  description = "Allows cross-account management of DNS for ${var.dns_zone_name}"
  policy = templatefile("${path.module}/resources/route53-policy.json", {
    zone_id = data.aws_route53_zone.aws_surfline_com.zone_id
  })
}
