# Cross account roles infrastructure

- [Cross account roles infrastructure](#cross-account-roles-infrastructure)
  - [Sumary](#sumary)
  - [Naming Conventions](#naming-conventions)
  - [Terraform](#terraform)
    - [Using Terraform](#using-terraform)
      - [State Management](#state-management)

## Sumary

This is a general-purpose `cross-account-roles` stack. It allows roles to be created in AWS accounts and then associated with other accounts that may assume the roles.

It makes use of the [terraform-aws-cross-account-role](https://github.com/Surfline/terraform-aws-cross-account-role) module in Github.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.0`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=surfline-prod make plan
ENV=surfline-prod make apply
```

Valid environment is `surfline-prod`, although other environments may be added. Use the AWS account name as the name of the environment.

#### State Management

State is uploaded to S3 after `make setup` and `make apply` are run. State details can be found in each environment's `main.tf` file.
