provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "cross-account-roles/surfline-prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cross-account-roles" {
  source = "../../"

  company     = "sl"
  application = "cross-account-roles"
  environment = "prod"

  dns_zone_name              = "aws.surfline.com."
  surfline_dev_account_id    = "665294954271"
  surfline_legacy_account_id = "094422976157"
  surfline_prod_account_id   = "833713747344"
}
