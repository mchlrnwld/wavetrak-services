variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "dns_zone_name" {
}

variable "surfline_dev_account_id" {
}

variable "surfline_legacy_account_id" {
}

variable "surfline_prod_account_id" {
}
