variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "alb_name" {
}

variable "sql_action" {
  default = "COUNT"
}
