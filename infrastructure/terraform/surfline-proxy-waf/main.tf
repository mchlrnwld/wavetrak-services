data "aws_alb" "alb" {
  name = var.alb_name
}

resource "aws_wafregional_sql_injection_match_set" "sql_set" {
  name = "${var.company}-${var.application}-${var.environment}-sqlmatchset"
  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "QUERY_STRING"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "QUERY_STRING"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "BODY"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "BODY"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "URI"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "URI"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "HEADER"
      data = "Cookie"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "HEADER"
      data = "Cookie"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "HEADER"
      data = "Authorization"
    }
  }
  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "HEADER"
      data = "Authorization"
    }
  }
}

resource "aws_wafregional_rule" "sql_rule" {
  name        = "${var.company}-${var.application}-${var.environment}-sqlrule"
  metric_name = "${var.environment}WAFSqlRule"

  predicate {
    type    = "SqlInjectionMatch"
    data_id = aws_wafregional_sql_injection_match_set.sql_set.id
    negated = false
  }
}

resource "aws_wafregional_web_acl" "waf_acl" {
  name        = "${var.company}-${var.application}-${var.environment}-acl"
  metric_name = "${var.environment}WafACL"
  default_action {
    type = "ALLOW"
  }
  rule {
    action {
      type = var.sql_action
    }
    priority = 1
    rule_id  = aws_wafregional_rule.sql_rule.id
  }
}

resource "aws_wafregional_web_acl_association" "alb_waf" {
  resource_arn = data.aws_alb.alb.arn
  web_acl_id   = aws_wafregional_web_acl.waf_acl.id
}
