provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-proxy-waf/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

#Service WAF
module "service-proxy-waf" {
  source = "../../"

  company     = "sl"
  application = "service-waf"
  environment = "staging"

  alb_name   = "sl-ecs-services-proxy-staging"
  sql_action = "COUNT"
}

#Web WAF
module "web-proxy-waf" {
  source = "../../"

  company     = "sl"
  application = "web-waf"
  environment = "staging"

  alb_name   = "sl-ecs-web-proxy-int-staging"
  sql_action = "COUNT"
}
