provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-proxy-waf/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

#Service WAF
module "service-proxy-waf" {
  source = "../../"

  company     = "sl"
  application = "service-waf"
  environment = "sbox"

  alb_name   = "sl-ecs-services-proxy-sandbox"
  sql_action = "COUNT"
}

#Web WAF
module "web-proxy-waf" {
  source = "../../"

  company     = "sl"
  application = "web-waf"
  environment = "sbox"

  alb_name   = "sl-ecs-web-proxy-int-sandbox"
  sql_action = "COUNT"
}
