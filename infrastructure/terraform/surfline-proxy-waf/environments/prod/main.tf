provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-proxy-waf/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

#Service WAF
module "service-proxy-waf" {
  source = "../../"

  company     = "sl"
  application = "service-waf"
  environment = "prod"

  alb_name   = "sl-ecs-services-proxy-prod"
  sql_action = "COUNT"
}

#Web WAF
module "web-proxy-waf" {
  source = "../../"

  company     = "sl"
  application = "web-waf"
  environment = "prod"

  alb_name   = "sl-ecs-web-proxy-int-prod"
  sql_action = "COUNT"
}
