# Cam thumbnail generation

- [Cam thumbnail generation](#cam-thumbnail-generation)
  - [Usage](#usage)
    - [Lambda placeholder zip](#lambda-placeholder-zip)
  - [Setup](#setup)
  - [Deploying to a AWS environment](#deploying-to-a-aws-environment)
  - [Terraform / AWS specifics](#terraform--aws-specifics)
    - [Naming Conventions](#naming-conventions)
    - [State Management](#state-management)
    - [Modules](#modules)
    - [Further Reading](#further-reading)

## Usage

### Lambda placeholder zip

Lambdas with s3 bucket sources need a zip in place before deploying. If you destroy the target configuration, you may have to re-deploy the lambda function zip.

`make lambdazip` zips up a placeholder lambda function for test deployment

Warning: When you deploy a real version of the zip, terraform may attempt to re-write the zip during config changes. Perhaps we can add logic like `if !lambda.zip then addzip`.

## Setup

- `export ENV=staging` Configures your target environment choice
- `make setup` Configures your local terraform's state to match your target environment
- `make plan` Performs a dry run so you can verify changes
- `make apply` Applies your changes

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)

```bash
make plan
make apply
```

## Terraform / AWS specifics

### Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

### State Management

For now keep each environment `.tfstate` in S3. Future plans could use other providers such as Artifactory.

All state files are stored in their corresponding S3 bucket.

### Modules

https://www.terraform.io/docs/modules/usage.html (caveat all resources are prefixed with `module.`)

### Further Reading

- https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/
- https://charity.wtf/2016/02/23/two-weeks-with-terraform/
- http://blog.mattiasgees.be/2015/07/29/terraform-remote-state/
- http://code.hootsuite.com/how-to-use-terraform-and-remote-state-with-s3/
