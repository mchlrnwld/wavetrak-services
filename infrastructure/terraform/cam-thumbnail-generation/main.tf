provider "aws" {
  region = var.region
}

module "sns_cam_thumbnail_generation" {
  source = "./modules/sns/"

  topic_name  = "cam_thumbnail_generation_request"
  environment = var.environment
  region      = var.region
}

# S3
resource "aws_s3_bucket" "lambda-healthcheck-zip" {
  bucket = "${var.company}-lambda-healthcheck-zip-${var.environment}"
}

resource "aws_s3_bucket" "cam-thumbnails" {
  bucket = "${var.company}-cam-thumbnails-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 86400 // 1 day
  }
}

# Placeholder zips for lambda deployment
resource "aws_s3_bucket_object" "consumer-zip" {
  bucket = aws_s3_bucket.lambda-healthcheck-zip.bucket
  key    = var.consumer_zip
  source = "./resources/${var.consumer_zip}"
}

resource "aws_s3_bucket_object" "producer-zip" {
  bucket = aws_s3_bucket.lambda-healthcheck-zip.bucket
  key    = var.producer_zip
  source = "./resources/${var.producer_zip}"
}

# Cloudwatch
resource "aws_cloudwatch_event_rule" "thumbnail_generation_rate" {
  name                = "thumbnail-generation-rate"
  description         = "Fires to generate new thumbnails"
  schedule_expression = var.thumbnail_generation_rate_expression
}

resource "aws_cloudwatch_event_target" "cam_thumbnail_producer_thumbnail_generation_rate" {
  rule      = aws_cloudwatch_event_rule.thumbnail_generation_rate.name
  target_id = "cam-thumbnail-producer-lambda"
  arn       = aws_lambda_function.cam-thumbnail-producer-lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_cam-thumbnail-producer-lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.cam-thumbnail-producer-lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.thumbnail_generation_rate.arn
}

# Lambda
resource "aws_lambda_function" "cam-thumbnail-producer-lambda" {
  function_name = "${var.company}-cam-thumbnail-producer-lambda-${var.environment}"
  runtime       = "python2.7"
  timeout       = 120
  s3_bucket     = aws_s3_bucket.lambda-healthcheck-zip.bucket
  s3_key        = var.producer_zip
  handler       = "cam_thumb_producer.lambda_handler"
  role          = aws_iam_role.lambda_role.arn
  memory_size   = 1024

  vpc_config {
    subnet_ids         = var.instance_subnets
    security_group_ids = var.security_group_ids
  }

  environment {
    variables = {
      camThumbnailSnsArn = module.sns_cam_thumbnail_generation.topic
      CAMERA_API         = var.camera_api
    }
  }
}

resource "aws_lambda_function" "cam-thumbnail-consumer-lambda" {
  depends_on    = [module.sns_cam_thumbnail_generation]
  function_name = "${var.company}-cam-thumbnail-consumer-lambda-${var.environment}"
  runtime       = "python2.7"
  timeout       = 120
  s3_bucket     = aws_s3_bucket.lambda-healthcheck-zip.bucket
  s3_key        = var.consumer_zip
  handler       = "cam_thumb_consumer.lambda_handler"
  role          = aws_iam_role.lambda_role.arn
  memory_size   = 1024

  vpc_config {
    subnet_ids         = var.instance_subnets
    security_group_ids = var.security_group_ids
  }

  environment {
    variables = {
      camThumbnailBucket = aws_s3_bucket.cam-thumbnails.bucket
    }
  }
}

resource "aws_sns_topic_subscription" "cam_thumbnail_consumer_subscription" {
  depends_on = [
    aws_lambda_function.cam-thumbnail-consumer-lambda,
    module.sns_cam_thumbnail_generation,
  ]
  topic_arn = module.sns_cam_thumbnail_generation.topic
  protocol  = "lambda"
  endpoint  = aws_lambda_function.cam-thumbnail-consumer-lambda.arn
}

resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-lambda-${var.application}-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/lambda-policy.json", {})
}

resource "aws_iam_role_policy_attachment" "lambda_execution_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda_sns_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = module.sns_cam_thumbnail_generation.topic_policy
}

resource "aws_iam_role_policy_attachment" "lambda_s3_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.s3_policy.arn
}

resource "aws_lambda_permission" "with_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.cam-thumbnail-consumer-lambda.arn
  principal     = "sns.amazonaws.com"
  source_arn    = module.sns_cam_thumbnail_generation.topic
}

resource "aws_iam_policy" "s3_policy" {
  name        = "s3_policy_cam-thumbnails_${var.environment}"
  description = "policy to let lambda publish to s3"
  policy = templatefile("${path.module}/resources/s3-write-policy.json", {
    s3_bucket_arn = aws_s3_bucket.cam-thumbnails.arn
  })
}

# S3 bucket policy for CloudFront access
resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.cam-thumbnails.id
  policy = templatefile("${path.module}/resources/s3-read-policy-${var.environment == "prod" ? "prod" : "dev"}.json", {
    s3_bucket_arn = aws_s3_bucket.cam-thumbnails.arn
    batch_operation_role = var.batch_operation_role
  })
}

resource "aws_cloudfront_distribution" "cam_stills_s3_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.cam-thumbnails.bucket}.s3.amazonaws.com"
    origin_id   = "S3-camThumbnailOrigin"
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = "Surfline camera stills"

  aliases = [var.cam_stills_domain_name]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-camThumbnailOrigin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 900   // 15 minutes
    default_ttl            = 900   // 15 minutes
    max_ttl                = 86400 // 1 day
  }

  ordered_cache_behavior {
    path_pattern     = "*latest*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-camThumbnailOrigin"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 300  // 5 minutes
    default_ttl            = 300  // 5 minutes
    max_ttl                = 3600 // 1 hour
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment    = var.environment
    Name           = "sl-cam-thumbnails-${var.environment}"
    category       = "cdn"
    subcategory    = "thumbnails"
    subsubcategory = "cams"
  }

  viewer_certificate {
    acm_certificate_arn      = var.cam_stills_acm_cert
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

resource "aws_route53_record" "cam_stills_domain" {
  zone_id = var.alias_hosted_zone_id
  name    = var.cam_stills_domain_name
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.cam_stills_s3_distribution.domain_name
    zone_id                = var.cloudfront_zone_id
    evaluate_target_health = false // Cannot set to true for cloudfront.
  }
}
