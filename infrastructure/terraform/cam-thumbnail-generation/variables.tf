variable "company" {
  default = "sl"
}

variable "application" {
  default = "cam-thumbnail-generation"
}

variable "environment" {
  default = "staging"
}

variable "region" {
  default = "us-west-1"
}

variable "producer_zip" {
  default = "cam-thumbnail-producer-lambda.zip"
}

variable "consumer_zip" {
  default = "cam-thumbnail-consumer-lambda.zip"
}

variable "security_group_ids" {
  default = ["sg-91aeaef4", "sg-90aeaef5"]
}

variable "instance_subnets" {
  default = ["subnet-0909466c", "subnet-f2d458ab"]
}

variable "key_name" {
  default = "sturdy-surfline-dev"
}

variable "alias_hosted_zone_id" {
  default = "Z3JHKQ8ELQG5UE"
}

variable "cloudfront_zone_id" {
  default = "Z2FDTNDATAQYW2"
}

variable "camera_api" {
  default = "http://cameras-service.staging.surfline.com"
}

variable "cam_stills_acm_cert" {
  default = "arn:aws:acm:us-east-1:665294954271:certificate/07c4d79f-1151-42ed-bb62-5ea7c0f93208"
}

variable "cam_stills_domain_name" {
  default = "cam-stills.staging.surfline.com"
}

variable "thumbnail_generation_rate_expression" {
  default = "rate(1 day)"
}

variable "batch_operation_role" {
  default = ""
}
