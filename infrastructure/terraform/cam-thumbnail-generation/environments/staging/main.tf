provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "cam-thumbnail-generation/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam-thumbnail-generation" {
  source = "../../"

  company     = "sl"
  application = "cam-thumbnail"
  environment = "staging"
  region      = "us-west-1"

  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]
  security_group_ids = [
    "sg-91aeaef4",
    "sg-90aeaef5"
  ]

  thumbnail_generation_rate_expression = "rate(1 day)"

  alias_hosted_zone_id   = "Z3JHKQ8ELQG5UE"
  cam_stills_domain_name = "cam-stills.staging.surfline.com"
}
