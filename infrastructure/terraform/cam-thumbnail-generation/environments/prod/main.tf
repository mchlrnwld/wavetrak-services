provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "cam-thumbnail-generation/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam-thumbnail-generation" {
  source = "../../"

  company     = "sl"
  application = "cam-thumbnail"
  environment = "prod"
  region      = "us-west-1"

  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0"
  ]
  security_group_ids = [
    "sg-a4a0e5c1",
    "sg-a5a0e5c0"
  ]

  thumbnail_generation_rate_expression = "rate(10 minutes)"

  alias_hosted_zone_id = "Z1OCTVEV5XMNUZ"
  camera_api           = "http://cameras-service.prod.surfline.com"

  cam_stills_domain_name = "camstills.cdn-surfline.com"
  cam_stills_acm_cert    = "arn:aws:acm:us-east-1:833713747344:certificate/522318ff-4e5b-4642-b217-a932bc4df082"

  batch_operation_role = "arn:aws:iam::665294954271:role/prototype-ml-rerun-batch-operation-role"
}
