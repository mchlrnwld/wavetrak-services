provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-lola-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "lola_cdn" {
  source = "../../"

  environment = "prod"

  cdn_acm_domains = [
    "cdn-surfline.com"
  ]
  cdn_fqdn = {
    "cdn-surfline.com" = [
      "lola.cdn-surfline.com",
    ]
  }

  default_ttl       = 300
  max_ttl           = 300
  whitelist_headers = ["Origin"]
}

data "aws_route53_zone" "cdn" {
  name = "cdn-surfline.com."
}

resource "aws_route53_record" "lola" {
  zone_id = data.aws_route53_zone.cdn.zone_id
  name    = "lola"
  type    = "CNAME"
  records = module.lola_cdn.domain_name
  ttl     = 60
}
