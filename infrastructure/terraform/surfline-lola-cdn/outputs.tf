output "domain_name" {
  value = module.surfline-lola-cdn.domain_name
}

output "domain_alias" {
  value = module.surfline-lola-cdn.domain_alias
}

output "hosted_zone_id" {
  value = module.surfline-lola-cdn.hosted_zone_id
}
