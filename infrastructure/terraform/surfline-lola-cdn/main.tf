module "surfline-lola-cdn" {
  source             = "../modules/aws/cloudfront-with-s3"
  company            = "surfline"
  environment        = var.environment
  application        = "lola"
  service            = "cdn"
  cdn_acm_count      = length(var.cdn_acm_domains)
  cdn_acm_domains    = var.cdn_acm_domains
  comment            = "${var.environment} Surfline LOLA CDN wth S3"
  cdn_fqdn           = var.cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
  default_ttl        = var.default_ttl
  max_ttl            = var.max_ttl
  whitelist_headers  = var.whitelist_headers
}
