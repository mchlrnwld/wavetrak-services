variable "environment" {
}

variable "cdn_acm_domains" {
  type = list(string)
}

variable "cdn_fqdn" {
  type = map(list(string))
}

variable "default_ttl" {
}

variable "max_ttl" {
}

variable "whitelist_headers" {
  type = list(string)
}
