provider "aws" {
  region = var.region
}

resource "aws_cloudfront_distribution" "tile_cdn" {
  origin {
    domain_name = var.origin_domain
    origin_id   = "origin-${var.application}-${var.environment}"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols   = ["SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment
  aliases         = [var.cdn_fqdn]

  logging_config {
    include_cookies = false
    bucket          = var.data_lake_bucket
    prefix          = "infrastructure/cloudfront"
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "origin-${var.application}-${var.environment}"

    forwarded_values {
      query_string = true
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = var.default_ttl
    default_ttl            = var.default_ttl
    max_ttl                = var.default_ttl
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
    Terraform   = "true"
  }

  viewer_certificate {
    acm_certificate_arn      = var.cdn_acm
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

resource "aws_route53_record" "tile_cdn" {
  zone_id = var.alias_hosted_zone_id
  name    = var.cdn_fqdn
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.tile_cdn.domain_name
    zone_id                = aws_cloudfront_distribution.tile_cdn.hosted_zone_id
    evaluate_target_health = false
  }
}
