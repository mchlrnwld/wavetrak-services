variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "region" {
}

variable "service" {
}

variable "comment" {
}

variable "origin_domain" {
}

variable "default_ttl" {
}

variable "cdn_acm" {
}

variable "cdn_fqdn" {
}

variable "alias_hosted_zone_id" {
}

variable "data_lake_bucket" {
}
