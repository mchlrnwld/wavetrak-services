provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-tile-cdn/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "tile-server-cdn" {
  source = "../../"

  company              = "sl"
  application          = "surfline-tile-cdn"
  environment          = "staging"
  region               = "us-west-1"
  service              = "tile-cdn"
  comment              = "Tile CDN Cloudfront Distribution"
  origin_domain        = "tile-origin.surfline.com"
  default_ttl          = 300
  cdn_acm              = "arn:aws:acm:us-east-1:665294954271:certificate/07c4d79f-1151-42ed-bb62-5ea7c0f93208"
  cdn_fqdn             = "tile-cdn.staging.surfline.com"
  alias_hosted_zone_id = "Z3JHKQ8ELQG5UE"
  data_lake_bucket     = "wt-data-lake-dev.s3.amazonaws.com"
}
