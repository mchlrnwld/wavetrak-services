provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-tile-cdn/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "tile-server-cdn" {
  source = "../../"

  company              = "sl"
  application          = "surfline-tile-cdn"
  environment          = "sbox"
  region               = "us-west-1"
  service              = "tile-cdn"
  comment              = "Tile CDN Cloudfront Distribution"
  origin_domain        = "tile-origin.surfline.com"
  default_ttl          = 300
  cdn_acm              = "arn:aws:acm:us-east-1:665294954271:certificate/a48acac9-c364-4186-b33a-73543e5f72c0"
  cdn_fqdn             = "tile-cdn.sandbox.surfline.com"
  alias_hosted_zone_id = "Z3DM6R3JR1RYXV"
  data_lake_bucket     = "wt-data-lake-dev.s3.amazonaws.com"
}
