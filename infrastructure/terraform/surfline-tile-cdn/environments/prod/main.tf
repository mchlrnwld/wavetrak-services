provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-tile-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "tile-server-cdn" {
  source = "../../"

  company              = "sl"
  application          = "surfline-tile-cdn"
  environment          = "prod"
  region               = "us-west-1"
  service              = "tile-cdn"
  comment              = "Tile CDN Cloudfront Distribution"
  origin_domain        = "tile-origin.surfline.com"
  default_ttl          = 300
  cdn_acm              = "arn:aws:acm:us-east-1:833713747344:certificate/0c9db677-e92e-4455-b241-2ada4520d52e"
  cdn_fqdn             = "tile.surfline.com"
  alias_hosted_zone_id = "ZY7MYOQ65TY5X"
  data_lake_bucket     = "wt-data-lake-prod.s3.amazonaws.com"
}
