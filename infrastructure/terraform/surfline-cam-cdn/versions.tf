terraform {
  required_version = ">= 0.12"
  required_providers {
    archive  = "~> 1.3"
    aws      = "~> 2.70"
    null     = "~> 2.1"
    template = "~> 2.1"
  }
}
