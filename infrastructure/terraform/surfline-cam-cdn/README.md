# Surfline Cam CDN

- [Surfline Cam CDN](#surfline-cam-cdn)
  - [Summary](#summary)
  - [Wowza Naming Conventions](#wowza-naming-conventions)
  - [Wowza Structure](#wowza-structure)
  - [Adding a new transcoder instance](#adding-a-new-transcoder-instance)
    - [Manual step](#manual-step)
    - [Automated step (goal)](#automated-step-goal)
  - [Resource Naming Conventions](#resource-naming-conventions)
  - [Deploying to a AWS environment](#deploying-to-a-aws-environment)
    - [State Management](#state-management)
    - [Modules](#modules)
  - [Further Reading](#further-reading)

## Summary

This Terraform stack is deployed in the legacy account alongside the other `cam` resources:

- There is only a `prod` env for the cams.
- Camera infrastructure is at AWS legacy env.

> _Terraform Version: 0.12.28_

## Wowza Naming Conventions

- Ingest ( sl-wowza-encoder-(ec|wc) )

  - wsc-west / wsc-east
  - paywall-west / paywall-east
  - other-xxxx (msw cams that not show on surfline, other partner webcams)

- Transcoder ( sl-wowza-transcoder-(#) )

  - paywall-#

## Wowza Structure

- Camera (RTSP) -> Ingest (encoder) -> Transcoder and Recorder -> CloudFront

## Adding a new transcoder instance

### Manual step

- Add a new CloudFormation instance and afterwards add block of terraform for origin / behavioral routes.
- Create script to add a new route to terraform via a fragment and run terraform plan.

### Automated step (goal)

- Add new camera to CMS, mark it as "ABR Default Profile (1080p, 720p, 480p, 360p)" or just "ABR"
- That CMS allocation triggers a transcode slot allocation
  - If all transcode slots are filled on all instances, a new instance is provisioned (terraform) and a slot is allocated, otherwise it's allocated to an existing instance
    - UserData init scripts? Register and get instance information from a registration endpoint on camera-service
    - Automate Terraform applies for new origins and routing behaviors
- After a transcode slot is allocated via Wowza's REST API, the allocation service populates the playlist attribute on the camera object and optionally notifies the initiating user

## Resource Naming Conventions

See [AWS Naming Conventions](https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions).

## Deploying to a AWS environment

Ensure that AWS environment is set to `legacy`.

Ensure that `ENV` is set to the proper environment (`prod`).

```shell
export ENV=prod
```

**NOTE**: The Route53 record for the stack is created manually in the `prod` account. As We do not allow cross account updates form `legacy -> prod`.

Check the implications of your change with:

```shell
make plan
```

Apply changes upstream:

```shell
make apply
```

This stack create a security group that will need to be attached to the Wowza encoders. The security group is in the output section at the bottom of the `make apply`.

After the `apply` is done, create an alias record for `cams.cdn-surfline.com` -> the Cloudfront Distribution created above. You can use the below `output` which will be at the bottom of the `make apply`. You will also need to attach the created security group to the wowza encoder server(s). This is done manually as the resources are created outside of teraform.

```shell
State path:

Outputs:

cloudfront_distribution_fqdn = d1uyivzyvl03yl.cloudfront.net
wowza_cf_sg = sg-aea9afc9
```

When you deploy this stack you need to subscribe to the AWS IP change SNS topic with the cli using:

```shell
aws sns subscribe --topic-arn arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged --protocol lambda --notification-endpoint <Lambda ARN>
```

So in the example above `cams.cdn-surfline.com` -> `d1uyivzyvl03yl.cloudfront.net`.
And `sg-aea9afc9` needs to be attached to the WC & EC encoders.

### State Management

For now keep each environment `.tfstate` in S3.

### Modules

https://www.terraform.io/docs/modules/usage.html (caveat all resources are prefixed with `module.`)

## Further Reading

- https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/
- https://charity.wtf/2016/02/23/two-weeks-with-terraform/
- http://blog.mattiasgees.be/2015/07/29/terraform-remote-state/
- http://code.hootsuite.com/how-to-use-terraform-and-remote-state-with-s3/
- https://aws.amazon.com/blogs/security/how-to-automatically-update-your-security-groups-for-amazon-cloudfront-and-aws-waf-by-using-aws-lambda/
