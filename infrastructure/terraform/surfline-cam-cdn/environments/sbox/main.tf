provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-cam-cdn/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  application = "cam-cdn"
  company     = "sl"
  environment = "sbox"
  service     = "cdn"

  services_origin_fqdn = "services.staging.surfline.com"
  default_origin_fqdn  = "sl-wowza-encoder-wc-prod-stream-2112071942.us-west-1.elb.amazonaws.com"
  wowza_map = {
    "/abr-0/*"    = "sl-wowza-transcoder-1-prod-st-1852543002.us-west-1.elb.amazonaws.com",
    "/abr-1/*"    = "sl-wowza-transcoder-2-prod-st-718473076.us-west-1.elb.amazonaws.com",
    "/abr-2/*"    = "sl-wowza-transcoder-3-prod-st-2139830758.us-west-1.elb.amazonaws.com",
    "/abr-3/*"    = "sl-wowza-transcoder-4-prod-st-1488850646.us-west-1.elb.amazonaws.com",
    "/wsc-east/*" = "sl-wowza-encoder-ec-prod-stream-293970049.us-west-1.elb.amazonaws.com",
    "/wsc-west/*" = "sl-wowza-encoder-wc-prod-stream-2112071942.us-west-1.elb.amazonaws.com"
  }
  wowza_secure_map = {
    "/east-secure/*" = "sl-wowza-encoder-ec-prod-stream-293970049.us-west-1.elb.amazonaws.com",
    "/west-secure/*" = "sl-wowza-encoder-wc-prod-stream-2112071942.us-west-1.elb.amazonaws.com"
  }
  price_class = "PriceClass_100"
}

module "cam-cdn" {
  source = "../../modules/http-origin-cdn"

  application = local.application
  company     = local.company
  environment = local.environment
  service     = local.service

  versioning_enabled = false

  comment                 = "sandbox/staging surfline cam cdn"
  cdn_fqdn                = "cams-staging.cdn-surfline.com"
  cdn_ssl_certificate_arn = "arn:aws:acm:us-east-1:665294954271:certificate/ccf3e272-f524-4d06-a4ef-a0191ed6bb05"

  services_origin_fqdn = local.services_origin_fqdn
  default_origin_fqdn  = local.default_origin_fqdn
  wowza_map            = local.wowza_map
  wowza_secure_map     = local.wowza_secure_map

  price_class = local.price_class
  web_acl_id  = ""
}

module "secure-cam-cdn" {
  source = "../../modules/http-origin-cdn"

  application = local.application
  company     = local.company
  environment = local.environment
  service     = local.service

  versioning_enabled = false

  comment                 = "sandbox/staging secure cam cdn"
  cdn_fqdn                = "cams-cdn.staging.surfline.com"
  cdn_ssl_certificate_arn = "arn:aws:acm:us-east-1:665294954271:certificate/07c4d79f-1151-42ed-bb62-5ea7c0f93208"

  services_origin_fqdn = local.services_origin_fqdn
  default_origin_fqdn  = local.default_origin_fqdn
  wowza_map            = local.wowza_map
  wowza_secure_map     = local.wowza_secure_map

  price_class = local.price_class
  web_acl_id  = ""
}

module "secure-cam-cdn-dns" {
  source = "../../modules/secure-cam-cdn-dns"

  distribution_dns_name       = module.secure-cam-cdn.domain_name
  distribution_hosted_zone_id = module.secure-cam-cdn.hosted_zone_id
  secure_cam_cdn_dns_record   = "cams-cdn.staging.surfline.com"
  internal_dns_zone_id        = "Z3JHKQ8ELQG5UE"
}

module "surfline-cam-cdn" {
  source = "../../"

  application = local.application
  company     = local.company
  environment = local.environment
  region      = "us-west-1"

  account_id  = "665294954271"
  vpc_id      = "vpc-981887fd"
  allowed_sg  = "sl-sbox-wowza_cf_sg"
  domain_name = module.cam-cdn.domain_name
}
