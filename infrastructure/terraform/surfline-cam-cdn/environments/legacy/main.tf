provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-legacy-tf-state-prod"
    key    = "surfline-cam-cdn/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  application = "cam-cdn"
  company     = "sl"
  environment = "prod"
  service     = "cdn"

  services_origin_fqdn = "services.surfline.com"
  default_origin_fqdn  = "wowza-cdn-ec-stream.aws.surfline.com"
  wowza_map = {
    "/abr-1/*"    = "sl-wowza-transcoder-1-prod-st-1852543002.us-west-1.elb.amazonaws.com",
    "/abr-2/*"    = "sl-wowza-transcoder-2-prod-st-718473076.us-west-1.elb.amazonaws.com",
    "/abr-3/*"    = "sl-wowza-transcoder-3-prod-st-2139830758.us-west-1.elb.amazonaws.com",
    "/abr-4/*"    = "sl-wowza-transcoder-4-prod-st-1488850646.us-west-1.elb.amazonaws.com",
    "/cdn-au/*"   = "wowza-cdn-au-stream.aws.surfline.com"
    "/cdn-ec/*"   = "wowza-cdn-ec-stream.aws.surfline.com",
    "/cdn-int/*"  = "wowza-cdn-int-stream.aws.surfline.com",
    "/cdn-wc/*"   = "wowza-cdn-wc-stream.aws.surfline.com",
    "/wsc-east/*" = "sl-wowza-encoder-ec-prod-stream-293970049.us-west-1.elb.amazonaws.com",
    "/wsc-west/*" = "sl-wowza-encoder-wc-prod-stream-2112071942.us-west-1.elb.amazonaws.com"
  }
  wowza_secure_map = {
    "/east-secure/*" = "sl-wowza-encoder-ec-prod-stream-293970049.us-west-1.elb.amazonaws.com",
    "/west-secure/*" = "sl-wowza-encoder-wc-prod-stream-2112071942.us-west-1.elb.amazonaws.com"
  }
  price_class = "PriceClass_100"
}

module "cam-cdn" {
  source = "../../modules/http-origin-cdn"

  application = local.application
  company     = local.company
  environment = local.environment
  service     = local.service

  versioning_enabled = false

  comment                 = "prod surfline cam cdn"
  cdn_fqdn                = "cams.cdn-surfline.com"
  cdn_ssl_certificate_arn = "arn:aws:acm:us-east-1:094422976157:certificate/9ad38508-02a2-4fa0-b266-6e7ce5b34526"

  services_origin_fqdn = local.services_origin_fqdn
  default_origin_fqdn  = local.default_origin_fqdn
  wowza_map            = local.wowza_map
  wowza_secure_map     = local.wowza_secure_map

  web_acl_id = "bfc5edbd-d199-4fd3-983c-9dde765d673a"
}

module "surfline-cam-cdn" {
  source = "../../"

  application = "cam-cdn"
  company     = "sl"
  environment = "prod"
  region      = "us-west-1"
  account_id  = "094422976157"
  vpc_id      = "vpc-29f3f241"
  allowed_sg  = "*"
  domain_name = module.cam-cdn.domain_name
}
