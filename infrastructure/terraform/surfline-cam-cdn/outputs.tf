output "cloudfront_distribution_fqdn" {
  value = var.domain_name
}

output "wowza_cf_sg" {
  value = aws_security_group.wowza_cf_sg.id
}
