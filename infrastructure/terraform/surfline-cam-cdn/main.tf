# Security group
resource "aws_security_group" "wowza_cf_sg" {
  name        = "${var.company}-${var.environment}-wowza_cf_sg"
  description = "Allow all inbound traffic"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "${var.company}-${var.environment}-wowza_cf_sg"
    AutoUpdate = "true"
    Port       = "1935"
  }
}

# Lambda function
# create zip IF the sha of python script changes
resource "null_resource" "lambda" {
  triggers = {
    package_json = filebase64sha256("${path.module}/resources/lambda-update-sg.py")
  }
}

# create zip for use by lambda resource below
data "archive_file" "update_sg" {
  type        = "zip"
  source_dir  = "${path.module}/resources/"
  output_path = "${path.module}/environments/${var.environment}/.terraform/files/update-sg.zip"
  depends_on  = [null_resource.lambda]
}

resource "aws_lambda_function" "cloudfront_sg_updater" {
  runtime          = "python2.7"
  function_name    = "${var.company}-${var.application}-cloudfront_sg_updater-${var.environment}"
  filename         = "${path.module}/environments/${var.environment}/.terraform/files/update-sg.zip"
  source_code_hash = data.archive_file.update_sg.output_base64sha256
  handler          = "update-sg.lambda_handler"
  role             = aws_iam_role.lambda_role.arn
  memory_size      = 128
  timeout          = 30
}

# Lambda Role
resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-lambda-${var.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/lambda-role-policy.json")
}

# Policy to allow updates of Ec2 SG by Lambda
resource "aws_iam_policy" "update_cf_sg_policy" {
  name        = "${var.company}-${var.application}-update-cf-sg-policy-${var.environment}"
  description = "policy to update ec2 sg via lambda"
  policy = templatefile("${path.module}/resources/update-cf-sg-policy.json", {
    account_id = var.account_id
    allowed_sg = var.allowed_sg
    region     = var.region
  })
}

# Update Ec2 SG Attachment
resource "aws_iam_role_policy_attachment" "update_cf_sg_policy_attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.update_cf_sg_policy.arn
}
