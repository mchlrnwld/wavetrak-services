variable "internal_dns_zone_id" {
}

variable "secure_cam_cdn_dns_record" {
}

variable "distribution_dns_name" {
}

variable "distribution_hosted_zone_id" {
}
