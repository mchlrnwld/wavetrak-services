resource "aws_route53_record" "secure-cam-cdn" {
  zone_id = var.internal_dns_zone_id
  name    = var.secure_cam_cdn_dns_record
  type    = "A"

  alias {
    name                   = var.distribution_dns_name
    zone_id                = var.distribution_hosted_zone_id
    evaluate_target_health = true
  }
}
