variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "service" {
}

variable "versioning_enabled" {
}

variable "comment" {
}

variable "cdn_fqdn" {
}

variable "cdn_ssl_certificate_arn" {
}

variable "services_origin_fqdn" {
}

variable "default_origin_fqdn" {
}

variable "wowza_map" {
  type = map(string)
}

variable "wowza_secure_map" {
  type = map(string)
}

variable "web_acl_id" {
}

variable "price_class" {
  default = "PriceClass_All"
}
