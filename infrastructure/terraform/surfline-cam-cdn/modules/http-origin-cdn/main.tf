# CloudFront distribution
resource "aws_cloudfront_distribution" "cdn" {
  default_cache_behavior {
    allowed_methods  = ["HEAD", "GET", "OPTIONS"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "HTTP-${var.default_origin_fqdn}"
    compress         = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "all"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  dynamic "origin" {
    for_each = var.wowza_map
    iterator = it

    content {
      domain_name = it.value
      origin_id   = "HTTP-${it.value}"

      custom_origin_config {
        http_port              = 1935
        https_port             = 443
        origin_protocol_policy = "http-only"
        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }
    }
  }

  origin {
    domain_name = var.services_origin_fqdn
    origin_id   = "HTTPS-services"

    custom_origin_config {
      http_port              = 1935
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = var.comment
  aliases         = [var.cdn_fqdn]
  web_acl_id      = var.web_acl_id

  ordered_cache_behavior {
    allowed_methods  = ["HEAD", "GET", "OPTIONS"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "HTTPS-services"
    compress         = true
    path_pattern     = "/cameras/authorization/*"

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.wowza_map
    iterator = it

    content {
      allowed_methods  = ["HEAD", "GET", "OPTIONS"]
      cached_methods   = ["HEAD", "GET", "OPTIONS"]
      target_origin_id = "HTTP-${it.value}"
      compress         = true
      path_pattern     = it.key

      forwarded_values {
        query_string = false

        cookies {
          forward = "none"
        }
      }

      viewer_protocol_policy = "redirect-to-https"
      min_ttl                = 0
      default_ttl            = 3600
      max_ttl                = 86400
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.wowza_secure_map
    iterator = it

    content {
      allowed_methods  = ["HEAD", "GET", "OPTIONS"]
      cached_methods   = ["HEAD", "GET", "OPTIONS"]
      target_origin_id = "HTTP-${it.value}"
      compress         = true
      path_pattern     = it.key

      forwarded_values {
        query_string = false

        cookies {
          forward = "none"
        }

        headers = ["Origin"]
      }

      trusted_signers        = ["self"]
      viewer_protocol_policy = "redirect-to-https"
      min_ttl                = 0
      default_ttl            = 3600
      max_ttl                = 86400
    }
  }

  price_class = var.price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.service
  }

  viewer_certificate {
    acm_certificate_arn      = var.cdn_ssl_certificate_arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}
