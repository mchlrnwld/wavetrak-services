variable "application" {
}

variable "company" {
}

variable "environment" {
}

variable "region" {
}

variable "account_id" {
}

variable "vpc_id" {
}

variable "allowed_sg" {
}

variable "domain_name" {
}
