variable "environment" {
  description = "Environment (dev/prod) using the S3 bucket."
}

variable "cloudfront" {
  description = "CloudFront ARN that will use the S3 bucket as an origin."
}

variable "lotus_role" {
  description = "IAM role ARN for MSW LOTUS instances that will upload objects into the S3 bucket."
}

variable "gfs_expiration_days" {
  description = "Days to expire gfs/ prefix."
}

variable "inventory_expiration_days" {
  description = "Days to expire inventory/ prefix."
}

variable "jobs_expiration_days" {
  description = "Days to expire jobs/ prefix."
}

variable "lotus_archive_grids_expiration_days" {
  description = "Days to expire lotus/archive/grids/ prefix."
}

variable "lotus_archive_grids_analysis_expiration_days" {
  description = "Days to expire lotus/archive/grids_analysis/ prefix."
  default     = null
}

variable "lotus_archive_hvp_expiration_days" {
  description = "Days to expire lotus/archive/hvp/ prefix."
}

variable "lotus_archive_logs_expiration_days" {
  description = "Days to expire lotus/archive/logs/ prefix."
}

variable "lotus_archive_points_expiration_days" {
  description = "Days to expire lotus/archive/points/ prefix."
}

variable "nasa_mur_sst_expiration_days" {
  description = "Days to expire nasa/mur-sst/ prefix."
}

variable "noaa_gefs_wave_expiration_days" {
  description = "Days to expire noaa/gefs-wave/ prefix."
}

variable "noaa_nam_expiration_days" {
  description = "Days to expire noaa/nam/ prefix."
}

variable "science_models_db_backups_expiration_days" {
  description = "Days to expire science-models-db/backups/ prefix."
}

variable "wrf_wrfinput_nam_expiration_days" {
  description = "Days to expire wrf/wrfinput/nam/ prefix."
}
