resource "aws_s3_bucket" "surfline_science" {
  bucket = "surfline-science-s3-${var.environment}"

  lifecycle_rule {
    id      = "expire gfs/"
    enabled = true
    prefix  = "gfs/"

    expiration {
      days = var.gfs_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire inventory/"
    enabled = true
    prefix  = "inventory/"

    expiration {
      days = var.inventory_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire jobs/"
    enabled = true
    prefix  = "jobs/"

    expiration {
      days = var.jobs_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire lotus/archive/grids/"
    enabled = true
    prefix  = "lotus/archive/grids/"

    expiration {
      days = var.lotus_archive_grids_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire lotus/archive/grids_analysis/"
    enabled = var.lotus_archive_grids_analysis_expiration_days != null ? true : false
    prefix  = "lotus/archive/grids_analysis/"

    expiration {
      days = var.lotus_archive_grids_analysis_expiration_days
    }
  }

  lifecycle_rule {
    id      = "archive lotus/archive/grids_analysis/"
    enabled = true
    prefix  = "lotus/archive/grids_analysis/"

    transition {
      days          = 65
      storage_class = "GLACIER"
    }
  }

  lifecycle_rule {
    id      = "expire lotus/archive/hvp/"
    enabled = true
    prefix  = "lotus/archive/hvp/"

    expiration {
      days = var.lotus_archive_hvp_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire lotus/archive/logs/"
    enabled = true
    prefix  = "lotus/archive/logs/"

    expiration {
      days = var.lotus_archive_logs_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire lotus/archive/points/"
    enabled = true
    prefix  = "lotus/archive/points/"

    expiration {
      days = var.lotus_archive_points_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire nasa/mur-sst/"
    enabled = true
    prefix  = "nasa/mur-sst/"

    expiration {
      days = var.nasa_mur_sst_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire noaa/gefs-wave/"
    enabled = true
    prefix  = "noaa/gefs-wave/"

    expiration {
      days = var.noaa_gefs_wave_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire noaa/nam/"
    enabled = true
    prefix  = "noaa/nam/"

    expiration {
      days = var.noaa_nam_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire science-models-db/backups/"
    enabled = true
    prefix  = "science-models-db/backups/"

    expiration {
      days = var.science_models_db_backups_expiration_days
    }
  }

  lifecycle_rule {
    id      = "expire wrf/wrfinput/nam/"
    enabled = true
    prefix  = "wrf/wrfinput/nam/"

    expiration {
      days = var.wrf_wrfinput_nam_expiration_days
    }
  }
}

data "aws_caller_identity" "current" {}

resource "aws_s3_bucket_policy" "surfline_science" {
  bucket = aws_s3_bucket.surfline_science.id
  policy = templatefile("${path.module}/resources/s3-policy.json", {
    s3_bucket  = aws_s3_bucket.surfline_science.arn
    cloudfront = var.cloudfront
    lotus_role = var.lotus_role
    account_id = data.aws_caller_identity.current.account_id
  })
}
