provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-science-s3/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline_science_s3" {
  source = "../../"

  environment = "prod"
  cloudfront  = "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E2BDL4AH71GBGS"
  lotus_role  = "arn:aws:iam::521820898417:role/LOTUS_ec2"

  gfs_expiration_days                       = 10
  inventory_expiration_days                 = 9
  jobs_expiration_days                      = 7
  lotus_archive_grids_expiration_days       = 65
  lotus_archive_hvp_expiration_days         = 65
  lotus_archive_logs_expiration_days        = 65
  lotus_archive_points_expiration_days      = 65
  nasa_mur_sst_expiration_days              = 10
  noaa_gefs_wave_expiration_days            = 2
  noaa_nam_expiration_days                  = 10
  science_models_db_backups_expiration_days = 3
  wrf_wrfinput_nam_expiration_days          = 10
}
