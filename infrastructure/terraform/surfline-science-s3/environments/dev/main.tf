provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-science-s3/dev/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  max_expiration_days = 5
}

module "surfline_science_s3" {
  source = "../../"

  environment = "dev"
  cloudfront  = "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ET9K1QYSSIYGN"
  lotus_role  = "arn:aws:iam::521820898417:role/LOTUS_ec2"

  gfs_expiration_days                          = local.max_expiration_days
  inventory_expiration_days                    = local.max_expiration_days
  jobs_expiration_days                         = local.max_expiration_days
  lotus_archive_grids_expiration_days          = local.max_expiration_days
  lotus_archive_grids_analysis_expiration_days = local.max_expiration_days
  lotus_archive_hvp_expiration_days            = local.max_expiration_days
  lotus_archive_logs_expiration_days           = local.max_expiration_days
  lotus_archive_points_expiration_days         = local.max_expiration_days
  nasa_mur_sst_expiration_days                 = local.max_expiration_days
  noaa_gefs_wave_expiration_days               = 2
  noaa_nam_expiration_days                     = local.max_expiration_days
  science_models_db_backups_expiration_days    = 3
  wrf_wrfinput_nam_expiration_days             = local.max_expiration_days
}
