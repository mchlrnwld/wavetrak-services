{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "allow CloudFront to get bucket objects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${cloudfront}"
      },
      "Action": "s3:GetObject",
      "Resource": "${s3_bucket}/*"
    },
    {
      "Sid": "allow LOTUS instances to list bucket objects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${lotus_role}"
      },
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "${s3_bucket}"
      ]
    },
    {
      "Sid": "allow LOTUS instances to get and put lotus bucket objects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${lotus_role}"
      },
      "Action": [
        "s3:GetObject",
        "s3:PutObject"
      ],
      "Resource": [
        "${s3_bucket}/lotus/*"
      ]
    },
    {
      "Sid": "allow LOTUS instances to get gfs bucket objects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${lotus_role}"
      },
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "${s3_bucket}/gfs/*"
      ]
    },
    {
      "Sid": "Allow bucket to write to itself for inventories",
      "Effect":"Allow",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource":[
        "${s3_bucket}/inventory/*"
      ],
      "Condition": {
          "ArnLike": {
              "aws:SourceArn": "${s3_bucket}"
           },
         "StringEquals": {
             "aws:SourceAccount": "${account_id}",
             "s3:x-amz-acl": "bucket-owner-full-control"
          }
       }
    }
  ]
}
