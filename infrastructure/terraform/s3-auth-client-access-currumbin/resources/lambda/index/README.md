# s3-auth-client-access-currumbin-lambda-index

Lambda function to generate HTML indexes for S3 buckets. When an object in
an S3 bucket is updated, this Lambda function is triggered. It reads the
directory that the object is in, creates an HTML index file, and traverses
up the directory tree creating more HTML index files until it reaches a
predefined root. Then it uploads all the index files to the S3 bucket in
their respective directories.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate s3-auth-client-access-currumbin-lambda-index
$ make run
```

### Build and Run

```sh
$ docker build -t s3-auth-client-access-currumbin-lambda-index .
$ docker run --rm -it s3-auth-client-access-currumbin-lambda-index
```
