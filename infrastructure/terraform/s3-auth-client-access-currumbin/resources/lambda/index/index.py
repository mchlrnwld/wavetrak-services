from datetime import datetime
import json
import logging
from typing import List, Tuple

import boto3  # type: ignore

# Remove handler so that logging works correctly within Lambda
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# boto3 is very noisy at the log levels we'd like to support
# for the rest of our application
logging.getLogger('botocore').setLevel(logging.ERROR)
logging.getLogger('boto3').setLevel(logging.ERROR)

client = boto3.client('s3')

SAMPLE_EVENT = 'event.json'
ROOT_PREFIX = '/'

PAGE_TEMPLATE_FILENAME = 'page_template.html'


def load_page_template(filename: str) -> str:
    '''
    Load the HTML page template into a string

    Args:
        filename: Filename of the HTML page template

    Returns:
        A string containing the HTML page template
    '''
    logger.info(f'Loading page template {filename}')
    with open(filename) as file:
        return file.read()


def get_s3_paths(object_key: str, root_path: str = '') -> List[str]:
    '''
    Given an s3 object_key, get a list of all parent "directories" up to the
    supplied root

    Args:
        object_key: An S3 object key
        root_path:  The path to treat as the root. Parent directories of the
                    root path will not be returned.

    Returns:
        Given an s3 object_key, get a list of all parent "directories".

        Example:

        get_s3_paths(
            'au-currumbin4k/2020/05/26/filename.jpg',
            'au-currumbin4k/'
        )

        =>

        [
            'au-currumbin4k/2020/05/26/'
            'au-currumbin4k/2020/05/'
            'au-currumbin4k/2020/'
            'au-currumbin4k/'
        ]
    '''
    paths = []

    # If the root isn't contained in the object key, return []
    if root_path not in object_key:
        logger.warning(
            f'Root path {root_path} not present in object key {object_key}. '
            f'Skipping...'
        )
        return []

    path_parts = object_key.split('/')
    for i in range(1, len(path_parts) + 1):
        path = '/'.join(path_parts[:-i]) + '/'
        paths.append(path if path is not '/' else '')
        if root_path == path:
            break

    return paths


def list_s3_objects(
    bucket: str, prefix: str
) -> Tuple[List[str], List[Tuple[str, datetime, int]]]:
    '''
    Returns all prefixes and objects given an S3 bucket and prefix. Uses
    pagination to return all objects with the given prefix, regardless of
    how many prefixes and objects are in the bucket.

    Args:
        bucket: An S3 bucket
        prefix: An S3 object key prefix

    Returns:
        A tuple containing a list of prefixes names and a list of object
        tuples containing the object's name, last modified time, and size.
    '''
    prefixes = []
    objects = []

    logger.info(f'Getting S3 list for s3://{bucket}/{prefix}')

    paginator = client.get_paginator('list_objects_v2')
    operation_parameters = {
        'Bucket': bucket,
        'Prefix': prefix,
        'Delimiter': '/',
    }
    page_iterator = paginator.paginate(**operation_parameters)

    for page in page_iterator:
        prefixes += [
            common_prefix['Prefix'].replace(prefix, '')
            for common_prefix in page.get('CommonPrefixes', [])
        ]
        objects += [
            (obj['Key'].replace(prefix, ''), obj['LastModified'], obj['Size'],)
            for obj in page.get('Contents', [])
        ]

    return (sorted(prefixes), sorted(objects))


def build_link(name: str, width: int) -> str:
    '''
    Builds an HTML link truncated or padded with spaces up to the given width

    Args:
        name:  The object or prefix name
        width: The width to truncate or pad to

    Returns:
        An HTML string truncated or padded with spaces up to a given width

        Ex. '<a href="{file_path}">{file_name}</a>{padding}'
    '''
    file_path = name
    file_name = name if len(name) <= width else (name[: width - 3] + '..&gt;')
    padding = " " * (width - len(name))
    return f'<a href="{file_path}">{file_name}</a>{padding}'


def build_prefix_html(prefixes: List[str]) -> str:
    '''
    Builds HTML for a list of prefixes. Last modified time and size are empty
    for prefixes.

    Args:
        prefixes: A list of prefix names

    Returns:
        An HTML string containing entries for a list of prefixes
    '''
    # |-- filename - 51 char --|-- date - 17 char --|-- size - 20 char --|
    return "\n".join(
        [f'{build_link(prefix, 50)}{" " * 37}-' for prefix in prefixes]
    )


def build_object_html(objects: List[Tuple[str, datetime, int]]) -> str:
    '''
    Builds HTML for a list of objects.

    Args:
        objects: A list of object tuples containing the object's name, last
        modified time, and size.

    Returns:
        An HTML string containing entries for a list of objects
    '''
    # |-- filename - 51 char --|-- date - 17 char --|-- size - 20 char --|
    return "\n".join(
        [
            (
                f'{build_link(key, 50)} '
                f'{last_mod.strftime("%d-%b-%Y %H:%M")} '
                f'{str(size).rjust(19)}'
            )
            for (key, last_mod, size) in objects
        ]
    )


def build_html(
    bucket: str, prefix: str, root_prefix: str = '', page_template: str = ''
) -> str:
    '''
    Builds an HTML document representing an S3 bucket and prefix

    Args:
        bucket:        An S3 bucket
        prefix:        An S3 object key prefix
        page_template: The html document to insert content into

    Returns:
        An HTML string representing a document with entries for a list of
        objects
    '''
    display_prefix = prefix.replace(root_prefix, '/')
    (prefixes, objects) = list_s3_objects(bucket, prefix)
    return "\n".join(
        [
            page_template.format(
                path=display_prefix,
                content='\n'.join(
                    [build_prefix_html(prefixes), build_object_html(objects)]
                ),
            )
        ]
    )


def lambda_handler(event: dict, context: dict = None):
    '''
    The entry point for this lambda function. This will be triggered from an
    AWS S3 object update.

    Args:
        event:   The event that triggered this Lambda
        context: Provides runtime information to your handler
    '''
    logger.info(f'Event: {json.dumps(event, indent=2, sort_keys=True)}')

    try:
        event_record = json.loads(event['Records'][0]['Sns']['Message'])
        bucket = event_record['Records'][0]['s3']['bucket']['name']
        object_key = event_record['Records'][0]['s3']['object']['key']

        logger.info(f'Updated object: s3://{bucket}/{object_key}')
    except (KeyError, IndexError) as e:
        logger.error(e)
        raise Exception(e)

    # If the updated file is index.html, skip the update
    if object_key.split('/')[-1] == 'index.html':
        logger.warn('Updated object is index.html. Skipping...')
        return

    # Build indexes for each path
    page_template = load_page_template(PAGE_TEMPLATE_FILENAME)
    indexes = [
        (path, build_html(bucket, path, ROOT_PREFIX, page_template))
        for path in get_s3_paths(object_key, ROOT_PREFIX)
    ]

    # Upload indexes to S3
    for (path, html) in indexes:
        logger.info(
            f'Uploading s3://{bucket}/{path}index.html ({len(html)} bytes)'
        )

        response = client.put_object(
            Body=html,
            Bucket=bucket,
            Key=f'{path}index.html',
            ContentType='text/html',
        )


def main():
    '''
    Run Lambda function manually for development
    '''
    # Load sample event, run lambda handler
    with open(SAMPLE_EVENT) as sample_event_file:
        sample_event = json.load(sample_event_file)
        lambda_handler(sample_event)


if __name__ == "__main__":
    main()
