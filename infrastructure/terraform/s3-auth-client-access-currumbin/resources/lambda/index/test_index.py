import index


def test_get_s3_paths():
    paths = index.get_s3_paths(
        'au-currumbin4k/2020/05/26/filename.jpg', 'au-currumbin4k/',
    )

    assert paths == [
        'au-currumbin4k/2020/05/26/',
        'au-currumbin4k/2020/05/',
        'au-currumbin4k/2020/',
        'au-currumbin4k/',
    ]


def test_get_s3_paths_with_empty_root():
    paths = index.get_s3_paths('au-currumbin4k/2020/05/26/filename.jpg')

    assert paths == [
        'au-currumbin4k/2020/05/26/',
        'au-currumbin4k/2020/05/',
        'au-currumbin4k/2020/',
        'au-currumbin4k/',
        '',
    ]


def test_get_s3_paths_with_slash_root():
    paths = index.get_s3_paths('au-currumbin4k/2020/05/26/filename.jpg', '/',)

    assert paths == [
        'au-currumbin4k/2020/05/26/',
        'au-currumbin4k/2020/05/',
        'au-currumbin4k/2020/',
        'au-currumbin4k/',
        '',
    ]
