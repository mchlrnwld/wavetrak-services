# S3 Auth Client Access [currumbin] module

## Prerequisites

This terraform module has a couple prerequisites:

- The bucket that contains the statically-served S3 content should already exist, and be configured for [static website hosting ](https://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html).
- The DNS records that will be used for client access must already exist.

Note: the authorization credentials used for this module are saved in the dev AWS account under dev/function/wt-s3-auth-client-access-currumbin/AUTH.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)

```bash
make plan
make apply
```
