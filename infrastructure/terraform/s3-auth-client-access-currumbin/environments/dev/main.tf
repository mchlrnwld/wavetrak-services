provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "s3-auth-client-access-currumbin/dev/terraform.tfstate"
    region = "us-west-1"
  }
}

module "s3-auth-client-access-currumbin" {
  source = "../../"

  company     = "wt"
  application = "s3-auth-client-access-currumbin"
  environment = "dev"

  cf_domains     = ["surfzone-currumbin.surfline.com", "wt-data-lake-dev-au-currumbin4k.cdn-surfline.com"]
  cf_cert_domain = "*.cdn-surfline.com"

  cdn_acm_arn    = "arn:aws:acm:us-east-1:665294954271:certificate/296211d1-2e77-47b2-b3ea-f8ec623c2804"
  s3_bucket_name = "surfzoneai-griffith-currumbin"
  cf_ttl         = 0
}
