variable "company" {
  description = "The company for which these resources are being created."
}

variable "application" {
  description = "The application for which these resources are being created."
}

variable "environment" {
  description = "The environment in which these resources are being created."
}

variable "cf_domains" {
  description = "The domain names that will alias to Cloudfront."
}

variable "cf_cert_domain" {
  description = "The Cloudfront certificate domain."
}

variable "cdn_acm_arn" {
  description = "The arn of the SSL certificate."
}

variable "s3_bucket_name" {
  description = "The environment in which these resources are being created."
}

variable "cf_ttl" {
  description = "The time to live for the Cloudfront distribution."
}
