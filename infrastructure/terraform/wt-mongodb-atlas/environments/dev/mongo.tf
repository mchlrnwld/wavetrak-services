module "mongo_common" {
  source = "../../modules/mongo"

  environment      = local.environment
  mongo_project_id = "5994d9efd383ad4715c73636"

  # CPU alert
  mongo_cpu_alert_threshold_value = 70

  # Resident memory alert in gigabytes
  mongo_resident_memory_alert_threshold_value = 2

  # Virtual memory alert in gigabytes
  mongo_virtual_memory_alert_threshold_value = ".5"

  # Read latency alert in milliseconds
  mongo_read_latency_alert_threshold_value = 20

  # Write latency alert in milliseconds
  mongo_write_latency_alert_threshold_value = 10
}
