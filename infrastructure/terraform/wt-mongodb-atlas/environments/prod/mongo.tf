module "mongo_common" {
  source = "../../modules/mongo"

  environment      = local.environment
  mongo_project_id = "5a94a8290bd66b4935f075c3"

  # CPU alert
  mongo_cpu_alert_threshold_value = 70

  # Resident memory alert in gigabytes
  mongo_resident_memory_alert_threshold_value = 1

  # Virtual memory alert in gigabytes
  mongo_virtual_memory_alert_threshold_value = 1

  # Read latency alert in milliseconds
  mongo_read_latency_alert_threshold_value = 20

  # Write latency alert in milliseconds
  mongo_write_latency_alert_threshold_value = 10
}
