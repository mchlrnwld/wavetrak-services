variable "environment" {
  description = "Execution environment"
}

variable "mongo_project_id" {
  description = "Mongo project id"
}

variable "mongo_cpu_alert_threshold_value" {
  description = "Threshold value outside of which an alert will be triggered"
}

variable "mongo_resident_memory_alert_threshold_value" {
  description = "Threshold value for memory remaining (in gigabytes)"
}

variable "mongo_virtual_memory_alert_threshold_value" {
  description = "Threshold value for memory remaining (in gigabytes)"
}

variable "mongo_read_latency_alert_threshold_value" {
  description = "Threshold value outside of which an alert will be triggered (in milliseconds)"
}

variable "mongo_write_latency_alert_threshold_value" {
  description = "Threshold value outside of which an alert will be triggered (in milliseconds)"
}
