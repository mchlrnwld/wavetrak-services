locals {
  mongo_cluster_name = "${var.environment}-common-monogodb-atlas"
}

data "aws_ssm_parameter" "pagerduty_integration_key" {
  name = "/${var.environment}/infra/wt-mongodb-atlas/pagerduty/mongo_common_integration_key"
}

# CPU alert
resource "mongodbatlas_alert_configuration" "mongo_cpu_alert" {
  project_id = var.mongo_project_id
  event_type = "OUTSIDE_METRIC_THRESHOLD"
  enabled    = true

  notification {
    type_name    = "PAGER_DUTY"
    delay_min    = 10
    service_key  = data.aws_ssm_parameter.pagerduty_integration_key.value
  }

  matcher {
    field_name = "CLUSTER_NAME"
    operator   = "EQUALS"
    value      = local.mongo_cluster_name
  }

  metric_threshold = {
    metric_name = "NORMALIZED_FTS_PROCESS_CPU_USER"
    operator    = "GREATER_THAN"
    threshold   = var.mongo_cpu_alert_threshold_value
    units       = "RAW"
    mode        = "AVERAGE"
  }
}

# Resident memory alert in gigabytes
resource "mongodbatlas_alert_configuration" "mongo_resident_memory_alert" {
  project_id = var.mongo_project_id
  event_type = "OUTSIDE_METRIC_THRESHOLD"
  enabled    = true

  notification {
    type_name    = "PAGER_DUTY"
    delay_min    = 10
    service_key  = data.aws_ssm_parameter.pagerduty_integration_key.value
  }

  matcher {
    field_name = "CLUSTER_NAME"
    operator   = "EQUALS"
    value      = local.mongo_cluster_name
  }

  metric_threshold = {
    metric_name = "MEMORY_RESIDENT"
    operator    = "LESS_THAN"
    threshold   = var.mongo_resident_memory_alert_threshold_value
    units       = "GIGABYTES"
    mode        = "AVERAGE"
  }
}

# Virtual memory alert in gigabytes
resource "mongodbatlas_alert_configuration" "mongo_virtual_memory_alert" {
  project_id = var.mongo_project_id
  event_type = "OUTSIDE_METRIC_THRESHOLD"
  enabled    = true

  notification {
    type_name    = "PAGER_DUTY"
    delay_min    = 10
    service_key  = data.aws_ssm_parameter.pagerduty_integration_key.value
  }

  matcher {
    field_name = "CLUSTER_NAME"
    operator   = "EQUALS"
    value      = local.mongo_cluster_name
  }

  metric_threshold = {
    metric_name = "MEMORY_VIRTUAL"
    operator    = "LESS_THAN"
    threshold   = var.mongo_virtual_memory_alert_threshold_value
    units       = "GIGABYTES"
    mode        = "AVERAGE"
  }
}

# Read latency alert in milliseconds
resource "mongodbatlas_alert_configuration" "mongo_read_latency_alert" {
  project_id = var.mongo_project_id
  event_type = "OUTSIDE_METRIC_THRESHOLD"
  enabled    = true

  notification {
    type_name    = "PAGER_DUTY"
    delay_min    = 10
    service_key  = data.aws_ssm_parameter.pagerduty_integration_key.value
  }

  matcher {
    field_name = "CLUSTER_NAME"
    operator   = "EQUALS"
    value      = local.mongo_cluster_name
  }

  metric_threshold = {
    metric_name = "AVG_READ_EXECUTION_TIME"
    operator    = "GREATER_THAN"
    threshold   = var.mongo_read_latency_alert_threshold_value
    units       = "MILLISECONDS"
    mode        = "AVERAGE"
  }
}

# Write latency alert in milliseconds
resource "mongodbatlas_alert_configuration" "mongo_write_latency_alert" {
  project_id = var.mongo_project_id
  event_type = "OUTSIDE_METRIC_THRESHOLD"
  enabled    = true

  notification {
    type_name    = "PAGER_DUTY"
    interval_min = 30
    delay_min    = 10
    service_key  = data.aws_ssm_parameter.pagerduty_integration_key.value
  }

  matcher {
    field_name = "CLUSTER_NAME"
    operator   = "EQUALS"
    value      = local.mongo_cluster_name
  }

  metric_threshold = {
    metric_name = "AVG_WRITE_EXECUTION_TIME"
    operator    = "GREATER_THAN"
    threshold   = var.mongo_write_latency_alert_threshold_value
    units       = "MILLISECONDS"
    mode        = "AVERAGE"
  }
}
