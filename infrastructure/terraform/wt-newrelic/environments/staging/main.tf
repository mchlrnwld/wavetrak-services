terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "wt-newrelic/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

provider "aws" {
  region = "us-west-1"
}

locals {
  environment = "staging"
  company     = "wt"
}
