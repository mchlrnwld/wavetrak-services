terraform {
  required_providers {
    aws = "~> 2.70"
    newrelic = {
      source = "newrelic/newrelic"
    }
  }
  required_version = ">= 0.13"
}
