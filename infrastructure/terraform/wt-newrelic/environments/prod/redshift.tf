locals {
  redshift_application  = "Redshift"
  redshift_cluster_name = "sl-redshift-segment-prod-redshiftcluster-cdbaf49aa6bc130be49"
}

data "aws_ssm_parameter" "redshift_integration_key" {
  name = "/${local.environment}/infra/wt-newrelic/pagerduty/redshift_integration_key"
}

# Create alert channel
resource "newrelic_alert_channel" "redshift_alert_channel" {
  name = local.redshift_application
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.redshift_integration_key.value
  }
}

# Create new alert policy
resource "newrelic_alert_policy" "redshift_alert_policy" {
  name                = local.redshift_application
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_alert_policy_channel" "redshift_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.redshift_alert_policy.id
  channel_ids = [newrelic_alert_channel.redshift_alert_channel.id]
}

# High CPU
resource "newrelic_infra_alert_condition" "redshift_high_cpu_alert" {
  policy_id = newrelic_alert_policy.redshift_alert_policy.id

  name                 = "${local.redshift_application} - High CPU"
  type                 = "infra_metric"
  select               = "provider.cpuUtilization.Average"
  comparison           = "above"
  where                = "displayName = '${local.redshift_cluster_name}'"
  integration_provider = "RedshiftCluster"

  warning {
    duration      = 20
    value         = 90
    time_function = "all"
  }

  critical {
    duration      = 30
    value         = 95
    time_function = "all"
  }
}

# High Disk Usage
resource "newrelic_infra_alert_condition" "redshift_storage_alert" {
  policy_id = newrelic_alert_policy.redshift_alert_policy.id

  name                 = "${local.redshift_application} - High Disk Usage"
  type                 = "infra_metric"
  select               = "provider.PercentageDiskSpaceUsed.Average"
  comparison           = "above"
  where                = "displayName = '${local.redshift_cluster_name}'"
  integration_provider = "RedshiftCluster"

  warning {
    duration      = 5
    value         = 85
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 90
    time_function = "all"
  }
}

# Health status
resource "newrelic_infra_alert_condition" "redshift_health_alert" {
  policy_id = newrelic_alert_policy.redshift_alert_policy.id

  name                 = "${local.redshift_application} - Cluster Unhealthy"
  type                 = "infra_metric"
  select               = "provider.HealthStatus.Average"
  comparison           = "equal"
  where                = "displayName = '${local.redshift_cluster_name}'"
  integration_provider = "RedshiftCluster"

  critical {
    duration      = 10
    value         = "0"
    time_function = "all"
  }
}
