locals {
  chatops_application = "Chatops Service"
}

data "aws_ssm_parameter" "infrastructure_integration_key" {
  name = "/prod/newrelic/chatops/integration-key"
}

resource "newrelic_alert_policy" "chatops" {
  name                = "${local.chatops_application} Synthetics Check"
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_alert_channel" "chatops" {
  name = local.chatops_application
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.infrastructure_integration_key.value
  }
}

resource "newrelic_alert_policy_channel" "chatops_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.chatops.id
  channel_ids = [newrelic_alert_channel.chatops.id]
}

resource "newrelic_synthetics_monitor" "chatops" {
  name       = local.chatops_application
  type       = "SIMPLE"
  frequency  = 5
  status     = "ENABLED"
  locations  = ["AWS_US_WEST_1"]
  uri        = "http://chatops-hubot-staging.us-west-1.elasticbeanstalk.com/health"
  verify_ssl = false
}

resource "newrelic_synthetics_alert_condition" "chatops" {
  policy_id  = newrelic_alert_policy.chatops.id
  name       = "${local.chatops_application} Synthetics Check"
  monitor_id = newrelic_synthetics_monitor.chatops.id
}
