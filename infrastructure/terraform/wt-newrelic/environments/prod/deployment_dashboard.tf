locals {
  applications = [
    "service",
    "function",
    "static webapp",
    "npm package",
    "pip package",
    "airflow job",
  ]
  two_tier_environments = [
    {
      name = "production",
      row  = 1
    },
    {
      name = "dev",
      row  = 4
    }
  ]
  three_tier_environments = [
    {
      name = "prod",
      row  = 1
    },
    {
      name = "staging",
      row  = 4
    },
    {
      name = "sandbox",
      row  = 7
    }
  ]
  environments = {
    "service" = local.three_tier_environments,
    "function" = local.three_tier_environments,
    "static webapp" = local.three_tier_environments,
    "npm package" = local.two_tier_environments,
    "pip package" = local.two_tier_environments,
    "airflow job" = local.two_tier_environments
  }
}

resource "newrelic_one_dashboard" "deployments_dash" {
  name = "Deployments"

  dynamic "page" {
    for_each = local.applications
    iterator = application
    content {
      name = "${title(application.value)}s"

      dynamic "widget_table" {
        for_each = local.environments[application.value]
        iterator = environment
        content {
          title  = "${title(application.value)} Deployments to ${title(environment.value.name)}"
          row    = environment.value.row
          column = 1
          height = 3
          width  = 12
          nrql_query {
            query = "SELECT timestamp, commit_sha, name, deploy_type, user, workflow_run_url FROM `Deployment` WHERE app_type = '${application.value}' AND environment = '${environment.value.name}' SINCE 6 MONTHS AGO LIMIT 1000"
          }
        }
      }

      widget_table {
        title  = "Most Recent Deployment By ${title(application.value)} and Environment"
        row    = 10
        column = 1
        height = 6
        width  = 12
        nrql_query {
          query = "SELECT latest(timestamp), latest(commit_sha) FROM Deployment WHERE app_type = '${application.value}' FACET name, environment ORDER BY name SINCE 6 MONTHS AGO LIMIT 1000"
        }
      }
    }
  }
}
