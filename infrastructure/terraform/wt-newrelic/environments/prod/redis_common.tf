locals {
  sl_common_cluster_name = "slegcpl2npsknxv"

}

data "aws_ssm_parameter" "newrelic_integration_key" {
  name = "/${local.environment}/newrelic/redis-common/integration-key"
}

# Create alerting channel
resource "newrelic_alert_channel" "redis_common_alert_channel" {
  name = "Redis - Common"
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.newrelic_integration_key.value
  }
}

# Create alert policy
resource "newrelic_alert_policy" "redis_common_alert_policy" {
  name                = "Redis - Common"
  incident_preference = "PER_CONDITION_AND_TARGET"
}

# Associate policy with channel
resource "newrelic_alert_policy_channel" "redis_common_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.redis_common_alert_policy.id
  channel_ids = [newrelic_alert_channel.redis_common_alert_channel.id]
}

# Alerts for slegcpl2npsknxv-001 node

# High CPU
resource "newrelic_infra_alert_condition" "redis_common_high" {
  policy_id = newrelic_alert_policy.redis_common_alert_policy.id

  name                 = "High CPU"
  type                 = "infra_metric"
  select               = "provider.cpuUtilization.Average"
  comparison           = "above"
  where                = "displayName like '${local.sl_common_cluster_name}-001%'"
  integration_provider = "ElastiCacheRedisCluster"

  warning {
    duration      = 5
    value         = 20
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 30
    time_function = "all"
  }
}

# Swap usage
resource "newrelic_infra_alert_condition" "redis_common_high_swap_usage" {
  policy_id = newrelic_alert_policy.redis_common_alert_policy.id

  name                 = "High Swap Usage"
  type                 = "infra_metric"
  select               = "provider.swapUsage.Average"
  comparison           = "above"
  where                = "displayName like '${local.sl_common_cluster_name}-001%'"
  integration_provider = "ElastiCacheRedisCluster"

  warning {
    duration      = 5
    value         = 40000000
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 50000000
    time_function = "all"
  }
}

# Low freeable memory
resource "newrelic_infra_alert_condition" "redis_common_low_freeable_memory" {
  policy_id = newrelic_alert_policy.redis_common_alert_policy.id

  name                 = "Low Freeable Memory"
  type                 = "infra_metric"
  select               = "provider.freeableMemory.Average"
  comparison           = "below"
  where                = "displayName like '${local.sl_common_cluster_name}-001%'"
  integration_provider = "ElastiCacheRedisCluster"

  warning {
    duration      = 5
    value         = 4000000000
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 3000000000
    time_function = "all"
  }
}
