terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wt-newrelic/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

provider "aws" {
  region = "us-west-1"
}

locals {
  environment = "prod"
  company     = "wt"
}
