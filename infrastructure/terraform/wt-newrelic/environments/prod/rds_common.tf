locals {
  rds_common_application  = "RDS Common"
  rds_common_display_name = "sl-common-prod"
}

data "aws_ssm_parameter" "rds_common_integration_key" {
  name = "/${local.environment}/newrelic/rds/integration-key"
}

# Create alert channel
resource "newrelic_alert_channel" "rds_common_alert_channel" {
  name = local.rds_common_application
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.rds_common_integration_key.value
  }
}

# Create new alert policy
resource "newrelic_alert_policy" "rds_common_alert_policy" {
  name                = local.rds_common_application
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_alert_policy_channel" "rds_common_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.rds_common_alert_policy.id
  channel_ids = [newrelic_alert_channel.rds_common_alert_channel.id]
}

# High CPU
resource "newrelic_infra_alert_condition" "rds_high_cpu_alert" {
  policy_id = newrelic_alert_policy.rds_common_alert_policy.id

  name                 = "${local.rds_common_application} - High CPU"
  type                 = "infra_metric"
  select               = "provider.cpuUtilization.Average"
  comparison           = "above"
  where                = "displayName = '${local.rds_common_display_name}'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = 50
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 60
    time_function = "all"
  }
}

# Low memory
resource "newrelic_infra_alert_condition" "rds_memory_alert" {
  policy_id = newrelic_alert_policy.rds_common_alert_policy.id

  name                 = "${local.rds_common_application} - Low Memory"
  type                 = "infra_metric"
  select               = "provider.freeableMemoryBytes.Average"
  comparison           = "below"
  where                = "displayName = '${local.rds_common_display_name}'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = 2000000000
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 1000000000
    time_function = "all"
  }
}

# Low available storage
resource "newrelic_infra_alert_condition" "rds_storage_alert" {
  policy_id = newrelic_alert_policy.rds_common_alert_policy.id

  name                 = "${local.rds_common_application} - Low Available Storage"
  type                 = "infra_metric"
  select               = "provider.freeStorageSpaceBytes.Average"
  comparison           = "below"
  where                = "displayName = '${local.rds_common_display_name}'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = 40000000000
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 30000000000
    time_function = "all"
  }
}
