locals {
  varnish_application = "varnish"
}

data "aws_ssm_parameter" "varnish_integration_key" {
  name = "/${local.environment}/newrelic/varnish/integration-key"
}

# Create alert channel
resource "newrelic_alert_channel" "varnish_alert_channel" {
  name = title(local.varnish_application)
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.varnish_integration_key.value
  }
}

# Create new alert policy
resource "newrelic_alert_policy" "varnish_alert_policy" {
  name                = title(local.varnish_application)
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_alert_policy_channel" "varnish_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.varnish_alert_policy.id
  channel_ids = [newrelic_alert_channel.varnish_alert_channel.id]
}

# Varnish high server CPU
resource "newrelic_infra_alert_condition" "varnish_server_high_cpu_alert" {
  policy_id = newrelic_alert_policy.varnish_alert_policy.id

  name       = "${title(local.varnish_application)} - High Server CPU"
  type       = "infra_metric"
  event      = "SystemSample"
  select     = "cpuPercent"
  comparison = "above"
  where      = "(hostname LIKE '%${local.environment}-${local.varnish_application}-cache-%')"

  warning {
    duration      = 5
    value         = 10
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 20
    time_function = "all"
  }
}

# Varnish low memory
resource "newrelic_infra_alert_condition" "varnish_low_memory_alert" {
  policy_id = newrelic_alert_policy.varnish_alert_policy.id

  name       = "${title(local.varnish_application)} - Low Server Memory"
  type       = "infra_metric"
  event      = "SystemSample"
  select     = "memoryFreePercent"
  comparison = "below"
  where      = "(hostname LIKE '%${local.environment}-${local.varnish_application}-cache-%')"

  warning {
    duration      = 5
    value         = 60
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 50
    time_function = "all"
  }
}
# Varnish server high disk usage
resource "newrelic_infra_alert_condition" "varnish_server_high_disk_usage_alert" {
  policy_id = newrelic_alert_policy.varnish_alert_policy.id

  name       = "${title(local.varnish_application)} - High Server Disk Usage"
  type       = "infra_metric"
  event      = "StorageSample"
  select     = "diskFreePercent"
  comparison = "below"
  where      = "(hostname LIKE '%${local.environment}-${local.varnish_application}-cache-%')"

  warning {
    duration      = 5
    value         = 20
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 10
    time_function = "all"
  }
}

# Varnish process not running
resource "newrelic_infra_alert_condition" "varnish_not_running_alert" {
  policy_id = newrelic_alert_policy.varnish_alert_policy.id

  name          = "${title(local.varnish_application)} - Process Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname LIKE '%${local.environment}-${local.varnish_application}-cache-%')"
  process_where = "processDisplayName = 'varnishd'"

  critical {
    duration = 10
    value    = 0
  }
}
