locals {
  jenkins_application     = "Jenkins"
  jenkins_server_hostname = "prod-jenkins-master-2"
}

data "aws_ssm_parameter" "jenkins_integration_key" {
  name = "/${local.environment}/newrelic/jenkins/integration-key"
}

# Create alert channel
resource "newrelic_alert_channel" "jenkins_alert_channel" {
  name = local.jenkins_application
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.jenkins_integration_key.value
  }
}

# Create alert policy
resource "newrelic_alert_policy" "jenkins_alert_policy" {
  name                = local.jenkins_application
  incident_preference = "PER_CONDITION_AND_TARGET"
}

# Associate policy with channel
resource "newrelic_alert_policy_channel" "jenkins_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.jenkins_alert_policy.id
  channel_ids = [newrelic_alert_channel.jenkins_alert_channel.id]
}

# Jenkins high server CPU
resource "newrelic_infra_alert_condition" "jenkins_server_high_cpu_alert" {
  policy_id = newrelic_alert_policy.jenkins_alert_policy.id

  name       = "${local.jenkins_application} - High Server CPU"
  type       = "infra_metric"
  event      = "SystemSample"
  select     = "cpuPercent"
  comparison = "above"
  where      = "(hostname = '${local.jenkins_server_hostname}')"

  warning {
    duration      = 5
    value         = 80
    time_function = "all"
  }

  critical {
    duration      = 15
    value         = 90
    time_function = "all"
  }
}

# Jenkins server low memory
resource "newrelic_infra_alert_condition" "jenkins_server_low_memory_alert" {
  policy_id = newrelic_alert_policy.jenkins_alert_policy.id

  name       = "${local.jenkins_application} - Low Memory"
  type       = "infra_metric"
  event      = "SystemSample"
  select     = "memoryFreePercent"
  comparison = "below"
  where      = "(hostname = '${local.jenkins_server_hostname}')"

  warning {
    duration      = 5
    value         = 30
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 20
    time_function = "all"
  }
}

# Jenkins server high disk usage
resource "newrelic_infra_alert_condition" "jenkins_server_high_disk_usage_alert" {
  policy_id = newrelic_alert_policy.jenkins_alert_policy.id

  name       = "${local.jenkins_application} - High Server Disk Usage"
  type       = "infra_metric"
  event      = "StorageSample"
  select     = "diskUsedPercent"
  comparison = "above"
  where      = "(hostname = '${local.jenkins_server_hostname}')"

  warning {
    duration      = 5
    value         = 80
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 90
    time_function = "all"
  }
}

# Jenkins process not running
resource "newrelic_infra_alert_condition" "jenkins_not_running_alert" {
  policy_id = newrelic_alert_policy.jenkins_alert_policy.id

  name          = "${local.jenkins_application} - Process Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname = '${local.jenkins_server_hostname}')"
  process_where = "processDisplayName = 'jenkins'"

  critical {
    duration = 5
    value    = 0
  }
}
