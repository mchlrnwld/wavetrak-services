locals {
  airflow_application            = "airflow"
  schedule_monitor_function_name = "airflow-scheduler-monitor-prod"
}

# Reference existing APM service
data "newrelic_entity" "apm_service_name" {
  name   = title(local.airflow_application)
  type   = "APPLICATION"
  domain = "APM"
}

# Reference existing alert channel
data "newrelic_alert_channel" "airflow_alert_channel" {
  name = "Airflow Platform"
}

# Create new alert policy
resource "newrelic_alert_policy" "airflow_platform_policy" {
  name                = "${title(local.airflow_application)} Platform"
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_alert_policy_channel" "airflow_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.airflow_platform_policy.id
  channel_ids = [data.newrelic_alert_channel.airflow_alert_channel.id]
}

# Airflow lambda NRQL Alert: Airflow Scheduler Monitor Lambda - Runtime Error
resource "newrelic_nrql_alert_condition" "airflow_scheduler_monitor_lambda" {
  name                         = "Lambda Function Error: ${local.schedule_monitor_function_name}"
  policy_id                    = newrelic_alert_policy.airflow_platform_policy.id
  type                         = "static"
  description                  = "scheduler_monitor_alert_description"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("./${path.module}/resources/schedule-monitor-nrql-query.tmpl", { schedule_monitor_function_name = local.schedule_monitor_function_name })
    evaluation_offset = "20"
  }

  critical {
    operator              = "above"
    threshold             = 1
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}

# Airflow high server CPU
resource "newrelic_infra_alert_condition" "airflow_server_high_cpu_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name       = "High Server CPU"
  type       = "infra_metric"
  event      = "SystemSample"
  select     = "cpuPercent"
  comparison = "above"
  where      = "(hostname LIKE '%${local.airflow_application}%')"

  warning {
    duration      = 30
    value         = 90
    time_function = "all"
  }

  critical {
    duration      = 40
    value         = 95
    time_function = "all"
  }
}

# Airflow server high disk usage
resource "newrelic_infra_alert_condition" "airflow_server_high_disk_usage_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name       = "High Server Disk Usage"
  type       = "infra_metric"
  event      = "StorageSample"
  select     = "diskFreePercent"
  comparison = "below"
  where      = "(hostname LIKE '%${local.airflow_application}%')"

  warning {
    duration      = 5
    value         = 30
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 20
    time_function = "all"
  }
}

# Airflow APM low apdex
resource "newrelic_alert_condition" "airflow_low_apdex_alert" {
  name            = "Low Apdex Score"
  enabled         = "true"
  entities        = [data.newrelic_entity.apm_service_name.application_id]
  policy_id       = newrelic_alert_policy.airflow_platform_policy.id
  type            = "apm_app_metric"
  condition_scope = "application"
  metric          = "apdex"

  term {
    priority      = "warning"
    duration      = 5
    operator      = "below"
    threshold     = ".97"
    time_function = "all"
  }

  term {
    priority      = "critical"
    duration      = 10
    operator      = "below"
    threshold     = ".95"
    time_function = "all"
  }
}

# Airflow RDS high CPU
resource "newrelic_infra_alert_condition" "airflow_rds_high_cpu_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name                 = "High RDS CPU"
  type                 = "infra_metric"
  select               = "provider.cpuUtilization.Average"
  comparison           = "above"
  where                = "displayName = 'sl-airflow-celery-prod'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = 80
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 90
    time_function = "all"
  }
}

# Airflow RDS high read latency
resource "newrelic_infra_alert_condition" "airflow_rds_high_read_latency_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name                 = "High RDS Read Latency"
  type                 = "infra_metric"
  select               = "provider.readLatency.Average"
  comparison           = "above"
  where                = "displayName = 'sl-airflow-celery-prod'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = ".015"
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = ".020"
    time_function = "all"
  }
}

# Airflow RDS high write latency
resource "newrelic_infra_alert_condition" "airflow_rds_high_write_latency_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name                 = "High RDS Write Latency"
  type                 = "infra_metric"
  select               = "provider.writeLatency.Average"
  comparison           = "above"
  where                = "displayName = 'sl-airflow-celery-prod'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = "0.001"
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = ".0015"
    time_function = "all"
  }
}

# Airflow RDS high read IOPS
resource "newrelic_infra_alert_condition" "airflow_high_read_iops_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name                 = "High RDS Read IOPS"
  type                 = "infra_metric"
  select               = "provider.readIops.Average"
  comparison           = "above"
  where                = "displayName = 'sl-airflow-celery-prod'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 20
    value         = "1.5"
    time_function = "all"
  }

  critical {
    duration      = 30
    value         = 2
    time_function = "all"
  }
}

# Airflow RDS high write IOPS
resource "newrelic_infra_alert_condition" "airflow_high_write_iops_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name                 = "High RDS Write IOPS"
  type                 = "infra_metric"
  select               = "provider.writeIops.Average"
  comparison           = "above"
  where                = "displayName = 'sl-airflow-celery-prod'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 20
    value         = 460
    time_function = "all"
  }

  critical {
    duration      = 30
    value         = 500
    time_function = "all"
  }
}

# Airflow RDS low storage
resource "newrelic_infra_alert_condition" "airflow_rds_storage_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name                 = "Low RDS Storage"
  type                 = "infra_metric"
  select               = "provider.freeStorageSpaceBytes.Average"
  comparison           = "below"
  where                = "displayName = 'sl-airflow-celery-prod'"
  integration_provider = "RdsDbInstance"

  warning {
    duration      = 5
    value         = 8000000000
    time_function = "all"
  }

  critical {
    duration      = 10
    value         = 4000000000
    time_function = "all"
  }
}

# Airflow flower process not running
resource "newrelic_infra_alert_condition" "airflow_flower_not_running_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name          = "Flower Process Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname = 'prod-airflow-celery-1')"
  process_where = "commandLine = '/usr/bin/python27 /usr/local/bin/flower -b redis://sl-bestmodels-redis-prod.aws.surfline.com:6379/15 --port=5555'"

  critical {
    duration = 10
    value    = 0
  }
}

# Airflow scheduler process not running
resource "newrelic_infra_alert_condition" "airflow_scheduler_not_running_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name          = "Scheduler Process Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname = 'prod-airflow-celery-1')"
  process_where = "processDisplayName = 'airflow-scheduler'"

  critical {
    duration = 10
    value    = 0
  }
}

# Airflow web server not running
resource "newrelic_infra_alert_condition" "airflow_web_server_not_running_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  name          = "Web Server Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname LIKE '%${local.airflow_application}%')"
  process_where = "commandLine = 'gunicorn: master [airflow-webserver]'"

  critical {
    duration = 30
    value    = 0
  }
}

# Airflow jobs not running
resource "newrelic_infra_alert_condition" "airflow_jobs_not_running_alert" {
  policy_id = newrelic_alert_policy.airflow_platform_policy.id

  enabled       = false
  name          = "No Jobs Run in 60 Minutes"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "hostname LIKE 'prod-airflow-celery-%' AND hostname != 'prod-airflow-celery-1'"
  process_where = "commandName = 'airflow'"

  critical {
    duration = 60
    value    = 0
  }
}
