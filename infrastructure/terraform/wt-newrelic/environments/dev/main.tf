terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "wt-newrelic/dev/terraform.tfstate"
    region = "us-west-1"
  }
}

provider "aws" {
  region = "us-west-1"
}

locals {
  environment = "dev"
}
