module "newrelic_events" {
  source = "../../modules/newrelic_events"

  company            = local.company
  events_application = "newrelic-events"
  environment        = local.environment

  alert_channel_name               = "Lower Tiers"
  alert_policy_name                = "Sandbox"
  ecs_task_monitor_alert_threshold = 2
  ecs_task_monitor_alert_duration  = 300
}
