terraform {
  backend "s3" {
    bucket = "sl-tf-state-legacy"
    key    = "wt-newrelic/legacy/terraform.tfstate"
    region = "us-west-1"
  }
}

provider "aws" {
  region = "us-west-1"
}

locals {
  environment = "legacy"
}
