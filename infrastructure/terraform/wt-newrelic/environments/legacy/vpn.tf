locals {
  vpn_application = "VPN"
}

data "aws_ssm_parameter" "vpn_integration_key" {
  name = "/${local.environment}/newrelic/vpn/integration-key"
}

module "vpn_newrelic_alerts" {
  source = "../../modules/vpn"

  # Common vars
  environment         = local.environment
  vpn_application     = local.vpn_application
  vpn_server_hostname = "surfline-vpn"

  # Alert channel
  alert_channel_service_key = data.aws_ssm_parameter.vpn_integration_key.value

  # VPN high server CPU
  vpn_high_cpu_warning_value  = 80
  vpn_high_cpu_critical_value = 90

  # VPN server low memory
  vpn_low_memory_warning_value  = 60
  vpn_low_memory_critical_value = 50

  # VPN server high disk usage
  high_disk_usage_warning_value  = 75
  high_disk_usage_critical_value = 80
}
