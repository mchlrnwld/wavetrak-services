# New Relic Resource Module

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Resources](#resources)
- [New Relic Provider](#new-relic-provider)
- [Terraform](#terraform)
  - [Using Terraform](#using-terraform)
    - [State Management](#state-management)
  - [Directory Structure](#directory-structure)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

This module contains standalone New Relic resources.

Whenever possible, New Relic resources should be created as part of the applications they monitor (i.e. lambda alerts created in the lambda module). All _other_ New Relic resources should be stored in this module.


## Resources

This module contains the code that provisions New Relic infrastructure alerts for the following resources:
- Airflow platform
- Chatops infrastructure
- Deployment dashboards
- Jenkins infrastructure
- RDS
- Redis common cluster
- Redshift infrastructure
- Varnish infrastructure
- VPN infrastructure
- Various alerts that monitor New Relic events

## New Relic Provider
This project uses Terraform's New Relic provider to provision alerting and monitoring resources.

New Relic developer credentials must be configured on your machine to successfully invoke the terraform commands in this project. For credential setup information, see the [New Relic Credentials ](https://github.com/Surfline/styleguide/tree/master/terraform#new-relic-credentials) section of the Wavetrak Styleguide.

For general information about configuring the New Relic resources, see [Configuring New Relic Alerts](https://github.com/Surfline/styleguide/tree/master/terraform#configuring-new-relic-alerts).

## Terraform

### Using Terraform

You must explicitly set two environment variable in order to use this terraform module: `NEW_RELIC_ACCOUNT_ID` and `NEW_RELIC_API_KEY`. For information on how to set these, please see the [styleguide](https://github.com/Surfline/styleguide/blob/master/terraform/README.md#new-relic-credentials).

The following commands are used to create resources:

```bash
ENV=prod make plan
ENV=prod make apply
```

#### State Management

State is uploaded to the S3 directory specified in the environment's `main.tf` file.

### Lower tier monitoring

All lower tier monitoring reports to the primary New Relic account. See the [styleguide](https://github.com/Surfline/styleguide/tree/master/terraform#using-terraform-managed-new-relic-alerts-in-lower-tiers) for more information.
