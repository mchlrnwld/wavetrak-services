locals {
  infra_alert_type          = "infra_metric"
  system_sample_event       = "SystemSample"
  above_comparison_operator = "above"
  warning_duration          = 3
  critical_duration         = 5
  time_function             = "all"
}

# Create alert channel
resource "newrelic_alert_channel" "vpn_alert_channel" {
  name = "${var.vpn_application} - ${var.environment}"
  type = "pagerduty"

  config {
    service_key = var.alert_channel_service_key
  }
}

# Create alert policy
resource "newrelic_alert_policy" "vpn_alert_policy" {
  name                = "${var.vpn_application} - ${var.environment}"
  incident_preference = "PER_CONDITION_AND_TARGET"
}

# Associate policy with channel
resource "newrelic_alert_policy_channel" "vpn_alert_policy_channel" {
  policy_id   = newrelic_alert_policy.vpn_alert_policy.id
  channel_ids = [newrelic_alert_channel.vpn_alert_channel.id]
}

# VPN high server CPU
resource "newrelic_infra_alert_condition" "vpn_server_high_cpu_alert" {
  policy_id = newrelic_alert_policy.vpn_alert_policy.id

  name       = "${var.vpn_application} ${title(var.environment)} - High Server CPU"
  type       = local.infra_alert_type
  event      = local.system_sample_event
  select     = "cpuPercent"
  comparison = local.above_comparison_operator
  where      = "(hostname = '${var.vpn_server_hostname}-${var.environment}')"

  warning {
    duration      = local.warning_duration
    value         = var.vpn_high_cpu_warning_value
    time_function = local.time_function
  }

  critical {
    duration      = local.critical_duration
    value         = var.vpn_high_cpu_critical_value
    time_function = local.time_function
  }
}

# VPN server low memory
resource "newrelic_infra_alert_condition" "vpn_server_low_memory_alert" {
  policy_id = newrelic_alert_policy.vpn_alert_policy.id

  name       = "${var.vpn_application} ${title(var.environment)} - Low Memory"
  type       = local.infra_alert_type
  event      = local.system_sample_event
  select     = "memoryFreePercent"
  comparison = "below"
  where      = "(hostname = '${var.vpn_server_hostname}-${var.environment}')"

  warning {
    duration      = local.warning_duration
    value         = var.vpn_low_memory_warning_value
    time_function = local.time_function
  }

  critical {
    duration      = local.critical_duration
    value         = var.vpn_low_memory_critical_value
    time_function = local.time_function
  }
}

# VPN server high disk usage
resource "newrelic_infra_alert_condition" "vpn_server_high_disk_usage_alert" {
  policy_id = newrelic_alert_policy.vpn_alert_policy.id

  name       = "${var.vpn_application} ${title(var.environment)} - High Server Disk Usage"
  type       = local.infra_alert_type
  event      = "StorageSample"
  select     = "diskUsedPercent"
  comparison = local.above_comparison_operator
  where      = "(hostname = '${var.vpn_server_hostname}-${var.environment}')"

  warning {
    duration      = local.warning_duration
    value         = var.high_disk_usage_warning_value
    time_function = local.time_function
  }

  critical {
    duration      = local.critical_duration
    value         = var.high_disk_usage_critical_value
    time_function = local.time_function
  }
}

# OpenVPN process not running
resource "newrelic_infra_alert_condition" "openvpn_not_running_alert" {
  policy_id = newrelic_alert_policy.vpn_alert_policy.id

  name          = "${var.vpn_application} ${title(var.environment)} - OpenVPN Process Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname = '${var.vpn_server_hostname}-${var.environment}')"
  process_where = "commandLine LIKE '/usr/sbin/openvpn%'"

  critical {
    duration = local.critical_duration
    value    = 0
  }
}
