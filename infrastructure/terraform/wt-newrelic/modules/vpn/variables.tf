variable "environment" {
  description = "Execution environment"
}

variable "vpn_application" {
  description = "Name of the VPN application"
}

variable "vpn_server_hostname" {
  description = "Hostname that New Relic uses to find VPN servers"
  default     = "surfline-vpn"
}

# Alert channel

variable "alert_channel_service_key" {
  description = "Key used for alert channel integration"
}

# VPN high server CPU

variable "vpn_high_cpu_warning_value" {
  description = "Threshold of CPU warning (percentage)"
}

variable "vpn_high_cpu_critical_value" {
  description = "Threshold of CPU critical alert (percentage)"
}

# VPN low memory

variable "vpn_low_memory_warning_value" {
  description = "Threshold of low memory warning"
}

variable "vpn_low_memory_critical_value" {
  description = "Threshold of low memory critical alert"
}

# High Disk usage

variable "high_disk_usage_warning_value" {
  description = "Threshold of disk usage warning alert"
}

variable "high_disk_usage_critical_value" {
  description = "Threshold of disk usage critical alert"
}
