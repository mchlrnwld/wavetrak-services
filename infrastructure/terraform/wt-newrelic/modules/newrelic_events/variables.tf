variable "company" {
  description = "The company for which these resources are being created"
}

variable "events_application" {
  description = "The application for which these resources are being created"
}

variable "environment" {
  description = "The environment in which these resources are being created"
}

variable "alert_channel_name" {
  description = "Name of New Relic alert channel"
}

variable "alert_policy_name" {
  description = "Name of New Relic alert policy"
}

variable "ecs_task_monitor_alert_threshold" {
  description = "Threshold of the alert"
}

variable "ecs_task_monitor_alert_duration" {
  description = "Duration prior to triggering alert"
}
