data "newrelic_alert_policy" "ecs_alert_policy" {
  name = var.alert_policy_name
}

resource "newrelic_nrql_alert_condition" "newrelic_events_task_checker_nrql" {
  name                         = var.environment == "prod" ? "ECS Task Error: Tasks Failing Reapeatedly" : "ECS Task Error: Tasks Failing Reapeatedly (${var.environment})"
  policy_id                    = data.newrelic_alert_policy.ecs_alert_policy.id
  type                         = "static"
  description                  = "NRQL query to alert on churning ECS tasks"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = templatefile("./${path.module}/resources/ecs-task-monitor.tmpl", {})
    evaluation_offset = "20"
  }

  critical {
    operator              = "above"
    threshold             = var.ecs_task_monitor_alert_threshold
    threshold_duration    = var.ecs_task_monitor_alert_duration
    threshold_occurrences = "all"
  }
}

resource "newrelic_nrql_alert_condition" "newrelic_events_chef_checker_nrql" {
  count = var.environment != "sandbox" ? 1 : 0

  name                         = var.environment == "prod" ? "ECS Chef Error: Chef Error in Logs" : "ECS Chef Error: Chef Error in Logs - ${var.environment}"
  policy_id                    = data.newrelic_alert_policy.ecs_alert_policy.id
  type                         = "static"
  description                  = "NRQL query to alert on chefrun errors"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query = templatefile("./${path.module}/resources/chef-log-monitor.tmpl", {
      environment = var.environment
    })
    evaluation_offset = "1"
  }

  critical {
    operator              = "above"
    threshold             = 0
    threshold_duration    = 60
    threshold_occurrences = "all"
  }
}
