# Staging

## Makefile

To make use of the Makefile set the environment variable `APP` to be the folder you wish to run

- `export APP=nami-aru-cdn`
- commands available
  - checkenv
  - init
  - plan
  - apply

## Nami Aru CDN

| Terraform Version | `0.12.28` |
| ----------------- | --------- |


The data was all migrated to S3 and the CDNs were replaced with CloudFront

### Deploy Nami Aru CDN

```shell
export APP=nami-aru-cdn
make plan
make apply
```
