terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "cdn-migration"
    region = "us-west-1"
  }
}

provider "aws" {
  region = var.region
}

module "bitgravity-cdn" {
  source = "../../../../modules/aws/cloudfront-with-s3"

  company            = var.company
  environment        = var.environment
  application        = "bitgravity"
  service            = "cdn"
  cdn_acm_count      = length(var.bitgravity_cdn_acm_domains)
  cdn_acm_domains    = var.bitgravity_cdn_acm_domains
  comment            = "${var.environment} BitGravity CDN wth S3"
  cdn_fqdn           = var.bitgravity_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
}

module "highwinds-bucket" {
  source = "../../../../modules/aws/highwinds/s3-bucket"

  company            = var.company
  environment        = var.environment
  application        = "highwinds"
  service            = "cdn"
  comment            = "${var.environment} Highwinds CDN wth S3"
  versioning_enabled = false
  allowed_origins    = "*"
}

module "highwinds-cdn-buoyweather" {
  source = "../../../../modules/aws/highwinds/cloudfront"

  company                = var.company
  environment            = var.environment
  application            = "highwinds"
  service                = "cdn"
  s3_bucket              = module.highwinds-bucket.bucket_name
  s3_bucket_domain       = module.highwinds-bucket.bucket_domain
  origin_access_identity = module.highwinds-bucket.origin_access_identity
  origin_path            = var.highwinds_cdn_buoyweather_origin_path
  cdn_acm_domain         = var.highwinds_cdn_buoyweather_acm_domain
  comment                = "${var.environment} Highwinds CDN wth S3"
  cdn_aliases            = var.highwinds_cdn_buoyweather_aliases
  default_ttl            = "0"
  max_ttl                = "0"

  # Set to zero because legacy was leveraging browser cache(Set to 86400 seconds), but not the cdn cache
  # See https://striketracker.highwinds.com/accounts/z4t6m6c7/configure/hosts/b4x6p9m7/scope/65788
  # Shown under the CDN Caching Policy
}

module "highwinds-cdn-cdn02_surfline" {
  source = "../../../../modules/aws/highwinds/cloudfront"

  company                = var.company
  environment            = var.environment
  application            = "highwinds"
  service                = "cdn"
  s3_bucket              = module.highwinds-bucket.bucket_name
  s3_bucket_domain       = module.highwinds-bucket.bucket_domain
  origin_access_identity = module.highwinds-bucket.origin_access_identity
  origin_path            = var.highwinds_cdn_cdn02_origin_path
  cdn_acm_domain         = var.highwinds_cdn_cdn02_acm_domain
  comment                = "${var.environment} Highwinds CDN wth S3"
  cdn_aliases            = var.highwinds_cdn_cdn02_aliases
  default_ttl            = "0"
  max_ttl                = "0"

  # Set to zero because legacy was leveraging browser cache(Set to 86400 seconds), but not the cdn cache
  # See https://striketracker.highwinds.com/accounts/z4t6m6c7/configure/hosts/b4x6p9m7/scope/65788
  # Shown under the CDN Caching Policy
}

module "highwinds-cdn-cdn-surfline" {
  source = "../../../../modules/aws/highwinds/cloudfront"

  company                = var.company
  environment            = var.environment
  application            = "highwinds"
  service                = "cdn"
  s3_bucket              = module.highwinds-bucket.bucket_name
  s3_bucket_domain       = module.highwinds-bucket.bucket_domain
  origin_access_identity = module.highwinds-bucket.origin_access_identity
  origin_path            = var.highwinds_cdn_cdn-surfline_origin_path
  cdn_acm_domain         = var.highwinds_cdn_cdn-surfline_acm_domain
  comment                = "${var.environment} Highwinds CDN wth S3"
  cdn_aliases            = var.highwinds_cdn_cdn-surfline_aliases
  default_ttl            = "0"
  max_ttl                = "0"

  # Set to zero because legacy was leveraging browser cache(Set to 86400 seconds), but not the cdn cache
  # See https://striketracker.highwinds.com/accounts/z4t6m6c7/configure/hosts/b4x6p9m7/scope/65788
  # Shown under the CDN Caching Policy
}

module "highwinds-cdn-ads" {
  source = "../../../../modules/aws/highwinds/cloudfront"

  company                = var.company
  environment            = var.environment
  application            = "highwinds"
  service                = "cdn"
  s3_bucket              = module.highwinds-bucket.bucket_name
  s3_bucket_domain       = module.highwinds-bucket.bucket_domain
  origin_access_identity = module.highwinds-bucket.origin_access_identity
  origin_path            = var.highwinds_cdn_ads_origin_path
  cdn_acm_domain         = var.highwinds_cdn_ads_acm_domain
  comment                = "${var.environment} Highwinds CDN wth S3"
  cdn_aliases            = var.highwinds_cdn_ads_aliases
  default_ttl            = "0"
  max_ttl                = "0"

  # Set to zero because legacy was leveraging browser cache(Set to 86400 seconds), but not the cdn cache
  # See https://striketracker.highwinds.com/accounts/z4t6m6c7/configure/hosts/b4x6p9m7/scope/65788
  # Shown under the CDN Caching Policy
}

module "highwinds-asg" {
  source = "../../../../modules/aws/ftp-asg"

  company                = var.company
  environment            = var.environment
  application            = "highwinds"
  service                = "asg"
  region                 = var.region
  desired_instance_count = var.highwinds_desired_instance_count
  instance_subnets       = var.ftp_instance_subnets
  s3_bucket_name         = module.highwinds-bucket.bucket_name
  image_id               = var.highwinds_image_id
  instance_type          = var.highwinds_instance_type
  key_name               = var.highwinds_key_name
  default_vpc            = var.default_vpc
  chef_bucket_name       = var.chef_bucket_name
  chef_version           = var.chef_version
  ftp_zone               = var.highwinds_ftp_zone
  ftp_domain             = var.highwinds_ftp_domain
  ftp_internal_domain    = var.highwinds_ftp_internal_domain
}

module "e-cdn-fishtrack" {
  source = "../../../../modules/aws/edgecast-cdn-s3"

  company            = var.company
  environment        = var.environment
  application        = "edgecast-ft"
  service            = "cdn"
  cdn_acm_count      = length(var.fishtrack_cdn_acm_domains)
  cdn_acm_domains    = var.fishtrack_cdn_acm_domains
  comment            = "${var.environment} EdgeCast CDN with S3"
  cdn_fqdn           = var.fishtrack_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
}

module "edgecast-cdn" {
  company            = var.company
  environment        = var.environment
  application        = "edgecast"
  service            = "cdn"
  cdn_acm_count      = length(var.edgecast_cdn_acm_domains)
  cdn_acm_domains    = var.edgecast_cdn_acm_domains
  comment            = "${var.environment} EdgeCast CDN with S3"
  cdn_fqdn           = var.edgecast_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"

  source = "../../../../modules/aws/edgecast-cdn-s3"
}

module "edgecast-cdns" {
  company            = var.company
  environment        = var.environment
  application        = "edgecast"
  service            = "cdn"
  cdn_s3_count       = length(var.edgecast_cf_s3_map)
  s3_bucket_map      = var.edgecast_cf_s3_map
  comment            = "${var.environment} EdgeCast CDN without S3"
  versioning_enabled = false
  allowed_origins    = "*"

  source = "../../../../modules/aws/cloudfront-without-s3"
}

module "edgecast-custom" {
  company            = var.company
  environment        = var.environment
  application        = "edgecast"
  service            = "cdn"
  comment            = "${var.environment} EdgeCast CDN without S3"
  domain_map         = var.edgecast_custom_map
  domain_count       = length(var.edgecast_custom_map)
  versioning_enabled = false
  allowed_origins    = "*"

  source = "../../../../modules/aws/cloudfront-custom"
}

module "edgecast-custom-tile" {
  company     = var.company
  environment = var.environment
  application = "edgecast"
  service     = "cdn"
  comment     = "${var.environment} tile EdgeCast CDN without S3"
  domain_map = [
    var.edgecast_tile_map["domain_aliases"],
    var.edgecast_tile_map["certificate"],
    var.edgecast_tile_map["origin"]
  ]
  query_strings      = var.edgecast_tile_map["forward_query_strings"]
  versioning_enabled = false
  allowed_origins    = "*"

  source = "../../../../modules/aws/cloudfront-custom"
}
