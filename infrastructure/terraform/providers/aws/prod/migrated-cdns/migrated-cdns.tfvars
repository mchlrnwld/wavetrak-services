region               = "us-west-1"
environment          = "prod"
company              = "sl"
state_bucket         = "sl-tf-state-prod"
state_key            = "cdn_migration"
default_vpc          = "vpc-116fdb74"
ftp_instance_subnets = ["subnet-baab36df", "subnet-debb6987"]
chef_bucket_name     = "sl-chef-prod"
chef_version         = "13.2.20"

# BitGravity CDNs
bitgravity_cdn_fqdn = {
  "cdn-surfline.com" = [
    "livestream.cdn-surfline.com",
    "livestreamwc.cdn-surfline.com"
  ]
  "surfline.com" = [
    "media1.surfline.com"
  ]
}
bitgravity_cdn_acm_domains = [
  "cdn-surfline.com",
  "surfline.com"
]

# HighWinds CDNs
highwinds_cdn_buoyweather_origin_path  = "/cds/buoyweather"
highwinds_cdn_buoyweather_acm_domain   = "buoyweather.com"
highwinds_cdn_buoyweather_aliases      = ["cdn.buoyweather.com"]
highwinds_cdn_cdn02_origin_path        = ""
highwinds_cdn_cdn02_acm_domain         = "surfline.com"
highwinds_cdn_cdn02_aliases            = ["cdn02.surfline.com"]
highwinds_cdn_cdn-surfline_origin_path = "/cds"
highwinds_cdn_cdn-surfline_acm_domain  = "cdn-surfline.com"
highwinds_cdn_cdn-surfline_aliases     = ["a.cdn-surfline.com", "i.cdn-surfline.com"]
highwinds_cdn_ads_origin_path          = "/cds/ads"
highwinds_cdn_ads_acm_domain           = "cdn-surfline.com"
highwinds_cdn_ads_aliases              = ["ads.cdn-surfline.com"]

highwinds_desired_instance_count = "1"
highwinds_image_id               = "ami-a51f27c5"
highwinds_key_name               = "sturdy_surfline_id_rsa"
highwinds_instance_type          = "t2.micro"
highwinds_ftp_domain             = "hwupload.surfline.com"
highwinds_ftp_zone               = "surfline.com"
highwinds_ftp_internal_domain    = "internal-hwupload.surfline.com"

# EdgeCast CDNs
edgecast_cdn_acm_domains = ["cdn-surfline.com"]
edgecast_cdn_fqdn = {
  "cdn-surfline.com" = [
    "e.cdn-surfline.com",
    # "f.cdn-surfline.com",
    # "live-cam-archive.cdn-surfline.com",
    # "mobile-assets-qa.cdn-surfline.com",
    # "mobile-assets.cdn-surfline.com",
    # "p.cdn-surfline.com",
    # "suser-upload.cdn-surfline.com",
    # "charts.cdn-surfline.com",
    # "slcharts01.cdn-surfline.com",
    # "slcharts02.cdn-surfline.com",
    # "tile.cdn-surfline.com"
  ]
  "cdn-fishtrack.com" = [
    # "e.cdn-fishtrack.com",
    # "fish-reports.cdn-fishtrack.com",
    # "p.cdn-fishtrack.com",
    # "charts.cdn-fishtrack.com"
  ]
  "surfline.tv" = [
    # "www.surfline.tv", # migrated to d2585935b5p4px.cloudfront.net ?
    # "x1.surfline.tv",
    # "surfline.tv"
  ]
}
fishtrack_cdn_acm_domains = ["cdn-fishtrack.com"]
fishtrack_cdn_fqdn = {
  "cdn-fishtrack.com" = [
    "e.cdn-fishtrack.com"
  ]
}
# Format for variables is domain alias,cert domain,origin,ttl
edgecast_cf_s3_map = [
  "fish-reports.cdn-fishtrack.com,*.cdn-fishtrack.com,fishtrack-fish-reports,600",
  "p.cdn-fishtrack.com,*.cdn-fishtrack.com,fishtrack-photos",
  "charts.cdn-surfline.com,*.cdn-surfline.com,charts.surfline.com,600",
  "f.cdn-surfline.com,*.cdn-surfline.com,fishtrack-photos",
  "live-cam-archive.cdn-surfline.com,*.cdn-surfline.com,live-cam-archive",
  "mobile-assets-qa.cdn-surfline.com,*.cdn-surfline.com,sl-mobile-webapp-assets-origin-qa",
  "mobile-assets.cdn-surfline.com,*.cdn-surfline.com,sl-mobile-webapp-assets-origin",
  "p.cdn-surfline.com,*.cdn-surfline.com,surfline-photos",
  "suser-upload.cdn-surfline.com,*.cdn-surfline.com,sl-superuser-upload,600",
  "premium.surfline.com,*.surfline.com,premium.surfline.com,600",
  "live-camera-videos.cdn-surfline.com,*.cdn-surfline.com,live-cam-archive",
  "sl-live-cam-archive-prod.cdn-surfline.com,*.cdn-surfline.com,sl-live-cam-archive-prod"
]
# Format for variables is domain alias,cert domain,origin
edgecast_custom_map = [
  "charts.cdn-fishtrack.com,*.cdn-fishtrack.com,bwdev.buoyweather.com",
  "slcharts01.cdn-surfline.com,*.cdn-surfline.com,slcharts01.surfline.com",
  "slcharts02.cdn-surfline.com,*.cdn-surfline.com,slcharts02.surfline.com",
]
edgecast_tile_map = {
  "domain_aliases"        = "tile.cdn-surfline.com",
  "certificate"           = "*.cdn-surfline.com",
  "origin"                = "tile.surfline.com",
  "forward_query_strings" = "true"
}
