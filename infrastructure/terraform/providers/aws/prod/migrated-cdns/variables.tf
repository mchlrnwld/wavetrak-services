variable "region" {
}

variable "environment" {
}

variable "company" {
}

variable "state_bucket" {
}

variable "state_key" {
}

variable "default_vpc" {
}

variable "chef_bucket_name" {
}

variable "chef_version" {
}

variable "ftp_instance_subnets" {
  type = list(string)
}

variable "bitgravity_cdn_acm_domains" {
  type = list(string)
}

variable "bitgravity_cdn_fqdn" {
  type = map(list(string))
}

variable "highwinds_cdn_buoyweather_origin_path" {
}

variable "highwinds_cdn_buoyweather_acm_domain" {
}

variable "highwinds_cdn_buoyweather_aliases" {
  type = list(string)
}

variable "highwinds_cdn_cdn02_origin_path" {
}

variable "highwinds_cdn_cdn02_acm_domain" {
}

variable "highwinds_cdn_cdn02_aliases" {
  type = list(string)
}

variable "highwinds_cdn_cdn-surfline_origin_path" {
}

variable "highwinds_cdn_cdn-surfline_acm_domain" {
}

variable "highwinds_cdn_cdn-surfline_aliases" {
  type = list(string)
}

variable "highwinds_cdn_ads_origin_path" {
}

variable "highwinds_cdn_ads_acm_domain" {
}

variable "highwinds_cdn_ads_aliases" {
  type = list(string)
}

variable "highwinds_desired_instance_count" {
}

variable "highwinds_image_id" {
}

variable "highwinds_instance_type" {
}

variable "highwinds_key_name" {
}

variable "highwinds_ftp_zone" {
}

variable "highwinds_ftp_domain" {
}

variable "highwinds_ftp_internal_domain" {
}

variable "edgecast_cf_s3_map" {
  type = list(string)
}

variable "edgecast_cdn_acm_domains" {
  type = list(string)
}

variable "edgecast_cdn_fqdn" {
  type = map(list(string))
}

variable "fishtrack_cdn_acm_domains" {
  type = list(string)
}

variable "fishtrack_cdn_fqdn" {
  type = map(list(string))
}

variable "edgecast_custom_map" {
  type = list(string)
}

variable "edgecast_tile_map" {
  type = map(string)
}
