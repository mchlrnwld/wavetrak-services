variable "region" {
}

variable "environment" {
}

variable "company" {
}

variable "nami_cdn_acm_domains" {
  type = list(string)
}

variable "nami_cdn_fqdn" {
  type = map(list(string))
}

variable "nami_default_ttl" {
  default = 60
}

variable "nami_max_ttl" {
  default = 60
}

variable "whitelist_headers" {
  type = list(string)
}
