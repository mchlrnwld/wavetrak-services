provider "aws" {
  region = var.region
}

provider "aws" {
  region = "us-east-1"
  alias  = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "nami-aru-cdn"
    region = "us-west-1"
  }
}

module "nami-aru-cdn" {
  source             = "../../../../modules/aws/cloudfront-with-s3"
  company            = var.company
  environment        = var.environment
  application        = "nami-aru-cdn"
  service            = "cdn"
  cdn_acm_count      = length(var.nami_cdn_acm_domains)
  cdn_acm_domains    = var.nami_cdn_acm_domains
  comment            = "${var.environment} Nami Aru CDN wth S3"
  cdn_fqdn           = var.nami_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
  default_ttl        = var.nami_default_ttl
  max_ttl            = var.nami_max_ttl
  whitelist_headers  = var.whitelist_headers
}

resource "aws_iam_user" "namiaru_upload" {
  name = "namiaru-upload"
}

resource "aws_iam_group" "namiaru_upload" {
  name = "namiaru-upload"
  path = "/"
}

resource "aws_iam_group_membership" "namiaru_upload_members" {
  name = "namiaru_upload"

  users = [
    aws_iam_user.namiaru_upload.name,
  ]

  group = aws_iam_group.namiaru_upload.name
}

resource "aws_iam_group_policy_attachment" "test-attach" {
  group      = aws_iam_group.namiaru_upload.name
  policy_arn = aws_iam_policy.namiaru_policy.arn
}

data "template_file" "s3_policy" {
  template = templatefile("${path.module}/resources/namiaru-policy.json", {
    bucket = module.nami-aru-cdn.bucket_name
  })
}

resource "aws_iam_policy" "namiaru_policy" {
  name        = "${var.company}-namiaru-policy-${var.environment}"
  description = "namiaru_policy"
  policy      = data.template_file.s3_policy.rendered
}
