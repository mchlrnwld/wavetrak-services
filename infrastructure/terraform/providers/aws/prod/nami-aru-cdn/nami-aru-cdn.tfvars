region      = "us-west-1"
company     = "sl"
environment = "prod"

# Nami Aru CDNs
nami_cdn_fqdn = {
  "cdn-surfline.com" = [
    "namiaru-upload-migration.cdn-surfline.com",
    "namiaru-upload.cdn-surfline.com"
  ]
}

nami_cdn_acm_domains = [
  "cdn-surfline.com"
]

whitelist_headers = [
  "Origin"
]
