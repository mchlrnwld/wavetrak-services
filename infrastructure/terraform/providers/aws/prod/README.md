# Prod

## Makefile

To make use of the Makefile set the environment variable `APP` to be the folder you wish to run

- `export APP=migrated-cdns`
- commands available
  - checkenv
  - init
  - plan
  - apply

## Migrated CDNs

| Terraform Version | `0.12.28` |
| ----------------- | --------- |

Contains the CDNs that were migrated from

- HighWinds
  - Also has an FTP Server that is S3 backed
- EdgeCast
  - Also pointed to a couple of ELBs
- BitGravity

The data was all migrated to S3 and the CDNs were replaced with CloudFront

### Deploy Migrated CDNs

```shell
export APP=migrated-cdns
make plan
make apply
```

---

## Nami Aru CDN

| Terraform Version | `0.10.8` |
| ----------------- | -------- |

The data was all migrated to S3 and the CDNs were replaced with CloudFront

### Deploy Nami Aru CDN

```shell
export APP=nami-aru-cdn
make plan
make apply
```
