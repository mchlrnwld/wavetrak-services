terraform {
  required_version = ">= 0.12"
  required_providers {
    pagerduty = "~> 1.7.3"
  }
}
