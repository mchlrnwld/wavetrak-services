# Overview

## Summary

Sets up PagerDuty integration with Cloudwatch.

## Updates

A PagerDuty api v2 token is required to use this module. Specify it in the on the command line in the following manner:

- `terragrunt plan -var pagerduty_token=LONGVALUEFROMLASTPASS`
