provider "pagerduty" {
  token = var.pagerduty_token
}

data "pagerduty_vendor" "aws_cloudwatch" {
  name_regex = "Cloudwatch$"
}

resource "pagerduty_service" "alert_critical" {
  name                 = var.pagerduty_critical_alert_name
  description          = var.pagerduty_critical_alert_description
  auto_resolve_timeout = 43200
  escalation_policy    = var.pagerduty_escalation_policy_sturdy_support
}

resource "pagerduty_service_integration" "aws_cloudwatch" {
  name    = "Amazon Cloudwatch"
  type    = "event_transformer_api_inbound_integration"
  service = pagerduty_service.alert_critical.id
  vendor  = data.pagerduty_vendor.aws_cloudwatch.id
}

output "aws_cloudwatch_integration_key" {
  value = pagerduty_service_integration.aws_cloudwatch.integration_key
}
