resource "cloudflare_page_rule" "news" {
  priority = 3
  status   = "active"
  target   = "*.buoyweather.com/news/*"
  actions {
    security_level = "essentially_off"
    waf            = "off"
  }
  zone_id = resource.cloudflare_zone.buoyweather.id
}

resource "cloudflare_page_rule" "api_v1" {
  priority = 2
  status   = "active"
  target   = "*.buoyweather.com/api/v1/*"
  actions {
    always_online     = "on"
    browser_cache_ttl = 7200
    browser_check     = "off"
    cache_level       = "bypass"
    disable_security  = true
    mirage            = "off"
    security_level    = "essentially_off"
  }
  zone_id = resource.cloudflare_zone.buoyweather.id
}

resource "cloudflare_page_rule" "default" {
  priority = 1
  status   = "active"
  target   = "*.buoyweather.com/*"
  actions {
    browser_cache_ttl = 14400
  }
  zone_id = resource.cloudflare_zone.buoyweather.id
}

