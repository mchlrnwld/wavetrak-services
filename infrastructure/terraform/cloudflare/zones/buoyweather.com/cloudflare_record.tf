locals {
  proxied_cname_records = {
    "boatus"              = "www.buoyweather.com"
    "bw4"                 = "www.buoyweather.com"
    "cpanel"              = "buoyweather.com"
    "ensim"               = "buoyweather.com"
    "ftp"                 = "buoyweather.com"
    "mobile"              = "www.buoyweather.com"
    "pa"                  = "www.buoyweather.com"
    "plesk"               = "buoyweather.com"
    "product-cdn.staging" = "d1x2dykseoivi8.cloudfront.net"
    "webmail"             = "buoyweather.com"
    "www"                 = "web-proxy.prod.surfline.com"
    "www-cloudflare"      = "web-proxy.prod.surfline.com"
  }
  unproxied_cname_records = {
    "admin"                                         = "internal-bw-wordpress-wordpress-prod-1036080462.us-west-1.elb.amazonaws.com"
    "analytic"                                      = "buoyweather.112.2o7.net"
    "analytics"                                     = "buoyweather.com.102.112.2o7.net"
    "api"                                           = "sl-ecs-web-proxy-int-prod-245790451.us-west-1.elb.amazonaws.com"
    "autodiscover"                                  = "autodiscover.outlook.com"
    "aws-api"                                       = "sl-marineapi-coldfusion-prod.aws.surfline.com"
    "aws-www"                                       = "sl-lolabw5-science-prod-442194381.us-west-1.elb.amazonaws.com"
    "benefits"                                      = "unbouncepages.com"
    "buoyweather-api.prod"                          = "internal-sl-int-core-srvs-1-prod-768518521.us-west-1.elb.amazonaws.com"
    "buoyweather-api.staging"                       = "internal-sl-int-core-srvs-1-staging-2106918770.us-west-1.elb.amazonaws.com"
    "buoyweather.com"                               = "web-proxy.prod.surfline.com"
    "bw2"                                           = "sl-lolabw2-science-prod.aws.surfline.com"
    "bw3"                                           = "sl-lolabw3-science-prod.aws.surfline.com"
    "bw5"                                           = "web-proxy.prod.surfline.com"
    "bwdev"                                         = "sl-lolabwdev-science-prod.aws.surfline.com"
    "cdn"                                           = "drbhm2zh4qehf.cloudfront.net"
    "cdn-legacy"                                    = "deo5fswh3j5v9.cloudfront.net"
    "cdn-prod"                                      = "d2f7mm33qkomqo.cloudfront.net"
    "cdn-staging"                                   = "d1x2dykseoivi8.cloudfront.net"
    "critsend2._domainkey"                          = "dkim.critsend.com"
    "customer"                                      = "e.customeriomail.com"
    "developer"                                     = "proxy.kickofflabs.com"
    "enterpriseenrollment"                          = "enterpriseenrollment.manage.microsoft.com"
    "enterpriseregistration"                        = "enterpriseregistration.windows.net"
    "k1._domainkey"                                 = "dkim.mcsv.net"
    "lyncdiscover"                                  = "webdir.online.lync.com"
    "msoid"                                         = "clientconfig.microsoftonline-p.net"
    "my"                                            = "sl-ecs-web-proxy-int-prod-245790451.us-west-1.elb.amazonaws.com"
    "prod"                                          = "web-proxy.prod.surfline.com"
    "sailthru._domainkey.email"                     = "email.buoyweather.com.sailthrudkim.com"
    "sailthru-horizon"                              = "horizon.sailthru.com"
    "sailthru-link"                                 = "cb.sailthru.com"
    "segment"                                       = "whitelabel-berry.xid.segment.com"
    "services"                                      = "sl-ecs-services-proxy-prod-29300544.us-west-1.elb.amazonaws.com"
    "services.staging"                              = "sl-ecs-services-proxy-staging-44399717.us-west-1.elb.amazonaws.com"
    "sip"                                           = "sipdir.online.lync.com"
    "staging"                                       = "web-proxy.staging.surfline.com"
    "subscribe"                                     = "sl-universal-funnel-prod-elb-1690143062.us-west-1.elb.amazonaws.com"
    "support"                                       = "buoyweather.zendesk.com"
    "webapp.prod"                                   = "internal-sl-int-core-srvs-1-prod-768518521.us-west-1.elb.amazonaws.com"
    "webapp.staging"                                = "internal-sl-int-core-srvs-1-staging-2106918770.us-west-1.elb.amazonaws.com"
    "_0687bc78096ee25ecc3bf9457716b0eb.sandbox"     = "_5175797823496fdf6551efb2e19c7679.tljzshvwok.acm-validations.aws"
    "_3d0417c4b378bacc21833bf06960bd42"             = "_3cb536f004b330da50e3522101b588de.tljzshvwok.acm-validations.aws"
    "_a8c2d08adbba01bbe93cd44c634ffa61"             = "_59273771e9a51cebb6ecfb53e9b715ce.tljzshvwok.acm-validations.aws"
    "_bd19529938c3d1a76773f30b20eb0663.cdn-staging" = "_e22f3030ba96f8e3206ac2f0ff17c843.bbfvkzsszw.acm-validations.aws"
    "_d6578ef0aeeb18536b6440049aa890b2.staging"     = "_7e300d4d16b4b63e8e2ac18e5bbc23f5.tljzshvwok.acm-validations.aws"
  }

  unproxied_a_records = {
    "dev"            = "10.70.132.118"
    "legacy-int"     = "192.168.147.195"
    "legacy.sandbox" = "10.70.135.217"
    "sandbox"        = "10.70.66.226"
  }

  zone_id = resource.cloudflare_zone.buoyweather.id

}

resource "cloudflare_record" "proxied_cname" {
  for_each = local.proxied_cname_records
  name     = each.key
  value    = each.value
  type     = "CNAME"
  proxied  = true
  ttl      = 1
  zone_id  = local.zone_id
}

resource "cloudflare_record" "unproxied_cname" {
  for_each = local.unproxied_cname_records
  name     = each.key
  value    = each.value
  type     = "CNAME"
  proxied  = false
  ttl      = 1
  zone_id  = local.zone_id
}

resource "cloudflare_record" "unproxied_a" {
  for_each = local.unproxied_a_records
  name     = each.key
  value    = each.value
  type     = "A"
  proxied  = false
  ttl      = 1
  zone_id  = local.zone_id
}

resource "cloudflare_record" "MX-buoyweather-com" {
  name     = "buoyweather.com"
  priority = 0
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "buoyweather-com.mail.protection.outlook.com"
  zone_id  = local.zone_id
}

resource "cloudflare_record" "MX-cio22037-1" {
  name     = "cio22037"
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "mxb.mailgun.org"
  priority = 10
  zone_id  = local.zone_id
}

resource "cloudflare_record" "MX-cio22037-0" {
  name     = "cio22037"
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "mxa.mailgun.org"
  priority = 10
  zone_id  = local.zone_id
}

resource "cloudflare_record" "SRV-_sipfederationtls-_tcp" {
  data = {
    name     = "buoyweather.com"
    port     = 5061
    priority = 100
    proto    = "_tcp"
    service  = "_sipfederationtls"
    target   = "sipfed.online.lync.com"
    weight   = 1
  }
  name     = "_sipfederationtls._tcp"
  priority = 100
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = local.zone_id
}

resource "cloudflare_record" "SRV-_sip-_tls" {
  data = {
    name     = "buoyweather.com"
    port     = 443
    priority = 100
    proto    = "_tls"
    service  = "_sip"
    target   = "sipdir.online.lync.com"
    weight   = 1
  }
  name     = "_sip._tls"
  priority = 100
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = local.zone_id
}

resource "cloudflare_record" "TXT-22037-_cio" {
  name    = "22037._cio"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "ba5ea09c9fd69fdd4caddcffaace1aeef782535a"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-buoyweather-com-0" {
  name    = "buoyweather.com"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=spf1 include:spf.mandrillapp.com include:customeriomail.com include:servers.mcsv.net mx ptr v=spf1 include:spf.protection.outlook.com -all"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-buoyweather-com-1" {
  name    = "buoyweather.com"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "google-site-verification=a6THB9ZYpYpQEX357wTtxaAf4S_9JZrzIg614LUjikI"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-cio22037" {
  name    = "cio22037"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=spf1 include:mailgun.org ~all"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-default-_domainkey" {
  name    = "default._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "\"k=rsa\\; p=MHwwDQYJKoZIhvcNAQEBBQADawAwaAJhAKYyPxVB+8jcsieOkbG17UFySLz9l4dgkYAjGWcNFZj0Lzw5kbwzjt3Kr7ekZ4TsUlR3MY1lgYAizPXVjY0o81f8ElJRCk+QWCYspTMMxrHjaQpR+i+vrRbFb43H5DV4wQIDAQAB\""
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-_domainkey-0" {
  name    = "_domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "\"t=y\\; o=~\\;\""
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-_domainkey-1" {
  name    = "_domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "t=y; o=~;"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-key1-_domainkey" {
  name    = "key1._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "\"k=rsa\\; p=MHwwDQYJKoZIhvcNAQEBBQADawAwaAJhAKYyPxVB+8jcsieOkbG17UFySLz9l4dgkYAjGWcNFZj0Lzw5kbwzjt3Kr7ekZ4TsUlR3MY1lgYAizPXVjY0o81f8ElJRCk+QWCYspTMMxrHjaQpR+i+vrRbFb43H5DV4wQIDAQAB\""
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-mandrill-_domainkey" {
  name    = "mandrill._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB;"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-pic-_domainkey-cio22037" {
  name    = "pic._domainkey.cio22037"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHvLRlyz0ccgJYJXi7QXrccZNK6Eb6VWsDy2zrte7CNM9De5wfXphQF0QvV+IvSBlYNqLW7F1csMZnJlYO/U8HYuU/YKYid2gDCoe/JJx2jZ8Mo9IfwFfTiRx9XM4+wdyR2cFPh+psFxEcMa8dzuTZbpuc6UNuHBNR/jCVxw35QQIDAQAB"
  zone_id = local.zone_id
}

resource "cloudflare_record" "TXT-smtpapi-_domainkey" {
  name    = "smtpapi._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "k=rsa; t=s; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPtW5iwpXVPiH5FzJ7Nrl8USzuY9zqqzjE0D1r04xDN6qwziDnmgcFNNfMewVKN2D1O+2J9N14hRprzByFwfQW76yojh54Xu3uSbQ3JP0A7k8o8GutRF8zbFUA8n0ZH2y0cIEjMliXY4W4LwPA7m4q0ObmvSjhd63O9d8z1XkUBwIDAQAB"
  zone_id = local.zone_id
}
