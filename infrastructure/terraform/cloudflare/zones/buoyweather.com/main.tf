terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "cloudflare/zones/buoyweather.com/terraform.tfstate"
    region = "us-west-1"
  }
}

provider "cloudflare" {
  account_id = "bb3b4bac67a854c871c8493475db3af8"
}
