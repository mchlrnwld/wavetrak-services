resource "cloudflare_zone" "buoyweather" {
  zone   = "buoyweather.com"
  plan   = "enterprise"
  type   = "full"
  paused = false
}
