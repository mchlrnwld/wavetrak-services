/*
 * cloudflare_records with proxied = false (also known as DNS only)
 */

locals {
  value_records = {
    "A" = {
      "api"             = "169.60.181.252"
      "autoteststore"   = "91.186.19.196"
      "ben"             = "169.60.181.252"
      "beta"            = "169.60.181.252"
      "cam"             = "31.193.138.45"
      "camimages"       = "31.193.138.45"
      "ciswan"          = "82.69.84.117"
      "danstore"        = "91.186.19.192"
      "develop"         = "169.60.181.252"
      "developer"       = "169.60.181.252"
      "eoghan"          = "169.60.181.252"
      "gavin"           = "173.192.35.21"
      "gavinstore"      = "91.186.19.192"
      "gavinswan"       = "82.69.84.117"
      "images"          = "169.60.181.253"
      "images.pipeline" = "34.202.184.221"
      "jira"            = "92.48.118.39"
      "latest"          = "169.60.181.252"
      "liam"            = "169.60.181.252"
      "mag"             = "54.194.41.141"
      "md"              = "169.60.181.252"
      "mentawai-prod"   = "31.193.138.45"
      "michael"         = "169.60.181.252"
      "mijahn"          = "169.60.181.252"
      "newsletter"      = "213.229.84.199"
      "nickstore"       = "91.186.19.192"
      "ns1"             = "151.236.45.23"
      "ns2"             = "213.229.113.28"
      "piwik"           = "169.60.181.255"
      "present"         = "151.236.48.232"
      "qasecure"        = "91.186.19.195"
      "qastore"         = "91.186.19.195"
      "release"         = "169.60.181.252"
      "sandbox"         = "169.60.181.252"
      "secure"          = "91.186.19.194"
      "socialassets"    = "169.60.181.253"
      "static"          = "169.60.181.253"
      "storeimages"     = "91.186.19.199"
      "tbl"             = "169.60.181.252"
      "ternate"         = "173.192.5.32"
      "testing"         = "169.60.181.252"
      "tide"            = "169.60.181.254"
      "video"           = "169.60.181.253"
      "will"            = "169.60.181.252"
      "wstest"          = "151.236.48.232"

      # in conflict with the NS record defined below
      # "store"           = "91.186.19.193"
      # "www.store"       = "91.186.19.192"
    }
    "CNAME" = {
      "autodiscover"           = "autodiscover.outlook.com"
      "bamboo"                 = "unit1c.metcentral.com"
      "build"                  = "molokini.metcentral.com"
      "cams-cdn"               = "dtxatoodmx36k.cloudfront.net"
      "cdn"                    = "charts.metcentral.com"
      "cdnimages"              = "im.msw.ms"
      "cdnvideo"               = "video.magicseaweed.com"
      "cis"                    = "build.magicseaweed.com"
      "community"              = "kahoolawe.metcentral.com"
      "confluence"             = "jira.magicseaweed.com"
      "csp-report"             = "kauai.metcentral.com"
      "deploy"                 = "mokuhonu.metcentral.com"
      "devnetcam"              = "cam.magicseaweed.com"
      "discover"               = "947659e885fa4397bd27b4e4f89f25a2.unbouncepages.com"
      "display"                = "kahoolawe.metcentral.com"
      "email"                  = "mailgun.org"
      "enterpriseenrollment"   = "enterpriseenrollment.manage.microsoft.com"
      "enterpriseregistration" = "enterpriseregistration.windows.net"
      "hwcharts"               = "charts.magicseaweed.com"
      "hwimages"               = "images.magicseaweed.com"
      "images.ec2-elb"         = "images.elb.metcentral.com"
      "images.ec2"             = "images.elb.metcentral.com"
      "images.elb"             = "images.elb.metcentral.com"
      "lyncdiscover"           = "webdir.online.lync.com"
      "mail"                   = "maui.metcentral.com"
      "msoid"                  = "clientconfig.microsoftonline-p.net"
      "netcam"                 = "cam.magicseaweed.com"
      "netcamimages"           = "netcam.magicseaweed.com"
      "opcharts"               = "charts4.metcentral.com"
      "segment"                = "whitelabel-berry.xid.segment.com"
      "si0"                    = "im.msw.ms"
      "si1"                    = "im.msw.ms"
      "si2"                    = "im.msw.ms"
      "si3"                    = "im.msw.ms"
      "si4"                    = "im.msw.ms"
      "si5"                    = "im.msw.ms"
      "sip"                    = "sipdir.online.lync.com"
      "site-search.sandbox"    = "search-msw-es-site-search-sandbox-c22kumpdh4jequvcupuezpsocq.us-east-1.es.amazonaws.com"
      "site-search"            = "search-msw-es-site-search-msw2-bwsias55yubbacxqz26dzftfeu.us-east-1.es.amazonaws.com"
      "socialcdn"              = "im.msw.ms"
      "stash"                  = "jira.magicseaweed.com"
      "storecdn"               = "storeimages.magicseaweed.com"
      "support"                = "magicseaweed.zendesk.com"
      "surfforecasting"        = "kahoolawe.metcentral.com"

      # domainkey validations:
      "_78d2801635cf602571545acb5ee380b9"           = "_27a5f590c0d7fb7966f02c47016c6079.tljzshvwok.acm-validations.aws"
      "mkxg7tz2cbw6ivveijmcu6ly4f3unsv5._domainkey" = "mkxg7tz2cbw6ivveijmcu6ly4f3unsv5.dkim.amazonses.com"
      "mzl7d5yjkwdba26fia4g7sm45436q6yp._domainkey" = "mzl7d5yjkwdba26fia4g7sm45436q6yp.dkim.amazonses.com"
      "orb55bmqoruz42ha7y7t2wb4pqazf2rv._domainkey" = "orb55bmqoruz42ha7y7t2wb4pqazf2rv.dkim.amazonses.com"
    }
  }

  array_records = {
    "TXT" = {
      "_amazonses"                                      = ["hCVs0mQXExcxOkuIJXNclepnTo48aB6vN7+VzWL429M="]
      "amazonses"                                       = ["hCVs0mQXExcxOkuIJXNclepnTo48aB6vN7+VzWL429M="]
      "cloudflare-verify"                               = ["648448778-455259343"]
      "_dmarc"                                          = ["v=DMARC1;p=none;rua=mailto:abuse@magicseaweed.com"]
      "f9c46e78-d750-11e5-b856-92d67d3a0a75._domainkey" = ["v=DKIM1; k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiA1DA0R0tycwz5/vOe6yjCijpL4ObraRbRvcpX9uafk5eBFPuPER+m6FrfGMk98uw8VbLjtgeVzg/TWerai7siCjBnDjkz+vrJKO/0qPd2xEB2JbQMtJSMoWzSzvxeFdpqEeyHIqHJfzEE8Yr4HcLRMMT/ZsAjeVTT4QsJkZQbwIDAQAB"]
      "mandrill._domainkey"                             = ["v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB"]
      "newsletter._domainkey"                           = ["v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOcV6kUCsyl1pOAvYY7wX/uzL33+HxaJB1L0H11/MCzDBh/4p58K6mnIRFWo/+xh/+Pzj1WjSNnJSIwc4JEn9TfN2+dk3rD4OcB5TDYVueFM9oRLXrW6q8nKWtxBoDX1r/Hh0yc+BpxPYeIqCJLYNuB9MD1cLptClJOLGzf1n1GwIDAQAB"]
      "smtp._domainkey"                                 = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/GypMMJqO8s8aoZgeAWeigvc5Cau7FD/ZGzRXd8EjoJHXm4f5N0rEV1fTVxI3Ug4SMZg1gRFZKuHN/w16wnikOmjWJmfkxIVWxDwdOsN/ql+DY8khQABxJ+lH8i2oJO1mrVTmAyZ/XEND5utHet1/Tg1dOiMcwuuAl4SWicA/5wIDAQAB"]

      "@" = [
        "v=spf1 a mx ip4:213.229.110.204 ip6:2a02:af8:3:1500::8312 ip4:169.62.218.229 ip4:94.76.201.90 include:spf.protection.outlook.com include:mailgun.org include:msgfocus.com ~all",
        "C99502EB7E",
        "atlassian-domain-verification=Y9o507RMsnnZeMli0ROR+ta57xVKt6n-LhhKZQ6R/rmrHM7fwMj/qTzUzb+3tn16",
        "adobe-sign-verification=f70f63876dd0b95fecddc76638aaf807adobe-sign-verification=f70f63876dd0b95fecddc76638aaf807",
        "google-site-verification=5AFBP-OVCsfzZ4-TbA2PVUhIOul8_P0EYhsBPTe1KwQ",
        "ca3-7a1b99d52b5c40be8db1b78521a8189a",
      ]
    }
    "NS" = {
      "cloud" = [
        "ns-1258.awsdns-29.org",
        "ns-123.awsdns-15.com",
        "ns-827.awsdns-39.net",
        "ns-2009.awsdns-59.co.uk",
      ]
    }
  }

  value_records_flat = flatten([
    for type, mappings in local.value_records : [
      for name, value in mappings : {
        type  = type
        name  = name
        value = value
      }
    ]
  ])

  array_records_flat = flatten([
    for type, mappings in local.array_records : [
      for name, array in mappings : [
        for index, value in array : {
          type  = type
          name  = name
          index = index
          value = value
        }
      ]
    ]
  ])

}

resource "cloudflare_record" "value_record" {
  for_each = {
    for rec in local.value_records_flat : "${rec.type}:${rec.name}" => rec
  }
  name    = each.value.name
  value   = each.value.value
  type    = each.value.type
  proxied = false
  ttl     = 1
  zone_id = local.zone_id
}

resource "cloudflare_record" "array_record" {
  for_each = {
    for rec in local.array_records_flat : "${rec.type}:${rec.name}:${rec.index}" => rec
  }
  name    = each.value.name
  value   = each.value.value
  type    = each.value.type
  proxied = false
  ttl     = 1
  zone_id = local.zone_id
}

resource "cloudflare_record" "mail_server" {
  name     = "@"
  priority = 0
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "magicseaweed-com.mail.protection.outlook.com"
  zone_id  = local.zone_id
}

resource "cloudflare_record" "sipfederationtls_service" {
  data = {
    service  = "_sipfederationtls"
    proto    = "_tcp"
    name     = "@"
    priority = 100
    weight   = 1
    port     = 5061
    target   = "sipfed.online.lync.com"
  }
  name     = "_sipfederationtls._tcp"
  priority = 100
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = local.zone_id
}

resource "cloudflare_record" "sip_service" {
  data = {
    service  = "_sip"
    proto    = "_tls"
    name     = "@"
    priority = 100
    weight   = 1
    port     = 443
    target   = "sipdir.online.lync.com"
  }
  name     = "_sip._tls"
  priority = 100
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = local.zone_id
}
