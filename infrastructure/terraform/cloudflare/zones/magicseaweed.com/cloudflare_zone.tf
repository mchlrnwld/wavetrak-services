resource "cloudflare_zone" "magicseaweed" {
  zone   = "magicseaweed.com"
  plan   = "enterprise"
  type   = "full"
  paused = false
}

locals {
  zone_id = resource.cloudflare_zone.magicseaweed.id
}
