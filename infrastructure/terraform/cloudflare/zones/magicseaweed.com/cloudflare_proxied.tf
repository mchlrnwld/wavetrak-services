/*
 * cloudflare_records with proxied = true
 */

locals {
  web_server = "169.62.218.229"
  web_records = [
    "@", # zone apex
    "de",
    "es",
    "fr",
    "ja",
    "pt",
    "www",
    "iphone",
  ]

}

resource "cloudflare_record" "proxied_web" {
  for_each = toset(local.web_records)
  name     = each.key
  value    = local.web_server
  type     = "A"
  proxied  = true
  ttl      = 1
  zone_id  = local.zone_id
}
