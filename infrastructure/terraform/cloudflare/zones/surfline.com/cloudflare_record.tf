locals {
  cname_records = {
    "surfparks.sandbox" = "sl-ecs-web-proxy-int-sandbox-115058441.us-west-1.elb.amazonaws.com"
    "surfparks.staging" = "sl-ecs-web-proxy-int-staging-1666074802.us-west-1.elb.amazonaws.com"
    api                 = "dualstack.sl-ecs-web-proxy-int-prod-245790451.us-west-1.elb.amazonaws.com"
    cloudflare          = "d2r4cc3gdr1to1.cloudfront.net"
    platform            = "sl-ecs-services-proxy-prod-29300544.us-west-1.elb.amazonaws.com"
    services            = "sl-ecs-services-proxy-prod-29300544.us-west-1.elb.amazonaws.com"
    surfparks           = "dualstack.sl-ecs-web-proxy-int-prod-245790451.us-west-1.elb.amazonaws.com"
    tools               = "httpbin.org"
    www                 = "d2r4cc3gdr1to1.cloudfront.net"
  }
}

resource "cloudflare_record" "cname" {
  for_each = local.cname_records
  name     = each.key
  value    = each.value
  type     = "CNAME"
  proxied  = true
  ttl      = 1
  zone_id  = resource.cloudflare_zone.surfline.id
}
