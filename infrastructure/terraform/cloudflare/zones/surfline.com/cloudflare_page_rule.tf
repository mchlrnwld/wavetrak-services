locals {
  web_paths = [
    "www.surfline.com/*",
  ]
  api_paths = [
    "www.surfline.com/basp/basp_xml_generator.cfm",
    "www.surfline.com/surfline/forecasts2/scripts/*.cfm",
    "www.surfline.com/surfline/forecasts2/data/*.cfm",
    "www.surfline.com/reports/*.cfm",
    "www.surfline.com/buoyweather/*.cfm",
    "www.surfline.com/xml/*.cfm",
    "www.surfline.com//wp-json/*",
    "www.surfline.com/wp-json/*",
    "api.surfline.com/*",
    "platform.surfline.com/*",
    "services.surfline.com/*",
  ]
}

resource "cloudflare_page_rule" "web" {
  for_each = { for idx, path in local.web_paths : path => idx }
  target   = each.key
  priority = each.value + 1
  status   = "active"
  zone_id  = resource.cloudflare_zone.surfline.id
  actions {
    browser_check = "on"
  }
}

resource "cloudflare_page_rule" "turn_on_ssl_on_remaining_pages" {
  priority = length(local.web_paths) + 1
  status   = "active"
  target   = "http://*surfline.com/*"
  zone_id  = resource.cloudflare_zone.surfline.id
  actions {
    always_use_https = "true"
  }
}

resource "cloudflare_page_rule" "api" {
  for_each = { for idx, path in local.api_paths : path => idx }
  target   = each.key
  priority = each.value + length(local.web_paths) + 2
  status   = "active"
  zone_id  = resource.cloudflare_zone.surfline.id
  actions {
    browser_check = "off"
  }
}

resource "cloudflare_page_rule" "turn_off_ssl_on_cfm" {
  priority = length(local.web_paths) + length(local.api_paths) + 1
  status   = "active"
  target   = "http://*surfline.com/*.cfm*"
  zone_id  = resource.cloudflare_zone.surfline.id
  actions {
    ssl = "off"
  }
}

resource "cloudflare_page_rule" "turn_off_ssl_on_buoy_report" {
  priority = length(local.web_paths) + length(local.api_paths) + 2
  status   = "active"
  target   = "http://*surfline.com/buoy-report/*"
  zone_id  = resource.cloudflare_zone.surfline.id
  actions {
    ssl = "off"
  }
}
