resource "cloudflare_zone" "surfline" {
  zone   = "surfline.com"
  plan   = "enterprise"
  type   = "partial"
  paused = false
}
