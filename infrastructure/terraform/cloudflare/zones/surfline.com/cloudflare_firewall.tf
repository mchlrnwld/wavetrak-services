locals {
  zone_id = resource.cloudflare_zone.surfline.id

  bot_management = {
    "services" = {
      host = "services.surfline.com"
      patterns = [
        "^/graphql",
        "^/kbyg/spots/reports",
        "^/kbyg/spots/forecasts",
        "^/kbyg/regions/forecasts/conditions",
        "^/kbyg/mapview/spot",
      ]
    },
    "api" = {
      host = "api.surfline.com"
      patterns = [
        "^/v1/forecasts",
        "^/v1/mobile/report/",
      ]
    },
    "www" = {
      host = "www.surfline.com"
      patterns = [
        "^/surfdata/.*\\.cfm$",
        "^/surf-charts/",
        "^/surf-report/",
        "^/surf-forecasts/",
        "^/surf-news/",
        "^/buoy-report/",
        "^/travel",
        "^/api/ooh/",
        "^/create-account",
        "^/sign-in",
      ]
    }
  }

  validate_origin = {
    "services" = {
      host = "services.surfline.com"
      patterns = [
        "^/kbyg/mapview/spot",
        "^/kbyg/regions/forecasts/conditions",
        "^/kbyg/regions/overview",
        "^/kbyg/spots/batch",
        "^/kbyg/spots/forecasts/",
        "^/kbyg/spots/forecasts/conditions",
        "^/kbyg/spots/forecasts/tides",
        "^/kbyg/spots/forecasts/wave",
        "^/kbyg/spots/forecasts/weather",
        "^/kbyg/spots/forecasts/wind",
        "^/kbyg/spots/nearby",
        "^/search/site",
        "^/taxonomy",
      ]
    }
    "api" = {
      host = "api.surfline.com"
      patterns = [
        "^/v1/geoip",
        "^/v1/cams/",
      ]
    }
  }

  good_origins = [
    "surfline\\.com",
    "fishtrack\\.com",
    "buoyweather\\.com",
    "magicseaweed\\.com",
    "localhost:.*",
  ]

  # see here for details: https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/132560024/CF+API+Keys
  user_keys = [
    "0bb3412dfab12ab358c833d9197294ac",
    "1c138bdcdc338726961863e08b431224",
    "1e07b76decb6fafaa83b7c1b011a0afa",
    "1e9a9f819606dd60d013e035e8614cea",
    "4fec025c18b7c5b9a4b73d15b9792211",
    "7ba068007a95e1267691519495d16cfd",
    "8b7dc0f70163ca9a538ce7563f83aa85",
    "8e9c4695393fcdd815a248c129c4269f",
    "8f7b0adcea82584a73d4624d302b00bf",
    "14e0a6b05729ed3d58c2a9ecf7964b67",
    "19b6dbcf2380fe706c227d1290c319c4",
    "38a2b4945bfbb969b01cd30777e1e249",
    "63cd4e2c3f3f29d6809e117c59ee3f90",
    "76ba1c36a915b109741cb7420090a774",
    "308a6714b6176f93a7f47fcbb2ca2e9a",
    "493cc2ad658ebaf3fcb21078be72aabf",
    "857aec44c2a312f4b77db5bb60857e57",
    "885c82f87937aeebbefc9bfb28765763",
    "931ebdd5d7e3e5f92c6f0aa9ed3fd8c0",
    "1907f855aee4977a21a7a625dbf9616f",
    "2041ca25cb3bed0b77e124da993dfa6b",
    "08801c759b8d56152129c3a9a64af28e",
    "9863ec58495955ca79055918803f167a",
    "78806f9a6e04bdd73ab14f8748fc3dd9",
    "503952bf71777d713327c23539c6ead8",
    "1316393f5e12dd226b2a4d0e166a0c75",
    "8966090a497f3186c57bcd3db454375d",
    "a2695dce78e092e69385817cc90927ac",
    "a2784a2cee4b87ec6ee95d78942d494e",
    "abca50124d77d41504c4d53e29c8d359",
    "b1dbe14d0ee7f643e4a21d34a12722ab",
    "b8e1f342e14f480849daf4095336c3a5",
    "ba2a19d9c030ebb63c6f8e72dcd50f8d",
    "ba5ff60fa2e30940f94379d97193a334",
    "bb3160dce56264212bbfd42dfa711b44",
    "c7bb5d1ce8e4f947d15fa5e7dcf65659",
    "c50fe484f33276d0f6dbff2a8ec08dbd",
    "d020d302cf86a28e79153fbba563675b",
    "e3f567c5ac489ddef701b434af76af92",
    "e795e089247e76e0ae7b5a13fa8be24b",
    "ed8617eb004cf4e1cca893b232fa88a1",
    "ff80722fbb0a8be055d4d0b2c7b933df",
    "ffab3ca047e9577cff90037ec61d5887",
  ]

  # ASN numbers for the most common cloud and hosting providers
  hosting_providers = [
    14618,  # Amazon.com Inc.
    16509,  # Amazon.com Inc.
    33083,  # AxcelX Technologies LLC
    36352,  # ColoCrossing
    14061,  # DigitalOcean LLC
    10297,  # eNET Inc.
    30083,  # GoDaddy.com LLC
    398110, # GoDaddy.com LLC
    15169,  # Google LLC
    8972,   # Host Europe GmbH
    30633,  # Leaseweb USA Inc.
    63949,  # Linode LLC
    8075,   # Microsoft Corporation
    16276,  # OVH SAS
    36351,  # SoftLayer Technologies Inc.
    20473,  # The Constant Company LLC
    397423, # Tier.Net Technologies LLC
    53766,  # VMWare Inc.
    55293,  # A2 Hosting, Inc.
    203020, # HostRoyale Technologies Pvt Ltd
    24940,  # Hetzner Online GmbH
    203380, # DA International Group Ltd.
  ]
  vpn_providers = [
    22616,  # ZSCALER INC.
    396319, # CLOUDVPN INC.
  ]
}

###
resource "cloudflare_filter" "bots-www" {
  expression = <<-EOS
    (
      http.host eq "www.surfline.com"
      and ip.geoip.asnum in {
        ${join("\n    ", local.hosting_providers)}
      }
      and cf.bot_management.score le 3
      and not cf.bot_management.verified_bot
      and not cf.bot_management.static_resource
      and not http.request.uri.path matches "^/rss/"
      and not http.request.uri.path matches "^/surf-news/"
      and not http.request.uri.path matches "sitemap.*xml$"
      and not http.user_agent contains "www.admantx.com"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "bots-www" {
  action      = "js_challenge"
  description = "Bots: www"
  filter_id   = cloudflare_filter.bots-www.id
  paused      = false
  priority    = 12000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "bots-api" {
  expression = <<-EOS
    (
      http.host eq "api.surfline.com"
      and ip.geoip.asnum in {
        ${join("\n    ", local.hosting_providers)}
      }
      and cf.bot_management.score le 3
      and not cf.bot_management.verified_bot
      and not cf.bot_management.static_resource
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "bots-api" {
  action      = "block"
  description = "Bots: api"
  filter_id   = cloudflare_filter.bots-api.id
  paused      = false
  priority    = 12000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "bots-services" {
  expression = <<-EOS
    (
      http.host eq "services.surfline.com"
      and ip.geoip.asnum in {
        ${join("\n    ", local.hosting_providers)}
      }
      and cf.bot_management.score le 3
      and not cf.bot_management.verified_bot
      and not cf.bot_management.static_resource
      and not http.user_agent matches "^Mozilla/5.0 "
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "bots-services" {
  action      = "block"
  description = "Bots: services"
  filter_id   = cloudflare_filter.bots-services.id
  paused      = false
  priority    = 12000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "bots-platform" {
  expression = <<-EOS
    (
      http.host eq "platform.surfline.com"
      and ip.geoip.asnum in {
        ${join("\n    ", local.hosting_providers)}
      }
      and cf.bot_management.score le 3
      and not cf.bot_management.verified_bot
      and not cf.bot_management.static_resource
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "bots-platform" {
  action      = "log"
  description = "Bots: platform"
  filter_id   = cloudflare_filter.bots-platform.id
  paused      = false
  priority    = 12000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "bots" {
  for_each   = local.bot_management
  expression = <<-EOS
    (
      not cf.bot_management.verified_bot
      and cf.bot_management.score le 1
      and ip.geoip.asnum in {
        ${join("\n    ", local.hosting_providers)}
      }
      and http.host eq "${each.value.host}"
      and http.request.uri.path matches "${join("|", each.value.patterns)}"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}

resource "cloudflare_firewall_rule" "bots" {
  for_each    = local.bot_management
  action      = "log"
  description = "(old version) Bots: ${each.key}"
  filter_id   = cloudflare_filter.bots[each.key].id
  paused      = false
  priority    = 14000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "ip-source-whitelist" {
  expression = <<-EOS
    (
      ip.src in {
        52.8.248.28
        52.8.40.203
        54.67.96.119
        54.201.229.106
        54.215.225.129
        54.219.140.86
        13.57.98.240
        210.140.121.166
        210.140.121.167
        210.140.120.181
        210.140.120.189
        37.189.92.204
        169.62.218.229
        2a02:af8:2:4100::9980
      }
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}

resource "cloudflare_firewall_rule" "ip-source-whitelist" {
  action      = "allow"
  description = "IP Source: Whitelisted"
  filter_id   = cloudflare_filter.ip-source-whitelist.id
  paused      = false
  priority    = 250
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "allow-user-keys" {
  expression = <<-EOS
    (
      http.host eq "api.surfline.com"
      and http.request.uri.query matches "user_key=(${join("|", local.user_keys)})"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "allow-user-keys" {
  action      = "allow"
  description = "Requests with legacy user keys"
  filter_id   = cloudflare_filter.allow-user-keys.id
  paused      = false
  priority    = 10000
  zone_id     = local.zone_id
}


###
resource "cloudflare_filter" "requests-from-bw5" {
  expression = <<-EOS
    (
      http.host eq "services.surfline.com"
      and http.user_agent eq "Java/1.6.0_45"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "requests-from-bw5" {
  action      = "allow"
  description = "(to be retired) Requests from BW5"
  filter_id   = cloudflare_filter.requests-from-bw5.id
  paused      = false
  priority    = 500
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "amazon-api-gateway" {
  # "i21u5k7cda" is the API ID unique to us in Amazon API Gateway
  expression = <<-EOS
    (
      ip.geoip.asnum eq 16509
      and http.user_agent eq "AmazonAPIGateway_i21u5k7cda"
      and http.request.uri.path eq "/feed/events"
      and http.host eq "services.surfline.com"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "amazon-api-gateway" {
  action      = "allow"
  description = "Amazon API Gateway"
  filter_id   = cloudflare_filter.amazon-api-gateway.id
  paused      = false
  priority    = 500
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "amazon-cloudfront" {
  expression = <<-EOS
    (
      ip.geoip.asnum eq 16509
      and http.user_agent eq "Amazon CloudFront"
      and http.request.uri.path matches "^/cameras/recording/redirect/"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "amazon-cloudfront" {
  action      = "allow"
  description = "Amazon CloudFront"
  filter_id   = cloudflare_filter.amazon-cloudfront.id
  paused      = false
  priority    = 500
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "amazon-simple-notification-service-agent" {
  expression = <<-EOS
    (
      ip.geoip.asnum eq 16509
      and http.user_agent eq "Amazon Simple Notification Service Agent"
      and http.request.uri.path matches "^/sessions/webhooks/|^/notifications/webhooks/|^/science-data-service/sns/"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "amazon-simple-notification-service-agent" {
  action      = "allow"
  description = "Amazon Simple Notification Service Agent"
  filter_id   = cloudflare_filter.amazon-simple-notification-service-agent.id
  paused      = false
  priority    = 500
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "good-bots" {
  expression = "(cf.bot_management.verified_bot)"
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "good-bots" {
  action      = "allow"
  description = "Good Bots"
  filter_id   = cloudflare_filter.good-bots.id
  paused      = false
  priority    = 750
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "upgrade-funnel-new" {
  expression = <<-EOS
    (
      http.request.uri.path eq "/upgrade"
      and not cf.bot_management.verified_bot
      and cf.bot_management.score le 3
      and not any(lower(http.request.headers.values[*])[*] contains "5dba7247-db8d-47a9-b238-ec2a916b2ee8")
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "upgrade-funnel-new" {
  action      = "js_challenge"
  description = "Upgrade Funnel New"
  filter_id   = cloudflare_filter.upgrade-funnel-new.id
  paused      = false
  priority    = 1000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "upgrade-python-requests-challenge" {
  expression = <<-EOS
    (
      http.user_agent contains "python-requests"
      and http.request.uri.path eq "/upgrade"
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "upgrade-python-requests-challenge" {
  action      = "js_challenge"
  description = "Upgrade python-requests Challenge"
  filter_id   = cloudflare_filter.upgrade-python-requests-challenge.id
  paused      = false
  priority    = 4000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "ip-source-known-malicious-users" {
  expression = "(ip.src in {34.226.207.233 3.82.206.18 192.145.116.252 192.145.116.91 73.37.20.242 103.88.184.51})"
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "ip-source-known-malicious-users" {
  action      = "block"
  description = "IP Source: Known Malicious Users"
  filter_id   = cloudflare_filter.ip-source-known-malicious-users.id
  paused      = false
  priority    = 5000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "bad-origin" {
  for_each = local.validate_origin
  # if the origin header is not empty it must match an explicit whitelist
  expression = <<-EOS
    (
      http.host eq "${each.value.host}"
      and http.request.uri.path matches "${join("|", each.value.patterns)}"
      and any(http.request.headers["origin"][*] matches ".")
      and not any(
              http.request.headers["origin"][*] matches "(${join("|", local.good_origins)})\.?$"
      )
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "bad-origin" {
  for_each    = local.validate_origin
  action      = "block"
  description = "Bad Origin: ${each.key}"
  filter_id   = cloudflare_filter.bad-origin[each.key].id
  paused      = false
  priority    = 11000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "beachday-isitabeachday-com" {
  expression = <<-EOS
    (http.referer contains "isitabeachday.com")
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "beachday-isitabeachday-com" {
  action      = "block"
  description = "(to be retired) BeachDay (isitabeachday.com)"
  filter_id   = cloudflare_filter.beachday-isitabeachday-com.id
  paused      = false
  priority    = 16000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "possible-bots-high-risk-networks" {
  expression = <<-EOS
    (
      not cf.bot_management.verified_bot
      and cf.bot_management.score le 30
      and ip.geoip.asnum in {
        ${join("\n    ", local.vpn_providers)}
        ${join("\n    ", local.hosting_providers)}
      }
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "possible-bots-high-risk-networks" {
  action      = "log"
  description = "Possible bots: high risk networks"
  filter_id   = cloudflare_filter.possible-bots-high-risk-networks.id
  paused      = false
  priority    = 16000
  zone_id     = local.zone_id
}

###
resource "cloudflare_filter" "possible-bots-other-networks" {
  expression = <<-EOS
    (
      not cf.bot_management.verified_bot
      and cf.bot_management.score le 30
      and not ip.geoip.asnum in {
        ${join("\n    ", local.vpn_providers)}
        ${join("\n    ", local.hosting_providers)}
      }
    )
  EOS
  paused     = false
  zone_id    = local.zone_id
}
resource "cloudflare_firewall_rule" "possible-bots-other-networks" {
  action      = "log"
  description = "Possible bots: other networks"
  filter_id   = cloudflare_filter.possible-bots-other-networks.id
  paused      = false
  priority    = 16000
  zone_id     = local.zone_id
}
