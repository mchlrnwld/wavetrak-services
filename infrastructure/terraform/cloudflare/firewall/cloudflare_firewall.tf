data "cloudflare_zones" "needs_firewall" {
  filter {
    # todo: a future version should match all zones
    match = "^(buoyweather\\.com|magicseaweed\\.com)$"
  }
}

locals {
  zones_to_protect = { for z in data.cloudflare_zones.needs_firewall.zones : z.name => z.id }

  good_origins = [
    "surfline\\.com",
    "fishtrack\\.com",
    "buoyweather\\.com",
    "magicseaweed\\.com",
    "localhost:.*",
  ]

  # ASN numbers for the most common cloud and hosting providers
  hosting_providers = [
    14618,  # Amazon.com Inc.
    16509,  # Amazon.com Inc.
    33083,  # AxcelX Technologies LLC
    36352,  # ColoCrossing
    14061,  # DigitalOcean LLC
    10297,  # eNET Inc.
    30083,  # GoDaddy.com LLC
    398110, # GoDaddy.com LLC
    15169,  # Google LLC
    8972,   # Host Europe GmbH
    30633,  # Leaseweb USA Inc.
    63949,  # Linode LLC
    8075,   # Microsoft Corporation
    16276,  # OVH SAS
    36351,  # SoftLayer Technologies Inc.
    20473,  # The Constant Company LLC
    397423, # Tier.Net Technologies LLC
    53766,  # VMWare Inc.
    55293,  # A2 Hosting, Inc.
    203020, # HostRoyale Technologies Pvt Ltd
    24940,  # Hetzner Online GmbH
    12876,  # Online SAS
    136907, # HUAWEI CLOUDS
  ]
  vpn_providers = [
    22616,  # ZSCALER INC.
    396319, # CLOUDVPN INC.
  ]
}

resource "cloudflare_filter" "ip-source-whitelist" {
  for_each   = local.zones_to_protect
  expression = "(ip.src in $global_whitelist)"
  paused     = false
  zone_id    = each.value
}

resource "cloudflare_firewall_rule" "ip-source-whitelist" {
  for_each    = local.zones_to_protect
  action      = "allow"
  description = "IP Source: Whitelisted"
  filter_id   = cloudflare_filter.ip-source-whitelist[each.key].id
  paused      = false
  priority    = 250
  zone_id     = each.value
}

resource "cloudflare_filter" "bots-all" {
  for_each   = local.zones_to_protect
  expression = <<-EOS
    (
      ip.geoip.asnum in {
        ${join("\n    ", local.hosting_providers)}
      }
      and cf.bot_management.score le 3
      and not cf.bot_management.static_resource
      and not cf.bot_management.verified_bot
      and not http.user_agent contains "okhttp"
    )
  EOS
  paused     = false
  zone_id    = each.value
}

resource "cloudflare_firewall_rule" "bots-all" {
  for_each    = local.zones_to_protect
  action      = each.key == "magicseaweed.com" ? "log" : "js_challenge"
  description = "Bot mitigation"
  filter_id   = cloudflare_filter.bots-all[each.key].id
  paused      = false
  priority    = 11000
  zone_id     = each.value
}

resource "cloudflare_filter" "bad-origin" {
  for_each = local.zones_to_protect
  # if the origin header is not empty it must match an explicit whitelist
  expression = <<-EOS
    (
      any(http.request.headers["origin"][*] matches ".")
      and not any(http.request.headers["origin"][*] matches "(${join("|", local.good_origins)})\.?$")
      and not http.user_agent contains "okhttp"
    )
  EOS
  paused     = false
  zone_id    = each.value
}

resource "cloudflare_firewall_rule" "bad-origin" {
  for_each    = local.zones_to_protect
  action      = each.key == "magicseaweed.com" ? "log" : "js_challenge"
  description = "Bad Origin"
  filter_id   = cloudflare_filter.bad-origin[each.key].id
  paused      = false
  priority    = 11000
  zone_id     = each.value
}

resource "cloudflare_filter" "possible-bots" {
  for_each   = local.zones_to_protect
  expression = <<-EOS
    (
      not cf.bot_management.verified_bot
      and cf.bot_management.score le 30
      and not http.user_agent contains "okhttp"
    )
  EOS
  paused     = false
  zone_id    = each.value
}

resource "cloudflare_firewall_rule" "possible-bots" {
  for_each    = local.zones_to_protect
  action      = "log"
  description = "Possible bots"
  filter_id   = cloudflare_filter.possible-bots[each.key].id
  paused      = false
  priority    = 16000
  zone_id     = each.value
}
