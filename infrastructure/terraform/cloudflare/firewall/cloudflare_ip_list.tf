resource "cloudflare_ip_list" "global-whitelist" {
  name        = "global_whitelist"
  kind        = "ip"
  description = "Global Whitelist"
  account_id  = "bb3b4bac67a854c871c8493475db3af8"

  item {
    value   = "52.8.248.28"
    comment = "AWS - NAT Gateway - surfline-prod - us-west-1b"
  }

  item {
    value   = "52.8.40.203"
    comment = "AWS - NAT (EC2) - surfline-dev - us-west-1b"
  }

  item {
    value   = "54.67.96.119"
    comment = "AWS - NAT Gateway - surfline-prod - us-west-1c"
  }

  item {
    value   = "54.215.225.129"
    comment = "AWS - NAT (EC2) - surfline-legacy - us-west-1b"
  }

  item {
    value   = "54.219.140.86"
    comment = "AWS - NAT (EC2) - surfline-legacy - us-west-1c"
  }

  item {
    value   = "13.57.98.240"
    comment = "AWS - NAT (EC2) - surfline-dev - us-west-1c"
  }

  item {
    value   = "210.140.121.166"
    comment = "Partner - Namiaru"
  }

  item {
    value   = "210.140.121.167"
    comment = "Partner - Namiaru"
  }

  item {
    value   = "210.140.120.181"
    comment = "Partner - Namiaru"
  }

  item {
    value   = "210.140.120.189"
    comment = "Partner - Namiaru"
  }

  item {
    value   = "37.189.92.204"
    comment = "Internal Developer (Assumed) - Portugal"
  }

  item {
    value   = "169.62.218.229"
    comment = "MSW - Softlayer"
  }

  item {
    value   = "2a02:af8:2:4100::/64"
    comment = "MSW - UK"
  }

}
