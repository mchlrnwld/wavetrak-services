# Cloudflare

These are Terraform configurations to manage firewall rules and zones on Cloudflare's `prod` environment.

## Table of Contents <!-- omit in toc -->

- [Requirements](#requirements)
  - [AWS Profile](#aws-profile)
  - [Cloudflare Token](#cloudflare-token)
    - [Permissions](#permissions)
    - [Account Resources](#account-resources)
    - [Zone Resources](#zone-resources)
- [Development](#development)

## Requirements

### AWS Profile

We need to have the `surfline-prod` AWS profile active.

### Cloudflare Token

Create or update a [Personal API Token](https://dash.cloudflare.com/profile/api-tokens) from your Cloudflare individual account with, at least, the next permissions and resources:

#### Permissions

| Section | Permission | Action |
|--- |--- |--- |
| Account | Account Filter Lists | Edit |
| Account | Account Firewall Access Rules | Edit |
| Zone | Zone Settings | Edit |
| Zone | Page Rules | Edit |
| Zone | Firewall Services | Edit |
| Zone | DNS | Edit |

#### Account Resources

| Action | Account |
|--- |--- |
| Include | All accounts |

#### Zone Resources

| Action | Account |
|--- |--- |
| Include | All zones |

Then store this token locally in a way you can export it as `CLOUDFLARE_API_TOKEN` environment variable:

## Development

The way to plan a apply possible changes is using Terraform commands directly.

```shell
export CLOUDFLARE_API_TOKEN=******
assume-role "surfline-prod"
terraform init
terraform plan
terraform apply
```
