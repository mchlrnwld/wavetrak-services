data "cloudflare_zones" "needs_logpush" {
  filter {
    // match all zones
  }
}

locals {
  zones_to_log = { for z in data.cloudflare_zones.needs_logpush.zones : z.name => z.id }

  logs_bucket = "wt-cloudflare-logs-prod"

  log_fields = [
    "CacheCacheStatus",
    "CacheResponseBytes",
    "CacheResponseStatus",
    "CacheTieredFill",
    "ClientIP",
    "ClientRequestBytes",
    "ClientRequestHost",
    "ClientRequestMethod",
    "ClientRequestPath",
    "ClientRequestProtocol",
    "ClientRequestReferer",
    "ClientRequestURI",
    "ClientRequestUserAgent",
    "EdgeColoCode",
    "EdgeColoID",
    "EdgeEndTimestamp",
    "EdgeResponseBytes",
    "EdgeResponseContentType",
    "EdgeResponseStatus",
    "EdgeStartTimestamp",
    "FirewallMatchesActions",
    "FirewallMatchesRuleIDs",
    "FirewallMatchesSources",
    "OriginIP",
    "OriginResponseBytes",
    "OriginResponseHTTPExpires",
    "OriginResponseHTTPLastModified",
    "OriginResponseStatus",
    "OriginResponseTime",
    "OriginSSLProtocol",
    "RayID",
    "WAFAction",
    "WAFFlags",
    "WAFMatchedVar",
    "WAFProfile",
    "WAFRuleID",
    "WAFRuleMessage",
    "WorkerCPUTime",
    "WorkerStatus",
    "WorkerSubrequest",
    "WorkerSubrequestCount",
  ]
}

resource "cloudflare_logpush_ownership_challenge" "s3_ownership" {
  for_each         = local.zones_to_log
  destination_conf = "s3://${local.logs_bucket}/logs/${each.key}/dt={DATE}?region=us-west-1"
  zone_id          = each.value
}

data "aws_s3_bucket_object" "s3_challenge_file" {
  for_each = local.zones_to_log
  bucket   = local.logs_bucket
  key      = cloudflare_logpush_ownership_challenge.s3_ownership[each.key].ownership_challenge_filename
}

resource "cloudflare_logpush_job" "s3_logpush" {
  for_each            = local.zones_to_log
  dataset             = "http_requests"
  destination_conf    = "s3://${local.logs_bucket}/logs/${each.key}/dt={DATE}?region=us-west-1"
  enabled             = true
  logpull_options     = "fields=${join(",", local.log_fields)}&timestamps=rfc3339"
  ownership_challenge = data.aws_s3_bucket_object.s3_challenge_file[each.key].body
  zone_id             = each.value
}
