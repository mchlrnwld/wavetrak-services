provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wt-live-weather/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "wt_live_weather" {
  source = "../../"

  environment = "prod"
}
