provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-search-analytics/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "analytics-firehose" {
  source = "../../"

  environment         = "prod"
  producer_role_name  = "search_service_task_role_prod"
  analytics_retention = "180"
}
