provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-search-analytics/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "analytics-firehose" {
  source = "../../"

  environment         = "sbox"
  producer_role_name  = "search_service_task_role_sandbox"
  analytics_retention = "30"
}
