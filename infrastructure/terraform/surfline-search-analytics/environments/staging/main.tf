provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-search-analytics/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "analytics-firehose" {
  source = "../../"

  environment         = "staging"
  producer_role_name  = "search_service_task_role_staging"
  analytics_retention = "30"
}
