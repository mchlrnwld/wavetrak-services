resource "aws_s3_bucket" "bucket" {
  bucket = "sl-search-analytics-${var.environment}"
  acl    = "private"

  lifecycle_rule {
    prefix  = "events/"
    enabled = true

    expiration {
      days = var.analytics_retention
    }
  }

  tags = {
    Company     = "sl"
    Service     = "s3"
    Application = "search-analytics"
    Environment = var.environment
    Terraform   = "true"
  }
}

resource "aws_iam_role" "firehose_role" {
  name               = "sl-search-analytics-firehose-delivery-policy-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/firehose-delivery-policy.json", {})
}

resource "aws_iam_policy" "firehose_write_to_s3_policy" {
  name = "sl-search-analytics-firehose-dest-write-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/s3-readwrite-policy.json", {
    bucket_arn = aws_s3_bucket.bucket.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_write_policy" {
  role       = aws_iam_role.firehose_role.name
  policy_arn = aws_iam_policy.firehose_write_to_s3_policy.arn
}

resource "aws_iam_policy" "firehose_producer_policy" {
  name = "sl-search-analytics-firehose-producer-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/firehose-producer-policy.json", {
    firehose_stream_arn = aws_kinesis_firehose_delivery_stream.analytics_stream.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_producer_policy" {
  role       = var.producer_role_name
  policy_arn = aws_iam_policy.firehose_producer_policy.arn
}

resource "aws_kinesis_firehose_delivery_stream" "analytics_stream" {
  name        = "sl-search-analytics-${var.environment}"
  destination = "s3"

  s3_configuration {
    role_arn           = aws_iam_role.firehose_role.arn
    bucket_arn         = aws_s3_bucket.bucket.arn
    compression_format = "GZIP"
    buffer_size        = "3"
    prefix             = "events/"
  }
}

resource "aws_iam_role" "glue_role" {
  name               = "sl-search-analytics-glue-etl-policy-${var.environment}"
  assume_role_policy = templatefile("${path.module}/resources/glue-etl-policy.json", {})
}

resource "aws_iam_policy" "glue_read_policy" {
  name = "sl-search-analytics-glue-source-read-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/s3-readwrite-policy.json", {
    bucket_arn = aws_s3_bucket.bucket.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_glue_policy" {
  role       = aws_iam_role.glue_role.name
  policy_arn = aws_iam_policy.glue_read_policy.arn
}

resource "aws_iam_role_policy_attachment" "attach_glue_managed_policy" {
  role       = aws_iam_role.glue_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
}
