provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "s3-events/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "s3_events_with_sns" {
  source = "../../"

  science_s3_bucket      = "surfline-science-s3-dev"
  live_weather_s3_bucket = "wt-live-weather-sandbox"
  surf_park_s3_bucket    = "sl-surf-park-sessions-clips-holding-pen-sandbox"
}
