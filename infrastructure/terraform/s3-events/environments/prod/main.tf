provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "s3-events/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "s3_events" {
  source = "../../"

  science_s3_bucket        = "surfline-science-s3-prod"
  cam_thumbnails_s3_bucket = "sl-cam-thumbnails-prod"
  live_weather_s3_bucket   = "wt-live-weather-prod"
  surf_park_s3_bucket      = "sl-surf-park-sessions-clips-holding-pen-prod"
}
