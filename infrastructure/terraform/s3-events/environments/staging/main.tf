provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "s3-events/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "s3_events" {
  source = "../../"

  cam_thumbnails_s3_bucket = "sl-cam-thumbnails-staging"
  surf_park_s3_bucket      = "sl-surf-park-sessions-clips-holding-pen-staging"
}
