variable "cam_thumbnails_s3_bucket" {
  description = "The S3 bucket that the notification events will be linked to"
  default     = ""
}

variable "cam_thumbnails_full_event_name" {
  description = "The name of the cam stills _full.jpg S3 event that will be used to create the name of the sns topic"
  default     = "full-upload"
}

variable "cam_thumbnails_full_suffix" {
  description = "The object key suffix in S3 that will trigger the SNS:Publish action"
  default     = "_full.jpg"
}

variable "science_s3_bucket" {
  description = "The S3 bucket that the notification events will be linked to"
  default     = ""
}

variable "jobs_event_name" {
  description = "The name of the jobs S3 event that will be used to create the name of the sns topic"
  default     = "jobs-upload"
}

variable "gfs_event_name" {
  description = "The name of the gfs S3 event that will be used to create the name of the sns topic"
  default     = "gfs-upload"
}

variable "proteus_ww3_fields_event_name" {
  description = "The name of the proteus_ww3_fields S3 event that will be used to create the name of the sns topic"
  default     = "proteus-ww3-fields-upload"
}

variable "lotus_event_name" {
  description = "The name of the lotus S3 event that will be used to create the name of the sns topic"
  default     = "lotus-upload"
}

variable "nasa_mur_sst_event_name" {
  description = "The name of the NASA MUR SST S3 event that will be used to create the name of the sns topic"
  default     = "nasa-mur-sst-upload"
}

variable "noaa_gefs_wave_event_name" {
  description = "The name of the GEFS-Wave S3 event that will be used to create the name of the sns topic"
  default     = "noaa-gefs-wave-upload"
}

variable "noaa_ww3_event_name" {
  description = "The name of the WW3 S3 event that will be used to create the name of the sns topic"
  default     = "noaa-ww3-upload"
}

variable "noaa_nam_event_name" {
  description = "The name of the NOAA NAM S3 event that will be used to create the name of the sns topic"
  default     = "noaa-nam-upload"
}

variable "events" {
  description = "The types of S3 events that will trigger the SNS:Publish action"
  default     = ["s3:ObjectCreated:*"]
}

variable "jobs_prefix" {
  description = "The object key prefix for jobs in S3 that will trigger the SNS:Publish action"
  default     = "jobs/"
}

variable "proteus_ww3_field_prefix" {
  description = "The object key prefix for proteus_ww3_fields in S3 that will trigger the SNS:Publish action"
  default     = "proteus/ww3/fields/"
}

variable "lotus_prefix" {
  description = "The object key prefix for lotus in S3 that will trigger the SNS:Publish action"
  default     = "lotus/"
}

variable "nasa_mur_sst_prefix" {
  description = "The object key prefix for NASA MUR SST in S3 that will trigger the SNS:Publish action"
  default     = "nasa/mur-sst/"
}

variable "gfs_prefix" {
  description = "The object key prefix for gfs in S3 that will trigger the SNS:Publish action"
  default     = "gfs/"
}

variable "noaa_gefs_wave_prefix" {
  description = "The object key prefix for NOAA WW3-Ensemble in S3 that will trigger the SNS:Publish action"
  default     = "noaa/gefs-wave/"
}

variable "noaa_ww3_prefix" {
  description = "The object key prefix for NOAA WW3 in S3 that will trigger the SNS:Publish action"
  default     = "noaa/ww3/"
}

variable "noaa_nam_prefix" {
  description = "The object key prefix for NOAA NAM in S3 that will trigger the SNS:Publish action"
  default     = "noaa/nam/"
}

variable "live_weather_s3_bucket" {
  description = "The S3 bucket that the notification events will be linked to"
  default     = ""
}

variable "live_weather_event_name" {
  description = "The name of the Live Weather S3 event that will be used to create the name of the sns topic"
  default     = "weatherlink-upload"
}

variable "live_weather_weatherlink_prefix" {
  description = "The object key prefix for WeatherLink data in S3 that will trigger the SNS:Publish action"
  default     = "weatherlink/data/"
}

variable "surf_park_s3_bucket" {
  description = "The S3 bucket that the notification events will be linked to"
  default     = ""
}

variable "surf_park_sessions_suffix" {
  description = "The object key suffix for surfpark sessions data in S3 that will trigger the SNS:Publish action"
  default     = ".json"
}

variable "surf_park_session_clip_event_name" {
  description = "The name of the surfparks S3 event that will be used to create the name of the sns topic"
  default     = "surf-park-clip-upload"
}

variable "wavetrak_lotus_ww3_event_name" {
  description = "The name of the Wavetrak Lotus WW3 S3 event that will be used to create the name of the sns topic"
  default     = "wavetrak-lotus-ww3-upload"
}

variable "wavetrak_lotus_ww3_prefix" {
  description = "The object key prefix for Wavetrak Lotus WW3 data in S3 that will trigger the SNS:Publish action"
  default     = "wavetrak/lotus-ww3/"
}
