data "aws_s3_bucket" "s3_bucket" {
  count  = var.resource_count
  bucket = var.s3_bucket
}

resource "aws_sns_topic" "s3_event" {
  count = var.resource_count
  name  = "${var.s3_bucket}-${var.event_name}-event"
  policy = templatefile("${path.module}/resources/s3-event-policy.json", {
    s3_bucket_arn  = data.aws_s3_bucket.s3_bucket[0].arn
    s3_bucket_name = var.s3_bucket
    event_name     = var.event_name
  })
}
