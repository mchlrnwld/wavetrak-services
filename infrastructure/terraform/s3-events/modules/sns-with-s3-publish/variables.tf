variable "s3_bucket" {
  description = "The S3 bucket that you want to grant SNS:Publish permissions to"
  default     = ""
}

variable "resource_count" {
  description = "The number of resources to create"
  default     = 1
}

variable "event_name" {
  description = "The name of the s3 event that will be used to create the name of the sns topic"
}
