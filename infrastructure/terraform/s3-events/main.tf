locals {
  science_resource_count        = var.science_s3_bucket != "" ? 1 : 0
  cam_thumbnails_resource_count = var.cam_thumbnails_s3_bucket != "" ? 1 : 0
}

module "s3_jobs_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.jobs_event_name
}

module "s3_proteus_ww3_fields_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.proteus_ww3_fields_event_name
}

module "s3_lotus_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.lotus_event_name
}

module "s3_gfs_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.gfs_event_name
}

module "s3_noaa_gefs_wave_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.noaa_gefs_wave_event_name
}

module "s3_noaa_ww3_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.noaa_ww3_event_name
}

module "s3_nasa_mur_sst_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.nasa_mur_sst_event_name
}

module "s3_noaa_nam_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.noaa_nam_event_name
}

module "s3_cam_thumbnails_full_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.cam_thumbnails_resource_count
  s3_bucket      = var.cam_thumbnails_s3_bucket
  event_name     = var.cam_thumbnails_full_event_name
}

module "s3_live_weather_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = var.live_weather_s3_bucket != "" ? 1 : 0
  s3_bucket      = var.live_weather_s3_bucket
  event_name     = var.live_weather_event_name
}

module "s3_surf_park_session_clip_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = var.surf_park_s3_bucket != "" ? 1 : 0
  s3_bucket      = var.surf_park_s3_bucket
  event_name     = var.surf_park_session_clip_event_name
}

module "s3_wavetrak_lotus_ww3_event" {
  source = "./modules/sns-with-s3-publish"

  resource_count = local.science_resource_count
  s3_bucket      = var.science_s3_bucket
  event_name     = var.wavetrak_lotus_ww3_event_name
}

resource "aws_s3_bucket_notification" "s3_thumbnails_event" {
  bucket = var.cam_thumbnails_s3_bucket
  count  = local.cam_thumbnails_resource_count

  topic {
    topic_arn     = module.s3_cam_thumbnails_full_event.sns_topic_arn
    events        = var.events
    filter_suffix = var.cam_thumbnails_full_suffix
  }
}

resource "aws_s3_bucket_notification" "s3_science_event" {
  bucket = var.science_s3_bucket
  count  = local.science_resource_count

  topic {
    topic_arn     = module.s3_jobs_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.jobs_prefix
  }

  topic {
    topic_arn     = module.s3_proteus_ww3_fields_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.proteus_ww3_field_prefix
  }

  topic {
    topic_arn     = module.s3_gfs_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.gfs_prefix
  }

  topic {
    topic_arn     = module.s3_lotus_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.lotus_prefix
  }

  topic {
    topic_arn     = module.s3_noaa_gefs_wave_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.noaa_gefs_wave_prefix
  }

  topic {
    topic_arn     = module.s3_noaa_ww3_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.noaa_ww3_prefix
  }

  topic {
    topic_arn     = module.s3_nasa_mur_sst_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.nasa_mur_sst_prefix
  }

  topic {
    topic_arn     = module.s3_noaa_nam_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.noaa_nam_prefix
  }

  topic {
    topic_arn     = module.s3_wavetrak_lotus_ww3_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.wavetrak_lotus_ww3_prefix
  }
}

resource "aws_s3_bucket_notification" "s3_live_weather_event" {
  bucket = var.live_weather_s3_bucket
  count  = var.live_weather_s3_bucket != "" ? 1 : 0

  topic {
    topic_arn     = module.s3_live_weather_event.sns_topic_arn
    events        = var.events
    filter_prefix = var.live_weather_weatherlink_prefix
  }
}

resource "aws_s3_bucket_notification" "s3_surf_park_session_clip_event" {
  bucket = var.surf_park_s3_bucket
  count  = var.surf_park_s3_bucket != "" ? 1 : 0

  topic {
    topic_arn     = module.s3_surf_park_session_clip_event.sns_topic_arn
    events        = var.events
    filter_suffix = var.surf_park_sessions_suffix
  }
}
