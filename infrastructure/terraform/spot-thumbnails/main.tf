provider "aws" {
  region = "us-west-1"
}

# S3
resource "aws_s3_bucket" "spot-thumbnails" {
  bucket = "${var.company}-spot-thumbnails-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 86400
  }

  policy = templatefile("${path.module}/resources/s3-read-policy.json", {
    s3_bucket = "arn:aws:s3:::${var.company}-spot-thumbnails-${var.environment}"
  })
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.spot-thumbnails.id
  policy = templatefile("${path.module}/resources/s3-read-policy.json", {
    s3_bucket = aws_s3_bucket.spot-thumbnails.arn
  })
}

# cloudfront
resource "aws_cloudfront_distribution" "spot_thumbnails_s3_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.spot-thumbnails.bucket}.s3.amazonaws.com"
    origin_id   = "S3-spotThumbnailOrigin"
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = "spot thumbnails cdn"

  aliases = [var.spot_thumbnail_cdn_domain]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-spotThumbnailOrigin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 900   # 15 minutes
    default_ttl            = 900   # 15 minutes
    max_ttl                = 86400 # 1 day
  }

  price_class = var.price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = var.environment
  }

  viewer_certificate {
    acm_certificate_arn      = var.spot_thumbnails_acm
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}

resource "aws_route53_record" "spot_thumbnail_domain" {
  zone_id = var.alias_hosted_zone_id
  name    = var.spot_thumbnail_cdn_domain
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.spot_thumbnails_s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.spot_thumbnails_s3_distribution.hosted_zone_id
    evaluate_target_health = false # cannot set to true for cloudfront
  }
}

# permissions
resource "aws_iam_role_policy_attachment" "image-service-role" {
  role       = var.image_service_role_name
  policy_arn = aws_iam_policy.bucket-access.arn
}

resource "aws_iam_policy" "bucket-access" {
  name        = "spot-thumbnail-policy-${var.environment}"
  description = "access to spot thumbnail bucket"
  policy = templatefile("${path.module}/resources/s3-write-policy.json", {
    s3_bucket = aws_s3_bucket.spot-thumbnails.arn
  })
}
