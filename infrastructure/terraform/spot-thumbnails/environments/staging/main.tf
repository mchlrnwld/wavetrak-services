provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "spot-thumbnails/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "spot-thumbnails" {
  source = "../../"

  application = "spot-thumbnails"
  company     = "sl"
  environment = "staging"

  image_service_role_name   = "sl-lambda-image-service-staging"
  spot_thumbnails_acm       = "arn:aws:acm:us-east-1:665294954271:certificate/07c4d79f-1151-42ed-bb62-5ea7c0f93208"
  spot_thumbnail_cdn_domain = "spot-thumbnails.staging.surfline.com"
  alias_hosted_zone_id      = "Z3JHKQ8ELQG5UE"
  price_class               = "PriceClass_All"
}
