provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "spot-thumbnails/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "spot-thumbnails" {
  source = "../../"

  application = "spot-thumbnails"
  company     = "sl"
  environment = "sandbox"

  image_service_role_name   = "sl-lambda-image-service-sandbox"
  spot_thumbnails_acm       = "arn:aws:acm:us-east-1:665294954271:certificate/a48acac9-c364-4186-b33a-73543e5f72c0"
  spot_thumbnail_cdn_domain = "spot-thumbnails.sandbox.surfline.com"
  alias_hosted_zone_id      = "Z3DM6R3JR1RYXV"
  price_class               = "PriceClass_100"
}
