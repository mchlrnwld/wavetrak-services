provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "spot-thumbnails/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "spot-thumbnails" {
  source = "../../"

  application = "spot-thumbnails"
  company     = "sl"
  environment = "prod"

  image_service_role_name   = "sl-lambda-image-service-prod"
  spot_thumbnails_acm       = "arn:aws:acm:us-east-1:833713747344:certificate/522318ff-4e5b-4642-b217-a932bc4df082"
  spot_thumbnail_cdn_domain = "spot-thumbnails.cdn-surfline.com"
  alias_hosted_zone_id      = "Z1OCTVEV5XMNUZ"
  price_class               = "PriceClass_All"
}
