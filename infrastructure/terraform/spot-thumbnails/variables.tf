variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "image_service_role_name" {
}

variable "spot_thumbnails_acm" {
}

variable "spot_thumbnail_cdn_domain" {
}

variable "alias_hosted_zone_id" {
}

variable "price_class" {
}
