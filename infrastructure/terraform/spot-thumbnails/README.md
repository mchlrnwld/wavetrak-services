# Spot Thumbnail manager

## Usage

* `export ENV=staging` Configures your target environment choice
* `make plan` Performs a dry run so you can verify changes
* `make apply` Applies your changes

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)

```bash
make plan
make apply
```
