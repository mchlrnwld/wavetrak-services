variable "company" {
  type        = string
  description = "The brand that this service will be used for. Wavetrak can be used for cross-brand services"
  default     = "wt"
}

variable "application" {
  type        = string
  description = "Name of the service"
  default     = "cloudflare-logs"
}

variable "environment" {
  type        = string
  description = "The environment in which the infrastructure is being created"
}
