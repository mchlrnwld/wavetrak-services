provider "aws" {
  region = "us-west-1"
}

resource "aws_s3_bucket" "cloudflare_logs_bucket" {
  bucket = "${var.company}-${var.application}-${var.environment}"
  acl    = "private"

  versioning {
    enabled = false
  }

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Terraform   = "true"
    Source      = "github://wavetrak-infrastructure/terraform/wt-cloudflare-logs"
  }
}

data "template_file" "policy" {
  template = templatefile("${path.module}/resources/s3-bucket-policy.tpl", {})
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.cloudflare_logs_bucket.id
  policy = data.template_file.policy.rendered
}
