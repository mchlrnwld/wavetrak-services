terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wt-cloudflare-logs/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cloudflare-logs" {
  source = "../../"

  environment = "prod"
}
