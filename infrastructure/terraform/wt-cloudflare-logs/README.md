# Cloudflare Logs

This Terraform project defines an S3 bucket to store Cloudflare logs and sets permissions to allow Cloudflare to write its logs to the bucket.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.20`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=prod make plan
ENV=prod make apply
```

The only valid environment is `prod`.

#### State Management

State is uploaded to S3 after `make apply` is run.
