resource "aws_s3_bucket" "wavetrak_artifacts" {
  bucket = "wt-artifacts-${var.environment}"

  versioning {
    enabled = true
  }
}
