provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "wt-artifacts/dev/terraform.tfstate"
    region = "us-west-1"
  }
}

module "wt_artifacts" {
  source      = "../../"
  environment = "dev"
}
