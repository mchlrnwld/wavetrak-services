provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wt-artifacts/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "wt_artifacts" {
  source      = "../../"
  environment = "prod"
}
