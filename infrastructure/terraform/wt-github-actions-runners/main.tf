locals {
  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Terraform   = true
    Source      = "wavetrak-infrastructure/terraform/wt-github-actions-runners"
  }
}

data "aws_ami" "runner" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"] # Canonical's Amazon Account ID
}

data "aws_ssm_parameter" "github_access_token" {
  name = "/${var.environment}/infrastructure/github-actions-runners/GITHUB_ACCESS_TOKEN"
}

data "template_file" "install_actions_runner" {
  template = file("${path.module}/resources/scripts/install-actions-runner.sh")
  vars = {
    github_access_token = data.aws_ssm_parameter.github_access_token.value
  }
}

data "template_file" "configure_actions_runner" {
  template = file("${path.module}/resources/scripts/configure-actions-runner.exp")
}

data "template_file" "rename_instance" {
  template = file("${path.module}/resources/scripts/rename-instance.py")
  vars = {
    application = var.application
    company     = var.company
    environment = var.environment
  }
}

data "template_file" "cloudwatch_agent" {
  template = file("${path.module}/resources/conf/amazon-cloudwatch-agent.json")
}

data "aws_ssm_parameter" "newrelic_license_key" {
  name = "/${var.environment}/common/NEW_RELIC_LICENSE_KEY"
}

data "template_file" "newrelic_infra" {
  template = file("${path.module}/resources/conf/newrelic-infra.yml")
  vars = {
    newrelic_license_key = data.aws_ssm_parameter.newrelic_license_key.value
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/resources/user-data/user-data.yml")
  vars = {
    cloudwatch_agent_source         = data.template_file.cloudwatch_agent.rendered
    configure_actions_runner_source = data.template_file.configure_actions_runner.rendered
    install_actions_runner_source   = data.template_file.install_actions_runner.rendered
    newrelic_infra_source           = data.template_file.newrelic_infra.rendered
    rename_instance_source          = data.template_file.rename_instance.rendered
  }
}

resource "aws_security_group" "client_sg_group" {
  name        = "${var.company}-${var.application}-multiclient"
  description = "Client security group for database and upstream access by tests"
  vpc_id      = var.default_vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}

resource "aws_iam_role" "runner_role" {
  name               = "${var.company}-${var.application}"
  assume_role_policy = file("${path.module}/resources/policies/ec2-assume-role-policy.json")
  tags               = local.tags
}

data "template_file" "ec2_instance_role_policy" {
  template = file("${path.module}/resources/policies/ec2-instance-role-policy.json")
}

resource "aws_iam_role_policy" "ec2_instance_role_policy" {
  name   = "${var.company}-${var.application}"
  policy = data.template_file.ec2_instance_role_policy.rendered
  role   = aws_iam_role.runner_role.id
}

resource "aws_iam_instance_profile" "runner" {
  name = "${var.company}-${var.application}"
  path = "/"
  role = aws_iam_role.runner_role.name
  tags = local.tags
}

resource "aws_launch_template" "runner" {
  name          = "${var.company}-${var.application}"
  image_id      = data.aws_ami.runner.id
  instance_type = var.instance_type
  key_name      = var.key_name
  vpc_security_group_ids = concat(
    [aws_security_group.client_sg_group.id],
    [var.internal_sg_group],
    [var.internal_sg_airflow]
  )
  user_data              = base64encode(data.template_file.user_data.rendered)
  update_default_version = true

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size           = var.root_volume_size
      volume_type           = "gp3"
      delete_on_termination = "true"
      encrypted             = "false"
    }
  }

  iam_instance_profile {
    name = aws_iam_instance_profile.runner.name
  }

  tags = local.tags
}

resource "aws_autoscaling_group" "runner_cluster" {
  name                = "${var.company}-${var.application}"
  min_size            = var.min_instance_count
  max_size            = var.max_instance_count
  health_check_type   = "EC2"
  vpc_zone_identifier = var.instance_subnets
  enabled_metrics     = var.enabled_metrics

  launch_template {
    id      = aws_launch_template.runner.id
    version = "$Latest"
  }

  tag {
    key                 = "Name" # This names the launched instances
    value               = "${var.company}-${var.application}"
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = local.tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}
