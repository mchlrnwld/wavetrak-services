#!/bin/bash

set -euxo pipefail

target_user="ubuntu"
if [ "$(whoami)" != "$${target_user}" ]; then
  exec sudo -u "$${target_user}" -- "$0" "$@"
fi

workdir=/home/ubuntu/actions-runner
mkdir -p "$${workdir}"
cd "$${workdir}"

curl \
  -o actions-runner-linux-x64-2.281.1.tar.gz \
  -L https://github.com/actions/runner/releases/download/v2.281.1/actions-runner-linux-x64-2.281.1.tar.gz

tar xzf ./actions-runner-linux-x64-2.281.1.tar.gz

registration_token="$(curl -s -XPOST \
  -H "authorization: token ${github_access_token}" \
  https://api.github.com/repos/Surfline/wavetrak-services/actions/runners/registration-token |\
  jq -r .token)"

expect -f ./configure-actions-runner.exp "$${registration_token}" "$(hostname)"

sudo ./svc.sh install
sudo ./svc.sh start
