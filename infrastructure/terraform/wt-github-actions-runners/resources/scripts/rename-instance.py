#!/usr/bin/env python3

import json
import subprocess

import boto3  # type: ignore
import requests

print("[INFO] Getting instance identity")
metadata_url = (
    "http://169.254.169.254/latest/dynamic/instance-identity/document"
)
inst_data = requests.get(metadata_url).json()

print("[INFO] Getting tags for current instance")
ec2 = boto3.resource("ec2", region_name=inst_data["region"])
inst = ec2.Instance(inst_data["instanceId"])

tags = {tag["Key"]: tag["Value"] for tag in inst.tags}

inst_name = tags.get("Name", "${company}-${application}")

print("[INFO] Generating instance name with instance hash")
short_id_hash = inst_data["instanceId"][2:10]
name = f"{inst_name}-{short_id_hash}"

# Ensure the generated name with hash doesn't exceed 63 chars
short_name = name[:63]

print(f"[INFO] Renaming ECS instance to {short_name}")
inst.create_tags(Tags=[{"Key": "Name", "Value": short_name}])

print(f"[INFO] Renaming locally to {name}")
poutput = subprocess.check_output(
    f"hostnamectl set-hostname {name}", shell=True
)
