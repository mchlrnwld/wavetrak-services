variable "company" {
  type        = string
  description = "The brand that this service will be used for. Wavetrak can be used for cross-brand services"
  default     = "wt"
}

variable "application" {
  type        = string
  description = "Name of the service"
  default     = "github-actions-runners"
}

variable "environment" {
  type        = string
  description = "The environment in which the infrastructure is being created"
}

variable "instance_type" {
  default = ""
}

variable "root_volume_size" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "vpc_security_group_ids" {
  type    = list(string)
  default = []
}

variable "default_vpc" {
  type        = string
  description = "The default VPC for the service"
}

variable "iam_instance_profile" {
  default = ""
}

variable "internal_sg_group" {
  default = ""
}

variable "internal_sg_airflow" {
  default = ""
}

variable "availability_zones" {
}

variable "min_instance_count" {
}

variable "max_instance_count" {
}

variable "instance_subnets" {
}

variable "enabled_metrics" {
}
