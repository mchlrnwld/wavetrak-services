provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "wt-github-actions-runners/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "github-actions-runners" {
  source = "../../"

  environment         = "sl-dev-us-west-1"
  instance_type       = "m5.large"
  root_volume_size    = 55
  key_name            = "sturdy-surfline-dev"
  default_vpc         = "vpc-981887fd"
  internal_sg_group   = "sg-90aeaef5"
  internal_sg_airflow = "sg-2aafbf4e"
  availability_zones = [
    "us-west-1b",
    "us-west-1c",
  ]
  min_instance_count = 3
  max_instance_count = 3
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab",
  ]
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances"
  ]
}
