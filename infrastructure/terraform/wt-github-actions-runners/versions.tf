terraform {
  required_version = ">= 0.12"
  required_providers {
    aws      = "~> 3.57.0"
    template = "~> 2.1"
    newrelic = {
      source  = "newrelic/newrelic"
      version = "~> 2.25.0"
    }
  }
}
