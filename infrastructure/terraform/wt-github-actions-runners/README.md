# GitHub Actions Self-Hosted Runners Infrastructure

This application sets up [GitHub Actions Self-Hosted Runners](https://docs.github.com/en/actions/hosting-your-own-runners) for use in CI/CD workflows.

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Naming Conventions](#naming-conventions)
- [Terraform](#terraform)
  - [Using Terraform](#using-terraform)
    - [State Management](#state-management)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `1.0.6`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sandbox make plan
ENV=sandbox make apply
```

Valid environments are `sandbox`, `staging`, and `prod`.

#### State Management

State is uploaded to S3 after `make apply` is run.
