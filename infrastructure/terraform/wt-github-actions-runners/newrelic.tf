locals {
  newrelic_application     = "GitHub Actions Self-Hosted Runners"
  newrelic_hostname_prefix = "${var.company}-${var.application}-"
}

data "aws_ssm_parameter" "github_actions_integration_key" {
  name = "/${var.environment}/newrelic/github-actions-runners/integration-key"
}

resource "newrelic_alert_channel" "github_actions_channel" {
  name = local.newrelic_application
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.github_actions_integration_key.value
  }
}

resource "newrelic_alert_policy" "github_actions_alert_policy" {
  name                = local.newrelic_application
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_alert_policy_channel" "github_actions_policy_channel" {
  policy_id   = newrelic_alert_policy.github_actions_alert_policy.id
  channel_ids = [newrelic_alert_channel.github_actions_channel.id]
}

# GitHub Actions runner process not running
resource "newrelic_infra_alert_condition" "github_actions_runner_not_running_alert" {
  policy_id = newrelic_alert_policy.github_actions_alert_policy.id

  name          = "${local.newrelic_application} - Process Not Running"
  type          = "infra_process_running"
  comparison    = "equal"
  where         = "(hostname LIKE '${local.newrelic_hostname_prefix}%')"
  process_where = "processDisplayName = 'Runner.Listener'"

  critical {
    duration = 5
    value    = 0
  }
}
