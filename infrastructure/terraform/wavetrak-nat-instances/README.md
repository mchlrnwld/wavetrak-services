# surfline-nat-instances

This project provisions AWS NAT instances for the Surfline **dev** account.

It creates an EC2 instance, an Elastic IP Address, and a route table entry. The instances begin serving traffic after the route table has been updated. **This only applies to the dev accounts. Surfline prod uses the AWS NAT Gateway service.**

**Note:** The existing route to the internet (0.0.0.0/0) must be manually removed from the private route tables referenced in this stack in order for this to apply cleanly.

Additional info:

[NAT Instance - AWS NAT Instances](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html)

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.28`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement its respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=<environment_folder> make plan
ENV=<environment_folder> make apply
```
ex:
```bash
ENV=sl-dev-us-west-1 make plan
ENV=sl-dev-us-west-1 make apply
```

Valid environments currently only include `sl-dev-us-west-1`.

#### State Management

State is uploaded to S3 after `make setup` and `make apply` are run. State details can be found in each environment's `main.tf` file.
