provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-nat-instance/sl-dev-us-west-1/terraform.tfstate"
    region = "us-west-1"
  }
}

module "nat-instance-common-us-west-1c" {
  source = "../../"

  ami_name               = "amzn-ami-vpc-nat-2018.03.0.20200206.0-x86_64-ebs"
  application            = "nat-instance"
  company                = "sl"
  destination_cidr_block = "0.0.0.0/0"
  environment            = "sl-dev-us-west-1"
  instance_type          = "t3.large"
  key_name               = "sturdy-surfline-dev"
  region                 = "us-west-1"
  route_table_id         = "rtb-29711b4c"
  security_groups        = ["sg-99aeaefc", "sg-91aeaef4"]
  subnet_id              = "subnet-0b09466e"
}

module "nat-instance-common-us-west-1b" {
  source = "../../"

  ami_name               = "amzn-ami-vpc-nat-2018.03.0.20200206.0-x86_64-ebs"
  application            = "nat-instance"
  company                = "sl"
  destination_cidr_block = "0.0.0.0/0"
  environment            = "sl-dev-us-west-1"
  instance_type          = "t3.large"
  key_name               = "sturdy-surfline-dev"
  region                 = "us-west-1"
  route_table_id         = "rtb-28711b4d"
  security_groups        = ["sg-99aeaefc", "sg-91aeaef4"]
  subnet_id              = "subnet-f4d458ad"
}
