data "aws_ami" "nat_instance_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_name]
  }

  owners = ["amazon"]
}

resource "aws_eip" "elastic_ip" {
  instance = aws_instance.nat_instance.id
  vpc      = true
}

resource "aws_eip_association" "nat_instance_eip_association" {
  instance_id   = aws_instance.nat_instance.id
  allocation_id = aws_eip.elastic_ip.id
}

resource "aws_route" "route_table_entry" {
  instance_id            = aws_instance.nat_instance.id
  destination_cidr_block = var.destination_cidr_block
  route_table_id         = var.route_table_id
}

resource "aws_instance" "nat_instance" {
  ami                    = data.aws_ami.nat_instance_ami.id
  instance_type          = var.instance_type
  key_name               = var.key_name
  source_dest_check      = false
  vpc_security_group_ids = var.security_groups
  subnet_id              = var.subnet_id

  tags = {
    Name        = "${var.company}-${var.application}-${var.subnet_id}"
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Region      = var.region
    Terraform   = true
  }
}
