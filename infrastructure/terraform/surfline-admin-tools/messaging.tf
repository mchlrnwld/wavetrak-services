module "sns_spot_report_updated" {
  source      = "./modules/sns/"
  topic_name  = "spot_report_updated"
  environment = var.environment
}

module "sns_spot_details_updated" {
  source      = "./modules/sns/"
  topic_name  = "spot_details_updated"
  environment = var.environment
}

module "sqs_spot_report_updated" {
  source        = "./modules/sqs/"
  environment   = var.environment
  sns_topic_arn = module.sns_spot_report_updated.topic
  queue_name    = "spot_report_updated_cache_service"
}

module "sns_clear_cf_redis" {
  source      = "./modules/sns/"
  topic_name  = "clear_cf_redis"
  environment = var.environment
}
