provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-admin-tools/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-admin-tools" {
  source = "../../"

  company     = "sl"
  application = "admin-tools"
  environment = "prod"

  internal_sg_group = "sg-a5a0e5c0"
  instance_subnets = [
    "subnet-d1bb6988",
    "subnet-85ab36e0"
  ]

  sg_mongo       = "sg-7b978f1e"
  sg_all_servers = "sg-a4a0e5c1"
  sg_cfredis     = "sg-a5d302c1"
}
