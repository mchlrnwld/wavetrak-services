provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-admin-tools/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-admin-tools" {
  source = "../../"

  company     = "sl"
  application = "admin-tools"
  environment = "sandbox"

  internal_sg_group = "sg-90aeaef5"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]

  sg_mongo   = "sg-38903d5c"
  sg_cfredis = "sg-7967c51d"
}
