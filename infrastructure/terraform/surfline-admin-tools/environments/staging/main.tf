provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-admin-tools/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surfline-admin-tools" {
  source = "../../"

  company     = "sl"
  application = "admin-tools"
  environment = "staging"

  internal_sg_group = "sg-90aeaef5"
  instance_subnets = [
    "subnet-0909466c",
    "subnet-f2d458ab"
  ]

  sg_mongo   = "sg-38903d5c"
  sg_cfredis = "sg-7967c51d"
}
