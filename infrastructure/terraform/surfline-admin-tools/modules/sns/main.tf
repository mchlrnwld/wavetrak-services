resource "aws_sns_topic" "topic" {
  name = "${var.topic_name}_${var.environment}"
}

resource "aws_iam_policy" "policy" {
  name        = "sns_policy_${var.topic_name}_${var.environment}"
  description = "policy to publish to sns topic"
  policy = templatefile("${path.module}/resources/sns-policy.json", {
    sns_topic = aws_sns_topic.topic.id
  })
}
