output "topic" {
  value = aws_sns_topic.topic.arn
}

output "topic_policy" {
  value = aws_iam_policy.policy.id
}
