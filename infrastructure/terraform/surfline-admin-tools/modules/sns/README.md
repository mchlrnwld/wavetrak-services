# SNS Module

## Summary

This module creates an SNS topic and a policy granting access to publish to the topic.

## Usage

```hcl
module "sns_topic" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"
  environment = "sandbox"
  topic_name  = "sl-careers-update-cache-sns"
}
```
