resource "aws_sqs_queue" "queue_deadletter" {
  name = "${var.queue_name}_deadletter_${var.environment}"
}

resource "aws_sqs_queue" "queue" {
  name = "${var.queue_name}_${var.environment}"
  redrive_policy = templatefile("${path.module}/resources/sqs-deadletter-policy.json", {
    sqs_queue_deadletter = aws_sqs_queue.queue_deadletter.arn
  })
}

resource "aws_sqs_queue_policy" "publish_to_queue" {
  queue_url = aws_sqs_queue.queue.id
  policy = templatefile("${path.module}/resources/sqs-queue-policy.json", {
    sqs_queue = aws_sqs_queue.queue.arn
    sns_topic = var.sns_topic_arn
  })
}

resource "aws_sns_topic_subscription" "queue_subscription" {
  topic_arn              = var.sns_topic_arn
  protocol               = "sqs"
  endpoint               = aws_sqs_queue.queue.arn
  endpoint_auto_confirms = "true"
}

resource "aws_iam_policy" "policy" {
  name        = "sqs_policy_${var.queue_name}_${var.environment}"
  description = "policy access sqs queue"
  policy = templatefile("${path.module}/resources/sqs-policy.json", {
    sqs_queue = aws_sqs_queue.queue.arn
  })
}
