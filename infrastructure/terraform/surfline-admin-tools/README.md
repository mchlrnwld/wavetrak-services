# Surfline Admin Tools

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Deploying to a AWS environment

Ensure that `ENV` is set to the proper environment. (`sbox`, `staging,` `prod`)

```bash
make plan
make apply
```
