# Logs
resource "aws_cloudwatch_log_group" "main" {
  name              = "${var.company}-logs-${var.application}-${var.environment}"
  retention_in_days = 3
}
