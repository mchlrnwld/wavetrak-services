resource "aws_s3_bucket" "admin_lambda_functions" {
  bucket = "${var.company}-admin-lambda-functions-${var.environment}"
}

# Sort Subregion Spots
resource "aws_sns_topic_subscription" "subscribe_sort_subregion_spots_to_sns_spot_details_updated" {
  topic_arn = module.sns_spot_details_updated.topic
  protocol  = "lambda"
  endpoint  = aws_lambda_function.sort_subregion_spots_lambda.arn
}

resource "aws_lambda_permission" "sns_sort_subregion_spots_lambda" {
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  function_name = aws_lambda_function.sort_subregion_spots_lambda.arn
  statement_id  = "AllowExecutionFromSNS"
  source_arn    = module.sns_spot_details_updated.topic
}

resource "aws_lambda_function" "sort_subregion_spots_lambda" {
  function_name = "${var.company}-sort-subregion-spots-${var.environment}"
  runtime       = "nodejs12.x"
  timeout       = 60
  s3_bucket     = aws_s3_bucket.admin_lambda_functions.bucket
  s3_key        = "${var.company}-sort-subregion-spots-lambda.zip"
  handler       = "dist/index.default"
  role          = aws_iam_role.lambda_role.arn
  memory_size   = 512
  vpc_config {
    subnet_ids         = var.instance_subnets
    security_group_ids = [var.sg_mongo]
  }
}

data "aws_kms_key" "wt_common_kms_key_sort_subregion_spots" {
  key_id = "alias/${var.environment}/common"
}

resource "aws_iam_policy" "wt_common_kms_key_sort_subregion_spots" {
  name        = "wt-common-kms-key-sort-subregion-spots-${var.environment}"
  description = "Policy for access to KMS key that encrypts commonly shared service/lambda secrets."
  policy = templatefile("${path.module}/resources/ssm-kms-policy.json", {
    resource    = "${var.environment}/common"
    kms_key_arn = data.aws_kms_key.wt_common_kms_key_sort_subregion_spots.arn
  })
}

resource "aws_iam_role_policy_attachment" "attach_wt_common_kms_key_sort_subregion_spots" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.wt_common_kms_key_sort_subregion_spots.arn
}

resource "aws_kms_key" "wt_function_kms_key_sort_subregion_spots" {
  description             = var.key_description
  key_usage               = var.key_usage
  is_enabled              = var.key_is_enabled
  enable_key_rotation     = var.key_enable_rotation
  deletion_window_in_days = var.key_deletion_window_in_days

  tags = {
    Company     = var.company
    Service     = "lambda"
    Application = "sort-subregion-spots"
    Function    = "${var.company}-sort-subregion-spots-${var.environment}"
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_iam_policy" "wt_function_kms_key_sort_subregion_spots" {
  name        = "wt-function-kms-key-sort-subregion-spots-${var.environment}"
  description = "Policy for access to KMS key that encrypts lambda secrets."
  policy = templatefile("${path.module}/resources/ssm-kms-policy.json", {
    resource    = "${var.environment}/functions/admin-tools-sort-subregion-spots"
    kms_key_arn = aws_kms_key.wt_function_kms_key_sort_subregion_spots.arn
  })
}

resource "aws_kms_alias" "wt_function_kms_key_alias_sort_subregion_spots" {
  name          = "alias/${var.environment}/functions/admin-tools-sort-subregion-spots"
  target_key_id = aws_kms_key.wt_function_kms_key_sort_subregion_spots.key_id
}

resource "aws_iam_role_policy_attachment" "attach_wt_function_kms_key_sort_subregion_spots" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.wt_function_kms_key_sort_subregion_spots.arn
}

# Lambda
resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-lambda-${var.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/lambda-role-policy.json")
}

resource "aws_iam_role_policy_attachment" "lambda_execution_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda_sns_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = module.sns_spot_report_updated.topic_policy
}
