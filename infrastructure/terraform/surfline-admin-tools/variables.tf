variable "company" {
  default = "sl"
}

variable "application" {
  default = "admin-tools"
}

variable "environment" {
  default = "staging"
}

variable "internal_sg_group" {
  default = "sg-90aeaef5"
}

variable "sg_mongo" {
  default = "sg-38903d5c"
}

variable "sg_all_servers" {
  default = "sg-91aeaef4"
}

variable "sg_cfredis" {
  default = "sg-7967c51d"
}

variable "instance_subnets" {
  default = ["subnet-0909466c", "subnet-f2d458ab"]
}

# KMS.
variable "key_description" {
  description = "Description of the resource's use."
  default     = "Function specific KMS key."
}

variable "key_usage" {
  description = "Intended key operations."
  default     = "ENCRYPT_DECRYPT"
}

variable "key_is_enabled" {
  description = "Specifies whether key is enabled."
  default     = true
}

variable "key_enable_rotation" {
  description = "Specifies whether key rotation is enabled."
  default     = false
}

variable "key_deletion_window_in_days" {
  description = "Number of days a secret is retained after being marked for deletion."
  default     = 30
}
