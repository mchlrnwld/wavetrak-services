provider "aws" {
  region = var.region
}

locals {
  prefix = "cw-3ct"
  common_tags = {
    Company     = var.company
    Service     = var.application
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_customer_gateway" "main" {
  bgp_asn    = var.gateway_bgp_asn
  ip_address = var.gateway_ip_address
  type       = var.gateway_type
  count      = var.main_gateway_enabled ? 1 : 0
  tags = merge(
    {
      Name = "${local.prefix}-network-gw-${var.environment}"
    },
    local.common_tags
  )
}

resource "aws_vpn_connection" "main" {
  vpn_gateway_id      = var.vpn_gateway_id
  customer_gateway_id = aws_customer_gateway.main[0].id
  type                = var.gateway_type
  static_routes_only  = var.vpn_static_routes_only
  count               = var.main_gateway_enabled ? 1 : 0
  tags = merge(
    {
      Name = "${local.prefix}-vpn-connection-${var.environment}"
    },
    local.common_tags
  )
}

# This uses `secondary` naming rather than `main` like the other environments.
# This particular environment required a lot of back-and-forth with 3CT and it's
# now that it's working, it's easier to leave it as-is with a divergent name than
# it is to recreate the connections and go through config with 3CT again.
resource "aws_customer_gateway" "secondary" {
  bgp_asn    = var.gateway_bgp_asn
  ip_address = var.gateway_ip_address
  type       = var.gateway_type
  count      = var.secondary_gateway_enabled ? 1 : 0
  tags = merge(
    {
      Name = "${local.prefix}-network-gw-secondary-${var.environment}"
    },
    local.common_tags
  )
}

resource "aws_vpn_connection" "secondary" {
  vpn_gateway_id      = var.vpn_gateway_id
  customer_gateway_id = aws_customer_gateway.secondary[0].id
  static_routes_only  = var.vpn_static_routes_only
  type                = var.gateway_type
  count               = var.secondary_gateway_enabled ? 1 : 0
  tags = merge(
    {
      Name = "${local.prefix}-vpn-connection-secondary-${var.environment}"
    },
    local.common_tags
  )
}
