variable "company" {
  description = "The company that these resources are being created for"
  default     = "sl"
}

variable "application" {
  description = "The application that these resources are being created for"
  default     = "cw-network-gw"
}

variable "environment" {
  description = "The environment that these resources are being created in"
}

variable "region" {
  description = "The region these resources are being created in"
  default     = "us-west-1"
}

variable "gateway_ip_address" {
  description = "The IP address of the gateway's Internet-routable external interface"
}

variable "gateway_bgp_asn" {
  description = "The gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN)"
  default     = "38829"
}

variable "gateway_type" {
  description = "The type of customer gateway"
  default     = "ipsec.1"
}

variable "vpn_gateway_id" {
  description = "The ID of the Virtual Private Gateway"
}

variable "vpn_static_routes_only" {
  description = "Whether the VPN connection uses static routes exclusively."
  default     = false
}

variable "main_gateway_enabled" {
  description = "Enable or not the creation of the main Gateway and VPC connection."
  default     = true
}

variable "secondary_gateway_enabled" {
  description = "Enable or not the creation of the secondary Gateway and VPC connection."
  default     = false
}
