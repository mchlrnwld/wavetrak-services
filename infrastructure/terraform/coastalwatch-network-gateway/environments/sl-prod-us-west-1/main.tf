terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "cw-network-gw/sl-prod-us-west-1/terraform.tfstate"
    region = "us-west-1"
  }
}

module "coastalwatch-network-gateway" {
  source = "../../"

  environment               = "prod"
  gateway_ip_address        = "118.127.180.241"
  vpn_gateway_id            = "vgw-53faa716"
  main_gateway_enabled      = false
  secondary_gateway_enabled = true
}

module "coastalwatch-network-gateway-dc2" {
  source = "../../"

  environment        = "prod"
  gateway_ip_address = "118.127.162.1"
  vpn_gateway_id     = "vgw-53faa716"
}
