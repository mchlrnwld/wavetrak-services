terraform {
  backend "s3" {
    bucket = "sl-tf-state-legacy"
    key    = "cw-network-gw/sl-legacy-us-west-1/terraform.tfstate"
    region = "us-west-1"
  }
}

module "coastalwatch-network-gateway" {
  source = "../../"

  environment        = "legacy"
  gateway_ip_address = "118.127.180.245"
  vpn_gateway_id     = "vgw-fa2f7cbf"
}

module "coastalwatch-network-gateway-dc2" {
  source = "../../"

  environment        = "legacy"
  gateway_ip_address = "118.127.162.1"
  vpn_gateway_id     = "vgw-fa2f7cbf"
}
