terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "cw-network-gw/sl-dev-us-west-1/terraform.tfstate"
    region = "us-west-1"
  }
}

module "coastalwatch-network-gateway" {
  source = "../../"

  environment        = "dev"
  gateway_ip_address = "118.127.180.241"
  vpn_gateway_id     = "vgw-785a043d"
}

module "coastalwatch-network-gateway-dc2" {
  source = "../../"

  environment        = "dev"
  gateway_ip_address = "118.127.162.1"
  vpn_gateway_id     = "vgw-785a043d"
}
