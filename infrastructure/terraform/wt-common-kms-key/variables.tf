variable "company" {
  description = "The company that these resources are being created for."
  default     = "wt"
}

variable "application" {
  description = "The application that these resources are being created for."
  default     = "common-kms-key"
}

variable "environment" {
  description = "The environment that these resources are being created in."
}

variable "name" {
  description = "The name of the resource in the AWS console."
  default     = ""
}

variable "description" {
  description = "Description of the key."
  default     = "KMS key to encrypt common shared secrets, mainly used by ECS services and Lambda functions."
}

variable "key_usage" {
  description = "Intended use of the key."
  default     = "ENCRYPT_DECRYPT"
}

variable "is_enabled" {
  description = "Specifies whether key is enabled."
  default     = true
}

variable "enable_key_rotation" {
  description = "Specifies whether key rotation is enabled."
  default     = false
}

variable "deletion_window_in_days" {
  description = "Number of days a secret is retained after being marked for deletion."
  default     = 30
}
