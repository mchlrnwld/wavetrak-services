terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wt-common-kms-key/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "wt_common_kms_key" {
  source = "../.."

  environment = "prod"
}
