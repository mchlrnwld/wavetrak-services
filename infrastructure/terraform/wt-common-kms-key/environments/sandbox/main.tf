terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "wt-common-kms-key/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "wt_common_kms_key" {
  source = "../.."

  environment = "sandbox"
}
