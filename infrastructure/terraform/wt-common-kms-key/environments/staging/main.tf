terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "wt-common-kms-key/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "wt_common_kms_key" {
  source = "../.."

  environment = "staging"
}
