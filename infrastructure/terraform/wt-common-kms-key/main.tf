provider "aws" {
  region = "us-west-1"
}

locals {
  name = "${var.company}-${var.application}-${var.environment}"
}

resource "aws_kms_key" "wt_common_kms_key" {
  description             = var.description
  key_usage               = var.key_usage
  is_enabled              = var.is_enabled
  enable_key_rotation     = var.enable_key_rotation
  deletion_window_in_days = var.deletion_window_in_days

  tags = {
    Name        = local.name
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

resource "aws_kms_alias" "wt_common_kms_key_alias" {
  name          = "alias/${var.environment}/common"
  target_key_id = aws_kms_key.wt_common_kms_key.key_id
}
