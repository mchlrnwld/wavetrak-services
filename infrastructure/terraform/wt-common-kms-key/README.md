# KMS Key Module

This module creates a common KMS key that is shared by services with overlapping variables.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.06`._

Terraform projects are broken up into modules and environments. `wt-common-kms-key/main.tf` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to create a KMS key.

```bash
ENV=sandbox make plan
ENV=sandbox make apply
```

Valid environments are `sandbox`, `staging` and `prod`.

#### State Management

State is uploaded to the S3 directory specified in the environment's `main.tf` file.

### Directory Structure

```bash
wt-common-kms-key
├── environments/
│   ├── prod/
│   │   └── main.tf
│   ├── sandbox/
│   │   └── main.tf
│   └── staging/
│       └── main.tf
├── modules/
├── resources/
├── .gitignore
├── .terraform-version
├── Makefile
├── README.md
├── main.tf
├── outputs.tf
├── variables.tf
└── versions.tf
```
