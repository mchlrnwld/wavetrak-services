# ECS Service Warmer

This is the start of a boto script that will warm ECS services in preparation for an upcoming push notification or event where we expect to see an immediate increase in traffic to our services where auto-scaling won't have a chance to kick in.

## Running the script

```
python warm_services.py --plan ios_push_prod --scale=out
```

## Future Work

- This should create a new auto scaling group for each service. The script in it's current form naively increase the desired count by a scaling factor. This works for some services, but for other service with aggresive scale-in policies the desired count won't remain in affect long enough to handle the incoming request spike.
