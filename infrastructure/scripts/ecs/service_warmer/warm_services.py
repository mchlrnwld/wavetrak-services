import boto3
import argparse
import pickle
import configparser
import sys
import os


class Plan:

    def __init__(self, plan_name, dry_run):
        config = configparser.ConfigParser()
        config.read('service_scaling_plans.ini')

        self.client = boto3.client('ecs', region_name='us-west-1')
        self.plan = {}
        self.dry_run = dry_run

        self.plan_name = plan_name
        self.services = [srv.strip() for srv in config.get(
            plan_name, 'services').split('\n') if len(srv) > 0]
        self.cluster = config.get(plan_name, 'cluster')
        self.scale_factor = float(config.get(plan_name, 'scale_factor'))

        self.ecs_client = boto3.client('ecs', region_name='us-west-1')
        self.build_plan()

    def build_plan(self):
        plan = {}
        for service in self.services:
            service_query = self.ecs_client.describe_services(
                cluster=self.cluster,
                services=[service]
            )

            if not service_query['services']:
                raise ValueError('Service not found', service)

            srv = service_query['services'][0]
            running = srv['runningCount']
            plan[service] = [running, int(round(running * self.scale_factor))]

        self.plan = plan

    def print_plan(self):
        print('PLAN: Scaling the following')
        for service, scale_plan in self.plan.items():
            original, to = scale_plan
            print("\t{} {} -> {}".format(service, original, to))

    def do_scale(self, service, count):
        if not self.dry_run:
            self.client.update_service(
                cluster=self.cluster,
                service=service,
                desiredCount=count
            )

    def scale_out(self, dry_run=True):
        # TODO check if lock exists
        print('APPLY: Applying scale out')
        for service, scale_plan in self.plan.items():
            original, to = scale_plan
            print("\t scaling {}: from {} to {}".format(service, original, to))
            self.do_scale(service, to)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Warm ecs services.')
    parser.add_argument('--dry-run', dest='dry_run', action='store_true',
                        help='perform a dry run of the warm up')
    parser.add_argument('--plan', dest='plan',
                        help='the location of the plan file')
    parser.add_argument('--scale', dest='scale',
                        help='should the plan be scaled in or out')

    args = parser.parse_args()
    plan = Plan(args.plan, args.dry_run)
    plan.print_plan()

    if not args.scale:
        print('you must specify a scale operation')
        sys.exit(1)

    if args.scale == 'out':
        plan.scale_out()

    if args.scale == 'in':
        print('Not implemented')
