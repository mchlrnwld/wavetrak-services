from __future__ import print_function
import boto3
import json
import re

ecs = boto3.client('ecs')

for cluster_arn in ecs.list_clusters()['clusterArns']:
    for service_arn in ecs.list_services(cluster=cluster_arn)['serviceArns']:
        for svc_desc in ecs.describe_services(cluster=cluster_arn,
                                              services=[service_arn])['services']:  # pylint: disable=C0301
            task_def_arn = svc_desc['taskDefinition']
            task_def = ecs.describe_task_definition(taskDefinition=task_def_arn)  # pylint: disable=C0301
            task_def_name = re.match(r".*\/(.*):[0-9]+", task_def_arn).group(1)
            task_def_filename = 'taskdefs/{}.json'.format(task_def_name)

            print('Writing {}...'.format(task_def_filename), end='')

            with open(task_def_filename, 'w') as f:
                f.write(json.dumps(task_def['taskDefinition'],
                                   indent=2, separators=(',', ': '),
                                   sort_keys=True))
                f.close()
            print('Done')
