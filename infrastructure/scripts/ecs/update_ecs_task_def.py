from __future__ import print_function
import boto3
import json
import re

# make sure to replace <PASSWORD> with actual password
MONGO_HOSTS = 'mongodb://admin:<PASSWORD>@dev-common-mongodb-atlas-shard-00-00-baspr.mongodb.net:27017,dev-common-mongodb-atlas-shard-00-01-baspr.mongodb.net:27017,dev-common-mongodb-atlas-shard-00-02-baspr.mongodb.net:27017'  # pylint: disable=C0301

# set to 'file' to dump the process task definition to file for inspection
TASK_DESTINATION = 'ecs'

ecs = boto3.client('ecs')

# uncomment this and set a cluster ARN(s) for limited touch
# cluster = ['arn:aws:ecs:us-west-1:665294954271:cluster/sl-admin-tools-sandbox']
# for cluster_arn in cluster:
for cluster_arn in ecs.list_clusters()['clusterArns']:
    print('Working on cluster {}...'.format(cluster_arn))
    for service_arn in ecs.list_services(cluster=cluster_arn)['serviceArns']:
        for svc_desc in ecs.describe_services(cluster=cluster_arn,
                                              services=[service_arn])['services']:  # pylint: disable=C0301
            task_def_arn = svc_desc['taskDefinition']
            task_def = ecs.describe_task_definition(taskDefinition=task_def_arn)  # pylint: disable=C0301
            task_def_name = re.match(r".*\/(.*):[0-9]+", task_def_arn).group(1)
            task_def_filename = 'taskdefs/{}.json'.format(task_def_name)

            # filter out the compatabilities to match Jenkins task def output
            if 'compatibilities' in task_def['taskDefinition']:
                task_def['taskDefinition'].pop('compatibilities')

            # iterate over each possible container def even though likely
            # there's only 1 for each task def
            for c in task_def['taskDefinition']['containerDefinitions']:
                for e in c['environment']:
                    # update MongoDB related connection strings where they exist
                    if ('MONGO_CONNECTION_' or 'MONGO_IP') in e['name']:
                        mongo = e['value']
                        print("Before: {}".format(mongo))
                        mongo = re.sub('.*/', "{}/".format(MONGO_HOSTS), mongo)
                        mongo = re.sub('\?.*',
                                            '?ssl=true&replicaSet=dev-common-mongodb-atlas-shard-0&authSource=admin&readPreference=secondary',  # pylint: disable=C0301
                                            mongo)
                        print("After: {}".format(mongo))
                        # update the environment have we've modified it
                        e['value'] = mongo
            # send the modified task definition to the desired destination
            if TASK_DESTINATION == 'file':
                # write the rendered task definition back to file
                print('Writing {}...'.format(task_def_filename))
                with open(task_def_filename, 'w') as f:
                    f.write(json.dumps(task_def['taskDefinition'],
                                       indent=2, separators=(',', ': '),
                                       sort_keys=True))
                    f.close()
            elif TASK_DESTINATION == 'ecs':
                # register new task definitions
                task_role_arn = task_def['taskDefinition'].setdefault('taskRoleArn', None)
                if task_role_arn:
                    print('Registering {} with custom role {}...'.format(task_def_name, task_role_arn))
                    ecs.register_task_definition(family=task_def_name,
                                                 taskRoleArn=task_role_arn,
                                                 networkMode='bridge',
                                                 containerDefinitions=task_def['taskDefinition']['containerDefinitions'],  # pylint: disable=C0301
                                                 requiresCompatibilities=['EC2']
                                            )
                else:
                    print('Registering {}...'.format(task_def_name))
                    ecs.register_task_definition(family=task_def_name,
                                                 networkMode='bridge',
                                                 containerDefinitions=task_def['taskDefinition']['containerDefinitions'],  # pylint: disable=C0301
                                                 requiresCompatibilities=['EC2']
                                            )
                # updating services
                print('Updating {}...'.format(svc_desc['serviceName']))
                print(task_def_arn)
                new_task_def_arn = "arn:aws:ecs:us-west-1:665294954271:task-definition/{}".format(task_def_name)
                print(new_task_def_arn)
                ecs.update_service(cluster=cluster_arn,
                                   service=svc_desc['serviceName'],
                                   taskDefinition=new_task_def_arn)
            else:
                print('ERROR')
