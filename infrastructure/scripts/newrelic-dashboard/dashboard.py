#!/bin/env python

# Standard libraries.
import json
import logging
import sys

# Third-party libraries.
from envyaml import EnvYAML as env
from jinja2 import Environment as j2env
from jinja2 import FileSystemLoader as j2fs
import requests


# The configuration filename and location.
CONFIG = "./config.yml"

# The global content of the configuration file.
config = {}


# Main function.
def execute():
    # Validation.
    try:
        global config
        config = env(CONFIG)

    except Exception:
        logging.error("Configuration file invalid: '{}'.".format(CONFIG))
        sys.exit(1)

    # Process.
    try:
        if not get_dashboard():
            raise ValueError(
                "Dashboard not found: "
                "Please verify the API Key and the Dashboard ID.")

        logging.info("Dashboard found in NewRelic.")

        active = config.get("applications.active")
        if not len(active):
            raise ValueError(
                "No active applications in config file.")

        apps = get_applications(active)
        if not len(apps):
            raise ValueError(
                "One or more applications weren't found "
                "on NewRelic's provided account.")

        logging.info("Active applications = {}.".format(len(apps)))

        logging.info("Parsing data...")
        jinjaEnv = j2env(loader=j2fs("./"))
        template = jinjaEnv.get_template(config.get("template"))
        payload = template.render(
            apps=apps,
            account_id=config.get("account_id"),
        )

        logging.info("Saving dashboard...")
        if not save_dashboard(payload):
            raise ValueError(
                "There was a problem saving the dashboard information.")

        logging.info("Execution was successful!")
        logging.info("Visit the NewRelic Services Dashboard here:\n{}".format(
            config.get("web.dashboard")))

        # Finished normally.
        sys.exit(0)

    except ValueError as err:
        logging.error(err)

    except TypeError:
        logging.error("Configuration file malformed.")

    except (KeyboardInterrupt, StopIteration):
        logging.info("\n\nExecution aborted.")

    # Something prevented the script from finishing normally.
    sys.exit(1)


# Retrieve a set of applications by their ids.
def get_applications(app_ids):
    try:
        response = requests.get(
            config.get("api.applications"),
            headers={
                "X-Api-Key": config.get("api-key"),
                "Content-Type": "application/json",
            },
            params={
                "filter[ids]": ",".join(map(str, app_ids)),
                "exclude_links": True,
            },
        )
        data = json.loads(response.text)["applications"]
        return sorted(data, key=lambda i: i["name"])

    except Exception:
        return None


# Check the existence of the dashboard.
# NewRelic's API needs extra permissions that exceeds the scope of this script.
# A valid and accessible dashboard should have being created with anticipation.
def get_dashboard():
    try:
        response = requests.get(
            config.get("api.dashboard"),
            headers={
                "X-Api-Key": config.get("api-key"),
                "Content-Type": "application/json",
            },
        )
        return json.loads(response.text)["dashboard"]

    except Exception:
        return None


# Update dashboard.
def save_dashboard(payload):
    try:
        response = requests.put(
            config.get("api.dashboard"),
            headers={
                "X-Api-Key": config.get("api-key"),
                "Content-Type": "application/json",
            },
            data=payload
        )
        return json.loads(response.text)["dashboard"]

    except Exception:
        return None


# Execute script.
if __name__ == "__main__":
    logging.basicConfig(
        format="{}".format("[%(levelname)s] %(message)s"),
        level=logging.INFO,
    )
    execute()
