# NewRelic Services Dashboard

A script that recreates a dashboard in [NewRelic] showing health status and metrics for the configured services in a single page.

- [Summary](#summary)
- [Installation](#installation)
- [Uninstallation](#uninstallation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Verification](#verification)
- [Adding or removing a service from the dashboard](#adding-or-removing-a-service-from-the-dashboard)
  - [How to check for application IDs](#how-to-check-for-application-ids)

## Summary

The script takes a [NewRelic]'s API Key, an Account ID and a Dashboard ID in order to update its data according to a configuration changes, like a new service being added in our infrastructure.

It should be that simple as modify the `config.yml` file that comes with this script, setting the right values.

## Installation

This script is intended to run in a Conda environment, so having Anaconda or Miniconda installed, you'll need to run the next commands:

```console
conda env create -f environment.yml
conda activate newrelic-services-dashboard
```

## Uninstallation

Running the `env remove` command will be enough to delete all Conda environment files, but first you'll want to deactivate it:

```console
conda deactivate
conda env remove -n newrelic-services-dashboard
```

## Usage

The script doesn't need any arguments. It only will alert you if something wrong happened with the configuration:

```console
python dashboard.py
```

```bash
[INFO] Dashboard found in NewRelic.
[INFO] Active applications = 40.
[INFO] Parsing data...
[INFO] Saving dashboard...
[INFO] Execution was successful!
```

## Configuration

You'll need to retrieve a few secrets from AWS Parameter Store and save them into a `.env` file before to proceed.

If you have access to the AWS Console on `prod` go to **Systems Manager > Parameter Store**:

|Parameter name|
|---|
|[/prod/common/NEWRELIC_API_KEY]|
|[/prod/common/NEWRELIC_ACCOUNT_ID]|
|[/prod/common/NEWRELIC_DASHBOARD_ID]|

Create a copy of `.env.sample` and store the values:

```console
cp .env.sample .env
vi .env
```

```bash
# .env
NEWRELIC_API_KEY="NRAK-CLSMPTBKZS9I115NM310627HOGF"
NEWRELIC_ACCOUNT_ID="356245"
NEWRELIC_DASHBOARD_ID="1722718"
```

> _TIP: The values for Account ID and Dashboard ID are the right ones ;)_

## Verification

Unless a new dashboard was created after the creation of this document, you can go to the **[Dashboard on this link]** and verify that all "active" applications in `config.yml` are present in this page.

This is part of the configuration file:

```yaml
api-key: "${NEWRELIC_API_KEY}"
account_id: "${NEWRELIC_ACCOUNT_ID}"
dashboard_id: "${NEWRELIC_DASHBOARD_ID}"
template: "./dashboard.j2"
api:
  applications: "https://api.newrelic.com/v2/applications.json"
  dashboard: "https://api.newrelic.com/v2/dashboards/${NEWRELIC_DASHBOARD_ID}.json"
applications:
  active:
    - 85485931   # Buoyweather Web
    - 93545102   # Buoyweather API
    - 10564777   # BW WEB
...
```

## Adding or removing a service from the dashboard

You can do any change from the NewRelic's UI, but the script will overwrite any customization if it runs again.

If a new service needs to be added to this dashboard, it needs to be added to `config.yml` under `applications.active`.

The changes in `config.yml` should be commited once tested.

### How to check for application IDs

On the previous NewRelic's version (it was updated on March 22th, 2021) you could step on the app name and then **Settings > Application** and the ID was in a metadata panel on the right. Now it can't be found with a single way from the UI for all of the applications because each of them may have a different visualization.

If you go to **Browser** on the top menu, you can see a list of the apps available on that mode, under the side menu Browser applications. But this only happens with applications tagged as "web applications".

A proper and consistent way to find the application IDs is using is the `curl` command:

```console
curl -X GET 'https://api.newrelic.com/v2/applications.json' -H 'X-Api-Key: REDACTED' | \
  jq -r '.applications[] | [.id, ": ", .name] | join("")' | \
  sort
```

```bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 70477  100 70477    0     0   7090      0  0:00:09  0:00:09 --:--:-- 20631

101862038: Surfline Platform - Sessions
102922610: Surfline Services - Forecasts API
10564777:  BW WEB
10764748:  Surfline CF
107766152: Surfline Services - Auth (undefined)
10796053:  Surfline Admin
10796594:  Beachlive
10816472:  Surfline Tile Server
...
```

---

[NewRelic]: https://rpm.newrelic.com/accounts/356245/applications
[Dashboard on this link]: https://one.newrelic.com/launcher/dashboards.launcher?pane=eyJuZXJkbGV0SWQiOiJkYXNoYm9hcmRzLmRhc2hib2FyZCIsImVudGl0eUlkIjoiTXpVMk1qUTFmRlpKV254RVFWTklRazlCVWtSOE1UY3lNamN4T0EiLCJ1c2VEZWZhdWx0VGltZVJhbmdlIjpmYWxzZSwiaXNUZW1wbGF0ZUVtcHR5IjpmYWxzZX0=&platform[accountId]=356245&platform[$isFallbackTimeRange]=false&platform[filters]=IihuYW1lIExJS0UgJ3N1cmZsaW5lJyBPUiBpZCA9ICdzdXJmbGluZScgT1IgZG9tYWluSWQgPSAnc3VyZmxpbmUnKSI=&platform[timeRange][duration]=1800000
[/prod/common/NEWRELIC_API_KEY]: https://us-west-1.console.aws.amazon.com/systems-manager/parameters/prod/common/NEWRELIC_ACCOUNT_ID/description?region=us-west-1&tab=Table
[/prod/common/NEWRELIC_ACCOUNT_ID]: https://us-west-1.console.aws.amazon.com/systems-manager/parameters/prod/common/NEWRELIC_ACCOUNT_ID/description?region=us-west-1&tab=Table
[/prod/common/NEWRELIC_DASHBOARD_ID]: https://us-west-1.console.aws.amazon.com/systems-manager/parameters/prod/common/NEWRELIC_DASHBOARD_ID/description?region=us-west-1&tab=Table