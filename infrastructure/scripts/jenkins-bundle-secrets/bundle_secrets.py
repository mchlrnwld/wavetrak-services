#!/bin/env python

# Standard libraries.
import argparse
import json
import logging
import os
import sys

# Third-party libraries.
import boto3
from botocore.exceptions import ClientError


# Global exit code:
#   0 = Success.
#   1 = Error.
exit_code = 0


# Main function.
def execute():
    global exit_code
    try:
        args = parse_arguments()

        ci_json = args['ci_json']
        if not os.path.exists(ci_json):
            exit_code = 1
            raise FileNotFoundError(
                'Secrets file not found: {}'
                .format(ci_json))

        env = args['env']
        parameters = get_parameters(ci_json, env)
        if len(parameters):
            secrets = get_secrets(parameters, env)
            for secret in secrets:
                print('{}={}'.format(
                    secret['name'],
                    secret['value']))

    except Exception as err:
        exit_code = 1
        logging.error(err)

    sys.exit(exit_code)


# Handles the script arguments and options validation.
def parse_arguments():
    ap = argparse.ArgumentParser(
        description='''
            A script to create a .env file and add it to the lambda functions
            zip file bundle.
        '''
    )
    ap.add_argument(
        '--ci-json',
        help='the location of the ci.json file where to fetch variables from',
        required=True,
    )
    ap.add_argument(
        '--env',
        help='environment to pick secrets for',
        required=True,
    )
    return vars(ap.parse_args())


# Retrieves a complete list of parameters according to variables in json file.
def get_parameters(ci_json, env):
    global exit_code
    try:
        parameters = []

        with open(ci_json, 'r') as ci_json_file:
            content = json.load(ci_json_file)
            blocks = content.get('secrets')

            if not blocks:
                exit_code = 1
                logging.warning(
                    'No secrets found in file: {}'
                    .format(ci_json))

            else:
                for block in blocks.keys():
                    if block not in ['common', 'service']:
                        continue

                    group = blocks[block]
                    prefix = (None, group['prefix'])['prefix' in group]
                    params = (None, group['secrets'])['secrets' in group]

                    if not prefix and not params:
                        exit_code = 1
                        logging.warning(
                            'Secrets structure invalid in file: {}'
                            .format(ci_json))
                        continue

                    for param in params:
                        parameters.append({
                            'prefix': prefix.replace('{env}', env),
                            'name': param,
                        })

        logging.debug(
            'data = {}'
            .format(json.dumps(parameters, indent=4)))
        return parameters

    except Exception as err:
        logging.error(err)


# Creates a list of the parameters and their secrets, retrieved from AWS.
def get_secrets(parameters, env):
    try:
        secrets = []
        ssm = boto3.client('ssm')

        for param in parameters:
            if param['name'].lower() == 'env':
                secrets.append({
                    'name': param['name'],
                    'value': env,
                })
                continue

            secret = get_secret(ssm, param)
            secrets.append({
                'name': param['name'],
                'value': secret.get('value') if secret else '',
            })

        logging.debug(
            'data = {}'
            .format(json.dumps(secrets, indent=4)))
        return secrets

    except Exception as err:
        logging.error(err)


# Retrieves the parameter store data from AWS.
# The script assumes all secrets need a KMS key.
def get_secret(ssm, parameter):
    global exit_code
    try:
        secret = ssm.get_parameter(
            Name='{}{}'.format(
                parameter['prefix'],
                parameter['name'],
            ),
            WithDecryption=True
        )
        logging.debug(
            'Parameter found: "{}{}".'
            .format(parameter['prefix'], parameter['name']))

        data = {
            'name': secret['Parameter']['Name'],
            'value': secret['Parameter']['Value'],
        }
        logging.debug(
            'data = {}'
            .format(json.dumps(data, indent=4)))
        return data

    except ClientError as err:
        exit_code = 1
        code = err.response['Error']['Code']
        if code == 'ParameterNotFound':
            logging.warning(
                'Parameter not found: "{}{}".'
                .format(parameter['prefix'], parameter['name']))
        else:
            logging.error(err)

    except Exception as err:
        logging.error(err)


# Execute script.
if __name__ == '__main__':
    logging.basicConfig(
        format='[%(levelname)s] %(name)s: %(funcName)s - %(message)s',
        level=logging.WARNING,
    )
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    execute()
