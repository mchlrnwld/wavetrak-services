# A simple list of Python packages for the Secrets Migration script to run.
# Please see the Dependencies section in the README.md file.
boto3    ~= 1.14.62
botocore ~= 1.17.62
