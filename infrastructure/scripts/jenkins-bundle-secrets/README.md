# Bundle Secrets to a file

- [Bundle Secrets to a file](#bundle-secrets-to-a-file)
  - [Summary](#summary)
  - [Usage](#usage)
  - [Requirements](#requirements)
    - [AWS profiles configuration](#aws-profiles-configuration)
  - [Dependencies](#dependencies)
  - [Debugging](#debugging)

## Summary

A script to retrieve secrets and configurations from AWS Systems Manager Parameter Store and bundle them into an environment variables formatted file to be added on lambda functions zip files ([INF-328]).

## Usage

```bash
python bundle_secrets.py \
  --ci-json CI_JSON \
  --env ENVIRONMENT \
  [--out OUTPUT] \
  [--profile AWS_PROFILE]
```

**Examples:**

```bash
python bundle_secrets.py --ci-json ../../image-service/ci.json --env staging --profile dev
```

If the AWS profile name is the same as the deploying environment, you don't need to add it as an argument.

You can change the output file to write the secrets into another name and location

```bash
python bundle_secrets.py --ci-json ./ci.json --env staging --out ../deploy/.vars
```

## Requirements

### AWS profiles configuration

It's not in the scope of this script to describe how to configure the AWS credentials to work with the CLI or the `boto3` Python package, but your files should look like this, taking into account the profiles set in `.env`:

**Example:**

```bash
# ~/.aws/credentials
[prod]
aws_access_key_id = ************
aws_secret_access_key = ************
region = us-west-1

[dev]
aws_access_key_id = ************
aws_secret_access_key = ************
region = us-west-1
```

```bash
# ~/.aws/config
[profile prod]
region = us-west-1
format = json

[profile dev]
region = us-west-1
format = json
```

## Dependencies

You can make use of `pip` and the `requirements.txt` file in this repository or installing each of them manually, in which case you'll need to have `pip` installed locally.

This is how you can install this script dependencies in a single command:

```bash
pip install -r requirements.txt
```

**Python version used:** 3.8.2

## Debugging

The script will print to `stdout` the status of the process.

You can change the debugging `level` option to print more output data from the `logging.basicConfig` object.

[INF-328]: https://wavetrak.atlassian.net/browse/INF-328
