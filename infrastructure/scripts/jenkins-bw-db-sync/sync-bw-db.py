#!/usr/bin/python
import subprocess
import json
import os

outAssumeRole = subprocess.check_output('aws sts assume-role --role-arn "arn:aws:iam::833713747344:role/cross-account-jenkins-role" --role-session-name "Jenkins-DBSync"', shell=True)
awsAuth = json.loads(outAssumeRole)

os.environ["AWS_SECRET_ACCESS_KEY"] = awsAuth.get('Credentials').get('SecretAccessKey')
os.environ["AWS_SESSION_TOKEN"] = awsAuth.get('Credentials').get('SessionToken')
os.environ["AWS_ACCESS_KEY_ID"] = awsAuth.get('Credentials').get('AccessKeyId')

getMongo = subprocess.check_output('aws s3 cp s3://bw-mongodb-prod/mongodump.tar.gz mongodump.tar.gz', shell=True)
getWordpress = subprocess.check_output('aws s3 cp s3://bw-wordpress-prod/wordpress.sql wordpress.sql', shell=True)

del os.environ["AWS_SECRET_ACCESS_KEY"]
del os.environ["AWS_SESSION_TOKEN"]
del os.environ["AWS_ACCESS_KEY_ID"]

putMongo = subprocess.check_output('aws s3 cp mongodump.tar.gz s3://bw-databases-from-prod/mongodump.tar.gz', shell=True)
putWordpress = subprocess.check_output('aws s3 cp wordpress.sql s3://bw-databases-from-prod/wordpress.sql', shell=True)
