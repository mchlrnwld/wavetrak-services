#!/bin/bash

set -x

USAGE=$(cat <<-END
Usage:
  tmux-ssh <instance type> [environment]

Examples:
  tmux-ssh \*airflow-celery\* prod
  tmux-ssh \*coldfusion-app\*
END
)

# Is the user requesting help?
if [[ "$1" = "-h" ]]; then
  echo "${USAGE}"
  exit
fi

# Set shell for tmux to use
# TMUX_SHELL=/bin/sh

TMUX_SESSION="ssh_ip_list_${RANDOM}"

# SHELL="${TMUX_SHELL}" tmux new -s "${TMUX_SESSION}" -d
tmux new -s "${TMUX_SESSION}" -d

COUNTER=0

# Check to see if a pipe exists on stdin.
if [ -p /dev/stdin ]; then
  echo "[DEBUG] Receiving data from stdin"

  # If we want to read the input line by line
  while IFS= read -r SERVER; do
    # Don't split on the first iteration
    if [[ "${COUNTER}" != 0 ]]; then
      echo "[DEBUG] splitting window"
      tmux split-window -v -t "${TMUX_SESSION}"
      tmux select-layout -t "${TMUX_SESSION}" tiled
    fi

    # Send command
    echo "[DEBUG] ssh'ing to instance ${SERVER}"
    tmux send-keys -t "${TMUX_SESSION}" "/usr/bin/ssh -t ${SERVER}" C-m

    ((COUNTER++))
  done
else
  echo "Error: Didn't receive IP addresses on stdin"
  echo "${USAGE}"
  exit 0
fi

# Start in broadcast mode
# tmux setw -t "${TMUX_SESSION}" synchronize-panes
# tmux set-option -t "${TMUX_SESSION}" -g pane-border-bg colour52
# tmux set-option -t "${TMUX_SESSION}" -g pane-active-border-bg colour52

# Attach to the tmux session
echo "[DEBUG] attaching to window in TERM=${TERM}"
tmux ls
tmux attach -t "${TMUX_SESSION}"
