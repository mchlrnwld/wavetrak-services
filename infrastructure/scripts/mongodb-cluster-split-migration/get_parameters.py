import argparse
import logging
import pprint

import boto3

from services import GROUPS, PARAMETERS, SERVICE_GROUPS, SERVICE_PREFIXES

CLUSTER_PREFIX = "sl-core-svc"

parser = argparse.ArgumentParser()
parser.add_argument(
    "--environment", default="sandbox", choices=["sandbox", "staging", "prod"]
)

service_group = parser.add_mutually_exclusive_group()
service_group.add_argument(
    "--service", default="all", choices=SERVICE_PREFIXES
)
service_group.add_argument("--group", default="all", choices=GROUPS)

parser.add_argument("--debug", action="store_true")
parser.add_argument("--dry-run", action="store_true")

args = parser.parse_args()

logging.basicConfig(level=(logging.DEBUG if args.debug else logging.INFO))
logging.getLogger("boto3").setLevel(logging.ERROR)
logging.getLogger("botocore").setLevel(logging.ERROR)
logging.getLogger("urllib3").setLevel(logging.ERROR)
logger = logging.getLogger()

logger.debug(f"Args: {args}")

ssm = boto3.client("ssm")

# Build list of services to update
services = (
    SERVICE_PREFIXES
    if args.service == "all" and args.group == "all"
    else [args.service]
    if args.service != "all" and args.group == "all"
    else SERVICE_GROUPS[args.group]
)

logger.debug(f"Services to retrieve parameters for: {services}")

for service_prefix in services:
    key = PARAMETERS.get(service_prefix, "").format(env=args.environment)
    parameter_history_response = ssm.get_parameter_history(
        Name=key, WithDecryption=True
    )

    logger.debug(
        "Parameter History Response: "
        f"{pprint.pformat(parameter_history_response)}"
    )

    print(service_prefix)
    print(key)
    print(parameter_history_response["Parameters"][0]["Value"])
    print()
