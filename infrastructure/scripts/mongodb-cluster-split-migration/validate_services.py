import argparse
import logging
import pprint

import requests

CLUSTER_PREFIX = "sl-core-svc"

parser = argparse.ArgumentParser()
parser.add_argument(
    "--environment", default="sandbox", choices=["sandbox", "staging", "prod"]
)

parser.add_argument("--debug", action="store_true")
parser.add_argument("--dry-run", action="store_true")

args = parser.parse_args()

logging.basicConfig(level=(logging.DEBUG if args.debug else logging.INFO))
logging.getLogger("boto3").setLevel(logging.ERROR)
logging.getLogger("botocore").setLevel(logging.ERROR)
logging.getLogger("urllib3").setLevel(logging.ERROR)
logger = logging.getLogger()

logger.debug(f"Args: {args}")

URLS = [
    "http://geo-target-service.{env}.surfline.com/Waterways?lat=49.41&lon=-123.99",
    "http://cameras-service.{env}.surfline.com/cameras",
    "http://charts-service.surfline.com/legacy/images?legacyId=2143&type=localswell",
    "http://forecasts-api.{env}.surfline.com/admin/forecasts?subregionId=58581a836630e24c4487902f",
    "http://kbyg-api.{env}.surfline.com/spots/forecasts/conditions?spotId=5842041f4e65fad6a77088ed&days=3&offset=1",
    "http://reports-api.{env}.surfline.com/admin/reports/spot/?spotId=5842041f4e65fad6a770888a&limit=1",
    "http://spots-api.{env}.surfline.com/taxonomy?type=geoname&id=2077456",
    "http://auth-service.{env}.surfline.com/trusted/authorise?access_token={access_token}",
    "http://entitlements-service.{env}.surfline.com/entitlements?objectId=5cb8d5724409ae00100400fb",
    "http://metering-service.{env}.surfline.com/",
    "http://promotions-service.{env}.surfline.com/admin/promotions",
    "http://subscription-service.{env}.surfline.com/subscriptions",
    "http://user-service.{env}.surfline.com/user/validateemail?email=mmalchak@surfline.com",
    "http://user-service.{env}.surfline.com/admin/users?search=mmalchak@surfline.com",
    "http://favorites-api.{env}.surfline.com/favorites?type=spots",
    "http://notifications-service.{env}.surfline.com/preferences/sessions",
]

for url in URLS:

    headers = {}
    if "metering" in url or "favorites" in url:
        headers["x-geo-country-iso"] = "US"
        if args.environment == "prod":
            headers["x-auth-userId"] = "5e7bba9338079d008f53498d"
        elif args.environment == "staging":
            headers["x-auth-userId"] = "6082520cfccecb00157d5c2c"
        else:
            headers["x-auth-userId"] = "603fd47c06870b0013cd4c6e"

    if "notifications" in url:
        if args.environment == "prod":
            headers["x-auth-userId"] = "5e7bba9338079d008f53498d"
        elif args.environment == "staging":
            headers["x-auth-userId"] = "5ef513746c32320011e0a46b"
        else:
            headers["x-auth-userId"] = "6081cb73d09cdd0013748b42"

    access_token = "74784e0e33f957d954259bbce29abcd26fe6e943"
    if args.environment == "prod":
        access_token = "562f0c13d3e6ecf937e971b65535f3aefb1e5734"

    url_with_env = url.format(env=args.environment, access_token=access_token)

    r = requests.get(url_with_env, headers=headers)

    print(f"{r.status_code}: {url_with_env}")
    logger.debug(pprint.pformat(r))
