# MongoDB Cluster Split Migration

This migration contains scripts to move from a single monolithic MongoDB cluster to three separate MongoDB clusters: 

- `prod-common-mongodb-atlas`
- `prod-kbyg-mongodb-atlas`
- `prod-user-mongodb-atlas`

More information on the initiative is available at
[Confluence - Split MongoDB Cluster](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1096876200/Split+MongoDB+Cluster)

## Setup

```sh
conda env create -f environment.yml
conda activate mongodb-cluster-split-migration
```

## Environment

This migration requires that AWS credentials are available in the environment or in config files.

More inforamtion on AWS credentials is available at
[Surfline/styleguide/terraform](https://github.com/Surfline/styleguide/tree/master/terraform#aws-credentials).

## Files

### `create_task_definitions.py`

For each service to be updated, do the following:

1. Get the latest task definition
1. Add or remove `_MIGRATION` from all secrets starting with `MONGO_CONNECTION_STRING`
1. Register the updated task definition

```console
python create_task_definitions.py --help

usage: create_task_definitions.py [-h] [--environment {sandbox,staging,prod}]
                                  [--service {sl-onboarding-api,sl-sessions-api,sl-cameras-service,sl-forecasts-api,sl-geo-target-service,sl-kbyg-product-api,sl-reports-api,sl-spots-api,wt-charts-service,sl-auth-service,sl-entitlements-service,sl-favorites-api,sl-promotions-service,sl-subscription-service,sl-user-service,wt-metering-service,wt-notifications-service} | --group {common,kbyg,user}]
                                  [--add-migration | --remove-migration]
                                  [--debug] [--dry-run]

optional arguments:
  -h, --help            show this help message and exit
  --environment {sandbox,staging,prod}
  --service {sl-onboarding-api,sl-sessions-api,sl-cameras-service,sl-forecasts-api,sl-geo-target-service,sl-kbyg-product-api,sl-reports-api,sl-spots-api,wt-charts-service,sl-auth-service,sl-entitlements-service,sl-favorites-api,sl-promotions-service,sl-subscription-service,sl-user-service,wt-metering-service,wt-notifications-service}
  --group {common,kbyg,user}
  --add-migration
  --remove-migration
  --debug
  --dry-run
```

### `get_parameters.py`

For each service, do the following:

1. Print all of its secret names and values from Parameter Store

```console
python get_parameters.py --help

usage: get_parameters.py [-h] [--environment {sandbox,staging,prod}]
                         [--service {sl-onboarding-api,sl-sessions-api,sl-cameras-service,sl-forecasts-api,sl-geo-target-service,sl-kbyg-product-api,sl-reports-api,sl-spots-api,wt-charts-service,sl-auth-service,sl-entitlements-service,sl-favorites-api,sl-promotions-service,sl-subscription-service,sl-user-service,wt-metering-service,wt-notifications-service} | --group {common,kbyg,user}]
                         [--debug] [--dry-run]

optional arguments:
  -h, --help            show this help message and exit
  --environment {sandbox,staging,prod}
  --service {sl-onboarding-api,sl-sessions-api,sl-cameras-service,sl-forecasts-api,sl-geo-target-service,sl-kbyg-product-api,sl-reports-api,sl-spots-api,wt-charts-service,sl-auth-service,sl-entitlements-service,sl-favorites-api,sl-promotions-service,sl-subscription-service,sl-user-service,wt-metering-service,wt-notifications-service}
  --group {common,kbyg,user}
  --debug
  --dry-run
```

### `update_services.py`

For each service, do the following:

1. Update the service in ECS, forcing redeployment

```console
python update_services.py --help

usage: update_services.py [-h] [--environment {sandbox,staging,prod}]
                          [--service {sl-onboarding-api,sl-sessions-api,sl-cameras-service,sl-forecasts-api,sl-geo-target-service,sl-kbyg-product-api,sl-reports-api,sl-spots-api,wt-charts-service,sl-auth-service,sl-entitlements-service,sl-favorites-api,sl-promotions-service,sl-subscription-service,sl-user-service,wt-metering-service,wt-notifications-service} | --group {common,kbyg,user}]
                          [--debug] [--dry-run]

optional arguments:
  -h, --help            show this help message and exit
  --environment {sandbox,staging,prod}
  --service {sl-onboarding-api,sl-sessions-api,sl-cameras-service,sl-forecasts-api,sl-geo-target-service,sl-kbyg-product-api,sl-reports-api,sl-spots-api,wt-charts-service,sl-auth-service,sl-entitlements-service,sl-favorites-api,sl-promotions-service,sl-subscription-service,sl-user-service,wt-metering-service,wt-notifications-service}
  --group {common,kbyg,user}
  --debug
  --dry-run
```

### `validate-services.py`

For each service, do the following:

1. Execute a validation request and confirm that it returns status code `200`

```console
python validate_services.py --help

usage: validate_services.py [-h] [--environment {sandbox,staging,prod}]
                            [--debug] [--dry-run]

optional arguments:
  -h, --help            show this help message and exit
  --environment {sandbox,staging,prod}
  --debug
  --dry-run
```
