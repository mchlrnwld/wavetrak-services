import argparse
import logging
import pprint

import boto3

from services import GROUPS, SERVICE_GROUPS, SERVICE_PREFIXES

CLUSTER_PREFIX = "sl-core-svc"

parser = argparse.ArgumentParser()
parser.add_argument(
    "--environment", default="sandbox", choices=["sandbox", "staging", "prod"]
)

service_group = parser.add_mutually_exclusive_group()
service_group.add_argument(
    "--service", default="all", choices=SERVICE_PREFIXES
)
service_group.add_argument("--group", default="all", choices=GROUPS)

parser.add_argument("--debug", action="store_true")
parser.add_argument("--dry-run", action="store_true")

args = parser.parse_args()

logging.basicConfig(level=(logging.DEBUG if args.debug else logging.INFO))
logging.getLogger("boto3").setLevel(logging.ERROR)
logging.getLogger("botocore").setLevel(logging.ERROR)
logging.getLogger("urllib3").setLevel(logging.ERROR)
logger = logging.getLogger()

logger.debug(f"Args: {args}")

ecs = boto3.client("ecs")

# Build list of services to update
services_to_update = (
    SERVICE_PREFIXES
    if args.service == "all" and args.group == "all"
    else [args.service]
    if args.service != "all" and args.group == "all"
    else SERVICE_GROUPS[args.group]
)

logger.debug(f"Services to update: {services_to_update}")

for service_prefix in services_to_update:
    if not args.dry_run:
        update_service_response = ecs.update_service(
            cluster=f"{CLUSTER_PREFIX}-{args.environment}",
            service=f"{service_prefix}-{args.environment}",
            forceNewDeployment=True,
        )

        logger.info(
            f"Service {service_prefix}-{args.environment} updated. "
            "It may take a few minutes for task deployment to complete."
        )

        logger.debug(
            "Update Service Response: "
            f"{pprint.pformat(update_service_response)}"
        )
    else:
        logger.info(
            f"Service {service_prefix}-{args.environment} not updated due to "
            "--dry-run flag"
        )
