import argparse
import logging
import pprint

import boto3

from services import GROUPS, SERVICE_GROUPS, SERVICE_PREFIXES

CLUSTER_PREFIX = "sl-core-svc"

parser = argparse.ArgumentParser()
parser.add_argument(
    "--environment", default="sandbox", choices=["sandbox", "staging", "prod"]
)

service_group = parser.add_mutually_exclusive_group()
service_group.add_argument(
    "--service", default="all", choices=SERVICE_PREFIXES
)
service_group.add_argument("--group", default="all", choices=GROUPS)

action_group = parser.add_mutually_exclusive_group()
action_group.add_argument("--add-migration", action="store_true", default=True)
action_group.add_argument(
    "--remove-migration", action="store_true", default=False
)

parser.add_argument("--debug", action="store_true")
parser.add_argument("--dry-run", action="store_true")

args = parser.parse_args()

logging.basicConfig(level=(logging.DEBUG if args.debug else logging.INFO))
logging.getLogger("boto3").setLevel(logging.ERROR)
logging.getLogger("botocore").setLevel(logging.ERROR)
logging.getLogger("urllib3").setLevel(logging.ERROR)
logger = logging.getLogger()

logger.debug(f"Args: {pprint.pformat(args)}")

ecs = boto3.client("ecs")

# Build list of services to update
services_to_update = (
    SERVICE_PREFIXES
    if args.service == "all" and args.group == "all"
    else [args.service]
    if args.service != "all" and args.group == "all"
    else SERVICE_GROUPS[args.group]
)

logger.debug(f"Services to update: {services_to_update}")

for service_prefix in services_to_update:
    # Get current task definition arn
    service_desc_response = ecs.describe_services(
        cluster=f"{CLUSTER_PREFIX}-{args.environment}",
        services=[f"{service_prefix}-{args.environment}"],
    )
    task_def_arn = service_desc_response["services"][0]["taskDefinition"]

    # Get current task def
    task_def_response = ecs.describe_task_definition(
        taskDefinition=task_def_arn
    )
    task_def = task_def_response["taskDefinition"]
    logger.debug(f"Current Task Definition: {pprint.pformat(task_def)}")

    # Build new task definition
    container_def = task_def["containerDefinitions"][0]

    # Update MongoDB connection string refs
    for index, secret in enumerate(container_def["secrets"]):
        # Support adding or removing `_MIGRATION` from parameter store ref
        if secret["name"].startswith("MONGO_CONNECTION_STRING"):
            if args.remove_migration:
                if secret["valueFrom"].endswith("_MIGRATION"):
                    # Remove `_MIGRATION` from the end of the string
                    secret["valueFrom"] = secret["valueFrom"][0:-10]
                    container_def["secrets"][index] = secret

            # Use `elif` rather than `if` because add_migration defaults True
            elif args.add_migration:
                if not secret["valueFrom"].endswith("_MIGRATION"):
                    # Append `_MIGRATION` to the end of the string
                    secret["valueFrom"] += "_MIGRATION"
                    container_def["secrets"][index] = secret

    logger.debug(
        f"Updated Container Definition: {pprint.pformat(container_def)}"
    )

    # Push updated task def to AWS
    if not args.dry_run:
        updated_task_def_response = ecs.register_task_definition(
            containerDefinitions=[container_def],
            executionRoleArn=task_def["executionRoleArn"],
            family=task_def["family"],
            placementConstraints=task_def["placementConstraints"],
            requiresCompatibilities=task_def["requiresCompatibilities"],
            taskRoleArn=task_def["taskRoleArn"],
            volumes=task_def["volumes"],
        )
        updated_task_def_arn = updated_task_def_response["taskDefinition"][
            "taskDefinitionArn"
        ]

        logger.debug(
            "Updated Task Definition: "
            f"{pprint.pformat(updated_task_def_response)}"
        )

        # Output task def refs
        print(f"{service_prefix}-{args.environment}|{updated_task_def_arn}")
