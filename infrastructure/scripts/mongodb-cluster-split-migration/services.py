# This could be DRY'er but ¯\_(ツ)_/¯

PARAMETERS = {
    "sl-auth-service": "/{env}/services/auth-service/MONGO_CONNECTION_STRING",
    "sl-cameras-service": "/{env}/services/cameras-service/MONGO_CONNECTION_STRING_KBYG",
    "sl-entitlements-service": "/{env}/services/entitlements-service/MONGO_CONNECTION_STRING",
    "sl-favorites-api": "/{env}/services/favorites-api/MONGO_CONNECTION_STRING",
    "sl-forecasts-api": "/{env}/services/forecasts-api/MONGO_CONNECTION_STRING_KBYG",
    "sl-geo-target-service": "/{env}/services/geo-target-service/MONGO_CONNECTION_STRING_KBYG",
    "sl-kbyg-product-api": "/{env}/services/kbyg-product-api/MONGO_CONNECTION_STRING_KBYG",
    "sl-onboarding-api": "/{env}/services/onboarding-api/MONGO_CONNECTION_STRING",
    "sl-promotions-service": "/{env}/services/promotions-service/MONGO_CONNECTION_STRING",
    "sl-reports-api": "/{env}/services/reports-api/MONGO_CONNECTION_STRING_KBYG",
    "sl-sessions-api": "/{env}/services/sessions-api/MONGO_CONNECTION_STRING_SESSIONS",
    "sl-spots-api": "/{env}/services/spots-api/MONGO_CONNECTION_STRING_KBYG",
    "sl-subscription-service": "/{env}/services/subscription-service/MONGO_CONNECTION_STRING",
    "sl-user-service": "/{env}/services/user-service/MONGO_CONNECTION_STRING",
    "wt-charts-service": "/{env}/services/charts-service/MONGO_CONNECTION_STRING_KBYG",
    "wt-metering-service": "/{env}/services/metering-service/MONGO_CONNECTION_STRING",
    "wt-notifications-service": "/{env}/services/notifications-service/MONGO_CONNECTION_STRING",
}

GROUPS = ["common", "kbyg", "user"]

SERVICE_GROUPS = {
    "common": ["sl-onboarding-api", "sl-sessions-api"],
    "kbyg": [
        "sl-cameras-service",
        "sl-forecasts-api",
        "sl-geo-target-service",
        "sl-kbyg-product-api",
        "sl-reports-api",
        "sl-spots-api",
        "wt-charts-service",
    ],
    "user": [
        "sl-auth-service",
        "sl-entitlements-service",
        "sl-favorites-api",
        "sl-onboarding-api",
        "sl-promotions-service",
        "sl-subscription-service",
        "sl-user-service",
        "wt-metering-service",
        "wt-notifications-service",
    ],
}

SERVICE_PREFIXES = [
    "sl-onboarding-api",
    "sl-sessions-api",
    "sl-cameras-service",
    "sl-forecasts-api",
    "sl-geo-target-service",
    "sl-kbyg-product-api",
    "sl-reports-api",
    "sl-spots-api",
    "wt-charts-service",
    "sl-auth-service",
    "sl-entitlements-service",
    "sl-favorites-api",
    "sl-promotions-service",
    "sl-subscription-service",
    "sl-user-service",
    "wt-metering-service",
    "wt-notifications-service",
]
