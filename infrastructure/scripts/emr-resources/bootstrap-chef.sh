#!/bin/bash
# bootstrap-chef.sh
# Usage: bootstrap-chef.sh
# 1.1.0

# User Variables
DRYRUN="false" # true | false

# --- YOU SHOULD NOT NEED TO EDIT ANYTHING BELOW THIS LINE --- #

# System Variables
REGION=$(/usr/bin/curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | grep region | awk -F\" '{ print $4 }')
INSTANCE_ID=$(ec2-metadata -i | sed 's/\instance-id: //g')
INSTANCE_ENV=$1

# Functions
get_tag_value() {
  [[ -z $2 ]] && CUT_NUMBER="5" || CUT_NUMBER=$2
  aws --region $REGION ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$1" --output=text | cut -f$CUT_NUMBER
}

# Install Chef
if [[ $DRYRUN == "false" ]]; then
  sudo mkdir -p /var/log/chef /etc/chef
  sudo curl -L https://www.chef.io/chef/install.sh | sudo bash -s -- -v 12.4
fi

# Set Recipes Tag
COMPANY="sl"
INSTANCE_APP="bestwinds"
INSTANCE_SERVICE="emr"
EMR_ROLE=$(get_tag_value aws:elasticmapreduce:instance-group-role | awk '{ print tolower($0) }')
CLUSTER_ID=$(get_tag_value aws:elasticmapreduce:job-flow-id)
CHEF_ROLE=$COMPANY-$INSTANCE_APP-$INSTANCE_SERVICE-$EMR_ROLE

# Tag Instance Recipes
[[ $DRYRUN == "false" ]] && sudo /usr/bin/aws --region $REGION ec2 create-tags --resources $INSTANCE_ID --tags Key=recipes,Value=\"role[base],role[$CHEF_ROLE]\"

# Pull Chef Config
[[ $DRYRUN == "false" ]] && sudo /usr/bin/aws --region $REGION s3 sync s3://sl-chef-$INSTANCE_ENV/config /etc/chef/

[[ $EMR_ROLE == "core" ]] && NAME_SUFFIX="-$INSTANCE_ID"

# Tag Instance Name
sudo /usr/bin/aws --region $REGION ec2 create-tags --resources $INSTANCE_ID --tags Key=Name,Value=$INSTANCE_ENV-$INSTANCE_APP-$INSTANCE_SERVICE-$EMR_ROLE$NAME_SUFFIX

# Run Chef
[[ $DRYRUN == "false" ]] && sudo chef-client -E $INSTANCE_ENV -N $(get_tag_value Name) -r $(get_tag_value recipes)

# Dry Run
if [[ $DRYRUN == "true" ]]; then
  echo -e "\e[35m"
  echo "--- DRY RUN ---"
  echo -e "\e[93m"
  echo "This script requires the following packages: aws-cli, curl, ec2-utils"
  echo -e "\e[95m"
  echo "CHEF_ROLE is $CHEF_ROLE (role[base],role[$CHEF_ROLE])"
  echo "EMR_ROLE is $EMR_ROLE"
  echo "COMPANY is $COMPANY"
  echo "INSTANCE_APP is $INSTANCE_APP"
  echo "INSTANCE_SERVICE is $INSTANCE_SERVICE"
  echo "REGION is $REGION"
  echo "INSTANCE_ID is $INSTANCE_ID"
  echo -e "\e[35m"
  echo "--- DRY RUN ---"
  echo -e "\e[39m"
fi

exit 0
