#!/bin/env python

# Standard libraries.
import argparse
import json
import logging
import os
import sys
import xml.etree.ElementTree as xmldoc

# Third-party libraries.
import boto3
from botocore.exceptions import ClientError, NoRegionError
import pandas as pd
import yaml


# The configuration filename and location.
CONFIG = './.config.yml'

# The global content of the configuration file.
config = {}


# Main function.
def execute():
    global config

    with open(CONFIG) as content:
        config = yaml.full_load(content)

    try:
        args = parse_arguments()

        data = filter_data(args)
        if not data or not len(data):
            raise ValueError

        parameters = get_parameters(data, args['env'])
        if not len(parameters):
            raise ValueError

        stored = get_stored(parameters, args['env'])
        print(
            '> Parameters:\n'
            '  {} in files, {} total, {} already saved, {} missing.'
            .format(
                len(parameters),
                len(stored),
                len([x for x in stored if x['date'] != '']),
                len([x for x in stored if x['date'] == ''])))

        create_report(parameters, stored, args['env'])

    except ValueError as err:
        logging.error(
            ('No parameters match the criteria.', err)
            [not err])

    except NoRegionError:
        logging.error('AWS profile not set correctly.')

    except (KeyboardInterrupt, StopIteration):
        print('\n\n  Execution aborted.')

    sys.exit(1)


# Handles the script arguments and options validation.
def parse_arguments():
    ap = argparse.ArgumentParser(
        description='''
            A script to check if values in Jenkins managed files are consistent
            among them and their values in AWS Systems Manager Parameter Store.
        ''',
    )
    ap.add_argument(
        '--env',
        help='environment to check secrets for',
        required=True,
    )
    ap.add_argument(
        '--debug',
        help='change logging level to debug',
        action='store_true',
    )
    return vars(ap.parse_args())


# Validates all arguments to match with each other and creates an object for
# all of the entities involved in the execution.
def filter_data(args):
    data = []

    for job_name in config['jobs']:
        job = get_job_details(job_name, args['env'])
        if job:
            data.append(job)

    logging.debug(
        'data = {}'
        .format(json.dumps(data, indent=4)))
    return data


# Browses a Jenkins job config file and retrieves its details.
# The XML document was validated in a previous procedure.
def get_job_details(job_name, env):
    job = config['jobs'].get(job_name)

    conf = xmldoc.parse(job['config'])
    root = conf.getroot()

    # Job parameters.
    node = 'hudson.model.ChoiceParameterDefinition'
    defs = [
        'BUILD_FUNCTION',
        'BUILD_ENVIRONMENT',
        'CONTAINER_PATH',
        'ENVIRONMENT',
        'MODULE_PATH',
        'SERVICE',
    ]
    excl = '---'
    services = None
    environment = None
    for prop in root.iter(node):
        name = prop.find('name')
        if name.text in defs:
            choices = prop.find('choices').find('a').findall('string')
            if name.text in ['ENVIRONMENT', 'BUILD_ENVIRONMENT']:
                environment = len([x for x in choices if env in x.text])
            else:
                services = [x.text for x in choices if excl not in x.text]

    if not environment:
        return

    # Managed files reference.
    node = 'org.jenkinsci.plugins.configfiles.buildwrapper.ManagedFile'
    file = None
    for managed in root.iter(node):
        target = managed.find('targetLocation').text.replace('.env', '')
        if target == env:
            file = managed.find('fileId').text
            continue

    job.update({
        'services': services,
        'file': file,
    })

    logging.debug(
        'data = {}'
        .format(json.dumps(job, indent=4)))

    return job


# Reads the managed config file to retrieve job secret values from its file ID.
# It assumes that secrets are json-formatted, otherwise, ini-formatted.
# The XML document was validated in a previous procedure.
def get_job_secrets(managed_file):
    secrets = []

    conf = xmldoc.parse(config['jenkins']['config'])
    root = conf.getroot()

    try:
        node = root.find('.//*[id="{}"]'.format(managed_file))
        data = node.find('content').text

        # JSON format.
        secrets = json.loads(data)

    except json.decoder.JSONDecodeError:
        # INI format.
        configs = [
            x.strip().split('=', 1)
            for x in data.split('\n')
            if x and not x.startswith('#')
        ]
        secrets = [
            {'name': key, 'value': val.strip('"\'')}
            for key, val in dict(configs).items()
        ]

    except Exception:
        logging.debug(
            'Secrets structure invalid in managed file: "{}"'
            .format(managed_file))

    logging.debug(
        'file = {}'
        .format(managed_file))

    logging.debug(
        'data = {}'
        .format(json.dumps(secrets, indent=4)))

    return secrets


# Retrieves a complete list of parameters according to the arguments passed.
def get_parameters(data, env):
    parameters = []

    for job in data:
        secrets = get_job_secrets(job['file'])

        for service in job['services']:

            # Look for the service/function's 'ci.json' file.
            ci_json = '{}/{}'.format(
                job['workspace'],
                job['secrets'].replace('{service}', service)
            )
            if not os.path.exists(ci_json):
                logging.debug(
                    'File not found: {}'
                    .format(ci_json))
                continue

            with open(ci_json, 'r') as ci_json_file:
                content = json.load(ci_json_file)
                blocks = content.get('secrets')

                if not blocks:
                    logging.debug(
                        'No secrets found in file: "{}"'
                        .format(ci_json))
                    continue

                logging.debug(
                    'Parsing secrets from file: "{}"'
                    .format(ci_json))

                for block in blocks.keys():
                    if block not in ['common', 'service']:
                        continue

                    group = blocks[block]
                    prefix = (None, group['prefix'])['prefix' in group]
                    params = (None, group['secrets'])['secrets' in group]

                    if not prefix and not params:
                        logging.debug(
                            'Secrets structure invalid in file: "{}"'
                            .format(ci_json))
                        continue

                    for param in params:

                        value = ''
                        found = [
                            k for k in secrets if k['name'] == param
                        ]

                        if len(found):
                            value = found[0]['value'].replace('{env}', env)

                        # Flag as 'ignore' if the parameter is an empty
                        # string, since AWS Parameter Store won't allow
                        # us to save a blank secret.
                        parameters.append({
                            'name': '{}{}'.format(
                                prefix.replace('{env}', env),
                                param),
                            'json': '{}/{}'.format(
                                job['repository'],
                                job['secrets'].replace('{service}', service)),
                            'file': job['file'],
                            'value': value,
                        })

    logging.debug(
        'data = {}'
        .format(json.dumps(parameters, indent=4)))

    return parameters


# Retrieves parameter values from AWS Parameter Store.
def get_stored(parameters, env):
    data = []
    already = []

    file = 'stored-{}.json'.format(env)
    if os.path.exists(file):
        logging.debug(
            'Retrieving parameters from file: {}'
            .format(file))

        with open(file, 'r') as f:
            data = json.load(f)
            already = [x['name'] for x in data]
            f.close()

    ssm = boto3.client('ssm')
    for param in parameters:

        if param['name'] in already:
            continue

        secret = get_secret(ssm, param['name'])
        if secret:
            data.append(secret)
        else:
            data.append({
                'name': param['name'],
                'value': '',
                'date': '',
            })

        already.append(param['name'])

    with open(file, 'w') as f:
        f.write(json.dumps(data, indent=4))
        f.close()

    logging.debug(
        'data = {}'
        .format(json.dumps(data, indent=4)))

    return data


# Retrieves the parameter store data from AWS.
# The script assumes all secrets need a KMS key.
def get_secret(ssm, parameter):
    try:
        secret = ssm.get_parameter(
            Name=parameter,
            WithDecryption=True
        )
        logging.info(
            'Parameter found: "{}".'
            .format(parameter))

        data = {
            'name': secret['Parameter']['Name'],
            'value': secret['Parameter']['Value'],
            'date': secret['Parameter']['LastModifiedDate'].strftime(
                '%Y-%m-%d %H:%M:%S'
            ),
        }
        logging.debug(
            'data = {}'
            .format(json.dumps(data, indent=4)))

        return data

    except ClientError as err:
        code = err.response['Error']['Code']

        if code == 'ParameterNotFound':
            logging.info(
                'Parameter not found: "{}".'
                .format(parameter))

        elif code == 'UnrecognizedClientException':
            logging.error('AWS credentials are incorrect.')
            sys.exit(1)

        else:
            logging.error(err)
            sys.exit(1)


# Generates a report file with the merged data retrieved from managed files
# and values from AWS Parameter Store.
def create_report(parameters, stored, env):
    data = []

    for p in parameters:
        data.append({
            **p,
            **{'saved': x['value'] for x in stored if x['name'] == p['name']}})

    logging.debug(
        'data = {}'
        .format(json.dumps(data, indent=4)))

    try:
        file = 'report-{}.json'.format(env)
        exported = 'report-{}.csv'.format(env)

        with open(file, 'w') as f:
            f.write(json.dumps(data, indent=4))
            f.close()

        report = pd.read_json(file)
        report.sort_values(by='name').to_csv(exported, index=False)

    except Exception as err:
        logging.error(err)
        sys.exit(1)


# Execute script.
if __name__ == '__main__':
    logging.basicConfig(
        format='{}'.format((
            '[%(levelname)s] %(message)s',
            '[%(levelname)s] %(funcName)s - %(message)s'
        )['--debug' in sys.argv]),
        level=(
            logging.WARNING,
            logging.DEBUG
        )['--debug' in sys.argv],
    )
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    execute()
