# Secrets Migration

A migration script to move secrets from a Jenkins instance to AWS Systems Manager Parameter Store ([INF-311]).

- [Summary](#summary)
- [Installation](#installation)
- [Usage](#usage)
- [Use cases](#use-cases)
- [Requirements](#requirements)
  - [Configuration file](#configuration-file)
  - [AWS profiles](#aws-profiles)
- [Debugging](#debugging)
- [Uninstallation](#uninstallation)
- [What's next](#whats-next)

## Summary

This script will build a stateless list of secrets and environment credentials belonging to one or more of the services passed by argument, to then create or update the secrets and their values into AWS Systems Manager Parameter Store.

It will fetch their values from a Jenkins global config file according to the content of the job files, their services and their environments.

## Installation

This script is intended to run in a Conda environment, so having Anaconda or Miniconda installed, you'll need to run the next commands:

```console
conda env create -f environment.yml
conda activate secrets-migration
```

## Usage

```bash
python migrate_secrets.py \
  --env ENVIRONMENT \
  [--job JOB_NAME] \
  [--repo REPOSITORY] \
  [--service SERVICE_NAME] \
  [--dry-run] \
  [--debug]
```

You can send multiple services, repositories and job names in the same call, but the execution will depend on how the entities are linked with each other. If you send a job and a service, and the service doesn't belong to the Jenkins job, the program will be interrupted.

**Example:**

```bash
python migrate_secrets.py --env staging --job web --service account --service homepage
```

All arguments except `--env` are optional and they act as filters.

Not sending any service, repository or job argument, will assume all of them are available for execution.

## Use cases

Jobs and repositories can cancel each other. You can have a repository in several Jenkins jobs but you don't have jobs with more than one repository. So if you send the `--job` argument, the `--repo` argument should not be needed. But instead, sending a `--repo` argument configured in multiple jobs, you can restrict which of the jobs you want to execute.

If a parameter is found and its value needs to be updated, a confirmation query will be prompted asking the user if it's safe to proceed with the corresponding update.

Sending the `--dry-run` argument will only show the total of parameters found, which of them will be created and which of them need to be updated. Both arguments cannot be sent in the same execution.

Adding the `--debug` argument will output all information gathered from Jenkins jobs, secret values and all of the data retrieved from AWS Parameter Store.

**Examples:**

```bash
python migrate_secrets.py --env staging --job web --repo surfline-web
```

This will work!

```bash
python migrate_secrets.py --env staging --job services --repo wavetrak-services
```

This will work! The `wavetrak-services` repository is present in `services` and `editorial` jobs. So if you want to only execute the `services` job you wouldn't need to pass the repository. If you whish to execute all jobs for the repository, don't add the job.

```bash
python migrate_secrets.py --env staging --job admin --repo wavetrak-services
```

This will not work! The `admin` job is not configured to make use of the `wavetrak-services` repository.

```bash
python migrate_secrets.py --env integration --job admin --job editorial
```

This will work! The `integration` environment is only present in `editorial` job but it doesn't in `admin`. The execution will run on valid scenarios.

## Requirements

This script is intended to run in the Jenkins instance where credentials and jobs configuration files are being stored. But, as you don't actually need to have access to the Jenkins application since the script is not calling any API endpoint or making use of any database, you could have a local copy of those files (trying not to expose or create any security issue) or a minimal version of them, allowing you to execute the script from your host machine and having the exactly same results, since all the magic actually happens in AWS.

### Configuration file

You need to configure a few file and folder locations.

Copy the content of the `.config-example.yml` file provided along with this script, into a new file named `.config.xml` and do the changes there.

**Example:**

```yaml
# .config.yml
jenkins:
  config: "config/org.jenkinsci.plugins.configfiles.GlobalConfigFiles.xml"
jobs:
  admin:
    config: "jobs/admin.jobs.deploy-container.config.xml"
    repository: "surfline-admin"
    secrets: "{service}/ci.json"
    workspace: "/home/user/workspace/surfline-admin"
  services:
    config: "jobs/surfline.jobs.Web.jobs.deploy-service-to-ecs.config.xml"
    repository: "wavetrak-services"
    secrets: "services/{service}/ci.json"
    workspace: "/home/user/workspace/wavetrak-services"
  ...
```

### AWS profiles

The script assumes it will be running in a environment where AWS profiles are configured and ready to use.

When the environment is sent with the `--env` argument, a profile should be already set to have access to the corresponding account. You would need to execute the `aws sts assume-role` command before running the script.

It's not in the scope of this script to describe how to configure the AWS credentials, but your files should look like this:

**Example:**

```bash
# ~/.aws/credentials
[surfline-prod]
aws_access_key_id = ************
aws_secret_access_key = ************
region = us-west-1

[surfline-dev]
aws_access_key_id = ************
aws_secret_access_key = ************
region = us-west-1
```

```bash
# ~/.aws/config
[profile surfline-prod]
region = us-west-1
format = json

[profile surfline-dev]
region = us-west-1
format = json
```

## Debugging

The script will print to `stdout` the status of the process.

You can change the debugging `level` option to print more output data from the `logging.basicConfig` object.

## Uninstallation

Running the `env remove` command will be enough to delete all Conda environment files, but first you'll want to deactivate it:

```console
conda deactivate
conda env remove -n secrets-migration
```

## What's next

If you want to add a Jenkins job to the list of available processes, you just need to modify the `.config.yml` file and set the right values.

```yaml
# .config.yml
  ...
  editorial:
    config: "jobs/surfline.jobs.Web.jobs.deploy-editorial-to-ecs.config.xml"
    repository: "wavetrak-services"
    secrets: "services/{service}/ci.json"
    workspace: "/home/user/workspace/wavetrak-services"
```

[INF-311]: https://wavetrak.atlassian.net/browse/INF-311
