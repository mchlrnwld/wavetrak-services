#!/bin/env python

# Standard libraries.
import argparse
import json
import logging
import os
import sys
import xml.etree.ElementTree as xmldoc

# Third-party libraries.
import boto3
from botocore.exceptions import ClientError, NoRegionError
import yaml


# The configuration filename and location.
CONFIG = './.config.yml'

# A symbol to output according to the possible actions for a parameter.
ACTIONS = {
    'create': '+',
    'update': '~',
    'ignore': ' ',
}

# The global content of the configuration file.
config = {}

# Temporary lists of KMS keys retrieved by alias.
# The purpose of this variables is to reduce the AWS calls to the minimum.
kms_keys = {}
kms_keys_not_found = []


# Main function.
def execute():
    # Validations.
    try:
        global config

        # Minimum arguments.
        if not len(sys.argv) > 1:
            raise SystemError

        # Main config file.
        if not os.path.exists(CONFIG):
            raise FileNotFoundError(CONFIG)

        with open(CONFIG) as content:
            config = yaml.full_load(content)

        # Jenkins configuration.
        if not config.get('jenkins') or not config['jenkins'].get('config'):
            raise ValueError('jenkins')

        jenkins = config['jenkins']['config']
        if not os.path.exists(jenkins):
            raise FileNotFoundError(jenkins)

        xmldoc.parse(jenkins)

        # Jobs configuration.
        if not config.get('jobs') or not len(config['jobs']):
            raise ValueError('jobs')

        jobs = config['jobs']
        for key in jobs:
            conf = jobs[key]['config']
            if not os.path.exists(conf):
                raise FileNotFoundError(conf)

            xmldoc.parse(conf)

    except SystemError:
        logging.error(
            'Script won\'t run without any arguments.\n')

    except FileNotFoundError as file:
        logging.error(
            'File not found: "{}". '
            .format(file))

    except ValueError as val:
        logging.error(
            'Configuration error: {} in "{}". '
            .format(val, CONFIG))

    except yaml.scanner.ScannerError:
        logging.error(
            'Error reading YAML file: "{}". '
            .format(CONFIG))

    except xmldoc.ParseError:
        logging.error(
            'Error reading XML file: "{}". '
            .format((conf, jenkins)[not conf]))

    # Process.
    try:
        args = parse_arguments()

        data = filter_data(args)
        if not data or not len(data):
            raise ValueError

        parameters = get_parameters(data)
        if not len(parameters):
            raise ValueError

        print(
            '> Parameters found = {}\n'
            '> Retrieving secrets from AWS Parameter Store...\n'
            .format(len(parameters)))
        planning = plan(parameters)

        if len([p for p in planning if p['action'] in ['create', 'update']]):

            # Report the actions to make.
            for item in planning:
                print(
                    '{} {:<10}: {}{}'
                    .format(
                        ACTIONS.get(item['action'], ACTIONS['ignore']),
                        item['action'],
                        item['prefix'],
                        item['name']))

            print(
                '\n> Actions:\n  '
                '{} to create, {} to update, {} unchanged, {} to be ignored.\n'
                .format(
                    len([x for x in planning if x['action'] == 'create']),
                    len([x for x in planning if x['action'] == 'update']),
                    len([x for x in planning if x['action'] == 'unchanged']),
                    len([x for x in planning if x['action'] == 'ignore'])))

            if not args['dry_run']:
                result = None

                print(
                    '> Please confirm you want to perform this actions.\n'
                    '  Only "yes" will be accepted to approve.\n\n'
                    '  Enter a value:', end=' ')
                confirm = input()

                if confirm.lower() == 'yes':
                    print('\n> Processing...')
                    result = apply(planning)
                else:
                    print('  Execution aborted.')

                if result:
                    print(
                        '\n> Execution results:'
                        '  {} successful, {} with errors, {} skipped.\n'
                        .format(
                            result['success'],
                            result['error'],
                            result['skipped']))

            print('  Done!')
        else:
            print('  No changes need to be made.')

        # Finished normally.
        sys.exit(0)

    except TypeError as job_name:
        logging.error(
            'Job not found: "{}"'
            .format(job_name))

    except ValueError as err:
        logging.error(
            ('No parameters match the criteria.', err)
            [not err])

    except NoRegionError:
        logging.error('AWS profile not set correctly.')

    except (KeyboardInterrupt, StopIteration):
        print('\n\n  Execution aborted.')

    # Something prevented the script from finishing normally.
    sys.exit(1)


# Handles the script arguments and options validation.
def parse_arguments():
    ap = argparse.ArgumentParser(
        description='''
            The migration script to create and update credentials from Jenkins
            configuration files into AWS Systems Manager Parameter Store paths.
        ''',
    )
    ap.add_argument(
        '--env',
        help='environment to migrate secrets for',
        action='append',
        required=True,
    )
    ap.add_argument(
        '--job',
        help='job to migrate secrets for',
        action='append',
    )
    ap.add_argument(
        '--repo',
        help='repository containing the service/s to migrate secrets for',
        action='append',
    )
    ap.add_argument(
        '--service',
        help='particular service slug name to migrate secrets for',
        action='append',
    )
    ap.add_argument(
        '--dry-run',
        help='show secrets to be created/updated without actually doing it',
        action='store_true',
    )
    ap.add_argument(
        '--debug',
        help='change logging level to debug',
        action='store_true',
    )
    return vars(ap.parse_args())


# Validates all arguments to match with each other and creates an object for
# all of the entities involved in the execution.
def filter_data(args):
    data = []

    if not args['job']:
        args['job'] = [job for job in config['jobs']]

    for job_name in args['job']:
        job = get_job_details(job_name)

        job['environments'] = [
            env for env in args['env']
            if env in job['environments']
        ]

        if args['service']:
            job['services'] = [
                serv for serv in args['service']
                if serv in job['services']
            ]

        if len(job['environments']) and len(job['services']):
            job['files'] = [
                file for file in job['files']
                if file['env'] in job['environments']
            ]
            data.append(job)

    logging.debug(
        'data = {}'
        .format(json.dumps(data, indent=4)))

    return data


# Browses a Jenkins job config file and retrieves its details.
# The XML document was validated in a previous procedure.
def get_job_details(job_name):
    job = config['jobs'].get(job_name)

    if not job:
        raise TypeError(job_name)

    conf = xmldoc.parse(job['config'])
    root = conf.getroot()

    # Job parameters.
    node = 'hudson.model.ChoiceParameterDefinition'
    defs = [
        'BUILD_FUNCTION',
        'BUILD_ENVIRONMENT',
        'CONTAINER_PATH',
        'ENVIRONMENT',
        'MODULE_PATH',
        'SERVICE',
    ]
    excl = '---'
    services = None
    environments = None
    for prop in root.iter(node):
        name = prop.find('name')
        if name.text in defs:
            choices = prop.find('choices').find('a').findall('string')
            if name.text in ['ENVIRONMENT', 'BUILD_ENVIRONMENT']:
                environments = [x.text for x in choices]
            else:
                services = [x.text for x in choices if excl not in x.text]

    # Managed files reference.
    node = 'org.jenkinsci.plugins.configfiles.buildwrapper.ManagedFile'
    files = []
    for managed in root.iter(node):
        target = managed.find('targetLocation').text.replace('.env', '')
        if target in environments:
            files.append({
                'id': managed.find('fileId').text,
                'env': target,
            })

    job.update({
        'environments': environments,
        'services': services,
        'files': files,
    })

    logging.debug(
        'data = {}'
        .format(json.dumps(job, indent=4)))

    return job


# Reads the managed config file to retrieve job secret values from its file ID.
# It assumes that secrets are json-formatted, otherwise, ini-formatted.
# The XML document was validated in a previous procedure.
def get_job_secrets(job_files):
    secrets = []

    conf = xmldoc.parse(config['jenkins']['config'])
    root = conf.getroot()

    for file in job_files:
        node = root.find('.//*[id="{}"]'.format(file['id']))
        data = node.find('content').text

        try:
            # JSON format.
            content = json.loads(data)
        except json.decoder.JSONDecodeError:
            try:
                # INI format.
                configs = [
                    x.strip().split('=', 1)
                    for x in data.split('\n')
                    if x and not x.startswith('#')
                ]
                content = [
                    {'name': key, 'value': val}
                    for key, val in dict(configs).items()
                ]
            except Exception:
                logging.debug(
                    'Secrets structure invalid in managed file: "{}"'
                    .format(file['id']))
                continue

        secrets.append({
            'env': file['env'],
            'items': content,
        })

    logging.debug(
        'data = {}'
        .format(json.dumps(secrets, indent=4)))

    return secrets


# Retrieves a complete list of parameters according to the arguments passed.
def get_parameters(data):
    parameters = []
    defaults = config.get('default-parameters').values()

    for job in data:
        secrets = get_job_secrets(job['files'])
        for service in job['services']:

            # Look for the service/function's 'ci.json' file.
            ci_json = '{}/{}'.format(
                job['workspace'],
                job['secrets'].replace('{service}', service)
            )
            if not os.path.exists(ci_json):
                logging.debug(
                    'File not found: {}'
                    .format(ci_json))
                continue

            with open(ci_json, 'r') as ci_json_file:
                content = json.load(ci_json_file)
                blocks = content.get('secrets')

                if not blocks:
                    logging.debug(
                        'No secrets found in file: "{}"'
                        .format(ci_json))
                    continue

                logging.debug(
                    'Parsing secrets from file: "{}"'
                    .format(ci_json))

                for block in blocks.keys():
                    if block not in ['common', 'service']:
                        continue

                    group = blocks[block]
                    prefix = (None, group['prefix'])['prefix' in group]
                    params = (None, group['secrets'])['secrets' in group]

                    if not prefix and not params:
                        logging.debug(
                            'Secrets structure invalid in file: "{}"'
                            .format(ci_json))
                        continue

                    for param in params:
                        for secret in secrets:

                            value = ''
                            found = False

                            # Look for the value matching the parameter name.
                            path = prefix.replace('{env}', secret['env'])
                            for item in secret['items']:
                                if item['name'] == param:
                                    value = item['value']
                                    found = True
                                    continue

                            if not found:

                                # Check if we have a default value for a param
                                # that wasn't found among available secrets.
                                default = [
                                    k for k in defaults if k['name'] == param
                                ]
                                if len(default):
                                    value = default[0]['value'].replace(
                                        '{env}',
                                        secret['env']
                                    )

                            # Flag as 'ignore' if the parameter is an empty
                            # string, since AWS Parameter Store won't allow
                            # us to save a blank secret.
                            parameters.append({
                                'env': secret['env'],
                                'name': param,
                                'prefix': path,
                                'value': value,
                                'ignore': (True, False)[value != ''],
                            })

    # Reduce parameters list by sorting and removing duplicates.
    parameters = sorted(
        [dict(t) for t in {tuple(d.items()) for d in parameters}],
        key=lambda p: (p['env'], p['prefix'], p['name'])
    )

    logging.debug(
        'data = {}'
        .format(json.dumps(parameters, indent=4)))

    return parameters


# Reports the parameters changes. It will iterate the whole list of parameters
# and call the AWS SSM service to retrieve their status and information.
def plan(parameters):
    plan = []

    ssm = boto3.client('ssm')
    for param in parameters:

        secret = get_secret(ssm, param)

        after = param['value'].strip('"\'')
        before = secret.get('value') if secret else ''

        action = 'unchanged'
        if param['ignore']:
            action = 'ignore'
        elif not secret:
            action = 'create'
        elif before != after:
            action = 'update'

        plan.append({
            'action': action,
            'env': param['env'],
            'name': param['name'],
            'prefix': param['prefix'],
            'before': before,
            'after': after,
        })

    logging.debug(
        'data = {}'
        .format(json.dumps(plan, indent=4)))

    return plan


# Execute changes.
def apply(plan):
    result = {
        'success': 0,
        'skipped': 0,
        'error': 0,
    }

    kms = boto3.client('kms')
    ssm = boto3.client('ssm')
    kms_mapping = config.get('kms-mapping').values()
    for item in plan:

        action = item['action']
        if action in ['create', 'update']:

            name = '{}{}'.format(item['prefix'], item['name'])
            alias = item['prefix'].strip('/')

            if alias not in kms_keys_not_found:
                key = alias.replace(item['env'], '{env}')

                # Check if kms key is different than the parameter's prefix.
                mapped = [k for k in kms_mapping if k['name'] == key]
                if len(mapped):
                    alias = mapped[0]['alias'].replace('{env}', item['env'])

                get_kms_key(kms, alias)
                if alias not in kms_keys:
                    result['error'] += 1
                    logging.warning(
                        'Can\'t {} "{}" with a non-existent key.'
                        .format(action, name))
                    continue

                logging.info(
                    '{}ing parameter "{}"'
                    .format(action[:-1].capitalize(), name))

                logging.debug(
                    'process = {}'
                    .format(json.dumps(item, indent=4)))

                value = item['after']
                if action == 'create':
                    confirm = 'yes'
                else:
                    # Update action needs confirmation.
                    print(
                        '\n> Are you sure do you want to update this value?\n'
                        '  Parameter: "{}"\n'
                        '  Old value: "{}"\n'
                        '  New value: "{}"\n\n'
                        '  Write "yes" if you approve:'
                        .format(name, item['before'], value), end=' ')
                    confirm = input()

                # Save.
                if confirm.lower() == 'yes':
                    if save_secret(ssm, name, value, alias):
                        result['success'] += 1
                    else:
                        result['error'] += 1
                else:
                    print('  Skipped.')
                    result['skipped'] += 1

            else:
                result['error'] += 1
                logging.warning(
                    'Can\'t {} "{}" with a non-existent key.'
                    .format(item['action'], name))

    logging.debug(
        'result = {}'
        .format(json.dumps(result, indent=4)))

    return result


# Retrieves the KMS key by alias.
def get_kms_key(kms, alias):
    try:
        if alias not in kms_keys_not_found and alias not in kms_keys:
            kms_key = kms.describe_key(
                KeyId='alias/{}'.format(alias),
            )
            logging.debug(
                'KMS key found: "{}".'
                .format(alias))
            kms_keys[alias] = kms_key['KeyMetadata']['KeyId']

            data = {
                'id': kms_key['KeyMetadata']['KeyId'],
                'alias': alias,
            }
            logging.debug(
                'data = {}'
                .format(json.dumps(data, indent=4)))

            return data

    except ClientError as err:
        code = err.response['Error']['Code']

        if code == 'NotFoundException':
            kms_keys_not_found.append(alias)
            logging.warning(
                'KMS key not found: "{}".'
                .format(alias))

        elif code == 'UnrecognizedClientException':
            logging.error('AWS credentials are incorrect.')
            sys.exit(1)

        else:
            logging.error(err)
            sys.exit(1)


# Retrieves the parameter store data from AWS.
# The script assumes all secrets need a KMS key.
def get_secret(ssm, parameter):
    try:
        secret = ssm.get_parameter(
            Name='{}{}'.format(
                parameter['prefix'].replace('{env}', parameter['env']),
                parameter['name'],
            ),
            WithDecryption=True
        )
        logging.info(
            'Parameter found: "{}{}".'
            .format(parameter['prefix'], parameter['name']))

        data = {
            'name': secret['Parameter']['Name'],
            'value': secret['Parameter']['Value'],
        }
        logging.debug(
            'data = {}'
            .format(json.dumps(data, indent=4)))

        return data

    except ClientError as err:
        code = err.response['Error']['Code']

        if code == 'ParameterNotFound':
            logging.info(
                'Parameter not found: "{}{}".'
                .format(parameter['prefix'], parameter['name']))

        elif code == 'UnrecognizedClientException':
            logging.error('AWS credentials are incorrect.')
            sys.exit(1)

        else:
            logging.error(err)
            sys.exit(1)


# Creates or updates parameter store.
def save_secret(ssm, name, value, key_alias):
    try:
        if value == '':
            raise ValueError

        save = ssm.put_parameter(
            Name=name,
            Value=value,
            KeyId=kms_keys[key_alias],
            Overwrite=True,
            Type='SecureString',
        )
        logging.debug(
            'response = {}'
            .format(json.dumps(save, indent=4)))

        return save

    except ValueError:
        logging.warning(
            'Parameter won\'t be saved with an empty value: "{}"'
            .format(name))

    except ClientError as err:
        code = err.response['Error']['Code']

        if code == 'AccessDeniedException':
            logging.warning(
                'User is not allowed to save parameter "{}".'
                .format(name))

        elif code == 'ValidationException':
            logging.warning(
                'Parameter could not be saved: "{}".'
                .format(name))

        else:
            logging.error(err)


# Execute script.
if __name__ == '__main__':
    logging.basicConfig(
        format='{}'.format((
            '[%(levelname)s] %(message)s',
            '[%(levelname)s] %(funcName)s - %(message)s'
        )['--debug' in sys.argv]),
        level=(
            logging.WARNING,
            logging.DEBUG
        )['--debug' in sys.argv],
    )
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    execute()
