#!/bin/bash

# Deploy Admin Service to ECS
# https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-admin-service-to-ecs/
#
# Repository: surfline-admin
# Parameters:
# - CONTAINER_PATH: [
#     cameras,
#     forecasts,
#     login,
#     reports,
#     search,
#     spots,
#     users
#   ]
# - BUILD_ENVIRONMENT: [
#     sandbox,
#     staging,
#     prod
#   ]
# - BUILD_TAG: master (default)
# - FORCE_REBUILD: true|false

# Set '-e' for strict mode to immediately interrupt the script if any
# of the command executions returns an exit status greater than zero.
# Set '-u' to exit if a variable is being called and doesn't exists.
# Set '-x' to output the command itself being called.
# Set '-o pipefail' prevents errors in a pipeline from being masked.
set -euxo pipefail

# Tag Jenkins job.
echo "build description:${CONTAINER_PATH}|${BUILD_ENVIRONMENT}|${BUILD_TAG}|"

# AWS configuration and path to assume-role.
AWS_PROFILE="${BUILD_ENVIRONMENT}"
if [[ "${BUILD_ENVIRONMENT}" == "sandbox" ]]; then
    AWS_PROFILE="dev"
fi
export AWS_REGION="us-west-1"
export AWS_DEFAULT_REGION="us-west-1"
export PATH=${PATH}:/usr/local/bin

cd "${WORKSPACE}/modules/${CONTAINER_PATH}"

# Service configuration.
APP_NAME="$(jq -r .serviceName ci.json)"
APP_VERSION="${GIT_COMMIT:0:7}"
IMAGE_REPO="$(jq -r .repository ci.json)"
IMAGE_NAME="$(jq -r .imageName ci.json)"
IMAGE_HOST="${IMAGE_REPO/\/${IMAGE_NAME}/}"
IMAGE_TAG="${BUILD_ENVIRONMENT}-${APP_VERSION}"
TASK_FAMILY="sl-admin-${APP_NAME}-service-${BUILD_ENVIRONMENT}"
ECS_CLUSTER="sl-core-svc-${BUILD_ENVIRONMENT}"
ECS_SERVICE="$(jq -r .[\""${APP_NAME}"\"] ../../docker/serviceMaps.json)-${BUILD_ENVIRONMENT}"
BUNDLE_SECRETS_SCRIPT="${JENKINS_HOME}/repos/wavetrak-infrastructure/scripts/jenkins-bundle-secrets/bundle_secrets.py"

# Use 'prod' profile to pull/push images from/to ECR.
eval "$(assume-role prod)"
eval "$(aws ecr get-login --no-include-email --region "${AWS_REGION}")"

# If you want to pull the Docker image to test locally with 'aws-cli'
# version > 1.18, you may run this command to login correctly:
#
# aws ecr
#     get-login-password \
#     --region ${AWS_REGION} | \
# docker \
#     login \
#     --username AWS \
#     --password-stdin ${IMAGE_HOST}

ECR_IMAGE_TAG=$(
    aws ecr \
        describe-images \
        --region "${AWS_REGION}" \
        --repository-name "${IMAGE_NAME}" | \
    jq -r ".imageDetails[].imageTags[]?" | \
    grep "${IMAGE_TAG}" \
    || echo ""
)
# Build image only if forced or if it doesn't exists already.
if [[ "${ECR_IMAGE_TAG}" == "" || "${FORCE_REBUILD}" == "true" ]]; then

    # Environment argument.
    APP_ENV="${BUILD_ENVIRONMENT}"
    if [ "${BUILD_ENVIRONMENT}" == "prod" ]; then
        APP_ENV="production"
    fi

    # New Relic argument.
    NEW_RELIC_APP_NAME="Surfline Admin Tools"
    if [ "${BUILD_ENVIRONMENT}" != "prod" ]; then
        NEW_RELIC_APP_NAME="Surfline Admin Tools (${BUILD_ENVIRONMENT^})"
    fi

    # Download '.npmrc' file for environment.
    eval "$(assume-role "${BUILD_ENVIRONMENT}")"
    aws secretsmanager \
        get-secret-value \
        --secret-id "${BUILD_ENVIRONMENT}/deployment/npm/npmrc" \
        --region "${AWS_REGION}" | \
    jq .SecretString -r \
    > .npmrc

    # Run the tests.
    make test

    # Build and tag the image.
    docker build \
        -t "${IMAGE_NAME}" \
        --build-arg APP_ENV="${APP_ENV}" \
        --build-arg APP_VERSION="${APP_VERSION}" \
        --build-arg NEW_RELIC_APP_NAME="${NEW_RELIC_APP_NAME}" \
        --tag "${IMAGE_REPO}:${IMAGE_TAG}" \
        --tag "${IMAGE_REPO}:build-cache" \
        .

    # Push it to ECR.
    docker push "${IMAGE_REPO}:${IMAGE_TAG}"
    docker push "${IMAGE_REPO}:build-cache"
else
    # Pull the image locally.
    docker pull "${IMAGE_REPO}:${IMAGE_TAG}"
fi

# Assume role based on profile environment back again.
eval "$(assume-role "${AWS_PROFILE}")"

# Bundle secrets.
python \
    "${BUNDLE_SECRETS_SCRIPT}" \
    --ci-json "./ci.json" \
    --env "${BUILD_ENVIRONMENT}" > ".env"
jq --raw-input 'split("=") | {name:(.[0]), value:(.[1])}' ".env" | \
    jq -s . \
    > ".env.json"

# Build task definition from a docker-compose file.
# The 'container-transform' is a small utility to transform various docker
# container formats to one another:
# https://github.com/micahhausler/container-transform
docker pull micahhausler/container-transform:latest

# Configuration retrieved into ECS container definition format.
docker run --rm -i micahhausler/container-transform < "${WORKSPACE}/docker/${APP_NAME}.yml" | \
    jq ".containerDefinitions[].environment = $(cat .env.json)" | \
    sed -e "s/\${REPOSITORY_HOST}/${IMAGE_HOST}/g" | \
    sed -e "s/\${IMAGE_TAG}/${IMAGE_TAG}/g" | \
    sed -e "s/\${ENVIRONMENT}/${BUILD_ENVIRONMENT}/g" \
    > "container-definitions.json"
cat "container-definitions.json"

# Create a new task definition version.
aws ecs \
    register-task-definition \
    --cli-input-json "file://$(pwd)/container-definitions.json" \
    --family "${TASK_FAMILY}" \
    --region "${AWS_REGION}"

# Name of the task definition.
TASK_DEFINITION=$(
    aws ecs \
        list-task-definitions \
        --region "${AWS_REGION}" \
        --family-prefix "${TASK_FAMILY}" | \
    jq -r '.taskDefinitionArns[-1:][]'
)

# Update the ECS service with corresponding the task definition.
aws ecs \
    update-service \
    --cluster "${ECS_CLUSTER}" \
    --service "${ECS_SERVICE}" \
    --region "${AWS_REGION}" \
    --task-definition "${TASK_DEFINITION}"

# Wait for the task definition to be ready before leaving script.
aws ecs \
    wait services-stable \
    --cluster "${ECS_CLUSTER}" \
    --services "${ECS_SERVICE}" \
    --region "${AWS_REGION}"

# Send deployment information to NewRelic.
NEWRELIC_PARAMETER_NAME="/${BUILD_ENVIRONMENT}/common/NEWRELIC_API_KEY"
NEWRELIC_APPLICATION_ID=$(jq -er ".newRelic.applicationId.${BUILD_ENVIRONMENT} // empty" ci.json || echo "")
BUILD_CHANGELOG=$(git --no-pager log --oneline "${GIT_COMMIT}" | head -15 || echo "")
BUILD_USER="${BUILD_USER:-Jenkins}"

if [[ ! -z "${NEWRELIC_APPLICATION_ID}" && "${BUILD_ENVIRONMENT}" == "prod" ]]; then
    # Retrieve NewRelic API Key from AWS Parameter Store.
    NEWRELIC_API_KEY=$(
        aws ssm \
            get-parameter \
            --name "${NEWRELIC_PARAMETER_NAME}" \
            --region "${AWS_REGION}" \
            --with-decryption | \
        jq -r '.Parameter.Value' \
        || echo ""
    )
    if [[ ! -z "${NEWRELIC_API_KEY}" ]]; then
        # Don't fail build if deployment couldn't be saved.
        set +x
        NEWRELIC_DEPLOY_INFO=$(jq -n \
            --arg rev "${IMAGE_TAG}" \
            --arg usr "${BUILD_USER}" \
            --arg log "${BUILD_CHANGELOG}" \
            --arg txt "Jenkins deployment #${BUILD_NUMBER}" \
            '{deployment: {revision: $rev, user: $usr, changelog: $log, description: $txt}}'
        )
        # Ref: https://rpm.newrelic.com/api/explore/application_deployments/create
        curl -X POST \
            "https://api.newrelic.com/v2/applications/${NEWRELIC_APPLICATION_ID}/deployments.json" \
            -H "Content-Type: application/json" \
            -H "X-Api-Key: ${NEWRELIC_API_KEY}" \
            -d "${NEWRELIC_DEPLOY_INFO}"
        set -x
    fi
fi

echo "Deployment successful"
