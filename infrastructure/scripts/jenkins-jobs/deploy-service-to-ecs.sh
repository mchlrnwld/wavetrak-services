#!/bin/bash

# Deploy Service to ECS and notify to NewRelic.
# https://surfline-jenkins-master-prod.surflineadmin.com/job/deploy-service-to-ecs-newrelic/
#
# Repository: wavetrak-services
# Dependencies:
# - wavetrak-services/scripts/create_ecs_params.py
# - wavetrak-services/scripts/verify_variables_in_parameter_store.py
# - wavetrak-services/docker/{SERVICE}.yml
# Parameters:
# - SERVICE: [
#     auth-service,
#     buoyweather-api,
#     cameras-service,
#     charts-service,
#     entitlements-service,
#     favorites-service,
#     feed-service,
#     forecasts-api,
#     graphql-gateway,
#     geo-target-service,
#     kbyg-product-api,
#     location-view-service,
#     metering-service,
#     notifications-service,
#     onboarding-service,
#     password-reset-service,
#     promotions-service,
#     reports-api,
#     science-data-service,
#     science-models-db,
#     search-service,
#     services-proxy,
#     session-analyzer,
#     sessions-api,
#     spots-api,
#     subscription-service,
#     user-service,
#     wave-api,
#     weather-api
#   ]
# - ENVIRONMENT: [
#     sandbox,
#     staging,
#     prod
#   ]
# - VERSION: master (default)
# - FORCE_REBUILD: false (default)

# Set '-e' for strict mode to immediately interrupt the script if any
# of the command executions returns an exit status greater than zero.
# Set '-u' to exit if a variable is being called and doesn't exists.
# Set '-x' to output the command itself being called.
# Set '-o pipefail' prevents errors in a pipeline from being masked.
set -euxo pipefail

# Wrapper functions.
function info {
    echo "[INFO] ${*}"
}

function error {
    echo "[ERROR] ${*}" >&2
    exit 1
}

function catch_errors() {
    echo "Script aborted because of errors."
    exit 1
}

trap catch_errors ERR

# Tag Jenkins job.
info "build description:${SERVICE}|${ENVIRONMENT}|${VERSION}|"

# Service folder.
cd "services/${SERVICE}"
if [[ ! -f "./ci.json" ]]; then
    error "No [ci.json] file found on service folder. Aborting script."
fi

# Configuration.
SERVICE_NAME="$(jq -er '.serviceName' ci.json)"
IMAGE_REPO="$(jq -er '.repository // empty' ci.json || echo '833713747344.dkr.ecr.us-west-1.amazonaws.com')"
IMAGE_NAME="$(jq -er '.imageName' ci.json)"
IMAGE_HOST="${IMAGE_REPO/\/${IMAGE_NAME}/}"
IMAGE_TAG="${ENVIRONMENT}-${GIT_COMMIT:0:7}"
COMPOSE_NAME="$(jq -er '.compose' ci.json)"
COMPOSE_FILE="${WORKSPACE}/docker/${COMPOSE_NAME}.yml"
ECS_SERVICE="$(jq -er '.serviceName' ci.json)-${ENVIRONMENT}"
ECS_CLUSTER="$(jq -er '.cluster' ci.json)-${ENVIRONMENT}"
ECS_PARAMS_FILE="${WORKSPACE}/docker/${COMPOSE_NAME}.ecs-params.yml"
TASK_FAMILY="$(jq -er '.projectBase // empty' "ci.json" || echo ${ECS_CLUSTER})-${COMPOSE_NAME}"
TEST_COMMAND="$(jq -er '.test.command // empty' ci.json || echo '')"
SCRIPT_SSM_VERIFY="${WORKSPACE}/scripts/verify_variables_in_parameter_store.py"
SCRIPT_ECS_PARAMS="${WORKSPACE}/scripts/create_ecs_params.py"

# AWS region and path to assume role.
AWS_PROFILE="${ENVIRONMENT}"
if [[ "${ENVIRONMENT}" == "sandbox" ]]; then
    AWS_PROFILE="dev"
fi
export AWS_REGION="us-west-1"
export AWS_DEFAULT_REGION="us-west-1"
export PATH="${PATH}:/usr/local/bin"

# Validations and file manipulations.
info "Assuming role for [${ENVIRONMENT}] environment..."
eval "$(assume-role "${ENVIRONMENT}")"

info "Checking that all secrets already exists in AWS Parameter Store..."
python "${SCRIPT_SSM_VERIFY}" \
    --ci_file_path "ci.json" \
    --environment "${ENVIRONMENT}"

info "Creating [docker-compose.yml] file from template..."
sed -e "s/\${IMAGE_TAG}/${IMAGE_TAG}/g" \
    -e "s/\${ENVIRONMENT}/${ENVIRONMENT}/g" \
    -e "s/\${REPOSITORY_HOST}/${IMAGE_HOST}/g" \
    "${COMPOSE_FILE}" > docker-compose.yml

info "Content of [docker-compose.yml] after configuration:"
cat docker-compose.yml

info "Creating [ecs-params.yml] file storing secrets from [ci.json] file..."
if [[ ! -f "${ECS_PARAMS_FILE}" ]]; then
    echo -e "version: 1\ntask_definition: {}" > "${ECS_PARAMS_FILE}"
fi
python "${SCRIPT_ECS_PARAMS}" \
    --ci_file_path "ci.json" \
    --ecs_params_file_path "${ECS_PARAMS_FILE}" \
    --environment "${ENVIRONMENT}"

info "Validating [ecs-params.yml] file format..."
python -c "import yaml, sys; yaml.safe_load(sys.stdin)" < ecs-params.yml \
    || error "Script-generated [ecs-params.yml] file is invalid."

info "Content of [ecs-params.yml] after configuration:"
cat ecs-params.yml

info "Retrieving [.npmrc] file in case we need to regenerate the image..."
aws secretsmanager \
    get-secret-value \
    --secret-id "${ENVIRONMENT}/deployment/npm/npmrc" \
    --region "${AWS_REGION}" | \
jq -r '.SecretString' \
> .npmrc

info "Content of [.npmrc] after configuration:"
cat .npmrc

# Service image.
info "Assuming role for [prod] environment and logging into ECR..."
eval "$(assume-role "prod")"
eval "$(aws ecr get-login --no-include-email --region "${AWS_REGION}")"

# If you want to pull the Docker image to test locally with 'aws-cli'
# version > 1.18, you may run this command to login correctly:
#
# aws ecr
#     get-login-password \
#     --region ${AWS_REGION} | \
# docker \
#     login \
#     --username AWS \
#     --password-stdin ${IMAGE_HOST}

info "Checking if image tag already exists in ECR..."
ECR_IMAGE_TAG=$(
    aws ecr \
        describe-images \
        --region "${AWS_REGION}" \
        --repository-name "${IMAGE_NAME}" | \
    jq -r ".imageDetails[].imageTags[]?" | \
    grep "${IMAGE_TAG}" | \
    grep -v "sandbox\|staging\|prod" \
    || echo ""
)

# Build image only if forced or if it doesn't exists already.
if [[ -z "${ECR_IMAGE_TAG}" || "${FORCE_REBUILD}" == "true" ]]; then

    if [[ "${TEST_COMMAND}" != "" ]]; then
        info "Running tests from command retrieved from [ci.json]..."
        info "Once completed, Jenkins deploy job will continue."
        eval "${TEST_COMMAND}"
    fi

    if [[ -f "infra/scripts/pre-docker-build-actions.sh" ]]; then
        info "Running pre-docker build actions..."
        sh "infra/scripts/pre-docker-build-actions.sh ${AWS_PROFILE} ${WORKSPACE} services/${SERVICE}"
    fi

    APP_ENV="${ENVIRONMENT}"
    if [ "${ENVIRONMENT}" == "prod" ]; then
        APP_ENV="production"
    fi

    info "Building image..."
    docker build \
        --build-arg "APP_ENV=${APP_ENV}" \
        --build-arg "APP_VERSION=${IMAGE_TAG}" \
        --build-arg "NPMRC=.npmrc" \
        --tag "${IMAGE_HOST}/${IMAGE_NAME}:${IMAGE_TAG}" \
        --tag "${IMAGE_HOST}/${IMAGE_NAME}:build-cache" \
        .

    info "Pushing image to ECR..."
    docker push "${IMAGE_HOST}/${IMAGE_NAME}:${IMAGE_TAG}"
    docker push "${IMAGE_HOST}/${IMAGE_NAME}:build-cache"
else
    info "Retrieving image from ECR..."
    docker pull "${IMAGE_HOST}/${IMAGE_NAME}:${IMAGE_TAG}"
fi

# Create new task definition and update service in environment.
info "Assuming role for [${ENVIRONMENT}] environment..."
eval "$(assume-role "${ENVIRONMENT}")"

info "Registering new task definition..."
TASK_DEFINITION=$(
    ecs-cli \
        compose \
        --file "docker-compose.yml" \
        --ecs-params "ecs-params.yml" \
        --project-name "${TASK_FAMILY}" \
        create \
        --launch-type "EC2" \
        --cluster "${ECS_CLUSTER}" \
        --region "${AWS_REGION}" | \
    grep 'TaskDefinition' | \
    sed 's/.*TaskDefinition="\(.*\)"/\1/' \
    || echo ""
)

if [[ -z "${TASK_DEFINITION}" ]]; then
    error "Failed to receive a task definition from ecs-cli command."
fi

info "Updating service to use the new task definition [${TASK_DEFINITION}]..."
aws ecs \
    update-service \
    --service "${ECS_SERVICE}" \
    --cluster "${ECS_CLUSTER}" \
    --region "${AWS_REGION}" \
    --task-definition "${TASK_DEFINITION}" \
    --force-new-deployment

info "Waiting for service to become stable..."
aws ecs \
    wait services-stable \
    --services "${ECS_SERVICE}" \
    --cluster "${ECS_CLUSTER}" \
    --region "${AWS_REGION}"

# Send deployment information to NewRelic.
NEWRELIC_APPLICATION_ID="$(jq -er '.newRelic.applicationId.${ENVIRONMENT} // empty' ci.json || echo '')"
NEWRELIC_PARAMETER_NAME="/${ENVIRONMENT}/common/NEWRELIC_API_KEY"
BUILD_CHANGELOG="$(git --no-pager log --oneline ${GIT_COMMIT} | head -15 || echo '')"
BUILD_USER="${BUILD_USER:-Jenkins}"

if [[ ! -z "${NEWRELIC_APPLICATION_ID}" && "${ENVIRONMENT}" == "prod" ]]; then
    info "Retrieving NewRelic API Key from AWS Parameter Store..."
    NEWRELIC_API_KEY=$(
        aws ssm \
            get-parameter \
            --name "${NEWRELIC_PARAMETER_NAME}" \
            --region "${AWS_REGION}" \
            --with-decryption | \
        jq -r '.Parameter.Value' \
        || echo ""
    )
    if [[ ! -z "${NEWRELIC_API_KEY}" ]]; then
        # Don't fail build if deployment couldn't be saved.
        set +x
        info "Logging deploy information into NewRelic..."
        NEWRELIC_DEPLOY_INFO=$(jq -n \
            --arg rev "${IMAGE_TAG}" \
            --arg usr "${BUILD_USER}" \
            --arg log "${BUILD_CHANGELOG}" \
            --arg txt "Jenkins deployment #${BUILD_NUMBER} (${ENVIRONMENT}" \
            '{deployment: {revision: $rev, user: $usr, changelog: $log, description: $txt}}'
        )
        # Ref: https://rpm.newrelic.com/api/explore/application_deployments/create
        curl -X POST \
            "https://api.newrelic.com/v2/applications/${NEWRELIC_APPLICATION_ID}/deployments.json" \
            -H "Content-Type: application/json" \
            -H "X-Api-Key: ${NEWRELIC_API_KEY}" \
            -d "${NEWRELIC_DEPLOY_INFO}"
        set -x
    fi
fi

info "Deployed ${TASK_DEFINITION} OK"
