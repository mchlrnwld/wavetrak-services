#!/bin/bash

# Deploy Editorial Service to ECS
# https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-editorial-to-ecs/
#
# Repository: surfline-admin
# Parameters:
# - SERVICE: [
#     editorial-wp,
#     editorial-wp-admin,
#     editorial-varnish
#   ]
# - ENVIRONMENT: [
#     sandbox,
#     staging,
#     prod
#   ]
# - VERSION: master (default)
# - FORCE_REBUILD: true|false

# Set '-e' for strict mode to immediately interrupt the script if any
# of the command executions returns an exit status greater than zero.
# Set '-u' to exit if a variable is being called and doesn't exists.
# Set '-x' to output the command itself being called.
# Set '-o pipefail' prevents errors in a pipeline from being masked.
set -euxo

# Wrapper functions.
function info {
    echo "[INFO] ${*}"
}

function error {
    echo "[ERROR] ${*}" >&2
    exit 1
}

function catch_errors() {
    echo "Script aborted because of errors."
    exit 1
}

trap catch_errors ERR

# Tag Jenkins job.
info "build description:${SERVICE}|${ENVIRONMENT}|${VERSION}|"

# Configuration.
TITLE="Surfline Web Editorial"
CI_FILE="${WORKSPACE}/docker/ci-${SERVICE}.json"
IMAGE_REPO="$(jq -er '.repository // empty' ${CI_FILE} || echo '833713747344.dkr.ecr.us-west-1.amazonaws.com')"
IMAGE_NAME="$(jq -er '.imageName' ${CI_FILE})"
IMAGE_HOST="${IMAGE_REPO/\/${IMAGE_NAME}/}"
IMAGE_TAG="${ENVIRONMENT}-${GIT_COMMIT:0:7}"
COMPOSE_NAME="$(jq -er '.compose' ${CI_FILE})"
COMPOSE_FILE="${WORKSPACE}/docker/${COMPOSE_NAME}.yml"
ECS_SERVICE="$(jq -er '.serviceName' ${CI_FILE})-${ENVIRONMENT}"
ECS_CLUSTER="$(jq -er '.cluster' ${CI_FILE})-${ENVIRONMENT}"
TASK_FAMILY="$(jq -er '.projectBase // empty' ${CI_FILE} || echo 'sl-cms')-${ENVIRONMENT}-${COMPOSE_NAME}"
TASK_ROLE="$(jq -er --arg env "${ENVIRONMENT}" '.taskRoleArn[$env] // empty' ${CI_FILE} || echo '')"
TEST_COMMAND="$(jq -er '.test.command // empty' ${CI_FILE} || echo '')"
BUNDLE_SECRETS_SCRIPT="${JENKINS_HOME}/repos/wavetrak-infrastructure/scripts/jenkins-bundle-secrets/bundle_secrets.py"

# AWS region and path to assume role.
AWS_PROFILE="${ENVIRONMENT}"
if [[ "${ENVIRONMENT}" == "sandbox" ]]; then
    AWS_PROFILE="dev"
fi
export AWS_REGION="us-west-1"
export AWS_DEFAULT_REGION="us-west-1"
export PATH="${PATH}:/usr/local/bin"

# Use 'prod' profile to pull/push images from/to ECR.
eval "$(assume-role prod)"
eval "$(aws ecr get-login --no-include-email --region "${AWS_REGION}")"

# If you want to pull the Docker image to test locally with 'aws-cli'
# version > 1.18, you may run this command to login correctly:
#
# aws ecr
#     get-login-password \
#     --region ${AWS_REGION} | \
# docker \
#     login \
#     --username AWS \
#     --password-stdin ${IMAGE_HOST}

ECR_IMAGE_TAG=$(
    aws ecr \
        describe-images \
        --region "${AWS_REGION}" \
        --repository-name "${IMAGE_NAME}" | \
    jq -r ".imageDetails[].imageTags[]?" | \
    grep "${IMAGE_TAG}" \
    || echo ""
)

# Build image only if forced or if it doesn't exists already.
if [[ "${ECR_IMAGE_TAG}" == "" || "${FORCE_REBUILD}" == "true" ]]; then

    if [[ "${TEST_COMMAND}" != "" ]]; then
        info "Running tests from command retrieved from [ci.json]..."
        info "Once completed, Jenkins deploy job will continue."
        eval "${TEST_COMMAND}"
    else
        # Run simple tests if not specified in ci.json.
        make test
    fi

    # Environment argument.
    APP_ENV="${ENVIRONMENT}"
    if [ "${ENVIRONMENT}" == "prod" ]; then
        APP_ENV="production"
    fi

    # New Relic argument.
    NEW_RELIC_APP_NAME="${TITLE}"
    if [ "${ENVIRONMENT}" != "prod" ]; then
        NEW_RELIC_APP_NAME="${TITLE} (${ENVIRONMENT^})"
    fi

    if [[ "${SERVICE}" == "editorial-varnish" ]]; then
        cd "varnish"
    else
        cd "wordpress"
    fi

    # Build and tag the image.
    docker build \
        -t "${IMAGE_NAME}" \
        --build-arg APP_ENV="${APP_ENV}" \
        --build-arg APP_VERSION="${VERSION}" \
        --build-arg NEW_RELIC_APP_NAME="${NEW_RELIC_APP_NAME}" \
        --tag "${IMAGE_REPO}/${IMAGE_NAME}:${IMAGE_TAG}" \
        .

    # Push it to ECR.
    docker push "${IMAGE_REPO}/${IMAGE_NAME}:${IMAGE_TAG}"
else
    # Pull the image locally.
    docker pull "${IMAGE_REPO}/${IMAGE_NAME}:${IMAGE_TAG}"
fi

# Assume role based on profile environment back again.
eval "$(assume-role "${AWS_PROFILE}")"

# Bundle secrets.
cd "${WORKSPACE}"
python \
    "${BUNDLE_SECRETS_SCRIPT}" \
    --ci-json "${CI_FILE}" \
    --env "${ENVIRONMENT}" > ".env"
jq --raw-input 'split("=") | {name:(.[0]), value:(.[1])}' ".env" | \
    jq -s . \
    > ".env.json"

# Build task definition from a docker-compose file.
# The 'container-transform' is a small utility to transform various docker
# container formats to one another:
# https://github.com/micahhausler/container-transform
docker pull micahhausler/container-transform:latest

# Configuration retrieved into ECS container definition format.
docker run --rm -i micahhausler/container-transform < "${COMPOSE_FILE}" | \
    jq ".containerDefinitions[].environment = $(cat .env.json)" | \
    sed -e "s/\${REPOSITORY_HOST}/${IMAGE_HOST}/g" | \
    sed -e "s/\${IMAGE_TAG}/${IMAGE_TAG}/g" | \
    sed -e "s/\${ENVIRONMENT}/${ENVIRONMENT}/g" \
    > "container-definitions.json"

# If compose file contains memory limit and reservation, use them.
MEM_RESERVATION=$(grep mem_reservation ${COMPOSE_FILE} | sed -e 's/.*mem_reservation: \([0-9]*\)m$/\1/')
if [[ "${MEM_RESERVATION}" != "" ]]; then
    jq ".containerDefinitions[].memoryReservation = ${MEM_RESERVATION}" "container-definitions.json" > "container-definitions.json.tmp"
    mv "container-definitions.json.tmp" "container-definitions.json"
fi

MEM_LIMIT=$(grep mem_limit ${COMPOSE_FILE} | sed -e 's/.*mem_limit: \([0-9]*\)m$/\1/')
if [[ "${MEM_LIMIT}" != "" ]]; then
    jq ".containerDefinitions[].memory = ${MEM_LIMIT}" "container-definitions.json" > "container-definitions.json.tmp"
    mv "container-definitions.json.tmp" "container-definitions.json"
fi
cat "container-definitions.json"

# Create a new task definition version.
if [[ "${TASK_ROLE}" != "" ]]; then
  aws ecs \
      register-task-definition \
      --cli-input-json "file://$(pwd)/container-definitions.json" \
      --task-role-arn ${TASK_ROLE} \
      --region "us-west-1" \
      --family ${TASK_FAMILY}
else
  aws ecs \
      register-task-definition \
      --cli-input-json "file://$(pwd)/container-definitions.json" \
      --family "${TASK_FAMILY}" \
      --region "${AWS_REGION}"
fi

# Name of the task definition.
TASK_DEFINITION=$(
    aws ecs \
        list-task-definitions \
        --region "${AWS_REGION}" \
        --family-prefix "${TASK_FAMILY}" | \
    jq -r '.taskDefinitionArns[-1:][]'
)

# Update the ECS service with corresponding the task definition.
aws ecs \
    update-service \
    --cluster "${ECS_CLUSTER}" \
    --service "${ECS_SERVICE}" \
    --region "${AWS_REGION}" \
    --task-definition "${TASK_DEFINITION}"

# Wait for the task definition to be ready before leaving script.
aws ecs \
    wait services-stable \
    --cluster "${ECS_CLUSTER}" \
    --services "${ECS_SERVICE}" \
    --region "${AWS_REGION}"

# Send deployment information to NewRelic.
NEWRELIC_PARAMETER_NAME="/${ENVIRONMENT}/common/NEWRELIC_API_KEY"
NEWRELIC_APPLICATION_ID=$(jq -er ".newRelic.applicationId.${ENVIRONMENT} // empty" ci.json || echo "")
BUILD_CHANGELOG=$(git --no-pager log --oneline "${GIT_COMMIT}" | head -15 || echo "")
BUILD_USER="${BUILD_USER:-Jenkins}"

if [[ ! -z "${NEWRELIC_APPLICATION_ID}" && "${ENVIRONMENT}" == "prod" ]]; then
    # Retrieve NewRelic API Key from AWS Parameter Store.
    NEWRELIC_API_KEY=$(
        aws ssm \
            get-parameter \
            --name "${NEWRELIC_PARAMETER_NAME}" \
            --region "${AWS_REGION}" \
            --with-decryption | \
        jq -r '.Parameter.Value' \
        || echo ""
    )
    if [[ ! -z "${NEWRELIC_API_KEY}" ]]; then
        # Don't fail build if deployment couldn't be saved.
        set +x
        NEWRELIC_DEPLOY_INFO=$(jq -n \
            --arg rev "${IMAGE_TAG}" \
            --arg usr "${BUILD_USER}" \
            --arg log "${BUILD_CHANGELOG}" \
            --arg txt "Jenkins deployment #${BUILD_NUMBER}" \
            '{deployment: {revision: $rev, user: $usr, changelog: $log, description: $txt}}'
        )
        # Ref: https://rpm.newrelic.com/api/explore/application_deployments/create
        curl -X POST \
            "https://api.newrelic.com/v2/applications/${NEWRELIC_APPLICATION_ID}/deployments.json" \
            -H "Content-Type: application/json" \
            -H "X-Api-Key: ${NEWRELIC_API_KEY}" \
            -d "${NEWRELIC_DEPLOY_INFO}"
        set -x
    fi
fi

echo "Deployment successful"
