// Deploy crossband frontend applications to ECS
// https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-crossbrand-frontend-to-ecs-master/
import hudson.FilePath

def imageName = ""
def imageTag = ""
def buildSha = ""
def awsProfile = ""
def awsRegion = "us-west-1"

def brand = ""
def compose = ""
def taskRoleArn = ""
def family = ""
def projectPrefix = ""
def baseService = ""
def baseCluster = ""

def baseCDN = [
    "bw": "bw-public-static",
    "sl": "sl-product-cdn"
]
def repos = [
    "sl": "git@github.com:Surfline/surfline-web.git",
    "bw": "git@github.com:Surfline/bw.git"
]
def serviceMaps = [
  "kbyg": "sl-kbyg-product-svc",
  "onboarding": "sl-onboarding-product-svc",
  "homepage": "sl-homepage-product-svc",
  "charts": "sl-charts-product-svc",
  "travel": "sl-travel-product-svc"
]

// FIXME: Uncomment to run on docker slaves
// node("ecs-docker-2cpu-2gb") {
node {
    currentBuild.result = "SUCCESS"
    workspace = pwd()
    registryHost = "833713747344.dkr.ecr.us-west-1.amazonaws.com"
    bundleSecretsScript = "/var/lib/jenkins/repos/wavetrak-infrastructure/scripts/jenkins-bundle-secrets/bundle_secrets.py"
    properties([
        parameters([
            choice(
                name: "CONTAINER_PATH",
                description: "container to deploy",
                choices: [
                    "account",
                    "kbyg",
                    "homepage",
                    "onboarding",
                    "charts",
                    "travel",
                    "backplane",
                    "bw/webapp"
                ].join("\n")
            ),
            choice(
                name: "ENVIRONMENT",
                description: "target deployment environment",
                choices: [
                    "sandbox",
                    "staging",
                    "prod"
                ].join("\n")
            ),
            string(
                name: "REF",
                defaultValue: "master",
                description: "git reference to deploy"
            ),
        ])
    ])

    def debug = { msg ->
        echo "[DEBUG] ${msg}"
    }

    def info = { msg ->
        echo "[INFO] ${msg}"
    }

    def warning = { msg ->
        echo "[WARNING] ${msg}"
    }

    def error = { msg ->
        echo "[ERROR] ${msg}"
    }

    try {
        stage("Prepare") {
            info "Prepare step started"

            brand = env.CONTAINER_PATH.startsWith("bw") ? "bw" : "sl"
            buildEnv = env.ENVIRONMENT
            buildNo = env.BUILD_NUMBER
            wrap([$class: "BuildUser"]) {
                buildUser = "${BUILD_USER}"
            }
            version = env.REF

            if (brand == "bw") {
                containerPath = env.CONTAINER_PATH.substring(3, env.CONTAINER_PATH.length())
            } else {
                containerPath = env.CONTAINER_PATH
            }

            // git(url: "${repos[brand]}", branch: "${version}")

            checkout scm: [
                $class: "GitSCM",
                branches: [[name: "${version}"]],
                userRemoteConfigs: [[
                    credentialsId: "3f26d183-e3d2-4df4-a773-7f4285098a73",
                    url: "${repos[brand]}"
                ]],

            ]

            buildSha = sh(script: "git rev-parse HEAD", returnStdout: true).trim()
            currentBuild.description = "${containerPath} - ${buildEnv} (${buildSha})"

            info "app: ${containerPath}"
            info "brand: ${brand}"
            info "git ref: ${buildSha}"
            info "environment: ${buildEnv}"

            awsProfile = buildEnv

            dir("${workspace}/${containerPath}") {
                imageTag     = "${buildEnv}-${buildSha}"
                imageName    = sh(script: "cat ci.json | jq -r '.imageName'", returnStdout: true).trim()
                artifactPath = sh(script: "cat ci.json | jq -r '.cdnArtifactPath'", returnStdout: true).trim()
                compose      = sh(script: "cat ci.json | jq -r '.compose'", returnStdout: true).trim()
                taskRoleArn  = sh(script: "cat ci.json | jq -r '.taskRoleArn.${buildEnv}'", returnStdout: true).trim()
                baseService  = sh(script: "cat ci.json | jq -r '.serviceName'", returnStdout: true).trim()
                baseCluster  = sh(script: "cat ci.json | jq -r '.cluster'", returnStdout: true).trim()
                if (baseCluster == "null") {
                    baseCluster = "sl-product-svc"
                }

                info "${baseCluster}"

                projectPrefix="${baseCluster}-${buildEnv}"
                family = "${projectPrefix}-${compose}".replace("sl-", "${brand}-")
            }

            info "Prepare step finished"
        }

        stage("Build") {
            info "Build step started"

            dir("${workspace}/${containerPath}") {
                appEnv = (buildEnv == "prod") ? "production": buildEnv

                sh "export AWS_REGION=${awsRegion} && \
                    export AWS_DEFAULT_REGION=${awsRegion} && \
                    eval \$(/usr/local/bin/assume-role 'staging') && \
                    aws secretsmanager \
                        get-secret-value \
                        --secret-id 'staging/deployment/npm/npmrc' | \
                        jq .SecretString -r \
                        > .npmrc && \
                    cat .npmrc && \
                    eval \$(/usr/local/bin/assume-role 'prod') && \
                    eval \$(aws ecr get-login --no-include-email | sed 's/ -e none//') && \
                    docker pull ${registryHost}/${imageName}:build-cache > /dev/null 2>&1 || true && \
                    docker build \
                        --cache-from ${registryHost}/${imageName}:build-cache \
                        --build-arg APP_ENV=${appEnv} \
                        --build-arg APP_VERSION=${buildSha} \
                        --tag ${registryHost}/${imageName}:${imageTag} \
                        --tag ${registryHost}/${imageName}:build-cache \
                        ."
            }

            info "Build step ended"
        }

        stage("Publish") {
            info "Publish step started"

            sh "export AWS_REGION=${awsRegion} && \
                export AWS_DEFAULT_REGION=${awsRegion} && \
                eval \$(/usr/local/bin/assume-role 'prod') && \
                eval \$(aws ecr get-login --no-include-email | sed 's/ -e none//') && \
                docker push \"${registryHost}/${imageName}:${imageTag}\" && \
                docker push \"${registryHost}/${imageName}:build-cache\""

            publicAssets = "${workspace}/tmp_public_assets"

            sh "mkdir -p ${publicAssets}"
            artifactSrc = sh(script: "\
                docker create \"${registryHost}/${imageName}:${imageTag}\"", returnStdout: true).trim()

            info "${artifactSrc}"

            sh "docker cp \"${artifactSrc}:${artifactPath}/.\" \"${publicAssets}\" && \
                docker rm \"${artifactSrc}\""

            info "Publish profile is ${awsProfile}"

            env = (buildEnv == "sandbox") ? "sbox" : buildEnv
            cdn = "s3://${baseCDN[brand]}-${env}/${containerPath}"
            info "cdn: ${cdn}"
            sh "export AWS_REGION=${awsRegion} && \
                export AWS_DEFAULT_REGION=${awsRegion} && \
                eval \$(/usr/local/bin/assume-role '${awsProfile}') && \
                aws s3 \
                    cp \
                    --recursive \"${publicAssets}\" \"${cdn}\" \
                    --cache-control max-age=604800,public"

            info "Publish step finished"
        }

        stage("Deploy") {
            info "Deploy step started"

            dir(workspace) {
                composeFile = "docker/${compose}.yml"
                sh "export AWS_REGION=${awsRegion} && \
                    export AWS_DEFAULT_REGION=${awsRegion} && \
                    eval \$(/usr/local/bin/assume-role '${awsProfile}') && \
                    python \
                        \"${bundleSecretsScript}\" \
                        --ci-json '${containerPath}/ci.json' \
                        --env \"${buildEnv}\" \
                        > '.env' && \
                    jq --raw-input 'split(\"=\") | {name:(.[0]), value:(.[1])}' '.env' | \
                        jq -s . \
                        > '.env.json' && \
                    cat .env.json && \
                    docker pull micahhausler/container-transform:latest && \
                    cat \"${composeFile}\" | \
                        docker run --rm -i micahhausler/container-transform | \
                        jq \".containerDefinitions[].environment = \$(cat .env.json)\" | \
                        sed -e 's/\${REPOSITORY_HOST}/${registryHost}/g' | \
                        sed -e 's/\${IMAGE_TAG}/${imageTag}/g' | \
                        sed -e 's/\${ENVIRONMENT}/${buildEnv}/g' \
                        > def.json"

                info "Register task definition"
                info "Deploy profile is ${awsProfile}"

                sh "export AWS_REGION=${awsRegion} && \
                    export AWS_DEFAULT_REGION=${awsRegion} && \
                    eval \$(/usr/local/bin/assume-role '${awsProfile}') && \
                    aws ecs \
                        register-task-definition \
                        --cli-input-json 'file://${workspace}/def.json' \
                        --task-role-arn ${taskRoleArn} \
                        --region '${awsRegion}' \
                        --family ${family}"

                task = sh(script: "\
                    export AWS_REGION=${awsRegion} && \
                    export AWS_DEFAULT_REGION=${awsRegion} && \
                    eval \$(/usr/local/bin/assume-role '${awsProfile}') && \
                    aws ecs \
                        list-task-definitions \
                        --region '${awsRegion}' \
                        --family-prefix ${family} | \
                        jq -r '.taskDefinitionArns[-1:][]'", returnStdout: true).trim()

                cluster = "${baseCluster}-${buildEnv}"

                if (serviceMaps.containsKey(containerPath)) {
                    service = "${serviceMaps[containerPath]}-${buildEnv}"
                } else {
                    service = "${brand}-${baseService}-${buildEnv}"
                }

                sh "export AWS_REGION=${awsRegion} && \
                    export AWS_DEFAULT_REGION=${awsRegion} && \
                    eval \$(/usr/local/bin/assume-role '${awsProfile}') && \
                    aws ecs \
                        update-service \
                        --region '${awsRegion}' \
                        --cluster '${cluster}' \
                        --service '${service}' \
                        --task-definition '${task}' && \
                    aws ecs \
                        wait services-stable \
                        --region '${awsRegion}' \
                        --cluster '${cluster}' \
                        --services '${service}'"
            }

            date = sh(script: "date +%s", returnStdout: true).trim()
            deployKey = "${brand}/web/${containerPath}/${buildEnv}"
            sh "export AWS_REGION=${awsRegion} && \
                export AWS_DEFAULT_REGION=${awsRegion} && \
                eval \$(/usr/local/bin/assume-role 'dev') && \
                aws dynamodb \
                    put-item \
                    --region '${awsRegion}' \
                    --table-name 'ServiceDeployments' \
                    --item '{\"DeployKey\":{\"S\": \"${deployKey}\"},\"ServiceName\": {\"S\": \"${containerPath}\"},\"Environment\": {\"S\": \"${buildEnv}\"},\"CommitSha\": {\"S\": \"${buildSha}\"},\"Brand\": {\"S\": \"${brand}\"}, \"Component\":{\"S\": \"web\"}}'"

            dir("${workspace}/${containerPath}") {
                newRelicAppId = sh(script: "cat ci.json | jq -r '.newRelic.applicationId.${buildEnv}'", returnStdout: true).trim()
                newRelicParam = "/${buildEnv}/common/NEWRELIC_API_KEY"
                buildLog = sh(script: "git --no-pager log --oneline | head -15", returnStdout: true).trim()
                buildTag = "${buildEnv}-${buildSha}"

                info "newRelicAppId: ${newRelicAppId}"
                info "newRelicParam: ${newRelicParam}"
                info "buildLog: ${buildLog}"

                if (newRelicAppId != "" && buildEnv == "prod") {
                    newRelicApiKey = sh(script: "\
                        export AWS_REGION=${awsRegion} && \
                        export AWS_DEFAULT_REGION=${awsRegion} && \
                        eval \$(/usr/local/bin/assume-role '${awsProfile}') && \
                        aws ssm \
                            get-parameter \
                            --name '${newRelicParam}' \
                            --region '${awsRegion}' \
                            --with-decryption | \
                            jq -r '.Parameter.Value'", returnStdout: true).trim()

                    info "newRelicApiKey: ${newRelicApiKey}"

                    if (newRelicApiKey != "") {
                        newRelicDeployInfo = sh(script: "jq -n \
                            --arg rev '${buildTag}' \
                            --arg usr '${buildUser}' \
                            --arg log '${buildLog}' \
                            --arg txt 'Jenkins test deployment #${buildNo}' \
                            '{deployment: {revision: \$rev, user: \$usr, changelog: \$log, description: \$txt}}'", returnStdout: true).trim()
                        info "newRelicDeployInfo: ${newRelicDeployInfo}"
                        sh "curl -X POST \
                            'https://api.newrelic.com/v2/applications/${newRelicAppId}/deployments.json' \
                            -H 'Content-Type: application/json' \
                            -H 'X-Api-Key: ${newRelicApiKey}' \
                            -d '${newRelicDeployInfo}'"
                    }
                }
            }

            deleteDir()
            info "Success, deployed ${brand}/${containerPath}@${buildSha} to ${buildEnv}"
        }

    } catch (err) {
        error err.toString()
        currentBuild.result = "FAILURE"
        deleteDir()
    }
}
