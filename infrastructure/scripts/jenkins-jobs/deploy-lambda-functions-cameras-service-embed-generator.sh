#!/bin/bash

# Update Cameras Service 'embed-generator' Lambda Function
# https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-cameras-service-embed-generator/
#
# Repository: wavetrak-services
# Parameters:
# - BUILD_FUNCTION: [
#     embed-generator
#   ]
# - BUILD_ENVIRONMENT: [
#     sandbox,
#     staging,
#     prod
#   ]
# - BUILD_TAG: master (default)
# Node version: 8.10.0

# Set '-e' for strict mode to immediately interrupt the script if any
# of the command executions returns an exit status greater than zero.
# Set '-u' to exit if a variable is being called and doesn't exists.
# Set '-x' to output the command itself being called.
# Set '-o pipefail' prevents errors in a pipeline from being masked.
set -euxo pipefail

# Tag Jenkins job.
echo "build description:${BUILD_FUNCTION}|${BUILD_ENVIRONMENT}|${BUILD_TAG}|"

# Configuration.
APP_NAME="sl-cameras-service-${BUILD_FUNCTION}"
APP_FOLDER="${WORKSPACE}/services/cameras-service/functions/${BUILD_FUNCTION}"
BUNDLE_FILENAME="${APP_NAME}.zip"
BUNDLE_SECRETS_SCRIPT="${JENKINS_HOME}/repos/wavetrak-infrastructure/scripts/jenkins-bundle-secrets/bundle_secrets.py"
S3_KEY="${BUNDLE_FILENAME/\.zip/-${BUILD_TAG}.zip}"
S3_PREFIX="lambda-functions/${APP_NAME/sl-}"
S3_BUCKET="sl-artifacts-${BUILD_ENVIRONMENT}"
if [[ "${BUILD_ENVIRONMENT}" == "sandbox" ]]; then
    S3_BUCKET="sl-artifacts-dev"
fi

# AWS region and path to assume role.
export AWS_REGION="us-west-1"
export AWS_DEFAULT_REGION="us-west-1"
export PATH="${PATH}:/usr/local/bin"
eval "$(assume-role "${BUILD_ENVIRONMENT}")"

# Working directory.
cd "${APP_FOLDER}"

# Bundle secrets.
python \
    "${BUNDLE_SECRETS_SCRIPT}" \
    --ci-json "./ci.json" \
    --env "${BUILD_ENVIRONMENT}" > .env

# Bundle artifactory credentials.
aws secretsmanager \
    get-secret-value \
        --secret-id "${BUILD_ENVIRONMENT}/deployment/npm/npmrc" \
        --region "${AWS_REGION}" \
        --profile "${BUILD_ENVIRONMENT}" | \
    jq .SecretString -r > .npmrc

# Install, test and build application.
rm -rf node_modules
npm install
npm run lint
npm run bundle

# Upload function file.
aws s3 \
    cp \
    "${BUNDLE_FILENAME}" \
    "s3://${S3_BUCKET}/${S3_PREFIX}/${S3_KEY}"

# Update lambda function from uploaded file.
aws lambda \
    update-function-code \
    --function-name "${APP_NAME}-${BUILD_ENVIRONMENT}" \
    --s3-bucket "${S3_BUCKET}" \
    --s3-key "${S3_PREFIX}/${S3_KEY}"
