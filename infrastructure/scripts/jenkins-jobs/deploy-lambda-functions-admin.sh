#!/bin/bash

# Update Admin Lambda Functions
# https://surfline-jenkins-master-prod.surflineadmin.com/job/admin/job/deploy-lambda-functions-admin/
#
# Repository: surfline-admin
# Parameters:
# - BUILD_FUNCTION: [
#     spotreportview-builder,
#     sort-subregion-spots,
#     clear-cf-redis
#   ]
# - BUILD_ENVIRONMENT: [
#     sandbox,
#     staging,
#     prod
#   ]
# - BUILD_TAG: master (default)
# Node version: 12.13.0

# Set '-e' for strict mode to immediately interrupt the script if any
# of the command executions returns an exit status greater than zero.
# Set '-u' to exit if a variable is being called and doesn't exists.
# Set '-x' to output the command itself being called.
# Set '-o pipefail' prevents errors in a pipeline from being masked.
set -euxo pipefail

# Tag Jenkins job.
echo "build description:${BUILD_FUNCTION}|${BUILD_ENVIRONMENT}|${BUILD_TAG}|"

# Configuration.
APP_NAME="sl-${BUILD_FUNCTION}-${BUILD_ENVIRONMENT}"
APP_FOLDER="${WORKSPACE}/functions/${BUILD_FUNCTION}"
BUNDLE_FILENAME="sl-${BUILD_FUNCTION}-lambda.zip"
BUNDLE_SECRETS_SCRIPT="${JENKINS_HOME}/repos/wavetrak-infrastructure/scripts/jenkins-bundle-secrets/bundle_secrets.py"
S3_BUCKET="sl-admin-lambda-functions-${BUILD_ENVIRONMENT}"
S3_KEY="${BUNDLE_FILENAME/\.zip/-${BUILD_TAG}.zip}"

# AWS region and path to assume role.
export AWS_REGION="us-west-1"
export AWS_DEFAULT_REGION="us-west-1"
export PATH="${PATH}:/usr/local/bin"
eval "$(assume-role "${BUILD_ENVIRONMENT}")"

# Working directory.
cd "${APP_FOLDER}"

# Bundle secrets.
python \
    "${BUNDLE_SECRETS_SCRIPT}" \
    --ci-json "./ci.json" \
    --env "${BUILD_ENVIRONMENT}" > .env

# Install, test and bundle application.
rm -rf node_modules
npm install
npm run lint
npm test
npm run bundle

# Upload function file.
aws s3 \
    cp \
    "${BUNDLE_FILENAME}" \
    "s3://${S3_BUCKET}/${S3_KEY}"

# Update lambda function from uploaded file.
aws lambda \
    update-function-code \
    --function-name "${APP_NAME}" \
    --s3-bucket "${S3_BUCKET}" \
    --s3-key "${S3_KEY}"
