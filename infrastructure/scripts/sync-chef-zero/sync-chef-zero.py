import json
import subprocess
import boto3
import botocore
import os
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument("env", help="environment name, eg: 'staging' or 'staging/role-name'")
parser.add_argument("cookbook", help="cookbook name as looked up from chef-data.json")
parser.add_argument("--build", action='store_true', help="Build the berkshelf before uploading")
parser.add_argument("--existing", action='store_true', help="Use existing cookbooks, skip build and upload. In case you want to just trigger a chef run")
parser.add_argument("--plan", action='store_true', help="Show what environments will be uploaded without doing any work")
parser.add_argument("--chefrun", action='store_true', help="prompt a chef run via ssm on each instance that targets the uploaded cookbook")
parser.add_argument("--delay", help="seconds to delay between chef runs")
args = parser.parse_args()

#Boto setup
s3 = boto3.resource('s3')
ssm = boto3.client('ssm', region_name='us-west-1')
client = boto3.client('ec2', region_name='us-west-1')

company = 'sl'
chefdata = json.load(open('chef-data.json'))

def ssm_getdoc(ssm, docname):
    ssmdocs = ssm.list_documents(
        DocumentFilterList=[
        {
            'key': 'Owner',
            'value': 'Self'
        }
        ]
    )
    chefdoc = (doc for doc in ssmdocs["DocumentIdentifiers"] if docname in doc["Name"]).next()
    if chefdoc["Name"]:
        return chefdoc["Name"]
    else:
        return -1

def ssm_chefrun(ssm, client, role, doc, plan):
    role_clients = client.describe_instances(Filters=[{'Name':'tag:Application', 'Values':[role]}])
    for reservation in role_clients["Reservations"]:
        for instance in reservation["Instances"]:
            nameTag = (tag for tag in instance["Tags"] if tag["Key"] == "Name").next()
            if plan:
                print "PLAN: Syncing and running cookbooks via SSM on " + nameTag["Value"]
            else:
                print "Syncing and running cookbooks via SSM on " + nameTag["Value"]
                instanceid = instance["InstanceId"]
                sent_command = ssm.send_command(
                    InstanceIds=[
                        instanceid
                    ],
                    DocumentName=doc
                )

                print "  Started Command: " + sent_command["Command"]["CommandId"]
                time.sleep(5)
                status = ssm.get_command_invocation(
                       CommandId = sent_command["Command"]["CommandId"],
                       InstanceId = instanceid
                    )
                print "   " + nameTag["Value"] + ": " + status["Status"]

                while status["Status"] in ["Pending","InProgress","Delayed"]:
                    time.sleep(10)
                    status = ssm.get_command_invocation(
                       CommandId= sent_command["Command"]["CommandId"],
                       InstanceId = instanceid
                    )
                    print "   " + nameTag["Value"] + ": " + status["Status"]

                if args.delay:
                    print "waiting " + args.delay + " seconds before next run"
                    time.sleep(int(args.delay))

def upload_cookbook(client, company, role, env, tar_file, plan):
    bucket = company + '-chef-' + env
    key = role + '/config/' + os.path.basename(tar_file)
    try:
        client.Object(bucket, key).load()
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            if plan:
                print "PLAN: upload " + bucket + "/" + key
            else:
                print "Adding " + key + " to " + bucket
                s3obj = client.Object(bucket, key)
                s3obj.put(Body=open(tar_file))
    else:
        print "cookbook package " + os.path.basename(tar_file) + " already exists for " + role + " in " + bucket

if "/" in args.env:
    specific = args.env.split("/")
    chefenv = specific[0]
    specified_role = specific[1]
else:
    specified_role = "all"
    chefenv = args.env

cookbook = args.cookbook

if not chefdata[cookbook]:
    print "Error: Cookbook not defined"
    exit()

if not chefenv in chefdata[cookbook]["roles"].keys():
    print "Error: Cookbook not valid to be deployed in " + chefenv
    exit()

print "Processing cookbook: " + cookbook + " (" + chefdata[cookbook]['source'] + ") for " + chefenv

if args.build and not args.existing:
    build_cmd = '''
        pushd %s
        rm ./cookbooks-*.tar.gz
        export BERKS_PACKAGE=$(basename $(berks package | awk ' { print $4 } '))
        export BERKS_PACKAGE_VERSION=$(echo ${BERKS_PACKAGE} | grep -oE '[0-9]' | tr -d '\n')
        gunzip -d ${BERKS_PACKAGE}
        tar -C ../../ -uvf cookbooks-${BERKS_PACKAGE_VERSION}.tar data_bags/
        gzip cookbooks-${BERKS_PACKAGE_VERSION}.tar
        popd
        ''' % (chefdata[cookbook]['source'])
    poutput = subprocess.check_output(build_cmd , shell=True)

if not args.existing:
    try:
        cookbook_tar = (subprocess.check_output("ls " + chefdata[cookbook]['source'] + "/cookbooks-*.tar.gz" , shell=True)).strip()
        tar_file = os.path.realpath(cookbook_tar)
    except:
        print "Berksfile has not been built for this cookbook. Specify --build to build one."
        exit()
else:
    print "INFO: With --existing any chef runs will use existing cookbooks from s3"
    tar_file = ""

#Prepare for chef run if needed
if args.chefrun:
    try:
        chef_ssmdoc = ssm_getdoc(ssm, chefdata["config"]["ssm_doc_filter"])
        print "Using document for chef runs: " + chef_ssmdoc
    except Exception as e:
        print "Error: Could not locate SSM Document (" + chefdata["config"]["ssm_doc_filter"] + ")"
        exit()


if specified_role != "all":
    if not args.existing:
        upload_cookbook(s3, company, specified_role, chefenv, tar_file, args.plan)
    if args.chefrun:
        ssm_chefrun(ssm, client, specified_role, chef_ssmdoc, args.plan)
else:
    for role in chefdata[cookbook]["roles"][chefenv]:
        print('')
        print ":::" + role + ":::"
        if not args.existing:
            upload_cookbook(s3, company, role, chefenv, tar_file, args.plan)
        if args.chefrun:
            ssm_chefrun(ssm, client, role, chef_ssmdoc, args.plan)
