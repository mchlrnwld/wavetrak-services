# sync-chef-zero

## About

Automates the berks package and upload to environment-specific chef-zero buckets in place of the [manual procedure](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/130820940/Chef+Zero)

## Environments

Environments are specified in `chef-data.json`:

```json
{
  "friendlyname": {
    "source":"../../chef/cookbooks/path-to-cookbook",
    "roles": {
      "sbox": ["friendlyname-sandbox","friendlyname-app","friendlyname-api"],
      "staging": ["friendlyname-app","friendlyname-api"],
      "prod": ["friendlyname-app","friendlyname-api"]
    }
  }
}
```

## Usage

### Package cookbook, push to S3, and update application instances

1. Build and upload a cookbook to an environment:

    ```bash
    python sync-chef-zero.py staging varnish --build
    ```

2. Run Chef on all instances matching the application and environment:

    ```bash
    python sync-chef-zero.py staging varnish --chefrun
    ```

### Deploy existing cookbook bundle to a single application

1. Deploy a existing build archive to single application:

    ```bash
    python sync-chef-zero.py staging/coldfusion-api coldfusion
    ```

### Perform a dry-run

Show plan of what will be done:

```bash
python sync-chef-zero.py staging coldfusion --plan

Processing cookbook: coldfusion(../../chef/cookbooks/surfline-coldfusion) for staging
PLAN: upload sl-chef-staging/coldfusion-admin/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-api-int/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-app/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-api/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-fishtrack/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-jobs/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-marineapi/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-qa/config/cookbooks-1522851730.tar.gz
cookbook package cookbooks-1522851730.tar.gz already exists for coldfusion-sandbox in sl-chef-staging
PLAN: upload sl-chef-staging/coldfusion-sciencejobs/config/cookbooks-1522851730.tar.gz
PLAN: upload sl-chef-staging/coldfusion-tile/config/cookbooks-1522851730.tar.gz
```
