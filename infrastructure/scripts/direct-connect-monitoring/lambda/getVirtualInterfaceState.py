from __future__ import print_function

import boto3
import json

VERSION = '1.0.0'

print('Loading function')

def lambda_handler(event, context):
    # default state
    unhealthyVirtualInterfaces = 0
    healthyVirtualInterfaces = 0
    virtualInterfaceHealth = ''

    # event data
    print("received event: %s" % json.dumps(event, indent=2))
    print("connectionId = %s" % event['connectionId'])
    print("region: %s" % event['region'])

    # connect to Direct Connect
    connectionId = event['connectionId']
    region = event['region']

    client = boto3.client('directconnect', region_name=region)
    response = client.describe_virtual_interfaces(connectionId=connectionId)

    # evaluate virtualInterface health
    for virtualInterface in response['virtualInterfaces']:
        print("virtualInterface %s is %s" % (virtualInterface['virtualInterfaceId'], virtualInterface['virtualInterfaceState']))
        if (virtualInterface['virtualInterfaceState'] == 'available'):
            healthyVirtualInterfaces += 1
        else:
            unhealthyVirtualInterfaces += 1
    print("healthy virtual interfaces: %s" % healthyVirtualInterfaces)
    print("unhealthy virtual interfaces: %s" % unhealthyVirtualInterfaces)

    # consider the connection to be unhealthy if even 1 interface is unhealthy
    if (unhealthyVirtualInterfaces >= 1):
        virtualInterfaceHealth = 'Unhealthy'
    elif (healthyVirtualInterfaces == 0 and unhealthyVirtualInterfaces == 0):
        virtualInterfaceHealth = 'Unknown'
    else:
        virtualInterfaceHealth = 'Healthy'
    print("connection is %s" % virtualInterfaceHealth)

    # return to client
    healthStatus = {
        'status': '200',
        'message': virtualInterfaceHealth,
        'awsStatus': response['ResponseMetadata']['HTTPStatusCode']
    }
    return healthStatus
