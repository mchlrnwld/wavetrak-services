/* Environment Variables */

var apiUrl = ''; /* API Gateway endpoint */
var apiBody = ''; /* JSON to send to API Gateway > Lambda */
var apiKey = ''; /* AWS API Gateway Key */

/* Core Script */

var request = require('request');
var options = {
  url: apiUrl, 
  body: apiBody, 
  headers: {
    'x-api-key': apiKey, 
    'Content-Type': 'application/json'
  }
};

function callback(err, res, data) {
  /* if there's an error during POST, log it */
	if (err) { console.log ("Network Error:", err); }
  else {
    /* there was data! so parse the data returned by Lambda */
    var apiReturn = JSON.parse(data);
    if (apiReturn.status === '200' && apiReturn.message === 'Healthy' && apiReturn.awsStatus === 200) {
      /* if return status is '200', message is 'Healthy' and awsStatus is 200 then the connection is probably up */
			console.log ("Direct Connect Status: OK");
      /* control GET request to Google to 'pass' the API Test because New Relic doesn't support SNI API endpoints */
      $http.get("http://www.google.com"); //.on('response', function(ctrlReturn) { console.log(ctrlReturn.statusCode); });
    } else {
      /* it failed, log it to console */
      console.log ("Direct Connect Status: FAIL");
    }
    /* log the complete return message to the console for debugging */
    console.log ("Return:", apiReturn);
  }
}

/* make the request */
request.post(options, callback);

