# Static redirects generator

A simple script that takes an XLS file with source and destination URLs, and creates a file with all the static redirects in an understandable configuration language for Nginx.

## Table of contents <!-- omit in toc -->

- [Installation](#installation)
- [Requirements](#requirements)
- [Usage](#usage)
  - [--input](#--input)
  - [--output](#--output)
  - [--cols (optional)](#--cols-optional)
- [Test](#test)
- [Uninstallation](#uninstallation)
- [It also works if](#it-also-works-if)

## Installation

This script is intended to run in a Conda environment, so having Anaconda or Miniconda installed, you'll need to run the next commands:

```console
conda env create
conda activate static-redirects-generator
```

## Requirements

The script assumes:

- All of the information is present in the first sheet of the document.
- The URLs must have their host names, not simple routes.
- One redirect per row.
- If a source URL is present, it must also have a destination URL in the same row.
- You can have a header rows or other notations in the document without any problem.

## Usage

```bash
python generator.py \
  --input XML_FILE \
  --output CONF_FILE
  [--cols COLUMN_NUMBERS]
```

### --input

The XLS file should have a column with all of the source URLs.

### --output

The XLS file should have another column with all of the destinations URLs next to the source URLs.

### --cols (optional)

The script assumes that the first and second columns belongs to the source and destination URLs. But in case the distribution in the file is different, you can set the column numbers separated by a comma, like having `--cols 1,3` or `--cols 4,2` as needed. Just take into account that the first number corresponds to the source URL and the second one to the destination URL.

**Example:**

Having an XLS file with this example format:

|Landing Page|Url Clicks|Surfline Redirect To URL|
|- |- |- |
|https://www.coastalwatch.com/|4000110|https://www.surfline.com|
|https://www.coastalwatch.com/surf-cams-surf-reports|20110|https://www.surfline.com/surf-reports-forecasts-cams/australia/2077456|
|https://www.coastalwatch.com/surf-cams-surf-reports/nsw/bondi-beach|18678|https://www.surfline.com/surf-report/bondi-beach/5842041f4e65fad6a7708bf8|
|...|...|...|

Because source and destination columns are 1 and 3, it will require that the arguments look like this:

```bash
time python generator.py --input "redirects.xlsx" --output "redirects.conf" --cols "1,3"
```

if everything went well, the console output should be like this:

```shellscript
[INFO] File created: redirects.conf
[INFO] Redirects: 26150

real  0m5.081s
user  0m4.964s
sys   0m0.117s
```

## Test

If the static redirects are meant to be added to our `web-proxy` configuration, please refer to the `redirectGeneration` utility in [surfline-web/web-proxy/redirectGeneration](https://github.com/Surfline/surfline-web/tree/master/web-proxy/utils/redirectGeneration) and follow instructions.

## Uninstallation

Running the `env remove` command will be enough to delete the conda environment, but first you'll want to deactivate it:

```bash
conda deactivate
conda env remove -n static-redirects-generator
```

## It also works if

You want to fetch URLs from an Excel file and export it to another kind of template file.

Just change the content of the `./template.j2` file.
