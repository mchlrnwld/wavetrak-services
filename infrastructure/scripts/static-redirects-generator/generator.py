#!/bin/env python

# Standard libraries.
import argparse
import datetime
import logging
import os
from urllib.parse import urlparse

# Third-party libraries.
from jinja2 import Environment as j2env
from jinja2 import FileSystemLoader as j2fs
from openpyxl import load_workbook


# Main function.
def execute():
    args = parse_arguments()

    try:
        if not os.path.exists(args.get("input")):
            raise FileNotFoundError(
                "File not found: '{}'"
                .format(args.get("input")))

        if not os.path.exists(os.path.dirname(args.get("output"))):
            raise FileNotFoundError(
                "Destination folder does not exists: '{}'"
                .format(os.path.dirname(args.get("output"))))

        cols = args.get("cols").split(",")
        if len(cols) != 2:
            raise ValueError(
                "Columns should have only 2 numeric values: '{}' provided"
                .format(args.get("cols")))
        col_orig = int(cols[0]) - 1
        col_dest = int(cols[1]) - 1

        sheet = load_workbook(filename=args.get("input")).active
        redirects = []
        for row in sheet.values:
            if len(row) < col_orig or len(row) < col_dest:
                raise TypeError(
                    "At least one of the columns provided is out of bounds: {}"
                    .format(cols))
            route = {}
            for i, val in enumerate(row):
                if i in [col_orig, col_dest]:
                    url = urlparse(val)
                    if url.netloc:
                        val = "{}{}{}".format(
                            "/" if not url.path else url.path,
                            "?" if url.query else "",
                            url.query)
                        column = "orig"
                        if i == col_dest:
                            column = "dest"
                        route[column] = val
                        if route.get("orig") != "/" and route.get("dest"):
                            redirects.append(route)

        if len(redirects):
            redirects = sorted(
                [dict(t) for t in {tuple(d.items()) for d in redirects}],
                key=lambda p: (p["orig"], p["dest"])
            )

        jinjaEnv = j2env(loader=j2fs("./"))
        template = jinjaEnv.get_template("template.j2")
        content = template.render(
            date=datetime.datetime.now(),
            redirects=redirects,
        )
        output = open(args.get("output"), "w")
        output.write(content)
        output.close()

        logging.info("File created: {}".format(args.get("output")))
        logging.info("Redirects: {}".format(len(redirects)))

    except Exception as err:
        logging.error(err)


# Handles the script arguments and options validation.
def parse_arguments():
    ap = argparse.ArgumentParser(
        description="""
            A simple script to grab redirect urls from a XLS file and export
            them to the needed template format for Nginx configuration.
        """,
    )
    ap.add_argument(
        "--input",
        help="the XLS source file location with the redirections",
        required=True,
    )
    ap.add_argument(
        "--output",
        help="the generated file location with the redirections",
        required=True,
    )
    ap.add_argument(
        "--cols",
        help="the column numbers to fetch urls from the file. E.g: '1,3'",
        default="1,2"
    )
    return vars(ap.parse_args())


if __name__ == "__main__":
    logging.basicConfig(
        format="{}".format("[%(levelname)s] %(message)s"),
        level=logging.INFO,
    )
    execute()
