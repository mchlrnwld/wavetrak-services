import boto3

elbv2 = boto3.client('elbv2')
cloudwatch = boto3.client('cloudwatch')

# How many columns of widgets should the dashboard have?
DASH_COLS = 3

# How tall should each widget be?
WIDGET_HEIGHT = 3

# Which dashboard should we update?
DASH_NAME = 'sl-core-services-individual-services-autoscaling'

TG_ARN_PREFIX = 'arn:aws:elasticloadbalancing:us-west-1:833713747344:'
LB_ARN_PREFIX = '{}loadbalancer/'.format(TG_ARN_PREFIX)

WIDGET_TPL = """
{{
    "type": "metric",
    "x": {x},
    "y": {y},
    "width": {w},
    "height": {h},
    "properties": {{
        "metrics": [
            [
                "AWS/ApplicationELB",
                "HealthyHostCount",
                "TargetGroup",
                "{tg}",
                "LoadBalancer",
                "{lb}",
                {{ "stat": "Maximum", "yAxis": "left" }}
            ]
        ],
        "period": 300,
        "stat": "Average",
        "start": "-P1D",
        "end": "P0D",
        "region": "us-west-1",
        "view": "timeSeries",
        "stacked": false,
        "title": "{name}"
    }}
}}
"""

DASHBOARD_TPL = """
{{
    "widgets": [
        {{
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 24,
            "height": 6,
            "properties": {{
                "metrics": [
                    [ "AWS/AutoScaling", "GroupInServiceInstances", "AutoScalingGroupName", "sl-core-svc-ecs-prod", {{ "stat": "Average" }} ]
                ],
                "period": 300,
                "stat": "Average",
                "start": "-PT3H",
                "end": "P0D",
                "region": "us-west-1",
                "view": "timeSeries",
                "stacked": false,
                "title": "Core Services Host Count"
            }}
        }},
        {widgets}
    ]
}}
"""

tgroups = elbv2.describe_target_groups()

tgroups_strings = []
for idx, tg in enumerate(sorted(tgroups['TargetGroups'], key=lambda k: k['TargetGroupName'])):
    try:
        name = tg['TargetGroupName']
        tg_arn = tg['TargetGroupArn'].replace(TG_ARN_PREFIX, '')
        lb_arn = tg['LoadBalancerArns'][0].replace(LB_ARN_PREFIX, '')

        tgroups_strings.append(
            WIDGET_TPL.format(
                x=int(((idx % DASH_COLS) * 24 / DASH_COLS)),
                y=int((idx / DASH_COLS + 1)),
                w=int((24 / DASH_COLS)),
                h=WIDGET_HEIGHT,
                tg=tg_arn,
                lb=lb_arn,
                name=name
            )
        )
    except IndexError:
        print('[WARNING] {} has no load balancer. Skipping...'.format(name))


response = cloudwatch.put_dashboard(
    DashboardName=DASH_NAME,
    DashboardBody=DASHBOARD_TPL.format(
        widgets=','.join(tgroups_strings),
        index=len(tgroups_strings)
    )
)

print(response)
