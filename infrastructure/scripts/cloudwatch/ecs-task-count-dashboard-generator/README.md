# ECS Task Count Dashboard Generator

This Python notebook builds a Cloudwatch dashboard showing all target groups for the given environment with their respective healthy host counts. 

It's best used to monitor service autoscaling.

## Quick Start

1. Set AWS environment variables

    ```bash
    export AWS_ACCESS_KEY_ID=<redacted>
    export AWS_SECRET_ACCESS_KEY=<redacted>
    export AWS_DEFAULT_REGION=us-west-1
    ```

1. Run a virtual environment

    ```bash
    virtualenv ~/dev/venvs/ecs-task-count-dashboard-generator
    ```

1. Install dependencies

    ```bash
    pip install -r requirements.txt
    ```

1. Start the notebook server


    ```bash
    jupyter notebook
    ```

1. Load the `ecs-task-count-dashboard-generator.ipynb` Python notebook

1. Configure the desired number of dashboard columns `DASH_COLS` and widget height `WIDGET_HEIGHT` (or use defaults)

1. Run all cells

1. Confirm that the `cloudwatch.put_dashboard()` call in the last cell executed successfully

1. Browse to the Cloudwatch dashboard

    - <https://us-west-1.console.aws.amazon.com/cloudwatch/home?region=us-west-1#dashboards:name=sl-core-services-individual-services-autoscaling>
