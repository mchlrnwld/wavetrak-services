#!/bin/bash
set -euxo pipefail

function info {
  echo "[INFO] ${*}"
}

# Add to path for assume-role
export PATH="${PATH}:/usr/local/bin"

info "build description:${COOKBOOK}|${ENVIRONMENT}|${VERSION}|"

# Trap script errors
trap catch_errors ERR;

function catch_errors() {
  echo "script aborted, because of errors"
  exit 1
}

S3_CHEF_DEPLOYMENT_BUCKET="sl-chef-${ENVIRONMENT}"

info "Assuming proper AWS profile:"
eval "$(assume-role "${ENVIRONMENT}")"

# Generate cookbook bundle with berks
cd chef
docker run \
  --rm \
  -v "$(pwd)":/opt/chef \
  -v "$(pwd)/.chef":/root/.chef \
  -w /opt/chef/cookbooks/"${COOKBOOK}"/ \
  zuazo/chef-local:ubuntu-16.04 \
  /bin/bash -c "berks package;
                chown -R 495:493 /opt/chef/cookbooks/*" # Run chown on the jenkins user / group id number so server jenkins user has permission to clear out workspace next run

# Untar bundle. Add data_bags directory. Retar.
cd cookbooks/"${COOKBOOK}"
ALL_BUNDLES=(cookbooks-*.tar.gz)
COOKBOOK_FILENAME="${ALL_BUNDLES[@]: -1}"
export COOKBOOK_FILENAME
tar -zxvf "${COOKBOOK_FILENAME}"
rm "${COOKBOOK_FILENAME}"
cp -r ../../data_bags ./
tar -zcvf "${COOKBOOK_FILENAME}" cookbooks data_bags

# Push to S3
info "Pushing cookbook to S3:"
aws s3 cp "${COOKBOOK_FILENAME}" s3://"${S3_CHEF_DEPLOYMENT_BUCKET}"/"${COOKBOOK#surfline-}"/config/"${COOKBOOK_FILENAME}"
