#!/usr/bin/env bash
#
# Description: Restarts ColdFusion and related components while performing a number of health and sanity checks
# Version: 1.0.0
#

# service name definitions
CF_SERVICE="coldfusion_8"
CRON_SERVICE="crond"
HTTP_SERVICE="httpd"


# require root / sudo
if [ "$UID" -ne 0 ] ; then
  echo "ERROR: script must be ran with root permissions"
  exit 1
fi


# command system services
service_handler () {
  local SERVICE_NAME="$1"
  local SERVICE_COMMAND="$2"
  echo "INFO: sending $SERVICE_COMMAND to $SERVICE_NAME"
  /sbin/service $SERVICE_NAME $SERVICE_COMMAND
}


# truncate log files
log_handler () {
  local CFLOGDIR="/opt/coldfusion8/logs"
  echo "INFO: truncating logs in $CFLOGDIR"
  pushd $CFLOGDIR
  /bin/rm -f *.log.*
  for f in *.log ; \
    do cat /dev/null > $f ; \
  done
  popd
}


# sleep timer
sleep_handler () {
  local SLEEPTIME="$1"
  while [ $SLEEPTIME -gt 0 ]; do
    echo -ne "INFO: continuing in $SLEEPTIME\033[0K\r"
    sleep 1
    : $((SLEEPTIME--))
  done
}


# clear CF class cache
cf_class_cache_handler () {
  local CFCLASSCACHEDIR="/opt/coldfusion8/wwwroot/WEB-INF/cfclasses"
  echo "INFO: clearing CF class cache"
  /bin/rm -f $CFCLASSCACHEDIR/*.class
}


# check the license file
cf_license_handler() {
  local CFLICENSEFILE="/opt/coldfusion8/lib/license.properties"
  local CFLICENSECHECKSUM=$(wc -l $CFLICENSEFILE | awk ' { print $1 } ')
  if [ $CFLICENSECHECKSUM -eq 12 ] ; then
    echo "INFO: CF license is valid"
  else
    echo "WARNING: CF license is invalid - fixing"
    # BEGIN HERE DOC - THE LINES AFTER 'cat' MUST BEGIN WITH TABS - NOT SPACES!
    cat > $CFLICENSEFILE <<-EOF
			#CF License File
			#Wed Nov 18 01:36:42 PST 2015
			appserver=0V\=LUFWP]J(Z^V)[G?G5'C@	\n
			code=6702397151437044636
			sn=1185-5049-5670-4917-9240-3949
			listen=true
			user=
			installlanguage=en
			installtype=standalone
			company=
			previous_sn=
			vendor=0V\=LUFWP]J(Z^V)[G?G5'C@	\n
		EOF
    # END HERE DOC -
  fi
}


# stop services
service_handler $CRON_SERVICE stop
service_handler $HTTP_SERVICE stop
service_handler $CF_SERVICE stop

# clean up and sanity checks
log_handler
cf_class_cache_handler
cf_license_handler

# start services
service_handler $CF_SERVICE start
sleep_handler 90
service_handler $HTTP_SERVICE start
sleep_handler 30
service_handler $CRON_SERVICE start
