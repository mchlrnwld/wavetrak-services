"""
sturdy-s3-migrator

Usage:
  sturdy-s3-migrator [--pattern=<re>] [--after=<date>] prepare <jobname> <srcbucket> <dstbucket>
  sturdy-s3-migrator [--concurrency=<n>] run <jobname>
  sturdy-s3-migrator -h | --help
  sturdy-s3-migrator --version

Options:
  -h --help                         Show this screen.
  --concurrency=<n>                 Number of concurrent transfers [default: 10].
  --pattern=<re>                    The key matching pattern to use [default: .*].
  --after=<date>                    A date, in any reasonable format
  --version                         Show version.

Examples:
  sturdy-s3-migrator prepare myjob my-src my-dst
  sturdy-s3-migrator run myjob
"""

import dateutil
import boto3
from docopt import docopt

from . import __version__ as VERSION  # pylint: disable=E0611,W0406
from manifest import Manifest
from transfer_manager import TransferManager


class App(object):
    """The whole app"""

    S3 = boto3.client('s3')
    S3R = boto3.resource('s3')

    def __init__(self, jobname):
        self.jobname = jobname
        self.manifest = Manifest(jobname)

    def prepare(self, srcbucket, dstbucket, pattern, after):
        if after:
            after = dateutil.parser.parse(after)

        self.manifest.create(srcbucket, dstbucket, pattern, after)

    def run(self, concurrency):
        self.manifest.load()
        TransferManager(self.manifest, concurrency).run()


def main():
    """Main CLI entrypoint."""
    # boto3.set_stream_logger('')
    options = docopt(__doc__, version=VERSION)
    app = App(options['<jobname>'])

    if options['prepare']:
        app.prepare(options['<srcbucket>'], options['<dstbucket>'], options['--pattern'], options['--after'])

    if options['run']:
        app.run(int(options['--concurrency']))
