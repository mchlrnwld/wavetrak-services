"""Manages the actual transfers"""

import threading
import time
from collections import deque

import boto3
import botocore

from atomic_counter import AtomicCounter


class TransferManager(object):
    """Manages S3 copying"""

    def __init__(self, manifest, concurrency):
        self.manifest = manifest
        self.concurrency = concurrency
        self.queue = deque(self.manifest.files)
        self.srcbucket = self.manifest.srcbucket
        self.dstbucket = self.manifest.dstbucket
        self.total_size = sum(x['Size'] for x in self.queue)
        self.S3 = boto3.client('s3', config=botocore.config.Config(max_pool_connections=self.concurrency*2))

    def run(self):
        print "Starting transfers - %s files, %s mb queued" % (len(self.queue), self.total_size/1024/1024)

        total_files = AtomicCounter()
        total_bytes = AtomicCounter()
        threads = []

        start_time = time.time()

        for x in range(0, self.concurrency):
            t = threading.Thread(target=self.worker, args=[total_files, total_bytes])
            threads.append(t)
            t.start()
            if x % 100 == 0:
                time.sleep(1.2)
                print "%s threads started" % x

        print "All threads started"

        while True in (t.is_alive() for t in threads):
            runtime = time.time() - start_time
            files_per_sec = int(total_files.value / runtime)
            mb_per_sec = int(total_bytes.value/1024/1024 / runtime)
            print "Running (%s min): %s files (%s/sec) / %s mb (%s/sec) completed" % (
                int(runtime/60),
                total_files.value,
                files_per_sec,
                total_bytes.value/1024/1024,
                mb_per_sec
            )
            time.sleep(1)

        # We have to join them or we'll never exit
        for t in threads:
            t.join()

        print "All threads finished"
        print "Done: %s files / %s mb completed" % (total_files.value, total_bytes.value/1024/1024)

    def worker(self, total_files, total_bytes):
        try:
            while True:
                self.transfer_file(self.queue.popleft(), total_files, total_bytes)
        except IndexError:
            pass

    def transfer_file(self, item, total_files, total_bytes):
        key = item['Key']

        self.S3.copy_object(
            Bucket=self.dstbucket,
            Key=key,
            CopySource={
                'Bucket': self.srcbucket,
                'Key': key
            }
        )

        self.manifest.file_complete(key)

        total_files.increment()
        total_bytes.increment(item['Size'])
