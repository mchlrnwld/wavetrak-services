"""Manages the manifest of files"""

import sqlite3
import os.path
import threading
import sys
import time
import datetime
import dateutil
import re
import urllib
from random import randrange
import boto3

from contextlib import contextmanager
from collections import deque
from atomic_counter import AtomicCounter

NUM_LOADING_THREADS = 10


class Manifest(object):

    S3 = boto3.client('s3')

    def __init__(self, jobname):
        self.jobname = jobname
        self.db_file = 'job-%s.db' % self.jobname
        self.srcbucket = None
        self.dstbucket = None
        self.pattern = None
        self.after = None
        self.files = None
        self.local = threading.local()

    def create(self, srcbucket, dstbucket, pattern, after):
        if (os.path.exists(self.db_file)):
            raise Exception('Cannot prepare if database already exists. Try deleting %s' % self.db_file)

        self.srcbucket = srcbucket
        self.dstbucket = dstbucket
        self.pattern = pattern
        self.after = after

        self.prepare_schema()
        self.load_metadata()
        self.load_objects()

    def load(self):
        with self.cursor() as c:
            c.execute('SELECT * FROM metadata')
            (self.srcbucket, self.dstbucket, self.pattern) = c.fetchone()

        with self.cursor() as c:
            c.execute('SELECT key, size FROM files WHERE complete=0 ORDER BY num')
            self.files = ({
                'Key': x[0],
                'Size': x[1]
            } for x in c.fetchall())

    def file_complete(self, key):
        with self.cursor() as c:
            c.execute('UPDATE files SET complete=1 WHERE key=?', [key])

    def prepare_schema(self):
        with self.cursor() as c:
            c.execute('CREATE TABLE metadata (srcbucket text not null, dstbucket text not null, pattern text not null)')
            c.execute('CREATE TABLE files (key text primary key not null, num integer not null, size integer, complete integer default 0)')
            c.execute('CREATE INDEX file_num_index ON files (num)')

    def load_metadata(self):
        with self.cursor() as c:
            c.execute('INSERT INTO metadata (srcbucket, dstbucket, pattern) VALUES (?,?,?)',
                      (self.srcbucket, self.dstbucket, self.pattern))

    def load_objects(self):
        """
        The strategy here is to load the top level list of directories,
        then go into each directory and get all contents (i.e. without further
        delimiter magic -- just a flat listing of everything). This allows
        us to use the top-level directories as a way to load the list in
        parallel, but avoids going crazy with the recursion.

        Note: this currently does not handle any files at the top-level.
        It'll kinda start to work, but the recursion bit will fail because
        I didn't finish it.
        """

        start = datetime.datetime.now(dateutil.tz.tzutc())

        print "Using pattern: %s" % self.pattern

        if self.after:
            print "User after: %s" % self.after

        paginator = self.S3.get_paginator('list_objects_v2')
        pagination = paginator.paginate(
            Bucket=self.srcbucket,
            Delimiter='/',
            EncodingType='url',
            Prefix='')

        res = deque(
            urllib.unquote(x.get('Key', x.get('Prefix')))
            for x in pagination.search('[CommonPrefixes,Contents][]'))

        match_counter = AtomicCounter()
        total_counter = AtomicCounter()
        threads = []

        for x in range(0, NUM_LOADING_THREADS):
            t = threading.Thread(target=self.load_objects_worker, args=[res, match_counter, total_counter])
            threads.append(t)

        # Start all threads
        for t in threads:
            t.start()

        print "All threads started"

        while True in (t.is_alive() for t in threads):
            print "Loading manifest, so far: %s matched out of %s evaulated" % (match_counter.value, total_counter.value)
            time.sleep(1)

        # We have to join them or we'll never exit
        for t in threads:
            t.join()

        print "All threads finished"
        print "Done: %s matched out of %s evaulated" % (match_counter.value, total_counter.value)
        print "Suggested 'after' value for next run is: %s" % (start - datetime.timedelta(minutes=1)).isoformat()

    def load_objects_worker(self, queue, match_counter, total_counter):
        try:
            while True:
                self.load_sub_objects(queue.popleft(), match_counter, total_counter)
        except IndexError:
            pass

    def load_sub_objects(self, path, match_counter, total_counter):
        print "Working on path: %s" % path

        pattern_re = re.compile(self.pattern)

        paginator = self.S3.get_paginator('list_objects_v2')
        pagination = paginator.paginate(
            Bucket=self.srcbucket,
            EncodingType='url',
            Prefix=path)

        for page in pagination:
            items = list((urllib.unquote(x['Key']), x['Size'], randrange(0, sys.maxint))
                         for x in page['Contents']
                         if pattern_re.match(x['Key']) and
                         ((not self.after) or (x['LastModified'] > self.after)))

            with self.cursor() as c:
                c.executemany('INSERT INTO files (key,size,num) VALUES (?,?,?)', items)

            match_counter.increment(len(items))
            total_counter.increment(len(page['Contents']))

    @contextmanager
    def cursor(self):
        try:
            conn = self.local.conn
        except AttributeError:
            self.local.conn = sqlite3.connect(self.db_file, timeout=120000)
            conn = self.local.conn

        with conn:
            c = conn.cursor()
            yield c
            c.close()
