# sturdy-s3-migrator

A tool to move files around in S3

## Usage

```
sturdy-s3-migrator
Usage:
  sturdy-s3-migrator [--pattern=<re>] [--after=<date>] prepare <jobname> <srcbucket> <dstbucket>
  sturdy-s3-migrator [--concurrency=<n>] run <jobname>
  sturdy-s3-migrator -h | --help
  sturdy-s3-migrator --version
Options:
  -h --help                         Show this screen.
  --concurrency=<n>                 Number of concurrent transfers [default: 10].
  --pattern=<re>                    The key matching pattern to use [default: .*].
  --after=<date>                    A date, in any reasonable format
  --version                         Show version.
Examples:
  sturdy-s3-migrator prepare myjob my-src my-dst
  sturdy-s3-migrator run myjob
```
  
## Quick Start
  
1. **Prepare job**

    This generates a local list of all files in your source directory.

    Example:
    
    ```bash
    sturdy-s3-migrator prepare move-recordings s3://live-cam-archive/ s3://cam-rewind-cold-storage/
    ```

2. **Run job**

    This does the copy. If this is interrupted, it can be run again and it will resume.

    Example:
    
    ```bash
    sturdy-s3-migrator run move-recordings
    ```