#!/usr/bin/env python
'''
DIY cluster controller
'''

import argparse
import boto.ec2
import os
import requests
import sys


# exit status constants
EXIT_NO_ERROR = 0
EXIT_GENERAL_ERROR = 1
EXIT_TAG_NOT_FOUND = 2
EXIT_UNKNOWN_NFS_STATE = 3
EXIT_INVALID_NODE_COUNT = 4
EXIT_UNSUPPORTED_SERVICE_TYPE = 5

# counters
activenodecount = 0
passivenodecount = 0

# parse parameters and options
parser = argparse.ArgumentParser(description='check service role state and reassign as needed')

parser.add_argument('-t', '--instance-tag',
                    dest='instancetag',
                    help='instance tag to check for',
                    required=False,
                    default='nfs-role')
parser.add_argument('-s', '--service-type',
                    dest='servicetype',
                    help='type of service for health check',
                    required=False,
                    default='nfs',
                    choices=['http', 'nfs', 'tcp'])
parser.add_argument('-a', '--active-condition',
                    dest='activecond',
                    help='tag value to set/check active condition',
                    required=False,
                    default='active')
parser.add_argument('-p', '--passive-condition',
                    dest='passivecond',
                    help='tag value to set/check passive condition',
                    required=False,
                    default='passive')

args = parser.parse_args()

# set variables based on parameters
instancetag = args.instancetag
activecond = args.activecond
passivecond = args.passivecond
servicetype = args.servicetype

# get our instance data in JSON
instance_data = requests.get('http://169.254.169.254/latest/dynamic/instance-identity/document').json()

# loop through the instance data and extract region and instanceId
for d in instance_data.iteritems():
    d_name = d[0]
    d_detail = d[1]
    if d_name == 'region':
        region = d_detail

# connect to EC2 and identify NFS instances
conn = boto.ec2.connect_to_region(region)
reservations = conn.get_all_reservations()
for res in reservations:
    for inst in res.instances:
        if instancetag in inst.tags:
            # it's a match
            instanceId = inst.id
            instanceAddress = inst.private_ip_address
            rolestate = inst.tags[instancetag]
            print 'Info: found %s server: %s' % (servicetype, instanceId)
            # active node
            if rolestate == activecond:
                activenodeid = instanceId
                activenodeip = instanceAddress
                activenodecount += 1
                print 'Info: instance %s is in state: %s' % (activenodeid, rolestate)
            # passive node
            elif rolestate == passivecond:
                passivenodeid = instanceId
                passivenodeip = instanceAddress
                passivenodecount += 1
                print 'Info: instance %s is in state: %s' % (passivenodeid, rolestate)
            # it has the tag but unknown value
            else:
                print 'Error: instance %s is in unknown state' % (instanceId)
                sys.exit(EXIT_UNKNOWN_NFS_STATE)

# we should have exactly one of each rolestate otherwise something is wrong
if (activenodecount != 1) or (passivenodecount != 1):
    print 'Error: invalid number of nodes detected'
    sys.exit(EXIT_INVALID_NODE_COUNT)

# check the health of the active node
if servicetype == 'http':
    print 'Error: %s not yet supported' % (servicetype)
    sys.exit(EXIT_UNSUPPORTED_SERVICE_TYPE)
elif servicetype == 'nfs':
    print 'Info: checking health of %s (%s)' % (activenodeid, activenodeip)
    health = os.system('rpcinfo -p ' + activenodeip)
elif servicetype == 'tcp':
    print 'Error: %s not yet supported' % (servicetype)
    sys.exit(EXIT_UNSUPPORTED_SERVICE_TYPE)
else:
    print 'Error: not yet supported'
    sys.exit(EXIT_UNSUPPORTED_SERVICE_TYPE)


# take action
if health > 0:
    print 'Warning: active node %s (%s) unhealthy. Switching %s role to %s (%s)' % (activenodeid, activenodeip, activecond, passivenodeid, passivenodeip)
    # switch roles
    conn.create_tags([activenodeid], {instancetag: passivecond})
    conn.create_tags([passivenodeid], {instancetag: activecond})
    # assign the IP
else:
    print 'Info: active node %s (%s) is healthy' % (activenodeid, activenodeip)
    sys.exit(EXIT_NO_ERROR)
