#!/usr/bin/env python
'''
rsync to remote host based on tag condition
'''

import argparse
import boto.ec2
import requests
import subprocess
import sys


# exit status constants
EXIT_NO_ERROR = 0
EXIT_GENERAL_ERROR = 1
EXIT_TAG_NOT_FOUND = 2


# parse parameters and options
parser = argparse.ArgumentParser(description='rsync to remote host based on tag condition')

parser.add_argument('-H', '--dest-host',
                    dest='desthost',
                    help='destination host to sync to',
                    required=True)
parser.add_argument('-P', '--dest-path',
                    dest='destpath',
                    help='destination path to sync to',
                    required=True)
parser.add_argument('-U', '--dest-user',
                    dest='destuser',
                    help='destination user to sync as',
                    required=False,
                    default='root')
parser.add_argument('-s', '--src-path',
                    dest='srcpath',
                    help='source path to sync from',
                    required=True)
parser.add_argument('-t', '--instance-tag',
                    dest='instancetag',
                    help='instance tag to use for sync condition',
                    required=False,
                    default='nfs-role')
parser.add_argument('-a', '--active-condition',
                    dest='activecond',
                    help='tag value to set active condition',
                    required=False,
                    default='active')
parser.add_argument('-p', '--passive-condition',
                    dest='passivecond',
                    help='tag value to set passive condition',
                    required=False,
                    default='passive')

args = parser.parse_args()


# set variables based on parameters
desthost = args.desthost
destpath = args.destpath
destuser = args.destuser
srcpath = args.srcpath
instancetag = args.instancetag
activecond = args.activecond
passivecond = args.passivecond


# get our instance data in JSON
instance_data = requests.get('http://169.254.169.254/latest/dynamic/instance-identity/document').json()

# loop through the instance data and extract region and instanceId
for d in instance_data.iteritems():
    d_name = d[0]
    d_detail = d[1]
    if d_name == 'region':
        region = d_detail
    elif d_name == 'instanceId':
        instanceId = d_detail

# connect to EC2 and check instance status/tag
conn = boto.ec2.connect_to_region(region)
reservations = conn.get_all_reservations()
for res in reservations:
    for inst in res.instances:
        if inst.id == instanceId:
            if instancetag in inst.tags:
                # we're an NFS server
                rolestate = inst.tags[instancetag]
                print 'Role state: %s' % rolestate
            else:
                # nothing to do here
                print 'Error: instance tag not found'
                sys.exit(EXIT_TAG_NOT_FOUND)


# if active, sync
if rolestate == activecond:
    # sync
    src = srcpath
    dest = destuser + '@' + desthost + ':' + destpath
    print 'syncing...'
    subprocess.Popen(['rsync', '-xav', '--delete', src, dest])
    sys.exit(EXIT_NO_ERROR)
else:
    # don't sync
    print 'not syncing...'
    sys.exit(EXIT_NO_ERROR)
