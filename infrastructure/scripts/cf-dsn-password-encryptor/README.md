# CF DSN Password Encryptor

## Summary

ColdFusion uses a particular encryption format for the passwords in its DSNs. These are stored in `neo-datasource.xml` on ColdFusion application servers. Since we dynamically build `neo-datasource.xml` on server bootstrapping, we need a utility to encrypt DSN passwords outside of ColdFusion. 

## Dependencies

- `docker`
- `make`

## Usage

### Build Container

```sh
make docker-build
```

### Encrypt Password

```sh
docker run cf-dsn-password-encryptor password-to-be-encrypted

# =>

plaintextPassword: password-to-be-encrypted
encryptedPassword: Zn0oZAYvGeRq4YYWpJaSBq7D0qS8iKXj1hgPMXu63eM=
```