import coldfusion.runtime.CFPage;

/*
 * This class encrypts a ColdFusion8 DSN password so that it can be
 * dynamically included as configuration in neo-datasource.xml.
 */
class CFDSNPasswordEncryptor {
    public static void main(String[] args) {
        if(args.length != 1 || args[0] == "") {
            System.out.println("Usage: java CFDSNPasswordEncryptor plaintext_password");
            System.exit(1);
        }

        // Including the 3DES key in source is insecure but this key
        // has been widely compromised. Given this fact, the simplicity
        // of keeping it in code outweighs the security implications.
        String secKey = CFPage.generate3DesKey("0yJ!@1$r8p0L@r1$6yJ!@1rj");

        String plaintextPassword = args[0];
        System.out.println("plaintext: " + plaintextPassword);

        String encryptedPassword = CFPage.Encrypt(plaintextPassword, secKey, "DESede", "Base64");
        System.out.println("encrypted: " + encryptedPassword);
    }
}

