# User Control Package

This is a Python package that is used to create and delete accounts in the Surfline Database. It should **NOT** be used on the production databse.

## Installing from Artifactory

### Artifactory Access

1.  Contact a Surfline admin (mwalker, mchivers)
2.  surfline.jfrog.io
3.  follow the instructions to add `pypi-dev` to your pip.config
4.  Its likely to be something similar to:
    ```
    [Default]
    extra-index-url = https://surfline.jfrog.io/surfline/api/pypi/pypi-dev/simple
    ```
    I recommend adding the repo ad an `extra-index-url` so that you can still fetch packages from the normal pypi repo.

### Instructions

1.  Add the pypi-local-dev server to your python config
2.  Run `pip install surfline-users-management`
3.  Create a [.slusers.config](./.slusers.config.sample) file in in your **home** folder.
4.  Update the config file with your mongodb connection string and stripe api key
    - Contact [mchivers](mchivers@surfline.com) if you need help with this.
5.  Run the program with `slusers <commands here>`

## For a local installation

1.  Create a [.slusers.config](./.slusers.config.sample) file in in your **home** folder.
2.  Update the config file with your mongodb connection string and stripe api key
3.  Run `pip install wheel setuptools`
4.  Run `pip install .`
5.  Run the program with `slusers <commands here>`

Created Users default to premium monthly (non-trialing) and expire in 30 days.
Password defaults to `surfline`

## Usage

`slusers create <email>`

`slusers create --trial false --expiring true <email>`

`slusers delete <email>`

## Development

### Push to Artifactory

1.  Switch to python version for building
2.  Run `python setup.py bdist_wheel upload -r dev`
3.  Repeat for other python versions as needed

## TODO:

- custom password
- custom datetime for plans, user creation, and last updated
- user info carrying over to stripe. Its using
  ```
  "name": "full name",
  "firstName": "firstName",
  "lastName": "lastName"
  ```
- selecting which plan expired users had
  - defaults to monthly_v2 right now
- updating users with new info
  - e.g. expiring a subscription that was created previously
