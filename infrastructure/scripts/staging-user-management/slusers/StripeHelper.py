""" Stripe Wrapper for SL Automation helper class """
import os
import time
import datetime
import stripe


class StripeHelper:
    """Helper Class for Surfline Stripe Automated Testing
    Many of these methods are specificly designed for automation reusability
    """

    def __init__(self, api_key, api_version):
        stripe.api_key = api_key
        stripe.api_version = api_version

    def create_customer(self, customer_email, customer_sl_id):
        """
        creates a test customer with some prefilled data:

        name=``full name``
        firstName=``firstName``
        lastName=``lastName``
        """
        customer = stripe.Customer.create(
            description="automation account",
            email=customer_email,
            metadata={
                "name": "full name",
                "firstName": "firstName",
                "lastName": "lastName",
                "userId": customer_sl_id
            }
        )
        return customer["id"]

    def add_test_card(self, customer_id):
        """
        Adds a standard Visa test card to the selected customer
        """
        customer = stripe.Customer.retrieve(customer_id)
        exp_month = int(datetime.date.today().month)
        exp_year = int(datetime.date.today().year) - 1999
        customer.sources.create(source={
            "object": "card",
            "exp_month": exp_month,
            "exp_year": exp_year,
            "number": "4242424242424242",
            "cvc": "123"
        })

    def create_subscription(self, customer_id, plan_id, trial_end_timestamp):
        """Creates a Stripe subscription for the customer with/without a trial

        trial_end_timestamp=when to end the trial. ``now`` ends immediately

        returns a subscription ID`
        """
        subscription_id = stripe.Subscription.create(
            customer=customer_id,
            items=[
                {
                    "plan": plan_id,
                },
            ],
            trial_end=trial_end_timestamp
        )["id"]
        return subscription_id

    def cancel_subscription(self, subscription_id, at_period_end):
        """retrieves the selected subscription id
        and deletes it using the Stripe API
        if at_period_end is true, then expiring at period end
        """
        try:
            stripe.Subscription.retrieve(subscription_id).delete(
                at_period_end=at_period_end)
        except stripe.error.InvalidRequestError as reqestError:
            print(reqestError)
            print('Review the error and check the Stripe Dashboard if needed.')
            pass
        finally:
            print('From Stripe canceled: {}'.format(subscription_id))

    def delete_customer(self, customer_id):
        """
        retrieves the selected customer id
        and deletes it using the Stripe API
        """
        try:
            stripe.Customer.retrieve(customer_id).delete()
        except stripe.error.InvalidRequestError as reqestError:
            print(reqestError)
            print('Review the error and check the Stripe Dashboard if needed.')
            pass
        finally:
            print('From Stripe deleted: {}'.format(customer_id))
