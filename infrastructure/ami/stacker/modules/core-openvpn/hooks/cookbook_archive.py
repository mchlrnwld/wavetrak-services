"""Handles tasks related to cookbook archives."""

from contextlib import contextmanager
from subprocess import check_call

import logging
import os
import re

from stacker.lookups.handlers.default import handler as default_handler
from stacker.lookups.handlers.output import handler as output_handler
from stacker.session_cache import get_session
from stacker.util import get_config_directory

LOGGER = logging.getLogger(__name__)
COOKBOOK_PKG_PATTERN = re.compile(r'^cookbooks-[0-9]*\.(tar\.gz|tgz)')


@contextmanager
def change_dir(newdir):
    """Change directory.

    Adapted from http://stackoverflow.com/a/24176022

    """
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def get_cookbook_archive_filename(path):
    """Find highest numbered cookbook archive file in `path`.

    Args:
        path (str): Filesystem path

    Returns: string of filename

    """
    for i in sorted(os.listdir(path), reverse=True):
        if COOKBOOK_PKG_PATTERN.match(i):
            return i


def purge_cookbook_archives(path):
    """Remove all cookbook archives from `path`.

    Args:
        path (str): Filesystem path

    """
    for i in os.listdir(path):
        if COOKBOOK_PKG_PATTERN.match(i):
            LOGGER.info("Deleting existing cookbook archive %s.", i)
            os.remove(os.path.join(path, i))


def package_and_upload(provider, context, **kwargs):  # pylint: disable=W0613
    """Ensure a cookbook archive is present on S3.

    Args:
        provider (:class:`stacker.providers.base.BaseProvider`): provider
            instance
        context (:class:`stacker.context.Context`): context instance

    Returns: boolean for whether or not the hook succeeded.

    """
    always_upload_new_archive = kwargs.get('always_upload_new_archive', False)
    environment = kwargs.get('environment', 'common')
    cookbook_relative_path = default_handler(
        kwargs.get('cookbook_relative_path_default'),
        provider=provider,
        context=context
    )
    cookbook_path = os.path.normpath(
        os.path.join(os.path.abspath(get_config_directory()),
                     cookbook_relative_path)
    )
    LOGGER.debug("cookbook path found to be %s.", cookbook_path)
    chef_config_bucket = output_handler(
        kwargs.get('chef_config_bucket_output'),
        provider=provider,
        context=context
    )
    bucket_key = default_handler(
        kwargs.get('s3_bucket_key_default'),
        provider=provider,
        context=context
    )
    bucket_prefix = '%s/%s' % (environment, bucket_key)

    session = get_session(provider.region)
    client = session.client('s3')

    if always_upload_new_archive is False:
        list_results = client.list_objects(
            Bucket=chef_config_bucket,
            Prefix='%s/cookbooks-' % bucket_prefix
        )
        if 'Contents' in list_results:
            LOGGER.info('Skipping cookbook upload; archive already present '
                        'on S3.')
            return True
    # Before creating a fresh cookbook archive, ensure no existing ones are
    # present (as they'll be duplicated in the archive)
    purge_cookbook_archives(cookbook_path)

    # Generate the cookbook archive
    with change_dir(cookbook_path):
        check_call(['berks', 'package', '--except=integration'])

    # Find the archive filename
    cookbook_archive = get_cookbook_archive_filename(cookbook_path)
    # Upload it to S3
    client.put_object(Body=open(os.path.join(cookbook_path,
                                             cookbook_archive), 'rb'),
                      Bucket=chef_config_bucket,
                      Key='%s/%s' % (bucket_prefix, cookbook_archive))
    return True
