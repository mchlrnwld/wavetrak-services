"""Version module for repo."""
__version__ = "8.3.8"


def version():
    """Return current version number."""
    return __version__
