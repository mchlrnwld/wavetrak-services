# sturdy-stacker-core CHANGELOG

This file is used to list changes made in each version of the
sturdy-stacker-core Stacker blueprint collection.

## 8.3.8 (2017-09-27)

* roles: Add ssm:DescribeParameters * permissions to base policy
    * Will allow wildcard lookups of parameters under `all.`

## 8.3.7 (2017-09-20)

* vpc: Add CidrBlock output

## 8.3.6 (2017-08-11)

* cw_alarm: Make `AlertTopicArn` a comma delimited list parameter
    * Backwards compatible with existing single topic uses, but allows multiple ARNs to be specified
* cw_alarm: Make `CustomerName` a required parameter
    * Should functionally be backwards compatible as there were no uses without the parameter being specified

## 8.3.5 (2017-08-10)

* Add `ComparisonOperator` parameter to cloudwatch alarm blueprint

## 8.3.4 (2017-07-28)

* Update yaml configs to use the new specific hook/blueprint directory symlinks

## 8.3.3 (2017-07-27)

* Add `sturdy_stacker_core_hooks` symlink to hooks directory.
  * Symlink is for future uses of repo where multiple `hooks` directories may be present in a system python path (causing imports to fail). Would be ideal to just change the directory name now, but the symlink should allow for a better transition strategy.

## 8.3.2 (2017-07-27)

* Add troposphere version check to cw_alarm
  * No change to any of the CFN templates; just a user experience improvement

## 8.3.1 (2017-07-26)

* Add `sturdy_stacker_core_blueprints` symlink to blueprints directory.
  * Symlink is for future uses of repo where multiple `blueprints` directories may be present in a system python path (causing imports to fail). Would be ideal to just change the directory name now, but the symlink should allow for a better transition strategy.

## 8.3.0 (2017-07-25)

* Add automatic upload of vpnserver cookbook archive.

## 8.2.0 (2017-07-25)

* Make `ChefBucketName` & `ChefDataBucketName` parameters of chef_bucket optional.
  * This would be a breaking change if the old template defaults could have actually been used anywhere.

## 8.1.3 (2017-07-21)

* Started the deprecation process of `aws-ec2-assign-elastic-ip` in favor of the sturdy_openvpn cookbook performing the association.
  * Next major version of sturdy-stacker-core will require sturdy_openvpn cookbook v2.3.0+

## 8.1.2 (2017-07-14)

* Double VPN alarm EvaluationPeriods - should prevent false positive alerts
* Make lambda variables optional

## 8.1.1 (2017-07-03)

* Add `set -e -o pipefail` to instance bash scripts

## 8.1.0 (2017-07-02)

* Integrate SNS topic for CloudWatch alerts
* Add CloudWatch alarm for VPN server status (designed for use with sturdy_openvpn >= v2.2.3)

## 8.0.0 (2017-06-23)

Breaking changes:
* variable: stacker_bucket_name is now required for to support the option for some environments
* variable: vpc_az_offset is now required with a value of zero for no change(described below)

New features:
* Added option AzOffset to vpc in order to use different availability zones
    * Set an integer value for AzOffset in the core-vpc stack
        * The AzOffset is added to the Select count for choosing AZs
            * Default AZs us-east-1a, us-east-1b
            * If AzOffset is 1 AZs are us-east-1b, us-east-1c
* Added support for China Region with added Arns to the role policy

Additional changes:
* roles: removed input of ChefBucketName and ChefDataBucketName in favor of ChefBucketArn and ChefDataBucketArn
* Added output of ChefBucket Arn and ChefDataBucket Arn
* Added AZS constant for loop support of more than three availability zones
* Changed loops to be integers using `range()` instead of list of strings

## 7.0.0 (2017-05-31)

Breaking changes:
* vpn_server: refactor userdata into separate scripts matching format expected by ssm_chef_document document
    * New S3 folder structure requires moving existing cookbook packages and generated 'archive' files - files should now be in `ENVIRONMENT/vpnservers` (e.g. `common/vpnservers`) instead of the top level `vpnservers`. To minimize disruption:
        * Copy the artifact bucket files/dir (e.g. `client.ovpn`, `ssl`) to the new environment folder structure.
        * Update the client vpn cookbook:
            * Bump sturdy_openvpn cookbook dependency to v2 (which removes the hardcoded `vpnservers` prefix use in `sturdy_openvpn_create_ssl`)
            * In the default recipe, update the `sturdy_openvpn_create_ssl` & `sturdy_openvpn_create_client_config` resources to specify a folder property with the new "chef_data_bucket_folder" attribute as its value:
```
sturdy_openvpn_create_ssl node['sturdy']['openvpn']['vpn_url'] do
  bucket node['sturdy']['openvpn']['chef_data_bucket_name']
  folder node['sturdy']['openvpn']['chef_data_bucket_folder']
  region node['sturdy']['openvpn']['chef_data_bucket_region']
end
...
sturdy_openvpn_create_client_config node['sturdy']['openvpn']['vpn_address'] do
  bucket node['sturdy']['openvpn']['chef_data_bucket_name']
  folder node['sturdy']['openvpn']['chef_data_bucket_folder']
  region node['sturdy']['openvpn']['chef_data_bucket_region']
  reneg_sec node['openvpn']['config']['reneg-sec']
end
```
            * Package the updated cookbook archive:
                * `berks update sturdy_openvpn`
                * `berks package`
            * Upload the cookbook tarball to the chef config bucket under `ENVIRONMENT/vpnservers`
                * The old tarball can be left in the top level `vpnservers` folder - it will be ignored after the upgrade
        * Perform a `stacker build` on core.yaml
        * Verify the new instance starts OpenVPN properly
        * Delete the files in the old (non-environment-prefixed) locations
    * The former `vpn_chef_client_recipe` environment variable is now `vpn_chef_client_runlist`. Any existing uses of it will need to be updated to the full runlist syntax (e.g. `foocorp_openvpn::default` to `recipe[foocorp_openvpn::default]`)
* vpn_server: Drop `VPNChefClientSetupCommand` variable - doesn't fit the model of chef-client runs initiated via SSM and was likely never used.

New features:
* Add chefrun-docs stack to core.yaml; this will allow ad-hoc executions of instance chef-client runs

Additional changes:
* roles: Fix incorrect S3 permissions in managed role (s3 prefix didn't allow listing subfolders under all/)
* vpn_server: Add new `['sturdy']['openvpn']['chef_data_bucket_folder']` attribute to the environment file for use in the client vpn cookbook
* vpn_server: Fix incorrect S3 permissions in role (s3 prefix list wasn't being correctly used)
* vpn_server: Change `CommonPolicy` parameter to `VPNManagedPolicies` to better model how lists of policies should be used as parameters
* vpn_server: Change `VPNChefClientRecipe` parameter to `ChefRunList` as noted above re: `vpn_chef_client_runlist`
* vpn_server: Drop `vpn_asg_launching_hook_topic` & `vpn_asg_terminating_hook_topic` from environment - never implemented and doesn't fit the model of chef-client runs initiated via SSM

## 6.1.0 (2017-05-31)

* Externalize individual blueprint versions into inherited version file
* Rename bucketandip.yaml to bootstrap.yaml
* Update `nopep8` lint disable messages to `noqa` used by flake8
* roles: Remove the `*` SSM GetParameters access and replaced it with retrieval permissions for parameters prefixed with `all.`
* vpn_server: Add retry options to curl installation of chef-client
* vpn_server: Disable new-style chef-client exit codes to prevent cloud-init failure reports on initial reboot
