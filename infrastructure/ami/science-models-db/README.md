#### Install Packer

Please refer to this [Packer page](https://www.packer.io/intro/getting-started/install.html)

The required Packer version is v1.3.2 (or later).

#### Create an AMI

```sh
$ AWS_PROFILE=[prod] packer build science-models-db.json
```

The resulting AMI will be created in the `surfline-prod` account but shared with `surfline-dev` and `surfline-legacy` as well.
