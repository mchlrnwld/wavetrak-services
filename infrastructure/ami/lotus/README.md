# Lotus WW3 Master / Slave EC2 Instances

Amazon Machine Images for running Lotus WW3.

#### Install Packer

Please refer to this [Packer page](https://www.packer.io/intro/getting-started/install.html)

The required Packer version is v1.3.2 (or later).

#### Create an AMI

```sh
$ AWS_PROFILE=[prod] packer build lotus-master.json
$ AWS_PROFILE=[prod] packer build lotus-slave.json
```

The resulting AMI will be created in the `surfline-prod` account. 
