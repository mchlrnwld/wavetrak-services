packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "source_ami" {
  # ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20211129
  default = "ami-009726b835c24a3aa"
  type    = string
}

variable "region" {
  default = "us-west-1"
  type    = string
}

local "ami_name" {
  expression = "wt-lotus-master-${formatdate("YYYY-MM-DD-hh-mm-ss", timestamp())}"
}

source "amazon-ebs" "lotus-master" {
  ami_name      = local.ami_name
  source_ami    = var.source_ami
  instance_type = "t2.micro"
  region        = var.region
  ssh_username  = "ubuntu"

  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_size           = 100
    volume_type           = "gp2"
    delete_on_termination = true
  }

  launch_block_device_mappings {
    device_name           = "/dev/sdb"
    volume_size           = 100
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tag {
    key   = "Name"
    value = local.ami_name
  }
}

build {
  name = "build-wt-lotus-master"
  sources = [
    "source.amazon-ebs.lotus-master"
  ]
  provisioner "shell" {
    inline = [

      "sudo mkdir -p -m777 /export-local/LOTUS /export/LOTUS ~/dl /models_source/wwatch_v_607",
      "sudo chown -R ubuntu /export-local",

      # apt
      "sudo add-apt-repository main",
      "sudo add-apt-repository universe",
      "sudo apt-get update",
      "sudo apt-get -y upgrade",
      "sudo apt-get -y install python3-pip gcc gfortran libnetcdf-dev libnetcdff-dev mpich binutils nfs-common octave liboctave-dev",

      # cloudwatch
      "sudo wget https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb",
      "sudo dpkg -i -E ./amazon-cloudwatch-agent.deb",
      "sudo rm -f ./amazon-cloudwatch-agent.deb",

      # efs
      "git clone https://github.com/aws/efs-utils ~/dl/efs-utils",
      "cd ~/dl/efs-utils && ./build-deb.sh",
      "sudo apt-get -y install ./build/amazon-efs-utils*deb",
      "cd ~",
      "sudo rm -rf ~/dl/efs-utils",

      # octave
      "echo 'pkg install -forge divand' | octave",

      # pip
      "sudo python3 -m pip install --upgrade pip",
      "sudo python3 -m pip install numpy awscli boto3 scipy matplotlib pandas netCDF4",

      # wgrib
      "cd ~/dl && wget ftp://ftp.cpc.ncep.noaa.gov/wd51we/wgrib2/wgrib2.tgz",
      "tar -xzvf wgrib2.tgz && cd grib2",
      "sed -i '/#export CC=gcc/s/^#//g' makefile",
      "sed -i '/#export FC=gfortran/s/^#//g' makefile",
      "make",
      "sudo mv ~/dl/grib2/wgrib2/wgrib2 /usr/local/bin/wgrib2",

      # ww3
      "cd /models_source/wwatch_v_607",
      "git clone -b 6.07.1 https://github.com/NOAA-EMC/WW3 /models_source/wwatch_v_607",
      "cd /models_source/wwatch_v_607",
      "sh model/bin/ww3_from_ftp.sh"
    ]
  }
  provisioner "file" {
    source      = "./w3_setup_files/switch_MSW"
    destination = "/models_source/wwatch_v_607/model/bin/switch_MSW"
  }
  provisioner "file" {
    source      = "./w3_setup_files/wwatch3.env"
    destination = "/models_source/wwatch_v_607/model/bin/wwatch3.env"
  }
  provisioner "shell" {
    inline = [

      # set up ww3
      "export PATH=/usr/local/lib:$PATH",
      "export WWATCH3_NETCDF=NC4",
      "export PATH=/usr/bin:$PATH",
      "export PATH=/usr/lib:$PATH",
      "export PATH=/usr/lib64:$PATH",
      "export PATH=/usr/include:$PATH",
      "export NETCDF_CONFIG=/usr/bin/nc-config",
      "cd /models_source/wwatch_v_607/model/bin",
      "./w3_clean",
      "./w3_new",
      "echo n | ./w3_setup /models_source/wwatch_v_607/model/ -c Gnu -s MSW",
      "./w3_make"
    ]
  }
  provisioner "file" {
    source      = "./w3_setup_files/distance.m"
    destination = "/home/ubuntu/octave/distance.m"
  }
  provisioner "file" {
    source      = "./w3_setup_files/qinterp2.m"
    destination = "/home/ubuntu/octave/qinterp2.m"
  }
  provisioner "file" {
    source      = "./w3_setup_files/ww3_uprstr_serial"
    destination = "/models_source/wwatch_v_607/model/exe/ww3_uprstr_serial"
  }
  provisioner "shell" {
    inline = [
      "chmod 775 /home/ubuntu/octave/distance.m",
      "chmod 775 /home/ubuntu/octave/qinterp2.m",
      "chmod 775 /models_source/wwatch_v_607/model/exe/ww3_uprstr_serial"
    ]
  }
}
