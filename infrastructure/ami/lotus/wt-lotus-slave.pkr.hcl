packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "source_ami" {
  # ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20211129
  default = "ami-009726b835c24a3aa"
  type    = string
}

variable "region" {
  default = "us-west-1"
  type    = string
}

local "ami_name" {
  expression = "wt-lotus-slave-${formatdate("YYYY-MM-DD-hh-mm-ss", timestamp())}"
}

source "amazon-ebs" "wt-lotus-slave" {
  ami_name      = local.ami_name
  source_ami    = var.source_ami
  instance_type = "t2.micro"
  region        = var.region
  ssh_username  = "ubuntu"

  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_size           = 8
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tag {
    key   = "Name"
    value = local.ami_name
  }
}

build {
  name = "build-wt-lotus-slave"
  sources = [
    "source.amazon-ebs.wt-lotus-slave"
  ]
  provisioner "shell" {
    inline = [

      "sudo mkdir -p -m777 /export ~/dl",

      # apt
      "sudo add-apt-repository main",
      "sudo add-apt-repository universe",
      "sudo apt-get update",
      "sudo apt-get -y upgrade",
      "sudo apt-get -y install python3-pip gcc gfortran libnetcdf-dev libnetcdff-dev mpich binutils nfs-common",

      # efs
      "git clone https://github.com/aws/efs-utils ~/dl/efs-utils",
      "cd ~/dl/efs-utils && ./build-deb.sh",
      "sudo apt-get -y install ./build/amazon-efs-utils*deb",
      "cd ~",
      "sudo rm -rf ~/dl/efs-utils",

      # pip
      "sudo python3 -m pip install --upgrade pip",
      "sudo python3 -m pip install awscli boto3"
    ]
  }
}
