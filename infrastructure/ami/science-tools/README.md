## Prerequisites

#### Install Packer

Please refer to this [Packer page](https://www.packer.io/docs/install/index.html)

The required Packer version is v1.0.2 (or later).

## Usage

#### Setup the desired AWS account in which to create the AMI
```
export AWS_PROFILE="surfline-prod"
```

#### Download the cookbooks to a temp directory for packer to use
```
In this location of the cookbook "/chef/cookbooks/surfline-science" run "berks vendor /tmp/berks-vendor" to download the cookbook dependencies locally.
```
#### Create an AMI using the science-tools cookbook

```shell
export SL_SCIENCE_VERSION=1.21.2
make build
```
**Note:** Be sure to increment the `SL_SCIENCE_VERSION`.

#### AMI References

The packer image generated from this script is used in the following locations:

- [Cloudformation Airflow Celery template](https://github.com/Surfline/wavetrak-infrastructure/blob/master/cloudformation/surfline-airflow-celery.json)
- [Cloudformation Airflow Celery Chef Zero template](https://github.com/Surfline/wavetrak-infrastructure/blob/master/cloudformation/surfline-airflow-celery-chef-zero.json)
- [Cloudformation Airflow Webserver template](https://github.com/Surfline/wavetrak-infrastructure/blob/master/cloudformation/surfline-airflow-webserver.json)
- [Cloudformation Airflow Worker template](https://github.com/Surfline/wavetrak-infrastructure/blob/master/cloudformation/surfline-airflow-worker.json)
- [Chef Kitchen script](https://github.com/Surfline/wavetrak-infrastructure/blob/master/chef/cookbooks/surfline-airflow-celery/.kitchen.yml)
- [Dynamic Airflow Celery EC2 instances]: `/ocean/static/ec2/ec2_instance.cfg` folder. (This folder is mounted from an NFS server on nearly all science and airflow instances as part of the legacy science workflow.)

#### Purpose of this packer step 

- The purpose of the `science-tools.json` packer template is to generate a base AMI that contains tools & applications common to science use-cases at Surfline. 
- The EC2 instance that is deployed from this AMI is augmented by additional configuration steps before it's finally ready for use.
