#### Install Packer

```
brew cask install packer
```


#### Setup the desired AWS account in which to create the AMI
```
export AWS_PROFILE="surfline-prod"
```


#### Create an AMI using Wowza's unattended installation

```
packer build \
  -var 'wowza_version=4.5.0' \
  -var 'wowza_username=some_username' \
  -var 'wowza_password=some_password' \
  -var 'wowza_licensekey=wowza-license-key' \
  wowza.json
```


#### Create an AMI using NIST expect (edit wowza.exp for customization)

```
packer build \
  -var 'wowza_version=4.5.0' \
  -var 'wowza_username=some_username' \
  -var 'wowza_password=some_password' \
  -var 'wowza_licensekey=wowza-license-key' \
  wowza-expect.json
```


The resulting AMI will be created in the `surfline-prod` account but
shared with `surfline-dev` and `surfline-legacy` as well.
