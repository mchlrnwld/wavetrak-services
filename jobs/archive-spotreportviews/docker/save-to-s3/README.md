# Save to S3

Docker image to grab latest SpotReportView data and save to S3. 

## Docker

### Build and Run

```sh
$ docker build -t archive-spotreportviews/save-to-s3 .
$ cp .env.sample .env
$ docker run --env-file=.env --volume $(pwd)/data:/opt/app/data --rm archive-spotreportviews/save-to-s3
```
