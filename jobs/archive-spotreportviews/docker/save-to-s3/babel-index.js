require('dotenv').config({ silent: true });
require('babel-register')({ presets: [ 'env' ] });
require('./index');
