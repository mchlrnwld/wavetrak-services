import { spawn } from 'child_process';
import fs from 'fs';
import path from 'path';
import moment from 'moment';
import S3 from 'aws-sdk/clients/s3';

const s3 = new S3({ region: 'us-west-1' });

const mongoConnectionString = process.env.MONGO_CONNECTION_STRING_KBYG || '';
const currPath = path.resolve(process.cwd());
const dataPath = process.env.AIRFLOW_TMP_DIR || path.join(currPath, 'data');
const file = path.join(dataPath, 'spotReportViews.json');

// get full SpotReportViews collection via mongoexport and save to file
const fetchSRVData = spawn('mongoexport', [ 
   '--uri',
   `${mongoConnectionString}`,
   '--ssl',
   '--collection',
   'SpotReportViews', 
   '--out',
   `${file}`,
   ]);

fetchSRVData.on('error', error => {
  console.log('Mongoexport process error');
  throw (error);
});

fetchSRVData.on('close', info => {
  console.log(`Mongoexport process finished, initiating s3 upload`);
  
  const bucketName = process.env.WAVETRAK_DATA_LAKE_BUCKET || 'no-bucket';
  const timestamp = new Date();
  const srvDatedKey = moment(timestamp).utc().format('YYYY/MM/DD/HH');
  var uploadParams = {
    Bucket: `${bucketName}`,
    Key: `product/surfline/spotreportviews/${srvDatedKey}/spotReportViews.json`,
    Body: '',
  };
  
  const fileStream = fs.createReadStream(file);

  fileStream.on('error', function(err) {
    console.log('fileStream error');
    throw (err);
  });

  fileStream.on('open', function() {
    console.log('fileStream open');
  });

  uploadParams.Body = fileStream;
  
  s3.upload (uploadParams, function (err, data) {
    if (err) {
      console.log('s3 upload error');
      throw (err);
    }
    if (data) {
      console.log('s3 upload Success', data.Location);
    }
  });

});
