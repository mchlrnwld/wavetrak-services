from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTBranchDockerOperator, WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator

from dag_helpers.docker import docker_image

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
WAVETRAK_DATA_LAKE_BUCKET = Variable.get('WAVETRAK_DATA_LAKE_BUCKET')
MONGO_CONNECTION_STRING_KBYG = Variable.get('MONGO_CONNECTION_STRING_KBYG')


# save_to_s3
save_to_s3_environment = {
    'SPOT_REPORT_PATH_PREFIX': 'product/surfline/spotreportviews',
    'WAVETRAK_DATA_LAKE_BUCKET': WAVETRAK_DATA_LAKE_BUCKET,
    'MONGO_CONNECTION_STRING_KBYG': MONGO_CONNECTION_STRING_KBYG,
    'AWS_ACCESS_KEY_ID': AWS_ACCESS_KEY_ID,
    'AWS_SECRET_ACCESS_KEY': AWS_SECRET_ACCESS_KEY,
}

# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2018, 10, 17, 22),
    'email': 'mike@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
}

dag = DAG(
    'archive_spotreportviews_v1',
    schedule_interval=timedelta(minutes=60),
    default_args=default_args,
)

save_to_s3 = WTDockerOperator(
    task_id='save_to_s3',
    image=docker_image('save-to-s3', 'archive-spotreportviews'),
    environment=save_to_s3_environment,
    force_pull=True,
    dag=dag,
)
