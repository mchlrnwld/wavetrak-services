# Sync Tide Data To Fauna Job

Docker image to sync daily tide data to FaunaDB.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate sync-tide-data-to-fauna
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

### Build and Run

```sh
$ docker build -t sync-tide-data-to-fauna/sync-tide-data-to-fauna .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env sync-tide-data-to-fauna/sync-tide-data-to-fauna
```
