import logging
from timeit import default_timer

import lib.tide_helpers as tide_helpers

logger = logging.getLogger('sync-tide-data-to-fauna')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info('Starting sync-tide-data-to-fauna job')
    job_start_time = default_timer()

    # Pull tide data and do post processing here
    tide_helpers.sync_tide_data_to_fauna()

    elapsed = default_timer() - job_start_time
    logger.info('Finished sync-tide-data-to-fauna job')
    logger.info(f'Downloaded tide data in {elapsed}s')


if __name__ == '__main__':
    main()
