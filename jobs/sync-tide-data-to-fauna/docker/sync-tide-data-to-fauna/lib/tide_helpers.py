import logging

import requests

import lib.config as config
from lib.fauna_helper import FaunaHelper

logger = logging.getLogger('sync-tide-data-to-fauna')


def pull_tide_data_for_spot(spot_id):
    r = requests.get(config.KBYG_PRODUCT_API, {'spotId': spot_id, 'days': 2},)
    r.raise_for_status()
    response = r.json()

    # Add spot id to tide_location dict
    tide_location = response['associated']['tideLocation']
    tide_location['spot_id'] = spot_id

    # Add spot id to each dict in tides
    tides = [dict(item, spot_id=spot_id) for item in response['data']['tides']]

    return tide_location, tides


def update_tide_loc_doc(fauna_helper, spot_id, new_data):
    # get tide location doc for spot id
    tide_loc_doc = fauna_helper.get_tide_location_document(spot_id)

    if not tide_loc_doc:
        logger.info(f'Creating new TideLocation doc for: {spot_id}')
        fauna_helper.create_tide_location_document(new_data)
    elif tide_loc_doc['data'] != new_data:
        logger.info(f'Replacing TideLocation doc for: {spot_id}')
        fauna_helper.replace_tide_location_document(tide_loc_doc, new_data)


def sync_tide_data_to_fauna():
    fauna_helper = FaunaHelper(config.FAUNA_SECRET)

    # get spot ids from Spots collection
    spot_ids = fauna_helper.get_all_spot_ids()

    # Loop through each spot id
    for _id in spot_ids:
        # Pull tide data
        logger.info(f'Pulling tide data for spot: {_id}')
        tide_loc, tide_data = pull_tide_data_for_spot(_id)

        # Update/Create TideLocation document for spot
        update_tide_loc_doc(fauna_helper, _id, tide_loc)

        # Fetch latest timestamp in collection
        latest_timestamp = fauna_helper.get_latest_tide_timestamp(_id)

        # Splice tide data by timestamp
        if latest_timestamp:
            tide_entries_to_add = list(
                filter(
                    lambda entry: entry['timestamp'] > latest_timestamp,
                    tide_data,
                )
            )
        else:
            tide_entries_to_add = tide_data

        # Print entries to be added to collection
        logger.info(
            f'Adding {len(tide_entries_to_add)} documents to '
            'FaunaDB Tides Collection.'
        )
        fauna_helper.create_tide_documents(tide_entries_to_add)
