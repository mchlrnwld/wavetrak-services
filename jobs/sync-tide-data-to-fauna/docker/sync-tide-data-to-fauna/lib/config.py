import os

from job_secrets_helper import get_secret

FAUNA_DB = os.environ['FAUNA_DB']
FAUNA_SECRET = get_secret('sandbox/jobs/fauna/', FAUNA_DB)
KBYG_PRODUCT_API = os.environ['KBYG_PRODUCT_API']
