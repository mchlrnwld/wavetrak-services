from faunadb import query as q  # type: ignore
from faunadb.client import FaunaClient  # type: ignore
from faunadb.errors import NotFound  # type: ignore


class FaunaHelper:
    """


    """

    def __init__(self, db_secret):
        self.client = FaunaClient(secret=db_secret)

    def get_all_spot_ids(self):
        return [
            self.client.query(q.get(q.ref(ref)))['data']['id']
            for ref in self.client.query(
                q.paginate(q.match(q.index('all_spots')))
            )['data']
        ]

    def get_latest_tide_timestamp(self, spot_id):
        result = self.client.query(
            q.paginate(
                q.match(q.index("latest_tide_timestamps_by_spot_id"), spot_id),
                size=1,
            )
        )

        if not result['data']:
            return None

        return result['data'][0]

    def create_tide_documents(self, data_array):
        self.client.query(
            q.map_(
                lambda tide_entry: q.create(
                    q.collection('Tides'), {"data": tide_entry}
                ),
                data_array,
            )
        )

    def get_tide_location_document(self, spot_id):
        try:
            return self.client.query(
                q.get(
                    q.match(
                        q.index('search_tide_locations_by_spot_id'), spot_id
                    )
                )
            )
        except NotFound:
            return {}

    def replace_tide_location_document(self, doc, new_data):
        self.client.query(q.replace(q.ref(doc['ref']), {"data": new_data}))

    def create_tide_location_document(self, data):
        self.client.query(
            q.create(q.collection('TideLocations'), {'data': data})
        )
