import re
from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback

ENV = Variable.get('ENVIRONMENT')

JOB_NAME = 'process-forecast-platform-nasa-mur-sst'
MODEL_RUN = '{{ task_instance.xcom_pull(task_ids=\'check-model-ready\') }}'
SCIENCE_KEY_PREFIX = 'nasa/mur-sst'

APP_ENV = 'sandbox' if ENV == 'dev' else ENV
SQS_QUEUE_NAME = 'wt-{0}-s3-events-{1}'.format(JOB_NAME, APP_ENV)

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, APP_ENV
)

common_environment_values = {
    'AWS_DEFAULT_REGION': Variable.get('AWS_DEFAULT_REGION'),
    'ENV': APP_ENV,
    'JOB_NAME': JOB_NAME,
    'MODEL_AGENCY': 'NASA',
    'MODEL_NAME': 'MUR-SST',
    'MODEL_RUN': MODEL_RUN,
    'MODEL_RUN_TYPE': 'POINT_OF_INTEREST',
    'SCIENCE_KEY_PREFIX': SCIENCE_KEY_PREFIX,
}


def task_environment(*keys, **keyvalues):
    environment = keyvalues.copy()
    for key in keys:
        environment[key] = common_environment_values[key]
    return environment


check_model_ready_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'MODEL_AGENCY',
    'MODEL_NAME',
    'SCIENCE_KEY_PREFIX',
    SQS_QUEUE_NAME=SQS_QUEUE_NAME,
)

process_point_of_interest_data_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'JOB_NAME',
    'MODEL_AGENCY',
    'MODEL_NAME',
    'SCIENCE_KEY_PREFIX',
    'MODEL_RUN',
    SLACK_CHANNEL='surfspots',
)

write_point_of_interest_data_to_database_environment = task_environment(
    'AWS_DEFAULT_REGION', 'ENV', 'JOB_NAME', RUN=MODEL_RUN, COMMIT='true',
)

mark_model_run_complete_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'JOB_NAME',
    'MODEL_AGENCY',
    'MODEL_NAME',
    'MODEL_RUN',
    'MODEL_RUN_TYPE',
)

prune_model_runs_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'MODEL_AGENCY',
    'MODEL_NAME',
    'MODEL_RUN',
    'MODEL_RUN_TYPE',
    MODEL_LIMIT='3',
)

# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2019, 12, 5),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
    'on_failure_callback': pager_duty_failure_callback,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=30),
    default_args=default_args,
)

check_model_ready = WTDockerOperator(
    task_id='check-model-ready',
    image=docker_image('check-model-ready', prefix=JOB_NAME),
    environment=check_model_ready_environment,
    force_pull=True,
    xcom_push=True,
    depends_on_past=True,
    dag=dag,
)

check_model_ready_branch = BranchPythonOperator(
    task_id='check-model-ready-branch',
    templates_dict={'model_run': MODEL_RUN},
    python_callable=lambda **kwargs: (
        'begin-processing-model'
        if kwargs['templates_dict']['model_run']
        # HACK: Check model run to prevent 'None' from triggering processing.
        and re.match(r'\d{10}', kwargs['templates_dict']['model_run'])
        else 'no-model-ready'
    ),
    dag=dag,
)

no_model_ready = DummyOperator(task_id='no-model-ready', dag=dag)

begin_processing_model = DummyOperator(
    task_id='begin-processing-model', dag=dag
)

process_point_of_interest_data = WTDockerOperator(
    task_id='process-point-of-interest-data',
    image=docker_image('process-point-of-interest-data', JOB_NAME),
    environment=process_point_of_interest_data_environment,
    force_pull=True,
    retries=0,
    dag=dag,
)

write_point_of_interest_data_to_database = WTDockerOperator(
    task_id='write-point-of-interest-data-to-database',
    image=docker_image('write-point-of-interest-data-to-database', JOB_NAME),
    environment=write_point_of_interest_data_to_database_environment,
    force_pull=True,
    dag=dag,
)

prune_model_runs = WTDockerOperator(
    task_id='prune-model-runs',
    image=docker_image('prune-model-runs'),
    environment=prune_model_runs_environment,
    force_pull=True,
    dag=dag,
)

mark_model_run_complete = WTDockerOperator(
    task_id='mark-model-run-complete',
    image=docker_image('mark-model-run-complete'),
    environment=mark_model_run_complete_environment,
    force_pull=True,
    dag=dag,
)

check_model_ready_branch.set_upstream(check_model_ready)
no_model_ready.set_upstream(check_model_ready_branch)
begin_processing_model.set_upstream(check_model_ready_branch)
begin_processing_model.set_downstream(process_point_of_interest_data)
write_point_of_interest_data_to_database.set_upstream(
    process_point_of_interest_data
)
mark_model_run_complete.set_upstream(write_point_of_interest_data_to_database)
prune_model_runs.set_upstream(mark_model_run_complete)
