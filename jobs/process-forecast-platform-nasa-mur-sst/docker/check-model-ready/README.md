# Check Model Ready

Docker image to check if latest NASA MUR SST analysis is ready for processing.

## Setup

```sh
$ conda devenv
$ source activate process-forecast-platform-nasa-mur-sst-check-model-ready
$ cp .env.sample .env
$ env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-forecast-platform-nasa-mur-sst/check-model-ready .
$ cp .env.sample .env
$ docker run --env-file=.env --rm process-forecast-platform-nasa-mur-sst/check-model-ready
```
