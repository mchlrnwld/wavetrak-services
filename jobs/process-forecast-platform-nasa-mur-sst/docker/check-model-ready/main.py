from functools import partial
import logging

import boto3  # type: ignore
from requests import HTTPError

from check_model_ready import (  # type: ignore
    check_model_run_already_exists,
    check_sqs_messages,
    set_pending_model_run,
)
import lib.config as config
from lib.helpers import model_run_from_key

logger = logging.getLogger('check-model-ready')


def main():
    # TODO: Uncomment below to stream info level logging to stderr once
    #       WTDockerOperator can distinguish between stdout and stderr streams.
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler()) ### COMMENT OUT BEFORE COMMITING

    logger.info(f'Polling queue: {config.SQS_QUEUE_NAME}...')

    result = None
    total_messages = 0

    while True:
        model_run, messages_found = check_sqs_messages(
            config.SCIENCE_BUCKET,
            (config.SCIENCE_KEY_PREFIX,),
            config.SQS_QUEUE_NAME,
            # There is only one file, therefore the SQS message notifiying that
            # a file is in S3 is sufficent. Just check processing has not
            # already exists.
            lambda run: not check_model_run_already_exists(
                config.MODEL_AGENCY,
                config.MODEL_NAME,
                run,
                'POINT_OF_INTEREST',
                config.SCIENCE_DATA_SERVICE,
            ),
            partial(model_run_from_key, config.SCIENCE_KEY_PREFIX),
        )
        result = model_run
        total_messages += messages_found
        if result is not None or messages_found == 0:
            break

    logger.info(f'Checked {total_messages} messages.')
    logger.debug(f'Resulting model run: {result}')

    # Write to stdout for xcoms_push
    if result is not None:
        set_pending_model_run(
            config.MODEL_AGENCY,
            config.MODEL_NAME,
            result,
            'POINT_OF_INTEREST',
            config.SCIENCE_DATA_SERVICE,
        )
        print(result)
    else:
        print('')


if __name__ == '__main__':
    main()
