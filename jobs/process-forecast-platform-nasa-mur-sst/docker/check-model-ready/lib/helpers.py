# TODO: Extract these helper functions into modules.
def model_run_from_key(science_key_prefix: str, key: str) -> int:
    """
    Extracts a model run time from the given key string

    Args:
        science_key_prefix: Prefix in S3 to check for
                            ww3-ensemble uploads.
        key: The key string to extract data from

    Returns:
        A string representing the model run time
    """
    return int(key.replace(science_key_prefix, '').split('/')[1])
