import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'

MODEL_AGENCY = os.environ['MODEL_AGENCY']
MODEL_NAME = os.environ['MODEL_NAME']
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']
SQS_QUEUE_NAME = os.environ['SQS_QUEUE_NAME']
