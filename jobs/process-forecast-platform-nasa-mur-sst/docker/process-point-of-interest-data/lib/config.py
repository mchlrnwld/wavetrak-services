import os

from job_secrets_helper import get_secret  # type: ignore

DATA_DIR = os.getenv('AIRFLOW_TMP_DIR', 'data')
ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
AGENCY = os.environ['MODEL_AGENCY']
MODEL = os.environ['MODEL_NAME']
RUN = os.environ['MODEL_RUN']
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']

SECRETS_PREFIX = f'{ENV}/common/'
JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
SCIENCE_PLATFORM_PSQL_JOB = get_secret(
    SECRETS_PREFIX, 'SCIENCE_PLATFORM_PSQL_JOB'
)
SLACK_API_TOKEN = get_secret(SECRETS_PREFIX, 'SLACK_API_TOKEN', '')
SLACK_CHANNEL = os.getenv('SLACK_CHANNEL', '')
