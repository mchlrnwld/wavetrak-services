class PointOfInterestGridPoint:
    """
    Point of interest grid point.
    Arguments:
        point_of_interest_id: Point of interest ID.
        lon: Grid point longitude as a float.
        lat: Grid point latitude as a float.
        agency: Name of model agency for the grid point.
        model: Name of model for the grid point.
        grid: Name of model grid for the grid point.
    Attributes:
        point_of_interest_id: Point of interest ID.
        lon: Grid point longitude as a float.
        lat: Grid point latitude as a float.
        agency: Name of model agency for the grid point.
        model: Name of model for the grid point.
        grid: Name of model grid for the grid point.
    """

    def __init__(
        self,
        point_of_interest_id: str,
        lon: float,
        lat: float,
        agency: str,
        model: str,
        grid: str,
    ):
        self.point_of_interest_id = point_of_interest_id
        self.agency = agency
        self.model = model
        self.grid = grid
        # Adjust to (-180, 180]
        self.lon = lon % 360 if lon % 360 < 180 else lon % 360 - 360
        self.lat = lat
