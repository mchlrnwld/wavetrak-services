from typing import List, TextIO

import psycopg2  # type: ignore


# TODO: Extract to a job-helpers module. Function almost exactly matches that
# in corresponding file in process-forecast-platform-wavetrak-lotus-ww3 job,
# and other jobs.
def copy_csv_to_postgres(
    csvfiles: List[TextIO],
    connection_uri: str,
    tables: List[str],
    fieldnames: List[List[str]],
):
    """
    Use Postgres COPY function to import data from a CSV files to
    Science Platform Postgres tables.

    Arguments:
        csvfiles: List of file objects representing readable CSV file with a
            header row.
        connection_uri: URI to connect to the database.
        tables: List of names of the tables to write to. The list should
            correspond placewise with the csvfiles argument.
        fieldnames: List of the lists of headers from the CSV to write to
            Postgres.The list should correspond placewise with the csvfiles
            argument.
    """
    with psycopg2.connect(connection_uri) as conn:
        with conn.cursor() as cur:
            for csvfile, table, fieldname in zip(csvfiles, tables, fieldnames):
                columns = ', '.join(fieldname)
                query = f'COPY {table} ({columns}) FROM STDIN WITH CSV HEADER'
                cur.copy_expert(query, csvfile)
            conn.commit()
