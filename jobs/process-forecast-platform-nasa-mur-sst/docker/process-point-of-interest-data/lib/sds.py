import json
from typing import List

from lib.models import PointOfInterestGridPoint
import requests


# TODO: move to common/job-helpers
class SDS:
    """
    Client for querying the Science Data Service GraphQL endpoint.

    Args:
        host_url: URL for Science Data Service host.

    Attributes:
        host_url: URL for Science Data Service host.
    """

    def __init__(self, host_url: str):
        self.host_url = host_url

    def get_points_of_interest_for_grid(
        self, agency: str, model: str, grid: str
    ) -> List[PointOfInterestGridPoint]:
        """
        Get a list of points_of_interest_grid_points associated with the
        agency, model, and grid

        Returns:
            A list of PointOfInterestGridPoint objects.
        """
        query = f'''
        {{
          models(agency: "{agency}", model: "{model}"){{
            grids(grid: "{grid}"){{
              pointsOfInterest {{
                id
                lat
                lon
              }}
            }}
          }}
        }}'''
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }
        response = requests.post(
            f'{self.host_url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        grid_data = response.json()['data']['models'][0]['grids']
        if not grid_data:
            return list()
        return [
            PointOfInterestGridPoint(
                poi['id'], poi['lon'], poi['lat'], agency, model, grid
            )
            for poi in grid_data[0]['pointsOfInterest']
        ]
