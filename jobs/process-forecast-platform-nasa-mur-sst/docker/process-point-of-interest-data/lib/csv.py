from typing import Optional
import pandas as pd  # type: ignore


def write_spot_data_to_csv(
    point_of_interest_spot_data: pd.DataFrame, csv_file_path: Optional[str]
) -> Optional[str]:
    """
    Writes the point_of_interest_spot_data DataFrame to csv for sst table.

    Args:
        point_of_interest_spot_data: DataFrame containing all point of interest
            data needed to write to the sst table.
        csv_file_path: File path string where the csv file will be written, or
            None.

    Returns:
        None if argument csv_file_path is provided, or the csv data as a string
        if csv_file_path is None
    """
    point_of_interest_spot_data_copy: pd.DataFrame = (
        point_of_interest_spot_data.copy()
    )
    point_of_interest_spot_data_copy['run'] = point_of_interest_spot_data_copy[
        'forecast_time'
    ]
    return point_of_interest_spot_data_copy.to_csv(
        path_or_buf=csv_file_path,
        mode='w',
        index=False,
        header=True,
        date_format='%Y-%m-%d %H:%M:%S',
        columns=[
            'point_of_interest_id',
            'agency',
            'model',
            'grid',
            'latitude',
            'longitude',
            'run',
            'forecast_time',
            'temperature',
        ],
    )
