import lib.config as config


def _create_filename(run: str) -> str:
    return f'{run}0000-JPL-L4_GHRSST-SSTfnd-MUR-GLOB-v02.0-fv04.1.nc'


def analysis_file_s3_key(run: str, agency: str, model_name: str) -> str:
    """
    Form the S3 object key for the SST analysis file.

    Args:
        run: String in form 'YYYYMMDDHH'.
        agency: Agency as a string.
        model_name: Model name as a string.

    Returns:
        Object key as a string.
    """
    filename = _create_filename(run)
    return f'{config.SCIENCE_KEY_PREFIX}/{run}/{filename}'
