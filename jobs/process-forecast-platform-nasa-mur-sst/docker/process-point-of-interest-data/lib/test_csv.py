from datetime import datetime

from lib.csv import write_spot_data_to_csv
import pandas as pd  # type: ignore


def test_write_spot_data_to_csv():
    data = pd.DataFrame.from_dict(
        {
            'forecast_time': [
                datetime(2020, 1, 1, 9),
                datetime(2020, 1, 1, 9),
            ],
            'latitude': [50.25, 35.25],
            'longitude': [-4, -23],
            'agency': ['NASA', 'NASA'],
            'model': ['MUR-SST', 'MUR-SST'],
            'grid': ['0p01', '0p01'],
            'point_of_interest_id': ['aaa-aaa', 'bbb-bbb'],
            'temperature': [284, 293],
        }
    )
    csv = write_spot_data_to_csv(data, csv_file_path=None)
    assert csv == (
        """point_of_interest_id,agency,model,grid,latitude,longitude,run,forecast_time,temperature
aaa-aaa,NASA,MUR-SST,0p01,50.25,-4,2020-01-01 09:00:00,2020-01-01 09:00:00,284
bbb-bbb,NASA,MUR-SST,0p01,35.25,-23,2020-01-01 09:00:00,2020-01-01 09:00:00,293
"""
    )
