from lib.sst_netcdf import _date_from_non_standard_timestamp, _get_location_sst
import numpy.ma as ma  # type: ignore
from pytest import raises  # type: ignore

lats = ma.asarray([-1, 0, 1])
lons = ma.asarray([-3, -2, -1, 0, 1, 2, 3])
sst_matrix = ma.asarray(
    [
        [
            [-1, 11, 12, 13, 14, 15, 16],
            [17, 18, 19, 20, 21, 22, 23],
            [24, 25, 26, 27, 28, 29, 30],
        ]
    ]
)
sst_matrix[0, 0, 0] = ma.masked


def test_date_from_non_standard_timestamp_happy_case():
    date_iso_string = _date_from_non_standard_timestamp(
        86400, 'Seconds since 2016-12-20 01:00:00 UTC'
    )
    assert date_iso_string == '2016-12-21T01:00:00'


def test_date_from_non_standard_timestamp_with_bad_unit():
    with raises(ValueError, match='Time units does not match expected format'):
        date_iso_string = _date_from_non_standard_timestamp(
            86400, 'Seconds since some time ago'
        )


def test_make_sst_row_happy_case():
    assert _get_location_sst(lats, lons, sst_matrix, 1, 2) == 29


def test_make_sst_row_happy_case_with_rounding_issues():
    assert _get_location_sst(lats, lons, sst_matrix, 0.9999, 2.0001) == 29


def test_make_sst_row_lat_out_of_range():
    with raises(ValueError, match='Latitude out of range, 2'):
        _get_location_sst(lats, lons, sst_matrix, 2, 2)


def test_make_sst_row_lon_out_of_range():
    with raises(ValueError, match='Longitude out of range, -4'):
        _get_location_sst(lats, lons, sst_matrix, 1, -4)


def test_make_sst_row_on_land():
    with raises(ValueError, match='No SST data for -1, -3'):
        _get_location_sst(lats, lons, sst_matrix, -1, -3)
