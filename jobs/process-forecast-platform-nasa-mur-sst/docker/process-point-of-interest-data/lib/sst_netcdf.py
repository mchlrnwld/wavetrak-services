import logging
import re
from datetime import datetime, timedelta
from typing import Callable, List

import numpy as np  # type: ignore
import numpy.ma as ma  # type: ignore
from netCDF4 import Dataset  # type: ignore
from pandas import DataFrame  # type: ignore

from lib.models import PointOfInterestGridPoint

logger = logging.getLogger('process-point-of-interest-data')

TIME_UNITS_RE = re.compile(
    r'seconds since ([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} UTC)',
    flags=re.IGNORECASE,
)


def _date_from_non_standard_timestamp(timestamp: int, unit_string: str) -> str:
    """
    Gets the analysis time the netcdf data is valid for.

    Args:
        timestamp: Number of seconds from some start datetime. Integer.
        unit_string: Should be in the format
            "seconds since YYYY-MM-DD HH:MM:SS UTC".

    Returns:
        A date string in ISO 8601 format.
    """
    time_match = TIME_UNITS_RE.match(unit_string)
    if time_match is None:
        raise ValueError(
            f'Time units does not match expected format ({unit_string})'
        )
    # Because NASA seems to use seconds since 1 Jan 1981 (!?). And since it is
    # non-stadard, be prepared for changes.
    non_standard_epoch = datetime.strptime(
        time_match[1], '%Y-%m-%d %H:%M:%S %Z'
    )
    return (non_standard_epoch + timedelta(seconds=timestamp)).isoformat()


def _get_location_sst(lats, lons, sst_matrix, lat: float, lon: float) -> float:
    """
    Get SST for the given point.

    Args:
        lats:       1d numpy array of the latitudes in the grid.
        lons:       1d numpy array of the longitudes in the grid.
        sst_matrix: netCDF4.Variable of the sst values.
        lat:        Latitude to get SST for (float).
        lon:        Longitude to get SST for (float).

    Returns:
        Sea surface temperature at the point.
    """
    if lat < lats[0] or lat > lats[lats.size - 1]:
        raise ValueError(f'Latitude out of range, {lat}')
    if lon < lons[0] or lon > lons[lons.size - 1]:
        raise ValueError(f'Longitude out of range, {lon}')

    lat_index = lats.searchsorted(lat)
    lon_index = lons.searchsorted(lon)
    # searchsorted is good because it's a fast binary search, but searching for
    # exact values, can return an index one too high because of floating point
    # errors.
    if lat_index > 0 and lat - lats[lat_index - 1] < lats[lat_index] - lat:
        lat_index -= 1
    if lon_index > 0 and lon - lons[lon_index - 1] < lons[lon_index] - lon:
        lon_index -= 1

    location_sst = sst_matrix[0, lat_index, lon_index]

    if ma.is_masked(location_sst):
        raise ValueError(f'No SST data for {lat}, {lon}')

    return location_sst


def read_sst_netdcf_file(
    local_file_path: str,
    points_of_interest: List[PointOfInterestGridPoint],
    invalid_poi_callback: Callable[[List[PointOfInterestGridPoint]], None],
) -> DataFrame:
    """
    Prepare a pandas DataFrame of SST data for the given points of interest,
    from the netcdf file.

    Args:
        local_file_path: path to netcdf file.
        points_of_interest: list of dictionaries of POI data. Each should
            contain at lease `lat`, `lon`, and `id`.

    Returns:
        Pandas DataFrame of SST data.
    """
    with Dataset(local_file_path, 'r', format='NETCDF4') as rootgrp:
        analysis_time = _date_from_non_standard_timestamp(
            int(rootgrp['time'][0]), rootgrp['time'].units
        )
        lats = np.asarray(rootgrp['lat'])
        lons = np.asarray(rootgrp['lon'])
        sst_matrix = rootgrp['analysed_sst']

        if lats[0] > lats[1]:
            raise Exception('Latitude array not sorted as expected')
        if lons[0] > lons[1]:
            raise Exception('Longitude array not sorted as expected')

        records = []
        invalid_points_of_interest = []
        for poi in points_of_interest:
            try:
                temperature = _get_location_sst(
                    lats, lons, sst_matrix, poi.lat, poi.lon
                )
            except ValueError:
                invalid_points_of_interest.append(poi)
                continue

            records.append(
                {
                    'forecast_time': analysis_time,
                    'latitude': poi.lat,
                    'longitude': poi.lon,
                    'agency': poi.agency,
                    'model': poi.model,
                    'grid': poi.grid,
                    'point_of_interest_id': poi.point_of_interest_id,
                    'temperature': temperature,
                }
            )

        invalid_poi_callback(invalid_points_of_interest)

        return DataFrame.from_records(records)
