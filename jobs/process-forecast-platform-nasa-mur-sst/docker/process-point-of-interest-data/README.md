# Process Point of Interest Data

Docker image to process NASA MUR SST analysis netcdf file, inserting point of interest data to the Science Platform Postgres sst table.

## Setup

```sh
$ conda devenv
$ source activate process-forecast-platform-nasa-mur-sst-process-point-of-interest-data
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-forecast-platform-nasa-mur-sst/process-point-of-interest-data .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-nasa-mur-sst/process-point-of-interest-data
```
