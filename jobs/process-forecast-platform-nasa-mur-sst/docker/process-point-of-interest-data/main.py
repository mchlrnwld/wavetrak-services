import asyncio
import logging
import os
from tempfile import TemporaryDirectory
from timeit import default_timer
from slack_helper import send_invalid_points_of_interest_notification

import lib.config as config
from lib.csv import write_spot_data_to_csv
from lib.helpers import analysis_file_s3_key

from lib.s3_client import S3Client
from lib.sds import SDS
from lib.sst_netcdf import read_sst_netdcf_file

logger = logging.getLogger('process-point-of-interest-data')

SST_CSV_FILE_NAME = 'sst.csv'
SST_TABLE = 'sst'
SST_FIELDS = [
    'point_of_interest_id',
    'agency',
    'model',
    'grid',
    'latitude',
    'longitude',
    'run',
    'forecast_time',
    'temperature',
]
GRID = '0p01'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    logger.info(f'Starting job for model run: {config.RUN}')

    sds = SDS(config.SCIENCE_DATA_SERVICE)
    grid_points = sds.get_points_of_interest_for_grid(
        config.AGENCY, config.MODEL, GRID
    )
    if not grid_points:
        logger.info(
            f'No POI grid points for {config.AGENCY} {config.MODEL}. '
            f'Nothing to do.'
        )
        return
    s3_key = analysis_file_s3_key(config.RUN, config.AGENCY, config.MODEL)

    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir:
        local_file_path = os.path.join(
            temp_dir, f'SST-{config.AGENCY}-{config.MODEL}-{config.RUN}.nc'
        )

        s3_client = S3Client()
        await s3_client.download_file(
            config.SCIENCE_BUCKET, s3_key, local_file_path
        )

        sst_data = read_sst_netdcf_file(
            local_file_path,
            grid_points,
            lambda x: send_invalid_points_of_interest_notification(
                config.JOB_NAME,
                {GRID: [poi.point_of_interest_id for poi in x]},
                config.SLACK_API_TOKEN,
                config.SLACK_CHANNEL,
                logger.error,
            )
            if x and config.SLACK_API_TOKEN and config.SLACK_CHANNEL
            else None,
        )
        logger.info('Writing point of interest SST data to CSV.')
        sst_csv_file_path = os.path.join(temp_dir, SST_CSV_FILE_NAME)
        start = default_timer()
        write_spot_data_to_csv(sst_data, sst_csv_file_path)
        elapsed = default_timer() - start
        logger.info(f'Finished writing to CSV in {elapsed}s')

        logger.info('Uploading CSV to S3...')
        start = default_timer()
        await s3_client.upload_file(
            sst_csv_file_path,
            config.JOBS_BUCKET,
            (
                f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/'
                f'{config.RUN}/{SST_CSV_FILE_NAME}'
            ),
        )
        elapsed = default_timer() - start
        logger.info(f'Finished uploading CSVs to S3 in {elapsed}s.')


if __name__ == '__main__':
    asyncio.run(main())
