variable "environment" {
}

variable "topic_names" {
  type = list(string)
}

variable "queue_name" {
  default = "s3-events"
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "process-forecast-platform-nasa-mur-sst"
}
