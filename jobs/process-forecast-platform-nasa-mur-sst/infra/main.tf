module "sqs_with_sns_subscription" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs-with-sns-subscription"

  application = var.application
  environment = var.environment
  queue_name  = var.queue_name
  topic_names = var.topic_names
  company     = var.company
}
