provider "aws" {
  region  = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-forecast-platform-nasa-mur-sst/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_nasa_mur_sst" {
  source = "../../"

  environment = "prod"
  topic_names = [
    "surfline-science-s3-prod-nasa-mur-sst-upload-event"
  ]
}
