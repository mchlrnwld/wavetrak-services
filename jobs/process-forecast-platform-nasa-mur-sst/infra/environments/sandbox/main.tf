provider "aws" {
  region  = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/process-forecast-platform-nasa-mur-sst/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_nasa_mur_sst" {
  source = "../../"

  environment = "sandbox"
  topic_names = [
    "surfline-science-s3-dev-nasa-mur-sst-upload-event"
  ]
}
