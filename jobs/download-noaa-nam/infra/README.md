# `download-noaa-nam` AWS Batch Job

This infrastructure defines the `download-noaa-nam` AWS Batch Job.

## Submitting Jobs

The following command shows how to submit a `download-noaa-nam` job to AWS Batch.

To specify dynamic parameters at job submission time, you must include the CLI argument `--container-overrides` with any environment variable you'd like to override.

AWS Batch offers _Job Definition Parameters_ that more cleanly serve this purpose but these can only be expanded into the container command, not into environment variables.

### CLI Container Overrides Example

```bash
aws batch submit-job \
  --job-name wt-jobs-download-noaa-nam-batch-job-dev \
  --job-queue wt-jobs-common-download-files-job-queue-dev \
  --job-definition wt-jobs-download-noaa-nam-batch-job-definition-dev \
  --container-overrides '{"environment": [{"name": "DATE", "value": "2019-12-09"}]}'
```

### `boto3` Container Overrides Example

```python
import boto3
client = boto3.client("batch")

response = client.submit_job(
    jobName="wt-jobs-download-noaa-nam-batch-job-dev",
    jobQueue="wt-jobs-common-download-files-job-queue-dev",
    jobDefinition="wt-jobs-download-noaa-nam-batch-job-definition-dev",
    containerOverrides={
        "environment": [
            {
                "name": "DATE",
                "value": "2019-12-11"
            }
        ]
    }
)
```
