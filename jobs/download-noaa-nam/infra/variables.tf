variable "environment" {}

variable "s3_bucket" {
  description = "Science S3 bucket."
}
