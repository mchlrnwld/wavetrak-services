module "batch_job_definition" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company     = "wt"
  application = "jobs-download-noaa-nam"
  environment = var.environment
  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/download-noaa-nam/download-to-s3:sandbox",
    "memory" : 1024,
    "vcpus" : 1,
  }
  custom_policy = templatefile("${path.module}/policy.json", {
    s3_bucket = var.s3_bucket
  })
}
