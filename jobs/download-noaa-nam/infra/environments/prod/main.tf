provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/download-noaa-nam/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "download_noaa_nam" {
  source = "../../"

  environment = "prod"
  s3_bucket   = "surfline-science-s3-prod"
}
