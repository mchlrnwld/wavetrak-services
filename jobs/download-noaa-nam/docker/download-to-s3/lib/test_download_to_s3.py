import asyncio
import unittest.mock as mock
from datetime import datetime
from typing import Optional

import boto3  # type: ignore
import pytest  # type: ignore
from moto import mock_s3  # type: ignore

from lib.download_to_s3 import get_files_to_download

NOAA_HTTP = 'https://nomads.ncep.noaa.gov'
NAM_BASE_PATH = 'pub/data/nccf/com/nam/prod/'
NAM_HTTP_BASE_URL = f'{NOAA_HTTP}/{NAM_BASE_PATH}'
SCIENCE_BUCKET = 'surfline-science-test'
SCIENCE_KEY_PREFIX = 'noaa/nam'
DATE = '20190716'
DATE_DIR = f'nam.{DATE}'
RUN = f'{DATE}'

MODEL_DATE_FORMAT = '%Y%m%d'


@pytest.fixture(scope='function')
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3')
        s3_client.create_bucket(Bucket=SCIENCE_BUCKET)
        yield s3_client


@pytest.fixture(autouse=True, scope='function')
def aws_credentials(monkeypatch):
    """
    Mocked AWS Credentials for moto.
    This is a temporary workaround,
    otherwise moto will throw 'NoCredentialsError'.
    """
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')


async def mock_list_http_contents(
    url: str, semaphore: Optional[asyncio.Semaphore] = None
) -> list:
    if url == NAM_HTTP_BASE_URL:
        return ['nam.20190716/']
    elif url == f'{NAM_HTTP_BASE_URL}/{DATE_DIR}/':
        return [
            'nam.t00z.conusnest.hiresf00.tm00.grib2',
            'nam.t00z.conusnest.hiresf00.tm00.grib2.idx',
            'nam.t00z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t00z.hawaiinest.hiresf00.tm00.grib2.idx',
            'nam.t00z.priconest.hiresf00.tm00.grib2',
            'nam.t00z.priconest.hiresf00.tm00.grib2.idx',
            'nam.t06z.conusnest.hiresf00.tm00.grib2',
            'nam.t06z.conusnest.hiresf00.tm00.grib2.idx',
            'nam.t06z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t06z.hawaiinest.hiresf00.tm00.grib2.idx',
            'nam.t06z.priconest.hiresf00.tm00.grib2',
            'nam.t06z.priconest.hiresf00.tm00.grib2.idx',
            'nam.t12z.conusnest.hiresf00.tm00.grib2',
            'nam.t12z.conusnest.hiresf00.tm00.grib2.idx',
            'nam.t12z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t12z.hawaiinest.hiresf00.tm00.grib2.idx',
            'nam.t12z.priconest.hiresf00.tm00.grib2',
            'nam.t12z.priconest.hiresf00.tm00.grib2.idx',
            'nam.t18z.conusnest.hiresf00.tm00.grib2',
            'nam.t18z.conusnest.hiresf00.tm00.grib2.idx',
            'nam.t18z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t18z.hawaiinest.hiresf00.tm00.grib2.idx',
            'nam.t18z.priconest.hiresf00.tm00.grib2',
            'nam.t18z.priconest.hiresf00.tm00.grib2.idx',
        ]
    else:
        return list()


@pytest.mark.asyncio
async def test_get_run_files_to_download_available_files(create_s3_bucket):
    with mock.patch(
        'lib.download_to_s3.list_http_contents',
        side_effect=mock_list_http_contents,
    ):
        files_to_download = await get_files_to_download(
            NAM_HTTP_BASE_URL,
            SCIENCE_BUCKET,
            SCIENCE_KEY_PREFIX,
            datetime.strptime(DATE, MODEL_DATE_FORMAT),
        )
        assert files_to_download == {
            (20190716, 'nam.t00z.conusnest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t00z.hawaiinest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t00z.priconest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t06z.conusnest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t06z.hawaiinest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t06z.priconest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t12z.conusnest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t12z.hawaiinest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t12z.priconest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t18z.conusnest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t18z.hawaiinest.hiresf00.tm00.grib2'),
            (20190716, 'nam.t18z.priconest.hiresf00.tm00.grib2'),
        }


@pytest.mark.asyncio
async def test_get_run_files_to_download_no_files_available(create_s3_bucket):
    with mock.patch(
        'lib.download_to_s3.list_http_contents',
        side_effect=mock_list_http_contents,
    ):
        files = [
            'nam.t00z.conusnest.hiresf00.tm00.grib2',
            'nam.t00z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t00z.priconest.hiresf00.tm00.grib2',
            'nam.t06z.conusnest.hiresf00.tm00.grib2',
            'nam.t06z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t06z.priconest.hiresf00.tm00.grib2',
            'nam.t12z.conusnest.hiresf00.tm00.grib2',
            'nam.t12z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t12z.priconest.hiresf00.tm00.grib2',
            'nam.t18z.conusnest.hiresf00.tm00.grib2',
            'nam.t18z.hawaiinest.hiresf00.tm00.grib2',
            'nam.t18z.priconest.hiresf00.tm00.grib2',
        ]
        for file in files:
            create_s3_bucket.put_object(
                Bucket=SCIENCE_BUCKET,
                Key=f'{SCIENCE_KEY_PREFIX}/{RUN}/{file}',
            )

        files_to_download = await get_files_to_download(
            NAM_HTTP_BASE_URL,
            SCIENCE_BUCKET,
            SCIENCE_KEY_PREFIX,
            datetime.strptime(DATE, MODEL_DATE_FORMAT),
        )
        assert files_to_download == set()
