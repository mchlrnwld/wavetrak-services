import logging
import os

from job_secrets_helper import get_secret  # type: ignore

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO').upper()
level = (
    logging.getLevelName(LOG_LEVEL)
    if LOG_LEVEL in ('CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'NOTSET')
    else logging.INFO
)
logging.basicConfig(level=level)

NOAA_HTTP = os.environ['NOAA_HTTP']
NAM_BASE_PATH = os.environ['NAM_BASE_PATH']
NAM_HTTP_BASE_URL = f'{NOAA_HTTP}/{NAM_BASE_PATH}'
ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']
LOCAL_FILE_PATH = os.getenv('AIRFLOW_TMP_DIR', 'data')
MAX_CONCURRENCY = int(os.environ['MAX_CONCURRENCY'])
CHUNK_SIZE = int(os.environ['CHUNK_SIZE'])
DOWNLOAD_LIMIT = int(os.environ['DOWNLOAD_LIMIT'])
