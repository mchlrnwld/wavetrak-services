import asyncio
import logging
import re
from contextlib import AsyncExitStack
from datetime import datetime
from typing import List, Optional, Set, Tuple

import boto3  # type: ignore
from grib_helpers.inventory import parse_byte_ranges_from_grib_inventory

from lib.helpers import list_http_contents, list_s3_run_files

logger = logging.getLogger('download-to-s3')

client = boto3.client('s3')

# Matches: nam.20190612/ nam.20190612
# Does not match: nam.2019061200/ nam.2019061200
DATE_REGEX = re.compile(r'^nam\.\d{8}\/?$')

# Matches:
# nam.t00z.conusnest.hiresf00.tm00.grib2
# nam.t00z.hawaiinest.hiresf00.tm00.grib2
# nam.t00z.priconest.hiresf00.tm00.grib2
NAM_BASE_REGEX_STR = (
    r'^nam\.'
    r't\d{2}z\.'
    r'(conus|hawaii|prico)nest\.'
    r'hiresf\d{2}\.'
    r'tm\d{2}\.'
    r'grib2'
)

NAM_GRIB_REGEX_STR = NAM_BASE_REGEX_STR + r'$'
NAM_GRIB_REGEX = re.compile(NAM_GRIB_REGEX_STR)

NAM_IDX_REGEX_STR = NAM_BASE_REGEX_STR + r'\.idx$'
NAM_IDX_REGEX = re.compile(NAM_IDX_REGEX_STR)

MODEL_DATE_FORMAT = '%Y%m%d'


async def build_download_spec(
    run: int,
    grib_file: str,
    science_bucket: str,
    science_key_prefix: str,
    run_url: str,
    indices: Set[Tuple[str, str]],
    semaphore: Optional[asyncio.Semaphore] = None,
) -> Tuple[str, str, List[Tuple[int, Optional[int]]]]:
    """
    Builds a download specification containing an HTTP URL, an S3 URL, and a
    byte range.

    Args:
        run:                The model run associated with the grib file.
        grib_file:          The grib file to download from NOAA then
                            upload to S3.
        science_bucket:     The S3 bucket to store the file.
        science_key_prefix: The prefix which the run_files
                            should be stored under.
        run_url:            The url location from where to download
                            the grib files for the specific run.
        indices:            A set of tuples containing indices of NAM
                            variables to download.
        semaphore:          (optional) The asyncio Semaphore to control
                            concurrency.

    Returns:
        A tuple containing an HTTP URL, an S3 URL, and a byte range.
    """
    async with AsyncExitStack() as stack:
        if semaphore:
            await stack.enter_async_context(semaphore)

        grib_url = f'{run_url}/nam.{run}/{grib_file}'
        inventory_url = f'{grib_url}.idx'
        s3_url = (
            f's3://{science_bucket}/{science_key_prefix}/{run}/{grib_file}'
        )
        byte_ranges = await parse_byte_ranges_from_grib_inventory(
            inventory_url, indices
        )

        return (grib_url, s3_url, byte_ranges)


async def get_available_dates(
    model_url: str, timestamp: datetime
) -> List[str]:
    """
    Gets a list of dates with grib files available, starting at the time
    represented by `timestamp`

    NOAA NAM file and directory structure:
        date: 20190710, 20181201
        run:  20190710, 20181201

    Args:
        model_url: The URL where the runs should be downloaded from for the
                   desired model.
        timestamp: Datetime object representing the desired start date to
                   check for runs.

    Returns:
        A list of date strings with grib files available, in yyyymmdd format
    """
    raw_dates = [
        dir.replace('/', '').replace('nam.', '')
        for dir in await list_http_contents(model_url)
        if DATE_REGEX.match(dir)
    ]

    valid_dates = [
        date
        for date in raw_dates
        if datetime.strptime(date, MODEL_DATE_FORMAT) >= timestamp
    ]

    return valid_dates


async def get_available_grib_files(
    model_url: str, dates: List[str], max_concurrency: Optional[int] = None,
) -> Set[Tuple[int, str]]:

    """
    Gets a list of grib files available for a list of dates.

    Args:
        model_url:       The URL where the runs should be downloaded from for
                         the desired model.
        dates:           A list of date strings in yyyymmdd format
        max_concurrency: The max number of concurrent downloads

    Returns:
        A set of tuples containing run date string in `yyyymmdd` format
        and a grib filename for all dates given with grib files available
    """
    # Get available gribs for each date
    semaphore = asyncio.Semaphore(max_concurrency) if max_concurrency else None
    raw_gribs = list(
        zip(
            dates,
            await asyncio.gather(
                *[
                    list_http_contents(f'{model_url}/nam.{date}/', semaphore)
                    for date in dates
                ]
            ),
        )
    )

    # Determine which grib files have indices
    grib_files_with_idx = set(
        [
            f'nam.{date}/{grib}'
            for (date), gribs in raw_gribs
            for grib in gribs
            if NAM_IDX_REGEX.match(grib)
        ]
    )

    available_grib_files = set(
        [
            (int(date), grib)
            for (date), gribs in raw_gribs
            for grib in gribs
            if NAM_GRIB_REGEX.match(grib)
            if f'nam.{date}/{grib}.idx' in grib_files_with_idx
        ]
    )

    return available_grib_files


def get_existing_grib_files(
    science_bucket: str, science_key_prefix: str, dates: List[str],
) -> Set[Tuple[int, str]]:

    """
    Gets a list of grib files existing in S3 for a list of dates.

    Note: We should do this async too but there are currently version mismatch
    issues with aioboto3, aiobotocore, and boto3. It's cleaner to hold off
    on boto3 async for now.

    Args:
        science_bucket:     The S3 bucket to check.
        science_key_prefix: The prefix to look at in S3.
        dates:              A list of date strings in yyyymmdd format

    Returns:
        A set of tuples containing run datetime string in `yyyymmddHH` format
        and a grib filename for all dates given with grib files existing in S3
    """
    raw_gribs = list(
        zip(
            dates,
            [
                list_s3_run_files(
                    int(date), science_bucket, science_key_prefix
                )
                for date in dates
            ],
        )
    )
    existing_files = set(
        [
            (
                int(date),
                grib['Key'].replace(f'{science_key_prefix}/{date}/', ''),
            )
            for (date), gribs in raw_gribs
            for grib in gribs
        ]
    )

    return existing_files


async def get_files_to_download(
    model_url: str,
    science_bucket: str,
    science_key_prefix: str,
    timestamp: datetime,
    max_concurrency: Optional[int] = None,
) -> Set[Tuple[int, str]]:
    """
    Gets a list of grib files available that have not yet been downloaded from
    NOAA and uploaded to S3.

    NOAA NAM file and directory structure:
        date: 20190710, 20181201
        run:  20190710, 20181201

    Args:
        model_url:          The URL where the runs should be downloaded from
                            for the desired model.
        science_bucket:     The S3 bucket to check.
        science_key_prefix: The prefix to look at in S3.
        timestamp:          Datetime object representing the desired start
                            date to check for runs.
        max_concurrency:    The max number of concurrent downloads

    Returns:
        A set of tuples containing run datetime string in `yyyymmddHH` format
        and a grib filename for all dates given with grib files available that
        have not yet been downloaded from NOAA and uploaded to S3.
    """
    available_dates = await get_available_dates(model_url, timestamp)
    logger.debug(f'Available dates: {available_dates}')

    available_grib_files = await get_available_grib_files(
        model_url, available_dates, max_concurrency
    )
    logger.debug(f'Available files: {list(available_grib_files)}')

    # We should do this async too but there are currently version mismatch
    # issues with aioboto3, aiobotocore, and boto3. It's cleaner to hold off
    # on boto3 async for now.
    existing_grib_files = get_existing_grib_files(
        science_bucket, science_key_prefix, available_dates
    )
    logger.debug(f'Existing files: {list(existing_grib_files)}')

    files_to_download = available_grib_files - existing_grib_files
    logger.debug(f'Files to download (len): {list(files_to_download)}')

    return files_to_download
