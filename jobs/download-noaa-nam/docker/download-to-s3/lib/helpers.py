import asyncio
from contextlib import AsyncExitStack
from typing import Dict, List, Optional

import boto3  # type: ignore
from bs4 import BeautifulSoup  # type: ignore
from file_transfer import async_get_file_from_http  # type: ignore

client = boto3.client('s3')


async def list_http_contents(
    url: str, semaphore: Optional[asyncio.Semaphore] = None
) -> List[str]:
    """
    Gets a list of the http contents from the given URL.

    Args:
        url: The url from which to get a list of http contents.

    Returns:
        A list of http contents from the given URL if present,
        otherwise the empty list is returned.
    """
    async with AsyncExitStack() as stack:
        if semaphore:
            await stack.enter_async_context(semaphore)

        text = await async_get_file_from_http(url)
        soup = BeautifulSoup(text, 'html.parser')
        return [
            node.get('href')
            for node in soup.find_all('a')
            if 'Parent Directory' not in node.contents
        ]


def list_s3_run_files(
    run: int, science_bucket: str, science_key_prefix: str
) -> List[Dict[str, str]]:
    """
    List existing run files from the
    given S3 bucket and prefix.

    Args:
        run:                The run to check for in S3.
        science_bucket:     The S3 bucket to check.
        science_key_prefix: The prefix to look at in S3.

    Returns:
        A list of s3 object dictionaries.
    """
    response = client.list_objects_v2(
        Bucket=science_bucket, Prefix=f'{science_key_prefix}/{run}',
    )
    if 'Contents' not in response:
        return []
    return response['Contents']
