import asyncio
import logging
from datetime import datetime, timedelta
from timeit import default_timer

from file_transfer import async_stream_files_from_http_to_s3  # type: ignore
from grib_helpers.indices import NAM_INDICES

import lib.config as config
from lib.download_to_s3 import build_download_spec, get_files_to_download

logger = logging.getLogger('download-to-s3')

MODEL_HOUR_FORMAT = '%Y%m%d'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    timestamp = datetime.now() - timedelta(hours=48)
    logger.info(f'Indexing new model data starting {timestamp}...')
    start = default_timer()

    # Get a list of files to download
    files_to_download = set(
        list(
            await get_files_to_download(
                config.NAM_HTTP_BASE_URL,
                config.SCIENCE_BUCKET,
                config.SCIENCE_KEY_PREFIX,
                timestamp,
                config.MAX_CONCURRENCY,
            )
        )[: config.DOWNLOAD_LIMIT]
    )
    if not files_to_download:
        logger.info('No files to download')
        return

    elapsed = default_timer() - start
    logger.info(
        f'Indexed {len(files_to_download)} GRIBS to download in {elapsed}s'
    )

    # Build a list of URLs for the files to download. This involves
    # downloading inventory files and parsing byte ranges for the variables
    # that we care about.
    start = default_timer()
    logger.info(f'Downloading {len(files_to_download)} GRIB inventories...')

    semaphore = asyncio.Semaphore(config.MAX_CONCURRENCY)
    results = await asyncio.gather(
        *[
            build_download_spec(
                run,
                grib_file,
                config.SCIENCE_BUCKET,
                config.SCIENCE_KEY_PREFIX,
                config.NAM_HTTP_BASE_URL,
                NAM_INDICES,
                semaphore,
            )
            for run, grib_file in files_to_download
        ],
        return_exceptions=True,
    )

    # Since we might be dealing with a large number of files and HTTP can be
    # fickle, we should warn about download failures, download as many files
    # as we can, and raise the exception at the end
    inventory_exceptions = [
        result for result in results if isinstance(result, BaseException)
    ]

    if len(inventory_exceptions) > 0:
        logger.warning(
            f'The following async exceptions were raised while '
            f'downloading and parsing inventory files: '
            f'{inventory_exceptions}'
        )

    urls = [
        result for result in results if not isinstance(result, BaseException)
    ]

    elapsed = default_timer() - start
    logger.info(
        f'Downloaded {len(files_to_download)} GRIB inventories in {elapsed}s'
    )

    # Stream the files from their HTTP source to S3
    start = default_timer()
    logger.info(f'Streaming {len(urls)} GRIBs to S3')
    await async_stream_files_from_http_to_s3(
        urls=urls,
        max_concurrency=config.MAX_CONCURRENCY,
        chunk_size=config.CHUNK_SIZE,
    )
    elapsed = default_timer() - start

    logger.info(f'Streamed {len(urls)} GRIBs from NOAA to S3 in {elapsed}s')

    if len(inventory_exceptions) > 0:
        raise BaseException(inventory_exceptions)


if __name__ == '__main__':
    asyncio.run(main())
