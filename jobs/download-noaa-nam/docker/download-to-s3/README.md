# Download to S3

Docker image to download NAM model data to S3. This image downloads any model files we're missing from NOAA into in S3 to be ready for processing.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate download-noaa-nam-download-to-s3
$ cp .env.sample .env
$ env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t download-noaa-nam/download-to-s3 .
$ cp .env.sample .env
$ docker run --env-file=.env --volume $(pwd)/data:/opt/app/data --rm -it download-noaa-nam/download-to-s3
```
