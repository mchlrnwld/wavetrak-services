#!/usr/bin/env bash
mkdir -p dependencies
aws --profile=dev s3 sync --exact-timestamps s3://wt-ml-artifacts/dependencies dependencies
aws --profile=dev s3 sync --exact-timestamps s3://wt-ml-artifacts/models models
