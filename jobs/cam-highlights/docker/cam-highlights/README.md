# Highlights Batch Job

This service implements the logic to create highlight clips for the ML smart-highlights/crowd-counting pipeline. Please refer to the
ML pipeline architecture [document](https://github.com/Surfline/wavetrak-services/pull/4106) for more information regarding the pipeline
as a whole. In summary, this service:
1. Is triggered by the `wt-frame-extractor-and-highlights-trigger` lambda function.
2. Receives a 10 minute rewind as input (via recording id which it uses to fetch clip details)
3. The job will then get timestamps for which sets occur, and concatenate the sets into one clip.
4. The highlight clip will be put in S3 into the highlights bucket, and the locations of the clips and thumbnails will be added to the recording object in mongo via the cameras/recording API.

## Development

This service requires a hefty amount of hard-drive/RAM space to build and run. Hence, instead of developing locally,
it is often easier to spin up a EC2 instance, and develop remotely. The below commands show you how to spin up a `g4dn.xlarge`
instance (16 GB of RAM with 120 GB of hard-drive space). Before running these commands, make sure to download the `.pem` file
from `1Password` (which `.pem` file depends on the environment you are choosing, i.e. `surfline-dev|prod|legacy`) and export
your environment variables (described in [this](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1107951999/Create+a+Temporary+EC2+Instance) document).

```
# Enter into the remote environment
make enter_remote_environment

# Copy your local work to the remote environment
make copy_dir_to_remote_environment

# Shutdown the remote environment
make shutdown_remote_environment
```

### copy the dev credentials over to the ec2 instance:

1. copy the sample creds to a proper file

  `cp dev_credentials.sample dev_credentials`

2. populate that file with your dev credentials

3. run `make copy_credentials` (ensuring you have an ec2 instance running)


Make sure you are connected to the corresponding VPN.

## Development Commands

To run the service, use the below commands:

```
# Copy sample environment file, make sure to populate yours
cp .env.sample .env

# sets up the conda devenv (for linting/formatting only) and copies in the test clip
# (if the clip can't be found find a new one in s3 but make sure it's saved with the
# same filename as the one in the makefile)
make setup

# Run the integration test
make test
```

## Deployment

To deploy the terraform run `make plan ENV=SANDBOX` (swapping out sandbox for the required env) and
if the output from that looks sensible (there's often some strange diff with the `container_properties`)
then run `make apply ENV=SANDBOX`.

The docker image can be deployed in jenkins in `shared/deploy-airflow-job` this will automatically update the batch job as the arn doesn't change

## Monitoring

Can be done in the aws console by viewing the Batch dashboard. Jobs should come in and move into RUNNABLE status, then after a while they'll go into starting and after another while they'll go into running and then succeeded, if they go into failed then there's a bug or an error we need to preempt to ensure it fails gracefully.