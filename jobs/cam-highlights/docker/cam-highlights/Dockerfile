FROM nvcr.io/nvidia/cuda:10.2-cudnn7-runtime-ubuntu18.04

ENV TENSORRT_TAR=TensorRT-7.0.0.11.Ubuntu-18.04.x86_64-gnu.cuda-10.2.cudnn7.6.tar.gz
ENV TENSORRT_VERSION=TensorRT-7.0.0.11
ENV TENSORFLOW_WHL_FILE=tensorflow-2.3.0-cp37-cp37m-linux_x86_64.whl

ENV NVIDIA_HEADERS_VERSION=9.1.23.1
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES video,compute,utility

ARG FFMPEG_VERSION=4.2.2
ARG FFMPEG_SRC=/usr/local
ARG PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  autoconf \
  automake \
  build-essential \
  bzip2 \
  ca-certificates \
  checkinstall \
  cmake \
  curl \
  dialog \
  g++ \
  gcc \
  git \
  libsm6 \
  libssl-dev \
  libtool \
  libxext6 \
  libxml2-dev \
  libxslt-dev \
  libxrender-dev \
  libx264-dev \
  lsb-release \
  make \
  nasm \
  net-tools \
  perl \
  python3.7 \
  python3-pip \
  pkg-config \
  tar \
  wget \
  x264 \
  yasm \
  zlib1g-dev \
  && apt-get clean all \
  && rm -rf /var/lib/apt/lists/*

RUN \
	DIR=/tmp/nv-codec-headers && \
	git clone https://github.com/FFmpeg/nv-codec-headers ${DIR} && \
	cd ${DIR} && \
	git checkout n${NVIDIA_HEADERS_VERSION} && \
	sed -i 's@/usr/local@'"$FFMPEG_SRC"'@' Makefile && \
	make && \
	make install  && \
    rm -rf ${DIR}

RUN mkdir ffmpeg-$FFMPEG_VERSION && cd ffmpeg-$FFMPEG_VERSION \
  && curl -sO http://ffmpeg.org/releases/ffmpeg-$FFMPEG_VERSION.tar.gz \
  && tar --strip-components=1 -xzf ffmpeg-$FFMPEG_VERSION.tar.gz \
  && rm ffmpeg-$FFMPEG_VERSION.tar.gz \
  && ./configure --prefix=$SRC \
  --extra-cflags="-I$SRC/include -I${FFMPEG_SRC}/include/ffnvcodec -I/usr/local/cuda/include/" \
  --extra-ldflags="-L$SRC/lib -L/usr/local/cuda/lib64/ -L/usr/local/cuda/lib32/" \
  --bindir=$SRC/bin \
  --disable-ffplay \
  --enable-gpl \
  --enable-nonfree \
  --enable-libx264 \
  --enable-zlib \
  --enable-postproc \
  --enable-swscale \
  --enable-pthreads \
  --enable-version3 \
  --enable-libx264 \
  --enable-cuda \
  --enable-cuvid \
  --enable-nvenc && \
  make -j`nproc` && \
  make install && \
  make distclean && \
  hash -r && \
  cd tools && \
  make qt-faststart && \
  cp qt-faststart $SRC/bin \
  && cd ../ \
  && rm -rf ffmpeg-$FFMPEG_VERSION\
  && rm -rf /ffmpeg-4.2.2/tests

####

# Make a fake cuda 10.1 runtime symlink so tf and torch binaries work with 10.2
RUN ln -s /usr/local/cuda/lib64/libcudart.so.10.2 /usr/local/cuda/lib64/libcudart.so.10.1

# Copy over custom tensorflow binaries (original copy in s3)
COPY dependencies /opt/
COPY requirements.txt ./
# Install requirements and custom tensorflow wheel file compiled against cuda 10.2
RUN python3.7 -m pip install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache-dir --upgrade setuptools wheel
RUN pip3 install --no-cache-dir /opt/$TENSORFLOW_WHL_FILE && rm /opt/$TENSORFLOW_WHL_FILE

# TRTorch download and install
# TRTorch wants torch==1.5.1 but torchvision wants 1.5.0, so use 1.5.0. Don't see any issues with TRTorch + torch1.5.0
RUN wget https://github.com/NVIDIA/TRTorch/releases/download/v0.0.3/trtorch-0.0.3-cp37-cp37m-linux_x86_64.whl \
  && pip3 install --no-cache-dir trtorch-0.0.3-cp37-cp37m-linux_x86_64.whl && rm trtorch-0.0.3-cp37-cp37m-linux_x86_64.whl\
  && pip3 install --no-cache-dir torchvision==0.6.0 \
  && tar xvfz /opt/$TENSORRT_TAR -C /opt && rm /opt/$TENSORRT_TAR \
  && chown $(whoami) -R /opt/$TENSORRT_VERSION \
  && cd /opt/$TENSORRT_VERSION/python \
  && pip3 install --no-cache-dir $(ls *cp37*) \
  && pip3 install --no-cache-dir /opt/$TENSORRT_VERSION/uff/*py3* \
  && pip3 install --no-cache-dir /opt/$TENSORRT_VERSION/graphsurgeon/*py3* \
  && rm -rf ./opt/$TENSORRT_VERSION/doc \
  && rm -rf ./opt/$TENSORRT_VERSION/data \
  && rm -rf ./opt/$TENSORRT_VERSION/samples\
  && echo "export LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda/lib:/usr/local/cuda/lib64/:/usr/local/cuda/extras/CUPTI/lib64/:/opt/$TENSORRT_VERSION/lib:$LD_LIBRARY_PATH" >> $HOME/.bashrc

ENV PATH="/opt/$TENSORRT_VERSION/targets/x86_64-linux-gnu/lib:/opt/$TENSORRT_VERSION/include:/opt/$TENSORRT_VERSION/targets/x86_64-linux-gnu/lib/:${PATH}"
ENV LD_LIBRARY_PATH="/opt/$TENSORRT_VERSION/lib:/opt/$TENSORRT_VERSION/targets/x86_64-linux-gnu/lib/:/opt/$TENSORRT_VERSION/targets/x86_64-linux-gnu/lib/:${LD_LIBRARY_PATH}"

# RUN source $HOME/.bashrc
ENV PYTHONPATH=/opt:${PYTHONPATH}:/usr/src/app/

RUN pip3 install --no-cache-dir -r requirements.txt
COPY models /opt/ml/models

# Install sl_camera_sensor package
COPY sl_camera_sensor/ /opt/sl_camera_sensor/
RUN python3.7 /opt/sl_camera_sensor/setup.py install

COPY src/ /usr/src/app
WORKDIR /usr/src/app
