import json
import os
from urllib.parse import urlparse

import boto3  # type: ignore
from moto import mock_s3  # type: ignore
import pytest  # type: ignore
import responses

from entrypoint_highlights import main

RECORDING_ID = '58349c1fe411dc743a5d52ad'
CAMERA_ID = '583499c4e411dc743a5d5296'
RECORDING_PATH = '/cameras/recording/'
RECORDING_URL = f'{os.environ["CAMERAS_API"]}{RECORDING_PATH}{RECORDING_ID}'
RECORDING_GET_URL = (
    f'{os.environ["CAMERAS_API"]}{RECORDING_PATH}recordingId/{RECORDING_ID}'
)
CLIP_NAME = 'live/wc-hbpierns.stream.20180227T201716.mp4'
RECORDING_BASE_URL = 'http://camrewinds.cdn-surfline.com/'


def get_all_keys_from_bucket(bucket: str):
    s3 = boto3.resource('s3')
    bucket_client = s3.Bucket(bucket)
    return [obj.key for obj in bucket_client.objects.all()]


@pytest.fixture
def results_dir():
    if not os.path.isdir('results'):
        os.mkdir('results')


@pytest.fixture
def create_s3_buckets():
    with mock_s3():
        s3_client = boto3.resource('s3')
        s3_client.create_bucket(Bucket=os.environ['DESTINATION_BUCKET'])
        s3_client.create_bucket(Bucket=os.environ['REWIND_BUCKET'])
        s3_client.create_bucket(Bucket=os.environ['WAVETRACK_BUCKET'])
        s3_client.create_bucket(Bucket=os.environ['HOMOGRAPHY_BUCKET'])
        yield s3_client


def upload_clip(
    filename,
    mp4_name='/usr/src/app/fixtures/wc-hbpierns.stream.20210208T212408646.mp4',
):
    s3_client = boto3.resource('s3')
    s3_client.meta.client.upload_file(
        mp4_name,
        os.environ['REWIND_BUCKET'],
        filename,
        ExtraArgs={'ContentType': 'video', 'ACL': 'public-read'},
    )


@pytest.fixture
def intercept_requests():
    with responses.RequestsMock() as rsps:
        rsps.add(responses.PATCH, RECORDING_URL, json={})
        rsps.add(
            responses.GET,
            RECORDING_GET_URL,
            json={
                "_id": "58349c1fe411dc743a5d52ad",
                "cameraId": CAMERA_ID,
                "alias": "a-test",
                "startDate": "2018-02-28T23:26:07.455Z",
                "endDate": "2018-02-28T23:36:07.455Z",
                "recordingUrl": RECORDING_BASE_URL + CLIP_NAME,
                "resolution": "1280x720",
            },
        )
        yield rsps


def test_highlights_cam(create_s3_buckets, intercept_requests, results_dir):
    upload_clip(CLIP_NAME)

    main(['--recording_id', '58349c1fe411dc743a5d52ad'])

    start = '20180228T232607455Z'

    highlights_slug = f'{CAMERA_ID}-{start}'

    highlights_url = os.environ["HIGHLIGHTS_URL"]

    highlights_thumbs = get_all_keys_from_bucket(
        os.environ['DESTINATION_BUCKET']
    )
    expected_keys = [
        f'thumbnails/{highlights_slug}.jpg',
        f'gifs/{highlights_slug}.gif',
        f'clips/{highlights_slug}.mp4',
    ]

    assert all(
        expected_key in highlights_thumbs for expected_key in expected_keys
    )

    # download the actual clip. (This is for a human, i.e. you, to look at rather
    # than for any futher assertions. (By now the temp dir it gets stored in will
    # have been destroyed.))
    s3_client = boto3.resource('s3')
    s3_client.Bucket(os.environ['DESTINATION_BUCKET']).download_file(
        f'clips/{highlights_slug}.mp4', f'results/{highlights_slug}.mp4'
    )
    s3_client.Bucket(os.environ['DESTINATION_BUCKET']).download_file(
        f'gifs/{highlights_slug}.gif', f'results/{highlights_slug}.gif'
    )
    s3_client.Bucket(os.environ['DESTINATION_BUCKET']).download_file(
        f'thumbnails/{highlights_slug}.jpg', f'results/{highlights_slug}.jpg'
    )

    wavetracks = get_all_keys_from_bucket(os.environ['WAVETRACK_BUCKET'])

    assert len(wavetracks) == 1
    assert wavetracks[0] == 'wc-hbpierns.wavetracks.20180227T201716.parquet'

    recording_requests = [
        call.request
        for call in intercept_requests.calls
        if urlparse(call.request.url).path.startswith(RECORDING_PATH)
    ]

    assert len(recording_requests) == 2
    assert json.loads(recording_requests[1].body) == {
        'highlights': {
            'url': f'{highlights_url}/clips/{highlights_slug}.mp4',
            'thumbUrl': f'{highlights_url}/thumbnails/{highlights_slug}.jpg',
            'gifUrl': f'{highlights_url}/gifs/{highlights_slug}.gif',
        }
    }


@pytest.fixture(
    params=[
        '/usr/src/app/test-clips/uk-praasands.trim.20210910T133010400.mp4',
        '/usr/src/app/test-clips/uk-praasands.trim.20210910T134010480.mp4',
    ]
)
def flat_rewind(request):
    yield request.param


def test_flat_conditions(
    create_s3_buckets, intercept_requests, results_dir, flat_rewind
):
    # Test some flat days where we know it has been prone to error.
    start = '20180228T232607455Z'
    highlights_slug = f'{CAMERA_ID}-{start}'
    print(f"testing flat rewind {flat_rewind}")
    upload_clip(CLIP_NAME, mp4_name=flat_rewind)
    main(['--recording_id', '58349c1fe411dc743a5d52ad'])

    s3_client = boto3.resource('s3')
    s3_client.Bucket(os.environ['DESTINATION_BUCKET']).download_file(
        f'clips/{highlights_slug}.mp4', f'results/{highlights_slug}.mp4'
    )

    assert os.path.isfile(f'results/{highlights_slug}.mp4')
    assert os.path.getsize(f'results/{highlights_slug}.mp4') > 1e6
