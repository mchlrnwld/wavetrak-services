import logging

from common import config

# set log level incase external modules create their own loggers
logging.basicConfig(level=config.LOG_LEVEL)
# create the main logger and stream log to stderr
logger = logging.getLogger()
logger.setLevel(config.LOG_LEVEL)
logger.addHandler(logging.StreamHandler())


def get_logger():
    return logger
