import re
from typing import Any, Dict
from urllib.parse import urljoin

import boto3  # type: ignore
import requests

from common import config, logger

_logger = logger.get_logger()
S3_TYPE_PARAMS = {
    'thumb': {
        's3_path': 'thumbnails',
        'file_ending': 'jpg',
        'content_type': 'image/jpeg',
    },
    'gif': {
        's3_path': 'gifs',
        'file_ending': 'gif',
        'content_type': 'image/gif',
    },
    'clip': {
        's3_path': 'clips',
        'file_ending': 'mp4',
        'content_type': 'video/mp4',
    },
}


class Clips:
    """
    Save data to S3 and Mongo.
    """

    def __init__(self, recording, highlight_clip):
        self.camera_id = recording.camera_id
        self.highlight_clip = highlight_clip
        self.dest_bucket = config.DESTINATION_BUCKET
        self.recording_id = recording.recording_id
        self.start_date = re.sub(r'[:\-\.]', '', recording.start_date_string)

    def upload_to_s3(self, type, path):
        params = S3_TYPE_PARAMS[type]
        filename = f'{self.camera_id}-{self.start_date}'
        object_key = f'{params["s3_path"]}/{filename}.{params["file_ending"]}'
        s3 = boto3.client('s3')
        s3.upload_file(
            path,
            self.dest_bucket,
            object_key,
            ExtraArgs={
                'ACL': 'public-read',
                'ContentType': params['content_type'],
            },
        )
        _logger.info(
            f'Uploaded asset {path} to: s3://{self.dest_bucket}/{object_key}'
        )
        return object_key

    def upload(self):
        """
        Uploads highlight clips, along with jpg and gif
        thumbnails to S3.

        Returns:
            Data payload of object keys.
        """
        _logger.info('Saving output to Mongo and S3.')

        for file_type in S3_TYPE_PARAMS.keys():
            path = self.highlight_clip[f'{file_type}_path']
            self.highlight_clip[f'{file_type}_object_key'] = self.upload_to_s3(
                file_type, path
            )

        data = {
            'highlights': {
                'url': (
                    f'{config.HIGHLIGHTS_URL}/'
                    f'{self.highlight_clip["clip_object_key"]}'
                ),
                'thumbUrl': (
                    f'{config.HIGHLIGHTS_URL}/'
                    f'{self.highlight_clip["thumb_object_key"]}'
                ),
                'gifUrl': (
                    f'{config.HIGHLIGHTS_URL}/'
                    f'{self.highlight_clip["gif_object_key"]}'
                ),
            }
        }

        # PATCH highlights info.
        self.patch_request(data)

    def patch_request(self, data: Dict) -> Dict[str, Any]:
        """
        Given a domain and endpoint strings, and a dict of data,
        send a HTTP PATCH request, returning a JSON response.

        Args:
            domain: HTTP domain
            endpoint: HTTP endpoint
            data: Payload to PATCH

        Returns:
            Response payload
        """
        url = urljoin(
            config.CAMERAS_API, f'cameras/recording/{self.recording_id}'
        )
        r = requests.patch(url, json=data)
        _logger.info(f'PATCH {url} with {data}: {r.status_code}')
        r.raise_for_status()

        return r.json()
