import os

from common import config, logger, timer
from sl_camera_sensor.lib.crowds.clip_above_thresh import (  # type: ignore
    clip_above_thresh,
)
from sl_camera_sensor.lib.localization.localize_dets import (  # type: ignore
    LocalizeDets,
)
from sl_camera_sensor.lib.wave_tracking.wave_tracker_iou import (  # type: ignore
    WaveTracker,
)
from sl_camera_sensor.nets.detection.detection_model import (  # type: ignore
    DetectionModel,
)
from sl_camera_sensor.nets.segmentation.segmentation_model import (  # type: ignore
    SegmentationModel,
)

_logger = logger.get_logger()


def generate_wavetracks(recording, fps, threshold=0.25, run_detections=False):
    """
    This runs 50% of our vision stack.
    Can add vis and texture if we want.
    """
    seg_bot = SegmentationModel(batch_size=config.BATCH_SIZE)

    if run_detections:
        _logger.info('Running object detection.')
        det_bot = DetectionModel()

        localizer = LocalizeDets(
            dets_label_map=det_bot.label_map,
            mask_label_map=seg_bot.label_map,
            target_size=config.TARGET_FRAME_SIZE[::-1],
        )

    wave_tracker = WaveTracker(
        fps, seg_bot.label_map, target_size=config.TARGET_FRAME_SIZE[::-1]
    )

    batches = recording.get_frame_batch(config.BATCH_SIZE, fps=fps)

    start = timer.start()
    for batch in batches:
        frames, timestamps = batch
        # run the two deep nets
        masks = seg_bot.predict(
            frames, output_size=config.TARGET_FRAME_SIZE[::-1]
        )
        if run_detections:
            dets = det_bot.predict(frames)
            # clip the dets
            dets = clip_above_thresh(dets, threshold)
            # Localize the dets to fix false surfers on sand -> beach people
            dets = localizer(dets, masks[0])
            # Include dets to populate surfing flag in tracks col
            for mask, det, timestamp in zip(masks, dets, timestamps):
                wave_tracker(
                    mask,
                    dets=det,
                    dets_label_map=det_bot.label_map,
                    timestamp=timestamp,
                )
        else:
            for mask, timestamp in zip(masks, timestamps):
                wave_tracker(mask, timestamp=timestamp)

    wave_tracks = wave_tracker.pop_tracks(return_blobs=False, done=True)

    # Push these wavetracks to S3?
    if config.WAVETRACK_BUCKET:
        if len(wave_tracks) < 0:
            _logger.error(
                'Tried to save wavetracks but wavetracks object is empty'
            )
            return wave_tracks
        pq_name = (
            os.path.basename(recording.local_copy)
            .replace('.stream.', '.wavetracks.')
            .replace('.mp4', '.parquet')
        )
        wavetrack_file_name = f"s3://{config.WAVETRACK_BUCKET}/{pq_name}"
        try:
            wave_tracks.to_parquet(wavetrack_file_name)
        except PermissionError:
            _logger.critical(
                'Unable to push wavetrack parquet to '
                f'{wavetrack_file_name} - check path/permissions'
            )
        except Exception as e:
            _logger.critical(e)
            _logger.critical(
                'Unable to push wavetrack parquet to '
                f'{wavetrack_file_name} - possible malformed dataframe'
            )
    else:
        _logger.error(
            'Requested to save wavetracks but ' 'WAVETRACK_BUCKET env not set.'
        )

    timer.checkpoint('Wave tracking stack took:', start)
    return wave_tracks
