import calendar
import os
import time
from urllib.parse import urlparse

import boto3
import cv2
import numpy as np
import requests
from PIL import Image

from common import config, logger

log = logger.get_logger()


class Recording:
    def __init__(self, recording_id, temp_dir) -> None:
        self.temp_dir = temp_dir
        self.recording_id = recording_id
        self._from_dict(self.fetch_recording())
        self.download_file()
        self.filename = self.object_key.split('/')[-1]

    def fetch_recording(self):
        url = requests.compat.urljoin(
            config.CAMERAS_API,
            f'cameras/recording/recordingId/{self.recording_id}',
        )
        r = requests.get(url)
        log.info(f'GET {url}: {r.status_code}')
        r.raise_for_status()

        return r.json()

    def _from_dict(self, recording):
        self.camera_id = recording['cameraId']
        self.alias = recording['alias']
        self.start_date = timestamp_to_epoch(recording['startDate'])
        self.start_date_string = recording['startDate']
        self.end_date = timestamp_to_epoch(recording['endDate'])
        self.recording_url = recording['recordingUrl']
        self.resolution = recording['resolution']

    def download_file(self):
        path = urlparse(self.recording_url).path
        if path is None:
            raise ValueError(
                f'Invalid recording url for recording id: {self.recording_id}'
            )
        self.object_key = path[1:]  # strips the leading slash
        self.local_copy = os.path.join(
            self.temp_dir, self.object_key.split('/')[-1]
        )
        bucket = config.REWIND_BUCKET
        log.info(
            f'Download s3://{bucket}/{self.object_key} > {self.local_copy}'
        )
        s3 = boto3.resource('s3')
        s3.Bucket(bucket).download_file(self.object_key, self.local_copy)
        log.info('Download succeeded.')

    def get_frame_batch(self, batch_size, max_frames=None, fps=None):
        """10 minutes of video is ~64g of ram in memory.
        This generator will return batches of frames,
        since we might not have enough memory to process
        a whole movie in a single pass.
        :param video_start_timestamp: time in seconds that the video begins.
        Timestamps will be offset by this value, otherwise they'll start at 0+fps.
        :at_timestamps: floating point input array where only frames at these
        timestamps will be returned.
        :returns: frames, timestamps tuple of lists.
        """
        input_video = self.local_copy

        if not os.path.isfile(input_video):
            raise ValueError(
                "Input video '{}' does not exist.".format(input_video)
            )

        resize_width, resize_height = config.TARGET_FRAME_SIZE

        max_frames = max_frames or 36000

        cap = cv2.VideoCapture(input_video)
        native_fps = cap.get(cv2.CAP_PROP_FPS)
        log.info(
            '{0} frames in {1} at {2:.3f} fps'.format(
                int(cap.get(cv2.CAP_PROP_FRAME_COUNT)), input_video, native_fps
            )
        )

        if not fps:
            fps = native_fps

        # Determine source video frame size.
        if cap.isOpened():
            source_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
            source_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

        log.info(
            f'Source size: {source_width}x{source_height} '
            f'target: {resize_width}x{resize_height}'
        )

        cnt = 0
        frames, timestamps = [], []
        timestamp = (cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0) + self.start_date
        flag, frame = cap.read()  # Put this after timestamp calc!
        if fps > native_fps * 1.1:
            log.warning(
                'Requested input fps is '
                'higher than native ({native_fps}fps).'
            )
        while flag:
            if (cnt > 0) and (len(timestamps) == batch_size):
                yield frames, timestamps
                frames, timestamps = [], []

            if cnt > max_frames - 1:
                break

            if cnt % round(native_fps / float(fps)) == 0:
                # Check input source size matches target size if not resize.
                # Model is trained at 720p, some cams are bigger.
                if (
                    source_width is not resize_width
                    and source_height is not resize_height
                ):
                    frame = np.array(
                        Image.fromarray(frame).resize(
                            (resize_width, resize_height), Image.NEAREST
                        )
                    )

                frames.append(bgr2rgb(frame))
                timestamps.append(timestamp)

            timestamp = (
                cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0
            ) + self.start_date
            flag, frame = cap.read()

            cnt += 1

        yield frames, timestamps


def timestamp_to_epoch(timestamp: str) -> float:
    """
    Given a timestamp and (optionally) a timestamp format,
    return the unix time in milliseconds.
    """
    if len(timestamp) < 16:
        # The file is in the old format.
        timestamp = f'{timestamp}000'
    date = time.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')
    unix_sec = calendar.timegm(date)

    log.debug(
        f'Timestamp {timestamp} converted to epoch in seconds: {unix_sec}.'
    )

    return unix_sec


def bgr2rgb(frame):
    """OpenCV convention is BGR, but network wants RGB.
    :param frame: BGR numpy array
    :return: RBG numpy array.
    """
    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
