# -*- coding: utf-8 -*-
import os
import subprocess
import tempfile
import time
from typing import Any, Dict, List

import numpy as np
import pandas as pd
from PIL import Image

from common import config, logger, timer

logger = logger.get_logger()


class HighlightsVideo:
    def __init__(self, video_path, video_start_timestamp, tracks):
        """
        video_path - source video file.
        video_start_timestamp - UNIX timestamp for the start of this file.
        tracks - tracks list
        clips_to_make - how many clips to make per source file
        """
        self.__video_path = video_path
        self.__tracks_df = tracks
        self.__video_start_timestamp = video_start_timestamp
        self.__max_duration = config.MAX_DURATION
        self.__start_buffer = config.START_BUFFER
        self.__end_buffer = config.END_BUFFER
        self.__temp_dir_obj = tempfile.TemporaryDirectory()

    def generate_clips(self, encode_video=True) -> Dict[str, Any]:
        # ==== Condense the list of tracks into clip ranges.
        # i.e. take overlappings tracks and just make one clip
        # vs clip per track.
        wave_timestamps = self.__get_timestamps_from_tracks(self.__tracks_df)
        try:
            df = pd.DataFrame(wave_timestamps, columns=['start', 'end'])
        except ValueError:
            # This happens with an empty df. We shouldn't be in here,
            # but if we are presumably there are no waves - either the
            # visibility is bad or it's flat and we haven't properly
            # added the logic to deal with that yet.
            df = pd.DataFrame(
                self.__empty_timestamps(), columns=['start', 'end']
            )

        tracks_count = len(df)

        if tracks_count == 0:
            raise ValueError('No waves found')

        # Offset to get seconds relative to start time of vid
        df -= self.__video_start_timestamp

        # Recalculate length so it matches the new clip length.
        df['length'] = df['end'] - df['start']

        df = df[df['length'] > 0]

        clips_count = len(df)
        logger.info(f'Generating {clips_count} clips')

        start = timer.start()
        t0 = time.time()
        # === Format clip ranges for ffmpeg
        clip_paths = []
        for index, row in df.iterrows():
            start, end = row[['start', 'end']]
            clip_path = f'{self.__temp_dir_obj.name}/clip-{index}.mp4'
            clip_paths.append(clip_path)

            if encode_video:
                self.__trim_clip(clip_path, start, end)

        timer.checkpoint('Generating clips took: ', start)
        logger.info(f'Generating clips took: {time.time()-t0:.1f}')
        df['clip_path'] = clip_paths

        # Concatenate all these clips into a single mp4, gif, and jpg.
        return self.__concat_clips(df)

    def __concat_clips(self, df) -> Dict[str, Any]:
        """Concatenate a list of clips into a single one, and modify the incoming df
        to reflect that change
        """
        cmd = ['ffmpeg']

        df['file_entry'] = 'file ' + df['clip_path'].astype(str)

        clips = '\n'.join(df['file_entry'].values)
        concat_list_file = f'{self.__temp_dir_obj.name}/concat_list.txt'
        with open(concat_list_file, 'w') as f:
            f.write(clips)

        output_path = os.path.join(
            os.path.dirname(df['clip_path'].values[0]), 'clips-full.mp4'
        )
        cmd += [
            '-f',
            'concat',
            '-safe',
            '0',
            '-i',
            concat_list_file,
            '-vcodec',
            'h264_nvenc',
            '-c',
            'copy',
            output_path,
        ]

        ok_msg = f'Concatenating {len(df)} clips to {output_path}'
        err_msg = 'Could not concatenate clips'
        self.__run_subprocess(cmd, ok_msg, err_msg)

        # Now concatenate the df, make a new thumbnail, and a gif.
        # thumbnail - first wave is always the largest
        thumb_path = output_path.replace('.mp4', '.jpg')
        self.__create_thumbnail(df['clip_path'].iloc[0], thumb_path)
        # gif
        gif_path = output_path.replace('.mp4', '.gif')
        self.__create_gif(df['clip_path'].iloc[0], gif_path)
        return {
            'length': df['length'].sum(),
            'clip_path': output_path,
            'thumb_path': thumb_path,
            'gif_path': gif_path,
            'start': df['start'].min(),
            'end': df['end'].max(),
        }

    def __create_gif(self, input_path, output_path, duration_seconds=4):
        cmd = ['ffmpeg']

        # can't be accelerated
        cmd += [
            '-y',
            '-ss',
            '0',
            '-t',
            str(duration_seconds),
            '-i',
            input_path,
            '-vf',
            'fps=10,scale=200:-1:flags=lanczos,split[s0][s1];'
            '[s0]palettegen[p];[s1][p]paletteuse',
            '-loop',
            '0',
            output_path,
        ]

        ok_msg = f'Making {duration_seconds}s gif from full clip'
        err_msg = 'Could not generate gif'
        self.__run_subprocess(cmd, ok_msg, err_msg)

    def __create_thumbnail(self, input_path, output_path):
        cmd = ['ffmpeg']
        # can't be accelerated
        cmd += [
            '-i',
            input_path,
            '-vframes',
            '1',
            '-y',
            output_path,
        ]

        ok_msg = f'Creating thumbnail at {output_path} from {input_path}'
        err_msg = 'Could not create thumbnail from {input_path}'
        self.__run_subprocess(cmd, ok_msg, err_msg)

        im = Image.open(output_path)

        # Scale image down.
        # PIL.Image.LANCZOS = a high-quality downsampling filter.
        im = im.resize((200, 113), resample=Image.LANCZOS)
        im.save(output_path, "JPEG")

    def __trim_clip(self, output, start, end=None):
        """
        Given a MP4 video, an output path, a start time,
        and an optional end time; trim the video.
        """

        # ffmpeg command explanation
        """
        Seeking: http://trac.ffmpeg.org/wiki/Seeking
        Note we can't put -ss before -i for faster seek before trimming
          because we'll miss keyframes in the video and
        won't produce a usable clip at the end.

        -vf format=yuv420p I belive this is 8bit H.264 video.
        -movflags +faststart moves the moov atom to the start of the file
        which means we seek in the video
        without downloading/opening the whole video.
      """
        cmd = ['ffmpeg']

        # Use fast and accurate seek if possible. This will skip through
        #  the file using keyframes Basically pass -ss twice before -i
        # and after. Before uses keyframes within the file to jump through
        # the video -ss after then goes frame by frame to find the frame to
        # cut. I did run into an issue using the same start time for both
        # the fast and accurate resulted in empty frames at the start or
        # incomplete videos I assume it had trouble with key frames so we'll
        # only use the fast seek if we're beyond 60s (arbitary) of video
        # which is where the main benefit of fast seeking occurs anyway.
        fast_seek_ss = start - 60
        if fast_seek_ss > 0:
            cmd += ['-ss', str(fast_seek_ss)]

        # -copyts is linked to the use of -ss before -i described above.
        # Doesn't matter if -ss is used.
        cmd += [
            '-i',
            self.__video_path,
            '-vcodec',
            'h264_nvenc',
            '-copyts',
            '-ss',
            str(start),
            '-to',
            str(end),
            '-vf',
            'format=yuv420p',
            '-preset',
            'fast',
            '-movflags',
            '+faststart',
            '-y',
            output,
        ]

        logger.debug(
            'Trimming {0} from {1} to {2} with command {3}'.format(
                self.__video_path, start, end, ' '.join(cmd)
            )
        )
        ok_msg = 'Clip saved at {}.'.format(output)
        err_msg = 'Could not trim clip {}'.format(output)
        self.__run_subprocess(cmd, ok_msg, err_msg)

    def __run_subprocess(self, cmd, ok_msg=None, err_msg=None):
        """
        Given a list of subprocess commands and flags,
        a success message and an error message, run a
        subprocess thread for that cmd.
        """
        ok_msg = ok_msg or 'Command {}: success.'.format(' '.join(cmd))
        err_msg = err_msg or 'Command {} unsuccess.'.format(' '.join(cmd))

        try:
            logger.info('Running subprocess: {0}'.format(' '.join(cmd)))
            with open(os.devnull) as devnull:
                subprocess.check_output(cmd, stdin=devnull)
            logger.debug(ok_msg)
        except Exception as e:
            logger.error("{0}: {1}".format(err_msg, e))
            raise e

    def _extract_timestamps_with_pcts(
        self,
        tracks: pd.DataFrame,
        wwpct_min: float,
        wwpct_max: float,
        metric: str,
    ) -> List:
        """
        We may need to call this multiple times with different percent
        thresholds. In longboard spots people are riding waves with no
        whitewater signal, so the default logic of looking for breaking
        waves fails. In some spots at high tides the waves also sometimes
        don't break, eg doho.

        Args:
            - tracks: tracks dataframe
            - wwpct_min: WhiteWater percent minimum (starting ts)
            - wwpct_max: WhiteWater percent max (end timestamp, less relevant).
            - metric: choose from 'y' or 'yMA', where it looks for smallest y
            - or largest MA/y ratio. Former is better for non-breaking waves.
        Returns:
            - timestamps list.
        """
        timestamps = []
        for track_num, track in tracks.groupby('track_num'):
            # Ignore the sometime long period prior to breaking.
            # Adding the wwpct constraint give many more smaller clips
            track = track[
                (track['wwpct'] <= wwpct_max) & (track['wwpct'] >= wwpct_min)
            ]  # This should also remove boat wake.
            # Accumulate only the start & stop times, and starting position.
            # Pad these, since they will clip pretty tightly.
            try:
                if np.count_nonzero(track['fit'] >= 0.9) > 2:
                    t0 = max(
                        [track.timestamp.iloc[0] - self.__start_buffer, 0]
                    )
                    t1 = track.timestamp.iloc[-1] + self.__end_buffer
                    if metric == 'y':
                        wave_size_proxy = (
                            1.0 / track[track['fit'] >= 0.9].y.iloc[0:3].mean()
                        )
                        t0 += (
                            t1 - t0
                        ) / 2.7  # Start ~1/3 of the way into the track
                    elif metric == 'yMA':
                        wave_size_proxy = (
                            (
                                track[track['fit'] >= 0.9].MA
                                / track[track['fit'] >= 0.9].y
                            )
                            .iloc[: max(len(track) // 5, 2)]
                            .mean()
                        )
                    else:
                        raise ValueError('Enter "y" or "yMA" for metric')
                    timestamps.append([t0, t1, wave_size_proxy])
            except IndexError:
                # wpct and wwpct are all 1
                pass

        return np.array(timestamps)

    def __empty_timestamps(self):
        return np.array(
            [self.__video_start_timestamp, self.__video_start_timestamp + 15.0]
        )[np.newaxis, :]

    def __get_timestamps_from_tracks(self, tracks):
        """
        Find the best wave times via sigma clipping, and return
        those, with overlaps merged properly.
        """
        # Sometimes there will be zero waves detected.
        if len(tracks) == 0:
            # Just return a 15 second clip of the lake-like conditions?
            return self.__empty_timestamps()

        # If we're here, we have more than 2 tracks.
        # First try with default wave pcts, if the cam is face on.
        # For lowers we want to skip the yMA metric altogether since it has a
        # tendency to flag the foreground waves in the lower left of the frame.
        # This likely applies to all obliqe-angle cams, so we can do this in
        # a slightly smarter way by looking at wave angles instead of
        # writing down cam ids. Some cams may have variabilty at the
        # 2.5 threshold depending on the swell angle, and therefore varying
        # outputs on consecutive rewinds. But TBD.
        if abs(90 - tracks["angle"].mean()) > 2.5:
            timestamps = self._extract_timestamps_with_pcts(
                tracks, config.MIN_WWPCT, config.MAX_WWPCT, 'y'
            )
        else:
            timestamps = self._extract_timestamps_with_pcts(
                tracks, config.MIN_WWPCT, config.MAX_WWPCT, 'yMA'
            )
        # Check the average clip length. If it's small then either it's flat or
        # there's no WhiteWater, like high tide doho breaking on the sand.
        if (
            len(timestamps) == 0
            or np.mean(timestamps[:, 1] - timestamps[:, 0])
            < 3  # mean duration
            or np.mean(tracks.groupby('track_num').wwpct.mean())
            < 0.1  # mean ww pct
        ):
            timestamps = self._extract_timestamps_with_pcts(tracks, 0, 1, 'y')
            logger.info("Using longer timestamps without wave pcts")

        if len(timestamps) == 0:
            # Just return a 15 second clip of the ripples
            logger.info('No waves found, returning first 15s')
            return self.__empty_timestamps()

        # Explicitly set each wave to be no longer than 10s,
        # so just replace all the end timestamps.
        timestamps[:, 1] = np.min(
            (timestamps[:, 0] + config.MAX_WAVE_DURATION, timestamps[:, 1]),
            axis=0,
        )

        # If there are a lot of waves, we need to at least remove
        # all the tiny ones. This won't be important if we only
        # have a few timestamps, which may have already been filtered
        # for mush.
        timestamps_raw = timestamps.copy()
        n_raw = len(timestamps)
        t_raw = sum(timestamps[:, 1] - timestamps[:, 0])  # Total time.
        if t_raw < 10:
            logger.info(
                'Total time of clips is low: {t_raw:.0f}s and filtering'
                ' should have been done without wwcpt.'
                ' Likely this day is flat, returning first 15s.'
            )
            return self.__empty_timestamps()

        # If we have a bunch of waves, remove the tiny ones.
        if len(timestamps) > 10 or t_raw > self.__max_duration:
            MA = timestamps[:, 2]
            avg_wave_idxs = np.where(MA >= MA.mean())[0]
            if len(avg_wave_idxs) == 0:
                avg_wave_idxs = np.arange(len(MA))
            else:
                logger.info(
                    f'Removing {len(timestamps) - len(avg_wave_idxs)} '
                    f'small waves.'
                )
            timestamps = timestamps[avg_wave_idxs]

        # If we've been too aggressive on filtering,
        # revert back to the raw timestamps
        if len(timestamps) == 0:
            logger.critical(
                'All timestamps have been filtered out,'
                f'from {n_raw} ({t_raw:.0f}s) -> 0. '
                'Check small wave filter. Using full list of timestamps.'
            )
            timestamps = timestamps_raw
        else:
            logger.info(
                f"Found {len(timestamps)}/{n_raw} "
                f"interesting segments before merging."
            )

        # Do not merge nearby waves.
        highlight_times = timestamps[timestamps[:, 2].argsort()[::-1]]

        # Now just ensure total time is less than max_duration.
        durations = np.cumsum(highlight_times[:, 1] - highlight_times[:, 0])
        low_durations = np.where(durations <= self.__max_duration)[0]

        if len(low_durations) == 0:
            logger.critical(
                'The first wave is longer than the max duration'
                ' {self.__max_duration}. This is likely a standing'
                ' wave/wake or some other bad segmentation artifact.'
            )
            # Since the first wave is > 45s, explicitly set
            # the end time of timestamp[0] to be 45s.
            low_durations = np.array([0])
            highlight_times[0][1] = highlight_times[0][0] + config.MAX_DURATION

        if len(low_durations) < len(highlight_times):
            logger.info(
                f"Keeping {len(low_durations)}/{len(highlight_times)} clips in"
                f" order to satisfy {self.__max_duration}s max_duration"
            )
        highlight_times = highlight_times[low_durations]
        highlight_times = highlight_times[
            highlight_times[:, 2].argsort()[::-1]
        ]  # Need to re-sort again.
        dts = highlight_times[:, 1] - highlight_times[:, 0]
        logger.info(
            f"Average clip time is {dts.mean():.1f}s, {dts.sum():.1f}s total"
        )

        return np.round(highlight_times[:, :2], 2)
