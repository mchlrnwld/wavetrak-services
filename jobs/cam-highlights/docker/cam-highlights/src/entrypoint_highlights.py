#!/usr/bin/python
# -*- coding: utf-8 -*-
import newrelic.agent  # type: ignore # noqa E402

from common import config

newrelic.agent.initialize('./newrelic.ini', config.ENV)
newrelic.agent.set_background_task(flag=True)

import argparse  # noqa E402
import sys  # noqa E402
import tempfile  # noqa E402
import time  # noqa E402
from distutils.util import strtobool  # noqa E402

from common.logger import get_logger  # noqa E402
from utils.Clips import Clips  # noqa E402
from utils.HighlightsVideo import HighlightsVideo  # noqa E402
from utils.Recording import Recording  # noqa E402
from utils.Wavetrack import generate_wavetracks  # noqa E402

_logger = get_logger()

start_time = time.time()

application = newrelic.agent.application()


def str2bool(v):
    try:
        return strtobool(v)
    except ValueError:
        raise argparse.ArgumentTypeError('Boolean value expected.')


@newrelic.agent.background_task()
def main(argv):
    parser = argparse.ArgumentParser(description='Process video.')

    parser.add_argument(
        '--recording_id',
        '-rec',
        default=None,
        type=str,
        required=True,
        help='Id of the recording to use for saving to db.',
    )

    parser.add_argument(
        '--run_detections',
        '-rd',
        default=config.RUN_DETECTIONS,
        type=str2bool,
        help='Whether or not to run object detection',
    )

    parser = parser.parse_args(argv)

    _logger.info(f'Started job on recording: {parser.recording_id}')

    with tempfile.TemporaryDirectory() as tmp_dir:
        # Download inputfile.
        recording = Recording(parser.recording_id, tmp_dir)

        newrelic.agent.add_custom_parameter(
            'queueTime',
            int(start_time - recording.end_date),
        )

        newrelic.agent.add_custom_parameter(
            'recordingId', recording.recording_id
        )
        newrelic.agent.add_custom_parameter('cameraAlias', recording.alias)

        wavetracks = generate_wavetracks(
            recording,
            config.FPS,
            run_detections=parser.run_detections,
        )

        hv = HighlightsVideo(
            recording.local_copy, recording.start_date, wavetracks
        )

        clips = Clips(recording, hv.generate_clips(True))

        clips.upload()


if __name__ == "__main__":
    main(sys.argv[1:])
