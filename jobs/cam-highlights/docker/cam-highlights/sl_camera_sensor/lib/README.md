<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [lib](#lib)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# lib
Analytic and empirical models that generally take neural network/decision tree model outputs as inputs.