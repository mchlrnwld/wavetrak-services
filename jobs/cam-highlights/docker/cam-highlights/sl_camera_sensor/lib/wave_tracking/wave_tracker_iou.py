# Looks at IoU of successive frames to associate individual
# wave blobs into tracks
#
import datetime
from collections import defaultdict
from io import StringIO
from itertools import chain
from typing import Dict, List, Optional, Union

import cv2
import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist

from ...utils.logger import getLogger
from ..localization.localize_dets import LocalizeDets
from .blobify import Blobify
from .wave_blob import WaveBlob

log = getLogger(__name__)


class WaveTracker:
    def __init__(self, fps, label_map, **kwargs):
        self.fps = float(fps)
        self.label_map = label_map
        self.target_size = (576, 1024)
        self.track_surfers = True
        # min number of blobs per wave track group to be considered interesting
        self.min_cnt = np.max([np.ceil(fps * 3), 2])
        self.min_area = 700  # min number of pix per wave blob
        # After this many no detects it's finished
        self.persist_cnt = np.max([np.ceil(fps * 3), 4])
        # Overwrite any params with kwargs
        self.dets_label_map = None
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.count = 1  # Count the total number of waves
        self.finished_cnt = 0
        # Store persistent tracks here.
        self.waves = defaultdict(lambda: defaultdict(list))
        self.finished_waves = {}
        self._cnt = 0
        self.blobber = Blobify(
            self.label_map,
            target_size=self.target_size,
            classes=["Wave", "WhiteWater", "Lip"],
            min_area=self.min_area,
        )
        self.offshore_blobber = Blobify(
            self.label_map,
            target_size=self.target_size,
            classes=["Offshore"],
            min_area=100,
        )
        self._localizer = None
        self.col_names = None
        self.bin_masks = {}
        self.pocket_locs = {}
        self.height_locs = (
            []
        )  # Store at which pixel we calculate physical wave heights at
        self.done_called = False
        self.timestamp = None

    def reset(self):
        """
        If we want to use the same tracking object across multiple
        rewinds, just call this between rewinds.
        """
        lm = self.label_map
        fps = self.fps
        target_size = self.target_size
        self.__init__(fps, lm, target_size=target_size)

    def wave_count(self) -> int:
        """Get how many waves the tracker has tracked, so far."""
        return len(self.finished_waves)

    def pop_tracks(
        self,
        done: Optional[bool] = False,
        return_blobs: Optional[bool] = False,
    ) -> Union[dict, pd.DataFrame]:
        """
        Pop off any finished tracks. There is a slight time delay here
        based on self.persistent_cnt, as this won't return any tracks
        that aren't deemed finished per self.persistent_cnt.

        :param done: Return all the tracks in self.waves,
        independent of self.persistent_cnt. If you're running
        on a rewind for example, call this after the frame
        generator is exhausted.
        :param return_blobs: Set to True if you want a dict with lists of blob objects,
        otherwise you get back a single dataframe.
        :returns: dataframe of wave tracks, or list of wave blob objects.
        """
        if self.done_called:
            log.info(
                "pop_tracks(done=True) has been called multiple times."
                " Tracks don't persist after a done=True invocation, "
                "returning None"
            )
            return
        # TODO done kwd doesn't work
        finished = {k: v["blobs"] for k, v in self.finished_waves.items()}
        delkeys = list(finished.keys())
        for k in delkeys:
            del self.finished_waves[k]
        # If we still have blobs in self.waves we need to pop those off as well.
        # Normally we would check displacement / length of track but in this
        # case it may be cut off because footage ends. So this actually isn't super
        # important but just add the remaining self.wave blobs to finished.
        finished_keys = list(finished.keys())
        if done and len(finished_keys) > 0:
            max_k = max(finished_keys)
            for cnt, blob_dict in enumerate(self.waves.values()):
                finished[max_k + cnt + 1] = blob_dict['blobs']
            self.done_called = True
        return finished if return_blobs else WaveTracker.blobs_to_df(finished)

    @staticmethod
    def blobs_to_df(blobs_dict: Dict[int, WaveBlob]):
        """Parse the blobs into a dataframe.
        :param blobs_dict: dict with track_num keys and values
        as lists of wave blobs that constitute a wave track.
        :returns: dataframe of wave track.
        """
        if len(blobs_dict) > 0:
            for k, v in blobs_dict.items():
                if isinstance(v, list):
                    track_strings = v[0].get_col_names()
                else:
                    track_strings = v.get_col_names()
                break
            for k, v in blobs_dict.items():
                if isinstance(v, list):
                    for blob in v:
                        track_strings += str(k) + "," + str(blob) + "\n"
                else:
                    track_strings += str(k) + "," + str(v) + "\n"
            try:
                df = pd.read_csv(StringIO(track_strings))
            except:  # noqa
                log.info("Unable to parse wavetrack strings to dataframes")
                return pd.DataFrame()
            return df
        return pd.DataFrame()  # Return empty dataframe if input is empty dict

    def batch_update(
        self,
        masks: np.ndarray,
        timestamps: Optional[Union[List, np.ndarray]] = None,
        logits: Optional[bool] = False,
    ) -> None:
        """
        Update the tracking state from a list of masks. Input
        a list of timestamps that correspond to each mask if you
        care about storing outputs.
        """
        if timestamps is None:
            timestamps = [None] * len(masks)
        for mask, timestamp in zip(masks, timestamps):
            self(mask, timestamp=timestamp, logits=logits)

    def update(self, mask, logits=False):
        """
        Update the tracking state with a single mask.
        :param mask: segmentation model output.
        :param logits: if it's a raw model output, set as true.
        :returns: updates self.waves
        """
        if isinstance(mask, list):
            self.batch_update(mask, logits)
        else:
            self(mask, logits=logits)

    def get_binary_masks(self):
        """
        Return the dict with wave_numi keys and binary mask of wave values.
        Useful for surfer tracking dLip and dTrough, and possibly relative wave
        heights to surfer surfing bbox.
        """
        return self.bin_masks

    def get_wave_pockets(self):
        """This will contain all the pocket locations, even for
        untracked (small) waves.
        If you want these labeled, call the wave tracker with return_contours=True,
        and then run waves_df = WaveTracker.blobs_to_df(wave_contours)
        and you'll get a dataframe with a 'pockets' column and associated wave_num IDs.
        """
        return np.vstack(chain.from_iterable(self.pocket_locs.values()))

    def __len__(self):
        return self.wave_count()

    def __call__(
        self,
        mask: np.ndarray,
        logits: Optional[bool] = False,
        timestamp: Optional[Union[List, np.ndarray, float]] = None,
        return_contours: Optional[bool] = True,
        dets: Optional[dict] = None,
        dets_label_map: Optional[dict] = None,
    ) -> None:
        """
        Make contours from this mask, combining all the
        attributes that make up a wave. If you want to also store the
        individual attributes that make up the wave, set store_sub_blobs=True,
        and each WaveBlob will hold those contours in a sub_blobs kwd.

        :param mask: a single segmentation mask nd array
        :param logits: if your mask needs an argmax applied, ie
        it's dimension is [n_class, H, W], set this as true.
        :param return contours: get back the wave conturs for this mask.
        if False this returns nothing and simply updates the tracker.
        You can get finished tracks via pop_tracks(). Use this for annotations.
        :param timestamp: timestamp that accompanies mask. These are important.
        :param dets: Optionally input a detection dictionary to check for SurferSurfing
        on each wave.
        :param dets_label_map: if you want to localize dets you need to supply
        the label map on the first call.
        """
        self._cnt += 1
        self.bin_masks = {}
        if self._localizer is None:
            # Construct localizer instance
            self._localizer = LocalizeDets(
                self.dets_label_map,
                self.label_map,
                target_size=self.target_size,
            )
        if dets is not None:
            if self.dets_label_map is None:
                if dets_label_map is None:
                    raise ValueError(
                        "You need to supply the dets_label_map"
                        "on the first call that supplies dets."
                    )
                self.dets_label_map = dets_label_map

        if timestamp is None:
            timestamp = (
                datetime.datetime.now()
                if self.timestamp is None
                else self.timestamp
                + datetime.timedelta(seconds=1.0 / self.fps)
            )
        if isinstance(timestamp, (float, int)):
            timestamp = datetime.datetime.utcfromtimestamp(timestamp)
        self.timestamp = timestamp

        # Update the wave blobber and get back binary wave masks to check IOUs.
        self.blobber.update(mask, store_sub_blobs=True, logits=logits)
        wave_contours, wave_masks = self.blobber.get(return_masks=True)

        # Update the offshore blobber and associate offshore flags to each wave blob.
        self.offshore_blobber.update(
            mask, store_sub_blobs=False, logits=logits
        )
        ofsh_contours = self.offshore_blobber.get(return_masks=False)
        associations = self._associate_offshore(wave_contours, ofsh_contours)

        # Find the wave pocket locations and store in self,
        # so it's accessible via get_wave_pockets()
        pocket_locs = self._localizer.localize_pockets(mask, unnormalize=True)
        # Above is a list of locations, now associate them with
        # each of the waves.
        self.pocket_locs = self._associate_locs_with_waves(
            pocket_locs, wave_contours
        )
        # Create a new wave blob for each wave individually,
        # and optionally check if anyone is surfing this wave.
        # Also calculate where the wave pockets are and put those
        # in the blobs as well?
        inputs, iblobs = [], []
        for i, key in enumerate(wave_contours):
            # Check each wave separately for someone riding it.
            if dets is not None:
                surfer_locs = self._localizer(
                    dets,
                    wave_masks[key],
                    binary_mask=True,
                    localize_surfing=True,
                    localize_shore=False,
                )
                # Store the binary masks, so we can access them from the
                # surfer tracker. Easiest to get the distance to lip/trough
                # with these binary masks and don't want to blob more than once.
                # self.bin_masks[key] = wave_masks[key]
            else:
                surfer_locs = []

            # Make blob objects. We need to store the wave info in these blobs...
            bin_mask = wave_masks[key]
            norm_pockets = self._localizer.normalize(
                self.pocket_locs[key].copy(), self.target_size
            )
            wave_slices = self._get_wave_slices(
                self.pocket_locs[key], bin_mask
            )
            blob = WaveBlob(
                wave_contours[key],
                timestamp=timestamp,
                pcts=key,
                mask=wave_masks[key],
                surfers=surfer_locs,
                wave_info=None,  # surfer_mask_pcts,
                bin_mask=bin_mask,
                pockets=norm_pockets,
                slices=wave_slices,
                blob_key=key,  # Store this key to map blobber keys -> wave keys
                ft='',
                height_locs='',
            )

            # Add sub_blob contours for each separate class for this specific wave.
            # TODO I think we should not do this.
            sub_blob_conts = self.blobber.sub_contours[key]
            if sub_blob_conts != []:
                blob.sub_blobs = sub_blob_conts
            if i in associations:
                blob.offshore = 1
            inputs.append(blob)
            iblobs.append(blob)

        # Archive all waves that haven't had a grid match in a while
        del_waves = [
            wave_key
            for wave_key in self.waves
            if (self._cnt - self.waves[wave_key]["persist_cnt"])
            >= self.persist_cnt
        ]
        for del_wave in del_waves:
            finished = self.waves.pop(del_wave)
            if len(finished["blobs"]) > self.min_cnt:
                # Also check displacement... TODO this should be perp to beach
                if abs(finished["blobs"][-1].y - finished["blobs"][0].y) > 30:
                    self.finished_cnt += 1
                    self.finished_waves[self.finished_cnt] = finished
                else:
                    log.debug("Low displacement wavetrack tossed")

        # If self.waves is empty, just fill with inputs and return
        if len(self.waves) == 0:
            for blob in inputs:
                self._add_wave(self.count, [blob])
                self.count += 1
            outputs = self._map_inputs(inputs)
            return None if not return_contours else outputs

        # Check IOUs. If there are a lot of waves this can get expensive. Modify
        # the min_area integer to omit smaller (noisy) waves.
        all_ovs = defaultdict(list)
        for wave_key in self.waves.keys():
            wave_mask = self.waves[wave_key]["blobs"][-1].mask
            for iblob_cnt, iblob in enumerate(iblobs):
                n_overlap = np.count_nonzero((iblob.mask + wave_mask) > 1)
                all_ovs[iblob_cnt].append(n_overlap)
        wave_keys = list(self.waves.keys())
        for blob_num, overlap_amounts in all_ovs.items():
            if np.any(overlap_amounts):
                # If we set the min_area kwd to be low enough to pick up small
                # contours (which we need for significant wave height statistics
                # on small days), then we'll often get multiple matches for each
                # legitimate wave at each timestamp. In order to omit those
                # small guys we need to check *again* if this wave was recently
                # matched, and what the confidence was.
                iwave_key = wave_keys[np.argmax(overlap_amounts)]
                if (
                    self.waves[iwave_key]['blobs'][-1].timestamp
                    == timestamp.timestamp()
                ) and self.waves[iwave_key]['ovlp'] > 0:
                    # If this wave already has a match on this timestamp,
                    # do we need to pop that off and replace?
                    if self.waves[iwave_key]['ovlp'] < np.max(overlap_amounts):
                        # Previous match is same timestamp and less
                        # than current overlap, pop it off!
                        new_wave = self.waves[iwave_key]['blobs'].pop()
                        self._add_wave(self.count, [new_wave])
                        self.count += 1
                        self._add_wave(
                            iwave_key,
                            [iblobs[blob_num]],
                            ovlp=np.max(overlap_amounts),
                        )
                    else:
                        self._add_wave(
                            self.count,
                            [iblobs[blob_num]],
                            ovlp=np.max(overlap_amounts),
                        )
                        self.count += 1
                else:
                    self._add_wave(
                        iwave_key,
                        [iblobs[blob_num]],
                        ovlp=np.max(overlap_amounts),
                    )
            else:
                self._add_wave(self.count, [iblobs[blob_num]])
                self.count += 1
        outputs = self._map_inputs(inputs)
        return None if not return_contours else outputs

    def print_waves(self):
        for wave_key in self.waves:
            print(f"wave num: {wave_key}")
            for blob in self.waves[wave_key]["blobs"]:
                print(f"{blob.x:0f}, {blob.y:.0f}")
        print("-" * 10)

    def _map_inputs(self, inputs):
        """
        In order to return the contours that correspond to the
        blobs we extracted from the input mask, we just map them
        based on our blobs. Gets contours that correspond to input/last call.
        Also store the binary masks in here, for the surfer_tracking...
        """
        output = {}
        self.bin_masks = {}
        for wave_key in self.waves:
            len(self.waves[wave_key]["blobs"])
            for blob in self.waves[wave_key]["blobs"]:
                for ipt in inputs:
                    if blob == ipt:
                        # output[(wave_key if group_len > 1 else 0)] = blob
                        output[wave_key] = blob
                        self.bin_masks[wave_key] = blob.bin_mask
        return output

    def _add_wave(self, wave_num, blob_list, ovlp=0):
        """Add a list of input blobs assigned to wave_num"""
        self.waves[wave_num]["blobs"] += blob_list
        self.waves[wave_num]["current_match"] = True
        self.waves[wave_num]["persist_cnt"] = self._cnt
        self.waves[wave_num]["ovlp"] = ovlp

    def _associate_offshore(self, wave_contours, offshore_contours):
        """I don't know if we really care about associating offshore
        components to waves. In my mind it's better to store this somewhere
        in the raw data and define the logic of when we report it
        at a higher level. So storing these flags in wave tracks
        makes 50% sense to me right now.
        """
        ofshs = [
            np.squeeze(offshore_contours[k]) for k in offshore_contours.keys()
        ]
        waves = [np.squeeze(wave_contours[k]) for k in wave_contours.keys()]
        associations = []
        for ofsh in ofshs:
            deltas = [cdist(ofsh, wave) for wave in waves]
            dists = [np.count_nonzero(d < np.mean(d)) / len(d) for d in deltas]
            if len(dists) > 0:
                associations.append(np.argmin(dists))
        return associations

    def isinside(self, loc, contours, idx=0):
        """List of locs and a dict of contours, check
        which contour each of the locs is isiside of, if any (-1 in that case).
        """
        if idx == len(contours):
            return -1
        for i in range(idx, len(contours)):
            k = list(contours.keys())[i]
            inside = cv2.pointPolygonTest(contours[k], tuple(loc), False)
            if inside >= 0:
                return k
            return self.isinside(loc, contours, i + 1)

    def _associate_locs_with_waves(
        self, locs: List[tuple], wave_contours: dict
    ) -> Dict[int, List]:
        """Given a list of location tuples as [(x, y),... ],
        get back which wave each location belongs to. This returns
        a dict of wave number keys with lists of locs. This is faster
        than running binary_dilation on multiple masks (per-wave vs single-shot).

        :param locs: (x, y) tuple list as returned from eg self.get_pocket_locs
        :param wave_conturs: {wave_num: contour} dict as returned from the
        blobber.
        :returns: {wave_num: locs, ...} dict. -1 keys correspond to
        pockets in untracked waves, eg when the min_area on the wave tracker
        is high, the pocket localizer will still detect those pockets.
        For validation purposes we want to see all the pockets, not just
        the tracked wave pockets.
        """
        assoc = defaultdict(list)
        if len(locs) == 0:
            return assoc
        if np.max(np.array(locs)) <= 1:
            raise ValueError(
                "Send unnormalized locs to "
                "localizer.associate_locs_with_waves"
            )
        tot_len = 0
        norm_locs = self._localizer.normalize(locs, self.target_size)
        for wave_key in wave_contours:
            cont = np.squeeze(wave_contours[wave_key])
            deltas = cdist(cont, locs)
            close_enough = np.where(deltas.min(axis=0) < 10)[0]
            if len(close_enough) > 0:
                assoc[wave_key] = norm_locs[close_enough].copy()
                tot_len += len(close_enough)
                if tot_len >= len(norm_locs):
                    break
        return assoc

    def _get_wave_slices(self, blob_pockets, blob_bin_mask):
        """Instead of doing the homography transform here,
        store the pocket height and we can do the transform
        directly from the wavetrack files instead.

        This is actually slightly different than the raw pocket locations.
        From a pocket loc, this find the bottom and top of the wave, at
        the pocket. So x,y of the incoming blob_pockets will be modified.

        This will give length-3 tuples of x, y, h. No need for multiple
        x values.
        """
        sy, sx = self._localizer.target_size
        slices = []
        if len(blob_pockets) > 0:
            norm_pockets = self._localizer.unnormalize(
                blob_pockets.copy(), blob_bin_mask.shape
            )
            for pocket in norm_pockets:
                x, y = pocket
                wave_slice = np.where(blob_bin_mask[:, x])[0]
                if len(wave_slice) == 0:
                    h = -1
                else:
                    # Make a new x, y based on Wave segmentation instead of
                    # dilated mask overlaps. Since we have height we just need x, y1
                    # since we want wave height from the *bottom* of the wave.
                    h = wave_slice.max() - wave_slice.min()
                    y = wave_slice.max()
                # In order to recover INTEGER pix height you'll
                # need to round the resulting homography transformation.
                slices.append((x / sx, y / sy, h / sy))
        return slices
