# Convert an input logits segmentation mask to a list of contours (blobs).
# This allows a ~30% reduction in memory size and allows us to do more
# interesting arithmetic with arbitrary mask regions.
#
from collections import OrderedDict, defaultdict
from typing import List, Optional, Union

import cv2
import numpy as np

from PIL import Image

from sl_camera_sensor.utils.logger import getLogger

log = getLogger(__name__)


class Blobify:
    """
    The segmentation model outputs a single mask with each pixel labeled as
    a class integer. We can't store masks as they're too large, so we
    convert them to blobs/contours for some list of classes we're interested
    in. Typical calling sequence:

    blobber = Blobify(label_map, classes=['Shore'])
    blobber.update(seg_mask, sort=True, store_sub_blobs=True)
    mask_blobs = blobber.get()
    # or get the largest blob for this mask:
    mask_blob = blobbler.get_largest()

    If you set store_sub_blobs=True in the update call, the keys of
    the self.contours blob dict will store the percents of each class
    that make up each blob.
    """

    def __init__(
        self,
        label_map: dict,
        classes: Optional[List[str]] = ["Wave", "WhiteWater", "Lip"],
        target_size: Optional[tuple] = (576, 1024),
        min_area: Optional[int] = 700,
        sub_cont_classes: Optional[List[List]] = None,
    ) -> None:
        """
        :param label_map: label map dict that corresponds to your
        segmentation model.
        :param classes: list of classes to blobify. They will all be combined
        into a single effective class, since cv2 will turn the mask into
        a binary image prior to looking for contours. This is what we
        want when looking for wave contours. For individual classes just
        call separately with a classes=['singleclass'].
        :param target_size: you can optinally specify a different frame
        size, although it's confusing if you do this and don't track it
        properly. In that case your contours may be misaligned with the
        corresponding RGB frame. When you're making a low-res annotated video,
        this is useful.
        :param min_area: minimum blob area, below which blobs will be
        ignored.
        """
        self.label_map = label_map
        self.classes = classes
        self._min_area = min_area
        self.label_map = label_map
        self.target_size = target_size
        self.sub_cont_classes = (
            sub_cont_classes  # [['Wave'], ['WhiteWater', 'Lip']]
        )
        self.contours = {}
        self.blob_masks = {}
        self.sub_contours = {}
        self._class_idxs = None
        self.is_sorted = False

    def get(self, return_masks=False) -> Union[dict, OrderedDict]:
        return (
            (self.contours, self.blob_masks) if return_masks else self.contours
        )

    def update(
        self,
        mask: np.ndarray,
        logits: Optional[bool] = False,
        store_sub_blobs: Optional[bool] = False,
        store_sub_conts: Optional[bool] = False,
        sub_cont_classes=None,
        sort: Optional[bool] = False,
    ) -> None:
        """
        This updates self.contours with those that correspond to input mask.
        Call .get() after updating blobs to get them back.

        :param mask: segmentation model output mask
        :param logits: bool to specfify if output mask is raw network output
        :param sort: bool to sort the blobs by decreasing size. Sometimes
        we're only interested in the largest ones.
        :param store_sub_blobs: keep track of the percents of each sub blob
        that are contained in each larger blob. If the classes set in the constructur
        are less than 1 then this won't be interesting. Will be forced true
        if user wants to store sub-contours.
        :param store_sub_conts: store the individual class contours that beloong
        to each large are contour. Again you'd need to have the large area contours
        be built from multiple classes.
        :returns: None - updates self.contours. call .get() to retrieve them
        """
        if store_sub_conts:
            if self.sub_cont_classes is None and sub_cont_classes is None:
                raise ValueError(
                    'Construct this instance with `sub_cont_classes` '
                    'or pass in update().'
                )
            store_sub_blobs = True
            self.sub_cont_classes = sub_cont_classes or self.sub_cont_classes
        mask = self.__check_mask(mask, logits)
        # Store that input mask so we can access it later. It's shaped correctly.
        self._mask = mask.copy()
        if not np.all([cls in self.label_map for cls in self.classes]):
            raise ValueError(f"Unallowed input class in {self.classes}")
        cls_ints = [self.label_map[cls] for cls in self.classes]
        # cv2 will turn mask into a binary image. keep the cls ints.
        bin_mask = mask.copy()
        bin_mask[~np.isin(bin_mask, cls_ints)] = 0
        conts, hei = cv2.findContours(
            bin_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        areas = np.asarray([cv2.contourArea(cont) for cont in conts])
        large_area_contours = np.asarray(conts)[
            np.where(areas > self._min_area)
        ]
        zmask = np.zeros(bin_mask.shape, dtype=np.uint8)
        contours = {}
        blob_masks = {}
        # We now think all the individual contours need to be stored.
        # I don't want to modify the original self.contours...
        sub_mask = np.zeros(bin_mask.shape, dtype=np.uint8)
        sub_contours = defaultdict(lambda: defaultdict(list))
        if len(large_area_contours) < 1:
            log.debug("No contours found for input mask")
        # plt.figure()
        for i, cont in enumerate(large_area_contours):
            zmask *= 0
            cont_mask = cv2.drawContours(zmask, [cont], -1, 1, cv2.FILLED)
            # Mask the mask to isolate this individual contour
            pct_mask = cont_mask * bin_mask
            if store_sub_blobs:
                pcts = []
                n_nonz = np.count_nonzero(cont_mask)
                for cls in self.classes:
                    cls_pct = (
                        np.count_nonzero(pct_mask == self.label_map[cls])
                        / n_nonz
                    )
                    pcts.append(f"{cls}:{cls_pct:.2f}")
                key = str(i) + "_" + "_".join(pcts)
                # Optionally store all the sub contours without modifying
                # the existing self.contours keys or values.
                if store_sub_conts:
                    sub_mask = mask.copy() * cont_mask
                    tmp_mask = np.zeros(sub_mask.shape, dtype=np.uint8)
                    key_cnts = {
                        '_'.join(sub_cls): 0
                        for sub_cls in self.sub_cont_classes
                    }
                    for classes in self.sub_cont_classes:
                        cls_ints = [self.label_map[cls] for cls in classes]
                        cls_key = '_'.join(classes)
                        idxs = np.where(np.isin(sub_mask, cls_ints))
                        if np.any(idxs):
                            tmp_mask *= 0
                            tmp_mask[idxs] = 1
                            sub_conts, hei = cv2.findContours(
                                tmp_mask,
                                cv2.RETR_TREE,
                                cv2.CHAIN_APPROX_SIMPLE,
                            )  # Approximations on these contours is OK?
                            # Can have multiple eg 'Wave' contours per
                            # wave... they will be segmented
                            cls_key_numbered = (
                                str(key_cnts[cls_key]) + '_' + cls_key
                            )
                            sub_contours[key][cls_key_numbered] = sub_conts
                            key_cnts[cls_key] += 1
            else:
                # Not storing
                key = str(i) + "_" + "_".join(self.classes)
            contours[key] = cont.copy()
            blob_masks[key] = cont_mask.copy()

        self.sub_contours = sub_contours
        self.contours = contours
        self.blob_masks = blob_masks
        if sort:
            if store_sub_blobs:
                raise NotImplementedError(
                    "Cannot sort contours and sub-blobs -- "
                    "blob_masks need sorting as well."
                )
            self.contours = self.sort_blobs()
            self.is_sorted = True

    def get_sub_conts(self):
        return self.sub_contours

    def get_largest(self):
        """Assumes self.contours has just one key"""
        if len(self.contours) > 0:
            return list(self.contours.values())[0]
        else:
            log.info('No contours to return largest')
        return {}

    def get_n_largest(self, n):
        if not self.is_sorted:
            self.contours = self.sort_blobs()
        return list(self.contours.values())[:n]

    def get_n_largest_sub_conts(self, n):
        """Get the sub-contours of the largest macro-contours."""
        if not self.is_sorted:
            self.contours = self.sort_blobs()
        sub_conts = []
        for key, i in enumerate(self.contours.keys()):
            # Sub contours have same keys as self.contours
            sub_conts.append(self.sub_contours[key])
            if i >= n:
                break
        return sub_conts

    def sort_blobs(self) -> OrderedDict:
        """Sort self.contours based on contour size"""
        return OrderedDict(
            sorted(self.contours.items(), key=lambda x: cv2.contourArea(x[1]))[
                ::-1
            ]
        )

    def __repr__(self):
        return ", ".join(self.contours.keys())

    def __str__(self):
        return self.__repr__()

    def draw_contours(
        self,
        frame: np.ndarray,
        color: Optional[tuple] = (0, 255, 0),
        fill: Optional[bool] = False,
    ) -> np.ndarray:
        """
        Draw all the blobs contained in self.contours on input frame
        :param frame: RGB or BGR unit8 frame you want to draw on
        :param color: color of contours as BGR
        :param fill: bool to fill in the contour areas or not
        :returns: annotated input frame.
        """
        for cont in self.contours.values():
            frame = cv2.drawContours(frame, [cont], -1, color)
        return frame

    def __check_mask(self, mask: np.ndarray, logits: bool) -> np.ndarray:
        """Checks the validity of the input mask. If you pass
        raw network outputs set logits=True and this will
        convert to class ints.

        :param mask: segmentation model output as numpy array
        :param logits: bool to specify if mask has been argmaxed or not
        :returns: valid mask for blobbing
        """
        if mask.shape[:-1] != self.target_size:
            if mask.ndim > 2:
                if mask.ndim == 3 and mask.shape[0] == 1:
                    # We can deal with extra batch axis, but no more.
                    mask = np.squeeze(mask)
                else:
                    raise ValueError(
                        f"Expected 2D input, got {mask.ndim}d instead"
                    )
            mask = np.array(
                Image.fromarray(mask).resize(
                    self.target_size[::-1], Image.NEAREST
                )
            )
        if logits:
            mask = np.argmax(mask, axis=0)
        if mask.dtype != np.uint8:
            mask = mask.astype(np.uint8)

        return mask

    def get_bbox(self):
        """
        Returns (x, y, w, h) tuples for each blob in self.contours
        """
        return {
            k: [cv2.boundingRect(cont) for cont in v]
            for (k, v) in self.contours.items()
        }
