# Wave Tracking

For face-on cameras, wave traverse longitudinally down the screen at a constant, predictable pace.
Depending on the FOV of the cam, there may be multiple breaking waves at the same latitude,
and sometimes waves can (re-)form in different spots across the image. Grouping all detections
at the same [possibly rotated] latitude simplifies tracking and counting, and effectively
removes a lot of the noise in the segmentation model outputs. Then to track waves all we
need to do is group by lattitude, which is faster than Kalman filtering.

The approach we've taken here is to split the frame into `n` equally spaced lateral grids.
The grids center themselves based on the longest breaking wave, and roll down the screen
at the same rate as the waves. Roation angles of non-perpendicular breaks are computed 
from the openCV contour parameters, and `n` is computed based on the largest wave heights
detected - as the wave heights increase, less grids are needed to accurately enclose each
breaking wave. This is the single most important hyperparameter, and is updated dynamically.

One final difficulty in this method is rapid initialization of `n` in the first 1-2 frames.
Currently if a set is occuring in the very beginning of a rewind, tracking errors will occur
since it hasn't had time to intialize, wihch takes ~10 frames at the moment. WIP.

```python
tracker = WaveTracker(fps, label_map)
masks = seg_bot.predict(frame)
tracker(masks)
tracks = tracker.pop_tracks(filtered=False)
```

The tracks will be a list of objects with string representations of openCV contour parameters,
wave attribute percents, and track `major.minor` numbers:

```
#track_num,timestamp,x,y,MA,ma,angle,arclen,wpct,wwpct,lpct
1.1, 1579642111.540, 227, 284, 9, 503, 89, 884, 1.000, 0.000, 0.000
1.1, 1579642111.840, 222, 285, 12, 502, 89, 936, 1.000, 0.000, 0.000
.
.
.
```

Where `wpct`, `wwpct`, `lpct` are respectively percents of `Wave`, `WhiteWater` and `Lip`.
Different waves at the same latitude (same track number) can be filtered apart
with kalman filtering, by calling `tracker.pop_tracks(filtered=True)`.
The code just looks at each track individually and separates intratrack waves
based on the classical IOU approach. If you had two waves in the first track,
the outputs would have `track_num` values of 1.1 and 1.2 for each of the two waves.

![alt text](lowers-grid.png "Tracking grid at Lowers")