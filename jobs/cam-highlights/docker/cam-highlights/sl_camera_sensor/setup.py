# -*- coding: utf-8 -*-
from setuptools import find_packages, setup

setup(
    name='sl-camera-sensor',
    version='1.0.0',
    description='Custom Neural Network Models & Post-processing on Surfline Cams',
    author='Ketron Mitchell-Wynne, Gracie Phillips, Ben Freeston',
    author_email='kwynne@surfline.com',
    url='https://github.com/Surfline/surfline-labs',
    packages=find_packages(),
)
