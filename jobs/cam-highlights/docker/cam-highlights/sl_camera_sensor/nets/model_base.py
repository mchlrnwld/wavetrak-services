# Model inference base class. Trying to be agnostic to input types and frameworks.
import os
import tarfile
from abc import ABC, abstractmethod
from typing import List, Union

import numpy as np
import PIL
import tensorflow as tf
import torch
from PIL import Image

from sl_camera_sensor.utils.load_label_map import load_label_map
from sl_camera_sensor.utils.logger import getLogger

global trtorch_import_fail
trtorch_import_error = False
try:
    import trtorch
except ImportError:
    trtorch_import_error = True


class ModelBase(ABC):
    """Each infernce class should inherit from this base class."""

    def __init__(self, device=None):
        # Set frozen graphs directory location
        if "GRAPHS_DIR" in os.environ.keys():
            self.graphs_dir = os.environ["GRAPHS_DIR"]
        else:
            self.graphs_dir = os.path.realpath(
                os.path.join(os.path.dirname(__file__), "../../../graphs/")
            )
            if not os.path.isdir(self.graphs_dir):
                raise ValueError(
                    f"Cannot find graphs directory: {self.graphs_dir}"
                )
        if device is None:
            self.torch_device = (
                torch.device("cuda")
                if torch.cuda.is_available()
                else torch.device("cpu")
            )
        elif device.lower() in ['gpu', 'cuda']:
            if not torch.cuda.is_available():
                raise ValueError('No cuda device found.')
            self.torch_device = torch.device('cuda')
        elif device.lower() == 'cpu':
            self.torch_device = torch.device('cpu')
        else:
            raise ValueError(f'Requested device not understood: {device}')
        self.is_trtorch = False

        self.log = getLogger(__name__)

    @staticmethod
    def _get_model_path(model_name):
        """We need static methods for loading label maps.
        Assuming os.environ['GRAPHS_DIR'] exists.
        """
        graphs_dir = os.environ['GRAPHS_DIR']
        if model_name == 'segmentation':
            return os.path.join(
                graphs_dir, os.environ['SEGMENTATION_MODEL_LOCATION']
            )
        else:
            return os.path.join(
                graphs_dir, os.environ['DETECTION_MODEL_LOCATION']
            )

    @staticmethod
    def _load_label_map(model_name):
        """Often need label maps without loading models. Assumes model is tarred"""
        model_path = ModelBase._get_model_path(model_name).replace(
            'assets', ''
        )  # TODO maybe should fix eventually
        lmf = os.path.join(model_path, "label_map.pbtxt")
        tar = tarfile.open(model_path + '/model.tar.gz', 'r:gz')
        tar.extractall(model_path)
        tar.close()
        if not os.path.isfile(lmf):
            raise ValueError(f'Cannot find label map: {lmf}')
        return load_label_map(lmf)

    def load_model(self, model_name):
        """
        Loads torch, trtorch, tf, tftrt, and xgb models. Everything should
        live in self.graphs_dir/{[SEGMENTATION|OBJECT_DETECTION]_MODEL_LOCATION}.

        Args:
            model_name: segmentation or object_detection

        Returns: Hydrated graph or tree and label_map, tuple
        """

        if model_name == 'segmentation':
            self.model_path = os.path.join(
                self.graphs_dir, os.environ['SEGMENTATION_MODEL_LOCATION']
            )
        else:
            self.model_path = os.path.join(
                self.graphs_dir, os.environ['DETECTION_MODEL_LOCATION']
            )

        model_tar_file = os.path.join(self.model_path, 'model.tar.gz')

        if tarfile.is_tarfile(model_tar_file):
            tar = tarfile.open(self.model_path + '/model.tar.gz', 'r:gz')
            tar.extractall(self.model_path)
            tar.close()
        else:
            raise FileNotFoundError('Model must be a .tar.gz file.')

        for pth, dr, files in os.walk(self.model_path):
            # TWO SCENARIOS
            # 1. We have a directory
            # 2. We have a model file

            # We have a model file
            if not dr:
                model_files = [
                    file
                    for file in files
                    if not file.startswith('._')
                    and file.endswith(('.pt', '.ts', '.pb', '.model'))
                ]
                if len(model_files) == 0:
                    raise ValueError(f'Model not found in: {self.model_path}')
                if len(model_files) > 1:
                    raise ValueError(
                        f'More than 1 model found in: {self.model_path} '
                        f'[{", ".join(model_files)}]'
                    )
                self.model_path = os.path.join(self.model_path, model_files[0])
            # We have a directory
            else:
                self.model_path = os.path.join(self.model_path, dr[0])
            break

        if self.model_path.endswith('.pt'):
            model = torch.jit.load(self.model_path)
        elif self.model_path.endswith('.ts'):
            if trtorch_import_error:
                raise ImportError(
                    'Unable to import trtorch. '
                    'Fix or use a .pt file instead.'
                )
            model = trtorch.torch.jit.load(self.model_path)
            self.is_trtorch = True
        elif self.model_path.endswith('.pb') or os.path.isdir(self.model_path):
            # model = tf.saved_model.load(os.path.dirname(self.model_path))
            # TODO again "assets" bug might need to fix
            model = tf.saved_model.load(self.model_path.replace('assets', ''))
            try:
                model = model.signatures['serving_default']
            except KeyError:
                raise ValueError(
                    f'Your TF export for {self.model_path} '
                    f'has gone wrong - check input and output signatures '
                    'with `saved_model_cli`'
                )
        elif self.model_path.endswith('.model'):
            raise NotImplementedError('Need to add XGB model loading')
        else:
            raise ValueError(f'Model type {self.model_path} not understood.')
        # Check for a label map.
        lmf = os.path.join(os.path.dirname(self.model_path), 'label_map.pbtxt')
        if os.path.isfile(lmf):
            label_map = load_label_map(lmf)
        else:
            lmf = os.path.join(self.model_path, 'label_map.pbtxt')
            if os.path.isfile(lmf):
                label_map = load_label_map(lmf)
            else:
                self.log.info(
                    f'No label map file exists for {self.model_path}'
                )
                label_map = None
        return model, label_map

    @abstractmethod
    def predict(
        self,
        frames: Union[
            np.ndarray,
            str,
            PIL.Image.Image,
            List[np.ndarray],
            List[str],
            List[PIL.Image.Image],
            torch.Tensor,
            tf.Tensor,
        ],
    ) -> Union[np.ndarray, torch.Tensor, tf.Tensor, dict]:
        """
        Public method to pass a batch of frames for
        forward passing through NN or decision tree.
        :frames: strings, lists, np arrays, tensors or lists of any of those.
        :returns: post-processed neural network outputs.
        """

    @abstractmethod
    def preprocess(self, frames):
        """Different networks may require different preprocessing -
        scaling/resizing/normalizing etc.
        :param frames: single np frame, list of frames, PIL.Image instance, tensors etc.
        See your specific model's class for type hints.
        """

    def check_input_torch(self, frames):
        """Given a single frame, list of frames, numpy array, string
        file paths, or torch tensors, make the model class amenable to any.
        """
        is_tensor = False
        if isinstance(frames, np.ndarray):
            if frames.ndim == 3:
                frames = [Image.fromarray(frames.astype(np.uint8))]
            elif frames.ndim == 4:
                frames = [
                    Image.fromarray(frame.astype(np.uint8)) for frame in frames
                ]
            else:
                raise ValueError(f"Input shape not allowed: {frames.shape}")
        elif isinstance(frames, list):
            if len(frames) == 0:
                return []
            if isinstance(frames[0], PIL.Image.Image):
                pass
            elif isinstance(frames[0], np.ndarray):
                frames = [
                    Image.fromarray(frame.astype(np.uint8)) for frame in frames
                ]
            elif isinstance(frames[0], str):
                frames = [Image.open(frame) for frame in frames]
            elif isinstance(frames[0], torch.Tensor):
                raise ValueError(
                    f"You passed a list of {len(frames)} tensors - "
                    "use torch.stack and pass single tensor instead."
                )
        elif isinstance(frames, str):
            frames = [Image.open(frames)]
        elif isinstance(frames, torch.Tensor):
            is_tensor = True
        else:
            raise ValueError(f"Input frames not understood: {type(frames)}")
        # Run the preprocessing and return. Expand dims if only one frame
        if is_tensor and frames.ndim == 3:
            tensor = self.preprocess(frames)
        else:
            tensor = torch.stack([self.preprocess(frame) for frame in frames])
        if tensor.ndim < 4:
            tensor = tensor.view((1, *tensor.shape))
        return tensor.to(self.torch_device)
