# -*- coding: utf-8 -*-
# TensorRT inference with Tensorflow 2.0.
# TRT engines should have been serialized prior to using this class.
#
from typing import Optional

import numpy as np
import tensorflow as tf
from PIL import Image

from sl_camera_sensor.utils import logger

from ..model_base import ModelBase

log = logger.getLogger(__name__)
# tf.logging.set_verbosity(tf.logging.ERROR)


class DetectionModel(ModelBase):
    def __init__(self, batch_size: int = 8, device: Optional[str] = None):
        """
        Constructor to deploy the object detection robot.
        Loads a TRT engine. One can get detections
        from jpg tensors with the .predict() method.

        Inference time on the Titan V for different batch sizes:
        | batch_size, fps |
        -------------------
        10, 81
        20, 85
        30, 87
        36, 86

        So I'm using batch size = 10 by default to conserve memory.

        :param model: Choose from any of the dirs at
        science_ml_models/cameras/object_detection_models/
        This points to the directory where the serialized TRT engines live
        :batch_size: this is TRT inference, which runs on pre-serialized
        sub-graph enginges. Generating these engines can take a while,
        so if you're streaming you need to use the same batch size
        as was used during engine serialization, or else TRT will
        build a new engine based on this batch size. These engines are
        also GPU-dependent.
        """
        super().__init__()
        self.batch_size = batch_size
        if device is None:
            # No input device - check there aren't any virtual ones configured
            # since this is such a large model.
            gpus = tf.config.experimental.list_physical_devices('GPU')
            tf.config.experimental.set_memory_growth(
                gpus[0], True
            )  # Done in gpu_config
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            if len(logical_gpus) > len(gpus):
                raise RuntimeError(
                    f'There are {len(logical_gpus)} virtual GPUs '
                    'configured but none passed to the object '
                    'detection ctor. This will likely cause '
                    'memory faults.'
                )
            # If there is no input device we'll take up the entire GPU by
            # default, so just set the device to be the physical device.

            self.device = '/' + gpus[0].name.split('/')[-1].replace(
                'physical_device:', ''
            )
            log.info(f'Using full device: {self.device}')
        else:
            self.device = device
            log.info(f'Using input device: {self.device}')

        # Load model via base class, after device is set
        self.model, self.label_map = self.load_model('object_detection')

    @staticmethod
    def get_label_map():
        return ModelBase._load_label_map("detection")

    def preprocess(self):
        pass

    def predict(self, image_list_or_tensor):
        """Forward inference on trained network: pass in a list of images,
        (or tensor or numpy array) and get back a list of detection dicts.

        :param image_list_or_tensor: list of images, np array, or tensor,
        ordered as RGB and NHWC. Should be uint8 data type.
        :return: list of detection dictionaries, same size as the first axis
        in the input.
        The number of detections you get per tensor is a function of
        the secondary post-processing parameters you set in the
        pipeline.config when the model was trained. Default is 160 I think.

        Example:
        >>> bot = detection_model.DetectionModel
        >>> detections = bot.predict([frame1, frame2, ..., frame15000])

        Or to reduce preprocessing time (which is minimial):
        >>> tensor = tf.convert_to_tensor(
            np.vstack([np.expand_dims(im, 0) for im in image_list])
        )
        >>> detections = bot.predict(tensor)
        """
        with tf.device(self.device):

            if tf.is_tensor(image_list_or_tensor):
                input_tensor = image_list_or_tensor
            else:
                # Input is np array or list?
                if isinstance(image_list_or_tensor, list):
                    input_tensor = self._preprocess_list(image_list_or_tensor)

                elif isinstance(image_list_or_tensor, np.ndarray):
                    # This can be super slow if you feed it a list instead of a np array
                    # https://github.com/tensorflow/tensorflow/issues/27692
                    input_tensor = tf.constant(np.array(image_list_or_tensor))
                else:
                    raise ValueError('Input list, 4d numpy array, or tensor.')

            # Check dtype
            if input_tensor.dtype != tf.uint8:
                log.critical(
                    "Input should be UINT8 but instead is "
                    f'{input_tensor.dtype}'
                )
                input_tensor = tf.cast(input_tensor, tf.uint8)

            if input_tensor.shape[0] == 0:
                log.info(
                    "You passed an empty input and "
                    "that's what you're getting back."
                )
                return []

            # Run inference in batches
            output_dicts = []
            Nframes = input_tensor.shape[0]
            for i in range(
                Nframes // self.batch_size + np.sign(Nframes % self.batch_size)
            ):

                tensor_batch = input_tensor[
                    i
                    * self.batch_size : (i + 1)  # noqa: E203
                    * self.batch_size
                ]

                # Pad if this batch is less than the batch size. Or else
                # TRT will build a new engine on-the-fly.
                dbatch = self.batch_size - tensor_batch.shape[0]
                if dbatch > 0:
                    zeros = tf.zeros(
                        (dbatch,) + tensor_batch.shape[1:], dtype=tf.uint8
                    )
                    tensor_batch = tf.concat([tensor_batch, zeros], 0)

                # TensorRT forward pass
                print('pre-run')
                output_dict = self.model(tensor_batch)
                print('post-run')

                # Post processing of detection dicts
                output_dicts_batch = self._postprocess(
                    output_dict, len(tensor_batch) - dbatch
                )
                output_dicts += output_dicts_batch

        return output_dicts

    def _preprocess_list(self, image_list):
        """Add extra axis to image list, vstack, and
        convert to tensor.
        """
        # This is bad, don't give me a string list
        if isinstance(image_list[0], str):
            uh = []
            for im in image_list:
                im = Image.open(im)
                if im.size != (1280, 720):
                    im = im.resize((1280, 720), Image.BILINEAR)
                uh.append(np.array(im))
            image_list = uh

        # Check all the images are the same size first!
        sz = np.array([im.shape for im in image_list])
        if not np.all(sz[0] == sz):
            log.critical('Not all input frames are the same shape:')
            log.critical(f'  {np.unique(sz)}')
            return []  # predict passes back a list of dicts

        return tf.constant(
            np.vstack([np.expand_dims(im, 0) for im in image_list])
        )

    def _postprocess(self, output_dict, n_batch):
        """Unwrap batch-processed output_dict to be one dict per image,
        instead of keys grouped in batches.

        If you process a batch of 200 frames, you'll get back
        a single dict. We need one dict per frame. Unpack,
        and add appropriate offsets to account for cropping, if
        necessary. All outputs are float32 numpy arrays, so
        convert types as appropriate.

        :param output_dict: tensorflow inference output
        :param n_batch:
        :return:
        """
        out_dict_list = []
        for ibatch in range(n_batch):
            idict = {}
            for k, v in list(output_dict.items()):
                if k == 'num_detections':
                    idict[k] = int(output_dict[k][ibatch].numpy())
                elif k == 'detection_classes':
                    idict[k] = output_dict[k][ibatch].numpy().astype(np.uint8)
                else:
                    idict[k] = (
                        output_dict[k][ibatch].numpy().astype(np.float16)
                    )
            out_dict_list.append(idict)
        return out_dict_list
