# Surface Texture Model Infernce

## Texture Classes
Currently we have a 5 class model - `clean`:0, `lightly affected`:1, `moderately affected`:2,
`strongly affected`:3, `victory at sea`:4. We can potentially add one more class to this
depending on how it looks over a few weeks time.

The inference class will return softmax values from a two layer convolutional classifier.
I think this makes more sense than looking at logits, but TBD.

## Running
Run this the same as any other network model API:
```python
tex_mod = sl_camera_sensor.nets.texture.texture_model.TextureModel()
frame = np.random.randint(low=0, high=255, size=(1, 720, 1280, 3))
preds = tex_mod.predict(frame)
```
Torch convention is NCHW but you can pass NHWC frames, as above, and the inference class will
roll the axes for you.

I think this needs to be run only when there aren't breaking waves in the scene,
at least for classes 0/1. If you're on the HB office VPN, see [here](http://192.168.101.50/surface_texture_test/).