from typing import List, Optional, Union

import numpy as np
import PIL
import torch
import torchvision.transforms as transforms

from ..model_base import ModelBase


class TextureModel(ModelBase):
    def __init__(
        self,
        model_name: Optional[str] = "texture-model-tdv0.pt",
        target_size: Optional[tuple] = (576, 1024),
        device='cuda',
    ) -> None:
        """
        :param model_name: name of the actual jit script file. This will look,
        via the base class, in $GRAPHS_DIR/texture/{model_name}.
        """
        super().__init__(device=device)
        self.model, self.label_map = self.load_model("texture", model_name)
        self.target_size = target_size
        self._sx = torch.nn.Softmax(dim=1)
        self.map_label = {v: k for k, v in self.label_map.items()}

    def predict(
        self,
        frames: Union[
            np.ndarray,
            str,
            PIL.Image.Image,
            List[np.ndarray],
            List[str],
            List[PIL.Image.Image],
            torch.Tensor,
        ],
    ) -> np.ndarray:
        tensors = self.check_input_torch(frames)
        with torch.no_grad():
            p = self.model.forward(tensors)
        s = self._sx(p).cpu().numpy().astype(np.float16)
        return s

    def preprocess(
        self, frame: Union[torch.Tensor, PIL.Image.Image]
    ) -> torch.Tensor:
        """Called from base class, which only passes PIL or torch.Tensor
        types.
        """
        if isinstance(frame, PIL.Image.Image):
            transf = transforms.Compose(
                [
                    transforms.Resize(self.target_size),
                    transforms.ToTensor(),
                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                ]
            )(frame)
        else:
            transf = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))(
                frame
            )
        return transf.half() if self.is_trtorch else transf

    def get_pred_string(self, preds: Union[torch.Tensor, np.ndarray]) -> str:
        """Input some predictions, perhaps averaged over time, and
        get back some sensible string from the label map
        """
        if isinstance(preds, torch.Tensor):
            preds = preds.cpu().numpy()
        elif not isinstance(preds, np.ndarray):
            raise ValueError(
                f'Input torch tensor or np array only, not {type(preds)}'
            )
        if preds.ndim == 2:
            preds = preds.mean(axis=0)
        elif preds.ndim != 1:
            raise ValueError(
                f"I don't understand your input shape: ({preds.shape})"
            )
        # Add these things in order of probability - 'clean/light' is
        # better than 'light/clean' for example.
        txt = []
        for arg in np.argsort(preds)[::-1]:
            if preds[arg] > 0.3:
                txt.append(self.map_label[arg])
        return 'uncertain' if 0 == len(txt) >= 3 else '/'.join(txt)
